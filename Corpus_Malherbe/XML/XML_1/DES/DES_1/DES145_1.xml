<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>9618 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>9618 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies</title>
					<author>Marceline Desbordes-Valmore</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>A. Boulland, libraire-éditeur</publisher>
						<date when="1830">1830</date>
					</imprint>
					<extent>9618 verstome second</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-12-20" who="RR">Insertion des corrections métriques dans le corps du texte.</change>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
				<change when="2018-11-02" who="RR">Correction dans le poème "Dors, ma Mère".</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">POÉSIES DIVERSES</head><div type="poem" key="DES145">
						<head type="main">LES DEUX ABEILLES</head>
						<head type="sub_1">À MON ONCLE</head>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">Au</w> <w n="1.2">fond</w> <w n="1.3">d</w>’<w n="1.4">une</w> <w n="1.5">vallée</w> <w n="1.6">où</w> <w n="1.7">s</w>’<w n="1.8">éveillaient</w> <w n="1.9">les</w> <w n="1.10">fleurs</w>,</l>
							<l n="2" num="1.2"><w n="2.1">On</w> <w n="2.2">vit</w> <w n="2.3">légèrement</w> <w n="2.4">descendre</w> <w n="2.5">deux</w> <w n="2.6">abeilles</w> ;</l>
							<l n="3" num="1.3"><w n="3.1">Elles</w> <w n="3.2">cherchaient</w> <w n="3.3">des</w> <w n="3.4">yeux</w> <w n="3.5">ces</w> <w n="3.6">fleurs</w>, <w n="3.7">tendres</w> <w n="3.8">merveilles</w>,</l>
							<l n="4" num="1.4"><w n="4.1">Où</w> <w n="4.2">l</w>’<w n="4.3">aurore</w> <w n="4.4">en</w> <w n="4.5">passant</w> <w n="4.6">avait</w> <w n="4.7">laissé</w> <w n="4.8">des</w> <w n="4.9">pleurs</w>.</l>
							<l n="5" num="1.5"><space quantity="2" unit="char"></space><w n="5.1">L</w>’<w n="5.2">herbe</w> <w n="5.3">brillait</w> <w n="5.4">de</w> <w n="5.5">perles</w> <w n="5.6">arrosée</w> ;</l>
							<l n="6" num="1.6"><space quantity="2" unit="char"></space><w n="6.1">L</w>’<w n="6.2">horizon</w> <w n="6.3">bleu</w>, <w n="6.4">les</w> <w n="6.5">gouttes</w> <w n="6.6">de</w> <w n="6.7">rosée</w>,</l>
							<l n="7" num="1.7"><space quantity="2" unit="char"></space><w n="7.1">Sur</w> <w n="7.2">la</w> <w n="7.3">colline</w> <w n="7.4">une</w> <w n="7.5">ardente</w> <w n="7.6">clarté</w>,</l>
							<l n="8" num="1.8"><space quantity="2" unit="char"></space><w n="8.1">Tout</w> <w n="8.2">annonçait</w> <w n="8.3">un</w> <w n="8.4">jour</w> <w n="8.5">brûlant</w> <w n="8.6">d</w>’<w n="8.7">été</w>,</l>
							<l n="9" num="1.9"><space quantity="2" unit="char"></space><w n="9.1">Tout</w> <w n="9.2">l</w>’<w n="9.3">attestait</w> ; <w n="9.4">car</w> <w n="9.5">un</w> <w n="9.6">jardin</w> <w n="9.7">rustique</w></l>
							<l n="10" num="1.10"><w n="10.1">Répandait</w> <w n="10.2">à</w> <w n="10.3">l</w>’<w n="10.4">entour</w> <w n="10.5">des</w> <w n="10.6">deux</w> <w n="10.7">errantes</w> <w n="10.8">sœurs</w></l>
							<l n="11" num="1.11"><space quantity="2" unit="char"></space><w n="11.1">De</w> <w n="11.2">frais</w> <w n="11.3">parfums</w>, <w n="11.4">d</w>’<w n="11.5">attrayantes</w> <w n="11.6">douceurs</w>,</l>
							<l n="12" num="1.12"><w n="12.1">Et</w> <w n="12.2">d</w>’<w n="12.3">un</w> <w n="12.4">souffle</w> <w n="12.5">embaumé</w> <w n="12.6">la</w> <w n="12.7">langueur</w> <w n="12.8">sympathique</w>.</l>
							<l n="13" num="1.13"><w n="13.1">Toutes</w> <w n="13.2">deux</w> <w n="13.3">ont</w> <w n="13.4">franchi</w> <w n="13.5">l</w>’<w n="13.6">enclos</w> <w n="13.7">vert</w> <w n="13.8">du</w> <w n="13.9">jardin</w> :</l>
							<l n="14" num="1.14">« <w n="14.1">Voyez</w> ! » <w n="14.2">dit</w> <w n="14.3">la</w> <w n="14.4">plus</w> <w n="14.5">vive</w>, <w n="14.6">elle</w> <w n="14.7">était</w> <w n="14.8">frêle</w> <w n="14.9">et</w> <w n="14.10">blonde</w>,</l>
							<l n="15" num="1.15">« <w n="15.1">Voyez</w> <w n="15.2">que</w> <w n="15.3">de</w> <w n="15.4">trésors</w> ! <w n="15.5">ce</w> <w n="15.6">n</w>’<w n="15.7">est</w> <w n="15.8">rien</w> <w n="15.9">que</w> <w n="15.10">jasmin</w>,</l>
							<l n="16" num="1.16"><w n="16.1">Lilas</w>, <w n="16.2">rose</w>, <w n="16.3">et</w> <w n="16.4">je</w> <w n="16.5">crois</w> <w n="16.6">toutes</w> <w n="16.7">les</w> <w n="16.8">fleurs</w> <w n="16.9">du</w> <w n="16.10">monde</w>. »</l>
							<l n="17" num="1.17"><w n="17.1">Cette</w> <w n="17.2">folle</w> <w n="17.3">suivait</w> <w n="17.4">son</w> <w n="17.5">volage</w> <w n="17.6">désir</w>,</l>
							<l n="18" num="1.18"><w n="18.1">Aux</w> <w n="18.2">suaves</w> <w n="18.3">bouquets</w> <w n="18.4">se</w> <w n="18.5">suspendait</w> <w n="18.6">à</w> <w n="18.7">peine</w>,</l>
							<l n="19" num="1.19"><w n="19.1">Prodiguant</w> <w n="19.2">ses</w> <w n="19.3">baisers</w> <w n="19.4">jusqu</w>’<w n="19.5">à</w> <w n="19.6">manquer</w> <w n="19.7">d</w>’<w n="19.8">haleine</w>,</l>
							<l n="20" num="1.20"><w n="20.1">Disant</w> : « <w n="20.2">Demain</w> <w n="20.3">le</w> <w n="20.4">miel</w>, <w n="20.5">aujourd</w>’<w n="20.6">hui</w> <w n="20.7">le</w> <w n="20.8">plaisir</w> ! »</l>
						</lg>
						<lg n="2">
							<l n="21" num="2.1"><w n="21.1">L</w>’<w n="21.2">autre</w>, <w n="21.3">plus</w> <w n="21.4">posément</w>, <w n="21.5">savourait</w> <w n="21.6">les</w> <w n="21.7">délices</w></l>
							<l n="22" num="2.2"><w n="22.1">Du</w> <w n="22.2">banquet</w> <w n="22.3">préparé</w> <w n="22.4">pour</w> <w n="22.5">les</w> <w n="22.6">filles</w> <w n="22.7">de</w> <w n="22.8">l</w>’<w n="22.9">air</w>,</l>
							<l n="23" num="2.3"><space quantity="2" unit="char"></space><w n="23.1">Et</w>, <w n="23.2">prévoyante</w> <w n="23.3">aux</w> <w n="23.4">besoins</w> <w n="23.5">de</w> <w n="23.6">l</w>’<w n="23.7">hiver</w>,</l>
							<l n="24" num="2.4"><w n="24.1">Pour</w> <w n="24.2">la</w> <w n="24.3">ruche</w> <w n="24.4">épuisée</w> <w n="24.5">en</w> <w n="24.6">gardait</w> <w n="24.7">les</w> <w n="24.8">prémices</w>.</l>
							<l n="25" num="2.5"><w n="25.1">Leurs</w> <w n="25.2">ailes</w> <w n="25.3">en</w> <w n="25.4">tremblaient</w> ; <w n="25.5">mais</w> <w n="25.6">un</w> <w n="25.7">globe</w> <w n="25.8">fatal</w>,</l>
							<l n="26" num="2.6"><w n="26.1">Suspendu</w> <w n="26.2">dans</w> <w n="26.3">les</w> <w n="26.4">fleurs</w> <w n="26.5">sous</w> <w n="26.6">la</w> <w n="26.7">méridienne</w>,</l>
							<l n="27" num="2.7"><w n="27.1">Semble</w> <w n="27.2">de</w> <w n="27.3">l</w>’<w n="27.4">ambroisie</w> <w n="27.5">offrir</w> <w n="27.6">le</w> <w n="27.7">doux</w> <w n="27.8">régal</w></l>
							<l n="28" num="2.8"><space quantity="6" unit="char"></space><w n="28.1">À</w> <w n="28.2">la</w> <w n="28.3">jeune</w> <w n="28.4">épicurienne</w>.</l>
							<l n="29" num="2.9"><w n="29.1">Sous</w> <w n="29.2">ce</w> <w n="29.3">cristal</w> <w n="29.4">frappé</w> <w n="29.5">de</w> <w n="29.6">tous</w> <w n="29.7">les</w> <w n="29.8">feux</w> <w n="29.9">du</w> <w n="29.10">ciel</w>,</l>
							<l n="30" num="2.10"><space quantity="6" unit="char"></space><w n="30.1">S</w>’<w n="30.2">échauffe</w> <w n="30.3">et</w> <w n="30.4">fermente</w> <w n="30.5">le</w> <w n="30.6">miel</w>,</l>
							<l n="31" num="2.11"><w n="31.1">Innocente</w> <w n="31.2">liqueur</w> <w n="31.3">pour</w> <w n="31.4">l</w>’<w n="31.5">homme</w> <w n="31.6">préparée</w>,</l>
							<l n="32" num="2.12"><w n="32.1">Mais</w> <w n="32.2">qui</w> <w n="32.3">donne</w> <w n="32.4">la</w> <w n="32.5">mort</w> <w n="32.6">à</w> <w n="32.7">la</w> <w n="32.8">mouche</w> <w n="32.9">dorée</w> ;</l>
							<l n="33" num="2.13"><w n="33.1">Sa</w> <w n="33.2">force</w> <w n="33.3">s</w>’<w n="33.4">y</w> <w n="33.5">consume</w>, <w n="33.6">et</w> <w n="33.7">sa</w> <w n="33.8">raison</w> <w n="33.9">s</w>’<w n="33.10">y</w> <w n="33.11">perd</w>.</l>
							<l n="34" num="2.14"><w n="34.1">L</w>’<w n="34.2">abîme</w> <w n="34.3">transparent</w> <w n="34.4">par</w> <w n="34.5">malheur</w> <w n="34.6">est</w> <w n="34.7">ouvert</w> ;</l>
							<l n="35" num="2.15"><w n="35.1">L</w>’<w n="35.2">imprudente</w> <w n="35.3">n</w>’<w n="35.4">y</w> <w n="35.5">voit</w> <w n="35.6">qu</w>’<w n="35.7">un</w> <w n="35.8">don</w> <w n="35.9">de</w> <w n="35.10">la</w> <w n="35.11">fortune</w> ;</l>
							<l n="36" num="2.16"><w n="36.1">Sa</w> <w n="36.2">sœur</w>, <w n="36.3">qui</w> <w n="36.4">l</w>’<w n="36.5">en</w> <w n="36.6">détourne</w>, <w n="36.7">est</w> <w n="36.8">presqu</w>’<w n="36.9">une</w> <w n="36.10">importune</w>,</l>
							<l n="37" num="2.17"><w n="37.1">Et</w>, <w n="37.2">malgré</w> <w n="37.3">ses</w> <w n="37.4">conseils</w>, <w n="37.5">elle</w> <w n="37.6">court</w> <w n="37.7">s</w>’<w n="37.8">y</w> <w n="37.9">plonger</w> :</l>
							<l n="38" num="2.18"><w n="38.1">Quand</w> <w n="38.2">on</w> <w n="38.3">veut</w> <w n="38.4">le</w> <w n="38.5">bonheur</w>, <w n="38.6">en</w> <w n="38.7">voit</w>-<w n="38.8">on</w> <w n="38.9">le</w> <w n="38.10">danger</w> ?</l>
							<l n="39" num="2.19">« <w n="39.1">Par</w> <w n="39.2">quel</w> <w n="39.3">charme</w> <w n="39.4">imposteur</w> <w n="39.5">vous</w> <w n="39.6">êtes</w> <w n="39.7">asservie</w>,</l>
							<l n="40" num="2.20"><w n="40.1">Dit</w> <w n="40.2">l</w>’<w n="40.3">autre</w> <w n="40.4">en</w> <w n="40.5">soupirant</w> ; <w n="40.6">vous</w> <w n="40.7">me</w> <w n="40.8">faites</w> <w n="40.9">pitié</w> ;</l>
							<l n="41" num="2.21"><w n="41.1">Quittez</w> <w n="41.2">ce</w> <w n="41.3">doux</w> <w n="41.4">breuvage</w>, <w n="41.5">au</w> <w n="41.6">nom</w> <w n="41.7">de</w> <w n="41.8">l</w>’<w n="41.9">amitié</w>,</l>
							<l n="42" num="2.22"><space quantity="2" unit="char"></space><w n="42.1">Peut</w>-<w n="42.2">être</w>, <w n="42.3">hélas</w> ! <w n="42.4">au</w> <w n="42.5">nom</w> <w n="42.6">de</w> <w n="42.7">votre</w> <w n="42.8">vie</w> !</l>
							<l n="43" num="2.23"><w n="43.1">Vous</w> <w n="43.2">ne</w> <w n="43.3">m</w>’<w n="43.4">écoutez</w> <w n="43.5">pas</w>. <w n="43.6">Je</w> <w n="43.7">reviendrai</w> <w n="43.8">ce</w> <w n="43.9">soir</w>.</l>
							<l n="44" num="2.24"><w n="44.1">Ô</w> <w n="44.2">ma</w> <w n="44.3">sœur</w> ! <w n="44.4">le</w> <w n="44.5">travail</w> <w n="44.6">est</w> <w n="44.7">utile</w> <w n="44.8">à</w> <w n="44.9">notre</w> <w n="44.10">âge</w>.</l>
							<l n="45" num="2.25"><w n="45.1">Puissé</w>-<w n="45.2">je</w> <w n="45.3">ne</w> <w n="45.4">pas</w> <w n="45.5">voir</w> <w n="45.6">bientôt</w>, <w n="45.7">chère</w> <w n="45.8">volage</w>,</l>
							<l n="46" num="2.26"><space quantity="6" unit="char"></space><w n="46.1">Ce</w> <w n="46.2">que</w> <w n="46.3">je</w> <w n="46.4">tremble</w> <w n="46.5">de</w> <w n="46.6">prévoir</w>. »</l>
						</lg>
						<lg n="3">
							<l n="47" num="3.1"><w n="47.1">Elle</w> <w n="47.2">retourne</w> <w n="47.3">aux</w> <w n="47.4">fleurs</w> <w n="47.5">avec</w> <w n="47.6">inquiétude</w>.</l>
							<l n="48" num="3.2"><w n="48.1">Ce</w> <w n="48.2">beau</w> <w n="48.3">jour</w> <w n="48.4">lui</w> <w n="48.5">paraît</w> <w n="48.6">plus</w> <w n="48.7">lent</w> <w n="48.8">qu</w>’<w n="48.9">un</w> <w n="48.10">autre</w> <w n="48.11">jour</w> ;</l>
							<l n="49" num="3.3"><w n="49.1">Tout</w> <w n="49.2">suc</w> <w n="49.3">lui</w> <w n="49.4">semble</w> <w n="49.5">amer</w>, <w n="49.6">et</w> <w n="49.7">sa</w> <w n="49.8">sollicitude</w></l>
							<l n="50" num="3.4"><w n="50.1">Implore</w>, <w n="50.2">et</w> <w n="50.3">croit</w> <w n="50.4">du</w> <w n="50.5">soir</w> <w n="50.6">avancer</w> <w n="50.7">le</w> <w n="50.8">retour</w>.</l>
						</lg>
						<lg n="4">
							<l n="51" num="4.1"><w n="51.1">Enfin</w> <w n="51.2">à</w> <w n="51.3">l</w>’<w n="51.4">horizon</w> <w n="51.5">le</w> <w n="51.6">soleil</w> <w n="51.7">va</w> <w n="51.8">s</w>’<w n="51.9">éteindre</w> ;</l>
							<l n="52" num="4.2"><w n="52.1">Elle</w> <w n="52.2">vole</w> <w n="52.3">à</w> <w n="52.4">sa</w> <w n="52.5">sœur</w>, <w n="52.6">et</w>, <w n="52.7">tout</w> <w n="52.8">près</w> <w n="52.9">de</w> <w n="52.10">l</w>’<w n="52.11">atteindre</w>,</l>
							<l n="53" num="4.3"><w n="53.1">L</w>’<w n="53.2">appelle</w> <w n="53.3">en</w> <w n="53.4">la</w> <w n="53.5">grondant</w> <w n="53.6">d</w>’<w n="53.7">un</w> <w n="53.8">ton</w> <w n="53.9">craintif</w> <w n="53.10">et</w> <w n="53.11">doux</w> :</l>
							<l n="54" num="4.4">« <w n="54.1">Allons</w>, <w n="54.2">il</w> <w n="54.3">se</w> <w n="54.4">fait</w> <w n="54.5">tard</w> ; <w n="54.6">me</w> <w n="54.7">voici</w>, <w n="54.8">venez</w>-<w n="54.9">vous</w> ?</l>
						</lg>
						<lg n="5">
							<l n="55" num="5.1">— <w n="55.1">Il</w> <w n="55.2">n</w>’<w n="55.3">est</w> <w n="55.4">plus</w> <w n="55.5">temps</w>, <w n="55.6">ma</w> <w n="55.7">sœur</w>, <w n="55.8">je</w> <w n="55.9">suis</w> <w n="55.10">trop</w> <w n="55.11">accablée</w> ;</l>
							<l n="56" num="5.2"><space quantity="2" unit="char"></space><w n="56.1">Je</w> <w n="56.2">ne</w> <w n="56.3">puis</w> <w n="56.4">plus</w> <w n="56.5">me</w> <w n="56.6">sauver</w> <w n="56.7">de</w> <w n="56.8">ce</w> <w n="56.9">lieu</w>.</l>
							<l n="57" num="5.3"><w n="57.1">Je</w> <w n="57.2">vous</w> <w n="57.3">regarde</w> <w n="57.4">encor</w>, <w n="57.5">mais</w> <w n="57.6">ma</w> <w n="57.7">vue</w> <w n="57.8">est</w> <w n="57.9">troublée</w> ;</l>
							<l n="58" num="5.4"><w n="58.1">Mon</w> <w n="58.2">corps</w> <w n="58.3">brûle</w> <w n="58.4">et</w> <w n="58.5">languit</w> ; <w n="58.6">venez</w> <w n="58.7">me</w> <w n="58.8">dire</w> <w n="58.9">adieu</w>,</l>
							<l n="59" num="5.5"><w n="59.1">Je</w> <w n="59.2">ne</w> <w n="59.3">puis</w> <w n="59.4">me</w> <w n="59.5">mouvoir</w>. <w n="59.6">Un</w> <w n="59.7">grand</w> <w n="59.8">feu</w> <w n="59.9">me</w> <w n="59.10">dévore</w> :</l>
							<l n="60" num="5.6"><w n="60.1">Mes</w> <w n="60.2">ailes</w>, <w n="60.3">je</w> <w n="60.4">le</w> <w n="60.5">sens</w>, <w n="60.6">ne</w> <w n="60.7">peuvent</w> <w n="60.8">m</w>’<w n="60.9">emporter</w> ;</l>
							<l n="61" num="5.7"><w n="61.1">Voyez</w> <w n="61.2">comme</w> <w n="61.3">je</w> <w n="61.4">suis</w> ! <w n="61.5">mais</w> <w n="61.6">soyez</w> <w n="61.7">bonne</w> <w n="61.8">encore</w> ;</l>
							<l n="62" num="5.8"><w n="62.1">Si</w> <w n="62.2">mon</w> <w n="62.3">crime</w> (<w n="62.4">il</w> <w n="62.5">est</w> <w n="62.6">grand</w> !) <w n="62.7">ne</w> <w n="62.8">peut</w> <w n="62.9">se</w> <w n="62.10">racheter</w>,</l>
							<l n="63" num="5.9"><w n="63.1">Ne</w> <w n="63.2">me</w> <w n="63.3">haïssez</w> <w n="63.4">pas</w>, <w n="63.5">je</w> <w n="63.6">n</w>’<w n="63.7">étais</w> <w n="63.8">pas</w> <w n="63.9">méchante</w> :</l>
							<l n="64" num="5.10"><w n="64.1">La</w> <w n="64.2">volupté</w> <w n="64.3">trompeuse</w> <w n="64.4">égarait</w> <w n="64.5">ma</w> <w n="64.6">raison</w> ;</l>
							<l n="65" num="5.11"><w n="65.1">Ce</w> <w n="65.2">breuvage</w> <w n="65.3">mortel</w> <w n="65.4">dont</w> <w n="65.5">l</w>’<w n="65.6">ardeur</w> <w n="65.7">nous</w> <w n="65.8">enchante</w>,</l>
							<l n="66" num="5.12"><w n="66.1">Que</w> <w n="66.2">je</w> <w n="66.3">l</w>’<w n="66.4">aimais</w>, <w n="66.5">ma</w> <w n="66.6">sœur</w> ! <w n="66.7">et</w> <w n="66.8">c</w>’<w n="66.9">était</w> <w n="66.10">un</w> <w n="66.11">poison</w> !</l>
							<l n="67" num="5.13"><space quantity="6" unit="char"></space><w n="67.1">Je</w> <w n="67.2">me</w> <w n="67.3">repens</w>, <w n="67.4">et</w> <w n="67.5">je</w> <w n="67.6">succombe</w>.</l>
							<l n="68" num="5.14"><space quantity="6" unit="char"></space><w n="68.1">Sous</w> <w n="68.2">une</w> <w n="68.3">fleur</w> <w n="68.4">creusez</w> <w n="68.5">ma</w> <w n="68.6">tombe</w>.</l>
							<l n="69" num="5.15"><w n="69.1">Adieu</w> ! <w n="69.2">Pourquoi</w> <w n="69.3">le</w> <w n="69.4">ciel</w> <w n="69.5">créa</w>-<w n="69.6">t</w>-<w n="69.7">il</w> <w n="69.8">le</w> <w n="69.9">désir</w>,</l>
							<l n="70" num="5.16"><space quantity="2" unit="char"></space><w n="70.1">S</w>’<w n="70.2">il</w> <w n="70.3">a</w> <w n="70.4">caché</w> <w n="70.5">la</w> <w n="70.6">mort</w> <w n="70.7">dans</w> <w n="70.8">le</w> <w n="70.9">plaisir</w> ? »</l>
							<l n="71" num="5.17"><w n="71.1">Elle</w> <w n="71.2">ne</w> <w n="71.3">parle</w> <w n="71.4">plus</w>. <w n="71.5">Ses</w> <w n="71.6">ailes</w> <w n="71.7">s</w>’<w n="71.8">étendirent</w>,</l>
							<l n="72" num="5.18"><space quantity="2" unit="char"></space><w n="72.1">Ses</w> <w n="72.2">petits</w> <w n="72.3">pieds</w> <w n="72.4">doucement</w> <w n="72.5">se</w> <w n="72.6">raidirent</w> ;</l>
							<l n="73" num="5.19"><w n="73.1">Et</w> <w n="73.2">sa</w> <w n="73.3">sœur</w> <w n="73.4">gémissante</w> <w n="73.5">eut</w> <w n="73.6">peine</w> <w n="73.7">à</w> <w n="73.8">s</w>’<w n="73.9">envoler</w>.</l>
							<l n="74" num="5.20"><w n="74.1">Ce</w> <w n="74.2">tableau</w> <w n="74.3">d</w>’<w n="74.4">un</w> <w n="74.5">long</w> <w n="74.6">deuil</w> <w n="74.7">accabla</w> <w n="74.8">sa</w> <w n="74.9">mémoire</w> ;</l>
							<l n="75" num="5.21"><w n="75.1">Elle</w> <w n="75.2">fut</w> <w n="75.3">toujours</w> <w n="75.4">triste</w>, <w n="75.5">et</w> <w n="75.6">jamais</w>, <w n="75.7">dit</w> <w n="75.8">l</w>’<w n="75.9">histoire</w>,</l>
							<l n="76" num="5.22"><w n="76.1">Même</w> <w n="76.2">au</w> <w n="76.3">sein</w> <w n="76.4">du</w> <w n="76.5">travail</w> <w n="76.6">ne</w> <w n="76.7">put</w> <w n="76.8">se</w> <w n="76.9">consoler</w>.</l>
						</lg>
					</div></body></text></TEI>