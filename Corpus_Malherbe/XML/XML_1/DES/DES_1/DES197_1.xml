<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>9618 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>9618 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies</title>
					<author>Marceline Desbordes-Valmore</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>A. Boulland, libraire-éditeur</publisher>
						<date when="1830">1830</date>
					</imprint>
					<extent>9618 verstome second</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-12-20" who="RR">Insertion des corrections métriques dans le corps du texte.</change>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
				<change when="2018-11-02" who="RR">Correction dans le poème "Dors, ma Mère".</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part"> POÉSIES INÉDITES <lb></lb>ROMANCES</head><div type="poem" key="DES197">
						<head type="main">LE BON ERMITE</head>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">Ermite</w>, <w n="1.2">votre</w> <w n="1.3">chapelle</w></l>
							<l n="2" num="1.2"><w n="2.1">S</w>’<w n="2.2">ouvre</w>-<w n="2.3">t</w>-<w n="2.4">elle</w> <w n="2.5">au</w> <w n="2.6">malheureux</w> !</l>
							<l n="3" num="1.3"><w n="3.1">Hélas</w> ! <w n="3.2">elle</w> <w n="3.3">me</w> <w n="3.4">rappelle</w></l>
							<l n="4" num="1.4"><w n="4.1">Un</w> <w n="4.2">temps</w> <w n="4.3">cher</w> <w n="4.4">et</w> <w n="4.5">douloureux</w> !</l>
							<l n="5" num="1.5"><w n="5.1">C</w>’<w n="5.2">est</w> <w n="5.3">moi</w>… <w n="5.4">de</w> <w n="5.5">votre</w> <w n="5.6">colère</w></l>
							<l n="6" num="1.6"><w n="6.1">Les</w> <w n="6.2">éclats</w> <w n="6.3">sont</w> <w n="6.4">superflus</w> ;</l>
							<l n="7" num="1.7"><w n="7.1">Un</w> <w n="7.2">autre</w> <w n="7.3">que</w> <w n="7.4">vous</w> <w n="7.5">m</w>’<w n="7.6">éclaire</w> :</l>
							<l n="8" num="1.8"><w n="8.1">Mon</w> <w n="8.2">père</w>, <w n="8.3">il</w> <w n="8.4">ne</w> <w n="8.5">m</w>’<w n="8.6">aime</w> <w n="8.7">plus</w> !</l>
						</lg>
						<lg n="2">
							<l n="9" num="2.1"><w n="9.1">Cette</w> <w n="9.2">jeune</w> <w n="9.3">infortunée</w></l>
							<l n="10" num="2.2"><w n="10.1">Que</w> <w n="10.2">vous</w> <w n="10.3">maudites</w> <w n="10.4">un</w> <w n="10.5">jour</w>,</l>
							<l n="11" num="2.3"><w n="11.1">Qui</w> <w n="11.2">devant</w> <w n="11.3">vous</w> <w n="11.4">prosternée</w>,</l>
							<l n="12" num="2.4"><w n="12.1">Osa</w> <w n="12.2">défendre</w> <w n="12.3">l</w>’<w n="12.4">amour</w>,</l>
							<l n="13" num="2.5"><w n="13.1">C</w>’<w n="13.2">est</w> <w n="13.3">moi</w>, <w n="13.4">faible</w> <w n="13.5">pénitente</w></l>
							<l n="14" num="2.6"><w n="14.1">Dans</w> <w n="14.2">tous</w> <w n="14.3">mes</w> <w n="14.4">vœux</w> <w n="14.5">confondus</w>.</l>
							<l n="15" num="2.7"><w n="15.1">Que</w> <w n="15.2">votre</w> <w n="15.3">âme</w> <w n="15.4">soit</w> <w n="15.5">contente</w> :</l>
							<l n="16" num="2.8"><w n="16.1">Mon</w> <w n="16.2">père</w>, <w n="16.3">il</w> <w n="16.4">ne</w> <w n="16.5">m</w>’<w n="16.6">aime</w> <w n="16.7">plus</w> !</l>
						</lg>
						<lg n="3">
							<l n="17" num="3.1"><w n="17.1">Ne</w> <w n="17.2">dites</w> <w n="17.3">plus</w>, <w n="17.4">ô</w> <w n="17.5">mon</w> <w n="17.6">père</w>,</l>
							<l n="18" num="3.2"><w n="18.1">Que</w> <w n="18.2">le</w> <w n="18.3">ciel</w> <w n="18.4">va</w> <w n="18.5">me</w> <w n="18.6">punir</w> ;</l>
							<l n="19" num="3.3"><w n="19.1">L</w>’<w n="19.2">amour</w>, <w n="19.3">comme</w> <w n="19.4">vous</w> <w n="19.5">sévère</w>,</l>
							<l n="20" num="3.4"><w n="20.1">A</w> <w n="20.2">daigné</w> <w n="20.3">le</w> <w n="20.4">prévenir</w>.</l>
							<l n="21" num="3.5"><w n="21.1">Ce</w> <w n="21.2">guide</w> <w n="21.3">ingrat</w> <w n="21.4">que</w> <w n="21.5">j</w>’<w n="21.6">adore</w></l>
							<l n="22" num="3.6"><w n="22.1">Fuit</w> <w n="22.2">mes</w> <w n="22.3">pas</w> <w n="22.4">qu</w>’<w n="22.5">il</w> <w n="22.6">a</w> <w n="22.7">perdus</w>.</l>
							<l n="23" num="3.7"><w n="23.1">Qui</w> <w n="23.2">peut</w> <w n="23.3">me</w> <w n="23.4">punir</w> <w n="23.5">encore</w> ?</l>
							<l n="24" num="3.8"><w n="24.1">Mon</w> <w n="24.2">père</w>, <w n="24.3">il</w> <w n="24.4">ne</w> <w n="24.5">m</w>’<w n="24.6">aime</w> <w n="24.7">plus</w> !</l>
						</lg>
						<lg n="4">
							<l n="25" num="4.1"><w n="25.1">Le</w> <w n="25.2">monde</w> <w n="25.3">n</w>’<w n="25.4">a</w> <w n="25.5">point</w> <w n="25.6">d</w>’<w n="25.7">asile</w></l>
							<l n="26" num="4.2"><w n="26.1">Qui</w> <w n="26.2">soit</w> <w n="26.3">doux</w> <w n="26.4">au</w> <w n="26.5">repentir</w>.</l>
							<l n="27" num="4.3"><w n="27.1">Hé</w> <w n="27.2">bien</w> ! <w n="27.3">rendez</w>-<w n="27.4">moi</w> <w n="27.5">facile</w></l>
							<l n="28" num="4.4"><w n="28.1">Un</w> <w n="28.2">chemin</w> <w n="28.3">pour</w> <w n="28.4">en</w> <w n="28.5">sortir</w>.</l>
							<l n="29" num="4.5"><w n="29.1">Me</w> <w n="29.2">faudra</w>-<w n="29.3">t</w>-<w n="29.4">il</w>, <w n="29.5">dans</w> <w n="29.6">l</w>’<w n="29.7">orage</w>,</l>
							<l n="30" num="4.6"><w n="30.1">Traîner</w> <w n="30.2">mes</w> <w n="30.3">jours</w> <w n="30.4">abattus</w> ?</l>
							<l n="31" num="4.7"><w n="31.1">Je</w> <w n="31.2">n</w>’<w n="31.3">en</w> <w n="31.4">ai</w> <w n="31.5">pas</w> <w n="31.6">le</w> <w n="31.7">courage</w> :</l>
							<l n="32" num="4.8"><w n="32.1">Mon</w> <w n="32.2">père</w>, <w n="32.3">il</w> <w n="32.4">ne</w> <w n="32.5">m</w>’<w n="32.6">aime</w> <w n="32.7">plus</w> !</l>
						</lg>
						<lg n="5">
							<l n="33" num="5.1"><w n="33.1">De</w> <w n="33.2">cette</w> <w n="33.3">croix</w> <w n="33.4">où</w> <w n="33.5">je</w> <w n="33.6">pleure</w></l>
							<l n="34" num="5.2"><w n="34.1">N</w>’<w n="34.2">exilez</w> <w n="34.3">pas</w> <w n="34.4">mes</w> <w n="34.5">aveux</w> ;</l>
							<l n="35" num="5.3"><w n="35.1">Et</w> <w n="35.2">vous</w> <w n="35.3">saurez</w> <w n="35.4">tout</w> <w n="35.5">à</w> <w n="35.6">l</w>’<w n="35.7">heure</w>,</l>
							<l n="36" num="5.4"><w n="36.1">Ermite</w>, <w n="36.2">ce</w> <w n="36.3">que</w> <w n="36.4">je</w> <w n="36.5">veux</w>.</l>
							<l n="37" num="5.5"><w n="37.1">Quelques</w> <w n="37.2">pleurs</w>, <w n="37.3">un</w> <w n="37.4">peu</w> <w n="37.5">de</w> <w n="37.6">cendre</w>,</l>
							<l n="38" num="5.6"><w n="38.1">Sur</w> <w n="38.2">ma</w> <w n="38.3">tombe</w> <w n="38.4">répandus</w>…</l>
							<l n="39" num="5.7"><w n="39.1">Ah</w> ! <w n="39.2">qu</w>’<w n="39.3">il</w> <w n="39.4">m</w>’<w n="39.5">est</w> <w n="39.6">doux</w> <w n="39.7">d</w>’<w n="39.8">y</w> <w n="39.9">descendre</w> :</l>
							<l n="40" num="5.8"><w n="40.1">Mon</w> <w n="40.2">père</w>, <w n="40.3">il</w> <w n="40.4">ne</w> <w n="40.5">m</w>’<w n="40.6">aime</w> <w n="40.7">plus</w> ! »</l>
						</lg>
						<lg n="6">
							<l n="41" num="6.1"><w n="41.1">À</w> <w n="41.2">peine</w> <w n="41.3">une</w> <w n="41.4">faible</w> <w n="41.5">aurore</w></l>
							<l n="42" num="6.2"><w n="42.1">Passait</w> <w n="42.2">sur</w> <w n="42.3">les</w> <w n="42.4">jeunes</w> <w n="42.5">fleurs</w>,</l>
							<l n="43" num="6.3"><w n="43.1">Que</w> <w n="43.2">le</w> <w n="43.3">bon</w> <w n="43.4">ermite</w> <w n="43.5">encore</w></l>
							<l n="44" num="6.4"><w n="44.1">Versait</w> <w n="44.2">la</w> <w n="44.3">cendre</w> <w n="44.4">et</w> <w n="44.5">les</w> <w n="44.6">pleurs</w>.</l>
							<l n="45" num="6.5"><w n="45.1">Longtemps</w> <w n="45.2">cet</w> <w n="45.3">objet</w> <w n="45.4">trop</w> <w n="45.5">tendre</w>,</l>
							<l n="46" num="6.6"><w n="46.1">Troubla</w> <w n="46.2">ses</w> <w n="46.3">songes</w> <w n="46.4">confus</w> ;</l>
							<l n="47" num="6.7"><w n="47.1">Et</w>, <w n="47.2">triste</w>, <w n="47.3">il</w> <w n="47.4">croyait</w> <w n="47.5">entendre</w> ;</l>
							<l n="48" num="6.8">« <w n="48.1">Mon</w> <w n="48.2">père</w>, <w n="48.3">il</w> <w n="48.4">ne</w> <w n="48.5">m</w>’<w n="48.6">aime</w> <w n="48.7">plus</w> ! »</l>
						</lg>
					</div></body></text></TEI>