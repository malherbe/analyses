<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>9618 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>9618 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies</title>
					<author>Marceline Desbordes-Valmore</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>A. Boulland, libraire-éditeur</publisher>
						<date when="1830">1830</date>
					</imprint>
					<extent>9618 verstome second</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-12-20" who="RR">Insertion des corrections métriques dans le corps du texte.</change>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
				<change when="2018-11-02" who="RR">Correction dans le poème "Dors, ma Mère".</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part"> POÉSIES INÉDITES <lb></lb>ROMANCES</head><div type="poem" key="DES193">
						<head type="main">REGARDE-LE</head>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">Regarde</w>-<w n="1.2">le</w>, <w n="1.3">mais</w> <w n="1.4">pas</w> <w n="1.5">longtemps</w> !</l>
							<l n="2" num="1.2"><w n="2.1">Un</w> <w n="2.2">regard</w> <w n="2.3">suffira</w>, <w n="2.4">sois</w> <w n="2.5">sûre</w>,</l>
							<l n="3" num="1.3"><w n="3.1">Pour</w> <w n="3.2">lui</w> <w n="3.3">pardonner</w> <w n="3.4">la</w> <w n="3.5">blessure</w></l>
							<l n="4" num="1.4"><w n="4.1">Qui</w> <w n="4.2">fit</w> <w n="4.3">languir</w> <w n="4.4">mes</w> <w n="4.5">doux</w> <w n="4.6">printemps</w>.</l>
							<l n="5" num="1.5"><w n="5.1">Regarde</w>-<w n="5.2">le</w>, <w n="5.3">mais</w> <w n="5.4">pas</w> <w n="5.5">longtemps</w> !</l>
						</lg>
						<lg n="2">
							<l n="6" num="2.1"><w n="6.1">S</w>’<w n="6.2">il</w> <w n="6.3">parle</w>, <w n="6.4">écoute</w> <w n="6.5">un</w> <w n="6.6">peu</w> <w n="6.7">sa</w> <w n="6.8">voix</w> !</l>
							<l n="7" num="2.2"><w n="7.1">Je</w> <w n="7.2">ne</w> <w n="7.3">veux</w> <w n="7.4">pas</w> <w n="7.5">trop</w> <w n="7.6">t</w>’<w n="7.7">y</w> <w n="7.8">contraindre</w> ;</l>
							<l n="8" num="2.3"><w n="8.1">Je</w> <w n="8.2">sais</w> <w n="8.3">combien</w> <w n="8.4">elle</w> <w n="8.5">est</w> <w n="8.6">à</w> <w n="8.7">craindre</w>,</l>
							<l n="9" num="2.4"><w n="9.1">Ne</w> <w n="9.2">l</w>’<w n="9.3">entendît</w>-<w n="9.4">on</w> <w n="9.5">qu</w>’<w n="9.6">une</w> <w n="9.7">fois</w> :</l>
							<l n="10" num="2.5"><w n="10.1">S</w>’<w n="10.2">il</w> <w n="10.3">parle</w>, <w n="10.4">écoute</w> <w n="10.5">un</w> <w n="10.6">peu</w> <w n="10.7">sa</w> <w n="10.8">voix</w> !</l>
						</lg>
						<lg n="3">
							<l n="11" num="3.1"><w n="11.1">Tu</w> <w n="11.2">ne</w> <w n="11.3">haïras</w> <w n="11.4">plus</w> <w n="11.5">son</w> <w n="11.6">nom</w>,</l>
							<l n="12" num="3.2"><w n="12.1">Ce</w> <w n="12.2">nom</w> <w n="12.3">mêlé</w> <w n="12.4">dans</w> <w n="12.5">ma</w> <w n="12.6">prière</w> ;</l>
							<l n="13" num="3.3"><w n="13.1">Tu</w> <w n="13.2">l</w>’<w n="13.3">écouteras</w> <w n="13.4">tout</w> <w n="13.5">entière</w>,</l>
							<l n="14" num="3.4"><w n="14.1">Sans</w> <w n="14.2">courroux</w>, <w n="14.3">sans</w> <w n="14.4">reproche</w> : <w n="14.5">oh</w> ! <w n="14.6">non</w> !</l>
							<l n="15" num="3.5"><w n="15.1">Tu</w> <w n="15.2">ne</w> <w n="15.3">haïras</w> <w n="15.4">plus</w> <w n="15.5">son</w> <w n="15.6">nom</w> !</l>
						</lg>
						<lg n="4">
							<l n="16" num="4.1"><w n="16.1">Au</w> <w n="16.2">fond</w> <w n="16.3">du</w> <w n="16.4">cœur</w> <w n="16.5">tu</w> <w n="16.6">m</w>’<w n="16.7">entendras</w>,</l>
							<l n="17" num="4.2"><w n="17.1">Quand</w> <w n="17.2">je</w> <w n="17.3">dis</w> : <w n="17.4">J</w>’<w n="17.5">ai</w> <w n="17.6">cessé</w> <w n="17.7">de</w> <w n="17.8">vivre</w> ;</l>
							<l n="18" num="4.3"><w n="18.1">Quand</w> <w n="18.2">je</w> <w n="18.3">refuse</w> <w n="18.4">de</w> <w n="18.5">te</w> <w n="18.6">suivre</w> ;</l>
							<l n="19" num="4.4"><w n="19.1">Enfin</w>, <w n="19.2">quand</w> <w n="19.3">tu</w> <w n="19.4">le</w> <w n="19.5">connaîtras</w>,</l>
							<l n="20" num="4.5"><w n="20.1">Au</w> <w n="20.2">fond</w> <w n="20.3">du</w> <w n="20.4">cœur</w> <w n="20.5">tu</w> <w n="20.6">m</w>’<w n="20.7">entendras</w> !</l>
						</lg>
						<lg n="5">
							<l n="21" num="5.1"><w n="21.1">Tais</w>-<w n="21.2">toi</w>, <w n="21.3">s</w>’<w n="21.4">il</w> <w n="21.5">demande</w> <w n="21.6">à</w> <w n="21.7">me</w> <w n="21.8">voir</w> :</l>
							<l n="22" num="5.2"><w n="22.1">J</w>’<w n="22.2">ai</w> <w n="22.3">pu</w> <w n="22.4">fuir</w> <w n="22.5">sa</w> <w n="22.6">volage</w> <w n="22.7">ivresse</w> ;</l>
							<l n="23" num="5.3"><w n="23.1">Mais</w> <w n="23.2">me</w> <w n="23.3">cacher</w> <w n="23.4">à</w> <w n="23.5">sa</w> <w n="23.6">tendresse</w>,</l>
							<l n="24" num="5.4"><w n="24.1">Dieu</w> <w n="24.2">n</w>’<w n="24.3">en</w> <w n="24.4">donne</w> <w n="24.5">pas</w> <w n="24.6">le</w> <w n="24.7">pouvoir</w> :</l>
							<l n="25" num="5.5"><w n="25.1">Tais</w>-<w n="25.2">toi</w>, <w n="25.3">s</w>’<w n="25.4">il</w> <w n="25.5">demande</w> <w n="25.6">à</w> <w n="25.7">me</w> <w n="25.8">voir</w> !</l>
						</lg>
						<lg n="6">
							<l n="26" num="6.1"><w n="26.1">Si</w> <w n="26.2">je</w> <w n="26.3">l</w>’<w n="26.4">accusais</w> <w n="26.5">devant</w> <w n="26.6">toi</w>,</l>
							<l n="27" num="6.2"><w n="27.1">Appelle</w> <w n="27.2">un</w> <w n="27.3">moment</w> <w n="27.4">son</w> <w n="27.5">image</w>.</l>
							<l n="28" num="6.3"><w n="28.1">Avec</w> <w n="28.2">le</w> <w n="28.3">feu</w> <w n="28.4">de</w> <w n="28.5">son</w> <w n="28.6">langage</w>,</l>
							<l n="29" num="6.4"><w n="29.1">Défends</w>-<w n="29.2">le</w> <w n="29.3">par</w> <w n="29.4">pitié</w> <w n="29.5">pour</w> <w n="29.6">moi</w>,</l>
							<l n="30" num="6.5"><w n="30.1">Si</w> <w n="30.2">je</w> <w n="30.3">l</w>’<w n="30.4">accusais</w> <w n="30.5">devant</w> <w n="30.6">toi</w> !</l>
						</lg>
					</div></body></text></TEI>