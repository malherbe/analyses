<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>9618 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>9618 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies</title>
					<author>Marceline Desbordes-Valmore</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>A. Boulland, libraire-éditeur</publisher>
						<date when="1830">1830</date>
					</imprint>
					<extent>9618 verstome second</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-12-20" who="RR">Insertion des corrections métriques dans le corps du texte.</change>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
				<change when="2018-11-02" who="RR">Correction dans le poème "Dors, ma Mère".</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">IDYLLES</head><div type="poem" key="DES7">
						<head type="main">L’ABSENCE</head>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">L</w>’<w n="1.2">avez</w>-<w n="1.3">vous</w> <w n="1.4">rencontré</w> ? <w n="1.5">guidez</w>-<w n="1.6">moi</w>, <w n="1.7">je</w> <w n="1.8">vous</w> <w n="1.9">prie</w>.</l>
							<l n="2" num="1.2"><w n="2.1">Il</w> <w n="2.2">est</w> <w n="2.3">jeune</w>, <w n="2.4">il</w> <w n="2.5">est</w> <w n="2.6">triste</w>, <w n="2.7">il</w> <w n="2.8">est</w> <w n="2.9">beau</w> <w n="2.10">comme</w> <w n="2.11">vous</w>,</l>
							<l n="3" num="1.3"><w n="3.1">Bel</w> <w n="3.2">enfant</w> ; <w n="3.3">et</w> <w n="3.4">sa</w> <w n="3.5">voix</w>, <w n="3.6">par</w> <w n="3.7">un</w> <w n="3.8">charme</w> <w n="3.9">attendrie</w>,</l>
							<l n="4" num="1.4"><w n="4.1">De</w> <w n="4.2">la</w> <w n="4.3">voix</w> <w n="4.4">qui</w> <w n="4.5">l</w>’<w n="4.6">accueille</w> <w n="4.7">est</w> <w n="4.8">l</w>’<w n="4.9">écho</w> <w n="4.10">le</w> <w n="4.11">plus</w> <w n="4.12">doux</w>.</l>
							<l n="5" num="1.5"><w n="5.1">Oh</w> ! <w n="5.2">rappelez</w>-<w n="5.3">vous</w> <w n="5.4">bien</w> ! <w n="5.5">Sa</w> <w n="5.6">démarche</w> <w n="5.7">pensive</w></l>
							<l n="6" num="1.6"><w n="6.1">Fait</w> <w n="6.2">qu</w>’<w n="6.3">on</w> <w n="6.4">le</w> <w n="6.5">suit</w> <w n="6.6">longtemps</w> <w n="6.7">et</w> <w n="6.8">du</w> <w n="6.9">cœur</w> <w n="6.10">et</w> <w n="6.11">des</w> <w n="6.12">yeux</w>.</l>
							<l n="7" num="1.7"><w n="7.1">Il</w> <w n="7.2">vous</w> <w n="7.3">aura</w> <w n="7.4">souri</w>. <w n="7.5">De</w> <w n="7.6">l</w>’<w n="7.7">enfance</w> <w n="7.8">naïve</w>,</l>
							<l n="8" num="1.8"><w n="8.1">Naïf</w> <w n="8.2">encore</w>, <w n="8.3">il</w> <w n="8.4">aime</w> <w n="8.5">à</w> <w n="8.6">contempler</w> <w n="8.7">les</w> <w n="8.8">jeux</w>.</l>
						</lg>
						<lg n="2">
							<l n="9" num="2.1"><w n="9.1">Écoute</w> ! <w n="9.2">ses</w> <w n="9.3">regards</w> <w n="9.4">distraits</w>, <w n="9.5">chargés</w> <w n="9.6">d</w>’<w n="9.7">alarmes</w>,</l>
							<l n="10" num="2.2"><w n="10.1">Effleuraient</w> <w n="10.2">tes</w> <w n="10.3">doux</w> <w n="10.4">jeux</w>, <w n="10.5">peut</w>-<w n="10.6">être</w> <w n="10.7">sans</w> <w n="10.8">les</w> <w n="10.9">voir</w>.</l>
							<l n="11" num="2.3"><w n="11.1">Plains</w>-<w n="11.2">moi</w>, <w n="11.3">car</w> <w n="11.4">c</w>’<w n="11.5">est</w> <w n="11.6">pour</w> <w n="11.7">moi</w> <w n="11.8">qu</w>’<w n="11.9">il</w> <w n="11.10">dévorait</w> <w n="11.11">ses</w> <w n="11.12">larmes</w>,</l>
							<l n="12" num="2.4"><w n="12.1">Et</w> <w n="12.2">de</w> <w n="12.3">m</w>’<w n="12.4">en</w> <w n="12.5">consoler</w> <w n="12.6">il</w> <w n="12.7">a</w> <w n="12.8">seul</w> <w n="12.9">le</w> <w n="12.10">pouvoir</w>.</l>
							<l n="13" num="2.5"><w n="13.1">Guide</w>-<w n="13.2">moi</w> ; <w n="13.3">réponds</w>-<w n="13.4">moi</w> !… <w n="13.5">Mais</w> <w n="13.6">tu</w> <w n="13.7">ne</w> <w n="13.8">peux</w> <w n="13.9">m</w>’<w n="13.10">entendre</w> :</l>
							<l n="14" num="2.6"><space quantity="10" unit="char"></space><w n="14.1">Tu</w> <w n="14.2">demandes</w> <w n="14.3">son</w> <w n="14.4">nom</w> ?</l>
							<l n="15" num="2.7"><w n="15.1">Ah</w> ! <w n="15.2">s</w>’<w n="15.3">il</w> <w n="15.4">t</w>’<w n="15.5">avait</w> <w n="15.6">parlé</w>, <w n="15.7">m</w>’<w n="15.8">aurais</w>-<w n="15.9">tu</w> <w n="15.10">fait</w> <w n="15.11">attendre</w> ?</l>
							<l n="16" num="2.8"><w n="16.1">L</w>’<w n="16.2">aurais</w>-<w n="16.3">tu</w> <w n="16.4">méconnu</w> <w n="16.5">dans</w> <w n="16.6">ma</w> <w n="16.7">prière</w> ? <w n="16.8">oh</w> ! <w n="16.9">non</w>,</l>
							<l n="17" num="2.9"><w n="17.1">Va</w> <w n="17.2">jouer</w>, <w n="17.3">bel</w> <w n="17.4">enfant</w>, <w n="17.5">va</w> <w n="17.6">rire</w> <w n="17.7">avec</w> <w n="17.8">la</w> <w n="17.9">vie</w>,</l>
							<l n="18" num="2.10"><w n="18.1">Car</w> <w n="18.2">ton</w> <w n="18.3">âge</w> <w n="18.4">est</w> <w n="18.5">sa</w> <w n="18.6">fête</w>, <w n="18.7">et</w> <w n="18.8">déjà</w> <w n="18.9">je</w> <w n="18.10">l</w>’<w n="18.11">envie</w>.</l>
							<l n="19" num="2.11"><w n="19.1">Va</w> ! <w n="19.2">mais</w> <w n="19.3">si</w> <w n="19.4">ton</w> <w n="19.5">bonheur</w> <w n="19.6">te</w> <w n="19.7">l</w>’<w n="19.8">amène</w> <w n="19.9">aujourd</w>’<w n="19.10">hui</w>,</l>
							<l n="20" num="2.12"><w n="20.1">Souviens</w>-<w n="20.2">toi</w> <w n="20.3">que</w> <w n="20.4">je</w> <w n="20.5">pleure</w>, <w n="20.6">et</w> <w n="20.7">ne</w> <w n="20.8">le</w> <w n="20.9">dis</w> <w n="20.10">qu</w>’<w n="20.11">à</w> <w n="20.12">lui</w>.</l>
							<l n="21" num="2.13"><w n="21.1">Comme</w> <w n="21.2">la</w> <w n="21.3">route</w> <w n="21.4">au</w> <w n="21.5">loin</w> <w n="21.6">se</w> <w n="21.7">prolonge</w> <w n="21.8">isolée</w> !</l>
							<l n="22" num="2.14"><w n="22.1">Eh</w> ! <w n="22.2">pour</w> <w n="22.3">qui</w> <w n="22.4">ces</w> <w n="22.5">jardins</w>, <w n="22.6">ce</w> <w n="22.7">soleil</w>, <w n="22.8">ces</w> <w n="22.9">ruisseaux</w> ?</l>
							<l n="23" num="2.15"><w n="23.1">Je</w> <w n="23.2">suis</w> <w n="23.3">seule</w>, <w n="23.4">et</w> <w n="23.5">là</w>-<w n="23.6">bas</w>, <w n="23.7">sous</w> <w n="23.8">de</w> <w n="23.9">noirs</w> <w n="23.10">arbrisseaux</w>,</l>
							<l n="24" num="2.16"><w n="24.1">La</w> <w n="24.2">moitié</w> <w n="24.3">de</w> <w n="24.4">mon</w> <w n="24.5">âme</w> <w n="24.6">est</w> <w n="24.7">errante</w> <w n="24.8">et</w> <w n="24.9">voilée</w>.</l>
							<l n="25" num="2.17"><w n="25.1">Mes</w> <w n="25.2">suppliantes</w> <w n="25.3">mains</w> <w n="25.4">voudraient</w> <w n="25.5">la</w> <w n="25.6">retenir</w> :</l>
							<l n="26" num="2.18"><w n="26.1">J</w>’<w n="26.2">ai</w> <w n="26.3">cru</w> <w n="26.4">respirer</w> <w n="26.5">l</w>’<w n="26.6">air</w> <w n="26.7">qui</w> <w n="26.8">va</w> <w n="26.9">nous</w> <w n="26.10">réunir</w> !</l>
							<l n="27" num="2.19"><w n="27.1">L</w>’<w n="27.2">avez</w>-<w n="27.3">vous</w> <w n="27.4">rencontré</w>, <w n="27.5">nymphe</w> <w n="27.6">à</w> <w n="27.7">la</w> <w n="27.8">voix</w> <w n="27.9">plaintive</w> ?</l>
							<l n="28" num="2.20"><w n="28.1">L</w>’<w n="28.2">avez</w>-<w n="28.3">vous</w> <w n="28.4">appelé</w> ? <w n="28.5">s</w>’<w n="28.6">est</w>-<w n="28.7">il</w> <w n="28.8">penché</w> <w n="28.9">vers</w> <w n="28.10">vous</w> ?</l>
							<l n="29" num="2.21"><w n="29.1">Si</w> <w n="29.2">son</w> <w n="29.3">ombre</w> <w n="29.4">a</w> <w n="29.5">passé</w> <w n="29.6">dans</w> <w n="29.7">votre</w> <w n="29.8">eau</w> <w n="29.9">fugitive</w>,</l>
							<l n="30" num="2.22"><w n="30.1">Nymphe</w>, <w n="30.2">rendez</w>-<w n="30.3">la</w> <w n="30.4">moi</w>, <w n="30.5">je</w> <w n="30.6">l</w>’<w n="30.7">attends</w> <w n="30.8">à</w> <w n="30.9">genoux</w>.</l>
							<l n="31" num="2.23"><w n="31.1">Mais</w> <w n="31.2">jusqu</w>’<w n="31.3">à</w> <w n="31.4">l</w>’<w n="31.5">oublier</w> <w n="31.6">si</w> <w n="31.7">vous</w> <w n="31.8">êtes</w> <w n="31.9">légère</w> ;</l>
							<l n="32" num="2.24"><w n="32.1">Mais</w> <w n="32.2">si</w> <w n="32.3">vous</w> <w n="32.4">n</w>’<w n="32.5">emportez</w> <w n="32.6">que</w> <w n="32.7">vous</w> <w n="32.8">dans</w> <w n="32.9">l</w>’<w n="32.10">avenir</w> ;</l>
							<l n="33" num="2.25"><w n="33.1">Si</w> <w n="33.2">l</w>’<w n="33.3">image</w> <w n="33.4">qui</w> <w n="33.5">fuit</w> <w n="33.6">vous</w> <w n="33.7">devient</w> <w n="33.8">étrangère</w>,</l>
							<l n="34" num="2.26"><w n="34.1">De</w> <w n="34.2">quoi</w> <w n="34.3">vous</w> <w n="34.4">plaignez</w>-<w n="34.5">vous</w>, <w n="34.6">nymphe</w> <w n="34.7">sans</w> <w n="34.8">souvenir</w> ?</l>
						</lg>
						<lg n="3">
							<l n="35" num="3.1"><w n="35.1">Quelle</w> <w n="35.2">est</w> <w n="35.3">cette</w> <w n="35.4">autre</w> <w n="35.5">enfant</w> <w n="35.6">sous</w> <w n="35.7">les</w> <w n="35.8">saules</w> <w n="35.9">couchée</w> ?</l>
							<l n="36" num="3.2"><w n="36.1">De</w> <w n="36.2">paisibles</w> <w n="36.3">rameaux</w> <w n="36.4">enveloppent</w> <w n="36.5">son</w> <w n="36.6">sort</w> ;</l>
							<l n="37" num="3.3"><w n="37.1">Comme</w> <w n="37.2">une</w> <w n="37.3">jeune</w> <w n="37.4">fleur</w> <w n="37.5">dans</w> <w n="37.6">la</w> <w n="37.7">mousse</w> <w n="37.8">cachée</w>,</l>
							<l n="38" num="3.4"><space quantity="6" unit="char"></space><w n="38.1">À</w> <w n="38.2">l</w>’<w n="38.3">abri</w> <w n="38.4">des</w> <w n="38.5">vents</w>, <w n="38.6">elle</w> <w n="38.7">dort</w>.</l>
							<l n="39" num="3.5"><w n="39.1">L</w>’<w n="39.2">orage</w> <w n="39.3">aux</w> <w n="39.4">traits</w> <w n="39.5">brûlants</w> <w n="39.6">ne</w> <w n="39.7">l</w>’<w n="39.8">a</w> <w n="39.9">pas</w> <w n="39.10">effeuillée</w> ;</l>
							<l n="40" num="3.6"><w n="40.1">Loin</w> <w n="40.2">du</w> <w n="40.3">monde</w> <w n="40.4">et</w> <w n="40.5">du</w> <w n="40.6">jour</w> <w n="40.7">lentement</w> <w n="40.8">éveillée</w>,</l>
							<l n="41" num="3.7"><w n="41.1">Un</w> <w n="41.2">jeune</w> <w n="41.3">songe</w> <w n="41.4">à</w> <w n="41.5">peine</w> <w n="41.6">ose</w> <w n="41.7">effleurer</w> <w n="41.8">ses</w> <w n="41.9">sens</w> ;</l>
							<l n="42" num="3.8"><w n="42.1">Elle</w> <w n="42.2">rit</w>… <w n="42.3">qu</w>’<w n="42.4">offre</w>-<w n="42.5">t</w>-<w n="42.6">il</w> <w n="42.7">à</w> <w n="42.8">ses</w> <w n="42.9">vœux</w> <w n="42.10">caressants</w> ?…</l>
							<l n="43" num="3.9"><w n="43.1">L</w>’<w n="43.2">avez</w>-<w n="43.3">vous</w> <w n="43.4">rencontré</w>, <w n="43.5">dites</w>, <w n="43.6">belle</w> <w n="43.7">ingénue</w> ?</l>
							<l n="44" num="3.10"><w n="44.1">Sa</w> <w n="44.2">voix</w>, <w n="44.3">qui</w> <w n="44.4">fait</w> <w n="44.5">rêver</w>, <w n="44.6">vous</w> <w n="44.7">est</w>-<w n="44.8">elle</w> <w n="44.9">connue</w> ?</l>
							<l n="45" num="3.11"><w n="45.1">Au</w> <w n="45.2">fond</w> <w n="45.3">d</w>’<w n="45.4">un</w> <w n="45.5">doux</w> <w n="45.6">sommeil</w> <w n="45.7">écoutez</w>-<w n="45.8">vous</w> <w n="45.9">ses</w> <w n="45.10">pas</w> ?</l>
							<l n="46" num="3.12"><w n="46.1">Non</w> ! <w n="46.2">si</w> <w n="46.3">vous</w> <w n="46.4">l</w>’<w n="46.5">aviez</w> <w n="46.6">vu</w>, <w n="46.7">vous</w> <w n="46.8">ne</w> <w n="46.9">dormiriez</w> <w n="46.10">pas</w> !</l>
							<l n="47" num="3.13"><w n="47.1">Dormez</w>. <w n="47.2">Je</w> <w n="47.3">vous</w> <w n="47.4">rendrais</w> <w n="47.5">et</w> <w n="47.6">pensive</w> <w n="47.7">et</w> <w n="47.8">peureuse</w>,</l>
							<l n="48" num="3.14"><w n="48.1">Vous</w> <w n="48.2">diriez</w> : « <w n="48.3">Dès</w> <w n="48.4">qu</w>’<w n="48.5">on</w> <w n="48.6">aime</w> <w n="48.7">on</w> <w n="48.8">n</w>’<w n="48.9">est</w> <w n="48.10">donc</w> <w n="48.11">plus</w> <w n="48.12">heureuse</w> ? »</l>
							<l n="49" num="3.15"><w n="49.1">Je</w> <w n="49.2">ne</w> <w n="49.3">sais</w>. <w n="49.4">Pour</w> <w n="49.5">la</w> <w n="49.6">paix</w> <w n="49.7">de</w> <w n="49.8">vos</w> <w n="49.9">nuits</w>, <w n="49.10">de</w> <w n="49.11">vos</w> <w n="49.12">jours</w>,</l>
							<l n="50" num="3.16"><space quantity="10" unit="char"></space><w n="50.1">Ignorez</w>-<w n="50.2">le</w> <w n="50.3">toujours</w>.</l>
						</lg>
						<lg n="4">
							<l n="51" num="4.1"><w n="51.1">Mais</w> <w n="51.2">de</w> <w n="51.3">nouveaux</w> <w n="51.4">sentiers</w> <w n="51.5">s</w>’<w n="51.6">ouvrent</w> <w n="51.7">à</w> <w n="51.8">ma</w> <w n="51.9">tristesse</w> :</l>
							<l n="52" num="4.2"><w n="52.1">Je</w> <w n="52.2">voudrais</w> <w n="52.3">tous</w> <w n="52.4">les</w> <w n="52.5">suivre</w>, <w n="52.6">et</w> <w n="52.7">je</w> <w n="52.8">n</w>’<w n="52.9">ose</w> <w n="52.10">choisir</w>.</l>
							<l n="53" num="4.3"><w n="53.1">L</w>’<w n="53.2">espoir</w> <w n="53.3">les</w> <w n="53.4">choisit</w> <w n="53.5">tous</w>. <w n="53.6">Oh</w> ! <w n="53.7">qu</w>’<w n="53.8">il</w> <w n="53.9">a</w> <w n="53.10">de</w> <w n="53.11">vitesse</w> !</l>
							<l n="54" num="4.4"><w n="54.1">Il</w> <w n="54.2">m</w>’<w n="54.3">appelle</w> <w n="54.4">partout</w>… <w n="54.5">Où</w> <w n="54.6">vais</w>-<w n="54.7">je</w> <w n="54.8">le</w> <w n="54.9">saisir</w> ?</l>
							<l n="55" num="4.5"><w n="55.1">Au</w> <w n="55.2">pied</w> <w n="55.3">de</w> <w n="55.4">la</w> <w n="55.5">chapelle</w> <w n="55.6">où</w> <w n="55.7">serpente</w> <w n="55.8">le</w> <w n="55.9">lierre</w>,</l>
							<l n="56" num="4.6"><space quantity="10" unit="char"></space><w n="56.1">Courbé</w> <w n="56.2">par</w> <w n="56.3">la</w> <w n="56.4">prière</w>,</l>
							<l n="57" num="4.7"><w n="57.1">Un</w> <w n="57.2">vieillard</w> <w n="57.3">indigent</w> <w n="57.4">porte</w> <w n="57.5">aussi</w> <w n="57.6">ses</w> <w n="57.7">douleurs</w>.</l>
							<l n="58" num="4.8"><w n="58.1">Allons</w> ! <w n="58.2">ses</w> <w n="58.3">yeux</w> <w n="58.4">éteints</w> <w n="58.5">ne</w> <w n="58.6">verront</w> <w n="58.7">pas</w> <w n="58.8">mes</w> <w n="58.9">pleurs</w>.</l>
							<l n="59" num="4.9"><w n="59.1">Comme</w> <w n="59.2">il</w> <w n="59.3">prie</w> ! <w n="59.4">on</w> <w n="59.5">dirait</w> <w n="59.6">qu</w>’<w n="59.7">une</w> <w n="59.8">lumière</w> <w n="59.9">heureuse</w>,</l>
							<l n="60" num="4.10"><w n="60.1">Pour</w> <w n="60.2">éclairer</w> <w n="60.3">son</w> <w n="60.4">front</w>, <w n="60.5">vient</w> <w n="60.6">d</w>’<w n="60.7">entr</w>’<w n="60.8">ouvrir</w> <w n="60.9">les</w> <w n="60.10">cieux</w> ;</l>
							<l n="61" num="4.11"><w n="61.1">On</w> <w n="61.2">dirait</w> <w n="61.3">que</w> <w n="61.4">le</w> <w n="61.5">jour</w> <w n="61.6">est</w> <w n="61.7">rentré</w> <w n="61.8">dans</w> <w n="61.9">ses</w> <w n="61.10">yeux</w>,</l>
							<l n="62" num="4.12"><w n="62.1">Ou</w> <w n="62.2">qu</w>’<w n="62.3">il</w> <w n="62.4">bénit</w> <w n="62.5">tout</w> <w n="62.6">bas</w> <w n="62.7">une</w> <w n="62.8">main</w> <w n="62.9">généreuse</w>.</l>
							<l n="63" num="4.13"><w n="63.1">Dieu</w> ! <w n="63.2">l</w>’<w n="63.3">a</w>-<w n="63.4">t</w>-<w n="63.5">il</w> <w n="63.6">rencontré</w> ? <w n="63.7">Si</w> <w n="63.8">calme</w>, <w n="63.9">si</w> <w n="63.10">content</w>,</l>
							<l n="64" num="4.14"><w n="64.1">Presse</w>-<w n="64.2">t</w>-<w n="64.3">il</w> <w n="64.4">un</w> <w n="64.5">bienfait</w> <w n="64.6">sur</w> <w n="64.7">son</w> <w n="64.8">cœur</w> <w n="64.9">palpitant</w> ?</l>
							<l n="65" num="4.15"><w n="65.1">Est</w>-<w n="65.2">ce</w> <w n="65.3">lui</w> <w n="65.4">qu</w>’<w n="65.5">il</w> <w n="65.6">bénit</w> ? <w n="65.7">et</w> <w n="65.8">la</w> <w n="65.9">voix</w> <w n="65.10">que</w> <w n="65.11">j</w> ’<w n="65.12">adore</w>,</l>
							<l n="66" num="4.16"><w n="66.1">Dans</w> <w n="66.2">ce</w> <w n="66.3">cœur</w> <w n="66.4">consolé</w> <w n="66.5">résonne</w>-<w n="66.6">t</w>-<w n="66.7">elle</w> <w n="66.8">encore</w> ?</l>
							<l n="67" num="4.17"><w n="67.1">Écoutez</w>-<w n="67.2">moi</w>, <w n="67.3">mon</w> <w n="67.4">père</w>, <w n="67.5">au</w> <w n="67.6">nom</w> <w n="67.7">de</w> <w n="67.8">ce</w> <w n="67.9">bienfait</w> !</l>
							<l n="68" num="4.18"><w n="68.1">Celui</w> <w n="68.2">qui</w> <w n="68.3">vous</w> <w n="68.4">l</w>’<w n="68.5">offrit</w> <w n="68.6">à</w> <w n="68.7">vous</w> <w n="68.8">m</w>’<w n="68.9">a</w> <w n="68.10">demandée</w></l>
							<l n="69" num="4.19"><w n="69.1">Peut</w>-<w n="69.2">être</w> ?… <w n="69.3">Oh</w> ! <w n="69.4">que</w> <w n="69.5">ma</w> <w n="69.6">main</w>, <w n="69.7">par</w> <w n="69.8">la</w> <w n="69.9">sienne</w> <w n="69.10">guidée</w>,</l>
							<l n="70" num="4.20"><w n="70.1">Joigne</w> <w n="70.2">son</w> <w n="70.3">humble</w> <w n="70.4">offrande</w> <w n="70.5">au</w> <w n="70.6">don</w> <w n="70.7">qu</w>’<w n="70.8">il</w> <w n="70.9">vous</w> <w n="70.10">a</w> <w n="70.11">fait</w>.</l>
							<l n="71" num="4.21"><w n="71.1">Mais</w>, <w n="71.2">en</w> <w n="71.3">vous</w> <w n="71.4">consolant</w>, <w n="71.5">soupirait</w>-<w n="71.6">il</w>, <w n="71.7">mon</w> <w n="71.8">père</w> ?</l>
							<l n="72" num="4.22"><w n="72.1">Déchiré</w> <w n="72.2">du</w> <w n="72.3">tourment</w> <w n="72.4">dont</w> <w n="72.5">il</w> <w n="72.6">me</w> <w n="72.7">désespère</w>,</l>
							<l n="73" num="4.23"><w n="73.1">Injuste</w>, <w n="73.2">mais</w> <w n="73.3">fidèle</w>, <w n="73.4">en</w> <w n="73.5">soupçonnant</w> <w n="73.6">ma</w> <w n="73.7">foi</w>,</l>
							<l n="74" num="4.24"><w n="74.1">Vous</w> <w n="74.2">a</w>-<w n="74.3">t</w>-<w n="74.4">il</w> <w n="74.5">dit</w> : « <w n="74.6">Priez</w> <w n="74.7">et</w> <w n="74.8">pour</w> <w n="74.9">elle</w> <w n="74.10">et</w> <w n="74.11">pour</w> <w n="74.12">moi</w> ? »</l>
							<l n="75" num="4.25"><w n="75.1">Oui</w>, <w n="75.2">je</w> <w n="75.3">sais</w> <w n="75.4">qu</w>’<w n="75.5">il</w> <w n="75.6">est</w> <w n="75.7">triste</w>, <w n="75.8">et</w> <w n="75.9">qu</w>’<w n="75.10">un</w> <w n="75.11">accent</w> <w n="75.12">plus</w> <w n="75.13">tendre</w></l>
							<l n="76" num="4.26"><w n="76.1">Au</w> <w n="76.2">malheureux</w> <w n="76.3">jamais</w> <w n="76.4">n</w>’<w n="76.5">a</w> <w n="76.6">su</w> <w n="76.7">se</w> <w n="76.8">faire</w> <w n="76.9">entendre</w>.</l>
							<l n="77" num="4.27"><w n="77.1">Oui</w>, <w n="77.2">je</w> <w n="77.3">vais</w> <w n="77.4">retrouver</w> <w n="77.5">mon</w> <w n="77.6">bonheur</w> <w n="77.7">qu</w>’<w n="77.8">il</w> <w n="77.9">troubla</w>,</l>
							<l n="78" num="4.28"><w n="78.1">Car</w> <w n="78.2">mon</w> <w n="78.3">bonheur</w>, <w n="78.4">c</w>’<w n="78.5">est</w> <w n="78.6">lui</w>, <w n="78.7">mon</w> <w n="78.8">père</w>, <w n="78.9">et</w> <w n="78.10">le</w> <w n="78.11">voilà</w> !</l>
						</lg>
					</div></body></text></TEI>