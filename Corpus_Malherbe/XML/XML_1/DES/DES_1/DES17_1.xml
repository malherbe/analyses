<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>9618 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>9618 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies</title>
					<author>Marceline Desbordes-Valmore</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>A. Boulland, libraire-éditeur</publisher>
						<date when="1830">1830</date>
					</imprint>
					<extent>9618 verstome second</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-12-20" who="RR">Insertion des corrections métriques dans le corps du texte.</change>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
				<change when="2018-11-02" who="RR">Correction dans le poème "Dors, ma Mère".</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">IDYLLES</head><div type="poem" key="DES17">
						<head type="main">LE SOIR D’ÉTÉ</head>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">Venez</w>, <w n="1.2">mes</w> <w n="1.3">chers</w> <w n="1.4">petits</w> ; <w n="1.5">venez</w>, <w n="1.6">mes</w> <w n="1.7">jeunes</w> <w n="1.8">âmes</w> ;</l>
							<l n="2" num="1.2"><w n="2.1">Sur</w> <w n="2.2">mes</w> <w n="2.3">genoux</w>, <w n="2.4">venez</w> <w n="2.5">tous</w> <w n="2.6">les</w> <w n="2.7">deux</w> <w n="2.8">vous</w> <w n="2.9">asseoir</w>.</l>
							<l n="3" num="1.3"><w n="3.1">Au</w> <w n="3.2">soleil</w> <w n="3.3">qui</w> <w n="3.4">se</w> <w n="3.5">couche</w> <w n="3.6">il</w> <w n="3.7">faut</w> <w n="3.8">dire</w> <w n="3.9">bonsoir</w>.</l>
							<l n="4" num="1.4"><w n="4.1">Voyez</w> <w n="4.2">comme</w> <w n="4.3">il</w> <w n="4.4">est</w> <w n="4.5">beau</w> <w n="4.6">dans</w> <w n="4.7">ses</w> <w n="4.8">mourantes</w> <w n="4.9">flammes</w> !</l>
							<l n="5" num="1.5"><w n="5.1">Sa</w> <w n="5.2">couronne</w> <w n="5.3">déjà</w> <w n="5.4">n</w>’<w n="5.5">a</w> <w n="5.6">plus</w> <w n="5.7">qu</w>’<w n="5.8">un</w> <w n="5.9">rayon</w> <w n="5.10">d</w>’<w n="5.11">or</w> ;</l>
							<l n="6" num="1.6"><w n="6.1">Demain</w>, <w n="6.2">plus</w> <w n="6.3">radieux</w> <w n="6.4">vous</w> <w n="6.5">le</w> <w n="6.6">verrez</w> <w n="6.7">encor</w>,</l>
							<l n="7" num="1.7"><w n="7.1">Car</w> <w n="7.2">on</w> <w n="7.3">ne</w> <w n="7.4">l</w>’<w n="7.5">a</w> <w n="7.6">point</w> <w n="7.7">vu</w> <w n="7.8">s</w>’<w n="7.9">enfuir</w> <w n="7.10">sous</w> <w n="7.11">un</w> <w n="7.12">nuage</w>.</l>
							<l n="8" num="1.8"><w n="8.1">La</w> <w n="8.2">cigale</w> <w n="8.3">a</w> <w n="8.4">chanté</w> : <w n="8.5">nous</w> <w n="8.6">n</w>’<w n="8.7">aurons</w> <w n="8.8">point</w> <w n="8.9">d</w>’<w n="8.10">orage</w>.</l>
							<l n="9" num="1.9"><w n="9.1">Ce</w> <w n="9.2">soleil</w> <w n="9.3">mûrira</w> <w n="9.4">les</w> <w n="9.5">fruits</w> <w n="9.6">que</w> <w n="9.7">vous</w> <w n="9.8">aimez</w> ;</l>
							<l n="10" num="1.10"><w n="10.1">Il</w> <w n="10.2">vous</w> <w n="10.3">rendra</w> <w n="10.4">vos</w> <w n="10.5">jeux</w>, <w n="10.6">vos</w> <w n="10.7">bouquets</w> <w n="10.8">parfumés</w>.</l>
							<l n="11" num="1.11"><w n="11.1">Dès</w> <w n="11.2">qu</w>’<w n="11.3">il</w> <w n="11.4">s</w>’<w n="11.5">éveillera</w>, <w n="11.6">je</w> <w n="11.7">vous</w> <w n="11.8">dirai</w> <w n="11.9">moi</w>-<w n="11.10">même</w> :</l>
							<l n="12" num="1.12">« <w n="12.1">Allons</w> <w n="12.2">voir</w> <w n="12.3">le</w> <w n="12.4">soleil</w>. » <w n="12.5">Jugez</w> <w n="12.6">si</w> <w n="12.7">je</w> <w n="12.8">vous</w> <w n="12.9">aime</w> !</l>
							<l n="13" num="1.13"><space quantity="6" unit="char"></space><w n="13.1">Les</w> <w n="13.2">charmantes</w> <w n="13.3">Heures</w> <w n="13.4">viendront</w></l>
							<l n="14" num="1.14"><space quantity="6" unit="char"></space><w n="14.1">Danser</w> <w n="14.2">autour</w> <w n="14.3">de</w> <w n="14.4">la</w> <w n="14.5">journée</w>,</l>
							<l n="15" num="1.15"><space quantity="6" unit="char"></space><w n="15.1">Et</w>, <w n="15.2">riantes</w>, <w n="15.3">s</w>’<w n="15.4">envoleront</w>,</l>
							<l n="16" num="1.16"><w n="16.1">Formant</w> <w n="16.2">avec</w> <w n="16.3">des</w> <w n="16.4">fleurs</w> <w n="16.5">la</w> <w n="16.6">trame</w> <w n="16.7">de</w> <w n="16.8">l</w>’<w n="16.9">année</w>.</l>
							<l n="17" num="1.17"><w n="17.1">Et</w> <w n="17.2">vous</w> <w n="17.3">appellerez</w> <w n="17.4">le</w> <w n="17.5">faible</w> <w n="17.6">agneau</w> <w n="17.7">qui</w> <w n="17.8">dort</w> ;</l>
							<l n="18" num="1.18"><w n="18.1">Pour</w> <w n="18.2">le</w> <w n="18.3">baigner</w> <w n="18.4">ce</w> <w n="18.5">soir</w> <w n="18.6">il</w> <w n="18.7">n</w>’<w n="18.8">est</w> <w n="18.9">pas</w> <w n="18.10">assez</w> <w n="18.11">fort</w> ;</l>
							<l n="19" num="1.19"><w n="19.1">Huit</w> <w n="19.2">jours</w> <w n="19.3">font</w> <w n="19.4">tout</w> <w n="19.5">son</w> <w n="19.6">âge</w> ; <w n="19.7">il</w> <w n="19.8">se</w> <w n="19.9">soutient</w> <w n="19.10">à</w> <w n="19.11">peine</w>,</l>
							<l n="20" num="1.20"><w n="20.1">Et</w> <w n="20.2">vous</w> <w n="20.3">le</w> <w n="20.4">fatiguez</w> <w n="20.5">à</w> <w n="20.6">courir</w> <w n="20.7">dans</w> <w n="20.8">la</w> <w n="20.9">plaine</w>.</l>
						</lg>
						<lg n="2">
							<l n="21" num="2.1"><w n="21.1">Venez</w>, <w n="21.2">il</w> <w n="21.3">en</w> <w n="21.4">est</w> <w n="21.5">temps</w>, <w n="21.6">vous</w> <w n="21.7">baigner</w> <w n="21.8">au</w> <w n="21.9">ruisseau</w>.</l>
							<l n="22" num="2.2"><w n="22.1">Tout</w> <w n="22.2">semble</w> <w n="22.3">se</w> <w n="22.4">pencher</w> <w n="22.5">vers</w> <w n="22.6">son</w> <w n="22.7">cristal</w> <w n="22.8">humide</w> :</l>
							<l n="23" num="2.3"><w n="23.1">Le</w> <w n="23.2">moucheron</w> <w n="23.3">brûlant</w> <w n="23.4">y</w> <w n="23.5">pose</w> <w n="23.6">un</w> <w n="23.7">pied</w> <w n="23.8">timide</w> ;</l>
							<l n="24" num="2.4"><w n="24.1">Et</w>, <w n="24.2">fatigué</w> <w n="24.3">du</w> <w n="24.4">jour</w>, <w n="24.5">le</w> <w n="24.6">flexible</w> <w n="24.7">arbrisseau</w></l>
							<l n="25" num="2.5"><w n="25.1">Y</w> <w n="25.2">trace</w> <w n="25.3">de</w> <w n="25.4">son</w> <w n="25.5">front</w> <w n="25.6">la</w> <w n="25.7">fugitive</w> <w n="25.8">empreinte</w>.</l>
							<l n="26" num="2.6"><w n="26.1">À</w> <w n="26.2">ses</w> <w n="26.3">flots</w> <w n="26.4">attiédis</w> <w n="26.5">confiez</w>-<w n="26.6">vous</w> <w n="26.7">sans</w> <w n="26.8">crainte</w> ;</l>
							<l n="27" num="2.7"><w n="27.1">Je</w> <w n="27.2">suis</w> <w n="27.3">là</w>. <w n="27.4">Voyez</w>-<w n="27.5">vous</w> <w n="27.6">ces</w> <w n="27.7">poissons</w> <w n="27.8">innocents</w> ?</l>
							<l n="28" num="2.8"><w n="28.1">Ne</w> <w n="28.2">les</w> <w n="28.3">effrayez</w> <w n="28.4">pas</w>, <w n="28.5">ils</w> <w n="28.6">s</w>’<w n="28.7">enfuiront</w> <w n="28.8">d</w>’<w n="28.9">eux</w>-<w n="28.10">mêmes</w>.</l>
							<l n="29" num="2.9"><w n="29.1">De</w> <w n="29.2">vos</w> <w n="29.3">jeunes</w> <w n="29.4">désirs</w> <w n="29.5">on</w> <w n="29.6">dirait</w> <w n="29.7">les</w> <w n="29.8">emblèmes</w> ;</l>
							<l n="30" num="2.10"><w n="30.1">Sans</w> <w n="30.2">les</w> <w n="30.3">troubler</w> <w n="30.4">encor</w> <w n="30.5">ils</w> <w n="30.6">glissent</w> <w n="30.7">sur</w> <w n="30.8">vos</w> <w n="30.9">sens</w>.</l>
						</lg>
						<lg n="3">
							<l n="31" num="3.1"><w n="31.1">Saluez</w>, <w n="31.2">mes</w> <w n="31.3">amours</w>, <w n="31.4">cette</w> <w n="31.5">vieille</w> <w n="31.6">bergère</w> :</l>
							<l n="32" num="3.2"><w n="32.1">Son</w> <w n="32.2">sourire</w> <w n="32.3">aux</w> <w n="32.4">enfants</w> <w n="32.5">donne</w> <w n="32.6">une</w> <w n="32.7">nuit</w> <w n="32.8">légère</w>.</l>
							<l n="33" num="3.3"><w n="33.1">Quoi</w> ! <w n="33.2">vous</w> <w n="33.3">voulez</w> <w n="33.4">courir</w>, <w n="33.5">pauvres</w> <w n="33.6">petits</w> <w n="33.7">mouillés</w> ?</l>
							<l n="34" num="3.4"><w n="34.1">Ce</w> <w n="34.2">papillon</w> <w n="34.3">tardif</w>, <w n="34.4">que</w> <w n="34.5">la</w> <w n="34.6">fraîcheur</w> <w n="34.7">attire</w>,</l>
							<l n="35" num="3.5"><w n="35.1">Baise</w> <w n="35.2">dans</w> <w n="35.3">vos</w> <w n="35.4">cheveux</w> <w n="35.5">les</w> <w n="35.6">lilas</w> <w n="35.7">effeuillés</w>,</l>
							<l n="36" num="3.6"><w n="36.1">Et</w>, <w n="36.2">tout</w> <w n="36.3">en</w> <w n="36.4">vous</w> <w n="36.5">bravant</w>, <w n="36.6">je</w> <w n="36.7">crois</w> <w n="36.8">l</w>’<w n="36.9">entendre</w> <w n="36.10">rire</w>.</l>
							<l n="37" num="3.7"><w n="37.1">C</w>’<w n="37.2">est</w> <w n="37.3">assez</w> <w n="37.4">le</w> <w n="37.5">poursuivre</w> <w n="37.6">et</w> <w n="37.7">lui</w> <w n="37.8">jeter</w> <w n="37.9">des</w> <w n="37.10">fleurs</w>,</l>
							<l n="38" num="3.8"><w n="38.1">Enfants</w> ! <w n="38.2">Vos</w> <w n="38.3">cris</w> <w n="38.4">de</w> <w n="38.5">joie</w> <w n="38.6">éveillent</w> <w n="38.7">la</w> <w n="38.8">colombe</w>.</l>
							<l n="39" num="3.9"><w n="39.1">Un</w> <w n="39.2">roseau</w> <w n="39.3">qui</w> <w n="39.4">s</w>’<w n="39.5">incline</w>, <w n="39.6">une</w> <w n="39.7">feuille</w> <w n="39.8">qui</w> <w n="39.9">tombe</w>,</l>
							<l n="40" num="3.10"><w n="40.1">Rompt</w> <w n="40.2">le</w> <w n="40.3">charme</w> <w n="40.4">léger</w> <w n="40.5">qui</w> <w n="40.6">suspend</w> <w n="40.7">les</w> <w n="40.8">douleurs</w>.</l>
							<l n="41" num="3.11"><w n="41.1">Écoutez</w> <w n="41.2">dans</w> <w n="41.3">son</w> <w n="41.4">nid</w> <w n="41.5">s</w>’<w n="41.6">agiter</w> <w n="41.7">l</w>’<w n="41.8">hirondelle</w> :</l>
							<l n="42" num="3.12"><w n="42.1">Tout</w> <w n="42.2">lui</w> <w n="42.3">semble</w> <w n="42.4">un</w> <w n="42.5">danger</w> ; <w n="42.6">car</w> <w n="42.7">elle</w> <w n="42.8">a</w> <w n="42.9">des</w> <w n="42.10">petits</w>.</l>
							<l n="43" num="3.13"><w n="43.1">Peut</w>-<w n="43.2">être</w> <w n="43.3">elle</w> <w n="43.4">a</w> <w n="43.5">rêvé</w> <w n="43.6">qu</w>’<w n="43.7">ils</w> <w n="43.8">étaient</w> <w n="43.9">tous</w> <w n="43.10">partis</w> ;</l>
							<l n="44" num="3.14"><w n="44.1">La</w> <w n="44.2">voilà</w> <w n="44.3">qui</w> <w n="44.4">se</w> <w n="44.5">calme</w> ; <w n="44.6">elle</w> <w n="44.7">les</w> <w n="44.8">sent</w> <w n="44.9">près</w> <w n="44.10">d</w>’<w n="44.11">elle</w> !</l>
						</lg>
						<lg n="4">
							<l n="45" num="4.1"><w n="45.1">Mais</w> <w n="45.2">la</w> <w n="45.3">lune</w> <w n="45.4">se</w> <w n="45.5">lève</w>, <w n="45.6">et</w> <w n="45.7">pâlit</w> <w n="45.8">mes</w> <w n="45.9">crayons</w>.</l>
							<l n="46" num="4.2"><w n="46.1">Ne</w> <w n="46.2">bravez</w> <w n="46.3">pas</w> <w n="46.4">dans</w> <w n="46.5">l</w>’<w n="46.6">eau</w> <w n="46.7">ses</w> <w n="46.8">humides</w> <w n="46.9">rayons</w> ;</l>
							<l n="47" num="4.3"><w n="47.1">Les</w> <w n="47.2">pavots</w> <w n="47.3">vont</w> <w n="47.4">pleuvoir</w> <w n="47.5">sur</w> <w n="47.6">sa</w> <w n="47.7">lente</w> <w n="47.8">carrière</w>.</l>
							<l n="48" num="4.4"><w n="48.1">Au</w> <w n="48.2">ciel</w>, <w n="48.3">qui</w> <w n="48.4">donne</w> <w n="48.5">tout</w>, <w n="48.6">offrez</w> <w n="48.7">votre</w> <w n="48.8">prière</w> ;</l>
							<l n="49" num="4.5"><w n="49.1">Elle</w> <w n="49.2">est</w> <w n="49.3">pure</w> <w n="49.4">et</w> <w n="49.5">charmante</w>, <w n="49.6">et</w> <w n="49.7">vous</w> <w n="49.8">la</w> <w n="49.9">dites</w> <w n="49.10">bien</w>.</l>
							<l n="50" num="4.6"><w n="50.1">La</w> <w n="50.2">voix</w> <w n="50.3">est</w> <w n="50.4">faible</w> <w n="50.5">encor</w> ; <w n="50.6">mais</w> <w n="50.7">c</w>’<w n="50.8">est</w> <w n="50.9">Dieu</w> <w n="50.10">qui</w> <w n="50.11">l</w>’<w n="50.12">écoute</w> !</l>
							<l n="51" num="4.7"><w n="51.1">Un</w> <w n="51.2">faible</w> <w n="51.3">accent</w> <w n="51.4">vers</w> <w n="51.5">lui</w> <w n="51.6">sait</w> <w n="51.7">trouver</w> <w n="51.8">une</w> <w n="51.9">route</w> ;</l>
							<l n="52" num="4.8"><w n="52.1">Il</w> <w n="52.2">entend</w> <w n="52.3">un</w> <w n="52.4">soupir</w> ; <w n="52.5">il</w> <w n="52.6">ne</w> <w n="52.7">dédaigne</w> <w n="52.8">rien</w>.</l>
							<l n="53" num="4.9"><w n="53.1">Et</w> <w n="53.2">maintenant</w> <w n="53.3">dormez</w> ! <w n="53.4">Leurs</w> <w n="53.5">mains</w> <w n="53.6">entrelacées</w></l>
							<l n="54" num="4.10"><w n="54.1">Semblent</w> <w n="54.2">lier</w> <w n="54.3">encor</w> <w n="54.4">leurs</w> <w n="54.5">naïves</w> <w n="54.6">pensées</w>.</l>
							<l n="55" num="4.11"><w n="55.1">Hélas</w> ! <w n="55.2">ces</w> <w n="55.3">cœurs</w> <w n="55.4">aimants</w> <w n="55.5">qu</w>’<w n="55.6">elles</w> <w n="55.7">viennent</w> <w n="55.8">d</w>’<w n="55.9">unir</w>,</l>
							<l n="56" num="4.12"><w n="56.1">Ne</w> <w n="56.2">les</w> <w n="56.3">séparez</w> <w n="56.4">pas</w>, <w n="56.5">mon</w> <w n="56.6">Dieu</w>, <w n="56.7">dans</w> <w n="56.8">l</w>’<w n="56.9">avenir</w> !</l>
						</lg>
						<lg n="5">
							<l n="57" num="5.1"><w n="57.1">Ils</w> <w n="57.2">dorment</w>. <w n="57.3">Qu</w>’<w n="57.4">ils</w> <w n="57.5">sont</w> <w n="57.6">beaux</w> ! <w n="57.7">que</w> <w n="57.8">leur</w> <w n="57.9">mère</w> <w n="57.10">est</w> <w n="57.11">heureuse</w> !</l>
							<l n="58" num="5.2"><w n="58.1">Dieu</w> <w n="58.2">n</w>’<w n="58.3">a</w> <w n="58.4">pas</w> <w n="58.5">oublié</w> <w n="58.6">ma</w> <w n="58.7">plainte</w> <w n="58.8">douloureuse</w> ;</l>
							<l n="59" num="5.3"><w n="59.1">Sa</w> <w n="59.2">pitié</w> <w n="59.3">m</w>’<w n="59.4">écouta</w>… <w n="59.5">Tout</w> <w n="59.6">ce</w> <w n="59.7">que</w> <w n="59.8">j</w>’<w n="59.9">ai</w> <w n="59.10">perdu</w>,</l>
							<l n="60" num="5.4"><w n="60.1">Sa</w> <w n="60.2">pitié</w>, <w n="60.3">je</w> <w n="60.4">le</w> <w n="60.5">sens</w>, <w n="60.6">me</w> <w n="60.7">l</w>’<w n="60.8">a</w> <w n="60.9">presque</w> <w n="60.10">rendu</w> !</l>
						</lg>
						<lg n="6">
							<l n="61" num="6.1"><w n="61.1">Sommeil</w> ! <w n="61.2">ange</w> <w n="61.3">invisible</w>, <w n="61.4">aux</w> <w n="61.5">ailes</w> <w n="61.6">caressantes</w>,</l>
							<l n="62" num="6.2"><w n="62.1">Verse</w> <w n="62.2">sur</w> <w n="62.3">mes</w> <w n="62.4">enfants</w> <w n="62.5">tes</w> <w n="62.6">fleurs</w> <w n="62.7">assoupissantes</w> ;</l>
							<l n="63" num="6.3"><w n="63.1">Que</w> <w n="63.2">ton</w> <w n="63.3">baiser</w> <w n="63.4">de</w> <w n="63.5">miel</w> <w n="63.6">enveloppe</w> <w n="63.7">leurs</w> <w n="63.8">yeux</w>,</l>
							<l n="64" num="6.4"><w n="64.1">Que</w> <w n="64.2">ton</w> <w n="64.3">vague</w> <w n="64.4">miroir</w> <w n="64.5">réfléchisse</w> <w n="64.6">leurs</w> <w n="64.7">jeux</w> ;</l>
							<l n="65" num="6.5"><w n="65.1">Au</w> <w n="65.2">pied</w> <w n="65.3">de</w> <w n="65.4">ce</w> <w n="65.5">berceau</w> <w n="65.6">que</w> <w n="65.7">mon</w> <w n="65.8">amour</w> <w n="65.9">balance</w>,</l>
							<l n="66" num="6.6"><w n="66.1">Fais</w> <w n="66.2">asseoir</w> <w n="66.3">avec</w> <w n="66.4">toi</w> <w n="66.5">l</w>’<w n="66.6">immobile</w> <w n="66.7">silence</w>.</l>
							<l n="67" num="6.7"><w n="67.1">Ma</w> <w n="67.2">prière</w> <w n="67.3">est</w> <w n="67.4">sans</w> <w n="67.5">voix</w> ; <w n="67.6">mais</w> <w n="67.7">elle</w> <w n="67.8">brûle</w> <w n="67.9">encor</w>.</l>
							<l n="68" num="6.8"><w n="68.1">Dieu</w> ! <w n="68.2">bénissez</w> <w n="68.3">ma</w> <w n="68.4">nuit</w> ! <w n="68.5">Dieu</w> ! <w n="68.6">gardez</w> <w n="68.7">mon</w> <w n="68.8">trésor</w> !</l>
						</lg>
					</div></body></text></TEI>