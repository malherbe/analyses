<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>9618 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>9618 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies</title>
					<author>Marceline Desbordes-Valmore</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>A. Boulland, libraire-éditeur</publisher>
						<date when="1830">1830</date>
					</imprint>
					<extent>9618 verstome second</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-12-20" who="RR">Insertion des corrections métriques dans le corps du texte.</change>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
				<change when="2018-11-02" who="RR">Correction dans le poème "Dors, ma Mère".</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">POÉSIES DIVERSES</head><div type="poem" key="DES142">
						<head type="main">LE BILLET D’UNE AMIE</head>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">Oh</w> ! <w n="1.2">qu</w>’<w n="1.3">il</w> <w n="1.4">ne</w> <w n="1.5">fût</w>, <w n="1.6">m</w>’<w n="1.7">écrivait</w> <w n="1.8">une</w> <w n="1.9">amie</w>,</l>
							<l n="2" num="1.2"><w n="2.1">Entre</w> <w n="2.2">nous</w> <w n="2.3">deux</w> <w n="2.4">qu</w>’<w n="2.5">un</w> <w n="2.6">fleuve</w> <w n="2.7">à</w> <w n="2.8">traverser</w> !</l>
							<l n="3" num="1.3"><w n="3.1">J</w>’<w n="3.2">irais</w> <w n="3.3">sans</w> <w n="3.4">peur</w> <w n="3.5">cette</w> <w n="3.6">nuit</w> <w n="3.7">t</w>’<w n="3.8">embrasser</w>,</l>
							<l n="4" num="1.4"><w n="4.1">Et</w> <w n="4.2">doucement</w> <w n="4.3">te</w> <w n="4.4">surprendre</w> <w n="4.5">endormie</w> ;</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1"><w n="5.1">Je</w> <w n="5.2">braverais</w> <w n="5.3">le</w> <w n="5.4">terrible</w> <w n="5.5">élément</w> ;</l>
							<l n="6" num="2.2"><w n="6.1">Et</w> <w n="6.2">quelque</w> <w n="6.3">flot</w>, <w n="6.4">ému</w> <w n="6.5">de</w> <w n="6.6">mon</w> <w n="6.7">courage</w>,</l>
							<l n="7" num="2.3"><w n="7.1">Me</w> <w n="7.2">pousserait</w> <w n="7.3">jusques</w> <w n="7.4">à</w> <w n="7.5">ton</w> <w n="7.6">rivage</w>,</l>
							<l n="8" num="2.4"><w n="8.1">Où</w> <w n="8.2">l</w>’<w n="8.3">amitié</w> <w n="8.4">serait</w> <w n="8.5">mon</w> <w n="8.6">seul</w> <w n="8.7">aimant</w>.</l>
						</lg>
						<lg n="3">
							<l n="9" num="3.1"><w n="9.1">De</w> <w n="9.2">l</w>’<w n="9.3">eau</w> <w n="9.4">qui</w> <w n="9.5">fuit</w> <w n="9.6">dans</w> <w n="9.7">cette</w> <w n="9.8">nuit</w> <w n="9.9">obscure</w></l>
							<l n="10" num="3.2"><w n="10.1">J</w>’<w n="10.2">affronterais</w> <w n="10.3">le</w> <w n="10.4">roulement</w> <w n="10.5">grondeur</w> ;</l>
							<l n="11" num="3.3"><w n="11.1">Car</w> <w n="11.2">de</w> <w n="11.3">cette</w> <w n="11.4">eau</w>, <w n="11.5">froide</w>, <w n="11.6">limpide</w> <w n="11.7">et</w> <w n="11.8">pure</w>,</l>
							<l n="12" num="3.4"><w n="12.1">L</w>’<w n="12.2">embrassement</w> <w n="12.3">rafraîchirait</w> <w n="12.4">mon</w> <w n="12.5">cœur</w>.</l>
						</lg>
						<lg n="4">
							<l n="13" num="4.1"><w n="13.1">Ce</w> <w n="13.2">cœur</w> <w n="13.3">blessé</w>, <w n="13.4">qui</w> <w n="13.5">ne</w> <w n="13.6">bat</w> <w n="13.7">plus</w> <w n="13.8">qu</w>’<w n="13.9">à</w> <w n="13.10">peine</w>,</l>
							<l n="14" num="4.2"><w n="14.1">Respirerait</w> <w n="14.2">pour</w> <w n="14.3">s</w>’<w n="14.4">élancer</w> <w n="14.5">vers</w> <w n="14.6">toi</w>.</l>
							<l n="15" num="4.3"><w n="15.1">Il</w> <w n="15.2">est</w> <w n="15.3">si</w> <w n="15.4">doux</w> <w n="15.5">de</w> <w n="15.6">soulever</w> <w n="15.7">sa</w> <w n="15.8">chaîne</w>,</l>
							<l n="16" num="4.4"><w n="16.1">Et</w> <w n="16.2">de</w> <w n="16.3">se</w> <w n="16.4">dire</w> : <w n="16.5">on</w> <w n="16.6">la</w> <w n="16.7">porte</w> <w n="16.8">avec</w> <w n="16.9">moi</w> !</l>
						</lg>
						<lg n="5">
							<l n="17" num="5.1"><w n="17.1">Des</w> <w n="17.2">flots</w> <w n="17.3">amers</w> <w n="17.4">et</w> <w n="17.5">du</w> <w n="17.6">bruit</w> <w n="17.7">de</w> <w n="17.8">la</w> <w n="17.9">vie</w></l>
							<l n="18" num="5.2"><w n="18.1">J</w>’<w n="18.2">irais</w> <w n="18.3">sauver</w> <w n="18.4">ou</w> <w n="18.5">distraire</w> <w n="18.6">mon</w> <w n="18.7">sort</w>,</l>
							<l n="19" num="5.3"><w n="19.1">Et</w>, <w n="19.2">je</w> <w n="19.3">le</w> <w n="19.4">sens</w>, <w n="19.5">tenter</w> <w n="19.6">un</w> <w n="19.7">vain</w> <w n="19.8">effort</w>,</l>
							<l n="20" num="5.4"><w n="20.1">Pour</w> <w n="20.2">retourner</w> <w n="20.3">à</w> <w n="20.4">mes</w> <w n="20.5">fers</w> <w n="20.6">asservie</w>.</l>
						</lg>
						<lg n="6">
							<l n="21" num="6.1"><w n="21.1">J</w>’<w n="21.2">irais</w> <w n="21.3">pleurer</w> <w n="21.4">à</w> <w n="21.5">ta</w> <w n="21.6">porte</w>, <w n="21.7">où</w> <w n="21.8">ma</w> <w n="21.9">voix</w></l>
							<l n="22" num="6.2"><w n="22.1">T</w>’<w n="22.2">attirerait</w> <w n="22.3">courageuse</w> <w n="22.4">et</w> <w n="22.5">timide</w> ;</l>
							<l n="23" num="6.3"><w n="23.1">En</w> <w n="23.2">saisissant</w> <w n="23.3">ma</w> <w n="23.4">main</w> <w n="23.5">encore</w> <w n="23.6">humide</w>,</l>
							<l n="24" num="6.4"><w n="24.1">Tu</w> <w n="24.2">me</w> <w n="24.3">plaindrais</w> : <w n="24.4">je</w> <w n="24.5">t</w>’<w n="24.6">ai</w> <w n="24.7">plainte</w> <w n="24.8">une</w> <w n="24.9">fois</w> !</l>
						</lg>
						<lg n="7">
							<l n="25" num="7.1"><w n="25.1">Quand</w> <w n="25.2">tu</w> <w n="25.3">partis</w>, <w n="25.4">oui</w>, <w n="25.5">j</w>’<w n="25.6">ai</w> <w n="25.7">plaint</w> <w n="25.8">ton</w> <w n="25.9">courage</w> :</l>
							<l n="26" num="7.2"><w n="26.1">J</w>’<w n="26.2">avais</w> <w n="26.3">tout</w> <w n="26.4">lu</w> <w n="26.5">dans</w> <w n="26.6">tes</w> <w n="26.7">yeux</w> <w n="26.8">qui</w> <w n="26.9">parlaient</w> ;</l>
							<l n="27" num="7.3"><w n="27.1">De</w> <w n="27.2">ta</w> <w n="27.3">pudeur</w> <w n="27.4">j</w>’<w n="27.5">imitais</w> <w n="27.6">le</w> <w n="27.7">langage</w>,</l>
							<l n="28" num="7.4"><w n="28.1">J</w>’<w n="28.2">étais</w> <w n="28.3">muette</w>, <w n="28.4">et</w> <w n="28.5">mes</w> <w n="28.6">larmes</w> <w n="28.7">coulaient</w>.</l>
						</lg>
						<lg n="8">
							<l n="29" num="8.1"><w n="29.1">Tes</w> <w n="29.2">vœux</w> <w n="29.3">brisés</w>, <w n="29.4">ta</w> <w n="29.5">blessure</w> <w n="29.6">profonde</w>,</l>
							<l n="30" num="8.2"><w n="30.1">Tous</w> <w n="30.2">tes</w> <w n="30.3">ennuis</w> <w n="30.4">répandus</w> <w n="30.5">sur</w> <w n="30.6">mes</w> <w n="30.7">jours</w>,</l>
							<l n="31" num="8.3"><w n="31.1">Ces</w> <w n="31.2">maux</w> <w n="31.3">affreux</w> <w n="31.4">qui</w> <w n="31.5">font</w> <w n="31.6">haïr</w> <w n="31.7">le</w> <w n="31.8">monde</w>,</l>
							<l n="32" num="8.4"><w n="32.1">En</w> <w n="32.2">les</w> <w n="32.3">fuyant</w>, <w n="32.4">s</w>’<w n="32.5">en</w> <w n="32.6">souvient</w>-<w n="32.7">on</w> <w n="32.8">toujours</w> ?</l>
						</lg>
						<lg n="9">
							<l n="33" num="9.1"><w n="33.1">Me</w> <w n="33.2">rendrais</w>-<w n="33.3">tu</w> <w n="33.4">ma</w> <w n="33.5">paix</w> <w n="33.6">évanouie</w> ?</l>
							<l n="34" num="9.2"><w n="34.1">Si</w>, <w n="34.2">dans</w> <w n="34.3">ton</w> <w n="34.4">sein</w>, <w n="34.5">gémissante</w> <w n="34.6">aujourd</w>’<w n="34.7">hui</w>,</l>
							<l n="35" num="9.3"><w n="35.1">Je</w> <w n="35.2">m</w>’<w n="35.3">écriais</w> : « <w n="35.4">Ma</w> <w n="35.5">chère</w>, <w n="35.6">il</w> <w n="35.7">m</w>’<w n="35.8">a</w> <w n="35.9">trahie</w> ! »</l>
							<l n="36" num="9.4"><w n="36.1">Répondrais</w>-<w n="36.2">tu</w> : « <w n="36.3">Pleure</w>, <w n="36.4">et</w> <w n="36.5">pardonne</w>-<w n="36.6">lui</w> ? »</l>
						</lg>
						<lg n="10">
							<l n="37" num="10.1"><w n="37.1">Comme</w> <w n="37.2">elle</w> <w n="37.3">aimait</w> ! <w n="37.4">quelle</w> <w n="37.5">âme</w> <w n="37.6">tendre</w> <w n="37.7">et</w> <w n="37.8">pure</w></l>
							<l n="38" num="10.2"><w n="38.1">M</w>’<w n="38.2">a</w> <w n="38.3">révélé</w> <w n="38.4">ce</w> <w n="38.5">douloureux</w> <w n="38.6">transport</w> !</l>
							<l n="39" num="10.3"><w n="39.1">Ah</w> ! <w n="39.2">si</w> <w n="39.3">l</w>’<w n="39.4">Amour</w> <w n="39.5">lui</w> <w n="39.6">fut</w> <w n="39.7">vraiment</w> <w n="39.8">parjure</w>,</l>
							<l n="40" num="10.4"><w n="40.1">Je</w> <w n="40.2">hais</w> <w n="40.3">l</w>’<w n="40.4">Amour</w>… <w n="40.5">Eh</w> <w n="40.6">quoi</w> ! <w n="40.7">l</w>’<w n="40.8">aimais</w>-<w n="40.9">je</w> <w n="40.10">encor</w> ?</l>
						</lg>
					</div></body></text></TEI>