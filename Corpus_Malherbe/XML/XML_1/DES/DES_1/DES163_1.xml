<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>9618 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>9618 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies</title>
					<author>Marceline Desbordes-Valmore</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>A. Boulland, libraire-éditeur</publisher>
						<date when="1830">1830</date>
					</imprint>
					<extent>9618 verstome second</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-12-20" who="RR">Insertion des corrections métriques dans le corps du texte.</change>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
				<change when="2018-11-02" who="RR">Correction dans le poème "Dors, ma Mère".</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part"> POÉSIES INÉDITES <lb></lb>MÉLANGES</head><div type="poem" key="DES163">
						<head type="main">L’IDIOT</head>
						<head type="sub_1">À MADAME PAULINE DUCHAMBGE</head>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">Avec</w> <w n="1.2">l</w>’<w n="1.3">aube</w> <w n="1.4">toujours</w> <w n="1.5">ta</w> <w n="1.6">plainte</w> <w n="1.7">me</w> <w n="1.8">réveille</w>,</l>
							<l n="2" num="1.2"><w n="2.1">André</w> ! <w n="2.2">toujours</w> <w n="2.3">ton</w> <w n="2.4">nom</w> <w n="2.5">tourmente</w> <w n="2.6">mon</w> <w n="2.7">oreille</w> ;</l>
							<l n="3" num="1.3"><w n="3.1">Car</w> <w n="3.2">toujours</w> <w n="3.3">sans</w> <w n="3.4">pitié</w>, <w n="3.5">persécuteurs</w> <w n="3.6">enfants</w>,</l>
							<l n="4" num="1.4"><w n="4.1">Vous</w> <w n="4.2">brisez</w> <w n="4.3">son</w> <w n="4.4">sommeil</w> <w n="4.5">par</w> <w n="4.6">vos</w> <w n="4.7">cris</w> <w n="4.8">triomphants</w>.</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1"><w n="5.1">Il</w> <w n="5.2">dormait</w>. <w n="5.3">De</w> <w n="5.4">la</w> <w n="5.5">nuit</w> <w n="5.6">la</w> <w n="5.7">fraîcheur</w> <w n="5.8">salutaire</w></l>
							<l n="6" num="2.2"><w n="6.1">Peut</w>-<w n="6.2">être</w> <w n="6.3">dans</w> <w n="6.4">son</w> <w n="6.5">sein</w> <w n="6.6">versait</w> <w n="6.7">un</w> <w n="6.8">songe</w> <w n="6.9">heureux</w>.</l>
							<l n="7" num="2.3"><w n="7.1">Quel</w> <w n="7.2">autre</w> <w n="7.3">bien</w> <w n="7.4">attend</w> <w n="7.5">l</w>’<w n="7.6">orphelin</w> <w n="7.7">solitaire</w> ?</l>
							<l n="8" num="2.4"><space quantity="6" unit="char"></space><w n="8.1">Son</w> <w n="8.2">réveil</w> <w n="8.3">est</w> <w n="8.4">si</w> <w n="8.5">douloureux</w> !</l>
							<l n="9" num="2.5"><w n="9.1">Dans</w> <w n="9.2">le</w> <w n="9.3">sommeil</w> <w n="9.4">du</w> <w n="9.5">moins</w>, <w n="9.6">l</w>’<w n="9.7">oubli</w> <w n="9.8">vient</w>, <w n="9.9">le</w> <w n="9.10">sort</w> <w n="9.11">change</w> ;</l>
							<l n="10" num="2.6"><w n="10.1">Et</w>, <w n="10.2">couché</w> <w n="10.3">sur</w> <w n="10.4">la</w> <w n="10.5">terre</w> <w n="10.6">où</w> <w n="10.7">le</w> <w n="10.8">soleil</w> <w n="10.9">a</w> <w n="10.10">lui</w>,</l>
							<l n="11" num="2.7"><space quantity="6" unit="char"></space><w n="11.1">Qui</w> <w n="11.2">sait</w> <w n="11.3">s</w>’<w n="11.4">il</w> <w n="11.5">ne</w> <w n="11.6">voit</w> <w n="11.7">pas</w> <w n="11.8">un</w> <w n="11.9">Ange</w></l>
							<l n="12" num="2.8"><space quantity="6" unit="char"></space><w n="12.1">Sourire</w> <w n="12.2">ou</w> <w n="12.3">pleurer</w> <w n="12.4">avec</w> <w n="12.5">lui</w> ?</l>
						</lg>
						<lg n="3">
							<l n="13" num="3.1"><w n="13.1">Pourquoi</w> <w n="13.2">faire</w> <w n="13.3">envoler</w> <w n="13.4">son</w> <w n="13.5">erreur</w> <w n="13.6">décevante</w> ?</l>
							<l n="14" num="3.2"><w n="14.1">Regardez</w>, <w n="14.2">inhumains</w>, <w n="14.3">cet</w> <w n="14.4">être</w> <w n="14.5">languissant</w>,</l>
							<l n="15" num="3.3"><w n="15.1">Comme</w> <w n="15.2">un</w> <w n="15.3">chevreuil</w> <w n="15.4">blessé</w> <w n="15.5">que</w> <w n="15.6">la</w> <w n="15.7">meute</w> <w n="15.8">épouvante</w>,</l>
							<l n="16" num="3.4"><w n="16.1">Essayer</w> <w n="16.2">pour</w> <w n="16.3">vous</w> <w n="16.4">fuir</w> <w n="16.5">un</w> <w n="16.6">effort</w> <w n="16.7">impuissant</w>.</l>
							<l n="17" num="3.5"><w n="17.1">Eh</w> ! <w n="17.2">que</w> <w n="17.3">vous</w> <w n="17.4">a</w>-<w n="17.5">t</w>-<w n="17.6">il</w> <w n="17.7">fait</w> ? <w n="17.8">Laissez</w> <w n="17.9">passer</w> <w n="17.10">sa</w> <w n="17.11">vie</w></l>
							<l n="18" num="3.6"><w n="18.1">Sous</w> <w n="18.2">le</w> <w n="18.3">nuage</w> <w n="18.4">triste</w> <w n="18.5">où</w> <w n="18.6">Dieu</w> <w n="18.7">l</w>’<w n="18.8">enveloppa</w> :</l>
							<l n="19" num="3.7"><w n="19.1">Il</w> <w n="19.2">n</w>’<w n="19.3">a</w> <w n="19.4">plus</w> <w n="19.5">sa</w> <w n="19.6">raison</w> <w n="19.7">que</w> <w n="19.8">le</w> <w n="19.9">malheur</w> <w n="19.10">frappa</w> ;</l>
							<l n="20" num="3.8"><w n="20.1">Mais</w> <w n="20.2">votre</w> <w n="20.3">voix</w> <w n="20.4">est</w> <w n="20.5">dure</w> ; <w n="20.6">et</w> <w n="20.7">tout</w> <w n="20.8">ce</w> <w n="20.9">qu</w>’<w n="20.10">il</w> <w n="20.11">envie</w>,</l>
							<l n="21" num="3.9"><w n="21.1">C</w>’<w n="21.2">est</w> <w n="21.3">l</w>’<w n="21.4">indulgent</w> <w n="21.5">silence</w> : <w n="21.6">il</w> <w n="21.7">parle</w> <w n="21.8">au</w> <w n="21.9">malheureux</w>,</l>
							<l n="22" num="3.10"><w n="22.1">Il</w> <w n="22.2">assoupit</w> <w n="22.3">l</w>’<w n="22.4">éclat</w> <w n="22.5">de</w> <w n="22.6">vos</w> <w n="22.7">rires</w> <w n="22.8">affreux</w>.</l>
							<l n="23" num="3.11"><w n="23.1">Quand</w> <w n="23.2">vous</w> <w n="23.3">l</w>’<w n="23.4">avez</w> <w n="23.5">blessé</w> <w n="23.6">de</w> <w n="23.7">vos</w> <w n="23.8">cruelles</w> <w n="23.9">armes</w>,</l>
							<l n="24" num="3.12"><w n="24.1">André</w> <w n="24.2">frappe</w> <w n="24.3">son</w> <w n="24.4">cœur</w> <w n="24.5">où</w> <w n="24.6">s</w>’<w n="24.7">amassent</w> <w n="24.8">ses</w> <w n="24.9">larmes</w>.</l>
							<l n="25" num="3.13"><w n="25.1">L</w>’<w n="25.2">homme</w>, <w n="25.3">pour</w> <w n="25.4">tous</w> <w n="25.5">ses</w> <w n="25.6">jours</w>, <w n="25.7">en</w> <w n="25.8">apporte</w> <w n="25.9">en</w> <w n="25.10">naissant</w> ;</l>
							<l n="26" num="3.14"><w n="26.1">C</w>’<w n="26.2">est</w> <w n="26.3">le</w> <w n="26.4">calice</w> <w n="26.5">amer</w> <w n="26.6">où</w> <w n="26.7">son</w> <w n="26.8">orgueil</w> <w n="26.9">s</w>’<w n="26.10">abreuve</w>.</l>
							<l n="27" num="3.15"><w n="27.1">Bientôt</w>, <w n="27.2">jeunes</w> <w n="27.3">railleurs</w>, <w n="27.4">vous</w> <w n="27.5">en</w> <w n="27.6">ferez</w> <w n="27.7">l</w>’<w n="27.8">épreuve</w>,</l>
							<l n="28" num="3.16"><w n="28.1">Et</w> <w n="28.2">le</w> <w n="28.3">plus</w> <w n="28.4">gai</w> <w n="28.5">de</w> <w n="28.6">vous</w> <w n="28.7">s</w>’<w n="28.8">en</w> <w n="28.9">ira</w> <w n="28.10">gémissant</w>.</l>
							<l n="29" num="3.17"><w n="29.1">Vos</w> <w n="29.2">teints</w> <w n="29.3">de</w> <w n="29.4">fleur</w>, <w n="29.5">vos</w> <w n="29.6">jeux</w>, <w n="29.7">votre</w> <w n="29.8">éclatante</w> <w n="29.9">joie</w>,</l>
							<l n="30" num="3.18"><w n="30.1">Votre</w> <w n="30.2">âge</w> <w n="30.3">audacieux</w> <w n="30.4">qui</w> <w n="30.5">croit</w> <w n="30.6">régner</w> <w n="30.7">toujours</w>,</l>
							<l n="31" num="3.19"><w n="31.1">Du</w> <w n="31.2">temps</w> <w n="31.3">qui</w> <w n="31.4">raille</w> <w n="31.5">aussi</w> <w n="31.6">seront</w> <w n="31.7">bientôt</w> <w n="31.8">la</w> <w n="31.9">proie</w> :</l>
							<l n="32" num="3.20"><space quantity="6" unit="char"></space><w n="32.1">Vous</w> <w n="32.2">serez</w> <w n="32.3">vieux</w> <w n="32.4">dans</w> <w n="32.5">quelques</w> <w n="32.6">jours</w>.</l>
							<l n="33" num="3.21"><space quantity="6" unit="char"></space><w n="33.1">Des</w> <w n="33.2">vieillards</w> <w n="33.3">assis</w> <w n="33.4">sur</w> <w n="33.5">les</w> <w n="33.6">places</w>,</l>
							<l n="34" num="3.22"><space quantity="6" unit="char"></space><w n="34.1">À</w> <w n="34.2">l</w>’<w n="34.3">ombre</w> <w n="34.4">des</w> <w n="34.5">ormeaux</w> <w n="34.6">vivaces</w></l>
							<l n="35" num="3.23"><space quantity="6" unit="char"></space><w n="35.1">Qu</w>’<w n="35.2">ils</w> <w n="35.3">y</w> <w n="35.4">plantèrent</w> <w n="35.5">autrefois</w>,</l>
							<l n="36" num="3.24"><w n="36.1">Vous</w> <w n="36.2">aurez</w> <w n="36.3">la</w> <w n="36.4">langueur</w> <w n="36.5">et</w> <w n="36.6">les</w> <w n="36.7">débiles</w> <w n="36.8">voix</w> ;</l>
							<l n="37" num="3.25"><w n="37.1">La</w> <w n="37.2">vie</w> <w n="37.3">à</w> <w n="37.4">vos</w> <w n="37.5">regards</w> <w n="37.6">retirera</w> <w n="37.7">ses</w> <w n="37.8">flammes</w> ;</l>
							<l n="38" num="3.26"><w n="38.1">Vous</w> <w n="38.2">croirez</w> <w n="38.3">que</w> <w n="38.4">l</w>’<w n="38.5">oiseau</w> <w n="38.6">vous</w> <w n="38.7">refuse</w> <w n="38.8">son</w> <w n="38.9">chant</w> ;</l>
							<l n="39" num="3.27"><w n="39.1">Quelque</w> <w n="39.2">chose</w> <w n="39.3">d</w>’<w n="39.4">amer</w> <w n="39.5">coulera</w> <w n="39.6">dans</w> <w n="39.7">vos</w> <w n="39.8">âmes</w>,</l>
							<l n="40" num="3.28"><space quantity="6" unit="char"></space><w n="40.1">Car</w> <w n="40.2">vous</w> <w n="40.3">direz</w> : « <w n="40.4">Je</w> <w n="40.5">fus</w> <w n="40.6">méchant</w> ! »</l>
							<l n="41" num="3.29"><w n="41.1">Dieu</w> <w n="41.2">plaindra</w> <w n="41.3">du</w> <w n="41.4">roseau</w> <w n="41.5">le</w> <w n="41.6">naufrage</w> <w n="41.7">rapide</w>,</l>
							<l n="42" num="3.30"><w n="42.1">Bien</w> <w n="42.2">qu</w>’<w n="42.3">il</w> <w n="42.4">fasse</w> <w n="42.5">en</w> <w n="42.6">tournant</w> <w n="42.7">rire</w> <w n="42.8">les</w> <w n="42.9">matelots</w> !</l>
							<l n="43" num="3.31">« <w n="43.1">Qu</w>’<w n="43.2">eût</w>-<w n="43.3">il</w> <w n="43.4">vu</w>, <w n="43.5">disent</w>-<w n="43.6">ils</w>, <w n="43.7">dans</w> <w n="43.8">son</w> <w n="43.9">destin</w> <w n="43.10">timide</w> ?</l>
							<l n="44" num="3.32"><w n="44.1">Il</w> <w n="44.2">eût</w> <w n="44.3">bordé</w> <w n="44.4">la</w> <w n="44.5">rive</w> <w n="44.6">et</w> <w n="44.7">caressé</w> <w n="44.8">les</w> <w n="44.9">flots</w> ! »</l>
						</lg>
						<lg n="4">
							<l n="45" num="4.1"><w n="45.1">Triste</w> <w n="45.2">un</w> <w n="45.3">jour</w> <w n="45.4">comme</w> <w n="45.5">André</w>, <w n="45.6">je</w> <w n="45.7">suivis</w> <w n="45.8">sa</w> <w n="45.9">détresse</w> :</l>
							<l n="46" num="4.2"><w n="46.1">Loin</w> <w n="46.2">de</w> <w n="46.3">la</w> <w n="46.4">ville</w> <w n="46.5">heureuse</w> <w n="46.6">elle</w> <w n="46.7">nous</w> <w n="46.8">égara</w>.</l>
							<l n="47" num="4.3"><w n="47.1">L</w>’<w n="47.2">église</w> <w n="47.3">du</w> <w n="47.4">coteau</w> <w n="47.5">fit</w> <w n="47.6">rêver</w> <w n="47.7">sa</w> <w n="47.8">tristesse</w> ;</l>
							<l n="48" num="4.4"><w n="48.1">Il</w> <w n="48.2">salua</w> <w n="48.3">l</w>’<w n="48.4">église</w>, <w n="48.5">et</w> <w n="48.6">puis</w> <w n="48.7">il</w> <w n="48.8">soupira</w>.</l>
							<l n="49" num="4.5"><w n="49.1">Chancelant</w> <w n="49.2">et</w> <w n="49.3">courbé</w> <w n="49.4">sur</w> <w n="49.5">son</w> <w n="49.6">appui</w> <w n="49.7">de</w> <w n="49.8">frêne</w>,</l>
							<l n="50" num="4.6"><w n="50.1">Il</w> <w n="50.2">s</w>’<w n="50.3">arrêtait</w> <w n="50.4">pensif</w>, <w n="50.5">il</w> <w n="50.6">cueillait</w> <w n="50.7">une</w> <w n="50.8">fleur</w> ;</l>
							<l n="51" num="4.7"><w n="51.1">Et</w> <w n="51.2">du</w> <w n="51.3">jeune</w> <w n="51.4">idiot</w> <w n="51.5">la</w> <w n="51.6">mousse</w> <w n="51.7">et</w> <w n="51.8">le</w> <w n="51.9">troène</w></l>
							<l n="52" num="4.8"><space quantity="10" unit="char"></space><w n="52.1">Couronnaient</w> <w n="52.2">la</w> <w n="52.3">pâleur</w>.</l>
						</lg>
						<lg n="5">
							<l n="53" num="5.1"><space quantity="2" unit="char"></space><w n="53.1">Le</w> <w n="53.2">vent</w> <w n="53.3">qui</w> <w n="53.4">passe</w> <w n="53.5">et</w> <w n="53.6">courbe</w> <w n="53.7">la</w> <w n="53.8">verdure</w></l>
							<l n="54" num="5.2"><w n="54.1">Étonnait</w> <w n="54.2">son</w> <w n="54.3">oreille</w> ; <w n="54.4">il</w> <w n="54.5">cherchait</w> <w n="54.6">ce</w> <w n="54.7">murmure</w>,</l>
							<l n="55" num="5.3"><w n="55.1">Et</w> <w n="55.2">comptait</w> <w n="55.3">sur</w> <w n="55.4">ses</w> <w n="55.5">doigts</w> <w n="55.6">le</w> <w n="55.7">brisement</w> <w n="55.8">égal</w></l>
							<l n="56" num="5.4"><w n="56.1">De</w> <w n="56.2">l</w>’<w n="56.3">eau</w> <w n="56.4">dans</w> <w n="56.5">les</w> <w n="56.6">cailloux</w> <w n="56.7">épurant</w> <w n="56.8">son</w> <w n="56.9">cristal</w>.</l>
							<l n="57" num="5.5"><w n="57.1">Le</w> <w n="57.2">jeu</w> <w n="57.3">d</w>’<w n="57.4">un</w> <w n="57.5">papillon</w>, <w n="57.6">qui</w> <w n="57.7">planait</w> <w n="57.8">sur</w> <w n="57.9">sa</w> <w n="57.10">tête</w>,</l>
							<l n="58" num="5.6"><space quantity="6" unit="char"></space><w n="58.1">Le</w> <w n="58.2">fit</w> <w n="58.3">rire</w> <w n="58.4">et</w> <w n="58.5">tourner</w> <w n="58.6">longtemps</w> ;</l>
							<l n="59" num="5.7"><w n="59.1">Il</w> <w n="59.2">agitait</w> <w n="59.3">ses</w> <w n="59.4">mains</w> <w n="59.5">avec</w> <w n="59.6">un</w> <w n="59.7">air</w> <w n="59.8">de</w> <w n="59.9">fête</w> ;</l>
							<l n="60" num="5.8"><w n="60.1">Et</w> <w n="60.2">puis</w> <w n="60.3">il</w> <w n="60.4">oublia</w> <w n="60.5">l</w>’<w n="60.6">envoyé</w> <w n="60.7">du</w> <w n="60.8">printemps</w>.</l>
							<l n="61" num="5.9"><w n="61.1">Il</w> <w n="61.2">dansa</w>. <w n="61.3">Pauvre</w> <w n="61.4">André</w> ! <w n="61.5">La</w> <w n="61.6">lointaine</w> <w n="61.7">musette</w></l>
							<l n="62" num="5.10"><w n="62.1">Lui</w> <w n="62.2">disait</w> <w n="62.3">que</w> <w n="62.4">la</w> <w n="62.5">danse</w> <w n="62.6">avait</w> <w n="62.7">frappé</w> <w n="62.8">ses</w> <w n="62.9">yeux</w> :</l>
							<l n="63" num="5.11"><w n="63.1">La</w> <w n="63.2">mémoire</w> <w n="63.3">entendait</w>, <w n="63.4">mais</w> <w n="63.5">l</w>’<w n="63.6">âme</w> <w n="63.7">était</w> <w n="63.8">muette</w> ;</l>
							<l n="64" num="5.12"><space quantity="6" unit="char"></space><w n="64.1">Le</w> <w n="64.2">danseur</w> <w n="64.3">n</w>’<w n="64.4">était</w> <w n="64.5">point</w> <w n="64.6">joyeux</w>.</l>
							<l n="65" num="5.13"><w n="65.1">Sa</w> <w n="65.2">faiblesse</w> <w n="65.3">inclinée</w> <w n="65.4">au</w> <w n="65.5">bord</w> <w n="65.6">de</w> <w n="65.7">la</w> <w n="65.8">fontaine</w></l>
							<l n="66" num="5.14"><space quantity="10" unit="char"></space><w n="66.1">Y</w> <w n="66.2">suspendit</w> <w n="66.3">mes</w> <w n="66.4">pas</w> ;</l>
							<l n="67" num="5.15"><w n="67.1">Seul</w>, <w n="67.2">à</w> <w n="67.3">quelque</w> <w n="67.4">ombre</w> <w n="67.5">amie</w> <w n="67.6">il</w> <w n="67.7">racontait</w> <w n="67.8">sa</w> <w n="67.9">peine</w>,</l>
							<l n="68" num="5.16"><space quantity="10" unit="char"></space><w n="68.1">Car</w> <w n="68.2">il</w> <w n="68.3">parlait</w> <w n="68.4">tout</w> <w n="68.5">bas</w>.</l>
							<l n="69" num="5.17">« <w n="69.1">Peut</w>-<w n="69.2">être</w>, <w n="69.3">me</w> <w n="69.4">disais</w>-<w n="69.5">je</w>, <w n="69.6">heureux</w> <w n="69.7">sous</w> <w n="69.8">sa</w> <w n="69.9">couronne</w>,</l>
							<l n="70" num="5.18"><w n="70.1">Plus</w> <w n="70.2">légère</w> <w n="70.3">à</w> <w n="70.4">son</w> <w n="70.5">front</w> <w n="70.6">que</w> <w n="70.7">le</w> <w n="70.8">bandeau</w> <w n="70.9">d</w>’<w n="70.10">un</w> <w n="70.11">roi</w>,</l>
							<l n="71" num="5.19"><w n="71.1">Il</w> <w n="71.2">rend</w> <w n="71.3">grâce</w> <w n="71.4">à</w> <w n="71.5">l</w>’<w n="71.6">air</w> <w n="71.7">libre</w> <w n="71.8">et</w> <w n="71.9">pur</w> <w n="71.10">qui</w> <w n="71.11">l</w>’<w n="71.12">environne</w> ;</l>
							<l n="72" num="5.20"><w n="72.1">À</w> <w n="72.2">l</w>’<w n="72.3">image</w> <w n="72.4">d</w>’<w n="72.5">un</w> <w n="72.6">homme</w> <w n="72.7">il</w> <w n="72.8">sourit</w> <w n="72.9">sans</w> <w n="72.10">effroi</w>. »</l>
							<l n="73" num="5.21"><w n="73.1">Tout</w>-<w n="73.2">à</w>-<w n="73.3">coup</w>, <w n="73.4">de</w> <w n="73.5">ses</w> <w n="73.6">fleurs</w> <w n="73.7">la</w> <w n="73.8">parure</w> <w n="73.9">éphémère</w></l>
							<l n="74" num="5.22"><w n="74.1">D</w>’<w n="74.2">un</w> <w n="74.3">souvenir</w> <w n="74.4">aigu</w> <w n="74.5">sembla</w> <w n="74.6">le</w> <w n="74.7">déchirer</w> ;</l>
							<l n="75" num="5.23"><w n="75.1">Il</w> <w n="75.2">étendit</w> <w n="75.3">les</w> <w n="75.4">bras</w> <w n="75.5">en</w> <w n="75.6">s</w>’<w n="75.7">écriant</w> : « <w n="75.8">Ma</w> <w n="75.9">mère</w> ! »</l>
							<l n="76" num="5.24"><w n="76.1">Et</w> <w n="76.2">plus</w> <w n="76.3">faible</w> <w n="76.4">et</w> <w n="76.5">plus</w> <w n="76.6">pâle</w> <w n="76.7">il</w> <w n="76.8">s</w>’<w n="76.9">assit</w> <w n="76.10">pour</w> <w n="76.11">pleurer</w>.</l>
							<l n="77" num="5.25"><w n="77.1">Dans</w> <w n="77.2">le</w> <w n="77.3">ruisseau</w> <w n="77.4">longtemps</w> <w n="77.5">je</w> <w n="77.6">vis</w> <w n="77.7">tomber</w> <w n="77.8">ses</w> <w n="77.9">larmes</w> ;</l>
							<l n="78" num="5.26"><w n="78.1">À</w> <w n="78.2">leur</w> <w n="78.3">chute</w> <w n="78.4">rapide</w> <w n="78.5">André</w> <w n="78.6">trouvait</w> <w n="78.7">des</w> <w n="78.8">charmes</w>,</l>
							<l n="79" num="5.27"><w n="79.1">Et</w> <w n="79.2">curieusement</w> <w n="79.3">les</w> <w n="79.4">regardait</w> <w n="79.5">couler</w>.</l>
							<l n="80" num="5.28"><w n="80.1">La</w> <w n="80.2">pitié</w> <w n="80.3">m</w>’<w n="80.4">oppressait</w> ; <w n="80.5">je</w> <w n="80.6">ne</w> <w n="80.7">pouvais</w> <w n="80.8">parler</w>.</l>
						</lg>
						<lg n="6">
							<l n="81" num="6.1">« <w n="81.1">André</w> ! <w n="81.2">lui</w> <w n="81.3">dis</w>-<w n="81.4">je</w> <w n="81.5">enfin</w>, <w n="81.6">retourne</w> <w n="81.7">vers</w> <w n="81.8">la</w> <w n="81.9">ville</w>.</l>
							<l n="82" num="6.2"><w n="82.1">Ne</w> <w n="82.2">crains</w>-<w n="82.3">tu</w> <w n="82.4">pas</w> <w n="82.5">la</w> <w n="82.6">nuit</w> <w n="82.7">passée</w> <w n="82.8">hors</w> <w n="82.9">des</w> <w n="82.10">remparts</w> ?</l>
							<l n="83" num="6.3"><w n="83.1">Vois</w>-<w n="83.2">tu</w> <w n="83.3">les</w> <w n="83.4">habitants</w> <w n="83.5">rentrer</w> <w n="83.6">de</w> <w n="83.7">toutes</w> <w n="83.8">parts</w> ?</l>
							<l n="84" num="6.4"><w n="84.1">Va</w> ! <w n="84.2">pauvre</w> <w n="84.3">agneau</w> <w n="84.4">perdu</w>, <w n="84.5">cherche</w> <w n="84.6">au</w> <w n="84.7">moins</w> <w n="84.8">un</w> <w n="84.9">asile</w>. »</l>
							<l n="85" num="6.5"><w n="85.1">Alors</w>, <w n="85.2">sans</w> <w n="85.3">me</w> <w n="85.4">répondre</w>, <w n="85.5">il</w> <w n="85.6">reprit</w> <w n="85.7">son</w> <w n="85.8">chemin</w>.</l>
							<l n="86" num="6.6"><w n="86.1">Il</w> <w n="86.2">était</w> <w n="86.3">sous</w> <w n="86.4">ma</w> <w n="86.5">porte</w> <w n="86.6">assis</w> <w n="86.7">le</w> <w n="86.8">lendemain</w>.</l>
						</lg>
						<lg n="7">
							<l n="87" num="7.1"><w n="87.1">D</w>’<w n="87.2">un</w> <w n="87.3">air</w> <w n="87.4">doux</w> <w n="87.5">et</w> <w n="87.6">stupide</w> <w n="87.7">il</w> <w n="87.8">m</w>’<w n="87.9">offrit</w> <w n="87.10">une</w> <w n="87.11">feuille</w></l>
							<l n="88" num="7.2"><w n="88.1">De</w> <w n="88.2">la</w> <w n="88.3">guirlande</w> <w n="88.4">encor</w> <w n="88.5">pendante</w> <w n="88.6">sur</w> <w n="88.7">son</w> <w n="88.8">front</w>.</l>
							<l n="89" num="7.3"><w n="89.1">Ah</w> ! <w n="89.2">le</w> <w n="89.3">présent</w> <w n="89.4">du</w> <w n="89.5">pauvre</w> <w n="89.6">est</w> <w n="89.7">digne</w> <w n="89.8">qu</w>’<w n="89.9">on</w> <w n="89.10">l</w>’<w n="89.11">accueille</w> ;</l>
							<l n="90" num="7.4"><w n="90.1">Dieu</w> <w n="90.2">veut</w> <w n="90.3">qu</w>’<w n="90.4">il</w> <w n="90.5">soit</w> <w n="90.6">sauvé</w> <w n="90.7">d</w>’<w n="90.8">un</w> <w n="90.9">douloureux</w> <w n="90.10">affront</w>.</l>
							<l n="91" num="7.5"><w n="91.1">Et</w> <w n="91.2">j</w>’<w n="91.3">offris</w> <w n="91.4">à</w> <w n="91.5">mon</w> <w n="91.6">tour</w> <w n="91.7">l</w>’<w n="91.8">espoir</w> <w n="91.9">de</w> <w n="91.10">l</w>’<w n="91.11">infortune</w>,</l>
							<l n="92" num="7.6"><w n="92.1">Ce</w> <w n="92.2">métal</w> <w n="92.3">où</w> <w n="92.4">le</w> <w n="92.5">riche</w> <w n="92.6">attache</w> <w n="92.7">le</w> <w n="92.8">bonheur</w>.</l>
							<l n="93" num="7.7"><space quantity="6" unit="char"></space><w n="93.1">L</w>’<w n="93.2">enfant</w> <w n="93.3">mit</w> <w n="93.4">la</w> <w n="93.5">main</w> <w n="93.6">sur</w> <w n="93.7">son</w> <w n="93.8">cœur</w>,</l>
							<l n="94" num="7.8"><w n="94.1">En</w> <w n="94.2">détournant</w> <w n="94.3">les</w> <w n="94.4">yeux</w> <w n="94.5">de</w> <w n="94.6">l</w>’<w n="94.7">offrande</w> <w n="94.8">importune</w>.</l>
						</lg>
						<lg n="8">
							<l n="95" num="8.1">« <w n="95.1">André</w> ! <w n="95.2">pardonne</w>-<w n="95.3">moi</w>, » <w n="95.4">lui</w> <w n="95.5">dis</w>-<w n="95.6">je</w> ; <w n="95.7">il</w> <w n="95.8">me</w> <w n="95.9">sourit</w>.</l>
							<l n="96" num="8.2"><w n="96.1">Que</w> <w n="96.2">ce</w> <w n="96.3">touchant</w> <w n="96.4">effort</w> <w n="96.5">renfermait</w> <w n="96.6">d</w>’<w n="96.7">amertume</w> !</l>
							<l n="97" num="8.3"><w n="97.1">Quand</w> <w n="97.2">de</w> <w n="97.3">pleurer</w> <w n="97.4">toujours</w> <w n="97.5">nos</w> <w n="97.6">yeux</w> <w n="97.7">ont</w> <w n="97.8">la</w> <w n="97.9">coutume</w>,</l>
							<l n="98" num="8.4"><w n="98.1">Dans</w> <w n="98.2">leur</w> <w n="98.3">sourire</w> <w n="98.4">encor</w> <w n="98.5">le</w> <w n="98.6">malheur</w> <w n="98.7">est</w> <w n="98.8">écrit</w>.</l>
							<l n="99" num="8.5"><w n="99.1">Et</w> <w n="99.2">moi</w> : « <w n="99.3">Veux</w>-<w n="99.4">tu</w> <w n="99.5">venir</w> ? <w n="99.6">veux</w>-<w n="99.7">tu</w> <w n="99.8">changer</w> <w n="99.9">ta</w> <w n="99.10">vie</w>,</l>
							<l n="100" num="8.6"><space quantity="2" unit="char"></space><w n="100.1">Enfant</w> ? <w n="100.2">veux</w>-<w n="100.3">tu</w> <w n="100.4">voyager</w> <w n="100.5">avec</w> <w n="100.6">nous</w> ?</l>
							<l n="101" num="8.7"><w n="101.1">Tu</w> <w n="101.2">verras</w> <w n="101.3">d</w>’<w n="101.4">autres</w> <w n="101.5">cieux</w>. <w n="101.6">Va</w> ! <w n="101.7">tous</w> <w n="101.8">les</w> <w n="101.9">cieux</w> <w n="101.10">sont</w> <w n="101.11">doux</w> ;</l>
							<l n="102" num="8.8"><w n="102.1">Ils</w> <w n="102.2">cachent</w> <w n="102.3">tant</w> <w n="102.4">d</w>’<w n="102.5">espoir</w> ! <w n="102.6">Les</w> <w n="102.7">fleurs</w> <w n="102.8">te</w> <w n="102.9">font</w> <w n="102.10">envie</w> ?</l>
							<l n="103" num="8.9"><w n="103.1">Viens</w> ; <w n="103.2">partout</w> <w n="103.3">la</w> <w n="103.4">rosée</w> <w n="103.5">y</w> <w n="103.6">répand</w> <w n="103.7">sa</w> <w n="103.8">fraîcheur</w>.</l>
							<l n="104" num="8.10"><w n="104.1">Tu</w> <w n="104.2">ne</w> <w n="104.3">dormiras</w> <w n="104.4">plus</w> <w n="104.5">sur</w> <w n="104.6">une</w> <w n="104.7">pierre</w> <w n="104.8">humide</w> ;</l>
							<l n="105" num="8.11"><w n="105.1">Et</w> <w n="105.2">comme</w> <w n="105.3">à</w> <w n="105.4">des</w> <w n="105.5">ramiers</w> <w n="105.6">le</w> <w n="105.7">passereau</w> <w n="105.8">timide</w></l>
							<l n="106" num="8.12"><w n="106.1">Se</w> <w n="106.2">donne</w>, <w n="106.3">tu</w> <w n="106.4">suivras</w> <w n="106.5">notre</w> <w n="106.6">essaim</w> <w n="106.7">voyageur</w> ;</l>
							<l n="107" num="8.13"><w n="107.1">Veux</w>-<w n="107.2">tu</w> ?… » <w n="107.3">Ses</w> <w n="107.4">yeux</w> <w n="107.5">erraient</w> ; <w n="107.6">j</w>’<w n="107.7">y</w> <w n="107.8">vis</w> <w n="107.9">paraître</w> <w n="107.10">une</w> <w n="107.11">âme</w> ;</l>
							<l n="108" num="8.14"><w n="108.1">Son</w> <w n="108.2">teint</w> <w n="108.3">morne</w> <w n="108.4">et</w> <w n="108.5">mourant</w> <w n="108.6">soudain</w> <w n="108.7">se</w> <w n="108.8">ranima</w>.</l>
							<l n="109" num="8.15"><space quantity="6" unit="char"></space><w n="109.1">Vous</w> <w n="109.2">allez</w> <w n="109.3">juger</w> <w n="109.4">quelle</w> <w n="109.5">flamme</w></l>
							<l n="110" num="8.16"><space quantity="6" unit="char"></space><w n="110.1">Dans</w> <w n="110.2">ce</w> <w n="110.3">cœur</w> <w n="110.4">éteint</w> <w n="110.5">s</w>’<w n="110.6">alluma</w>.</l>
							<l n="111" num="8.17"><space quantity="2" unit="char"></space><w n="111.1">Un</w> <w n="111.2">signe</w> <w n="111.3">prompt</w> <w n="111.4">m</w>’<w n="111.5">attire</w> <w n="111.6">sur</w> <w n="111.7">sa</w> <w n="111.8">trace</w> ;</l>
							<l n="112" num="8.18"><w n="112.1">Il</w> <w n="112.2">monte</w> <w n="112.3">vers</w> <w n="112.4">l</w>’<w n="112.5">église</w>, <w n="112.6">il</w> <w n="112.7">a</w> <w n="112.8">franchi</w> <w n="112.9">l</w>’<w n="112.10">enclos</w></l>
							<l n="113" num="8.19"><space quantity="2" unit="char"></space><w n="113.1">Où</w> <w n="113.2">d</w>’<w n="113.3">humbles</w> <w n="113.4">croix</w>, <w n="113.5">d</w>’<w n="113.6">humbles</w> <w n="113.7">fleurs</w>, <w n="113.8">tout</w> <w n="113.9">retrace</w></l>
							<l n="114" num="8.20"><space quantity="2" unit="char"></space><w n="114.1">D</w>’<w n="114.2">objets</w> <w n="114.3">aimés</w> <w n="114.4">l</w>’<w n="114.5">invisible</w> <w n="114.6">repos</w>.</l>
							<l n="115" num="8.21"><space quantity="2" unit="char"></space><w n="115.1">Sur</w> <w n="115.2">une</w> <w n="115.3">tombe</w>, <w n="115.4">à</w> <w n="115.5">genoux</w>, <w n="115.6">sans</w> <w n="115.7">haleine</w>,</l>
							<l n="116" num="8.22"><space quantity="2" unit="char"></space><w n="116.1">André</w> <w n="116.2">s</w>’<w n="116.3">étend</w>, <w n="116.4">l</w>’<w n="116.5">enferme</w> <w n="116.6">dans</w> <w n="116.7">ses</w> <w n="116.8">bras</w> ;</l>
							<l n="117" num="8.23"><w n="117.1">Puis</w>, <w n="117.2">avec</w> <w n="117.3">un</w> <w n="117.4">accent</w> <w n="117.5">que</w> <w n="117.6">l</w>’<w n="117.7">on</w> <w n="117.8">devine</w> <w n="117.9">à</w> <w n="117.10">peine</w>,</l>
							<l n="118" num="8.24"><w n="118.1">Il</w> <w n="118.2">se</w> <w n="118.3">lève</w> <w n="118.4">en</w> <w n="118.5">criant</w> : « <w n="118.6">Ma</w> <w n="118.7">mère</w> ! <w n="118.8">tu</w> <w n="118.9">viendras</w> ! »</l>
							<l n="119" num="8.25"><space quantity="2" unit="char"></space><w n="119.1">Mais</w> <w n="119.2">épuisé</w> <w n="119.3">par</w> <w n="119.4">cet</w> <w n="119.5">élan</w> <w n="119.6">pénible</w>,</l>
							<l n="120" num="8.26"><space quantity="2" unit="char"></space><w n="120.1">Cachant</w> <w n="120.2">ses</w> <w n="120.3">yeux</w> <w n="120.4">dans</w> <w n="120.5">l</w>’<w n="120.6">herbe</w> <w n="120.7">du</w> <w n="120.8">tombeau</w>,</l>
							<l n="121" num="8.27"><space quantity="2" unit="char"></space><w n="121.1">André</w> <w n="121.2">s</w>’<w n="121.3">endort</w> <w n="121.4">comme</w> <w n="121.5">un</w> <w n="121.6">enfant</w> <w n="121.7">paisible</w></l>
							<l n="122" num="8.28"><space quantity="2" unit="char"></space><w n="122.1">Qu</w>’<w n="122.2">a</w> <w n="122.3">réveillé</w> <w n="122.4">quelque</w> <w n="122.5">importun</w> <w n="122.6">flambeau</w>.</l>
						</lg>
						<lg n="9">
							<l n="123" num="9.1"><w n="123.1">Vous</w> <w n="123.2">que</w> <w n="123.3">je</w> <w n="123.4">ne</w> <w n="123.5">hais</w> <w n="123.6">plus</w>, <w n="123.7">car</w> <w n="123.8">vos</w> <w n="123.9">yeux</w> <w n="123.10">sont</w> <w n="123.11">humides</w>,</l>
							<l n="124" num="9.2"><w n="124.1">Des</w> <w n="124.2">pleurs</w> <w n="124.3">d</w>’<w n="124.4">un</w> <w n="124.5">insensé</w> <w n="124.6">vous</w> <w n="124.7">voilà</w> <w n="124.8">moins</w> <w n="124.9">avides</w> !</l>
							<l n="125" num="9.3"><w n="125.1">Oui</w>, <w n="125.2">croyez</w>-<w n="125.3">moi</w>, <w n="125.4">le</w> <w n="125.5">cœur</w> <w n="125.6">survit</w> <w n="125.7">à</w> <w n="125.8">la</w> <w n="125.9">raison</w> :</l>
							<l n="126" num="9.4"><w n="126.1">C</w>’<w n="126.2">est</w> <w n="126.3">là</w> <w n="126.4">que</w> <w n="126.5">se</w> <w n="126.6">retire</w> <w n="126.7">un</w> <w n="126.8">reste</w> <w n="126.9">de</w> <w n="126.10">lumière</w></l>
							<l n="127" num="9.5"><space quantity="6" unit="char"></space><w n="127.1">Qui</w> <w n="127.2">doit</w> <w n="127.3">échapper</w> <w n="127.4">à</w> <w n="127.5">la</w> <w n="127.6">terre</w> :</l>
							<l n="128" num="9.6"><w n="128.1">Toujours</w> <w n="128.2">d</w>’<w n="128.3">un</w> <w n="128.4">dard</w> <w n="128.5">moqueur</w> <w n="128.6">on</w> <w n="128.7">y</w> <w n="128.8">sent</w> <w n="128.9">le</w> <w n="128.10">poison</w>.</l>
						</lg>
						<lg n="10">
							<l n="129" num="10.1"><w n="129.1">Ô</w> <w n="129.2">mes</w> <w n="129.3">jeunes</w> <w n="129.4">amis</w>, <w n="129.5">prenez</w> <w n="129.6">bien</w> <w n="129.7">sa</w> <w n="129.8">défense</w> !</l>
							<l n="130" num="10.2"><w n="130.1">Nés</w> <w n="130.2">sur</w> <w n="130.3">le</w> <w n="130.4">même</w> <w n="130.5">sol</w>, <w n="130.6">charmez</w> <w n="130.7">sa</w> <w n="130.8">longue</w> <w n="130.9">enfance</w> ;</l>
							<l n="131" num="10.3"><w n="131.1">Sous</w> <w n="131.2">vos</w> <w n="131.3">toits</w> <w n="131.4">généreux</w> <w n="131.5">qu</w>’<w n="131.6">il</w> <w n="131.7">entre</w> <w n="131.8">quelquefois</w> !</l>
							<l n="132" num="10.4"><w n="132.1">Enfants</w>, <w n="132.2">ne</w> <w n="132.3">raillez</w> <w n="132.4">plus</w> <w n="132.5">ses</w> <w n="132.6">naïves</w> <w n="132.7">chimères</w> ;</l>
							<l n="133" num="10.5"><w n="133.1">Éveillez</w> <w n="133.2">sur</w> <w n="133.3">son</w> <w n="133.4">sort</w> <w n="133.5">la</w> <w n="133.6">pitié</w> <w n="133.7">de</w> <w n="133.8">vos</w> <w n="133.9">mères</w>,</l>
							<l n="134" num="10.6"><w n="134.1">Et</w>, <w n="134.2">quand</w> <w n="134.3">je</w> <w n="134.4">serai</w> <w n="134.5">loin</w>, <w n="134.6">rappelez</w>-<w n="134.7">lui</w> <w n="134.8">ma</w> <w n="134.9">voix</w> :</l>
							<l n="135" num="10.7"><w n="135.1">Cette</w> <w n="135.2">voix</w> <w n="135.3">triste</w> <w n="135.4">est</w> <w n="135.5">douce</w> <w n="135.6">à</w> <w n="135.7">l</w>’<w n="135.8">indigent</w> <w n="135.9">timide</w> ;</l>
							<l n="136" num="10.8"><w n="136.1">Le</w> <w n="136.2">pauvre</w> <w n="136.3">aime</w> <w n="136.4">l</w>’<w n="136.5">accent</w> <w n="136.6">ému</w> <w n="136.7">de</w> <w n="136.8">sa</w> <w n="136.9">douleur</w>.</l>
							<l n="137" num="10.9"><w n="137.1">Vous</w>-<w n="137.2">mêmes</w>, <w n="137.3">croyez</w>-<w n="137.4">moi</w>, <w n="137.5">souvent</w> <w n="137.6">un</w> <w n="137.7">humble</w> <w n="137.8">guide</w></l>
							<l n="138" num="10.10"><w n="138.1">Peut</w> <w n="138.2">en</w> <w n="138.3">vous</w> <w n="138.4">éclairant</w> <w n="138.5">vous</w> <w n="138.6">conduire</w> <w n="138.7">au</w> <w n="138.8">bonheur</w>.</l>
						</lg>
						<lg n="11">
							<l n="139" num="11.1"><w n="139.1">Qui</w> <w n="139.2">ne</w> <w n="139.3">veut</w> <w n="139.4">le</w> <w n="139.5">bonheur</w> ? <w n="139.6">L</w>’<w n="139.7">homme</w>, <w n="139.8">dès</w> <w n="139.9">qu</w>’<w n="139.10">il</w> <w n="139.11">respire</w>,</l>
							<l n="140" num="11.2"><w n="140.1">Le</w> <w n="140.2">demande</w> <w n="140.3">au</w> <w n="140.4">breuvage</w> <w n="140.5">à</w> <w n="140.6">ses</w> <w n="140.7">lèvres</w> <w n="140.8">promis</w> ;</l>
							<l n="141" num="11.3"><w n="141.1">Plus</w> <w n="141.2">tard</w> <w n="141.3">il</w> <w n="141.4">le</w> <w n="141.5">demande</w> <w n="141.6">à</w> <w n="141.7">des</w> <w n="141.8">songes</w> <w n="141.9">amis</w> ;</l>
							<l n="142" num="11.4"><w n="142.1">Hélas</w> ! <w n="142.2">il</w> <w n="142.3">le</w> <w n="142.4">demande</w> <w n="142.5">encor</w> <w n="142.6">quand</w> <w n="142.7">il</w> <w n="142.8">expire</w>.</l>
						</lg>
						<lg n="12">
							<l n="143" num="12.1"><w n="143.1">André</w> <w n="143.2">l</w>’<w n="143.3">attend</w> <w n="143.4">aussi</w> ; <w n="143.5">comme</w> <w n="143.6">un</w> <w n="143.7">frêle</w> <w n="143.8">arbrisseau</w></l>
							<l n="144" num="12.2"><space quantity="6" unit="char"></space><w n="144.1">Jeté</w> <w n="144.2">sur</w> <w n="144.3">un</w> <w n="144.4">terrain</w> <w n="144.5">aride</w>,</l>
							<l n="145" num="12.3"><space quantity="6" unit="char"></space><w n="145.1">Sous</w> <w n="145.2">l</w>’<w n="145.3">ardent</w> <w n="145.4">soleil</w> <w n="145.5">qui</w> <w n="145.6">le</w> <w n="145.7">ride</w>,</l>
							<l n="146" num="12.4"><space quantity="6" unit="char"></space><w n="146.1">Attend</w> <w n="146.2">la</w> <w n="146.3">fraîcheur</w> <w n="146.4">du</w> <w n="146.5">ruisseau</w> ;</l>
							<l n="147" num="12.5"><space quantity="6" unit="char"></space><w n="147.1">Sa</w> <w n="147.2">jeunesse</w> <w n="147.3">se</w> <w n="147.4">fane</w> <w n="147.5">et</w> <w n="147.6">tombe</w></l>
							<l n="148" num="12.6"><space quantity="6" unit="char"></space><w n="148.1">Sans</w> <w n="148.2">éclat</w>, <w n="148.3">sans</w> <w n="148.4">sève</w>, <w n="148.5">sans</w> <w n="148.6">bruit</w> ;</l>
							<l n="149" num="12.7"><space quantity="6" unit="char"></space><w n="149.1">Et</w>, <w n="149.2">loin</w> <w n="149.3">du</w> <w n="149.4">monde</w> <w n="149.5">et</w> <w n="149.6">loin</w> <w n="149.7">du</w> <w n="149.8">bruit</w>,</l>
							<l n="150" num="12.8"><space quantity="6" unit="char"></space><w n="150.1">André</w> <w n="150.2">l</w>’<w n="150.3">attend</w> <w n="150.4">sur</w> <w n="150.5">une</w> <w n="150.6">tombe</w> !</l>
						</lg>
					</div></body></text></TEI>