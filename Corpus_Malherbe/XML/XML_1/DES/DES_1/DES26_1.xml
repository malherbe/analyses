<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>9618 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>9618 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies</title>
					<author>Marceline Desbordes-Valmore</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>A. Boulland, libraire-éditeur</publisher>
						<date when="1830">1830</date>
					</imprint>
					<extent>9618 verstome second</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-12-20" who="RR">Insertion des corrections métriques dans le corps du texte.</change>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
				<change when="2018-11-02" who="RR">Correction dans le poème "Dors, ma Mère".</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ÉLÉGIES</head><div type="poem" key="DES26">
						<head type="main">À L’AMOUR</head>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">Reprends</w> <w n="1.2">de</w> <w n="1.3">ce</w> <w n="1.4">bouquet</w> <w n="1.5">les</w> <w n="1.6">trompeuses</w> <w n="1.7">couleurs</w>,</l>
							<l n="2" num="1.2"><space quantity="6" unit="char"></space><w n="2.1">Ces</w> <w n="2.2">lettres</w> <w n="2.3">qui</w> <w n="2.4">font</w> <w n="2.5">mon</w> <w n="2.6">supplice</w>,</l>
							<l n="3" num="1.3"><space quantity="6" unit="char"></space><w n="3.1">Ce</w> <w n="3.2">portrait</w> <w n="3.3">qui</w> <w n="3.4">fut</w> <w n="3.5">ton</w> <w n="3.6">complice</w> ;</l>
							<l n="4" num="1.4"><w n="4.1">Il</w> <w n="4.2">te</w> <w n="4.3">ressemble</w>, <w n="4.4">il</w> <w n="4.5">rit</w>, <w n="4.6">tout</w> <w n="4.7">baigné</w> <w n="4.8">de</w> <w n="4.9">mes</w> <w n="4.10">pleurs</w>.</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1"><space quantity="6" unit="char"></space><w n="5.1">Je</w> <w n="5.2">te</w> <w n="5.3">rends</w> <w n="5.4">ce</w> <w n="5.5">trésor</w> <w n="5.6">funeste</w>,</l>
							<l n="6" num="2.2"><space quantity="2" unit="char"></space><w n="6.1">Ce</w> <w n="6.2">froid</w> <w n="6.3">témoin</w> <w n="6.4">de</w> <w n="6.5">mon</w> <w n="6.6">affreux</w> <w n="6.7">ennui</w>.</l>
							<l n="7" num="2.3"><space quantity="2" unit="char"></space><w n="7.1">Ton</w> <w n="7.2">souvenir</w> <w n="7.3">brûlant</w>, <w n="7.4">que</w> <w n="7.5">je</w> <w n="7.6">déteste</w>,</l>
							<l n="8" num="2.4"><space quantity="6" unit="char"></space><w n="8.1">Sera</w> <w n="8.2">bientôt</w> <w n="8.3">froid</w> <w n="8.4">comme</w> <w n="8.5">lui</w>.</l>
							<l n="9" num="2.5"><space quantity="2" unit="char"></space><w n="9.1">Oh</w> ! <w n="9.2">reprends</w> <w n="9.3">tout</w>. <w n="9.4">Si</w> <w n="9.5">ma</w> <w n="9.6">main</w> <w n="9.7">tremble</w> <w n="9.8">encore</w>,</l>
							<l n="10" num="2.6"><w n="10.1">C</w>’<w n="10.2">est</w> <w n="10.3">que</w> <w n="10.4">j</w>’<w n="10.5">ai</w> <w n="10.6">cru</w> <w n="10.7">te</w> <w n="10.8">voir</w> <w n="10.9">sous</w> <w n="10.10">ces</w> <w n="10.11">traits</w> <w n="10.12">que</w> <w n="10.13">j</w>’<w n="10.14">abhorre</w>.</l>
							<l n="11" num="2.7"><w n="11.1">Oui</w>, <w n="11.2">j</w>’<w n="11.3">ai</w> <w n="11.4">cru</w> <w n="11.5">rencontrer</w> <w n="11.6">le</w> <w n="11.7">regard</w> <w n="11.8">d</w>’<w n="11.9">un</w> <w n="11.10">trompeur</w> ;</l>
							<l n="12" num="2.8"><w n="12.1">Ce</w> <w n="12.2">fantôme</w> <w n="12.3">a</w> <w n="12.4">troublé</w> <w n="12.5">mon</w> <w n="12.6">courage</w> <w n="12.7">timide</w>.</l>
							<l n="13" num="2.9"><w n="13.1">Ciel</w> ! <w n="13.2">on</w> <w n="13.3">peut</w> <w n="13.4">donc</w> <w n="13.5">mourir</w> <w n="13.6">à</w> <w n="13.7">l</w>’<w n="13.8">aspect</w> <w n="13.9">d</w>’<w n="13.10">un</w> <w n="13.11">perfide</w>,</l>
							<l n="14" num="2.10"><space quantity="6" unit="char"></space><w n="14.1">Si</w> <w n="14.2">son</w> <w n="14.3">ombre</w> <w n="14.4">fait</w> <w n="14.5">tant</w> <w n="14.6">de</w> <w n="14.7">peur</w> !</l>
						</lg>
						<lg n="3">
							<l n="15" num="3.1"><w n="15.1">Comme</w> <w n="15.2">ces</w> <w n="15.3">feux</w> <w n="15.4">errants</w> <w n="15.5">dont</w> <w n="15.6">le</w> <w n="15.7">reflet</w> <w n="15.8">égare</w>,</l>
							<l n="16" num="3.2"><w n="16.1">La</w> <w n="16.2">flamme</w> <w n="16.3">de</w> <w n="16.4">ses</w> <w n="16.5">yeux</w> <w n="16.6">a</w> <w n="16.7">passé</w> <w n="16.8">devant</w> <w n="16.9">moi</w> ;</l>
							<l n="17" num="3.3"><w n="17.1">Je</w> <w n="17.2">rougis</w> <w n="17.3">d</w>’<w n="17.4">oublier</w> <w n="17.5">qu</w>’<w n="17.6">enfin</w> <w n="17.7">tout</w> <w n="17.8">nous</w> <w n="17.9">sépare</w> ;</l>
							<l n="18" num="3.4"><space quantity="6" unit="char"></space><w n="18.1">Mais</w> <w n="18.2">je</w> <w n="18.3">n</w>’<w n="18.4">en</w> <w n="18.5">rougis</w> <w n="18.6">que</w> <w n="18.7">pour</w> <w n="18.8">toi</w>.</l>
							<l n="19" num="3.5"><w n="19.1">Que</w> <w n="19.2">mes</w> <w n="19.3">froids</w> <w n="19.4">sentiments</w> <w n="19.5">s</w>’<w n="19.6">expriment</w> <w n="19.7">avec</w> <w n="19.8">peine</w> !</l>
							<l n="20" num="3.6"><w n="20.1">Amour</w>… <w n="20.2">que</w> <w n="20.3">je</w> <w n="20.4">te</w> <w n="20.5">hais</w> <w n="20.6">de</w> <w n="20.7">m</w>’<w n="20.8">apprendre</w> <w n="20.9">la</w> <w n="20.10">haine</w> !</l>
							<l n="21" num="3.7"><w n="21.1">Éloigne</w>-<w n="21.2">toi</w>, <w n="21.3">reprends</w> <w n="21.4">ces</w> <w n="21.5">trompeuses</w> <w n="21.6">couleurs</w>,</l>
							<l n="22" num="3.8"><space quantity="6" unit="char"></space><w n="22.1">Ces</w> <w n="22.2">lettres</w>, <w n="22.3">qui</w> <w n="22.4">font</w> <w n="22.5">mon</w> <w n="22.6">supplice</w>,</l>
							<l n="23" num="3.9"><space quantity="6" unit="char"></space><w n="23.1">Ce</w> <w n="23.2">portrait</w>, <w n="23.3">qui</w> <w n="23.4">fut</w> <w n="23.5">ton</w> <w n="23.6">complice</w> ;</l>
							<l n="24" num="3.10"><w n="24.1">Il</w> <w n="24.2">te</w> <w n="24.3">ressemble</w>, <w n="24.4">il</w> <w n="24.5">rit</w>, <w n="24.6">tout</w> <w n="24.7">baigné</w> <w n="24.8">de</w> <w n="24.9">mes</w> <w n="24.10">pleurs</w> !</l>
						</lg>
						<lg n="4">
							<l n="25" num="4.1"><w n="25.1">Cache</w> <w n="25.2">au</w> <w n="25.3">moins</w> <w n="25.4">ma</w> <w n="25.5">colère</w> <w n="25.6">au</w> <w n="25.7">cruel</w> <w n="25.8">qui</w> <w n="25.9">t</w>’<w n="25.10">envoie</w> ;</l>
							<l n="26" num="4.2"><w n="26.1">Dis</w> <w n="26.2">que</w> <w n="26.3">j</w>’<w n="26.4">ai</w> <w n="26.5">tout</w> <w n="26.6">brisé</w>, <w n="26.7">sans</w> <w n="26.8">larmes</w>, <w n="26.9">sans</w> <w n="26.10">efforts</w> ;</l>
							<l n="27" num="4.3"><space quantity="2" unit="char"></space><w n="27.1">En</w> <w n="27.2">lui</w> <w n="27.3">peignant</w> <w n="27.4">mes</w> <w n="27.5">douloureux</w> <w n="27.6">transports</w>,</l>
							<l n="28" num="4.4"><space quantity="6" unit="char"></space><w n="28.1">Tu</w> <w n="28.2">lui</w> <w n="28.3">donnerais</w> <w n="28.4">trop</w> <w n="28.5">de</w> <w n="28.6">joie</w>.</l>
							<l n="29" num="4.5"><w n="29.1">Reprends</w> <w n="29.2">aussi</w>, <w n="29.3">reprends</w> <w n="29.4">les</w> <w n="29.5">écrits</w> <w n="29.6">dangereux</w>,</l>
							<l n="30" num="4.6"><w n="30.1">Où</w>, <w n="30.2">cachant</w> <w n="30.3">sous</w> <w n="30.4">des</w> <w n="30.5">fleurs</w> <w n="30.6">son</w> <w n="30.7">premier</w> <w n="30.8">artifice</w>,</l>
							<l n="31" num="4.7"><w n="31.1">Il</w> <w n="31.2">voulut</w> <w n="31.3">essayer</w> <w n="31.4">sa</w> <w n="31.5">cruauté</w> <w n="31.6">novice</w></l>
							<l n="32" num="4.8"><space quantity="6" unit="char"></space><w n="32.1">Sur</w> <w n="32.2">un</w> <w n="32.3">cœur</w> <w n="32.4">simple</w> <w n="32.5">et</w> <w n="32.6">malheureux</w>.</l>
							<l n="33" num="4.9"><w n="33.1">Quand</w> <w n="33.2">tu</w> <w n="33.3">voudras</w> <w n="33.4">encore</w> <w n="33.5">égarer</w> <w n="33.6">l</w>’<w n="33.7">innocence</w>,</l>
							<l n="34" num="4.10"><space quantity="2" unit="char"></space><w n="34.1">Quand</w> <w n="34.2">tu</w> <w n="34.3">voudras</w> <w n="34.4">voir</w> <w n="34.5">brûler</w> <w n="34.6">et</w> <w n="34.7">languir</w>,</l>
							<l n="35" num="4.11"><space quantity="2" unit="char"></space><w n="35.1">Quand</w> <w n="35.2">tu</w> <w n="35.3">voudras</w> <w n="35.4">faire</w> <w n="35.5">aimer</w> <w n="35.6">et</w> <w n="35.7">mourir</w>,</l>
							<l n="36" num="4.12"><space quantity="6" unit="char"></space><w n="36.1">N</w>’<w n="36.2">emprunte</w> <w n="36.3">pas</w> <w n="36.4">d</w>’<w n="36.5">autre</w> <w n="36.6">éloquence</w>.</l>
							<l n="37" num="4.13"><w n="37.1">L</w>’<w n="37.2">art</w> <w n="37.3">de</w> <w n="37.4">séduire</w> <w n="37.5">est</w> <w n="37.6">là</w>, <w n="37.7">comme</w> <w n="37.8">il</w> <w n="37.9">est</w> <w n="37.10">dans</w> <w n="37.11">son</w> <w n="37.12">cœur</w> !</l>
							<l n="38" num="4.14"><space quantity="6" unit="char"></space><w n="38.1">Va</w>, <w n="38.2">tu</w> <w n="38.3">n</w>’<w n="38.4">as</w> <w n="38.5">plus</w> <w n="38.6">besoin</w> <w n="38.7">d</w>’<w n="38.8">étude</w>.</l>
							<l n="39" num="4.15"><w n="39.1">Sois</w> <w n="39.2">léger</w> <w n="39.3">par</w> <w n="39.4">penchant</w>, <w n="39.5">ingrat</w> <w n="39.6">par</w> <w n="39.7">habitude</w> ;</l>
							<l n="40" num="4.16"><w n="40.1">Donne</w> <w n="40.2">la</w> <w n="40.3">fièvre</w>, <w n="40.4">Amour</w>, <w n="40.5">et</w> <w n="40.6">garde</w> <w n="40.7">ta</w> <w n="40.8">froideur</w>.</l>
							<l n="41" num="4.17"><space quantity="2" unit="char"></space><w n="41.1">Ne</w> <w n="41.2">change</w> <w n="41.3">rien</w> <w n="41.4">aux</w> <w n="41.5">aveux</w> <w n="41.6">pleins</w> <w n="41.7">de</w> <w n="41.8">charmes</w></l>
							<l n="42" num="4.18"><space quantity="2" unit="char"></space><w n="42.1">Dont</w> <w n="42.2">la</w> <w n="42.3">magie</w> <w n="42.4">entraîne</w> <w n="42.5">au</w> <w n="42.6">désespoir</w> :</l>
							<l n="43" num="4.19"><w n="43.1">Tu</w> <w n="43.2">peux</w> <w n="43.3">de</w> <w n="43.4">chaque</w> <w n="43.5">mot</w> <w n="43.6">calculer</w> <w n="43.7">le</w> <w n="43.8">pouvoir</w>,</l>
							<l n="44" num="4.20"><w n="44.1">Et</w> <w n="44.2">choisir</w> <w n="44.3">ceux</w> <w n="44.4">encore</w> <w n="44.5">imprégnés</w> <w n="44.6">de</w> <w n="44.7">mes</w> <w n="44.8">larmes</w>.</l>
							<l n="45" num="4.21"><w n="45.1">Il</w> <w n="45.2">n</w>’<w n="45.3">ose</w> <w n="45.4">me</w> <w n="45.5">répondre</w>, <w n="45.6">il</w> <w n="45.7">s</w>’<w n="45.8">envole</w>… <w n="45.9">il</w> <w n="45.10">est</w> <w n="45.11">loin</w>.</l>
							<l n="46" num="4.22"><w n="46.1">Puisse</w>-<w n="46.2">t</w>-<w n="46.3">il</w> <w n="46.4">d</w>’<w n="46.5">un</w> <w n="46.6">ingrat</w> <w n="46.7">éterniser</w> <w n="46.8">l</w>’<w n="46.9">absence</w> !</l>
							<l n="47" num="4.23"><w n="47.1">Il</w> <w n="47.2">faudrait</w> <w n="47.3">par</w> <w n="47.4">fierté</w> <w n="47.5">sourire</w> <w n="47.6">en</w> <w n="47.7">sa</w> <w n="47.8">présence</w> :</l>
							<l n="48" num="4.24"><space quantity="6" unit="char"></space><w n="48.1">J</w>’<w n="48.2">aime</w> <w n="48.3">mieux</w> <w n="48.4">souffrir</w> <w n="48.5">sans</w> <w n="48.6">témoin</w>.</l>
							<l n="49" num="4.25"><w n="49.1">Il</w> <w n="49.2">ne</w> <w n="49.3">reviendra</w> <w n="49.4">plus</w>, <w n="49.5">il</w> <w n="49.6">sait</w> <w n="49.7">que</w> <w n="49.8">je</w> <w n="49.9">l</w>’<w n="49.10">abhorre</w> ;</l>
							<l n="50" num="4.26"><w n="50.1">Je</w> <w n="50.2">l</w>’<w n="50.3">ai</w> <w n="50.4">dit</w> <w n="50.5">à</w> <w n="50.6">l</w>’<w n="50.7">Amour</w>, <w n="50.8">qui</w> <w n="50.9">déjà</w> <w n="50.10">s</w>’<w n="50.11">est</w> <w n="50.12">enfui</w>.</l>
							<l n="51" num="4.27"><w n="51.1">S</w>’<w n="51.2">il</w> <w n="51.3">osait</w> <w n="51.4">revenir</w>, <w n="51.5">je</w> <w n="51.6">le</w> <w n="51.7">dirais</w> <w n="51.8">encore</w> :</l>
							<l n="52" num="4.28"><w n="52.1">Mais</w> <w n="52.2">on</w> <w n="52.3">approche</w>, <w n="52.4">on</w> <w n="52.5">parle</w>… <w n="52.6">Hélas</w> ! <w n="52.7">ce</w> <w n="52.8">n</w>’<w n="52.9">est</w> <w n="52.10">pas</w> <w n="52.11">lui</w> !</l>
						</lg>
					</div></body></text></TEI>