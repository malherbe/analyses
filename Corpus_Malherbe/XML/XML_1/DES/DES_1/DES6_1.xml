<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>9618 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>9618 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies</title>
					<author>Marceline Desbordes-Valmore</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>A. Boulland, libraire-éditeur</publisher>
						<date when="1830">1830</date>
					</imprint>
					<extent>9618 verstome second</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-12-20" who="RR">Insertion des corrections métriques dans le corps du texte.</change>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
				<change when="2018-11-02" who="RR">Correction dans le poème "Dors, ma Mère".</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">IDYLLES</head><div type="poem" key="DES6">
						<head type="main">LA NUIT</head>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">Viens</w> ! <w n="1.2">le</w> <w n="1.3">jour</w> <w n="1.4">va</w> <w n="1.5">s</w>’<w n="1.6">éteindre</w>… <w n="1.7">il</w> <w n="1.8">s</w>’<w n="1.9">efface</w>, <w n="1.10">et</w> <w n="1.11">je</w> <w n="1.12">pleure</w>.</l>
							<l n="2" num="1.2"><w n="2.1">N</w>’<w n="2.2">as</w>-<w n="2.3">tu</w> <w n="2.4">pas</w> <w n="2.5">entendu</w> <w n="2.6">ma</w> <w n="2.7">voix</w> ? <w n="2.8">Écoute</w> <w n="2.9">l</w>’<w n="2.10">heure</w> ;</l>
							<l n="3" num="1.3"><w n="3.1">C</w>’<w n="3.2">est</w> <w n="3.3">ma</w> <w n="3.4">voix</w> <w n="3.5">qui</w> <w n="3.6">te</w> <w n="3.7">nomme</w> <w n="3.8">et</w> <w n="3.9">t</w>’<w n="3.10">accuse</w> <w n="3.11">tout</w> <w n="3.12">bas</w> ;</l>
							<l n="4" num="1.4"><w n="4.1">C</w>’<w n="4.2">est</w> <w n="4.3">l</w>’<w n="4.4">Amour</w> <w n="4.5">qui</w> <w n="4.6">t</w>’<w n="4.7">appelle</w>, <w n="4.8">et</w> <w n="4.9">tu</w> <w n="4.10">ne</w> <w n="4.11">l</w>’<w n="4.12">entends</w> <w n="4.13">pas</w> !</l>
							<l n="5" num="1.5"><w n="5.1">Mon</w> <w n="5.2">courage</w> <w n="5.3">se</w> <w n="5.4">meurt</w>. <w n="5.5">Tout</w> <w n="5.6">à</w> <w n="5.7">ta</w> <w n="5.8">chère</w> <w n="5.9">idée</w>,</l>
							<l n="6" num="1.6"><w n="6.1">D</w>’<w n="6.2">elle</w>, <w n="6.3">de</w> <w n="6.4">toi</w> <w n="6.5">toujours</w> <w n="6.6">tendrement</w> <w n="6.7">obsédée</w>,</l>
							<l n="7" num="1.7"><w n="7.1">Pour</w> <w n="7.2">ton</w> <w n="7.3">ombre</w> <w n="7.4">j</w>’<w n="7.5">ai</w> <w n="7.6">pris</w> <w n="7.7">l</w>’<w n="7.8">ombre</w> <w n="7.9">d</w>’<w n="7.10">un</w> <w n="7.11">voyageur</w>,</l>
							<l n="8" num="1.8"><w n="8.1">Et</w> <w n="8.2">c</w>’<w n="8.3">était</w> <w n="8.4">un</w> <w n="8.5">vieillard</w> <w n="8.6">riant</w> <w n="8.7">de</w> <w n="8.8">ma</w> <w n="8.9">rougeur</w>.</l>
						</lg>
						<lg n="2">
							<l n="9" num="2.1"><w n="9.1">Eh</w> <w n="9.2">quoi</w> ! <w n="9.3">le</w> <w n="9.4">jour</w> <w n="9.5">s</w>’<w n="9.6">éteint</w> ? <w n="9.7">n</w>’<w n="9.8">est</w>-<w n="9.9">ce</w> <w n="9.10">pas</w> <w n="9.11">un</w> <w n="9.12">nuage</w>,</l>
							<l n="10" num="2.2"><w n="10.1">Un</w> <w n="10.2">vain</w> <w n="10.3">semblant</w> <w n="10.4">du</w> <w n="10.5">soir</w>, <w n="10.6">un</w> <w n="10.7">fugitif</w> <w n="10.8">orage</w> ?</l>
							<l n="11" num="2.3"><w n="11.1">Que</w> <w n="11.2">je</w> <w n="11.3">voudrais</w> <w n="11.4">le</w> <w n="11.5">croire</w> ! <w n="11.6">Hélas</w> ! <w n="11.7">un</w> <w n="11.8">si</w> <w n="11.9">beau</w> <w n="11.10">jour</w></l>
							<l n="12" num="2.4"><w n="12.1">Ne</w> <w n="12.2">devrait</w> <w n="12.3">pas</w> <w n="12.4">mourir</w> <w n="12.5">sans</w> <w n="12.6">consoler</w> <w n="12.7">l</w>’<w n="12.8">Amour</w>.</l>
							<l n="13" num="2.5"><w n="13.1">Viens</w> ! <w n="13.2">ce</w> <w n="13.3">voile</w> <w n="13.4">jaloux</w> <w n="13.5">ne</w> <w n="13.6">doit</w> <w n="13.7">pas</w> <w n="13.8">te</w> <w n="13.9">surprendre</w>.</l>
							<l n="14" num="2.6"><w n="14.1">Dans</w> <w n="14.2">les</w> <w n="14.3">cieux</w> <w n="14.4">à</w> <w n="14.5">son</w> <w n="14.6">gré</w> <w n="14.7">laisse</w>-<w n="14.8">le</w> <w n="14.9">se</w> <w n="14.10">répandre</w> ;</l>
							<l n="15" num="2.7"><w n="15.1">Ne</w> <w n="15.2">va</w> <w n="15.3">pas</w> <w n="15.4">comme</w> <w n="15.5">moi</w> <w n="15.6">le</w> <w n="15.7">prendre</w> <w n="15.8">pour</w> <w n="15.9">la</w> <w n="15.10">nuit</w></l>
							<l n="16" num="2.8"><w n="16.1">Quand</w> <w n="16.2">son</w> <w n="16.3">obscurité</w> <w n="16.4">m</w>’<w n="16.5">importune</w> <w n="16.6">et</w> <w n="16.7">me</w> <w n="16.8">nuit</w> !</l>
							<l n="17" num="2.9"><w n="17.1">Si</w> <w n="17.2">le</w> <w n="17.3">soleil</w> <w n="17.4">plus</w> <w n="17.5">pur</w> <w n="17.6">allait</w> <w n="17.7">paraître</w> <w n="17.8">encore</w> !</l>
							<l n="18" num="2.10"><w n="18.1">Si</w> <w n="18.2">j</w>’<w n="18.3">allais</w> <w n="18.4">avec</w> <w n="18.5">lui</w> <w n="18.6">revoir</w> <w n="18.7">ce</w> <w n="18.8">que</w> <w n="18.9">j</w>’<w n="18.10">adore</w> !</l>
							<l n="19" num="2.11"><w n="19.1">Si</w> <w n="19.2">je</w> <w n="19.3">pouvais</w> <w n="19.4">du</w> <w n="19.5">moins</w>, <w n="19.6">en</w> <w n="19.7">lui</w> <w n="19.8">livrant</w> <w n="19.9">ces</w> <w n="19.10">fleurs</w>,</l>
							<l n="20" num="2.12"><w n="20.1">Me</w> <w n="20.2">cacher</w> <w n="20.3">dans</w> <w n="20.4">son</w> <w n="20.5">sein</w>, <w n="20.6">et</w> <w n="20.7">rougir</w> <w n="20.8">de</w> <w n="20.9">mes</w> <w n="20.10">pleurs</w> !</l>
							<l n="21" num="2.13"><w n="21.1">Il</w> <w n="21.2">me</w> <w n="21.3">dirait</w> : « <w n="21.4">Je</w> <w n="21.5">viens</w>, <w n="21.6">j</w>’<w n="21.7">accours</w>, <w n="21.8">ma</w> <w n="21.9">bien</w>-<w n="21.10">aimée</w> !</l>
							<l n="22" num="2.14">« <w n="22.1">Ce</w> <w n="22.2">nuage</w> <w n="22.3">qui</w> <w n="22.4">fuit</w> <w n="22.5">t</w>’<w n="22.6">aurait</w>-<w n="22.7">il</w> <w n="22.8">alarmée</w> ?</l>
							<l n="23" num="2.15">« <w n="23.1">La</w> <w n="23.2">nuit</w> <w n="23.3">est</w> <w n="23.4">loin</w>, <w n="23.5">regarde</w> ! » <w n="23.6">Et</w> <w n="23.7">je</w> <w n="23.8">verrais</w> <w n="23.9">ses</w> <w n="23.10">yeux</w></l>
							<l n="24" num="2.16"><w n="24.1">Rendre</w> <w n="24.2">la</w> <w n="24.3">vie</w> <w n="24.4">aux</w> <w n="24.5">miens</w>, <w n="24.6">et</w> <w n="24.7">la</w> <w n="24.8">lumière</w> <w n="24.9">aux</w> <w n="24.10">cieux</w>.</l>
						</lg>
						<lg n="3">
							<l n="25" num="3.1"><w n="25.1">Non</w> ! <w n="25.2">le</w> <w n="25.3">jour</w> <w n="25.4">est</w> <w n="25.5">fini</w>. <w n="25.6">Ce</w> <w n="25.7">calme</w> <w n="25.8">inaltérable</w>,</l>
							<l n="26" num="3.2"><w n="26.1">L</w>’<w n="26.2">oiseau</w> <w n="26.3">silencieux</w> <w n="26.4">fatigué</w> <w n="26.5">de</w> <w n="26.6">bonheur</w>,</l>
							<l n="27" num="3.3"><w n="27.1">Le</w> <w n="27.2">chant</w> <w n="27.3">vague</w> <w n="27.4">et</w> <w n="27.5">lointain</w> <w n="27.6">du</w> <w n="27.7">jeune</w> <w n="27.8">moissonneur</w>,</l>
							<l n="28" num="3.4"><w n="28.1">Tout</w> <w n="28.2">m</w>’<w n="28.3">invite</w> <w n="28.4">au</w> <w n="28.5">repos</w>… <w n="28.6">tout</w> <w n="28.7">m</w>’<w n="28.8">insulte</w> <w n="28.9">et</w> <w n="28.10">m</w>’<w n="28.11">accable</w>.</l>
							<l n="29" num="3.5"><w n="29.1">Un</w> <w n="29.2">seul</w> <w n="29.3">et</w> <w n="29.4">doux</w> <w n="29.5">objet</w> <w n="29.6">me</w> <w n="29.7">plaint</w> <w n="29.8">dans</w> <w n="29.9">ce</w> <w n="29.10">séjour</w>,</l>
							<l n="30" num="3.6"><w n="30.1">Il</w> <w n="30.2">a</w> <w n="30.3">subi</w> <w n="30.4">mon</w> <w n="30.5">sort</w> : <w n="30.6">c</w>’<w n="30.7">est</w> <w n="30.8">la</w> <w n="30.9">pâle</w> <w n="30.10">anémone</w>,</l>
							<l n="31" num="3.7"><w n="31.1">Sous</w> <w n="31.2">le</w> <w n="31.3">vent</w> <w n="31.4">qui</w> <w n="31.5">l</w>’<w n="31.6">effeuille</w>, <w n="31.7">elle</w> <w n="31.8">tombe</w> ; <w n="31.9">et</w> <w n="31.10">ce</w> <w n="31.11">jour</w>,</l>
							<l n="32" num="3.8"><w n="32.1">Pour</w> <w n="32.2">nous</w> <w n="32.3">brûler</w> <w n="32.4">ensemble</w>, <w n="32.5">en</w> <w n="32.6">orna</w> <w n="32.7">ma</w> <w n="32.8">couronne</w>.</l>
						</lg>
						<lg n="4">
							<l n="33" num="4.1"><w n="33.1">Mais</w> <w n="33.2">adieu</w> <w n="33.3">tout</w> ; <w n="33.4">adieu</w>, <w n="33.5">toi</w> <w n="33.6">qui</w> <w n="33.7">ne</w> <w n="33.8">m</w>’<w n="33.9">entends</w> <w n="33.10">pas</w> ;</l>
							<l n="34" num="4.2"><w n="34.1">Toi</w> <w n="34.2">qui</w> <w n="34.3">m</w>’<w n="34.4">as</w> <w n="34.5">retenu</w> <w n="34.6">la</w> <w n="34.7">moitié</w> <w n="34.8">de</w> <w n="34.9">mon</w> <w n="34.10">être</w>,</l>
							<l n="35" num="4.3"><w n="35.1">Qui</w> <w n="35.2">n</w>’<w n="35.3">as</w> <w n="35.4">pu</w> <w n="35.5">m</w>’<w n="35.6">oublier</w>, <w n="35.7">qui</w> <w n="35.8">vas</w> <w n="35.9">venir</w>, <w n="35.10">peut</w>-<w n="35.11">être</w> !</l>
							<l n="36" num="4.4"><w n="36.1">Tu</w> <w n="36.2">trouveras</w> <w n="36.3">au</w> <w n="36.4">moins</w> <w n="36.5">la</w> <w n="36.6">trace</w> <w n="36.7">de</w> <w n="36.8">mes</w> <w n="36.9">pas</w>,</l>
							<l n="37" num="4.5"><w n="37.1">Si</w> <w n="37.2">tu</w> <w n="37.3">viens</w> ! <w n="37.4">Adieu</w>, <w n="37.5">bois</w> <w n="37.6">où</w> <w n="37.7">l</w>’<w n="37.8">ombre</w> <w n="37.9">est</w> <w n="37.10">si</w> <w n="37.11">brûlante</w> ;</l>
							<l n="38" num="4.6"><w n="38.1">Nuit</w> <w n="38.2">plus</w> <w n="38.3">brûlante</w> <w n="38.4">encor</w>, <w n="38.5">nuit</w> <w n="38.6">sans</w> <w n="38.7">pavots</w> <w n="38.8">pour</w> <w n="38.9">moi</w>,</l>
							<l n="39" num="4.7"><w n="39.1">Tu</w> <w n="39.2">règnes</w> <w n="39.3">donc</w> <w n="39.4">enfin</w> ! <w n="39.5">Oui</w>, <w n="39.6">c</w>’<w n="39.7">est</w> <w n="39.8">toi</w>, <w n="39.9">c</w>’<w n="39.10">est</w> <w n="39.11">bien</w> <w n="39.12">toi</w> !</l>
							<l n="40" num="4.8"><w n="40.1">Quand</w> <w n="40.2">me</w> <w n="40.3">rendras</w>-<w n="40.4">tu</w> <w n="40.5">l</w>’<w n="40.6">aube</w> ? <w n="40.7">Oh</w> ! <w n="40.8">que</w> <w n="40.9">la</w> <w n="40.10">nuit</w> <w n="40.11">est</w> <w n="40.12">lente</w> !</l>
							<l n="41" num="4.9"><w n="41.1">Hélas</w> ! <w n="41.2">si</w> <w n="41.3">du</w> <w n="41.4">soleil</w> <w n="41.5">tu</w> <w n="41.6">balances</w> <w n="41.7">le</w> <w n="41.8">cours</w>,</l>
							<l n="42" num="4.10"><w n="42.1">Tu</w> <w n="42.2">vas</w> <w n="42.3">donc</w> <w n="42.4">ressembler</w> <w n="42.5">au</w> <w n="42.6">plus</w> <w n="42.7">long</w> <w n="42.8">de</w> <w n="42.9">mes</w> <w n="42.10">jours</w> !</l>
							<l n="43" num="4.11"><w n="43.1">L</w>’<w n="43.2">alouette</w> <w n="43.3">est</w> <w n="43.4">rentrée</w> <w n="43.5">aux</w> <w n="43.6">sillons</w> ; <w n="43.7">la</w> <w n="43.8">cigale</w></l>
							<l n="44" num="4.12"><w n="44.1">À</w> <w n="44.2">peine</w> <w n="44.3">dans</w> <w n="44.4">les</w> <w n="44.5">airs</w> <w n="44.6">jette</w> <w n="44.7">sa</w> <w n="44.8">note</w> <w n="44.9">égale</w> ;</l>
							<l n="45" num="4.13"><w n="45.1">Un</w> <w n="45.2">souffle</w> <w n="45.3">éveillerait</w> <w n="45.4">les</w> <w n="45.5">échos</w> <w n="45.6">du</w> <w n="45.7">vallon</w>,</l>
							<l n="46" num="4.14"><w n="46.1">Et</w> <w n="46.2">les</w> <w n="46.3">échos</w> <w n="46.4">muets</w> <w n="46.5">ne</w> <w n="46.6">diront</w> <w n="46.7">pas</w> <w n="46.8">mon</w> <w n="46.9">nom</w>.</l>
							<l n="47" num="4.15"><w n="47.1">Et</w> <w n="47.2">vous</w>, <w n="47.3">dont</w> <w n="47.4">la</w> <w n="47.5">fatigue</w> <w n="47.6">a</w> <w n="47.7">suspendu</w> <w n="47.8">la</w> <w n="47.9">course</w>,</l>
							<l n="48" num="4.16"><w n="48.1">Vieillard</w> ! <w n="48.2">ne</w> <w n="48.3">riez</w> <w n="48.4">plus</w> ; <w n="48.5">si</w> <w n="48.6">mes</w> <w n="48.7">tristes</w> <w n="48.8">accents</w>…</l>
							<l n="49" num="4.17"><w n="49.1">Non</w> ! <w n="49.2">déjà</w> <w n="49.3">le</w> <w n="49.4">sommeil</w> <w n="49.5">appesantit</w> <w n="49.6">ses</w> <w n="49.7">sens</w> ;</l>
							<l n="50" num="4.18"><w n="50.1">Il</w> <w n="50.2">rêve</w> <w n="50.3">sa</w> <w n="50.4">jeunesse</w> <w n="50.5">au</w> <w n="50.6">doux</w> <w n="50.7">bruit</w> <w n="50.8">de</w> <w n="50.9">la</w> <w n="50.10">source</w>.</l>
							<l n="51" num="4.19"><w n="51.1">Oh</w> ! <w n="51.2">que</w> <w n="51.3">je</w> <w n="51.4">porte</w> <w n="51.5">envie</w> <w n="51.6">à</w> <w n="51.7">ses</w> <w n="51.8">songes</w> <w n="51.9">confus</w> !</l>
							<l n="52" num="4.20"><w n="52.1">Que</w> <w n="52.2">je</w> <w n="52.3">le</w> <w n="52.4">trouve</w> <w n="52.5">heureux</w> ! <w n="52.6">Il</w> <w n="52.7">dort</w>, <w n="52.8">il</w> <w n="52.9">n</w>’<w n="52.10">attend</w> <w n="52.11">plus</w>.</l>
						</lg>
					</div></body></text></TEI>