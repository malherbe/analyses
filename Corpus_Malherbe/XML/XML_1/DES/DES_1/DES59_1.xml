<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>9618 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>9618 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies</title>
					<author>Marceline Desbordes-Valmore</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>A. Boulland, libraire-éditeur</publisher>
						<date when="1830">1830</date>
					</imprint>
					<extent>9618 verstome second</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-12-20" who="RR">Insertion des corrections métriques dans le corps du texte.</change>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
				<change when="2018-11-02" who="RR">Correction dans le poème "Dors, ma Mère".</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ÉLÉGIES</head><div type="poem" key="DES59">
						<head type="main">L’ISOLEMENT</head>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">Quoi</w> ! <w n="1.2">ce</w> <w n="1.3">n</w>’<w n="1.4">est</w> <w n="1.5">plus</w> <w n="1.6">pour</w> <w n="1.7">lui</w>, <w n="1.8">ce</w> <w n="1.9">n</w>’<w n="1.10">est</w> <w n="1.11">plus</w> <w n="1.12">pour</w> <w n="1.13">l</w>’<w n="1.14">attendre</w>,</l>
							<l n="2" num="1.2"><w n="2.1">Que</w> <w n="2.2">je</w> <w n="2.3">vois</w> <w n="2.4">arriver</w> <w n="2.5">ces</w> <w n="2.6">jours</w> <w n="2.7">longs</w> <w n="2.8">et</w> <w n="2.9">brûlants</w> ?</l>
							<l n="3" num="1.3"><w n="3.1">Ce</w> <w n="3.2">n</w>’<w n="3.3">est</w> <w n="3.4">plus</w> <w n="3.5">son</w> <w n="3.6">amour</w> <w n="3.7">que</w> <w n="3.8">je</w> <w n="3.9">cherche</w> <w n="3.10">à</w> <w n="3.11">pas</w> <w n="3.12">lents</w> ?</l>
							<l n="4" num="1.4"><w n="4.1">Ce</w> <w n="4.2">n</w>’<w n="4.3">est</w> <w n="4.4">plus</w> <w n="4.5">cette</w> <w n="4.6">voix</w> <w n="4.7">si</w> <w n="4.8">puissante</w>, <w n="4.9">si</w> <w n="4.10">tendre</w>,</l>
							<l n="5" num="1.5"><w n="5.1">Qui</w> <w n="5.2">m</w>’<w n="5.3">implore</w> <w n="5.4">dans</w> <w n="5.5">l</w>’<w n="5.6">ombre</w>, <w n="5.7">ou</w> <w n="5.8">que</w> <w n="5.9">je</w> <w n="5.10">crois</w> <w n="5.11">entendre</w> ?</l>
							<l n="6" num="1.6"><w n="6.1">Ce</w> <w n="6.2">n</w>’<w n="6.3">est</w> <w n="6.4">plus</w> <w n="6.5">rien</w> ? <w n="6.6">Où</w> <w n="6.7">donc</w> <w n="6.8">est</w> <w n="6.9">tout</w> <w n="6.10">ce</w> <w n="6.11">que</w> <w n="6.12">j</w>’<w n="6.13">aimais</w> ?</l>
							<l n="7" num="1.7"><w n="7.1">Que</w> <w n="7.2">le</w> <w n="7.3">monde</w> <w n="7.4">est</w> <w n="7.5">désert</w> ! <w n="7.6">n</w>’<w n="7.7">y</w> <w n="7.8">laissa</w>-<w n="7.9">t</w>-<w n="7.10">il</w> <w n="7.11">personne</w> ?</l>
							<l n="8" num="1.8"><w n="8.1">Le</w> <w n="8.2">temps</w> <w n="8.3">s</w>’<w n="8.4">arrête</w> <w n="8.5">et</w> <w n="8.6">dort</w> : <w n="8.7">jamais</w> <w n="8.8">l</w>’<w n="8.9">heure</w> <w n="8.10">ne</w> <w n="8.11">sonne</w>.</l>
							<l n="9" num="1.9"><w n="9.1">Toujours</w> <w n="9.2">vivre</w>, <w n="9.3">toujours</w> ! <w n="9.4">On</w> <w n="9.5">ne</w> <w n="9.6">meurt</w> <w n="9.7">donc</w> <w n="9.8">jamais</w> ?</l>
							<l n="10" num="1.10"><w n="10.1">Est</w>-<w n="10.2">ce</w> <w n="10.3">l</w>’<w n="10.4">éternité</w> <w n="10.5">qui</w> <w n="10.6">pèse</w> <w n="10.7">sur</w> <w n="10.8">mon</w> <w n="10.9">âme</w> ?</l>
							<l n="11" num="1.11"><w n="11.1">Interminable</w> <w n="11.2">nuit</w>, <w n="11.3">que</w> <w n="11.4">tu</w> <w n="11.5">couvres</w> <w n="11.6">de</w> <w n="11.7">flamme</w> !</l>
							<l n="12" num="1.12"><w n="12.1">Comme</w> <w n="12.2">l</w>’<w n="12.3">oiseau</w> <w n="12.4">du</w> <w n="12.5">soir</w> <w n="12.6">qu</w>’<w n="12.7">on</w> <w n="12.8">n</w>’<w n="12.9">entend</w> <w n="12.10">plus</w> <w n="12.11">gémir</w>,</l>
							<l n="13" num="1.13"><w n="13.1">Auprès</w> <w n="13.2">des</w> <w n="13.3">feux</w> <w n="13.4">éteints</w> <w n="13.5">que</w> <w n="13.6">ne</w> <w n="13.7">puis</w>-<w n="13.8">je</w> <w n="13.9">dormir</w> !</l>
							<l n="14" num="1.14"><w n="14.1">Car</w> <w n="14.2">ce</w> <w n="14.3">n</w>’<w n="14.4">est</w> <w n="14.5">plus</w> <w n="14.6">pour</w> <w n="14.7">lui</w> <w n="14.8">qu</w>’<w n="14.9">en</w> <w n="14.10">silence</w> <w n="14.11">éveillée</w>,</l>
							<l n="15" num="1.15"><w n="15.1">La</w> <w n="15.2">muse</w> <w n="15.3">qui</w> <w n="15.4">me</w> <w n="15.5">plaint</w>, <w n="15.6">assise</w> <w n="15.7">sur</w> <w n="15.8">des</w> <w n="15.9">fleurs</w>,</l>
							<l n="16" num="1.16"><w n="16.1">M</w>’<w n="16.2">attire</w> <w n="16.3">dans</w> <w n="16.4">les</w> <w n="16.5">bois</w>, <w n="16.6">sous</w> <w n="16.7">l</w>’<w n="16.8">humide</w> <w n="16.9">feuillée</w>,</l>
							<l n="17" num="1.17"><w n="17.1">Et</w> <w n="17.2">répand</w> <w n="17.3">sur</w> <w n="17.4">mes</w> <w n="17.5">vers</w> <w n="17.6">des</w> <w n="17.7">parfums</w> <w n="17.8">et</w> <w n="17.9">des</w> <w n="17.10">pleurs</w>.</l>
							<l n="18" num="1.18"><w n="18.1">Il</w> <w n="18.2">ne</w> <w n="18.3">lit</w> <w n="18.4">plus</w> <w n="18.5">mes</w> <w n="18.6">chants</w>, <w n="18.7">il</w> <w n="18.8">croit</w> <w n="18.9">mon</w> <w n="18.10">âme</w> <w n="18.11">éteinte</w>.</l>
							<l n="19" num="1.19"><w n="19.1">Jamais</w> <w n="19.2">son</w> <w n="19.3">cœur</w> <w n="19.4">guéri</w> <w n="19.5">n</w>’<w n="19.6">a</w> <w n="19.7">soupçonné</w> <w n="19.8">ma</w> <w n="19.9">plainte</w> ;</l>
							<l n="20" num="1.20"><w n="20.1">Il</w> <w n="20.2">n</w>’<w n="20.3">a</w> <w n="20.4">pas</w> <w n="20.5">deviné</w> <w n="20.6">ce</w> <w n="20.7">qu</w>’<w n="20.8">il</w> <w n="20.9">m</w>’<w n="20.10">a</w> <w n="20.11">fait</w> <w n="20.12">souffrir</w>.</l>
							<l n="21" num="1.21"><w n="21.1">Qu</w>’<w n="21.2">importe</w> <w n="21.3">qu</w>’<w n="21.4">il</w> <w n="21.5">l</w>’<w n="21.6">apprenne</w> ? <w n="21.7">il</w> <w n="21.8">ne</w> <w n="21.9">peut</w> <w n="21.10">me</w> <w n="21.11">guérir</w>.</l>
							<l n="22" num="1.22"><w n="22.1">J</w>’<w n="22.2">épargne</w> <w n="22.3">à</w> <w n="22.4">son</w> <w n="22.5">orgueil</w> <w n="22.6">la</w> <w n="22.7">volupté</w> <w n="22.8">cruelle</w></l>
							<l n="23" num="1.23"><w n="23.1">De</w> <w n="23.2">juger</w> <w n="23.3">dans</w> <w n="23.4">mes</w> <w n="23.5">pleurs</w> <w n="23.6">l</w>’<w n="23.7">excès</w> <w n="23.8">de</w> <w n="23.9">mon</w> <w n="23.10">amour</w>.</l>
							<l n="24" num="1.24"><w n="24.1">Que</w> <w n="24.2">devrais</w>-<w n="24.3">je</w> <w n="24.4">à</w> <w n="24.5">mes</w> <w n="24.6">cris</w> ? <w n="24.7">Sa</w> <w n="24.8">frayeur</w> ? <w n="24.9">son</w> <w n="24.10">retour</w> ?</l>
							<l n="25" num="1.25"><w n="25.1">Sa</w> <w n="25.2">pitié</w> ?… <w n="25.3">C</w>’<w n="25.4">est</w> <w n="25.5">la</w> <w n="25.6">mort</w> <w n="25.7">que</w> <w n="25.8">je</w> <w n="25.9">veux</w> <w n="25.10">avant</w> <w n="25.11">elle</w>.</l>
							<l n="26" num="1.26"><w n="26.1">Tout</w> <w n="26.2">est</w> <w n="26.3">détruit</w> : <w n="26.4">lui</w>-<w n="26.5">même</w>, <w n="26.6">il</w> <w n="26.7">n</w>’<w n="26.8">est</w> <w n="26.9">plus</w> <w n="26.10">le</w> <w n="26.11">bonheur</w> ;</l>
							<l n="27" num="1.27"><w n="27.1">Il</w> <w n="27.2">brisa</w> <w n="27.3">son</w> <w n="27.4">image</w> <w n="27.5">en</w> <w n="27.6">déchirant</w> <w n="27.7">mon</w> <w n="27.8">cœur</w>.</l>
							<l n="28" num="1.28"><w n="28.1">Me</w> <w n="28.2">rapporterait</w>-<w n="28.3">il</w> <w n="28.4">ma</w> <w n="28.5">douce</w> <w n="28.6">imprévoyance</w>,</l>
							<l n="29" num="1.29"><w n="29.1">Et</w> <w n="29.2">le</w> <w n="29.3">prisme</w> <w n="29.4">charmant</w> <w n="29.5">de</w> <w n="29.6">l</w>’<w n="29.7">inexpérience</w> ?</l>
							<l n="30" num="1.30"><w n="30.1">L</w>’<w n="30.2">amour</w> <w n="30.3">en</w> <w n="30.4">s</w>’<w n="30.5">envolant</w> <w n="30.6">ne</w> <w n="30.7">me</w> <w n="30.8">l</w>’<w n="30.9">a</w> <w n="30.10">pas</w> <w n="30.11">rendu</w> :</l>
							<l n="31" num="1.31"><w n="31.1">Ce</w> <w n="31.2">qu</w>’<w n="31.3">on</w> <w n="31.4">donne</w> <w n="31.5">à</w> <w n="31.6">l</w>’<w n="31.7">amour</w> <w n="31.8">est</w> <w n="31.9">à</w> <w n="31.10">jamais</w> <w n="31.11">perdu</w>.</l>
						</lg>
					</div></body></text></TEI>