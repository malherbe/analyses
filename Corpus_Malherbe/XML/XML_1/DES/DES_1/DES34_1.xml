<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>9618 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>9618 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies</title>
					<author>Marceline Desbordes-Valmore</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>A. Boulland, libraire-éditeur</publisher>
						<date when="1830">1830</date>
					</imprint>
					<extent>9618 verstome second</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-12-20" who="RR">Insertion des corrections métriques dans le corps du texte.</change>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
				<change when="2018-11-02" who="RR">Correction dans le poème "Dors, ma Mère".</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ÉLÉGIES</head><div type="poem" key="DES34">
						<head type="main">À DÉLIE</head>
						<lg n="1">
							<l n="1" num="1.1"><space quantity="2" unit="char"></space><w n="1.1">Oui</w> ! <w n="1.2">cette</w> <w n="1.3">plainte</w> <w n="1.4">échappe</w> <w n="1.5">à</w> <w n="1.6">ma</w> <w n="1.7">douleur</w> :</l>
							<l n="2" num="1.2"><space quantity="6" unit="char"></space><w n="2.1">Je</w> <w n="2.2">le</w> <w n="2.3">sens</w>, <w n="2.4">vous</w> <w n="2.5">m</w>’<w n="2.6">avez</w> <w n="2.7">perdue</w>.</l>
							<l n="3" num="1.3"><w n="3.1">Vous</w> <w n="3.2">avez</w>, <w n="3.3">malgré</w> <w n="3.4">moi</w>, <w n="3.5">disposé</w> <w n="3.6">de</w> <w n="3.7">mon</w> <w n="3.8">cœur</w>,</l>
							<l n="4" num="1.4"><w n="4.1">Et</w> <w n="4.2">du</w> <w n="4.3">vôtre</w> <w n="4.4">jamais</w> <w n="4.5">je</w> <w n="4.6">ne</w> <w n="4.7">fus</w> <w n="4.8">entendue</w>.</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1"><space quantity="6" unit="char"></space><w n="5.1">Ah</w> ! <w n="5.2">que</w> <w n="5.3">vous</w> <w n="5.4">me</w> <w n="5.5">faites</w> <w n="5.6">haïr</w></l>
							<l n="6" num="2.2"><w n="6.1">Cette</w> <w n="6.2">feinte</w> <w n="6.3">amitié</w> <w n="6.4">qui</w> <w n="6.5">coûte</w> <w n="6.6">tant</w> <w n="6.7">de</w> <w n="6.8">larmes</w> !</l>
							<l n="7" num="2.3"><space quantity="2" unit="char"></space><w n="7.1">Je</w> <w n="7.2">n</w>’<w n="7.3">étais</w> <w n="7.4">point</w> <w n="7.5">jalouse</w> <w n="7.6">de</w> <w n="7.7">vos</w> <w n="7.8">charmes</w>,</l>
							<l n="8" num="2.4"><w n="8.1">Cruelle</w> ! <w n="8.2">de</w> <w n="8.3">quoi</w> <w n="8.4">donc</w> <w n="8.5">vouliez</w>-<w n="8.6">vous</w> <w n="8.7">me</w> <w n="8.8">punir</w> ?</l>
							<l n="9" num="2.5"><space quantity="6" unit="char"></space><w n="9.1">Vos</w> <w n="9.2">succès</w> <w n="9.3">me</w> <w n="9.4">rendaient</w> <w n="9.5">heureuse</w> ;</l>
							<l n="10" num="2.6"><space quantity="2" unit="char"></space><w n="10.1">Votre</w> <w n="10.2">bonheur</w> <w n="10.3">me</w> <w n="10.4">tenait</w> <w n="10.5">lieu</w> <w n="10.6">du</w> <w n="10.7">mien</w> :</l>
							<l n="11" num="2.7"><w n="11.1">Et</w> <w n="11.2">quand</w> <w n="11.3">je</w> <w n="11.4">vous</w> <w n="11.5">voyais</w> <w n="11.6">attristée</w> <w n="11.7">ou</w> <w n="11.8">rêveuse</w>,</l>
							<l n="12" num="2.8"><w n="12.1">Pour</w> <w n="12.2">vous</w> <w n="12.3">distraire</w> <w n="12.4">encor</w> <w n="12.5">j</w>’<w n="12.6">oubliais</w> <w n="12.7">mon</w> <w n="12.8">chagrin</w>.</l>
							<l n="13" num="2.9"><w n="13.1">Mais</w> <w n="13.2">ce</w> <w n="13.3">perfide</w> <w n="13.4">amant</w> <w n="13.5">dont</w> <w n="13.6">j</w>’<w n="13.7">évitais</w> <w n="13.8">l</w>’<w n="13.9">empire</w>,</l>
							<l n="14" num="2.10"><w n="14.1">Que</w> <w n="14.2">vous</w> <w n="14.3">avez</w> <w n="14.4">instruit</w> <w n="14.5">dans</w> <w n="14.6">l</w>’<w n="14.7">art</w> <w n="14.8">de</w> <w n="14.9">me</w> <w n="14.10">séduire</w>,</l>
							<l n="15" num="2.11"><w n="15.1">Qui</w> <w n="15.2">trompa</w> <w n="15.3">ma</w> <w n="15.4">raison</w> <w n="15.5">par</w> <w n="15.6">des</w> <w n="15.7">accents</w> <w n="15.8">si</w> <w n="15.9">doux</w>,</l>
							<l n="16" num="2.12"><space quantity="6" unit="char"></space><w n="16.1">Je</w> <w n="16.2">le</w> <w n="16.3">hais</w> <w n="16.4">encor</w> <w n="16.5">plus</w> <w n="16.6">que</w> <w n="16.7">vous</w>.</l>
							<l n="17" num="2.13"><w n="17.1">Par</w> <w n="17.2">quelle</w> <w n="17.3">cruauté</w> <w n="17.4">me</w> <w n="17.5">l</w>’<w n="17.6">avoir</w> <w n="17.7">fait</w> <w n="17.8">connaître</w> ?</l>
							<l n="18" num="2.14"><w n="18.1">Par</w> <w n="18.2">quel</w> <w n="18.3">affreux</w> <w n="18.4">orgueil</w> <w n="18.5">voulut</w>-<w n="18.6">il</w> <w n="18.7">me</w> <w n="18.8">charmer</w> ?</l>
							<l n="19" num="2.15"><space quantity="6" unit="char"></space><w n="19.1">Ah</w> ! <w n="19.2">si</w> <w n="19.3">l</w>’<w n="19.4">ingrat</w> <w n="19.5">ne</w> <w n="19.6">peut</w> <w n="19.7">aimer</w>,</l>
							<l n="20" num="2.16"><space quantity="6" unit="char"></space><w n="20.1">À</w> <w n="20.2">quoi</w> <w n="20.3">sert</w> <w n="20.4">l</w>’<w n="20.5">amour</w> <w n="20.6">qu</w>’<w n="20.7">il</w> <w n="20.8">fait</w> <w n="20.9">naître</w> ?</l>
							<l n="21" num="2.17"><space quantity="6" unit="char"></space><w n="21.1">Je</w> <w n="21.2">l</w>’<w n="21.3">ai</w> <w n="21.4">prévu</w>, <w n="21.5">j</w>’<w n="21.6">ai</w> <w n="21.7">voulu</w> <w n="21.8">fuir</w> :</l>
							<l n="22" num="2.18"><space quantity="2" unit="char"></space><w n="22.1">L</w>’<w n="22.2">Amour</w> <w n="22.3">jamais</w> <w n="22.4">n</w>’<w n="22.5">eut</w> <w n="22.6">de</w> <w n="22.7">moi</w> <w n="22.8">que</w> <w n="22.9">des</w> <w n="22.10">larmes</w>.</l>
							<l n="23" num="2.19"><space quantity="6" unit="char"></space><w n="23.1">Vous</w> <w n="23.2">avez</w> <w n="23.3">ri</w> <w n="23.4">de</w> <w n="23.5">mes</w> <w n="23.6">alarmes</w>,</l>
							<l n="24" num="2.20"><w n="24.1">Et</w> <w n="24.2">vous</w> <w n="24.3">riez</w> <w n="24.4">encor</w> <w n="24.5">quand</w> <w n="24.6">je</w> <w n="24.7">me</w> <w n="24.8">sens</w> <w n="24.9">mourir</w> !</l>
						</lg>
						<lg n="3">
							<l n="25" num="3.1"><w n="25.1">Grâce</w> <w n="25.2">à</w> <w n="25.3">vous</w>, <w n="25.4">j</w>’<w n="25.5">ai</w> <w n="25.6">perdu</w> <w n="25.7">le</w> <w n="25.8">repos</w> <w n="25.9">de</w> <w n="25.10">ma</w> <w n="25.11">vie</w> :</l>
							<l n="26" num="3.2"><space quantity="2" unit="char"></space><w n="26.1">Votre</w> <w n="26.2">imprudence</w> <w n="26.3">a</w> <w n="26.4">causé</w> <w n="26.5">mon</w> <w n="26.6">malheur</w>,</l>
							<l n="27" num="3.3"><w n="27.1">Et</w> <w n="27.2">vous</w> <w n="27.3">m</w>’<w n="27.4">avez</w> <w n="27.5">ravi</w> <w n="27.6">jusques</w> <w n="27.7">à</w> <w n="27.8">la</w> <w n="27.9">douceur</w></l>
							<l n="28" num="3.4"><space quantity="6" unit="char"></space><w n="28.1">De</w> <w n="28.2">pleurer</w> <w n="28.3">avec</w> <w n="28.4">mon</w> <w n="28.5">amie</w> !</l>
							<l n="29" num="3.5"><space quantity="2" unit="char"></space><w n="29.1">Laissez</w>-<w n="29.2">moi</w> <w n="29.3">seule</w> <w n="29.4">avec</w> <w n="29.5">mon</w> <w n="29.6">désespoir</w>,</l>
							<l n="30" num="3.6"><space quantity="2" unit="char"></space><w n="30.1">Vous</w> <w n="30.2">ne</w> <w n="30.3">pouvez</w> <w n="30.4">me</w> <w n="30.5">plaindre</w> <w n="30.6">ni</w> <w n="30.7">m</w>’<w n="30.8">entendre</w> ;</l>
							<l n="31" num="3.7"><w n="31.1">Vous</w> <w n="31.2">causez</w> <w n="31.3">la</w> <w n="31.4">douleur</w>, <w n="31.5">sans</w> <w n="31.6">même</w> <w n="31.7">la</w> <w n="31.8">comprendre</w> ;</l>
							<l n="32" num="3.8"><w n="32.1">À</w> <w n="32.2">quoi</w> <w n="32.3">me</w> <w n="32.4">servirait</w> <w n="32.5">de</w> <w n="32.6">vous</w> <w n="32.7">la</w> <w n="32.8">laisser</w> <w n="32.9">voir</w> ?</l>
							<l n="33" num="3.9"><w n="33.1">Victime</w> <w n="33.2">d</w>’<w n="33.3">un</w> <w n="33.4">amant</w>, <w n="33.5">par</w> <w n="33.6">vous</w>-<w n="33.7">même</w> <w n="33.8">trahie</w>,</l>
							<l n="34" num="3.10"><w n="34.1">J</w>’<w n="34.2">abhorre</w> <w n="34.3">l</w>’<w n="34.4">Amitié</w>, <w n="34.5">je</w> <w n="34.6">la</w> <w n="34.7">fuis</w> <w n="34.8">sans</w> <w n="34.9">retour</w> ;</l>
							<l n="35" num="3.11"><space quantity="6" unit="char"></space><w n="35.1">Et</w> <w n="35.2">je</w> <w n="35.3">vois</w>, <w n="35.4">à</w> <w n="35.5">sa</w> <w n="35.6">perfidie</w>,</l>
							<l n="36" num="3.12"><space quantity="6" unit="char"></space><w n="36.1">Que</w> <w n="36.2">l</w>’<w n="36.3">ingrate</w> <w n="36.4">est</w> <w n="36.5">sœur</w> <w n="36.6">de</w> <w n="36.7">l</w>’<w n="36.8">Amour</w>.</l>
						</lg>
					</div></body></text></TEI>