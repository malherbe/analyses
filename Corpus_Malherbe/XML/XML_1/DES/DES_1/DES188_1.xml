<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>9618 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>9618 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies</title>
					<author>Marceline Desbordes-Valmore</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>A. Boulland, libraire-éditeur</publisher>
						<date when="1830">1830</date>
					</imprint>
					<extent>9618 verstome second</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-12-20" who="RR">Insertion des corrections métriques dans le corps du texte.</change>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
				<change when="2018-11-02" who="RR">Correction dans le poème "Dors, ma Mère".</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part"> POÉSIES INÉDITES <lb></lb>ROMANCES</head><div type="poem" key="DES188">
						<head type="main">LA PIQÛRE</head>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">De</w> <w n="1.2">ses</w> <w n="1.3">fuseaux</w> <w n="1.4">légèrement</w> <w n="1.5">blessée</w>,</l>
							<l n="2" num="1.2"><w n="2.1">D</w>’<w n="2.2">où</w> <w n="2.3">vient</w> <w n="2.4">qu</w>’<w n="2.5">Isaure</w> <w n="2.6">a</w> <w n="2.7">regardé</w> <w n="2.8">vers</w> <w n="2.9">toi</w> ?</l>
							<l n="3" num="1.3"><w n="3.1">J</w>’<w n="3.2">allais</w> <w n="3.3">courir</w>, <w n="3.4">à</w> <w n="3.5">ses</w> <w n="3.6">cris</w> <w n="3.7">empressée</w>,</l>
							<l n="4" num="1.4"><w n="4.1">J</w>’<w n="4.2">allais</w> <w n="4.3">courir</w>… <w n="4.4">mais</w> <w n="4.5">tu</w> <w n="4.6">cours</w> <w n="4.7">mieux</w> <w n="4.8">que</w> <w n="4.9">moi</w>.</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1"><w n="5.1">Pourquoi</w> <w n="5.2">tes</w> <w n="5.3">yeux</w>, <w n="5.4">pleins</w> <w n="5.5">d</w>’<w n="5.6">une</w> <w n="5.7">pitié</w> <w n="5.8">tendre</w>,</l>
							<l n="6" num="2.2"><w n="6.1">Sont</w>-<w n="6.2">ils</w> <w n="6.3">restés</w> <w n="6.4">si</w> <w n="6.5">longtemps</w> <w n="6.6">sur</w> <w n="6.7">les</w> <w n="6.8">siens</w> ?</l>
							<l n="7" num="2.3"><w n="7.1">D</w>’<w n="7.2">où</w> <w n="7.3">vient</w> <w n="7.4">qu</w>’<w n="7.5">Isaure</w> <w n="7.6">a</w> <w n="7.7">paru</w> <w n="7.8">les</w> <w n="7.9">entendre</w> ?</l>
							<l n="8" num="2.4"><w n="8.1">Qu</w>’<w n="8.2">ils</w> <w n="8.3">me</w> <w n="8.4">font</w> <w n="8.5">mal</w> <w n="8.6">sur</w> <w n="8.7">d</w>’<w n="8.8">autres</w> <w n="8.9">que</w> <w n="8.10">les</w> <w n="8.11">miens</w> !</l>
						</lg>
						<lg n="3">
							<l n="9" num="3.1"><w n="9.1">Que</w> <w n="9.2">je</w> <w n="9.3">fus</w> <w n="9.4">triste</w> <w n="9.5">en</w> <w n="9.6">la</w> <w n="9.7">voyant</w> <w n="9.8">sourire</w> !</l>
							<l n="10" num="3.2"><w n="10.1">Que</w> <w n="10.2">je</w> <w n="10.3">tremblai</w> <w n="10.4">quand</w> <w n="10.5">tu</w> <w n="10.6">soutins</w> <w n="10.7">ses</w> <w n="10.8">pas</w> !</l>
							<l n="11" num="3.3"><w n="11.1">Tu</w> <w n="11.2">la</w> <w n="11.3">plaignais</w>… <w n="11.4">Que</w> <w n="11.5">n</w>’<w n="11.6">ai</w>-<w n="11.7">je</w> <w n="11.8">osé</w> <w n="11.9">te</w> <w n="11.10">dire</w> :</l>
							<l n="12" num="3.4">« <w n="12.1">C</w>’<w n="12.2">est</w> <w n="12.3">moi</w> <w n="12.4">qui</w> <w n="12.5">souffre</w>, <w n="12.6">et</w> <w n="12.7">tu</w> <w n="12.8">ne</w> <w n="12.9">le</w> <w n="12.10">vois</w> <w n="12.11">pas</w> ! »</l>
						</lg>
						<lg n="4">
							<l n="13" num="4.1"><w n="13.1">Tu</w> <w n="13.2">pris</w> <w n="13.3">sa</w> <w n="13.4">main</w>, <w n="13.5">tu</w> <w n="13.6">cherchas</w> <w n="13.7">sa</w> <w n="13.8">blessure</w>,</l>
							<l n="14" num="4.2"><w n="14.1">Pour</w> <w n="14.2">la</w> <w n="14.3">guérir</w>, <w n="14.4">tu</w> <w n="14.5">la</w> <w n="14.6">couvris</w> <w n="14.7">de</w> <w n="14.8">fleurs</w> ;</l>
							<l n="15" num="4.3"><w n="15.1">C</w>’<w n="15.2">étaient</w> <w n="15.3">mes</w> <w n="15.4">fleurs</w> ! <w n="15.5">elle</w> <w n="15.6">est</w> <w n="15.7">mieux</w>, <w n="15.8">j</w>’<w n="15.9">en</w> <w n="15.10">suis</w> <w n="15.11">sûre</w>.</l>
							<l n="16" num="4.4"><w n="16.1">Pourquoi</w> <w n="16.2">faut</w>-<w n="16.3">il</w> <w n="16.4">qu</w>’<w n="16.5">il</w> <w n="16.6">m</w>’<w n="16.7">en</w> <w n="16.8">coûte</w> <w n="16.9">des</w> <w n="16.10">pleurs</w> !</l>
						</lg>
					</div></body></text></TEI>