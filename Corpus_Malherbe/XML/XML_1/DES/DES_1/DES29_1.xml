<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>9618 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>9618 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies</title>
					<author>Marceline Desbordes-Valmore</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>A. Boulland, libraire-éditeur</publisher>
						<date when="1830">1830</date>
					</imprint>
					<extent>9618 verstome second</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-12-20" who="RR">Insertion des corrections métriques dans le corps du texte.</change>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
				<change when="2018-11-02" who="RR">Correction dans le poème "Dors, ma Mère".</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ÉLÉGIES</head><div type="poem" key="DES29">
						<head type="main">LA NUIT D’HIVER</head>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">Qui</w> <w n="1.2">m</w>’<w n="1.3">appelle</w> <w n="1.4">à</w> <w n="1.5">cette</w> <w n="1.6">heure</w> <w n="1.7">et</w> <w n="1.8">par</w> <w n="1.9">le</w> <w n="1.10">temps</w> <w n="1.11">qu</w>’<w n="1.12">il</w> <w n="1.13">fait</w> ?</l>
							<l n="2" num="1.2"><w n="2.1">C</w>’<w n="2.2">est</w> <w n="2.3">une</w> <w n="2.4">douce</w> <w n="2.5">voix</w>, <w n="2.6">c</w>’<w n="2.7">est</w> <w n="2.8">la</w> <w n="2.9">voix</w> <w n="2.10">d</w>’<w n="2.11">une</w> <w n="2.12">fille</w>.</l>
							<l n="3" num="1.3"><w n="3.1">Ah</w> ! <w n="3.2">je</w> <w n="3.3">te</w> <w n="3.4">reconnais</w> ; <w n="3.5">c</w>’<w n="3.6">est</w> <w n="3.7">toi</w>, <w n="3.8">Muse</w> <w n="3.9">gentille</w> ?</l>
							<l n="4" num="1.4"><space quantity="6" unit="char"></space><w n="4.1">Ton</w> <w n="4.2">souvenir</w> <w n="4.3">est</w> <w n="4.4">un</w> <w n="4.5">bienfait</w>.</l>
							<l n="5" num="1.5"><w n="5.1">Inespéré</w> <w n="5.2">retour</w> ! <w n="5.3">aimable</w> <w n="5.4">fantaisie</w> !</l>
							<l n="6" num="1.6"><w n="6.1">Après</w> <w n="6.2">un</w> <w n="6.3">an</w> <w n="6.4">d</w>’<w n="6.5">exil</w> <w n="6.6">qui</w> <w n="6.7">t</w>’<w n="6.8">amène</w> <w n="6.9">vers</w> <w n="6.10">moi</w> ?</l>
							<l n="7" num="1.7"><w n="7.1">Je</w> <w n="7.2">ne</w> <w n="7.3">t</w>’<w n="7.4">attendais</w> <w n="7.5">plus</w>, <w n="7.6">aimable</w> <w n="7.7">Poésie</w> ;</l>
							<l n="8" num="1.8"><w n="8.1">Je</w> <w n="8.2">ne</w> <w n="8.3">t</w>’<w n="8.4">attendais</w> <w n="8.5">plus</w>, <w n="8.6">mais</w> <w n="8.7">je</w> <w n="8.8">rêvais</w> <w n="8.9">à</w> <w n="8.10">toi</w>.</l>
						</lg>
						<lg n="2">
							<l n="9" num="2.1"><w n="9.1">Loin</w> <w n="9.2">du</w> <w n="9.3">réduit</w> <w n="9.4">obscur</w> <w n="9.5">où</w> <w n="9.6">lu</w> <w n="9.7">viens</w> <w n="9.8">de</w> <w n="9.9">descendre</w>,</l>
							<l n="10" num="2.2"><w n="10.1">L</w>’<w n="10.2">amitié</w>, <w n="10.3">le</w> <w n="10.4">bonheur</w>, <w n="10.5">la</w> <w n="10.6">gaieté</w>, <w n="10.7">tout</w> <w n="10.8">a</w> <w n="10.9">fui</w>.</l>
							<l n="11" num="2.3"><w n="11.1">Ô</w> <w n="11.2">ma</w> <w n="11.3">Muse</w> ! <w n="11.4">est</w>-<w n="11.5">ce</w> <w n="11.6">toi</w> <w n="11.7">que</w> <w n="11.8">j</w>’<w n="11.9">y</w> <w n="11.10">devais</w> <w n="11.11">attendre</w> ?</l>
							<l n="12" num="2.4"><w n="12.1">Il</w> <w n="12.2">est</w> <w n="12.3">fait</w> <w n="12.4">pour</w> <w n="12.5">les</w> <w n="12.6">pleurs</w> <w n="12.7">et</w> <w n="12.8">voilé</w> <w n="12.9">par</w> <w n="12.10">l</w>’<w n="12.11">ennui</w>.</l>
							<l n="13" num="2.5"><w n="13.1">Ce</w> <w n="13.2">triste</w> <w n="13.3">balancier</w>, <w n="13.4">dans</w> <w n="13.5">son</w> <w n="13.6">bruit</w> <w n="13.7">monotone</w>,</l>
							<l n="14" num="2.6"><w n="14.1">Marque</w> <w n="14.2">d</w>’<w n="14.3">un</w> <w n="14.4">temps</w> <w n="14.5">perdu</w> <w n="14.6">l</w>’<w n="14.7">inutile</w> <w n="14.8">lenteur</w> ;</l>
							<l n="15" num="2.7"><w n="15.1">Et</w> <w n="15.2">j</w>’<w n="15.3">ai</w> <w n="15.4">cru</w> <w n="15.5">vivre</w> <w n="15.6">un</w> <w n="15.7">siècle</w>, <w n="15.8">enfin</w>, <w n="15.9">quand</w> <w n="15.10">l</w>’<w n="15.11">heure</w> <w n="15.12">sonne</w>,</l>
							<l n="16" num="2.8"><space quantity="6" unit="char"></space><w n="16.1">Vide</w> <w n="16.2">d</w>’<w n="16.3">espoir</w> <w n="16.4">et</w> <w n="16.5">de</w> <w n="16.6">bonheur</w>.</l>
							<l n="17" num="2.9"><w n="17.1">L</w>’<w n="17.2">hiver</w> <w n="17.3">est</w> <w n="17.4">tout</w> <w n="17.5">entier</w> <w n="17.6">dans</w> <w n="17.7">ma</w> <w n="17.8">sombre</w> <w n="17.9">retraite</w> :</l>
							<l n="18" num="2.10"><space quantity="6" unit="char"></space><w n="18.1">Quel</w> <w n="18.2">temps</w> <w n="18.3">as</w>-<w n="18.4">tu</w> <w n="18.5">daigné</w> <w n="18.6">choisir</w> ?</l>
							<l n="19" num="2.11"><space quantity="2" unit="char"></space><w n="19.1">Que</w> <w n="19.2">doucement</w> <w n="19.3">par</w> <w n="19.4">toi</w> <w n="19.5">j</w>’<w n="19.6">en</w> <w n="19.7">suis</w> <w n="19.8">distraite</w> !</l>
							<l n="20" num="2.12"><w n="20.1">Oh</w> ! <w n="20.2">quand</w> <w n="20.3">il</w> <w n="20.4">nous</w> <w n="20.5">surprend</w>, <w n="20.6">qu</w>’<w n="20.7">il</w> <w n="20.8">est</w> <w n="20.9">beau</w> <w n="20.10">le</w> <w n="20.11">plaisir</w> !</l>
							<l n="21" num="2.13"><w n="21.1">D</w>’<w n="21.2">un</w> <w n="21.3">foyer</w> <w n="21.4">presque</w> <w n="21.5">éteint</w> <w n="21.6">la</w> <w n="21.7">flamme</w> <w n="21.8">salutaire</w></l>
							<l n="22" num="2.14"><w n="22.1">Par</w> <w n="22.2">intervalle</w> <w n="22.3">encor</w> <w n="22.4">trompe</w> <w n="22.5">l</w>’<w n="22.6">obscurité</w> ;</l>
							<l n="23" num="2.15"><w n="23.1">Si</w> <w n="23.2">tu</w> <w n="23.3">veux</w> <w n="23.4">écouter</w> <w n="23.5">ma</w> <w n="23.6">plainte</w> <w n="23.7">solitaire</w>,</l>
							<l n="24" num="2.16"><space quantity="6" unit="char"></space><w n="24.1">Nous</w> <w n="24.2">causerons</w> <w n="24.3">à</w> <w n="24.4">sa</w> <w n="24.5">clarté</w>.</l>
						</lg>
						<lg n="3">
							<l n="25" num="3.1"><space quantity="2" unit="char"></space><w n="25.1">Petite</w> <w n="25.2">Muse</w>, <w n="25.3">autrefois</w> <w n="25.4">vive</w> <w n="25.5">et</w> <w n="25.6">tendre</w>,</l>
							<l n="26" num="3.2"><w n="26.1">Dont</w> <w n="26.2">j</w>’<w n="26.3">ai</w> <w n="26.4">perdu</w> <w n="26.5">la</w> <w n="26.6">trace</w> <w n="26.7">au</w> <w n="26.8">temps</w> <w n="26.9">de</w> <w n="26.10">mes</w> <w n="26.11">malheurs</w>,</l>
							<l n="27" num="3.3"><w n="27.1">As</w>-<w n="27.2">tu</w> <w n="27.3">quelque</w> <w n="27.4">secret</w> <w n="27.5">pour</w> <w n="27.6">charmer</w> <w n="27.7">les</w> <w n="27.8">douleurs</w> ?</l>
							<l n="28" num="3.4"><w n="28.1">Viens</w> ! <w n="28.2">nul</w> <w n="28.3">autre</w> <w n="28.4">que</w> <w n="28.5">toi</w> <w n="28.6">n</w>’<w n="28.7">a</w> <w n="28.8">daigné</w> <w n="28.9">me</w> <w n="28.10">l</w>’<w n="28.11">apprendre</w>.</l>
							<l n="29" num="3.5"><w n="29.1">Écoute</w> ! <w n="29.2">nous</w> <w n="29.3">voilà</w> <w n="29.4">seules</w> <w n="29.5">dans</w> <w n="29.6">l</w>’<w n="29.7">univers</w>,</l>
							<l n="30" num="3.6"><space quantity="6" unit="char"></space><w n="30.1">Naïvement</w> <w n="30.2">je</w> <w n="30.3">vais</w> <w n="30.4">tout</w> <w n="30.5">dire</w> :</l>
							<l n="31" num="3.7"><w n="31.1">J</w>’<w n="31.2">ai</w> <w n="31.3">rencontré</w> <w n="31.4">l</w>’<w n="31.5">Amour</w>, <w n="31.6">il</w> <w n="31.7">a</w> <w n="31.8">brisé</w> <w n="31.9">ma</w> <w n="31.10">lyre</w> ;</l>
							<l n="32" num="3.8"><w n="32.1">Jaloux</w> <w n="32.2">d</w>’<w n="32.3">un</w> <w n="32.4">peu</w> <w n="32.5">de</w> <w n="32.6">gloire</w>, <w n="32.7">il</w> <w n="32.8">a</w> <w n="32.9">brûlé</w> <w n="32.10">mes</w> <w n="32.11">vers</w>.</l>
						</lg>
						<lg n="4">
							<l n="33" num="4.1">« <w n="33.1">Je</w> <w n="33.2">t</w>’<w n="33.3">ai</w> <w n="33.4">chanté</w>, <w n="33.5">lui</w> <w n="33.6">dis</w>-<w n="33.7">je</w>, <w n="33.8">et</w> <w n="33.9">ma</w> <w n="33.10">voix</w>, <w n="33.11">faible</w> <w n="33.12">encore</w>,</l>
							<l n="34" num="4.2"><w n="34.1">Dans</w> <w n="34.2">ses</w> <w n="34.3">premiers</w> <w n="34.4">accents</w> <w n="34.5">parut</w> <w n="34.6">juste</w> <w n="34.7">et</w> <w n="34.8">sonore</w>.</l>
							<l n="35" num="4.3"><w n="35.1">Pourquoi</w> <w n="35.2">briser</w> <w n="35.3">ma</w> <w n="35.4">lyre</w> ? <w n="35.5">elle</w> <w n="35.6">essayait</w> <w n="35.7">ta</w> <w n="35.8">loi</w>.</l>
							<l n="36" num="4.4"><w n="36.1">Pourquoi</w> <w n="36.2">brûler</w> <w n="36.3">mes</w> <w n="36.4">vers</w> ? <w n="36.5">je</w> <w n="36.6">les</w> <w n="36.7">ai</w> <w n="36.8">faits</w> <w n="36.9">pour</w> <w n="36.10">toi</w>.</l>
							<l n="37" num="4.5"><w n="37.1">Si</w> <w n="37.2">de</w> <w n="37.3">jeunes</w> <w n="37.4">amants</w> <w n="37.5">tu</w> <w n="37.6">troubles</w> <w n="37.7">le</w> <w n="37.8">délire</w>,</l>
							<l n="38" num="4.6"><w n="38.1">Cruel</w>, <w n="38.2">tu</w> <w n="38.3">n</w>’<w n="38.4">auras</w> <w n="38.5">plus</w> <w n="38.6">de</w> <w n="38.7">fleurs</w> <w n="38.8">dans</w> <w n="38.9">ton</w> <w n="38.10">empire</w> ;</l>
							<l n="39" num="4.7"><w n="39.1">Il</w> <w n="39.2">en</w> <w n="39.3">faut</w> <w n="39.4">à</w> <w n="39.5">mon</w> <w n="39.6">âge</w>, <w n="39.7">et</w> <w n="39.8">je</w> <w n="39.9">voulais</w>, <w n="39.10">un</w> <w n="39.11">jour</w>,</l>
							<l n="40" num="4.8"><w n="40.1">M</w>’<w n="40.2">en</w> <w n="40.3">parer</w> <w n="40.4">pour</w> <w n="40.5">te</w> <w n="40.6">plaire</w>, <w n="40.7">et</w> <w n="40.8">te</w> <w n="40.9">les</w> <w n="40.10">rendre</w>, <w n="40.11">Amour</w> !</l>
							<l n="41" num="4.9"><w n="41.1">Déjà</w> <w n="41.2">je</w> <w n="41.3">te</w> <w n="41.4">formais</w> <w n="41.5">une</w> <w n="41.6">simple</w> <w n="41.7">couronne</w>,</l>
							<l n="42" num="4.10"><w n="42.1">Fraîche</w>, <w n="42.2">douce</w> <w n="42.3">en</w> <w n="42.4">parfums</w>. <w n="42.5">Quand</w> <w n="42.6">un</w> <w n="42.7">cœur</w> <w n="42.8">pur</w> <w n="42.9">la</w> <w n="42.10">donne</w>,</l>
							<l n="43" num="4.11"><w n="43.1">Peux</w>-<w n="43.2">tu</w> <w n="43.3">la</w> <w n="43.4">dédaigner</w> ? <w n="43.5">Je</w> <w n="43.6">te</w> <w n="43.7">l</w>’<w n="43.8">offre</w> <w n="43.9">à</w> <w n="43.10">genoux</w> ;</l>
							<l n="44" num="4.12"><w n="44.1">Souris</w> <w n="44.2">à</w> <w n="44.3">mon</w> <w n="44.4">orgueil</w> <w n="44.5">et</w> <w n="44.6">n</w>’<w n="44.7">en</w> <w n="44.8">sois</w> <w n="44.9">point</w> <w n="44.10">jaloux</w>.</l>
							<l n="45" num="4.13"><w n="45.1">Je</w> <w n="45.2">n</w>’<w n="45.3">ai</w> <w n="45.4">jamais</w> <w n="45.5">senti</w> <w n="45.6">cet</w> <w n="45.7">orgueil</w> <w n="45.8">pour</w> <w n="45.9">moi</w>-<w n="45.10">même</w>,</l>
							<l n="46" num="4.14"><w n="46.1">Mais</w> <w n="46.2">il</w> <w n="46.3">dit</w> <w n="46.4">mon</w> <w n="46.5">secret</w>, <w n="46.6">mais</w> <w n="46.7">il</w> <w n="46.8">prouve</w> <w n="46.9">que</w> <w n="46.10">j</w>’<w n="46.11">aime</w>.</l>
							<l n="47" num="4.15"><w n="47.1">Eh</w> <w n="47.2">bien</w> ! <w n="47.3">fais</w> <w n="47.4">le</w> <w n="47.5">partage</w> <w n="47.6">en</w> <w n="47.7">généreux</w> <w n="47.8">vainqueur</w> :</l>
							<l n="48" num="4.16"><w n="48.1">Amour</w>, <w n="48.2">pour</w> <w n="48.3">toi</w> <w n="48.4">la</w> <w n="48.5">gloire</w>, <w n="48.6">et</w> <w n="48.7">pour</w> <w n="48.8">moi</w> <w n="48.9">le</w> <w n="48.10">bonheur</w>.</l>
							<l n="49" num="4.17"><w n="49.1">C</w>’<w n="49.2">est</w> <w n="49.3">un</w> <w n="49.4">bonheur</w> <w n="49.5">d</w>’<w n="49.6">aimer</w>, <w n="49.7">c</w>’<w n="49.8">en</w> <w n="49.9">est</w> <w n="49.10">un</w> <w n="49.11">de</w> <w n="49.12">le</w> <w n="49.13">dire</w>.</l>
							<l n="50" num="4.18"><w n="50.1">Amour</w>, <w n="50.2">prends</w> <w n="50.3">ma</w> <w n="50.4">couronne</w>, <w n="50.5">et</w> <w n="50.6">laisse</w>-<w n="50.7">moi</w> <w n="50.8">ma</w> <w n="50.9">lyre</w> ;</l>
							<l n="51" num="4.19"><w n="51.1">Prends</w> <w n="51.2">mes</w> <w n="51.3">vœux</w>, <w n="51.4">prends</w> <w n="51.5">ma</w> <w n="51.6">vie</w> ; <w n="51.7">enfin</w>, <w n="51.8">prends</w> <w n="51.9">tout</w>, <w n="51.10">cruel</w> !</l>
							<l n="52" num="4.20"><w n="52.1">Mais</w> <w n="52.2">laisse</w>-<w n="52.3">moi</w> <w n="52.4">chanter</w> <w n="52.5">au</w> <w n="52.6">pied</w> <w n="52.7">de</w> <w n="52.8">ton</w> <w n="52.9">autel</w>. »</l>
						</lg>
						<lg n="5">
							<l n="53" num="5.1"><space quantity="2" unit="char"></space><w n="53.1">Et</w> <w n="53.2">lui</w> : « <w n="53.3">Non</w>, <w n="53.4">non</w> ! <w n="53.5">Ta</w> <w n="53.6">prière</w> <w n="53.7">me</w> <w n="53.8">blesse</w> ;</l>
							<l n="54" num="5.2"><space quantity="2" unit="char"></space><w n="54.1">Dans</w> <w n="54.2">le</w> <w n="54.3">silence</w>, <w n="54.4">obéis</w> <w n="54.5">à</w> <w n="54.6">ma</w> <w n="54.7">loi</w> :</l>
							<l n="55" num="5.3"><space quantity="2" unit="char"></space><w n="55.1">Tes</w> <w n="55.2">yeux</w> <w n="55.3">en</w> <w n="55.4">pleurs</w>, <w n="55.5">plus</w> <w n="55.6">éloquents</w> <w n="55.7">que</w> <w n="55.8">toi</w>,</l>
							<l n="56" num="5.4"><w n="56.1">Révéleront</w> <w n="56.2">assez</w> <w n="56.3">ma</w> <w n="56.4">force</w> <w n="56.5">et</w> <w n="56.6">ta</w> <w n="56.7">faiblesse</w>. »</l>
						</lg>
						<lg n="6">
							<l n="57" num="6.1"><w n="57.1">Muse</w>, <w n="57.2">voilà</w> <w n="57.3">le</w> <w n="57.4">ton</w> <w n="57.5">de</w> <w n="57.6">ce</w> <w n="57.7">maître</w> <w n="57.8">si</w> <w n="57.9">doux</w>.</l>
							<l n="58" num="6.2"><w n="58.1">Je</w> <w n="58.2">n</w>’<w n="58.3">osai</w> <w n="58.4">lui</w> <w n="58.5">répondre</w>, <w n="58.6">et</w> <w n="58.7">je</w> <w n="58.8">versai</w> <w n="58.9">des</w> <w n="58.10">larmes</w> ;</l>
							<l n="59" num="6.3"><w n="59.1">Je</w> <w n="59.2">sentis</w> <w n="59.3">ma</w> <w n="59.4">blessure</w>, <w n="59.5">et</w> <w n="59.6">je</w> <w n="59.7">maudis</w> <w n="59.8">ses</w> <w n="59.9">armes</w>.</l>
							<l n="60" num="6.4"><w n="60.1">Pauvre</w> <w n="60.2">lyre</w> ! <w n="60.3">je</w> <w n="60.4">fus</w> <w n="60.5">muette</w> <w n="60.6">comme</w> <w n="60.7">vous</w> !</l>
						</lg>
						<lg n="7">
							<l n="61" num="7.1"><w n="61.1">L</w>’<w n="61.2">ingrat</w> ! <w n="61.3">il</w> <w n="61.4">a</w> <w n="61.5">puni</w> <w n="61.6">jusques</w> <w n="61.7">à</w> <w n="61.8">mon</w> <w n="61.9">silence</w>.</l>
							<l n="62" num="7.2"><space quantity="6" unit="char"></space><w n="62.1">Lassée</w> <w n="62.2">enfin</w> <w n="62.3">de</w> <w n="62.4">sa</w> <w n="62.5">puissance</w>,</l>
							<l n="63" num="7.3"><w n="63.1">Muse</w>, <w n="63.2">je</w> <w n="63.3">te</w> <w n="63.4">redonne</w> <w n="63.5">et</w> <w n="63.6">mes</w> <w n="63.7">vœux</w> <w n="63.8">et</w> <w n="63.9">mes</w> <w n="63.10">chants</w>.</l>
							<l n="64" num="7.4"><w n="64.1">Viens</w> <w n="64.2">leur</w> <w n="64.3">prêter</w> <w n="64.4">ta</w> <w n="64.5">grâce</w>, <w n="64.6">et</w> <w n="64.7">rends</w>-<w n="64.8">les</w> <w n="64.9">plus</w> <w n="64.10">touchants</w>.</l>
							<l n="65" num="7.5"><w n="65.1">Mais</w> <w n="65.2">tu</w> <w n="65.3">pâlis</w>, <w n="65.4">ma</w> <w n="65.5">chère</w>, <w n="65.6">et</w> <w n="65.7">le</w> <w n="65.8">froid</w> <w n="65.9">t</w>’<w n="65.10">a</w> <w n="65.11">saisie</w> !</l>
							<l n="66" num="7.6"><w n="66.1">C</w>’<w n="66.2">est</w> <w n="66.3">l</w>’<w n="66.4">hiver</w> <w n="66.5">qui</w> <w n="66.6">t</w>’<w n="66.7">opprime</w> <w n="66.8">et</w> <w n="66.9">ternit</w> <w n="66.10">tes</w> <w n="66.11">couleurs</w>.</l>
							<l n="67" num="7.7"><w n="67.1">Je</w> <w n="67.2">ne</w> <w n="67.3">puis</w> <w n="67.4">t</w>’<w n="67.5">arrêter</w>, <w n="67.6">charmante</w> <w n="67.7">Poésie</w> !</l>
							<l n="68" num="7.8"><w n="68.1">Adieu</w> ! <w n="68.2">tu</w> <w n="68.3">reviendras</w> <w n="68.4">dans</w> <w n="68.5">la</w> <w n="68.6">saison</w> <w n="68.7">des</w> <w n="68.8">fleurs</w>.</l>
						</lg>
					</div></body></text></TEI>