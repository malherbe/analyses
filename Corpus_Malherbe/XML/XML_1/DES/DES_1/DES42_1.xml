<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>9618 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>9618 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies</title>
					<author>Marceline Desbordes-Valmore</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>A. Boulland, libraire-éditeur</publisher>
						<date when="1830">1830</date>
					</imprint>
					<extent>9618 verstome second</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-12-20" who="RR">Insertion des corrections métriques dans le corps du texte.</change>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
				<change when="2018-11-02" who="RR">Correction dans le poème "Dors, ma Mère".</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ÉLÉGIES</head><div type="poem" key="DES42">
						<head type="main">LA DOULEUR</head>
						<lg n="1">
							<l n="1" num="1.1"><space quantity="6" unit="char"></space><w n="1.1">Sombre</w> <w n="1.2">douleur</w>, <w n="1.3">dégoût</w> <w n="1.4">du</w> <w n="1.5">monde</w>,</l>
							<l n="2" num="1.2"><space quantity="6" unit="char"></space><w n="2.1">Fruit</w> <w n="2.2">amer</w> <w n="2.3">de</w> <w n="2.4">l</w>’<w n="2.5">adversité</w>,</l>
							<l n="3" num="1.3"><w n="3.1">Où</w> <w n="3.2">l</w>’ <w n="3.3">âme</w> <w n="3.4">anéantie</w>, <w n="3.5">en</w> <w n="3.6">sa</w> <w n="3.7">chute</w> <w n="3.8">profonde</w>,</l>
							<l n="4" num="1.4"><space quantity="6" unit="char"></space><w n="4.1">Rêve</w> <w n="4.2">à</w> <w n="4.3">peine</w> <w n="4.4">à</w> <w n="4.5">l</w>’<w n="4.6">éternité</w>,</l>
							<l n="5" num="1.5"><space quantity="6" unit="char"></space><w n="5.1">Soulève</w> <w n="5.2">ton</w> <w n="5.3">poids</w> <w n="5.4">qui</w> <w n="5.5">m</w>’<w n="5.6">opprime</w>,</l>
							<l n="6" num="1.6"><w n="6.1">Dieu</w> <w n="6.2">l</w>’<w n="6.3">ordonne</w> ; <w n="6.4">un</w> <w n="6.5">moment</w> <w n="6.6">laisse</w>-<w n="6.7">moi</w> <w n="6.8">respirer</w>.</l>
							<l n="7" num="1.7"><w n="7.1">Ah</w> ! <w n="7.2">si</w> <w n="7.3">le</w> <w n="7.4">désespoir</w> <w n="7.5">à</w> <w n="7.6">ses</w> <w n="7.7">yeux</w> <w n="7.8">est</w> <w n="7.9">un</w> <w n="7.10">crime</w>,</l>
							<l n="8" num="1.8"><space quantity="2" unit="char"></space><w n="8.1">Laisse</w>-<w n="8.2">moi</w> <w n="8.3">donc</w> <w n="8.4">la</w> <w n="8.5">force</w> <w n="8.6">d</w>’<w n="8.7">espérer</w> !</l>
						</lg>
						<lg n="2">
							<l n="9" num="2.1"><w n="9.1">Si</w> <w n="9.2">dès</w> <w n="9.3">mes</w> <w n="9.4">jeunes</w> <w n="9.5">ans</w> <w n="9.6">j</w>’<w n="9.7">ai</w> <w n="9.8">repoussé</w> <w n="9.9">la</w> <w n="9.10">vie</w>,</l>
							<l n="10" num="2.2"><w n="10.1">Si</w> <w n="10.2">la</w> <w n="10.3">mélancolie</w> <w n="10.4">enveloppa</w> <w n="10.5">mes</w> <w n="10.6">jours</w>,</l>
							<l n="11" num="2.3"><space quantity="6" unit="char"></space><w n="11.1">Si</w> <w n="11.2">l</w>’<w n="11.3">amitié</w>, <w n="11.4">si</w> <w n="11.5">les</w> <w n="11.6">amours</w>,</l>
							<l n="12" num="2.4"><w n="12.1">M</w>’<w n="12.2">ont</w> <w n="12.3">attristée</w> <w n="12.4">autant</w> <w n="12.5">qu</w>’<w n="12.6">ils</w> <w n="12.7">m</w>’<w n="12.8">avaient</w> <w n="12.9">asservie</w> ;</l>
							<l n="13" num="2.5"><w n="13.1">Si</w> <w n="13.2">déjà</w> <w n="13.3">mon</w> <w n="13.4">printemps</w> <w n="13.5">n</w>’<w n="13.6">est</w> <w n="13.7">qu</w>’<w n="13.8">un</w> <w n="13.9">froid</w> <w n="13.10">souvenir</w>,</l>
							<l n="14" num="2.6"><w n="14.1">Si</w> <w n="14.2">la</w> <w n="14.3">mort</w> <w n="14.4">a</w> <w n="14.5">soufflé</w> <w n="14.6">sur</w> <w n="14.7">une</w> <w n="14.8">jeune</w> <w n="14.9">flamme</w></l>
							<l n="15" num="2.7"><w n="15.1">Qui</w> <w n="15.2">vient</w>, <w n="15.3">en</w> <w n="15.4">s</w>’<w n="15.5">éteignant</w>, <w n="15.6">d</w>’<w n="15.7">éteindre</w> <w n="15.8">aussi</w> <w n="15.9">mon</w> <w n="15.10">âme</w>,</l>
							<l n="16" num="2.8"><w n="16.1">Laisse</w>-<w n="16.2">moi</w> <w n="16.3">vivre</w> <w n="16.4">au</w> <w n="16.5">moins</w> <w n="16.6">dans</w> <w n="16.7">un</w> <w n="16.8">autre</w> <w n="16.9">avenir</w> !</l>
							<l n="17" num="2.9"><w n="17.1">Laisse</w>-<w n="17.2">moi</w> <w n="17.3">respirer</w>, <w n="17.4">désespoir</w> <w n="17.5">d</w>’<w n="17.6">une</w> <w n="17.7">mère</w> !</l>
							<l n="18" num="2.10"><w n="18.1">Dieu</w> <w n="18.2">l</w>’<w n="18.3">ordonne</w>, <w n="18.4">Dieu</w> <w n="18.5">parle</w> <w n="18.6">à</w> <w n="18.7">mon</w> <w n="18.8">cœur</w> <w n="18.9">éperdu</w>.</l>
							<l n="19" num="2.11">« <w n="19.1">Suis</w> <w n="19.2">mon</w> <w n="19.3">arrêt</w>, <w n="19.4">dit</w>-<w n="19.5">il</w>, <w n="19.6">reste</w> <w n="19.7">encor</w> <w n="19.8">sur</w> <w n="19.9">la</w> <w n="19.10">terre</w>. »</l>
							<l n="20" num="2.12"><w n="20.1">S</w>’<w n="20.2">il</w> <w n="20.3">ne</w> <w n="20.4">venait</w> <w n="20.5">de</w> <w n="20.6">Dieu</w>, <w n="20.7">serait</w>-<w n="20.8">il</w> <w n="20.9">entendu</w> ?</l>
							<l n="21" num="2.13"><w n="21.1">Mais</w>, <w n="21.2">vers</w> <w n="21.3">l</w>’<w n="21.4">éternité</w> <w n="21.5">quand</w> <w n="21.6">cette</w> <w n="21.7">âme</w> <w n="21.8">brûlante</w></l>
							<l n="22" num="2.14"><space quantity="2" unit="char"></space><w n="22.1">S</w>’<w n="22.2">envolera</w>, <w n="22.3">baignée</w> <w n="22.4">encor</w> <w n="22.5">de</w> <w n="22.6">pleurs</w>,</l>
							<l n="23" num="2.15"><w n="23.1">Délivrée</w> <w n="23.2">à</w> <w n="23.3">jamais</w> <w n="23.4">d</w>’<w n="23.5">une</w> <w n="23.6">chaîne</w> <w n="23.7">accablante</w>,</l>
							<l n="24" num="2.16"><w n="24.1">Je</w> <w n="24.2">reverrai</w> <w n="24.3">mon</w> <w n="24.4">fils</w> : <w n="24.5">quel</w> <w n="24.6">prix</w> <w n="24.7">de</w> <w n="24.8">mes</w> <w n="24.9">douleurs</w> !</l>
							<l n="25" num="2.17"><space quantity="2" unit="char"></space><w n="25.1">Éternité</w> <w n="25.2">consolante</w> <w n="25.3">ou</w> <w n="25.4">terrible</w> !</l>
							<l n="26" num="2.18"><space quantity="2" unit="char"></space><w n="26.1">Pour</w> <w n="26.2">le</w> <w n="26.3">méchant</w>, <w n="26.4">c</w>’<w n="26.5">est</w> <w n="26.6">l</w>’<w n="26.7">enfer</w>, <w n="26.8">c</w>’<w n="26.9">est</w> <w n="26.10">son</w> <w n="26.11">cœur</w> ;</l>
							<l n="27" num="2.19"><w n="27.1">Mais</w> <w n="27.2">pour</w> <w n="27.3">l</w>’<w n="27.4">être</w> <w n="27.5">innocent</w>, <w n="27.6">malheureux</w> <w n="27.7">et</w> <w n="27.8">sensible</w>,</l>
							<l n="28" num="2.20"><space quantity="6" unit="char"></space><w n="28.1">C</w>’<w n="28.2">est</w> <w n="28.3">le</w> <w n="28.4">repos</w>, <w n="28.5">c</w>’<w n="28.6">est</w> <w n="28.7">le</w> <w n="28.8">bonheur</w> !</l>
						</lg>
						<lg n="3">
							<l n="29" num="3.1"><w n="29.1">Ô</w> <w n="29.2">Dieu</w> ! <w n="29.3">quand</w> <w n="29.4">de</w> <w n="29.5">mon</w> <w n="29.6">fils</w> <w n="29.7">sonna</w> <w n="29.8">l</w>’<w n="29.9">heure</w> <w n="29.10">suprême</w>,</l>
							<l n="30" num="3.2"><space quantity="2" unit="char"></space><w n="30.1">Un</w> <w n="30.2">doute</w> <w n="30.3">affreux</w> <w n="30.4">ne</w> <w n="30.5">m</w>’<w n="30.6">a</w> <w n="30.7">pas</w> <w n="30.8">fait</w> <w n="30.9">frémir</w> :</l>
							<l n="31" num="3.3"><w n="31.1">Non</w>, <w n="31.2">cet</w> <w n="31.3">être</w> <w n="31.4">charmant</w>, <w n="31.5">au</w> <w n="31.6">sein</w> <w n="31.7">de</w> <w n="31.8">la</w> <w n="31.9">mort</w> <w n="31.10">même</w>,</l>
							<l n="32" num="3.4"><space quantity="10" unit="char"></space><w n="32.1">N</w>’<w n="32.2">a</w> <w n="32.3">fait</w> <w n="32.4">que</w> <w n="32.5">s</w>’<w n="32.6">endormir</w>.</l>
						</lg>
						<lg n="4">
							<l n="33" num="4.1"><w n="33.1">Ô</w> <w n="33.2">tendresse</w> ! <w n="33.3">ô</w> <w n="33.4">douleur</w> ! <w n="33.5">ô</w> <w n="33.6">sublime</w> <w n="33.7">mélange</w> !</l>
							<l n="34" num="4.2"><w n="34.1">Ses</w> <w n="34.2">yeux</w> <w n="34.3">remplis</w> <w n="34.4">d</w>’<w n="34.5">amour</w> <w n="34.6">se</w> <w n="34.7">ferment</w> <w n="34.8">sur</w> <w n="34.9">mes</w> <w n="34.10">yeux</w> ;</l>
							<l n="35" num="4.3"><w n="35.1">Je</w> <w n="35.2">m</w>’<w n="35.3">attache</w> <w n="35.4">à</w> <w n="35.5">son</w> <w n="35.6">corps</w>… <w n="35.7">Ce</w> <w n="35.8">n</w>’<w n="35.9">était</w> <w n="35.10">plus</w> <w n="35.11">qu</w>’<w n="35.12">un</w> <w n="35.13">ange</w></l>
							<l n="36" num="4.4"><space quantity="10" unit="char"></space><w n="36.1">Qui</w> <w n="36.2">s</w>’<w n="36.3">envolait</w> <w n="36.4">aux</w> <w n="36.5">cieux</w>.</l>
						</lg>
					</div></body></text></TEI>