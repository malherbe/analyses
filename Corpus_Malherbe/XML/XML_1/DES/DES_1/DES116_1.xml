<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>9618 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>9618 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies</title>
					<author>Marceline Desbordes-Valmore</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>A. Boulland, libraire-éditeur</publisher>
						<date when="1830">1830</date>
					</imprint>
					<extent>9618 verstome second</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-12-20" who="RR">Insertion des corrections métriques dans le corps du texte.</change>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
				<change when="2018-11-02" who="RR">Correction dans le poème "Dors, ma Mère".</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ROMANCES</head><div type="poem" key="DES116">
						<head type="main">UN MOMENT</head>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">Un</w> <w n="1.2">moment</w> <w n="1.3">suffira</w> <w n="1.4">pour</w> <w n="1.5">payer</w> <w n="1.6">une</w> <w n="1.7">année</w> ;</l>
							<l n="2" num="1.2"><w n="2.1">Le</w> <w n="2.2">regret</w> <w n="2.3">plus</w> <w n="2.4">longtemps</w> <w n="2.5">ne</w> <w n="2.6">peut</w> <w n="2.7">nourrir</w> <w n="2.8">mon</w> <w n="2.9">sort</w>.</l>
							<l n="3" num="1.3"><w n="3.1">Quoi</w> ! <w n="3.2">l</w>’<w n="3.3">Amour</w> <w n="3.4">n</w>’<w n="3.5">a</w>-<w n="3.6">t</w>-<w n="3.7">il</w> <w n="3.8">pas</w> <w n="3.9">une</w> <w n="3.10">heure</w> <w n="3.11">fortunée</w></l>
							<l n="4" num="1.4"><w n="4.1">Pour</w> <w n="4.2">celle</w> <w n="4.3">dont</w>, <w n="4.4">peut</w>-<w n="4.5">être</w>, <w n="4.6">il</w> <w n="4.7">avance</w> <w n="4.8">la</w> <w n="4.9">mort</w> ?</l>
							<l n="5" num="1.5"><w n="5.1">Une</w> <w n="5.2">heure</w>, <w n="5.3">une</w> <w n="5.4">heure</w>, <w n="5.5">Amour</w> ! <w n="5.6">une</w> <w n="5.7">heure</w> <w n="5.8">sans</w> <w n="5.9">alarmes</w>,</l>
							<l n="6" num="1.6"><w n="6.1">Avec</w> <w n="6.2">lui</w>, <w n="6.3">loin</w> <w n="6.4">du</w> <w n="6.5">monde</w> ! <w n="6.6">après</w> <w n="6.7">ce</w> <w n="6.8">long</w> <w n="6.9">tourment</w>,</l>
							<l n="7" num="1.7"><w n="7.1">Laisse</w> <w n="7.2">encor</w> <w n="7.3">se</w> <w n="7.4">mêler</w> <w n="7.5">nos</w> <w n="7.6">regards</w> <w n="7.7">et</w> <w n="7.8">nos</w> <w n="7.9">larmes</w> ;</l>
							<l n="8" num="1.8"><w n="8.1">Et</w> <w n="8.2">si</w> <w n="8.3">c</w>’<w n="8.4">est</w> <w n="8.5">trop</w> <w n="8.6">d</w>’<w n="8.7">une</w> <w n="8.8">heure</w>… <w n="8.9">un</w> <w n="8.10">moment</w> ! <w n="8.11">un</w> <w n="8.12">moment</w> !</l>
						</lg>
						<lg n="2">
							<l n="9" num="2.1"><w n="9.1">Vois</w>-<w n="9.2">tu</w> <w n="9.3">ces</w> <w n="9.4">fleurs</w>, <w n="9.5">Amour</w> ? <w n="9.6">c</w>’<w n="9.7">est</w> <w n="9.8">lui</w> <w n="9.9">qui</w> <w n="9.10">les</w> <w n="9.11">envoie</w>,</l>
							<l n="10" num="2.2"><w n="10.1">Brûlantes</w> <w n="10.2">de</w> <w n="10.3">son</w> <w n="10.4">souffle</w>, <w n="10.5">humides</w> <w n="10.6">de</w> <w n="10.7">ses</w> <w n="10.8">pleurs</w> ;</l>
							<l n="11" num="2.3"><w n="11.1">Sèche</w>-<w n="11.2">les</w> <w n="11.3">sur</w> <w n="11.4">mon</w> <w n="11.5">sein</w> <w n="11.6">par</w> <w n="11.7">un</w> <w n="11.8">rayon</w> <w n="11.9">de</w> <w n="11.10">joie</w>,</l>
							<l n="12" num="2.4"><w n="12.1">Et</w> <w n="12.2">que</w> <w n="12.3">je</w> <w n="12.4">vive</w> <w n="12.5">assez</w> <w n="12.6">pour</w> <w n="12.7">lui</w> <w n="12.8">rendre</w> <w n="12.9">ses</w> <w n="12.10">fleurs</w> !</l>
							<l n="13" num="2.5"><w n="13.1">Une</w> <w n="13.2">heure</w>, <w n="13.3">une</w> <w n="13.4">heure</w>, <w n="13.5">Amour</w> ! <w n="13.6">une</w> <w n="13.7">heure</w> <w n="13.8">sans</w> <w n="13.9">alarmes</w>,</l>
							<l n="14" num="2.6"><w n="14.1">Avec</w> <w n="14.2">lui</w>, <w n="14.3">loin</w> <w n="14.4">du</w> <w n="14.5">monde</w> ! <w n="14.6">après</w> <w n="14.7">ce</w> <w n="14.8">long</w> <w n="14.9">tourment</w>,</l>
							<l n="15" num="2.7"><w n="15.1">Laisse</w> <w n="15.2">encor</w> <w n="15.3">se</w> <w n="15.4">mêler</w> <w n="15.5">nos</w> <w n="15.6">regards</w> <w n="15.7">et</w> <w n="15.8">nos</w> <w n="15.9">larmes</w> ;</l>
							<l n="16" num="2.8"><w n="16.1">Et</w> <w n="16.2">si</w> <w n="16.3">c</w>’<w n="16.4">est</w> <w n="16.5">trop</w> <w n="16.6">d</w>’<w n="16.7">une</w> <w n="16.8">heure</w>… <w n="16.9">un</w> <w n="16.10">moment</w> ! <w n="16.11">un</w> <w n="16.12">moment</w> !</l>
						</lg>
						<lg n="3">
							<l n="17" num="3.1"><w n="17.1">Rends</w>-<w n="17.2">moi</w> <w n="17.3">le</w> <w n="17.4">son</w> <w n="17.5">chéri</w> <w n="17.6">de</w> <w n="17.7">cette</w> <w n="17.8">voix</w> <w n="17.9">fidèle</w> :</l>
							<l n="18" num="3.2"><w n="18.1">Il</w> <w n="18.2">m</w>’<w n="18.3">aime</w>, <w n="18.4">il</w> <w n="18.5">souffre</w>, <w n="18.6">il</w> <w n="18.7">meurt</w>, <w n="18.8">et</w> <w n="18.9">tu</w> <w n="18.10">peux</w> <w n="18.11">le</w> <w n="18.12">guérir</w> !</l>
							<l n="19" num="3.3"><w n="19.1">Que</w> <w n="19.2">je</w> <w n="19.3">sente</w> <w n="19.4">sa</w> <w n="19.5">main</w>, <w n="19.6">que</w> <w n="19.7">je</w> <w n="19.8">dise</w> : « <w n="19.9">C</w>’<w n="19.10">est</w> <w n="19.11">elle</w> ! »</l>
							<l n="20" num="3.4"><w n="20.1">Qu</w>’<w n="20.2">il</w> <w n="20.3">me</w> <w n="20.4">dise</w> : « <w n="20.5">Je</w> <w n="20.6">meurs</w> ! » <w n="20.7">Alors</w>, <w n="20.8">fais</w>-<w n="20.9">moi</w> <w n="20.10">mourir</w>.</l>
							<l n="21" num="3.5"><w n="21.1">Une</w> <w n="21.2">heure</w>, <w n="21.3">une</w> <w n="21.4">heure</w>, <w n="21.5">Amour</w> ! <w n="21.6">une</w> <w n="21.7">heure</w> <w n="21.8">sans</w> <w n="21.9">alarmes</w>,</l>
							<l n="22" num="3.6"><w n="22.1">Avec</w> <w n="22.2">lui</w>, <w n="22.3">loin</w> <w n="22.4">du</w> <w n="22.5">monde</w> ! <w n="22.6">après</w> <w n="22.7">ce</w> <w n="22.8">long</w> <w n="22.9">tourment</w>,</l>
							<l n="23" num="3.7"><w n="23.1">Laisse</w> <w n="23.2">encor</w> <w n="23.3">se</w> <w n="23.4">mêler</w> <w n="23.5">nos</w> <w n="23.6">regards</w> <w n="23.7">et</w> <w n="23.8">nos</w> <w n="23.9">larmes</w> ;</l>
							<l n="24" num="3.8"><w n="24.1">Et</w> <w n="24.2">si</w> <w n="24.3">c</w>’<w n="24.4">est</w> <w n="24.5">trop</w> <w n="24.6">d</w>’<w n="24.7">une</w> <w n="24.8">heure</w>… <w n="24.9">un</w> <w n="24.10">moment</w> ! <w n="24.11">un</w> <w n="24.12">moment</w> !</l>
						</lg>
					</div></body></text></TEI>