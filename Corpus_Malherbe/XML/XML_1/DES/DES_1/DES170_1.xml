<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>9618 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>9618 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies</title>
					<author>Marceline Desbordes-Valmore</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>A. Boulland, libraire-éditeur</publisher>
						<date when="1830">1830</date>
					</imprint>
					<extent>9618 verstome second</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-12-20" who="RR">Insertion des corrections métriques dans le corps du texte.</change>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
				<change when="2018-11-02" who="RR">Correction dans le poème "Dors, ma Mère".</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part"> POÉSIES INÉDITES <lb></lb>MÉLANGES</head><div type="poem" key="DES170">
						<head type="main">UN JOUR DE DEUIL</head>
						<lg n="1">
							<head>La mère</head>
								<l n="1" num="1.1"><w n="1.1">Rentrons</w>, <w n="1.2">mes</w> <w n="1.3">chers</w> <w n="1.4">enfants</w> ; <w n="1.5">de</w> <w n="1.6">la</w> <w n="1.7">foule</w> <w n="1.8">éplorée</w></l>
								<l n="2" num="1.2"><w n="2.1">Laissons</w> <w n="2.2">les</w> <w n="2.3">flots</w> <w n="2.4">émus</w> <w n="2.5">s</w>’<w n="2.6">écouler</w> <w n="2.7">loin</w> <w n="2.8">de</w> <w n="2.9">nous</w>.</l>
								<l n="3" num="1.3"><w n="3.1">D</w>’<w n="3.2">une</w> <w n="3.3">grande</w> <w n="3.4">douleur</w> <w n="3.5">je</w> <w n="3.6">me</w> <w n="3.7">sens</w> <w n="3.8">déchirée</w> :</l>
								<l n="4" num="1.4"><w n="4.1">Notre</w> <w n="4.2">France</w> <w n="4.3">est</w> <w n="4.4">en</w> <w n="4.5">deuil</w>, <w n="4.6">mettez</w>-<w n="4.7">vous</w> <w n="4.8">à</w> <w n="4.9">genoux</w>.</l>
						</lg>
						<lg n="2">
							<head>L’enfant</head>
								<l n="5" num="2.1"><w n="5.1">Que</w> <w n="5.2">d</w>’<w n="5.3">hommes</w>, <w n="5.4">ô</w> <w n="5.5">ma</w> <w n="5.6">mère</w>, <w n="5.7">ont</w> <w n="5.8">passé</w> <w n="5.9">tout</w> <w n="5.10">à</w> <w n="5.11">l</w>’<w n="5.12">heure</w> !</l>
								<l n="6" num="2.2"><w n="6.1">De</w> <w n="6.2">la</w> <w n="6.3">même</w> <w n="6.4">tristesse</w> <w n="6.5">ils</w> <w n="6.6">paraissaient</w> <w n="6.7">souffrir</w>.</l>
								<l n="7" num="2.3"><space quantity="6" unit="char"></space><w n="7.1">D</w>’<w n="7.2">où</w> <w n="7.3">vient</w> <w n="7.4">que</w> <w n="7.5">tout</w> <w n="7.6">le</w> <w n="7.7">monde</w> <w n="7.8">pleure</w> ?</l>
								<l n="8" num="2.4"><space quantity="6" unit="char"></space><w n="8.1">Est</w>-<w n="8.2">ce</w> <w n="8.3">un</w> <w n="8.4">roi</w> <w n="8.5">qui</w> <w n="8.6">vient</w> <w n="8.7">de</w> <w n="8.8">mourir</w> ?</l>
						</lg>
						<lg n="3">
							<head>La mère</head>
								<l n="9" num="3.1"><w n="9.1">C</w>’<w n="9.2">est</w> <w n="9.3">un</w> <w n="9.4">homme</w>, <w n="9.5">ô</w> <w n="9.6">mon</w> <w n="9.7">fils</w> ! <w n="9.8">un</w> <w n="9.9">génie</w> <w n="9.10">adorable</w>,</l>
								<l n="10" num="3.2"><w n="10.1">L</w>’<w n="10.2">amour</w> <w n="10.3">d</w>’<w n="10.4">un</w> <w n="10.5">peuple</w> <w n="10.6">immense</w> <w n="10.7">et</w> <w n="10.8">son</w> <w n="10.9">plus</w> <w n="10.10">ferme</w> <w n="10.11">appui</w> ;</l>
								<l n="11" num="3.3"><w n="11.1">C</w>’<w n="11.2">est</w> <w n="11.3">de</w> <w n="11.4">tout</w> <w n="11.5">notre</w> <w n="11.6">espoir</w> <w n="11.7">la</w> <w n="11.8">perte</w> <w n="11.9">irréparable</w> ;</l>
								<l n="12" num="3.4"><w n="12.1">C</w>’<w n="12.2">est</w> <w n="12.3">notre</w> <w n="12.4">gloire</w> <w n="12.5">éteinte</w>, <w n="12.6">elle</w> <w n="12.7">était</w> <w n="12.8">toute</w> <w n="12.9">en</w> <w n="12.10">lui</w>.</l>
						</lg>
						<lg n="4">
							<head>L’enfant</head>
								<l part="I" n="13" num="4.1"><w n="13.1">Ô</w> <w n="13.2">ma</w> <w n="13.3">mère</w> ! </l>
						</lg>
						<lg n="5">
							<head>La mère</head>
								<l part="F" n="13"><w n="13.4">Ô</w> <w n="13.5">douleur</w> ! <w n="13.6">ô</w> <w n="13.7">lugubre</w> <w n="13.8">journée</w> !</l>
								<l n="14" num="5.1"><w n="14.1">Voyez</w>-<w n="14.2">vous</w>, <w n="14.3">mes</w> <w n="14.4">enfants</w>, <w n="14.5">la</w> <w n="14.6">cité</w> <w n="14.7">consternée</w> ?</l>
								<l n="15" num="5.2"><w n="15.1">Tout</w> <w n="15.2">un</w> <w n="15.3">peuple</w> <w n="15.4">en</w> <w n="15.5">cortège</w>, <w n="15.6">et</w> <w n="15.7">tous</w> <w n="15.8">nos</w> <w n="15.9">toits</w> <w n="15.10">en</w> <w n="15.11">deuil</w>,</l>
								<l n="16" num="5.3"><w n="16.1">Et</w> <w n="16.2">tous</w> <w n="16.3">ces</w> <w n="16.4">bras</w> <w n="16.5">unis</w> <w n="16.6">pour</w> <w n="16.7">porter</w> <w n="16.8">un</w> <w n="16.9">cercueil</w> ?</l>
						</lg>
						<lg n="6">
							<head>L’enfant</head>
								<l part="I" n="17" num="6.1"><w n="17.1">Nous</w> <w n="17.2">ne</w> <w n="17.3">les</w> <w n="17.4">voyons</w> <w n="17.5">plus</w> ! </l>
						</lg>
						<lg n="7">
							<head>La mère</head>
								<l part="F" n="17"><w n="17.6">Non</w> ; <w n="17.7">sous</w> <w n="17.8">de</w> <w n="17.9">sombres</w> <w n="17.10">voiles</w></l>
								<l n="18" num="7.1"><w n="18.1">La</w> <w n="18.2">nuit</w> <w n="18.3">comme</w> <w n="18.4">la</w> <w n="18.5">mort</w> <w n="18.6">les</w> <w n="18.7">dérobe</w> <w n="18.8">à</w> <w n="18.9">nos</w> <w n="18.10">yeux</w> ;</l>
								<l n="19" num="7.2"><w n="19.1">Non</w>, <w n="19.2">le</w> <w n="19.3">ciel</w> <w n="19.4">attristé</w> <w n="19.5">ne</w> <w n="19.6">montre</w> <w n="19.7">point</w> <w n="19.8">d</w>’<w n="19.9">étoiles</w>,</l>
								<l n="20" num="7.3"><w n="20.1">Mais</w> <w n="20.2">des</w> <w n="20.3">sanglots</w> <w n="20.4">lointains</w> <w n="20.5">dirigent</w> <w n="20.6">nos</w> <w n="20.7">adieux</w>.</l>
								<l n="21" num="7.4"><w n="21.1">Ainsi</w> <w n="21.2">des</w> <w n="21.3">rois</w> <w n="21.4">de</w> <w n="21.5">l</w>’<w n="21.6">air</w> <w n="21.7">les</w> <w n="21.8">cohortes</w> <w n="21.9">hardies</w></l>
								<l n="22" num="7.5"><w n="22.1">Ont</w> <w n="22.2">suivi</w> <w n="22.3">dans</w> <w n="22.4">l</w>’<w n="22.5">orage</w> <w n="22.6">un</w> <w n="22.7">aigle</w> <w n="22.8">insurmonté</w> ;</l>
								<l n="23" num="7.6"><w n="23.1">Impatient</w> <w n="23.2">des</w> <w n="23.3">cieux</w> <w n="23.4">et</w> <w n="23.5">de</w> <w n="23.6">la</w> <w n="23.7">liberté</w>,</l>
								<l n="24" num="7.7"><w n="24.1">Si</w> <w n="24.2">la</w> <w n="24.3">foudre</w> <w n="24.4">a</w> <w n="24.5">brûlé</w> <w n="24.6">ses</w> <w n="24.7">ailes</w> <w n="24.8">agrandies</w>,</l>
								<l n="25" num="7.8"><w n="25.1">Il</w> <w n="25.2">tombe</w> ; <w n="25.3">et</w>, <w n="25.4">d</w>’<w n="25.5">un</w> <w n="25.6">long</w> <w n="25.7">cri</w> <w n="25.8">proclamant</w> <w n="25.9">leur</w> <w n="25.10">douleur</w>,</l>
								<l n="26" num="7.9"><w n="26.1">Les</w> <w n="26.2">bataillons</w> <w n="26.3">troublés</w> <w n="26.4">s</w>’<w n="26.5">abattent</w>, <w n="26.6">se</w> <w n="26.7">confondent</w> ;</l>
								<l n="27" num="7.10"><w n="27.1">Des</w> <w n="27.2">échos</w> <w n="27.3">orageux</w> <w n="27.4">les</w> <w n="27.5">soupirs</w> <w n="27.6">leur</w> <w n="27.7">répondent</w>,</l>
								<l n="28" num="7.11"><w n="28.1">Et</w> <w n="28.2">le</w> <w n="28.3">deuil</w> <w n="28.4">de</w> <w n="28.5">la</w> <w n="28.6">terre</w> <w n="28.7">encense</w> <w n="28.8">leur</w> <w n="28.9">malheur</w>.</l>
								<l n="29" num="7.12"><w n="29.1">Comme</w> <w n="29.2">elle</w> <w n="29.3">a</w> <w n="29.4">retenti</w> <w n="29.5">cette</w> <w n="29.6">mort</w> <w n="29.7">éloquente</w> !</l>
								<l n="30" num="7.13"><w n="30.1">Quel</w> <w n="30.2">cœur</w> <w n="30.3">n</w>’<w n="30.4">a</w> <w n="30.5">tressailli</w> <w n="30.6">de</w> <w n="30.7">son</w> <w n="30.8">dernier</w> <w n="30.9">soupir</w> ?</l>
								<l n="31" num="7.14"><space quantity="6" unit="char"></space><w n="31.1">Quelle</w> <w n="31.2">calamité</w> <w n="31.3">frappante</w> !</l>
								<l n="32" num="7.15"><w n="32.1">Quel</w> <w n="32.2">courage</w> <w n="32.3">assez</w> <w n="32.4">dur</w> <w n="32.5">pour</w> <w n="32.6">ne</w> <w n="32.7">la</w> <w n="32.8">point</w> <w n="32.9">sentir</w> ?</l>
								<l n="33" num="7.16"><w n="33.1">Inclinez</w>-<w n="33.2">vous</w>, <w n="33.3">priez</w> <w n="33.4">devant</w> <w n="33.5">cette</w> <w n="33.6">ombre</w> <w n="33.7">auguste</w> !</l>
								<l n="34" num="7.17"><w n="34.1">Tous</w> <w n="34.2">ses</w> <w n="34.3">jours</w> <w n="34.4">sont</w> <w n="34.5">écrits</w> <w n="34.6">dans</w> <w n="34.7">ce</w> <w n="34.8">funeste</w> <w n="34.9">jour</w>.</l>
								<l n="35" num="7.18"><w n="35.1">Ah</w> ! <w n="35.2">jugez</w> <w n="35.3">si</w> <w n="35.4">sa</w> <w n="35.5">voix</w> <w n="35.6">était</w> <w n="35.7">la</w> <w n="35.8">voix</w> <w n="35.9">du</w> <w n="35.10">juste</w>,</l>
								<l n="36" num="7.19"><w n="36.1">Puisqu</w>’<w n="36.2">elle</w> <w n="36.3">a</w> <w n="36.4">pénétré</w> <w n="36.5">dans</w> <w n="36.6">notre</w> <w n="36.7">humble</w> <w n="36.8">séjour</w> !</l>
						</lg>
						<lg n="8">
							<head>L’enfant</head>
								<l part="I" n="37" num="8.1"><w n="37.1">Vous</w> <w n="37.2">l</w>’<w n="37.3">avez</w> <w n="37.4">donc</w> <w n="37.5">connu</w> ? </l>
						</lg>
						<lg n="9">
							<head>La mère</head>
								<l part="F" n="37"><w n="37.6">Jamais</w> <w n="37.7">de</w> <w n="37.8">sa</w> <w n="37.9">présence</w></l>
								<l n="38" num="9.1"><w n="38.1">Mes</w> <w n="38.2">regards</w> <w n="38.3">attendris</w> <w n="38.4">n</w>’<w n="38.5">ont</w> <w n="38.6">goûté</w> <w n="38.7">la</w> <w n="38.8">douceur</w>.</l>
								<l n="39" num="9.2"><w n="39.1">Il</w> <w n="39.2">attirait</w>, <w n="39.3">absent</w>, <w n="39.4">notre</w> <w n="39.5">reconnaissance</w>,</l>
								<l n="40" num="9.3"><w n="40.1">Et</w> <w n="40.2">de</w> <w n="40.3">son</w> <w n="40.4">nom</w> <w n="40.5">lui</w> <w n="40.6">seul</w> <w n="40.7">ignorait</w> <w n="40.8">la</w> <w n="40.9">splendeur</w>.</l>
								<l n="41" num="9.4"><space quantity="6" unit="char"></space><w n="41.1">Au</w> <w n="41.2">sein</w> <w n="41.3">de</w> <w n="41.4">sa</w> <w n="41.5">gloire</w> <w n="41.6">éclatante</w></l>
								<l n="42" num="9.5"><space quantity="6" unit="char"></space><w n="42.1">Son</w> <w n="42.2">âme</w> <w n="42.3">n</w>’<w n="42.4">était</w> <w n="42.5">pas</w> <w n="42.6">contente</w> ;</l>
								<l n="43" num="9.6"><w n="43.1">Il</w> <w n="43.2">n</w>’<w n="43.3">obtenait</w> <w n="43.4">jamais</w> <w n="43.5">ce</w> <w n="43.6">qu</w>’<w n="43.7">imploraient</w> <w n="43.8">ses</w> <w n="43.9">vœux</w>.</l>
								<l n="44" num="9.7"><w n="44.1">Ses</w> <w n="44.2">vœux</w> <w n="44.3">étaient</w> <w n="44.4">si</w> <w n="44.5">purs</w> ! <w n="44.6">son</w> <w n="44.7">âme</w> <w n="44.8">était</w> <w n="44.9">si</w> <w n="44.10">belle</w> !</l>
								<l n="45" num="9.8"><w n="45.1">L</w>’<w n="45.2">esprit</w> <w n="45.3">qu</w>’<w n="45.4">il</w> <w n="45.5">combattait</w> <w n="45.6">lui</w> <w n="45.7">restait</w> <w n="45.8">si</w> <w n="45.9">rebelle</w> !</l>
								<l n="46" num="9.9"><w n="46.1">Esprit</w> <w n="46.2">d</w>’<w n="46.3">un</w> <w n="46.4">meilleur</w> <w n="46.5">monde</w>, <w n="46.6">il</w> <w n="46.7">va</w> <w n="46.8">nous</w> <w n="46.9">plaindre</w> <w n="46.10">aux</w> <w n="46.11">cieux</w>.</l>
						</lg>
						<lg n="10">
							<head>L’enfant</head>
								<l part="I" n="47" num="10.1"><w n="47.1">Mère</w>, <w n="47.2">étiez</w>-<w n="47.3">vous</w> <w n="47.4">moins</w> <w n="47.5">pauvre</w> ? </l>
						</lg>
						<lg n="11">
							<head>La mère</head>
								<l part="F" n="47"><w n="47.6">Oui</w> ! <w n="47.7">j</w>’<w n="47.8">avais</w> <w n="47.9">l</w>’<w n="47.10">espérance</w> ;</l>
								<l n="48" num="11.1"><w n="48.1">J</w>’<w n="48.2">en</w> <w n="48.3">palpitais</w> <w n="48.4">pour</w> <w n="48.5">vous</w>, <w n="48.6">pour</w> <w n="48.7">notre</w> <w n="48.8">belle</w> <w n="48.9">France</w> ;</l>
								<l n="49" num="11.2"><w n="49.1">Enfants</w> ! <w n="49.2">je</w> <w n="49.3">vous</w> <w n="49.4">voyais</w> <w n="49.5">libres</w> <w n="49.6">dans</w> <w n="49.7">l</w>’<w n="49.8">avenir</w>.</l>
								<l n="50" num="11.3"><w n="50.1">Il</w> <w n="50.2">n</w>’<w n="50.3">est</w> <w n="50.4">plus</w>, <w n="50.5">rien</w> <w n="50.6">n</w>’<w n="50.7">est</w> <w n="50.8">plus</w> ; <w n="50.9">qu</w>’<w n="50.10">allez</w>-<w n="50.11">vous</w> <w n="50.12">devenir</w> ?</l>
						</lg>
						<lg n="12">
							<head>L’enfant</head>
								<l part="I" n="51" num="12.1"><w n="51.1">Pour</w> <w n="51.2">qui</w> <w n="51.3">faut</w>-<w n="51.4">il</w> <w n="51.5">prier</w> ? </l>
						</lg>
						<lg n="13">
							<head>La mère</head>
								<l part="F" n="51"><w n="51.6">Pour</w> <w n="51.7">ceux</w> <w n="51.8">qui</w> <w n="51.9">lui</w> <w n="51.10">survivent</w>,</l>
								<l n="52" num="13.1"><w n="52.1">Ceux</w> <w n="52.2">qu</w>’<w n="52.3">à</w> <w n="52.4">la</w> <w n="52.5">terre</w> <w n="52.6">encor</w> <w n="52.7">de</w> <w n="52.8">chers</w> <w n="52.9">liens</w> <w n="52.10">captivent</w> ;</l>
								<l n="53" num="13.2"><w n="53.1">Pour</w> <w n="53.2">ses</w> <w n="53.3">jeunes</w> <w n="53.4">rameaux</w> <w n="53.5">qui</w> <w n="53.6">croissaient</w> <w n="53.7">près</w> <w n="53.8">de</w> <w n="53.9">lui</w> ;</l>
								<l n="54" num="13.3"><w n="54.1">Pour</w> <w n="54.2">sa</w> <w n="54.3">moitié</w> <w n="54.4">mourante</w> <w n="54.5">et</w> <w n="54.6">qui</w> <w n="54.7">n</w>’<w n="54.8">a</w> <w n="54.9">plus</w> <w n="54.10">d</w>’<w n="54.11">appui</w> !</l>
								<l n="55" num="13.4"><w n="55.1">Vous</w> <w n="55.2">l</w>’<w n="55.3">avez</w> <w n="55.4">vu</w> <w n="55.5">passer</w> <w n="55.6">sur</w> <w n="55.7">un</w> <w n="55.8">plus</w> <w n="55.9">beau</w> <w n="55.10">rivage</w> :</l>
								<l n="56" num="13.5"><w n="56.1">De</w> <w n="56.2">ses</w> <w n="56.3">jours</w> <w n="56.4">courageux</w> <w n="56.5">prolongeant</w> <w n="56.6">les</w> <w n="56.7">hasards</w>,</l>
								<l n="57" num="13.6"><w n="57.1">Il</w> <w n="57.2">allait</w> <w n="57.3">d</w>’<w n="57.4">un</w> <w n="57.5">ciel</w> <w n="57.6">pur</w> <w n="57.7">essayer</w> <w n="57.8">les</w> <w n="57.9">regards</w>.</l>
								<l n="58" num="13.7"><w n="58.1">Oh</w> ! <w n="58.2">rappelez</w>-<w n="58.3">vous</w> <w n="58.4">bien</w> <w n="58.5">les</w> <w n="58.6">traits</w> <w n="58.7">de</w> <w n="58.8">son</w> <w n="58.9">visage</w> !</l>
								<l n="59" num="13.8"><w n="59.1">La</w> <w n="59.2">pâleur</w> <w n="59.3">de</w> <w n="59.4">son</w> <w n="59.5">front</w> <w n="59.6">faisait</w> <w n="59.7">déjà</w> <w n="59.8">frémir</w></l>
								<l n="60" num="13.9"><w n="60.1">Tous</w> <w n="60.2">les</w> <w n="60.3">cœurs</w> <w n="60.4">qu</w>’<w n="60.5">à</w> <w n="60.6">présent</w> <w n="60.7">vous</w> <w n="60.8">entendez</w> <w n="60.9">gémir</w>.</l>
								<l n="61" num="13.10"><w n="61.1">Sur</w> <w n="61.2">ses</w> <w n="61.3">pas</w> <w n="61.4">chancelants</w> <w n="61.5">quelle</w> <w n="61.6">foule</w> <w n="61.7">empressée</w> !</l>
								<l n="62" num="13.11"><w n="62.1">Que</w> <w n="62.2">d</w>’<w n="62.3">amour</w> ! <w n="62.4">sa</w> <w n="62.5">grande</w> <w n="62.6">âme</w> <w n="62.7">en</w> <w n="62.8">était</w> <w n="62.9">oppressée</w>.</l>
								<l n="63" num="13.12"><w n="63.1">N</w>’<w n="63.2">oubliez</w> <w n="63.3">pas</w> <w n="63.4">ce</w> <w n="63.5">jour</w>, <w n="63.6">le</w> <w n="63.7">plus</w> <w n="63.8">beau</w> <w n="63.9">de</w> <w n="63.10">vos</w> <w n="63.11">jours</w> ;</l>
								<l n="64" num="13.13"><w n="64.1">Nourrissez</w>-<w n="64.2">en</w> <w n="64.3">mes</w> <w n="64.4">pleurs</w>, <w n="64.5">et</w> <w n="64.6">parlez</w>-<w n="64.7">m</w>’<w n="64.8">en</w> <w n="64.9">toujours</w> !</l>
						</lg>
						<lg n="14">
							<head>L’enfant</head>
								<l n="65" num="14.1"><w n="65.1">Toujours</w> <w n="65.2">je</w> <w n="65.3">m</w>’<w n="65.4">en</w> <w n="65.5">souviens</w>, <w n="65.6">ma</w> <w n="65.7">mère</w> : <w n="65.8">sur</w> <w n="65.9">la</w> <w n="65.10">rive</w>,</l>
								<l n="66" num="14.2"><w n="66.1">Mon</w> <w n="66.2">père</w> <w n="66.3">qui</w> <w n="66.4">courait</w> <w n="66.5">m</w>’<w n="66.6">élevait</w> <w n="66.7">dans</w> <w n="66.8">ses</w> <w n="66.9">bras</w> ;</l>
								<l n="67" num="14.3"><w n="67.1">L</w>’<w n="67.2">homme</w> <w n="67.3">qu</w>’<w n="67.4">on</w> <w n="67.5">adorait</w> <w n="67.6">n</w>’<w n="67.7">avait</w> <w n="67.8">point</w> <w n="67.9">de</w> <w n="67.10">soldats</w>,</l>
								<l n="68" num="14.4"><w n="68.1">Il</w> <w n="68.2">avait</w> <w n="68.3">ses</w> <w n="68.4">enfants</w>, <w n="68.5">et</w> <w n="68.6">l</w>’<w n="68.7">on</w> <w n="68.8">criait</w> : « <w n="68.9">Qu</w>’<w n="68.10">il</w> <w n="68.11">vive</w> !</l>
								<l n="69" num="14.5"><w n="69.1">Qu</w>’<w n="69.2">il</w> <w n="69.3">vive</w> ! <w n="69.4">il</w> <w n="69.5">est</w> <w n="69.6">l</w>’<w n="69.7">ami</w> <w n="69.8">du</w> <w n="69.9">pauvre</w> <w n="69.10">vertueux</w> ! »</l>
								<l n="70" num="14.6"><w n="70.1">Moi</w>, <w n="70.2">je</w> <w n="70.3">criais</w> <w n="70.4">aussi</w> ; <w n="70.5">car</w> <w n="70.6">je</w> <w n="70.7">voyais</w> <w n="70.8">ses</w> <w n="70.9">yeux</w></l>
								<l n="71" num="14.7"><w n="71.1">Répondre</w> <w n="71.2">avec</w> <w n="71.3">douceur</w> <w n="71.4">à</w> <w n="71.5">ces</w> <w n="71.6">âmes</w> <w n="71.7">contentes</w>,</l>
								<l n="72" num="14.8"><w n="72.1">Qui</w> <w n="72.2">jetaient</w> <w n="72.3">devant</w> <w n="72.4">lui</w> <w n="72.5">leurs</w> <w n="72.6">clameurs</w> <w n="72.7">éclatantes</w>.</l>
								<l n="73" num="14.9"><w n="73.1">On</w> <w n="73.2">suivit</w> <w n="73.3">son</w> <w n="73.4">navire</w>, <w n="73.5">on</w> <w n="73.6">le</w> <w n="73.7">couvrit</w> <w n="73.8">de</w> <w n="73.9">fleurs</w> ;</l>
								<l n="74" num="14.10"><w n="74.1">Il</w> <w n="74.2">détourna</w> <w n="74.3">ses</w> <w n="74.4">yeux</w> <w n="74.5">comme</w> <w n="74.6">en</w> <w n="74.7">cachant</w> <w n="74.8">des</w> <w n="74.9">pleurs</w>.</l>
								<l n="75" num="14.11"><w n="75.1">Partout</w> <w n="75.2">des</w> <w n="75.3">chants</w> <w n="75.4">français</w> <w n="75.5">appelaient</w> <w n="75.6">son</w> <w n="75.7">sourire</w> :</l>
								<l n="76" num="14.12"><w n="76.1">Son</w> <w n="76.2">sourire</w> <w n="76.3">était</w> <w n="76.4">triste</w> ; <w n="76.5">il</w> <w n="76.6">paraissait</w> <w n="76.7">nous</w> <w n="76.8">dire</w> :</l>
								<l n="77" num="14.13">« <w n="77.1">Adieu</w> ! <w n="77.2">vos</w> <w n="77.3">vœux</w> <w n="77.4">bientôt</w> <w n="77.5">me</w> <w n="77.6">seront</w> <w n="77.7">superflus</w>. »</l>
								<l n="78" num="14.14"><w n="78.1">Ma</w> <w n="78.2">mère</w> ! <w n="78.3">et</w> <w n="78.4">c</w>’<w n="78.5">est</w> <w n="78.6">donc</w> <w n="78.7">lui</w> <w n="78.8">que</w> <w n="78.9">je</w> <w n="78.10">ne</w> <w n="78.11">verrai</w> <w n="78.12">plus</w> ?</l>
						</lg>
						<lg n="15">
							<head>La mère</head>
								<l n="79" num="15.1"><w n="79.1">Pour</w> <w n="79.2">la</w> <w n="79.3">dernière</w> <w n="79.4">fois</w> <w n="79.5">la</w> <w n="79.6">France</w> <w n="79.7">l</w>’<w n="79.8">environne</w>.</l>
								<l n="80" num="15.2"><w n="80.1">Riche</w>, <w n="80.2">pauvre</w>, <w n="80.3">tout</w> <w n="80.4">pleure</w> <w n="80.5">à</w> <w n="80.6">ce</w> <w n="80.7">noble</w> <w n="80.8">convoi</w> ;</l>
								<l n="81" num="15.3"><w n="81.1">Le</w> <w n="81.2">méchant</w> <w n="81.3">devant</w> <w n="81.4">lui</w> <w n="81.5">recule</w> <w n="81.6">avec</w> <w n="81.7">effroi</w>,</l>
								<l n="82" num="15.4"><w n="82.1">Devant</w> <w n="82.2">lui</w> <w n="82.3">le</w> <w n="82.4">bonheur</w> <w n="82.5">effeuille</w> <w n="82.6">sa</w> <w n="82.7">couronne</w>.</l>
								<l n="83" num="15.5"><w n="83.1">Du</w> <w n="83.2">haut</w> <w n="83.3">d</w>’<w n="83.4">un</w> <w n="83.5">char</w> <w n="83.6">léger</w> <w n="83.7">tristement</w> <w n="83.8">descendus</w>,</l>
								<l n="84" num="15.6"><w n="84.1">Pâlissant</w> <w n="84.2">sous</w> <w n="84.3">les</w> <w n="84.4">fleurs</w> <w n="84.5">qui</w> <w n="84.6">brillaient</w> <w n="84.7">sur</w> <w n="84.8">leur</w> <w n="84.9">tête</w>,</l>
								<l n="85" num="15.7"><w n="85.1">De</w> <w n="85.2">jeunes</w> <w n="85.3">fiancés</w> <w n="85.4">ont</w> <w n="85.5">oublié</w> <w n="85.6">leur</w> <w n="85.7">fête</w>,</l>
								<l n="86" num="15.8"><w n="86.1">Et</w> <w n="86.2">dans</w> <w n="86.3">le</w> <w n="86.4">deuil</w> <w n="86.5">public</w> <w n="86.6">ils</w> <w n="86.7">marchent</w> <w n="86.8">confondus</w>.</l>
								<l n="87" num="15.9"><w n="87.1">Que</w> <w n="87.2">sur</w> <w n="87.3">tous</w>, <w n="87.4">à</w> <w n="87.5">cette</w> <w n="87.6">heure</w>, <w n="87.7">une</w> <w n="87.8">femme</w> <w n="87.9">est</w> <w n="87.10">à</w> <w n="87.11">plaindre</w> !</l>
								<l n="88" num="15.10"><w n="88.1">Quel</w> <w n="88.2">lien</w> <w n="88.3">glorieux</w> <w n="88.4">se</w> <w n="88.5">brise</w> <w n="88.6">dans</w> <w n="88.7">son</w> <w n="88.8">cœur</w> !</l>
								<l n="89" num="15.11"><w n="89.1">Que</w> <w n="89.2">de</w> <w n="89.3">femmes</w> <w n="89.4">naguère</w> <w n="89.5">enviaient</w> <w n="89.6">son</w> <w n="89.7">bonheur</w>,</l>
								<l n="90" num="15.12"><space quantity="6" unit="char"></space><w n="90.1">Et</w> <w n="90.2">que</w> <w n="90.3">le</w> <w n="90.4">bonheur</w> <w n="90.5">est</w> <w n="90.6">à</w> <w n="90.7">craindre</w> !</l>
								<l n="91" num="15.13"><w n="91.1">Dans</w> <w n="91.2">sa</w> <w n="91.3">gloire</w> <w n="91.4">funèbre</w>, <w n="91.5">oh</w> ! <w n="91.6">qu</w>’<w n="91.7">elle</w> <w n="91.8">doit</w> <w n="91.9">souffrir</w> !</l>
								<l n="92" num="15.14"><w n="92.1">Au</w> <w n="92.2">pied</w> <w n="92.3">d</w>’<w n="92.4">un</w> <w n="92.5">lit</w> <w n="92.6">désert</w> <w n="92.7">sa</w> <w n="92.8">douleur</w> <w n="92.9">s</w>’<w n="92.10">est</w> <w n="92.11">cachée</w> :</l>
								<l n="93" num="15.15"><w n="93.1">C</w>’<w n="93.2">est</w> <w n="93.3">là</w> <w n="93.4">que</w>, <w n="93.5">gémissants</w>, <w n="93.6">ses</w> <w n="93.7">enfants</w> <w n="93.8">l</w>’<w n="93.9">ont</w> <w n="93.10">cherchée</w> ;</l>
								<l n="94" num="15.16"><w n="94.1">C</w>’<w n="94.2">est</w> <w n="94.3">là</w> <w n="94.4">que</w> <w n="94.5">leurs</w> <w n="94.6">sanglots</w> <w n="94.7">l</w>’<w n="94.8">empêchent</w> <w n="94.9">de</w> <w n="94.10">mourir</w>.</l>
						</lg>
						<lg n="16">
							<head>L’enfant</head>
								<l part="I" n="95" num="16.1"><w n="95.1">Ils</w> <w n="95.2">sont</w> <w n="95.3">donc</w> <w n="95.4">orphelins</w> ? </l>
						</lg>
						<lg n="17">
							<head>La mère</head>
								<l part="F" n="95"><w n="95.5">On</w> <w n="95.6">le</w> <w n="95.7">voit</w> <w n="95.8">à</w> <w n="95.9">nos</w> <w n="95.10">larmes</w>.</l>
								<l n="96" num="17.1"><w n="96.1">Sur</w> <w n="96.2">son</w> <w n="96.3">corps</w> <w n="96.4">immobile</w> <w n="96.5">on</w> <w n="96.6">a</w> <w n="96.7">posé</w> <w n="96.8">ses</w> <w n="96.9">armes</w>,</l>
								<l n="97" num="17.2"><w n="97.1">Ses</w> <w n="97.2">armes</w> <w n="97.3">que</w> <w n="97.4">pour</w> <w n="97.5">nous</w> <w n="97.6">Dieu</w> <w n="97.7">guida</w> <w n="97.8">tant</w> <w n="97.9">de</w> <w n="97.10">fois</w>,</l>
								<l n="98" num="17.3"><w n="98.1">Avant</w> <w n="98.2">qu</w>’<w n="98.3">en</w> <w n="98.4">ses</w> <w n="98.5">discours</w> <w n="98.6">Dieu</w> <w n="98.7">répandît</w> <w n="98.8">sa</w> <w n="98.9">voix</w>.</l>
						</lg>
						<lg n="18">
							<head>L’enfant</head>
								<l part="I" n="99" num="18.1"><w n="99.1">Ses</w> <w n="99.2">enfants</w> ! <w n="99.3">ses</w> <w n="99.4">enfants</w> ! </l>
						</lg>
						<lg n="19">
							<head>La mère</head>
								<l part="F" n="99"><w n="99.5">La</w> <w n="99.6">France</w> <w n="99.7">est</w> <w n="99.8">leur</w> <w n="99.9">égide</w> ;</l>
								<l n="100" num="19.1"><w n="100.1">Elle</w> <w n="100.2">couve</w> <w n="100.3">en</w> <w n="100.4">son</w> <w n="100.5">sein</w> <w n="100.6">ces</w> <w n="100.7">fruits</w> <w n="100.8">faibles</w> <w n="100.9">encor</w> ;</l>
								<l n="101" num="19.2"><w n="101.1">Ils</w> <w n="101.2">n</w>’<w n="101.3">ont</w> <w n="101.4">que</w> <w n="101.5">des</w> <w n="101.6">lauriers</w>, <w n="101.7">leur</w> <w n="101.8">patrie</w> <w n="101.9">et</w> <w n="101.10">point</w> <w n="101.11">d</w>’<w n="101.12">or</w>.</l>
								<l n="102" num="19.3"><w n="102.1">L</w>’<w n="102.2">ami</w> <w n="102.3">du</w> <w n="102.4">peuple</w> <w n="102.5">est</w> <w n="102.6">pauvre</w>, <w n="102.7">et</w> <w n="102.8">sa</w> <w n="102.9">gloire</w> <w n="102.10">est</w> <w n="102.11">rigide</w>.</l>
								<l n="103" num="19.4"><w n="103.1">Nos</w> <w n="103.2">maux</w> <w n="103.3">étaient</w> <w n="103.4">les</w> <w n="103.5">siens</w>, <w n="103.6">nos</w> <w n="103.7">biens</w> <w n="103.8">seront</w> <w n="103.9">les</w> <w n="103.10">leurs</w> ;</l>
								<l n="104" num="19.5"><w n="104.1">L</w>’<w n="104.2">offrande</w> <w n="104.3">jaillira</w> <w n="104.4">d</w>’<w n="104.5">une</w> <w n="104.6">source</w> <w n="104.7">innocente</w> ;</l>
								<l n="105" num="19.6"><space quantity="6" unit="char"></space><w n="105.1">Et</w> <w n="105.2">la</w> <w n="105.3">France</w> <w n="105.4">reconnaissante</w></l>
								<l n="106" num="19.7"><space quantity="6" unit="char"></space><w n="106.1">N</w>’<w n="106.2">a</w> <w n="106.3">point</w> <w n="106.4">de</w> <w n="106.5">stériles</w> <w n="106.6">douleurs</w>.</l>
						</lg>
					</div></body></text></TEI>