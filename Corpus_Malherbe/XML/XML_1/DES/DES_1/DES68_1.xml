<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>9618 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>9618 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies</title>
					<author>Marceline Desbordes-Valmore</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>A. Boulland, libraire-éditeur</publisher>
						<date when="1830">1830</date>
					</imprint>
					<extent>9618 verstome second</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-12-20" who="RR">Insertion des corrections métriques dans le corps du texte.</change>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
				<change when="2018-11-02" who="RR">Correction dans le poème "Dors, ma Mère".</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ÉLÉGIES</head><div type="poem" key="DES68">
						<head type="main">LE RÊVE DE MON ENFANT</head>
						<head type="sub_1">À MADAME PAULINE DUCHAMBGE</head>
						<lg n="1">
							<l n="1" num="1.1">« <w n="1.1">Mère</w> ! <w n="1.2">petite</w> <w n="1.3">mère</w> ! » <w n="1.4">Il</w> <w n="1.5">m</w>’<w n="1.6">appelait</w> <w n="1.7">ainsi</w> ;</l>
							<l n="2" num="1.2"><w n="2.1">Et</w> <w n="2.2">moi</w>, <w n="2.3">je</w> <w n="2.4">tressaillais</w> <w n="2.5">à</w> <w n="2.6">cet</w> <w n="2.7">accent</w> <w n="2.8">si</w> <w n="2.9">tendre</w> ;</l>
							<l n="3" num="1.3"><w n="3.1">Tout</w> <w n="3.2">mon</w> <w n="3.3">être</w> <w n="3.4">agité</w> <w n="3.5">s</w>’<w n="3.6">éveillait</w> <w n="3.7">pour</w> <w n="3.8">l</w>’<w n="3.9">entendre</w> ;</l>
							<l n="4" num="1.4"><w n="4.1">Je</w> <w n="4.2">ne</w> <w n="4.3">l</w>’<w n="4.4">entendrai</w> <w n="4.5">plus</w> : <w n="4.6">il</w> <w n="4.7">ne</w> <w n="4.8">dort</w> <w n="4.9">plus</w> <w n="4.10">ici</w>.</l>
							<l n="5" num="1.5"><w n="5.1">Où</w> <w n="5.2">retentit</w> <w n="5.3">sa</w> <w n="5.4">voix</w>, <w n="5.5">qui</w> <w n="5.6">calmait</w> <w n="5.7">ma</w> <w n="5.8">souffrance</w>,</l>
							<l n="6" num="1.6"><space quantity="6" unit="char"></space><w n="6.1">Comme</w> <w n="6.2">la</w> <w n="6.3">voix</w> <w n="6.4">de</w> <w n="6.5">l</w>’<w n="6.6">espérance</w>,</l>
							<l n="7" num="1.7"><w n="7.1">Formée</w> (<w n="7.2">on</w> <w n="7.3">l</w>’<w n="7.4">aurait</w> <w n="7.5">dit</w>) <w n="7.6">de</w> <w n="7.7">rosée</w> <w n="7.8">et</w> <w n="7.9">de</w> <w n="7.10">miel</w> ?</l>
							<l n="8" num="1.8"><w n="8.1">Le</w> <w n="8.2">ciel</w> <w n="8.3">en</w> <w n="8.4">fut</w> <w n="8.5">jaloux</w>, <w n="8.6">elle</w> <w n="8.7">doit</w> <w n="8.8">être</w> <w n="8.9">au</w> <w n="8.10">ciel</w>.</l>
							<l n="9" num="1.9"><w n="9.1">Non</w> ! <w n="9.2">elle</w> <w n="9.3">est</w> <w n="9.4">dans</w> <w n="9.5">mon</w> <w n="9.6">cœur</w> : <w n="9.7">je</w> <w n="9.8">l</w>’<w n="9.9">y</w> <w n="9.10">tiens</w> <w n="9.11">enfermée</w> ;</l>
							<l n="10" num="1.10"><w n="10.1">Elle</w> <w n="10.2">soupire</w> <w n="10.3">encore</w>, <w n="10.4">elle</w> <w n="10.5">parle</w> <w n="10.6">avec</w> <w n="10.7">moi</w>.</l>
							<l n="11" num="1.11"><w n="11.1">Durant</w> <w n="11.2">mes</w> <w n="11.3">longues</w> <w n="11.4">nuits</w>, <w n="11.5">cette</w> <w n="11.6">voix</w> <w n="11.7">tant</w> <w n="11.8">aimée</w></l>
							<l n="12" num="1.12"><w n="12.1">Me</w> <w n="12.2">dit</w> : « <w n="12.3">Ne</w> <w n="12.4">pleure</w> <w n="12.5">plus</w> ! <w n="12.6">je</w> <w n="12.7">ne</w> <w n="12.8">dors</w> <w n="12.9">pas</w> <w n="12.10">pour</w> <w n="12.11">toi</w>. »</l>
							<l n="13" num="1.13"><w n="13.1">Oh</w> ! <w n="13.2">moitié</w> <w n="13.3">de</w> <w n="13.4">ma</w> <w n="13.5">vie</w>, <w n="13.6">à</w> <w n="13.7">ma</w> <w n="13.8">vie</w> <w n="13.9">arrachée</w> !</l>
							<l n="14" num="1.14"><w n="14.1">Viens</w> ! <w n="14.2">redis</w>-<w n="14.3">moi</w> <w n="14.4">ton</w> <w n="14.5">rêve</w> ; <w n="14.6">il</w> <w n="14.7">m</w>’<w n="14.8">a</w> <w n="14.9">prédit</w> <w n="14.10">ton</w> <w n="14.11">sort</w>.</l>
							<l n="15" num="1.15"><w n="15.1">Que</w> <w n="15.2">ta</w> <w n="15.3">plainte</w>, <w n="15.4">une</w> <w n="15.5">fois</w> <w n="15.6">de</w> <w n="15.7">mon</w> <w n="15.8">cœur</w> <w n="15.9">épanchée</w>,</l>
							<l n="16" num="1.16"><w n="16.1">Rappelle</w> <w n="16.2">un</w> <w n="16.3">jeune</w> <w n="16.4">cygne</w> <w n="16.5">et</w> <w n="16.6">son</w> <w n="16.7">doux</w> <w n="16.8">chant</w> <w n="16.9">de</w> <w n="16.10">mort</w> !</l>
							<l n="17" num="1.17">« <w n="17.1">Écoute</w>, <w n="17.2">m</w>’<w n="17.3">as</w>-<w n="17.4">tu</w> <w n="17.5">dit</w>, <w n="17.6">écoute</w> <w n="17.7">mon</w> <w n="17.8">beau</w> <w n="17.9">songe</w> ! »</l>
							<l n="18" num="1.18"><w n="18.1">Le</w> <w n="18.2">premier</w>… <w n="18.3">le</w> <w n="18.4">dernier</w> <w n="18.5">qui</w> <w n="18.6">berça</w> <w n="18.7">ton</w> <w n="18.8">sommeil</w> !</l>
							<l n="19" num="1.19"><w n="19.1">De</w> <w n="19.2">ce</w> <w n="19.3">récit</w> <w n="19.4">confus</w>, <w n="19.5">prophétique</w> <w n="19.6">mensonge</w>,</l>
							<l n="20" num="1.20"><w n="20.1">Cher</w> <w n="20.2">innocent</w>, <w n="20.3">tu</w> <w n="20.4">vins</w> <w n="20.5">saluer</w> <w n="20.6">mon</w> <w n="20.7">réveil</w>.</l>
							<l n="21" num="1.21">« <w n="21.1">Écoute</w> ! <w n="21.2">je</w> <w n="21.3">dormais</w> ; <w n="21.4">j</w>’<w n="21.5">avais</w> <w n="21.6">dit</w> <w n="21.7">ma</w> <w n="21.8">prière</w>.</l>
							<l n="22" num="1.22"><w n="22.1">J</w>’<w n="22.2">ai</w> <w n="22.3">vu</w> <w n="22.4">venir</w> <w n="22.5">vers</w> <w n="22.6">moi</w> <w n="22.7">deux</w> <w n="22.8">anges</w> : <w n="22.9">qu</w>’<w n="22.10">ils</w> <w n="22.11">sont</w> <w n="22.12">beaux</w> !</l>
							<l n="23" num="1.23"><w n="23.1">Je</w> <w n="23.2">voudrais</w> <w n="23.3">être</w> <w n="23.4">un</w> <w n="23.5">ange</w>. <w n="23.6">Ils</w> <w n="23.7">portent</w> <w n="23.8">des</w> <w n="23.9">flambeaux</w></l>
							<l n="24" num="1.24"><w n="24.1">Que</w> <w n="24.2">le</w> <w n="24.3">vent</w> <w n="24.4">n</w>’<w n="24.5">éteint</w> <w n="24.6">pas</w>. <w n="24.7">L</w>’<w n="24.8">un</w> <w n="24.9">d</w>’<w n="24.10">eux</w> <w n="24.11">a</w> <w n="24.12">dit</w> : « <w n="24.13">Mon</w> <w n="24.14">frère</w>,</l>
							<l n="25" num="1.25"><w n="25.1">Nous</w> <w n="25.2">venons</w> <w n="25.3">te</w> <w n="25.4">chercher</w> ; <w n="25.5">veux</w>-<w n="25.6">tu</w> <w n="25.7">nous</w> <w n="25.8">suivre</w> ? — <w n="25.9">Oh</w> ! <w n="25.10">oui</w>,</l>
							<l n="26" num="1.26"><w n="26.1">Je</w> <w n="26.2">veux</w> <w n="26.3">vous</w> <w n="26.4">suivre</w>… <w n="26.5">On</w> <w n="26.6">chante</w> ; <w n="26.7">est</w>-<w n="26.8">ce</w> <w n="26.9">fête</w> <w n="26.10">aujourd</w>’<w n="26.11">hui</w> ?</l>
							<l n="27" num="1.27">— <w n="27.1">C</w>’<w n="27.2">est</w> <w n="27.3">fête</w>. <w n="27.4">Viens</w> <w n="27.5">chercher</w> <w n="27.6">des</w> <w n="27.7">parures</w> <w n="27.8">nouvelles</w>. »</l>
							<l n="28" num="1.28"><w n="28.1">Et</w> <w n="28.2">mes</w> <w n="28.3">bras</w> <w n="28.4">s</w>’<w n="28.5">étendaient</w> <w n="28.6">pour</w> <w n="28.7">imiter</w> <w n="28.8">leurs</w> <w n="28.9">ailes</w> ;</l>
							<l n="29" num="1.29"><w n="29.1">Je</w> <w n="29.2">m</w>’<w n="29.3">envolais</w> <w n="29.4">comme</w> <w n="29.5">eux</w>, <w n="29.6">je</w> <w n="29.7">riais</w>… <w n="29.8">j</w>’<w n="29.9">avais</w> <w n="29.10">peur</w> !</l>
							<l n="30" num="1.30"><w n="30.1">Dieu</w> <w n="30.2">parlait</w> ! <w n="30.3">Dieu</w> <w n="30.4">pour</w> <w n="30.5">moi</w> <w n="30.6">montrait</w> <w n="30.7">une</w> <w n="30.8">couronne</w> :</l>
							<l n="31" num="1.31"><w n="31.1">C</w>’<w n="31.2">est</w> <w n="31.3">aux</w> <w n="31.4">enfants</w> <w n="31.5">chéris</w> <w n="31.6">que</w> <w n="31.7">sa</w> <w n="31.8">bonté</w> <w n="31.9">la</w> <w n="31.10">donne</w>,</l>
							<l n="32" num="1.32"><w n="32.1">Et</w> <w n="32.2">Dieu</w> <w n="32.3">me</w> <w n="32.4">l</w>’<w n="32.5">a</w> <w n="32.6">promise</w>, <w n="32.7">et</w> <w n="32.8">Dieu</w> <w n="32.9">n</w>’<w n="32.10">est</w> <w n="32.11">pas</w> <w n="32.12">trompeur</w>.</l>
							<l n="33" num="1.33"><w n="33.1">J</w>’<w n="33.2">irai</w> <w n="33.3">bientôt</w> <w n="33.4">le</w> <w n="33.5">voir</w> ; <w n="33.6">j</w>’<w n="33.7">irai</w> <w n="33.8">bientôt</w>… — <w n="33.9">Ma</w> <w n="33.10">vie</w> !</l>
							<l n="34" num="1.34"><w n="34.1">Où</w> <w n="34.2">donc</w> <w n="34.3">étais</w>-<w n="34.4">je</w> <w n="34.5">alors</w> ?… — <w n="34.6">Attends</w>… <w n="34.7">je</w> <w n="34.8">ne</w> <w n="34.9">sais</w> <w n="34.10">pas</w>…</l>
							<l n="35" num="1.35"><w n="35.1">Tu</w> <w n="35.2">pleurais</w> <w n="35.3">sur</w> <w n="35.4">la</w> <w n="35.5">terre</w>, <w n="35.6">où</w> <w n="35.7">je</w> <w n="35.8">t</w>’<w n="35.9">avais</w> <w n="35.10">suivie</w>.</l>
							<l n="36" num="1.36">— <w n="36.1">Tu</w> <w n="36.2">me</w> <w n="36.3">laissais</w> <w n="36.4">pleurer</w> ? —<w n="36.5">Je</w> <w n="36.6">t</w>’<w n="36.7">appelais</w> <w n="36.8">tout</w> <w n="36.9">bas</w>.</l>
							<l n="37" num="1.37">— <w n="37.1">Tu</w> <w n="37.2">voulais</w> <w n="37.3">me</w> <w n="37.4">revoir</w> ? — <w n="37.5">Je</w> <w n="37.6">ne</w> <w n="37.7">pouvais</w>, <w n="37.8">ma</w> <w n="37.9">mère</w>,</l>
							<l n="38" num="1.38"><w n="38.1">Dieu</w> <w n="38.2">ne</w> <w n="38.3">t</w>’<w n="38.4">appelait</w> <w n="38.5">pas</w>. » <w n="38.6">Un</w> <w n="38.7">froid</w> <w n="38.8">saisissement</w></l>
							<l n="39" num="1.39"><w n="39.1">Passa</w> <w n="39.2">jusqu</w>’<w n="39.3">à</w> <w n="39.4">mon</w> <w n="39.5">cœur</w>, <w n="39.6">et</w> <w n="39.7">cet</w> <w n="39.8">être</w> <w n="39.9">charmant</w>,</l>
							<l n="40" num="1.40"><w n="40.1">Calme</w>, <w n="40.2">rêvait</w> <w n="40.3">encor</w> <w n="40.4">sa</w> <w n="40.5">céleste</w> <w n="40.6">chimère</w>.</l>
							<l n="41" num="1.41"><w n="41.1">Dès</w> <w n="41.2">lors</w> <w n="41.3">un</w> <w n="41.4">mal</w> <w n="41.5">secret</w> <w n="41.6">répandit</w> <w n="41.7">sa</w> <w n="41.8">pâleur</w></l>
							<l n="42" num="1.42"><w n="42.1">Sur</w> <w n="42.2">ce</w> <w n="42.3">front</w> <w n="42.4">incliné</w>, <w n="42.5">qui</w> <w n="42.6">brûlait</w> <w n="42.7">sous</w> <w n="42.8">mes</w> <w n="42.9">larmes</w>.</l>
							<l n="43" num="1.43"><w n="43.1">Je</w> <w n="43.2">voyais</w> <w n="43.3">se</w> <w n="43.4">détruire</w> <w n="43.5">avant</w> <w n="43.6">moi</w> <w n="43.7">tant</w> <w n="43.8">de</w> <w n="43.9">charmes</w>,</l>
							<l n="44" num="1.44"><w n="44.1">Comme</w> <w n="44.2">un</w> <w n="44.3">frêle</w> <w n="44.4">bouton</w> <w n="44.5">s</w>’<w n="44.6">effeuille</w> <w n="44.7">avant</w> <w n="44.8">la</w> <w n="44.9">fleur</w>.</l>
							<l n="45" num="1.45"><w n="45.1">Je</w> <w n="45.2">le</w> <w n="45.3">voyais</w> ! <w n="45.4">et</w> <w n="45.5">moi</w>, <w n="45.6">rebelle</w>… <w n="45.7">suppliante</w>,</l>
							<l n="46" num="1.46"><w n="46.1">Je</w> <w n="46.2">disputais</w> <w n="46.3">un</w> <w n="46.4">ange</w> <w n="46.5">à</w> <w n="46.6">l</w>’<w n="46.7">immortel</w> <w n="46.8">séjour</w>.</l>
							<l n="47" num="1.47"><w n="47.1">Après</w> <w n="47.2">soixante</w> <w n="47.3">jours</w> <w n="47.4">de</w> <w n="47.5">deuil</w> <w n="47.6">et</w> <w n="47.7">d</w>’<w n="47.8">épouvante</w>,</l>
							<l n="48" num="1.48"><w n="48.1">Je</w> <w n="48.2">criais</w> <w n="48.3">vers</w> <w n="48.4">le</w> <w n="48.5">ciel</w> : « <w n="48.6">Encore</w>, <w n="48.7">encore</w> <w n="48.8">un</w> <w n="48.9">jour</w> ! »</l>
							<l n="49" num="1.49"><w n="49.1">Vainement</w>. <w n="49.2">J</w>’<w n="49.3">épuisai</w> <w n="49.4">mon</w> <w n="49.5">âme</w> <w n="49.6">tout</w> <w n="49.7">entière</w> ;</l>
							<l n="50" num="1.50"><w n="50.1">À</w> <w n="50.2">ce</w> <w n="50.3">berceau</w> <w n="50.4">plaintif</w> <w n="50.5">j</w>’<w n="50.6">enchaînai</w> <w n="50.7">mes</w> <w n="50.8">douleurs</w> ;</l>
							<l n="51" num="1.51"><w n="51.1">Repoussant</w> <w n="51.2">le</w> <w n="51.3">sommeil</w> <w n="51.4">et</w> <w n="51.5">m</w>’<w n="51.6">abreuvant</w> <w n="51.7">de</w> <w n="51.8">pleurs</w>,</l>
							<l n="52" num="1.52"><w n="52.1">Je</w> <w n="52.2">criais</w> <w n="52.3">à</w> <w n="52.4">la</w> <w n="52.5">mort</w> : « <w n="52.6">Frappe</w>-<w n="52.7">moi</w> <w n="52.8">la</w> <w n="52.9">première</w> ! »</l>
							<l n="53" num="1.53"><w n="53.1">Vainement</w>. <w n="53.2">Et</w> <w n="53.3">la</w> <w n="53.4">mort</w>, <w n="53.5">froide</w> <w n="53.6">dans</w> <w n="53.7">son</w> <w n="53.8">courroux</w>,</l>
							<l n="54" num="1.54"><w n="54.1">Irritée</w> <w n="54.2">à</w> <w n="54.3">l</w>’<w n="54.4">espoir</w> <w n="54.5">qu</w>’<w n="54.6">elle</w> <w n="54.7">accourait</w> <w n="54.8">éteindre</w></l>
							<l n="55" num="1.55"><w n="55.1">Et</w> <w n="55.2">moissonnant</w> <w n="55.3">l</w>’<w n="55.4">enfant</w>, <w n="55.5">ne</w> <w n="55.6">daigna</w> <w n="55.7">pas</w> <w n="55.8">atteindre</w></l>
							<l n="56" num="1.56"><space quantity="6" unit="char"></space><w n="56.1">La</w> <w n="56.2">mère</w> <w n="56.3">expirante</w> <w n="56.4">à</w> <w n="56.5">genoux</w>.</l>
							<l n="57" num="1.57"><w n="57.1">Et</w> <w n="57.2">quand</w> <w n="57.3">je</w> <w n="57.4">reparus</w> <w n="57.5">morne</w> <w n="57.6">et</w> <w n="57.7">découronnée</w>,</l>
							<l n="58" num="1.58"><w n="58.1">Après</w> <w n="58.2">avoir</w> <w n="58.3">longtemps</w> <w n="58.4">craint</w> <w n="58.5">jusqu</w>’<w n="58.6">à</w> <w n="58.7">l</w>’<w n="58.8">amitié</w>,</l>
							<l n="59" num="1.59"><w n="59.1">Cette</w> <w n="59.2">troupe</w> <w n="59.3">légère</w>, <w n="59.4">un</w> <w n="59.5">moment</w> <w n="59.6">consternée</w>,</l>
							<l n="60" num="1.60"><w n="60.1">Suspendit</w> <w n="60.2">ses</w> <w n="60.3">plaisirs</w>, <w n="60.4">et</w> <w n="60.5">sentit</w> <w n="60.6">la</w> <w n="60.7">pitié</w>.</l>
							<l n="61" num="1.61">« <w n="61.1">D</w>’<w n="61.2">où</w> <w n="61.3">viens</w>-<w n="61.4">tu</w> ? <w n="61.5">m</w>’<w n="61.6">a</w>-<w n="61.7">t</w>-<w n="61.8">on</w> <w n="61.9">dit</w>, <w n="61.10">et</w> <w n="61.11">quels</w> <w n="61.12">nuages</w> <w n="61.13">sombres</w></l>
							<l n="62" num="1.62"><space quantity="10" unit="char"></space><w n="62.1">Ont</w> <w n="62.2">environné</w> <w n="62.3">d</w>’<w n="62.4">ombres</w></l>
							<l n="63" num="1.63"><space quantity="10" unit="char"></space><w n="63.1">Tes</w> <w n="63.2">yeux</w> <w n="63.3">noyés</w> <w n="63.4">de</w> <w n="63.5">pleurs</w> ?</l>
							<l n="64" num="1.64"><space quantity="10" unit="char"></space><w n="64.1">Ton</w> <w n="64.2">soir</w> <w n="64.3">est</w> <w n="64.4">loin</w> <w n="64.5">encore</w>,</l>
							<l n="65" num="1.65"><space quantity="10" unit="char"></space><w n="65.1">Et</w> <w n="65.2">ta</w> <w n="65.3">paisible</w> <w n="65.4">aurore</w></l>
							<l n="66" num="1.66"><space quantity="10" unit="char"></space><w n="66.1">T</w>’<w n="66.2">avait</w> <w n="66.3">promis</w> <w n="66.4">des</w> <w n="66.5">fleurs</w>. »</l>
							<l n="67" num="1.67"><w n="67.1">Oui</w>, <w n="67.2">la</w> <w n="67.3">rose</w> <w n="67.4">a</w> <w n="67.5">brillé</w> <w n="67.6">sur</w> <w n="67.7">mon</w> <w n="67.8">riant</w> <w n="67.9">voyage</w> ;</l>
							<l n="68" num="1.68"><w n="68.1">Tous</w> <w n="68.2">les</w> <w n="68.3">yeux</w> <w n="68.4">l</w>’<w n="68.5">admiraient</w> <w n="68.6">dans</w> <w n="68.7">son</w> <w n="68.8">jeune</w> <w n="68.9">feuillage</w> ;</l>
							<l n="69" num="1.69"><w n="69.1">L</w>’<w n="69.2">étoile</w> <w n="69.3">du</w> <w n="69.4">matin</w> <w n="69.5">l</w>’<w n="69.6">aidait</w> <w n="69.7">à</w> <w n="69.8">s</w>’<w n="69.9">entr</w>’<w n="69.10">ouvrir</w>,</l>
							<l n="70" num="1.70"><w n="70.1">Et</w> <w n="70.2">l</w>’<w n="70.3">étoile</w> <w n="70.4">du</w> <w n="70.5">soir</w> <w n="70.6">la</w> <w n="70.7">regardait</w> <w n="70.8">mourir</w>.</l>
							<l n="71" num="1.71"><w n="71.1">Vers</w> <w n="71.2">la</w> <w n="71.3">terre</w> <w n="71.4">déjà</w> <w n="71.5">sa</w> <w n="71.6">tête</w> <w n="71.7">était</w> <w n="71.8">penchée</w> ;</l>
							<l n="72" num="1.72"><w n="72.1">L</w>’<w n="72.2">insecte</w> <w n="72.3">inaperçu</w> <w n="72.4">s</w>’<w n="72.5">y</w> <w n="72.6">creusait</w> <w n="72.7">un</w> <w n="72.8">tombeau</w> ;</l>
							<l n="73" num="1.73"><w n="73.1">Sa</w> <w n="73.2">feuille</w> <w n="73.3">murmurait</w>, <w n="73.4">en</w> <w n="73.5">tombant</w> <w n="73.6">desséchée</w> :</l>
							<l n="74" num="1.74">« <w n="74.1">Déjà</w> <w n="74.2">la</w> <w n="74.3">nuit</w> ! <w n="74.4">déjà</w>… <w n="74.5">Le</w> <w n="74.6">jour</w> <w n="74.7">était</w> <w n="74.8">si</w> <w n="74.9">beau</w> ! »</l>
						</lg>
					</div></body></text></TEI>