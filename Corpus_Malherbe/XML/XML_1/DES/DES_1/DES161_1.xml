<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>9618 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>9618 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies</title>
					<author>Marceline Desbordes-Valmore</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>A. Boulland, libraire-éditeur</publisher>
						<date when="1830">1830</date>
					</imprint>
					<extent>9618 verstome second</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-12-20" who="RR">Insertion des corrections métriques dans le corps du texte.</change>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
				<change when="2018-11-02" who="RR">Correction dans le poème "Dors, ma Mère".</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part"> POÉSIES INÉDITES <lb></lb>MÉLANGES</head><div type="poem" key="DES161">
						<head type="main">LE REGARD</head>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">Laisse</w> ! <w n="1.2">j</w>’<w n="1.3">ai</w> <w n="1.4">vu</w> <w n="1.5">tes</w> <w n="1.6">yeux</w>, <w n="1.7">dans</w> <w n="1.8">leur</w> <w n="1.9">douce</w> <w n="1.10">lumière</w>,</l>
							<l n="2" num="1.2"><w n="2.1">S</w>’<w n="2.2">attacher</w> <w n="2.3">sur</w> <w n="2.4">des</w> <w n="2.5">yeux</w> <w n="2.6">qui</w> <w n="2.7">donnent</w> <w n="2.8">le</w> <w n="2.9">bonheur</w> ;</l>
							<l n="3" num="1.3"><w n="3.1">Et</w> <w n="3.2">je</w> <w n="3.3">ne</w> <w n="3.4">sais</w> <w n="3.5">quel</w> <w n="3.6">deuil</w> <w n="3.7">accable</w> <w n="3.8">ma</w> <w n="3.9">paupière</w>,</l>
							<l n="4" num="1.4"><w n="4.1">Je</w> <w n="4.2">ne</w> <w n="4.3">sais</w> <w n="4.4">quelle</w> <w n="4.5">nuit</w> <w n="4.6">environne</w> <w n="4.7">mon</w> <w n="4.8">cœur</w>.</l>
							<l n="5" num="1.5"><w n="5.1">On</w> <w n="5.2">dirait</w> <w n="5.3">que</w>, <w n="5.4">pressé</w> <w n="5.5">par</w> <w n="5.6">une</w> <w n="5.7">main</w> <w n="5.8">cruelle</w>,</l>
							<l n="6" num="1.6"><w n="6.1">Il</w> <w n="6.2">ne</w> <w n="6.3">se</w> <w n="6.4">débat</w> <w n="6.5">plus</w> <w n="6.6">sous</w> <w n="6.7">son</w> <w n="6.8">arrêt</w> <w n="6.9">de</w> <w n="6.10">mort</w>.</l>
							<l n="7" num="1.7"><w n="7.1">Laisse</w> ! <w n="7.2">il</w> <w n="7.3">faut</w> <w n="7.4">nous</w> <w n="7.5">ravir</w> <w n="7.6">une</w> <w n="7.7">erreur</w> <w n="7.8">mutuelle</w> ;</l>
							<l n="8" num="1.8"><w n="8.1">Ce</w> <w n="8.2">cœur</w> <w n="8.3">n</w>’<w n="8.4">est</w> <w n="8.5">plus</w> <w n="8.6">à</w> <w n="8.7">toi</w>… <w n="8.8">je</w> <w n="8.9">te</w> <w n="8.10">sauve</w> <w n="8.11">un</w> <w n="8.12">remord</w>.</l>
							<l n="9" num="1.9"><w n="9.1">Seule</w>, <w n="9.2">avec</w> <w n="9.3">désespoir</w>, <w n="9.4">j</w>’<w n="9.5">y</w> <w n="9.6">suis</w> <w n="9.7">redescendue</w> ;</l>
							<l n="10" num="1.10"><w n="10.1">Ton</w> <w n="10.2">portrait</w> <w n="10.3">déchiré</w> <w n="10.4">s</w>’<w n="10.5">y</w> <w n="10.6">baignait</w> <w n="10.7">dans</w> <w n="10.8">les</w> <w n="10.9">pleurs</w>.</l>
							<l n="11" num="1.11"><w n="11.1">Quoi</w> ! <w n="11.2">cette</w> <w n="11.3">image</w> <w n="11.4">aimante</w> <w n="11.5">est</w> <w n="11.6">à</w> <w n="11.7">jamais</w> <w n="11.8">perdue</w> !</l>
							<l n="12" num="1.12"><w n="12.1">Qui</w> <w n="12.2">donc</w> <w n="12.3">pouvait</w> <w n="12.4">l</w>’<w n="12.5">atteindre</w> <w n="12.6">et</w> <w n="12.7">changer</w> <w n="12.8">ses</w> <w n="12.9">couleurs</w> ?</l>
							<l n="13" num="1.13"><w n="13.1">Toi</w> <w n="13.2">seul</w> ! <w n="13.3">Je</w> <w n="13.4">voudrais</w> <w n="13.5">croire</w> <w n="13.6">à</w> <w n="13.7">ta</w> <w n="13.8">voix</w> <w n="13.9">généreuse</w>,</l>
							<l n="14" num="1.14"><w n="14.1">Mais</w> <w n="14.2">j</w>’<w n="14.3">ai</w> <w n="14.4">vu</w>… <w n="14.5">Qu</w>’<w n="14.6">ils</w> <w n="14.7">sont</w> <w n="14.8">beaux</w> <w n="14.9">les</w> <w n="14.10">yeux</w> <w n="14.11">qui</w> <w n="14.12">te</w> <w n="14.13">parlaient</w> !</l>
							<l n="15" num="1.15"><w n="15.1">J</w>’<w n="15.2">avais</w> <w n="15.3">donc</w> <w n="15.4">oublié</w> <w n="15.5">que</w> <w n="15.6">je</w> <w n="15.7">suis</w> <w n="15.8">malheureuse</w> ?</l>
							<l n="16" num="1.16"><w n="16.1">Va</w> ! <w n="16.2">je</w> <w n="16.3">n</w>’<w n="16.4">oublîrai</w> <w n="16.5">plus</w> <w n="16.6">qu</w>’<w n="16.7">ils</w> <w n="16.8">me</w> <w n="16.9">le</w> <w n="16.10">rappelaient</w>.</l>
						</lg>
						<lg n="2">
							<l n="17" num="2.1"><w n="17.1">Toi</w>, <w n="17.2">de</w> <w n="17.3">quoi</w> <w n="17.4">pleures</w>-<w n="17.5">tu</w> ? <w n="17.6">Je</w> <w n="17.7">n</w>’<w n="17.8">entends</w> <w n="17.9">pas</w> <w n="17.10">tes</w> <w n="17.11">larmes</w> :</l>
							<l n="18" num="2.2"><w n="18.1">J</w>’<w n="18.2">y</w> <w n="18.3">vois</w> <w n="18.4">briller</w> <w n="18.5">ces</w> <w n="18.6">yeux</w> <w n="18.7">dont</w> <w n="18.8">tu</w> <w n="18.9">m</w>’<w n="18.10">as</w> <w n="18.11">dit</w> <w n="18.12">les</w> <w n="18.13">charmes</w> ;</l>
							<l n="19" num="2.3"><w n="19.1">Laisse</w>-<w n="19.2">moi</w> <w n="19.3">les</w> <w n="19.4">haïr</w>, <w n="19.5">mais</w> <w n="19.6">de</w> <w n="19.7">loin</w>, <w n="19.8">mais</w> <w n="19.9">tout</w> <w n="19.10">bas</w>.</l>
							<l n="20" num="2.4"><w n="20.1">Quels</w> <w n="20.2">yeux</w> !… <w n="20.3">Ils</w> <w n="20.4">sont</w> <w n="20.5">partout</w>. <w n="20.6">Oh</w> ! <w n="20.7">ne</w> <w n="20.8">me</w> <w n="20.9">parle</w> <w n="20.10">pas</w> !</l>
							<l n="21" num="2.5"><w n="21.1">Va</w>-<w n="21.2">t</w>’<w n="21.3">en</w> ! <w n="21.4">Va</w> ! <w n="21.5">sois</w> <w n="21.6">heureux</w>, <w n="21.7">je</w> <w n="21.8">le</w> <w n="21.9">veux</w>, <w n="21.10">je</w> <w n="21.11">t</w>’<w n="21.12">en</w> <w n="21.13">prie</w> !</l>
							<l n="22" num="2.6"><w n="22.1">Tes</w> <w n="22.2">pleurs</w> <w n="22.3">me</w> <w n="22.4">font</w> <w n="22.5">mourir</w>… <w n="22.6">Je</w> <w n="22.7">crois</w> <w n="22.8">que</w> <w n="22.9">je</w> <w n="22.10">t</w>’<w n="22.11">aimais</w> !</l>
							<l n="23" num="2.7"><w n="23.1">Va</w>-<w n="23.2">t</w>’<w n="23.3">en</w> ! <w n="23.4">Je</w> <w n="23.5">suis</w> <w n="23.6">jalouse</w>, <w n="23.7">et</w> <w n="23.8">je</w> <w n="23.9">fus</w> <w n="23.10">trop</w> <w n="23.11">chérie</w></l>
							<l n="24" num="2.8"><w n="24.1">Pour</w> <w n="24.2">oser</w> <w n="24.3">te</w> <w n="24.4">le</w> <w n="24.5">dire</w> <w n="24.6">et</w> <w n="24.7">te</w> <w n="24.8">revoir</w> <w n="24.9">jamais</w> !</l>
						</lg>
					</div></body></text></TEI>