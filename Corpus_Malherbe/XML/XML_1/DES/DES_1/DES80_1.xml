<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>9618 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>9618 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies</title>
					<author>Marceline Desbordes-Valmore</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>A. Boulland, libraire-éditeur</publisher>
						<date when="1830">1830</date>
					</imprint>
					<extent>9618 verstome second</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-12-20" who="RR">Insertion des corrections métriques dans le corps du texte.</change>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
				<change when="2018-11-02" who="RR">Correction dans le poème "Dors, ma Mère".</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ROMANCES</head><div type="poem" key="DES80">
						<head type="main">LE CHIEN D’OLIVIER</head>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">Pour</w> <w n="1.2">trouver</w> <w n="1.3">le</w> <w n="1.4">bonheur</w>, <w n="1.5">je</w> <w n="1.6">me</w> <w n="1.7">ferai</w> <w n="1.8">bergère</w> :</l>
							<l n="2" num="1.2"><w n="2.1">Le</w> <w n="2.2">bonheur</w> <w n="2.3">est</w> <w n="2.4">aux</w> <w n="2.5">champs</w>, <w n="2.6">s</w>’<w n="2.7">il</w> <w n="2.8">existe</w> <w n="2.9">pour</w> <w n="2.10">moi</w> !</l>
							<l n="3" num="1.3"><w n="3.1">Oui</w>, <w n="3.2">du</w> <w n="3.3">temps</w>, <w n="3.4">au</w> <w n="3.5">hameau</w>, <w n="3.6">la</w> <w n="3.7">course</w> <w n="3.8">est</w> <w n="3.9">plus</w> <w n="3.10">légère</w> ;</l>
							<l n="4" num="1.4"><w n="4.1">La</w> <w n="4.2">veillée</w> <w n="4.3">est</w> <w n="4.4">paisible</w>, <w n="4.5">et</w> <w n="4.6">la</w> <w n="4.7">nuit</w> <w n="4.8">sans</w> <w n="4.9">effroi</w>.</l>
							<l n="5" num="1.5"><w n="5.1">Le</w> <w n="5.2">laboureur</w>, <w n="5.3">couché</w> <w n="5.4">sous</w> <w n="5.5">son</w> <w n="5.6">toit</w> <w n="5.7">de</w> <w n="5.8">fougère</w>,</l>
							<l n="6" num="1.6"><w n="6.1">Ne</w> <w n="6.2">dormirait</w> <w n="6.3">pas</w> <w n="6.4">mieux</w> <w n="6.5">sur</w> <w n="6.6">l</w>’<w n="6.7">oreiller</w> <w n="6.8">du</w> <w n="6.9">roi</w>.</l>
						</lg>
						<lg n="2">
							<l n="7" num="2.1"><w n="7.1">D</w>’<w n="7.2">un</w> <w n="7.3">simple</w> <w n="7.4">ajustement</w> <w n="7.5">j</w>’<w n="7.6">ai</w> <w n="7.7">déjà</w> <w n="7.8">fait</w> <w n="7.9">l</w>’<w n="7.10">emplette</w>.</l>
							<l n="8" num="2.2"><w n="8.1">On</w> <w n="8.2">ressemble</w> <w n="8.3">au</w> <w n="8.4">Plaisir</w>, <w n="8.5">sous</w> <w n="8.6">un</w> <w n="8.7">chapeau</w> <w n="8.8">de</w> <w n="8.9">fleurs</w> ;</l>
							<l n="9" num="2.3"><w n="9.1">Les</w> <w n="9.2">prés</w> <w n="9.3">m</w>’<w n="9.4">en</w> <w n="9.5">offriront</w> <w n="9.6">pour</w> <w n="9.7">garnir</w> <w n="9.8">ma</w> <w n="9.9">houlette</w> ;</l>
							<l n="10" num="2.4"><w n="10.1">On</w> <w n="10.2">n</w>’<w n="10.3">y</w> <w n="10.4">forcera</w> <w n="10.5">pas</w> <w n="10.6">mon</w> <w n="10.7">choix</w> <w n="10.8">pour</w> <w n="10.9">leurs</w> <w n="10.10">couleurs</w> ;</l>
							<l n="11" num="2.5"><w n="11.1">J</w>’<w n="11.2">y</w> <w n="11.3">mêlerai</w> <w n="11.4">le</w> <w n="11.5">lis</w> <w n="11.6">à</w> <w n="11.7">l</w>’<w n="11.8">humble</w> <w n="11.9">violette</w>,</l>
							<l n="12" num="2.6"><w n="12.1">Sans</w> <w n="12.2">crainte</w> <w n="12.3">qu</w>’<w n="12.4">un</w> <w n="12.5">bouquet</w> <w n="12.6">me</w> <w n="12.7">prépare</w> <w n="12.8">des</w> <w n="12.9">pleurs</w>.</l>
						</lg>
						<lg n="3">
							<l n="13" num="3.1"><w n="13.1">Des</w> <w n="13.2">moutons</w>, <w n="13.3">un</w> <w n="13.4">bélier</w>, <w n="13.5">deux</w> <w n="13.6">agneaux</w> <w n="13.7">et</w> <w n="13.8">leur</w> <w n="13.9">mère</w>,</l>
							<l n="14" num="3.2"><w n="14.1">Composeront</w> <w n="14.2">ma</w> <w n="14.3">cour</w>, <w n="14.4">mon</w> <w n="14.5">empire</w> <w n="14.6">et</w> <w n="14.7">mon</w> <w n="14.8">bien</w>.</l>
							<l n="15" num="3.3"><w n="15.1">L</w>’<w n="15.2">écho</w> <w n="15.3">me</w> <w n="15.4">distraira</w> <w n="15.5">d</w>’<w n="15.6">une</w> <w n="15.7">douce</w> <w n="15.8">chimère</w></l>
							<l n="16" num="3.4"><w n="16.1">Que</w> <w n="16.2">je</w> <w n="16.3">veux</w> <w n="16.4">oublier</w>, <w n="16.5">aussi</w> <w n="16.6">je</w> <w n="16.7">n</w>’<w n="16.8">en</w> <w n="16.9">dis</w> <w n="16.10">rien</w> ;</l>
							<l n="17" num="3.5"><w n="17.1">Et</w> <w n="17.2">pour</w> <w n="17.3">me</w> <w n="17.4">suivre</w> <w n="17.5">aux</w> <w n="17.6">bois</w>, <w n="17.7">où</w> <w n="17.8">je</w> <w n="17.9">suis</w> <w n="17.10">étrangère</w>,</l>
							<l n="18" num="3.6"><w n="18.1">Il</w> <w n="18.2">me</w> <w n="18.3">faudrait</w> <w n="18.4">encore</w>… <w n="18.5">il</w> <w n="18.6">me</w> <w n="18.7">faudrait</w> <w n="18.8">un</w> <w n="18.9">chien</w>.</l>
						</lg>
						<lg n="4">
							<l n="19" num="4.1"><w n="19.1">Que</w> <w n="19.2">le</w> <w n="19.3">chien</w> <w n="19.4">d</w>’<w n="19.5">Olivier</w> <w n="19.6">paraît</w> <w n="19.7">tendre</w> <w n="19.8">et</w> <w n="19.9">fidèle</w> !</l>
							<l n="20" num="4.2"><w n="20.1">Sous</w> <w n="20.2">sa</w> <w n="20.3">garde</w> <w n="20.4">un</w> <w n="20.5">troupeau</w> <w n="20.6">bondirait</w> <w n="20.7">sans</w> <w n="20.8">danger</w>.</l>
							<l n="21" num="4.3"><w n="21.1">Mais</w> <w n="21.2">des</w> <w n="21.3">maîtres</w> <w n="21.4">son</w> <w n="21.5">maître</w> <w n="21.6">est</w>, <w n="21.7">dit</w>-<w n="21.8">on</w>, <w n="21.9">le</w> <w n="21.10">modèle</w> ;</l>
							<l n="22" num="4.4"><w n="22.1">À</w> <w n="22.2">le</w> <w n="22.3">quitter</w> <w n="22.4">pour</w> <w n="22.5">moi</w> <w n="22.6">je</w> <w n="22.7">n</w>’<w n="22.8">ose</w> <w n="22.9">l</w>’<w n="22.10">engager</w>.</l>
							<l n="23" num="4.5"><w n="23.1">Ah</w> ! <w n="23.2">pour</w> <w n="23.3">ne</w> <w n="23.4">pas</w> <w n="23.5">détruire</w> <w n="23.6">une</w> <w n="23.7">amitié</w> <w n="23.8">si</w> <w n="23.9">belle</w>,</l>
							<l n="24" num="4.6"><w n="24.1">Je</w> <w n="24.2">voudrais</w> <w n="24.3">qu</w>’<w n="24.4">Olivier</w> <w n="24.5">se</w> <w n="24.6">fît</w> <w n="24.7">aussi</w> <w n="24.8">berger</w>.</l>
						</lg>
					</div></body></text></TEI>