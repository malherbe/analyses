<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>9618 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>9618 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies</title>
					<author>Marceline Desbordes-Valmore</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>A. Boulland, libraire-éditeur</publisher>
						<date when="1830">1830</date>
					</imprint>
					<extent>9618 verstome second</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-12-20" who="RR">Insertion des corrections métriques dans le corps du texte.</change>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
				<change when="2018-11-02" who="RR">Correction dans le poème "Dors, ma Mère".</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ÉLÉGIES</head><div type="poem" key="DES36">
						<head type="main">LA SÉPARATION</head>
						<lg n="1">
							<l n="1" num="1.1"><space quantity="6" unit="char"></space><w n="1.1">Il</w> <w n="1.2">est</w> <w n="1.3">fini</w> <w n="1.4">ce</w> <w n="1.5">long</w> <w n="1.6">supplice</w> !</l>
							<l n="2" num="1.2"><space quantity="2" unit="char"></space><w n="2.1">Je</w> <w n="2.2">t</w>’<w n="2.3">ai</w> <w n="2.4">rendu</w> <w n="2.5">tes</w> <w n="2.6">serments</w> <w n="2.7">et</w> <w n="2.8">ta</w> <w n="2.9">foi</w>,</l>
							<l n="3" num="1.3"><space quantity="10" unit="char"></space><w n="3.1">Je</w> <w n="3.2">n</w>’<w n="3.3">ai</w> <w n="3.4">plus</w> <w n="3.5">rien</w> <w n="3.6">à</w> <w n="3.7">toi</w>.</l>
							<l n="4" num="1.4"><w n="4.1">Quel</w> <w n="4.2">douloureux</w> <w n="4.3">effort</w> ! <w n="4.4">quel</w> <w n="4.5">entier</w> <w n="4.6">sacrifice</w> !</l>
							<l n="5" num="1.5"><space quantity="2" unit="char"></space><w n="5.1">Mais</w>, <w n="5.2">en</w> <w n="5.3">brisant</w> <w n="5.4">les</w> <w n="5.5">plus</w> <w n="5.6">aimables</w> <w n="5.7">nœuds</w>,</l>
							<l n="6" num="1.6"><w n="6.1">Nos</w> <w n="6.2">cœurs</w> <w n="6.3">toujours</w> <w n="6.4">unis</w> <w n="6.5">semblent</w> <w n="6.6">toujours</w> <w n="6.7">s</w>’<w n="6.8">entendre</w> ;</l>
							<l n="7" num="1.7"><w n="7.1">On</w> <w n="7.2">ne</w> <w n="7.3">saura</w> <w n="7.4">jamais</w> <w n="7.5">lequel</w> <w n="7.6">fut</w> <w n="7.7">le</w> <w n="7.8">plus</w> <w n="7.9">tendre</w>,</l>
							<l n="8" num="1.8"><space quantity="10" unit="char"></space><w n="8.1">Ou</w> <w n="8.2">le</w> <w n="8.3">plus</w> <w n="8.4">malheureux</w>.</l>
						</lg>
						<lg n="2">
							<l n="9" num="2.1"><space quantity="2" unit="char"></space><w n="9.1">À</w> <w n="9.2">t</w>’<w n="9.3">oublier</w> <w n="9.4">c</w>’<w n="9.5">est</w> <w n="9.6">l</w>’<w n="9.7">honneur</w> <w n="9.8">qui</w> <w n="9.9">m</w>’<w n="9.10">engage</w>,</l>
							<l n="10" num="2.2"><space quantity="2" unit="char"></space><w n="10.1">Tu</w> <w n="10.2">t</w>’<w n="10.3">y</w> <w n="10.4">soumets</w>, <w n="10.5">je</w> <w n="10.6">n</w>’<w n="10.7">ai</w> <w n="10.8">plus</w> <w n="10.9">d</w>’<w n="10.10">autre</w> <w n="10.11">loi</w>.</l>
							<l n="11" num="2.3"><w n="11.1">Ô</w> <w n="11.2">toi</w> <w n="11.3">qui</w> <w n="11.4">m</w>’<w n="11.5">as</w> <w n="11.6">donné</w> <w n="11.7">l</w>’<w n="11.8">exemple</w> <w n="11.9">du</w> <w n="11.10">courage</w>,</l>
							<l n="12" num="2.4"><space quantity="10" unit="char"></space><w n="12.1">Aimais</w>-<w n="12.2">tu</w> <w n="12.3">moins</w> <w n="12.4">que</w> <w n="12.5">moi</w> ?</l>
							<l n="13" num="2.5"><space quantity="2" unit="char"></space><w n="13.1">Va</w> ! <w n="13.2">je</w> <w n="13.3">te</w> <w n="13.4">plains</w> <w n="13.5">autant</w> <w n="13.6">que</w> <w n="13.7">je</w> <w n="13.8">t</w>’<w n="13.9">adore</w> ;</l>
							<l n="14" num="2.6"><space quantity="2" unit="char"></space><w n="14.1">Je</w> <w n="14.2">t</w>’<w n="14.3">ai</w> <w n="14.4">permis</w> <w n="14.5">de</w> <w n="14.6">trahir</w> <w n="14.7">tes</w> <w n="14.8">amours</w>,</l>
							<l n="15" num="2.7"><w n="15.1">Mais</w> <w n="15.2">moi</w>, <w n="15.3">pour</w> <w n="15.4">t</w>’<w n="15.5">adorer</w>, <w n="15.6">je</w> <w n="15.7">serai</w> <w n="15.8">libre</w> <w n="15.9">encore</w> ;</l>
							<l n="16" num="2.8"><space quantity="10" unit="char"></space><w n="16.1">Je</w> <w n="16.2">veux</w> <w n="16.3">l</w>’<w n="16.4">être</w> <w n="16.5">toujours</w>.</l>
						</lg>
						<lg n="3">
							<l n="17" num="3.1"><space quantity="2" unit="char"></space><w n="17.1">Je</w> <w n="17.2">l</w>’<w n="17.3">ai</w> <w n="17.4">promis</w>, <w n="17.5">je</w> <w n="17.6">vivrai</w> <w n="17.7">pour</w> <w n="17.8">ta</w> <w n="17.9">gloire</w>.</l>
							<l n="18" num="3.2"><space quantity="6" unit="char"></space><w n="18.1">Cher</w> <w n="18.2">objet</w> <w n="18.3">de</w> <w n="18.4">mon</w> <w n="18.5">souvenir</w>,</l>
							<l n="19" num="3.3"><space quantity="6" unit="char"></space><w n="19.1">Sois</w> <w n="19.2">le</w> <w n="19.3">charme</w> <w n="19.4">de</w> <w n="19.5">ma</w> <w n="19.6">mémoire</w>,</l>
							<l n="20" num="3.4"><space quantity="6" unit="char"></space><w n="20.1">Et</w> <w n="20.2">l</w>’<w n="20.3">espoir</w> <w n="20.4">de</w> <w n="20.5">mon</w> <w n="20.6">avenir</w>.</l>
							<l n="21" num="3.5"><space quantity="6" unit="char"></space><w n="21.1">Si</w> <w n="21.2">jamais</w> <w n="21.3">dans</w> <w n="21.4">ma</w> <w n="21.5">solitude</w>,</l>
							<l n="22" num="3.6"><space quantity="6" unit="char"></space><w n="22.1">Ton</w> <w n="22.2">nom</w> <w n="22.3">pour</w> <w n="22.4">toujours</w> <w n="22.5">adoré</w>,</l>
							<l n="23" num="3.7"><space quantity="6" unit="char"></space><w n="23.1">Vient</w> <w n="23.2">frapper</w> <w n="23.3">mon</w> <w n="23.4">cœur</w> <w n="23.5">déchiré</w></l>
							<l n="24" num="3.8"><w n="24.1">Qu</w>’<w n="24.2">il</w> <w n="24.3">adoucisse</w> <w n="24.4">au</w> <w n="24.5">moins</w> <w n="24.6">ma</w> <w n="24.7">tendre</w> <w n="24.8">inquiétude</w> !</l>
							<l n="25" num="3.9"><space quantity="6" unit="char"></space><w n="25.1">Que</w> <w n="25.2">l</w>’<w n="25.3">on</w> <w n="25.4">me</w> <w n="25.5">dise</w> : <w n="25.6">Il</w> <w n="25.7">est</w> <w n="25.8">heureux</w>,</l>
							<l n="26" num="3.10"><space quantity="2" unit="char"></space><w n="26.1">Oui</w>, <w n="26.2">sois</w> <w n="26.3">heureux</w>, <w n="26.4">ou</w> <w n="26.5">du</w> <w n="26.6">moins</w> <w n="26.7">plus</w> <w n="26.8">paisible</w>,</l>
							<l n="27" num="3.11"><space quantity="2" unit="char"></space><w n="27.1">Malgré</w> <w n="27.2">l</w>’<w n="27.3">Amour</w>, <w n="27.4">et</w> <w n="27.5">le</w> <w n="27.6">sort</w> <w n="27.7">inflexible</w></l>
							<l n="28" num="3.12"><space quantity="10" unit="char"></space><w n="28.1">Qui</w> <w n="28.2">m</w>’<w n="28.3">enlève</w> <w n="28.4">à</w> <w n="28.5">tes</w> <w n="28.6">vœux</w>.</l>
						</lg>
						<lg n="4">
							<l n="29" num="4.1"><space quantity="6" unit="char"></space><w n="29.1">Adieu</w>,… <w n="29.2">mon</w> <w n="29.3">âme</w> <w n="29.4">se</w> <w n="29.5">déchire</w> !</l>
							<l n="30" num="4.2"><w n="30.1">Ce</w> <w n="30.2">mot</w> <w n="30.3">que</w>, <w n="30.4">dans</w> <w n="30.5">mes</w> <w n="30.6">pleurs</w>, <w n="30.7">je</w> <w n="30.8">n</w>’<w n="30.9">ai</w> <w n="30.10">pu</w> <w n="30.11">prononcer</w>,</l>
							<l n="31" num="4.3"><w n="31.1">Adieu</w> ! <w n="31.2">ma</w> <w n="31.3">bouche</w> <w n="31.4">encor</w> <w n="31.5">n</w>’<w n="31.6">oserait</w> <w n="31.7">te</w> <w n="31.8">le</w> <w n="31.9">dire</w>,</l>
							<l n="32" num="4.4"><space quantity="6" unit="char"></space><w n="32.1">Et</w> <w n="32.2">ma</w> <w n="32.3">main</w> <w n="32.4">vient</w> <w n="32.5">de</w> <w n="32.6">le</w> <w n="32.7">tracer</w>.</l>
						</lg>
					</div></body></text></TEI>