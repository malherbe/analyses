<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Ïambes et poèmes</title>
				<title type="medium">Édition électronique</title>
				<author key="BRB">
					<name>
						<forename>Auguste</forename>
						<surname>BARBIER</surname>
					</name>
					<date from="1805" to="1882">1805-1882</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement métrique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4203 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BRB_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Iambes et poèmes</title>
						<author>Auguste BARBIER</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L947</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Iambes et poèmes</title>
								<author>Auguste BARBIER</author>
								<idno type="URL">https://archive.org/details/iambesetpomes03barbgoog</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Paul Masgana</publisher>
									<date when="1840">1840</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1831">1831</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">IL PIANTO</head><div type="poem" key="BRB35">
					<head type="main">L’ADIEU</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Ah</w> ! <w n="1.2">Quel</w> <w n="1.3">que</w> <w n="1.4">soit</w> <w n="1.5">le</w> <w n="1.6">deuil</w> <w n="1.7">jeté</w> <w n="1.8">sur</w> <w n="1.9">cette</w> <w n="1.10">terre</w></l>
						<l n="2" num="1.2"><w n="2.1">Qui</w> <w n="2.2">par</w> <w n="2.3">deux</w> <w n="2.4">fois</w> <w n="2.5">du</w> <w n="2.6">monde</w> <w n="2.7">a</w> <w n="2.8">changé</w> <w n="2.9">le</w> <w n="2.10">destin</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Quels</w> <w n="3.2">que</w> <w n="3.3">soient</w> <w n="3.4">ses</w> <w n="3.5">malheurs</w> <w n="3.6">et</w> <w n="3.7">sa</w> <w n="3.8">longue</w> <w n="3.9">misère</w>,</l>
						<l n="4" num="1.4"><w n="4.1">On</w> <w n="4.2">ne</w> <w n="4.3">peut</w> <w n="4.4">la</w> <w n="4.5">quitter</w> <w n="4.6">sans</w> <w n="4.7">peine</w> <w n="4.8">et</w> <w n="4.9">sans</w> <w n="4.10">chagrin</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Ainsi</w>, <w n="5.2">près</w> <w n="5.3">de</w> <w n="5.4">sortir</w> <w n="5.5">du</w> <w n="5.6">céleste</w> <w n="5.7">jardin</w>,</l>
						<l n="6" num="2.2"><w n="6.1">Je</w> <w n="6.2">me</w> <w n="6.3">retourne</w> <w n="6.4">encor</w> <w n="6.5">sur</w> <w n="6.6">les</w> <w n="6.7">cimes</w> <w n="6.8">hautaines</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Pour</w> <w n="7.2">contempler</w> <w n="7.3">de</w> <w n="7.4">là</w> <w n="7.5">son</w> <w n="7.6">horizon</w> <w n="7.7">divin</w></l>
						<l n="8" num="2.4"><w n="8.1">Et</w> <w n="8.2">longtemps</w> <w n="8.3">m</w>’<w n="8.4">enivrer</w> <w n="8.5">de</w> <w n="8.6">ses</w> <w n="8.7">grâces</w> <w n="8.8">lointaines</w> :</l>
						<l n="9" num="2.5"><w n="9.1">Et</w> <w n="9.2">puis</w> <w n="9.3">le</w> <w n="9.4">froid</w> <w n="9.5">me</w> <w n="9.6">prend</w> <w n="9.7">et</w> <w n="9.8">me</w> <w n="9.9">glace</w> <w n="9.10">les</w> <w n="9.11">veines</w></l>
						<l n="10" num="2.6"><w n="10.1">Et</w> <w n="10.2">tout</w> <w n="10.3">mon</w> <w n="10.4">cœur</w> <w n="10.5">soupire</w>, <w n="10.6">oh</w> ! <w n="10.7">Comme</w> <w n="10.8">si</w> <w n="10.9">j</w>’<w n="10.10">avais</w>,</l>
						<l n="11" num="2.7"><w n="11.1">Aux</w> <w n="11.2">champs</w> <w n="11.3">de</w> <w n="11.4">l</w>’<w n="11.5">Italie</w> <w n="11.6">et</w> <w n="11.7">dans</w> <w n="11.8">ses</w> <w n="11.9">larges</w> <w n="11.10">plaines</w>,</l>
						<l n="12" num="2.8"><w n="12.1">De</w> <w n="12.2">mes</w> <w n="12.3">jours</w> <w n="12.4">effeuillé</w> <w n="12.5">le</w> <w n="12.6">rameau</w> <w n="12.7">le</w> <w n="12.8">plus</w> <w n="12.9">frais</w>,</l>
					</lg>
					<lg n="3">
						<l n="13" num="3.1"><w n="13.1">Et</w> <w n="13.2">sur</w> <w n="13.3">le</w> <w n="13.4">sein</w> <w n="13.5">vermeil</w> <w n="13.6">de</w> <w n="13.7">la</w> <w n="13.8">brune</w> <w n="13.9">déesse</w></l>
						<l n="14" num="3.2"><w n="14.1">Épuisé</w> <w n="14.2">pour</w> <w n="14.3">toujours</w> <w n="14.4">ma</w> <w n="14.5">vie</w> <w n="14.6">et</w> <w n="14.7">ma</w> <w n="14.8">jeunesse</w>.</l>
					</lg>
					<ab type="star"></ab>
					<lg n="4">
						<l n="15" num="4.1"><w n="15.1">Divine</w> <w n="15.2">Juliette</w> <w n="15.3">au</w> <w n="15.4">cercueil</w> <w n="15.5">étendue</w>,</l>
						<l n="16" num="4.2"><w n="16.1">Toi</w> <w n="16.2">qui</w> <w n="16.3">n</w>’<w n="16.4">es</w> <w n="16.5">qu</w>’<w n="16.6">endormie</w> <w n="16.7">et</w> <w n="16.8">que</w> <w n="16.9">l</w>’<w n="16.10">on</w> <w n="16.11">croit</w> <w n="16.12">perdue</w>,</l>
						<l n="17" num="4.3"><w n="17.1">Italie</w>, <w n="17.2">ô</w> <w n="17.3">beauté</w> ! <w n="17.4">Si</w> <w n="17.5">malgré</w> <w n="17.6">ta</w> <w n="17.7">pâleur</w>,</l>
						<l n="18" num="4.4"><w n="18.1">Tes</w> <w n="18.2">membres</w> <w n="18.3">ont</w> <w n="18.4">encor</w> <w n="18.5">gardé</w> <w n="18.6">de</w> <w n="18.7">la</w> <w n="18.8">chaleur</w> ;</l>
						<l n="19" num="4.5"><w n="19.1">Si</w> <w n="19.2">du</w> <w n="19.3">sang</w> <w n="19.4">généreux</w> <w n="19.5">coule</w> <w n="19.6">encor</w> <w n="19.7">dans</w> <w n="19.8">ta</w> <w n="19.9">veine</w> ;</l>
						<l n="20" num="4.6"><w n="20.1">Si</w> <w n="20.2">le</w> <w n="20.3">monstre</w> <w n="20.4">qui</w> <w n="20.5">semble</w> <w n="20.6">avoir</w> <w n="20.7">bu</w> <w n="20.8">ton</w> <w n="20.9">haleine</w>,</l>
						<l n="21" num="4.7"><w n="21.1">La</w> <w n="21.2">mort</w>, <w n="21.3">planant</w> <w n="21.4">sur</w> <w n="21.5">toi</w> <w n="21.6">comme</w> <w n="21.7">un</w> <w n="21.8">heureux</w> <w n="21.9">amant</w>,</l>
						<l n="22" num="4.8"><w n="22.1">Pour</w> <w n="22.2">toujours</w> <w n="22.3">ne</w> <w n="22.4">t</w>’<w n="22.5">a</w> <w n="22.6">pas</w> <w n="22.7">clouée</w> <w n="22.8">au</w> <w n="22.9">monument</w> ;</l>
						<l n="23" num="4.9"><w n="23.1">Si</w> <w n="23.2">tu</w> <w n="23.3">n</w>’<w n="23.4">es</w> <w n="23.5">pas</w> <w n="23.6">enfin</w> <w n="23.7">son</w> <w n="23.8">entière</w> <w n="23.9">conquête</w> ;</l>
						<l n="24" num="4.10"><w n="24.1">Alors</w> <w n="24.2">quelque</w> <w n="24.3">beau</w> <w n="24.4">jour</w> <w n="24.5">tu</w> <w n="24.6">lèveras</w> <w n="24.7">la</w> <w n="24.8">tête</w>,</l>
						<l n="25" num="4.11"><w n="25.1">Et</w>, <w n="25.2">privés</w> <w n="25.3">bien</w> <w n="25.4">longtemps</w> <w n="25.5">du</w> <w n="25.6">soleil</w>, <w n="25.7">tes</w> <w n="25.8">grands</w> <w n="25.9">yeux</w></l>
						<l n="26" num="4.12"><w n="26.1">S</w>’<w n="26.2">ouvriront</w> <w n="26.3">pour</w> <w n="26.4">revoir</w> <w n="26.5">le</w> <w n="26.6">pur</w> <w n="26.7">éclat</w> <w n="26.8">des</w> <w n="26.9">cieux</w> :</l>
						<l n="27" num="4.13"><w n="27.1">Puis</w> <w n="27.2">ton</w> <w n="27.3">corps</w> <w n="27.4">ranimé</w> <w n="27.5">par</w> <w n="27.6">la</w> <w n="27.7">chaude</w> <w n="27.8">lumière</w>,</l>
						<l n="28" num="4.14"><w n="28.1">Se</w> <w n="28.2">dressera</w> <w n="28.3">tout</w> <w n="28.4">droit</w> <w n="28.5">sur</w> <w n="28.6">la</w> <w n="28.7">funèbre</w> <w n="28.8">pierre</w>.</l>
						<l n="29" num="4.15"><w n="29.1">Alors</w>, <w n="29.2">être</w> <w n="29.3">plaintif</w>, <w n="29.4">ne</w> <w n="29.5">pouvant</w> <w n="29.6">marcher</w> <w n="29.7">seul</w>,</l>
						<l n="30" num="4.16"><w n="30.1">Et</w> <w n="30.2">tout</w> <w n="30.3">embarrassé</w> <w n="30.4">des</w> <w n="30.5">longs</w> <w n="30.6">plis</w> <w n="30.7">du</w> <w n="30.8">linceul</w>,</l>
						<l n="31" num="4.17"><w n="31.1">Tu</w> <w n="31.2">chercheras</w> <w n="31.3">dans</w> <w n="31.4">l</w>’<w n="31.5">ombre</w> <w n="31.6">une</w> <w n="31.7">épaule</w> <w n="31.8">adorée</w> ;</l>
						<l n="32" num="4.18"><w n="32.1">Et</w>, <w n="32.2">les</w> <w n="32.3">deux</w> <w n="32.4">pieds</w> <w n="32.5">sortis</w> <w n="32.6">de</w> <w n="32.7">la</w> <w n="32.8">tombe</w> <w n="32.9">sacrée</w>,</l>
						<l n="33" num="4.19"><w n="33.1">Tu</w> <w n="33.2">voudras</w> <w n="33.3">un</w> <w n="33.4">soutien</w> <w n="33.5">pour</w> <w n="33.6">faire</w> <w n="33.7">quelques</w> <w n="33.8">pas</w>.</l>
						<l n="34" num="4.20"><w n="34.1">Alors</w> <w n="34.2">à</w> <w n="34.3">l</w>’<w n="34.4">étranger</w>, <w n="34.5">oh</w> ! <w n="34.6">Ne</w> <w n="34.7">tends</w> <w n="34.8">point</w> <w n="34.9">les</w> <w n="34.10">bras</w> :</l>
						<l n="35" num="4.21"><w n="35.1">Car</w> <w n="35.2">ce</w> <w n="35.3">qui</w> <w n="35.4">n</w>’<w n="35.5">est</w> <w n="35.6">pas</w> <w n="35.7">toi</w>, <w n="35.8">ni</w> <w n="35.9">la</w> <w n="35.10">Grèce</w> <w n="35.11">ta</w> <w n="35.12">mère</w>,</l>
						<l n="36" num="4.22"><w n="36.1">Ce</w> <w n="36.2">qui</w> <w n="36.3">ne</w> <w n="36.4">parle</w> <w n="36.5">point</w> <w n="36.6">ton</w> <w n="36.7">langage</w> <w n="36.8">sur</w> <w n="36.9">terre</w>,</l>
						<l n="37" num="4.23"><w n="37.1">Et</w> <w n="37.2">ce</w> <w n="37.3">qui</w> <w n="37.4">ne</w> <w n="37.5">vit</w> <w n="37.6">pas</w> <w n="37.7">sous</w> <w n="37.8">ton</w> <w n="37.9">ciel</w> <w n="37.10">enchanteur</w>,</l>
						<l n="38" num="4.24"><w n="38.1">Bien</w> <w n="38.2">souvent</w> <w n="38.3">est</w> <w n="38.4">barbare</w> <w n="38.5">et</w> <w n="38.6">frappé</w> <w n="38.7">de</w> <w n="38.8">laideur</w>.</l>
						<l n="39" num="4.25"><w n="39.1">L</w>’<w n="39.2">étranger</w> <w n="39.3">ne</w> <w n="39.4">viendrait</w> <w n="39.5">sur</w> <w n="39.6">ta</w> <w n="39.7">couche</w> <w n="39.8">de</w> <w n="39.9">lave</w>,</l>
						<l n="40" num="4.26"><w n="40.1">Que</w> <w n="40.2">pour</w> <w n="40.3">te</w> <w n="40.4">garrotter</w> <w n="40.5">comme</w> <w n="40.6">une</w> <w n="40.7">blanche</w> <w n="40.8">esclave</w> ;</l>
						<l n="41" num="4.27"><w n="41.1">L</w>’<w n="41.2">étranger</w> <w n="41.3">corrompu</w>, <w n="41.4">s</w>’<w n="41.5">il</w> <w n="41.6">te</w> <w n="41.7">donnait</w> <w n="41.8">la</w> <w n="41.9">main</w>,</l>
						<l n="42" num="4.28"><w n="42.1">Avilirait</w> <w n="42.2">ton</w> <w n="42.3">front</w> <w n="42.4">et</w> <w n="42.5">flétrirait</w> <w n="42.6">ton</w> <w n="42.7">sein</w>.</l>
						<l n="43" num="4.29"><w n="43.1">Belle</w> <w n="43.2">ressuscitée</w>, <w n="43.3">ô</w> <w n="43.4">princesse</w> <w n="43.5">chérie</w>,</l>
						<l n="44" num="4.30"><w n="44.1">N</w>’<w n="44.2">arrête</w> <w n="44.3">tes</w> <w n="44.4">yeux</w> <w n="44.5">noirs</w> <w n="44.6">qu</w>’<w n="44.7">au</w> <w n="44.8">sol</w> <w n="44.9">de</w> <w n="44.10">la</w> <w n="44.11">patrie</w> ;</l>
						<l n="45" num="4.31"><w n="45.1">Dans</w> <w n="45.2">tes</w> <w n="45.3">fils</w> <w n="45.4">réunis</w> <w n="45.5">cherche</w> <w n="45.6">ton</w> <w n="45.7">Roméo</w>,</l>
						<l n="46" num="4.32"><w n="46.1">Noble</w> <w n="46.2">et</w> <w n="46.3">douce</w> <w n="46.4">Italie</w>, <w n="46.5">ô</w> <w n="46.6">mère</w> <w n="46.7">du</w> <w n="46.8">vrai</w> <w n="46.9">beau</w> !</l>
					</lg>
				</div></body></text></TEI>