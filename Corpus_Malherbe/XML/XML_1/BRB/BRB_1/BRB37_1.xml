<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Ïambes et poèmes</title>
				<title type="medium">Édition électronique</title>
				<author key="BRB">
					<name>
						<forename>Auguste</forename>
						<surname>BARBIER</surname>
					</name>
					<date from="1805" to="1882">1805-1882</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement métrique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4203 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BRB_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Iambes et poèmes</title>
						<author>Auguste BARBIER</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L947</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Iambes et poèmes</title>
								<author>Auguste BARBIER</author>
								<idno type="URL">https://archive.org/details/iambesetpomes03barbgoog</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Paul Masgana</publisher>
									<date when="1840">1840</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1831">1831</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LAZARE</head><div type="poem" key="BRB37">
					<head type="main">LONDRES</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">C</w>’<w n="1.2">est</w> <w n="1.3">un</w> <w n="1.4">espace</w> <w n="1.5">immense</w> <w n="1.6">et</w> <w n="1.7">d</w>’<w n="1.8">une</w> <w n="1.9">longueur</w> <w n="1.10">telle</w></l>
						<l n="2" num="1.2"><w n="2.1">Qu</w>’<w n="2.2">il</w> <w n="2.3">faut</w> <w n="2.4">pour</w> <w n="2.5">le</w> <w n="2.6">franchir</w> <w n="2.7">un</w> <w n="2.8">jour</w> <w n="2.9">à</w> <w n="2.10">l</w>’<w n="2.11">hirondelle</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Et</w> <w n="3.2">ce</w> <w n="3.3">n</w>’<w n="3.4">est</w>, <w n="3.5">bien</w> <w n="3.6">au</w> <w n="3.7">loin</w>, <w n="3.8">que</w> <w n="3.9">des</w> <w n="3.10">entassements</w></l>
						<l n="4" num="1.4"><w n="4.1">De</w> <w n="4.2">maisons</w>, <w n="4.3">de</w> <w n="4.4">palais</w>, <w n="4.5">et</w> <w n="4.6">de</w> <w n="4.7">hauts</w> <w n="4.8">monuments</w>,</l>
						<l n="5" num="1.5"><w n="5.1">Plantés</w> <w n="5.2">là</w> <w n="5.3">par</w> <w n="5.4">le</w> <w n="5.5">temps</w> <w n="5.6">sans</w> <w n="5.7">trop</w> <w n="5.8">de</w> <w n="5.9">symétrie</w> ;</l>
						<l n="6" num="1.6"><w n="6.1">De</w> <w n="6.2">noirs</w> <w n="6.3">et</w> <w n="6.4">longs</w> <w n="6.5">tuyaux</w>, <w n="6.6">clochers</w> <w n="6.7">de</w> <w n="6.8">l</w>’<w n="6.9">industrie</w>,</l>
						<l n="7" num="1.7"><w n="7.1">Ouvrant</w> <w n="7.2">toujours</w> <w n="7.3">la</w> <w n="7.4">gueule</w>, <w n="7.5">et</w> <w n="7.6">de</w> <w n="7.7">leurs</w> <w n="7.8">ventres</w> <w n="7.9">chauds</w></l>
						<l n="8" num="1.8"><w n="8.1">Exhalant</w> <w n="8.2">dans</w> <w n="8.3">les</w> <w n="8.4">airs</w> <w n="8.5">la</w> <w n="8.6">fumée</w> <w n="8.7">à</w> <w n="8.8">longs</w> <w n="8.9">flots</w> ;</l>
						<l n="9" num="1.9"><w n="9.1">De</w> <w n="9.2">vastes</w> <w n="9.3">dômes</w> <w n="9.4">blancs</w> <w n="9.5">et</w> <w n="9.6">des</w> <w n="9.7">flèches</w> <w n="9.8">gothiques</w></l>
						<l n="10" num="1.10"><w n="10.1">Flottant</w> <w n="10.2">dans</w> <w n="10.3">la</w> <w n="10.4">vapeur</w> <w n="10.5">sur</w> <w n="10.6">des</w> <w n="10.7">monceaux</w> <w n="10.8">de</w> <w n="10.9">briques</w> ;</l>
						<l n="11" num="1.11"><w n="11.1">Un</w> <w n="11.2">fleuve</w> <w n="11.3">inabordable</w>, <w n="11.4">un</w> <w n="11.5">fleuve</w> <w n="11.6">tout</w> <w n="11.7">houleux</w></l>
						<l n="12" num="1.12"><w n="12.1">Roulant</w> <w n="12.2">sa</w> <w n="12.3">vase</w> <w n="12.4">noire</w> <w n="12.5">en</w> <w n="12.6">détours</w> <w n="12.7">sinueux</w>,</l>
						<l n="13" num="1.13"><w n="13.1">Et</w> <w n="13.2">rappelant</w> <w n="13.3">l</w>’<w n="13.4">effroi</w> <w n="13.5">des</w> <w n="13.6">ondes</w> <w n="13.7">infernales</w> ;</l>
						<l n="14" num="1.14"><w n="14.1">De</w> <w n="14.2">gigantesques</w> <w n="14.3">ponts</w> <w n="14.4">aux</w> <w n="14.5">piles</w> <w n="14.6">colossales</w>,</l>
						<l n="15" num="1.15"><w n="15.1">Comme</w> <w n="15.2">l</w>’<w n="15.3">homme</w> <w n="15.4">de</w> <w n="15.5">Rhode</w>, <w n="15.6">à</w> <w n="15.7">travers</w> <w n="15.8">leurs</w> <w n="15.9">arceaux</w></l>
						<l n="16" num="1.16"><w n="16.1">Pouvant</w> <w n="16.2">laisser</w> <w n="16.3">passer</w> <w n="16.4">des</w> <w n="16.5">milliers</w> <w n="16.6">de</w> <w n="16.7">vaisseaux</w> ;</l>
						<l n="17" num="1.17"><w n="17.1">Une</w> <w n="17.2">marée</w> <w n="17.3">infecte</w> <w n="17.4">et</w> <w n="17.5">toujours</w> <w n="17.6">avec</w> <w n="17.7">l</w>’<w n="17.8">onde</w></l>
						<l n="18" num="1.18"><w n="18.1">Apportant</w>, <w n="18.2">remportant</w> <w n="18.3">les</w> <w n="18.4">richesses</w> <w n="18.5">du</w> <w n="18.6">monde</w> ;</l>
						<l n="19" num="1.19"><w n="19.1">Des</w> <w n="19.2">chantiers</w> <w n="19.3">en</w> <w n="19.4">travail</w>, <w n="19.5">des</w> <w n="19.6">magasins</w> <w n="19.7">ouverts</w>,</l>
						<l n="20" num="1.20"><w n="20.1">Capables</w> <w n="20.2">de</w> <w n="20.3">tenir</w> <w n="20.4">dans</w> <w n="20.5">leurs</w> <w n="20.6">flancs</w> <w n="20.7">l</w>’<w n="20.8">univers</w> ;</l>
						<l n="21" num="1.21"><w n="21.1">Puis</w> <w n="21.2">un</w> <w n="21.3">ciel</w> <w n="21.4">tourmenté</w>, <w n="21.5">nuage</w> <w n="21.6">sur</w> <w n="21.7">nuage</w> ;</l>
						<l n="22" num="1.22"><w n="22.1">Le</w> <w n="22.2">soleil</w>, <w n="22.3">comme</w> <w n="22.4">un</w> <w n="22.5">mort</w>, <w n="22.6">le</w> <w n="22.7">drap</w> <w n="22.8">sur</w> <w n="22.9">le</w> <w n="22.10">visage</w>,</l>
						<l n="23" num="1.23"><w n="23.1">Ou</w>, <w n="23.2">parfois</w>, <w n="23.3">dans</w> <w n="23.4">les</w> <w n="23.5">flots</w> <w n="23.6">d</w>’<w n="23.7">un</w> <w n="23.8">air</w> <w n="23.9">empoisonné</w></l>
						<l n="24" num="1.24"><w n="24.1">Montrant</w> <w n="24.2">comme</w> <w n="24.3">un</w> <w n="24.4">mineur</w> <w n="24.5">son</w> <w n="24.6">front</w> <w n="24.7">tout</w> <w n="24.8">charbonné</w> ;</l>
						<l n="25" num="1.25"><w n="25.1">Enfin</w>, <w n="25.2">dans</w> <w n="25.3">un</w> <w n="25.4">amas</w> <w n="25.5">de</w> <w n="25.6">choses</w>, <w n="25.7">sombre</w>, <w n="25.8">immense</w>,</l>
						<l n="26" num="1.26"><w n="26.1">Un</w> <w n="26.2">peuple</w> <w n="26.3">noir</w>, <w n="26.4">vivant</w> <w n="26.5">et</w> <w n="26.6">mourant</w> <w n="26.7">en</w> <w n="26.8">silence</w>,</l>
						<l n="27" num="1.27"><w n="27.1">Des</w> <w n="27.2">êtres</w> <w n="27.3">par</w> <w n="27.4">milliers</w> <w n="27.5">suivant</w> <w n="27.6">l</w>’<w n="27.7">instinct</w> <w n="27.8">fatal</w>,</l>
						<l n="28" num="1.28"><w n="28.1">Et</w> <w n="28.2">courant</w> <w n="28.3">après</w> <w n="28.4">l</w>’<w n="28.5">or</w> <w n="28.6">par</w> <w n="28.7">le</w> <w n="28.8">bien</w> <w n="28.9">et</w> <w n="28.10">le</w> <w n="28.11">mal</w>.</l>
					</lg>
				</div></body></text></TEI>