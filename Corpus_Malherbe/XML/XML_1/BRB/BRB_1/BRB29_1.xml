<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Ïambes et poèmes</title>
				<title type="medium">Édition électronique</title>
				<author key="BRB">
					<name>
						<forename>Auguste</forename>
						<surname>BARBIER</surname>
					</name>
					<date from="1805" to="1882">1805-1882</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement métrique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4203 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BRB_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Iambes et poèmes</title>
						<author>Auguste BARBIER</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L947</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Iambes et poèmes</title>
								<author>Auguste BARBIER</author>
								<idno type="URL">https://archive.org/details/iambesetpomes03barbgoog</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Paul Masgana</publisher>
									<date when="1840">1840</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1831">1831</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">IL PIANTO</head><div type="poem" key="BRB29">
					<head type="main">CIMAROSA</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Chantre</w> <w n="1.2">mélodieux</w> <w n="1.3">né</w> <w n="1.4">sous</w> <w n="1.5">le</w> <w n="1.6">plus</w> <w n="1.7">beau</w> <w n="1.8">ciel</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Au</w> <w n="2.2">nom</w> <w n="2.3">doux</w> <w n="2.4">et</w> <w n="2.5">fleuri</w> <w n="2.6">comme</w> <w n="2.7">une</w> <w n="2.8">lyre</w> <w n="2.9">antique</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Léger</w> <w n="3.2">napolitain</w>, <w n="3.3">dont</w> <w n="3.4">la</w> <w n="3.5">folle</w> <w n="3.6">musique</w></l>
						<l n="4" num="1.4"><w n="4.1">A</w> <w n="4.2">frotté</w>, <w n="4.3">tout</w> <w n="4.4">enfant</w>, <w n="4.5">les</w> <w n="4.6">deux</w> <w n="4.7">lèvres</w> <w n="4.8">de</w> <w n="4.9">miel</w>,</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Ô</w> <w n="5.2">bon</w> <w n="5.3">Cimarosa</w> ! <w n="5.4">Nul</w> <w n="5.5">poëte</w> <w n="5.6">immortel</w>,</l>
						<l n="6" num="2.2"><w n="6.1">Nul</w> <w n="6.2">peintre</w>, <w n="6.3">comme</w> <w n="6.4">toi</w>, <w n="6.5">dans</w> <w n="6.6">sa</w> <w n="6.7">verve</w> <w n="6.8">comique</w>,</l>
						<l n="7" num="2.3"><w n="7.1">N</w>’<w n="7.2">égaya</w> <w n="7.3">des</w> <w n="7.4">humains</w> <w n="7.5">la</w> <w n="7.6">face</w> <w n="7.7">léthargique</w></l>
						<l n="8" num="2.4"><w n="8.1">D</w>’<w n="8.2">un</w> <w n="8.3">rayon</w> <w n="8.4">de</w> <w n="8.5">gaîté</w> <w n="8.6">plus</w> <w n="8.7">franc</w> <w n="8.8">et</w> <w n="8.9">naturel</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Et</w> <w n="9.2">pourtant</w> <w n="9.3">tu</w> <w n="9.4">gardas</w> <w n="9.5">à</w> <w n="9.6">travers</w> <w n="9.7">ton</w> <w n="9.8">délire</w>,</l>
						<l n="10" num="3.2"><w n="10.1">Sous</w> <w n="10.2">les</w> <w n="10.3">grelots</w> <w n="10.4">du</w> <w n="10.5">fou</w>, <w n="10.6">sous</w> <w n="10.7">le</w> <w n="10.8">masque</w> <w n="10.9">du</w> <w n="10.10">rire</w>,</l>
						<l n="11" num="3.3"><w n="11.1">Un</w> <w n="11.2">cœur</w> <w n="11.3">toujours</w> <w n="11.4">sensible</w> <w n="11.5">et</w> <w n="11.6">plein</w> <w n="11.7">de</w> <w n="11.8">dignité</w> ;</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1"><w n="12.1">Oui</w>, <w n="12.2">ton</w> <w n="12.3">âme</w> <w n="12.4">fut</w> <w n="12.5">belle</w>, <w n="12.6">ainsi</w> <w n="12.7">que</w> <w n="12.8">ton</w> <w n="12.9">génie</w> ;</l>
						<l n="13" num="4.2"><w n="13.1">Elle</w> <w n="13.2">ne</w> <w n="13.3">faillit</w> <w n="13.4">point</w> <w n="13.5">devant</w> <w n="13.6">la</w> <w n="13.7">tyrannie</w>,</l>
						<l n="14" num="4.3"><w n="14.1">Et</w> <w n="14.2">chanta</w> <w n="14.3">dans</w> <w n="14.4">les</w> <w n="14.5">fers</w> <w n="14.6">l</w>’<w n="14.7">hymne</w> <w n="14.8">de</w> <w n="14.9">liberté</w>.</l>
					</lg>
				</div></body></text></TEI>