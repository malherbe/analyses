<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Ïambes et poèmes</title>
				<title type="medium">Édition électronique</title>
				<author key="BRB">
					<name>
						<forename>Auguste</forename>
						<surname>BARBIER</surname>
					</name>
					<date from="1805" to="1882">1805-1882</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement métrique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4203 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BRB_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Iambes et poèmes</title>
						<author>Auguste BARBIER</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L947</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Iambes et poèmes</title>
								<author>Auguste BARBIER</author>
								<idno type="URL">https://archive.org/details/iambesetpomes03barbgoog</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Paul Masgana</publisher>
									<date when="1840">1840</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1831">1831</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LAZARE</head><div type="poem" key="BRB50">
					<head type="main">SHAKSPEARE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Hélas</w> ! <w n="1.2">Hélas</w> ! <w n="1.3">Faut</w>-<w n="1.4">il</w> <w n="1.5">qu</w>’<w n="1.6">une</w> <w n="1.7">haleine</w> <w n="1.8">glacée</w></l>
						<l n="2" num="1.2"><w n="2.1">Ternisse</w> <w n="2.2">le</w> <w n="2.3">front</w> <w n="2.4">pur</w> <w n="2.5">des</w> <w n="2.6">maîtres</w> <w n="2.7">glorieux</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Et</w> <w n="3.2">faut</w>-<w n="3.3">il</w> <w n="3.4">qu</w>’<w n="3.5">ici</w>-<w n="3.6">bas</w> <w n="3.7">les</w> <w n="3.8">dieux</w> <w n="3.9">de</w> <w n="3.10">la</w> <w n="3.11">pensée</w>,</l>
						<l n="4" num="1.4"><w n="4.1">S</w>’<w n="4.2">en</w> <w n="4.3">aillent</w> <w n="4.4">tristement</w> <w n="4.5">comme</w> <w n="4.6">les</w> <w n="4.7">autres</w> <w n="4.8">dieux</w> !</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">De</w> <w n="5.2">Shakspeare</w> <w n="5.3">aujourd</w>’<w n="5.4">hui</w> <w n="5.5">les</w> <w n="5.6">sublimes</w> <w n="5.7">merveilles</w></l>
						<l n="6" num="2.2"><w n="6.1">Vont</w> <w n="6.2">frapper</w> <w n="6.3">sans</w> <w n="6.4">émoi</w> <w n="6.5">les</w> <w n="6.6">humaines</w> <w n="6.7">oreilles</w> ;</l>
						<l n="7" num="2.3"><w n="7.1">Dans</w> <w n="7.2">ses</w> <w n="7.3">temples</w> <w n="7.4">déserts</w> <w n="7.5">et</w> <w n="7.6">vides</w> <w n="7.7">de</w> <w n="7.8">clameurs</w>,</l>
						<l n="8" num="2.4"><w n="8.1">À</w> <w n="8.2">peine</w> <w n="8.3">trouve</w>-<w n="8.4">t</w>-<w n="8.5">on</w> <w n="8.6">quelques</w> <w n="8.7">adorateurs</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Albion</w> <w n="9.2">perd</w> <w n="9.3">le</w> <w n="9.4">goût</w> <w n="9.5">de</w> <w n="9.6">ses</w> <w n="9.7">divins</w> <w n="9.8">symboles</w>,</l>
						<l n="10" num="3.2"><w n="10.1">Hors</w> <w n="10.2">du</w> <w n="10.3">vrai</w> <w n="10.4">par</w> <w n="10.5">l</w>’<w n="10.6">ennui</w> <w n="10.7">les</w> <w n="10.8">esprits</w> <w n="10.9">égarés</w></l>
						<l n="11" num="3.3"><w n="11.1">Tombent</w> <w n="11.2">dans</w> <w n="11.3">le</w> <w n="11.4">barbare</w>, <w n="11.5">et</w> <w n="11.6">les</w> <w n="11.7">choses</w> <w n="11.8">frivoles</w></l>
						<l n="12" num="3.4"><w n="12.1">Parlent</w> <w n="12.2">plus</w> <w n="12.3">haut</w> <w n="12.4">aux</w> <w n="12.5">cœurs</w> <w n="12.6">que</w> <w n="12.7">les</w> <w n="12.8">chants</w> <w n="12.9">inspirés</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Et</w> <w n="13.2">pourtant</w> <w n="13.3">quel</w> <w n="13.4">titan</w> <w n="13.5">à</w> <w n="13.6">la</w> <w n="13.7">céleste</w> <w n="13.8">flamme</w></l>
						<l n="14" num="4.2"><w n="14.1">Alluma</w> <w n="14.2">comme</w> <w n="14.3">lui</w> <w n="14.4">plus</w> <w n="14.5">de</w> <w n="14.6">limons</w> <w n="14.7">divers</w> ?</l>
						<l n="15" num="4.3"><w n="15.1">Quel</w> <w n="15.2">plongeur</w>, <w n="15.3">entr</w>’<w n="15.4">ouvrant</w> <w n="15.5">du</w> <w n="15.6">sein</w> <w n="15.7">les</w> <w n="15.8">flots</w> <w n="15.9">amers</w>,</l>
						<l n="16" num="4.4"><w n="16.1">Descendit</w> <w n="16.2">plus</w> <w n="16.3">avant</w> <w n="16.4">dans</w> <w n="16.5">les</w> <w n="16.6">gouffres</w> <w n="16.7">de</w> <w n="16.8">l</w>’<w n="16.9">âme</w> ?</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Quel</w> <w n="17.2">poëte</w> <w n="17.3">vit</w> <w n="17.4">mieux</w> <w n="17.5">au</w> <w n="17.6">fond</w> <w n="17.7">du</w> <w n="17.8">cœur</w> <w n="17.9">humain</w></l>
						<l n="18" num="5.2"><w n="18.1">Les</w> <w n="18.2">sombres</w> <w n="18.3">passions</w>, <w n="18.4">ces</w> <w n="18.5">reptiles</w> <w n="18.6">énormes</w>,</l>
						<l n="19" num="5.3"><w n="19.1">Dragons</w> <w n="19.2">impétueux</w>, <w n="19.3">monstres</w> <w n="19.4">de</w> <w n="19.5">mille</w> <w n="19.6">formes</w>,</l>
						<l n="20" num="5.4"><w n="20.1">Se</w> <w n="20.2">tordre</w> <w n="20.3">et</w> <w n="20.4">s</w>’<w n="20.5">agiter</w> ? <w n="20.6">Quel</w> <w n="20.7">homme</w> <w n="20.8">de</w> <w n="20.9">sa</w> <w n="20.10">main</w></l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Sut</w>, <w n="21.2">comme</w> <w n="21.3">lui</w>, <w n="21.4">les</w> <w n="21.5">prendre</w> <w n="21.6">au</w> <w n="21.7">fort</w> <w n="21.8">de</w> <w n="21.9">leurs</w> <w n="21.10">ténèbres</w>,</l>
						<l n="22" num="6.2"><w n="22.1">Et</w>, <w n="22.2">découvrant</w> <w n="22.3">leur</w> <w n="22.4">face</w> <w n="22.5">à</w> <w n="22.6">la</w> <w n="22.7">pure</w> <w n="22.8">clarté</w>,</l>
						<l n="23" num="6.3"><w n="23.1">Faire</w> <w n="23.2">comme</w> <w n="23.3">un</w> <w n="23.4">Hercule</w> <w n="23.5">au</w> <w n="23.6">monde</w> <w n="23.7">épouvanté</w></l>
						<l n="24" num="6.4"><w n="24.1">Entendre</w> <w n="24.2">le</w> <w n="24.3">concert</w> <w n="24.4">de</w> <w n="24.5">leurs</w> <w n="24.6">plaintes</w> <w n="24.7">funèbres</w> ?</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">Ah</w> ! <w n="25.2">Toujours</w> <w n="25.3">verra</w>-<w n="25.4">t</w>-<w n="25.5">on</w>, <w n="25.6">d</w>’<w n="25.7">un</w> <w n="25.8">pied</w> <w n="25.9">lourd</w> <w n="25.10">et</w> <w n="25.11">brutal</w>,</l>
						<l n="26" num="7.2"><w n="26.1">Sur</w> <w n="26.2">son</w> <w n="26.3">trône</w> <w n="26.4">bondir</w> <w n="26.5">la</w> <w n="26.6">stupide</w> <w n="26.7">matière</w>,</l>
						<l n="27" num="7.3"><w n="27.1">Et</w> <w n="27.2">l</w>’<w n="27.3">anglais</w> <w n="27.4">préférer</w> <w n="27.5">une</w> <w n="27.6">fausse</w> <w n="27.7">lumière</w></l>
						<l n="28" num="7.4"><w n="28.1">Aux</w> <w n="28.2">sublimes</w> <w n="28.3">reflets</w> <w n="28.4">de</w> <w n="28.5">l</w>’<w n="28.6">astre</w> <w n="28.7">impérial</w> ?</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1"><w n="29.1">C</w>’<w n="29.2">en</w> <w n="29.3">est</w>-<w n="29.4">il</w> <w n="29.5">fait</w> <w n="29.6">du</w> <w n="29.7">beau</w> <w n="29.8">sur</w> <w n="29.9">cette</w> <w n="29.10">terre</w> <w n="29.11">sombre</w>,</l>
						<l n="30" num="8.2"><w n="30.1">Et</w> <w n="30.2">doit</w>-<w n="30.3">il</w> <w n="30.4">sous</w> <w n="30.5">la</w> <w n="30.6">nuit</w> <w n="30.7">se</w> <w n="30.8">perdre</w> <w n="30.9">entièrement</w> ?</l>
						<l n="31" num="8.3"><w n="31.1">Non</w>, <w n="31.2">non</w>, <w n="31.3">la</w> <w n="31.4">nuit</w> <w n="31.5">peut</w> <w n="31.6">bien</w> <w n="31.7">jeter</w> <w n="31.8">au</w> <w n="31.9">ciel</w> <w n="31.10">son</w> <w n="31.11">ombre</w>,</l>
						<l n="32" num="8.4"><w n="32.1">Elle</w> <w n="32.2">n</w>’<w n="32.3">éteindra</w> <w n="32.4">pas</w> <w n="32.5">les</w> <w n="32.6">feux</w> <w n="32.7">du</w> <w n="32.8">firmament</w>.</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1"><w n="33.1">Ô</w> <w n="33.2">toi</w> <w n="33.3">qui</w> <w n="33.4">fus</w> <w n="33.5">l</w>’<w n="33.6">enfant</w> <w n="33.7">de</w> <w n="33.8">la</w> <w n="33.9">grande</w> <w n="33.10">nature</w>,</l>
						<l n="34" num="9.2"><w n="34.1">Robuste</w> <w n="34.2">nourrisson</w> <w n="34.3">dans</w> <w n="34.4">ses</w> <w n="34.5">deux</w> <w n="34.6">bras</w> <w n="34.7">porté</w> ;</l>
						<l n="35" num="9.3"><w n="35.1">Toi</w> <w n="35.2">qui</w>, <w n="35.3">mordant</w> <w n="35.4">le</w> <w n="35.5">bout</w> <w n="35.6">de</w> <w n="35.7">sa</w> <w n="35.8">mamelle</w> <w n="35.9">pure</w>,</l>
						<l n="36" num="9.4"><w n="36.1">D</w>’<w n="36.2">une</w> <w n="36.3">lèvre</w> <w n="36.4">puissante</w> <w n="36.5">y</w> <w n="36.6">bus</w> <w n="36.7">la</w> <w n="36.8">vérité</w> ;</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1"><w n="37.1">Tout</w> <w n="37.2">ce</w> <w n="37.3">que</w> <w n="37.4">ta</w> <w n="37.5">pensée</w> <w n="37.6">a</w> <w n="37.7">touché</w> <w n="37.8">de</w> <w n="37.9">son</w> <w n="37.10">aile</w>,</l>
						<l n="38" num="10.2"><w n="38.1">Tout</w> <w n="38.2">ce</w> <w n="38.3">que</w> <w n="38.4">ton</w> <w n="38.5">regard</w> <w n="38.6">a</w> <w n="38.7">fait</w> <w n="38.8">naître</w> <w n="38.9">ici</w>-<w n="38.10">bas</w>,</l>
						<l n="39" num="10.3"><w n="39.1">Tout</w> <w n="39.2">ce</w> <w n="39.3">qu</w>’<w n="39.4">il</w> <w n="39.5">a</w> <w n="39.6">paré</w> <w n="39.7">d</w>’<w n="39.8">une</w> <w n="39.9">forme</w> <w n="39.10">nouvelle</w></l>
						<l n="40" num="10.4"><w n="40.1">Croîtra</w> <w n="40.2">dans</w> <w n="40.3">l</w>’<w n="40.4">avenir</w> <w n="40.5">sans</w> <w n="40.6">crainte</w> <w n="40.7">du</w> <w n="40.8">trépas</w>.</l>
					</lg>
					<lg n="11">
						<l n="41" num="11.1"><w n="41.1">Shakspeare</w> ! <w n="41.2">Vainement</w> <w n="41.3">sous</w> <w n="41.4">les</w> <w n="41.5">voûtes</w> <w n="41.6">suprêmes</w></l>
						<l n="42" num="11.2"><w n="42.1">Passe</w> <w n="42.2">le</w> <w n="42.3">vil</w> <w n="42.4">troupeau</w> <w n="42.5">des</w> <w n="42.6">mortels</w> <w n="42.7">inconstants</w>,</l>
						<l n="43" num="11.3"><w n="43.1">Comme</w> <w n="43.2">du</w> <w n="43.3">sable</w>, <w n="43.4">en</w> <w n="43.5">vain</w> <w n="43.6">sur</w> <w n="43.7">l</w>’<w n="43.8">abîme</w> <w n="43.9">des</w> <w n="43.10">temps</w></l>
						<l n="44" num="11.4"><w n="44.1">L</w>’<w n="44.2">un</w> <w n="44.3">par</w> <w n="44.4">l</w>’<w n="44.5">autre</w> <w n="44.6">écrasés</w> <w n="44.7">s</w>’<w n="44.8">entassent</w> <w n="44.9">les</w> <w n="44.10">systèmes</w> ;</l>
					</lg>
					<lg n="12">
						<l n="45" num="12.1"><w n="45.1">Ton</w> <w n="45.2">génie</w> <w n="45.3">est</w> <w n="45.4">pareil</w> <w n="45.5">au</w> <w n="45.6">soleil</w> <w n="45.7">radieux</w></l>
						<l n="46" num="12.2"><w n="46.1">Qui</w>, <w n="46.2">toujours</w> <w n="46.3">immobile</w> <w n="46.4">au</w> <w n="46.5">haut</w> <w n="46.6">de</w> <w n="46.7">l</w>’<w n="46.8">empyrée</w>,</l>
						<l n="47" num="12.3"><w n="47.1">Verse</w> <w n="47.2">tranquillement</w> <w n="47.3">sa</w> <w n="47.4">lumière</w> <w n="47.5">sacrée</w></l>
						<l n="48" num="12.4"><w n="48.1">Sur</w> <w n="48.2">la</w> <w n="48.3">folle</w> <w n="48.4">rumeur</w> <w n="48.5">des</w> <w n="48.6">flots</w> <w n="48.7">tumultueux</w>.</l>
					</lg>
				</div></body></text></TEI>