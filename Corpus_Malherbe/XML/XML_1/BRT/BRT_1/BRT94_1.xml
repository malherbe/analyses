<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PROVERBES</head><div type="poem" key="BRT94">
				<head type="number">XCIV</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Ce</w> <w n="1.2">qui</w> <w n="1.3">très</w> <w n="1.4">fort</w> <w n="1.5">me</w> <w n="1.6">choque</w> <w n="1.7">en</w> <w n="1.8">vous</w>, <w n="1.9">mam</w>’<w n="1.10">zelle</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Ah</w> ! <w n="2.2">ce</w> <w n="2.3">n</w>’<w n="2.4">est</w> <w n="2.5">pas</w> <w n="2.6">votre</w> <w n="2.7">air</w> <w n="2.8">impertinent</w>…</l>
					<l n="3" num="1.3">— <w n="3.1">Impertinent</w> <w n="3.2">vous</w>-<w n="3.3">même</w> ! <w n="3.4">Répond</w>-<w n="3.5">elle</w> ;</l>
					<l n="4" num="1.4"><w n="4.1">A</w>-<w n="4.2">t</w>-<w n="4.3">on</w> <w n="4.4">jamais</w> <w n="4.5">ouï</w> <w n="4.6">pareil</w> <w n="4.7">manant</w> ! …</l>
					<l n="5" num="1.5">— <w n="5.1">Je</w> <w n="5.2">ne</w> <w n="5.3">dis</w> <w n="5.4">rien</w> <w n="5.5">de</w> <w n="5.6">votre</w> <w n="5.7">caractère</w> ;</l>
					<l n="6" num="1.6"><w n="6.1">Il</w> <w n="6.2">est</w> <w n="6.3">fougueux</w>, <w n="6.4">et</w> <w n="6.5">chacun</w> <w n="6.6">le</w> <w n="6.7">connaît</w> :</l>
					<l n="7" num="1.7"><w n="7.1">Votre</w> <w n="7.2">cervelle</w> <w n="7.3">est</w> <w n="7.4">vraiment</w> <w n="7.5">un</w> <w n="7.6">cratère</w>,</l>
					<l n="8" num="1.8"><w n="8.1">Et</w> <w n="8.2">votre</w> <w n="8.3">tête</w> <w n="8.4">est</w> <w n="8.5">tout</w> <w n="8.6">près</w> <w n="8.7">du</w> <w n="8.8">bonnet</w>.</l>
					<l n="9" num="1.9"><w n="9.1">Cela</w> <w n="9.2">me</w> <w n="9.3">va</w>. <w n="9.4">Je</w> <w n="9.5">ne</w> <w n="9.6">cherche</w> <w n="9.7">pas</w> <w n="9.8">noise</w></l>
					<l n="10" num="1.10"><w n="10.1">À</w> <w n="10.2">votre</w> <w n="10.3">goût</w> <w n="10.4">pour</w> <w n="10.5">les</w> <w n="10.6">colifichets</w>,</l>
					<l n="11" num="1.11"><w n="11.1">Aux</w> <w n="11.2">traits</w> <w n="11.3">malins</w> <w n="11.4">de</w> <w n="11.5">votre</w> <w n="11.6">humeur</w> <w n="11.7">sournoise</w>,</l>
					<l n="12" num="1.12"><w n="12.1">À</w> <w n="12.2">d</w>’<w n="12.3">autres</w> <w n="12.4">torts</w> <w n="12.5">devant</w> <w n="12.6">tous</w> <w n="12.7">affichés</w>.</l>
					<l n="13" num="1.13"><w n="13.1">Non</w> ! … <w n="13.2">Ce</w> <w n="13.3">par</w> <w n="13.4">quoi</w> <w n="13.5">de</w> <w n="13.6">notre</w> <w n="13.7">mariage</w></l>
					<l n="14" num="1.14"><w n="14.1">L</w>’<w n="14.2">heureux</w> <w n="14.3">moment</w> <w n="14.4">peut</w> <w n="14.5">être</w> <w n="14.6">différé</w></l>
					<l n="15" num="1.15"><w n="15.1">N</w>’<w n="15.2">est</w> <w n="15.3">pas</w> <w n="15.4">cela</w>… <w n="15.5">Vous</w> <w n="15.6">devinez</w>, <w n="15.7">je</w> <w n="15.8">gage</w> ? …</l>
					<l n="16" num="1.16"><w n="16.1">Point</w> ? … <w n="16.2">Franchement</w>, <w n="16.3">alors</w>, <w n="16.4">je</w> <w n="16.5">le</w> <w n="16.6">dirai</w> :</l>
					<l n="17" num="1.17"><w n="17.1">Ce</w> <w n="17.2">qui</w> <w n="17.3">me</w> <w n="17.4">fait</w> <w n="17.5">ainsi</w> <w n="17.6">faire</w> <w n="17.7">la</w> <w n="17.8">moue</w>,</l>
					<l n="18" num="1.18"><w n="18.1">Ce</w> <w n="18.2">qui</w> <w n="18.3">m</w>’<w n="18.4">agace</w> <w n="18.5">et</w> <w n="18.6">m</w>’<w n="18.7">éloigne</w>, <w n="18.8">mordieu</w> !</l>
					<l n="19" num="1.19"><w n="19.1">C</w>’<w n="19.2">est</w> <w n="19.3">ce</w> <w n="19.4">point</w> <w n="19.5">noir</w> <w n="19.6">perdu</w> <w n="19.7">sur</w> <w n="19.8">votre</w> <w n="19.9">joue</w>,</l>
					<l n="20" num="1.20"><w n="20.1">Avec</w> <w n="20.2">des</w> <w n="20.3">poils</w> <w n="20.4">hérissés</w> <w n="20.5">au</w> <w n="20.6">milieu</w> ! »</l>
				</lg>
					<closer>
						<note type="footnote" id="none">▪ Ce qui est différé n’est pas perdu.</note>
					</closer>
			</div></body></text></TEI>