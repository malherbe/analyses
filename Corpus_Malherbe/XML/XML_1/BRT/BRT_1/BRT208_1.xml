<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">HOMONYMES</head><div type="poem" key="BRT208">
				<head type="number">XXXIII</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">D</w>’<w n="1.2">aucuns</w> <w n="1.3">l</w>’<w n="1.4">ont</w> <w n="1.5">sur</w> <w n="1.6">un</w> <w n="1.7">œil</w> <w n="1.8">et</w> <w n="1.9">quelquefois</w> <w n="1.10">sur</w> <w n="1.11">deux</w> !</l>
					<l n="2" num="1.2"><w n="2.1">Je</w> <w n="2.2">les</w> <w n="2.3">plains</w> <w n="2.4">pour</w> <w n="2.5">de</w> <w n="2.6">bon</w>, <w n="2.7">car</w> <w n="2.8">c</w>’<w n="2.9">est</w> <w n="2.10">fort</w> <w n="2.11">ennuyeux</w>.</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="2">
					<l n="3" num="2.1"><space unit="char" quantity="8"></space><w n="3.1">D</w>’<w n="3.2">autres</w> <w n="3.3">y</w> <w n="3.4">froncent</w> <w n="3.5">la</w> <w n="3.6">dentelle</w>,</l>
					<l n="4" num="2.2"><space unit="char" quantity="8"></space><w n="4.1">Y</w> <w n="4.2">brodent</w> <w n="4.3">leur</w> <w n="4.4">chiffre</w>, <w n="4.5">leur</w> <w n="4.6">nom</w>,</l>
					<l n="5" num="2.3"><space unit="char" quantity="8"></space><w n="5.1">Mais</w> <w n="5.2">sous</w> <w n="5.3">le</w> <w n="5.4">dais</w> <w n="5.5">de</w> <w n="5.6">brocatelle</w></l>
					<l n="6" num="2.4"><space unit="char" quantity="8"></space><w n="6.1">Y</w> <w n="6.2">dort</w>-<w n="6.3">on</w> <w n="6.4">mieux</w> ? <w n="6.5">Ah</w> ! <w n="6.6">certes</w>, <w n="6.7">non</w>.</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="3">
					<l n="7" num="3.1"><w n="7.1">S</w>’<w n="7.2">il</w> <w n="7.3">est</w> <w n="7.4">un</w> <w n="7.5">peu</w> <w n="7.6">trop</w> <w n="7.7">fort</w>, <w n="7.8">craignez</w>-<w n="7.9">en</w> <w n="7.10">l</w>’<w n="7.11">amertume</w>,</l>
					<l n="8" num="3.2"><w n="8.1">Et</w>, <w n="8.2">de</w> <w n="8.3">cette</w> <w n="8.4">boisson</w>, <w n="8.5">ne</w> <w n="8.6">prenez</w> <w n="8.7">pas</w> <w n="8.8">coutume</w>.</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="4">
					<l n="9" num="4.1"><space unit="char" quantity="8"></space><w n="9.1">Il</w> <w n="9.2">n</w>’<w n="9.3">a</w> <w n="9.4">que</w> <w n="9.5">le</w> <w n="9.6">vingtième</w> <w n="9.7">rang</w>,</l>
					<l n="10" num="4.2"><space unit="char" quantity="8"></space><w n="10.1">Mais</w> <w n="10.2">il</w> <w n="10.3">n</w>’<w n="10.4">en</w> <w n="10.5">fait</w> <w n="10.6">point</w> <w n="10.7">la</w> <w n="10.8">grimace</w>.</l>
					<l n="11" num="4.3"><space unit="char" quantity="8"></space><w n="11.1">N</w>’<w n="11.2">est</w>-<w n="11.3">on</w> <w n="11.4">pas</w> <w n="11.5">toujours</w> <w n="11.6">assez</w> <w n="11.7">grand</w></l>
					<l n="12" num="4.4"><space unit="char" quantity="8"></space><w n="12.1">Lorsqu</w>’<w n="12.2">on</w> <w n="12.3">occupe</w> <w n="12.4">bien</w> <w n="12.5">sa</w> <w n="12.6">place</w> ? …</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="5">
					<l n="13" num="5.1"><w n="13.1">Après</w> <w n="13.2">Dunkel</w> <w n="13.3">et</w> <w n="13.4">Perth</w>, <w n="13.5">c</w>’<w n="13.6">est</w> <w n="13.7">un</w> <w n="13.8">lac</w> : <w n="13.9">Traversez</w>,</l>
					<l n="14" num="5.2"><w n="14.1">Formez</w>, <w n="14.2">près</w> <w n="14.3">de</w> <w n="14.4">la</w> <w n="14.5">mer</w>, <w n="14.6">un</w> <w n="14.7">golfe</w>. <w n="14.8">C</w>’<w n="14.9">est</w> <w n="14.10">assez</w>.</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="6">
					<l n="15" num="6.1"><space unit="char" quantity="8"></space><w n="15.1">Maître</w> <w n="15.2">Adam</w> <w n="15.3">cultiva</w> <w n="15.4">la</w> <w n="15.5">rime</w>,</l>
					<l n="16" num="6.2"><space unit="char" quantity="8"></space><w n="16.1">Sans</w> <w n="16.2">doute</w> <w n="16.3">sur</w> <w n="16.4">ses</w> <w n="16.5">gazons</w> <w n="16.6">verts</w> ;</l>
					<l n="17" num="6.3"><space unit="char" quantity="8"></space><w n="17.1">La</w> <w n="17.2">muse</w> <w n="17.3">volontiers</w> <w n="17.4">s</w>’<w n="17.5">escrime</w></l>
					<l n="18" num="6.4"><space unit="char" quantity="8"></space><w n="18.1">Dans</w> <w n="18.2">les</w> <w n="18.3">champs</w> <w n="18.4">au</w> <w n="18.5">soleil</w> <w n="18.6">ouverts</w>.</l>
				</lg>
					<closer>
						<note id="none" type="footnote">▪ Taie, taie, thé, t, Tay, Thaix.</note>
					</closer>
			</div></body></text></TEI>