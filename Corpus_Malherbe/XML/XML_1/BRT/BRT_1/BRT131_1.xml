<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CHARADES</head><div type="poem" key="BRT131">
				<head type="number">XXXI</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">La</w> <w n="1.2">vieille</w> <w n="1.3">médecine</w> <w n="1.4">en</w> <w n="1.5">a</w> <w n="1.6">fait</w> <w n="1.7">long</w> <w n="1.8">usage</w> ;</l>
					<l n="2" num="1.2"><w n="2.1">Et</w> <w n="2.2">Molière</w> <w n="2.3">lui</w>-<w n="2.4">même</w>… <w n="2.5">Oh</w> ! <w n="2.6">n</w>’<w n="2.7">allez</w> <w n="2.8">pas</w> <w n="2.9">trop</w> <w n="2.10">loin</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Convenable</w> <w n="3.2">est</w> <w n="3.3">ce</w> <w n="3.4">mot</w>. <w n="3.5">Le</w> <w n="3.6">plus</w> <w n="3.7">prude</w> <w n="3.8">langage</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Sans</w> <w n="4.2">hésitation</w>, <w n="4.3">s</w>’<w n="4.4">en</w> <w n="4.5">empare</w> <w n="4.6">au</w> <w n="4.7">besoin</w>.</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Molière</w>, <w n="5.2">encore</w> <w n="5.3">lui</w> ! <w n="5.4">les</w> <w n="5.5">traçait</w> <w n="5.6">tous</w> <w n="5.7">en</w> <w n="5.8">maître</w></l>
					<l n="6" num="2.2"><w n="6.1">Et</w> <w n="6.2">lui</w>-<w n="6.3">même</w> <w n="6.4">daignait</w> <w n="6.5">en</w> <w n="6.6">tenir</w> <w n="6.7">à</w> <w n="6.8">son</w> <w n="6.9">tour</w>.</l>
					<l n="7" num="2.3"><w n="7.1">Dans</w> <w n="7.2">le</w> <w n="7.3">monde</w>, <w n="7.4">chacun</w> <w n="7.5">a</w> <w n="7.6">le</w> <w n="7.7">sien</w>, <w n="7.8">et</w> <w n="7.9">peut</w>-<w n="7.10">être</w></l>
					<l n="8" num="2.4"><w n="8.1">Plus</w> <w n="8.2">d</w>’<w n="8.3">un</w>, <w n="8.4">tout</w> <w n="8.5">en</w> <w n="8.6">riant</w>, <w n="8.7">le</w> <w n="8.8">trouve</w>-<w n="8.9">t</w>-<w n="8.10">il</w> <w n="8.11">bien</w> <w n="8.12">lourd</w>.</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Pour</w> <w n="9.2">certains</w> <w n="9.3">odorats</w>, <w n="9.4">l</w>’<w n="9.5">odeur</w> <w n="9.6">qui</w> <w n="9.7">s</w>’<w n="9.8">en</w> <w n="9.9">échappe</w></l>
					<l n="10" num="3.2"><w n="10.1">A</w> <w n="10.2">cent</w> <w n="10.3">fois</w> <w n="10.4">plus</w> <w n="10.5">d</w>’<w n="10.6">attraits</w> <w n="10.7">que</w> <w n="10.8">l</w>’<w n="10.9">arôme</w> <w n="10.10">des</w> <w n="10.11">fleurs</w>.</l>
					<l n="11" num="3.3"><w n="11.1">On</w> <w n="11.2">blâme</w> <w n="11.3">parmi</w> <w n="11.4">nous</w> <w n="11.5">les</w> <w n="11.6">instincts</w> <w n="11.7">du</w> <w n="11.8">satrape</w>,</l>
					<l n="12" num="3.4"><w n="12.1">Mais</w> <w n="12.2">sommes</w>-<w n="12.3">nous</w> <w n="12.4">vraiment</w> <w n="12.5">plus</w> <w n="12.6">sobres</w> <w n="12.7">et</w> <w n="12.8">meilleurs</w> ?</l>
				</lg>
					<closer>
						<note id="none" type="footnote">▪ Casse-role.</note>
					</closer>
			</div></body></text></TEI>