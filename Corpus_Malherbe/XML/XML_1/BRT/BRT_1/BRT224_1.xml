<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">HOMONYMES</head><div type="poem" key="BRT224">
				<head type="number">XLIX</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Il</w> <w n="1.2">a</w> <w n="1.3">voué</w> <w n="1.4">sa</w> <w n="1.5">vie</w> <w n="1.6">à</w> <w n="1.7">plus</w> <w n="1.8">d</w>’<w n="1.9">une</w> <w n="1.10">chimère</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Épuisant</w> <w n="2.2">le</w> <w n="2.3">nectar</w> <w n="2.4">jusqu</w>’<w n="2.5">en</w> <w n="2.6">sa</w> <subst reason="analysis" hand="ML" type="completion"><del>...</del><add rend="hidden"><w n="2.7">lie</w></add></subst> <w n="2.8">amère</w>.</l>
					<l n="3" num="1.3"><w n="3.1">Il</w> <w n="3.2">a</w> <w n="3.3">vu</w> <w n="3.4">satisfait</w> <w n="3.5">chacun</w> <w n="3.6">de</w> <w n="3.7">ses</w> <w n="3.8">désirs</w></l>
					<l n="4" num="1.4"><w n="4.1">Sous</w> <w n="4.2">la</w> <w n="4.3">chaîne</w> <w n="4.4">qui</w> <subst reason="analysis" hand="ML" type="completion"><del>...</del><add rend="hidden"><w n="4.5">lie</w></add></subst> <w n="4.6">une</w> <w n="4.7">âme</w> <w n="4.8">aux</w> <w n="4.9">vains</w> <w n="4.10">plaisirs</w>.</l>
					<l n="5" num="1.5"><w n="5.1">Il</w> <w n="5.2">gémit</w> <w n="5.3">à</w> <w n="5.4">présent</w> <w n="5.5">sur</w> <w n="5.6">un</w> <subst reason="analysis" hand="ML" type="completion"><del>...</del><add rend="hidden"><w n="5.7">lit</w></add></subst> <w n="5.8">de</w> <w n="5.9">souffrance</w>.</l>
					<l n="6" num="1.6"><w n="6.1">On</w> <subst reason="analysis" hand="ML" type="completion"><del>...</del><add rend="hidden"><w n="6.2">lit</w></add></subst> <w n="6.3">dans</w> <w n="6.4">ses</w> <w n="6.5">regards</w> <w n="6.6">qu</w>’<w n="6.7">il</w> <w n="6.8">n</w>’<w n="6.9">a</w> <w n="6.10">plus</w> <w n="6.11">d</w>’<w n="6.12">espérance</w>.</l>
					<l n="7" num="1.7"><w n="7.1">Qu</w>’<w n="7.2">importent</w> <w n="7.3">les</w> <w n="7.4">trésors</w> <w n="7.5">sous</w> <w n="7.6">ses</w> <w n="7.7">mains</w> <w n="7.8">entassés</w> !</l>
					<l n="8" num="1.8"><w n="8.1">Les</w> <w n="8.2">cupides</w> <w n="8.3">flatteurs</w> <w n="8.4">autour</w> <w n="8.5">de</w> <w n="8.6">lui</w> <w n="8.7">pressés</w> !</l>
					<l n="9" num="1.9"><w n="9.1">Le</w> <w n="9.2">blason</w> <w n="9.3">des</w> <w n="9.4">aïeux</w>, <w n="9.5">héros</w> <w n="9.6">portant</w> <w n="9.7">de</w> <w n="9.8">gueules</w> !</l>
					<l n="10" num="1.10"><w n="10.1">Et</w> <w n="10.2">la</w> <w n="10.3">fleur</w> <w n="10.4">de</w> <subst reason="analysis" hand="ML" type="completion"><del>...</del><add rend="hidden"><w n="10.5">lis</w></add></subst> <w n="10.6">d</w>’<w n="10.7">or</w> <w n="10.8">sur</w> <w n="10.9">celui</w> <w n="10.10">des</w> <w n="10.11">aïeules</w> !</l>
					<l n="11" num="1.11"><w n="11.1">Qu</w>’<w n="11.2">importent</w> ! <w n="11.3">Le</w> <w n="11.4">passé</w>, <w n="11.5">comme</w> <w n="11.6">un</w> <w n="11.7">épi</w> <w n="11.8">trop</w> <w n="11.9">mûr</w>,</l>
					<l n="12" num="1.12"><w n="12.1">S</w>’<w n="12.2">égrène</w> <w n="12.3">dans</w> <w n="12.4">la</w> <w n="12.5">nuit</w> <w n="12.6">au</w> <w n="12.7">bord</w> <w n="12.8">du</w> <w n="12.9">gouffre</w> <w n="12.10">obscur</w> !</l>
					<l n="13" num="1.13"><w n="13.1">A</w> <w n="13.2">l</w>’<w n="13.3">horloge</w> <w n="13.4">des</w> <w n="13.5">temps</w>, <w n="13.6">le</w> <w n="13.7">grand</w> <subst reason="analysis" hand="ML" type="completion"><del>...</del><add rend="hidden"><w n="13.8">lit</w></add></subst> <w n="13.9">de</w> <w n="13.10">justice</w></l>
					<l n="14" num="1.14"><w n="14.1">Tinte</w> ! <w n="14.2">Et</w> <w n="14.3">le</w> <w n="14.4">moribond</w>, <w n="14.5">vidant</w> <w n="14.6">tout</w> <w n="14.7">le</w> <w n="14.8">calice</w>,</l>
					<l n="15" num="1.15"><w n="15.1">Balbutie</w> <w n="15.2">en</w> <w n="15.3">pleurant</w>, <w n="15.4">hagard</w>, <w n="15.5">épouvanté</w> :</l>
					<l n="16" num="1.16"><w n="16.1">Mensonge</w> <w n="16.2">du</w> <w n="16.3">bonheur</w> ! <w n="16.4">tout</w> <w n="16.5">n</w>’<w n="16.6">est</w> <w n="16.7">que</w> <w n="16.8">vanité</w> ! »</l>
				</lg>
					<closer>
						<note id="none" type="footnote">▪ Lie, lie, lit, lit, lis, lit.</note>
					</closer>
			</div></body></text></TEI>