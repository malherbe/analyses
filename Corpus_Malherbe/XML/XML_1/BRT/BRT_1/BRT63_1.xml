<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PROVERBES</head><div type="poem" key="BRT63">
				<head type="number">LXIII</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">La</w> <w n="1.2">petite</w> <w n="1.3">Ninette</w> <w n="1.4">accompagne</w> <w n="1.5">sa</w> <w n="1.6">mère</w></l>
					<l n="2" num="1.2"><w n="2.1">Vers</w> <w n="2.2">l</w>’<w n="2.3">étable</w> <w n="2.4">odorante</w> <w n="2.5">et</w> <w n="2.6">dans</w> <w n="2.7">la</w> <w n="2.8">basse</w>-<w n="2.9">cour</w>.</l>
					<l n="3" num="1.3"><w n="3.1">La</w> <w n="3.2">poule</w> <w n="3.3">favorite</w>, <w n="3.4">aussitôt</w>, <w n="3.5">la</w> <w n="3.6">première</w>,</l>
					<l n="4" num="1.4"><space unit="char" quantity="12"></space><w n="4.1">En</w> <w n="4.2">sautillant</w> <w n="4.3">accourt</w>.</l>
					<l n="5" num="1.5"><w n="5.1">Ninette</w> <w n="5.2">voudrait</w> <w n="5.3">bien</w> <w n="5.4">lui</w> <w n="5.5">faire</w> <w n="5.6">un</w> <w n="5.7">privilège</w>.</l>
					<l n="6" num="1.6"><w n="6.1">Mais</w> <w n="6.2">elle</w> <w n="6.3">n</w>’<w n="6.4">ose</w> <w n="6.5">point</w>… <w n="6.6">et</w>, <w n="6.7">d</w>’<w n="6.8">ailleurs</w>, <w n="6.9">ne</w> <w n="6.10">doit</w> <w n="6.11">pas</w></l>
					<l n="7" num="1.7"><w n="7.1">Frustrer</w> <w n="7.2">d</w>’<w n="7.3">orge</w> <w n="7.4">mondé</w> <w n="7.5">le</w> <w n="7.6">sémillant</w> <w n="7.7">cortège</w></l>
					<l n="8" num="1.8"><space unit="char" quantity="12"></space><w n="8.1">Empressé</w> <w n="8.2">sur</w> <w n="8.3">ses</w> <w n="8.4">pas</w> :</l>
					<l n="9" num="1.9"><w n="9.1">Pour</w> <w n="9.2">attirer</w> <w n="9.3">l</w>’<w n="9.4">enfant</w>, <w n="9.5">le</w> <w n="9.6">dindon</w> <w n="9.7">fait</w> <w n="9.8">la</w> <w n="9.9">roue</w> ;</l>
					<l n="10" num="1.10"><w n="10.1">Le</w> <w n="10.2">poulet</w> <w n="10.3">se</w> <w n="10.4">pavane</w> <w n="10.5">et</w> <w n="10.6">s</w>’<w n="10.7">exerce</w> <w n="10.8">à</w> <w n="10.9">chanter</w> ;</l>
					<l n="11" num="1.11"><w n="11.1">Le</w> <w n="11.2">paon</w> <w n="11.3">se</w> <w n="11.4">montre</w> <w n="11.5">beau</w> <w n="11.6">de</w> <w n="11.7">la</w> <w n="11.8">poupe</w> <w n="11.9">à</w> <w n="11.10">la</w> <w n="11.11">proue</w> ;</l>
					<l n="12" num="1.12"><space unit="char" quantity="12"></space><w n="12.1">Le</w> <w n="12.2">canard</w> <w n="12.3">veut</w> <w n="12.4">sauter</w> !</l>
					<l n="13" num="1.13"><w n="13.1">Mais</w>, <w n="13.2">plus</w> <w n="13.3">haut</w> <w n="13.4">que</w> <w n="13.5">ces</w> <w n="13.6">bruits</w>, <w n="13.7">ces</w> <w n="13.8">efforts</w>, <w n="13.9">ce</w> <w n="13.10">tumulte</w>,</l>
					<l n="14" num="1.14"><w n="14.1">Chœur</w> <w n="14.2">discordant</w> <w n="14.3">parfois</w>, <w n="14.4">ensemble</w> <w n="14.5">fanfaron</w>,</l>
					<l n="15" num="1.15"><w n="15.1">S</w>’<w n="15.2">élève</w> <w n="15.3">mâle</w> <w n="15.4">et</w> <w n="15.5">fier</w> <w n="15.6">le</w> <w n="15.7">chant</w> <w n="15.8">d</w>’<w n="15.9">un</w> <w n="15.10">coq</w> <w n="15.11">adulte</w>.</l>
					<l n="16" num="1.16"><space unit="char" quantity="12"></space><w n="16.1">Honneur</w> <w n="16.2">à</w> <w n="16.3">son</w> <w n="16.4">clairon</w> !</l>
				</lg>
					<closer>
						<note type="footnote" id="none">▪ La poule ne doit pas chanter plus haut que le coq.</note>
					</closer>
			</div></body></text></TEI>