<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PROVERBES</head><div type="poem" key="BRT73">
				<head type="number">LXXIII</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Les</w> <w n="1.2">nouveaux</w> <w n="1.3">mariés</w> <w n="1.4">se</w> <w n="1.5">boudent</w>, <w n="1.6">l</w>’<w n="1.7">air</w> <w n="1.8">morose</w>,</l>
					<l n="2" num="1.2"><space unit="char" quantity="8"></space><w n="2.1">Le</w> <w n="2.2">ton</w> <w n="2.3">sec</w>, <w n="2.4">le</w> <w n="2.5">front</w> <w n="2.6">soucieux</w> ;</l>
					<l n="3" num="1.3"><w n="3.1">Et</w> <w n="3.2">l</w>’<w n="3.3">on</w> <w n="3.4">dirait</w> <w n="3.5">madame</w> <w n="3.6">atteinte</w> <w n="3.7">d</w>’<w n="3.8">amaurose</w>,</l>
					<l n="4" num="1.4"><space unit="char" quantity="8"></space><w n="4.1">À</w> <w n="4.2">la</w> <w n="4.3">rougeur</w> <w n="4.4">de</w> <w n="4.5">ses</w> <w n="4.6">grands</w> <w n="4.7">yeux</w> :</l>
					<l n="5" num="1.5"><w n="5.1">Oh</w> ! <w n="5.2">fait</w>-<w n="5.3">elle</w> <w n="5.4">en</w> <w n="5.5">courroux</w>, <w n="5.6">quel</w> <w n="5.7">réveil</w> <w n="5.8">d</w>’<w n="5.9">un</w> <w n="5.10">beau</w> <w n="5.11">songe</w></l>
					<l n="6" num="1.6"><space unit="char" quantity="8"></space><w n="6.1">D</w>’<w n="6.2">une</w> <w n="6.3">trop</w> <w n="6.4">chère</w> <w n="6.5">et</w> <w n="6.6">douce</w> <w n="6.7">erreur</w> !</l>
					<l n="7" num="1.7"><w n="7.1">Non</w> : <w n="7.2">votre</w> <w n="7.3">cœur</w> <w n="7.4">n</w>’<w n="7.5">est</w> <w n="7.6">pas</w> <w n="7.7">à</w> <w n="7.8">moi</w> <w n="7.9">seule</w>, <w n="7.10">ô</w> <w n="7.11">mensonge</w> !</l>
					<l n="8" num="1.8"><space unit="char" quantity="8"></space><w n="8.1">Mais</w> <w n="8.2">au</w> <w n="8.3">club</w>, <w n="8.4">au</w> <w n="8.5">tabac</w>… <w n="8.6">horreur</w> !</l>
					<l n="9" num="1.9">— <w n="9.1">Je</w> <w n="9.2">vous</w> <w n="9.3">croyais</w>, <w n="9.4">dit</w>-<w n="9.5">il</w>, <w n="9.6">simple</w>, <w n="9.7">économe</w>… <w n="9.8">ô</w> <w n="9.9">honte</w> !</l>
					<l n="10" num="1.10"><space unit="char" quantity="8"></space><w n="10.1">L</w>’<w n="10.2">affreux</w> <w n="10.3">contraire</w> <w n="10.4">seul</w> <w n="10.5">est</w> <w n="10.6">vrai</w> ;</l>
					<l n="11" num="1.11"><w n="11.1">Il</w> <w n="11.2">pleut</w>, <w n="11.3">par</w> <w n="11.4">votre</w> <w n="11.5">fait</w>, <w n="11.6">chez</w> <w n="11.7">moi</w> <w n="11.8">compte</w> <w n="11.9">sur</w> <w n="11.10">compte</w>…</l>
					<l n="12" num="1.12"><space unit="char" quantity="8"></space><w n="12.1">De</w> <w n="12.2">payer</w> <w n="12.3">je</w> <w n="12.4">refuserai</w> ! »</l>
				</lg>
				<lg n="2">
					<l n="13" num="2.1"><w n="13.1">Cette</w> <w n="13.2">histoire</w> <w n="13.3">est</w> <w n="13.4">commune</w> <w n="13.5">à</w> <w n="13.6">de</w> <w n="13.7">nombreux</w> <w n="13.8">ménages</w></l>
					<l n="14" num="2.2"><space unit="char" quantity="8"></space><w n="14.1">Déconsidérés</w>, <w n="14.2">malheureux</w> ;</l>
					<l n="15" num="2.3"><w n="15.1">Si</w> <w n="15.2">l</w>’<w n="15.3">exemple</w> <w n="15.4">ne</w> <w n="15.5">rend</w> <w n="15.6">pas</w> <w n="15.7">les</w> <w n="15.8">autres</w> <w n="15.9">plus</w> <w n="15.10">sages</w>,</l>
					<l n="16" num="2.4"><space unit="char" quantity="16"></space><w n="16.1">Tant</w> <w n="16.2">pis</w> <w n="16.3">pour</w> <w n="16.4">eux</w> !</l>
				</lg>
					<closer>
						<note type="footnote" id="none">▪ Erreur n’est pas compte.</note>
					</closer>
			</div></body></text></TEI>