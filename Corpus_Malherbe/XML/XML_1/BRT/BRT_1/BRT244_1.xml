<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">MOTS CARRÉS</head><div type="poem" key="BRT244">
				<head type="number">XIX</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Les</w> <w n="1.2">dieux</w> <w n="1.3">s</w>’<w n="1.4">en</w> <w n="1.5">vont</w> : <w n="1.6">Jupin</w>, <subst hand="ML" reason="analysis" type="completion"><del>.....</del><add rend="hidden"><w n="1.7">Vénus</w></add></subst>, <w n="1.8">Junon</w>, <w n="1.9">Mercure</w></l>
					<l n="2" num="1.2"><w n="2.1">Ont</w> <w n="2.2">achevé</w> <w n="2.3">leur</w> <w n="2.4">temps</w>, <w n="2.5">même</w> <w n="2.6">en</w> <w n="2.7">littérature</w> !</l>
					<l n="3" num="1.3"><space unit="char" quantity="12"></space><w n="3.1">Mais</w> <w n="3.2">on</w> <w n="3.3">lira</w> <w n="3.4">toujours</w></l>
					<l n="4" num="1.4"><w n="4.1">Molière</w> <w n="4.2">le</w> <w n="4.3">réel</w> <w n="4.4">avec</w> <w n="4.5">ses</w> <w n="4.6">Cydalise</w>,</l>
					<l n="5" num="1.5"><w n="5.1">Avec</w> <w n="5.2">ses</w> <w n="5.3">Harpagon</w>, <w n="5.4">ses</w> <w n="5.5">Agnès</w>, <w n="5.6">ses</w> <subst hand="ML" reason="analysis" type="completion"><del>.....</del><add rend="hidden"><w n="5.7">Élise</w></add></subst></l>
					<l n="6" num="1.6"><space unit="char" quantity="12"></space><w n="6.1">Et</w> <w n="6.2">ses</w> <w n="6.3">ruses</w> <w n="6.4">d</w>’<w n="6.5">amours</w>.</l>
					<l n="7" num="1.7"><w n="7.1">Aux</w> <w n="7.2">générations</w> <w n="7.3">qui</w> <w n="7.4">vont</w> <w n="7.5">suivre</w> <w n="7.6">les</w> <w n="7.7">nôtres</w>,</l>
					<l n="8" num="1.8"><hi rend="ital"><w n="8.1">La</w> <w n="8.2">Guerre</w> <w n="8.3">du</w> <subst hand="ML" reason="analysis" type="completion"><del>.....</del><add rend="hidden"><w n="8.4">Nizam</w></add></subst></hi>, <hi rend="ital"><w n="8.5">la</w> <w n="8.6">Floride</w></hi> <w n="8.7">et</w> <w n="8.8">bien</w> <w n="8.9">d</w>’<w n="8.10">autres</w></l>
					<l n="9" num="1.9"><space unit="char" quantity="12"></space><w n="9.1">Parleront</w> <w n="9.2">de</w> <w n="9.3">Méry</w> ;</l>
					<l n="10" num="1.10"><w n="10.1">Et</w> <w n="10.2">longtemps</w> <w n="10.3">il</w> <w n="10.4">sera</w> <w n="10.5">d</w>’<w n="10.6">habitude</w> <w n="10.7">et</w> <w n="10.8">d</w>’<subst hand="ML" reason="analysis" type="completion"><del>.....</del><add rend="hidden"><w n="10.9">usage</w></add></subst></l>
					<l n="11" num="1.11"><w n="11.1">D</w>’<w n="11.2">avoir</w> <w n="11.3">sur</w> <w n="11.4">ses</w> <w n="11.5">rayons</w> <w n="11.6">le</w> <w n="11.7">saisissant</w> <w n="11.8">ouvrage</w></l>
					<l n="12" num="1.12"><space unit="char" quantity="12"></space><w n="12.1">De</w> <w n="12.2">Gabriel</w> <w n="12.3">Ferry</w>.</l>
					<l n="13" num="1.13"><w n="13.1">Ainsi</w> <w n="13.2">les</w> <w n="13.3">bous</w> <w n="13.4">auteurs</w>, <w n="13.5">aux</w> <w n="13.6">champs</w> <w n="13.7">de</w> <w n="13.8">la</w> <w n="13.9">pensée</w>,</l>
					<l n="14" num="1.14"><w n="14.1">Poursuivent</w> <w n="14.2">pas</w> <w n="14.3">à</w> <w n="14.4">pas</w> <w n="14.5">leur</w> <w n="14.6">marche</w> <w n="14.7">cadencée</w></l>
					<l n="15" num="1.15"><space unit="char" quantity="12"></space><w n="15.1">Pour</w> <w n="15.2">y</w> <subst hand="ML" reason="analysis" type="completion"><del>.....</del><add rend="hidden"><w n="15.3">semer</w></add></subst> <w n="15.4">le</w> <w n="15.5">grain</w>.</l>
					<l n="16" num="1.16"><w n="16.1">Et</w> <w n="16.2">la</w> <w n="16.3">postérité</w> <w n="16.4">dans</w> <w n="16.5">leurs</w> <w n="16.6">sillons</w> <w n="16.7">moissonne</w>,</l>
					<l n="17" num="1.17"><w n="17.1">Gravant</w> <w n="17.2">avec</w> <w n="17.3">honneur</w> <w n="17.4">les</w> <w n="17.5">traits</w> <w n="17.6">de</w> <w n="17.7">leur</w> <w n="17.8">personne</w></l>
					<l n="18" num="1.18"><space unit="char" quantity="12"></space><w n="18.1">Sur</w> <w n="18.2">ses</w> <w n="18.3">tables</w> <w n="18.4">d</w>’<w n="18.5">airain</w>.</l>
				</lg>
					<closer>
						<note id="none" type="footnote">▪ VÉNUS<lb></lb>▪ ÉLISE<lb></lb>▪ NIZAM<lb></lb>▪ USAGE<lb></lb>▪ SEMER</note>
					</closer>
			</div></body></text></TEI>