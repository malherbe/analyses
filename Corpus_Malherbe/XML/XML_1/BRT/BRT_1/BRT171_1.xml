<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ÉNIGMES</head><head type="sub_1">BOTANIQUE<lb></lb>(<hi rend="smallcap">EMBLÈMES</hi>.)</head><div type="poem" key="BRT171">
				<head type="number">XXI</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Je</w> <w n="1.2">suis</w> <w n="1.3">un</w> <w n="1.4">point</w> <w n="1.5">perdu</w> <w n="1.6">des</w> <w n="1.7">Vosges</w></l>
					<l n="2" num="1.2"><w n="2.1">Et</w> <w n="2.2">n</w>’<w n="2.3">ai</w> <w n="2.4">pas</w> <w n="2.5">d</w>’<w n="2.6">autres</w> <w n="2.7">ornements</w></l>
					<l n="3" num="1.3"><w n="3.1">Que</w> <w n="3.2">les</w> <w n="3.3">convolvulus</w>, <w n="3.4">les</w> <w n="3.5">sauges</w></l>
					<l n="4" num="1.4"><w n="4.1">Et</w> <w n="4.2">la</w> <w n="4.3">vigne</w> <w n="4.4">aux</w> <w n="4.5">noueux</w> <w n="4.6">sarments</w>.</l>
					<l n="5" num="1.5"><w n="5.1">L</w>’<w n="5.2">humble</w> <w n="5.3">clocher</w> <w n="5.4">de</w> <w n="5.5">mon</w> <w n="5.6">église</w>,</l>
					<l n="6" num="1.6"><w n="6.1">Le</w> <w n="6.2">curé</w> <w n="6.3">qui</w> <w n="6.4">m</w>’<w n="6.5">évangélise</w>,</l>
					<l n="7" num="1.7"><w n="7.1">Aux</w> <w n="7.2">grands</w> <w n="7.3">demeurent</w> <w n="7.4">inconnus</w> ;</l>
					<l n="8" num="1.8"><w n="8.1">Mes</w> <w n="8.2">princesses</w> <w n="8.3">sont</w> <w n="8.4">des</w> <w n="8.5">fermières</w> ;</l>
					<l n="9" num="1.9"><w n="9.1">Parfois</w> <w n="9.2">l</w>’<w n="9.3">on</w> <w n="9.4">trouve</w> <w n="9.5">en</w> <w n="9.6">mes</w> <w n="9.7">chaumières</w></l>
					<l n="10" num="1.10"><w n="10.1">Huches</w> <w n="10.2">vides</w> <w n="10.3">et</w> <w n="10.4">foyers</w> <w n="10.5">nus</w>…</l>
					<l n="11" num="1.11"><w n="11.1">Pourtant</w> <w n="11.2">l</w>’<w n="11.3">on</w> <w n="11.4">me</w> <w n="11.5">jalouse</w> <w n="11.6">en</w> <w n="11.7">France</w></l>
					<l n="12" num="1.12"><w n="12.1">Depuis</w> <w n="12.2">un</w> <w n="12.3">âge</w> <w n="12.4">reculé</w>,</l>
					<l n="13" num="1.13"><w n="13.1">Car</w>, <w n="13.2">sous</w> <w n="13.3">mon</w> <w n="13.4">chaume</w>, <w n="13.5">prit</w> <w n="13.6">naissance</w></l>
					<l n="14" num="1.14"><w n="14.1">La</w> <w n="14.2">vierge</w> <w n="14.3">au</w> <w n="14.4">glaive</w> <w n="14.5">immaculé</w>.</l>
				</lg>
					<closer>
						<note type="footnote" id="none">▪ Domrémy.</note>
					</closer>
			</div></body></text></TEI>