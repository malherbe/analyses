<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PROVERBES</head><div type="poem" key="BRT86">
				<head type="number">LXXXVI</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Vite</w>, <w n="1.2">vite</w> ! <w n="1.3">un</w> <w n="1.4">peu</w> <w n="1.5">moins</w> <w n="1.6">de</w> <w n="1.7">temps</w> <w n="1.8">à</w> <w n="1.9">la</w> <w n="1.10">toilette</w> !</l>
					<l n="2" num="1.2"><w n="2.1">Quand</w> <w n="2.2">nous</w> <w n="2.3">allons</w> <w n="2.4">dîner</w> <w n="2.5">chez</w> <w n="2.6">notre</w> <w n="2.7">tante</w> <w n="2.8">Annette</w>,</l>
					<l n="3" num="1.3"><space unit="char" quantity="8"></space><w n="3.1">L</w>’<w n="3.2">exactitude</w> <w n="3.3">est</w> <w n="3.4">de</w> <w n="3.5">rigueur</w>.</l>
					<l n="4" num="1.4"><w n="4.1">J</w>’<w n="4.2">en</w> <w n="4.3">suis</w> <w n="4.4">sûre</w>, <w n="4.5">déjà</w> <w n="4.6">son</w> <w n="4.7">couvert</w> <w n="4.8">étincelle</w> :</l>
					<l n="5" num="1.5"><w n="5.1">La</w> <w n="5.2">vieille</w> <w n="5.3">argenterie</w> <w n="5.4">et</w> <w n="5.5">la</w> <w n="5.6">blanche</w> <w n="5.7">vaisselle</w></l>
					<l n="6" num="1.6"><space unit="char" quantity="8"></space><w n="6.1">Que</w> <w n="6.2">décorent</w> <w n="6.3">le</w> <w n="6.4">fruit</w>, <w n="6.5">la</w> <w n="6.6">fleur</w>.</l>
					<l n="7" num="1.7"><w n="7.1">Elle</w> <w n="7.2">a</w> <w n="7.3">choisi</w> <w n="7.4">le</w> <w n="7.5">vin</w> <w n="7.6">des</w> <w n="7.7">meilleures</w> <w n="7.8">années</w> ;</l>
					<l n="8" num="1.8"><w n="8.1">Elle</w> <w n="8.2">a</w> <w n="8.3">fait</w> <w n="8.4">de</w> <w n="8.5">gâteaux</w> <w n="8.6">trois</w> <w n="8.7">ou</w> <w n="8.8">quatre</w> <w n="8.9">fournées</w></l>
					<l n="9" num="1.9"><space unit="char" quantity="8"></space><w n="9.1">Et</w> <w n="9.2">dépeuplé</w> <w n="9.3">sa</w> <w n="9.4">basse</w>-<w n="9.5">cour</w>.</l>
					<l n="10" num="1.10"><w n="10.1">Le</w> <w n="10.2">linge</w> <w n="10.3">de</w> <w n="10.4">Hollande</w> <w n="10.5">est</w> <w n="10.6">tiré</w> <w n="10.7">de</w> <w n="10.8">l</w>’<w n="10.9">armoire</w> ;</l>
					<l n="11" num="1.11"><w n="11.1">Elle</w> <w n="11.2">met</w> <w n="11.3">sa</w> <w n="11.4">perruque</w> <w n="11.5">et</w> <w n="11.6">sa</w> <w n="11.7">robe</w> <w n="11.8">de</w> <w n="11.9">moire</w>,</l>
					<l n="12" num="1.12"><space unit="char" quantity="12"></space><w n="12.1">Vrai</w> <w n="12.2">costume</w> <w n="12.3">de</w> <w n="12.4">cour</w> !</l>
					<l n="13" num="1.13"><w n="13.1">Il</w> <w n="13.2">nous</w> <w n="13.3">faut</w> <w n="13.4">à</w> <w n="13.5">l</w>’<w n="13.6">envi</w>, <w n="13.7">vraiment</w> <w n="13.8">cela</w> <w n="13.9">s</w>’<w n="13.10">impose</w>,</l>
					<l n="14" num="1.14"><w n="14.1">Entonner</w> <w n="14.2">sa</w> <w n="14.3">louange</w> <w n="14.4">en</w> <w n="14.5">poésie</w>, <w n="14.6">en</w> <w n="14.7">prose</w>,</l>
					<l n="15" num="1.15"><space unit="char" quantity="8"></space><w n="15.1">Avec</w> <w n="15.2">l</w>’<w n="15.3">estomac</w> <w n="15.4">et</w> <w n="15.5">le</w> <w n="15.6">cœur</w> !</l>
					<l n="16" num="1.16"><w n="16.1">Et</w> <w n="16.2">boire</w> <w n="16.3">tant</w> <w n="16.4">de</w> <w n="16.5">fois</w> <w n="16.6">en</w> <w n="16.7">l</w>’<w n="16.8">honneur</w> <w n="16.9">de</w> <w n="16.10">la</w> <w n="16.11">tante</w></l>
					<l n="17" num="1.17"><w n="17.1">Que</w>, <w n="17.2">du</w> <w n="17.3">catarrhe</w> <w n="17.4">aigu</w>, <w n="17.5">de</w> <w n="17.6">la</w> <w n="17.7">fièvre</w> <w n="17.8">latente</w>,</l>
					<l n="18" num="1.18"><space unit="char" quantity="8"></space><w n="18.1">Son</w> <w n="18.2">grand</w> <w n="18.3">âge</w> <w n="18.4">reste</w> <w n="18.5">vainqueur</w> ! »</l>
				</lg>
					<closer>
						<note type="footnote" id="none">▪ Quand le vin est tiré, il faut le boire.</note>
					</closer>
			</div></body></text></TEI>