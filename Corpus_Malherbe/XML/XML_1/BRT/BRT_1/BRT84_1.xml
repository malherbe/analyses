<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PROVERBES</head><div type="poem" key="BRT84">
				<head type="number">LXXXIV</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Le</w> <w n="1.2">professeur</w> <w n="1.3">s</w>’<w n="1.4">écrie</w> <w n="1.5">en</w> <w n="1.6">courroux</w> : « <w n="1.7">Ma</w> <w n="1.8">science</w></l>
					<l n="2" num="1.2"><space unit="char" quantity="8"></space><w n="2.1">N</w>’<w n="2.2">est</w> <w n="2.3">que</w> <w n="2.4">perles</w> <w n="2.5">devant</w> <w n="2.6">pourceaux</w>.</l>
					<l n="3" num="1.3"><w n="3.1">À</w> <w n="3.2">la</w> <w n="3.3">fin</w>, <w n="3.4">je</w> <w n="3.5">me</w> <w n="3.6">trouve</w> <w n="3.7">à</w> <w n="3.8">bout</w> <w n="3.9">de</w> <w n="3.10">patience</w> !</l>
					<l n="4" num="1.4"><w n="4.1">Ânes</w>, <w n="4.2">vous</w> <w n="4.3">n</w>’<w n="4.4">êtes</w> <w n="4.5">bons</w> <w n="4.6">qu</w>’<w n="4.7">à</w> <w n="4.8">boire</w> <w n="4.9">dans</w> <w n="4.10">des</w> <w n="4.11">seaux</w> !</l>
					<l n="5" num="1.5"><w n="5.1">Je</w> <w n="5.2">dépense</w> <w n="5.3">pour</w> <w n="5.4">vous</w> <w n="5.5">le</w> <w n="5.6">meilleur</w> <w n="5.7">de</w> <w n="5.8">ma</w> <w n="5.9">vie</w>,</l>
					<l n="6" num="1.6"><space unit="char" quantity="8"></space><w n="6.1">Et</w> <w n="6.2">j</w>’<w n="6.3">en</w> <w n="6.4">atteste</w> <w n="6.5">ici</w> <w n="6.6">le</w> <w n="6.7">ciel</w> !</l>
					<l n="7" num="1.7"><w n="7.1">Écoliers</w> <w n="7.2">paresseux</w>, <w n="7.3">rien</w> <w n="7.4">ne</w> <w n="7.5">vous</w> <w n="7.6">justifie</w> :</l>
					<l n="8" num="1.8"><w n="8.1">Vous</w> <w n="8.2">changeriez</w> <w n="8.3">pour</w> <w n="8.4">moi</w> <w n="8.5">les</w> <w n="8.6">meilleurs</w> <w n="8.7">vins</w> <w n="8.8">en</w> <w n="8.9">fiel</w>.</l>
					<l n="9" num="1.9"><w n="9.1">Eh</w> <w n="9.2">bien</w> ! <w n="9.3">continuez</w> <w n="9.4">de</w> <w n="9.5">déserter</w> <w n="9.6">la</w> <w n="9.7">classe</w> ;</l>
					<l n="10" num="1.10"><space unit="char" quantity="8"></space><w n="10.1">Restez</w> <w n="10.2">busons</w>, <w n="10.3">cancres</w>, <w n="10.4">vauriens</w> ;</l>
					<l n="11" num="1.11"><w n="11.1">Salement</w> <w n="11.2">croupissez</w> <w n="11.3">dans</w> <w n="11.4">l</w>’<w n="11.5">ignorance</w> <w n="11.6">crasse</w> ! …</l>
					<l n="12" num="1.12"><w n="12.1">Chacun</w> <w n="12.2">son</w> <w n="12.3">goût</w>, <w n="12.4">messieurs</w>, <w n="12.5">et</w> <w n="12.6">chacun</w> <w n="12.7">ses</w> <w n="12.8">moyens</w>. »</l>
				</lg>
					<closer>
						<note type="footnote" id="none">▪ La fin justifie les moyens.</note>
					</closer>
			</div></body></text></TEI>