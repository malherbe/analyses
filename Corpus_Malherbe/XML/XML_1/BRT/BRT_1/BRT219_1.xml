<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">HOMONYMES</head><div type="poem" key="BRT219">
				<head type="number">XLIV</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">A</w> <w n="1.2">l</w>’<w n="1.3">école</w> <w n="1.4">de</w> <subst type="completion" reason="analysis" hand="ML"><del>...</del><add rend="hidden"><w n="1.5">tir</w></add></subst> <w n="1.6">il</w> <w n="1.7">avait</w> <w n="1.8">fait</w> <w n="1.9">merveille</w></l>
					<l n="2" num="1.2"><w n="2.1">Et</w> <w n="2.2">se</w> <w n="2.3">croyait</w> <w n="2.4">chasseur</w> <w n="2.5">plus</w> <w n="2.6">adroit</w> <w n="2.7">que</w> <w n="2.8">pas</w> <w n="2.9">un</w> !</l>
					<l n="3" num="1.3"><w n="3.1">Il</w> <w n="3.2">s</w>’<w n="3.3">en</w> <w n="3.4">allait</w> <w n="3.5">donc</w> <w n="3.6">fier</w>, <w n="3.7">la</w> <w n="3.8">toque</w> <w n="3.9">sur</w> <w n="3.10">l</w>’<w n="3.11">oreille</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Foulant</w> <w n="4.2">à</w> <w n="4.3">pas</w> <w n="4.4">pressés</w> <w n="4.5">le</w> <w n="4.6">pâtural</w> <w n="4.7">commun</w>.</l>
					<l n="5" num="1.5"><w n="5.1">Près</w> <w n="5.2">de</w> <w n="5.3">là</w>, <w n="5.4">des</w> <w n="5.5">soldats</w>, <w n="5.6">l</w>’<w n="5.7">œil</w> <w n="5.8">fixe</w>, <w n="5.9">la</w> <w n="5.10">main</w> <w n="5.11">ferme</w>,</l>
					<l n="6" num="1.6"><w n="6.1">S</w>’<w n="6.2">exerçaient</w> <w n="6.3">sur</w> <w n="6.4">le</w> <subst type="completion" reason="analysis" hand="ML"><del>...</del><add rend="hidden"><w n="6.5">tir</w></add></subst> <w n="6.6">tapissé</w> <w n="6.7">de</w> <w n="6.8">gazon</w>.</l>
					<l n="7" num="1.7"><w n="7.1">Il</w> <w n="7.2">passa</w> <w n="7.3">dédaigneux</w>, <w n="7.4">tourna</w> <w n="7.5">la</w> <w n="7.6">vieille</w> <w n="7.7">ferme</w></l>
					<l n="8" num="1.8"><w n="8.1">Et</w>, <w n="8.2">gourmandant</w> <w n="8.3">son</w> <w n="8.4">chien</w>, <w n="8.5">consulta</w> <w n="8.6">l</w>’<w n="8.7">horizon</w>.</l>
					<l n="9" num="1.9"><w n="9.1">Rien</w> ! <w n="9.2">et</w> <w n="9.3">Finaut</w> <w n="9.4">battait</w> <w n="9.5">en</w> <w n="9.6">vain</w> <w n="9.7">chaque</w> <w n="9.8">broussaille</w></l>
					<l n="10" num="1.10"><w n="10.1">Depuis</w> <w n="10.2">le</w> <w n="10.3">grand</w> <w n="10.4">matin</w> ! <w n="10.5">Le</w> <w n="10.6">chasseur</w> <w n="10.7">devint</w> <w n="10.8">las</w></l>
					<l n="11" num="1.11"><w n="11.1">Et</w> <w n="11.2">son</w> <w n="11.3">attention</w>, <w n="11.4">comme</w> <w n="11.5">un</w> <w n="11.6">train</w> <w n="11.7">qui</w> <w n="11.8">déraille</w>,</l>
					<l n="12" num="1.12"><w n="12.1">Distraite</w>, <w n="12.2">à</w> <subst type="completion" reason="analysis" hand="ML"><del>....</del><add rend="hidden"><w n="12.3">tire</w></add></subst> <w n="12.4">d</w>’<w n="12.5">aile</w>, <w n="12.6">au</w> <w n="12.7">loin</w> <w n="12.8">s</w>’<w n="12.9">enfuit</w>, <w n="12.10">hélas</w> !</l>
					<l n="13" num="1.13"><w n="13.1">Son</w> <w n="13.2">esprit</w>, <w n="13.3">remontant</w> <w n="13.4">d</w>’<w n="13.5">âge</w> <w n="13.6">en</w> <w n="13.7">âge</w> <w n="13.8">au</w> <w n="13.9">vieux</w> <w n="13.10">monde</w>,</l>
					<l n="14" num="1.14"><w n="14.1">Carthage</w>, <w n="14.2">Rome</w> <subst type="completion" reason="analysis" hand="ML"><del>...</del><add rend="hidden"><w n="14.3">Tyr</w></add></subst> <w n="14.4">lui</w> <w n="14.5">contaient</w> <w n="14.6">leurs</w> <w n="14.7">secrets</w>.</l>
					<l n="15" num="1.15"><w n="15.1">Et</w> <w n="15.2">pendant</w> <w n="15.3">ce</w> <w n="15.4">temps</w>-<w n="15.5">là</w>, <w n="15.6">le</w> <w n="15.7">chien</w> <w n="15.8">faisant</w> <w n="15.9">sa</w> <w n="15.10">ronde</w>,</l>
					<l n="16" num="1.16"><w n="16.1">Allait</w> <w n="16.2">de</w> <w n="16.3">faute</w> <w n="16.4">en</w> <w n="16.5">faute</w> <w n="16.6">et</w> <w n="16.7">manquait</w> <w n="16.8">ses</w> <w n="16.9">arrêts</w> !</l>
					<l n="17" num="1.17"><w n="17.1">Mais</w>, <w n="17.2">réveillé</w> <w n="17.3">soudain</w> <w n="17.4">par</w> <w n="17.5">un</w> <w n="17.6">bruit</w> <w n="17.7">sur</w> <w n="17.8">la</w> <w n="17.9">branche</w>,</l>
					<l n="18" num="1.18"><w n="18.1">Et</w>, <w n="18.2">sans</w> <w n="18.3">même</w> <w n="18.4">viser</w>, <subst type="completion" reason="analysis" hand="ML"><del>....</del><add rend="hidden"><w n="18.5">tire</w></add></subst> <w n="18.6">alors</w> <w n="18.7">le</w> <w n="18.8">songeur</w>.</l>
					<l n="19" num="1.19"><w n="19.1">Qui</w> <w n="19.2">tombe</w> <w n="19.3">de</w> <w n="19.4">là</w>-<w n="19.5">haut</w> ? <w n="19.6">C</w>’<w n="19.7">est</w> <w n="19.8">une</w> <w n="19.9">chatte</w> <w n="19.10">blanche</w></l>
					<l n="20" num="1.20"><w n="20.1">Qu</w>’<w n="20.2">à</w> <w n="20.3">la</w> <w n="20.4">ferme</w> <w n="20.5">prochaine</w> <w n="20.6">on</w> <w n="20.7">va</w> <w n="20.8">pleurer</w>. <w n="20.9">Horreur</w> !</l>
					<l n="21" num="1.21"><w n="21.1">Le</w> <w n="21.2">meurtrier</w> <w n="21.3">se</w> <w n="21.4">trouble</w> <w n="21.5">et</w> <w n="21.6">fait</w> <w n="21.7">piteuse</w> <w n="21.8">mine</w></l>
					<l n="22" num="1.22"><w n="22.1">Devant</w> <w n="22.2">ce</w> <w n="22.3">corps</w> <w n="22.4">sans</w> <w n="22.5">vie</w> <w n="22.6">et</w> <w n="22.7">ce</w> <w n="22.8">poil</w> <w n="22.9">rebroussé</w>.</l>
					<l n="23" num="1.23"><w n="23.1">Comme</w> <w n="23.2">il</w> <w n="23.3">préférerait</w>, <w n="23.4">dans</w> <w n="23.5">son</w> <w n="23.6">humeur</w> <w n="23.7">chagrine</w>,</l>
					<l n="24" num="1.24"><w n="24.1">Qu</w>’<w n="24.2">un</w> <w n="24.3">voleur</w> <w n="24.4">à</w> <w n="24.5">la</w> <subst type="completion" reason="analysis" hand="ML"><del>....</del><add rend="hidden"><w n="24.6">tire</w></add></subst> <w n="24.7">eût</w> <w n="24.8">vidé</w> <w n="24.9">son</w> <w n="24.10">gousset</w> !</l>
				</lg>
					<closer>
						<note type="footnote" id="none">▪ Tir, tir, tire, Tyr, tire, tire.</note>
					</closer>
			</div></body></text></TEI>