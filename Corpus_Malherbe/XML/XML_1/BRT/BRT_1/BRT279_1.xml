<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">SONNETS-PORTRAITS</head><div type="poem" key="BRT279">
				<head type="number">IV</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Il</w> <w n="1.2">déchire</w> <w n="1.3">un</w> <w n="1.4">lion</w> <w n="1.5">sans</w> <w n="1.6">effort</w> <w n="1.7">et</w> <w n="1.8">sans</w> <w n="1.9">peine</w> ;</l>
					<l n="2" num="1.2"><w n="2.1">Incendie</w> <w n="2.2">en</w> <w n="2.3">jouant</w> <w n="2.4">le</w> <w n="2.5">blé</w> <w n="2.6">du</w> <w n="2.7">Philistin</w> ;</l>
					<l n="3" num="1.3"><w n="3.1">Court</w> <w n="3.2">aux</w> <w n="3.3">rudes</w> <w n="3.4">combats</w> <w n="3.5">comme</w> <w n="3.6">un</w> <w n="3.7">autre</w> <w n="3.8">au</w> <w n="3.9">festin</w> ;</l>
					<l n="4" num="1.4"><w n="4.1">Et</w>, <w n="4.2">tout</w> <w n="4.3">à</w> <w n="4.4">ses</w> <w n="4.5">exploits</w>, <w n="4.6">ne</w> <w n="4.7">reprend</w> <w n="4.8">pas</w> <w n="4.9">haleine</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Gaza</w> <w n="5.2">tremble</w> <w n="5.3">à</w> <w n="5.4">sa</w> <w n="5.5">voix</w> <w n="5.6">du</w> <w n="5.7">soir</w> <w n="5.8">jusqu</w>’<w n="5.9">au</w> <w n="5.10">matin</w>…</l>
					<l n="6" num="2.2"><w n="6.1">Mais</w> <w n="6.2">le</w> <w n="6.3">poison</w> <w n="6.4">d</w>’<w n="6.5">amour</w> <w n="6.6">s</w>’<w n="6.7">infiltre</w> <w n="6.8">dans</w> <w n="6.9">sa</w> <w n="6.10">veine</w> :</l>
					<l n="7" num="2.3"><w n="7.1">Il</w> <w n="7.2">boit</w> <w n="7.3">imprudemment</w> <w n="7.4">à</w> <w n="7.5">sa</w> <w n="7.6">coupe</w> <w n="7.7">trop</w> <w n="7.8">pleine</w></l>
					<l n="8" num="2.4"><w n="8.1">Et</w> <w n="8.2">le</w> <w n="8.3">fort</w> <w n="8.4">devient</w> <w n="8.5">faible</w>. <w n="8.6">Humiliant</w> <w n="8.7">destin</w> !</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Pourtant</w> <w n="9.2">son</w> <w n="9.3">dernier</w> <w n="9.4">râle</w> <w n="9.5">est</w> <w n="9.6">comme</w> <w n="9.7">un</w> <w n="9.8">vent</w> <w n="9.9">d</w>’<w n="9.10">orage</w></l>
					<l n="10" num="3.2"><w n="10.1">Et</w> <w n="10.2">son</w> <w n="10.3">bras</w> <w n="10.4">désarmé</w> <w n="10.5">venge</w> <w n="10.6">un</w> <w n="10.7">suprême</w> <w n="10.8">outrage</w></l>
					<l n="11" num="3.3"><w n="11.1">En</w> <w n="11.2">écrasant</w> <w n="11.3">les</w> <w n="11.4">nains</w> <w n="11.5">qui</w> <w n="11.6">se</w> <w n="11.7">riaient</w> <w n="11.8">de</w> <w n="11.9">lui</w> ! …</l>
				</lg>
				<lg n="4">
					<l n="12" num="4.1"><w n="12.1">Les</w> <w n="12.2">hommes</w> <w n="12.3">de</w> <w n="12.4">nos</w> <w n="12.5">temps</w> <w n="12.6">ont</w> <w n="12.7">pareille</w> <w n="12.8">faiblesse</w>…</l>
					<l n="13" num="4.2"><w n="13.1">Mais</w> <w n="13.2">lequel</w> <w n="13.3">se</w> <w n="13.4">relève</w> <w n="13.5">avec</w> <w n="13.6">cette</w> <w n="13.7">noblesse</w> ? …</l>
					<l n="14" num="4.3"><w n="14.1">Lequel</w> <w n="14.2">saurait</w> <w n="14.3">mourir</w> <w n="14.4">en</w> <w n="14.5">vainqueur</w> <w n="14.6">aujourd</w>’<w n="14.7">hui</w> ?</l>
				</lg>
					<closer>
						<note id="none" type="footnote">▪ Samson.</note>
					</closer>
			</div></body></text></TEI>