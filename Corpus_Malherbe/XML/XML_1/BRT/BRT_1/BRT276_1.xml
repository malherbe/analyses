<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">SONNETS-PORTRAITS</head><div type="poem" key="BRT276">
				<head type="number">I</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Sa</w> <w n="1.2">vague</w> <w n="1.3">silhouette</w> <w n="1.4">émerge</w> <w n="1.5">d</w>’<w n="1.6">un</w> <w n="1.7">nuage</w></l>
					<l n="2" num="1.2"><w n="2.1">Dès</w> <w n="2.2">le</w> <w n="2.3">matin</w> <w n="2.4">confus</w> <w n="2.5">du</w> <w n="2.6">vieux</w> <w n="2.7">monde</w> <w n="2.8">au</w> <w n="2.9">berceau</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Quand</w>, <w n="3.2">pour</w> <w n="3.3">ses</w> <w n="3.4">descendants</w>, <w n="3.5">nul</w> <w n="3.6">peintre</w> <w n="3.7">et</w> <w n="3.8">nul</w> <w n="3.9">pinceau</w></l>
					<l n="4" num="1.4"><w n="4.1">Ne</w> <w n="4.2">pouvaient</w> <w n="4.3">reproduire</w> <w n="4.4">encore</w> <w n="4.5">son</w> <w n="4.6">visage</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Et</w> <w n="5.2">pourtant</w> <w n="5.3">l</w>’<w n="5.4">on</w> <w n="5.5">connaît</w> <w n="5.6">le</w> <w n="5.7">verdoyant</w> <w n="5.8">arceau</w></l>
					<l n="6" num="2.2"><w n="6.1">Qui</w>, <w n="6.2">sur</w> <w n="6.3">elle</w>, <w n="6.4">versa</w> <w n="6.5">l</w>’<w n="6.6">attrait</w> <w n="6.7">de</w> <w n="6.8">son</w> <w n="6.9">ombrage</w> ;</l>
					<l n="7" num="2.3"><w n="7.1">On</w> <w n="7.2">nomme</w> <w n="7.3">l</w>’<w n="7.4">amer</w> <w n="7.5">fruit</w> <w n="7.6">caché</w> <w n="7.7">dans</w> <w n="7.8">ce</w> <w n="7.9">feuillage</w> ;</l>
					<l n="8" num="2.4"><w n="8.1">On</w> <w n="8.2">sait</w> <w n="8.3">de</w> <w n="8.4">quels</w> <w n="8.5">destins</w> <w n="8.6">l</w>’<w n="8.7">arbre</w> <w n="8.8">portait</w> <w n="8.9">le</w> <w n="8.10">sceau</w> ! …</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Un</w> <w n="9.2">jour</w> <w n="9.3">elle</w> <w n="9.4">y</w> <w n="9.5">cueillait</w> <w n="9.6">la</w> <w n="9.7">mort</w> <w n="9.8">et</w> <w n="9.9">même</w> <w n="9.10">pire</w></l>
					<l n="10" num="3.2"><w n="10.1">Pour</w> <w n="10.2">ses</w> <w n="10.3">filles</w>, <w n="10.4">hélas</w> ! <w n="10.5">et</w> <w n="10.6">chacune</w> <w n="10.7">en</w> <w n="10.8">soupire</w> :</l>
					<l n="11" num="3.3"><w n="11.1">Déclin</w>, <w n="11.2">rides</w>, <w n="11.3">pâleur</w>, <w n="11.4">fausses</w> <w n="11.5">dents</w>, <w n="11.6">faux</w> <w n="11.7">cheveux</w> ! !</l>
				</lg>
				<lg n="4">
					<l n="12" num="4.1"><w n="12.1">Tout</w> <w n="12.2">cela</w> <w n="12.3">nous</w> <w n="12.4">vient</w> <w n="12.5">d</w>’<w n="12.6">elle</w>, <w n="12.7">et</w> <w n="12.8">du</w> <w n="12.9">fond</w> <w n="12.10">de</w> <w n="12.11">votre</w> <w n="12.12">âme</w>,</l>
					<l n="13" num="4.2"><w n="13.1">Vous</w> <w n="13.2">jetez</w> <w n="13.3">l</w>’<w n="13.4">anathème</w> <w n="13.5">à</w> <w n="13.6">ses</w> <w n="13.7">torts</w>, <w n="13.8">ô</w> <w n="13.9">madame</w> !</l>
					<l n="14" num="4.3"><w n="14.1">Mais</w> <w n="14.2">dites</w>… <w n="14.3">À</w> <w n="14.4">sa</w> <w n="14.5">place</w>… <w n="14.6">auriez</w>-<w n="14.7">vous</w> <w n="14.8">donc</w> <w n="14.9">fait</w> <w n="14.10">mieux</w> ?</l>
				</lg>
					<closer>
						<note id="none" type="footnote">▪ Ève.</note>
					</closer>
			</div></body></text></TEI>