<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">MOTS CARRÉS</head><div type="poem" key="BRT247">
				<head type="number">XXII</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Enfant</w> <w n="1.2">du</w> <w n="1.3">vieux</w> <w n="1.4">Cahors</w> <w n="1.5">et</w> <w n="1.6">valet</w> <w n="1.7">d</w>’<w n="1.8">une</w> <w n="1.9">reine</w>,</l>
					<l n="2" num="1.2"><subst type="completion" reason="analysis" hand="ML"><del>.....</del><add rend="hidden"><w n="2.1">Marot</w></add></subst>, <w n="2.2">le</w> <w n="2.3">fin</w> <w n="2.4">poète</w>, <w n="2.5">eut</w> <w n="2.6">des</w> <w n="2.7">destins</w> <w n="2.8">divers</w> :</l>
					<l n="3" num="1.3"><w n="3.1">Il</w> <w n="3.2">connut</w> <w n="3.3">tour</w> <w n="3.4">à</w> <w n="3.5">tour</w> <w n="3.6">et</w> <w n="3.7">la</w> <w n="3.8">gloire</w> <w n="3.9">sereine</w></l>
					<l n="4" num="1.4"><w n="4.1">Et</w> <w n="4.2">l</w>’<subst type="completion" reason="analysis" hand="ML"><del>.....</del><add rend="hidden"><w n="4.3">aride</w></add></subst> <w n="4.4">sentier</w> <w n="4.5">sous</w> <w n="4.6">les</w> <w n="4.7">âpres</w> <w n="4.8">hivers</w>.</l>
					<l n="5" num="1.5"><w n="5.1">Le</w> <w n="5.2">bonheur</w> <w n="5.3">de</w> <subst type="completion" reason="analysis" hand="ML"><del>.....</del><add rend="hidden"><w n="5.4">rimer</w></add></subst> <w n="5.5">lui</w> <w n="5.6">fit</w> <w n="5.7">des</w> <w n="5.8">jours</w> <w n="5.9">sans</w> <w n="5.10">ombre</w></l>
					<l n="6" num="1.6"><w n="6.1">Et</w> <w n="6.2">l</w>’<subst type="completion" reason="analysis" hand="ML"><del>.....</del><add rend="hidden"><w n="6.3">odeur</w></add></subst> <w n="6.4">de</w> <w n="6.5">l</w>’<w n="6.6">encens</w> <w n="6.7">lui</w> <w n="6.8">plut</w> <w n="6.9">à</w> <w n="6.10">respirer</w>.</l>
					<l n="7" num="1.7"><w n="7.1">Il</w> <w n="7.2">sommeille</w> <w n="7.3">sous</w> <subst type="completion" reason="analysis" hand="ML"><del>.....</del><add rend="hidden"><w n="7.4">terre</w></add></subst>, <w n="7.5">et</w> <w n="7.6">ses</w> <w n="7.7">œuvres</w> <w n="7.8">en</w> <w n="7.9">nombre</w></l>
					<l n="8" num="1.8"><w n="8.1">Par</w> <w n="8.2">nos</w> <w n="8.3">siècles</w> <w n="8.4">nouveaux</w> <w n="8.5">se</w> <w n="8.6">laissent</w> <w n="8.7">admirer</w>.</l>
				</lg>
					<closer>
						<note type="footnote" id="none">▪ MAROT<lb></lb>▪ ARIDE<lb></lb>▪ RIMER<lb></lb>▪ ODEUR<lb></lb>▪ TERRE</note>
					</closer>
			</div></body></text></TEI>