<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">MOTS EN TRIANGLES</head><div type="poem" key="BRT270">
				<head type="number">XX</head>
					<div type="section" n="1">
					<head type="number">1.</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Il</w> <w n="1.2">sert</w> <w n="1.3">à</w> <w n="1.4">préparer</w> <w n="1.5">le</w> <w n="1.6">verdâtre</w> <w n="1.7">jus</w> <w n="1.8">d</w>’<w n="1.9">herbe</w></l>
					<l n="2" num="1.2"><w n="2.1">Qui</w> <w n="2.2">coule</w>, <w n="2.3">goutte</w> <w n="2.4">à</w> <w n="2.5">goutte</w>, <w n="2.6">à</w> <w n="2.7">son</w> <w n="2.8">rude</w> <w n="2.9">contact</w>.</l>
				</lg>
					</div>
					<div type="section" n="2">
					<head type="number">2.</head>
				<lg n="1">
					<l n="3" num="1.1"><w n="3.1">Walter</w> <w n="3.2">Scott</w> <w n="3.3">en</w> <w n="3.4">a</w> <w n="3.5">fait</w> <w n="3.6">un</w> <w n="3.7">montagnard</w> <w n="3.8">superbe</w>,</l>
					<l n="4" num="1.2"><w n="4.1">Héros</w> <w n="4.2">plus</w> <w n="4.3">valeureux</w> <w n="4.4">que</w> <w n="4.5">pourvu</w> <w n="4.6">d</w>’<w n="4.7">un</w> <w n="4.8">grand</w> <w n="4.9">tact</w>.</l>
				</lg>
					</div>
					<div type="section" n="3">
					<head type="number">3.</head>
				<lg n="1">
					<l n="5" num="1.1"><w n="5.1">Nul</w> <w n="5.2">ne</w> <w n="5.3">la</w> <w n="5.4">voit</w>. <w n="5.5">Pourtant</w>, <w n="5.6">tous</w> <w n="5.7">en</w> <w n="5.8">sont</w> <w n="5.9">tributaires</w> :</l>
					<l n="6" num="1.2"><w n="6.1">Des</w> <w n="6.2">méchants</w>, <w n="6.3">c</w>’<w n="6.4">est</w> <w n="6.5">le</w> <w n="6.6">frein</w> ; <w n="6.7">des</w> <w n="6.8">faibles</w>, <w n="6.9">c</w>’<w n="6.10">est</w> <w n="6.11">l</w>’<w n="6.12">appui</w>.</l>
				</lg>
					</div>
					<div type="section" n="4">
					<head type="number">4.</head>
				<lg n="1">
					<l n="7" num="1.1"><w n="7.1">On</w> <w n="7.2">trouve</w> <w n="7.3">à</w> <w n="7.4">ses</w> <w n="7.5">attraits</w> <w n="7.6">peu</w> <w n="7.7">d</w>’<w n="7.8">hommes</w> <w n="7.9">réfractaires</w>…</l>
					<l n="8" num="1.2"><w n="8.1">Hélas</w> ! <w n="8.2">c</w>’<w n="8.3">est</w> <w n="8.4">le</w> <w n="8.5">faux</w> <w n="8.6">dieu</w> <w n="8.7">qu</w>’<w n="8.8">on</w> <w n="8.9">encense</w> <w n="8.10">aujourd</w>’<w n="8.11">hui</w>.</l>
				</lg>
					</div>
					<div type="section" n="5">
					<head type="number">5.</head>
				<lg n="1">
					<l n="9" num="1.1"><w n="9.1">En</w> <w n="9.2">l</w>’<w n="9.3">alphabet</w> <w n="9.4">il</w> <w n="9.5">fait</w> <w n="9.6">figure</w></l>
					<l n="10" num="1.2"><w n="10.1">Et</w> <w n="10.2">se</w> <w n="10.3">trouve</w> <w n="10.4">dans</w>… <w n="10.5">signature</w>.</l>
				</lg>
					</div>
						<closer>
						<note id="none" type="footnote">▪ Pilon<lb></lb>▪ Ivor<lb></lb>▪ loi<lb></lb>▪ or<lb></lb>▪ n</note>
					</closer>
			</div></body></text></TEI>