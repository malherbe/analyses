<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">HOMONYMES</head><div type="poem" key="BRT212">
				<head type="number">XXXVII</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Que</w> <w n="1.2">chacun</w> <w n="1.3">à</w> <w n="1.4">son</w> <w n="1.5">gré</w> <w n="1.6">se</w> <subst hand="ML" reason="analysis" type="completion"><del>.....</del><add rend="hidden"><w n="1.7">carre</w></add></subst> <w n="1.8">sur</w> <w n="1.9">son</w> <w n="1.10">siège</w></l>
					<l n="2" num="1.2"><subst hand="ML" reason="analysis" type="completion"><del>...</del><add rend="hidden"><w n="2.1">Car</w></add></subst> <w n="2.2">mon</w> <w n="2.3">discours</w> <w n="2.4">commence</w> <w n="2.5">et</w> <w n="2.6">je</w> <w n="2.7">parle</w> <w n="2.8">longtemps</w>.</l>
					<l n="3" num="1.3"><w n="3.1">Je</w> <w n="3.2">vous</w> <w n="3.3">préviens</w> <w n="3.4">d</w>’<w n="3.5">avance</w> <w n="3.6">et</w> <w n="3.7">ne</w> <w n="3.8">tends</w> <w n="3.9">pas</w> <w n="3.10">de</w> <w n="3.11">piége</w> ;</l>
					<l n="4" num="1.4"><w n="4.1">Tant</w> <w n="4.2">pis</w> <w n="4.3">pour</w> <w n="4.4">les</w> <w n="4.5">grincheux</w> <w n="4.6">et</w> <w n="4.7">pour</w> <w n="4.8">les</w> <w n="4.9">mécontents</w> !</l>
					<l n="5" num="1.5"><w n="5.1">M</w>’<w n="5.2">écoutez</w>-<w n="5.3">vous</w> ? <w n="5.4">Oui</w>-<w n="5.5">dà</w> ! <w n="5.6">le</w> <w n="5.7">joueur</w> <w n="5.8">de</w> <w n="5.9">bouillotte</w></l>
					<l n="6" num="1.6"><w n="6.1">A</w> <w n="6.2">retiré</w> <w n="6.3">sa</w> <subst hand="ML" reason="analysis" type="completion"><del>.....</del><add rend="hidden"><w n="6.4">carre</w></add></subst>, <w n="6.5">ô</w> <w n="6.6">mémorable</w> <w n="6.7">trait</w> !</l>
					<l n="7" num="1.7"><w n="7.1">Le</w> <w n="7.2">cordonnier</w> <w n="7.3">suspend</w> <w n="7.4">la</w> <subst hand="ML" reason="analysis" type="completion"><del>.....</del><add rend="hidden"><w n="7.5">carre</w></add></subst> <w n="7.6">d</w>’<w n="7.7">une</w> <w n="7.8">botte</w> ;</l>
					<l n="8" num="1.8"><w n="8.1">Et</w> <w n="8.2">celle</w> <w n="8.3">d</w>’<w n="8.4">un</w> <w n="8.5">habit</w> <w n="8.6">laisse</w> <w n="8.7">un</w> <w n="8.8">tailleur</w> <w n="8.9">distrait</w>.</l>
					<l n="9" num="1.9"><w n="9.1">De</w> <w n="9.2">même</w> <w n="9.3">au</w> <w n="9.4">chapelier</w> <w n="9.5">la</w> <subst hand="ML" reason="analysis" type="completion"><del>.....</del><add rend="hidden"><w n="9.6">carre</w></add></subst> <w n="9.7">de</w> <w n="9.8">sa</w> <w n="9.9">forme</w> ;</l>
					<l n="10" num="1.10"><w n="10.1">De</w> <w n="10.2">même</w> <w n="10.3">à</w> <w n="10.4">l</w>’<w n="10.5">Auvergnat</w> <w n="10.6">la</w> <subst hand="ML" reason="analysis" type="completion"><del>.....</del><add rend="hidden"><w n="10.7">carre</w></add></subst> <w n="10.8">du</w> <w n="10.9">chaudron</w> ;</l>
					<l n="11" num="1.11"><w n="11.1">Et</w>, <w n="11.2">quittant</w> <w n="11.3">à</w> <w n="11.4">son</w> <w n="11.5">tour</w> <w n="11.6">sa</w> <subst hand="ML" reason="analysis" type="completion"><del>......</del><add rend="hidden"><w n="11.7">quarre</w></add></subst> <w n="11.8">au</w> <w n="11.9">bord</w> <w n="11.10">énorme</w>,</l>
					<l n="12" num="1.12"><w n="12.1">L</w>’<w n="12.2">ouvrier</w> <w n="12.3">des</w> <w n="12.4">pécheurs</w> <w n="12.5">vient</w>, <w n="12.6">s</w>’<w n="12.7">essuyant</w> <w n="12.8">le</w> <w n="12.9">front</w> !</l>
					<l n="13" num="1.13"><w n="13.1">Mais</w> <w n="13.2">ce</w> <w n="13.3">n</w>’<w n="13.4">est</w> <w n="13.5">point</w> <w n="13.6">assez</w> : <w n="13.7">J</w>’<w n="13.8">appelle</w> <w n="13.9">pour</w> <w n="13.10">m</w>’<w n="13.11">entendre</w></l>
					<l n="14" num="1.14"><w n="14.1">Le</w> <w n="14.2">soldat</w> <w n="14.3">amateur</w> <w n="14.4">du</w> <w n="14.5">bouquet</w> <w n="14.6">de</w> <w n="14.7">son</w> <subst hand="ML" reason="analysis" type="completion"><del>.....</del><add rend="hidden"><w n="14.8">quart</w></add></subst></l>
					<l n="15" num="1.15"><w n="15.1">Le</w> <w n="15.2">marin</w> <w n="15.3">qui</w>, <w n="15.4">joyeux</w>, <w n="15.5">à</w> <w n="15.6">terre</w> <w n="15.7">va</w> <w n="15.8">descendre</w></l>
					<l n="16" num="1.16"><w n="16.1">Trouvant</w> <w n="16.2">très</w> <w n="16.3">à</w> <w n="16.4">propos</w> <w n="16.5">d</w>’<w n="16.6">avoir</w> <w n="16.7">fini</w> <w n="16.8">son</w> <subst hand="ML" reason="analysis" type="completion"><del>.....</del><add rend="hidden"><w n="16.9">quart</w></add></subst></l>
					<l n="17" num="1.17"><w n="17.1">À</w> <w n="17.2">d</w>’<w n="17.3">autres</w> ! <w n="17.4">Approchez</w> ! <w n="17.5">je</w> <w n="17.6">n</w>’<w n="17.7">ai</w> <w n="17.8">point</w> <w n="17.9">le</w> <subst hand="ML" reason="analysis" type="completion"><del>.....</del><add rend="hidden"><w n="17.10">quart</w></add></subst> <w n="17.11">même</w></l>
					<l n="18" num="1.18"><w n="18.1">Des</w> <w n="18.2">auditeurs</w> <w n="18.3">nombreux</w> <w n="18.4">que</w> <w n="18.5">j</w>’<w n="18.6">appelle</w> <w n="18.7">à</w> <w n="18.8">l</w>’<w n="18.9">envi</w> :</l>
					<l n="19" num="1.19"><w n="19.1">Je</w> <w n="19.2">veux</w> <w n="19.3">la</w> <w n="19.4">qualité</w> ! <w n="19.5">la</w> <w n="19.6">fine</w> <w n="19.7">fleur</w> ! <w n="19.8">la</w> <w n="19.9">crème</w> :</l>
					<l n="20" num="1.20"><w n="20.1">Banville</w>, <subst hand="ML" reason="analysis" type="completion"><del>....</del><add rend="hidden"><w n="20.2">Karr</w></add></subst>, <w n="20.3">Dumas</w>, <w n="20.4">Deroulède</w>, <w n="20.5">Halévy</w> !</l>
					<l n="21" num="1.21"><w n="21.1">C</w>’<w n="21.2">est</w> <w n="21.3">trop</w> <w n="21.4">peu</w> <w n="21.5">des</w> <w n="21.6">vivants</w> : <w n="21.7">À</w> <w n="21.8">moi</w>, <w n="21.9">morts</w> <w n="21.10">de</w> <w n="21.11">l</w>’<w n="21.12">histoire</w>,</l>
					<l n="22" num="1.22"><w n="22.1">Que</w> <w n="22.2">vois</w>-<w n="22.3">je</w> ? <w n="22.4">Robert</w> <subst hand="ML" reason="analysis" type="completion"><del>....</del><add rend="hidden"><w n="22.5">Carr</w></add></subst>, <w n="22.6">l</w>’<w n="22.7">empoisonneur</w> <w n="22.8">anglais</w> !</l>
					<l n="23" num="1.23"><w n="23.1">Mais</w> <w n="23.2">que</w> <w n="23.3">voulais</w>-<w n="23.4">je</w> <w n="23.5">dire</w> <w n="23.6">à</w> <w n="23.7">ce</w> <w n="23.8">vaste</w> <w n="23.9">auditoire</w> ?</l>
					<l n="24" num="1.24"><w n="24.1">Je</w> <w n="24.2">ne</w> <w n="24.3">m</w>’<w n="24.4">en</w> <w n="24.5">souviens</w> <w n="24.6">plus</w> : <w n="24.7">mes</w> <w n="24.8">esprits</w> <w n="24.9">sont</w> <w n="24.10">troublés</w> !</l>
				</lg>
					<closer>
						<note id="none" type="footnote">▪ Carre, car, carre, carre, carre, quarre, quarre, quart, quart, Karr, Carr.</note>
					</closer>
			</div></body></text></TEI>