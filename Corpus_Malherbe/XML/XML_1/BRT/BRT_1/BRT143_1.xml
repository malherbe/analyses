<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CHARADES</head><div type="poem" key="BRT143">
				<head type="number">XLIII</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Quelques</w> <w n="1.2">fous</w>, <w n="1.3">m</w>’<w n="1.4">a</w>-<w n="1.5">t</w>-<w n="1.6">on</w> <w n="1.7">dit</w>, <w n="1.8">et</w> <w n="1.9">vraiment</w> <w n="1.10">c</w>’<w n="1.11">est</w> <w n="1.12">possible</w>,</l>
					<l n="2" num="1.2"><w n="2.1">En</w> <w n="2.2">ont</w> <w n="2.3">fait</w> <w n="2.4">dans</w> <w n="2.5">la</w> <w n="2.6">lune</w> ! <w n="2.7">eh</w> <w n="2.8">bien</w>, <w n="2.9">tant</w> <w n="2.10">pis</w> <w n="2.11">pour</w> <w n="2.12">eux</w> !</l>
					<l n="3" num="1.3"><w n="3.1">Je</w> <w n="3.2">me</w> <w n="3.3">contente</w>, <w n="3.4">moi</w>, <w n="3.5">d</w>’<w n="3.6">en</w> <w n="3.7">faire</w> <w n="3.8">dans</w> <w n="3.9">la</w> <w n="3.10">cible</w>.</l>
					<l n="4" num="1.4"><w n="4.1">Celui</w>-<w n="4.2">ci</w> <w n="4.3">n</w>’<w n="4.4">est</w> <w n="4.5">qu</w>’<w n="4.6">adroit</w> ; <w n="4.7">mais</w> <w n="4.8">l</w>’<w n="4.9">autre</w> <w n="4.10">est</w> <w n="4.11">dangereux</w>.</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Tel</w> <w n="5.2">a</w> <w n="5.3">la</w> <w n="5.4">sienne</w> <w n="5.5">lisse</w> <w n="5.6">et</w> <w n="5.7">blanche</w> <w n="5.8">et</w> <w n="5.9">satinée</w> ;</l>
					<l n="6" num="2.2"><w n="6.1">Tel</w> <w n="6.2">autre</w>, <w n="6.3">à</w> <w n="6.4">rudes</w> <w n="6.5">grains</w> <w n="6.6">et</w> <w n="6.7">couleur</w> <w n="6.8">de</w> <w n="6.9">corbeau</w>.</l>
					<l n="7" num="2.3"><w n="7.1">Cependant</w>, <w n="7.2">qu</w>’<w n="7.3">elle</w> <w n="7.4">soit</w> <w n="7.5">glabre</w>, <w n="7.6">fraîche</w> <w n="7.7">ou</w> <w n="7.8">tannée</w>,</l>
					<l n="8" num="2.4"><w n="8.1">Chacun</w> <w n="8.2">se</w> <w n="8.3">trouve</w> <w n="8.4">bien</w>, <w n="8.5">chacun</w> <w n="8.6">se</w> <w n="8.7">trouve</w> <w n="8.8">beau</w>.</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">J</w>’<w n="9.2">aime</w>, <w n="9.3">à</w> <w n="9.4">travers</w> <w n="9.5">la</w> <w n="9.6">lande</w>, <w n="9.7">en</w> <w n="9.8">sa</w> <w n="9.9">fantasque</w> <w n="9.10">marche</w>,</l>
					<l n="10" num="3.2"><w n="10.1">Ou</w> <w n="10.2">ses</w> <w n="10.3">folâtres</w> <w n="10.4">bonds</w> <w n="10.5">ou</w> <w n="10.6">sa</w> <w n="10.7">lourde</w> <w n="10.8">lenteur</w>…</l>
					<l n="11" num="3.3"><w n="11.1">Je</w> <w n="11.2">vois</w> <w n="11.3">en</w> <w n="11.4">rêve</w> <w n="11.5">alors</w> <w n="11.6">Laban</w>, <w n="11.7">le</w> <w n="11.8">patriarche</w>,</l>
					<l n="12" num="3.4"><w n="12.1">Et</w> <w n="12.2">Jacob</w> <w n="12.3">endormi</w> <w n="12.4">dans</w> <w n="12.5">un</w> <w n="12.6">espoir</w> <w n="12.7">menteur</w>.</l>
				</lg>
					<closer>
						<note id="none" type="footnote">▪ Trou-peau.</note>
					</closer>
			</div></body></text></TEI>