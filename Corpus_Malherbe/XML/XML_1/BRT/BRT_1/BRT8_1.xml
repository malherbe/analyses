<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PROVERBES</head><div type="poem" key="BRT8">
				<head type="number">VIII</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">À</w> <w n="1.2">l</w>’<w n="1.3">ombre</w> <w n="1.4">du</w> <w n="1.5">manoir</w> <w n="1.6">une</w> <w n="1.7">foule</w> <w n="1.8">accourue</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Aux</w> <w n="2.2">jeux</w> <w n="2.3">naïfs</w> <w n="2.4">du</w> <w n="2.5">temps</w> <w n="2.6">s</w>’<w n="2.7">adonne</w> <w n="2.8">et</w> <w n="2.9">s</w>’<w n="2.10">évertue</w> ;</l>
					<l n="3" num="1.3"><w n="3.1">Et</w>, <w n="3.2">dans</w> <w n="3.3">la</w> <w n="3.4">cour</w> <w n="3.5">d</w>’<w n="3.6">honneur</w>, <w n="3.7">les</w> <w n="3.8">juges</w> <w n="3.9">du</w> <w n="3.10">tournoi</w></l>
					<l n="4" num="1.4"><w n="4.1">Vont</w> <w n="4.2">rendre</w> <w n="4.3">leur</w> <w n="4.4">verdict</w> <w n="4.5">et</w> <w n="4.6">proclamer</w> <w n="4.7">le</w> <w n="4.8">roi</w>.</l>
					<l n="5" num="1.5"><w n="5.1">Le</w> <w n="5.2">front</w> <w n="5.3">ceint</w> <w n="5.4">de</w> <w n="5.5">rougeur</w>, <w n="5.6">plus</w> <w n="5.7">d</w>’<w n="5.8">une</w> <w n="5.9">haute</w> <w n="5.10">dame</w>,</l>
					<l n="6" num="1.6"><w n="6.1">En</w> <w n="6.2">battant</w> <w n="6.3">des</w> <w n="6.4">deux</w> <w n="6.5">mains</w>, <w n="6.6">d</w>’<w n="6.7">avance</w> <w n="6.8">le</w> <w n="6.9">proclame</w>…</l>
					<l n="7" num="1.7"><w n="7.1">Mais</w> <w n="7.2">lui</w>, <w n="7.3">la</w> <w n="7.4">lance</w> <w n="7.5">au</w> <w n="7.6">poing</w>, <w n="7.7">modeste</w> <w n="7.8">autant</w> <w n="7.9">que</w> <w n="7.10">preux</w>,</l>
					<l n="8" num="1.8"><w n="8.1">Craint</w> <w n="8.2">encor</w> <w n="8.3">du</w> <w n="8.4">hasard</w> <w n="8.5">la</w> <w n="8.6">malice</w> <w n="8.7">et</w> <w n="8.8">les</w> <w n="8.9">jeux</w>…</l>
					<l n="9" num="1.9"><w n="9.1">Enfin</w>, <w n="9.2">son</w> <w n="9.3">nom</w> <w n="9.4">résonne</w> <w n="9.5">et</w> <w n="9.6">sa</w> <w n="9.7">gloire</w> <w n="9.8">est</w> <w n="9.9">connue</w></l>
					<l n="10" num="1.10"><w n="10.1">Couvert</w> <w n="10.2">de</w> <w n="10.3">son</w> <w n="10.4">armure</w>, <w n="10.5">il</w> <w n="10.6">reçoit</w>, <w n="10.7">tête</w> <w n="10.8">nue</w>,</l>
					<l n="11" num="1.11"><w n="11.1">Le</w> <w n="11.2">laurier</w> <w n="11.3">du</w> <w n="11.4">vainqueur</w> <w n="11.5">taillé</w> <w n="11.6">dans</w> <w n="11.7">l</w>’<w n="11.8">or</w> <w n="11.9">massif</w>.</l>
					<l n="12" num="1.12"><w n="12.1">Nobles</w>, <w n="12.2">vilains</w>, <w n="12.3">chacun</w> <w n="12.4">l</w>’<w n="12.5">envie</w> <w n="12.6">et</w> <w n="12.7">le</w> <w n="12.8">regarde</w> ;</l>
					<l n="13" num="1.13"><w n="13.1">Mais</w> <w n="13.2">il</w> <w n="13.3">se</w> <w n="13.4">sent</w> <w n="13.5">plus</w> <w n="13.6">fier</w> <w n="13.7">du</w> <w n="13.8">geste</w> <w n="13.9">que</w> <w n="13.10">hasarde</w></l>
					<l n="14" num="1.14"><w n="14.1">Sa</w> <w n="14.2">blonde</w> <w n="14.3">fiancée</w> <w n="14.4">au</w> <w n="14.5">sourire</w> <w n="14.6">expressif</w>.</l>
				</lg>
					<closer>
						<note type="footnote" id="none">▪ Jeux de mains, jeux de vilains.</note>
					</closer>
			</div></body></text></TEI>