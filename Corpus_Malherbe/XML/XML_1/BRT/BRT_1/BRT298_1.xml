<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">SONNETS-PORTRAITS</head><div type="poem" key="BRT298">
				<head type="number">XXIII</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Son</w> <w n="1.2">père</w>, <w n="1.3">honnête</w> <w n="1.4">Belge</w>, <w n="1.5">est</w> <w n="1.6">homme</w> <w n="1.7">de</w> <w n="1.8">charrue</w> ;</l>
					<l n="2" num="1.2"><w n="2.1">Mais</w> <w n="2.2">elle</w> <w n="2.3">prise</w> <w n="2.4">peu</w> <w n="2.5">la</w> <w n="2.6">sainte</w> <w n="2.7">paix</w> <w n="2.8">des</w> <w n="2.9">champs</w>.</l>
					<l n="3" num="1.3"><w n="3.1">Il</w> <w n="3.2">lui</w> <w n="3.3">faut</w> <w n="3.4">le</w> <w n="3.5">plaisir</w> <w n="3.6">et</w> <w n="3.7">l</w>’<w n="3.8">orgie</w> <w n="3.9">et</w> <w n="3.10">ses</w> <w n="3.11">chants</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Et</w> <w n="4.2">le</w> <w n="4.3">contact</w> <w n="4.4">impur</w> <w n="4.5">des</w> <w n="4.6">fanges</w> <w n="4.7">de</w> <w n="4.8">la</w> <w n="4.9">rue</w> !</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Elle</w> <w n="5.2">cède</w> <w n="5.3">sans</w> <w n="5.4">lutte</w> <w n="5.5">à</w> <w n="5.6">ses</w> <w n="5.7">fougueux</w> <w n="5.8">penchants</w>.</l>
					<l n="6" num="2.2"><w n="6.1">La</w> <w n="6.2">pente</w> <w n="6.3">qui</w> <w n="6.4">l</w>’<w n="6.5">entraîne</w> <w n="6.6">est</w> <w n="6.7">vite</w> <w n="6.8">parcourue</w> ;</l>
					<l n="7" num="2.3"><w n="7.1">Elle</w> <w n="7.2">parle</w>… <w n="7.3">et</w> <w n="7.4">Paris</w> <w n="7.5">aux</w> <w n="7.6">clubs</w> <w n="7.7">ouverts</w> <w n="7.8">se</w> <w n="7.9">rue</w>.</l>
					<l n="8" num="2.4"><w n="8.1">Et</w> <w n="8.2">les</w> <w n="8.3">mauvais</w> <w n="8.4">encor</w> <w n="8.5">se</w> <w n="8.6">sentent</w> <w n="8.7">plus</w> <w n="8.8">méchants</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Et</w>, <w n="9.2">par</w> <w n="9.3">elle</w> <w n="9.4">inspiré</w>, <w n="9.5">le</w> <w n="9.6">tribunal</w> <w n="9.7">des</w> <w n="9.8">crimes</w>,</l>
					<l n="10" num="3.2"><w n="10.1">D</w>’<w n="10.2">heure</w> <w n="10.3">en</w> <w n="10.4">heure</w> <w n="10.5">atteignant</w> <w n="10.6">de</w> <w n="10.7">nouvelles</w> <w n="10.8">victimes</w>,</l>
					<l n="11" num="3.3"><w n="11.1">Signe</w> <w n="11.2">pour</w> <w n="11.3">l</w>’<w n="11.4">échafaud</w> <w n="11.5">son</w> <w n="11.6">horrible</w> <w n="11.7">exeat</w> ! …</l>
				</lg>
				<lg n="4">
					<l n="12" num="4.1"><w n="12.1">Mais</w> <w n="12.2">ce</w> <w n="12.3">rouge</w> <w n="12.4">cerveau</w> <w n="12.5">se</w> <w n="12.6">détraque</w> <w n="12.7">et</w> <w n="12.8">délire</w>.</l>
					<l n="13" num="4.2"><w n="13.1">A</w> <w n="13.2">la</w> <w n="13.3">Salpêtrière</w>, <w n="13.4">en</w> <w n="13.5">hurlant</w>, <w n="13.6">elle</w> <w n="13.7">expire</w>,</l>
					<l n="14" num="4.3"><w n="14.1">Et</w> <w n="14.2">laisse</w> <w n="14.3">un</w> <w n="14.4">nom</w> <w n="14.5">maudit</w> ! … « <w n="14.6">Jupiter</w> <w n="14.7">dementat</w> ! »</l>
				</lg>
					<closer>
						<note type="footnote" id="none">▪ Théroigne de Méricourt.</note>
					</closer>
			</div></body></text></TEI>