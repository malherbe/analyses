<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">SONNETS-PORTRAITS</head><div type="poem" key="BRT277">
				<head type="number">II</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Il</w> <w n="1.2">avait</w> <w n="1.3">vu</w> <w n="1.4">son</w> <w n="1.5">père</w>, <w n="1.6">ô</w> <w n="1.7">merveille</w> ! <w n="1.8">ô</w> <w n="1.9">prodige</w> !</l>
					<l n="2" num="1.2"><w n="2.1">S</w>’<w n="2.2">élever</w> <w n="2.3">dans</w> <w n="2.4">le</w> <w n="2.5">ciel</w>, <w n="2.6">épargné</w> <w n="2.7">par</w> <w n="2.8">la</w> <w n="2.9">mort</w> !</l>
					<l n="3" num="1.3"><w n="3.1">Il</w> <w n="3.2">avait</w> <w n="3.3">vu</w> <w n="3.4">son</w> <w n="3.5">fils</w>, <w n="3.6">comme</w> <w n="3.7">lui</w> <w n="3.8">grand</w> <w n="3.9">et</w> <w n="3.10">fort</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Vieillir</w> <w n="4.2">sans</w> <w n="4.3">se</w> <w n="4.4">courber</w> <w n="4.5">sous</w> <w n="4.6">les</w> <w n="4.7">ans</w>. <w n="4.8">Mais</w>, <w n="4.9">que</w> <w n="4.10">dis</w>-<w n="4.11">je</w> ?</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Lui</w>-<w n="5.2">même</w>, <w n="5.3">sur</w> <w n="5.4">sa</w> <w n="5.5">tête</w>, <w n="5.6">a</w> <w n="5.7">porté</w> <w n="5.8">sans</w> <w n="5.9">effort</w></l>
					<l n="6" num="2.2"><w n="6.1">Neuf</w> <w n="6.2">siècles</w>, <w n="6.3">comme</w> <w n="6.4">un</w> <w n="6.5">cèdre</w> <w n="6.6">à</w> <w n="6.7">l</w>’<w n="6.8">inflexible</w> <w n="6.9">tige</w> !</l>
					<l n="7" num="2.3"><w n="7.1">Et</w>, <w n="7.2">du</w> <w n="7.3">haut</w> <w n="7.4">de</w> <w n="7.5">cet</w> <w n="7.6">âge</w>, <w n="7.7">en</w> <w n="7.8">son</w> <w n="7.9">lointain</w> <w n="7.10">prestige</w>,</l>
					<l n="8" num="2.4"><w n="8.1">Il</w> <w n="8.2">a</w> <w n="8.3">noble</w> <w n="8.4">attitude</w> <w n="8.5">et</w> <w n="8.6">majestueux</w> <w n="8.7">port</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Ses</w> <w n="9.2">descendants</w> <w n="9.3">déchus</w> <w n="9.4">ne</w> <w n="9.5">lui</w> <w n="9.6">ressemblent</w> <w n="9.7">guère</w> :</l>
					<l n="10" num="3.2"><w n="10.1">La</w> <w n="10.2">fièvre</w> <w n="10.3">de</w> <w n="10.4">l</w>’<w n="10.5">orgueil</w>, <w n="10.6">de</w> <w n="10.7">l</w>’<w n="10.8">or</w> <w n="10.9">et</w> <w n="10.10">du</w> <w n="10.11">plaisir</w></l>
					<l n="11" num="3.3"><w n="11.1">Aide</w> <w n="11.2">la</w> <w n="11.3">pâle</w> <w n="11.4">mort</w> <w n="11.5">si</w> <w n="11.6">prompte</w> <w n="11.7">à</w> <w n="11.8">les</w> <w n="11.9">saisir</w>…</l>
				</lg>
				<lg n="4">
					<l n="12" num="4.1"><w n="12.1">Eux</w>-<w n="12.2">mêmes</w> <w n="12.3">à</w> <w n="12.4">leurs</w> <w n="12.5">jours</w> <w n="12.6">font</w> <w n="12.7">une</w> <w n="12.8">absurde</w> <w n="12.9">guerre</w> ;</l>
					<l n="13" num="4.2"><w n="13.1">Et</w>, <w n="13.2">de</w> <w n="13.3">presser</w> <w n="13.4">le</w> <w n="13.5">temps</w>, <w n="13.6">ne</w> <w n="13.7">se</w> <w n="13.8">lassent</w> <w n="13.9">jamais</w>.</l>
					<l n="14" num="4.3"><w n="14.1">C</w>’<w n="14.2">est</w> <w n="14.3">vivre</w> <w n="14.4">assez</w>, <w n="14.5">dit</w>-<w n="14.6">on</w>, <w n="14.7">que</w> <w n="14.8">de</w> <w n="14.9">bien</w> <w n="14.10">vivre</w>. <w n="14.11">Mais</w>…</l>
				</lg>
					<closer>
						<note id="none" type="footnote">▪ Mathusalem.</note>
					</closer>
			</div></body></text></TEI>