<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CHARADES</head><div type="poem" key="BRT102">
				<head type="number">II</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">C</w>’<w n="1.2">était</w> <w n="1.3">jour</w> <w n="1.4">de</w> <w n="1.5">malheur</w> : <w n="1.6">Paul</w> <w n="1.7">fut</w> <w n="1.8">en</w> <w n="1.9">pénitence</w></l>
					<l n="2" num="1.2"><space unit="char" quantity="8"></space><w n="2.1">Pour</w> <w n="2.2">certaine</w> <w n="2.3">conjonction</w></l>
					<l n="3" num="1.3"><w n="3.1">Qu</w>’<w n="3.2">il</w> <w n="3.3">prit</w> <w n="3.4">nonchalamment</w> <w n="3.5">pour</w> <w n="3.6">préposition</w>.</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="2">
					<l n="4" num="2.1"><w n="4.1">Pierre</w> <w n="4.2">fut</w>, <w n="4.3">au</w> <w n="4.4">salon</w>, <w n="4.5">corrigé</w> <w n="4.6">d</w>’<w n="4.7">importance</w></l>
					<l n="5" num="2.2"><space unit="char" quantity="8"></space><w n="5.1">Par</w> <w n="5.2">la</w> <w n="5.3">faute</w> <w n="5.4">d</w>’<w n="5.5">un</w> <w n="5.6">encrier</w></l>
					<l n="6" num="2.3"><w n="6.1">Qui</w> <w n="6.2">noircit</w> <w n="6.3">pour</w> <w n="6.4">toujours</w> <w n="6.5">un</w> <w n="6.6">fragment</w> <w n="6.7">de</w> <w n="6.8">clavier</w>.</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="3">
					<l n="7" num="3.1"><w n="7.1">Et</w> <w n="7.2">Jacques</w>, <w n="7.3">dérobant</w> <w n="7.4">dans</w> <w n="7.5">une</w> <w n="7.6">cartouchière</w></l>
					<l n="8" num="3.2"><space unit="char" quantity="8"></space><w n="8.1">Un</w> <w n="8.2">étroit</w> <w n="8.3">rouleau</w> <w n="8.4">de</w> <w n="8.5">métal</w>,</l>
					<l n="9" num="3.3"><w n="9.1">Étourdiment</w> <w n="9.2">faillit</w>, <w n="9.3">dans</w> <w n="9.4">son</w> <w n="9.5">ardeur</w> <w n="9.6">guerrière</w>,</l>
					<l n="10" num="3.4"><space unit="char" quantity="8"></space><w n="10.1">Causer</w> <w n="10.2">un</w> <w n="10.3">accident</w> <w n="10.4">fatal</w>.</l>
				</lg>
					<closer>
						<note type="footnote" id="none">▪ Car-touche.</note>
					</closer>
			</div></body></text></TEI>