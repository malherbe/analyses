<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PROVERBES</head><div type="poem" key="BRT91">
				<head type="number">XCI</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Entre</w> <w n="1.2">tous</w> <w n="1.3">les</w> <w n="1.4">géants</w> <w n="1.5">de</w> <w n="1.6">la</w> <w n="1.7">sylviculture</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Se</w> <w n="2.2">distingue</w> <w n="2.3">le</w> <w n="2.4">chêne</w> <w n="2.5">à</w> <w n="2.6">la</w> <w n="2.7">forte</w> <w n="2.8">structure</w>.</l>
				</lg>
				<lg n="2">
					<l n="3" num="2.1"><space unit="char" quantity="8"></space><w n="3.1">C</w>’<w n="3.2">est</w> <w n="3.3">l</w>’<w n="3.4">arbre</w> <w n="3.5">aimé</w> <w n="3.6">de</w> <w n="3.7">nos</w> <w n="3.8">aïeux</w> :</l>
					<l n="4" num="2.2"><space unit="char" quantity="8"></space><w n="4.1">Sa</w> <w n="4.2">racine</w> <w n="4.3">plonge</w> <w n="4.4">profonde</w>,</l>
					<l n="5" num="2.3"><space unit="char" quantity="8"></space><w n="5.1">Son</w> <w n="5.2">faîte</w> <w n="5.3">se</w> <w n="5.4">perd</w> <w n="5.5">dans</w> <w n="5.6">les</w> <w n="5.7">cieux</w>,</l>
					<l n="6" num="2.4"><space unit="char" quantity="8"></space><w n="6.1">Et</w> <w n="6.2">ses</w> <w n="6.3">rameaux</w> <w n="6.4">semblent</w> <w n="6.5">un</w> <w n="6.6">monde</w>.</l>
					<l n="7" num="2.5"><space unit="char" quantity="8"></space><w n="7.1">L</w>’<w n="7.2">écorce</w> <w n="7.3">en</w> <w n="7.4">est</w> <w n="7.5">rude</w> <w n="7.6">au</w> <w n="7.7">toucher</w> ;</l>
					<l n="8" num="2.6"><space unit="char" quantity="8"></space><w n="8.1">Le</w> <w n="8.2">tronc</w>, <w n="8.3">noueux</w> ; <w n="8.4">la</w> <w n="8.5">feuille</w>, <w n="8.6">sombre</w>.</l>
					<l n="9" num="2.7"><space unit="char" quantity="8"></space><w n="9.1">Quand</w> <w n="9.2">le</w> <w n="9.3">soleil</w> <w n="9.4">va</w> <w n="9.5">se</w> <w n="9.6">coucher</w>,</l>
					<l n="10" num="2.8"><space unit="char" quantity="8"></space><w n="10.1">L</w>’<w n="10.2">arbre</w> <w n="10.3">entier</w> <w n="10.4">semble</w> <w n="10.5">un</w> <w n="10.6">gouffre</w> <w n="10.7">d</w>’<w n="10.8">ombre</w>.</l>
					<l n="11" num="2.9"><space unit="char" quantity="8"></space><w n="11.1">Il</w> <w n="11.2">ne</w> <w n="11.3">craint</w> <w n="11.4">pas</w> <w n="11.5">les</w> <w n="11.6">fiers</w> <w n="11.7">autans</w>,</l>
					<l n="12" num="2.10"><space unit="char" quantity="8"></space><w n="12.1">Ni</w> <w n="12.2">les</w> <w n="12.3">hivers</w> <w n="12.4">aux</w> <w n="12.5">froides</w> <w n="12.6">bises</w>.</l>
					<l n="13" num="2.11"><space unit="char" quantity="8"></space><w n="13.1">Il</w> <w n="13.2">brave</w> <w n="13.3">les</w> <w n="13.4">efforts</w> <w n="13.5">du</w> <w n="13.6">temps</w>,</l>
					<l n="14" num="2.12"><space unit="char" quantity="8"></space><w n="14.1">Plus</w> <w n="14.2">ferme</w> <w n="14.3">que</w> <w n="14.4">les</w> <w n="14.5">roches</w> <w n="14.6">grises</w>.</l>
					<l n="15" num="2.13"><space unit="char" quantity="8"></space><w n="15.1">Au</w> <w n="15.2">chasseur</w> <w n="15.3">fatigué</w>, <w n="15.4">meurtri</w>,</l>
					<l n="16" num="2.14"><space unit="char" quantity="8"></space><w n="16.1">Fuyant</w> <w n="16.2">devant</w> <w n="16.3">les</w> <w n="16.4">avalanches</w>,</l>
					<l n="17" num="2.15"><space unit="char" quantity="8"></space><w n="17.1">S</w>’<w n="17.2">il</w> <w n="17.3">faut</w> <w n="17.4">un</w> <w n="17.5">refuge</w>, <w n="17.6">un</w> <w n="17.7">abri</w>,</l>
					<l n="18" num="2.16"><space unit="char" quantity="8"></space><w n="18.1">Le</w> <w n="18.2">chêne</w> <w n="18.3">étend</w> <w n="18.4">ses</w> <w n="18.5">vastes</w> <w n="18.6">branches</w>.</l>
					<l n="19" num="2.17"><space unit="char" quantity="8"></space><w n="19.1">Il</w> <w n="19.2">n</w>’<w n="19.3">est</w> <w n="19.4">pas</w>, <w n="19.5">sous</w> <w n="19.6">son</w> <w n="19.7">dôme</w> <w n="19.8">vert</w>,</l>
					<l n="20" num="2.18"><space unit="char" quantity="8"></space><w n="20.1">Une</w> <w n="20.2">rustique</w> <w n="20.3">fagoteuse</w></l>
					<l n="21" num="2.19"><space unit="char" quantity="8"></space><w n="21.1">Qui</w> <w n="21.2">n</w>’<w n="21.3">aime</w> <w n="21.4">à</w> <w n="21.5">se</w> <w n="21.6">mettre</w> <w n="21.7">à</w> <w n="21.8">couvert</w></l>
					<l n="22" num="2.20"><space unit="char" quantity="8"></space><w n="22.1">Les</w> <w n="22.2">pieds</w> <w n="22.3">dans</w> <w n="22.4">la</w> <w n="22.5">mousse</w> <w n="22.6">laineuse</w>.</l>
					<l n="23" num="2.21"><space unit="char" quantity="8"></space><w n="23.1">Il</w> <w n="23.2">est</w> <w n="23.3">encor</w> <w n="23.4">vivant</w> <w n="23.5">et</w> <w n="23.6">droit</w></l>
					<l n="24" num="2.22"><space unit="char" quantity="8"></space><w n="24.1">Malgré</w> <w n="24.2">les</w> <w n="24.3">siècles</w> <w n="24.4">sur</w> <w n="24.5">sa</w> <w n="24.6">tête</w> !</l>
					<l n="25" num="2.23"><space unit="char" quantity="8"></space><w n="25.1">Mais</w> <w n="25.2">Dieu</w> <w n="25.3">l</w>’<w n="25.4">a</w> <w n="25.5">marqué</w> <w n="25.6">de</w> <w n="25.7">son</w> <w n="25.8">doigt</w> :</l>
					<l n="26" num="2.24"><space unit="char" quantity="8"></space><w n="26.1">Du</w> <w n="26.2">bûcheron</w> <w n="26.3">la</w> <w n="26.4">hache</w> <w n="26.5">est</w> <w n="26.6">prête</w>.</l>
				</lg>
					<closer>
						<note type="footnote" id="none">▪ Entre l’arbre et l’écorce, il ne faut pas mettre le doigt.</note>
					</closer>
			</div></body></text></TEI>