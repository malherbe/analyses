<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">HOMONYMES</head><div type="poem" key="BRT192">
				<head type="number">XVII</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Par</w> <w n="1.2">son</w> <w n="1.3">fait</w>, <w n="1.4">on</w> <w n="1.5">a</w> <w n="1.6">vu</w> <w n="1.7">plus</w> <w n="1.8">d</w>’<w n="1.9">un</w> <w n="1.10">estropié</w> !</l>
					<l n="2" num="1.2"><w n="2.1">Une</w> <w n="2.2">femme</w> <w n="2.3">coquette</w> <w n="2.4">et</w> <w n="2.5">posant</w> <w n="2.6">pour</w> <w n="2.7">le</w> <w n="2.8">pied</w></l>
					<l n="3" num="1.3"><space unit="char" quantity="12"></space><w n="3.1">En</w> <w n="3.2">souffre</w> <w n="3.3">sans</w> <w n="3.4">se</w> <w n="3.5">plaindre</w>.</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="2">
					<l n="4" num="2.1"><w n="4.1">L</w>’<w n="4.2">âme</w>, <w n="4.3">enfermée</w> <w n="4.4">en</w> <w n="4.5">lui</w>, <w n="4.6">voudrait</w> <w n="4.7">s</w>’<w n="4.8">en</w> <w n="4.9">affranchir</w>.</l>
					<l n="5" num="2.2"><w n="5.1">Dans</w> <w n="5.2">ses</w> <w n="5.3">étroits</w> <w n="5.4">liens</w>, <w n="5.5">elle</w> <w n="5.6">se</w> <w n="5.7">sent</w> <w n="5.8">fléchir</w>,</l>
					<l n="6" num="2.3"><space unit="char" quantity="12"></space><w n="6.1">Emprisonner</w>, <w n="6.2">étreindre</w> !</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="3">
					<l n="7" num="3.1"><w n="7.1">Il</w> <w n="7.2">a</w> <w n="7.3">sonné</w> <w n="7.4">joyeux</w> <w n="7.5">plus</w> <w n="7.6">d</w>’<w n="7.7">un</w> <w n="7.8">glas</w> <w n="7.9">triomphant</w>.</l>
					<l n="8" num="3.2"><w n="8.1">Faut</w>-<w n="8.2">il</w> <w n="8.3">s</w>’<w n="8.4">en</w> <w n="8.5">étonner</w> ? <w n="8.6">L</w>’<w n="8.7">homme</w> <w n="8.8">est</w> <w n="8.9">un</w> <w n="8.10">vieil</w> <w n="8.11">enfant</w></l>
					<l n="9" num="3.3"><space unit="char" quantity="12"></space><w n="9.1">Qu</w>’<w n="9.2">un</w> <w n="9.3">jeu</w> <w n="9.4">sanglant</w> <w n="9.5">amuse</w> !</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="4">
					<l n="10" num="4.1"><w n="10.1">Je</w> <w n="10.2">ne</w> <w n="10.3">vous</w> <w n="10.4">dirai</w> <w n="10.5">point</w>, <w n="10.6">moi</w>, <w n="10.7">s</w>’<w n="10.8">il</w> <w n="10.9">est</w> <w n="10.10">simple</w> <w n="10.11">ou</w> <w n="10.12">non</w>.</l>
					<l n="11" num="4.2"><w n="11.1">Il</w> <w n="11.2">n</w>’<w n="11.3">est</w> <w n="11.4">pas</w>, <w n="11.5">toutefois</w>, <w n="11.6">chimiste</w> <w n="11.7">de</w> <w n="11.8">renom</w></l>
					<l n="12" num="4.3"><space unit="char" quantity="12"></space><w n="12.1">Qui</w> <w n="12.2">sur</w> <w n="12.3">ce</w> <w n="12.4">point</w> <w n="12.5">s</w>’<w n="12.6">abuse</w>.</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="5">
					<l n="13" num="5.1"><w n="13.1">Il</w> <w n="13.2">en</w> <w n="13.3">est</w> <w n="13.4">dans</w> <w n="13.5">l</w>’<w n="13.6">armée</w> ; <w n="13.7">il</w> <w n="13.8">en</w> <w n="13.9">est</w> <w n="13.10">dans</w> <w n="13.11">l</w>’<w n="13.12">État</w>.</l>
					<l n="14" num="5.2"><w n="14.1">Chacun</w> <w n="14.2">a</w> <w n="14.3">ses</w> <w n="14.4">devoirs</w> <w n="14.5">et</w> <w n="14.6">chacun</w> <w n="14.7">son</w> <w n="14.8">mandat</w> ;</l>
					<l n="15" num="5.3"><space unit="char" quantity="12"></space><w n="15.1">Et</w> <w n="15.2">chacun</w> <w n="15.3">sa</w> <w n="15.4">faiblesse</w>.</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="6">
					<l n="16" num="6.1"><w n="16.1">Examinez</w>, <w n="16.2">piqueux</w>, <w n="16.3">s</w>’<w n="16.4">il</w> <w n="16.5">l</w>’<w n="16.6">est</w> <w n="16.7">dix</w> <w n="16.8">fois</w> <w n="16.9">ou</w> <w n="16.10">moins</w> ;</l>
					<l n="17" num="6.2"><w n="17.1">Et</w>, <w n="17.2">tantôt</w>, <w n="17.3">faites</w>-<w n="17.4">nous</w> <w n="17.5">avec</w> <w n="17.6">les</w> <w n="17.7">plus</w> <w n="17.8">grands</w> <w n="17.9">soins</w></l>
					<l n="18" num="6.3"><space unit="char" quantity="12"></space><w n="18.1">Un</w> <w n="18.2">rapport</w> <w n="18.3">sans</w> <w n="18.4">mollesse</w>.</l>
				</lg>
					<closer>
						<note type="footnote" id="none">▪ Cor, corps, cor, corps, corps, cors.</note>
					</closer>
			</div></body></text></TEI>