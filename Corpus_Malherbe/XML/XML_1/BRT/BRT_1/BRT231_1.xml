<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">MOTS CARRÉS</head><div type="poem" key="BRT231">
				<head type="number">VI</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">La</w> <w n="1.2">gondole</w> <w n="1.3">de</w> <w n="1.4">pêche</w> <w n="1.5">est</w> <w n="1.6">enfin</w> <w n="1.7">démarrée</w> ;</l>
					<l n="2" num="1.2"><w n="2.1">Le</w> <w n="2.2">patron</w> <w n="2.3">dit</w> : <w n="2.4">En</w> <w n="2.5">route</w> ! <w n="2.6">et</w> <w n="2.7">vogue</w> <w n="2.8">vers</w> <w n="2.9">le</w> <subst hand="ML" type="completion" reason="analysis"><del>....</del><add rend="hidden"><w n="2.10">nord</w></add></subst></l>
					<l n="3" num="1.3"><w n="3.1">Tout</w> <w n="3.2">semble</w> <w n="3.3">être</w> <w n="3.4">pour</w> <w n="3.5">lui</w> : <w n="3.6">le</w> <w n="3.7">vent</w> <w n="3.8">et</w> <w n="3.9">la</w> <w n="3.10">marée</w> !</l>
					<l n="4" num="1.4"><w n="4.1">Avec</w> <w n="4.2">le</w> <w n="4.3">gai</w> <w n="4.4">travail</w>, <w n="4.5">l</w>’<w n="4.6">espoir</w> <w n="4.7">monte</w> <w n="4.8">à</w> <w n="4.9">son</w> <w n="4.10">bord</w>.</l>
					<l n="5" num="1.5"><w n="5.1">Il</w> <w n="5.2">compte</w> <w n="5.3">sur</w> <w n="5.4">le</w> <w n="5.5">gain</w>, <w n="5.6">car</w> <w n="5.7">il</w> <w n="5.8">est</w> <w n="5.9">âpre</w>, <w n="5.10">avide</w> !</l>
					<l n="6" num="1.6"><w n="6.1">D</w>’<w n="6.2">avance</w>, <w n="6.3">en</w> <w n="6.4">son</w> <w n="6.5">honneur</w>, <w n="6.6">il</w> <w n="6.7">chante</w> <w n="6.8">un</w> <w n="6.9">Gloria</w></l>
					<l n="7" num="1.7"><w n="7.1">Et</w> <w n="7.2">rit</w> <w n="7.3">de</w> <w n="7.4">vingt</w> <w n="7.5">bateaux</w> <w n="7.6">qui</w> <w n="7.7">reviennent</w> <w n="7.8">à</w> <w n="7.9">vide</w></l>
					<l n="8" num="1.8"><w n="8.1">En</w> <w n="8.2">côtoyant</w> <w n="8.3">les</w> <w n="8.4">bords</w> <w n="8.5">de</w> <w n="8.6">l</w>’<w n="8.7">île</w> <w n="8.8">d</w>’<subst hand="ML" type="completion" reason="analysis"><del>....</del><add rend="hidden"><w n="8.9">Oria</w></add></subst>.</l>
					<l n="9" num="1.9"><w n="9.1">Mais</w> <w n="9.2">ce</w> <w n="9.3">rire</w> <w n="9.4">mauvais</w>, <w n="9.5">Dieu</w> <w n="9.6">même</w> <w n="9.7">le</w> <w n="9.8">condamne</w> :</l>
					<l n="10" num="1.10"><w n="10.1">À</w> <w n="10.2">son</w> <w n="10.3">tour</w> <w n="10.4">le</w> <w n="10.5">patron</w> <w n="10.6">tend</w> <w n="10.7">ses</w> <w n="10.8">filets</w> <w n="10.9">pour</w> <subst hand="ML" type="completion" reason="analysis"><del>....</del><add rend="hidden"><w n="10.10">rien</w></add></subst>.</l>
					<l n="11" num="1.11"><w n="11.1">Et</w> <w n="11.2">le</w> <w n="11.3">soir</w>, <w n="11.4">étendu</w> <w n="11.5">dans</w> <w n="11.6">un</w> <w n="11.7">coin</w> <w n="11.8">de</w> <w n="11.9">la</w> <subst hand="ML" type="completion" reason="analysis"><del>....</del><add rend="hidden"><w n="11.10">dane</w></add></subst>,</l>
					<l n="12" num="1.12"><w n="12.1">Il</w> <w n="12.2">ne</w> <w n="12.3">peut</w> <w n="12.4">s</w>’<w n="12.5">endormir</w> <w n="12.6">et</w> <w n="12.7">blasphème</w> <w n="12.8">en</w> <w n="12.9">païen</w>.</l>
				</lg>
					<closer>
						<note id="none" type="footnote">▪ NORD<lb></lb>▪ ORIA<lb></lb>▪ RIEN<lb></lb>▪ DANE</note>
					</closer>
			</div></body></text></TEI>