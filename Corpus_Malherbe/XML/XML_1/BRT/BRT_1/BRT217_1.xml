<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">HOMONYMES</head><div type="poem" key="BRT217">
				<head type="number">XLII</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Quelle</w> <w n="1.2">pièce</w> <w n="1.3">de</w> <w n="1.4">bois</w> ! <w n="1.5">dit</w> <w n="1.6">Gervaise</w> <w n="1.7">étonnée</w></l>
					<l n="2" num="1.2"><w n="2.1">Devant</w> <w n="2.2">un</w> <w n="2.3">noble</w> <w n="2.4">chêne</w> <w n="2.5">aux</w> <w n="2.6">branches</w> <w n="2.7">en</w> <w n="2.8">faisceaux</w> ;</l>
					<l n="3" num="1.3"><w n="3.1">Ce</w> <w n="3.2">serait</w> <w n="3.3">un</w> <w n="3.4">beau</w> <subst hand="ML" type="completion" reason="analysis"><del>....</del><add rend="hidden"><w n="3.5">tain</w></add></subst> <w n="3.6">pour</w> <w n="3.7">mon</w> <w n="3.8">oncle</w> <w n="3.9">Thonnée</w> ! »</l>
					<l n="4" num="1.4"><w n="4.1">L</w>’<w n="4.2">oncle</w> <w n="4.3">de</w> <w n="4.4">Gervaise</w> <w n="4.5">est</w> <w n="4.6">constructeur</w> <w n="4.7">de</w> <w n="4.8">vaisseaux</w>.</l>
					<l n="5" num="1.5"><w n="5.1">Plus</w> <w n="5.2">loin</w> <w n="5.3">le</w> <subst hand="ML" type="completion" reason="analysis"><del>....</del><add rend="hidden"><w n="5.4">thym</w></add></subst> <w n="5.5">sauvage</w> <w n="5.6">aux</w> <w n="5.7">senteurs</w> <w n="5.8">pénétrantes</w></l>
					<l n="6" num="1.6"><w n="6.1">Ne</w> <w n="6.2">lui</w> <w n="6.3">met</w> <w n="6.4">en</w> <w n="6.5">esprit</w> <w n="6.6">que</w> <w n="6.7">sauces</w> <w n="6.8">et</w> <w n="6.9">ragoûts</w>.</l>
					<l n="7" num="1.7"><w n="7.1">Gervaise</w> <w n="7.2">est</w> <w n="7.3">fort</w> <w n="7.4">pratique</w> <w n="7.5">et</w> <w n="7.6">suit</w> <w n="7.7">le</w> <w n="7.8">cours</w> <w n="7.9">des</w> <w n="7.10">rentes</w> ;</l>
					<l n="8" num="1.8"><w n="8.1">La</w> <w n="8.2">poésie</w> <w n="8.3">et</w> <w n="8.4">l</w>’<w n="8.5">art</w> <w n="8.6">ne</w> <w n="8.7">sont</w> <w n="8.8">point</w> <w n="8.9">dans</w> <w n="8.10">ses</w> <w n="8.11">goûts</w>.</l>
					<l n="9" num="1.9"><w n="9.1">Elle</w> <subst hand="ML" type="completion" reason="analysis"><del>....</del><add rend="hidden"><w n="9.2">tint</w></add></subst> <w n="9.3">de</w> <w n="9.4">son</w> <w n="9.5">père</w>, <w n="9.6">un</w> <w n="9.7">vieux</w> <w n="9.8">juif</w> <w n="9.9">sec</w> <w n="9.10">et</w> <w n="9.11">blême</w>,</l>
					<l n="10" num="1.10"><w n="10.1">La</w> <w n="10.2">science</w> <w n="10.3">du</w> <w n="10.4">chiffre</w> ; <w n="10.5">et</w> <w n="10.6">sa</w> <w n="10.7">mère</w> <w n="10.8">en</w> <w n="10.9">a</w> <w n="10.10">fait</w></l>
					<l n="11" num="1.11"><w n="11.1">Une</w> <w n="11.2">coquette</w> <w n="11.3">folle</w> <w n="11.4">éprise</w> <w n="11.5">d</w>’<w n="11.6">elle</w>-<w n="11.7">même</w>,</l>
					<l n="12" num="1.12"><w n="12.1">Soigneuse</w> <w n="12.2">de</w> <w n="12.3">son</w> <subst hand="ML" type="completion" reason="analysis"><del>.....</del><add rend="hidden"><w n="12.4">teint</w></add></subst> <w n="12.5">et</w> <w n="12.6">visant</w> <w n="12.7">à</w> <w n="12.8">l</w>’<w n="12.9">effet</w> :</l>
					<l n="13" num="1.13"><w n="13.1">Si</w> <w n="13.2">vous</w> <w n="13.3">me</w> <w n="13.4">demandez</w> <w n="13.5">où</w> <w n="13.6">sa</w> <w n="13.7">cour</w> <w n="13.8">est</w> <w n="13.9">tenue</w></l>
					<l n="14" num="1.14"><w n="14.1">Je</w> <w n="14.2">vous</w> <w n="14.3">renseignerai</w> : <w n="14.4">Dans</w> <w n="14.5">la</w> <w n="14.6">ville</w> <w n="14.7">de</w> <subst hand="ML" type="completion" reason="analysis"><del>....</del><add rend="hidden"><w n="14.8">Tain</w></add></subst>.</l>
					<l n="15" num="1.15"><w n="15.1">Sa</w> <w n="15.2">maison</w> <w n="15.3">neuve</w> <w n="15.4">perche</w> <w n="15.5">en</w> <w n="15.6">haut</w> <w n="15.7">d</w>’<w n="15.8">une</w> <w n="15.9">avenue</w></l>
					<l n="16" num="1.16"><w n="16.1">Et</w>, <w n="16.2">dans</w> <w n="16.3">ses</w> <w n="16.4">murs</w>, <w n="16.5">n</w>’<w n="16.6">a</w> <w n="16.7">pas</w> <w n="16.8">une</w> <w n="16.9">glace</w> <w n="16.10">sans</w> <subst hand="ML" type="completion" reason="analysis"><del>....</del><add rend="hidden"><w n="16.11">tain</w></add></subst>.</l>
				</lg>
					<closer>
						<note type="footnote" id="none">▪ Tin, thym, tint, teint, thain, Tain.</note>
					</closer>
			</div></body></text></TEI>