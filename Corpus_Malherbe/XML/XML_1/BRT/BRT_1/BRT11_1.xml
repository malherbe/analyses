<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PROVERBES</head><div type="poem" key="BRT11">
				<head type="number">XI</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Subtil</w> <w n="1.2">esprit</w>, <w n="1.3">dis</w>-<w n="1.4">moi</w> <w n="1.5">de</w> <w n="1.6">qui</w> <w n="1.7">tu</w> <w n="1.8">veux</w> <w n="1.9">la</w> <w n="1.10">perte</w></l>
					<l n="2" num="1.2"><w n="2.1">Quand</w> <w n="2.2">tu</w> <w n="2.3">nous</w> <w n="2.4">hantes</w> <w n="2.5">tous</w> <w n="2.6">avec</w> <w n="2.7">malignité</w>…</l>
					<l n="3" num="1.3"><w n="3.1">Aux</w> <w n="3.2">aguets</w> <w n="3.3">je</w> <w n="3.4">te</w> <w n="3.5">sens</w>, <w n="3.6">toujours</w> <w n="3.7">l</w>’<w n="3.8">oreille</w> <w n="3.9">ouverte</w></l>
					<l n="4" num="1.4"><w n="4.1">À</w> <w n="4.2">toute</w> <w n="4.3">explosion</w> <w n="4.4">de</w> <w n="4.5">notre</w> <w n="4.6">vanité</w>.</l>
					<l n="5" num="1.5"><w n="5.1">Je</w> <w n="5.2">dirai</w> <w n="5.3">même</w> <w n="5.4">plus</w> : <w n="5.5">Qui</w> <w n="5.6">t</w>’<w n="5.7">écoute</w> <w n="5.8">s</w>’<w n="5.9">expose</w></l>
					<l n="6" num="1.6"><w n="6.1">À</w> <w n="6.2">sombrer</w> <w n="6.3">dans</w> <w n="6.4">la</w> <w n="6.5">nuit</w> <w n="6.6">sur</w> <w n="6.7">l</w>’<w n="6.8">écueil</w> <w n="6.9">qui</w> <w n="6.10">l</w>’<w n="6.11">attend</w>…</l>
					<l n="7" num="1.7"><w n="7.1">Tu</w> <w n="7.2">changes</w> <w n="7.3">à</w> <w n="7.4">ton</w> <w n="7.5">gré</w> <w n="7.6">d</w>’<w n="7.7">aspect</w>, <w n="7.8">d</w>’<w n="7.9">air</w> <w n="7.10">et</w> <w n="7.11">de</w> <w n="7.12">pose</w>…</l>
					<l n="8" num="1.8"><space unit="char" quantity="12"></space><w n="8.1">Qui</w> <w n="8.2">donc</w> <w n="8.3">es</w>-<w n="8.4">tu</w> ? … — <w n="8.5">Satan</w>.</l>
				</lg>
					<closer>
						<note type="footnote" id="none">▪ Dis-moi qui tu hantes, je te dirai qui tu es.</note>
					</closer>
			</div></body></text></TEI>