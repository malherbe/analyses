<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PROVERBES</head><div type="poem" key="BRT26">
				<head type="number">XXVI</head>
				<lg n="1">
					<l n="1" num="1.1">« <w n="1.1">Quel</w> <w n="1.2">ennuyeux</w> <w n="1.3">travail</w> ! <w n="1.4">soupirait</w> <w n="1.5">Mignonnette</w> ;</l>
					<l n="2" num="1.2"><w n="2.1">Aide</w>-<w n="2.2">moi</w> <w n="2.3">donc</w> <w n="2.4">un</w> <w n="2.5">peu</w>, <w n="2.6">toi</w>, <w n="2.7">ma</w> <w n="2.8">chère</w> <w n="2.9">sœurette</w>,</l>
					<l n="3" num="1.3"><space unit="char" quantity="12"></space><w n="3.1">À</w> <w n="3.2">finir</w> <w n="3.3">ce</w> <w n="3.4">surjet</w>. »</l>
					<l n="4" num="1.4"><w n="4.1">Mais</w> <w n="4.2">sœurette</w>, <w n="4.3">écartant</w> <w n="4.4">la</w> <w n="4.5">fillette</w> <w n="4.6">ingénue</w>,</l>
					<l n="5" num="1.5"><w n="5.1">Poursuivait</w> <w n="5.2">d</w>’<w n="5.3">un</w> <w n="5.4">regard</w> <w n="5.5">qui</w> <w n="5.6">transperçait</w> <w n="5.7">la</w> <w n="5.8">nue</w></l>
					<l n="6" num="1.6"><space unit="char" quantity="12"></space><w n="6.1">Un</w> <w n="6.2">invisible</w> <w n="6.3">objet</w>…</l>
					<l n="7" num="1.7"><w n="7.1">Le</w> <w n="7.2">ciel</w> <w n="7.3">gardait</w> <w n="7.4">pour</w> <w n="7.5">lui</w> <w n="7.6">son</w> <w n="7.7">espiègle</w> <w n="7.8">mystère</w> ! …</l>
					<l n="8" num="1.8"><w n="8.1">Avec</w> <w n="8.2">dépit</w> <w n="8.3">alors</w> <w n="8.4">appelant</w> <w n="8.5">son</w> <w n="8.6">grand</w> <w n="8.7">frère</w>,</l>
					<l n="9" num="1.9"><space unit="char" quantity="12"></space><w n="9.1">Sœurette</w> <w n="9.2">l</w>’<w n="9.3">implora</w> :</l>
					<l n="10" num="1.10">« <w n="10.1">Donne</w>-<w n="10.2">moi</w>, <w n="10.3">par</w> <w n="10.4">pitié</w>, <w n="10.5">la</w> <w n="10.6">rime</w> <w n="10.7">que</w> <w n="10.8">je</w> <w n="10.9">cherche</w> !</l>
					<l n="11" num="1.11">— <w n="11.1">Quand</w> <w n="11.2">tu</w> <w n="11.3">viens</w>, <w n="11.4">à</w> <w n="11.5">ta</w> <w n="11.6">sœur</w>, <w n="11.7">de</w> <w n="11.8">refuser</w> <w n="11.9">la</w> <w n="11.10">perche</w>,</l>
					<l n="12" num="1.12"><w n="12.1">Demande</w> <w n="12.2">ailleurs</w>, <w n="12.3">dit</w>-<w n="12.4">il</w>, <w n="12.5">quelqu</w>’<w n="12.6">un</w> <w n="12.7">qui</w> <w n="12.8">t</w>’<w n="12.9">aidera</w> ! »</l>
				</lg>
					<closer>
						<note id="none" type="footnote">▪ Aide-toi, le ciel t’aidera.</note>
					</closer>
			</div></body></text></TEI>