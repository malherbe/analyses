<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ÉNIGMES</head><head type="sub_1">BOTANIQUE<lb></lb>(<hi rend="smallcap">EMBLÈMES</hi>.)</head><div type="poem" key="BRT160">
				<head type="number">X</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Modeste</w> <w n="1.2">port</w>, <w n="1.3">taille</w> <w n="1.4">fluette</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Feuille</w> <w n="2.2">étroite</w> <w n="2.3">et</w> <w n="2.4">d</w>’<w n="2.5">aspect</w> <w n="2.6">luisant</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Je</w> <w n="3.2">ne</w> <w n="3.3">porte</w> <w n="3.4">ni</w> <w n="3.5">fleur</w> <w n="3.6">coquette</w></l>
					<l n="4" num="1.4"><w n="4.1">Ni</w> <w n="4.2">grappe</w> <w n="4.3">d</w>’<w n="4.4">or</w> <w n="4.5">ni</w> <w n="4.6">fruit</w> <w n="4.7">pesant</w>.</l>
					<l n="5" num="1.5"><w n="5.1">Au</w> <w n="5.2">jardinet</w> <w n="5.3">du</w> <w n="5.4">presbytère</w></l>
					<l n="6" num="1.6"><w n="6.1">Je</w> <w n="6.2">fais</w> <w n="6.3">une</w> <w n="6.4">bordure</w> <w n="6.5">austère</w></l>
					<l n="7" num="1.7"><w n="7.1">Toujours</w> <w n="7.2">verte</w> <w n="7.3">en</w> <w n="7.4">chaque</w> <w n="7.5">saison</w> ;</l>
					<l n="8" num="1.8"><w n="8.1">Et</w>, <w n="8.2">bénit</w> <w n="8.3">par</w> <w n="8.4">la</w> <w n="8.5">main</w> <w n="8.6">du</w> <w n="8.7">prêtre</w>,</l>
					<l n="9" num="1.9"><w n="9.1">Je</w> <w n="9.2">protège</w> <w n="9.3">le</w> <w n="9.4">lit</w> <w n="9.5">d</w>’<w n="9.6">ancêtre</w>,</l>
					<l n="10" num="1.10"><w n="10.1">Le</w> <w n="10.2">berceau</w>, <w n="10.3">toute</w> <w n="10.4">la</w> <w n="10.5">maison</w>.</l>
					<l n="11" num="1.11"><w n="11.1">Je</w> <w n="11.2">suis</w> <w n="11.3">le</w> <w n="11.4">goupillon</w> <w n="11.5">suprême</w> ;</l>
					<l n="12" num="1.12"><w n="12.1">Sur</w> <w n="12.2">les</w> <w n="12.3">tombeaux</w> <w n="12.4">on</w> <w n="12.5">m</w>’<w n="12.6">a</w> <w n="12.7">planté</w> ;</l>
					<l n="13" num="1.13"><w n="13.1">Et</w> <w n="13.2">du</w> <hi rend="ital"><w n="13.3">stoïcisme</w></hi> <w n="13.4">lui</w>-<w n="13.5">même</w></l>
					<l n="14" num="1.14"><w n="14.1">J</w>’<w n="14.2">exprime</w> <w n="14.3">enfin</w> <w n="14.4">la</w> <w n="14.5">fermeté</w>.</l>
				</lg>
					<closer>
						<note type="footnote" id="none">▪ Buis.</note>
					</closer>
			</div></body></text></TEI>