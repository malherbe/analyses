<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="sub_2">TOME PREMIER</title>
				<title type="medium">Édition électronique</title>
				<author key="BLA">
					<name>
						<forename>Prosper</forename>
						<surname>BLANCHEMAIN</surname>
					</name>
					<date from="1816" to="1879">1816-1879</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>7196 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">BLA_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME PREMIER</title>
						<author>Prosper Blanchemain</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/pomesetposie01blan</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
								<title>TOME PREMIER</title>
								<author>Prosper Blanchemain</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE ANCIENNE ET MODERNE - ÉDOUARD ROUVEYRE</publisher>
									<date when="1880">1880</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME I</title>
						<title>POÈMES ET POÉSIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME V</title>
						<title>SONNETS ET FANTAISIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1845" to="1858">1845-1858</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">POÈMES ET POÉSIES</head><div type="poem" key="BLA18">
					<head type="main">JE PENSE A VOUS</head>
					<opener>
						<salute>A Marie Désirée.</salute>
					</opener>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Je</w> <w n="1.2">pense</w> <w n="1.3">à</w> <w n="1.4">vous</w>, <w n="1.5">ma</w> <w n="1.6">jeune</w> <w n="1.7">bien</w>-<w n="1.8">aimée</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Quand</w> <w n="2.2">le</w> <w n="2.3">jour</w> <w n="2.4">naît</w>, <w n="2.5">quand</w> <w n="2.6">la</w> <w n="2.7">rose</w> <w n="2.8">embaumée</w></l>
						<l n="3" num="1.3"><w n="3.1">S</w>’<w n="3.2">ouvre</w> <w n="3.3">au</w> <w n="3.4">matin</w> <w n="3.5">scintillante</w> <w n="3.6">de</w> <w n="3.7">pleurs</w> ;</l>
						<l n="4" num="1.4"><w n="4.1">Quand</w> <w n="4.2">l</w>’<w n="4.3">alouette</w> <w n="4.4">ouvre</w> <w n="4.5">son</w> <w n="4.6">aile</w> <w n="4.7">grise</w>,</l>
						<l n="5" num="1.5"><w n="5.1">Vole</w> <w n="5.2">en</w> <w n="5.3">chantant</w>, <w n="5.4">vole</w> <w n="5.5">au</w> <w n="5.6">ciel</w>, <w n="5.7">sur</w> <w n="5.8">la</w> <w n="5.9">brise</w></l>
						<l n="6" num="1.6"><space unit="char" quantity="8"></space><w n="6.1">Et</w> <w n="6.2">le</w> <w n="6.3">parfum</w> <w n="6.4">des</w> <w n="6.5">fleurs</w>.</l>
					</lg>
					<lg n="2">
						<l n="7" num="2.1"><w n="7.1">Je</w> <w n="7.2">pense</w> <w n="7.3">à</w> <w n="7.4">vous</w> <w n="7.5">quand</w> <w n="7.6">le</w> <w n="7.7">soleil</w> <w n="7.8">décline</w>,</l>
						<l n="8" num="2.2"><w n="8.1">Quand</w> <w n="8.2">le</w> <w n="8.3">brouillard</w>, <w n="8.4">sur</w> <w n="8.5">la</w> <w n="8.6">verte</w> <w n="8.7">colline</w>,</l>
						<l n="9" num="2.3"><w n="9.1">Étend</w> <w n="9.2">au</w> <w n="9.3">soir</w> <w n="9.4">ses</w> <w n="9.5">humides</w> <w n="9.6">réseaux</w> ;</l>
						<l n="10" num="2.4"><w n="10.1">Quand</w> <w n="10.2">la</w> <w n="10.3">forêt</w> <w n="10.4">a</w> <w n="10.5">de</w> <w n="10.6">plus</w> <w n="10.7">doux</w> <w n="10.8">murmures</w>,</l>
						<l n="11" num="2.5"><w n="11.1">Et</w> <w n="11.2">que</w> <w n="11.3">la</w> <w n="11.4">lune</w>, <w n="11.5">à</w> <w n="11.6">travers</w> <w n="11.7">ses</w> <w n="11.8">ramures</w>,</l>
						<l n="12" num="2.6"><space unit="char" quantity="8"></space><w n="12.1">Argenté</w> <w n="12.2">les</w> <w n="12.3">ruisseaux</w>.</l>
					</lg>
					<lg n="3">
						<l n="13" num="3.1"><w n="13.1">Je</w> <w n="13.2">pense</w> <w n="13.3">à</w> <w n="13.4">vous</w> <w n="13.5">lorsque</w> <w n="13.6">l</w>’<w n="13.7">éclair</w> <w n="13.8">s</w>’<w n="13.9">enflamme</w>,</l>
						<l n="14" num="3.2"><w n="14.1">Et</w> <w n="14.2">dis</w> : « <w n="14.3">Seigneur</w>, <w n="14.4">des</w> <w n="14.5">orages</w> <w n="14.6">de</w> <w n="14.7">l</w>’<w n="14.8">âme</w></l>
						<l n="15" num="3.3"><w n="15.1">Épargnez</w>-<w n="15.2">lui</w> <w n="15.3">la</w> <w n="15.4">fatigue</w> <w n="15.5">et</w> <w n="15.6">le</w> <w n="15.7">fiel</w> ! »</l>
						<l n="16" num="3.4"><w n="16.1">Quand</w> <w n="16.2">le</w> <w n="16.3">ciel</w> <w n="16.4">bleu</w> <w n="16.5">rayonne</w> <w n="16.6">sur</w> <w n="16.7">nos</w> <w n="16.8">tètes</w>,</l>
						<l n="17" num="3.5"><w n="17.1">Je</w> <w n="17.2">pense</w> <w n="17.3">à</w> <w n="17.4">vous</w>, <w n="17.5">mon</w> <w n="17.6">ange</w>, <w n="17.7">car</w> <w n="17.8">vous</w> <w n="17.9">êtes</w></l>
						<l n="18" num="3.6"><space unit="char" quantity="8"></space><w n="18.1">Pure</w> <w n="18.2">comme</w> <w n="18.3">un</w> <w n="18.4">beau</w> <w n="18.5">ciel</w>.</l>
					</lg>
					<lg n="4">
						<l n="19" num="4.1"><w n="19.1">Je</w> <w n="19.2">pense</w> <w n="19.3">à</w> <w n="19.4">vous</w> <w n="19.5">aux</w> <w n="19.6">pieds</w> <w n="19.7">de</w> <w n="19.8">la</w> <w n="19.9">Madone</w> ;</l>
						<l n="20" num="4.2"><w n="20.1">En</w> <w n="20.2">implorant</w> <w n="20.3">la</w> <w n="20.4">Vierge</w> <w n="20.5">qui</w> <w n="20.6">pardonne</w>,</l>
						<l n="21" num="4.3"><w n="21.1">C</w>’<w n="21.2">est</w> <w n="21.3">votre</w> <w n="21.4">nom</w> <w n="21.5">que</w> <w n="21.6">je</w> <w n="21.7">dis</w> <w n="21.8">à</w> <w n="21.9">genoux</w> ;</l>
						<l n="22" num="4.4"><w n="22.1">J</w>’<w n="22.2">espère</w> <w n="22.3">alors</w> <w n="22.4">que</w>, <w n="22.5">sur</w> <w n="22.6">ces</w> <w n="22.7">mêmes</w> <w n="22.8">pierres</w>,</l>
						<l n="23" num="4.5"><w n="23.1">Pour</w> <w n="23.2">moi</w>, <w n="23.3">plus</w> <w n="23.4">tard</w>, <w n="23.5">vous</w> <w n="23.6">aurez</w> <w n="23.7">des</w> <w n="23.8">prières</w>…</l>
						<l n="24" num="4.6"><space unit="char" quantity="8"></space><w n="24.1">J</w>’<w n="24.2">ai</w> <w n="24.3">tant</w> <w n="24.4">prié</w> <w n="24.5">pour</w> <w n="24.6">vous</w> !</l>
					</lg>
					<lg n="5">
						<l n="25" num="5.1"><w n="25.1">Je</w> <w n="25.2">pense</w> <w n="25.3">à</w> <w n="25.4">vous</w> ; <w n="25.5">car</w> <w n="25.6">sans</w> <w n="25.7">vous</w> <w n="25.8">point</w> <w n="25.9">de</w> <w n="25.10">joie</w> ;</l>
						<l n="26" num="5.2"><w n="26.1">Sans</w> <w n="26.2">vous</w>, <w n="26.3">les</w> <w n="26.4">jours</w> <w n="26.5">que</w> <w n="26.6">le</w> <w n="26.7">Seigneur</w> <w n="26.8">m</w>’<w n="26.9">envoie</w>,</l>
						<l n="27" num="5.3"><w n="27.1">Sombres</w> <w n="27.2">ou</w> <w n="27.3">purs</w> <w n="27.4">passent</w> <w n="27.5">inachevés</w> ;</l>
						<l n="28" num="5.4"><w n="28.1">Il</w> <w n="28.2">n</w>’<w n="28.3">es</w> : <w n="28.4">sans</w> <w n="28.5">vous</w> <w n="28.6">nul</w> <w n="28.7">plaisir</w> <w n="28.8">que</w> <w n="28.9">j</w>’<w n="28.10">envie</w> ;</l>
						<l n="29" num="5.5"><w n="29.1">Mon</w> <w n="29.2">cœur</w> <w n="29.3">n</w>’<w n="29.4">est</w> <w n="29.5">plus</w> <w n="29.6">en</w> <w n="29.7">moi</w>-<w n="29.8">même</w>, <w n="29.9">et</w> <w n="29.10">ma</w> <w n="29.11">vie</w></l>
						<l n="30" num="5.6"><space unit="char" quantity="8"></space><w n="30.1">Est</w> <w n="30.2">toute</w> <w n="30.3">où</w> <w n="30.4">vous</w> <w n="30.5">vivez</w>.</l>
					</lg>
					<lg n="6">
						<l n="31" num="6.1"><w n="31.1">Je</w> <w n="31.2">pense</w> <w n="31.3">à</w> <w n="31.4">vous</w>, <w n="31.5">que</w> <w n="31.6">j</w>’<w n="31.7">aille</w>, <w n="31.8">que</w> <w n="31.9">j</w>’<w n="31.10">arrive</w>,</l>
						<l n="32" num="6.2"><w n="32.1">Que</w> <w n="32.2">je</w> <w n="32.3">regarde</w>, <w n="32.4">en</w> <w n="32.5">rêvant</w> <w n="32.6">sur</w> <w n="32.7">la</w> <w n="32.8">rive</w>,</l>
						<l n="33" num="6.3"><w n="33.1">Le</w> <w n="33.2">ruisseau</w> <w n="33.3">fuir</w>, <w n="33.4">comme</w> <w n="33.5">fuiront</w> <w n="33.6">mes</w> <w n="33.7">jours</w> ;</l>
						<l n="34" num="6.4"><w n="34.1">Je</w> <w n="34.2">pense</w> <w n="34.3">à</w> <w n="34.4">vous</w>, <w n="34.5">que</w> <w n="34.6">je</w> <w n="34.7">m</w>’<w n="34.8">endorme</w> <w n="34.9">ou</w> <w n="34.10">veille</w> ;</l>
						<l n="35" num="6.5"><w n="35.1">Triste</w> <w n="35.2">ou</w> <w n="35.3">joyeux</w>, <w n="35.4">ô</w> <w n="35.5">ma</w> <w n="35.6">jeune</w> <w n="35.7">merveille</w> !</l>
						<l n="36" num="6.6"><space unit="char" quantity="8"></space><w n="36.1">Je</w> <w n="36.2">pense</w> <w n="36.3">à</w> <w n="36.4">vous</w> <w n="36.5">toujours</w>.</l>
					</lg>
				</div></body></text></TEI>