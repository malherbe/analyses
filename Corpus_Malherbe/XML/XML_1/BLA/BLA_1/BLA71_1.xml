<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="sub_2">TOME PREMIER</title>
				<title type="medium">Édition électronique</title>
				<author key="BLA">
					<name>
						<forename>Prosper</forename>
						<surname>BLANCHEMAIN</surname>
					</name>
					<date from="1816" to="1879">1816-1879</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>7196 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">BLA_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME PREMIER</title>
						<author>Prosper Blanchemain</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/pomesetposie01blan</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
								<title>TOME PREMIER</title>
								<author>Prosper Blanchemain</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE ANCIENNE ET MODERNE - ÉDOUARD ROUVEYRE</publisher>
									<date when="1880">1880</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME I</title>
						<title>POÈMES ET POÉSIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME V</title>
						<title>SONNETS ET FANTAISIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1845" to="1858">1845-1858</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">IDÉAL</head><div type="poem" key="BLA71">
					<head type="main">LES SYLPHES DES FEUILLES</head>
					<head type="form">BALLADE</head>
					<opener>
						<salute>A Noémi Blanchemain, ma chère fille.</salute>
					</opener>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Dès</w> <w n="1.2">que</w> <w n="1.3">la</w> <w n="1.4">saison</w> <w n="1.5">verte</w></l>
						<l n="2" num="1.2"><space unit="char" quantity="2"></space><w n="2.1">Vient</w> <w n="2.2">nous</w> <w n="2.3">ombrager</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Sous</w> <w n="3.2">la</w> <w n="3.3">feuille</w> <w n="3.4">entr</w>’<w n="3.5">ouverte</w>,</l>
						<l n="4" num="1.4"><space unit="char" quantity="2"></space><w n="4.1">Au</w> <w n="4.2">bois</w>, <w n="4.3">au</w> <w n="4.4">verger</w>,</l>
						<l n="5" num="1.5"><w n="5.1">Le</w> <w n="5.2">zéphyr</w> <w n="5.3">de</w> <w n="5.4">l</w>’<w n="5.5">aurore</w>,</l>
						<l n="6" num="1.6"><w n="6.1">En</w> <w n="6.2">soufflant</w>, <w n="6.3">fait</w> <w n="6.4">éclore</w>.</l>
						<l n="7" num="1.7"><w n="7.1">Habitant</w> <w n="7.2">incolore</w>,</l>
						<l n="8" num="1.8"><space unit="char" quantity="2"></space><w n="8.1">Un</w> <w n="8.2">sylphe</w> <w n="8.3">léger</w>.</l>
					</lg>
					<lg n="2">
						<l n="9" num="2.1"><w n="9.1">Toute</w> <w n="9.2">feuille</w> <w n="9.3">flexible</w></l>
						<l n="10" num="2.2"><space unit="char" quantity="2"></space><w n="10.1">Que</w> <w n="10.2">l</w>’<w n="10.3">on</w> <w n="10.4">voit</w> <w n="10.5">frémir</w></l>
						<l n="11" num="2.3"><w n="11.1">Cache</w> <w n="11.2">un</w> <w n="11.3">sylphe</w> <w n="11.4">invisible</w>,</l>
						<l n="12" num="2.4"><space unit="char" quantity="2"></space><w n="12.1">Prompt</w> <w n="12.2">à</w> <w n="12.3">s</w>’<w n="12.4">y</w> <w n="12.5">blottir</w>.</l>
						<l n="13" num="2.5"><w n="13.1">Feuille</w> <w n="13.2">et</w> <w n="13.3">sylphe</w>, <w n="13.4">tout</w> <w n="13.5">tremble</w> ;</l>
						<l n="14" num="2.6"><w n="14.1">Même</w> <w n="14.2">sort</w> <w n="14.3">les</w> <w n="14.4">rassemble</w> :</l>
						<l n="15" num="2.7"><w n="15.1">Ils</w> <w n="15.2">devront</w> <w n="15.3">vivre</w> <w n="15.4">ensemble</w>,</l>
						<l n="16" num="2.8"><space unit="char" quantity="2"></space><w n="16.1">Ensemble</w> <w n="16.2">mourir</w>.</l>
					</lg>
					<lg n="3">
						<l n="17" num="3.1"><w n="17.1">Lorsque</w> <w n="17.2">le</w> <w n="17.3">vent</w>, <w n="17.4">leur</w> <w n="17.5">père</w>,</l>
						<l n="18" num="3.2"><space unit="char" quantity="2"></space><w n="18.1">Frémit</w> <w n="18.2">dans</w> <w n="18.3">les</w> <w n="18.4">bois</w>,</l>
						<l n="19" num="3.3"><w n="19.1">Au</w> <w n="19.2">fond</w> <w n="19.3">de</w> <w n="19.4">leur</w> <w n="19.5">repaire</w>,</l>
						<l n="20" num="3.4"><space unit="char" quantity="2"></space><w n="20.1">Émus</w> <w n="20.2">à</w> <w n="20.3">la</w> <w n="20.4">fois</w>,</l>
						<l n="21" num="3.5"><w n="21.1">Les</w> <w n="21.2">sylphes</w> <w n="21.3">du</w> <w n="21.4">feuillage</w>,</l>
						<l n="22" num="3.6"><w n="22.1">Agitant</w> <w n="22.2">leur</w> <w n="22.3">ombrage</w>,</l>
						<l n="23" num="3.7"><w n="23.1">Mêlent</w> <w n="23.2">un</w> <w n="23.3">frais</w> <w n="23.4">langage</w></l>
						<l n="24" num="3.8"><space unit="char" quantity="2"></space><w n="24.1">A</w> <w n="24.2">sa</w> <w n="24.3">grande</w> <w n="24.4">voix</w>.</l>
					</lg>
					<lg n="4">
						<l n="25" num="4.1"><w n="25.1">Si</w>, <w n="25.2">le</w> <w n="25.3">matin</w>, <w n="25.4">s</w>’<w n="25.5">exhale</w></l>
						<l n="26" num="4.2"><space unit="char" quantity="2"></space><w n="26.1">Des</w> <w n="26.2">bois</w> <w n="26.3">un</w> <w n="26.4">doux</w> <w n="26.5">bruit</w>,</l>
						<l n="27" num="4.3"><w n="27.1">C</w>’<w n="27.2">est</w> <w n="27.3">leur</w> <w n="27.4">voix</w> <w n="27.5">idéale</w></l>
						<l n="28" num="4.4"><space unit="char" quantity="2"></space><w n="28.1">Qui</w> <w n="28.2">vient</w> <w n="28.3">et</w> <w n="28.4">s</w>’<w n="28.5">enfuit</w>.</l>
						<l n="29" num="4.5"><w n="29.1">Quand</w> <w n="29.2">le</w> <w n="29.3">jour</w> <w n="29.4">va</w> <w n="29.5">se</w> <w n="29.6">clore</w>,</l>
						<l n="30" num="4.6"><w n="30.1">Dans</w> <w n="30.2">la</w> <w n="30.3">forêt</w> <w n="30.4">sonore</w></l>
						<l n="31" num="4.7"><w n="31.1">Ils</w> <w n="31.2">soupirent</w> <w n="31.3">encore</w></l>
						<l n="32" num="4.8"><space unit="char" quantity="2"></space><w n="32.1">L</w>’<w n="32.2">hymne</w> <w n="32.3">de</w> <w n="32.4">la</w> <w n="32.5">nuit</w>.</l>
					</lg>
					<lg n="5">
						<l n="33" num="5.1"><w n="33.1">Quand</w> <w n="33.2">seul</w> <w n="33.3">dans</w> <w n="33.4">l</w>’<w n="33.5">ombre</w> <w n="33.6">obscure</w></l>
						<l n="34" num="5.2"><space unit="char" quantity="2"></space><w n="34.1">Chante</w> <w n="34.2">un</w> <w n="34.3">rossignol</w>,</l>
						<l n="35" num="5.3"><w n="35.1">Si</w> <w n="35.2">quelque</w> <w n="35.3">frais</w> <w n="35.4">murmure</w></l>
						<l n="36" num="5.4"><space unit="char" quantity="2"></space><w n="36.1">A</w> <w n="36.2">rasé</w> <w n="36.3">le</w> <w n="36.4">sol</w>,</l>
						<l n="37" num="5.5"><w n="37.1">Si</w> <w n="37.2">le</w> <w n="37.3">tremble</w> <w n="37.4">s</w>’<w n="37.5">agite</w>,</l>
						<l n="38" num="5.6"><w n="38.1">C</w>’<w n="38.2">est</w> <w n="38.3">qu</w>’<w n="38.4">un</w> <w n="38.5">lutin</w> <w n="38.6">palpite</w>,</l>
						<l n="39" num="5.7"><w n="39.1">Sous</w> <w n="39.2">la</w> <w n="39.3">feuille</w> <w n="39.4">petite</w>,</l>
						<l n="40" num="5.8"><space unit="char" quantity="2"></space><w n="40.1">Son</w> <w n="40.2">vert</w> <w n="40.3">parasol</w>.</l>
					</lg>
					<lg n="6">
						<l n="41" num="6.1"><w n="41.1">Quand</w> <w n="41.2">l</w>’<w n="41.3">aile</w> <w n="41.4">de</w> <w n="41.5">l</w>’<w n="41.6">orage</w></l>
						<l n="42" num="6.2"><space unit="char" quantity="2"></space><w n="42.1">Assombrit</w> <w n="42.2">les</w> <w n="42.3">champs</w>,</l>
						<l n="43" num="6.3"><w n="43.1">La</w> <w n="43.2">stupeur</w> <w n="43.3">décourage</w></l>
						<l n="44" num="6.4"><space unit="char" quantity="2"></space><w n="44.1">Leur</w> <w n="44.2">joie</w> <w n="44.3">et</w> <w n="44.4">leurs</w> <w n="44.5">chants</w></l>
						<l n="45" num="6.5"><w n="45.1">Puis</w> <w n="45.2">la</w> <w n="45.3">tempête</w> <w n="45.4">gronde</w>,</l>
						<l n="46" num="6.6"><w n="46.1">Et</w> <w n="46.2">l</w>’<w n="46.3">on</w> <w n="46.4">entend</w> <w n="46.5">sous</w> <w n="46.6">l</w>’<w n="46.7">onde</w>,</l>
						<l n="47" num="6.7"><w n="47.1">Dans</w> <w n="47.2">la</w> <w n="47.3">forêt</w> <w n="47.4">profonde</w>,</l>
						<l n="48" num="6.8"><space unit="char" quantity="2"></space><w n="48.1">Leurs</w> <w n="48.2">soupirs</w> <w n="48.3">touchants</w>.</l>
					</lg>
					<lg n="7">
						<l n="49" num="7.1"><w n="49.1">L</w>’<w n="49.2">été</w> <w n="49.3">fuit</w> <w n="49.4">infidèle</w>,</l>
						<l n="50" num="7.2"><space unit="char" quantity="2"></space><w n="50.1">La</w> <w n="50.2">feuille</w> <w n="50.3">jaunit</w> ;</l>
						<l n="51" num="7.3"><w n="51.1">Chaque</w> <w n="51.2">sylphe</w> <w n="51.3">ainsi</w> <w n="51.4">qu</w>’<w n="51.5">elle</w></l>
						<l n="52" num="7.4"><space unit="char" quantity="2"></space><w n="52.1">Tremble</w> <w n="52.2">et</w> <w n="52.3">se</w> <w n="52.4">ternit</w>.</l>
						<l n="53" num="7.5"><w n="53.1">Il</w> <w n="53.2">n</w>’<w n="53.3">a</w> <w n="53.4">pour</w> <w n="53.5">chant</w> <w n="53.6">d</w>’<w n="53.7">automne</w></l>
						<l n="54" num="7.6"><w n="54.1">Qu</w>’<w n="54.2">un</w> <w n="54.3">soupir</w> <w n="54.4">monotone</w> ;</l>
						<l n="55" num="7.7"><w n="55.1">Le</w> <w n="55.2">bois</w> <w n="55.3">perd</w> <w n="55.4">sa</w> <w n="55.5">couronne</w>…</l>
						<l n="56" num="7.8"><space unit="char" quantity="2"></space><w n="56.1">Tout</w> <w n="56.2">meurt</w> ! <w n="56.3">tout</w> <w n="56.4">finit</w> !</l>
					</lg>
					<lg n="8">
						<l n="57" num="8.1"><w n="57.1">Le</w> <w n="57.2">pâtre</w> <w n="57.3">solitaire</w>,</l>
						<l n="58" num="8.2"><space unit="char" quantity="2"></space><w n="58.1">Sous</w> <w n="58.2">son</w> <w n="58.3">pied</w> <w n="58.4">vibrant</w>,</l>
						<l n="59" num="8.3"><w n="59.1">Fait</w> <w n="59.2">résonner</w> <w n="59.3">à</w> <w n="59.4">terre</w></l>
						<l n="60" num="8.4"><space unit="char" quantity="2"></space><w n="60.1">Le</w> <w n="60.2">feuillage</w> <w n="60.3">errant</w>.</l>
						<l n="61" num="8.5"><w n="61.1">Chaque</w> <w n="61.2">plainte</w> <w n="61.3">que</w> <w n="61.4">pousse</w></l>
						<l n="62" num="8.6"><w n="62.1">La</w> <w n="62.2">feuille</w> <w n="62.3">sur</w> <w n="62.4">la</w> <w n="62.5">mousse</w>,</l>
						<l n="63" num="8.7"><w n="63.1">Est</w> <w n="63.2">la</w> <w n="63.3">voix</w> <w n="63.4">faible</w> <w n="63.5">et</w> <w n="63.6">douce</w></l>
						<l n="64" num="8.8"><space unit="char" quantity="2"></space><w n="64.1">D</w>’<w n="64.2">un</w> <w n="64.3">sylphe</w> <w n="64.4">expirant</w>.</l>
					</lg>
					<lg n="9">
						<l n="65" num="9.1"><w n="65.1">Un</w> <w n="65.2">effort</w> <w n="65.3">de</w> <w n="65.4">la</w> <w n="65.5">bise</w>,</l>
						<l n="66" num="9.2"><space unit="char" quantity="2"></space><w n="66.1">Parfois</w> <w n="66.2">en</w> <w n="66.3">passant</w>,</l>
						<l n="67" num="9.3"><w n="67.1">Réveille</w> <w n="67.2">et</w> <w n="67.3">galvanise</w></l>
						<l n="68" num="9.4"><space unit="char" quantity="2"></space><w n="68.1">Leur</w> <w n="68.2">amas</w> <w n="68.3">gisant</w>,</l>
						<l n="69" num="9.5"><w n="69.1">Et</w> <w n="69.2">dans</w> <w n="69.3">sa</w> <w n="69.4">feuille</w> <w n="69.5">blonde</w>,</l>
						<l n="70" num="9.6"><w n="70.1">Au</w> <w n="70.2">vent</w> <w n="70.3">qui</w> <w n="70.4">le</w> <w n="70.5">seconde</w>,</l>
						<l n="71" num="9.7"><w n="71.1">Chaque</w> <w n="71.2">sylphe</w> <w n="71.3">à</w> <w n="71.4">la</w> <w n="71.5">ronde</w></l>
						<l n="72" num="9.8"><space unit="char" quantity="2"></space><w n="72.1">Tournoie</w> <w n="72.2">en</w> <w n="72.3">dansant</w>.</l>
					</lg>
					<lg n="10">
						<l n="73" num="10.1"><w n="73.1">Mais</w> <w n="73.2">le</w> <w n="73.3">joyeux</w> <w n="73.4">cortège</w></l>
						<l n="74" num="10.2"><space unit="char" quantity="2"></space><w n="74.1">Retombe</w> <w n="74.2">tremblant</w>.</l>
						<l n="75" num="10.3"><w n="75.1">Seul</w> <w n="75.2">bientôt</w> <w n="75.3">sur</w> <w n="75.4">la</w> <w n="75.5">neige</w></l>
						<l n="76" num="10.4"><space unit="char" quantity="2"></space><w n="76.1">L</w>’<w n="76.2">autan</w> <w n="76.3">va</w> <w n="76.4">sifflant</w>…</l>
						<l n="77" num="10.5"><w n="77.1">Dans</w> <w n="77.2">vos</w> <w n="77.3">feuilles</w> <w n="77.4">roulées</w>,</l>
						<l n="78" num="10.6"><w n="78.1">Doux</w> <w n="78.2">sylphes</w> <w n="78.3">des</w> <w n="78.4">vallées</w>,</l>
						<l n="79" num="10.7"><w n="79.1">Dormez</w>, <w n="79.2">troupes</w> <w n="79.3">voilées</w>,</l>
						<l n="80" num="10.8"><space unit="char" quantity="2"></space><w n="80.1">Sous</w> <w n="80.2">un</w> <w n="80.3">linceul</w> <w n="80.4">blanc</w> !</l>
					</lg>
				</div></body></text></TEI>