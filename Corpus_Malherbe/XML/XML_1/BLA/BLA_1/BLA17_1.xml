<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="sub_2">TOME PREMIER</title>
				<title type="medium">Édition électronique</title>
				<author key="BLA">
					<name>
						<forename>Prosper</forename>
						<surname>BLANCHEMAIN</surname>
					</name>
					<date from="1816" to="1879">1816-1879</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>7196 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">BLA_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME PREMIER</title>
						<author>Prosper Blanchemain</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/pomesetposie01blan</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
								<title>TOME PREMIER</title>
								<author>Prosper Blanchemain</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE ANCIENNE ET MODERNE - ÉDOUARD ROUVEYRE</publisher>
									<date when="1880">1880</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME I</title>
						<title>POÈMES ET POÉSIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME V</title>
						<title>SONNETS ET FANTAISIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1845" to="1858">1845-1858</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">POÈMES ET POÉSIES</head><div type="poem" key="BLA17">
					<head type="main">LA COLOMBE BLANCHE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Dis</w>-<w n="1.2">moi</w>, <w n="1.3">dis</w>-<w n="1.4">moi</w>, <w n="1.5">blanche</w> <w n="1.6">colombe</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Ou</w> <w n="2.2">vas</w>-<w n="2.3">tu</w> <w n="2.4">d</w>’<w n="2.5">un</w> <w n="2.6">vol</w> <w n="2.7">si</w> <w n="2.8">léger</w> ?</l>
						<l n="3" num="1.3"><w n="3.1">Viens</w>-<w n="3.2">tu</w> <w n="3.3">du</w> <w n="3.4">ciel</w> <w n="3.5">ou</w> <w n="3.6">de</w> <w n="3.7">la</w> <w n="3.8">tombe</w> ?</l>
						<l n="4" num="1.4"><w n="4.1">Qu</w>’<w n="4.2">apportes</w>-<w n="4.3">tu</w>, <w n="4.4">doux</w> <w n="4.5">messager</w> ?</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">— <w n="5.1">Ma</w> <w n="5.2">patrie</w> <w n="5.3">est</w> <w n="5.4">aux</w> <w n="5.5">cieux</w> ; <w n="5.6">la</w> <w n="5.7">terre</w></l>
						<l n="6" num="2.2"><w n="6.1">N</w>’<w n="6.2">a</w> <w n="6.3">jamais</w> <w n="6.4">touché</w> <w n="6.5">mes</w> <w n="6.6">pieds</w> <w n="6.7">nus</w>.</l>
						<l n="7" num="2.3"><w n="7.1">En</w> <w n="7.2">Grèce</w> <w n="7.3">je</w> <w n="7.4">fus</w> <w n="7.5">Pérystère</w>,</l>
						<l n="8" num="2.4"><w n="8.1">Blanche</w> <w n="8.2">compagne</w> <w n="8.3">de</w> <w n="8.4">Vénus</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Mahomet</w> <w n="9.2">aux</w> <w n="9.3">croyants</w> <w n="9.4">fidèles</w></l>
						<l n="10" num="3.2"><w n="10.1">Me</w> <w n="10.2">montre</w> <w n="10.3">au</w> <w n="10.4">milieu</w> <w n="10.5">des</w> <w n="10.6">Houris</w>,</l>
						<l n="11" num="3.3"><w n="11.1">Et</w> <w n="11.2">l</w>’<w n="11.3">Inde</w> <w n="11.4">croit</w> <w n="11.5">que</w> <w n="11.6">sous</w> <w n="11.7">mes</w> <w n="11.8">ailes</w></l>
						<l n="12" num="3.4"><w n="12.1">Brahma</w> <w n="12.2">réchauffe</w> <w n="12.3">les</w> <w n="12.4">Péris</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Le</w> <w n="13.2">Christ</w> <w n="13.3">m</w>’<w n="13.4">orna</w> <w n="13.5">d</w>’<w n="13.6">une</w> <w n="13.7">auréole</w>,</l>
						<l n="14" num="4.2"><w n="14.1">Et</w> <w n="14.2">j</w>’<w n="14.3">apparais</w> <w n="14.4">sur</w> <w n="14.5">les</w> <w n="14.6">autels</w></l>
						<l n="15" num="4.3"><w n="15.1">Comme</w> <w n="15.2">un</w> <w n="15.3">mystérieux</w> <w n="15.4">symbole</w></l>
						<l n="16" num="4.4"><w n="16.1">Au</w>-<w n="16.2">dessus</w> <w n="16.3">du</w> <w n="16.4">sens</w> <w n="16.5">des</w> <w n="16.6">mortels</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">C</w>’<w n="17.2">est</w> <w n="17.3">moi</w> <w n="17.4">qui</w> <w n="17.5">porte</w> <w n="17.6">la</w> <w n="17.7">couronne</w></l>
						<l n="18" num="5.2"><w n="18.1">Et</w> <w n="18.2">du</w> <w n="18.3">martyre</w> <w n="18.4">et</w> <w n="18.5">du</w> <w n="18.6">bonheur</w> ;</l>
						<l n="19" num="5.3"><w n="19.1">J</w>’<w n="19.2">aime</w>, <w n="19.3">je</w> <w n="19.4">bénis</w>, <w n="19.5">je</w> <w n="19.6">pardonne</w>,</l>
						<l n="20" num="5.4"><w n="20.1">Je</w> <w n="20.2">suis</w> <w n="20.3">l</w>’<w n="20.4">Esprit</w>-<w n="20.5">Saint</w> <w n="20.6">du</w> <w n="20.7">Seigneur</w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Je</w> <w n="21.2">suis</w> <w n="21.3">cette</w> <w n="21.4">blanche</w> <w n="21.5">colombe</w></l>
						<l n="22" num="6.2"><w n="22.1">Qui</w> <w n="22.2">sauve</w> <w n="22.3">et</w> <w n="22.4">console</w> <w n="22.5">toujours</w>,</l>
						<l n="23" num="6.3"><w n="23.1">Oiseau</w> <w n="23.2">du</w> <w n="23.3">ciel</w> <w n="23.4">et</w> <w n="23.5">de</w> <w n="23.6">la</w> <w n="23.7">tombe</w>,</l>
						<l n="24" num="6.4"><w n="24.1">Oiseau</w> <w n="24.2">des</w> <w n="24.3">dernières</w> <w n="24.4">amours</w>.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">Au</w> <w n="25.2">pauvre</w> <w n="25.3">tremblant</w> <w n="25.4">dans</w> <w n="25.5">la</w> <w n="25.6">boue</w></l>
						<l n="26" num="7.2"><w n="26.1">J</w>’<w n="26.2">envoie</w> <w n="26.3">un</w> <w n="26.4">peu</w> <w n="26.5">de</w> <w n="26.6">l</w>’<w n="26.7">or</w> <w n="26.8">d</w>’<w n="26.9">Ophir</w>,</l>
						<l n="27" num="7.3"><w n="27.1">Au</w> <w n="27.2">roseau</w> <w n="27.3">que</w> <w n="27.4">le</w> <w n="27.5">vent</w> <w n="27.6">secoue</w></l>
						<l n="28" num="7.4"><w n="28.1">Je</w> <w n="28.2">ramène</w> <w n="28.3">un</w> <w n="28.4">plus</w> <w n="28.5">doux</w> <w n="28.6">zéphyr</w>.</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1"><w n="29.1">Au</w> <w n="29.2">magistrat</w> <w n="29.3">je</w> <w n="29.4">dis</w> : « <w n="29.5">Fais</w> <w n="29.6">grâce</w> ! »</l>
						<l n="30" num="8.2"><w n="30.1">Au</w> <w n="30.2">pontife</w> : « <w n="30.3">Sache</w> <w n="30.4">bénir</w> ! »</l>
						<l n="31" num="8.3"><w n="31.1">A</w> <w n="31.2">l</w>’<w n="31.3">homme</w> <w n="31.4">heureux</w> : « <w n="31.5">Le</w> <w n="31.6">bonheur</w> <w n="31.7">passe</w> ! »</l>
						<l n="32" num="8.4"><w n="32.1">Au</w> <w n="32.2">malheureux</w> : « <w n="32.3">Il</w> <w n="32.4">va</w> <w n="32.5">venir</w>. »</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1"><w n="33.1">Au</w> <w n="33.2">tombeau</w> <w n="33.3">qui</w> <w n="33.4">de</w> <w n="33.5">pleurs</w> <w n="33.6">s</w>’<w n="33.7">arrose</w>,</l>
						<l n="34" num="9.2"><w n="34.1">Je</w> <w n="34.2">dis</w> : « <w n="34.3">Fais</w>, <w n="34.4">tombeau</w> <w n="34.5">triste</w> <w n="34.6">et</w> <w n="34.7">noir</w>,</l>
						<l n="35" num="9.3"><w n="35.1">De</w> <w n="35.2">chaque</w> <w n="35.3">pleur</w> <w n="35.4">naître</w> <w n="35.5">une</w> <w n="35.6">rose</w>,</l>
						<l n="36" num="9.4"><w n="36.1">De</w> <w n="36.2">chaque</w> <w n="36.3">douleur</w> <w n="36.4">un</w> <w n="36.5">espoir</w>. »</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1"><w n="37.1">Je</w> <w n="37.2">suis</w> <w n="37.3">cette</w> <w n="37.4">blanche</w> <w n="37.5">colombe</w></l>
						<l n="38" num="10.2"><w n="38.1">Qui</w> <w n="38.2">sauve</w> <w n="38.3">et</w> <w n="38.4">console</w> <w n="38.5">toujours</w>,</l>
						<l n="39" num="10.3"><w n="39.1">Oiseau</w> <w n="39.2">du</w> <w n="39.3">ciel</w> <w n="39.4">et</w> <w n="39.5">de</w> <w n="39.6">la</w> <w n="39.7">tombe</w>,</l>
						<l n="40" num="10.4"><w n="40.1">Oiseau</w> <w n="40.2">des</w> <w n="40.3">dernières</w> <w n="40.4">amours</w>.</l>
					</lg>
					<lg n="11">
						<l n="41" num="11.1">— <w n="41.1">Blanche</w> <w n="41.2">colombe</w> <w n="41.3">qui</w> <w n="41.4">consoles</w>,</l>
						<l n="42" num="11.2"><w n="42.1">Accours</w> <w n="42.2">à</w> <w n="42.3">moi</w> <w n="42.4">d</w>’<w n="42.5">un</w> <w n="42.6">vol</w> <w n="42.7">léger</w>.</l>
						<l n="43" num="11.3"><w n="43.1">Écoute</w>, <w n="43.2">écoute</w> <w n="43.3">mes</w> <w n="43.4">paroles</w> ;</l>
						<l n="44" num="11.4"><w n="44.1">Je</w> <w n="44.2">sais</w> <w n="44.3">des</w> <w n="44.4">maux</w> <w n="44.5">à</w> <w n="44.6">soulager</w>.</l>
					</lg>
					<lg n="12">
						<l n="45" num="12.1"><w n="45.1">Auprès</w> <w n="45.2">d</w>’<w n="45.3">une</w> <w n="45.4">champêtre</w> <w n="45.5">église</w>,</l>
						<l n="46" num="12.2"><w n="46.1">Dans</w> <w n="46.2">le</w> <w n="46.3">champ</w> <w n="46.4">d</w>’<w n="46.5">éternel</w> <w n="46.6">repos</w>,</l>
						<l n="47" num="12.3"><w n="47.1">Une</w> <w n="47.2">croix</w> <w n="47.3">de</w> <w n="47.4">bois</w> <w n="47.5">est</w> <w n="47.6">assise</w></l>
						<l n="48" num="12.4"><w n="48.1">Entre</w> <w n="48.2">des</w> <w n="48.3">lis</w> <w n="48.4">et</w> <w n="48.5">des</w> <w n="48.6">pavots</w>.</l>
					</lg>
					<lg n="13">
						<l n="49" num="13.1"><w n="49.1">Sans</w> <w n="49.2">doute</w> <w n="49.3">une</w> <w n="49.4">lumière</w> <w n="49.5">sainte</w></l>
						<l n="50" num="13.2"><w n="50.1">Devra</w> <w n="50.2">te</w> <w n="50.3">guider</w> <w n="50.4">dans</w> <w n="50.5">ces</w> <w n="50.6">lieux</w>,</l>
						<l n="51" num="13.3"><w n="51.1">D</w>’<w n="51.2">où</w>, <w n="51.3">lorsque</w> <w n="51.4">la</w> <w n="51.5">vie</w> <w n="51.6">est</w> <w n="51.7">éteinte</w>,</l>
						<l n="52" num="13.4"><w n="52.1">L</w>’<w n="52.2">âme</w> <w n="52.3">s</w>’<w n="52.4">élance</w> <w n="52.5">vers</w> <w n="52.6">les</w> <w n="52.7">cieux</w> ;</l>
					</lg>
					<lg n="14">
						<l n="53" num="14.1"><w n="53.1">Car</w> <w n="53.2">c</w>’<w n="53.3">est</w> <w n="53.4">là</w> <w n="53.5">que</w>, <w n="53.6">du</w> <w n="53.7">corps</w> <w n="53.8">de</w> <w n="53.9">fange</w>,</l>
						<l n="54" num="14.2"><w n="54.1">L</w>’<w n="54.2">âme</w> <w n="54.3">de</w> <w n="54.4">l</w>’<w n="54.5">enfant</w> <w n="54.6">regretté</w></l>
						<l n="55" num="14.3"><w n="55.1">S</w>’<w n="55.2">éleva</w>, <w n="55.3">comme</w> <w n="55.4">un</w> <w n="55.5">lis</w> <w n="55.6">qu</w>’<w n="55.7">un</w> <w n="55.8">ange</w></l>
						<l n="56" num="14.4"><w n="56.1">Dans</w> <w n="56.2">l</w>’<w n="56.3">espace</w> <w n="56.4">aurait</w> <w n="56.5">emporté</w>.</l>
					</lg>
					<lg n="15">
						<l n="57" num="15.1"><w n="57.1">Vole</w>, <w n="57.2">vole</w>, <w n="57.3">blanche</w> <w n="57.4">colombe</w> !</l>
						<l n="58" num="15.2"><w n="58.1">Sauve</w> <w n="58.2">et</w> <w n="58.3">console</w>-<w n="58.4">nous</w> <w n="58.5">toujours</w>,</l>
						<l n="59" num="15.3"><w n="59.1">Oiseau</w> <w n="59.2">du</w> <w n="59.3">ciel</w> <w n="59.4">et</w> <w n="59.5">de</w> <w n="59.6">la</w> <w n="59.7">tombe</w>,</l>
						<l n="60" num="15.4"><w n="60.1">Oiseau</w> <w n="60.2">des</w> <w n="60.3">dernières</w> <w n="60.4">amours</w>.</l>
					</lg>
					<lg n="16">
						<l n="61" num="16.1"><w n="61.1">Monte</w>, <w n="61.2">avec</w> <w n="61.3">la</w> <w n="61.4">chaste</w> <w n="61.5">prière</w></l>
						<l n="62" num="16.2"><w n="62.1">Et</w> <w n="62.2">l</w>’<w n="62.3">encens</w> <w n="62.4">brûlé</w> <w n="62.5">sur</w> <w n="62.6">l</w>’<w n="62.7">autel</w> ;</l>
						<l n="63" num="16.3"><w n="63.1">Trouve</w> <w n="63.2">cette</w> <w n="63.3">âme</w> <w n="63.4">de</w> <w n="63.5">lumière</w></l>
						<l n="64" num="16.4"><w n="64.1">Dans</w> <w n="64.2">les</w> <w n="64.3">plus</w> <w n="64.4">beaux</w> <w n="64.5">parvis</w> <w n="64.6">du</w> <w n="64.7">ciel</w>.</l>
					</lg>
					<lg n="17">
						<l n="65" num="17.1"><w n="65.1">Parle</w>, <w n="65.2">comme</w> <w n="65.3">au</w> <w n="65.4">roi</w> <w n="65.5">sur</w> <w n="65.6">son</w> <w n="65.7">trône</w>,</l>
						<l n="66" num="17.2"><w n="66.1">A</w> <w n="66.2">ce</w> <w n="66.3">nouveau</w>-<w n="66.4">né</w> <w n="66.5">du</w> <w n="66.6">Seigneur</w>,</l>
						<l n="67" num="17.3"><w n="67.1">Et</w> <w n="67.2">demande</w>-<w n="67.3">lui</w> <w n="67.4">son</w> <w n="67.5">aumône</w></l>
						<l n="68" num="17.4"><w n="68.1">D</w>’<w n="68.2">un</w> <w n="68.3">peu</w> <w n="68.4">d</w>’<w n="68.5">amour</w> <w n="68.6">et</w> <w n="68.7">de</w> <w n="68.8">bonheur</w>.</l>
					</lg>
					<lg n="18">
						<l n="69" num="18.1"><w n="69.1">Si</w> <w n="69.2">quelque</w> <w n="69.3">retour</w> <w n="69.4">vers</w> <w n="69.5">la</w> <w n="69.6">terre</w></l>
						<l n="70" num="18.2"><w n="70.1">Arrache</w> <w n="70.2">une</w> <w n="70.3">larme</w> <w n="70.4">à</w> <w n="70.5">ses</w> <w n="70.6">yeux</w>,</l>
						<l n="71" num="18.3"><w n="71.1">Verse</w> <w n="71.2">dans</w> <w n="71.3">le</w> <w n="71.4">cœur</w> <w n="71.5">de</w> <w n="71.6">sa</w> <w n="71.7">mère</w></l>
						<l n="72" num="18.4"><w n="72.1">Ce</w> <w n="72.2">diamant</w> <w n="72.3">tombé</w> <w n="72.4">des</w> <w n="72.5">cieux</w> ;</l>
					</lg>
					<lg n="19">
						<l n="73" num="19.1"><w n="73.1">Et</w> <w n="73.2">quand</w>, <w n="73.3">d</w>’<w n="73.4">amertume</w> <w n="73.5">oppressée</w>,</l>
						<l n="74" num="19.2"><w n="74.1">Elle</w> <w n="74.2">pleurera</w>, <w n="74.3">que</w> <w n="74.4">ses</w> <w n="74.5">pleurs</w></l>
						<l n="75" num="19.3"><w n="75.1">Soient</w> <w n="75.2">comme</w> <w n="75.3">la</w> <w n="75.4">douce</w> <w n="75.5">rosée</w></l>
						<l n="76" num="19.4"><w n="76.1">Qui</w> <w n="76.2">brille</w> <w n="76.3">au</w> <w n="76.4">matin</w> <w n="76.5">sur</w> <w n="76.6">les</w> <w n="76.7">fleurs</w>.</l>
					</lg>
					<lg n="20">
						<l n="77" num="20.1"><w n="77.1">Vole</w>, <w n="77.2">vole</w>, <w n="77.3">blanche</w> <w n="77.4">colombe</w> !</l>
						<l n="78" num="20.2"><w n="78.1">Sauve</w> <w n="78.2">et</w> <w n="78.3">console</w>-<w n="78.4">nous</w> <w n="78.5">toujours</w>,</l>
						<l n="79" num="20.3"><w n="79.1">Oiseau</w> <w n="79.2">du</w> <w n="79.3">ciel</w> <w n="79.4">et</w> <w n="79.5">de</w> <w n="79.6">la</w> <w n="79.7">tombe</w>,</l>
						<l n="80" num="20.4"><w n="80.1">Oiseau</w> <w n="80.2">des</w> <w n="80.3">dernières</w> <w n="80.4">amours</w>.</l>
					</lg>
				</div></body></text></TEI>