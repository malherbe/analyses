<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="sub_2">TOME PREMIER</title>
				<title type="medium">Édition électronique</title>
				<author key="BLA">
					<name>
						<forename>Prosper</forename>
						<surname>BLANCHEMAIN</surname>
					</name>
					<date from="1816" to="1879">1816-1879</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>7196 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">BLA_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME PREMIER</title>
						<author>Prosper Blanchemain</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/pomesetposie01blan</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
								<title>TOME PREMIER</title>
								<author>Prosper Blanchemain</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE ANCIENNE ET MODERNE - ÉDOUARD ROUVEYRE</publisher>
									<date when="1880">1880</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME I</title>
						<title>POÈMES ET POÉSIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME V</title>
						<title>SONNETS ET FANTAISIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1845" to="1858">1845-1858</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">POÈMES ET POÉSIES</head><div type="poem" key="BLA41">
					<head type="main">LE RUISSEAU</head>
					<head type="form">BALLADE SUÉDOISE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">La</w> <w n="1.2">jeune</w> <w n="1.3">fille</w>, <w n="1.4">assise</w> <w n="1.5">sur</w> <w n="1.6">la</w> <w n="1.7">rive</w>,</l>
						<l n="2" num="1.2"><space unit="char" quantity="4"></space><w n="2.1">Baigne</w> <w n="2.2">ses</w> <w n="2.3">pieds</w> <w n="2.4">dans</w> <w n="2.5">le</w> <w n="2.6">ruisseau</w> ;</l>
						<l n="3" num="1.3"><w n="3.1">Quand</w> <w n="3.2">une</w> <w n="3.3">voix</w> <w n="3.4">à</w> <w n="3.5">son</w> <w n="3.6">oreille</w> <w n="3.7">arrive</w>,</l>
						<l n="4" num="1.4"><space unit="char" quantity="4"></space><w n="4.1">En</w> <w n="4.2">gémissant</w>, <w n="4.3">du</w> <w n="4.4">fond</w> <w n="4.5">de</w> <w n="4.6">l</w>’<w n="4.7">eau</w> :</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">« <w n="5.1">Que</w> <w n="5.2">faites</w>-<w n="5.3">vous</w>, <w n="5.4">enfant</w> <w n="5.5">aux</w> <w n="5.6">tresses</w> <w n="5.7">blondes</w> ?</l>
						<l n="6" num="2.2"><space unit="char" quantity="4"></space><w n="6.1">Ne</w> <w n="6.2">troublez</w> <w n="6.3">pas</w> <w n="6.4">mon</w> <w n="6.5">cristal</w> <w n="6.6">pur</w>.</l>
						<l n="7" num="2.3"><w n="7.1">Je</w> <w n="7.2">ne</w> <w n="7.3">vois</w> <w n="7.4">plus</w> <w n="7.5">dans</w> <w n="7.6">mes</w> <w n="7.7">limpides</w> <w n="7.8">ondes</w></l>
						<l n="8" num="2.4"><space unit="char" quantity="4"></space><w n="8.1">Se</w> <w n="8.2">refléter</w> <w n="8.3">le</w> <w n="8.4">ciel</w> <w n="8.5">d</w>’<w n="8.6">azur</w>. »</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Les</w> <w n="9.2">yeux</w> <w n="9.3">en</w> <w n="9.4">pleurs</w>, <w n="9.5">la</w> <w n="9.6">triste</w> <w n="9.7">jeune</w> <w n="9.8">fille</w></l>
						<l n="10" num="3.2"><space unit="char" quantity="4"></space><w n="10.1">Se</w> <w n="10.2">penche</w> <w n="10.3">et</w> <w n="10.4">dit</w> : « <w n="10.5">Ruisseau</w> <w n="10.6">plaintif</w>,</l>
						<l n="11" num="3.3"><w n="11.1">Ne</w> <w n="11.2">gémis</w> <w n="11.3">pas</w> <w n="11.4">si</w> <w n="11.5">ton</w> <w n="11.6">cristal</w> <w n="11.7">qui</w> <w n="11.8">brille</w></l>
						<l n="12" num="3.4"><space unit="char" quantity="4"></space><w n="12.1">Se</w> <w n="12.2">trouble</w> <w n="12.3">sous</w> <w n="12.4">mon</w> <w n="12.5">pied</w> <w n="12.6">furtif</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">» <w n="13.1">L</w>’<w n="13.2">onde</w> <w n="13.3">mobile</w>, <w n="13.4">à</w> <w n="13.5">présent</w> <w n="13.6">agitée</w>.</l>
						<l n="14" num="4.2"><space unit="char" quantity="4"></space><w n="14.1">Dans</w> <w n="14.2">un</w> <w n="14.3">moment</w> <w n="14.4">s</w>’<w n="14.5">éclaircira</w>,</l>
						<l n="15" num="4.3"><w n="15.1">Et</w> <w n="15.2">de</w> <w n="15.3">l</w>’<w n="15.4">azur</w> <w n="15.5">l</w>’<w n="15.6">image</w> <w n="15.7">reflétée</w></l>
						<l n="16" num="4.4"><space unit="char" quantity="4"></space><w n="16.1">Plus</w> <w n="16.2">brillante</w> <w n="16.3">y</w> <w n="16.4">resplendira</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">» <w n="17.1">Mais</w> <w n="17.2">quand</w> <w n="17.3">tu</w> <w n="17.4">vis</w> <w n="17.5">ce</w> <w n="17.6">jeune</w> <w n="17.7">homme</w> <w n="17.8">sourire</w></l>
						<l n="18" num="5.2"><space unit="char" quantity="4"></space><w n="18.1">En</w> <w n="18.2">me</w> <w n="18.3">parlant</w> <w n="18.4">à</w> <w n="18.5">deux</w> <w n="18.6">genoux</w>,</l>
						<l n="19" num="5.3"><w n="19.1">C</w>’<w n="19.2">était</w> <w n="19.3">à</w> <w n="19.4">lui</w> <w n="19.5">qu</w>’<w n="19.6">il</w> <w n="19.7">aurait</w> <w n="19.8">fallu</w> <w n="19.9">dire</w> :</l>
						<l n="20" num="5.4"><space unit="char" quantity="4"></space>« <w n="20.1">O</w> <w n="20.2">jeune</w> <w n="20.3">homme</w>, <w n="20.4">que</w> <w n="20.5">faites</w>-<w n="20.6">vous</w> ?</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1">» <w n="21.1">N</w>’<w n="21.2">agitez</w> <w n="21.3">pas</w> <w n="21.4">d</w>’<w n="21.5">un</w> <w n="21.6">trouble</w> <w n="21.7">qu</w>’<w n="21.8">il</w> <w n="21.9">ignore</w></l>
						<l n="22" num="6.2"><space unit="char" quantity="4"></space>» <w n="22.1">Ce</w> <w n="22.2">cœur</w> <w n="22.3">pur</w> <w n="22.4">et</w> <w n="22.5">silencieux</w>,</l>
						<l n="23" num="6.3">» <w n="23.1">Qui</w> <w n="23.2">ne</w> <w n="23.3">pourra</w> <w n="23.4">ni</w> <w n="23.5">s</w>’<w n="23.6">éclaircir</w> <w n="23.7">encore</w>,</l>
						<l n="24" num="6.4"><space unit="char" quantity="4"></space>» <w n="24.1">Ni</w> <w n="24.2">refléter</w> <w n="24.3">l</w>’<w n="24.4">azur</w> <w n="24.5">des</w> <w n="24.6">cieux</w> ! »</l>
					</lg>
				</div></body></text></TEI>