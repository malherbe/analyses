<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="sub_2">TOME PREMIER</title>
				<title type="medium">Édition électronique</title>
				<author key="BLA">
					<name>
						<forename>Prosper</forename>
						<surname>BLANCHEMAIN</surname>
					</name>
					<date from="1816" to="1879">1816-1879</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>7196 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">BLA_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME PREMIER</title>
						<author>Prosper Blanchemain</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/pomesetposie01blan</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
								<title>TOME PREMIER</title>
								<author>Prosper Blanchemain</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE ANCIENNE ET MODERNE - ÉDOUARD ROUVEYRE</publisher>
									<date when="1880">1880</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME I</title>
						<title>POÈMES ET POÉSIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME V</title>
						<title>SONNETS ET FANTAISIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1845" to="1858">1845-1858</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">POÈMES ET POÉSIES</head><div type="poem" key="BLA9">
					<head type="main">PENSÉE DE NUIT</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Voici</w> <w n="1.2">l</w>’<w n="1.3">heure</w> <w n="1.4">silencieuse</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Dans</w> <w n="2.2">l</w>’<w n="2.3">ombre</w> <w n="2.4">le</w> <w n="2.5">monde</w> <w n="2.6">s</w>’<w n="2.7">est</w> <w n="2.8">tu</w>.</l>
						<l n="3" num="1.3"><w n="3.1">O</w> <w n="3.2">nuit</w> ! <w n="3.3">quels</w> <w n="3.4">dons</w> <w n="3.5">amènes</w>-<w n="3.6">tu</w></l>
						<l n="4" num="1.4"><w n="4.1">A</w> <w n="4.2">mon</w> <w n="4.3">âme</w> <w n="4.4">triste</w> <w n="4.5">et</w> <w n="4.6">rêveuse</w> ?</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Le</w> <w n="5.2">sol</w> <w n="5.3">desséché</w> <w n="5.4">par</w> <w n="5.5">le</w> <w n="5.6">jour</w></l>
						<l n="6" num="2.2"><w n="6.1">Boit</w> <w n="6.2">ta</w> <w n="6.3">fraîcheur</w> <w n="6.4">tiède</w> <w n="6.5">et</w> <w n="6.6">charmante</w> ;</l>
						<l n="7" num="2.3"><w n="7.1">Pour</w> <w n="7.2">la</w> <w n="7.3">flamme</w> <w n="7.4">qui</w> <w n="7.5">me</w> <w n="7.6">tourmente</w></l>
						<l n="8" num="2.4"><w n="8.1">N</w>’<w n="8.2">as</w>-<w n="8.3">tu</w> <w n="8.4">pas</w> <w n="8.5">un</w> <w n="8.6">baume</w> <w n="8.7">d</w>’<w n="8.8">amour</w> !</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">O</w> <w n="9.2">nuit</w> ! <w n="9.3">quand</w> <w n="9.4">l</w>’<w n="9.5">absence</w> <w n="9.6">m</w>’<w n="9.7">enlève</w></l>
						<l n="10" num="3.2"><w n="10.1">Ma</w> <w n="10.2">bien</w>-<w n="10.3">aimée</w> <w n="10.4">avec</w> <w n="10.5">mon</w> <w n="10.6">cœur</w>,</l>
						<l n="11" num="3.3"><w n="11.1">Au</w> <w n="11.2">moins</w>, <w n="11.3">à</w> <w n="11.4">défaut</w> <w n="11.5">du</w> <w n="11.6">bonheur</w>,</l>
						<l n="12" num="3.4"><w n="12.1">Ne</w> <w n="12.2">peux</w>-<w n="12.3">tu</w> <w n="12.4">m</w>’<w n="12.5">en</w> <w n="12.6">donner</w> <w n="12.7">le</w> <w n="12.8">rêve</w> ?</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Porte</w>, <w n="13.2">sur</w> <w n="13.3">l</w>’<w n="13.4">aile</w> <w n="13.5">du</w> <w n="13.6">sommeil</w>,</l>
						<l n="14" num="4.2"><w n="14.1">Mes</w> <w n="14.2">songes</w> <w n="14.3">vers</w> <w n="14.4">la</w> <w n="14.5">jeune</w> <w n="14.6">fille</w>,</l>
						<l n="15" num="4.3"><w n="15.1">Dévoile</w>-<w n="15.2">moi</w> <w n="15.3">son</w> <w n="15.4">œil</w> <w n="15.5">qui</w> <w n="15.6">brille</w>,</l>
						<l n="16" num="4.4"><w n="16.1">Son</w> <w n="16.2">visage</w> <w n="16.3">frais</w> <w n="16.4">et</w> <w n="16.5">vermeil</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Je</w> <w n="17.2">veux</w> <w n="17.3">m</w>’<w n="17.4">incliner</w> <w n="17.5">sur</w> <w n="17.6">sa</w> <w n="17.7">couche</w>,</l>
						<l n="18" num="5.2"><w n="18.1">Dans</w> <w n="18.2">l</w>’<w n="18.3">ombre</w> <w n="18.4">deviner</w> <w n="18.5">ses</w> <w n="18.6">traits</w> ;</l>
						<l n="19" num="5.3"><w n="19.1">Je</w> <w n="19.2">veux</w> <w n="19.3">épier</w> <w n="19.4">les</w> <w n="19.5">secrets</w></l>
						<l n="20" num="5.4"><w n="20.1">Qui</w> <w n="20.2">passent</w> <w n="20.3">sans</w> <w n="20.4">bruit</w> <w n="20.5">sur</w> <w n="20.6">sa</w> <w n="20.7">bouche</w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Son</w> <w n="21.2">cœur</w> <w n="21.3">sans</w> <w n="21.4">remords</w> <w n="21.5">et</w> <w n="21.6">sans</w> <w n="21.7">fiel</w></l>
						<l n="22" num="6.2"><w n="22.1">Ne</w> <w n="22.2">peut</w> <w n="22.3">voiler</w> <w n="22.4">ou</w> <w n="22.5">haine</w> <w n="22.6">ou</w> <w n="22.7">blâme</w> ;</l>
						<l n="23" num="6.3"><w n="23.1">Elle</w> <w n="23.2">est</w> <w n="23.3">pure</w> <w n="23.4">comme</w> <w n="23.5">la</w> <w n="23.6">flamme</w>,</l>
						<l n="24" num="6.4"><w n="24.1">Elle</w> <w n="24.2">est</w> <w n="24.3">belle</w> <w n="24.4">comme</w> <w n="24.5">un</w> <w n="24.6">beau</w> <w n="24.7">ciel</w>.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">Car</w> <w n="25.2">toujours</w> <w n="25.3">l</w>’<w n="25.4">ange</w> <w n="25.5">de</w> <w n="25.6">lumière</w></l>
						<l n="26" num="7.2"><w n="26.1">Qu</w>’<w n="26.2">elle</w> <w n="26.3">prie</w> <w n="26.4">et</w> <w n="26.5">qui</w> <w n="26.6">la</w> <w n="26.7">conduit</w>,</l>
						<l n="27" num="7.3"><w n="27.1">Lui</w> <w n="27.2">fait</w> <w n="27.3">son</w> <w n="27.4">repos</w> <w n="27.5">de</w> <w n="27.6">la</w> <w n="27.7">nuit</w></l>
						<l n="28" num="7.4"><w n="28.1">Aussi</w> <w n="28.2">chaste</w> <w n="28.3">que</w> <w n="28.4">sa</w> <w n="28.5">prière</w>.</l>
					</lg>
				</div></body></text></TEI>