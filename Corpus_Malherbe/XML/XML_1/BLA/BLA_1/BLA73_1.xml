<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="sub_2">TOME PREMIER</title>
				<title type="medium">Édition électronique</title>
				<author key="BLA">
					<name>
						<forename>Prosper</forename>
						<surname>BLANCHEMAIN</surname>
					</name>
					<date from="1816" to="1879">1816-1879</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>7196 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">BLA_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME PREMIER</title>
						<author>Prosper Blanchemain</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/pomesetposie01blan</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
								<title>TOME PREMIER</title>
								<author>Prosper Blanchemain</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE ANCIENNE ET MODERNE - ÉDOUARD ROUVEYRE</publisher>
									<date when="1880">1880</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME I</title>
						<title>POÈMES ET POÉSIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME V</title>
						<title>SONNETS ET FANTAISIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1845" to="1858">1845-1858</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">IDÉAL</head><div type="poem" key="BLA73">
					<head type="main">AVRIL</head>
					<opener>
						<salute>A Achille Millien.</salute>
					</opener>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Mois</w> <w n="1.2">d</w>’<w n="1.3">ivresses</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Qui</w> <w n="2.2">nous</w> <w n="2.3">laisses</w></l>
						<l n="3" num="1.3"><w n="3.1">Tes</w> <w n="3.2">richesses</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Mois</w> <w n="4.2">d</w>’<w n="4.3">Avril</w>,</l>
						<l n="5" num="1.5"><w n="5.1">Qui</w> <w n="5.2">rappelles</w></l>
						<l n="6" num="1.6"><w n="6.1">Les</w> <w n="6.2">fidèles</w></l>
						<l n="7" num="1.7"><w n="7.1">Hirondelles</w></l>
						<l n="8" num="1.8"><w n="8.1">De</w> <w n="8.2">l</w>’<w n="8.3">exil</w> ;</l>
					</lg>
					<lg n="2">
						<l n="9" num="2.1"><w n="9.1">Sur</w> <w n="9.2">ta</w> <w n="9.3">trace</w></l>
						<l n="10" num="2.2"><w n="10.1">Dans</w> <w n="10.2">l</w>’<w n="10.3">espace</w>,</l>
						<l n="11" num="2.3"><w n="11.1">Zéphyr</w> <w n="11.2">chasse</w></l>
						<l n="12" num="2.4"><w n="12.1">Les</w> <w n="12.2">autans</w> ;</l>
						<l n="13" num="2.5"><w n="13.1">Chaque</w> <w n="13.2">aurore</w></l>
						<l n="14" num="2.6"><w n="14.1">Qui</w> <w n="14.2">te</w> <w n="14.3">dore</w></l>
						<l n="15" num="2.7"><w n="15.1">Fait</w> <w n="15.2">éclore</w></l>
						<l n="16" num="2.8"><w n="16.1">Un</w> <w n="16.2">printemps</w>.</l>
					</lg>
					<lg n="3">
						<l n="17" num="3.1"><w n="17.1">Rien</w> <w n="17.2">n</w>’<w n="17.3">outrage</w></l>
						<l n="18" num="3.2"><w n="18.1">Ton</w> <w n="18.2">feuillage</w> ;</l>
						<l n="19" num="3.3"><w n="19.1">Point</w> <w n="19.2">d</w>’<w n="19.3">orage</w></l>
						<l n="20" num="3.4"><w n="20.1">Importun</w>.</l>
						<l n="21" num="3.5"><w n="21.1">Toute</w> <w n="21.2">rose</w></l>
						<l n="22" num="3.6"><w n="22.1">Est</w> <w n="22.2">éclose</w></l>
						<l n="23" num="3.7"><w n="23.1">Et</w> <w n="23.2">t</w>’<w n="23.3">arrose</w></l>
						<l n="24" num="3.8"><w n="24.1">De</w> <w n="24.2">parfum</w>.</l>
					</lg>
					<lg n="4">
						<l n="25" num="4.1"><w n="25.1">La</w> <w n="25.2">pervenche</w></l>
						<l n="26" num="4.2"><w n="26.1">Bleue</w> <w n="26.2">et</w> <w n="26.3">blanche</w></l>
						<l n="27" num="4.3"><w n="27.1">Au</w> <w n="27.2">vent</w> <w n="27.3">penche</w></l>
						<l n="28" num="4.4"><w n="28.1">Tout</w> <w n="28.2">en</w> <w n="28.3">pleurs</w> ;</l>
						<l n="29" num="4.5"><w n="29.1">Et</w> <w n="29.2">l</w>’<w n="29.3">abeille</w></l>
						<l n="30" num="4.6"><w n="30.1">Qui</w> <w n="30.2">sommeille</w></l>
						<l n="31" num="4.7"><w n="31.1">Se</w> <w n="31.2">réveille</w></l>
						<l n="32" num="4.8"><w n="32.1">Dans</w> <w n="32.2">les</w> <w n="32.3">fleurs</w>.</l>
					</lg>
					<lg n="5">
						<l n="33" num="5.1"><w n="33.1">La</w> <w n="33.2">fauvette</w>,</l>
						<l n="34" num="5.2"><w n="34.1">Qui</w> <w n="34.2">béquette</w></l>
						<l n="35" num="5.3"><w n="35.1">Et</w> <w n="35.2">caquette</w></l>
						<l n="36" num="5.4"><w n="36.1">Tout</w> <w n="36.2">le</w> <w n="36.3">jour</w>,</l>
						<l n="37" num="5.5"><w n="37.1">Sémillante</w>,</l>
						<l n="38" num="5.6"><w n="38.1">Sautillante</w>,</l>
						<l n="39" num="5.7"><w n="39.1">Vole</w> <w n="39.2">et</w> <w n="39.3">chante</w></l>
						<l n="40" num="5.8"><w n="40.1">Tour</w> <w n="40.2">à</w> <w n="40.3">tour</w>.</l>
					</lg>
					<lg n="6">
						<l n="41" num="6.1"><w n="41.1">Seul</w> <w n="41.2">le</w> <w n="41.3">tremble</w></l>
						<l n="42" num="6.2"><w n="42.1">Là</w>-<w n="42.2">bas</w> <w n="42.3">tremble</w> ;</l>
						<l n="43" num="6.3"><w n="43.1">Le</w> <w n="43.2">lac</w> <w n="43.3">semble</w></l>
						<l n="44" num="6.4"><w n="44.1">Un</w> <w n="44.2">miroir</w> ;</l>
						<l n="45" num="6.5"><w n="45.1">Et</w> <w n="45.2">chaque</w> <w n="45.3">île</w>,</l>
						<l n="46" num="6.6"><w n="46.1">Frais</w> <w n="46.2">asile</w>,</l>
						<l n="47" num="6.7"><w n="47.1">Y</w> <w n="47.2">vacille</w>,</l>
						<l n="48" num="6.8"><w n="48.1">Belle</w> <w n="48.2">à</w> <w n="48.3">voir</w>.</l>
					</lg>
					<lg n="7">
						<l n="49" num="7.1"><w n="49.1">Là</w> <w n="49.2">s</w>’<w n="49.3">incline</w></l>
						<l n="50" num="7.2"><w n="50.1">La</w> <w n="50.2">colline</w></l>
						<l n="51" num="7.3"><w n="51.1">Que</w> <w n="51.2">domine</w></l>
						<l n="52" num="7.4"><w n="52.1">Un</w> <w n="52.2">clocher</w>.</l>
						<l n="53" num="7.5"><w n="53.1">Dans</w> <w n="53.2">l</w>’<w n="53.3">enceinte</w></l>
						<l n="54" num="7.6"><w n="54.1">L</w>’<w n="54.2">airain</w> <w n="54.3">tinte</w></l>
						<l n="55" num="7.7"><w n="55.1">Pour</w> <w n="55.2">la</w> <w n="55.3">sainte</w></l>
						<l n="56" num="7.8"><w n="56.1">Du</w> <w n="56.2">rocher</w>.</l>
					</lg>
					<lg n="8">
						<l n="57" num="8.1"><w n="57.1">Là</w> <w n="57.2">sans</w> <w n="57.3">cesse</w></l>
						<l n="58" num="8.2"><w n="58.1">Tout</w> <w n="58.2">se</w> <w n="58.3">presse</w>,</l>
						<l n="59" num="8.3"><w n="59.1">Chants</w> <w n="59.2">d</w>’<w n="59.3">ivresse</w>,</l>
						<l n="60" num="8.4"><w n="60.1">Pleurs</w> <w n="60.2">d</w>’<w n="60.3">adieu</w> ;</l>
						<l n="61" num="8.5"><w n="61.1">La</w> <w n="61.2">prière</w></l>
						<l n="62" num="8.6"><w n="62.1">Solitaire</w></l>
						<l n="63" num="8.7"><w n="63.1">De</w> <w n="63.2">la</w> <w n="63.3">terre</w></l>
						<l n="64" num="8.8"><w n="64.1">Monte</w> <w n="64.2">à</w> <w n="64.3">Dieu</w>.</l>
					</lg>
					<lg n="9">
						<l n="65" num="9.1"><w n="65.1">Tout</w> <w n="65.2">au</w> <w n="65.3">monde</w>,</l>
						<l n="66" num="9.2"><w n="66.1">Fauvette</w>, <w n="66.2">onde</w>,</l>
						<l n="67" num="9.3"><w n="67.1">Fleur</w> <w n="67.2">qu</w>’<w n="67.3">inonde</w></l>
						<l n="68" num="9.4"><w n="68.1">Un</w> <w n="68.2">doux</w> <w n="68.3">miel</w>,</l>
						<l n="69" num="9.5"><w n="69.1">Fraîche</w> <w n="69.2">brise</w>,</l>
						<l n="70" num="9.6"><w n="70.1">Roche</w> <w n="70.2">grise</w>,</l>
						<l n="71" num="9.7"><w n="71.1">Vieille</w> <w n="71.2">église</w>,</l>
						<l n="72" num="9.8"><w n="72.1">Terre</w> <w n="72.2">ou</w> <w n="72.3">ciel</w>,</l>
					</lg>
					<lg n="10">
						<l n="73" num="10.1"><w n="73.1">Tout</w> <w n="73.2">soupire</w>,</l>
						<l n="74" num="10.2"><w n="74.1">Tout</w> <w n="74.2">respire</w></l>
						<l n="75" num="10.3"><w n="75.1">Le</w> <w n="75.2">délire</w></l>
						<l n="76" num="10.4"><w n="76.1">Du</w> <w n="76.2">bonheur</w>.</l>
						<l n="77" num="10.5"><w n="77.1">Harmonies</w></l>
						<l n="78" num="10.6"><w n="78.1">Infinies</w>,</l>
						<l n="79" num="10.7"><w n="79.1">Voix</w> <w n="79.2">bénies</w></l>
						<l n="80" num="10.8"><w n="80.1">Du</w> <w n="80.2">Seigneur</w> !…</l>
					</lg>
				</div></body></text></TEI>