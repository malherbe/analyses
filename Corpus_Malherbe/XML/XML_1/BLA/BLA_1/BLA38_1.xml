<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="sub_2">TOME PREMIER</title>
				<title type="medium">Édition électronique</title>
				<author key="BLA">
					<name>
						<forename>Prosper</forename>
						<surname>BLANCHEMAIN</surname>
					</name>
					<date from="1816" to="1879">1816-1879</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>7196 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">BLA_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME PREMIER</title>
						<author>Prosper Blanchemain</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/pomesetposie01blan</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
								<title>TOME PREMIER</title>
								<author>Prosper Blanchemain</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE ANCIENNE ET MODERNE - ÉDOUARD ROUVEYRE</publisher>
									<date when="1880">1880</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME I</title>
						<title>POÈMES ET POÉSIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME V</title>
						<title>SONNETS ET FANTAISIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1845" to="1858">1845-1858</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">POÈMES ET POÉSIES</head><div type="poem" key="BLA38">
					<head type="main">AMOUR</head>
					<opener>
						<salute>A Marie Désirée.</salute>
					</opener>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Lorsque</w> <w n="1.2">je</w> <w n="1.3">vois</w> <w n="1.4">tes</w> <w n="1.5">yeux</w>, <w n="1.6">ma</w> <w n="1.7">plus</w> <w n="1.8">vive</w> <w n="1.9">souffrance</w></l>
						<l n="2" num="1.2"><space unit="char" quantity="8"></space><w n="2.1">Sous</w> <w n="2.2">ton</w> <w n="2.3">regard</w> <w n="2.4">s</w>’<w n="2.5">évanouit</w> ;</l>
						<l n="3" num="1.3"><w n="3.1">Lorsque</w> <w n="3.2">tu</w> <w n="3.3">me</w> <w n="3.4">souris</w>, <w n="3.5">la</w> <w n="3.6">fleur</w> <w n="3.7">de</w> <w n="3.8">l</w>’<w n="3.9">espérance</w></l>
						<l n="4" num="1.4"><space unit="char" quantity="8"></space><w n="4.1">En</w> <w n="4.2">moi</w> <w n="4.3">brille</w> <w n="4.4">et</w> <w n="4.5">s</w> <w n="4.6">épanouit</w> !</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Quand</w> <w n="5.2">tu</w> <w n="5.3">me</w> <w n="5.4">dis</w> : « <w n="5.5">Je</w> <w n="5.6">t</w>’<w n="5.7">aime</w> ! » <w n="5.8">ô</w> <w n="5.9">ma</w> <w n="5.10">seule</w> <w n="5.11">adorée</w> ;</l>
						<l n="6" num="2.2"><space unit="char" quantity="8"></space><w n="6.1">Les</w> <w n="6.2">cieux</w> <w n="6.3">alors</w> <w n="6.4">me</w> <w n="6.5">sont</w> <w n="6.6">ouverts</w>,</l>
						<l n="7" num="2.3"><w n="7.1">La</w> <w n="7.2">terre</w> <w n="7.3">est</w> <w n="7.4">trop</w> <w n="7.5">étroite</w> <w n="7.6">à</w> <w n="7.7">mon</w> <w n="7.8">âme</w> <w n="7.9">enivrée</w>,</l>
						<l n="8" num="2.4"><space unit="char" quantity="8"></space><w n="8.1">Je</w> <w n="8.2">suis</w> <w n="8.3">le</w> <w n="8.4">roi</w> <w n="8.5">de</w> <w n="8.6">l</w>’<w n="8.7">univers</w> !</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Je</w> <w n="9.2">suis</w> <w n="9.3">poète</w> <w n="9.4">alors</w> ; <w n="9.5">car</w> <w n="9.6">les</w> <w n="9.7">feux</w> <w n="9.8">du</w> <w n="9.9">génie</w></l>
						<l n="10" num="3.2"><space unit="char" quantity="8"></space><w n="10.1">Rayonnent</w> <w n="10.2">sur</w> <w n="10.3">moi</w> <w n="10.4">de</w> <w n="10.5">tes</w> <w n="10.6">yeux</w> ;</l>
						<l n="11" num="3.3"><w n="11.1">Car</w> <w n="11.2">de</w> <w n="11.3">ton</w> <w n="11.4">cœur</w> <w n="11.5">au</w> <w n="11.6">mien</w> <w n="11.7">un</w> <w n="11.8">fleuve</w> <w n="11.9">d</w>’<w n="11.10">harmonie</w></l>
						<l n="12" num="3.4"><space unit="char" quantity="8"></space><w n="12.1">Descend</w> <w n="12.2">à</w> <w n="12.3">flots</w> <w n="12.4">mélodieux</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Mais</w> <w n="13.2">viens</w>-<w n="13.3">tu</w> <w n="13.4">par</w> <w n="13.5">hasard</w> <w n="13.6">à</w> <w n="13.7">détourner</w> <w n="13.8">la</w> <w n="13.9">tète</w>,</l>
						<l n="14" num="4.2"><space unit="char" quantity="8"></space><w n="14.1">Je</w> <w n="14.2">m</w>’<w n="14.3">égare</w> <w n="14.4">en</w> <w n="14.5">tristes</w> <w n="14.6">accords</w> ;</l>
						<l n="15" num="4.3"><w n="15.1">Ma</w> <w n="15.2">poésie</w> <w n="15.3">en</w> <w n="15.4">deuil</w> <w n="15.5">perd</w> <w n="15.6">ses</w> <w n="15.7">habits</w> <w n="15.8">de</w> <w n="15.9">fête</w>,</l>
						<l n="16" num="4.4"><space unit="char" quantity="8"></space><w n="16.1">Ma</w> <w n="16.2">voix</w> <w n="16.3">et</w> <w n="16.4">mon</w> <w n="16.5">bonheur</w> <w n="16.6">sont</w> <w n="16.7">morts</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Et</w> <w n="17.2">si</w> <w n="17.3">je</w> <w n="17.4">cherche</w> <w n="17.5">encore</w> <w n="17.6">au</w> <w n="17.7">dedans</w> <w n="17.8">de</w> <w n="17.9">moi</w>-<w n="17.10">même</w>,</l>
						<l n="18" num="5.2"><space unit="char" quantity="8"></space><w n="18.1">Comme</w> <w n="18.2">on</w> <w n="18.3">cherche</w> <w n="18.4">un</w> <w n="18.5">songe</w> <w n="18.6">au</w> <w n="18.7">réveil</w>,</l>
						<l n="19" num="5.3"><w n="19.1">Ce</w> <w n="19.2">que</w> <w n="19.3">chantait</w> <w n="19.4">mon</w> <w n="19.5">cœur</w> <w n="19.6">quand</w> <w n="19.7">tu</w> <w n="19.8">disais</w> : « <w n="19.9">Je</w> <w n="19.10">t</w>’<w n="19.11">aime</w> !</l>
						<l n="20" num="5.4"><space unit="char" quantity="8"></space><w n="20.1">Quand</w> <w n="20.2">brillait</w> <w n="20.3">ton</w> <w n="20.4">regard</w> <w n="20.5">vermeil</w>,</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">La</w> <w n="21.2">joie</w> <w n="21.3">et</w> <w n="21.4">le</w> <w n="21.5">sourire</w>, <w n="21.6">avec</w> <w n="21.7">ton</w> <w n="21.8">doux</w> <w n="21.9">visage</w>,</l>
						<l n="22" num="6.2"><space unit="char" quantity="8"></space><w n="22.1">Abandonnent</w> <w n="22.2">mon</w> <w n="22.3">front</w> <w n="22.4">penché</w> :</l>
						<l n="23" num="6.3"><w n="23.1">L</w>’<w n="23.2">oiseau</w> <w n="23.3">mélodieux</w> <w n="23.4">se</w> <w n="23.5">tait</w> <w n="23.6">sous</w> <w n="23.7">le</w> <w n="23.8">feuillage</w>,</l>
						<l n="24" num="6.4"><space unit="char" quantity="8"></space><w n="24.1">Lorsque</w> <w n="24.2">le</w> <w n="24.3">soleil</w> <w n="24.4">est</w> <w n="24.5">caché</w>.</l>
					</lg>
				</div></body></text></TEI>