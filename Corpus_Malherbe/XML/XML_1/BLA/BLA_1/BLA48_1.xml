<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="sub_2">TOME PREMIER</title>
				<title type="medium">Édition électronique</title>
				<author key="BLA">
					<name>
						<forename>Prosper</forename>
						<surname>BLANCHEMAIN</surname>
					</name>
					<date from="1816" to="1879">1816-1879</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>7196 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">BLA_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME PREMIER</title>
						<author>Prosper Blanchemain</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/pomesetposie01blan</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
								<title>TOME PREMIER</title>
								<author>Prosper Blanchemain</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE ANCIENNE ET MODERNE - ÉDOUARD ROUVEYRE</publisher>
									<date when="1880">1880</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME I</title>
						<title>POÈMES ET POÉSIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME V</title>
						<title>SONNETS ET FANTAISIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1845" to="1858">1845-1858</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">POÈMES ET POÉSIES</head><div type="poem" key="BLA48">
					<head type="main">LES DEUX ANGES</head>
					<opener>
						<salute>A mon cher fils Paul.</salute>
					</opener>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Quand</w> <w n="1.2">j</w>’<w n="1.3">ignorais</w> <w n="1.4">encor</w> <w n="1.5">que</w> <w n="1.6">la</w> <w n="1.7">vie</w> <w n="1.8">est</w> <w n="1.9">amère</w> ;</l>
						<l n="2" num="1.2"><w n="2.1">Quand</w> <w n="2.2">je</w> <w n="2.3">ne</w> <w n="2.4">connaissais</w>, <w n="2.5">de</w> <w n="2.6">ce</w> <w n="2.7">monde</w> <w n="2.8">nouveau</w>,</l>
						<l n="3" num="1.3"><space unit="char" quantity="8"></space><w n="3.1">Que</w> <w n="3.2">la</w> <w n="3.3">douce</w> <w n="3.4">voix</w> <w n="3.5">de</w> <w n="3.6">ma</w> <w n="3.7">mère</w></l>
						<l n="4" num="1.4"><w n="4.1">Et</w> <w n="4.2">son</w> <w n="4.3">visage</w> <w n="4.4">aimant</w> <w n="4.5">penché</w> <w n="4.6">sur</w> <w n="4.7">mon</w> <w n="4.8">berceau</w>,</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Un</w> <w n="5.2">ange</w> <w n="5.3">radieux</w> <w n="5.4">me</w> <w n="5.5">souriait</w> <w n="5.6">en</w> <w n="5.7">rêve</w> ;</l>
						<l n="6" num="2.2"><w n="6.1">Sa</w> <w n="6.2">tète</w> <w n="6.3">aux</w> <w n="6.4">cheveux</w> <w n="6.5">blonds</w> <w n="6.6">brillait</w> <w n="6.7">d</w>’<w n="6.8">un</w> <w n="6.9">éclat</w> <w n="6.10">pur</w>,</l>
						<l n="7" num="2.3"><space unit="char" quantity="8"></space><w n="7.1">Comme</w> <w n="7.2">l</w>’<w n="7.3">aurore</w> <w n="7.4">qui</w> <w n="7.5">se</w> <w n="7.6">lève</w>,</l>
						<l n="8" num="2.4"><w n="8.1">Et</w> <w n="8.2">je</w> <w n="8.3">lisais</w> <w n="8.4">l</w>’<w n="8.5">espoir</w> <w n="8.6">dans</w> <w n="8.7">ses</w> <w n="8.8">grands</w> <w n="8.9">yeux</w> <w n="8.10">d</w>’<w n="8.11">azur</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Il</w> <w n="9.2">volait</w> <w n="9.3">devant</w> <w n="9.4">moi</w> <w n="9.5">tel</w> <w n="9.6">qu</w>’<w n="9.7">un</w> <w n="9.8">oiseau</w> <w n="9.9">rapide</w>,</l>
						<l n="10" num="3.2"><w n="10.1">Me</w> <w n="10.2">couvrant</w> <w n="10.3">de</w> <w n="10.4">son</w> <w n="10.5">aile</w>, <w n="10.6">et</w> <w n="10.7">la</w> <w n="10.8">paix</w> <w n="10.9">du</w> <w n="10.10">Seigneur</w></l>
						<l n="11" num="3.3"><space unit="char" quantity="8"></space><w n="11.1">Inondait</w> <w n="11.2">mon</w> <w n="11.3">âme</w> <w n="11.4">limpide</w> ;</l>
						<l n="12" num="3.4"><w n="12.1">Car</w> <w n="12.2">cet</w> <w n="12.3">ange</w> <w n="12.4">adoré</w> <w n="12.5">s</w>’<w n="12.6">appelait</w> <w n="12.7">le</w> <w n="12.8">bonheur</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Mais</w> <w n="13.2">les</w> <w n="13.3">jours</w> <w n="13.4">ont</w> <w n="13.5">passé</w> ; <w n="13.6">le</w> <w n="13.7">messager</w> <w n="13.8">de</w> <w n="13.9">joie</w></l>
						<l n="14" num="4.2"><w n="14.1">Qui</w> <w n="14.2">devançait</w> <w n="14.3">mes</w> <w n="14.4">pas</w> <w n="14.5">s</w>’<w n="14.6">est</w> <w n="14.7">lassé</w> <w n="14.8">du</w> <w n="14.9">chemin</w> ;’</l>
						<l n="15" num="4.3"><space unit="char" quantity="8"></space><w n="15.1">Il</w> <w n="15.2">s</w>’<w n="15.3">est</w> <w n="15.4">détourné</w> <w n="15.5">de</w> <w n="15.6">ma</w> <w n="15.7">voie</w>,</l>
						<l n="16" num="4.4"><w n="16.1">Sa</w> <w n="16.2">main</w> <w n="16.3">consolatrice</w> <w n="16.4">a</w> <w n="16.5">délaissé</w> <w n="16.6">ma</w> <w n="16.7">main</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">A</w> <w n="17.2">peine</w> <w n="17.3">si</w> <w n="17.4">parfois</w> <w n="17.5">je</w> <w n="17.6">l</w>’<w n="17.7">entrevois</w> <w n="17.8">encore</w>,</l>
						<l n="18" num="5.2"><w n="18.1">Cet</w> <w n="18.2">astre</w> <w n="18.3">qui</w> <w n="18.4">brilla</w> <w n="18.5">sur</w> <w n="18.6">mon</w> <w n="18.7">joyeux</w> <w n="18.8">matin</w>,</l>
						<l n="19" num="5.3"><space unit="char" quantity="8"></space><w n="19.1">Il</w> <w n="19.2">fuit</w>, <w n="19.3">pâlissant</w> <w n="19.4">météore</w>,</l>
						<l n="20" num="5.4"><w n="20.1">Et</w> <w n="20.2">se</w> <w n="20.3">perd</w> <w n="20.4">dans</w> <w n="20.5">la</w> <w n="20.6">brume</w> <w n="20.7">à</w> <w n="20.8">l</w>’<w n="20.9">horizon</w> <w n="20.10">lointain</w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Il</w> <w n="21.2">ne</w> <w n="21.3">reviendra</w> <w n="21.4">plus</w>, <w n="21.5">aux</w> <w n="21.6">heures</w> <w n="21.7">de</w> <w n="21.8">tristesse</w>,</l>
						<l n="22" num="6.2"><w n="22.1">Illuminer</w> <w n="22.2">ma</w> <w n="22.3">nuit</w> <w n="22.4">d</w>’<w n="22.5">un</w> <w n="22.6">rayon</w> <w n="22.7">de</w> <w n="22.8">ses</w> <w n="22.9">yeux</w> ;</l>
						<l n="23" num="6.3"><space unit="char" quantity="8"></space><w n="23.1">Dans</w> <w n="23.2">mon</w> <w n="23.3">cœur</w> <w n="23.4">ses</w> <w n="23.5">chants</w> <w n="23.6">d</w>’<w n="23.7">allégresse</w></l>
						<l n="24" num="6.4"><w n="24.1">Ne</w> <w n="24.2">réveilleront</w> <w n="24.3">plus</w> <w n="24.4">un</w> <w n="24.5">seul</w> <w n="24.6">écho</w> <w n="24.7">joyeux</w>.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">Doux</w> <w n="25.2">ange</w>, <w n="25.3">cher</w> <w n="25.4">soutien</w> <w n="25.5">de</w> <w n="25.6">mon</w> <w n="25.7">heureuse</w> <w n="25.8">enfance</w></l>
						<l n="26" num="7.2"><w n="26.1">Qui</w> <w n="26.2">me</w> <w n="26.3">guidais</w> <w n="26.4">partout</w>, <w n="26.5">quand</w> <w n="26.6">je</w> <w n="26.7">n</w>’<w n="26.8">avais</w> <w n="26.9">senti</w></l>
						<l n="27" num="7.3"><space unit="char" quantity="8"></space><w n="27.1">Ni</w> <w n="27.2">la</w> <w n="27.3">tristesse</w> <w n="27.4">ni</w> <w n="27.5">l</w>’<w n="27.6">offense</w>,</l>
						<l n="28" num="7.4"><w n="28.1">Pour</w> <w n="28.2">quels</w> <w n="28.3">bords</w> <w n="28.4">préférés</w>, <w n="28.5">doux</w> <w n="28.6">ange</w>, <w n="28.7">es</w>-<w n="28.8">tu</w> <w n="28.9">parti</w> ?</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1"><w n="29.1">Es</w>-<w n="29.2">tu</w> <w n="29.3">sous</w> <w n="29.4">l</w>’<w n="29.5">humble</w> <w n="29.6">toit</w> <w n="29.7">où</w> <w n="29.8">je</w> <w n="29.9">te</w> <w n="29.10">vis</w> <w n="29.11">sourire</w>,</l>
						<l n="30" num="8.2"><w n="30.1">Dans</w> <w n="30.2">les</w> <w n="30.3">yeux</w> <w n="30.4">de</w> <w n="30.5">ma</w> <w n="30.6">mère</w>, <w n="30.7">à</w> <w n="30.8">mon</w> <w n="30.9">premier</w> <w n="30.10">réveil</w> ?</l>
						<l n="31" num="8.3"><space unit="char" quantity="8"></space><w n="31.1">Aux</w> <w n="31.2">bords</w> <w n="31.3">du</w> <w n="31.4">ruisseau</w> <w n="31.5">qui</w> <w n="31.6">soupire</w> ?</l>
						<l n="32" num="8.4"><w n="32.1">Dans</w> <w n="32.2">le</w> <w n="32.3">bois</w> <w n="32.4">qui</w> <w n="32.5">frémit</w> <w n="32.6">au</w> <w n="32.7">lever</w> <w n="32.8">du</w> <w n="32.9">soleil</w> ?</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1"><w n="33.1">Es</w>-<w n="33.2">tu</w> <w n="33.3">sous</w> <w n="33.4">les</w> <w n="33.5">cyprès</w>, <w n="33.6">au</w> <w n="33.7">coin</w> <w n="33.8">du</w> <w n="33.9">cimetière</w>,</l>
						<l n="34" num="9.2"><w n="34.1">Où</w> <w n="34.2">j</w>’<w n="34.3">ai</w> <w n="34.4">vu</w> <w n="34.5">déposer</w> <w n="34.6">mon</w> <w n="34.7">frère</w>, <w n="34.8">mon</w> <w n="34.9">seul</w> <w n="34.10">bien</w>,</l>
						<l n="35" num="9.3"><space unit="char" quantity="8"></space><w n="35.1">Pauvre</w> <w n="35.2">enfant</w> <w n="35.3">qui</w> <w n="35.4">dort</w> <w n="35.5">sous</w> <w n="35.6">la</w> <w n="35.7">pierre</w>,</l>
						<l n="36" num="9.4"><w n="36.1">Cœur</w> <w n="36.2">brisé</w> <w n="36.3">qui</w> <w n="36.4">jamais</w> <w n="36.5">ne</w> <w n="36.6">battra</w> <w n="36.7">près</w> <w n="36.8">du</w> <w n="36.9">mien</w> ?</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1"><w n="37.1">Es</w>-<w n="37.2">tu</w> <w n="37.3">dans</w> <w n="37.4">un</w> <w n="37.5">baiser</w> <w n="37.6">de</w> <w n="37.7">celle</w> <w n="37.8">qui</w>, <w n="37.9">tranquille</w>,</l>
						<l n="38" num="10.2"><w n="38.1">Repose</w> <w n="38.2">sur</w> <w n="38.3">mon</w> <w n="38.4">sein</w> <w n="38.5">gonflé</w> <w n="38.6">d</w>’<w n="38.7">émotion</w> ;</l>
						<l n="39" num="10.3"><space unit="char" quantity="8"></space><w n="39.1">Comme</w> <w n="39.2">dans</w> <w n="39.3">son</w> <w n="39.4">nid</w> <w n="39.5">qui</w> <w n="39.6">vacille</w></l>
						<l n="40" num="10.4"><w n="40.1">Près</w> <w n="40.2">des</w> <w n="40.3">flots</w> <w n="40.4">orageux</w> <w n="40.5">se</w> <w n="40.6">berce</w> <w n="40.7">l</w>’<w n="40.8">alcyon</w> ?</l>
					</lg>
					<lg n="11">
						<l n="41" num="11.1"><w n="41.1">Es</w>-<w n="41.2">tu</w> <w n="41.3">dans</w> <w n="41.4">ces</w> <w n="41.5">milliers</w> <w n="41.6">de</w> <w n="41.7">limpides</w> <w n="41.8">étoiles</w>,</l>
						<l n="42" num="11.2"><w n="42.1">Dont</w> <w n="42.2">j</w>’<w n="42.3">admirais</w>, <w n="42.4">enfant</w>, <w n="42.5">l</w>’<w n="42.6">éclat</w> <w n="42.7">mystérieux</w> ?</l>
						<l n="43" num="11.3"><space unit="char" quantity="8"></space><w n="43.1">Où</w> <w n="43.2">te</w> <w n="43.3">caches</w>-<w n="43.4">tu</w> ? <w n="43.5">sous</w> <w n="43.6">quels</w> <w n="43.7">voiles</w> ?</l>
						<l n="44" num="11.4"><w n="44.1">Es</w>-<w n="44.2">tu</w> <w n="44.3">sur</w> <w n="44.4">terre</w> <w n="44.5">encore</w> ? <w n="44.6">as</w>-<w n="44.7">tu</w> <w n="44.8">fui</w> <w n="44.9">dans</w> <w n="44.10">les</w> <w n="44.11">cieux</w>.</l>
					</lg>
					<lg n="12">
						<l n="45" num="12.1"><w n="45.1">Hélas</w> ! <w n="45.2">c</w>’<w n="45.3">est</w> <w n="45.4">un</w> <w n="45.5">autre</w> <w n="45.6">ange</w> <w n="45.7">au</w> <w n="45.8">sévère</w> <w n="45.9">visage</w></l>
						<l n="46" num="12.2"><w n="46.1">Qui</w> <w n="46.2">me</w> <w n="46.3">montre</w> <w n="46.4">aujourd</w>’<w n="46.5">hui</w> <w n="46.6">l</w>’<w n="46.7">horizon</w> <w n="46.8">menaçant</w>.</l>
						<l n="47" num="12.3"><space unit="char" quantity="8"></space><w n="47.1">I</w> ! <w n="47.2">marche</w> <w n="47.3">au</w> <w n="47.4">milieu</w> <w n="47.5">d</w>’<w n="47.6">un</w> <w n="47.7">orage</w></l>
						<l n="48" num="12.4"><w n="48.1">Et</w> <w n="48.2">ses</w> <w n="48.3">yeux</w> <w n="48.4">sont</w> <w n="48.5">rougis</w> <w n="48.6">de</w> <w n="48.7">larmes</w> <w n="48.8">et</w> <w n="48.9">de</w> <w n="48.10">sang</w>.</l>
					</lg>
					<lg n="13">
						<l n="49" num="13.1"><w n="49.1">Lorsque</w> <w n="49.2">vers</w> <w n="49.3">l</w>’<w n="49.4">avenir</w>, <w n="49.5">où</w> <w n="49.6">maint</w> <w n="49.7">éclair</w> <w n="49.8">s</w>’<w n="49.9">allume</w>,</l>
						<l n="50" num="13.2"><w n="50.1">Il</w> <w n="50.2">tourne</w> <w n="50.3">son</w> <w n="50.4">œil</w> <w n="50.5">morne</w> <w n="50.6">et</w> <w n="50.7">son</w> <w n="50.8">front</w> <w n="50.9">sans</w> <w n="50.10">couleur</w></l>
						<l n="51" num="13.3"><space unit="char" quantity="8"></space><w n="51.1">Mon</w> <w n="51.2">cœur</w> <w n="51.3">se</w> <w n="51.4">gonfle</w> <w n="51.5">d</w>’<w n="51.6">amertume</w> ;</l>
						<l n="52" num="13.4"><w n="52.1">Car</w> <w n="52.2">cet</w> <w n="52.3">ange</w> <w n="52.4">effrayant</w> <w n="52.5">s</w>’<w n="52.6">appelle</w> <w n="52.7">LA</w> <w n="52.8">DOULEUR</w>.</l>
					</lg>
					<lg n="14">
						<l n="53" num="14.1"><w n="53.1">Adieu</w> <w n="53.2">donc</w> <w n="53.3">pour</w> <w n="53.4">jamais</w>, <w n="53.5">bel</w> <w n="53.6">ange</w> <w n="53.7">de</w> <w n="53.8">la</w> <w n="53.9">joie</w> !</l>
						<l n="54" num="14.2"><w n="54.1">Toi</w>, <w n="54.2">son</w> <w n="54.3">frère</w>, <w n="54.4">salut</w> ! <w n="54.5">je</w> <w n="54.6">t</w>’<w n="54.7">attends</w> <w n="54.8">sans</w> <w n="54.9">remord</w> ;</l>
						<l n="55" num="14.3"><space unit="char" quantity="8"></space><w n="55.1">C</w>’<w n="55.2">est</w> <w n="55.3">aussi</w> <w n="55.4">le</w> <w n="55.5">ciel</w> <w n="55.6">qui</w> <w n="55.7">t</w>’<w n="55.8">envoie</w>,</l>
						<l n="56" num="14.4"><w n="56.1">Ainsi</w> <w n="56.2">que</w> <w n="56.3">l</w>’<w n="56.4">ouragan</w>, <w n="56.5">la</w> <w n="56.6">tempête</w> <w n="56.7">et</w> <w n="56.8">la</w> <w n="56.9">mort</w>.</l>
					</lg>
					<lg n="15">
						<l n="57" num="15.1"><w n="57.1">Salut</w>, <w n="57.2">ange</w> <w n="57.3">des</w> <w n="57.4">pleurs</w> ! <w n="57.5">je</w> <w n="57.6">te</w> <w n="57.7">crains</w> <w n="57.8">et</w> <w n="57.9">je</w> <w n="57.10">t</w>’<w n="57.11">aime</w>.</l>
						<l n="58" num="15.2"><w n="58.1">Je</w> <w n="58.2">te</w> <w n="58.3">crains</w>, <w n="58.4">car</w> <w n="58.5">ton</w> <w n="58.6">œil</w> <w n="58.7">est</w> <w n="58.8">noir</w> <w n="58.9">comme</w> <w n="58.10">la</w> <w n="58.11">nuit</w> ;</l>
						<l n="59" num="15.3"><space unit="char" quantity="8"></space><w n="59.1">Je</w> <w n="59.2">t</w>’<w n="59.3">aime</w>, <w n="59.4">car</w> <w n="59.5">sur</w> <w n="59.6">ton</w> <w n="59.7">front</w> <w n="59.8">blême</w></l>
						<l n="60" num="15.4"><w n="60.1">J</w>’<w n="60.2">entrevois</w> <w n="60.3">un</w> <w n="60.4">reflet</w> <w n="60.5">du</w> <w n="60.6">jour</w> <w n="60.7">pur</w> <w n="60.8">qui</w> <w n="60.9">te</w> <w n="60.10">suit</w>.</l>
					</lg>
					<lg n="16">
						<l n="61" num="16.1"><w n="61.1">Je</w> <w n="61.2">te</w> <w n="61.3">crains</w>, <w n="61.4">car</w> <w n="61.5">ta</w> <w n="61.6">main</w> <w n="61.7">fatale</w> <w n="61.8">et</w> <w n="61.9">toujours</w> <w n="61.10">sûre</w></l>
						<l n="62" num="16.2"><w n="62.1">Ne</w> <w n="62.2">doit</w> <w n="62.3">toucher</w> <w n="62.4">mon</w> <w n="62.5">cœur</w> <w n="62.6">que</w> <w n="62.7">pour</w> <w n="62.8">l</w>’<w n="62.9">endolorir</w> ;</l>
						<l n="63" num="16.3"><space unit="char" quantity="8"></space><w n="63.1">Je</w> <w n="63.2">t</w>’<w n="63.3">aime</w>, <w n="63.4">car</w> <w n="63.5">chaque</w> <w n="63.6">blessure</w></l>
						<l n="64" num="16.4"><w n="64.1">Rend</w> <w n="64.2">mon</w> <w n="64.3">âme</w> <w n="64.4">plus</w> <w n="64.5">forte</w> <w n="64.6">et</w> <w n="64.7">m</w>’<w n="64.8">apprend</w> <w n="64.9">à</w> <w n="64.10">souffrir</w>.</l>
					</lg>
					<lg n="17">
						<l n="65" num="17.1"><w n="65.1">Frappe</w> <w n="65.2">donc</w> ! <w n="65.3">Je</w> <w n="65.4">suis</w> <w n="65.5">prêt</w>. <w n="65.6">Bien</w> <w n="65.7">que</w> <w n="65.8">mon</w> <w n="65.9">cœur</w> <w n="65.10">chancelle</w></l>
						<l n="66" num="17.2"><w n="66.1">L</w>’<w n="66.2">espérance</w> <w n="66.3">y</w> <w n="66.4">survit</w>, <w n="66.5">rebelle</w> <w n="66.6">à</w> <w n="66.7">ton</w> <w n="66.8">pouvoir</w> ;</l>
						<l n="67" num="17.3"><space unit="char" quantity="8"></space><w n="67.1">La</w> <w n="67.2">Consolatrice</w> <w n="67.3">éternelle</w></l>
						<l n="68" num="17.4"><w n="68.1">Dort</w> <w n="68.2">jusque</w> <w n="68.3">dans</w> <w n="68.4">les</w> <w n="68.5">plis</w> <w n="68.6">de</w> <w n="68.7">ton</w> <w n="68.8">vêtement</w> <w n="68.9">noir</w>.</l>
					</lg>
					<lg n="18">
						<l n="69" num="18.1"><w n="69.1">Que</w> <w n="69.2">dis</w>-<w n="69.3">je</w>, <w n="69.4">Esprit</w> <w n="69.5">fatal</w> ? <w n="69.6">mon</w> <w n="69.7">cœur</w> <w n="69.8">n</w>’<w n="69.9">est</w> <w n="69.10">point</w> <w n="69.11">ta</w> <w n="69.12">proie</w>,</l>
						<l n="70" num="18.2"><w n="70.1">Un</w> <w n="70.2">astre</w> <w n="70.3">brille</w> <choice reason="analysis" hand="RR" type="false_verse"><sic>encore</sic><corr source="edition_1866"><w n="70.4">encor</w></corr></choice> <w n="70.5">dans</w> <w n="70.6">mon</w> <w n="70.7">ciel</w> <w n="70.8">assombri</w>,</l>
						<l n="71" num="18.3"><space unit="char" quantity="8"></space><w n="71.1">Car</w> <w n="71.2">je</w> <w n="71.3">vois</w> <w n="71.4">l</w>’<w n="71.5">ange</w> <w n="71.6">de</w> <w n="71.7">la</w> <w n="71.8">joie</w></l>
						<l n="72" num="18.4"><w n="72.1">S</w>’<w n="72.2">éveiller</w> <w n="72.3">dans</w> <w n="72.4">les</w> <w n="72.5">yeux</w> <w n="72.6">de</w> <w n="72.7">mon</w> <w n="72.8">enfant</w> <w n="72.9">chéri</w>.</l>
					</lg>
				</div></body></text></TEI>