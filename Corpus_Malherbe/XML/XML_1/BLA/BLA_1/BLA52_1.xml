<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="sub_2">TOME PREMIER</title>
				<title type="medium">Édition électronique</title>
				<author key="BLA">
					<name>
						<forename>Prosper</forename>
						<surname>BLANCHEMAIN</surname>
					</name>
					<date from="1816" to="1879">1816-1879</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>7196 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">BLA_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME PREMIER</title>
						<author>Prosper Blanchemain</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/pomesetposie01blan</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
								<title>TOME PREMIER</title>
								<author>Prosper Blanchemain</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE ANCIENNE ET MODERNE - ÉDOUARD ROUVEYRE</publisher>
									<date when="1880">1880</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME I</title>
						<title>POÈMES ET POÉSIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME V</title>
						<title>SONNETS ET FANTAISIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1845" to="1858">1845-1858</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">POÈMES ET POÉSIES</head><div type="poem" key="BLA52">
					<head type="main">LA DERNIÈRE PAGE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Vers</w> ! <w n="1.2">songes</w> <w n="1.3">de</w> <w n="1.4">passé</w>, <w n="1.5">de</w> <w n="1.6">présent</w>, <w n="1.7">d</w>’<w n="1.8">avenir</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Tantôt</w> <w n="2.2">tristes</w>, <w n="2.3">cédant</w> <w n="2.4">au</w> <w n="2.5">poids</w> <w n="2.6">du</w> <w n="2.7">souvenir</w> ;</l>
						<l n="3" num="1.3"><space unit="char" quantity="8"></space><w n="3.1">Tantôt</w> <w n="3.2">gais</w> <w n="3.3">enfants</w> <w n="3.4">de</w> <w n="3.5">mes</w> <w n="3.6">veilles</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Joyeux</w> <w n="4.2">comme</w> <w n="4.3">captifs</w> <w n="4.4">qui</w> <w n="4.5">laissent</w> <w n="4.6">la</w> <w n="4.7">prison</w> ;</l>
						<l n="5" num="1.5"><w n="5.1">Tantôt</w> <w n="5.2">rêveurs</w>, <w n="5.3">cherchant</w> <w n="5.4">un</w> <w n="5.5">lointain</w> <w n="5.6">horizon</w></l>
						<l n="6" num="1.6"><space unit="char" quantity="8"></space><w n="6.1">Que</w> <w n="6.2">l</w>’<w n="6.3">amour</w> <w n="6.4">peuple</w> <w n="6.5">de</w> <w n="6.6">merveilles</w> !</l>
					</lg>
					<lg n="2">
						<l n="7" num="2.1"><w n="7.1">Vous</w> <w n="7.2">voilà</w> <w n="7.3">réunis</w> !… <w n="7.4">et</w> <w n="7.5">moi</w>, <w n="7.6">sur</w> <w n="7.7">le</w> <w n="7.8">passé</w>,</l>
						<l n="8" num="2.2"><w n="8.1">Comme</w> <w n="8.2">le</w> <w n="8.3">laboureur</w> <w n="8.4">sur</w> <w n="8.5">le</w> <w n="8.6">sillon</w> <w n="8.7">tracé</w>,</l>
						<l n="9" num="2.3"><space unit="char" quantity="8"></space><w n="9.1">Je</w> <w n="9.2">jette</w> <w n="9.3">un</w> <w n="9.4">coup</w> <w n="9.5">d</w>’<w n="9.6">œil</w> <w n="9.7">en</w> <w n="9.8">arrière</w>,</l>
						<l n="10" num="2.4">><w n="10.1">Et</w> <w n="10.2">je</w> <w n="10.3">vous</w> <w n="10.4">vois</w> <w n="10.5">tout</w> <w n="10.6">prêts</w> <w n="10.7">à</w> <w n="10.8">prendre</w> <w n="10.9">votre</w> <w n="10.10">essor</w> !</l>
						<l n="11" num="2.5"><w n="11.1">L</w>’<w n="11.2">obscurité</w> <w n="11.3">vous</w> <w n="11.4">sied</w>, <w n="11.5">votre</w> <w n="11.6">aile</w> <w n="11.7">est</w> <w n="11.8">faible</w> <w n="11.9">encor</w> :</l>
						<l n="12" num="2.6"><space unit="char" quantity="8"></space><w n="12.1">Pourquoi</w> <w n="12.2">tentez</w>-<w n="12.3">vous</w> <w n="12.4">la</w> <w n="12.5">carrière</w> ?</l>
					</lg>
					<lg n="3">
						<l n="13" num="3.1"><w n="13.1">Et</w> <w n="13.2">je</w> <w n="13.3">vous</w> <w n="13.4">vois</w> <w n="13.5">tout</w> <w n="13.6">prêts</w>, <w n="13.7">ainsi</w> <w n="13.8">que</w> <w n="13.9">dans</w> <w n="13.10">nos</w> <w n="13.11">bois</w></l>
						<l n="14" num="3.2"><w n="14.1">Une</w> <w n="14.2">troupe</w> <w n="14.3">d</w>’<w n="14.4">oiseaux</w>, <w n="14.5">pour</w> <w n="14.6">la</w> <w n="14.7">première</w> <w n="14.8">fois</w></l>
						<l n="15" num="3.3"><space unit="char" quantity="8"></space><w n="15.1">Essayant</w> <w n="15.2">sa</w> <w n="15.3">plume</w> <w n="15.4">encor</w> <w n="15.5">molle</w>,</l>
						<l n="16" num="3.4"><w n="16.1">Toute</w> <w n="16.2">ensemble</w> <w n="16.3">s</w>’<w n="16.4">avance</w> <w n="16.5">au</w> <w n="16.6">bord</w> <w n="16.7">du</w> <w n="16.8">même</w> <w n="16.9">nid</w>,</l>
						<l n="17" num="3.5"><w n="17.1">S</w>’<w n="17.2">appelle</w> <w n="17.3">de</w> <w n="17.4">la</w> <w n="17.5">voix</w>, <w n="17.6">s</w>’<w n="17.7">encourage</w>, <w n="17.8">s</w>’<w n="17.9">unit</w>,</l>
						<l n="18" num="3.6"><space unit="char" quantity="8"></space><w n="18.1">Et</w> <w n="18.2">puis</w> <w n="18.3">toute</w> <w n="18.4">ensemble</w> <w n="18.5">s</w>’<w n="18.6">envole</w>…</l>
					</lg>
					<lg n="4">
						<l n="19" num="4.1"><w n="19.1">Cherchez</w> <w n="19.2">des</w> <w n="19.3">cœurs</w> <w n="19.4">amis</w> ; <w n="19.5">je</w> <w n="19.6">vous</w> <w n="19.7">suivrai</w> <w n="19.8">des</w> <w n="19.9">yeux</w>,</l>
						<l n="20" num="4.2"><w n="20.1">Mais</w> <w n="20.2">ne</w> <w n="20.3">vous</w> <w n="20.4">fiez</w> <w n="20.5">pas</w> <w n="20.6">à</w> <w n="20.7">la</w> <w n="20.8">splendeur</w> <w n="20.9">des</w> <w n="20.10">cieux</w>,</l>
						<l n="21" num="4.3"><space unit="char" quantity="8"></space><w n="21.1">O</w> <w n="21.2">chère</w> <w n="21.3">et</w> <w n="21.4">débile</w> <w n="21.5">couvée</w> !</l>
						<l n="22" num="4.4"><w n="22.1">Ne</w> <w n="22.2">chantez</w> <w n="22.3">pas</w> <w n="22.4">trop</w> <w n="22.5">haut</w>, <w n="22.6">n</w>’<w n="22.7">allez</w> <w n="22.8">pas</w> <w n="22.9">trop</w> <w n="22.10">avant</w> ;</l>
						<l n="23" num="4.5"><w n="23.1">N</w>’<w n="23.2">exposez</w> <w n="23.3">pas</w> <w n="23.4">votre</w> <w n="23.5">aile</w> <w n="23.6">à</w> <w n="23.7">la</w> <w n="23.8">fureur</w> <w n="23.9">du</w> <w n="23.10">vent</w></l>
						<l n="24" num="4.6"><space unit="char" quantity="8"></space><w n="24.1">Que</w> <w n="24.2">vous</w> <w n="24.3">n</w>’<w n="24.4">avez</w> <w n="24.5">pas</w> <w n="24.6">éprouvée</w>.</l>
					</lg>
					<lg n="5">
						<l n="25" num="5.1"><w n="25.1">Livrez</w>-<w n="25.2">vous</w> <w n="25.3">aux</w> <w n="25.4">amis</w> <w n="25.5">qui</w> <w n="25.6">vous</w> <w n="25.7">tendent</w> <w n="25.8">les</w> <w n="25.9">bras</w> :</l>
						<l n="26" num="5.2"><w n="26.1">Mais</w> <w n="26.2">craignez</w> <w n="26.3">les</w> <w n="26.4">flatteurs</w>, <w n="26.5">et</w> <w n="26.6">ne</w> <w n="26.7">mendiez</w> <w n="26.8">pas</w></l>
						<l n="27" num="5.3"><space unit="char" quantity="8"></space><w n="27.1">Des</w> <w n="27.2">oreilles</w> <w n="27.3">pour</w> <w n="27.4">vous</w> <w n="27.5">entendre</w>.</l>
						<l n="28" num="5.4"><w n="28.1">Si</w> <w n="28.2">vous</w> <w n="28.3">le</w> <w n="28.4">méritez</w> <w n="28.5">on</w> <w n="28.6">viendra</w> <w n="28.7">jusqu</w>’<w n="28.8">à</w> <w n="28.9">vous</w> :</l>
						<l n="29" num="5.5"><w n="29.1">Le</w> <w n="29.2">cœur</w> <w n="29.3">a</w> <w n="29.4">des</w> <w n="29.5">parfums</w> <w n="29.6">aussi</w> <w n="29.7">subtils</w> <w n="29.8">que</w> <w n="29.9">doux</w>,</l>
						<l n="30" num="5.6"><space unit="char" quantity="8"></space><w n="30.1">Qui</w> <w n="30.2">s</w>’<w n="30.3">exhalent</w> <w n="30.4">sans</w> <w n="30.5">se</w> <w n="30.6">répandre</w>.</l>
					</lg>
				</div></body></text></TEI>