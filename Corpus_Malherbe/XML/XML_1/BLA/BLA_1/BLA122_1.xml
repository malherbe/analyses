<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="sub_2">TOME PREMIER</title>
				<title type="medium">Édition électronique</title>
				<author key="BLA">
					<name>
						<forename>Prosper</forename>
						<surname>BLANCHEMAIN</surname>
					</name>
					<date from="1816" to="1879">1816-1879</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>7196 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">BLA_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME PREMIER</title>
						<author>Prosper Blanchemain</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/pomesetposie01blan</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
								<title>TOME PREMIER</title>
								<author>Prosper Blanchemain</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE ANCIENNE ET MODERNE - ÉDOUARD ROUVEYRE</publisher>
									<date when="1880">1880</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME I</title>
						<title>POÈMES ET POÉSIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME V</title>
						<title>SONNETS ET FANTAISIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1845" to="1858">1845-1858</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">IDÉAL</head><div type="poem" key="BLA122">
					<head type="main">RÉSIGNATION</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Je</w> <w n="1.2">voudrais</w> <w n="1.3">te</w> <w n="1.4">savoir</w> <w n="1.5">heureuse</w> <w n="1.6">et</w> <w n="1.7">couronnée</w></l>
						<l n="2" num="1.2"><w n="2.1">Du</w> <w n="2.2">bonheur</w> <w n="2.3">idéal</w> <w n="2.4">que</w> <w n="2.5">j</w>’<w n="2.6">ai</w> <w n="2.7">rêvé</w> <w n="2.8">pour</w> <w n="2.9">toi</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Me</w> <w n="3.2">fallût</w>-<w n="3.3">il</w> <w n="3.4">te</w> <w n="3.5">perdre</w> <w n="3.6">et</w> <w n="3.7">te</w> <w n="3.8">voir</w> <w n="3.9">entraînée</w></l>
						<l n="4" num="1.4"><space unit="char" quantity="18"></space><w n="4.1">Loin</w> <w n="4.2">de</w> <w n="4.3">moi</w> !</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">A</w> <w n="5.2">toi</w> <w n="5.3">l</w>’<w n="5.4">éclat</w>, <w n="5.5">la</w> <w n="5.6">joie</w> ; <w n="5.7">à</w> <w n="5.8">moi</w> <w n="5.9">le</w> <w n="5.10">deuil</w> <w n="5.11">et</w> <w n="5.12">l</w>’<w n="5.13">ombre</w> !</l>
						<l n="6" num="2.2"><w n="6.1">Mais</w> <w n="6.2">au</w> <w n="6.3">sein</w> <w n="6.4">des</w> <w n="6.5">plaisirs</w> <w n="6.6">naissants</w> <w n="6.7">autour</w> <w n="6.8">de</w> <w n="6.9">toi</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Si</w> <w n="7.2">parfois</w> <w n="7.3">dans</w> <w n="7.4">ton</w> <w n="7.5">cœur</w> <w n="7.6">il</w> <w n="7.7">restait</w> <w n="7.8">un</w> <w n="7.9">coin</w> <w n="7.10">sombre</w>,</l>
						<l n="8" num="2.4"><space unit="char" quantity="18"></space><w n="8.1">Pense</w> <w n="8.2">à</w> <w n="8.3">moi</w> !</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Si</w> <w n="9.2">jamais</w> <w n="9.3">ta</w> <w n="9.4">gaîté</w> <w n="9.5">fuyait</w> <w n="9.6">à</w> <w n="9.7">tire</w>-<w n="9.8">d</w>’<w n="9.9">aile</w>,</l>
						<l n="10" num="3.2"><w n="10.1">Si</w> <w n="10.2">tes</w> <w n="10.3">amis</w> <w n="10.4">trompeurs</w> <w n="10.5">se</w> <w n="10.6">détournaient</w> <w n="10.7">de</w> <w n="10.8">toi</w>,</l>
						<l n="11" num="3.3"><w n="11.1">Songe</w> <w n="11.2">qu</w>’<w n="11.3">il</w> <w n="11.4">en</w> <w n="11.5">est</w> <w n="11.6">un</w> <w n="11.7">dont</w> <w n="11.8">le</w> <w n="11.9">cœur</w> <w n="11.10">est</w> <w n="11.11">fidèle</w> :</l>
						<l n="12" num="3.4"><space unit="char" quantity="18"></space><w n="12.1">Aime</w>-<w n="12.2">moi</w> !</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Si</w> <w n="13.2">le</w> <w n="13.3">sort</w> <w n="13.4">te</w> <w n="13.5">trahit</w> : — <w n="13.6">le</w> <w n="13.7">bonheur</w>, <w n="13.8">comme</w> <w n="13.9">une</w> <w n="13.10">onde</w>,</l>
						<l n="14" num="4.2"><w n="14.1">Peut</w> <w n="14.2">fuir</w>, <w n="14.3">et</w> <w n="14.4">dans</w> <w n="14.5">un</w> <w n="14.6">jour</w> <w n="14.7">tout</w> <w n="14.8">briser</w> <w n="14.9">devant</w> <w n="14.10">toi</w> ! —</l>
						<l n="15" num="4.3"><w n="15.1">Si</w>, <w n="15.2">pauvre</w> <w n="15.3">et</w> <w n="15.4">sans</w> <w n="15.5">appui</w>, <w n="15.6">tu</w> <w n="15.7">restes</w> <w n="15.8">seule</w> <w n="15.9">au</w> <w n="15.10">monde</w>,</l>
						<l n="16" num="4.4"><space unit="char" quantity="18"></space><w n="16.1">Viens</w> <w n="16.2">à</w> <w n="16.3">moi</w> !</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Mais</w> <w n="17.2">non</w> ! <w n="17.3">que</w> <w n="17.4">l</w>’<w n="17.5">Éternel</w> <w n="17.6">détourne</w> <w n="17.7">les</w> <w n="17.8">orages</w>,</l>
						<l n="18" num="5.2"><w n="18.1">Que</w> <w n="18.2">la</w> <w n="18.3">sérénité</w> <w n="18.4">rayonne</w> <w n="18.5">autour</w> <w n="18.6">de</w> <w n="18.7">toi</w> !</l>
						<l n="19" num="5.3"><w n="19.1">Que</w> <w n="19.2">l</w>’<w n="19.3">oubli</w>, <w n="19.4">la</w> <w n="19.5">douleur</w> <w n="19.6">et</w> <w n="19.7">les</w> <w n="19.8">sombres</w> <w n="19.9">nuages</w></l>
						<l n="20" num="5.4"><space unit="char" quantity="18"></space><w n="20.1">Soient</w> <w n="20.2">pour</w> <w n="20.3">moi</w> !</l>
					</lg>
				</div></body></text></TEI>