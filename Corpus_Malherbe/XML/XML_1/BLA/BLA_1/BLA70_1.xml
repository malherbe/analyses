<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="sub_2">TOME PREMIER</title>
				<title type="medium">Édition électronique</title>
				<author key="BLA">
					<name>
						<forename>Prosper</forename>
						<surname>BLANCHEMAIN</surname>
					</name>
					<date from="1816" to="1879">1816-1879</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>7196 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">BLA_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME PREMIER</title>
						<author>Prosper Blanchemain</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/pomesetposie01blan</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
								<title>TOME PREMIER</title>
								<author>Prosper Blanchemain</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE ANCIENNE ET MODERNE - ÉDOUARD ROUVEYRE</publisher>
									<date when="1880">1880</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME I</title>
						<title>POÈMES ET POÉSIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME V</title>
						<title>SONNETS ET FANTAISIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1845" to="1858">1845-1858</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">IDÉAL</head><div type="poem" key="BLA70">
					<head type="main">LA PREMIÈRE VIOLETTE</head>
					<opener>
						<salute>A Alexandre Piedagnel.</salute>
					</opener>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Oh</w> ! <w n="1.2">comme</w> <w n="1.3">il</w> <w n="1.4">rassérène</w> <w n="1.5">l</w>’<w n="1.6">âme</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Ce</w> <w n="2.2">nouveau</w> <w n="2.3">soleil</w> <w n="2.4">de</w> <w n="2.5">printemps</w> !</l>
						<l n="3" num="1.3"><w n="3.1">Comme</w> <w n="3.2">il</w> <w n="3.3">fait</w> <w n="3.4">renaître</w> <w n="3.5">A</w> <w n="3.6">sa</w> <w n="3.7">flamme</w></l>
						<l n="4" num="1.4"><w n="4.1">Les</w> <w n="4.2">fleurs</w> <w n="4.3">et</w> <w n="4.4">les</w> <w n="4.5">oiseaux</w> <w n="4.6">chantants</w> !</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">La</w> <w n="5.2">violette</w> <w n="5.3">sous</w> <w n="5.4">la</w> <w n="5.5">haie</w>,</l>
						<l n="6" num="2.2"><w n="6.1">S</w>’<w n="6.2">est</w> <w n="6.3">ouverte</w> <w n="6.4">au</w> <w n="6.5">pied</w> <w n="6.6">des</w> <w n="6.7">ormeaux</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Où</w> <w n="7.2">le</w> <w n="7.3">lézard</w> <w n="7.4">vert</w>, <w n="7.5">qui</w> <w n="7.6">s</w>’<w n="7.7">effraie</w>,</l>
						<l n="8" num="2.4"><w n="8.1">Glisse</w> <w n="8.2">et</w> <w n="8.3">fait</w> <w n="8.4">trembler</w> <w n="8.5">les</w> <w n="8.6">rameaux</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Première</w> <w n="9.2">fleur</w>, <w n="9.3">nouvelle</w> <w n="9.4">éclose</w>,</l>
						<l n="10" num="3.2"><w n="10.1">Qu</w>’<w n="10.2">on</w> <w n="10.3">a</w> <w n="10.4">de</w> <w n="10.5">joie</w> <w n="10.6">à</w> <w n="10.7">te</w> <w n="10.8">cueillir</w> ;</l>
						<l n="11" num="3.3"><w n="11.1">Goutte</w> <w n="11.2">de</w> <w n="11.3">parfum</w> <w n="11.4">pur</w>, <w n="11.5">enclose</w></l>
						<l n="12" num="3.4"><w n="12.1">Dans</w> <w n="12.2">une</w> <w n="12.3">coupe</w> <w n="12.4">de</w> <w n="12.5">saphir</w> !</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Avec</w> <w n="13.2">le</w> <w n="13.3">cœur</w> <w n="13.4">on</w> <w n="13.5">te</w> <w n="13.6">respire</w> ;</l>
						<l n="14" num="4.2"><w n="14.1">A</w> <w n="14.2">tant</w> <w n="14.3">d</w>’<w n="14.4">espoir</w> <w n="14.5">tu</w> <w n="14.6">fais</w> <w n="14.7">penser</w>,</l>
						<l n="15" num="4.3"><w n="15.1">Violette</w>, <w n="15.2">premier</w> <w n="15.3">sourire</w></l>
						<l n="16" num="4.4"><w n="16.1">Du</w> <w n="16.2">printemps</w> <w n="16.3">qui</w> <w n="16.4">va</w> <w n="16.5">commencer</w> !</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Combien</w> <w n="17.2">caches</w>-<w n="17.3">tu</w> <w n="17.4">de</w> <w n="17.5">promesses</w></l>
						<l n="18" num="5.2"><w n="18.1">Dans</w> <w n="18.2">tes</w> <w n="18.3">plis</w> <w n="18.4">frêles</w> <w n="18.5">et</w> <w n="18.6">soyeux</w> ?</l>
						<l n="19" num="5.3"><w n="19.1">Combien</w> <w n="19.2">exhales</w>-<w n="19.3">tu</w> <w n="19.4">d</w>’<w n="19.5">ivresses</w></l>
						<l n="20" num="5.4"><w n="20.1">De</w> <w n="20.2">ton</w> <w n="20.3">urne</w> <w n="20.4">couleur</w> <w n="20.5">des</w> <w n="20.6">cieux</w> ?</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Verse</w> <w n="21.2">en</w> <w n="21.3">moi</w> <w n="21.4">ta</w> <w n="21.5">douceur</w> <w n="21.6">secrète</w> ;</l>
						<l n="22" num="6.2"><w n="22.1">Viens</w> <w n="22.2">sur</w> <w n="22.3">mon</w> <w n="22.4">cœur</w>, <w n="22.5">frêle</w> <w n="22.6">trésor</w>.</l>
						<l n="23" num="6.3"><w n="23.1">Ne</w> <w n="23.2">sens</w>-<w n="23.3">tu</w> <w n="23.4">pas</w>, <w n="23.5">ô</w> <w n="23.6">violette</w> !</l>
						<l n="24" num="6.4"><w n="24.1">Qu</w>’<w n="24.2">il</w> <w n="24.3">palpite</w> <w n="24.4">et</w> <w n="24.5">qu</w>’<w n="24.6">il</w> <w n="24.7">aime</w> <w n="24.8">encor</w> ?</l>
					</lg>
				</div></body></text></TEI>