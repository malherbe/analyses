<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="sub_2">TOME PREMIER</title>
				<title type="medium">Édition électronique</title>
				<author key="BLA">
					<name>
						<forename>Prosper</forename>
						<surname>BLANCHEMAIN</surname>
					</name>
					<date from="1816" to="1879">1816-1879</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>7196 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">BLA_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME PREMIER</title>
						<author>Prosper Blanchemain</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/pomesetposie01blan</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
								<title>TOME PREMIER</title>
								<author>Prosper Blanchemain</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE ANCIENNE ET MODERNE - ÉDOUARD ROUVEYRE</publisher>
									<date when="1880">1880</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME I</title>
						<title>POÈMES ET POÉSIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME V</title>
						<title>SONNETS ET FANTAISIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1845" to="1858">1845-1858</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">POÈMES ET POÉSIES</head><div type="poem" key="BLA28">
					<head type="main">L’INCENDIE EN MER</head>
					<opener>
						<salute>A S. A. R. le prince de Joinville.</salute>
					</opener>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Vogue</w>, <w n="1.2">navire</w> <w n="1.3">aux</w> <w n="1.4">larges</w> <w n="1.5">voiles</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Entre</w> <w n="2.2">le</w> <w n="2.3">ciel</w> <w n="2.4">brillant</w> <w n="2.5">d</w>’<w n="2.6">étoiles</w></l>
						<l n="3" num="1.3"><w n="3.1">Et</w> <w n="3.2">la</w> <w n="3.3">mer</w>, <w n="3.4">abîme</w> <w n="3.5">béant</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Sous</w> <w n="4.2">ta</w> <w n="4.3">mâture</w> <w n="4.4">à</w> <w n="4.5">triple</w> <w n="4.6">tète</w>,</l>
						<l n="5" num="1.5"><w n="5.1">Pourrais</w>-<w n="5.2">tu</w> <w n="5.3">craindre</w> <w n="5.4">la</w> <w n="5.5">tempête</w>,</l>
						<l n="6" num="1.6"><w n="6.1">Toi</w>, <w n="6.2">monarque</w> <w n="6.3">de</w> <w n="6.4">l</w>’<w n="6.5">Océan</w> ?</l>
					</lg>
					<lg n="2">
						<l n="7" num="2.1"><w n="7.1">Vogue</w> <w n="7.2">à</w> <w n="7.3">travers</w> <w n="7.4">la</w> <w n="7.5">nuit</w> <w n="7.6">limpide</w> !</l>
						<l n="8" num="2.2"><w n="8.1">Mais</w> <w n="8.2">que</w> <w n="8.3">vois</w>-<w n="8.4">je</w> ? <w n="8.5">Un</w> <w n="8.6">éclair</w> <w n="8.7">rapide</w></l>
						<l n="9" num="2.3"><w n="9.1">S</w>’<w n="9.2">est</w> <w n="9.3">élancé</w> <w n="9.4">de</w> <w n="9.5">ton</w> <w n="9.6">flanc</w> <w n="9.7">noir</w> ;</l>
						<l n="10" num="2.4"><w n="10.1">Un</w> <w n="10.2">bruit</w> <w n="10.3">sourd</w> <w n="10.4">gronde</w> <w n="10.5">en</w> <w n="10.6">ta</w> <w n="10.7">carène</w>,</l>
						<l n="11" num="2.5"><w n="11.1">Et</w> <w n="11.2">la</w> <w n="11.3">voix</w> <w n="11.4">de</w> <w n="11.5">ton</w> <w n="11.6">capitaine</w></l>
						<l n="12" num="2.6"><w n="12.1">Jette</w> <w n="12.2">un</w> <w n="12.3">long</w> <w n="12.4">cri</w> <w n="12.5">de</w> <w n="12.6">désespoir</w>.</l>
					</lg>
					<lg n="3">
						<l n="13" num="3.1">« <w n="13.1">De</w> <w n="13.2">l</w>’<w n="13.3">eau</w> ! <w n="13.4">de</w> <w n="13.5">l</w>’<w n="13.6">eau</w> ! <w n="13.7">c</w>’<w n="13.8">est</w> <w n="13.9">l</w>’<w n="13.10">incendie</w> !…</l>
						<l n="14" num="3.2"><w n="14.1">Réveillez</w> <w n="14.2">la</w> <w n="14.3">foule</w> <w n="14.4">engourdie</w></l>
						<l n="15" num="3.3"><w n="15.1">Des</w> <w n="15.2">matelots</w> <w n="15.3">dans</w> <w n="15.4">l</w>’<w n="15.5">entre</w>-<w n="15.6">pont</w> ! »</l>
						<l n="16" num="3.4"><w n="16.1">Partout</w> <w n="16.2">le</w> <w n="16.3">cri</w> <w n="16.4">fatal</w> <w n="16.5">résonne</w>.</l>
						<l n="17" num="3.5"><w n="17.1">Le</w> <w n="17.2">feu</w> <w n="17.3">que</w> <w n="17.4">la</w> <w n="17.5">cale</w> <w n="17.6">emprisonne</w>,</l>
						<l n="18" num="3.6"><w n="18.1">Écho</w> <w n="18.2">sinistre</w>, <w n="18.3">lui</w> <w n="18.4">répond</w>.</l>
					</lg>
					<lg n="4">
						<l n="19" num="4.1"><w n="19.1">Partout</w> <w n="19.2">on</w> <w n="19.3">s</w>’<w n="19.4">empresse</w>, <w n="19.5">on</w> <w n="19.6">s</w>’<w n="19.7">élance</w> ;</l>
						<l n="20" num="4.2"><w n="20.1">De</w> <w n="20.2">la</w> <w n="20.3">pompe</w> <w n="20.4">qui</w> <w n="20.5">se</w> <w n="20.6">balance</w></l>
						<l n="21" num="4.3"><w n="21.1">L</w>’<w n="21.2">eau</w> <w n="21.3">jaillit</w> <w n="21.4">et</w> <w n="21.5">coule</w> <w n="21.6">à</w> <w n="21.7">longs</w> <w n="21.8">flots</w> ;</l>
						<l n="22" num="4.4"><w n="22.1">Mais</w> <w n="22.2">la</w> <w n="22.3">flamme</w> <w n="22.4">grandit</w> <w n="22.5">plus</w> <w n="22.6">vite</w>,</l>
						<l n="23" num="4.5"><w n="23.1">Et</w> <w n="23.2">déjà</w> <w n="23.3">le</w> <w n="23.4">pont</w> <w n="23.5">qui</w> <w n="23.6">crépite</w></l>
						<l n="24" num="4.6"><w n="24.1">Brûle</w> <w n="24.2">les</w> <w n="24.3">pieds</w> <w n="24.4">des</w> <w n="24.5">matelots</w>.</l>
					</lg>
					<lg n="5">
						<l n="25" num="5.1"><w n="25.1">Il</w> <w n="25.2">s</w>’<w n="25.3">élève</w>, <w n="25.4">l</w>’<w n="25.5">hôte</w> <w n="25.6">implacable</w>,</l>
						<l n="26" num="5.2"><w n="26.1">Dans</w> <w n="26.2">les</w> <w n="26.3">mâts</w>, <w n="26.4">sur</w> <w n="26.5">le</w> <w n="26.6">moindre</w> <w n="26.7">câble</w> ;</l>
						<l n="27" num="5.3"><w n="27.1">Et</w>, <w n="27.2">comme</w> <w n="27.3">un</w> <w n="27.4">linceul</w> <w n="27.5">agité</w>,</l>
						<l n="28" num="5.4"><w n="28.1">Se</w> <w n="28.2">déroule</w> <w n="28.3">le</w> <w n="28.4">flot</w> <w n="28.5">avide</w> :</l>
						<l n="29" num="5.5"><w n="29.1">Et</w> <w n="29.2">la</w> <w n="29.3">lueur</w> <w n="29.4">s</w>’<w n="29.5">étend</w>, <w n="29.6">livide</w>,</l>
						<l n="30" num="5.6"><w n="30.1">Sur</w> <w n="30.2">l</w>’<w n="30.3">effrayante</w> <w n="30.4">immensité</w>.</l>
					</lg>
					<lg n="6">
						<l n="31" num="6.1"><w n="31.1">Plus</w> <w n="31.2">d</w>’<w n="31.3">espoir</w> ! <w n="31.4">les</w> <w n="31.5">marins</w> <w n="31.6">s</w>’<w n="31.7">embrassent</w></l>
						<l n="32" num="6.2"><w n="32.1">Les</w> <w n="32.2">bras</w> <w n="32.3">douloureux</w> <w n="32.4">s</w>’<w n="32.5">entrelacent</w></l>
						<l n="33" num="6.3"><w n="33.1">Dans</w> <w n="33.2">un</w> <w n="33.3">long</w> <w n="33.4">et</w> <w n="33.5">funèbre</w> <w n="33.6">adieu</w>.</l>
						<l n="34" num="6.4"><w n="34.1">Les</w> <w n="34.2">pleurs</w> <w n="34.3">confus</w> <w n="34.4">et</w> <w n="34.5">la</w> <w n="34.6">prière</w></l>
						<l n="35" num="6.5"><w n="35.1">Montent</w>, <w n="35.2">espérance</w> <w n="35.3">dernière</w>,</l>
						<l n="36" num="6.6"><w n="36.1">Jusqu</w>’<w n="36.2">au</w> <w n="36.3">trône</w> <w n="36.4">éternel</w> <w n="36.5">de</w> <w n="36.6">Dieu</w>.</l>
					</lg>
					<lg n="7">
						<l n="37" num="7.1"><w n="37.1">Nous</w> <w n="37.2">entend</w>-<w n="37.3">il</w>, <w n="37.4">le</w> <w n="37.5">divin</w> <w n="37.6">Maître</w> ?</l>
						<l n="38" num="7.2"><w n="38.1">Oui</w> ! <w n="38.2">vers</w> <w n="38.3">lui</w> <w n="38.4">notre</w> <w n="38.5">voix</w> <w n="38.6">pénètre</w>.</l>
						<l n="39" num="7.3"><w n="39.1">La</w> <w n="39.2">mer</w> <w n="39.3">envahit</w> <w n="39.4">notre</w> <w n="39.5">bord</w> ;</l>
						<l n="40" num="7.4"><w n="40.1">Le</w> <w n="40.2">feu</w> <w n="40.3">redouble</w> <w n="40.4">sa</w> <w n="40.5">furie</w> ;</l>
						<l n="41" num="7.5"><w n="41.1">Mais</w> <w n="41.2">soudain</w> <w n="41.3">une</w> <w n="41.4">voix</w> <w n="41.5">s</w>’<w n="41.6">écrie</w> :</l>
						<l n="42" num="7.6">« <w n="42.1">Une</w> <w n="42.2">voile</w> ! <w n="42.3">une</w> <w n="42.4">voile</w> ! <w n="42.5">au</w> <w n="42.6">nord</w> ! »</l>
					</lg>
					<lg n="8">
						<l n="43" num="8.1"><w n="43.1">Salut</w> <w n="43.2">à</w> <w n="43.3">toi</w>, <w n="43.4">brick</w> <w n="43.5">intrépide</w> !</l>
						<l n="44" num="8.2"><w n="44.1">C</w>’<w n="44.2">est</w> <w n="44.3">un</w> <w n="44.4">jeune</w> <w n="44.5">homme</w> <w n="44.6">qui</w> <w n="44.7">te</w> <w n="44.8">guide</w>,</l>
						<l n="45" num="8.3"><w n="45.1">C</w>’<w n="45.2">est</w> <w n="45.3">un</w> <w n="45.4">jeune</w> <w n="45.5">homme</w> <w n="45.6">aux</w> <w n="45.7">noirs</w> <w n="45.8">cheveux</w>.</l>
						<l n="46" num="8.4"><w n="46.1">Il</w> <w n="46.2">te</w> <w n="46.3">conduit</w> <w n="46.4">d</w>’<w n="46.5">une</w> <w n="46.6">main</w> <w n="46.7">forte</w> ;</l>
						<l n="47" num="8.5"><w n="47.1">Le</w> <w n="47.2">vent</w> <w n="47.3">rapide</w> <w n="47.4">qui</w> <w n="47.5">l</w>’<w n="47.6">apporte</w></l>
						<l n="48" num="8.6"><w n="48.1">Est</w> <w n="48.2">moins</w> <w n="48.3">rapide</w> <w n="48.4">que</w> <w n="48.5">ses</w> <w n="48.6">vœux</w> !</l>
					</lg>
					<lg n="9">
						<l n="49" num="9.1"><w n="49.1">Il</w> <w n="49.2">vient</w>, <w n="49.3">béni</w> <w n="49.4">par</w> <w n="49.5">deux</w> <w n="49.6">cents</w> <w n="49.7">âmes</w> :</l>
						<l n="50" num="9.2"><w n="50.1">Sur</w> <w n="50.2">le</w> <w n="50.3">vaisseau</w> <w n="50.4">rongé</w> <w n="50.5">de</w> <w n="50.6">flammes</w>,</l>
						<l n="51" num="9.3"><w n="51.1">Le</w> <w n="51.2">premier</w> <w n="51.3">élancé</w>, <w n="51.4">c</w>’<w n="51.5">est</w> <w n="51.6">lui</w>.</l>
						<l n="52" num="9.4"><w n="52.1">A</w> <w n="52.2">ceux</w> <w n="52.3">que</w> <w n="52.4">la</w> <w n="52.5">force</w> <w n="52.6">abandonne</w>,</l>
						<l n="53" num="9.5"><w n="53.1">Aux</w> <w n="53.2">blessés</w>, <w n="53.3">aux</w> <w n="53.4">mourants</w>, <w n="53.5">il</w> <w n="53.6">donne</w></l>
						<l n="54" num="9.6"><w n="54.1">L</w>’<w n="54.2">espoir</w>, <w n="54.3">le</w> <w n="54.4">courage</w> <w n="54.5">et</w> <w n="54.6">l</w>’<w n="54.7">appui</w>.</l>
					</lg>
					<lg n="10">
						<l n="55" num="10.1"><w n="55.1">L</w>’<w n="55.2">œil</w> <w n="55.3">éclatant</w>, <w n="55.4">l</w>’<w n="55.5">âme</w> <w n="55.6">hardie</w>,</l>
						<l n="56" num="10.2"><w n="56.1">Il</w> <w n="56.2">est</w> <w n="56.3">debout</w> <w n="56.4">dans</w> <w n="56.5">l</w>’<w n="56.6">incendie</w></l>
						<l n="57" num="10.3"><w n="57.1">Tant</w> <w n="57.2">qu</w>’<w n="57.3">il</w> <w n="57.4">reste</w> <w n="57.5">un</w> <w n="57.6">être</w> <w n="57.7">en</w> <w n="57.8">danger</w> ;</l>
						<l n="58" num="10.4"><w n="58.1">Puis</w>, <w n="58.2">le</w> <w n="58.3">dernier</w>, <w n="58.4">pensif</w> <w n="58.5">et</w> <w n="58.6">sombre</w>,</l>
						<l n="59" num="10.5"><w n="59.1">Il</w> <w n="59.2">quitte</w> <w n="59.3">le</w> <w n="59.4">vaisseau</w> <w n="59.5">qui</w> <w n="59.6">sombre</w></l>
						<l n="60" num="10.6"><w n="60.1">Et</w> <w n="60.2">que</w> <w n="60.3">la</w> <w n="60.4">mer</w> <w n="60.5">va</w> <w n="60.6">submerger</w>.</l>
					</lg>
					<lg n="11">
						<l n="61" num="11.1"><w n="61.1">Alors</w> <w n="61.2">sa</w> <w n="61.3">voix</w> <w n="61.4">plaint</w> <w n="61.5">et</w> <w n="61.6">console</w> ;</l>
						<l n="62" num="11.2"><w n="62.1">Il</w> <w n="62.2">a</w> <w n="62.3">pour</w> <w n="62.4">tous</w> <w n="62.5">une</w> <w n="62.6">parole</w>,</l>
						<l n="63" num="11.3"><w n="63.1">Pour</w> <w n="63.2">tous</w> <w n="63.3">un</w> <w n="63.4">serrement</w> <w n="63.5">de</w> <w n="63.6">main</w> ;</l>
						<l n="64" num="11.4"><w n="64.1">Et</w> <w n="64.2">puis</w>, <w n="64.3">retournant</w> <w n="64.4">en</w> <w n="64.5">arrière</w>,</l>
						<l n="65" num="11.5"><w n="65.1">Sur</w> <w n="65.2">une</w> <w n="65.3">plage</w> <w n="65.4">hospitalière</w></l>
						<l n="66" num="11.6"><w n="66.1">Les</w> <w n="66.2">pose</w>… <w n="66.3">et</w> <w n="66.4">reprend</w> <w n="66.5">son</w> <w n="66.6">chemin</w>.</l>
					</lg>
					<lg n="12">
						<l n="67" num="12.1">— « <w n="67.1">Avant</w> <w n="67.2">de</w> <w n="67.3">nous</w> <w n="67.4">fuir</w>, <w n="67.5">ô</w> <w n="67.6">jeune</w> <w n="67.7">homme</w>,</l>
						<l n="68" num="12.2"><w n="68.1">Dis</w>-<w n="68.2">nous</w> <w n="68.3">de</w> <w n="68.4">quel</w> <w n="68.5">nom</w> <w n="68.6">l</w>’<w n="68.7">on</w> <w n="68.8">te</w> <w n="68.9">nomme</w> ;</l>
						<l n="69" num="12.3"><w n="69.1">Et</w> <w n="69.2">les</w> <w n="69.3">matelots</w> <w n="69.4">affligés</w></l>
						<l n="70" num="12.4"><w n="70.1">Imploreront</w> <w n="70.2">le</w> <w n="70.3">Dieu</w> <w n="70.4">suprême</w>,</l>
						<l n="71" num="12.5"><w n="71.1">Afin</w> <w n="71.2">qu</w>’<w n="71.3">il</w> <w n="71.4">te</w> <w n="71.5">protège</w> <w n="71.6">et</w> <w n="71.7">t</w>’<w n="71.8">aime</w>,</l>
						<l n="72" num="12.6"><w n="72.1">Toi</w> <w n="72.2">qu</w>’<w n="72.3">il</w> <w n="72.4">envoie</w> <w n="72.5">aux</w> <w n="72.6">naufragés</w>.</l>
					</lg>
					<lg n="13">
						<l n="73" num="13.1">« <w n="73.1">Combien</w> <w n="73.2">aux</w> <w n="73.3">bords</w> <w n="73.4">qui</w> <w n="73.5">t</w>’<w n="73.6">ont</w> <w n="73.7">vu</w> <w n="73.8">naître</w></l>
						<l n="74" num="13.2"><w n="74.1">On</w> <w n="74.2">doit</w> <w n="74.3">aimer</w> <w n="74.4">à</w> <w n="74.5">te</w> <w n="74.6">connaître</w> !</l>
						<l n="75" num="13.3"><w n="75.1">Béni</w> <w n="75.2">pour</w> <w n="75.3">le</w> <w n="75.4">bien</w> <w n="75.5">que</w> <w n="75.6">tu</w> <w n="75.7">fis</w>,</l>
						<l n="76" num="13.4"><w n="76.1">Tu</w> <w n="76.2">dois</w> <w n="76.3">n</w>’<w n="76.4">avoir</w> <w n="76.5">pas</w> <w n="76.6">d</w>’<w n="76.7">heure</w> <w n="76.8">amère</w>.</l>
						<l n="77" num="13.5"><w n="77.1">Qu</w>’<w n="77.2">heureuse</w> <w n="77.3">doit</w> <w n="77.4">être</w> <w n="77.5">ta</w> <w n="77.6">mère</w></l>
						<l n="78" num="13.6"><w n="78.1">D</w>’<w n="78.2">avoir</w> <w n="78.3">mis</w> <w n="78.4">au</w> <w n="78.5">monde</w> <w n="78.6">un</w> <w n="78.7">tel</w> <w n="78.8">fils</w> !</l>
					</lg>
					<lg n="14">
						<l n="79" num="14.1">« <w n="79.1">Ceux</w> <w n="79.2">que</w> <w n="79.3">tu</w> <w n="79.4">sauvas</w> <w n="79.5">dans</w> <w n="79.6">ta</w> <w n="79.7">route</w></l>
						<l n="80" num="14.2"><w n="80.1">Ne</w> <w n="80.2">sont</w> <w n="80.3">pas</w> <w n="80.4">les</w> <w n="80.5">premiers</w> <w n="80.6">sans</w> <w n="80.7">doute</w></l>
						<l n="81" num="14.3"><w n="81.1">Que</w> <w n="81.2">tu</w> <w n="81.3">rends</w> <w n="81.4">à</w> <w n="81.5">leur</w> <w n="81.6">cher</w> <w n="81.7">pays</w>.</l>
						<l n="82" num="14.4"><w n="82.1">Est</w>-<w n="82.2">il</w> <w n="82.3">un</w> <w n="82.4">plus</w> <w n="82.5">cruel</w> <w n="82.6">partage</w></l>
						<l n="83" num="14.5"><w n="83.1">Que</w> <w n="83.2">de</w> <w n="83.3">mourir</w> <w n="83.4">loin</w> <w n="83.5">du</w> <w n="83.6">rivage</w></l>
						<l n="84" num="14.6"><w n="84.1">Où</w> <w n="84.2">sont</w> <w n="84.3">morts</w> <w n="84.4">ceux</w> <w n="84.5">qu</w>’<w n="84.6">on</w> <w n="84.7">a</w> <w n="84.8">chéris</w> ? »</l>
					</lg>
					<lg n="15">
						<l n="85" num="15.1"><w n="85.1">Le</w> <w n="85.2">jeune</w> <w n="85.3">homme</w>, <w n="85.4">sans</w> <w n="85.5">leur</w> <w n="85.6">rien</w> <w n="85.7">dire</w>,</l>
						<l n="86" num="15.2"><w n="86.1">Tristement</w> <w n="86.2">se</w> <w n="86.3">prit</w> <w n="86.4">à</w> <w n="86.5">sourire</w>,</l>
						<l n="87" num="15.3"><w n="87.1">Puis</w> <w n="87.2">s</w>’<w n="87.3">éloigna</w> <w n="87.4">comme</w> <w n="87.5">à</w> <w n="87.6">regret</w>.</l>
						<l n="88" num="15.4"><w n="88.1">Quand</w> <w n="88.2">son</w> <w n="88.3">vaisseau</w> <w n="88.4">tourna</w> <w n="88.5">la</w> <w n="88.6">proue</w>,</l>
						<l n="89" num="15.5"><w n="89.1">Des</w> <w n="89.2">pleurs</w>, <w n="89.3">dit</w>-<w n="89.4">on</w>, <w n="89.5">mouillaient</w> <w n="89.6">sa</w> <w n="89.7">joue</w> ;</l>
						<l n="90" num="15.6"><w n="90.1">Mais</w> <w n="90.2">il</w> <w n="90.3">emporta</w> <w n="90.4">son</w> <w n="90.5">secret</w>.</l>
					</lg>
					<closer>
						<dateline>
							<date when="1848">1848.</date>
						</dateline>
					</closer>
				</div></body></text></TEI>