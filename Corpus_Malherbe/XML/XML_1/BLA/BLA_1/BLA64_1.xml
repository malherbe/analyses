<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="sub_2">TOME PREMIER</title>
				<title type="medium">Édition électronique</title>
				<author key="BLA">
					<name>
						<forename>Prosper</forename>
						<surname>BLANCHEMAIN</surname>
					</name>
					<date from="1816" to="1879">1816-1879</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>7196 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">BLA_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME PREMIER</title>
						<author>Prosper Blanchemain</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/pomesetposie01blan</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
								<title>TOME PREMIER</title>
								<author>Prosper Blanchemain</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE ANCIENNE ET MODERNE - ÉDOUARD ROUVEYRE</publisher>
									<date when="1880">1880</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME I</title>
						<title>POÈMES ET POÉSIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME V</title>
						<title>SONNETS ET FANTAISIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1845" to="1858">1845-1858</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">IDÉAL</head><div type="poem" key="BLA64">
					<head type="main">LE NOM DE MA MÈRE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Tu</w> <w n="1.2">portes</w> <w n="1.3">le</w> <w n="1.4">nom</w> <w n="1.5">de</w> <w n="1.6">ma</w> <w n="1.7">mère</w>,</l>
						<l n="2" num="1.2"><w n="2.1">De</w> <w n="2.2">ma</w> <w n="2.3">mère</w> <w n="2.4">que</w> <w n="2.5">j</w>’<w n="2.6">aimais</w> <w n="2.7">tant</w> ;</l>
						<l n="3" num="1.3"><w n="3.1">Doux</w> <w n="3.2">nom</w> <w n="3.3">plein</w> <w n="3.4">d</w>’<w n="3.5">une</w> <w n="3.6">ivresse</w> <w n="3.7">amère</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Mon</w> <w n="4.2">cœur</w> <w n="4.3">palpite</w> <w n="4.4">en</w> <w n="4.5">l</w>’<w n="4.6">écoutant</w> !</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Hélas</w> ! <w n="5.2">il</w> <w n="5.3">dort</w> <w n="5.4">sous</w> <w n="5.5">un</w> <w n="5.6">blanc</w> <w n="5.7">voile</w> ;</l>
						<l n="6" num="2.2"><w n="6.1">Il</w> <w n="6.2">s</w>’<w n="6.3">est</w> <w n="6.4">fermé</w>, <w n="6.5">l</w>’<w n="6.6">œil</w> <w n="6.7">maternel</w></l>
						<l n="7" num="2.3"><w n="7.1">Qui</w> <w n="7.2">me</w> <w n="7.3">guidait</w>, <w n="7.4">limpide</w> <w n="7.5">étoile</w>,</l>
						<l n="8" num="2.4"><w n="8.1">Rayon</w> <w n="8.2">de</w> <w n="8.3">l</w>’<w n="8.4">amour</w> <w n="8.5">éternel</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Si</w> <w n="9.2">quelqu</w>’<w n="9.3">un</w> <w n="9.4">te</w> <w n="9.5">nomme</w> <w n="9.6">ou</w> <w n="9.7">t</w>’<w n="9.8">appelle</w>,</l>
						<l n="10" num="3.2"><w n="10.1">Ému</w> <w n="10.2">soudain</w> <w n="10.3">à</w> <w n="10.4">cette</w> <w n="10.5">voix</w>,</l>
						<l n="11" num="3.3"><w n="11.1">Je</w> <w n="11.2">tressaille</w> ; <w n="11.3">je</w> <w n="11.4">me</w> <w n="11.5">rappelle</w> ;</l>
						<l n="12" num="3.4"><w n="12.1">Je</w> <w n="12.2">pleure</w> <w n="12.3">et</w> <w n="12.4">souris</w> <w n="12.5">à</w> <w n="12.6">la</w> <w n="12.7">fois</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Ce</w> <w n="13.2">nom</w> <w n="13.3">sacré</w> <w n="13.4">trouble</w> <w n="13.5">et</w> <w n="13.6">caresse</w></l>
						<l n="14" num="4.2"><w n="14.1">Les</w> <w n="14.2">fibres</w> <w n="14.3">de</w> <w n="14.4">mon</w> <w n="14.5">cœur</w> <w n="14.6">blessé</w> ;</l>
						<l n="15" num="4.3"><w n="15.1">C</w>’<w n="15.2">est</w> <w n="15.3">comme</w> <w n="15.4">un</w> <w n="15.5">parfum</w> <w n="15.6">de</w> <w n="15.7">tendresse</w></l>
						<l n="16" num="4.4"><w n="16.1">Que</w> <w n="16.2">sur</w> <w n="16.3">toi</w> <w n="16.4">ma</w> <w n="16.5">mère</w> <w n="16.6">a</w> <w n="16.7">versé</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Ce</w> <w n="17.2">nom</w> <w n="17.3">lui</w> <w n="17.4">seul</w> <w n="17.5">n</w>’<w n="17.6">est</w> <w n="17.7">pas</w> <w n="17.8">la</w> <w n="17.9">cause</w></l>
						<l n="18" num="5.2"><w n="18.1">De</w> <w n="18.2">ma</w> <w n="18.3">fraternelle</w> <w n="18.4">amitié</w> ;</l>
						<l n="19" num="5.3"><w n="19.1">Mais</w> <w n="19.2">il</w> <w n="19.3">y</w> <w n="19.4">donne</w> <w n="19.5">quelque</w> <w n="19.6">chose</w></l>
						<l n="20" num="5.4"><w n="20.1">De</w> <w n="20.2">pur</w> <w n="20.3">et</w> <w n="20.4">de</w> <w n="20.5">sanctifié</w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Je</w> <w n="21.2">cherche</w> <w n="21.3">quelque</w> <w n="21.4">ressemblance</w>,</l>
						<l n="22" num="6.2"><w n="22.1">Écho</w> <w n="22.2">de</w> <w n="22.3">moi</w> <w n="22.4">seul</w> <w n="22.5">entendu</w>,</l>
						<l n="23" num="6.3"><w n="23.1">Entre</w> <w n="23.2">toi</w>, <w n="23.3">vivante</w> <w n="23.4">espérance</w>,</l>
						<l n="24" num="6.4"><w n="24.1">Et</w> <w n="24.2">l</w>’<w n="24.3">être</w> <w n="24.4">aimé</w> <w n="24.5">que</w> <w n="24.6">j</w>’<w n="24.7">ai</w> <w n="24.8">perdu</w>.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">Je</w> <w n="25.2">veux</w> <w n="25.3">que</w> <w n="25.4">le</w> <w n="25.5">nom</w> <w n="25.6">de</w> <w n="25.7">ma</w> <w n="25.8">mère</w></l>
						<l n="26" num="7.2"><w n="26.1">Soit</w> <w n="26.2">une</w> <w n="26.3">étoile</w> <w n="26.4">sur</w> <w n="26.5">ton</w> <w n="26.6">front</w> :</l>
						<l n="27" num="7.3"><w n="27.1">Jamais</w> <w n="27.2">en</w> <w n="27.3">ce</w> <w n="27.4">monde</w> <w n="27.5">éphémère</w></l>
						<l n="28" num="7.4"><w n="28.1">Plus</w> <w n="28.2">de</w> <w n="28.3">vertus</w> <w n="28.4">ne</w> <w n="28.5">fleuriront</w>.</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1"><w n="29.1">On</w> <w n="29.2">eût</w> <w n="29.3">dit</w>, <w n="29.4">tant</w> <w n="29.5">elle</w> <w n="29.6">était</w> <w n="29.7">bonne</w>,</l>
						<l n="30" num="8.2"><w n="30.1">Que</w> <w n="30.2">l</w>’<w n="30.3">ange</w> <w n="30.4">de</w> <w n="30.5">la</w> <w n="30.6">piété</w></l>
						<l n="31" num="8.3"><w n="31.1">Tenait</w> <w n="31.2">sur</w> <w n="31.3">elle</w> <w n="31.4">une</w> <w n="31.5">couronne</w></l>
						<l n="32" num="8.4"><w n="32.1">De</w> <w n="32.2">lumière</w> <w n="32.3">et</w> <w n="32.4">de</w> <w n="32.5">pureté</w> !</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1"><w n="33.1">La</w> <w n="33.2">splendeur</w> <w n="33.3">de</w> <w n="33.4">sa</w> <w n="33.5">noble</w> <w n="33.6">tète</w></l>
						<l n="34" num="9.2"><w n="34.1">N</w>’<w n="34.2">était</w> <w n="34.3">pas</w> <w n="34.4">la</w> <w n="34.5">beauté</w> <w n="34.6">d</w>’<w n="34.7">un</w> <w n="34.8">jour</w>,</l>
						<l n="35" num="9.3"><w n="35.1">Que</w> <w n="35.2">le</w> <w n="35.3">temps</w> <w n="35.4">en</w> <w n="35.5">passant</w> <w n="35.6">nous</w> <w n="35.7">prête</w></l>
						<l n="36" num="9.4"><w n="36.1">Et</w> <w n="36.2">qu</w>’<w n="36.3">il</w> <w n="36.4">nous</w> <w n="36.5">reprend</w> <w n="36.6">sans</w> <w n="36.7">retour</w>.</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1"><w n="37.1">C</w>’<w n="37.2">était</w> <w n="37.3">la</w> <w n="37.4">flamme</w> <w n="37.5">intérieure</w>,</l>
						<l n="38" num="10.2"><w n="38.1">L</w>’<w n="38.2">éclat</w> <w n="38.3">rayonnant</w> <w n="38.4">au</w> <w n="38.5">dehors</w></l>
						<l n="39" num="10.3"><w n="39.1">D</w>’<w n="39.2">une</w> <w n="39.3">âme</w> <w n="39.4">plus</w> <w n="39.5">tendre</w> <w n="39.6">et</w> <w n="39.7">meilleure</w></l>
						<l n="40" num="10.4"><w n="40.1">Que</w> <w n="40.2">les</w> <w n="40.3">âmes</w> <w n="40.4">des</w> <w n="40.5">autres</w> <w n="40.6">corps</w> ;</l>
					</lg>
					<lg n="11">
						<l n="41" num="11.1"><w n="41.1">Un</w> <w n="41.2">je</w> <w n="41.3">ne</w> <w n="41.4">sais</w> <w n="41.5">quoi</w> <w n="41.6">de</w> <w n="41.7">céleste</w></l>
						<l n="42" num="11.2"><w n="42.1">Qui</w> <w n="42.2">faisait</w> <w n="42.3">de</w> <w n="42.4">son</w> <w n="42.5">cœur</w> <w n="42.6">mortel</w>,</l>
						<l n="43" num="11.3"><w n="43.1">A</w> <w n="43.2">la</w> <w n="43.3">fois</w> <w n="43.4">sublime</w> <w n="43.5">et</w> <w n="43.6">modeste</w>,</l>
						<l n="44" num="11.4"><w n="44.1">Le</w> <w n="44.2">tabernacle</w> <w n="44.3">d</w>’<w n="44.4">un</w> <w n="44.5">autel</w>.</l>
					</lg>
					<lg n="12">
						<l n="45" num="12.1"><w n="45.1">Demande</w> <w n="45.2">à</w> <w n="45.3">Dieu</w> <w n="45.4">dans</w> <w n="45.5">ta</w> <w n="45.6">prière</w></l>
						<l n="46" num="12.2"><w n="46.1">Qu</w>’<w n="46.2">il</w> <w n="46.3">t</w>’<w n="46.4">accorde</w> <w n="46.5">le</w> <w n="46.6">même</w> <w n="46.7">don</w> ;</l>
						<l n="47" num="12.3"><w n="47.1">Imite</w>-<w n="47.2">la</w> ; <w n="47.3">sois</w> <w n="47.4">héritière</w></l>
						<l n="48" num="12.4"><w n="48.1">De</w> <w n="48.2">son</w> <w n="48.3">cœur</w> <w n="48.4">comme</w> <w n="48.5">de</w> <w n="48.6">son</w> <w n="48.7">nom</w>.</l>
					</lg>
					<lg n="13">
						<l n="49" num="13.1"><w n="49.1">Car</w>, <w n="49.2">toujours</w> <w n="49.3">humble</w> <w n="49.4">et</w> <w n="49.5">salutaire</w>,</l>
						<l n="50" num="13.2"><w n="50.1">Elle</w> <w n="50.2">allait</w> <w n="50.3">répandant</w> <w n="50.4">le</w> <w n="50.5">miel</w> :</l>
						<l n="51" num="13.3"><w n="51.1">C</w>’<w n="51.2">était</w> <w n="51.3">un</w> <w n="51.4">ange</w> <w n="51.5">sur</w> <w n="51.6">la</w> <w n="51.7">terre</w> ;</l>
						<l n="52" num="13.4"><w n="52.1">C</w>’<w n="52.2">est</w> <w n="52.3">une</w> <w n="52.4">sainte</w> <w n="52.5">dans</w> <w n="52.6">le</w> <w n="52.7">ciel</w> !</l>
					</lg>
				</div></body></text></TEI>