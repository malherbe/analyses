<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="sub_2">TOME PREMIER</title>
				<title type="medium">Édition électronique</title>
				<author key="BLA">
					<name>
						<forename>Prosper</forename>
						<surname>BLANCHEMAIN</surname>
					</name>
					<date from="1816" to="1879">1816-1879</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>7196 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">BLA_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME PREMIER</title>
						<author>Prosper Blanchemain</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/pomesetposie01blan</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
								<title>TOME PREMIER</title>
								<author>Prosper Blanchemain</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE ANCIENNE ET MODERNE - ÉDOUARD ROUVEYRE</publisher>
									<date when="1880">1880</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME I</title>
						<title>POÈMES ET POÉSIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME V</title>
						<title>SONNETS ET FANTAISIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1845" to="1858">1845-1858</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">IDÉAL</head><div type="poem" key="BLA59">
					<head type="main">LE SOUVENIR</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Pour</w> <w n="1.2">soulager</w> <w n="1.3">dans</w> <w n="1.4">leur</w> <w n="1.5">souffrance</w></l>
						<l n="2" num="1.2"><w n="2.1">Ceux</w> <w n="2.2">qui</w> <w n="2.3">pleuraient</w> <w n="2.4">sans</w> <w n="2.5">avenir</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Dieu</w> <w n="3.2">fit</w> <w n="3.3">un</w> <w n="3.4">frère</w> <w n="3.5">à</w> <w n="3.6">l</w>’<w n="3.7">Espérance</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Et</w> <w n="4.2">le</w> <w n="4.3">nomma</w> <w n="4.4">le</w> <w n="4.5">Souvenir</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Le</w> <w n="5.2">Souvenir</w>, <w n="5.3">ange</w> <w n="5.4">fidèle</w>,</l>
						<l n="6" num="2.2"><w n="6.1">Qui</w> <w n="6.2">pleure</w> <w n="6.3">sur</w> <w n="6.4">les</w> <w n="6.5">trépassés</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Et</w> <w n="7.2">qui</w> <w n="7.3">réchauffe</w> <w n="7.4">sous</w> <w n="7.5">son</w> <w n="7.6">aile</w></l>
						<l n="8" num="2.4"><w n="8.1">Les</w> <w n="8.2">cœurs</w> <w n="8.3">mortellement</w> <w n="8.4">blessés</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Nulle</w> <w n="9.2">douleur</w> <w n="9.3">ne</w> <w n="9.4">lui</w> <w n="9.5">résiste</w>,</l>
						<l n="10" num="3.2"><w n="10.1">Quand</w> <w n="10.2">son</w> <w n="10.3">œil</w> <w n="10.4">tendre</w> <w n="10.5">et</w> <w n="10.6">langoureux</w></l>
						<l n="11" num="3.3"><w n="11.1">Montre</w> <w n="11.2">à</w> <w n="11.3">notre</w> <w n="11.4">âme</w> <w n="11.5">qui</w> <w n="11.6">s</w>’<w n="11.7">attriste</w></l>
						<l n="12" num="3.4"><w n="12.1">L</w>’<w n="12.2">ombre</w> <w n="12.3">d</w>’<w n="12.4">un</w> <w n="12.5">passé</w> <w n="12.6">plus</w> <w n="12.7">heureux</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Le</w> <w n="13.2">Souvenir</w> <w n="13.3">console</w> <w n="13.4">et</w> <w n="13.5">charme</w>,</l>
						<l n="14" num="4.2"><w n="14.1">Même</w> <w n="14.2">lorsque</w> <w n="14.3">du</w> <w n="14.4">gouffre</w> <w n="14.5">amer</w></l>
						<l n="15" num="4.3"><w n="15.1">On</w> <w n="15.2">ne</w> <w n="15.3">rapporte</w> <w n="15.4">qu</w>’<w n="15.5">une</w> <w n="15.6">larme</w>,</l>
						<l n="16" num="4.4"><w n="16.1">Comme</w> <w n="16.2">une</w> <w n="16.3">perle</w> <w n="16.4">de</w> <w n="16.5">la</w> <w n="16.6">mer</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Mais</w> <w n="17.2">le</w> <w n="17.3">Souvenir</w>, <w n="17.4">quand</w> <w n="17.5">on</w> <w n="17.6">aime</w>,</l>
						<l n="18" num="5.2"><w n="18.1">C</w>’<w n="18.2">est</w> <w n="18.3">écouter</w> <w n="18.4">de</w> <w n="18.5">douces</w> <w n="18.6">voix</w>,</l>
						<l n="19" num="5.3"><w n="19.1">C</w>’<w n="19.2">est</w> <w n="19.3">faire</w> <w n="19.4">vivre</w> <w n="19.5">la</w> <w n="19.6">mort</w> <w n="19.7">même</w>,</l>
						<l n="20" num="5.4"><w n="20.1">C</w>’<w n="20.2">est</w> <w n="20.3">naître</w> <w n="20.4">une</w> <w n="20.5">seconde</w> <w n="20.6">fois</w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Il</w> <w n="21.2">semble</w> <w n="21.3">qu</w>’<w n="21.4">une</w> <w n="21.5">clarté</w> <w n="21.6">pure</w></l>
						<l n="22" num="6.2"><w n="22.1">Luit</w> <w n="22.2">sur</w> <w n="22.3">notre</w> <w n="22.4">front</w> <w n="22.5">abattu</w>,</l>
						<l n="23" num="6.3"><w n="23.1">Quand</w> <w n="23.2">l</w>’<w n="23.3">ange</w> <w n="23.4">consolant</w> <w n="23.5">murmure</w></l>
						<l n="24" num="6.4"><w n="24.1">Ce</w> <w n="24.2">doux</w> <w n="24.3">mot</w> : « <w n="24.4">Te</w> <w n="24.5">rappelles</w>-<w n="24.6">tu</w> ? »</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">Te</w> <w n="25.2">rappelles</w>-<w n="25.3">tu</w> <w n="25.4">notre</w> <w n="25.5">joie</w></l>
						<l n="26" num="7.2"><w n="26.1">Quand</w>, <w n="26.2">sur</w> <w n="26.3">les</w> <w n="26.4">bords</w> <w n="26.5">irréguliers</w></l>
						<l n="27" num="7.3"><w n="27.1">Où</w> <w n="27.2">la</w> <w n="27.3">Creuse</w> <w n="27.4">indolente</w> <w n="27.5">ondoie</w>,</l>
						<l n="28" num="7.4"><w n="28.1">Nous</w> <w n="28.2">rêvions</w> <w n="28.3">sous</w> <w n="28.4">les</w> <w n="28.5">peupliers</w> ?</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1"><w n="29.1">Te</w> <w n="29.2">rappelles</w>-<w n="29.3">tu</w> <w n="29.4">la</w> <w n="29.5">nacelle</w></l>
						<l n="30" num="8.2"><w n="30.1">Où</w> <w n="30.2">tous</w>, <w n="30.3">en</w> <w n="30.4">chantant</w>, <w n="30.5">nous</w> <w n="30.6">glissions</w>,</l>
						<l n="31" num="8.3"><w n="31.1">Oubliant</w>, <w n="31.2">hélas</w> ! <w n="31.3">qu</w>’<w n="31.4">avec</w> <w n="31.5">elle</w></l>
						<l n="32" num="8.4"><w n="32.1">Le</w> <w n="32.2">temps</w> <w n="32.3">fuyait</w> <w n="32.4">et</w> <w n="32.5">nous</w> <w n="32.6">passions</w> ?</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1"><w n="33.1">Te</w> <w n="33.2">rappelles</w>-<w n="33.3">tu</w> <w n="33.4">notre</w> <w n="33.5">ivresse</w></l>
						<l n="34" num="9.2"><w n="34.1">En</w> <w n="34.2">ces</w> <w n="34.3">jours</w> <w n="34.4">par</w> <w n="34.5">le</w> <w n="34.6">ciel</w> <w n="34.7">bénis</w> ?</l>
						<l n="35" num="9.3"><w n="35.1">Te</w> <w n="35.2">rappelles</w>-<w n="35.3">tu</w> <w n="35.4">la</w> <w n="35.5">tendresse</w></l>
						<l n="36" num="9.4"><w n="36.1">Qui</w> <w n="36.2">nous</w> <w n="36.3">a</w> <w n="36.4">pour</w> <w n="36.5">jamais</w> <w n="36.6">unis</w> ?</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1"><w n="37.1">Je</w> <w n="37.2">pars</w> <w n="37.3">et</w> <w n="37.4">j</w>’<w n="37.5">emporte</w> <w n="37.6">ces</w> <w n="37.7">choses</w></l>
						<l n="38" num="10.2"><w n="38.1">Pour</w> <w n="38.2">me</w> <w n="38.3">consoler</w> <w n="38.4">en</w> <w n="38.5">chemin</w>,</l>
						<l n="39" num="10.3"><w n="39.1">Comme</w> <w n="39.2">on</w> <w n="39.3">garde</w> <w n="39.4">un</w> <w n="39.5">bouquet</w> <w n="39.6">de</w> <w n="39.7">roses</w></l>
						<l n="40" num="10.4"><w n="40.1">Qui</w> <w n="40.2">s</w>’<w n="40.3">est</w> <w n="40.4">fané</w> <w n="40.5">dans</w> <w n="40.6">une</w> <w n="40.7">main</w>.</l>
					</lg>
					<lg n="11">
						<l n="41" num="11.1"><w n="41.1">De</w> <w n="41.2">ce</w> <w n="41.3">passé</w>, <w n="41.4">fleur</w> <w n="41.5">idéale</w></l>
						<l n="42" num="11.2"><w n="42.1">Qu</w>’<w n="42.2">en</w> <w n="42.3">moi</w>-<w n="42.4">même</w> <w n="42.5">j</w>’<w n="42.6">enfermerai</w>,</l>
						<l n="43" num="11.3"><w n="43.1">Je</w> <w n="43.2">respirerai</w> <w n="43.3">le</w> <w n="43.4">pétale</w></l>
						<l n="44" num="11.4"><w n="44.1">Précieux</w> <w n="44.2">et</w> <w n="44.3">décoloré</w>.</l>
					</lg>
					<lg n="12">
						<l n="45" num="12.1"><w n="45.1">Là</w>-<w n="45.2">bas</w>, <w n="45.3">dans</w> <w n="45.4">ma</w> <w n="45.5">triste</w> <w n="45.6">demeure</w></l>
						<l n="46" num="12.2"><w n="46.1">Où</w> <w n="46.2">le</w> <w n="46.3">temps</w> <w n="46.4">semble</w> <w n="46.5">se</w> <w n="46.6">traîner</w>,</l>
						<l n="47" num="12.3"><w n="47.1">Ces</w> <w n="47.2">beaux</w> <w n="47.3">jours</w> <w n="47.4">enfuis</w> <w n="47.5">comme</w> <w n="47.6">une</w> <w n="47.7">heure</w></l>
						<l n="48" num="12.4"><w n="48.1">Viendront</w> <w n="48.2">souvent</w> <w n="48.3">m</w>’<w n="48.4">illuminer</w>.</l>
					</lg>
					<lg n="13">
						<l n="49" num="13.1"><w n="49.1">De</w> <w n="49.2">ses</w> <w n="49.3">mains</w> <w n="49.4">tendres</w> <w n="49.5">et</w> <w n="49.6">timides</w>,</l>
						<l n="50" num="13.2"><w n="50.1">Le</w> <w n="50.2">Souvenir</w>, <w n="50.3">ange</w> <w n="50.4">pieux</w>,</l>
						<l n="51" num="13.3"><w n="51.1">Touchant</w> <w n="51.2">mes</w> <w n="51.3">paupières</w> <w n="51.4">humides</w>,</l>
						<l n="52" num="13.4"><w n="52.1">Essuîra</w> <w n="52.2">les</w> <w n="52.3">pleurs</w> <w n="52.4">de</w> <w n="52.5">mes</w> <w n="52.6">yeux</w>.</l>
					</lg>
					<lg n="14">
						<l n="53" num="14.1"><w n="53.1">Il</w> <w n="53.2">viendra</w>, <w n="53.3">quand</w> <w n="53.4">la</w> <w n="53.5">nuit</w> <w n="53.6">m</w>’<w n="53.7">enlève</w></l>
						<l n="54" num="14.2"><w n="54.1">Au</w> <w n="54.2">souci</w> <w n="54.3">toujours</w> <w n="54.4">renaissant</w>,</l>
						<l n="55" num="14.3"><w n="55.1">A</w> <w n="55.2">travers</w> <w n="55.3">le</w> <w n="55.4">prisme</w> <w n="55.5">du</w> <w n="55.6">rêve</w></l>
						<l n="56" num="14.4"><w n="56.1">Me</w> <w n="56.2">peindre</w> <w n="56.3">ton</w> <w n="56.4">sourire</w> <w n="56.5">absent</w>.</l>
					</lg>
					<lg n="15">
						<l n="57" num="15.1"><w n="57.1">Plus</w> <w n="57.2">rapide</w> <w n="57.3">qu</w>’<w n="57.4">un</w> <w n="57.5">trait</w> <w n="57.6">de</w> <w n="57.7">flamme</w>,</l>
						<l n="58" num="15.2"><w n="58.1">De</w> <w n="58.2">l</w>’<w n="58.3">un</w> <w n="58.4">à</w> <w n="58.5">l</w>’<w n="58.6">autre</w> <w n="58.7">il</w> <w n="58.8">volera</w> ;</l>
						<l n="59" num="15.3"><w n="59.1">D</w>’<w n="59.2">une</w> <w n="59.3">même</w> <w n="59.4">voix</w>, <w n="59.5">dans</w> <w n="59.6">ton</w> <w n="59.7">âme</w></l>
						<l n="60" num="15.4"><w n="60.1">Et</w> <w n="60.2">dans</w> <w n="60.3">la</w> <w n="60.4">mienne</w> <w n="60.5">il</w> <w n="60.6">parlera</w>.</l>
					</lg>
					<lg n="16">
						<l n="61" num="16.1"><w n="61.1">Plus</w> <w n="61.2">tard</w>, <w n="61.3">si</w> <w n="61.4">je</w> <w n="61.5">reviens</w> <w n="61.6">encore</w></l>
						<l n="62" num="16.2"><w n="62.1">Dans</w> <w n="62.2">ces</w> <w n="62.3">lieux</w> <w n="62.4">féconds</w> <w n="62.5">en</w> <w n="62.6">beaux</w> <w n="62.7">jours</w>,</l>
						<l n="63" num="16.3"><w n="63.1">L</w>’<w n="63.2">ange</w> <w n="63.3">au</w> <w n="63.4">consolant</w> <w n="63.5">météore</w></l>
						<l n="64" num="16.4"><w n="64.1">Sur</w> <w n="64.2">nous</w> <w n="64.3">resplendira</w> <w n="64.4">toujours</w> ;</l>
					</lg>
					<lg n="17">
						<l n="65" num="17.1"><w n="65.1">Et</w>, <w n="65.2">confondant</w> <w n="65.3">nos</w> <w n="65.4">cœurs</w> <w n="65.5">fidèles</w></l>
						<l n="66" num="17.2"><w n="66.1">Dans</w> <w n="66.2">d</w>’<w n="66.3">ineffables</w> <w n="66.4">entretiens</w>,</l>
						<l n="67" num="17.3"><w n="67.1">Quand</w> <w n="67.2">je</w> <w n="67.3">dirai</w> : « <w n="67.4">Tu</w> <w n="67.5">te</w> <w n="67.6">rappelles</w> ? »</l>
						<l n="68" num="17.4"><w n="68.1">Tu</w> <w n="68.2">répondras</w> : « <w n="68.3">Je</w> <w n="68.4">me</w> <w n="68.5">souviens</w> ! »</l>
					</lg>
				</div></body></text></TEI>