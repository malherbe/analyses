<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="sub_2">TOME PREMIER</title>
				<title type="medium">Édition électronique</title>
				<author key="BLA">
					<name>
						<forename>Prosper</forename>
						<surname>BLANCHEMAIN</surname>
					</name>
					<date from="1816" to="1879">1816-1879</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>7196 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">BLA_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME PREMIER</title>
						<author>Prosper Blanchemain</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/pomesetposie01blan</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
								<title>TOME PREMIER</title>
								<author>Prosper Blanchemain</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE ANCIENNE ET MODERNE - ÉDOUARD ROUVEYRE</publisher>
									<date when="1880">1880</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME I</title>
						<title>POÈMES ET POÉSIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME V</title>
						<title>SONNETS ET FANTAISIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1845" to="1858">1845-1858</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">IDÉAL</head><div type="poem" key="BLA108">
					<head type="main">SÉCRÈTE PENSÉE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Dans</w> <w n="1.2">ton</w> <w n="1.3">cœur</w> <w n="1.4">joyeux</w> <w n="1.5">ou</w> <w n="1.6">blessé</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Tu</w> <w n="2.2">gardes</w> <w n="2.3">à</w> <w n="2.4">jamais</w> <w n="2.5">empreintes</w></l>
						<l n="3" num="1.3"><w n="3.1">Les</w> <w n="3.2">reliques</w> <w n="3.3">mornes</w> <w n="3.4">et</w> <w n="3.5">saintes</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Les</w> <w n="4.2">traces</w> <w n="4.3">mortes</w> <w n="4.4">du</w> <w n="4.5">passé</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Comme</w> <w n="5.2">un</w> <w n="5.3">bourdonnement</w> <w n="5.4">d</w>’<w n="5.5">abeille</w>,</l>
						<l n="6" num="2.2"><w n="6.1">Un</w> <w n="6.2">murmure</w> <w n="6.3">vague</w> <w n="6.4">et</w> <w n="6.5">confus</w></l>
						<l n="7" num="2.3"><w n="7.1">T</w>’<w n="7.2">entretient</w> <w n="7.3">de</w> <w n="7.4">ce</w> <w n="7.5">qui</w> <w n="7.6">n</w>’<w n="7.7">est</w> <w n="7.8">plus</w>,</l>
						<l n="8" num="2.4"><w n="8.1">Et</w> <w n="8.2">tu</w> <w n="8.3">prêtes</w> <w n="8.4">en</w> <w n="8.5">toi</w> <w n="8.6">l</w>’<w n="8.7">oreille</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Sur</w> <w n="9.2">ta</w> <w n="9.3">lèvre</w> <w n="9.4">où</w> <w n="9.5">tout</w> <w n="9.6">bruit</w> <w n="9.7">s</w>’<w n="9.8">est</w> <w n="9.9">tu</w></l>
						<l n="10" num="3.2"><w n="10.1">Posant</w> <w n="10.2">tout</w> <w n="10.3">à</w> <w n="10.4">coup</w> <w n="10.5">ton</w> <w n="10.6">doigt</w> <w n="10.7">rose</w>,</l>
						<l n="11" num="3.3"><w n="11.1">Tu</w> <w n="11.2">dis</w> : — « <w n="11.3">Je</w> <w n="11.4">pense</w> <w n="11.5">à</w> <w n="11.6">quelque</w> <w n="11.7">chose</w> ! »</l>
						<l n="12" num="3.4"><w n="12.1">O</w> <w n="12.2">rêveuse</w>, <w n="12.3">à</w> <w n="12.4">quoi</w> <w n="12.5">penses</w>-<w n="12.6">tu</w> ?</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Qui</w> <w n="13.2">le</w> <w n="13.3">sait</w> ? <w n="13.4">qui</w> <w n="13.5">le</w> <w n="13.6">pourrait</w> <w n="13.7">dire</w> ?</l>
						<l n="14" num="4.2"><w n="14.1">Tu</w> <w n="14.2">laisses</w> <w n="14.3">à</w> <w n="14.4">tous</w> <w n="14.5">ignorer</w></l>
						<l n="15" num="4.3"><w n="15.1">Que</w> ! <w n="15.2">souvenir</w> <w n="15.3">te</w> <w n="15.4">fait</w> <w n="15.5">pleurer</w>,</l>
						<l n="16" num="4.4"><w n="16.1">Quel</w> <w n="16.2">vague</w> <w n="16.3">espoir</w> <w n="16.4">te</w> <w n="16.5">fait</w> <w n="16.6">sourire</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Tu</w> <w n="17.2">dérobes</w> <w n="17.3">à</w> <w n="17.4">tous</w> <w n="17.5">les</w> <w n="17.6">yeux</w></l>
						<l n="18" num="5.2"><w n="18.1">Ton</w> <w n="18.2">âme</w> : <w n="18.3">il</w> <w n="18.4">faut</w> <w n="18.5">qu</w>’<w n="18.6">on</w> <w n="18.7">la</w> <w n="18.8">devine</w>.</l>
						<l n="19" num="5.3"><w n="19.1">C</w>’<w n="19.2">est</w> <w n="19.3">une</w> <w n="19.4">fleur</w> <w n="19.5">chaste</w> <w n="19.6">et</w> <w n="19.7">divine</w>,</l>
						<l n="20" num="5.4"><w n="20.1">Qui</w> <w n="20.2">ne</w> <w n="20.3">s</w>’<w n="20.4">ouvre</w> <w n="20.5">que</w> <w n="20.6">pour</w> <w n="20.7">les</w> <w n="20.8">deux</w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Mais</w> <w n="21.2">c</w>’<w n="21.3">est</w> <w n="21.4">un</w> <w n="21.5">bonheur</w> <w n="21.6">doux</w> <w n="21.7">et</w> <w n="21.8">triste</w></l>
						<l n="22" num="6.2"><w n="22.1">Que</w> <w n="22.2">de</w> <w n="22.3">la</w> <w n="22.4">respirer</w> <w n="22.5">d</w>’<w n="22.6">en</w> <w n="22.7">bas</w>,</l>
						<l n="23" num="6.3"><w n="23.1">Cette</w> <w n="23.2">fleur</w> <w n="23.3">qu</w>’<w n="23.4">on</w> <w n="23.5">n</w>’<w n="23.6">entrevoit</w> <w n="23.7">pas</w>,</l>
						<l n="24" num="6.4"><w n="24.1">Et</w> <w n="24.2">de</w> <w n="24.3">connaître</w> <w n="24.4">qu</w>’<w n="24.5">elle</w> <w n="24.6">existe</w>.</l>
					</lg>
				</div></body></text></TEI>