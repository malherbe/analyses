<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="sub_2">TOME PREMIER</title>
				<title type="medium">Édition électronique</title>
				<author key="BLA">
					<name>
						<forename>Prosper</forename>
						<surname>BLANCHEMAIN</surname>
					</name>
					<date from="1816" to="1879">1816-1879</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>7196 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">BLA_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME PREMIER</title>
						<author>Prosper Blanchemain</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/pomesetposie01blan</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
								<title>TOME PREMIER</title>
								<author>Prosper Blanchemain</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE ANCIENNE ET MODERNE - ÉDOUARD ROUVEYRE</publisher>
									<date when="1880">1880</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME I</title>
						<title>POÈMES ET POÉSIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME V</title>
						<title>SONNETS ET FANTAISIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1845" to="1858">1845-1858</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">IDÉAL</head><div type="poem" key="BLA77">
					<head type="main">REMEMBRANCE</head>
					<opener>
						<salute>A la mémoire de Mme Marie Camaret.</salute>
					</opener>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Je</w> <w n="1.2">ne</w> <w n="1.3">pourrai</w> <w n="1.4">jamais</w> <w n="1.5">passer</w> <w n="1.6">par</w> <w n="1.7">cette</w> <w n="1.8">rue</w></l>
						<l n="2" num="1.2"><space unit="char" quantity="8"></space><w n="2.1">Sans</w> <w n="2.2">être</w> <w n="2.3">atteint</w> <w n="2.4">d</w>’<w n="2.5">un</w> <w n="2.6">sombre</w> <w n="2.7">ennui</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Sans</w> <w n="3.2">y</w> <w n="3.3">pleurer</w> <w n="3.4">sur</w> <w n="3.5">vous</w>, <w n="3.6">pauvre</w> <w n="3.7">âme</w> <w n="3.8">disparue</w>,</l>
						<l n="4" num="1.4"><space unit="char" quantity="8"></space><w n="4.1">Astre</w> <w n="4.2">charmant</w> <w n="4.3">trop</w> <w n="4.4">vite</w> <w n="4.5">enfui</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Je</w> <w n="5.2">ne</w> <w n="5.3">pourrai</w> <w n="5.4">jamais</w> <w n="5.5">revoir</w> <w n="5.6">cette</w> <w n="5.7">fenêtre</w></l>
						<l n="6" num="2.2"><space unit="char" quantity="8"></space><w n="6.1">Où</w> <w n="6.2">vos</w> <w n="6.3">yeux</w> <w n="6.4">brillèrent</w> <w n="6.5">souvent</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Sans</w> <w n="7.2">croire</w> <w n="7.3">que</w> <w n="7.4">soudain</w> <w n="7.5">vous</w> <w n="7.6">allez</w> <w n="7.7">apparaître</w></l>
						<l n="8" num="2.4"><space unit="char" quantity="8"></space><w n="8.1">Sous</w> <w n="8.2">les</w> <w n="8.3">plis</w> <w n="8.4">du</w> <w n="8.5">rideau</w> <w n="8.6">mouvant</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">C</w>’<w n="9.2">est</w> <w n="9.3">dans</w> <w n="9.4">cette</w> <w n="9.5">maison</w> <w n="9.6">que</w> <w n="9.7">vous</w> <w n="9.8">viviez</w>, <w n="9.9">Marie</w>,</l>
						<l n="10" num="3.2"><space unit="char" quantity="8"></space><w n="10.1">Comme</w> <w n="10.2">un</w> <w n="10.3">oiseau</w> <w n="10.4">parmi</w> <w n="10.5">les</w> <w n="10.6">fleurs</w> ;</l>
						<l n="11" num="3.3"><w n="11.1">Maison</w> <w n="11.2">jadis</w> <w n="11.3">heureuse</w>, <w n="11.4">aujourd</w>’<w n="11.5">hui</w> <w n="11.6">défleurie</w>,</l>
						<l n="12" num="3.4"><space unit="char" quantity="8"></space><w n="12.1">Nid</w> <w n="12.2">pillé</w> <w n="12.3">par</w> <w n="12.4">des</w> <w n="12.5">oiseleurs</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Aujourd</w>’<w n="13.2">hui</w>, <w n="13.3">quand</w> <w n="13.4">je</w> <w n="13.5">songe</w> <w n="13.6">à</w> <w n="13.7">ces</w> <w n="13.8">rapides</w> <w n="13.9">heures</w></l>
						<l n="14" num="4.2"><space unit="char" quantity="8"></space><w n="14.1">Que</w> <w n="14.2">nous</w> <w n="14.3">passions</w> <w n="14.4">auprès</w> <w n="14.5">de</w> <w n="14.6">vous</w>,</l>
						<l n="15" num="4.3"><w n="15.1">A</w> <w n="15.2">l</w>’<w n="15.3">aimable</w> <w n="15.4">gaîté</w> <w n="15.5">qui</w> <w n="15.6">peuplait</w> <w n="15.7">vos</w> <w n="15.8">demeures</w>,</l>
						<l n="16" num="4.4"><space unit="char" quantity="8"></space><w n="16.1">A</w> <w n="16.2">vos</w> <w n="16.3">rires</w> <w n="16.4">brillants</w> <w n="16.5">et</w> <w n="16.6">doux</w> ;</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Quand</w> <w n="17.2">je</w> <w n="17.3">songe</w> <w n="17.4">comment</w> <w n="17.5">nous</w> <w n="17.6">dépensions</w> <w n="17.7">la</w> <w n="17.8">vie</w>,</l>
						<l n="18" num="5.2"><space unit="char" quantity="8"></space><w n="18.1">En</w> <w n="18.2">prodigues</w>, <w n="18.3">sans</w> <w n="18.4">rien</w> <w n="18.5">compter</w>,</l>
						<l n="19" num="5.3"><w n="19.1">Sans</w> <w n="19.2">penser</w> <w n="19.3">que</w> <w n="19.4">soudain</w> <w n="19.5">vous</w> <w n="19.6">nous</w> <w n="19.7">seriez</w> <w n="19.8">ravie</w></l>
						<l n="20" num="5.4"><space unit="char" quantity="8"></space><w n="20.1">Et</w> <w n="20.2">qu</w>’<w n="20.3">il</w> <w n="20.4">nous</w> <w n="20.5">faudrait</w> <w n="20.6">vous</w> <w n="20.7">quitter</w>,</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Je</w> <w n="21.2">crois</w> <w n="21.3">que</w> <w n="21.4">c</w>’<w n="21.5">est</w> <w n="21.6">un</w> <w n="21.7">rêve</w> <w n="21.8">et</w> <w n="21.9">que</w>, <w n="21.10">rieuse</w> <w n="21.11">encore</w>,</l>
						<l n="22" num="6.2"><space unit="char" quantity="8"></space><w n="22.1">Demain</w> <w n="22.2">vous</w> <w n="22.3">serez</w> <w n="22.4">de</w> <w n="22.5">retour</w> ;</l>
						<l n="23" num="6.3"><w n="23.1">Comme</w> <w n="23.2">au</w> <w n="23.3">sein</w> <w n="23.4">de</w> <w n="23.5">la</w> <w n="23.6">nuit</w> <w n="23.7">on</w> <w n="23.8">compte</w> <w n="23.9">sur</w> <w n="23.10">l</w>’<w n="23.11">aurore</w>.</l>
						<l n="24" num="6.4"><space unit="char" quantity="8"></space><w n="24.1">Comme</w> <w n="24.2">on</w> <w n="24.3">pressent</w> <w n="24.4">le</w> <w n="24.5">point</w> <w n="24.6">du</w> <w n="24.7">jour</w>.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">Le</w> <w n="25.2">jour</w> <w n="25.3">n</w>’<w n="25.4">éclora</w> <w n="25.5">plus</w> ; <w n="25.6">l</w>’<w n="25.7">espérance</w> <w n="25.8">est</w> <w n="25.9">trompée</w> !</l>
						<l n="26" num="7.2"><space unit="char" quantity="8"></space><w n="26.1">Adieu</w>, <w n="26.2">Marie</w>, <w n="26.3">un</w> <w n="26.4">long</w> <w n="26.5">adieu</w> !</l>
						<l n="27" num="7.3"><w n="27.1">Votre</w> <w n="27.2">corps</w> <w n="27.3">s</w>’<w n="27.4">est</w> <w n="27.5">flétri</w> <w n="27.6">comme</w> <w n="27.7">l</w>’<w n="27.8">herbe</w> <w n="27.9">coupée</w> ;</l>
						<l n="28" num="7.4"><space unit="char" quantity="8"></space><w n="28.1">Votre</w> <w n="28.2">âme</w> <w n="28.3">est</w> <w n="28.4">retournée</w> <w n="28.5">à</w> <w n="28.6">Dieu</w>.</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1"><w n="29.1">Je</w> <w n="29.2">crois</w> <w n="29.3">vous</w> <w n="29.4">voir</w> <w n="29.5">encor</w> <w n="29.6">brisée</w> <w n="29.7">en</w> <w n="29.8">votre</w> <w n="29.9">couche</w></l>
						<l n="30" num="8.2"><space unit="char" quantity="8"></space><w n="30.1">Par</w> <w n="30.2">tant</w> <w n="30.3">de</w> <w n="30.4">maux</w> <w n="30.5">multipliés</w> :</l>
						<l n="31" num="8.3"><w n="31.1">A</w> <w n="31.2">peine</w> <w n="31.3">entendait</w>-<w n="31.4">on</w> <w n="31.5">passer</w> <w n="31.6">sur</w> <w n="31.7">votre</w> <w n="31.8">bouche</w></l>
						<l n="32" num="8.4"><space unit="char" quantity="8"></space><w n="32.1">Un</w> <w n="32.2">murmure</w> <w n="32.3">quand</w> <w n="32.4">vous</w> <w n="32.5">parliez</w> ;</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1"><w n="33.1">Sans</w> <w n="33.2">l</w>’<w n="33.3">éclat</w> <w n="33.4">dont</w> <w n="33.5">brillait</w> <w n="33.6">votre</w> <w n="33.7">prunelle</w> <w n="33.8">noire</w></l>
						<l n="34" num="9.2"><space unit="char" quantity="8"></space><w n="34.1">Sur</w> <w n="34.2">votre</w> <w n="34.3">effrayante</w> <w n="34.4">pâleur</w>,</l>
						<l n="35" num="9.3"><w n="35.1">J</w>’<w n="35.2">aurais</w> <w n="35.3">cru</w> <w n="35.4">voir</w> <w n="35.5">en</w> <w n="35.6">vous</w> <w n="35.7">une</w> <w n="35.8">Vierge</w> <w n="35.9">d</w>’<w n="35.10">ivoire</w></l>
						<l n="36" num="9.4"><space unit="char" quantity="8"></space><w n="36.1">Sortant</w> <w n="36.2">des</w> <w n="36.3">mains</w> <w n="36.4">du</w> <w n="36.5">ciseleur</w>.</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1"><w n="37.1">Vous</w> <w n="37.2">étiez</w> <w n="37.3">déjà</w> <w n="37.4">morte</w>, <w n="37.5">ô</w> <w n="37.6">ma</w> <w n="37.7">blanche</w> <w n="37.8">martyre</w> !</l>
						<l n="38" num="10.2"><space unit="char" quantity="8"></space><w n="38.1">Vos</w> <w n="38.2">yeux</w> <w n="38.3">seuls</w> <w n="38.4">gardaient</w> <w n="38.5">un</w> <w n="38.6">éclair</w> ;</l>
						<l n="39" num="10.3"><w n="39.1">Mais</w> <w n="39.2">ce</w> <w n="39.3">dernier</w> <w n="39.4">regard</w> <w n="39.5">cherchait</w> <w n="39.6">à</w> <w n="39.7">nous</w> <w n="39.8">sourire</w>,</l>
						<l n="40" num="10.4"><space unit="char" quantity="8"></space><w n="40.1">Pâle</w> <w n="40.2">comme</w> <w n="40.3">un</w> <w n="40.4">rayon</w> <w n="40.5">d</w>’<w n="40.6">hiver</w>.</l>
					</lg>
					<lg n="11">
						<l n="41" num="11.1"><w n="41.1">Et</w> <w n="41.2">vous</w> <w n="41.3">redemandiez</w> <w n="41.4">votre</w> <w n="41.5">terre</w> <w n="41.6">natale</w> ;</l>
						<l n="42" num="11.2"><space unit="char" quantity="8"></space><w n="42.1">Son</w> <w n="42.2">soleil</w> <w n="42.3">vous</w> <w n="42.4">devait</w> <w n="42.5">guérir</w> ;</l>
						<l n="43" num="11.3"><w n="43.1">Et</w> <w n="43.2">vous</w> <w n="43.3">disiez</w> <w n="43.4">qu</w>’<w n="43.5">au</w> <w n="43.6">mois</w> <w n="43.7">des</w> <w n="43.8">roses</w> <w n="43.9">du</w> <w n="43.10">Bengale</w>.</l>
						<l n="44" num="11.4"><space unit="char" quantity="8"></space><w n="44.1">Vous</w> <w n="44.2">alliez</w> <w n="44.3">aussi</w> <w n="44.4">refleurir</w> !</l>
					</lg>
					<lg n="12">
						<l n="45" num="12.1"><w n="45.1">Vous</w> <w n="45.2">berciez</w>-<w n="45.3">vous</w> <w n="45.4">vraiment</w> <w n="45.5">avec</w> <w n="45.6">cette</w> <w n="45.7">chimère</w> ?</l>
						<l n="46" num="12.2"><space unit="char" quantity="8"></space><w n="46.1">Ou</w>, <w n="46.2">plutôt</w>, <w n="46.3">ne</w> <w n="46.4">vouliez</w>-<w n="46.5">vous</w> <w n="46.6">pas</w></l>
						<l n="47" num="12.3"><w n="47.1">Aller</w> <w n="47.2">mourir</w> <w n="47.3">aux</w> <w n="47.4">bords</w> <w n="47.5">où</w> <w n="47.6">mourut</w> <w n="47.7">votre</w> <w n="47.8">mère</w>,</l>
						<l n="48" num="12.4"><space unit="char" quantity="8"></space><w n="48.1">Et</w> <w n="48.2">vous</w> <w n="48.3">endormir</w> <w n="48.4">dans</w> <w n="48.5">ses</w> <w n="48.6">bras</w> ?</l>
					</lg>
					<lg n="13">
						<l n="49" num="13.1"><w n="49.1">Vous</w> <w n="49.2">reposez</w> <w n="49.3">près</w> <w n="49.4">d</w>’<w n="49.5">elle</w>. <w n="49.6">Heureux</w> <w n="49.7">qui</w>, <w n="49.8">jeune</w> <w n="49.9">encore</w>,</l>
						<l n="50" num="13.2"><space unit="char" quantity="8"></space><w n="50.1">Peut</w> <w n="50.2">retourner</w> <w n="50.3">à</w> <w n="50.4">l</w>’<w n="50.5">Éternel</w> !</l>
						<l n="51" num="13.3"><w n="51.1">Heureux</w> <w n="51.2">qui</w>, <w n="51.3">dans</w> <w n="51.4">les</w> <w n="51.5">champs</w> <w n="51.6">témoins</w> <w n="51.7">de</w> <w n="51.8">son</w> <w n="51.9">aurore</w>,</l>
						<l n="52" num="13.4"><space unit="char" quantity="8"></space><w n="52.1">S</w>’<w n="52.2">endort</w> <w n="52.3">au</w> <w n="52.4">tombeau</w> <w n="52.5">maternel</w> !</l>
					</lg>
					<closer>
						<dateline>
						<placeName>Paris</placeName>,
							<date when="1856">décembre 1856.</date>
						</dateline>
					</closer>
				</div></body></text></TEI>