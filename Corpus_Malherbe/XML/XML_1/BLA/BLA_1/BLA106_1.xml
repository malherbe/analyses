<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="sub_2">TOME PREMIER</title>
				<title type="medium">Édition électronique</title>
				<author key="BLA">
					<name>
						<forename>Prosper</forename>
						<surname>BLANCHEMAIN</surname>
					</name>
					<date from="1816" to="1879">1816-1879</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>7196 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">BLA_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME PREMIER</title>
						<author>Prosper Blanchemain</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/pomesetposie01blan</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
								<title>TOME PREMIER</title>
								<author>Prosper Blanchemain</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE ANCIENNE ET MODERNE - ÉDOUARD ROUVEYRE</publisher>
									<date when="1880">1880</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME I</title>
						<title>POÈMES ET POÉSIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME V</title>
						<title>SONNETS ET FANTAISIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1845" to="1858">1845-1858</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">IDÉAL</head><div type="poem" key="BLA106">
					<head type="main">LA JEUNE FILLE ET LES FLEURS</head>
					<head type="form">IDYLLE</head>
					<opener>
						<salute>A Ernest Courbet.</salute>
						<epigraph>
							<cit>
								<quote>
										L’âme de mille fleurs dans les zéphyrs semée.
								</quote>
								<bibl>
									<name>André Chénier</name>.
								</bibl>
							</cit>
						</epigraph>
					</opener>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Jeune</w> <w n="1.2">fille</w> <w n="1.3">des</w> <w n="1.4">champs</w>, <w n="1.5">vierge</w> <w n="1.6">aux</w> <w n="1.7">brillants</w> <w n="1.8">cheveux</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Tu</w> <w n="2.2">souris</w> <w n="2.3">et</w> <w n="2.4">ne</w> <w n="2.5">sais</w>, <w n="2.6">enfant</w>, <w n="2.7">ce</w> <w n="2.8">que</w> <w n="2.9">tu</w> <w n="2.10">veux</w>.</l>
						<l n="3" num="1.3"><w n="3.1">Tu</w> <w n="3.2">butines</w> <w n="3.3">des</w> <w n="3.4">fleurs</w> <w n="3.5">dont</w> <w n="3.6">tu</w> <w n="3.7">pares</w> <w n="3.8">ta</w> <w n="3.9">tète</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Et</w> <w n="4.2">seule</w> <w n="4.3">tu</w> <w n="4.4">te</w> <w n="4.5">plais</w> <w n="4.6">à</w> <w n="4.7">des</w> <w n="4.8">pensers</w> <w n="4.9">de</w> <w n="4.10">fête</w> ;</l>
						<l n="5" num="1.5"><w n="5.1">Puis</w> <w n="5.2">ces</w> <w n="5.3">fleurs</w> <w n="5.4">dont</w> <w n="5.5">ta</w> <w n="5.6">main</w>, <w n="5.7">ta</w> <w n="5.8">main</w> <w n="5.9">aux</w> <w n="5.10">légers</w> <w n="5.11">doigts</w>,</l>
						<l n="6" num="1.6"><w n="6.1">Entrelaçait</w> <w n="6.2">les</w> <w n="6.3">nœuds</w> <w n="6.4">recommencés</w> <w n="6.5">vingt</w> <w n="6.6">fois</w>,</l>
						<l n="7" num="1.7"><w n="7.1">Tu</w> <w n="7.2">n</w>’<w n="7.3">en</w> <w n="7.4">veux</w> <w n="7.5">plus</w> ; <w n="7.6">ces</w> <w n="7.7">fleurs</w> <w n="7.8">si</w> <w n="7.9">bien</w> <w n="7.10">faites</w> <w n="7.11">pour</w> <w n="7.12">plaire</w></l>
						<l n="8" num="1.8"><w n="8.1">Soulèvent</w>, <w n="8.2">et</w> <w n="8.3">pourquoi</w> ? <w n="8.4">ta</w> <w n="8.5">mutine</w> <w n="8.6">colère</w>.</l>
						<l n="9" num="1.9"><w n="9.1">Ton</w> <w n="9.2">giron</w> <w n="9.3">s</w>’<w n="9.4">embaumait</w> <w n="9.5">de</w> <w n="9.6">leurs</w> <w n="9.7">flots</w> <w n="9.8">diaprés</w>,</l>
						<l n="10" num="1.10"><w n="10.1">Et</w> <w n="10.2">tu</w> <w n="10.3">vois</w> <w n="10.4">en</w> <w n="10.5">dédain</w> <w n="10.6">ces</w> <w n="10.7">dépouilles</w> <w n="10.8">des</w> <w n="10.9">prés</w>,</l>
						<l n="11" num="1.11"><w n="11.1">Et</w> <w n="11.2">tes</w> <w n="11.3">jeux</w> <w n="11.4">enfantins</w> <w n="11.5">en</w> <w n="11.6">ont</w> <w n="11.7">jonché</w> <w n="11.8">la</w> <w n="11.9">route</w>,</l>
						<l n="12" num="1.12"><w n="12.1">Et</w> <w n="12.2">le</w> <w n="12.3">soleil</w> <w n="12.4">les</w> <w n="12.5">fane</w>. <w n="12.6">O</w> <w n="12.7">jeune</w> <w n="12.8">fille</w>, <w n="12.9">écoute</w> !</l>
						<l n="13" num="1.13"><w n="13.1">N</w>’<w n="13.2">entends</w>-<w n="13.3">tu</w> <w n="13.4">pas</w> <w n="13.5">des</w> <w n="13.6">voix</w>, <w n="13.7">de</w> <w n="13.8">faibles</w> <w n="13.9">voix</w>, <w n="13.10">tout</w> <w n="13.11">bas</w>,</l>
						<l n="14" num="1.14"><w n="14.1">Comme</w> <w n="14.2">un</w> <w n="14.3">soupir</w> <w n="14.4">du</w> <w n="14.5">vent</w>, <w n="14.6">murmurer</w> <w n="14.7">sous</w> <w n="14.8">tes</w> <w n="14.9">pas</w> ?</l>
						<l n="15" num="1.15"><w n="15.1">Dans</w> <w n="15.2">cet</w> <w n="15.3">air</w> <w n="15.4">pur</w> <w n="15.5">qui</w> <w n="15.6">joue</w> <w n="15.7">autour</w> <w n="15.8">de</w> <w n="15.9">ton</w> <w n="15.10">visage</w>,</l>
						<l n="16" num="1.16"><w n="16.1">Enfant</w>, <w n="16.2">ne</w> <w n="16.3">sens</w>-<w n="16.4">tu</w> <w n="16.5">rien</w> <w n="16.6">te</w> <w n="16.7">toucher</w> <w n="16.8">au</w> <w n="16.9">passage</w> ?</l>
						<l n="17" num="1.17"><w n="17.1">Ce</w> <w n="17.2">sont</w> <w n="17.3">les</w> <w n="17.4">voix</w>, <w n="17.5">hélas</w> ! <w n="17.6">les</w> <w n="17.7">spectres</w> <w n="17.8">de</w> <w n="17.9">ces</w> <w n="17.10">fleurs</w></l>
						<l n="18" num="1.18"><w n="18.1">Mortes</w> <w n="18.2">par</w> <w n="18.3">toi</w>, <w n="18.4">venant</w> <w n="18.5">te</w> <w n="18.6">chanter</w> <w n="18.7">leurs</w> <w n="18.8">douleurs</w> :</l>
						<l n="19" num="1.19">— « <w n="19.1">Jeune</w> <w n="19.2">fille</w> <w n="19.3">cruelle</w> <w n="19.4">entre</w> <w n="19.5">les</w> <w n="19.6">plus</w> <w n="19.7">cruelles</w>,</l>
						<l n="20" num="1.20"><w n="20.1">Pourquoi</w> <w n="20.2">nous</w> <w n="20.3">immoler</w>, <w n="20.4">ne</w> <w n="20.5">sommes</w>-<w n="20.6">nous</w> <w n="20.7">pas</w> <w n="20.8">belles</w> ?</l>
						<l n="21" num="1.21"><w n="21.1">Sur</w> <w n="21.2">le</w> <w n="21.3">front</w> <w n="21.4">de</w> <w n="21.5">nos</w> <w n="21.6">sœurs</w> <w n="21.7">le</w> <w n="21.8">soleil</w> <w n="21.9">matinal</w></l>
						<l n="22" num="1.22"><w n="22.1">Laisse</w> <w n="22.2">encor</w> <w n="22.3">la</w> <w n="22.4">rosée</w> <w n="22.5">et</w> <w n="22.6">l</w>’<w n="22.7">éclat</w> <w n="22.8">virginal</w>.</l>
						<l n="23" num="1.23"><w n="23.1">Nous</w>-<w n="23.2">mêmes</w> <w n="23.3">nous</w> <w n="23.4">n</w>’<w n="23.5">avions</w>, <w n="23.6">sous</w> <w n="23.7">une</w> <w n="23.8">douce</w> <w n="23.9">haleine</w>,</l>
						<l n="24" num="1.24"><w n="24.1">Qu</w>’<w n="24.2">entr</w>’<w n="24.3">ouvert</w> <w n="24.4">nos</w> <w n="24.5">boutons</w> <w n="24.6">qui</w> <w n="24.7">parfument</w> <w n="24.8">la</w> <w n="24.9">plaine</w> :</l>
						<l n="25" num="1.25"><w n="25.1">Aucun</w> <w n="25.2">hôte</w> <w n="25.3">de</w> <w n="25.4">l</w>’<w n="25.5">air</w>, <w n="25.6">aucune</w> <w n="25.7">abeille</w> <w n="25.8">encor</w></l>
						<l n="26" num="1.26"><w n="26.1">Ne</w> <w n="26.2">s</w>’<w n="26.3">étaient</w> <w n="26.4">enivrés</w> <w n="26.5">à</w> <w n="26.6">nos</w> <w n="26.7">calices</w> <w n="26.8">d</w>’<w n="26.9">or</w>.</l>
						<l n="27" num="1.27"><w n="27.1">Le</w> <w n="27.2">miel</w> <w n="27.3">y</w> <w n="27.4">reposait</w>. <w n="27.5">Ce</w> <w n="27.6">fut</w> <w n="27.7">toi</w> <w n="27.8">la</w> <w n="27.9">première</w></l>
						<l n="28" num="1.28"><w n="28.1">Qui</w> <w n="28.2">vins</w>, <w n="28.3">qui</w> <w n="28.4">respiras</w> <w n="28.5">notre</w> <w n="28.6">odeur</w> <w n="28.7">printanière</w> ;</l>
						<l n="29" num="1.29"><w n="29.1">Tu</w> <w n="29.2">nous</w> <w n="29.3">cueillis</w>, <w n="29.4">et</w> <w n="29.5">nous</w> <w n="29.6">qui</w> <w n="29.7">n</w>’<w n="29.8">avions</w> <w n="29.9">pour</w> <w n="29.10">fleurir</w></l>
						<l n="30" num="1.30"><w n="30.1">Qu</w>’<w n="30.2">un</w> <w n="30.3">matin</w>, <w n="30.4">avant</w> <w n="30.5">l</w>’<w n="30.6">heure</w> <w n="30.7">il</w> <w n="30.8">nous</w> <w n="30.9">fallait</w> <w n="30.10">mourir</w>.</l>
						<l n="31" num="1.31"><w n="31.1">Encor</w> <w n="31.2">nous</w> <w n="31.3">nous</w> <w n="31.4">donnions</w> <w n="31.5">avec</w> <w n="31.6">joie</w> <w n="31.7">en</w> <w n="31.8">offrande</w></l>
						<l n="32" num="1.32"><w n="32.1">Pour</w> <w n="32.2">orner</w> <w n="32.3">tes</w> <w n="32.4">cheveux</w> <w n="32.5">d</w>’<w n="32.6">une</w> <w n="32.7">fraîche</w> <w n="32.8">guirlande</w>,</l>
						<l n="33" num="1.33"><w n="33.1">Pour</w> <w n="33.2">briller</w> <w n="33.3">sur</w> <w n="33.4">ton</w> <w n="33.5">front</w>, <w n="33.6">pour</w> <w n="33.7">embaumer</w> <w n="33.8">ton</w> <w n="33.9">sein</w> ;</l>
						<l n="34" num="1.34"><w n="34.1">Et</w> <w n="34.2">voilà</w> <w n="34.3">que</w> <w n="34.4">tu</w> <w n="34.5">vas</w>, <w n="34.6">sans</w> <w n="34.7">regret</w>, <w n="34.8">sans</w> <w n="34.9">dessein</w>,</l>
						<l n="35" num="1.35"><w n="35.1">Nous</w> <w n="35.2">semant</w> <w n="35.3">par</w> <w n="35.4">la</w> <w n="35.5">plaine</w>, <w n="35.6">où</w> <w n="35.7">le</w> <w n="35.8">vent</w>, <w n="35.9">la</w> <w n="35.10">poussière</w>,</l>
						<l n="36" num="1.36"><w n="36.1">Et</w> <w n="36.2">le</w> <w n="36.3">pied</w> <w n="36.4">du</w> <w n="36.5">passant</w>, <w n="36.6">cette</w> <w n="36.7">injure</w> <w n="36.8">dernière</w>,</l>
						<l n="37" num="1.37"><w n="37.1">Flétriront</w> <w n="37.2">sans</w> <w n="37.3">retour</w> <w n="37.4">nos</w> <w n="37.5">pétales</w> <w n="37.6">meurtris</w>,</l>
						<l n="38" num="1.38"><w n="38.1">Qui</w> <w n="38.2">jusqu</w>’<w n="38.3">au</w> <w n="38.4">soir</w> <w n="38.5">peut</w>-<w n="38.6">être</w> <w n="38.7">auraient</w> <w n="38.8">été</w> <w n="38.9">fleuris</w> !</l>
						<l n="39" num="1.39"><w n="39.1">Retourne</w>-<w n="39.2">toi</w> ! <w n="39.3">contemple</w> <w n="39.4">un</w> <w n="39.5">instant</w> <w n="39.6">nos</w> <w n="39.7">corolles</w></l>
						<l n="40" num="1.40"><w n="40.1">Rouvrant</w> <w n="40.2">pour</w> <w n="40.3">t</w>’<w n="40.4">accuser</w> <w n="40.5">leurs</w> <w n="40.6">lèvres</w> <w n="40.7">sans</w> <w n="40.8">paroles</w> ;</l>
						<l n="41" num="1.41"><w n="41.1">Respire</w> <w n="41.2">encor</w>, <w n="41.3">respire</w> <w n="41.4">un</w> <w n="41.5">seul</w> <w n="41.6">instant</w>, <w n="41.7">rien</w> <w n="41.8">qu</w>’<w n="41.9">un</w>,</l>
						<l n="42" num="1.42"><w n="42.1">Leur</w> <w n="42.2">suprême</w> <w n="42.3">soupir</w>, <w n="42.4">leur</w> <w n="42.5">suprême</w> <w n="42.6">parfum</w> ;</l>
						<l n="43" num="1.43"><w n="43.1">Donne</w> <w n="43.2">un</w> <w n="43.3">dernier</w> <w n="43.4">regret</w> <w n="43.5">aux</w> <w n="43.6">victimes</w> <w n="43.7">gisantes</w></l>
						<l n="44" num="1.44"><w n="44.1">Qui</w> <w n="44.2">sous</w> <w n="44.3">tes</w> <w n="44.4">pieds</w> <w n="44.5">mutins</w> <w n="44.6">périssent</w> <w n="44.7">innocentes</w>,</l>
						<l n="45" num="1.45"><w n="45.1">Et</w> <w n="45.2">nos</w> <w n="45.3">âmes</w> <w n="45.4">de</w> <w n="45.5">fleur</w> <w n="45.6">en</w> <w n="45.7">paix</w> <w n="45.8">s</w>’<w n="45.9">envoleront</w></l>
						<l n="46" num="1.46"><w n="46.1">Où</w> <w n="46.2">tout</w> <w n="46.3">fuit</w>, <w n="46.4">où</w> <w n="46.5">fuira</w> <w n="46.6">la</w> <w n="46.7">beauté</w> <w n="46.8">de</w> <w n="46.9">ton</w> <w n="46.10">front</w>,</l>
						<l n="47" num="1.47"><w n="47.1">Et</w> <w n="47.2">ta</w> <w n="47.3">jeunesse</w> <w n="47.4">heureuse</w>, <w n="47.5">et</w> <w n="47.6">la</w> <w n="47.7">vive</w> <w n="47.8">allégresse</w></l>
						<l n="48" num="1.48"><w n="48.1">Qui</w> <w n="48.2">brille</w> <w n="48.3">sur</w> <w n="48.4">ta</w> <w n="48.5">lèvre</w>, <w n="48.6">ô</w> <w n="48.7">folle</w> <w n="48.8">enchanteresse</w> !</l>
						<l n="49" num="1.49"><w n="49.1">Où</w> <w n="49.2">fuiront</w> <w n="49.3">tes</w> <w n="49.4">désirs</w>, <w n="49.5">tes</w> <w n="49.6">rêves</w>, <w n="49.7">ton</w> <w n="49.8">amour</w> ;</l>
						<l n="50" num="1.50"><w n="50.1">Où</w> <w n="50.2">toi</w>-<w n="50.3">même</w>… <w n="50.4">Imprudente</w> ? <w n="50.5">Ah</w> ! <w n="50.6">garde</w> <w n="50.7">qu</w>’<w n="50.8">à</w> <w n="50.9">ton</w> <w n="50.10">tour</w></l>
						<l n="51" num="1.51"><w n="51.1">Un</w> <w n="51.2">être</w> <w n="51.3">sans</w> <w n="51.4">pitié</w> <w n="51.5">comme</w> <w n="51.6">toi</w> <w n="51.7">ne</w> <w n="51.8">te</w> <w n="51.9">cueille</w>,</l>
						<l n="52" num="1.52"><w n="52.1">Et</w> <w n="52.2">jouet</w> <w n="52.3">d</w>’<w n="52.4">un</w> <w n="52.5">instant</w> <w n="52.6">sans</w> <w n="52.7">remords</w> <w n="52.8">ne</w> <w n="52.9">t</w>’<w n="52.10">effeuille</w> ! »</l>
						<l n="53" num="1.53"><w n="53.1">Or</w> <w n="53.2">l</w>’<w n="53.3">enfant</w> <w n="53.4">s</w>’<w n="53.5">en</w> <w n="53.6">allait</w>, <w n="53.7">rieuse</w>, <w n="53.8">par</w> <w n="53.9">les</w> <w n="53.10">champs</w> ;</l>
						<l n="54" num="1.54"><w n="54.1">L</w>’<w n="54.2">oreille</w> <w n="54.3">inattentive</w> <w n="54.4">aux</w> <w n="54.5">reproches</w> <w n="54.6">touchants</w>,</l>
						<l n="55" num="1.55"><w n="55.1">Elle</w> <w n="55.2">allait</w>, <w n="55.3">et</w> <w n="55.4">l</w>’<w n="55.5">air</w> <w n="55.6">pur</w>, <w n="55.7">le</w> <w n="55.8">parfum</w> <w n="55.9">des</w> <w n="55.10">campagnes</w>,</l>
						<l n="56" num="1.56"><w n="56.1">Et</w> <w n="56.2">les</w> <w n="56.3">rires</w> <w n="56.4">lointains</w> <w n="56.5">de</w> <w n="56.6">ses</w> <w n="56.7">jeunes</w> <w n="56.8">compagnes</w>,</l>
						<l n="57" num="1.57"><w n="57.1">L</w>’<w n="57.2">excitaient</w> <w n="57.3">à</w> <w n="57.4">la</w> <w n="57.5">joie</w> ; <w n="57.6">et</w> <w n="57.7">sa</w> <w n="57.8">distraite</w> <w n="57.9">main</w>,</l>
						<l n="58" num="1.58"><w n="58.1">Semant</w> <w n="58.2">toujours</w> <w n="58.3">ses</w> <w n="58.4">fleurs</w>, <w n="58.5">en</w> <w n="58.6">jonchait</w> <w n="58.7">le</w> <w n="58.8">chemin</w>…</l>
					</lg>
					<lg n="2">
						<l n="59" num="2.1"><w n="59.1">Mais</w> <w n="59.2">quand</w> <w n="59.3">elle</w> <w n="59.4">revint</w>, <w n="59.5">sur</w> <w n="59.6">le</w> <w n="59.7">soir</w>, <w n="59.8">sa</w> <w n="59.9">figure</w></l>
						<l n="60" num="2.2"><w n="60.1">Était</w> <w n="60.2">triste</w> ; <w n="60.3">ses</w> <w n="60.4">pieds</w>, <w n="60.5">dans</w> <w n="60.6">la</w> <w n="60.7">poussière</w> <w n="60.8">impure</w>,</l>
						<l n="61" num="2.3"><w n="61.1">Soulevaient</w> <w n="61.2">cent</w> <w n="61.3">débris</w> <w n="61.4">informes</w> <w n="61.5">et</w> <w n="61.6">souillés</w> ;</l>
						<l n="62" num="2.4"><w n="62.1">Et</w>, <w n="62.2">le</w> <w n="62.3">cœur</w> <w n="62.4">gros</w> <w n="62.5">de</w> <w n="62.6">pleurs</w>, <w n="62.7">les</w> <w n="62.8">yeux</w> <w n="62.9">de</w> <w n="62.10">pleurs</w> <w n="62.11">mouillés</w> :</l>
					</lg>
					<lg n="3">
						<l n="63" num="3.1">— « <w n="63.1">O</w> <w n="63.2">mes</w> <w n="63.3">fleurs</w> ! <w n="63.4">disait</w>-<w n="63.5">elle</w>, <w n="63.6">ô</w> <w n="63.7">fleurs</w>, <w n="63.8">si</w> <w n="63.9">parfumées</w></l>
						<l n="64" num="3.2"><w n="64.1">Quand</w> <w n="64.2">je</w> <w n="64.3">vous</w> <w n="64.4">effeuillais</w>, <w n="64.5">quand</w> <w n="64.6">je</w> <w n="64.7">vous</w> <w n="64.8">ai</w> <w n="64.9">semées</w>,</l>
						<l n="65" num="3.3"><w n="65.1">Ce</w> <w n="65.2">matin</w>, <w n="65.3">sur</w> <w n="65.4">la</w> <w n="65.5">route</w>, <w n="65.6">où</w> <w n="65.7">donc</w> <w n="65.8">est</w> <w n="65.9">votre</w> <w n="65.10">éclat</w> ?</l>
						<l n="66" num="3.4"><w n="66.1">La</w> <w n="66.2">poussière</w> <w n="66.3">a</w> <w n="66.4">terni</w> <w n="66.5">ce</w> <w n="66.6">contour</w> <w n="66.7">délicat</w>,</l>
						<l n="67" num="3.5"><w n="67.1">Le</w> <w n="67.2">soleil</w> <w n="67.3">a</w> <w n="67.4">séché</w> <w n="67.5">ces</w> <w n="67.6">feuilles</w> <w n="67.7">odorantes</w>,</l>
						<l n="68" num="3.6"><w n="68.1">Et</w> <w n="68.2">les</w> <w n="68.3">passants</w> <w n="68.4">oisifs</w>, <w n="68.5">et</w> <w n="68.6">les</w> <w n="68.7">chèvres</w> <w n="68.8">errantes</w>,</l>
						<l n="69" num="3.7"><w n="69.1">Ont</w> <w n="69.2">fait</w> <w n="69.3">de</w> <w n="69.4">vous</w>, <w n="69.5">hélas</w> ! <w n="69.6">un</w> <w n="69.7">objet</w> <w n="69.8">de</w> <w n="69.9">mépris</w> ;</l>
						<l n="70" num="3.8"><w n="70.1">Et</w> <w n="70.2">moi</w>-<w n="70.3">même</w>… <w n="70.4">je</w> <w n="70.5">pleure</w> <w n="70.6">en</w> <w n="70.7">foulant</w> <w n="70.8">vos</w> <w n="70.9">débris</w>.</l>
						<l n="71" num="3.9"><w n="71.1">Combien</w> <w n="71.2">un</w> <w n="71.3">seul</w> <w n="71.4">matin</w> <w n="71.5">a</w> <w n="71.6">changé</w> <w n="71.7">mes</w> <w n="71.8">pensées</w> :</l>
						<l n="72" num="3.10"><w n="72.1">Je</w> <w n="72.2">vous</w> <w n="72.3">plains</w> <w n="72.4">à</w> <w n="72.5">mon</w> <w n="72.6">tour</w>, <w n="72.7">victimes</w> <w n="72.8">dispersées</w>,</l>
						<l n="73" num="3.11"><w n="73.1">Fleurs</w> <w n="73.2">à</w> <w n="73.3">qui</w> <w n="73.4">le</w> <w n="73.5">parfum</w> <w n="73.6">ne</w> <w n="73.7">peut</w> <w n="73.8">être</w> <w n="73.9">rendu</w>.</l>
						<l n="74" num="3.12"><w n="74.1">Comme</w>, <w n="74.2">votre</w> <w n="74.3">beauté</w>, <w n="74.4">mon</w> <w n="74.5">bonheur</w> <w n="74.6">est</w> <w n="74.7">perdu</w> ! »</l>
					</lg>
				</div></body></text></TEI>