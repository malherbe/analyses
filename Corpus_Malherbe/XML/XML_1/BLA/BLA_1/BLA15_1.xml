<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="sub_2">TOME PREMIER</title>
				<title type="medium">Édition électronique</title>
				<author key="BLA">
					<name>
						<forename>Prosper</forename>
						<surname>BLANCHEMAIN</surname>
					</name>
					<date from="1816" to="1879">1816-1879</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>7196 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">BLA_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME PREMIER</title>
						<author>Prosper Blanchemain</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/pomesetposie01blan</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
								<title>TOME PREMIER</title>
								<author>Prosper Blanchemain</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE ANCIENNE ET MODERNE - ÉDOUARD ROUVEYRE</publisher>
									<date when="1880">1880</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME I</title>
						<title>POÈMES ET POÉSIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME V</title>
						<title>SONNETS ET FANTAISIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1845" to="1858">1845-1858</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">POÈMES ET POÉSIES</head><div type="poem" key="BLA15">
					<head type="main">PHIALÉ</head>
					<head type="form">IDYLLE GRECQUE</head>
					<opener>
						<salute>A L. Becq de Fouquières,</salute>
					</opener>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">O</w> <w n="1.2">Fille</w> <w n="1.3">de</w> <w n="1.4">Latone</w>, <w n="1.5">ô</w> <w n="1.6">reine</w> <w n="1.7">au</w> <w n="1.8">front</w> <w n="1.9">d</w>’<w n="1.10">argent</w> !</l>
						<l n="2" num="1.2"><w n="2.1">Blanche</w> <w n="2.2">Phœbé</w>, <w n="2.3">protège</w> <w n="2.4">un</w> <w n="2.5">berger</w> <w n="2.6">diligent</w> !</l>
						<l n="3" num="1.3"><w n="3.1">Je</w> <w n="3.2">ne</w> <w n="3.3">vais</w> <w n="3.4">point</w>, <w n="3.5">bravant</w> <w n="3.6">tes</w> <w n="3.7">nocturnes</w> <w n="3.8">mystères</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Allumer</w> <w n="4.2">les</w> <w n="4.3">flambeaux</w> <w n="4.4">des</w> <w n="4.5">amours</w> <w n="4.6">adultères</w>,</l>
						<l n="5" num="1.5"><w n="5.1">Ni</w>, <w n="5.2">conduit</w> <w n="5.3">par</w> <w n="5.4">l</w>’<w n="5.5">espoir</w> <w n="5.6">d</w>’<w n="5.7">un</w> <w n="5.8">ténébreux</w> <w n="5.9">larcin</w>,</l>
						<l n="6" num="1.6"><w n="6.1">Préparer</w> <w n="6.2">l</w>’<w n="6.3">embuscade</w> <w n="6.4">et</w> <w n="6.5">le</w> <w n="6.6">fer</w> <w n="6.7">assassin</w> ;</l>
						<l n="7" num="1.7"><w n="7.1">Je</w> <w n="7.2">vais</w> (<w n="7.3">c</w>’<w n="7.4">est</w> <w n="7.5">le</w> <w n="7.6">seul</w> <w n="7.7">but</w> <w n="7.8">qui</w>, <w n="7.9">si</w> <w n="7.10">tard</w>, <w n="7.11">me</w> <w n="7.12">soutienne</w>),</l>
						<l n="8" num="1.8"><w n="8.1">Pour</w> <w n="8.2">plaire</w> <w n="8.3">à</w> <w n="8.4">Phialé</w>, <w n="8.5">la</w> <w n="8.6">blonde</w> <w n="8.7">Athénienne</w>,</l>
						<l n="9" num="1.9"><w n="9.1">La</w> <w n="9.2">jeune</w> <w n="9.3">Phialé</w> <w n="9.4">dont</w> <w n="9.5">les</w> <w n="9.6">cheveux</w> <w n="9.7">dorés</w></l>
						<l n="10" num="1.10"><w n="10.1">Aux</w> <w n="10.2">flammes</w> <w n="10.3">de</w> <w n="10.4">ton</w> <w n="10.5">frère</w> <w n="10.6">ont</w> <w n="10.7">été</w> <w n="10.8">colorés</w> ;</l>
						<l n="11" num="1.11"><w n="11.1">Te</w> <w n="11.2">vais</w> <w n="11.3">surprendre</w> <w n="11.4">un</w> <w n="11.5">nid</w> <w n="11.6">où</w> <w n="11.7">dort</w> <w n="11.8">une</w> <w n="11.9">couvée</w>,</l>
						<l n="12" num="1.12"><w n="12.1">Qui</w> <w n="12.2">près</w> <w n="12.3">d</w>’<w n="12.4">elle</w> <w n="12.5">vivra</w> <w n="12.6">par</w> <w n="12.7">mes</w> <w n="12.8">soins</w> <w n="12.9">élevée</w> ;</l>
						<l n="13" num="1.13"><w n="13.1">Car</w> <w n="13.2">naguère</w>, <w n="13.3">passant</w> <w n="13.4">près</w> <w n="13.5">de</w> <w n="13.6">ces</w> <w n="13.7">arbrisseaux</w>,</l>
						<l n="14" num="1.14"><w n="14.1">Phialé</w> <w n="14.2">s</w>’<w n="14.3">est</w> <w n="14.4">complu</w> <w n="14.5">aux</w> <w n="14.6">chansons</w> <w n="14.7">des</w> <w n="14.8">oiseaux</w>.</l>
					</lg>
					<lg n="2">
						<l n="15" num="2.1"><w n="15.1">Toi</w>, <w n="15.2">Phœbé</w>, <w n="15.3">si</w> <w n="15.4">jadis</w>, <w n="15.5">en</w> <w n="15.6">sa</w> <w n="15.7">grotte</w> <w n="15.8">dormante</w>,</l>
						<l n="16" num="2.2"><w n="16.1">D</w>’<w n="16.2">un</w> <w n="16.3">berger</w> <w n="16.4">comme</w> <w n="16.5">moi</w> <w n="16.6">tu</w> <w n="16.7">daignas</w> <w n="16.8">être</w> <w n="16.9">amante</w>,</l>
						<l n="17" num="2.3"><w n="17.1">Si</w> <w n="17.2">tu</w> <w n="17.3">vins</w> <w n="17.4">caresser</w> <w n="17.5">de</w> <w n="17.6">ton</w> <w n="17.7">pâle</w> <w n="17.8">rayon</w></l>
						<l n="18" num="2.4"><w n="18.1">Les</w> <w n="18.2">beaux</w> <w n="18.3">yeux</w> <w n="18.4">assoupis</w> <w n="18.5">du</w> <w n="18.6">pâtre</w> <w n="18.7">Endymion</w>,</l>
						<l n="19" num="2.5"><w n="19.1">O</w> <w n="19.2">Déesse</w>, <w n="19.3">entends</w>-<w n="19.4">moi</w> <w n="19.5">du</w> <w n="19.6">haut</w> <w n="19.7">de</w> <w n="19.8">ton</w> <w n="19.9">ciel</w> <w n="19.10">vaste</w>,</l>
						<l n="20" num="2.6"><w n="20.1">Prête</w>-<w n="20.2">moi</w> <w n="20.3">tes</w> <w n="20.4">clartés</w>, <w n="20.5">car</w> <w n="20.6">mon</w> <w n="20.7">amour</w> <w n="20.8">est</w> <w n="20.9">chaste</w>,</l>
						<l n="21" num="2.7"><w n="21.1">Et</w> <w n="21.2">dans</w> <w n="21.3">mon</w> <w n="21.4">cœur</w> <w n="21.5">limpide</w> <w n="21.6">il</w> <w n="21.7">rayonne</w> <w n="21.8">aussi</w> <w n="21.9">pur</w></l>
						<l n="22" num="2.8"><w n="22.1">Que</w> <w n="22.2">ton</w> <w n="22.3">disque</w> <w n="22.4">éclatant</w> <w n="22.5">dans</w> <w n="22.6">ce</w> <w n="22.7">limpide</w> <w n="22.8">azur</w> !</l>
						<l n="23" num="2.9"><w n="23.1">Au</w> <w n="23.2">mois</w> <w n="23.3">de</w> <w n="23.4">l</w>’<w n="23.5">hécatombe</w>, <w n="23.6">en</w> <w n="23.7">nos</w> <w n="23.8">Panathénées</w>,</l>
						<l n="24" num="2.10"><w n="24.1">Quand</w> <w n="24.2">de</w> <w n="24.3">fleurs</w> <w n="24.4">et</w> <w n="24.5">de</w> <w n="24.6">fruits</w> <w n="24.7">les</w> <w n="24.8">vierges</w> <w n="24.9">couronnées</w>,</l>
						<l n="25" num="2.11"><w n="25.1">Sur</w> <w n="25.2">leurs</w> <w n="25.3">tètes</w> <w n="25.4">portant</w> <w n="25.5">le</w> <w n="25.6">miel</w> <w n="25.7">et</w> <w n="25.8">les</w> <w n="25.9">gâteaux</w>,</l>
						<l n="26" num="2.12"><w n="26.1">S</w>’<w n="26.2">assemblent</w> <w n="26.3">dès</w> <w n="26.4">l</w>’<w n="26.5">aurore</w> <w n="26.6">au</w> <w n="26.7">penchant</w> <w n="26.8">des</w> <w n="26.9">coteaux</w></l>
						<l n="27" num="2.13"><w n="27.1">Puis</w> <w n="27.2">traversant</w> <w n="27.3">la</w> <w n="27.4">ville</w> <w n="27.5">en</w> <w n="27.6">blanches</w> <w n="27.7">Théories</w>,</l>
						<l n="28" num="2.14"><w n="28.1">Vont</w> <w n="28.2">à</w> <w n="28.3">Minerve</w> <w n="28.4">offrir</w> <w n="28.5">les</w> <w n="28.6">guirlandes</w> <w n="28.7">fleuries</w></l>
						<l n="29" num="2.15"><w n="29.1">Et</w> <w n="29.2">le</w> <w n="29.3">péplum</w> <w n="29.4">d</w>’<w n="29.5">azur</w> <w n="29.6">que</w> <w n="29.7">leurs</w> <w n="29.8">mains</w> <w n="29.9">ont</w> <w n="29.10">filé</w>,</l>
						<l n="30" num="2.16"><w n="30.1">J</w>’<w n="30.2">ai</w> <w n="30.3">vu</w>, <w n="30.4">je</w> <w n="30.5">ne</w> <w n="30.6">vois</w> <w n="30.7">plus</w> <w n="30.8">dès</w> <w n="30.9">lors</w> <w n="30.10">que</w> <w n="30.11">Phialé</w>.</l>
						<l n="31" num="2.17"><w n="31.1">Phialé</w>, <w n="31.2">ton</w> <w n="31.3">doux</w> <w n="31.4">nom</w> <w n="31.5">vient</w> <w n="31.6">sans</w> <w n="31.7">cesse</w> <w n="31.8">à</w> <w n="31.9">mes</w> <w n="31.10">lèvres</w> ;</l>
						<l n="32" num="2.18"><w n="32.1">Au</w> <w n="32.2">penchant</w> <w n="32.3">de</w> <w n="32.4">l</w>’<w n="32.5">Hymette</w>, <w n="32.6">où</w> <w n="32.7">je</w> <w n="32.8">conduis</w> <w n="32.9">mes</w> <w n="32.10">chèvres</w>,</l>
						<l n="33" num="2.19"><w n="33.1">Je</w> <w n="33.2">m</w>’<w n="33.3">assois</w> <w n="33.4">et</w> <w n="33.5">je</w> <w n="33.6">cherche</w>, <w n="33.7">en</w> <w n="33.8">redisant</w> <w n="33.9">ton</w> <w n="33.10">nom</w>,</l>
						<l n="34" num="2.20"><w n="34.1">L</w>’<w n="34.2">humble</w> <w n="34.3">toit</w> <w n="34.4">de</w> <w n="34.5">ta</w> <w n="34.6">mère</w> <w n="34.7">au</w> <w n="34.8">pied</w> <w n="34.9">du</w> <w n="34.10">Parthénon</w></l>
						<l n="35" num="2.21"><w n="35.1">Je</w> <w n="35.2">néglige</w>, <w n="35.3">en</w> <w n="35.4">pensant</w> <w n="35.5">à</w> <w n="35.6">toi</w>, <w n="35.7">vierge</w> <w n="35.8">adorée</w>,</l>
						<l n="36" num="2.22"><w n="36.1">Le</w> <w n="36.2">soleil</w> <w n="36.3">qui</w> <w n="36.4">descend</w> <w n="36.5">derrière</w> <w n="36.6">le</w> <w n="36.7">Pirée</w>.</l>
						<l n="37" num="2.23"><w n="37.1">La</w> <w n="37.2">nuit</w> <w n="37.3">vient</w> ; <w n="37.4">le</w> <w n="37.5">troupeau</w> <w n="37.6">me</w> <w n="37.7">demande</w> <w n="37.8">en</w> <w n="37.9">bêlant</w></l>
						<l n="38" num="2.24"><w n="38.1">Pourquoi</w> <w n="38.2">vers</w> <w n="38.3">le</w> <w n="38.4">bercail</w> <w n="38.5">le</w> <w n="38.6">retour</w> <w n="38.7">est</w> <w n="38.8">si</w> <w n="38.9">lent</w> :</l>
						<l n="39" num="2.25"><w n="39.1">Et</w> <w n="39.2">mon</w> <w n="39.3">père</w> <w n="39.4">s</w>’<w n="39.5">écrie</w> <w n="39.6">au</w> <w n="39.7">sein</w> <w n="39.8">de</w> <w n="39.9">sa</w> <w n="39.10">demeure</w> :</l>
						<l n="40" num="2.26">« <w n="40.1">O</w> <w n="40.2">l</w>’<w n="40.3">amoureux</w> <w n="40.4">berger</w>, <w n="40.5">peu</w> <w n="40.6">soucieux</w> <w n="40.7">de</w> <w n="40.8">l</w>’<w n="40.9">heure</w> ! »</l>
					</lg>
					<lg n="3">
						<l n="41" num="3.1"><w n="41.1">C</w>’<w n="41.2">est</w> <w n="41.3">toi</w>, <w n="41.4">vierge</w> <w n="41.5">aux</w> <w n="41.6">yeux</w> <w n="41.7">noirs</w>, <w n="41.8">au</w> <w n="41.9">visage</w> <w n="41.10">vermeil</w>,</l>
						<l n="42" num="3.2"><w n="42.1">C</w>’<w n="42.2">est</w> <w n="42.3">toi</w> <w n="42.4">pour</w> <w n="42.5">qui</w> <w n="42.6">j</w>’<w n="42.7">oublie</w> <w n="42.8">et</w> <w n="42.9">l</w>’<w n="42.10">heure</w> <w n="42.11">et</w> <w n="42.12">le</w> <w n="42.13">sommeil</w> ;</l>
						<l n="43" num="3.3"><w n="43.1">Toi</w> <w n="43.2">pour</w> <w n="43.3">qui</w>, <w n="43.4">m</w>’<w n="43.5">arrachant</w> <w n="43.6">à</w> <w n="43.7">ma</w> <w n="43.8">couche</w> <w n="43.9">lointaine</w>,</l>
						<l n="44" num="3.4"><w n="44.1">Jusques</w> <w n="44.2">à</w> <w n="44.3">l</w>’<w n="44.4">Ilyssus</w> <w n="44.5">j</w>’<w n="44.6">ai</w> <w n="44.7">traversé</w> <w n="44.8">la</w> <w n="44.9">plaine</w>.</l>
						<l n="45" num="3.5"><w n="45.1">Palès</w> <w n="45.2">m</w>’<w n="45.3">a</w> <w n="45.4">laissé</w> <w n="45.5">voir</w> <w n="45.6">sous</w> <w n="45.7">ces</w> <w n="45.8">yeuses</w> <w n="45.9">verts</w></l>
						<l n="46" num="3.6"><w n="46.1">Un</w> <w n="46.2">nid</w> <w n="46.3">où</w> <w n="46.4">trois</w> <w n="46.5">oiseaux</w>, <w n="46.6">d</w>’<w n="46.7">un</w> <w n="46.8">blanc</w> <w n="46.9">duvet</w> <w n="46.10">couverts</w>,</l>
						<l n="47" num="3.7"><w n="47.1">Se</w> <w n="47.2">pressent</w> <w n="47.3">dans</w> <w n="47.4">la</w> <w n="47.5">mousse</w> <w n="47.6">et</w> <w n="47.7">ne</w> <w n="47.8">font</w> <w n="47.9">que</w> <w n="47.10">d</w>’<w n="47.11">éclore</w>.</l>
						<l n="48" num="3.8"><w n="48.1">Moi</w>, <w n="48.2">tandis</w> <w n="48.3">que</w> <w n="48.4">leur</w> <w n="48.5">plume</w> <w n="48.6">inerte</w> <w n="48.7">et</w> <w n="48.8">faible</w> <w n="48.9">encore</w></l>
						<l n="49" num="3.9"><w n="49.1">Dans</w> <w n="49.2">le</w> <w n="49.3">liquide</w> <w n="49.4">éther</w> <w n="49.5">ne</w> <w n="49.6">peut</w> <w n="49.7">les</w> <w n="49.8">appuyer</w>,</l>
						<l n="50" num="3.10"><w n="50.1">J</w>’<w n="50.2">ai</w> <w n="50.3">tressé</w> <w n="50.4">de</w> <w n="50.5">mes</w> <w n="50.6">mains</w> <w n="50.7">cette</w> <w n="50.8">cage</w> <w n="50.9">d</w>’<w n="50.10">osier</w>.</l>
						<l n="51" num="3.11"><w n="51.1">Je</w> <w n="51.2">veux</w> <w n="51.3">y</w> <w n="51.4">réunir</w> <w n="51.5">les</w> <w n="51.6">petits</w> <w n="51.7">et</w> <w n="51.8">la</w> <w n="51.9">mère</w>,</l>
						<l n="52" num="3.12"><w n="52.1">Près</w> <w n="52.2">d</w>’<w n="52.3">eux</w> <w n="52.4">elle</w> <w n="52.5">oublira</w> <w n="52.6">la</w> <w n="52.7">servitude</w> <w n="52.8">amère</w> ;</l>
						<l n="53" num="3.13"><w n="53.1">J</w>’<w n="53.2">émietterai</w> <w n="53.3">le</w> <w n="53.4">pain</w> <w n="53.5">et</w> <w n="53.6">la</w> <w n="53.7">graine</w> <w n="53.8">pour</w> <w n="53.9">eux</w>,</l>
						<l n="54" num="3.14"><w n="54.1">Puis</w>, <w n="54.2">enflant</w> <w n="54.3">mes</w> <w n="54.4">pipeaux</w>, <w n="54.5">en</w> <w n="54.6">des</w> <w n="54.7">rythmes</w> <w n="54.8">nombreux</w>,</l>
						<l n="55" num="3.15"><w n="55.1">Longtemps</w> <w n="55.2">je</w> <w n="55.3">chanterai</w>, <w n="55.4">les</w> <w n="55.5">instruisant</w> <w n="55.6">moi</w>-<w n="55.7">même</w></l>
						<l n="56" num="3.16"><w n="56.1">A</w> <w n="56.2">moduler</w> <w n="56.3">pour</w> <w n="56.4">toi</w> <w n="56.5">les</w> <w n="56.6">chants</w> <w n="56.7">que</w> <w n="56.8">ta</w> <w n="56.9">voix</w> <w n="56.10">aime</w>.</l>
					</lg>
					<closer>
						<dateline>
							<date when="1851">Juin 1851.</date>
						</dateline>
					</closer>
				</div></body></text></TEI>