<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="sub_2">TOME PREMIER</title>
				<title type="medium">Édition électronique</title>
				<author key="BLA">
					<name>
						<forename>Prosper</forename>
						<surname>BLANCHEMAIN</surname>
					</name>
					<date from="1816" to="1879">1816-1879</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>7196 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">BLA_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME PREMIER</title>
						<author>Prosper Blanchemain</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/pomesetposie01blan</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
								<title>TOME PREMIER</title>
								<author>Prosper Blanchemain</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE ANCIENNE ET MODERNE - ÉDOUARD ROUVEYRE</publisher>
									<date when="1880">1880</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME I</title>
						<title>POÈMES ET POÉSIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME V</title>
						<title>SONNETS ET FANTAISIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1845" to="1858">1845-1858</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">POÈMES ET POÉSIES</head><div type="poem" key="BLA47">
					<head type="main">LA MORT D’UN CHAT FAVORI</head>
					<opener>
						<salute>A madame Victorine Riant.</salute>
					</opener>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">C</w>’<w n="1.2">était</w> <w n="1.3">dans</w> <w n="1.4">un</w> <w n="1.5">riant</w> <w n="1.6">parterre</w> :</l>
						<l n="2" num="1.2"><w n="2.1">Les</w> <w n="2.2">roses</w>, <w n="2.3">filles</w> <w n="2.4">du</w> <w n="2.5">Printemps</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Embaumaient</w> <w n="3.2">les</w> <w n="3.3">cieux</w> <w n="3.4">éclatants</w></l>
						<l n="4" num="1.4"><w n="4.1">Des</w> <w n="4.2">plus</w> <w n="4.3">doux</w> <w n="4.4">parfums</w> <w n="4.5">de</w> <w n="4.6">la</w> <w n="4.7">terre</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Trilby</w> <w n="5.2">courait</w> <w n="5.3">par</w> <w n="5.4">le</w> <w n="5.5">jardin</w>,</l>
						<l n="6" num="2.2"><w n="6.1">Se</w> <w n="6.2">faisant</w> <w n="6.3">jeu</w> <w n="6.4">de</w> <w n="6.5">toutes</w> <w n="6.6">choses</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Des</w> <w n="7.2">herbes</w>, <w n="7.3">du</w> <w n="7.4">sable</w>, <w n="7.5">des</w> <w n="7.6">roses</w>-,</l>
						<l n="8" num="2.4"><w n="8.1">Il</w> <w n="8.2">arriva</w> <w n="8.3">près</w> <w n="8.4">du</w> <w n="8.5">bassin</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">D</w>’<w n="9.2">abord</w>, <w n="9.3">côtoyant</w> <w n="9.4">le</w> <w n="9.5">rivage</w>,</l>
						<l n="10" num="3.2"><w n="10.1">Il</w> <w n="10.2">en</w> <w n="10.3">fit</w> <w n="10.4">le</w> <w n="10.5">tour</w>, <w n="10.6">s</w>’<w n="10.7">approcha</w>,</l>
						<l n="11" num="3.3"><w n="11.1">S</w>’<w n="11.2">enfuit</w>, <w n="11.3">revint</w>, <w n="11.4">puis</w> <w n="11.5">se</w> <w n="11.6">pencha</w>.</l>
						<l n="12" num="3.4"><w n="12.1">O</w> <w n="12.2">témérité</w> <w n="12.3">du</w> <w n="12.4">jeune</w> <w n="12.5">âge</w> !</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Alors</w> ! <w n="13.2">quel</w> <w n="13.3">prodige</w> <w n="13.4">inouï</w> !</l>
						<l n="14" num="4.2"><w n="14.1">Il</w> <w n="14.2">voit</w>, <w n="14.3">chaque</w> <w n="14.4">fois</w> <w n="14.5">qu</w>’<w n="14.6">il</w> <w n="14.7">s</w>’<w n="14.8">avance</w>,</l>
						<l n="15" num="4.3"><w n="15.1">S</w>’<w n="15.2">avancer</w> <w n="15.3">de</w> <w n="15.4">même</w>, <w n="15.5">en</w> <w n="15.6">silence</w>,</l>
						<l n="16" num="4.4"><w n="16.1">Un</w> <w n="16.2">compagnon</w> <w n="16.3">semblable</w> <w n="16.4">à</w> <w n="16.5">lui</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Ce</w> <w n="17.2">sont</w> <w n="17.3">bien</w> <w n="17.4">des</w> <w n="17.5">formes</w> <w n="17.6">pareilles</w> ;</l>
						<l n="18" num="5.2"><w n="18.1">C</w>’<w n="18.2">est</w> <w n="18.3">bien</w> <w n="18.4">son</w> <w n="18.5">air</w> <w n="18.6">doux</w> <w n="18.7">et</w> <w n="18.8">hardi</w>,</l>
						<l n="19" num="5.3"><w n="19.1">Son</w> <w n="19.2">front</w> <w n="19.3">avec</w> <w n="19.4">grâce</w> <w n="19.5">arrondi</w> ;</l>
						<l n="20" num="5.4"><w n="20.1">Ce</w> <w n="20.2">sont</w> <w n="20.3">ses</w> <w n="20.4">mobiles</w> <w n="20.5">oreilles</w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">C</w>’<w n="21.2">est</w> <w n="21.3">bien</w> <w n="21.4">son</w> <w n="21.5">pelage</w> <w n="21.6">soyeux</w>,</l>
						<l n="22" num="6.2"><w n="22.1">Zébré</w> <w n="22.2">de</w> <w n="22.3">lignes</w> <w n="22.4">chatoyantes</w>,</l>
						<l n="23" num="6.3"><w n="23.1">Ses</w> <w n="23.2">longues</w> <w n="23.3">moustaches</w> <w n="23.4">brillantes</w>,</l>
						<l n="24" num="6.4"><w n="24.1">Les</w> <w n="24.2">émeraudes</w> <w n="24.3">de</w> <w n="24.4">ses</w> <w n="24.5">yeux</w>.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">Il</w> <w n="25.2">regarde</w> <w n="25.3">et</w> <w n="25.4">frémit</w> <w n="25.5">de</w> <w n="25.6">joie</w> ;</l>
						<l n="26" num="7.2"><w n="26.1">Son</w> <w n="26.2">dos</w> <w n="26.3">se</w> <w n="26.4">gonfle</w> <w n="26.5">avec</w> <w n="26.6">amour</w> ;</l>
						<l n="27" num="7.3"><w n="27.1">Sa</w> <w n="27.2">queue</w>, <w n="27.3">en</w> <w n="27.4">ondoyant</w> <w n="27.5">contour</w>,</l>
						<l n="28" num="7.4"><w n="28.1">S</w>’<w n="28.2">étend</w>, <w n="28.3">se</w> <w n="28.4">roule</w> <w n="28.5">et</w> <w n="28.6">se</w> <w n="28.7">reploie</w>.</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1"><w n="29.1">A</w> <w n="29.2">son</w> <w n="29.3">plus</w> <w n="29.4">léger</w> <w n="29.5">mouvement</w>,</l>
						<l n="30" num="8.2"><w n="30.1">La</w> <w n="30.2">vision</w> <w n="30.3">enchanteresse</w></l>
						<l n="31" num="8.3"><w n="31.1">Semble</w> <w n="31.2">lui</w> <w n="31.3">rendre</w> <w n="31.4">sa</w> <w n="31.5">caresse</w></l>
						<l n="32" num="8.4"><w n="32.1">Et</w> <w n="32.2">se</w> <w n="32.3">rapprocher</w> <w n="32.4">doucement</w>.</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1"><w n="33.1">Il</w> <w n="33.2">se</w> <w n="33.3">penche</w>, <w n="33.4">se</w> <w n="33.5">penche</w> <w n="33.6">encore</w> ;</l>
						<l n="34" num="9.2"><w n="34.1">Son</w> <w n="34.2">mauvais</w> <w n="34.3">destin</w> <w n="34.4">l</w>’<w n="34.5">a</w> <w n="34.6">poussé</w> :</l>
						<l n="35" num="9.3"><w n="35.1">Dans</w> <w n="35.2">l</w>’<w n="35.3">eau</w> <w n="35.4">trompeuse</w> <w n="35.5">il</w> <w n="35.6">a</w> <w n="35.7">glissé</w>,</l>
						<l n="36" num="9.4"><w n="36.1">Et</w> <w n="36.2">la</w> <w n="36.3">vision</w> <w n="36.4">s</w>’<w n="36.5">évapore</w> !…</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1"><w n="37.1">Vainement</w> <w n="37.2">trois</w> <w n="37.3">fois</w> <w n="37.4">sur</w> <w n="37.5">les</w> <w n="37.6">flots</w></l>
						<l n="38" num="10.2"><w n="38.1">Il</w> <w n="38.2">releva</w> <w n="38.3">sa</w> <w n="38.4">tête</w> <w n="38.5">humide</w>,</l>
						<l n="39" num="10.3"><w n="39.1">En</w> <w n="39.2">invoquant</w> <w n="39.3">la</w> <w n="39.4">Néréide</w></l>
						<l n="40" num="10.4"><w n="40.1">Qui</w> <w n="40.2">resta</w> <w n="40.3">sourde</w> <w n="40.4">à</w> <w n="40.5">ses</w> <w n="40.6">sanglots</w>.</l>
					</lg>
					<lg n="11">
						<l n="41" num="11.1"><w n="41.1">Au</w> <w n="41.2">milieu</w> <w n="41.3">de</w> <w n="41.4">l</w>’<w n="41.5">herbe</w> <w n="41.6">embaumée</w></l>
						<l n="42" num="11.2"><w n="42.1">Voyez</w> - !<w n="42.2">e</w> <w n="42.3">maintenant</w> <w n="42.4">glacé</w> ;</l>
						<l n="43" num="11.3"><w n="43.1">Les</w> <w n="43.2">ondes</w> <w n="43.3">ne</w> <w n="43.4">nous</w> <w n="43.5">ont</w> <w n="43.6">laissé</w></l>
						<l n="44" num="11.4"><w n="44.1">Que</w> <w n="44.2">sa</w> <w n="44.3">dépouille</w> <w n="44.4">inanimée</w>.</l>
					</lg>
					<lg n="12">
						<l n="45" num="12.1"><w n="45.1">Ses</w> <w n="45.2">yeux</w> <w n="45.3">ne</w> <w n="45.4">se</w> <w n="45.5">rouvriront</w> <w n="45.6">plus</w> ;</l>
						<l n="46" num="12.2"><w n="46.1">Vainement</w> <w n="46.2">sa</w> <w n="46.3">mère</w> <w n="46.4">plaintive</w></l>
						<l n="47" num="12.3"><w n="47.1">Fera</w> <w n="47.2">retentir</w> <w n="47.3">sur</w> <w n="47.4">la</w> <w n="47.5">rive</w></l>
						<l n="48" num="12.4"><w n="48.1">Ses</w> <w n="48.2">gémissements</w> <w n="48.3">superflus</w>.</l>
					</lg>
					<lg n="13">
						<l n="49" num="13.1"><w n="49.1">Adieu</w> <w n="49.2">pour</w> <w n="49.3">jamais</w> <w n="49.4">sa</w> <w n="49.5">tendresse</w></l>
						<l n="50" num="13.2"><w n="50.1">Qui</w> <w n="50.2">vous</w> <w n="50.3">amusait</w> <w n="50.4">tous</w> <w n="50.5">les</w> <w n="50.6">jours</w>,</l>
						<l n="51" num="13.3"><w n="51.1">Adieu</w> <w n="51.2">ses</w> <w n="51.3">pattes</w> <w n="51.4">de</w> <w n="51.5">velours</w></l>
						<l n="52" num="13.4"><w n="52.1">Et</w> <w n="52.2">sa</w> <w n="52.3">doucereuse</w> <w n="52.4">allégresse</w></l>
					</lg>
					<lg n="14">
						<l n="53" num="14.1"><w n="53.1">Jamais</w> <w n="53.2">plus</w> <w n="53.3">vous</w> <w n="53.4">ne</w> <w n="53.5">le</w> <w n="53.6">verrez</w>,</l>
						<l n="54" num="14.2"><w n="54.1">Étalant</w> <w n="54.2">sa</w> <w n="54.3">grâce</w> <w n="54.4">coquette</w>,</l>
						<l n="55" num="14.3"><w n="55.1">De</w> <w n="55.2">sa</w> <w n="55.3">tète</w> <w n="55.4">aujourd</w>’<w n="55.5">hui</w> <w n="55.6">muette</w></l>
						<l n="56" num="14.4"><w n="56.1">Caresser</w> <w n="56.2">vos</w> <w n="56.3">pieds</w> <w n="56.4">adorés</w>.</l>
					</lg>
					<lg n="15">
						<l n="57" num="15.1"><w n="57.1">Vos</w> <w n="57.2">légers</w> <w n="57.3">pelotons</w> <w n="57.4">de</w> <w n="57.5">soie</w></l>
						<l n="58" num="15.2"><w n="58.1">Se</w> <w n="58.2">reposeront</w> <w n="58.3">désormais</w> ;</l>
						<l n="59" num="15.3"><w n="59.1">Ils</w> <w n="59.2">ne</w> <w n="59.3">lui</w> <w n="59.4">seront</w> <w n="59.5">plus</w> <w n="59.6">jamais</w></l>
						<l n="60" num="15.4"><w n="60.1">Un</w> <w n="60.2">sujet</w> <w n="60.3">de</w> <w n="60.4">jeux</w> <w n="60.5">et</w> <w n="60.6">de</w> <w n="60.7">joie</w>.</l>
					</lg>
					<lg n="16">
						<l n="61" num="16.1"><w n="61.1">Que</w> <w n="61.2">de</w> <w n="61.3">biens</w> <w n="61.4">en</w> <w n="61.5">un</w> <w n="61.6">jour</w> <w n="61.7">perdus</w> !</l>
						<l n="62" num="16.2"><w n="62.1">Pour</w> <w n="62.2">tant</w> <w n="62.3">de</w> <w n="62.4">beauté</w>, <w n="62.5">tant</w> <w n="62.6">de</w> <w n="62.7">charmes</w>,</l>
						<l n="63" num="16.3"><w n="63.1">Madame</w>, <w n="63.2">gardez</w> <w n="63.3">quelques</w> <w n="63.4">larmes</w> ;</l>
						<l n="64" num="16.4"><w n="64.1">Votre</w> <w n="64.2">joyeux</w> <w n="64.3">Trilby</w> <w n="64.4">n</w>’<w n="64.5">est</w> <w n="64.6">plus</w> !</l>
					</lg>
				</div></body></text></TEI>