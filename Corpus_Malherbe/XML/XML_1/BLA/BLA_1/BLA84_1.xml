<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="sub_2">TOME PREMIER</title>
				<title type="medium">Édition électronique</title>
				<author key="BLA">
					<name>
						<forename>Prosper</forename>
						<surname>BLANCHEMAIN</surname>
					</name>
					<date from="1816" to="1879">1816-1879</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>7196 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">BLA_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME PREMIER</title>
						<author>Prosper Blanchemain</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/pomesetposie01blan</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
								<title>TOME PREMIER</title>
								<author>Prosper Blanchemain</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE ANCIENNE ET MODERNE - ÉDOUARD ROUVEYRE</publisher>
									<date when="1880">1880</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME I</title>
						<title>POÈMES ET POÉSIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME V</title>
						<title>SONNETS ET FANTAISIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1845" to="1858">1845-1858</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">IDÉAL</head><div type="poem" key="BLA84">
					<head type="main">LA RUINE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Ils</w> <w n="1.2">sont</w> <w n="1.3">morts</w> ; <w n="1.4">la</w> <w n="1.5">race</w> <w n="1.6">est</w> <w n="1.7">éteinte</w> ;</l>
						<l n="2" num="1.2"><w n="2.1">Le</w> <w n="2.2">manoir</w> <w n="2.3">aux</w> <w n="2.4">massives</w> <w n="2.5">tours</w></l>
						<l n="3" num="1.3"><w n="3.1">Est</w> <w n="3.2">démantelé</w> <w n="3.3">pour</w> <w n="3.4">toujours</w> ;</l>
						<l n="4" num="1.4"><w n="4.1">Les</w> <w n="4.2">corbeaux</w> <w n="4.3">y</w> <w n="4.4">volent</w> <w n="4.5">sans</w> <w n="4.6">crainte</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Sur</w> <w n="5.2">le</w> <w n="5.3">sommet</w> <w n="5.4">du</w> <w n="5.5">pic</w> <w n="5.6">maudit</w></l>
						<l n="6" num="2.2"><w n="6.1">Se</w> <w n="6.2">dresse</w> <w n="6.3">la</w> <w n="6.4">sombre</w> <w n="6.5">ruine</w> ;</l>
						<l n="7" num="2.3"><w n="7.1">Et</w> <w n="7.2">lorsque</w> <w n="7.3">le</w> <w n="7.4">soleil</w> <w n="7.5">décline</w>,</l>
						<l n="8" num="2.4"><w n="8.1">Noir</w> <w n="8.2">géant</w>, <w n="8.3">son</w> <w n="8.4">ombre</w> <w n="8.5">grandit</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Pendant</w> <w n="9.2">la</w> <w n="9.3">nuit</w>, <w n="9.4">de</w> <w n="9.5">blanches</w> <w n="9.6">ombres</w></l>
						<l n="10" num="3.2"><w n="10.1">Descendent</w> <w n="10.2">d</w>’<w n="10.3">un</w> <w n="10.4">nuage</w> <w n="10.5">en</w> <w n="10.6">pleurs</w></l>
						<l n="11" num="3.3"><w n="11.1">Pour</w> <w n="11.2">cueillir</w> <w n="11.3">l</w>’<w n="11.4">asphodèle</w> <w n="11.5">en</w> <w n="11.6">fleurs</w></l>
						<l n="12" num="3.4"><w n="12.1">Qui</w> <w n="12.2">pousse</w> <w n="12.3">entre</w> <w n="12.4">les</w> <w n="12.5">créneaux</w> <w n="12.6">sombres</w></l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Et</w> <w n="13.2">de</w> <w n="13.3">ces</w> <w n="13.4">humides</w> <w n="13.5">trésors</w></l>
						<l n="14" num="4.2"><w n="14.1">Couronnant</w> <w n="14.2">leurs</w> <w n="14.3">têtes</w> <w n="14.4">d</w>’<w n="14.5">opales</w>,</l>
						<l n="15" num="4.3"><w n="15.1">Elles</w> <w n="15.2">dansent</w> <w n="15.3">aux</w> <w n="15.4">clartés</w> <w n="15.5">pâles</w></l>
						<l n="16" num="4.4"><w n="16.1">De</w> <w n="16.2">la</w> <w n="16.3">lune</w> <w n="16.4">amante</w> <w n="16.5">des</w> <w n="16.6">morts</w>.</l>
					</lg>
				</div></body></text></TEI>