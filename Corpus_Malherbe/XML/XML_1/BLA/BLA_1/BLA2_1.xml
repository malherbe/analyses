<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="sub_2">TOME PREMIER</title>
				<title type="medium">Édition électronique</title>
				<author key="BLA">
					<name>
						<forename>Prosper</forename>
						<surname>BLANCHEMAIN</surname>
					</name>
					<date from="1816" to="1879">1816-1879</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>7196 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">BLA_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME PREMIER</title>
						<author>Prosper Blanchemain</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/pomesetposie01blan</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
								<title>TOME PREMIER</title>
								<author>Prosper Blanchemain</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE ANCIENNE ET MODERNE - ÉDOUARD ROUVEYRE</publisher>
									<date when="1880">1880</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME I</title>
						<title>POÈMES ET POÉSIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME V</title>
						<title>SONNETS ET FANTAISIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1845" to="1858">1845-1858</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">POÈMES ET POÉSIES</head><div type="poem" key="BLA2">
					<head type="main">HYMNE À LA NORMANDIE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">O</w> <w n="1.2">terre</w> ! <w n="1.3">ô</w> <w n="1.4">souvenir</w> <w n="1.5">de</w> <w n="1.6">mon</w> <w n="1.7">âme</w> <w n="1.8">ravie</w>,</l>
						<l n="2" num="1.2"><space unit="char" quantity="8"></space><w n="2.1">Toi</w> <w n="2.2">qu</w>’<w n="2.3">en</w> <w n="2.4">mes</w> <w n="2.5">songes</w> <w n="2.6">j</w>’<w n="2.7">entrevois</w> !</l>
						<l n="3" num="1.3"><w n="3.1">Terre</w>, <w n="3.2">où</w> <w n="3.3">mon</w> <w n="3.4">œil</w>, <w n="3.5">enfant</w>, <w n="3.6">aux</w> <w n="3.7">splendeurs</w> <w n="3.8">de</w> <w n="3.9">la</w> <w n="3.10">vie</w></l>
						<l n="4" num="1.4"><space unit="char" quantity="8"></space><w n="4.1">S</w>’<w n="4.2">ouvrit</w> <w n="4.3">pour</w> <w n="4.4">la</w> <w n="4.5">première</w> <w n="4.6">fois</w> !</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Terre</w>, <w n="5.2">où</w> <w n="5.3">je</w> <w n="5.4">m</w>’<w n="5.5">éveillai</w> <w n="5.6">de</w> <w n="5.7">mon</w> <w n="5.8">sommeil</w> <w n="5.9">sans</w> <w n="5.10">rêve</w>,</l>
						<l n="6" num="2.2"><space unit="char" quantity="8"></space><w n="6.1">Sur</w> <w n="6.2">les</w> <w n="6.3">rivages</w> <w n="6.4">du</w> <w n="6.5">néant</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Ainsi</w> <w n="7.2">qu</w>’<w n="7.3">un</w> <w n="7.4">naufragé</w> <w n="7.5">sur</w> <w n="7.6">la</w> <w n="7.7">lointaine</w> <w n="7.8">grève</w></l>
						<l n="8" num="2.4"><space unit="char" quantity="8"></space><w n="8.1">Où</w> <w n="8.2">l</w>’<w n="8.3">a</w> <w n="8.4">déposé</w> <w n="8.5">l</w>’<w n="8.6">Océan</w> !</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Terre</w>, <w n="9.2">où</w> <w n="9.3">souvent</w> <w n="9.4">bercé</w> <w n="9.5">d</w>’<w n="9.6">une</w> <w n="9.7">douce</w> <w n="9.8">chimère</w>,</l>
						<l n="10" num="3.2"><space unit="char" quantity="8"></space><w n="10.1">Bercé</w> <w n="10.2">sur</w> <w n="10.3">le</w> <w n="10.4">sein</w> <w n="10.5">maternel</w>,</l>
						<l n="11" num="3.3"><w n="11.1">Je</w> <w n="11.2">crus</w>, <w n="11.3">dans</w> <w n="11.4">le</w> <w n="11.5">sourire</w> <w n="11.6">et</w> <w n="11.7">les</w> <w n="11.8">yeux</w> <w n="11.9">de</w> <w n="11.10">ma</w> <w n="11.11">mère</w>,</l>
						<l n="12" num="3.4"><space unit="char" quantity="8"></space><w n="12.1">Voir</w> <w n="12.2">un</w> <w n="12.3">rayon</w> <w n="12.4">de</w> <w n="12.5">l</w>’<w n="12.6">Éternel</w> !</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Terre</w>, <w n="13.2">où</w> <w n="13.3">mon</w> <w n="13.4">cœur</w> <w n="13.5">goûta</w> <w n="13.6">le</w> <w n="13.7">miel</w> <w n="13.8">de</w> <w n="13.9">la</w> <w n="13.10">tendresse</w></l>
						<l n="14" num="4.2"><space unit="char" quantity="8"></space><w n="14.1">Et</w> <w n="14.2">l</w>’<w n="14.3">amertume</w> <w n="14.4">des</w> <w n="14.5">douleurs</w>,</l>
						<l n="15" num="4.3"><w n="15.1">Dont</w> <w n="15.2">l</w>’<w n="15.3">écho</w> <w n="15.4">répéta</w> <w n="15.5">mes</w> <w n="15.6">premiers</w> <w n="15.7">chants</w> <w n="15.8">d</w>’<w n="15.9">ivresse</w>,</l>
						<l n="16" num="4.4"><space unit="char" quantity="8"></space><w n="16.1">Dont</w> <w n="16.2">le</w> <w n="16.3">sol</w> <w n="16.4">but</w> <w n="16.5">mes</w> <w n="16.6">premiers</w> <w n="16.7">pleurs</w> !</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">O</w> <w n="17.2">patrie</w> ! <w n="17.3">en</w> <w n="17.4">allant</w> <w n="17.5">jusqu</w>’<w n="17.6">où</w> <w n="17.7">va</w> <w n="17.8">la</w> <w n="17.9">poussière</w></l>
						<l n="18" num="5.2"><space unit="char" quantity="8"></space><w n="18.1">De</w> <w n="18.2">notre</w> <w n="18.3">frêle</w> <w n="18.4">humanité</w>,</l>
						<l n="19" num="5.3"><w n="19.1">En</w> <w n="19.2">allant</w> <w n="19.3">aussi</w> <w n="19.4">loin</w> <w n="19.5">que</w> <w n="19.6">s</w>’<w n="19.7">étend</w> <w n="19.8">la</w> <w n="19.9">frontière</w></l>
						<l n="20" num="5.4"><space unit="char" quantity="8"></space><w n="20.1">Des</w> <w n="20.2">jours</w> <w n="20.3">et</w> <w n="20.4">de</w> <w n="20.5">l</w>’<w n="20.6">immensité</w>,</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">O</w> <w n="21.2">patrie</w> ! <w n="21.3">où</w> <w n="21.4">jamais</w> <w n="21.5">trouver</w> <w n="21.6">une</w> <w n="21.7">contrée</w>,</l>
						<l n="22" num="6.2"><space unit="char" quantity="8"></space><w n="22.1">Un</w> <w n="22.2">Éden</w> <w n="22.3">aussi</w> <w n="22.4">doux</w> <w n="22.5">que</w> <w n="22.6">toi</w>,</l>
						<l n="23" num="6.3"><w n="23.1">Un</w> <w n="23.2">ciel</w> <w n="23.3">pur</w>, <w n="23.4">dont</w> <w n="23.5">l</w>’<w n="23.6">aspect</w> <w n="23.7">à</w> <w n="23.8">mon</w> <w n="23.9">âme</w> <w n="23.10">enivrée</w></l>
						<l n="24" num="6.4"><space unit="char" quantity="8"></space><w n="24.1">Donne</w> <w n="24.2">autant</w> <w n="24.3">d</w>’<w n="24.4">espoir</w> <w n="24.5">et</w> <w n="24.6">de</w> <w n="24.7">foi</w> ?</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">Nulle</w> <w n="25.2">part</w> <w n="25.3">il</w> <w n="25.4">n</w>’<w n="25.5">existe</w>, <w n="25.6">au</w> <w n="25.7">penchant</w> <w n="25.8">des</w> <w n="25.9">collines</w>,</l>
						<l n="26" num="7.2"><space unit="char" quantity="8"></space><w n="26.1">Plus</w> <w n="26.2">d</w>’<w n="26.3">ombre</w> <w n="26.4">et</w> <w n="26.5">d</w>’<w n="26.6">herbe</w> <w n="26.7">pour</w> <w n="26.8">s</w>’<w n="26.9">asseoir</w>,</l>
						<l n="27" num="7.3"><w n="27.1">Plus</w> <w n="27.2">de</w> <w n="27.3">bois</w> <w n="27.4">verdoyants</w>, <w n="27.5">plus</w> <w n="27.6">de</w> <w n="27.7">fleurs</w> <w n="27.8">sans</w> <w n="27.9">épines</w>,</l>
						<l n="28" num="7.4"><space unit="char" quantity="8"></space><w n="28.1">Plus</w> <w n="28.2">de</w> <w n="28.3">parfums</w> <w n="28.4">dans</w> <w n="28.5">l</w>’<w n="28.6">air</w> <w n="28.7">du</w> <w n="28.8">soir</w>.</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1"><w n="29.1">Que</w> <w n="29.2">pour</w> <w n="29.3">d</w>’<w n="29.4">autres</w> <w n="29.5">climats</w> <w n="29.6">l</w>’<w n="29.7">hiver</w> <w n="29.8">ait</w> <w n="29.9">moins</w> <w n="29.10">de</w> <w n="29.11">glace</w></l>
						<l n="30" num="8.2"><space unit="char" quantity="8"></space><w n="30.1">Et</w> <w n="30.2">moins</w> <w n="30.3">de</w> <w n="30.4">brume</w> <w n="30.5">dans</w> <w n="30.6">les</w> <w n="30.7">cieux</w> ;</l>
						<l n="31" num="8.3"><w n="31.1">Que</w> <w n="31.2">leur</w> <w n="31.3">soleil</w> <w n="31.4">d</w>’<w n="31.5">été</w>, <w n="31.6">rayonnant</w> <w n="31.7">dans</w> <w n="31.8">l</w>’<w n="31.9">espace</w>,</l>
						<l n="32" num="8.4"><space unit="char" quantity="8"></space><w n="32.1">Ignore</w> <w n="32.2">les</w> <w n="32.3">jours</w> <w n="32.4">pluvieux</w> !</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1"><w n="33.1">A</w> <w n="33.2">toi</w> <w n="33.3">le</w> <w n="33.4">vert</w> <w n="33.5">printemps</w>, <w n="33.6">ma</w> <w n="33.7">chère</w> <w n="33.8">Normandie</w> ;</l>
						<l n="34" num="9.2"><space unit="char" quantity="8"></space><w n="34.1">A</w> <w n="34.2">toi</w> <w n="34.3">ses</w> <w n="34.4">brillantes</w> <w n="34.5">couleurs</w>,</l>
						<l n="35" num="9.3"><w n="35.1">L</w>’<w n="35.2">émail</w> <w n="35.3">de</w> <w n="35.4">ses</w> <w n="35.5">gazons</w> <w n="35.6">et</w> <w n="35.7">sa</w> <w n="35.8">brise</w> <w n="35.9">attiédie</w> ;</l>
						<l n="36" num="9.4"><space unit="char" quantity="8"></space><w n="36.1">A</w> <w n="36.2">toi</w> <w n="36.3">les</w> <w n="36.4">pommiers</w> <w n="36.5">tout</w> <w n="36.6">en</w> <w n="36.7">fleurs</w> !</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1"><w n="37.1">Laisse</w> <w n="37.2">le</w> <w n="37.3">vent</w> <w n="37.4">d</w>’<w n="37.5">été</w> <w n="37.6">semer</w> <w n="37.7">sur</w> <w n="37.8">la</w> <w n="37.9">pelouse</w></l>
						<l n="38" num="10.2"><space unit="char" quantity="8"></space><w n="38.1">Ta</w> <w n="38.2">fraîche</w> <w n="38.3">parure</w> <w n="38.4">d</w>’<w n="38.5">un</w> <w n="38.6">jour</w>,</l>
						<l n="39" num="10.3"><w n="39.1">Pareille</w> <w n="39.2">au</w> <w n="39.3">bouquet</w> <w n="39.4">blanc</w> <w n="39.5">qui</w>, <w n="39.6">du</w> <w n="39.7">front</w> <w n="39.8">de</w> <w n="39.9">l</w>’<w n="39.10">épouse</w>,</l>
						<l n="40" num="10.4"><space unit="char" quantity="8"></space><w n="40.1">Tombe</w> <w n="40.2">au</w> <w n="40.3">premier</w> <w n="40.4">baiser</w> <w n="40.5">d</w>’<w n="40.6">amour</w> ;</l>
					</lg>
					<lg n="11">
						<l n="41" num="11.1"><w n="41.1">Car</w> <w n="41.2">l</w>’<w n="41.3">automne</w> <w n="41.4">revient</w>, <w n="41.5">d</w>’<w n="41.6">une</w> <w n="41.7">moisson</w> <w n="41.8">vermeille</w>,</l>
						<l n="42" num="11.2"><space unit="char" quantity="8"></space><w n="42.1">Couronner</w> <w n="42.2">tes</w> <w n="42.3">champs</w> <w n="42.4">et</w> <w n="42.5">tes</w> <w n="42.6">prés</w> ;</l>
						<l n="43" num="11.3"><w n="43.1">Car</w> <w n="43.2">le</w> <w n="43.3">fécond</w> <w n="43.4">octobre</w> <w n="43.5">enrichit</w> <w n="43.6">ta</w> <w n="43.7">corbeille</w></l>
						<l n="44" num="11.4"><space unit="char" quantity="8"></space><w n="44.1">Des</w> <w n="44.2">fruits</w> <w n="44.3">que</w> <w n="44.4">juillet</w> <w n="44.5">a</w> <w n="44.6">dorés</w>.</l>
					</lg>
					<lg n="12">
						<l n="45" num="12.1"><w n="45.1">O</w> <w n="45.2">vallons</w> <w n="45.3">verdoyants</w> <w n="45.4">où</w> <w n="45.5">serpente</w> <w n="45.6">la</w> <w n="45.7">Seine</w>,</l>
						<l n="46" num="12.2"><space unit="char" quantity="8"></space><w n="46.1">Frais</w> <w n="46.2">coteaux</w>, <w n="46.3">fertiles</w> <w n="46.4">guérets</w> !</l>
						<l n="47" num="12.3"><w n="47.1">Puissé</w>-<w n="47.2">je</w>, <w n="47.3">ô</w> <w n="47.4">mon</w> <w n="47.5">pays</w>, <w n="47.6">fuir</w> <w n="47.7">la</w> <w n="47.8">tempête</w> <w n="47.9">humaine</w></l>
						<l n="48" num="12.4"><space unit="char" quantity="8"></space><w n="48.1">Dans</w> <w n="48.2">tes</w> <w n="48.3">champs</w> <w n="48.4">et</w> <w n="48.5">dans</w> <w n="48.6">tes</w> <w n="48.7">forêts</w> !</l>
					</lg>
					<lg n="13">
						<l n="49" num="13.1"><w n="49.1">Près</w> <w n="49.2">du</w> <w n="49.3">fleuve</w> <w n="49.4">limpide</w> <w n="49.5">assis</w> <w n="49.6">avec</w> <w n="49.7">ivresse</w>,</l>
						<l n="50" num="13.2"><space unit="char" quantity="8"></space><w n="50.1">Comme</w> <w n="50.2">autrefois</w> <w n="50.3">puissé</w>-<w n="50.4">je</w> <w n="50.5">voir</w></l>
						<l n="51" num="13.3"><w n="51.1">Chaque</w> <w n="51.2">étoile</w> <w n="51.3">du</w> <w n="51.4">ciel</w> <w n="51.5">de</w> <w n="51.6">ma</w> <w n="51.7">belle</w> <w n="51.8">jeunesse</w></l>
						<l n="52" num="13.4"><space unit="char" quantity="8"></space><w n="52.1">Se</w> <w n="52.2">refléter</w> <w n="52.3">dans</w> <w n="52.4">son</w> <w n="52.5">miroir</w> !</l>
					</lg>
					<lg n="14">
						<l n="53" num="14.1"><w n="53.1">Rêvant</w> <w n="53.2">comme</w> <w n="53.3">autrefois</w> <w n="53.4">au</w> <w n="53.5">coin</w> <w n="53.6">d</w>’<w n="53.7">un</w> <w n="53.8">bois</w> <w n="53.9">sauvage</w>,</l>
						<l n="54" num="14.2"><space unit="char" quantity="8"></space><w n="54.1">Heureux</w> <w n="54.2">de</w> <w n="54.3">mon</w> <w n="54.4">bonheur</w> <w n="54.5">passé</w>,</l>
						<l n="55" num="14.3"><w n="55.1">Puissé</w>-<w n="55.2">je</w> <w n="55.3">des</w> <w n="55.4">beaux</w> <w n="55.5">jours</w> <w n="55.6">de</w> <w n="55.7">mon</w> <w n="55.8">pèlerinage</w></l>
						<l n="56" num="14.4"><space unit="char" quantity="8"></space><w n="56.1">Revoir</w> <w n="56.2">le</w> <w n="56.3">chemin</w> <w n="56.4">effacé</w> !</l>
					</lg>
					<lg n="15">
						<l n="57" num="15.1"><w n="57.1">Et</w> <w n="57.2">là</w>, <w n="57.3">sous</w> <w n="57.4">tes</w> <w n="57.5">pommiers</w> <w n="57.6">aux</w> <w n="57.7">rameaux</w> <w n="57.8">blancs</w> <w n="57.9">et</w> <w n="57.10">roses</w>,</l>
						<l n="58" num="15.2"><space unit="char" quantity="8"></space><w n="58.1">Sur</w> <w n="58.2">tes</w> <w n="58.3">gazons</w> <w n="58.4">verts</w> <w n="58.5">que</w> <w n="58.6">j</w>’<w n="58.7">aimai</w>,</l>
						<l n="59" num="15.3"><w n="59.1">Rendre</w> <w n="59.2">mon</w> <w n="59.3">âme</w> <w n="59.4">à</w> <w n="59.5">Dieu</w>, <w n="59.6">comme</w> <w n="59.7">tes</w> <w n="59.8">fleurs</w> <w n="59.9">écloses</w>,</l>
						<l n="60" num="15.4"><space unit="char" quantity="8"></space><w n="60.1">Qu</w>’<w n="60.2">emportent</w> <w n="60.3">les</w> <w n="60.4">zéphyrs</w> <w n="60.5">de</w> <w n="60.6">mai</w> !</l>
					</lg>
				</div></body></text></TEI>