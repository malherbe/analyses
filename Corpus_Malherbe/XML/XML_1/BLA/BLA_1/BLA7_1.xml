<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="sub_2">TOME PREMIER</title>
				<title type="medium">Édition électronique</title>
				<author key="BLA">
					<name>
						<forename>Prosper</forename>
						<surname>BLANCHEMAIN</surname>
					</name>
					<date from="1816" to="1879">1816-1879</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>7196 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">BLA_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME PREMIER</title>
						<author>Prosper Blanchemain</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/pomesetposie01blan</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
								<title>TOME PREMIER</title>
								<author>Prosper Blanchemain</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE ANCIENNE ET MODERNE - ÉDOUARD ROUVEYRE</publisher>
									<date when="1880">1880</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME I</title>
						<title>POÈMES ET POÉSIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME V</title>
						<title>SONNETS ET FANTAISIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1845" to="1858">1845-1858</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">POÈMES ET POÉSIES</head><div type="poem" key="BLA7">
					<head type="main">LEVER DE SOLEIL</head>
					<opener>
						<salute>A madame Jane Hauguet.</salute>
					</opener>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">En</w> <w n="1.2">vain</w> <w n="1.3">je</w> <w n="1.4">veux</w> <w n="1.5">forcer</w> <w n="1.6">ma</w> <w n="1.7">pensée</w> <w n="1.8">à</w> <w n="1.9">se</w> <w n="1.10">taire</w> :</l>
						<l n="2" num="1.2"><w n="2.1">Sous</w> <w n="2.2">le</w> <w n="2.3">bois</w> <w n="2.4">verdoyant</w> <w n="2.5">où</w> <w n="2.6">fleurit</w> <w n="2.7">le</w> <w n="2.8">glaïeul</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Rêvant</w> <w n="3.2">a</w> <w n="3.3">l</w>’<w n="3.4">avenir</w>, <w n="3.5">je</w> <w n="3.6">marche</w> <w n="3.7">solitaire</w>,</l>
						<l n="4" num="1.4"><space unit="char" quantity="12"></space><w n="4.1">Et</w> <w n="4.2">triste</w> <w n="4.3">d</w>’<w n="4.4">être</w> <w n="4.5">seul</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">L</w>’<w n="5.2">aube</w> <w n="5.3">déploie</w> <w n="5.4">au</w> <w n="5.5">ciel</w> <w n="5.6">sa</w> <w n="5.7">radieuse</w> <w n="5.8">écharpe</w> ;</l>
						<l n="6" num="2.2"><w n="6.1">Les</w> <w n="6.2">astres</w> <w n="6.3">éclipsés</w> <w n="6.4">s</w>’<w n="6.5">en</w> <w n="6.6">vont</w> <w n="6.7">disparaissant</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Et</w> <w n="7.2">le</w> <w n="7.3">monde</w> <w n="7.4">éveillé</w> <w n="7.5">vibre</w>, <w n="7.6">comme</w> <w n="7.7">une</w> <w n="7.8">harpe</w>,</l>
						<l n="8" num="2.4"><space unit="char" quantity="12"></space><w n="8.1">Aux</w> <w n="8.2">mains</w> <w n="8.3">du</w> <w n="8.4">Tout</w>-<w n="8.5">Puissant</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Déjà</w> <w n="9.2">le</w> <w n="9.3">soleil</w> <w n="9.4">monte</w> <w n="9.5">au</w> <w n="9.6">front</w> <w n="9.7">de</w> <w n="9.8">la</w> <w n="9.9">colline</w>,</l>
						<l n="10" num="3.2"><w n="10.1">Au</w> <w n="10.2">milieu</w> <w n="10.3">des</w> <w n="10.4">splendeurs</w> <w n="10.5">d</w>’<w n="10.6">une</w> <w n="10.7">aurore</w> <w n="10.8">d</w>’<w n="10.9">été</w> ;</l>
						<l n="11" num="3.3"><w n="11.1">Il</w> <w n="11.2">perce</w> <w n="11.3">le</w> <w n="11.4">feuillage</w>, <w n="11.5">et</w> <w n="11.6">le</w> <w n="11.7">bois</w> <w n="11.8">s</w>’<w n="11.9">illumine</w></l>
						<l n="12" num="3.4"><space unit="char" quantity="12"></space><w n="12.1">D</w>’<w n="12.2">un</w> <w n="12.3">reflet</w> <w n="12.4">velouté</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Les</w> <w n="13.2">perles</w> <w n="13.3">du</w> <w n="13.4">matin</w> <w n="13.5">tremblent</w> <w n="13.6">au</w> <w n="13.7">bout</w> <w n="13.8">des</w> <w n="13.9">herbes</w>,</l>
						<l n="14" num="4.2"><w n="14.1">Et</w> <w n="14.2">l</w>’<w n="14.3">on</w> <w n="14.4">croit</w> <w n="14.5">en</w> <w n="14.6">voyant</w> <w n="14.7">les</w> <w n="14.8">champs</w> <w n="14.9">de</w> <w n="14.10">fleurs</w> <w n="14.11">couverts</w>,</l>
						<l n="15" num="4.3"><w n="15.1">Que</w>, <w n="15.2">de</w> <w n="15.3">l</w>’<w n="15.4">écrin</w> <w n="15.5">des</w> <w n="15.6">nuits</w>, <w n="15.7">les</w> <w n="15.8">étoiles</w> <w n="15.9">en</w> <w n="15.10">gerbes</w></l>
						<l n="16" num="4.4"><space unit="char" quantity="12"></space><w n="16.1">Ont</w> <w n="16.2">plu</w> <w n="16.3">sur</w> <w n="16.4">les</w> <w n="16.5">prés</w> <w n="16.6">verts</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Et</w> <w n="17.2">l</w>’<w n="17.3">oiseau</w> <w n="17.4">matinal</w>, <w n="17.5">élancé</w> <w n="17.6">dans</w> <w n="17.7">l</w>’<w n="17.8">espace</w>,</l>
						<l n="18" num="5.2"><w n="18.1">L</w>’<w n="18.2">insecte</w> <w n="18.3">bourdonnant</w> <w n="18.4">sa</w> <w n="18.5">chanson</w> <w n="18.6">du</w> <w n="18.7">matin</w>,</l>
						<l n="19" num="5.3"><w n="19.1">Le</w> <w n="19.2">papillon</w> <w n="19.3">ouvrant</w> <w n="19.4">à</w> <w n="19.5">la</w> <w n="19.6">brise</w> <w n="19.7">qui</w> <w n="19.8">passe</w></l>
						<l n="20" num="5.4"><space unit="char" quantity="12"></space><w n="20.1">Ses</w> <w n="20.2">ailes</w> <w n="20.3">de</w> <w n="20.4">satin</w>,</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Et</w> <w n="21.2">l</w>’<w n="21.3">humide</w> <w n="21.4">gazon</w> <w n="21.5">que</w> <w n="21.6">la</w> <w n="21.7">rosée</w> <w n="21.8">argente</w>.</l>
						<l n="22" num="6.2"><w n="22.1">Et</w> <w n="22.2">la</w> <w n="22.3">glèbe</w> <w n="22.4">entr</w>’<w n="22.5">ouverte</w> <w n="22.6">où</w> <w n="22.7">le</w> <w n="22.8">soc</w> <w n="22.9">resplendit</w>,</l>
						<l n="23" num="6.3"><w n="23.1">Tout</w> <w n="23.2">ce</w> <w n="23.3">qui</w> <w n="23.4">crie</w> <w n="23.5">et</w> <w n="23.6">court</w>, <w n="23.7">tout</w> <w n="23.8">ce</w> <w n="23.9">qui</w> <w n="23.10">vole</w> <w n="23.11">et</w> <w n="23.12">chante</w>,</l>
						<l n="24" num="6.4"><space unit="char" quantity="12"></space><w n="24.1">Tout</w> <w n="24.2">se</w> <w n="24.3">confond</w> <w n="24.4">et</w> <w n="24.5">dit</w> :</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1">« <w n="25.1">Béni</w> <w n="25.2">soit</w> <w n="25.3">le</w> <w n="25.4">Seigneur</w>, <w n="25.5">le</w> <w n="25.6">Dieu</w> <w n="25.7">bon</w> <w n="25.8">et</w> <w n="25.9">superbe</w>,</l>
						<l n="26" num="7.2"><w n="26.1">Le</w> <w n="26.2">Seigneur</w> <w n="26.3">des</w> <w n="26.4">moissons</w> <w n="26.5">et</w> <w n="26.6">des</w> <w n="26.7">petits</w> <w n="26.8">oiseaux</w>,</l>
						<l n="27" num="7.3"><w n="27.1">Qui</w> <w n="27.2">fit</w> <w n="27.3">l</w>’<w n="27.4">azur</w> <w n="27.5">du</w> <w n="27.6">ciel</w>, <w n="27.7">l</w>’<w n="27.8">émeraude</w> <w n="27.9">de</w> <w n="27.10">l</w>’<w n="27.11">herbe</w>,</l>
						<l n="28" num="7.4"><space unit="char" quantity="12"></space><w n="28.1">Et</w> <w n="28.2">l</w>’<w n="28.3">argent</w> <w n="28.4">des</w> <w n="28.5">ruisseau</w> ! »</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1"><w n="29.1">Pourquoi</w> <w n="29.2">donc</w>, <w n="29.3">ô</w> <w n="29.4">matin</w> ! <w n="29.5">tes</w> <w n="29.6">lueurs</w> <w n="29.7">irisées</w></l>
						<l n="30" num="8.2"><w n="30.1">Laissent</w>-<w n="30.2">elles</w> <w n="30.3">en</w> <w n="30.4">moi</w> <w n="30.5">place</w> <w n="30.6">pour</w> <w n="30.7">les</w> <w n="30.8">douleurs</w> :</l>
						<l n="31" num="8.3"><w n="31.1">O</w> <w n="31.2">nuits</w> <w n="31.3">d</w>’<w n="31.4">été</w> ! <w n="31.5">pourquoi</w>, <w n="31.6">dans</w> <w n="31.7">vos</w> <w n="31.8">fraîches</w> <w n="31.9">rosées</w>,</l>
						<l n="32" num="8.4"><space unit="char" quantity="12"></space><w n="32.1">Trouvé</w>-<w n="32.2">je</w> <w n="32.3">aussi</w> <w n="32.4">des</w> <w n="32.5">pleurs</w> ?</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1"><w n="33.1">Oh</w> ! <w n="33.2">si</w> <w n="33.3">la</w> <w n="33.4">fleur</w> <w n="33.5">des</w> <w n="33.6">champs</w> <w n="33.7">que</w> <w n="33.8">la</w> <w n="33.9">brise</w> <w n="33.10">caresse</w>,</l>
						<l n="34" num="9.2"><w n="34.1">Et</w> <w n="34.2">qui</w> <w n="34.3">répand</w> <w n="34.4">sa</w> <w n="34.5">joie</w> <w n="34.6">en</w> <w n="34.7">parfum</w> <w n="34.8">sous</w> <w n="34.9">mes</w> <w n="34.10">pas</w>,</l>
						<l n="35" num="9.3"><w n="35.1">Me</w> <w n="35.2">disait</w> <w n="35.3">le</w> <w n="35.4">secret</w> <w n="35.5">de</w> <w n="35.6">sa</w> <w n="35.7">tranquille</w> <w n="35.8">ivresse</w> !…</l>
						<l n="36" num="9.4"><space unit="char" quantity="12"></space><w n="36.1">Mais</w> <w n="36.2">la</w> <w n="36.3">fleur</w> <w n="36.4">n</w>’<w n="36.5">entend</w> <w n="36.6">pas</w>.</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1"><w n="37.1">La</w> <w n="37.2">fleur</w> <w n="37.3">dilate</w> <w n="37.4">au</w> <w n="37.5">vent</w> <w n="37.6">l</w>’<w n="37.7">or</w> <w n="37.8">de</w> <w n="37.9">son</w> <w n="37.10">étamine</w> ;</l>
						<l n="38" num="10.2"><w n="38.1">Son</w> <w n="38.2">ovaire</w> <w n="38.3">fécond</w> <w n="38.4">aspire</w> <w n="38.5">avec</w> <w n="38.6">amour</w></l>
						<l n="39" num="10.3"><w n="39.1">Le</w> <w n="39.2">pollen</w> <w n="39.3">odorant</w> <w n="39.4">que</w> <w n="39.5">sa</w> <w n="39.6">sœur</w> <w n="39.7">dissémine</w></l>
						<l n="40" num="10.4"><space unit="char" quantity="12"></space><w n="40.1">Dans</w> <w n="40.2">un</w> <w n="40.3">rayon</w> <w n="40.4">du</w> <w n="40.5">jour</w>.</l>
					</lg>
					<lg n="11">
						<l n="41" num="11.1"><w n="41.1">Si</w> <w n="41.2">le</w> <w n="41.3">grillon</w> <w n="41.4">pouvait</w>, <w n="41.5">là</w>-<w n="41.6">bas</w>, <w n="41.7">dans</w> <w n="41.8">la</w> <w n="41.9">campagne</w>,</l>
						<l n="42" num="11.2"><w n="42.1">M</w>’<w n="42.2">apprendre</w> <w n="42.3">le</w> <w n="42.4">bonheur</w> <w n="42.5">qu</w>’<w n="42.6">il</w> <w n="42.7">goûte</w> <w n="42.8">en</w> <w n="42.9">son</w> <w n="42.10">sillon</w> !</l>
						<l n="43" num="11.3"><w n="43.1">Mais</w> <w n="43.2">il</w> <w n="43.3">est</w> <w n="43.4">loin</w>, <w n="43.5">et</w> <w n="43.6">chante</w> <w n="43.7">à</w> <w n="43.8">sa</w> <w n="43.9">noire</w> <w n="43.10">compagne</w></l>
						<l n="44" num="11.4"><space unit="char" quantity="12"></space><w n="44.1">Son</w> <w n="44.2">hymne</w> <w n="44.3">de</w> <w n="44.4">grillon</w>.</l>
					</lg>
					<lg n="12">
						<l n="45" num="12.1"><w n="45.1">Si</w> <w n="45.2">l</w>’<w n="45.3">oiseau</w> <w n="45.4">qui</w> <w n="45.5">sautille</w> <w n="45.6">et</w> <w n="45.7">court</w> <w n="45.8">de</w> <w n="45.9">branche</w> <w n="45.10">en</w> <w n="45.11">branche</w>.</l>
						<l n="46" num="12.2"><w n="46.1">Et</w> <w n="46.2">se</w> <w n="46.3">montre</w> <w n="46.4">et</w> <w n="46.5">se</w> <w n="46.6">cache</w>, <w n="46.7">et</w> <w n="46.8">gazouille</w> <w n="46.9">au</w> <w n="46.10">soleil</w>,</l>
						<l n="47" num="12.3"><w n="47.1">Enseignait</w> <w n="47.2">à</w> <w n="47.3">mon</w> <w n="47.4">cœur</w> <w n="47.5">comment</w> <w n="47.6">sur</w> <w n="47.7">nous</w> <w n="47.8">s</w>’<w n="47.9">épanche</w></l>
						<l n="48" num="12.4"><space unit="char" quantity="12"></space><w n="48.1">Un</w> <w n="48.2">paisible</w> <w n="48.3">sommeil</w> !</l>
					</lg>
					<lg n="13">
						<l n="49" num="13.1"><w n="49.1">Et</w> <w n="49.2">comment</w> <w n="49.3">on</w> <w n="49.4">oublie</w>, <w n="49.5">à</w> <w n="49.6">l</w>’<w n="49.7">abri</w> <w n="49.8">de</w> <w n="49.9">la</w> <w n="49.10">feuille</w>,</l>
						<l n="50" num="13.2"><w n="50.1">Que</w> <w n="50.2">le</w> <w n="50.3">printemps</w> <w n="50.4">fut</w> <w n="50.5">court</w> <w n="50.6">et</w> <w n="50.7">que</w> <w n="50.8">l</w>’<w n="50.9">été</w> <w n="50.10">finit</w> !</l>
						<l n="51" num="13.3"><w n="51.1">Mais</w> <w n="51.2">il</w> <w n="51.3">chante</w> <w n="51.4">l</w>’<w n="51.5">amour</w> <w n="51.6">à</w> <w n="51.7">sa</w> <w n="51.8">fauvette</w>, <w n="51.9">et</w> <w n="51.10">cueille</w></l>
						<l n="52" num="13.4"><space unit="char" quantity="12"></space><w n="52.1">Des</w> <w n="52.2">herbes</w> <w n="52.3">pour</w> <w n="52.4">son</w> <w n="52.5">nid</w>.</l>
					</lg>
					<lg n="14">
						<l n="53" num="14.1"><w n="53.1">Et</w> <w n="53.2">moi</w> <w n="53.3">je</w> <w n="53.4">cherche</w> <w n="53.5">en</w> <w n="53.6">vain</w> <w n="53.7">où</w> <w n="53.8">reposer</w> <w n="53.9">mon</w> <w n="53.10">âme</w> ;</l>
						<l n="54" num="14.2"><w n="54.1">Mon</w> <w n="54.2">cœur</w> <w n="54.3">est</w> <w n="54.4">déchiré</w> <w n="54.5">par</w> <w n="54.6">d</w>’<w n="54.7">intimes</w> <w n="54.8">combats</w> ;</l>
						<l n="55" num="14.3"><w n="55.1">Devant</w> <w n="55.2">ce</w> <w n="55.3">ciel</w> <w n="55.4">si</w> <w n="55.5">pur</w> <w n="55.6">qui</w> <w n="55.7">se</w> <w n="55.8">remplit</w> <w n="55.9">de</w> <w n="55.10">flamme</w>,</l>
						<l n="56" num="14.4"><space unit="char" quantity="12"></space><w n="56.1">Je</w> <w n="56.2">murmure</w> <w n="56.3">tout</w> <w n="56.4">bas</w> :</l>
					</lg>
					<lg n="15">
						<l n="57" num="15.1">« <w n="57.1">Mon</w> <w n="57.2">Dieu</w> ! <w n="57.3">dans</w> <w n="57.4">mon</w> <w n="57.5">sentier</w> <w n="57.6">combien</w> <w n="57.7">de</w> <w n="57.8">feuilles</w> <w n="57.9">mortes</w> !</l>
						<l n="58" num="15.2"><w n="58.1">Combien</w> <w n="58.2">déjà</w> <w n="58.3">de</w> <w n="58.4">ceux</w> <w n="58.5">qui</w> <w n="58.6">m</w>’<w n="58.7">ont</w> <w n="58.8">donné</w> <w n="58.9">la</w> <w n="58.10">main</w>,</l>
						<l n="59" num="15.3"><w n="59.1">Qui</w> <w n="59.2">m</w>’<w n="59.3">ont</w> <w n="59.4">instruit</w> <w n="59.5">au</w> <w n="59.6">monde</w> <w n="59.7">et</w> <w n="59.8">m</w>’<w n="59.9">ont</w> <w n="59.10">ouvert</w> <w n="59.11">ses</w> <w n="59.12">portes</w></l>
						<l n="60" num="15.4"><space unit="char" quantity="12"></space><w n="60.1">Sont</w> <w n="60.2">restés</w> <w n="60.3">en</w> <w n="60.4">chemin</w> !</l>
					</lg>
					<lg n="16">
						<l n="61" num="16.1">« <w n="61.1">J</w>’<w n="61.2">ai</w> <w n="61.3">vu</w> <w n="61.4">leurs</w> <w n="61.5">pâles</w> <w n="61.6">fronts</w> <w n="61.7">désormais</w> <w n="61.8">taciturnes</w>,</l>
						<l n="62" num="16.2"><w n="62.1">Et</w> <w n="62.2">sous</w> <w n="62.3">l</w>’<w n="62.4">aile</w> <w n="62.5">du</w> <w n="62.6">temps</w> <w n="62.7">disparus</w> <w n="62.8">sans</w> <w n="62.9">retour</w>,</l>
						<l n="63" num="16.3"><w n="63.1">Ainsi</w> <w n="63.2">que</w> <w n="63.3">la</w> <w n="63.4">lueur</w> <w n="63.5">de</w> <w n="63.6">ces</w> <w n="63.7">astres</w> <w n="63.8">nocturnes</w>,</l>
						<l n="64" num="16.4"><space unit="char" quantity="12"></space><w n="64.1">S</w>’<w n="64.2">effacer</w> <w n="64.3">tour</w> <w n="64.4">à</w> <w n="64.5">tour</w>.</l>
					</lg>
					<lg n="17">
						<l n="65" num="17.1">« <w n="65.1">La</w> <w n="65.2">nuit</w> <w n="65.3">s</w>’<w n="65.4">en</w> <w n="65.5">va</w> <w n="65.6">saisir</w> <w n="65.7">les</w> <w n="65.8">derniers</w> <w n="65.9">dans</w> <w n="65.10">son</w> <w n="65.11">voile</w> ;</l>
						<l n="66" num="17.2"><w n="66.1">Je</w> <w n="66.2">me</w> <w n="66.3">vois</w> <w n="66.4">isolé</w> <w n="66.5">dans</w> <w n="66.6">l</w>’<w n="66.7">espace</w> <w n="66.8">éclairci</w>,</l>
						<l n="67" num="17.3"><w n="67.1">Mélancolique</w> <w n="67.2">et</w> <w n="67.3">tel</w> <w n="67.4">que</w> <w n="67.5">la</w> <w n="67.6">dernière</w> <w n="67.7">étoile</w>,</l>
						<l n="68" num="17.4"><space unit="char" quantity="12"></space><w n="68.1">Prêt</w> <w n="68.2">à</w> <w n="68.3">m</w>’<w n="68.4">éteindre</w> <w n="68.5">aussi</w> ! »</l>
					</lg>
					<lg n="18">
						<l n="69" num="18.1"><w n="69.1">C</w>’<w n="69.2">est</w> <w n="69.3">ainsi</w> <w n="69.4">qu</w>’<w n="69.5">agité</w> <w n="69.6">par</w> <w n="69.7">ma</w> <w n="69.8">pensée</w> <w n="69.9">austère</w>,</l>
						<l n="70" num="18.2"><w n="70.1">Sous</w> <w n="70.2">le</w> <w n="70.3">bois</w> <w n="70.4">verdoyant</w> <w n="70.5">où</w> <w n="70.6">fleurit</w> <w n="70.7">le</w> <w n="70.8">glaïeul</w>,</l>
						<l n="71" num="18.3"><w n="71.1">Rêvant</w> <w n="71.2">à</w> <w n="71.3">l</w>’<w n="71.4">avenir</w>, <w n="71.5">je</w> <w n="71.6">marche</w> <w n="71.7">solitaire</w>,</l>
						<l n="72" num="18.4"><space unit="char" quantity="12"></space><w n="72.1">Et</w> <w n="72.2">triste</w> <w n="72.3">d</w>’<w n="72.4">être</w> <w n="72.5">seul</w>.</l>
					</lg>
					<div n="1" type="section">
						<head type="main">ENVOI</head>
						<lg n="1">
							<l n="73" num="1.1"><w n="73.1">A</w> <w n="73.2">vous</w> <w n="73.3">ces</w> <w n="73.4">vers</w>, <w n="73.5">enfants</w> <w n="73.6">d</w>’<w n="73.7">une</w> <w n="73.8">heure</w> <w n="73.9">de</w> <w n="73.10">tristesse</w> !</l>
							<l n="74" num="1.2"><w n="74.1">Mais</w>, <w n="74.2">tout</w> <w n="74.3">empreints</w> <w n="74.4">qu</w>’<w n="74.5">ils</w> <w n="74.6">soient</w> <w n="74.7">d</w>’<w n="74.8">une</w> <w n="74.9">sombre</w> <w n="74.10">langueur</w></l>
							<l n="75" num="1.3"><w n="75.1">Ne</w> <w n="75.2">croyez</w> <w n="75.3">pourtant</w> <w n="75.4">pas</w> <w n="75.5">que</w> <w n="75.6">je</w> <w n="75.7">pleure</w> <w n="75.8">sans</w> <w n="75.9">cesse</w>,</l>
							<l n="76" num="1.4"><w n="76.1">Et</w> <w n="76.2">que</w> <w n="76.3">toute</w> <w n="76.4">espérance</w> <w n="76.5">a</w> <w n="76.6">délaissé</w> <w n="76.7">mon</w> <w n="76.8">cœur</w>.</l>
						</lg>
						<lg n="2">
							<l n="77" num="2.1"><w n="77.1">Lorsque</w> <w n="77.2">je</w> <w n="77.3">vous</w> <w n="77.4">revois</w>, <w n="77.5">dans</w> <w n="77.6">votre</w> <w n="77.7">gai</w> <w n="77.8">parterre</w>,</l>
							<l n="78" num="2.2"><w n="78.1">Au</w> <w n="78.2">milieu</w> <w n="78.3">de</w> <w n="78.4">vos</w> <w n="78.5">fleurs</w> <w n="78.6">d</w>’<w n="78.7">azur</w>, <w n="78.8">de</w> <w n="78.9">pourpre</w> <w n="78.10">et</w> <w n="78.11">d</w>’<w n="78.12">or</w>,</l>
							<l n="79" num="2.3"><w n="79.1">Je</w> <w n="79.2">crois</w> <w n="79.3">qu</w>’<w n="79.4">il</w> <w n="79.5">est</w> <w n="79.6">pour</w> <w n="79.7">nous</w> <w n="79.8">du</w> <w n="79.9">bonheur</w> <w n="79.10">sur</w> <w n="79.11">la</w> <w n="79.12">terre</w>,</l>
							<l n="80" num="2.4"><w n="80.1">Je</w> <w n="80.2">crois</w> <w n="80.3">que</w> <w n="80.4">l</w>’<w n="80.5">homme</w> <w n="80.6">est</w> <w n="80.7">bon</w>, <w n="80.8">et</w> <w n="80.9">que</w> <w n="80.10">Dieu</w> <w n="80.11">l</w>’<w n="80.12">aime</w> <w n="80.13">encor</w> ;</l>
						</lg>
						<lg n="3">
							<l n="81" num="3.1"><w n="81.1">Car</w> <w n="81.2">votre</w> <w n="81.3">cœur</w> <w n="81.4">est</w> <w n="81.5">pur</w> <w n="81.6">comme</w> <w n="81.7">le</w> <w n="81.8">frais</w> <w n="81.9">dédale</w></l>
							<l n="82" num="3.2"><w n="82.1">De</w> <w n="82.2">ces</w> <w n="82.3">fleurs</w> <w n="82.4">que</w> <w n="82.5">Dieu</w> <w n="82.6">montre</w> <w n="82.7">à</w> <w n="82.8">notre</w> <w n="82.9">œil</w> <w n="82.10">enchanté</w> ;</l>
							<l n="83" num="3.3"><w n="83.1">Et</w>, <w n="83.2">sur</w> <w n="83.3">votre</w> <w n="83.4">passage</w>, <w n="83.5">autour</w> <w n="83.6">de</w> <w n="83.7">vous</w>, <w n="83.8">s</w>’<w n="83.9">exhale</w></l>
							<l n="84" num="3.4"><w n="84.1">Un</w> <w n="84.2">parfum</w> <w n="84.3">de</w> <w n="84.4">tendresse</w> <w n="84.5">et</w> <w n="84.6">de</w> <w n="84.7">sérénité</w>.</l>
						</lg>
						<lg n="4">
							<l n="85" num="4.1"><w n="85.1">Oh</w> ! <w n="85.2">vous</w> <w n="85.3">êtes</w> <w n="85.4">si</w> <w n="85.5">bonne</w> ! <w n="85.6">oh</w> ! <w n="85.7">quelle</w> <w n="85.8">peine</w> <w n="85.9">amère</w></l>
							<l n="86" num="4.2"><w n="86.1">Ne</w> <w n="86.2">s</w>’<w n="86.3">adoucirait</w> <w n="86.4">pas</w> <w n="86.5">avec</w> <w n="86.6">votre</w> <w n="86.7">pitié</w> !</l>
							<l n="87" num="4.3"><w n="87.1">Vous</w> <w n="87.2">avez</w> <w n="87.3">le</w> <w n="87.4">sourire</w> <w n="87.5">et</w> <w n="87.6">l</w>’<w n="87.7">âme</w> <w n="87.8">de</w> <w n="87.9">ma</w> <w n="87.10">mère</w> ;</l>
							<l n="88" num="4.4"><w n="88.1">Comme</w> <w n="88.2">elle</w>, <w n="88.3">vous</w> <w n="88.4">avez</w> <w n="88.5">des</w> <w n="88.6">trésors</w> <w n="88.7">d</w>’<w n="88.8">amitié</w>.</l>
						</lg>
						<lg n="5">
							<l n="89" num="5.1"><w n="89.1">Je</w> <w n="89.2">me</w> <w n="89.3">sens</w> <w n="89.4">plus</w> <w n="89.5">heureux</w> <w n="89.6">lorsque</w> <w n="89.7">le</w> <w n="89.8">jour</w> <w n="89.9">fidèle</w></l>
							<l n="90" num="5.2"><w n="90.1">Vers</w> <w n="90.2">votre</w> <w n="90.3">toit</w> <w n="90.4">champêtre</w> <w n="90.5">a</w> <w n="90.6">ramené</w> <w n="90.7">mes</w> <w n="90.8">pas</w>,</l>
							<l n="91" num="5.3"><w n="91.1">Et</w> <w n="91.2">lorsque</w> <w n="91.3">je</w> <w n="91.4">reviens</w> <w n="91.5">demander</w> <w n="91.6">ma</w> <w n="91.7">parcelle</w></l>
							<l n="92" num="5.4"><w n="92.1">De</w> <w n="92.2">ces</w> <w n="92.3">trésors</w> <w n="92.4">du</w> <w n="92.5">cœur</w> <w n="92.6">qui</w> <w n="92.7">ne</w> <w n="92.8">s</w>’<w n="92.9">épuisent</w> <w n="92.10">pas</w> !</l>
						</lg>
					</div>
					<closer>
						<dateline>
						<placeName>Limeil</placeName>,
							<date when="1840">juillet 1840.</date>
						</dateline>
					</closer>
				</div></body></text></TEI>