<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES POËMES DE L’AMOUR ET DE LA MER</title>
				<title type="medium">Édition électronique</title>
				<author key="BCH">
					<name>
						<forename>Maurice</forename>
						<surname>Bouchor</surname>
					</name>
					<date from="1855" to="1929">1855-1929</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3472 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BCH_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
					<title>LES POËMES DE L’AMOUR ET DE LA MER</title>
					<author>Maurice Bouchor</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k9736743c</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
							<title>LES POËMES DE L’AMOUR ET DE LA MER</title>
							<author>Maurice Bouchor</author>
							<imprint>
								<pubPlace>Paris</pubPlace>
								<publisher>Charpentier et Cie, Libraires-éditeurs</publisher>
								<date when="1876">1876</date>
							</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1876">1876</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p></p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">II</head><head type="main_part">LA MORT DE L’AMOUR</head><div type="poem" key="BCH91">
					<head type="number">XXXVII</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Mon</w> <w n="1.2">âme</w> <w n="1.3">quelquefois</w> <w n="1.4">me</w> <w n="1.5">semble</w> <w n="1.6">triompher</w></l>
						<l n="2" num="1.2"><w n="2.1">Des</w> <w n="2.2">mortelles</w> <w n="2.3">langueurs</w> <w n="2.4">dont</w> <w n="2.5">elle</w> <w n="2.6">est</w> <w n="2.7">poursuivie</w> :</l>
						<l n="3" num="1.3"><w n="3.1">Il</w> <w n="3.2">me</w> <w n="3.3">revient</w> <w n="3.4">au</w> <w n="3.5">cœur</w> <w n="3.6">une</w> <w n="3.7">soif</w> <w n="3.8">de</w> <w n="3.9">la</w> <w n="3.10">vie</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Et</w> <w n="4.2">je</w> <w n="4.3">t</w>’<w n="4.4">embrasserais</w> <w n="4.5">jusqu</w>’<w n="4.6">à</w> <w n="4.7">t</w>’<w n="4.8">en</w> <w n="4.9">étouffer</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Alors</w> <w n="5.2">je</w> <w n="5.3">prends</w> <w n="5.4">ta</w> <w n="5.5">main</w> <w n="5.6">et</w> <w n="5.7">nous</w> <w n="5.8">marchons</w> <w n="5.9">ensemble</w></l>
						<l n="6" num="2.2"><w n="6.1">Comme</w> <w n="6.2">des</w> <w n="6.3">amoureux</w> <w n="6.4">au</w> <w n="6.5">premier</w> <w n="6.6">rendez</w>-<w n="6.7">vous</w> ;</l>
						<l n="7" num="2.3"><w n="7.1">Ils</w> <w n="7.2">se</w> <w n="7.3">taisent</w> <w n="7.4">longtemps</w>, <w n="7.5">puis</w>, <w n="7.6">timides</w> <w n="7.7">et</w> <w n="7.8">doux</w>,</l>
						<l n="8" num="2.4"><w n="8.1">Parlent</w> <w n="8.2">les</w> <w n="8.3">yeux</w> <w n="8.4">baissés</w> <w n="8.5">et</w> <w n="8.6">d</w>’<w n="8.7">une</w> <w n="8.8">voix</w> <w n="8.9">qui</w> <w n="8.10">tremble</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Ah</w> ! <w n="9.2">te</w> <w n="9.3">rappelles</w>-<w n="9.4">tu</w> <w n="9.5">comme</w> <w n="9.6">nous</w> <w n="9.7">nous</w> <w n="9.8">aimions</w> !</l>
						<l n="10" num="3.2"><w n="10.1">Comme</w> <w n="10.2">le</w> <w n="10.3">frôlement</w> <w n="10.4">de</w> <w n="10.5">tes</w> <w n="10.6">cheveux</w> <w n="10.7">de</w> <w n="10.8">soie</w></l>
						<l n="11" num="3.3"><w n="11.1">Me</w> <w n="11.2">faisait</w> <w n="11.3">frissonner</w> <w n="11.4">de</w> <w n="11.5">désir</w> <w n="11.6">et</w> <w n="11.7">de</w> <w n="11.8">joie</w> !</l>
						<l n="12" num="3.4"><w n="12.1">Alors</w> <w n="12.2">tu</w> <w n="12.3">t</w>’<w n="12.4">arrêtais</w> <w n="12.5">et</w> <w n="12.6">nous</w> <w n="12.7">nous</w> <w n="12.8">embrassions</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Nous</w> <w n="13.2">regardions</w> <w n="13.3">la</w> <w n="13.4">mer</w> <w n="13.5">par</w> <w n="13.6">les</w> <w n="13.7">vents</w> <w n="13.8">soulevée</w>,</l>
						<l n="14" num="4.2"><w n="14.1">Nos</w> <w n="14.2">esprits</w> <w n="14.3">voyageaient</w> <w n="14.4">bien</w> <w n="14.5">loin</w> <w n="14.6">sans</w> <w n="14.7">savoir</w> <w n="14.8">où</w>…</l>
						<l n="15" num="4.3"><w n="15.1">Je</w> <w n="15.2">ne</w> <w n="15.3">sais</w> <w n="15.4">si</w> <w n="15.5">le</w> <w n="15.6">bruit</w> <w n="15.7">des</w> <w n="15.8">flots</w> <w n="15.9">m</w>’<w n="15.10">a</w> <w n="15.11">rendu</w> <w n="15.12">fou</w>,</l>
						<l n="16" num="4.4"><w n="16.1">Si</w> <w n="16.2">je</w> <w n="16.3">suis</w> <w n="16.4">mort</w> <w n="16.5">d</w>’<w n="16.6">amour</w> <w n="16.7">ou</w> <w n="16.8">si</w> <w n="16.9">je</w> <w n="16.10">t</w>’<w n="16.11">ai</w> <w n="16.12">rêvée</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Oui</w>, <w n="17.2">ma</w> <w n="17.3">jeunesse</w> <w n="17.4">dort</w>, <w n="17.5">et</w> <w n="17.6">pour</w> <w n="17.7">l</w>’<w n="17.8">éternité</w> !</l>
						<l n="18" num="5.2"><w n="18.1">Ce</w> <w n="18.2">n</w>’<w n="18.3">est</w> <w n="18.4">pas</w> <w n="18.5">elle</w>, <w n="18.6">hélas</w> ! <w n="18.7">qui</w> <w n="18.8">frappait</w> <w n="18.9">à</w> <w n="18.10">ma</w> <w n="18.11">porte</w>,</l>
						<l n="19" num="5.3"><w n="19.1">Mon</w> <w n="19.2">cœur</w> <w n="19.3">n</w>’<w n="19.4">a</w> <w n="19.5">point</w> <w n="19.6">parlé</w>, <w n="19.7">ma</w> <w n="19.8">jeunesse</w> <w n="19.9">est</w> <w n="19.10">bien</w> <w n="19.11">morte</w>,</l>
						<l n="20" num="5.4"><w n="20.1">Et</w> <w n="20.2">ce</w> <w n="20.3">n</w>’<w n="20.4">est</w> <w n="20.5">pas</w> <w n="20.6">sa</w> <w n="20.7">voix</w> <w n="20.8">qui</w> <w n="20.9">dans</w> <w n="20.10">l</w>’<w n="20.11">air</w> <w n="20.12">a</w> <w n="20.13">chanté</w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Nous</w> <w n="21.2">marcherons</w>, <w n="21.3">muets</w>, <w n="21.4">dans</w> <w n="21.5">les</w> <w n="21.6">longues</w> <w n="21.7">allées</w></l>
						<l n="22" num="6.2"><w n="22.1">Et</w> <w n="22.2">sans</w> <w n="22.3">joindre</w> <w n="22.4">nos</w> <w n="22.5">mains</w>, <w n="22.6">nos</w> <w n="22.7">lèvres</w> <w n="22.8">et</w> <w n="22.9">nos</w> <w n="22.10">cœurs</w>, —</l>
						<l n="23" num="6.3"><w n="23.1">Car</w> <w n="23.2">mon</w> <w n="23.3">âme</w> <w n="23.4">retombe</w> <w n="23.5">en</w> <w n="23.6">ses</w> <w n="23.7">mornes</w> <w n="23.8">langueurs</w></l>
						<l n="24" num="6.4"><w n="24.1">Et</w> <w n="24.2">j</w>’<w n="24.3">écoute</w> <w n="24.4">gémir</w> <w n="24.5">les</w> <w n="24.6">vagues</w> <w n="24.7">désolées</w>.</l>
					</lg>
				</div></body></text></TEI>