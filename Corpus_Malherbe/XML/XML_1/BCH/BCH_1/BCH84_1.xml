<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES POËMES DE L’AMOUR ET DE LA MER</title>
				<title type="medium">Édition électronique</title>
				<author key="BCH">
					<name>
						<forename>Maurice</forename>
						<surname>Bouchor</surname>
					</name>
					<date from="1855" to="1929">1855-1929</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3472 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BCH_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
					<title>LES POËMES DE L’AMOUR ET DE LA MER</title>
					<author>Maurice Bouchor</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k9736743c</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
							<title>LES POËMES DE L’AMOUR ET DE LA MER</title>
							<author>Maurice Bouchor</author>
							<imprint>
								<pubPlace>Paris</pubPlace>
								<publisher>Charpentier et Cie, Libraires-éditeurs</publisher>
								<date when="1876">1876</date>
							</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1876">1876</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p></p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">II</head><head type="main_part">LA MORT DE L’AMOUR</head><div type="poem" key="BCH84">
					<head type="number">XXX</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Est</w>-<w n="1.2">ce</w> <w n="1.3">donc</w> <w n="1.4">qu</w>’<w n="1.5">il</w> <w n="1.6">est</w> <w n="1.7">vrai</w>, <w n="1.8">dans</w> <w n="1.9">cette</w> <w n="1.10">âpre</w> <w n="1.11">vallée</w></l>
						<l n="2" num="1.2"><w n="2.1">De</w> <w n="2.2">larmes</w>, <w n="2.3">de</w> <w n="2.4">sanglots</w> <w n="2.5">et</w> <w n="2.6">de</w> <w n="2.7">gémissements</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Que</w> <w n="3.2">notre</w> <w n="3.3">âme</w> <w n="3.4">ne</w> <w n="3.5">peut</w> <w n="3.6">subir</w> <w n="3.7">inébranlée</w></l>
						<l n="4" num="1.4"><w n="4.1">Ni</w> <w n="4.2">d</w>’<w n="4.3">intenses</w> <w n="4.4">plaisirs</w> <w n="4.5">ni</w> <w n="4.6">de</w> <w n="4.7">rudes</w> <w n="4.8">tourments</w> ?</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Et</w> <w n="5.2">que</w> <w n="5.3">le</w> <w n="5.4">cœur</w> <w n="5.5">humain</w> <w n="5.6">sitôt</w> <w n="5.7">se</w> <w n="5.8">rassasie</w></l>
						<l n="6" num="2.2"><w n="6.1">De</w> <w n="6.2">sa</w> <w n="6.3">propre</w> <w n="6.4">jeunesse</w> <w n="6.5">et</w> <w n="6.6">de</w> <w n="6.7">la</w> <w n="6.8">volupté</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Et</w> <w n="7.2">qu</w>’<w n="7.3">admis</w> <w n="7.4">dans</w> <w n="7.5">l</w>’<w n="7.6">Olympe</w> <w n="7.7">à</w> <w n="7.8">manger</w> <w n="7.9">l</w>’<w n="7.10">ambroisie</w>,</l>
						<l n="8" num="2.4"><w n="8.1">Il</w> <w n="8.2">lui</w> <w n="8.3">prenne</w> <w n="8.4">un</w> <w n="8.5">dégoût</w> <w n="8.6">de</w> <w n="8.7">l</w>’<w n="8.8">immortalité</w> ?</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">A</w> <w n="9.2">peine</w> <w n="9.3">il</w> <w n="9.4">a</w> <w n="9.5">saisi</w> <w n="9.6">ce</w> <w n="9.7">qu</w>’<w n="9.8">il</w> <w n="9.9">suivait</w> <w n="9.10">sans</w> <w n="9.11">trêve</w></l>
						<l n="10" num="3.2"><w n="10.1">Qu</w>’<w n="10.2">il</w> <w n="10.3">le</w> <w n="10.4">rejette</w> <w n="10.5">au</w> <w n="10.6">loin</w> <w n="10.7">avec</w> <w n="10.8">des</w> <w n="10.9">pleurs</w> <w n="10.10">d</w>’<w n="10.11">enfant</w></l>
						<l n="11" num="3.3"><w n="11.1">Et</w> <w n="11.2">dès</w> <w n="11.3">qu</w>’<w n="11.4">il</w> <w n="11.5">a</w> <w n="11.6">touché</w> <w n="11.7">le</w> <w n="11.8">papillon</w> <w n="11.9">du</w> <w n="11.10">rêve</w>,</l>
						<l n="12" num="3.4"><w n="12.1">Il</w> <w n="12.2">voit</w> <w n="12.3">s</w>’<w n="12.4">en</w> <w n="12.5">envoler</w> <w n="12.6">la</w> <w n="12.7">poussière</w> <w n="12.8">d</w>’<w n="12.9">argent</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Et</w> <w n="13.2">rien</w> <w n="13.3">ne</w> <w n="13.4">reste</w> <w n="13.5">plus</w> <w n="13.6">qu</w>’<w n="13.7">un</w> <w n="13.8">peu</w> <w n="13.9">de</w> <w n="13.10">jouissance</w>,</l>
						<l n="14" num="4.2"><w n="14.1">Cadavre</w> <w n="14.2">du</w> <w n="14.3">désir</w> <w n="14.4">mort</w> <w n="14.5">dans</w> <w n="14.6">toute</w> <w n="14.7">sa</w> <w n="14.8">fleur</w> ;</l>
						<l n="15" num="4.3"><w n="15.1">Et</w> <w n="15.2">nous</w>, <w n="15.3">muets</w> <w n="15.4">d</w>’<w n="15.5">horreur</w> <w n="15.6">devant</w> <w n="15.7">notre</w> <w n="15.8">impuissance</w>,</l>
						<l n="16" num="4.4"><w n="16.1">Nous</w> <w n="16.2">restons</w> <w n="16.3">sans</w> <w n="16.4">amour</w> <w n="16.5">et</w> <w n="16.6">la</w> <w n="16.7">mort</w> <w n="16.8">dans</w> <w n="16.9">le</w> <w n="16.10">cœur</w>.</l>
					</lg>
					<ab type="star">⁂</ab>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Nous</w> <w n="17.2">avons</w> <w n="17.3">tant</w> <w n="17.4">vécu</w> <w n="17.5">que</w> <w n="17.6">notre</w> <w n="17.7">âme</w> <w n="17.8">lassée</w></l>
						<l n="18" num="5.2"><w n="18.1">Réclame</w> <w n="18.2">le</w> <w n="18.3">silence</w> <w n="18.4">et</w> <w n="18.5">la</w> <w n="18.6">paix</w> <w n="18.7">du</w> <w n="18.8">tombeau</w> ;</l>
						<l n="19" num="5.3"><w n="19.1">Et</w> <w n="19.2">debout</w> <w n="19.3">sur</w> <w n="19.4">le</w> <w n="19.5">seuil</w> <w n="19.6">une</w> <w n="19.7">morne</w> <w n="19.8">pensée</w>,</l>
						<l n="20" num="5.4"><w n="20.1">Chantant</w> <w n="20.2">des</w> <w n="20.3">chants</w> <w n="20.4">de</w> <w n="20.5">mort</w>, <w n="20.6">renverse</w> <w n="20.7">son</w> <w n="20.8">flambeau</w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Nous</w> <w n="21.2">avons</w> <w n="21.3">épuisé</w> <w n="21.4">les</w> <w n="21.5">fraîches</w> <w n="21.6">matinées</w>,</l>
						<l n="22" num="6.2"><w n="22.1">Les</w> <w n="22.2">aromes</w>, <w n="22.3">les</w> <w n="22.4">voix</w>, <w n="22.5">les</w> <w n="22.6">souffles</w>, <w n="22.7">les</w> <w n="22.8">rayons</w> ;</l>
						<l n="23" num="6.3"><w n="23.1">Les</w> <w n="23.2">fleurs</w> <w n="23.3">dont</w> <w n="23.4">nous</w> <w n="23.5">avions</w> <w n="23.6">nos</w> <w n="23.7">têtes</w> <w n="23.8">couronnées</w></l>
						<l n="24" num="6.4"><w n="24.1">Nous</w> <w n="24.2">ont</w> <w n="24.3">empoisonnés</w> <w n="24.4">pendant</w> <w n="24.5">que</w> <w n="24.6">nous</w> <w n="24.7">dormions</w>.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">Ah</w> ! <w n="25.2">les</w> <w n="25.3">nuits</w> <w n="25.4">de</w> <w n="25.5">plaisir</w> <w n="25.6">et</w> <w n="25.7">de</w> <w n="25.8">galanterie</w>,</l>
						<l n="26" num="7.2"><w n="26.1">Les</w> <w n="26.2">guitares</w> <w n="26.3">vibrant</w> <w n="26.4">dans</w> <w n="26.5">l</w>’<w n="26.6">air</w> <w n="26.7">léger</w> <w n="26.8">du</w> <w n="26.9">soir</w>,</l>
						<l n="27" num="7.3"><w n="27.1">La</w> <w n="27.2">fenêtre</w> <w n="27.3">qui</w> <w n="27.4">s</w>’<w n="27.5">ouvre</w>, <w n="27.6">et</w> <w n="27.7">puis</w> <w n="27.8">la</w> <w n="27.9">causerie</w></l>
						<l n="28" num="7.4"><w n="28.1">Avec</w> <w n="28.2">l</w>’<w n="28.3">amant</w> <w n="28.4">debout</w> <w n="28.5">au</w> <w n="28.6">pied</w> <w n="28.7">du</w> <w n="28.8">balcon</w> <w n="28.9">noir</w> !</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1"><w n="29.1">En</w> <w n="29.2">me</w> <w n="29.3">ressouvenant</w> <w n="29.4">de</w> <w n="29.5">ces</w> <w n="29.6">nuits</w> <w n="29.7">sans</w> <w n="29.8">pareilles</w>,</l>
						<l n="30" num="8.2"><w n="30.1">Si</w> <w n="30.2">je</w> <w n="30.3">veux</w> <w n="30.4">à</w> <w n="30.5">présent</w> <w n="30.6">jouer</w> <w n="30.7">des</w> <w n="30.8">airs</w> <w n="30.9">nouveaux</w>,</l>
						<l n="31" num="8.3"><w n="31.1">Les</w> <w n="31.2">passants</w> <w n="31.3">attardés</w> <w n="31.4">se</w> <w n="31.5">bouchent</w> <w n="31.6">les</w> <w n="31.7">oreilles</w>,</l>
						<l n="32" num="8.4"><w n="32.1">Car</w> <w n="32.2">mon</w> <w n="32.3">cœur</w> — <w n="32.4">le</w> <w n="32.5">maudit</w> ! —<w n="32.6">sonne</w> <w n="32.7">horriblement</w> <w n="32.8">faux</w>.</l>
					</lg>
					<ab type="star">⁂</ab>
					<lg n="9">
						<l n="33" num="9.1"><w n="33.1">Nous</w> <w n="33.2">n</w>’<w n="33.3">avons</w> <w n="33.4">pas</w> <w n="33.5">menti</w> <w n="33.6">pourtant</w>, <w n="33.7">chère</w> <w n="33.8">infidèle</w>,</l>
						<l n="34" num="9.2"><w n="34.1">Quand</w> <w n="34.2">la</w> <w n="34.3">première</w> <w n="34.4">fois</w> <w n="34.5">nous</w> <w n="34.6">étions</w> <w n="34.7">tout</w> <w n="34.8">émus</w> !</l>
						<l n="35" num="9.3"><w n="35.1">Mais</w> <w n="35.2">c</w>’<w n="35.3">est</w> <w n="35.4">la</w> <w n="35.5">loi</w> <w n="35.6">du</w> <w n="35.7">monde</w> <w n="35.8">inflexible</w> <w n="35.9">et</w> <w n="35.10">cruelle</w> :</l>
						<l n="36" num="9.4"><w n="36.1">Nous</w> <w n="36.2">avons</w> <w n="36.3">tant</w> <w n="36.4">aimé</w>, <w n="36.5">las</w> ! <w n="36.6">que</w> <w n="36.7">nous</w> <w n="36.8">n</w>’<w n="36.9">aimons</w> <w n="36.10">plus</w> !</l>
					</lg>
				</div></body></text></TEI>