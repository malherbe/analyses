<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES POËMES DE L’AMOUR ET DE LA MER</title>
				<title type="medium">Édition électronique</title>
				<author key="BCH">
					<name>
						<forename>Maurice</forename>
						<surname>Bouchor</surname>
					</name>
					<date from="1855" to="1929">1855-1929</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3472 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BCH_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
					<title>LES POËMES DE L’AMOUR ET DE LA MER</title>
					<author>Maurice Bouchor</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k9736743c</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
							<title>LES POËMES DE L’AMOUR ET DE LA MER</title>
							<author>Maurice Bouchor</author>
							<imprint>
								<pubPlace>Paris</pubPlace>
								<publisher>Charpentier et Cie, Libraires-éditeurs</publisher>
								<date when="1876">1876</date>
							</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1876">1876</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p></p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">II</head><head type="main_part">LA MORT DE L’AMOUR</head><div type="poem" key="BCH64">
					<head type="number">XIII</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Tu</w> <w n="1.2">t</w>’<w n="1.3">en</w> <w n="1.4">venais</w> <w n="1.5">à</w> <w n="1.6">moi</w> <w n="1.7">par</w> <w n="1.8">les</w> <w n="1.9">longs</w> <w n="1.10">soirs</w> <w n="1.11">d</w>’<w n="1.12">hiver</w></l>
						<l n="2" num="1.2"><space unit="char" quantity="8"></space><w n="2.1">Et</w> <w n="2.2">tu</w> <w n="2.3">frissonnais</w> <w n="2.4">sous</w> <w n="2.5">ton</w> <w n="2.6">châle</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Et</w> <w n="3.2">nous</w> <w n="3.3">contemplions</w> <w n="3.4">la</w> <w n="3.5">lune</w> <w n="3.6">douce</w> <w n="3.7">et</w> <w n="3.8">pâle</w></l>
						<l n="4" num="1.4"><space unit="char" quantity="8"></space><w n="4.1">Qui</w> <w n="4.2">se</w> <w n="4.3">lève</w> <w n="4.4">et</w> <w n="4.5">rit</w> <w n="4.6">sur</w> <w n="4.7">la</w> <w n="4.8">mer</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Dans</w> <w n="5.2">nos</w> <w n="5.3">regards</w> <w n="5.4">profonds</w> <w n="5.5">que</w> <w n="5.6">de</w> <w n="5.7">tendresse</w> <w n="5.8">enclose</w> !</l>
						<l n="6" num="2.2"><space unit="char" quantity="8"></space><w n="6.1">Le</w> <w n="6.2">vent</w> <w n="6.3">de</w> <w n="6.4">nuit</w> <w n="6.5">nous</w> <w n="6.6">caressait</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Et</w> <w n="7.2">tes</w> <w n="7.3">lèvres</w> <w n="7.4">en</w> <w n="7.5">fleur</w> <w n="7.6">étaient</w> <w n="7.7">pour</w> <w n="7.8">lui</w>, <w n="7.9">qui</w> <w n="7.10">sait</w> ?</l>
						<l n="8" num="2.4"><space unit="char" quantity="8"></space><w n="8.1">En</w> <w n="8.2">hiver</w> <w n="8.3">une</w> <w n="8.4">étrange</w> <w n="8.5">rose</w> :</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Sur</w> <w n="9.2">mon</w> <w n="9.3">épaule</w>, <w n="9.4">alors</w>, <w n="9.5">tes</w> <w n="9.6">bras</w> <w n="9.7">tremblants</w> <w n="9.8">posés</w>,</l>
						<l n="10" num="3.2"><space unit="char" quantity="8"></space><w n="10.1">Tu</w> <w n="10.2">semblais</w> <w n="10.3">plus</w> <w n="10.4">blanche</w> <w n="10.5">et</w> <w n="10.6">plus</w> <w n="10.7">belle</w>,</l>
						<l n="11" num="3.3"><w n="11.1">Et</w> <w n="11.2">je</w> <w n="11.3">sentais</w> <w n="11.4">mon</w> <w n="11.5">cœur</w> <w n="11.6">soudain</w> <w n="11.7">battre</w> <w n="11.8">de</w> <w n="11.9">l</w>’<w n="11.10">aile</w></l>
						<l n="12" num="3.4"><space unit="char" quantity="8"></space><w n="12.1">Et</w> <w n="12.2">s</w>’<w n="12.3">envoler</w> <w n="12.4">vers</w> <w n="12.5">tes</w> <w n="12.6">baisers</w> !</l>
					</lg>
				</div></body></text></TEI>