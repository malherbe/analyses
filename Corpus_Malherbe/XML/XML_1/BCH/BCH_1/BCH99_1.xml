<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES POËMES DE L’AMOUR ET DE LA MER</title>
				<title type="medium">Édition électronique</title>
				<author key="BCH">
					<name>
						<forename>Maurice</forename>
						<surname>Bouchor</surname>
					</name>
					<date from="1855" to="1929">1855-1929</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3472 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BCH_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
					<title>LES POËMES DE L’AMOUR ET DE LA MER</title>
					<author>Maurice Bouchor</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k9736743c</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
							<title>LES POËMES DE L’AMOUR ET DE LA MER</title>
							<author>Maurice Bouchor</author>
							<imprint>
								<pubPlace>Paris</pubPlace>
								<publisher>Charpentier et Cie, Libraires-éditeurs</publisher>
								<date when="1876">1876</date>
							</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1876">1876</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p></p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">III</head><head type="main_part">L’AMOUR DIVIN</head><div type="poem" key="BCH99">
					<head type="number">I</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Qu</w>’<w n="1.2">ai</w>-<w n="1.3">je</w> <w n="1.4">donc</w> ? <w n="1.5">le</w> <w n="1.6">printemps</w> <w n="1.7">me</w> <w n="1.8">trouble</w> <w n="1.9">et</w> <w n="1.10">me</w> <w n="1.11">soulève</w>.</l>
						<l n="2" num="1.2"><w n="2.1">Comme</w> <w n="2.2">Mercure</w>, <w n="2.3">j</w>’<w n="2.4">ai</w> <w n="2.5">des</w> <w n="2.6">ailes</w> <w n="2.7">aux</w> <w n="2.8">talons</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Et</w> <w n="3.2">pour</w> <w n="3.3">m</w>’<w n="3.4">épanouir</w> <w n="3.5">dans</w> <w n="3.6">le</w> <w n="3.7">ciel</w> <w n="3.8">bleu</w> <w n="3.9">du</w> <w n="3.10">rêve</w></l>
						<l n="4" num="1.4"><w n="4.1">J</w>’<w n="4.2">ai</w> <w n="4.3">la</w> <w n="4.4">légèreté</w> <w n="4.5">des</w> <w n="4.6">divins</w> <w n="4.7">papillons</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Dieu</w>, <w n="5.2">comme</w> <w n="5.3">mon</w> <w n="5.4">cœur</w> <w n="5.5">bat</w> ! <w n="5.6">Pourtant</w>, <w n="5.7">dans</w> <w n="5.8">les</w> <w n="5.9">ténèbres</w></l>
						<l n="6" num="2.2"><w n="6.1">S</w>’<w n="6.2">est</w> <w n="6.3">endormi</w> <w n="6.4">l</w>’<w n="6.5">amour</w> <w n="6.6">de</w> <w n="6.7">ma</w> <w n="6.8">jeunesse</w> <w n="6.9">en</w> <w n="6.10">fleur</w> ;</l>
						<l n="7" num="2.3"><w n="7.1">Je</w> <w n="7.2">l</w>’<w n="7.3">ai</w> <w n="7.4">mis</w> <w n="7.5">dans</w> <w n="7.6">la</w> <w n="7.7">tombe</w> <w n="7.8">avec</w> <w n="7.9">des</w> <w n="7.10">chants</w> <w n="7.11">funèbres</w>,</l>
						<l n="8" num="2.4"><w n="8.1">Et</w> <w n="8.2">voici</w> <w n="8.3">que</w> <w n="8.4">je</w> <w n="8.5">sens</w> <w n="8.6">se</w> <w n="8.7">réveiller</w> <w n="8.8">mon</w> <w n="8.9">cœur</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Je</w> <w n="9.2">n</w>’<w n="9.3">aime</w> <w n="9.4">pas</w>, <w n="9.5">pourtant</w>. <w n="9.6">Mais</w> <w n="9.7">le</w> <w n="9.8">ciel</w> <w n="9.9">est</w> <w n="9.10">en</w> <w n="9.11">joie</w>,</l>
						<l n="10" num="3.2"><w n="10.1">Mais</w> <w n="10.2">les</w> <w n="10.3">bois</w> <w n="10.4">ont</w> <w n="10.5">verdi</w> <w n="10.6">pendant</w> <w n="10.7">que</w> <w n="10.8">j</w>’<w n="10.9">ai</w> <w n="10.10">pleuré</w>,</l>
						<l n="11" num="3.3"><w n="11.1">Et</w> <w n="11.2">la</w> <w n="11.3">brise</w> <w n="11.4">de</w> <w n="11.5">Mai</w> <w n="11.6">sous</w> <w n="11.7">qui</w> <w n="11.8">le</w> <w n="11.9">gazon</w> <w n="11.10">ploie</w></l>
						<l n="12" num="3.4"><w n="12.1">Va</w> <w n="12.2">butiner</w> <w n="12.3">l</w>’<w n="12.4">encens</w> <w n="12.5">chez</w> <w n="12.6">les</w> <w n="12.7">roses</w> <w n="12.8">du</w> <w n="12.9">pré</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Et</w> <w n="13.2">sur</w> <w n="13.3">le</w> <w n="13.4">bord</w> <w n="13.5">des</w> <w n="13.6">mers</w>, <w n="13.7">le</w> <w n="13.8">long</w> <w n="13.9">du</w> <w n="13.10">sable</w> <w n="13.11">lisse</w>,</l>
						<l n="14" num="4.2"><w n="14.1">En</w> <w n="14.2">chantant</w> <w n="14.3">un</w> <w n="14.4">couplet</w> <w n="14.5">d</w>’<w n="14.6">amour</w> <w n="14.7">et</w> <w n="14.8">de</w> <w n="14.9">printemps</w>,</l>
						<l n="15" num="4.3"><w n="15.1">Comme</w> <w n="15.2">un</w> <w n="15.3">sylphe</w>, <w n="15.4">marchant</w> <w n="15.5">bien</w> <w n="15.6">moins</w> <w n="15.7">qu</w>’<w n="15.8">elle</w> <w n="15.9">ne</w> <w n="15.10">glisse</w>,</l>
						<l n="16" num="4.4"><w n="16.1">Passe</w> <w n="16.2">une</w> <w n="16.3">belle</w> <w n="16.4">fille</w> <w n="16.5">et</w> <w n="16.6">qui</w> <w n="16.7">n</w>’<w n="16.8">a</w> <w n="16.9">pas</w> <w n="16.10">vingt</w> <w n="16.11">ans</w>.</l>
					</lg>
					<ab type="star">⁂</ab>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Ce</w> <w n="17.2">n</w>’<w n="17.3">est</w> <w n="17.4">pas</w> <w n="17.5">toi</w> <w n="17.6">que</w> <w n="17.7">j</w>’<w n="17.8">aime</w>, <w n="17.9">ô</w> <w n="17.10">belle</w> <w n="17.11">jeune</w> <w n="17.12">fille</w></l>
						<l n="18" num="5.2"><w n="18.1">En</w> <w n="18.2">qui</w> <w n="18.3">palpite</w> <w n="18.4">et</w> <w n="18.5">vit</w> <w n="18.6">la</w> <w n="18.7">jeunesse</w> <w n="18.8">des</w> <w n="18.9">fleurs</w> ;</l>
						<l n="19" num="5.3"><w n="19.1">Et</w> <w n="19.2">pourtant</w>, <w n="19.3">sais</w>-<w n="19.4">tu</w> <w n="19.5">bien</w> ? <w n="19.6">ton</w> <w n="19.7">œil</w> <w n="19.8">superbe</w> <w n="19.9">brille</w>,</l>
						<l n="20" num="5.4"><w n="20.1">Et</w> <w n="20.2">je</w> <w n="20.3">pourrais</w> <w n="20.4">t</w>’<w n="20.5">aimer</w> <w n="20.6">dans</w> <w n="20.7">le</w> <w n="20.8">rire</w> <w n="20.9">ou</w> <w n="20.10">les</w> <w n="20.11">pleurs</w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Ce</w> <w n="21.2">n</w>’<w n="21.3">est</w> <w n="21.4">pas</w> <w n="21.5">toi</w> <w n="21.6">que</w> <w n="21.7">j</w>’<w n="21.8">aime</w>, <w n="21.9">et</w> <w n="21.10">cependant</w> <w n="21.11">je</w> <w n="21.12">t</w>’<w n="21.13">aime</w>,</l>
						<l n="22" num="6.2"><w n="22.1">Mon</w> <w n="22.2">cœur</w> <w n="22.3">te</w> <w n="22.4">trouve</w> <w n="22.5">belle</w> <w n="22.6">et</w> <w n="22.7">te</w> <w n="22.8">salue</w>, <w n="22.9">ô</w> <w n="22.10">toi</w></l>
						<l n="23" num="6.3"><w n="23.1">Dont</w> <w n="23.2">les</w> <w n="23.3">cheveux</w> <w n="23.4">tressés</w> <w n="23.5">forment</w> <w n="23.6">un</w> <w n="23.7">diadème</w></l>
						<l n="24" num="6.4"><w n="24.1">Près</w> <w n="24.2">de</w> <w n="24.3">qui</w> <w n="24.4">pâlirait</w> <w n="24.5">la</w> <w n="24.6">couronne</w> <w n="24.7">d</w>’<w n="24.8">un</w> <w n="24.9">roi</w>.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">Vas</w> <w n="25.2">en</w> <w n="25.3">paix</w>, <w n="25.4">car</w> <w n="25.5">mon</w> <w n="25.6">cœur</w> <w n="25.7">est</w> <w n="25.8">mort</w> <w n="25.9">dans</w> <w n="25.10">ma</w> <w n="25.11">poitrine</w>,</l>
						<l n="26" num="7.2"><w n="26.1">Car</w> <w n="26.2">le</w> <w n="26.3">souffle</w> <w n="26.4">des</w> <w n="26.5">vents</w> <w n="26.6">est</w> <w n="26.7">plus</w> <w n="26.8">doux</w> <w n="26.9">que</w> <w n="26.10">ta</w> <w n="26.11">voix</w>,</l>
						<l n="27" num="7.3"><w n="27.1">Et</w> <w n="27.2">quand</w> <w n="27.3">la</w> <w n="27.4">passion</w> <w n="27.5">gonflera</w> <w n="27.6">ta</w> <w n="27.7">narine</w>,</l>
						<l n="28" num="7.4"><w n="28.1">Je</w> <w n="28.2">me</w> <w n="28.3">réfugierai</w> <w n="28.4">dans</w> <w n="28.5">le</w> <w n="28.6">calme</w> <w n="28.7">des</w> <w n="28.8">bois</w>.</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1"><w n="29.1">J</w>’<w n="29.2">écouterai</w> <w n="29.3">chanter</w> <w n="29.4">les</w> <w n="29.5">vagues</w> <w n="29.6">immortelles</w></l>
						<l n="30" num="8.2"><w n="30.1">Qui</w> <w n="30.2">roulent</w> <w n="30.3">au</w> <w n="30.4">soleil</w> <w n="30.5">en</w> <w n="30.6">lançant</w> <w n="30.7">des</w> <w n="30.8">éclairs</w>,</l>
						<l n="31" num="8.3"><w n="31.1">Et</w> <w n="31.2">quand</w> <w n="31.3">j</w>’<w n="31.4">évoquerai</w> <w n="31.5">mes</w> <w n="31.6">amours</w> <w n="31.7">infidèles</w></l>
						<l n="32" num="8.4"><w n="32.1">Un</w> <w n="32.2">rire</w> <w n="32.3">éblouissant</w> <w n="32.4">traversera</w> <w n="32.5">les</w> <w n="32.6">airs</w>.</l>
					</lg>
					<ab type="star">⁂</ab>
					<lg n="9">
						<l n="33" num="9.1"><w n="33.1">O</w> <w n="33.2">vous</w> <w n="33.3">que</w> <w n="33.4">nous</w> <w n="33.5">prenions</w> <w n="33.6">pour</w> <w n="33.7">confidents</w> <w n="33.8">naguère</w>,</l>
						<l n="34" num="9.2"><w n="34.1">Splendide</w> <w n="34.2">ciel</w> <w n="34.3">d</w>’<w n="34.4">azur</w>, <w n="34.5">astres</w> <w n="34.6">de</w> <w n="34.7">diamant</w>,</l>
						<l n="35" num="9.3"><w n="35.1">Océan</w> <w n="35.2">dont</w> <w n="35.3">les</w> <w n="35.4">flots</w> <w n="35.5">vibraient</w> <w n="35.6">dans</w> <w n="35.7">la</w> <w n="35.8">lumière</w></l>
						<l n="36" num="9.4"><w n="36.1">Et</w> <w n="36.2">voilait</w> <w n="36.3">nos</w> <w n="36.4">aveux</w> <w n="36.5">d</w>’<w n="36.6">un</w> <w n="36.7">murmure</w> <w n="36.8">charmant</w> ;</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1"><w n="37.1">Certes</w>, <w n="37.2">vous</w> <w n="37.3">saviez</w> <w n="37.4">bien</w> <w n="37.5">qu</w>’<w n="37.6">insoucieuse</w> <w n="37.7">et</w> <w n="37.8">folle</w></l>
						<l n="38" num="10.2"><w n="38.1">S</w>’<w n="38.2">envolerait</w> <w n="38.3">l</w>’<w n="38.4">ardeur</w> <w n="38.5">des</w> <w n="38.6">premières</w> <w n="38.7">amours</w> ;</l>
						<l n="39" num="10.3"><w n="39.1">Et</w> <w n="39.2">tout</w> <w n="39.3">alors</w> <w n="39.4">semblait</w> <w n="39.5">croire</w> <w n="39.6">à</w> <w n="39.7">notre</w> <w n="39.8">parole</w></l>
						<l n="40" num="10.4"><w n="40.1">Qu</w>’<w n="40.2">un</w> <w n="40.3">souffle</w> <w n="40.4">cependant</w> <w n="40.5">emportait</w> <w n="40.6">pour</w> <w n="40.7">toujours</w>.</l>
					</lg>
					<lg n="11">
						<l n="41" num="11.1"><w n="41.1">Vous</w> <w n="41.2">nous</w> <w n="41.3">laissiez</w> <w n="41.4">l</w>’<w n="41.5">erreur</w> <w n="41.6">bien</w> <w n="41.7">aimée</w> <w n="41.8">et</w> <w n="41.9">bénie</w>,</l>
						<l n="42" num="11.2"><w n="42.1">Quand</w> <w n="42.2">nous</w> <w n="42.3">nous</w> <w n="42.4">parjurions</w>, <w n="42.5">vous</w> <w n="42.6">mentiez</w> <w n="42.7">avec</w> <w n="42.8">nous</w> ;</l>
						<l n="43" num="11.3"><w n="43.1">Vous</w> <w n="43.2">versiez</w> <w n="43.3">dans</w> <w n="43.4">nos</w> <w n="43.5">cœurs</w> <w n="43.6">votre</w> <w n="43.7">immense</w> <w n="43.8">harmonie</w></l>
						<l n="44" num="11.4"><w n="44.1">Et</w> <w n="44.2">rien</w> <w n="44.3">ne</w> <w n="44.4">nous</w> <w n="44.5">disait</w> <w n="44.6">que</w> <w n="44.7">nous</w> <w n="44.8">étions</w> <w n="44.9">des</w> <w n="44.10">fous</w> !</l>
					</lg>
					<lg n="12">
						<l n="45" num="12.1"><w n="45.1">Merci</w> <w n="45.2">d</w>’<w n="45.3">avoir</w> <w n="45.4">menti</w>, <w n="45.5">car</w> <w n="45.6">en</w> <w n="45.7">nos</w> <w n="45.8">âmes</w> <w n="45.9">vierges</w></l>
						<l n="46" num="12.2"><w n="46.1">Cet</w> <w n="46.2">infidèle</w> <w n="46.3">amour</w> <w n="46.4">nous</w> <w n="46.5">est</w> <w n="46.6">resté</w> <w n="46.7">sacré</w>,</l>
						<l n="47" num="12.3"><w n="47.1">Et</w> <w n="47.2">les</w> <w n="47.3">nuits</w> <w n="47.4">d</w>’<w n="47.5">autrefois</w> <w n="47.6">s</w>’<w n="47.7">illuminent</w> <w n="47.8">de</w> <w n="47.9">cierges</w></l>
						<l n="48" num="12.4"><w n="48.1">Qu</w>’<w n="48.2">aux</w> <w n="48.3">quatre</w> <w n="48.4">coins</w> <w n="48.5">du</w> <w n="48.6">ciel</w> <w n="48.7">tient</w> <w n="48.8">un</w> <w n="48.9">ange</w> <w n="48.10">éploré</w>.</l>
					</lg>
					<lg n="13">
						<l n="49" num="13.1"><w n="49.1">Vous</w> <w n="49.2">nous</w> <w n="49.3">avez</w> <w n="49.4">laissés</w> <w n="49.5">dans</w> <w n="49.6">notre</w> <w n="49.7">bonheur</w> <w n="49.8">vague</w>,</l>
						<l n="50" num="13.2"><w n="50.1">Eh</w> <w n="50.2">bien</w> ! <w n="50.3">soyez</w> <w n="50.4">bénis</w>, <w n="50.5">car</w> <w n="50.6">nous</w> <w n="50.7">étions</w> <w n="50.8">heureux</w> ; —</l>
						<l n="51" num="13.3"><w n="51.1">Depuis</w>, <w n="51.2">le</w> <w n="51.3">fiancé</w> <w n="51.4">n</w>’<w n="51.5">a</w> <w n="51.6">pu</w> <w n="51.7">garder</w> <w n="51.8">sa</w> <w n="51.9">bague</w>,</l>
						<l n="52" num="13.4"><w n="52.1">Et</w> <w n="52.2">c</w>’<w n="52.3">est</w> <w n="52.4">de</w> <w n="52.5">vous</w>, <w n="52.6">ô</w> <w n="52.7">ciel</w>, <w n="52.8">que</w> <w n="52.9">je</w> <w n="52.10">suis</w> <w n="52.11">amoureux</w> !</l>
					</lg>
					<lg n="14">
						<l n="53" num="14.1"><w n="53.1">C</w>’<w n="53.2">est</w> <w n="53.3">de</w> <w n="53.4">vous</w>, <w n="53.5">ô</w> <w n="53.6">vallons</w> <w n="53.7">paisibles</w>, <w n="53.8">pleins</w> <w n="53.9">de</w> <w n="53.10">roses</w>,</l>
						<l n="54" num="14.2"><w n="54.1">De</w> <w n="54.2">vous</w>, <w n="54.3">collines</w> <w n="54.4">d</w>’<w n="54.5">or</w> <w n="54.6">que</w> <w n="54.7">baise</w> <w n="54.8">le</w> <w n="54.9">soleil</w> ;</l>
						<l n="55" num="14.3"><w n="55.1">De</w> <w n="55.2">vous</w>, <w n="55.3">ô</w> <w n="55.4">chastes</w> <w n="55.5">fleurs</w> <w n="55.6">depuis</w> <w n="55.7">une</w> <w n="55.8">heure</w> <w n="55.9">écloses</w>,</l>
						<l n="56" num="14.4"><w n="56.1">Car</w> <w n="56.2">nul</w> <w n="56.3">visage</w> <w n="56.4">humain</w>, <w n="56.5">fleurs</w>, <w n="56.6">ne</w> <w n="56.7">vous</w> <w n="56.8">est</w> <w n="56.9">pareil</w>.</l>
					</lg>
					<lg n="15">
						<l n="57" num="15.1"><w n="57.1">C</w>’<w n="57.2">est</w> <w n="57.3">de</w> <w n="57.4">vous</w>, <w n="57.5">sombres</w> <w n="57.6">bois</w> <w n="57.7">dont</w> <w n="57.8">le</w> <w n="57.9">silence</w> <w n="57.10">enivre</w>,</l>
						<l n="58" num="15.2"><w n="58.1">C</w>’<w n="58.2">est</w> <w n="58.3">de</w> <w n="58.4">toi</w>, <w n="58.5">vaste</w> <w n="58.6">mer</w> <w n="58.7">qui</w> <w n="58.8">palpites</w> <w n="58.9">sans</w> <w n="58.10">fin</w>,</l>
						<l n="59" num="15.3"><w n="59.1">Et</w> <w n="59.2">que</w> <w n="59.3">mon</w> <w n="59.4">cœur</w> <w n="59.5">troublé</w> <w n="59.6">sent</w> <w n="59.7">frissonner</w> <w n="59.8">et</w> <w n="59.9">vivre</w></l>
						<l n="60" num="15.4"><w n="60.1">Quand</w> <w n="60.2">tu</w> <w n="60.3">bondis</w> <w n="60.4">devant</w> <w n="60.5">l</w>’<w n="60.6">étoile</w> <w n="60.7">du</w> <w n="60.8">matin</w> !</l>
					</lg>
					<ab type="star">⁂</ab>
					<lg n="16">
						<l n="61" num="16.1"><w n="61.1">Ainsi</w> <w n="61.2">donc</w> <w n="61.3">vous</w> <w n="61.4">pouvez</w> <w n="61.5">passer</w>, <w n="61.6">ô</w> <w n="61.7">filles</w> <w n="61.8">brunes</w>,</l>
						<l n="62" num="16.2"><w n="62.1">Et</w> <w n="62.2">vous</w> <w n="62.3">dont</w> <w n="62.4">le</w> <w n="62.5">soleil</w> <w n="62.6">dore</w> <w n="62.7">les</w> <w n="62.8">cheveux</w> <w n="62.9">blonds</w> ;</l>
						<l n="63" num="16.3"><w n="63.1">Je</w> <w n="63.2">n</w>’<w n="63.3">ai</w> <w n="63.4">plus</w> <w n="63.5">de</w> <w n="63.6">chansons</w> <w n="63.7">que</w> <w n="63.8">pour</w> <w n="63.9">la</w> <w n="63.10">douce</w> <w n="63.11">lune</w>,</l>
						<l n="64" num="16.4"><w n="64.1">Je</w> <w n="64.2">n</w>’<w n="64.3">ai</w> <w n="64.4">plus</w> <w n="64.5">de</w> <w n="64.6">baisers</w> <w n="64.7">que</w> <w n="64.8">pour</w> <w n="64.9">les</w> <w n="64.10">papillons</w>.</l>
					</lg>
					<lg n="17">
						<l n="65" num="17.1"><w n="65.1">Vous</w> <w n="65.2">ne</w> <w n="65.3">me</w> <w n="65.4">verrez</w> <w n="65.5">plus</w> <w n="65.6">à</w> <w n="65.7">genoux</w>, <w n="65.8">l</w>’<w n="65.9">œil</w> <w n="65.10">en</w> <w n="65.11">larmes</w>,</l>
						<l n="66" num="17.2"><w n="66.1">Je</w> <w n="66.2">ne</w> <w n="66.3">redoute</w> <w n="66.4">plus</w> <w n="66.5">votre</w> <w n="66.6">rire</w> <w n="66.7">moqueur</w> :</l>
						<l n="67" num="17.3"><w n="67.1">La</w> <w n="67.2">profonde</w> <w n="67.3">nature</w> <w n="67.4">a</w> <w n="67.5">d</w>’<w n="67.6">invincibles</w> <w n="67.7">charmes</w></l>
						<l n="68" num="17.4"><w n="68.1">Et</w> <w n="68.2">l</w>’<w n="68.3">univers</w> <w n="68.4">entier</w> <w n="68.5">va</w> <w n="68.6">me</w> <w n="68.7">remplir</w> <w n="68.8">le</w> <w n="68.9">cœur</w> !</l>
					</lg>
				</div></body></text></TEI>