<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES POËMES DE L’AMOUR ET DE LA MER</title>
				<title type="medium">Édition électronique</title>
				<author key="BCH">
					<name>
						<forename>Maurice</forename>
						<surname>Bouchor</surname>
					</name>
					<date from="1855" to="1929">1855-1929</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3472 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BCH_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
					<title>LES POËMES DE L’AMOUR ET DE LA MER</title>
					<author>Maurice Bouchor</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k9736743c</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
							<title>LES POËMES DE L’AMOUR ET DE LA MER</title>
							<author>Maurice Bouchor</author>
							<imprint>
								<pubPlace>Paris</pubPlace>
								<publisher>Charpentier et Cie, Libraires-éditeurs</publisher>
								<date when="1876">1876</date>
							</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1876">1876</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p></p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">II</head><head type="main_part">LA MORT DE L’AMOUR</head><div type="poem" key="BCH88">
					<head type="number">XXXIV</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Le</w> <w n="1.2">vent</w> <w n="1.3">était</w> <w n="1.4">bien</w> <w n="1.5">doux</w>, <w n="1.6">la</w> <w n="1.7">lune</w> <w n="1.8">était</w> <w n="1.9">bien</w> <w n="1.10">fine</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Par</w> <w n="2.2">une</w> <w n="2.3">nuit</w> <w n="2.4">de</w> <w n="2.5">mai</w>, <w n="2.6">par</w> <w n="2.7">une</w> <w n="2.8">nuit</w> <w n="2.9">divine</w></l>
						<l n="3" num="1.3"><space unit="char" quantity="8"></space><w n="3.1">Où</w> <w n="3.2">nous</w> <w n="3.3">nous</w> <w n="3.4">sommes</w> <w n="3.5">tant</w> <w n="3.6">aimés</w> !</l>
						<l n="4" num="1.4"><w n="4.1">Et</w> <w n="4.2">les</w> <w n="4.3">brises</w> <w n="4.4">de</w> <w n="4.5">mai</w>, <w n="4.6">chère</w>, <w n="4.7">sont</w> <w n="4.8">toujours</w> <w n="4.9">douces</w>,</l>
						<l n="5" num="1.5"><w n="5.1">Et</w> <w n="5.2">des</w> <w n="5.3">baisers</w> <w n="5.4">d</w>’<w n="5.5">argent</w> <w n="5.6">s</w>’<w n="5.7">endorment</w> <w n="5.8">sur</w> <w n="5.9">les</w> <w n="5.10">mousses</w>,</l>
						<l n="6" num="1.6"><space unit="char" quantity="8"></space><w n="6.1">Bien</w> <w n="6.2">que</w> <w n="6.3">nos</w> <w n="6.4">cœurs</w> <w n="6.5">se</w> <w n="6.6">soient</w> <w n="6.7">fermés</w>.</l>
					</lg>
					<lg n="2">
						<l n="7" num="2.1"><w n="7.1">Ah</w> ! <w n="7.2">si</w> <w n="7.3">nos</w> <w n="7.4">cœurs</w> <w n="7.5">pouvaient</w> <w n="7.6">oublier</w> <w n="7.7">ces</w> <w n="7.8">extases</w>,</l>
						<l n="8" num="2.2"><w n="8.1">S</w>’<w n="8.2">ils</w> <w n="8.3">pouvaient</w>, <w n="8.4">une</w> <w n="8.5">fois</w> <w n="8.6">brisés</w>, <w n="8.7">comme</w> <w n="8.8">des</w> <w n="8.9">vases</w></l>
						<l n="9" num="2.3"><space unit="char" quantity="8"></space><w n="9.1">Répandre</w> <w n="9.2">toute</w> <w n="9.3">leur</w> <w n="9.4">liqueur</w> !</l>
						<l n="10" num="2.4"><w n="10.1">Mais</w> <w n="10.2">le</w> <w n="10.3">vent</w> <w n="10.4">d</w>’<w n="10.5">autrefois</w> <w n="10.6">est</w> <w n="10.7">encore</w> <w n="10.8">sonore</w>,</l>
						<l n="11" num="2.5"><w n="11.1">La</w> <w n="11.2">lune</w> <w n="11.3">douloureuse</w> <w n="11.4">a</w> <w n="11.5">des</w> <w n="11.6">baisers</w> <w n="11.7">encore</w></l>
						<l n="12" num="2.6"><space unit="char" quantity="8"></space><w n="12.1">Qui</w> <w n="12.2">nous</w> <w n="12.3">tombent</w> <w n="12.4">au</w> <w n="12.5">fond</w> <w n="12.6">du</w> <w n="12.7">cœur</w>.</l>
					</lg>
					<lg n="3">
						<l n="13" num="3.1"><w n="13.1">Je</w> <w n="13.2">me</w> <w n="13.3">souviens</w> ! <w n="13.4">la</w> <w n="13.5">lune</w> <w n="13.6">éclaire</w> <w n="13.7">la</w> <w n="13.8">nuit</w> <w n="13.9">noire</w></l>
						<l n="14" num="3.2"><w n="14.1">Et</w> <w n="14.2">verse</w> <w n="14.3">un</w> <w n="14.4">jour</w> <w n="14.5">subit</w> <w n="14.6">au</w> <w n="14.7">fond</w> <w n="14.8">de</w> <w n="14.9">ma</w> <w n="14.10">mémoire</w> ;</l>
						<l n="15" num="3.3"><space unit="char" quantity="8"></space><w n="15.1">Je</w> <w n="15.2">me</w> <w n="15.3">souviens</w>, <w n="15.4">je</w> <w n="15.5">me</w> <w n="15.6">souviens</w> !</l>
						<l n="16" num="3.4"><w n="16.1">Et</w> <w n="16.2">je</w> <w n="16.3">maudis</w> <w n="16.4">la</w> <w n="16.5">nuit</w> <w n="16.6">que</w> <w n="16.7">la</w> <w n="16.8">lune</w> <w n="16.9">a</w> <w n="16.10">bénie</w></l>
						<l n="17" num="3.5"><w n="17.1">Et</w> <w n="17.2">que</w> <w n="17.3">les</w> <w n="17.4">vents</w> <w n="17.5">légers</w> <w n="17.6">emplissaient</w> <w n="17.7">d</w>’<w n="17.8">harmonie</w>,</l>
						<l n="18" num="3.6"><space unit="char" quantity="8"></space><w n="18.1">Où</w> <w n="18.2">tout</w> <w n="18.3">bas</w> <w n="18.4">je</w> <w n="18.5">te</w> <w n="18.6">disais</w> : <w n="18.7">Viens</w>…</l>
					</lg>
					<lg n="4">
						<l n="19" num="4.1"><w n="19.1">Je</w> <w n="19.2">maudis</w> <w n="19.3">ma</w> <w n="19.4">mémoire</w> <w n="19.5">et</w> <w n="19.6">la</w> <w n="19.7">traîtreuse</w> <w n="19.8">lune</w></l>
						<l n="20" num="4.2"><w n="20.1">Et</w> <w n="20.2">le</w> <w n="20.3">vent</w> <w n="20.4">qui</w> <w n="20.5">pleurait</w> <w n="20.6">parmi</w> <w n="20.7">la</w> <w n="20.8">forêt</w> <w n="20.9">brune</w>.</l>
						<l n="21" num="4.3"><space unit="char" quantity="8"></space><w n="21.1">Hélas</w> ! <w n="21.2">hélas</w> ! <w n="21.3">peut</w>-<w n="21.4">être</w> <w n="21.5">un</w> <w n="21.6">jour</w>,</l>
						<l n="22" num="4.4"><w n="22.1">Voulant</w> <w n="22.2">me</w> <w n="22.3">souvenir</w>, <w n="22.4">tout</w> <w n="22.5">navré</w> <w n="22.6">de</w> <w n="22.7">tristesse</w>,</l>
						<l n="23" num="4.5"><w n="23.1">Je</w> <w n="23.2">maudirai</w> <w n="23.3">la</w> <w n="23.4">froide</w> <w n="23.5">et</w> <w n="23.6">l</w>’<w n="23.7">inerte</w> <w n="23.8">vieillesse</w></l>
						<l n="24" num="4.6"><space unit="char" quantity="8"></space><w n="24.1">Capable</w> <w n="24.2">d</w>’<w n="24.3">oublier</w> <w n="24.4">l</w>’<w n="24.5">amour</w> !</l>
					</lg>
				</div></body></text></TEI>