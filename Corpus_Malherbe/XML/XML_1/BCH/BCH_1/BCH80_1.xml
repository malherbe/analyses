<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES POËMES DE L’AMOUR ET DE LA MER</title>
				<title type="medium">Édition électronique</title>
				<author key="BCH">
					<name>
						<forename>Maurice</forename>
						<surname>Bouchor</surname>
					</name>
					<date from="1855" to="1929">1855-1929</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3472 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BCH_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
					<title>LES POËMES DE L’AMOUR ET DE LA MER</title>
					<author>Maurice Bouchor</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k9736743c</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
							<title>LES POËMES DE L’AMOUR ET DE LA MER</title>
							<author>Maurice Bouchor</author>
							<imprint>
								<pubPlace>Paris</pubPlace>
								<publisher>Charpentier et Cie, Libraires-éditeurs</publisher>
								<date when="1876">1876</date>
							</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1876">1876</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p></p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">II</head><head type="main_part">LA MORT DE L’AMOUR</head><div type="poem" key="BCH80">
					<head type="number">XXVI</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Non</w>, <w n="1.2">ce</w> <w n="1.3">n</w>’<w n="1.4">est</w> <w n="1.5">pas</w> <w n="1.6">l</w>’<w n="1.7">hiver</w>, <w n="1.8">le</w> <w n="1.9">printemps</w> <w n="1.10">ni</w> <w n="1.11">l</w>’<w n="1.12">automne</w></l>
						<l n="2" num="1.2"><w n="2.1">Qui</w> <w n="2.2">fleurissent</w> <w n="2.3">les</w> <w n="2.4">cœurs</w> <w n="2.5">et</w> <w n="2.6">les</w> <w n="2.7">rendent</w> <w n="2.8">joyeux</w> ;</l>
						<l n="3" num="1.3"><w n="3.1">Car</w> <w n="3.2">d</w>’<w n="3.3">aucune</w> <w n="3.4">splendeur</w> <w n="3.5">l</w>’<w n="3.6">amour</w> <w n="3.7">vrai</w> <w n="3.8">ne</w> <w n="3.9">s</w>’<w n="3.10">étonne</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Il</w> <w n="4.2">s</w>’<w n="4.3">inquiète</w> <w n="4.4">peu</w> <w n="4.5">de</w> <w n="4.6">la</w> <w n="4.7">couleur</w> <w n="4.8">des</w> <w n="4.9">cieux</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Ce</w> <w n="5.2">n</w>’<w n="5.3">est</w> <w n="5.4">pas</w> <w n="5.5">la</w> <w n="5.6">douceur</w> <w n="5.7">et</w> <w n="5.8">le</w> <w n="5.9">charme</w> <w n="5.10">des</w> <w n="5.11">veilles</w>,</l>
						<l n="6" num="2.2"><w n="6.1">La</w> <w n="6.2">tristesse</w> <w n="6.3">du</w> <w n="6.4">blanc</w> <w n="6.5">paysage</w> <w n="6.6">glacé</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Qui</w> <w n="7.2">nous</w> <w n="7.3">manque</w> <w n="7.4">pour</w> <w n="7.5">nous</w> <w n="7.6">aimer</w> — <w n="7.7">mais</w> <w n="7.8">le</w> <w n="7.9">passé</w></l>
						<l n="8" num="2.4"><w n="8.1">N</w>’<w n="8.2">a</w> <w n="8.3">plus</w> <w n="8.4">qu</w>’<w n="8.5">un</w> <w n="8.6">chant</w> <w n="8.7">lointain</w> <w n="8.8">qui</w> <w n="8.9">meurt</w> <w n="8.10">à</w> <w n="8.11">nos</w> <w n="8.12">oreilles</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Non</w>, <w n="9.2">ce</w> <w n="9.3">n</w>’<w n="9.4">est</w> <w n="9.5">pas</w> <w n="9.6">la</w> <w n="9.7">chambre</w> <w n="9.8">où</w> <w n="9.9">nous</w> <w n="9.10">étions</w> <w n="9.11">si</w> <w n="9.12">bien</w>,</l>
						<l n="10" num="3.2"><w n="10.1">Écoutant</w> <w n="10.2">comme</w> <w n="10.3">un</w> <w n="10.4">loup</w> <w n="10.5">hurler</w> <w n="10.6">le</w> <w n="10.7">vent</w> <w n="10.8">farouche</w> :</l>
						<l n="11" num="3.3"><w n="11.1">Non</w> <w n="11.2">plus</w> <w n="11.3">que</w> <w n="11.4">le</w> <w n="11.5">printemps</w> <w n="11.6">et</w> <w n="11.7">qu</w>’<w n="11.8">une</w> <w n="11.9">verte</w> <w n="11.10">couche</w></l>
						<l n="12" num="3.4"><w n="12.1">Sous</w> <w n="12.2">les</w> <w n="12.3">arbres</w> <w n="12.4">géants</w> <w n="12.5">qui</w> <w n="12.6">ne</w> <w n="12.7">pensent</w> <w n="12.8">à</w> <w n="12.9">rien</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">A</w> <w n="13.2">présent</w> <w n="13.3">peut</w> <w n="13.4">venir</w> <w n="13.5">l</w>’<w n="13.6">insensible</w> <w n="13.7">marée</w></l>
						<l n="14" num="4.2"><w n="14.1">Des</w> <w n="14.2">jours</w> <w n="14.3">et</w> <w n="14.4">des</w> <w n="14.5">saisons</w>, <w n="14.6">des</w> <w n="14.7">neiges</w> <w n="14.8">et</w> <w n="14.9">des</w> <w n="14.10">fleurs</w> ;</l>
						<l n="15" num="4.3"><w n="15.1">La</w> <w n="15.2">neige</w> <w n="15.3">tombera</w> <w n="15.4">sans</w> <w n="15.5">rafraîchir</w> <w n="15.6">nos</w> <w n="15.7">cœurs</w>,</l>
						<l n="16" num="4.4"><w n="16.1">Et</w> <w n="16.2">nous</w> <w n="16.3">ne</w> <w n="16.4">boirons</w> <w n="16.5">plus</w> <w n="16.6">à</w> <w n="16.7">la</w> <w n="16.8">coupe</w> <w n="16.9">sacrée</w>.</l>
					</lg>
				</div></body></text></TEI>