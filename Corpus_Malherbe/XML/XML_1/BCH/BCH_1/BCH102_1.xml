<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES POËMES DE L’AMOUR ET DE LA MER</title>
				<title type="medium">Édition électronique</title>
				<author key="BCH">
					<name>
						<forename>Maurice</forename>
						<surname>Bouchor</surname>
					</name>
					<date from="1855" to="1929">1855-1929</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3472 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BCH_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
					<title>LES POËMES DE L’AMOUR ET DE LA MER</title>
					<author>Maurice Bouchor</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k9736743c</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
							<title>LES POËMES DE L’AMOUR ET DE LA MER</title>
							<author>Maurice Bouchor</author>
							<imprint>
								<pubPlace>Paris</pubPlace>
								<publisher>Charpentier et Cie, Libraires-éditeurs</publisher>
								<date when="1876">1876</date>
							</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1876">1876</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p></p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">III</head><head type="main_part">L’AMOUR DIVIN</head><div type="poem" key="BCH102">
					<head type="number">IV</head>
					<head type="main">LA MER AMOUREUSE</head>
					<lg n="1">
						<l n="1" num="1.1"><space unit="char" quantity="2"></space><w n="1.1">Un</w> <w n="1.2">murmure</w>, <w n="1.3">un</w> <w n="1.4">souffle</w>, <w n="1.5">un</w> <w n="1.6">rêve</w></l>
						<l n="2" num="1.2"><space unit="char" quantity="2"></space><w n="2.1">M</w>’<w n="2.2">est</w> <w n="2.3">parvenu</w> <w n="2.4">de</w> <w n="2.5">la</w> <w n="2.6">grève</w></l>
						<l n="3" num="1.3"><space unit="char" quantity="2"></space><w n="3.1">Où</w> <w n="3.2">pleurait</w> <w n="3.3">la</w> <w n="3.4">grande</w> <w n="3.5">mer</w>.</l>
						<l n="4" num="1.4"><space unit="char" quantity="2"></space><w n="4.1">Était</w>-<w n="4.2">ce</w> <w n="4.3">une</w> <w n="4.4">voix</w> <w n="4.5">de</w> <w n="4.6">femme</w> ?</l>
						<l n="5" num="1.5"><space unit="char" quantity="2"></space><w n="5.1">J</w>’<w n="5.2">en</w> <w n="5.3">ai</w> <w n="5.4">conservé</w> <w n="5.5">dans</w> <w n="5.6">l</w>’<w n="5.7">âme</w></l>
						<l n="6" num="1.6"><space unit="char" quantity="2"></space><w n="6.1">Comme</w> <w n="6.2">un</w> <w n="6.3">souvenir</w> <w n="6.4">amer</w>.</l>
					</lg>
					<lg n="2">
						<l rhyme="none" n="7" num="2.1"><w n="7.1">Ah</w> ! <w n="7.2">c</w>’<w n="7.3">est</w> <w n="7.4">le</w> <w n="7.5">vent</w> <w n="7.6">dans</w> <w n="7.7">l</w>’<w n="7.8">étendue</w> !</l>
					</lg>
					<lg n="3">
						<l n="8" num="3.1"><space unit="char" quantity="2"></space><w n="8.1">Amer</w>, <w n="8.2">et</w> <w n="8.3">pourtant</w> <w n="8.4">bien</w> <w n="8.5">doux</w> ;</l>
						<l n="9" num="3.2"><space unit="char" quantity="2"></space><w n="9.1">Je</w> <w n="9.2">me</w> <w n="9.3">suis</w> <w n="9.4">mis</w> <w n="9.5">à</w> <w n="9.6">genoux</w></l>
						<l n="10" num="3.3"><space unit="char" quantity="2"></space><w n="10.1">Dans</w> <w n="10.2">les</w> <w n="10.3">ténèbres</w> <w n="10.4">profondes</w></l>
						<l n="11" num="3.4"><space unit="char" quantity="2"></space><w n="11.1">De</w> <w n="11.2">la</w> <w n="11.3">triste</w> <w n="11.4">et</w> <w n="11.5">calme</w> <w n="11.6">nuit</w>,</l>
						<l n="12" num="3.5"><space unit="char" quantity="2"></space><w n="12.1">Où</w> <w n="12.2">passe</w> <w n="12.3">et</w> <w n="12.4">s</w>’<w n="12.5">évanouit</w></l>
						<l n="13" num="3.6"><space unit="char" quantity="2"></space><w n="13.1">Un</w> <w n="13.2">frisson</w> <w n="13.3">d</w>’<w n="13.4">or</w> <w n="13.5">sur</w> <w n="13.6">les</w> <w n="13.7">ondes</w>.</l>
					</lg>
					<lg n="4">
						<l rhyme="none" n="14" num="4.1"><w n="14.1">Ah</w> ! <w n="14.2">c</w>’<w n="14.3">est</w> <w n="14.4">la</w> <w n="14.5">houle</w> <w n="14.6">qui</w> <w n="14.7">gémit</w> !</l>
					</lg>
					<lg n="5">
						<l n="15" num="5.1"><space unit="char" quantity="2"></space><w n="15.1">Pourtant</w>, <w n="15.2">la</w> <w n="15.3">nuit</w> <w n="15.4">était</w> <w n="15.5">brune</w>,</l>
						<l n="16" num="5.2"><space unit="char" quantity="2"></space><w n="16.1">Bien</w> <w n="16.2">brune</w>, <w n="16.3">et</w> <w n="16.4">sans</w> <w n="16.5">clair</w> <w n="16.6">de</w> <w n="16.7">lune</w> ;</l>
						<l n="17" num="5.3"><space unit="char" quantity="2"></space><w n="17.1">Ce</w> <w n="17.2">mystérieux</w> <w n="17.3">frisson</w></l>
						<l n="18" num="5.4"><space unit="char" quantity="2"></space><w n="18.1">Venait</w> <w n="18.2">de</w> <w n="18.3">la</w> <w n="18.4">chevelure</w></l>
						<l n="19" num="5.5"><space unit="char" quantity="2"></space><w n="19.1">De</w> <w n="19.2">l</w>’<w n="19.3">ange</w> <w n="19.4">dont</w> <w n="19.5">la</w> <w n="19.6">voix</w> <w n="19.7">pure</w></l>
						<l n="20" num="5.6"><space unit="char" quantity="2"></space><w n="20.1">Murmurait</w> <w n="20.2">une</w> <w n="20.3">chanson</w>.</l>
					</lg>
					<lg n="6">
						<l rhyme="none" n="21" num="6.1"><w n="21.1">Ah</w> ! <w n="21.2">c</w>’<w n="21.3">est</w> <w n="21.4">le</w> <w n="21.5">vent</w> <w n="21.6">dans</w> <w n="21.7">l</w>’<w n="21.8">étendue</w> !</l>
					</lg>
					<lg n="7">
						<l n="22" num="7.1"><space unit="char" quantity="2"></space><w n="22.1">Il</w> <w n="22.2">existe</w> <w n="22.3">des</w> <w n="22.4">Esprits</w>,</l>
						<l n="23" num="7.2"><space unit="char" quantity="2"></space><w n="23.1">Bien</w> <w n="23.2">que</w> <w n="23.3">tous</w> <w n="23.4">n</w>’<w n="23.5">aient</w> <w n="23.6">pas</w> <w n="23.7">compris</w></l>
						<l n="24" num="7.3"><space unit="char" quantity="2"></space><w n="24.1">Ces</w> <w n="24.2">êtres</w> <w n="24.3">faits</w> <w n="24.4">de</w> <w n="24.5">chimères</w>,</l>
						<l n="25" num="7.4"><space unit="char" quantity="2"></space><w n="25.1">Par</w> <w n="25.2">les</w> <w n="25.3">poëtes</w> <w n="25.4">rêvés</w> ;</l>
						<l n="26" num="7.5"><space unit="char" quantity="2"></space><w n="26.1">Et</w> <w n="26.2">les</w> <w n="26.3">autres</w> <w n="26.4">sont</w> <w n="26.5">privés</w></l>
						<l n="27" num="7.6"><space unit="char" quantity="2"></space><w n="27.1">De</w> <w n="27.2">nos</w> <w n="27.3">voluptés</w> <w n="27.4">amères</w>.</l>
					</lg>
					<lg n="8">
						<l rhyme="none" n="28" num="8.1"><w n="28.1">Ah</w> ! <w n="28.2">c</w>’<w n="28.3">est</w> <w n="28.4">la</w> <w n="28.5">houle</w> <w n="28.6">qui</w> <w n="28.7">gémit</w> !</l>
					</lg>
					<lg n="9">
						<l n="29" num="9.1"><space unit="char" quantity="2"></space><w n="29.1">Celui</w>-<w n="29.2">ci</w>, <w n="29.3">je</w> <w n="29.4">m</w>’<w n="29.5">imagine</w>,</l>
						<l n="30" num="9.2"><space unit="char" quantity="2"></space><w n="30.1">Était</w> <w n="30.2">d</w>’<w n="30.3">essence</w> <w n="30.4">divine</w>,</l>
						<l n="31" num="9.3"><space unit="char" quantity="2"></space><w n="31.1">Invisible</w> <w n="31.2">et</w> <w n="31.3">souriant</w> ;</l>
						<l n="32" num="9.4"><space unit="char" quantity="2"></space><w n="32.1">Je</w> <w n="32.2">sentais</w> <w n="32.3">bien</w> <w n="32.4">sa</w> <w n="32.5">présence</w>,</l>
						<l n="33" num="9.5"><space unit="char" quantity="2"></space><w n="33.1">J</w>’<w n="33.2">écoutais</w> <w n="33.3">dans</w> <w n="33.4">le</w> <w n="33.5">silence</w></l>
						<l n="34" num="9.6"><space unit="char" quantity="2"></space><w n="34.1">Cette</w> <w n="34.2">âme</w> <w n="34.3">de</w> <w n="34.4">l</w>’<w n="34.5">océan</w>.</l>
					</lg>
					<lg n="10">
						<l rhyme="none" n="35" num="10.1"><w n="35.1">Ah</w> ! <w n="35.2">c</w>’<w n="35.3">est</w> <w n="35.4">le</w> <w n="35.5">vent</w> <w n="35.6">dans</w> <w n="35.7">l</w>’<w n="35.8">étendue</w>.</l>
					</lg>
					<lg n="11">
						<l n="36" num="11.1"><space unit="char" quantity="2"></space><w n="36.1">On</w> <w n="36.2">a</w> <w n="36.3">beau</w> <w n="36.4">parler</w> <w n="36.5">toujours</w></l>
						<l n="37" num="11.2"><space unit="char" quantity="2"></space><w n="37.1">Des</w> <w n="37.2">matelots</w> <w n="37.3">sans</w> <w n="37.4">secours</w></l>
						<l n="38" num="11.3"><space unit="char" quantity="2"></space><w n="38.1">Ensevelis</w> <w n="38.2">sous</w> <w n="38.3">les</w> <w n="38.4">vagues</w> ;</l>
						<l n="39" num="11.4"><space unit="char" quantity="2"></space><w n="39.1">Moi</w>, <w n="39.2">je</w> <w n="39.3">crois</w> <w n="39.4">à</w> <w n="39.5">ta</w> <w n="39.6">douceur</w>,</l>
						<l n="40" num="11.5"><space unit="char" quantity="2"></space><w n="40.1">Mer</w> ! <w n="40.2">tes</w> <w n="40.3">paroles</w> <w n="40.4">de</w> <w n="40.5">sœur</w></l>
						<l n="41" num="11.6"><space unit="char" quantity="2"></space><w n="41.1">Sont</w> <w n="41.2">amoureuses</w> <w n="41.3">et</w> <w n="41.4">vagues</w>.</l>
					</lg>
					<lg n="12">
						<l rhyme="none" n="42" num="12.1"><w n="42.1">Ah</w> ! <w n="42.2">c</w>’<w n="42.3">est</w> <w n="42.4">la</w> <w n="42.5">houle</w> <w n="42.6">qui</w> <w n="42.7">gémit</w> !</l>
					</lg>
					<lg n="13">
						<l n="43" num="13.1"><space unit="char" quantity="2"></space><w n="43.1">Jamais</w>, <w n="43.2">ô</w> <w n="43.3">profond</w> <w n="43.4">abîme</w>,</l>
						<l n="44" num="13.2"><space unit="char" quantity="2"></space><w n="44.1">Je</w> <w n="44.2">n</w>’<w n="44.3">ai</w> <w n="44.4">pu</w> <w n="44.5">croire</w> <w n="44.6">à</w> <w n="44.7">ton</w> <w n="44.8">crime</w>,</l>
						<l n="45" num="13.3"><space unit="char" quantity="2"></space><w n="45.1">A</w> <w n="45.2">tes</w> <w n="45.3">colères</w> <w n="45.4">d</w>’<w n="45.5">un</w> <w n="45.6">jour</w> ;</l>
						<l n="46" num="13.4"><space unit="char" quantity="2"></space><w n="46.1">Parce</w> <w n="46.2">que</w>, <w n="46.3">la</w> <w n="46.4">nuit</w>, <w n="46.5">tu</w> <w n="46.6">chantes</w></l>
						<l n="47" num="13.5"><space unit="char" quantity="2"></space><w n="47.1">De</w> <w n="47.2">longues</w> <w n="47.3">plaintes</w> <w n="47.4">touchantes</w></l>
						<l n="48" num="13.6"><space unit="char" quantity="2"></space><w n="48.1">Et</w> <w n="48.2">sembles</w> <w n="48.3">pâmer</w> <w n="48.4">d</w>’<w n="48.5">amour</w>.</l>
					</lg>
					<lg n="14">
						<l rhyme="none" n="49" num="14.1"><w n="49.1">Ah</w> ! <w n="49.2">c</w>’<w n="49.3">est</w> <w n="49.4">le</w> <w n="49.5">vent</w> <w n="49.6">dans</w> <w n="49.7">l</w>’<w n="49.8">étendue</w> !</l>
					</lg>
					<lg n="15">
						<l n="50" num="15.1"><space unit="char" quantity="2"></space><w n="50.1">Et</w> <w n="50.2">peut</w>-<w n="50.3">être</w> <w n="50.4">tu</w> <w n="50.5">gémis</w></l>
						<l n="51" num="15.2"><space unit="char" quantity="2"></space><w n="51.1">Que</w> <w n="51.2">des</w> <w n="51.3">souffles</w> <w n="51.4">ennemis</w></l>
						<l n="52" num="15.3"><space unit="char" quantity="2"></space><w n="52.1">T</w>’<w n="52.2">aient</w> <w n="52.3">fait</w> <w n="52.4">le</w> <w n="52.5">sépulcre</w> <w n="52.6">immense</w></l>
						<l n="53" num="15.4"><space unit="char" quantity="2"></space><w n="53.1">Où</w>, <w n="53.2">dans</w> <w n="53.3">un</w> <w n="53.4">linceul</w> <w n="53.5">de</w> <w n="53.6">flots</w>,</l>
						<l n="54" num="15.5"><space unit="char" quantity="2"></space><w n="54.1">D</w>’<w n="54.2">aventureux</w> <w n="54.3">matelots</w></l>
						<l n="55" num="15.6"><space unit="char" quantity="2"></space><w n="55.1">Ont</w> <w n="55.2">expié</w> <w n="55.3">leur</w> <w n="55.4">démence</w>.</l>
					</lg>
					<lg n="16">
						<l rhyme="none" n="56" num="16.1"><w n="56.1">Ah</w> ! <w n="56.2">c</w>’<w n="56.3">est</w> <w n="56.4">la</w> <w n="56.5">houle</w> <w n="56.6">qui</w> <w n="56.7">gémit</w> !</l>
					</lg>
					<lg n="17">
						<l n="57" num="17.1"><space unit="char" quantity="2"></space><w n="57.1">Tu</w> <w n="57.2">te</w> <w n="57.3">lamentes</w>, <w n="57.4">et</w> <w n="57.5">brises</w></l>
						<l n="58" num="17.2"><space unit="char" quantity="2"></space><w n="58.1">Doucement</w> <w n="58.2">tes</w> <w n="58.3">lames</w> <w n="58.4">grises</w></l>
						<l n="59" num="17.3"><space unit="char" quantity="2"></space><w n="59.1">Sur</w> <w n="59.2">le</w> <w n="59.3">sable</w>, <w n="59.4">et</w> <w n="59.5">tu</w> <w n="59.6">te</w> <w n="59.7">plains</w></l>
						<l n="60" num="17.4"><space unit="char" quantity="2"></space><w n="60.1">A</w> <w n="60.2">la</w> <w n="60.3">sourde</w> <w n="60.4">destinée</w></l>
						<l n="61" num="17.5"><space unit="char" quantity="2"></space><w n="61.1">D</w>’<w n="61.2">être</w> <w n="61.3">à</w> <w n="61.4">jamais</w> <w n="61.5">condamnée</w></l>
						<l n="62" num="17.6"><space unit="char" quantity="2"></space><w n="62.1">A</w> <w n="62.2">rouler</w> <w n="62.3">des</w> <w n="62.4">corps</w> <w n="62.5">humains</w>.</l>
					</lg>
					<lg n="18">
						<l rhyme="none" n="63" num="18.1"><w n="63.1">Ah</w> ! <w n="63.2">c</w>’<w n="63.3">est</w> <w n="63.4">le</w> <w n="63.5">vent</w> <w n="63.6">dans</w> <w n="63.7">l</w>’<w n="63.8">étendue</w> !</l>
					</lg>
					<lg n="19">
						<l n="64" num="19.1"><space unit="char" quantity="2"></space><w n="64.1">Mais</w> <w n="64.2">viens</w> <w n="64.3">à</w> <w n="64.4">moi</w>, <w n="64.5">viens</w> <w n="64.6">à</w> <w n="64.7">moi</w>,</l>
						<l n="65" num="19.2"><space unit="char" quantity="2"></space><w n="65.1">Jusqu</w>’<w n="65.2">à</w> <w n="65.3">mes</w> <w n="65.4">pieds</w> ; <w n="65.5">car</w> <w n="65.6">j</w>’<w n="65.7">ai</w> <w n="65.8">foi</w></l>
						<l n="66" num="19.3"><space unit="char" quantity="2"></space><w n="66.1">En</w> <w n="66.2">ta</w> <w n="66.3">mystique</w> <w n="66.4">tendresse</w> ;</l>
						<l n="67" num="19.4"><space unit="char" quantity="2"></space><w n="67.1">Je</w> <w n="67.2">sens</w> <w n="67.3">que</w> <w n="67.4">je</w> <w n="67.5">vais</w> <w n="67.6">t</w>’<w n="67.7">aimer</w>,</l>
						<l n="68" num="19.5"><space unit="char" quantity="2"></space><w n="68.1">Et</w> <w n="68.2">mon</w> <w n="68.3">cœur</w> <w n="68.4">peut</w> <w n="68.5">te</w> <w n="68.6">nommer</w></l>
						<l n="69" num="19.6"><space unit="char" quantity="2"></space><w n="69.1">Son</w> <w n="69.2">éternelle</w> <w n="69.3">maîtresse</w>.</l>
					</lg>
					<lg n="20">
						<l rhyme="none" n="70" num="20.1"><w n="70.1">Ah</w> ! <w n="70.2">c</w>’<w n="70.3">est</w> <w n="70.4">la</w> <w n="70.5">houle</w> <w n="70.6">qui</w> <w n="70.7">gémit</w> !</l>
					</lg>
					<lg n="21">
						<l n="71" num="21.1"><space unit="char" quantity="2"></space><w n="71.1">Je</w> <w n="71.2">jette</w> <w n="71.3">en</w> <w n="71.4">ton</w> <w n="71.5">sein</w> <w n="71.6">splendide</w></l>
						<l n="72" num="21.2"><space unit="char" quantity="2"></space><w n="72.1">Qui</w> <w n="72.2">monte</w> <w n="72.3">et</w> <w n="72.4">baisse</w>, <w n="72.5">sans</w> <w n="72.6">ride</w></l>
						<l n="73" num="21.3"><space unit="char" quantity="2"></space><w n="73.1">Et</w> <w n="73.2">comme</w> <w n="73.3">un</w> <w n="73.4">miroir</w> <w n="73.5">ami</w>,</l>
						<l n="74" num="21.4"><space unit="char" quantity="2"></space><w n="74.1">Tant</w> <w n="74.2">de</w> <w n="74.3">choses</w> <w n="74.4">dépensées</w></l>
						<l n="75" num="21.5"><space unit="char" quantity="2"></space><w n="75.1">Et</w> <w n="75.2">tant</w> <w n="75.3">de</w> <w n="75.4">vaines</w> <w n="75.5">pensées</w></l>
						<l n="76" num="21.6"><space unit="char" quantity="2"></space><w n="76.1">Qui</w> <w n="76.2">jusqu</w>’<w n="76.3">ici</w> <w n="76.4">m</w>’<w n="76.5">ont</w> <w n="76.6">blêmi</w>.</l>
					</lg>
					<lg n="22">
						<l rhyme="none" n="77" num="22.1"><w n="77.1">Ah</w> ! <w n="77.2">c</w>’<w n="77.3">est</w> <w n="77.4">le</w> <w n="77.5">vent</w> <w n="77.6">dans</w> <w n="77.7">l</w>’<w n="77.8">étendue</w> !</l>
					</lg>
					<lg n="23">
						<l n="78" num="23.1"><space unit="char" quantity="2"></space><w n="78.1">Désormais</w> <w n="78.2">tous</w> <w n="78.3">mes</w> <w n="78.4">sanglots</w></l>
						<l n="79" num="23.2"><space unit="char" quantity="2"></space><w n="79.1">Se</w> <w n="79.2">mêleront</w> <w n="79.3">à</w> <w n="79.4">tes</w> <w n="79.5">flots</w> ;</l>
						<l n="80" num="23.3"><space unit="char" quantity="2"></space><w n="80.1">J</w>’<w n="80.2">écouterai</w> <w n="80.3">ton</w> <w n="80.4">génie</w></l>
						<l n="81" num="23.4"><space unit="char" quantity="2"></space><w n="81.1">Psalmodier</w> <w n="81.2">tes</w> <w n="81.3">ennuis</w>,</l>
						<l n="82" num="23.5"><space unit="char" quantity="2"></space><w n="82.1">Et</w> <w n="82.2">je</w> <w n="82.3">bercerai</w> <w n="82.4">mes</w> <w n="82.5">nuits</w></l>
						<l n="83" num="23.6"><space unit="char" quantity="2"></space><w n="83.1">Avec</w> <w n="83.2">ta</w> <w n="83.3">grande</w> <w n="83.4">harmonie</w>.</l>
					</lg>
					<lg n="24">
						<l rhyme="none" n="84" num="24.1"><w n="84.1">Ah</w> ! <w n="84.2">c</w>’<w n="84.3">est</w> <w n="84.4">la</w> <w n="84.5">houle</w> <w n="84.6">qui</w> <w n="84.7">gémit</w> !</l>
					</lg>
					<lg n="25">
						<l n="85" num="25.1"><space unit="char" quantity="2"></space><w n="85.1">Viens</w>, <w n="85.2">ô</w> <w n="85.3">superbe</w> <w n="85.4">éplorée</w>,</l>
						<l n="86" num="25.2"><space unit="char" quantity="2"></space><w n="86.1">Donne</w>-<w n="86.2">moi</w> <w n="86.3">la</w> <w n="86.4">paix</w> <w n="86.5">sacrée</w></l>
						<l n="87" num="25.3"><space unit="char" quantity="2"></space><w n="87.1">D</w>’<w n="87.2">un</w> <w n="87.3">inaltérable</w> <w n="87.4">amour</w> ;</l>
						<l n="88" num="25.4"><space unit="char" quantity="2"></space><w n="88.1">Pour</w> <w n="88.2">adoucir</w> <w n="88.3">ton</w> <w n="88.4">murmure</w>,</l>
						<l n="89" num="25.5"><space unit="char" quantity="2"></space><w n="89.1">Je</w> <w n="89.2">te</w> <w n="89.3">donne</w>, <w n="89.4">je</w> <w n="89.5">le</w> <w n="89.6">jure</w>,</l>
						<l n="90" num="25.6"><space unit="char" quantity="2"></space><w n="90.1">Toute</w> <w n="90.2">ma</w> <w n="90.3">vie</w> <w n="90.4">en</w> <w n="90.5">retour</w>.</l>
					</lg>
					<ab type="star">⁂</ab>
					<lg n="26">
						<l n="91" num="26.1"><space unit="char" quantity="2"></space><w n="91.1">C</w>’<w n="91.2">est</w> <w n="91.3">le</w> <w n="91.4">vent</w> <w n="91.5">dans</w> <w n="91.6">l</w>’<w n="91.7">étendue</w>,</l>
						<l n="92" num="26.2"><space unit="char" quantity="2"></space><w n="92.1">C</w>’<w n="92.2">est</w> <w n="92.3">la</w> <w n="92.4">houle</w> <w n="92.5">qui</w> <w n="92.6">gémit</w>,</l>
						<l n="93" num="26.3"><space unit="char" quantity="2"></space><w n="93.1">C</w>’<w n="93.2">est</w> <w n="93.3">l</w>’<w n="93.4">amphitrite</w> <w n="93.5">éperdue</w></l>
						<l n="94" num="26.4"><space unit="char" quantity="2"></space><w n="94.1">Qui</w> <w n="94.2">sanglote</w> <w n="94.3">et</w> <w n="94.4">qui</w> <w n="94.5">frémit</w> ;</l>
					</lg>
					<lg n="27">
						<l n="95" num="27.1"><space unit="char" quantity="2"></space><w n="95.1">C</w>’<w n="95.2">est</w> <w n="95.3">la</w> <w n="95.4">mer</w> <w n="95.5">immense</w> <w n="95.6">et</w> <w n="95.7">belle</w></l>
						<l n="96" num="27.2"><space unit="char" quantity="2"></space><w n="96.1">Qui</w> <w n="96.2">vient</w> <w n="96.3">mumurant</w> <w n="96.4">à</w> <w n="96.5">moi</w>,</l>
						<l n="97" num="27.3"><space unit="char" quantity="2"></space><w n="97.1">Qui</w> <w n="97.2">me</w> <w n="97.3">flatte</w> <w n="97.4">et</w> <w n="97.5">qui</w> <w n="97.6">m</w>’<w n="97.7">appelle</w>,</l>
						<l n="98" num="27.4"><space unit="char" quantity="2"></space><w n="98.1">Prise</w> <w n="98.2">d</w>’<w n="98.3">un</w> <w n="98.4">étrange</w> <w n="98.5">émoi</w> ;</l>
					</lg>
					<lg n="28">
						<l n="99" num="28.1"><space unit="char" quantity="2"></space><w n="99.1">C</w>’<w n="99.2">est</w> <w n="99.3">la</w> <w n="99.4">voix</w> <w n="99.5">des</w> <w n="99.6">flots</w> <w n="99.7">tranquilles</w></l>
						<l n="100" num="28.2"><space unit="char" quantity="2"></space><w n="100.1">Qui</w> <w n="100.2">s</w>’<w n="100.3">élève</w> <w n="100.4">dans</w> <w n="100.5">la</w> <w n="100.6">nuit</w>,</l>
						<l n="101" num="28.3"><space unit="char" quantity="2"></space><w n="101.1">Et</w> <w n="101.2">l</w>’<w n="101.3">on</w> <w n="101.4">dirait</w> <w n="101.5">qu</w>’<w n="101.6">immobiles</w></l>
						<l n="102" num="28.4"><space unit="char" quantity="2"></space><w n="102.1">Ils</w> <w n="102.2">savent</w> <w n="102.3">parler</w> <w n="102.4">sans</w> <w n="102.5">bruit</w> ;</l>
					</lg>
					<lg n="29">
						<l n="103" num="29.1"><space unit="char" quantity="2"></space><w n="103.1">Un</w> <w n="103.2">je</w> <w n="103.3">ne</w> <w n="103.4">sais</w> <w n="103.5">quoi</w> <w n="103.6">qui</w> <w n="103.7">charme</w>,</l>
						<l n="104" num="29.2"><space unit="char" quantity="2"></space><w n="104.1">Qui</w> <w n="104.2">pénètre</w> <w n="104.3">et</w> <w n="104.4">qui</w> <w n="104.5">ravit</w> ;</l>
						<l n="105" num="29.3"><space unit="char" quantity="2"></space><w n="105.1">La</w> <w n="105.2">mer</w> <w n="105.3">n</w>’<w n="105.4">est</w> <w n="105.5">plus</w> <w n="105.6">qu</w>’<w n="105.7">une</w> <w n="105.8">larme</w>,</l>
						<l n="106" num="29.4"><space unit="char" quantity="2"></space><w n="106.1">Elle</w> <w n="106.2">aime</w> ! <w n="106.3">Elle</w> <w n="106.4">aime</w> ! <w n="106.5">Elle</w> <w n="106.6">vit</w>.</l>
					</lg>
				</div></body></text></TEI>