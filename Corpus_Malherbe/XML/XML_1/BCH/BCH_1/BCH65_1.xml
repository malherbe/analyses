<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES POËMES DE L’AMOUR ET DE LA MER</title>
				<title type="medium">Édition électronique</title>
				<author key="BCH">
					<name>
						<forename>Maurice</forename>
						<surname>Bouchor</surname>
					</name>
					<date from="1855" to="1929">1855-1929</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3472 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BCH_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
					<title>LES POËMES DE L’AMOUR ET DE LA MER</title>
					<author>Maurice Bouchor</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k9736743c</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
							<title>LES POËMES DE L’AMOUR ET DE LA MER</title>
							<author>Maurice Bouchor</author>
							<imprint>
								<pubPlace>Paris</pubPlace>
								<publisher>Charpentier et Cie, Libraires-éditeurs</publisher>
								<date when="1876">1876</date>
							</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1876">1876</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p></p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">II</head><head type="main_part">LA MORT DE L’AMOUR</head><div type="poem" key="BCH65">
					<head type="number">XIV</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Aimée</w>, <w n="1.2">aux</w> <w n="1.3">jours</w> <w n="1.4">lointains</w> <w n="1.5">où</w> <w n="1.6">nous</w> <w n="1.7">nous</w> <w n="1.8">reverrons</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Quel</w> <w n="2.2">baiser</w> <w n="2.3">collera</w> <w n="2.4">ma</w> <w n="2.5">bouche</w> <w n="2.6">sur</w> <w n="2.7">la</w> <w n="2.8">tienne</w> !</l>
						<l n="3" num="1.3"><w n="3.1">Et</w> <w n="3.2">pour</w> <w n="3.3">que</w> <w n="3.4">notre</w> <w n="3.5">cœur</w> <w n="3.6">s</w>’<w n="3.7">éveille</w> <w n="3.8">et</w> <w n="3.9">se</w> <w n="3.10">souvienne</w></l>
						<l n="4" num="1.4"><w n="4.1">Comme</w> <w n="4.2">nous</w> <w n="4.3">pleurerons</w> <w n="4.4">et</w> <w n="4.5">comme</w> <w n="4.6">nous</w> <w n="4.7">rirons</w> !</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Tu</w> <w n="5.2">te</w> <w n="5.3">rappelleras</w> <w n="5.4">comme</w>, <w n="5.5">inclinant</w> <w n="5.6">nos</w> <w n="5.7">fronts</w>,</l>
						<l n="6" num="2.2"><w n="6.1">Nous</w> <w n="6.2">écoutions</w> <w n="6.3">chanter</w> <w n="6.4">la</w> <w n="6.5">brise</w> <w n="6.6">aérienne</w></l>
						<l n="7" num="2.3"><w n="7.1">Qui</w>, <w n="7.2">parfumant</w> <w n="7.3">le</w> <w n="7.4">ciel</w> <w n="7.5">léger</w> <w n="7.6">de</w> <w n="7.7">son</w> <w n="7.8">haleine</w>,</l>
						<l n="8" num="2.4"><w n="8.1">Jetait</w> <w n="8.2">sur</w> <w n="8.3">nos</w> <w n="8.4">genoux</w> <w n="8.5">muguets</w> <w n="8.6">et</w> <w n="8.7">liserons</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Bien</w> <w n="9.2">des</w> <w n="9.3">jours</w> <w n="9.4">sont</w> <w n="9.5">passés</w>… <w n="9.6">Aux</w> <w n="9.7">soirs</w> <w n="9.8">mélancoliques</w>,</l>
						<l n="10" num="3.2"><w n="10.1">Alors</w> <w n="10.2">qu</w>’<w n="10.3">étincelaient</w> <w n="10.4">les</w> <w n="10.5">cieux</w> <w n="10.6">doux</w> <w n="10.7">et</w> <w n="10.8">mystiques</w>,</l>
						<l n="11" num="3.3"><w n="11.1">Le</w> <w n="11.2">cœur</w> <w n="11.3">de</w> <w n="11.4">la</w> <w n="11.5">Nature</w> <w n="11.6">amoureuse</w> <w n="11.7">battait</w>,</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1"><w n="12.1">Tressaillait</w> <w n="12.2">d</w>’<w n="12.3">une</w> <w n="12.4">joie</w> <w n="12.5">infinie</w>, <w n="12.6">et</w> <w n="12.7">chantait</w>…</l>
						<l n="13" num="4.2"><w n="13.1">Quelques</w> <w n="13.2">barques</w> <w n="13.3">fuyaient</w>, <w n="13.4">arrondissant</w> <w n="13.5">leurs</w> <w n="13.6">toiles</w>,</l>
						<l n="14" num="4.3"><w n="14.1">Et</w> <w n="14.2">je</w> <w n="14.3">te</w> <w n="14.4">regardais</w> <w n="14.5">regarder</w> <w n="14.6">les</w> <w n="14.7">étoiles</w> !</l>
					</lg>
				</div></body></text></TEI>