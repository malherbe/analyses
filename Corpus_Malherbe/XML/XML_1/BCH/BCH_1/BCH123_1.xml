<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES POËMES DE L’AMOUR ET DE LA MER</title>
				<title type="medium">Édition électronique</title>
				<author key="BCH">
					<name>
						<forename>Maurice</forename>
						<surname>Bouchor</surname>
					</name>
					<date from="1855" to="1929">1855-1929</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3472 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BCH_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
					<title>LES POËMES DE L’AMOUR ET DE LA MER</title>
					<author>Maurice Bouchor</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k9736743c</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
							<title>LES POËMES DE L’AMOUR ET DE LA MER</title>
							<author>Maurice Bouchor</author>
							<imprint>
								<pubPlace>Paris</pubPlace>
								<publisher>Charpentier et Cie, Libraires-éditeurs</publisher>
								<date when="1876">1876</date>
							</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1876">1876</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p></p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">III</head><head type="main_part">L’AMOUR DIVIN</head><div type="poem" key="BCH123">
					<head type="number">XXV</head>
					<head type="main">L’ART</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">J</w>’<w n="1.2">irai</w> <w n="1.3">bien</w> <w n="1.4">loin</w>, <w n="1.5">bien</w> <w n="1.6">loin</w>, <w n="1.7">fendant</w> <w n="1.8">l</w>’<w n="1.9">écume</w> <w n="1.10">blanche</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Avec</w> <w n="2.2">un</w> <w n="2.3">compagnon</w> <w n="2.4">sinistre</w> <w n="2.5">à</w> <w n="2.6">mon</w> <w n="2.7">côté</w></l>
						<l n="3" num="1.3"><w n="3.1">Qui</w> <w n="3.2">chaque</w> <w n="3.3">nuit</w> <w n="3.4">viendra</w> <w n="3.5">me</w> <w n="3.6">tirer</w> <w n="3.7">par</w> <w n="3.8">la</w> <w n="3.9">manche</w></l>
						<l n="4" num="1.4"><w n="4.1">Et</w> <w n="4.2">qui</w> <w n="4.3">me</w> <w n="4.4">laissera</w> <w n="4.5">dans</w> <w n="4.6">l</w>’<w n="4.7">ombre</w> <w n="4.8">épouvanté</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">La</w> <w n="5.2">joyeuse</w> <w n="5.3">espérance</w> <w n="5.4">a</w> <w n="5.5">fui</w> <w n="5.6">loin</w> <w n="5.7">de</w> <w n="5.8">mon</w> <w n="5.9">âme</w> ;</l>
						<l n="6" num="2.2"><w n="6.1">Je</w> <w n="6.2">sais</w> <w n="6.3">que</w> <w n="6.4">notre</w> <w n="6.5">amour</w>, <w n="6.6">dans</w> <w n="6.7">sa</w> <w n="6.8">tombe</w> <w n="6.9">endormi</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Est</w> <w n="7.2">tourné</w> <w n="7.3">vers</w> <w n="7.4">la</w> <w n="7.5">mer</w> <w n="7.6">et</w> <w n="7.7">battu</w> <w n="7.8">par</w> <w n="7.9">la</w> <w n="7.10">lame</w>,</l>
						<l n="8" num="2.4"><w n="8.1">Et</w> <w n="8.2">qu</w>’<w n="8.3">il</w> <w n="8.4">est</w> <w n="8.5">solitaire</w> <w n="8.6">et</w> <w n="8.7">que</w> <w n="8.8">le</w> <w n="8.9">vent</w> <w n="8.10">gémit</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Je</w> <w n="9.2">me</w> <w n="9.3">souviens</w> <w n="9.4">qu</w>’<w n="9.5">après</w> <w n="9.6">le</w> <w n="9.7">deuil</w> <w n="9.8">de</w> <w n="9.9">ma</w> <w n="9.10">jeunesse</w>,</l>
						<l n="10" num="3.2"><w n="10.1">Pris</w> <w n="10.2">d</w>’<w n="10.3">une</w> <w n="10.4">passion</w> <w n="10.5">invincible</w>, <w n="10.6">j</w>’<w n="10.7">ai</w> <w n="10.8">cru</w></l>
						<l n="11" num="3.3"><w n="11.1">Trouver</w> <w n="11.2">en</w> <w n="11.3">la</w> <w n="11.4">nature</w> <w n="11.5">une</w> <w n="11.6">immense</w> <w n="11.7">tendresse</w></l>
						<l n="12" num="3.4"><w n="12.1">Et</w> <w n="12.2">qu</w>’<w n="12.3">elle</w>, <w n="12.4">en</w> <w n="12.5">ma</w> <w n="12.6">douleur</w>, <w n="12.7">ne</w> <w n="12.8">m</w>’<w n="12.9">a</w> <w n="12.10">point</w> <w n="12.11">secouru</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Mais</w> <w n="13.2">toujours</w> <w n="13.3">près</w> <w n="13.4">de</w> <w n="13.5">moi</w> <w n="13.6">comme</w> <w n="13.7">un</w> <w n="13.8">spectre</w> <w n="13.9">livide</w></l>
						<l n="14" num="4.2"><w n="14.1">Le</w> <w n="14.2">sombre</w> <w n="14.3">compagnon</w> <w n="14.4">me</w> <w n="14.5">regarde</w> ; <w n="14.6">et</w> <w n="14.7">sitôt</w></l>
						<l n="15" num="4.3"><w n="15.1">Que</w> <w n="15.2">je</w> <w n="15.3">vais</w> <w n="15.4">pleurer</w>, <w n="15.5">triste</w> <w n="15.6">à</w> <w n="15.7">me</w> <w n="15.8">sentir</w> <w n="15.9">si</w> <w n="15.10">vide</w>,</l>
						<l n="16" num="4.4"><w n="16.1">Il</w> <w n="16.2">me</w> <w n="16.3">frappe</w> <w n="16.4">au</w> <w n="16.5">visage</w> <w n="16.6">et</w> <w n="16.7">me</w> <w n="16.8">dit</w> : <hi rend="ital"><w n="16.9">memento</w></hi> !</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Je</w> <w n="17.2">ne</w> <w n="17.3">suis</w> <w n="17.4">jamais</w> <w n="17.5">seul</w>. <w n="17.6">Si</w> <w n="17.7">mon</w> <w n="17.8">ennui</w> <w n="17.9">sans</w> <w n="17.10">borne</w></l>
						<l n="18" num="5.2"><w n="18.1">Est</w> <w n="18.2">prêt</w> <w n="18.3">à</w> <w n="18.4">m</w>’<w n="18.5">inonder</w>, <w n="18.6">lui</w>, <w n="18.7">de</w> <w n="18.8">son</w> <w n="18.9">maigre</w> <w n="18.10">doigt</w></l>
						<l n="19" num="5.3"><w n="19.1">Il</w> <w n="19.2">me</w> <w n="19.3">montre</w> <w n="19.4">une</w> <w n="19.5">page</w> <w n="19.6">inachevée</w>, <w n="19.7">et</w> <w n="19.8">morne</w></l>
						<l n="20" num="5.4"><w n="20.1">Me</w> <w n="20.2">répète</w> <w n="20.3">sans</w> <w n="20.4">fin</w> : <w n="20.5">Travaille</w>, <w n="20.6">et</w> <w n="20.7">souviens</w>-<w n="20.8">toi</w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Ah</w> ! <w n="21.2">je</w> <w n="21.3">te</w> <w n="21.4">comprends</w> <w n="21.5">bien</w>, <w n="21.6">frère</w> <w n="21.7">bizarre</w> <w n="21.8">et</w> <w n="21.9">triste</w></l>
						<l n="22" num="6.2"><w n="22.1">Qui</w> <w n="22.2">m</w>’<w n="22.3">accompagneras</w> <w n="22.4">jusqu</w>’<w n="22.5">au</w> <w n="22.6">seuil</w> <w n="22.7">du</w> <w n="22.8">tombeau</w> ;</l>
						<l n="23" num="6.3"><w n="23.1">C</w>’<w n="23.2">est</w> <w n="23.3">inutilement</w> <w n="23.4">que</w> <w n="23.5">mon</w> <w n="23.6">cœur</w> <w n="23.7">te</w> <w n="23.8">résiste</w>,</l>
						<l n="24" num="6.4"><w n="24.1">Je</w> <w n="24.2">subis</w> <w n="24.3">à</w> <w n="24.4">jamais</w> <w n="24.5">le</w> <w n="24.6">vertige</w> <w n="24.7">du</w> <w n="24.8">beau</w>.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">Ton</w> <w n="25.2">nom</w> <w n="25.3">est</w> <w n="25.4">l</w>’<w n="25.5">art</w> <w n="25.6">divin</w>, <w n="25.7">l</w>’<w n="25.8">art</w> <w n="25.9">éternel</w>, <w n="25.10">ô</w> <w n="25.11">frère</w></l>
						<l n="26" num="7.2"><w n="26.1">Qui</w> <w n="26.2">ne</w> <w n="26.3">me</w> <w n="26.4">laisses</w> <w n="26.5">pas</w> <w n="26.6">dormir</w> ! <w n="26.7">c</w>’<w n="26.8">est</w> <w n="26.9">l</w>’<w n="26.10">art</w> <w n="26.11">jaloux</w></l>
						<l n="27" num="7.3"><w n="27.1">Qui</w> <w n="27.2">me</w> <w n="27.3">fait</w> <w n="27.4">exhumer</w> <w n="27.5">de</w> <w n="27.6">leur</w> <w n="27.7">froide</w> <w n="27.8">poussière</w></l>
						<l n="28" num="7.4"><w n="28.1">Tant</w> <w n="28.2">de</w> <w n="28.3">lettres</w> <w n="28.4">d</w>’<w n="28.5">amour</w> <w n="28.6">et</w> <w n="28.7">de</w> <w n="28.8">gais</w> <w n="28.9">rendez</w>-<w n="28.10">vous</w> ;</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1"><w n="29.1">Qui</w> <w n="29.2">me</w> <w n="29.3">force</w>, <w n="29.4">mourant</w>, <w n="29.5">à</w> <w n="29.6">revivre</w> <w n="29.7">ma</w> <w n="29.8">vie</w>,</l>
						<l n="30" num="8.2"><w n="30.1">A</w> <w n="30.2">redire</w>, <w n="30.3">impassible</w> <w n="30.4">et</w> <w n="30.5">sans</w> <w n="30.6">pleurs</w> <w n="30.7">dans</w> <w n="30.8">la</w> <w n="30.9">voix</w>,</l>
						<l n="31" num="8.3"><w n="31.1">L</w>’<w n="31.2">histoire</w> <w n="31.3">de</w> <w n="31.4">mon</w> <w n="31.5">cœur</w> <w n="31.6">et</w> <w n="31.7">ma</w> <w n="31.8">peine</w> <w n="31.9">infinie</w></l>
						<l n="32" num="8.4"><w n="32.1">Aussi</w> <w n="32.2">paisiblement</w> <w n="32.3">qu</w>’<w n="32.4">un</w> <w n="32.5">oiseau</w> <w n="32.6">dans</w> <w n="32.7">les</w> <w n="32.8">bois</w>.</l>
					</lg>
				</div></body></text></TEI>