<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES POËMES DE L’AMOUR ET DE LA MER</title>
				<title type="medium">Édition électronique</title>
				<author key="BCH">
					<name>
						<forename>Maurice</forename>
						<surname>Bouchor</surname>
					</name>
					<date from="1855" to="1929">1855-1929</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3472 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BCH_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
					<title>LES POËMES DE L’AMOUR ET DE LA MER</title>
					<author>Maurice Bouchor</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k9736743c</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
							<title>LES POËMES DE L’AMOUR ET DE LA MER</title>
							<author>Maurice Bouchor</author>
							<imprint>
								<pubPlace>Paris</pubPlace>
								<publisher>Charpentier et Cie, Libraires-éditeurs</publisher>
								<date when="1876">1876</date>
							</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1876">1876</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p></p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">II</head><head type="main_part">LA MORT DE L’AMOUR</head><div type="poem" key="BCH94">
					<head type="number">XL</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">L</w>’<w n="1.2">année</w> <w n="1.3">est</w> <w n="1.4">morte</w>, <w n="1.5">ding</w> <w n="1.6">dong</w> !</l>
						<l n="2" num="1.2"><w n="2.1">Ding</w>, <w n="2.2">deng</w>, <w n="2.3">dong</w>, <w n="2.4">l</w>’<w n="2.5">année</w> <w n="2.6">est</w> <w n="2.7">morte</w> ;</l>
						<l n="3" num="1.3"><w n="3.1">Janvier</w> <w n="3.2">attend</w> <w n="3.3">à</w> <w n="3.4">la</w> <w n="3.5">porte</w></l>
						<l n="4" num="1.4"><w n="4.1">Qu</w>’<w n="4.2">on</w> <w n="4.3">lui</w> <w n="4.4">tire</w> <w n="4.5">le</w> <w n="4.6">cordon</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">L</w>’<w n="5.2">année</w> <w n="5.3">est</w> <w n="5.4">morte</w>, <w n="5.5">bien</w> <w n="5.6">morte</w> !</l>
						<l n="6" num="2.2"><w n="6.1">On</w> <w n="6.2">va</w> <w n="6.3">l</w>’<w n="6.4">enterrer</w> <w n="6.5">bientôt</w> ;</l>
						<l n="7" num="2.3"><w n="7.1">Par</w> <w n="7.2">la</w> <w n="7.3">Vierge</w>, <w n="7.4">il</w> <w n="7.5">ne</w> <w n="7.6">m</w>’<w n="7.7">en</w> <w n="7.8">chaut</w>,</l>
						<l n="8" num="2.4"><w n="8.1">Par</w> <w n="8.2">le</w> <w n="8.3">diable</w>, <w n="8.4">peu</w> <w n="8.5">m</w>’<w n="8.6">importe</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Que</w> <w n="9.2">l</w>’<w n="9.3">on</w> <w n="9.4">mette</w> <w n="9.5">aussi</w> <w n="9.6">mon</w> <w n="9.7">cœur</w></l>
						<l n="10" num="3.2"><w n="10.1">Dans</w> <w n="10.2">la</w> <w n="10.3">fosse</w> <w n="10.4">de</w> <w n="10.5">l</w>’<w n="10.6">année</w></l>
						<l n="11" num="3.3"><w n="11.1">Où</w> <w n="11.2">mon</w> <w n="11.3">âme</w> <w n="11.4">s</w>’<w n="11.5">est</w> <w n="11.6">damnée</w></l>
						<l n="12" num="3.4"><w n="12.1">Pour</w> <w n="12.2">un</w> <w n="12.3">sourire</w> <w n="12.4">moqueur</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Que</w> <w n="13.2">dans</w> <w n="13.3">cette</w> <w n="13.4">fosse</w> <w n="13.5">on</w> <w n="13.6">plonge</w></l>
						<l n="14" num="4.2"><w n="14.1">Tous</w> <w n="14.2">les</w> <w n="14.3">regrets</w> <w n="14.4">superflus</w>,</l>
						<l n="15" num="4.3"><w n="15.1">Et</w> <w n="15.2">qu</w>’<w n="15.3">on</w> <w n="15.4">ne</w> <w n="15.5">me</w> <w n="15.6">parle</w> <w n="15.7">plus</w></l>
						<l n="16" num="4.4"><w n="16.1">Du</w> <w n="16.2">passé</w> <w n="16.3">qui</w> <w n="16.4">n</w>’<w n="16.5">est</w> <w n="16.6">qu</w>’<w n="16.7">un</w> <w n="16.8">songe</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Lugubres</w> <w n="17.2">cloches</w> <w n="17.3">de</w> <w n="17.4">fer</w>,</l>
						<l n="18" num="5.2"><w n="18.1">Roulez</w> <w n="18.2">à</w> <w n="18.3">pleine</w> <w n="18.4">volée</w></l>
						<l n="19" num="5.3"><w n="19.1">Sur</w> <w n="19.2">mon</w> <w n="19.3">âme</w> <w n="19.4">désolée</w></l>
						<l n="20" num="5.4"><w n="20.1">Vos</w> <w n="20.2">lourds</w> <w n="20.3">carillons</w> <w n="20.4">d</w>’<w n="20.5">enfer</w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">L</w>’<w n="21.2">année</w> <w n="21.3">est</w> <w n="21.4">morte</w>, <w n="21.5">bien</w> <w n="21.6">morte</w> !</l>
						<l n="22" num="6.2"><w n="22.1">Et</w>, <w n="22.2">désespéré</w>, <w n="22.3">j</w>’<w n="22.4">attends</w></l>
						<l n="23" num="6.3"><w n="23.1">Qu</w>’<w n="23.2">il</w> <w n="23.3">naisse</w> <w n="23.4">un</w> <w n="23.5">nouveau</w> <w n="23.6">printemps</w>,</l>
						<l n="24" num="6.4"><w n="24.1">Ou</w> <w n="24.2">que</w> <w n="24.3">le</w> <w n="24.4">diable</w> <w n="24.5">m</w>’<w n="24.6">emporte</w>.</l>
					</lg>
				</div></body></text></TEI>