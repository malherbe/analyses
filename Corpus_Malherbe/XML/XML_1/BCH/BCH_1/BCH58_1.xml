<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES POËMES DE L’AMOUR ET DE LA MER</title>
				<title type="medium">Édition électronique</title>
				<author key="BCH">
					<name>
						<forename>Maurice</forename>
						<surname>Bouchor</surname>
					</name>
					<date from="1855" to="1929">1855-1929</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3472 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BCH_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
					<title>LES POËMES DE L’AMOUR ET DE LA MER</title>
					<author>Maurice Bouchor</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k9736743c</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
							<title>LES POËMES DE L’AMOUR ET DE LA MER</title>
							<author>Maurice Bouchor</author>
							<imprint>
								<pubPlace>Paris</pubPlace>
								<publisher>Charpentier et Cie, Libraires-éditeurs</publisher>
								<date when="1876">1876</date>
							</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1876">1876</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p></p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">II</head><head type="main_part">LA MORT DE L’AMOUR</head><div type="poem" key="BCH58">
					<head type="number">VII</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Moi</w>, <w n="1.2">par</w> <w n="1.3">la</w> <w n="1.4">neige</w> <w n="1.5">et</w> <w n="1.6">par</w> <w n="1.7">la</w> <w n="1.8">bise</w>,</l>
						<l n="2" num="1.2"><w n="2.1">A</w> <w n="2.2">tes</w> <w n="2.3">pieds</w> <w n="2.4">tremblant</w> <w n="2.5">et</w> <w n="2.6">joyeux</w> ;</l>
						<l n="3" num="1.3"><w n="3.1">Toi</w>, <w n="3.2">sur</w> <w n="3.3">un</w> <w n="3.4">banc</w> <w n="3.5">de</w> <w n="3.6">bois</w> <w n="3.7">assise</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Me</w> <w n="4.2">regardant</w> <w n="4.3">avec</w> <w n="4.4">tes</w> <w n="4.5">yeux</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Oh</w> ! <w n="5.2">tes</w> <w n="5.3">deux</w> <w n="5.4">yeux</w>, <w n="5.5">tes</w> <w n="5.6">deux</w> <w n="5.7">étoiles</w> !</l>
						<l n="6" num="2.2"><w n="6.1">Par</w> <w n="6.2">les</w> <w n="6.3">soirs</w> <w n="6.4">dont</w> <w n="6.5">je</w> <w n="6.6">me</w> <w n="6.7">souviens</w>,</l>
						<l n="7" num="2.3"><w n="7.1">De</w> <w n="7.2">tes</w> <w n="7.3">longs</w> <w n="7.4">cils</w> <w n="7.5">perçant</w> <w n="7.6">les</w> <w n="7.7">voiles</w>,</l>
						<l n="8" num="2.4"><w n="8.1">Tes</w> <w n="8.2">regards</w> <w n="8.3">tombaient</w> <w n="8.4">sur</w> <w n="8.5">les</w> <w n="8.6">miens</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Et</w> <w n="9.2">comme</w> <w n="9.3">au</w> <w n="9.4">vent</w> <w n="9.5">tremble</w> <w n="9.6">une</w> <w n="9.7">paille</w>,</l>
						<l n="10" num="3.2"><w n="10.1">Tout</w> <w n="10.2">seuls</w>, <w n="10.3">nous</w> <w n="10.4">tremblions</w> <w n="10.5">aussi</w> ;</l>
						<l n="11" num="3.3"><w n="11.1">Je</w> <w n="11.2">n</w>’<w n="11.3">osais</w> <w n="11.4">toucher</w> <w n="11.5">votre</w> <w n="11.6">taille</w>,</l>
						<l n="12" num="3.4"><w n="12.1">Nous</w> <w n="12.2">pâlissions</w> <w n="12.3">d</w>’<w n="12.4">aimer</w> <w n="12.5">ainsi</w> !</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Et</w> <w n="13.2">cependant</w> <w n="13.3">le</w> <w n="13.4">vent</w> <w n="13.5">d</w>’<w n="13.6">automne</w>,</l>
						<l n="14" num="4.2"><w n="14.1">L</w>’<w n="14.2">inconsolable</w> <w n="14.3">vent</w> <w n="14.4">des</w> <w n="14.5">nuits</w>,</l>
						<l n="15" num="4.3"><w n="15.1">Poursuivait</w> <w n="15.2">le</w> <w n="15.3">chant</w> <w n="15.4">monotone</w></l>
						<l n="16" num="4.4"><w n="16.1">Qu</w>’<w n="16.2">il</w> <w n="16.3">n</w>’<w n="16.4">a</w> <w n="16.5">pas</w> <w n="16.6">achevé</w> <w n="16.7">depuis</w>.</l>
					</lg>
				</div></body></text></TEI>