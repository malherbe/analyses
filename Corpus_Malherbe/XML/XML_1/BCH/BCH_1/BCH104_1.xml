<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES POËMES DE L’AMOUR ET DE LA MER</title>
				<title type="medium">Édition électronique</title>
				<author key="BCH">
					<name>
						<forename>Maurice</forename>
						<surname>Bouchor</surname>
					</name>
					<date from="1855" to="1929">1855-1929</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3472 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BCH_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
					<title>LES POËMES DE L’AMOUR ET DE LA MER</title>
					<author>Maurice Bouchor</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k9736743c</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
							<title>LES POËMES DE L’AMOUR ET DE LA MER</title>
							<author>Maurice Bouchor</author>
							<imprint>
								<pubPlace>Paris</pubPlace>
								<publisher>Charpentier et Cie, Libraires-éditeurs</publisher>
								<date when="1876">1876</date>
							</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1876">1876</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p></p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">III</head><head type="main_part">L’AMOUR DIVIN</head><div type="poem" key="BCH104">
					<head type="number">VI</head>
					<head type="main">AUTREFOIS</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Je</w> <w n="1.2">me</w> <w n="1.3">rappelle</w> <w n="1.4">un</w> <w n="1.5">soir</w> <w n="1.6">des</w> <w n="1.7">temps</w> <w n="1.8">où</w> <w n="1.9">j</w>’<w n="1.10">ai</w> <w n="1.11">vécu</w></l>
						<l n="2" num="1.2"><w n="2.1">Comme</w> <w n="2.2">un</w> <w n="2.3">autre</w>, <w n="2.4">laissant</w> <w n="2.5">s</w>’<w n="2.6">épanouir</w> <w n="2.7">mon</w> <w n="2.8">âme</w></l>
						<l n="3" num="1.3"><w n="3.1">Aux</w> <w n="3.2">sereines</w> <w n="3.3">clartés</w> <w n="3.4">des</w> <w n="3.5">beaux</w> <w n="3.6">yeux</w> <w n="3.7">d</w>’<w n="3.8">une</w> <w n="3.9">femme</w></l>
						<l n="4" num="1.4"><w n="4.1">Qui</w> <w n="4.2">m</w>’<w n="4.3">avait</w> <w n="4.4">regardé</w>, <w n="4.5">et</w> <w n="4.6">qui</w> <w n="4.7">m</w>’<w n="4.8">avait</w> <w n="4.9">vaincu</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Je</w> <w n="5.2">me</w> <w n="5.3">rappelle</w> <w n="5.4">un</w> <w n="5.5">soir</w> <w n="5.6">de</w> <w n="5.7">cette</w> <w n="5.8">époque</w> <w n="5.9">ancienne</w> ;</l>
						<l n="6" num="2.2"><w n="6.1">Au</w> <w n="6.2">brusque</w> <w n="6.3">vent</w> <w n="6.4">de</w> <w n="6.5">nuit</w> <w n="6.6">se</w> <w n="6.7">tordaient</w> <w n="6.8">ses</w> <w n="6.9">cheveux</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Et</w> <w n="7.2">je</w> <w n="7.3">la</w> <w n="7.4">suppliais</w> <w n="7.5">du</w> <w n="7.6">sourire</w> <w n="7.7">et</w> <w n="7.8">des</w> <w n="7.9">yeux</w>,</l>
						<l n="8" num="2.4"><w n="8.1">Et</w> <w n="8.2">ma</w> <w n="8.3">main</w> <w n="8.4">étreignait</w> <w n="8.5">si</w> <w n="8.6">doucement</w> <w n="8.7">la</w> <w n="8.8">sienne</w> !</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Pourtant</w>, <w n="9.2">bien</w> <w n="9.3">que</w> <w n="9.4">le</w> <w n="9.5">flot</w> <w n="9.6">murmurât</w> <w n="9.7">jusqu</w>’<w n="9.8">à</w> <w n="9.9">nous</w>,</l>
						<l n="10" num="3.2"><w n="10.1">Que</w> <w n="10.2">nous</w> <w n="10.3">eussions</w> <w n="10.4">vingt</w> <w n="10.5">ans</w> <w n="10.6">et</w> <w n="10.7">qu</w>’<w n="10.8">elle</w> <w n="10.9">fût</w> <w n="10.10">si</w> <w n="10.11">belle</w>,</l>
						<l n="11" num="3.3"><w n="11.1">Comme</w> <w n="11.2">un</w> <w n="11.3">ange</w> <w n="11.4">attristé</w> <w n="11.5">qui</w> <w n="11.6">referme</w> <w n="11.7">son</w> <w n="11.8">aile</w>,</l>
						<l n="12" num="3.4"><w n="12.1">Elle</w> <w n="12.2">ne</w> <w n="12.3">me</w> <w n="12.4">dit</w> <w n="12.5">rien</w>, <w n="12.6">ce</w> <w n="12.7">soir</w> <w n="12.8">de</w> <w n="12.9">rendez</w>-<w n="12.10">vous</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Si</w> <w n="13.2">ta</w> <w n="13.3">main</w> <w n="13.4">tout</w> <w n="13.5">à</w> <w n="13.6">coup</w>, <w n="13.7">chère</w> <w n="13.8">âme</w>, <w n="13.9">fut</w> <w n="13.10">glacée</w>,</l>
						<l n="14" num="4.2"><w n="14.1">Si</w> <w n="14.2">ta</w> <w n="14.3">bouche</w> <w n="14.4">perdit</w> <w n="14.5">son</w> <w n="14.6">rire</w> <w n="14.7">et</w> <w n="14.8">ses</w> <w n="14.9">baisers</w>,</l>
						<l n="15" num="4.3"><w n="15.1">C</w>’<w n="15.2">est</w> <w n="15.3">qu</w>’<w n="15.4">un</w> <w n="15.5">frisson</w> <w n="15.6">mortel</w> <w n="15.7">nous</w> <w n="15.8">ayant</w> <w n="15.9">traversés</w>,</l>
						<l n="16" num="4.4"><w n="16.1">Nous</w> <w n="16.2">eûmes</w> <w n="16.3">tous</w> <w n="16.4">les</w> <w n="16.5">deux</w> <w n="16.6">une</w> <w n="16.7">même</w> <w n="16.8">pensée</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">«<w n="17.1">La</w> <w n="17.2">mort</w> <w n="17.3">inévitable</w> <w n="17.4">et</w> <w n="17.5">la</w> <w n="17.6">fatale</w> <w n="17.7">nuit</w></l>
						<l n="18" num="5.2"><w n="18.1">Qui</w> <w n="18.2">devait</w>, <w n="18.3">tôt</w> <w n="18.4">ou</w> <w n="18.5">tard</w>, <w n="18.6">peser</w> <w n="18.7">sur</w> <w n="18.8">nos</w> <w n="18.9">paupières</w></l>
						<l n="19" num="5.3"><w n="19.1">Et</w> <w n="19.2">l</w>’<w n="19.3">éternel</w> <w n="19.4">sommeil</w> <w n="19.5">que</w> <w n="19.6">l</w>’<w n="19.7">on</w> <w n="19.8">dort</w> <w n="19.9">sur</w> <w n="19.10">les</w> <w n="19.11">pierres</w></l>
						<l n="20" num="5.4"><w n="20.1">Dans</w> <w n="20.2">la</w> <w n="20.3">vallée</w> <w n="20.4">où</w> <w n="20.5">nul</w> <w n="20.6">soleil</w> <w n="20.7">n</w>’<w n="20.8">a</w> <w n="20.9">jamais</w> <w n="20.10">lui</w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Cette</w> <w n="21.2">fin</w> <w n="21.3">de</w> <w n="21.4">l</w>’<w n="21.5">amour</w>, <w n="21.6">comme</w> <w n="21.7">de</w> <w n="21.8">toutes</w> <w n="21.9">choses</w>,</l>
						<l n="22" num="6.2"><w n="22.1">Ce</w> <w n="22.2">silence</w> <w n="22.3">des</w> <w n="22.4">cœurs</w> <w n="22.5">qui</w> <w n="22.6">battaient</w> <w n="22.7">autrefois</w>…</l>
						<l n="23" num="6.3"><w n="23.1">Puis</w>, <w n="23.2">autour</w> <w n="23.3">du</w> <w n="23.4">tombeau</w> <w n="23.5">des</w> <w n="23.6">pas</w>, <w n="23.7">des</w> <w n="23.8">bruits</w> <w n="23.9">de</w> <w n="23.10">voix</w>,</l>
						<l n="24" num="6.4"><w n="24.1">Et</w> <w n="24.2">le</w> <w n="24.3">suprême</w> <w n="24.4">oubli</w> <w n="24.5">tout</w> <w n="24.6">parfumé</w> <w n="24.7">de</w> <w n="24.8">roses</w> !</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">La</w> <w n="25.2">foi</w> <w n="25.3">des</w> <w n="25.4">jours</w> <w n="25.5">anciens</w> <w n="25.6">a</w> <w n="25.7">fini</w> <w n="25.8">par</w> <w n="25.9">tarir</w>,</l>
						<l n="26" num="7.2"><w n="26.1">Nul</w> <w n="26.2">ne</w> <w n="26.3">pense</w> <w n="26.4">bondir</w> <w n="26.5">jusqu</w>’<w n="26.6">aux</w> <w n="26.7">cieux</w> <w n="26.8">d</w>’<w n="26.9">un</w> <w n="26.10">coup</w> <w n="26.11">d</w>’<w n="26.12">aile</w> ;</l>
						<l n="27" num="7.3"><w n="27.1">Et</w> <w n="27.2">ne</w> <w n="27.3">comprenant</w> <w n="27.4">rien</w> <w n="27.5">à</w> <w n="27.6">la</w> <w n="27.7">vie</w> <w n="27.8">immortelle</w>,</l>
						<l n="28" num="7.4"><w n="28.1">Chaque</w> <w n="28.2">jour</w>, <w n="28.3">en</w> <w n="28.4">vivant</w>, <w n="28.5">nous</w> <w n="28.6">nous</w> <w n="28.7">sentons</w> <w n="28.8">mourir</w>.</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1"><w n="29.1">Ainsi</w>, <w n="29.2">rien</w> <w n="29.3">ne</w> <w n="29.4">devait</w> <w n="29.5">rester</w> <w n="29.6">de</w> <w n="29.7">notre</w> <w n="29.8">extase</w> !</l>
						<l n="30" num="8.2"><w n="30.1">Nos</w> <w n="30.2">jours</w> <w n="30.3">délicieux</w> <w n="30.4">devaient</w> <w n="30.5">donc</w> <w n="30.6">s</w>’<w n="30.7">échapper</w></l>
						<l n="31" num="8.3"><w n="31.1">Comme</w>, <w n="31.2">goutte</w> <w n="31.3">après</w> <w n="31.4">goutte</w>, <w n="31.5">et</w> <w n="31.6">pour</w> <w n="31.7">se</w> <w n="31.8">dissiper</w>,</l>
						<l n="32" num="8.4"><w n="32.1">L</w>’<w n="32.2">eau</w> <w n="32.3">s</w>’<w n="32.4">échappe</w> <w n="32.5">à</w> <w n="32.6">travers</w> <w n="32.7">les</w> <w n="32.8">fêlures</w> <w n="32.9">d</w>’<w n="32.10">un</w> <w n="32.11">vase</w> !</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1"><w n="33.1">Et</w> <w n="33.2">tristes</w>, <w n="33.3">nous</w> <w n="33.4">songions</w>. <w n="33.5">Le</w> <w n="33.6">sifflement</w> <w n="33.7">aigu</w></l>
						<l n="34" num="9.2"><w n="34.1">Des</w> <w n="34.2">bises</w> <w n="34.3">se</w> <w n="34.4">mêlait</w> <w n="34.5">à</w> <w n="34.6">la</w> <w n="34.7">clameur</w> <w n="34.8">des</w> <w n="34.9">vagues</w> ;</l>
						<l n="35" num="9.3"><w n="35.1">Et</w> <w n="35.2">maintenant</w>, <w n="35.3">perdu</w> <w n="35.4">dans</w> <w n="35.5">des</w> <w n="35.6">souvenirs</w> <w n="35.7">vagues</w>,</l>
						<l n="36" num="9.4"><w n="36.1">Je</w> <w n="36.2">me</w> <w n="36.3">rappelle</w> <w n="36.4">un</w> <w n="36.5">soir</w> <w n="36.6">du</w> <w n="36.7">temps</w> <w n="36.8">où</w> <w n="36.9">j</w>’<w n="36.10">ai</w> <w n="36.11">vécu</w>.</l>
					</lg>
				</div></body></text></TEI>