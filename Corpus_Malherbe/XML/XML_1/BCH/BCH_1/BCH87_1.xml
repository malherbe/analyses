<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES POËMES DE L’AMOUR ET DE LA MER</title>
				<title type="medium">Édition électronique</title>
				<author key="BCH">
					<name>
						<forename>Maurice</forename>
						<surname>Bouchor</surname>
					</name>
					<date from="1855" to="1929">1855-1929</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3472 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BCH_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
					<title>LES POËMES DE L’AMOUR ET DE LA MER</title>
					<author>Maurice Bouchor</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k9736743c</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
							<title>LES POËMES DE L’AMOUR ET DE LA MER</title>
							<author>Maurice Bouchor</author>
							<imprint>
								<pubPlace>Paris</pubPlace>
								<publisher>Charpentier et Cie, Libraires-éditeurs</publisher>
								<date when="1876">1876</date>
							</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1876">1876</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p></p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">II</head><head type="main_part">LA MORT DE L’AMOUR</head><div type="poem" key="BCH87">
					<head type="number">XXXIII</head>
					<head type="main">LA SYMPHONIE DES SANGLOTS</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">J</w>’<w n="1.2">ai</w> <w n="1.3">couru</w> <w n="1.4">comme</w> <w n="1.5">un</w> <w n="1.6">fou</w> <w n="1.7">sur</w> <w n="1.8">la</w> <w n="1.9">plage</w> <w n="1.10">déserte</w> ;</l>
						<l n="2" num="1.2"><w n="2.1">Et</w>, <w n="2.2">secouant</w> <w n="2.3">au</w> <w n="2.4">vent</w> <w n="2.5">leur</w> <w n="2.6">chevelure</w> <w n="2.7">verte</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Les</w> <w n="3.2">nymphes</w> <w n="3.3">de</w> <w n="3.4">la</w> <w n="3.5">mer</w>, <w n="3.6">furieuses</w>, <w n="3.7">hurlaient</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Et</w> <w n="4.2">les</w> <w n="4.3">bouches</w> <w n="4.4">du</w> <w n="4.5">vent</w> <w n="4.6">éperdument</w> <w n="4.7">soufflaient</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">On</w> <w n="5.2">entendait</w> <w n="5.3">au</w> <w n="5.4">loin</w> <w n="5.5">un</w> <w n="5.6">immense</w> <w n="5.7">murmure</w> ;</l>
						<l n="6" num="2.2"><w n="6.1">Les</w> <w n="6.2">arbres</w> <w n="6.3">secouaient</w>, <w n="6.4">sinistres</w>, <w n="6.5">leur</w> <w n="6.6">ramure</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Et</w> <w n="7.2">leurs</w> <w n="7.3">bruissements</w> <w n="7.4">sonores</w> <w n="7.5">et</w> <w n="7.6">profonds</w></l>
						<l n="8" num="2.4"><w n="8.1">Se</w> <w n="8.2">mêlaient</w> <w n="8.3">aux</w> <w n="8.4">sanglots</w> <w n="8.5">des</w> <w n="8.6">mers</w>. — <w n="8.7">Et</w> <w n="8.8">que</w> <w n="8.9">me</w> <w n="8.10">font</w></l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Ces</w> <w n="9.2">milliers</w> <w n="9.3">de</w> <w n="9.4">voix</w> <w n="9.5">pleurant</w> <w n="9.6">dans</w> <w n="9.7">les</w> <w n="9.8">ténèbres</w> ?</l>
						<l n="10" num="3.2"><w n="10.1">Et</w> <w n="10.2">ce</w> <w n="10.3">lugubre</w> <w n="10.4">oiseau</w> <w n="10.5">poussant</w> <w n="10.6">des</w> <w n="10.7">cris</w> <w n="10.8">funèbres</w></l>
						<l n="11" num="3.3"><w n="11.1">Qui</w> <w n="11.2">feraient</w> <w n="11.3">se</w> <w n="11.4">lever</w>, <w n="11.5">de</w> <w n="11.6">leurs</w> <w n="11.7">tombeaux</w>, <w n="11.8">les</w> <w n="11.9">morts</w>,</l>
						<l n="12" num="3.4"><w n="12.1">Comme</w> <w n="12.2">s</w>’<w n="12.3">ils</w> <w n="12.4">entendaient</w> <w n="12.5">la</w> <w n="12.6">voix</w> <w n="12.7">de</w> <w n="12.8">leurs</w> <w n="12.9">remords</w> ?</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Car</w> <w n="13.2">je</w> <w n="13.3">souffre</w> <w n="13.4">d</w>’<w n="13.5">un</w> <w n="13.6">mal</w> <w n="13.7">plus</w> <w n="13.8">terrible</w> <w n="13.9">et</w> <w n="13.10">plus</w> <w n="13.11">sombre</w></l>
						<l n="14" num="4.2"><w n="14.1">Que</w> <w n="14.2">ces</w> <w n="14.3">cris</w> <w n="14.4">d</w>’<w n="14.5">épouvante</w> <w n="14.6">et</w> <w n="14.7">que</w> <w n="14.8">toute</w> <w n="14.9">cette</w> <w n="14.10">ombre</w></l>
						<l n="15" num="4.3"><w n="15.1">Où</w> <w n="15.2">souffre</w> <w n="15.3">et</w> <w n="15.4">se</w> <w n="15.5">lamente</w> <w n="15.6">et</w> <w n="15.7">roule</w> <w n="15.8">des</w> <w n="15.9">sanglots</w></l>
						<l n="16" num="4.4"><w n="16.1">L</w>’<w n="16.2">immortelle</w> <w n="16.3">nature</w> <w n="16.4">en</w> <w n="16.5">proie</w> <w n="16.6">au</w> <w n="16.7">noir</w> <w n="16.8">chaos</w></l>
					</lg>
					<ab type="star">⁂</ab>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Eh</w> <w n="17.2">bien</w>, <w n="17.3">déchaînez</w>-<w n="17.4">vous</w>, <w n="17.5">tempêtes</w> ! <w n="17.6">dans</w> <w n="17.7">la</w> <w n="17.8">brume</w></l>
						<l n="18" num="5.2"><w n="18.1">Que</w> <w n="18.2">la</w> <w n="18.3">lune</w> <w n="18.4">apparaisse</w> <w n="18.5">un</w> <w n="18.6">nimbe</w> <w n="18.7">rouge</w> <w n="18.8">au</w> <w n="18.9">front</w>,</l>
						<l n="19" num="5.3"><w n="19.1">Et</w> <w n="19.2">de</w> <w n="19.3">l</w>’<w n="19.4">Océan</w> <w n="19.5">morne</w> <w n="19.6">ensanglante</w> <w n="19.7">l</w>’<w n="19.8">écume</w> !</l>
					</lg>
					<lg n="6">
						<l n="20" num="6.1"><w n="20.1">Que</w> <w n="20.2">le</w> <w n="20.3">vent</w> <w n="20.4">essoufflé</w> <w n="20.5">donne</w> <w n="20.6">de</w> <w n="20.7">l</w>’<w n="20.8">éperon</w></l>
						<l n="21" num="6.2"><w n="21.1">Au</w> <w n="21.2">troupeau</w> <w n="21.3">dispersé</w> <w n="21.4">des</w> <w n="21.5">fuyardes</w> <w n="21.6">nuées</w>,</l>
						<l n="22" num="6.3"><w n="22.1">Et</w> <w n="22.2">souffle</w> <w n="22.3">à</w> <w n="22.4">pleins</w> <w n="22.5">poumons</w> <w n="22.6">au</w> <w n="22.7">fond</w> <w n="22.8">de</w> <w n="22.9">son</w> <w n="22.10">clairon</w> !</l>
					</lg>
					<lg n="7">
						<l n="23" num="7.1"><w n="23.1">Et</w> <w n="23.2">vous</w>, <w n="23.3">les</w> <w n="23.4">yeux</w> <w n="23.5">hagards</w>, <w n="23.6">pâles</w> <w n="23.7">prostituées</w></l>
						<l n="24" num="7.2"><w n="24.1">Qu</w>’<w n="24.2">à</w> <w n="24.3">la</w> <w n="24.4">gorge</w> <w n="24.5">saisit</w> <w n="24.6">la</w> <w n="24.7">faim</w>, <w n="24.8">venez</w> <w n="24.9">pleurer</w></l>
						<l n="25" num="7.3"><w n="25.1">Vos</w> <w n="25.2">âmes</w> <w n="25.3">que</w> <w n="25.4">la</w> <w n="25.5">honte</w> <w n="25.6">en</w> <w n="25.7">vos</w> <w n="25.8">corps</w> <w n="25.9">a</w> <w n="25.10">tuées</w>.</l>
					</lg>
					<lg n="8">
						<l n="26" num="8.1"><w n="26.1">Amants</w> <w n="26.2">abandonnés</w>, <w n="26.3">venez</w> <w n="26.4">tous</w> <w n="26.5">soupirer</w> ;</l>
						<l n="27" num="8.2"><w n="27.1">Et</w> <w n="27.2">toi</w> <w n="27.3">que</w> <w n="27.4">délaissa</w> <w n="27.5">Don</w> <w n="27.6">Juan</w>, <w n="27.7">infortunée</w></l>
						<l n="28" num="8.3"><w n="28.1">Qui</w> <w n="28.2">devenais</w> <w n="28.3">déesse</w> <w n="28.4">à</w> <w n="28.5">t</w>’<w n="28.6">en</w> <w n="28.7">faire</w> <w n="28.8">adorer</w> !</l>
					</lg>
					<lg n="9">
						<l n="29" num="9.1"><w n="29.1">Et</w> <w n="29.2">toi</w>, <w n="29.3">triste</w> <w n="29.4">inventeur</w> <w n="29.5">dont</w> <w n="29.6">la</w> <w n="29.7">tête</w> <w n="29.8">inclinée</w></l>
						<l n="30" num="9.2"><w n="30.1">Sous</w> <w n="30.2">la</w> <w n="30.3">lampe</w> <w n="30.4">est</w> <w n="30.5">ridée</w> <w n="30.6">et</w> <w n="30.7">blême</w> <w n="30.8">à</w> <w n="30.9">faire</w> <w n="30.10">peur</w>,</l>
						<l n="31" num="9.3"><w n="31.1">Maudis</w> <w n="31.2">l</w>’<w n="31.3">inexplicable</w> <w n="31.4">et</w> <w n="31.5">sourde</w> <w n="31.6">destinée</w>.</l>
					</lg>
					<lg n="10">
						<l n="32" num="10.1"><w n="32.1">Roulant</w> <w n="32.2">dans</w> <w n="32.3">l</w>’<w n="32.4">étendue</w> <w n="32.5">ainsi</w> <w n="32.6">qu</w>’<w n="32.7">une</w> <w n="32.8">vapeur</w>,</l>
						<l n="33" num="10.2"><w n="33.1">Mêlez</w>-<w n="33.2">vous</w>, <w n="33.3">pressez</w>-<w n="33.4">vous</w>, <w n="33.5">âmes</w> <w n="33.6">désespérées</w>,</l>
						<l n="34" num="10.3"><w n="34.1">Légion</w> <w n="34.2">des</w> <w n="34.3">vivants</w> <w n="34.4">que</w> <w n="34.5">marqua</w> <w n="34.6">la</w> <w n="34.7">Douleur</w> !</l>
					</lg>
					<lg n="11">
						<l n="35" num="11.1"><w n="35.1">Que</w> <w n="35.2">le</w> <w n="35.3">gémissement</w> <w n="35.4">des</w> <w n="35.5">vagues</w> <w n="35.6">éplorées</w>,</l>
						<l n="36" num="11.2"><w n="36.1">Que</w> <w n="36.2">les</w> <w n="36.3">clameurs</w> <w n="36.4">du</w> <w n="36.5">vent</w> <w n="36.6">qui</w> <w n="36.7">passe</w> <w n="36.8">comme</w> <w n="36.9">un</w> <w n="36.10">fou</w></l>
						<l n="37" num="11.3"><w n="37.1">Et</w> <w n="37.2">que</w> <w n="37.3">toutes</w> <w n="37.4">les</w> <w n="37.5">voix</w> <w n="37.6">dans</w> <w n="37.7">la</w> <w n="37.8">nuit</w> <w n="37.9">égarées</w></l>
					</lg>
					<lg n="12">
						<l n="38" num="12.1"><w n="38.1">Pour</w> <w n="38.2">mieux</w> <w n="38.3">vous</w> <w n="38.4">écouter</w> <w n="38.5">se</w> <w n="38.6">taisent</w> <w n="38.7">tout</w> <w n="38.8">à</w> <w n="38.9">coup</w> ;</l>
						<l n="39" num="12.2"><w n="39.1">Et</w> <w n="39.2">laissez</w>, <w n="39.3">malheureux</w> <w n="39.4">qu</w>’<w n="39.5">a</w> <w n="39.6">reniés</w> <w n="39.7">la</w> <w n="39.8">terre</w>,</l>
						<l n="40" num="12.3"><w n="40.1">Vos</w> <w n="40.2">lamentations</w> <w n="40.3">monter</w> <w n="40.4">je</w> <w n="40.5">ne</w> <w n="40.6">sais</w> <w n="40.7">où</w> !</l>
					</lg>
					<ab type="star">⁂</ab>
					<lg n="13">
						<l n="41" num="13.1"><w n="41.1">Et</w> <w n="41.2">l</w>’<w n="41.3">on</w> <w n="41.4">eût</w> <w n="41.5">dit</w> <w n="41.6">vraiment</w> <w n="41.7">que</w> <w n="41.8">toute</w> <w n="41.9">la</w> <w n="41.10">Matière</w></l>
						<l n="42" num="13.2"><w n="42.1">Se</w> <w n="42.2">levait</w> <w n="42.3">dans</w> <w n="42.4">la</w> <w n="42.5">nuit</w> <w n="42.6">pour</w> <w n="42.7">maudire</w> <w n="42.8">son</w> <w n="42.9">Dieu</w> :</l>
						<l n="43" num="13.3"><w n="43.1">Les</w> <w n="43.2">vagues</w> <w n="43.3">bondissaient</w> <w n="43.4">vers</w> <w n="43.5">une</w> <w n="43.6">lune</w> <w n="43.7">en</w> <w n="43.8">feu</w>,</l>
						<l n="44" num="13.4"><w n="44.1">A</w> <w n="44.2">la</w> <w n="44.3">face</w> <w n="44.4">du</w> <w n="44.5">ciel</w> <w n="44.6">crachant</w> <w n="44.7">leur</w> <w n="44.8">froide</w> <w n="44.9">écume</w> ;</l>
						<l n="45" num="13.5"><w n="45.1">Et</w> <w n="45.2">le</w> <w n="45.3">vent</w>, <w n="45.4">comme</w> <w n="45.5">un</w> <w n="45.6">cœur</w> <w n="45.7">que</w> <w n="45.8">le</w> <w n="45.9">regret</w> <w n="45.10">consume</w>,</l>
						<l n="46" num="13.6"><w n="46.1">Tantôt</w> <w n="46.2">poussait</w> <w n="46.3">des</w> <w n="46.4">cris</w> <w n="46.5">et</w> <w n="46.6">tantôt</w> <w n="46.7">des</w> <w n="46.8">sanglots</w>.</l>
					</lg>
					<lg n="14">
						<l n="47" num="14.1"><w n="47.1">Ah</w> ! <w n="47.2">qui</w> <w n="47.3">pouvait</w> <w n="47.4">songer</w> <w n="47.5">aux</w> <w n="47.6">pauvres</w> <w n="47.7">matelots</w> ?</l>
						<l n="48" num="14.2"><w n="48.1">J</w>’<w n="48.2">avais</w> <w n="48.3">là</w> <w n="48.4">devant</w> <w n="48.5">moi</w>, <w n="48.6">sortis</w> <w n="48.7">de</w> <w n="48.8">dessous</w> <w n="48.9">terre</w>,</l>
						<l n="49" num="14.3"><w n="49.1">Ceux</w> <w n="49.2">que</w> <w n="49.3">toute</w> <w n="49.4">la</w> <w n="49.5">vie</w> <w n="49.6">étreindra</w> <w n="49.7">la</w> <w n="49.8">misère</w>,</l>
						<l n="50" num="14.4"><w n="50.1">Que</w> <w n="50.2">la</w> <w n="50.3">mélancolie</w> <w n="50.4">a</w> <w n="50.5">rongés</w> <w n="50.6">jusqu</w>’<w n="50.7">aux</w> <w n="50.8">os</w></l>
						<l n="51" num="14.5"><w n="51.1">Ou</w> <w n="51.2">qu</w>’<w n="51.3">un</w> <w n="51.4">chagrin</w> <w n="51.5">d</w>’<w n="51.6">amour</w> <w n="51.7">étouffe</w> <w n="51.8">en</w> <w n="51.9">ses</w> <w n="51.10">réseaux</w> ;</l>
						<l n="52" num="14.6"><w n="52.1">Et</w> <w n="52.2">pendant</w> <w n="52.3">cette</w> <w n="52.4">nuit</w> <w n="52.5">affreuse</w> <w n="52.6">et</w> <w n="52.7">tourmentée</w></l>
						<l n="53" num="14.7"><w n="53.1">Où</w> <w n="53.2">la</w> <w n="53.3">voix</w> <w n="53.4">de</w> <w n="53.5">la</w> <w n="53.6">mer</w> <w n="53.7">hurlait</w> <w n="53.8">épouvantée</w>,</l>
						<l n="54" num="14.8"><w n="54.1">Poussés</w> <w n="54.2">vers</w> <w n="54.3">moi</w> <w n="54.4">par</w> <w n="54.5">un</w> <w n="54.6">sombre</w> <w n="54.7">vent</w> <w n="54.8">de</w> <w n="54.9">douleur</w>,</l>
						<l n="55" num="14.9"><w n="55.1">Tous</w> <w n="55.2">les</w> <w n="55.3">désespérés</w> <w n="55.4">ont</w> <w n="55.5">pleuré</w> <w n="55.6">sur</w> <w n="55.7">mon</w> <w n="55.8">cœur</w>.</l>
					</lg>
				</div></body></text></TEI>