<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES POËMES DE L’AMOUR ET DE LA MER</title>
				<title type="medium">Édition électronique</title>
				<author key="BCH">
					<name>
						<forename>Maurice</forename>
						<surname>Bouchor</surname>
					</name>
					<date from="1855" to="1929">1855-1929</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3472 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BCH_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
					<title>LES POËMES DE L’AMOUR ET DE LA MER</title>
					<author>Maurice Bouchor</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k9736743c</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
							<title>LES POËMES DE L’AMOUR ET DE LA MER</title>
							<author>Maurice Bouchor</author>
							<imprint>
								<pubPlace>Paris</pubPlace>
								<publisher>Charpentier et Cie, Libraires-éditeurs</publisher>
								<date when="1876">1876</date>
							</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1876">1876</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p></p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">I</head><head type="main_part">LA FLEUR DES EAUX</head><div type="poem" key="BCH15">
					<head type="number">XIV</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Amour</w>, <w n="1.2">pensers</w> <w n="1.3">d</w>’<w n="1.4">amour</w>, <w n="1.5">rêves</w> <w n="1.6">subtils</w> <w n="1.7">et</w> <w n="1.8">doux</w>,</l>
						<l n="2" num="1.2"><w n="2.1">De</w> <w n="2.2">l</w>’<w n="2.3">air</w> <w n="2.4">tiède</w> <w n="2.5">du</w> <w n="2.6">soir</w> <w n="2.7">flottantes</w> <w n="2.8">rêveries</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Roses</w> <w n="3.2">qui</w> <w n="3.3">parfumez</w> <w n="3.4">nos</w> <w n="3.5">visions</w> <w n="3.6">fleuries</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Venez</w>-<w n="4.2">vous</w> <w n="4.3">en</w> <w n="4.4">vers</w> <w n="4.5">nous</w> <w n="4.6">qu</w>’<w n="4.7">on</w> <w n="4.8">appelle</w> <w n="4.9">des</w> <w n="4.10">fous</w> !</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Fous</w> <w n="5.2">de</w> <w n="5.3">joie</w> <w n="5.4">et</w> <w n="5.5">d</w>’<w n="5.6">amour</w> ! — <w n="5.7">Moi</w>, <w n="5.8">j</w>’<w n="5.9">étais</w> <w n="5.10">à</w> <w n="5.11">genoux</w>,</l>
						<l n="6" num="2.2"><w n="6.1">Les</w> <w n="6.2">mains</w> <w n="6.3">jointes</w>, <w n="6.4">comptant</w> <w n="6.5">les</w> <w n="6.6">secondes</w> <w n="6.7">bénies</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Et</w> <w n="7.2">parfois</w>, <w n="7.3">m</w>’<w n="7.4">éveillant</w> <w n="7.5">d</w>’<w n="7.6">extases</w> <w n="7.7">infinies</w>,</l>
						<l n="8" num="2.4"><w n="8.1">Du</w> <w n="8.2">vent</w> <w n="8.3">qui</w> <w n="8.4">vous</w> <w n="8.5">baisait</w> <w n="8.6">je</w> <w n="8.7">me</w> <w n="8.8">sentais</w> <w n="8.9">jaloux</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Que</w> <w n="9.2">se</w> <w n="9.3">passait</w>-<w n="9.4">il</w> <w n="9.5">donc</w> <w n="9.6">en</w> <w n="9.7">ces</w> <w n="9.8">deux</w> <w n="9.9">âmes</w> <w n="9.10">vierges</w> ?</l>
						<l n="10" num="3.2"><w n="10.1">Mais</w> <w n="10.2">les</w> <w n="10.3">étoiles</w> <w n="10.4">d</w>’<w n="10.5">or</w> <w n="10.6">luisaient</w> <w n="10.7">comme</w> <w n="10.8">des</w> <w n="10.9">cierges</w> ;</l>
						<l n="11" num="3.3"><w n="11.1">La</w> <w n="11.2">brise</w> <w n="11.3">soupirait</w> <w n="11.4">le</w> <w n="11.5">lied</w> <w n="11.6">aérien</w></l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1"><w n="12.1">D</w>’<w n="12.2">une</w> <w n="12.3">mélancolique</w> <w n="12.4">et</w> <w n="12.5">discrète</w> <w n="12.6">infortune</w> ;</l>
						<l n="13" num="4.2"><w n="13.1">La</w> <w n="13.2">mer</w> <w n="13.3">calme</w> <w n="13.4">venait</w> <w n="13.5">mourir</w> <w n="13.6">au</w> <w n="13.7">clair</w> <w n="13.8">de</w> <w n="13.9">lune</w>,</l>
						<l n="14" num="4.3"><w n="14.1">Et</w> <w n="14.2">nous</w> <w n="14.3">nous</w> <w n="14.4">aimions</w> <w n="14.5">tant</w> <w n="14.6">que</w> <w n="14.7">nous</w> <w n="14.8">ne</w> <w n="14.9">disions</w> <w n="14.10">rien</w>.</l>
					</lg>
				</div></body></text></TEI>