<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES POËMES DE L’AMOUR ET DE LA MER</title>
				<title type="medium">Édition électronique</title>
				<author key="BCH">
					<name>
						<forename>Maurice</forename>
						<surname>Bouchor</surname>
					</name>
					<date from="1855" to="1929">1855-1929</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3472 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BCH_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
					<title>LES POËMES DE L’AMOUR ET DE LA MER</title>
					<author>Maurice Bouchor</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k9736743c</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
							<title>LES POËMES DE L’AMOUR ET DE LA MER</title>
							<author>Maurice Bouchor</author>
							<imprint>
								<pubPlace>Paris</pubPlace>
								<publisher>Charpentier et Cie, Libraires-éditeurs</publisher>
								<date when="1876">1876</date>
							</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1876">1876</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p></p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">II</head><head type="main_part">LA MORT DE L’AMOUR</head><div type="poem" key="BCH53">
					<head type="number">II</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Paris</w>, <w n="1.2">terrible</w> <w n="1.3">et</w> <w n="1.4">grand</w> — <w n="1.5">aussi</w> <w n="1.6">grand</w> <w n="1.7">que</w> <w n="1.8">la</w> <w n="1.9">mer</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Où</w> <w n="2.2">tous</w> <w n="2.3">les</w> <w n="2.4">cris</w> <w n="2.5">humains</w> <w n="2.6">font</w> <w n="2.7">comme</w> <w n="2.8">une</w> <w n="2.9">tempête</w> !</l>
						<l n="3" num="1.3"><w n="3.1">Mais</w> <w n="3.2">mon</w> <w n="3.3">cœur</w> <w n="3.4">étouffé</w> <w n="3.5">gémit</w>, <w n="3.6">souffre</w> <w n="3.7">et</w> <w n="3.8">regrette</w> ;</l>
						<l n="4" num="1.4"><w n="4.1">Et</w> <w n="4.2">plaisir</w> <w n="4.3">et</w> <w n="4.4">travail</w> <w n="4.5">me</w> <w n="4.6">sont</w> <w n="4.7">un</w> <w n="4.8">double</w> <w n="4.9">enfer</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Je</w> <w n="5.2">veux</w> <w n="5.3">poser</w> <w n="5.4">ma</w> <w n="5.5">tête</w> <w n="5.6">en</w> <w n="5.7">mes</w> <w n="5.8">mains</w>, <w n="5.9">et</w> <w n="5.10">rêver</w></l>
						<l n="6" num="2.2"><w n="6.1">A</w> <w n="6.2">nos</w> <w n="6.3">mois</w> <w n="6.4">d</w>’<w n="6.5">amour</w>, <w n="6.6">pleins</w> <w n="6.7">de</w> <w n="6.8">volupté</w> <w n="6.9">discrète</w>.</l>
						<l n="7" num="2.3"><w n="7.1">Mon</w> <w n="7.2">âme</w>, <w n="7.3">barque</w> <w n="7.4">en</w> <w n="7.5">deuil</w>, <w n="7.6">lève</w> <w n="7.7">l</w>’<w n="7.8">ancre</w> <w n="7.9">et</w> <w n="7.10">s</w>’<w n="7.11">apprête</w></l>
						<l n="8" num="2.4"><w n="8.1">A</w> <w n="8.2">faire</w> <w n="8.3">voile</w> <w n="8.4">au</w> <w n="8.5">loin</w> <w n="8.6">vers</w> <w n="8.7">un</w> <w n="8.8">charmant</w> <w n="8.9">hiver</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Mon</w> <w n="9.2">esprit</w> <w n="9.3">orageux</w> <w n="9.4">a</w> <w n="9.5">calmé</w> <w n="9.6">sa</w> <w n="9.7">tourmente</w> ;</l>
						<l n="10" num="3.2"><w n="10.1">La</w> <w n="10.2">mer</w> <w n="10.3">du</w> <w n="10.4">souvenir</w> <w n="10.5">sourit</w>, <w n="10.6">lisse</w> <w n="10.7">et</w> <w n="10.8">dormante</w> —</l>
						<l n="11" num="3.3"><w n="11.1">Le</w> <w n="11.2">clair</w> <w n="11.3">de</w> <w n="11.4">lune</w> <w n="11.5">glisse</w> <w n="11.6">à</w> <w n="11.7">travers</w> <w n="11.8">le</w> <w n="11.9">ciel</w> <w n="11.10">noir</w>.</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1"><w n="12.1">Quand</w> <w n="12.2">verrai</w>-<w n="12.3">je</w>, <w n="12.4">ô</w> <w n="12.5">Passé</w>, <w n="12.6">dans</w> <w n="12.7">la</w> <w n="12.8">brume</w> <w n="12.9">indécise</w></l>
						<l n="13" num="4.2"><w n="13.1">Se</w> <w n="13.2">dessiner</w> <w n="13.3">ton</w> <w n="13.4">cher</w> <w n="13.5">et</w> <w n="13.6">douloureux</w> <w n="13.7">manoir</w>,</l>
						<l n="14" num="4.3"><w n="14.1">Et</w> <w n="14.2">l</w>’<w n="14.3">âme</w> <w n="14.4">des</w> <w n="14.5">vieux</w> <w n="14.6">jours</w> <w n="14.7">devant</w> <w n="14.8">ta</w> <w n="14.9">porte</w> <w n="14.10">assise</w> ?</l>
					</lg>
				</div></body></text></TEI>