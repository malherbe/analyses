<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES POËMES DE L’AMOUR ET DE LA MER</title>
				<title type="medium">Édition électronique</title>
				<author key="BCH">
					<name>
						<forename>Maurice</forename>
						<surname>Bouchor</surname>
					</name>
					<date from="1855" to="1929">1855-1929</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3472 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BCH_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
					<title>LES POËMES DE L’AMOUR ET DE LA MER</title>
					<author>Maurice Bouchor</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k9736743c</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
							<title>LES POËMES DE L’AMOUR ET DE LA MER</title>
							<author>Maurice Bouchor</author>
							<imprint>
								<pubPlace>Paris</pubPlace>
								<publisher>Charpentier et Cie, Libraires-éditeurs</publisher>
								<date when="1876">1876</date>
							</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1876">1876</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p></p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">I</head><head type="main_part">LA FLEUR DES EAUX</head><div type="poem" key="BCH3">
					<head type="number">II</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Je</w> <w n="1.2">m</w>’<w n="1.3">étais</w> <w n="1.4">enivré</w> <w n="1.5">d</w>’<w n="1.6">espace</w> <w n="1.7">et</w> <w n="1.8">de</w> <w n="1.9">ciel</w> <w n="1.10">bleu</w> ;</l>
						<l n="2" num="1.2"><w n="2.1">Tout</w> <w n="2.2">ébloui</w>, <w n="2.3">j</w>’<w n="2.4">avais</w> <w n="2.5">sur</w> <w n="2.6">l</w>’<w n="2.7">infini</w> <w n="2.8">des</w> <w n="2.9">ondes</w></l>
						<l n="3" num="1.3"><w n="3.1">Fatigué</w> <w n="3.2">mon</w> <w n="3.3">esprit</w> <w n="3.4">de</w> <w n="3.5">courses</w> <w n="3.6">vagabondes</w> :</l>
						<l n="4" num="1.4"><w n="4.1">Il</w> <w n="4.2">me</w> <w n="4.3">manquait</w> <w n="4.4">encor</w> <w n="4.5">la</w> <w n="4.6">déesse</w> <w n="4.7">du</w> <w n="4.8">lieu</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Elle</w> <w n="5.2">m</w>’<w n="5.3">est</w> <w n="5.4">apparue</w> <w n="5.5">un</w> <w n="5.6">jour</w>, <w n="5.7">et</w> <w n="5.8">j</w>’<w n="5.9">ai</w> <w n="5.10">fait</w> <w n="5.11">vœu</w></l>
						<l n="6" num="2.2"><w n="6.1">D</w>’<w n="6.2">aller</w> <w n="6.3">chercher</w> <w n="6.4">coraux</w> <w n="6.5">et</w> <w n="6.6">perles</w>, <w n="6.7">tout</w> <w n="6.8">au</w> <w n="6.9">monde</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Pour</w> <w n="7.2">embellir</w> <w n="7.3">encor</w> <w n="7.4">sa</w> <w n="7.5">belle</w> <w n="7.6">tête</w> <w n="7.7">blonde</w> —</l>
						<l n="8" num="2.4"><w n="8.1">Parce</w> <w n="8.2">qu</w>’<w n="8.3">il</w> <w n="8.4">m</w>’<w n="8.5">a</w> <w n="8.6">semblé</w> <w n="8.7">la</w> <w n="8.8">voir</w> <w n="8.9">sourire</w> <w n="8.10">un</w> <w n="8.11">peu</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Dans</w> <w n="9.2">mes</w> <w n="9.3">nuits</w> <w n="9.4">sans</w> <w n="9.5">sommeil</w> <w n="9.6">je</w> <w n="9.7">ne</w> <w n="9.8">vois</w> <w n="9.9">plus</w> <w n="9.10">rien</w> <w n="9.11">qu</w>’<w n="9.12">elle</w>,</l>
						<l n="10" num="3.2"><w n="10.1">Telle</w> <w n="10.2">qu</w>’<w n="10.3">elle</w> <w n="10.4">a</w> <w n="10.5">passé</w> <w n="10.6">devant</w> <w n="10.7">mes</w> <w n="10.8">yeux</w>, <w n="10.9">si</w> <w n="10.10">belle</w></l>
						<l n="11" num="3.3"><w n="11.1">Avec</w> <w n="11.2">ses</w> <w n="11.3">grands</w> <w n="11.4">cheveux</w> <w n="11.5">royalement</w> <w n="11.6">tordus</w> ;</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1"><w n="12.1">Blanche</w> <w n="12.2">comme</w> <w n="12.3">l</w>’<w n="12.4">écume</w> <w n="12.5">éclatante</w> <w n="12.6">des</w> <w n="12.7">vagues</w>,</l>
						<l n="13" num="4.2"><w n="13.1">Et</w> <w n="13.2">fixant</w> <w n="13.3">sur</w> <w n="13.4">mes</w> <w n="13.5">yeux</w> <w n="13.6">ses</w> <w n="13.7">yeux</w> <w n="13.8">charmants</w> <w n="13.9">et</w> <w n="13.10">vagues</w></l>
						<l n="14" num="4.3"><w n="14.1">Qui</w> <w n="14.2">reflètent</w> <w n="14.3">la</w> <w n="14.4">mer</w> <w n="14.5">et</w> <w n="14.6">le</w> <w n="14.7">ciel</w> <w n="14.8">confondus</w>.</l>
					</lg>
				</div></body></text></TEI>