<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES POËMES DE L’AMOUR ET DE LA MER</title>
				<title type="medium">Édition électronique</title>
				<author key="BCH">
					<name>
						<forename>Maurice</forename>
						<surname>Bouchor</surname>
					</name>
					<date from="1855" to="1929">1855-1929</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3472 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BCH_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
					<title>LES POËMES DE L’AMOUR ET DE LA MER</title>
					<author>Maurice Bouchor</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k9736743c</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
							<title>LES POËMES DE L’AMOUR ET DE LA MER</title>
							<author>Maurice Bouchor</author>
							<imprint>
								<pubPlace>Paris</pubPlace>
								<publisher>Charpentier et Cie, Libraires-éditeurs</publisher>
								<date when="1876">1876</date>
							</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1876">1876</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p></p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">I</head><head type="main_part">LA FLEUR DES EAUX</head><div type="poem" key="BCH26">
					<head type="number">XXV</head>
					<head type="main">TES CHEVEUX</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Défais</w> <w n="1.2">tes</w> <w n="1.3">cheveux</w>, <w n="1.4">que</w> <w n="1.5">l</w>’<w n="1.6">on</w> <w n="1.7">voie</w></l>
						<l n="2" num="1.2"><w n="2.1">Avec</w> <w n="2.2">mille</w> <w n="2.3">reflets</w> <w n="2.4">de</w> <w n="2.5">soie</w></l>
						<l n="3" num="1.3"><w n="3.1">Ondoyer</w> <w n="3.2">leur</w> <w n="3.3">flot</w> <w n="3.4">qui</w> <w n="3.5">descend</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Comme</w> <w n="4.2">un</w> <w n="4.3">soleil</w> <w n="4.4">dorant</w> <w n="4.5">les</w> <w n="4.6">nues</w>,</l>
						<l n="5" num="1.5"><w n="5.1">Sur</w> <w n="5.2">tes</w> <w n="5.3">blanches</w> <w n="5.4">épaules</w> <w n="5.5">nues</w></l>
						<l n="6" num="1.6"><w n="6.1">Et</w> <w n="6.2">sur</w> <w n="6.3">ton</w> <w n="6.4">dos</w> <w n="6.5">éblouissant</w>.</l>
					</lg>
					<lg n="2">
						<l n="7" num="2.1"><w n="7.1">N</w>’<w n="7.2">y</w> <w n="7.3">laisse</w> <w n="7.4">pas</w> <w n="7.5">mordre</w> <w n="7.6">ton</w> <w n="7.7">peigne</w> ;</l>
						<l n="8" num="2.2"><w n="8.1">Que</w> <w n="8.2">le</w> <w n="8.3">flot</w> <w n="8.4">t</w>’<w n="8.5">enlace</w> <w n="8.6">et</w> <w n="8.7">te</w> <w n="8.8">baigne</w>,</l>
						<l n="9" num="2.3"><w n="9.1">Et</w> <w n="9.2">s</w>’<w n="9.3">il</w> <w n="9.4">te</w> <w n="9.5">noie</w>, <w n="9.6">eh</w> <w n="9.7">bien</w>, <w n="9.8">tant</w> <w n="9.9">pis</w> !</l>
						<l n="10" num="2.4"><w n="10.1">Permets</w> <w n="10.2">à</w> <w n="10.3">cette</w> <w n="10.4">étrange</w> <w n="10.5">houle</w></l>
						<l n="11" num="2.5"><w n="11.1">Qu</w>’<w n="11.2">elle</w> <w n="11.3">s</w>’<w n="11.4">enroule</w> <w n="11.5">et</w> <w n="11.6">se</w> <w n="11.7">déroule</w></l>
						<l n="12" num="2.6"><w n="12.1">Et</w> <w n="12.2">ruisselle</w> <w n="12.3">jusqu</w>’<w n="12.4">au</w> <w n="12.5">tapis</w>.</l>
					</lg>
					<lg n="3">
						<l n="13" num="3.1"><w n="13.1">Demeure</w> <w n="13.2">immobile</w>, <w n="13.3">statue</w></l>
						<l n="14" num="3.2"><w n="14.1">D</w>’<w n="14.2">une</w> <w n="14.3">chevelure</w> <w n="14.4">vêtue</w> :</l>
						<l n="15" num="3.3"><w n="15.1">Que</w> <w n="15.2">ne</w> <w n="15.3">puis</w>-<w n="15.4">je</w>, <w n="15.5">ivre</w> <w n="15.6">de</w> <w n="15.7">désirs</w>,</l>
						<l n="16" num="3.4"><w n="16.1">Parmi</w> <w n="16.2">l</w>’<w n="16.3">or</w> <w n="16.4">de</w> <w n="16.5">tes</w> <w n="16.6">folles</w> <w n="16.7">boucles</w></l>
						<l n="17" num="3.5"><w n="17.1">Faire</w> <w n="17.2">flamber</w> <w n="17.3">les</w> <w n="17.4">escarboucles</w></l>
						<l n="18" num="3.6"><w n="18.1">Et</w> <w n="18.2">miroiter</w> <w n="18.3">l</w>’<w n="18.4">eau</w> <w n="18.5">des</w> <w n="18.6">saphirs</w> !</l>
					</lg>
					<lg n="4">
						<l n="19" num="4.1"><w n="19.1">Que</w> <w n="19.2">tes</w> <w n="19.3">cheveux</w> <w n="19.4">versent</w> <w n="19.5">de</w> <w n="19.6">joie</w>,</l>
						<l n="20" num="4.2"><w n="20.1">Et</w> <w n="20.2">quelle</w> <w n="20.3">lumière</w> <w n="20.4">flamboie</w></l>
						<l n="21" num="4.3"><w n="21.1">Aux</w> <w n="21.2">yeux</w> <w n="21.3">éblouis</w> <w n="21.4">et</w> <w n="21.5">grisés</w> !</l>
						<l n="22" num="4.4"><w n="22.1">Qu</w>’<w n="22.2">ils</w> <w n="22.3">sont</w> <w n="22.4">fins</w>, <w n="22.5">subtils</w> <w n="22.6">et</w> <w n="22.7">folâtres</w>,</l>
						<l n="23" num="4.5"><w n="23.1">Comme</w> <w n="23.2">la</w> <w n="23.3">cendre</w> <w n="23.4">autour</w> <w n="23.5">de</w> <w n="23.6">l</w>’<w n="23.7">âtre</w></l>
						<l n="24" num="4.6"><w n="24.1">Fuyant</w> <w n="24.2">au</w> <w n="24.3">souffle</w> <w n="24.4">des</w> <w n="24.5">baisers</w> !</l>
					</lg>
					<lg n="5">
						<l n="25" num="5.1"><w n="25.1">Laisse</w> <w n="25.2">mes</w> <w n="25.3">doigts</w> <w n="25.4">nerveux</w> <w n="25.5">les</w> <w n="25.6">tordre</w>,</l>
						<l n="26" num="5.2"><w n="26.1">Ma</w> <w n="26.2">bouche</w> <w n="26.3">à</w> <w n="26.4">belles</w> <w n="26.5">dents</w> <w n="26.6">les</w> <w n="26.7">mordre</w> !</l>
						<l n="27" num="5.3"><w n="27.1">Et</w> <w n="27.2">si</w>, <w n="27.3">lasse</w> <w n="27.4">d</w>’<w n="27.5">amour</w>, <w n="27.6">tu</w> <w n="27.7">veux</w></l>
						<l n="28" num="5.4"><w n="28.1">Que</w> <w n="28.2">notre</w> <w n="28.3">extase</w> <w n="28.4">enfin</w> <w n="28.5">s</w>’<w n="28.6">achève</w>,</l>
						<l n="29" num="5.5"><w n="29.1">Tu</w> <w n="29.2">peux</w> <w n="29.3">m</w>’<w n="29.4">embaumer</w> <w n="29.5">en</w> <w n="29.6">plein</w> <w n="29.7">rêve</w></l>
						<l n="30" num="5.6"><w n="30.1">Dans</w> <w n="30.2">le</w> <w n="30.3">linceul</w> <w n="30.4">de</w> <w n="30.5">tes</w> <w n="30.6">cheveux</w>.</l>
					</lg>
				</div></body></text></TEI>