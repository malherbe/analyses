<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA NÉGRESSE BLONDE</title>
				<title type="medium">Édition électronique</title>
				<author key="FOU">
					<name>
						<forename>Georges</forename>
						<surname>FOUREST</surname>
					</name>
					<date from="1864" to="1945">1864-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1172 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref> 
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">FOU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>La Négresse blonde</title>
						<author>Georges Fourest</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/La_N%C3%A9gresse_blonde_(recueil)</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>La Négresse blonde</title>
								<author>Georges Fourest</author>
								<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k1269960g</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A MESSEIN</publisher>
									<date when="1909">1909</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1909">1909</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-08-02" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-08-02" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">SIX PSEUDO-SONNETS <lb></lb>TRUCULENTS ET ALLÉGORIQUES</head><div type="poem" key="FOU5">
					<head type="main">PSEUDO-SONNET <lb></lb>AFRICAIN ET GASTRONOMIQUE <lb></lb>OU <lb></lb>(PLUS SIMPLEMENT) REPAS DE FAMILLE</head>
					<opener>
						<epigraph>
							<cit>
								<quote>Prenez et mangez : ceci est mon corps.</quote>
							</cit>
						</epigraph>
					</opener>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Au</w> <w n="1.2">bord</w> <w n="1.3">du</w> <w n="1.4">Loudjiji</w> <w n="1.5">qu</w>’<w n="1.6">embaument</w> <w n="1.7">les</w> <w n="1.8">arômes</w></l>
						<l n="2" num="1.2"><w n="2.1">des</w> <w n="2.2">toumbos</w> <w n="2.3">le</w> <w n="2.4">bon</w> <w n="2.5">roi</w> <w n="2.6">Makoko</w> <ref target="1" type="noteAnchor">1</ref> <w n="2.7">s</w>’<w n="2.8">est</w> <w n="2.9">assis</w>.</l>
						<l n="3" num="1.3"><w n="3.1">Un</w> <w n="3.2">m</w>’<w n="3.3">gannga</w> <w n="3.4">tatoua</w> <w n="3.5">de</w> <w n="3.6">zigzags</w> <w n="3.7">polychromes</w></l>
						<l n="4" num="1.4"><w n="4.1">sa</w> <w n="4.2">peau</w> <w n="4.3">d</w>’<w n="4.4">un</w> <w n="4.5">noir</w> <w n="4.6">vineux</w> <w n="4.7">tirant</w> <w n="4.8">sur</w> <w n="4.9">le</w> <w n="4.10">cassis</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Il</w> <w n="5.2">fait</w> <w n="5.3">nuit</w> : <w n="5.4">les</w> <w n="5.5">m</w>’<w n="5.6">pafous</w> <w n="5.7">ont</w> <w n="5.8">des</w> <w n="5.9">senteurs</w> <w n="5.10">plus</w> <w n="5.11">frêles</w> ;</l>
						<l n="6" num="2.2"><w n="6.1">sourd</w>, <w n="6.2">un</w> <w n="6.3">marimeba</w> <w n="6.4">vibre</w> <w n="6.5">en</w> <w n="6.6">des</w> <w n="6.7">temps</w> <w n="6.8">égaux</w> ;</l>
						<l n="7" num="2.3"><w n="7.1">des</w> <w n="7.2">alligators</w> <w n="7.3">d</w>’<w n="7.4">or</w> <w n="7.5">grouillent</w> <w n="7.6">parmi</w> <w n="7.7">les</w> <w n="7.8">prêles</w></l>
						<l n="8" num="2.4"><w n="8.1">un</w> <w n="8.2">vent</w> <w n="8.3">léger</w> <w n="8.4">courbe</w> <w n="8.5">la</w> <w n="8.6">tête</w> <w n="8.7">des</w> <w n="8.8">sorghos</w> ;</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">et</w> <w n="9.2">le</w> <w n="9.3">mont</w> <w n="9.4">Koungoua</w> <w n="9.5">rond</w> <w n="9.6">comme</w> <w n="9.7">une</w> <w n="9.8">bedaine</w>,</l>
						<l n="10" num="3.2"><w n="10.1">sous</w> <w n="10.2">la</w> <w n="10.3">Lune</w> <w n="10.4">aux</w> <w n="10.5">reflets</w> <w n="10.6">pâles</w> <w n="10.7">de</w> <w n="10.8">molybdène</w>,</l>
						<l n="11" num="3.3"><w n="11.1">se</w> <w n="11.2">mire</w> <w n="11.3">dans</w> <w n="11.4">le</w> <w n="11.5">fleuve</w> <w n="11.6">au</w> <w n="11.7">bleuâtre</w> <w n="11.8">circuit</w>.</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1"><w n="12.1">Makoko</w> <w n="12.2">reste</w> <w n="12.3">aveugle</w> <w n="12.4">à</w> <w n="12.5">tout</w> <w n="12.6">ce</w> <w n="12.7">qui</w> <w n="12.8">l</w>’<w n="12.9">entoure</w> :</l>
						<l n="13" num="4.2"><w n="13.1">avec</w> <w n="13.2">conviction</w> <w n="13.3">ce</w> <w n="13.4">potentat</w> <w n="13.5">savoure</w></l>
						<l n="14" num="4.3"><w n="14.1">un</w> <w n="14.2">bras</w> <w n="14.3">de</w> <w n="14.4">son</w> <w n="14.5">grand</w>-<w n="14.6">père</w> <w n="14.7">et</w> <w n="14.8">le</w> <w n="14.9">juge</w> <w n="14.10">trop</w> <w n="14.11">cuit</w>.</l>
					</lg>
					<closer>
						<note type="footnote" id="1">
							Makoko, souverain anthropophage, mais constitutionnel de l’Afrique Centrale. <lb></lb>
							(Note de l’Auteur.)
						</note>
					</closer>
				</div></body></text></TEI>