<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA NÉGRESSE BLONDE</title>
				<title type="medium">Édition électronique</title>
				<author key="FOU">
					<name>
						<forename>Georges</forename>
						<surname>FOUREST</surname>
					</name>
					<date from="1864" to="1945">1864-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1172 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref> 
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">FOU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>La Négresse blonde</title>
						<author>Georges Fourest</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/La_N%C3%A9gresse_blonde_(recueil)</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>La Négresse blonde</title>
								<author>Georges Fourest</author>
								<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k1269960g</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A MESSEIN</publisher>
									<date when="1909">1909</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1909">1909</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-08-02" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-08-02" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PETITES ÉLÉGIES FALOTES</head><div type="poem" key="FOU15">
					<head type="main">SOUVENIR <lb></lb>OU <lb></lb>AUTRE REPAS DE FAMILLE</head>
					<opener>
						<epigraph>
							<cit>
								<quote>
									Après avoir vidé et nettoyé vos boyaux, <lb></lb>
									coupez-les en filets de 25 centimètres auxquels <lb></lb>
									vous joindrez du lard maigre coupé aussi en filets. <lb></lb>
									M<hi rend="sup">lle</hi> Rosalie BLANQUET
								</quote>
								<bibl>
									(La cuisinière des ménages, partie III, cap. V.)
								</bibl>
							</cit>
						</epigraph>
					</opener>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Quand</w> <w n="1.2">j</w>’<w n="1.3">étais</w> <w n="1.4">tout</w> <w n="1.5">petit</w>, <w n="1.6">nous</w> <w n="1.7">dînions</w> <w n="1.8">chez</w> <w n="1.9">ma</w> <w n="1.10">tante</w>,</l>
						<l n="2" num="1.2"><w n="2.1">le</w> <w n="2.2">jeudi</w> <w n="2.3">soir</w> ; <w n="2.4">papa</w> <w n="2.5">la</w> <w n="2.6">jugeait</w> <w n="2.7">dégoûtante</w></l>
						<l n="3" num="1.3"><w n="3.1">à</w> <w n="3.2">cause</w> <w n="3.3">d</w>’<w n="3.4">un</w> <w n="3.5">lupus</w> <w n="3.6">qui</w> <w n="3.7">lui</w> <w n="3.8">mangeait</w> <w n="3.9">le</w> <w n="3.10">nez</w> :</l>
						<l n="4" num="1.4"><w n="4.1">ce</w> <w n="4.2">m</w>’<w n="4.3">est</w> <w n="4.4">un</w> <w n="4.5">souvenir</w> <w n="4.6">si</w> <w n="4.7">doux</w> <w n="4.8">que</w> <w n="4.9">ces</w> <w n="4.10">dîners</w> !</l>
						<l n="5" num="1.5"><w n="5.1">Après</w> <w n="5.2">le</w> <w n="5.3">pot</w>-<w n="5.4">au</w>-<w n="5.5">feu</w>, <w n="5.6">la</w> <w n="5.7">bonne</w>, <w n="5.8">Marguerite</w>,</l>
						<l n="6" num="1.6"><w n="6.1">apportait</w> <w n="6.2">le</w> <w n="6.3">gigot</w> <w n="6.4">avec</w> <w n="6.5">la</w> <w n="6.6">pomme</w> <w n="6.7">frite</w></l>
						<l n="7" num="1.7"><w n="7.1">classique</w> <w n="7.2">et</w> <w n="7.3">c</w>’<w n="7.4">était</w> <w n="7.5">bon</w> ! <w n="7.6">je</w> <w n="7.7">ne</w> <w n="7.8">vous</w> <w n="7.9">dis</w> <w n="7.10">que</w> <w n="7.11">ça</w> !</l>
						<l n="8" num="1.8"><w n="8.1">Chacun</w> <w n="8.2">jetait</w> <w n="8.3">son</w> <w n="8.4">os</w> <w n="8.5">à</w> <w n="8.6">la</w> <w n="8.7">chienne</w> <w n="8.8">Aïssa</w>,</l>
						<l n="9" num="1.9"><w n="9.1">Moi</w>, <w n="9.2">ce</w> <w n="9.3">que</w> <w n="9.4">j</w>’<w n="9.5">aimais</w> <w n="9.6">bien</w> <w n="9.7">c</w>’<w n="9.8">est</w> <w n="9.9">andouille</w> <w n="9.10">de</w> <w n="9.11">Vire</w> ;</l>
						<l n="10" num="1.10"><w n="10.1">je</w> <w n="10.2">contemplais</w> (<w n="10.3">ainsi</w> <w n="10.4">que</w> <w n="10.5">Lamartine</w> <w n="10.6">Elvire</w>)</l>
						<l n="11" num="1.11"><w n="11.1">sur</w> <w n="11.2">mon</w> <w n="11.3">assiette</w> <w n="11.4">à</w> <w n="11.5">fleurs</w> <w n="11.6">les</w> <w n="11.7">gros</w> <w n="11.8">morceaux</w> <w n="11.9">de</w> <w n="11.10">lard</w>,</l>
						<l n="12" num="1.12"><w n="12.1">et</w> <w n="12.2">je</w> <w n="12.3">roulais</w> <w n="12.4">des</w> <w n="12.5">yeux</w> <w n="12.6">béats</w> <w n="12.7">de</w> <w n="12.8">papelard</w></l>
						<l n="13" num="1.13"><w n="13.1">et</w> <w n="13.2">ma</w> <w n="13.3">tante</w> <w n="13.4">disait</w> : « <w n="13.5">Mange</w> <w n="13.6">donc</w>, <w n="13.7">niguedouille</w> ! »…</l>
						<l n="14" num="1.14"><w n="14.1">ô</w> <w n="14.2">Seigneur</w>, <w n="14.3">bénissez</w> <w n="14.4">ma</w> <w n="14.5">tante</w> <w n="14.6">et</w> <w n="14.7">son</w> <w n="14.8">andouille</w> !</l>
					</lg>
				</div></body></text></TEI>