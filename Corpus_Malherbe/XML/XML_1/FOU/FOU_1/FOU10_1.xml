<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA NÉGRESSE BLONDE</title>
				<title type="medium">Édition électronique</title>
				<author key="FOU">
					<name>
						<forename>Georges</forename>
						<surname>FOUREST</surname>
					</name>
					<date from="1864" to="1945">1864-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1172 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref> 
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">FOU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>La Négresse blonde</title>
						<author>Georges Fourest</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/La_N%C3%A9gresse_blonde_(recueil)</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>La Négresse blonde</title>
								<author>Georges Fourest</author>
								<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k1269960g</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A MESSEIN</publisher>
									<date when="1909">1909</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1909">1909</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-08-02" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-08-02" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PETITES ÉLÉGIES FALOTES</head><div type="poem" key="FOU10">
					<head type="main">SARDINES À L’HUILE</head>
					<opener>
						<epigraph>
							<cit>
								<quote>
									Sardines à l’huile fine sans têtes et sans arêtes.
								</quote>
								<bibl>
									(Réclames des sardiniers passim.)
								</bibl>
							</cit>
						</epigraph>
					</opener>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Dans</w> <w n="1.2">leur</w> <w n="1.3">cercueil</w> <w n="1.4">de</w> <w n="1.5">fer</w> <w n="1.6">blanc</w></l>
						<l n="2" num="1.2"><w n="2.1">plein</w> <w n="2.2">d</w>’<w n="2.3">huile</w> <w n="2.4">au</w> <w n="2.5">puant</w> <w n="2.6">relent</w></l>
						<l n="3" num="1.3"><w n="3.1">marinent</w> <w n="3.2">décapités</w></l>
						<l n="4" num="1.4"><w n="4.1">ces</w> <w n="4.2">petits</w> <w n="4.3">corps</w> <w n="4.4">argentés</w></l>
						<l n="5" num="1.5"><w n="5.1">pareils</w> <w n="5.2">aux</w> <w n="5.3">guillotinés</w></l>
						<l n="6" num="1.6"><w n="6.1">là</w>-<w n="6.2">bas</w> <w n="6.3">au</w> <w n="6.4">champ</w> <w n="6.5">des</w> <w n="6.6">navets</w> !</l>
						<l n="7" num="1.7"><w n="7.1">Elles</w> <w n="7.2">ont</w> <w n="7.3">vu</w> <w n="7.4">les</w> <w n="7.5">mers</w> <w n="7.6">les</w></l>
						<l n="8" num="1.8"><w n="8.1">côtes</w> <w n="8.2">grises</w> <w n="8.3">de</w> <w n="8.4">Thulé</w>,</l>
						<l n="9" num="1.9"><w n="9.1">sous</w> <w n="9.2">les</w> <w n="9.3">brumes</w> <w n="9.4">argentées</w></l>
						<l n="10" num="1.10"><w n="10.1">la</w> <w n="10.2">Mer</w> <w n="10.3">du</w> <w n="10.4">Nord</w> <w n="10.5">enchantée</w>…</l>
						<l n="11" num="1.11"><w n="11.1">Maintenant</w> <w n="11.2">dans</w> <w n="11.3">le</w> <w n="11.4">fer</w> <w n="11.5">blanc</w></l>
						<l n="12" num="1.12"><w n="12.1">et</w> <w n="12.2">l</w>’<w n="12.3">huile</w> <w n="12.4">au</w> <w n="12.5">puant</w> <w n="12.6">relent</w></l>
						<l n="13" num="1.13"><w n="13.1">de</w> <w n="13.2">toxiques</w> <w n="13.3">restaurants</w></l>
						<l n="14" num="1.14"><w n="14.1">les</w> <w n="14.2">servent</w> <w n="14.3">à</w> <w n="14.4">leurs</w> <w n="14.5">clients</w> !</l>
						<l n="15" num="1.15"><w n="15.1">Mais</w> <w n="15.2">loin</w> <w n="15.3">derrière</w> <w n="15.4">la</w> <w n="15.5">nue</w></l>
						<l n="16" num="1.16"><w n="16.1">leur</w> <w n="16.2">pauvre</w> <w n="16.3">âmette</w> <w n="16.4">ingénue</w></l>
						<l n="17" num="1.17"><w n="17.1">dit</w> <w n="17.2">sa</w> <w n="17.3">muette</w> <w n="17.4">chanson</w></l>
						<l n="18" num="1.18"><w n="18.1">au</w> <w n="18.2">Paradis</w>-<w n="18.3">des</w>-<w n="18.4">poissons</w>,</l>
						<l n="19" num="1.19"><w n="19.1">une</w> <w n="19.2">mer</w> <w n="19.3">fraîche</w> <w n="19.4">et</w> <w n="19.5">lunaire</w></l>
						<l n="20" num="1.20"><w n="20.1">pâle</w> <w n="20.2">comme</w> <w n="20.3">un</w> <w n="20.4">poitrinaire</w>,</l>
						<l n="21" num="1.21"><w n="21.1">la</w> <w n="21.2">Mer</w> <w n="21.3">de</w> <w n="21.4">Sérénité</w></l>
						<l n="22" num="1.22"><w n="22.1">aux</w> <w n="22.2">longs</w> <w n="22.3">reflets</w> <w n="22.4">argentés</w></l>
						<l n="23" num="1.23"><w n="23.1">où</w>, <w n="23.2">durant</w> <w n="23.3">l</w>’<w n="23.4">éternité</w>,</l>
						<l n="24" num="1.24"><w n="24.1">sans</w> <w n="24.2">plus</w> <w n="24.3">craindre</w> <w n="24.4">jamais</w> <w n="24.5">les</w></l>
						<l n="25" num="1.25"><w n="25.1">cormorans</w> <w n="25.2">et</w> <w n="25.3">les</w> <w n="25.4">filets</w>,</l>
						<l n="26" num="1.26"><w n="26.1">après</w> <w n="26.2">leur</w> <w n="26.3">mort</w> <w n="26.4">nageront</w></l>
						<l n="27" num="1.27"><w n="27.1">tous</w> <w n="27.2">les</w> <w n="27.3">bons</w> <w n="27.4">petits</w> <w n="27.5">poissons</w> !…</l>
					</lg>
					<lg n="2">
						<l n="28" num="2.1"><w n="28.1">Sans</w> <w n="28.2">voix</w>, <w n="28.3">sans</w> <w n="28.4">mains</w>, <w n="28.5">sans</w> <w n="28.6">genoux</w> <ref target="1" type="noteAnchor">1</ref> ,</l>
						<l n="29" num="2.2"><w n="29.1">sardines</w> <w n="29.2">priez</w> <w n="29.3">pour</w> <w n="29.4">nous</w> !…</l>
					</lg>
					<closer>
						<note type="footnote" id="1">
							Tout ce qu’il faut pour prier. <lb></lb>
							(Note de l’Auteur.)
						</note>
					</closer>
				</div></body></text></TEI>