<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA NÉGRESSE BLONDE</title>
				<title type="medium">Édition électronique</title>
				<author key="FOU">
					<name>
						<forename>Georges</forename>
						<surname>FOUREST</surname>
					</name>
					<date from="1864" to="1945">1864-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1172 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref> 
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">FOU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>La Négresse blonde</title>
						<author>Georges Fourest</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/La_N%C3%A9gresse_blonde_(recueil)</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>La Négresse blonde</title>
								<author>Georges Fourest</author>
								<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k1269960g</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A MESSEIN</publisher>
									<date when="1909">1909</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1909">1909</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-08-02" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-08-02" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">RENONCEMENT</head><div type="poem" key="FOU2">
					<head type="main">RENONCEMENT</head>
					<opener>
						<epigraph>
							<cit>
								<quote>
									Quid dignum stolidis mentibus imprecet ? <lb></lb>
									Opes honores ambiant ! <lb></lb>
									Et, quum falsa gravi mole paraverint <lb></lb>
									Tum vera cognoscant bona !
								</quote>
								<bibl>
									<name>S. Bœtius</name> (De consolatione philosophiæ. Lib. III).
								</bibl>
							</cit>
						</epigraph>
					</opener>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Bourgeois</w> <w n="1.2">hideux</w>, <w n="1.3">préfets</w>, <w n="1.4">charcutiers</w>, <w n="1.5">militaires</w>,</l>
						<l n="2" num="1.2"><w n="2.1">gens</w> <w n="2.2">de</w> <w n="2.3">lettres</w>, <w n="2.4">marlous</w>, <w n="2.5">juges</w>, <w n="2.6">mouchards</w>, <w n="2.7">notaires</w>,</l>
						<l n="3" num="1.3"><w n="3.1">généraux</w>, <w n="3.2">caporaux</w> <w n="3.3">et</w> <w n="3.4">tourneurs</w> <w n="3.5">de</w> <w n="3.6">barreaux</w></l>
						<l n="4" num="1.4"><w n="4.1">de</w> <w n="4.2">chaise</w>, <w n="4.3">lauréats</w> <w n="4.4">mornes</w> <w n="4.5">des</w> <w n="4.6">Jeux</w> <w n="4.7">Floraux</w>,</l>
						<l n="5" num="1.5"><w n="5.1">banquistes</w> <w n="5.2">et</w> <w n="5.3">banquiers</w>, <w n="5.4">architectes</w> <w n="5.5">pratiques</w></l>
						<l n="6" num="1.6"><w n="6.1">metteurs</w> <w n="6.2">de</w> <w n="6.3">Choubersky</w> <w n="6.4">dans</w> <w n="6.5">les</w> <w n="6.6">salles</w> <w n="6.7">gothiques</w>,</l>
						<l n="7" num="1.7"><w n="7.1">dentistes</w>, <w n="7.2">oyez</w> <w n="7.3">tous</w> ! — <w n="7.4">Lorsque</w> <w n="7.5">je</w> <w n="7.6">naquis</w> <w n="7.7">dans</w></l>
						<l n="8" num="1.8"><w n="8.1">mon</w> <w n="8.2">château</w> <w n="8.3">crénelé</w> <w n="8.4">j</w>’<w n="8.5">avais</w> <w n="8.6">trois</w> <w n="8.7">mille</w> <w n="8.8">dents</w></l>
						<l n="9" num="1.9"><w n="9.1">et</w> <w n="9.2">des</w> <w n="9.3">favoris</w> <w n="9.4">bleus</w> : <w n="9.5">on</w> <w n="9.6">narre</w> <w n="9.7">que</w> <w n="9.8">ma</w> <w n="9.9">mère</w></l>
						<l n="10" num="1.10">(<w n="10.1">et</w> <w n="10.2">croyez</w> <w n="10.3">que</w> <w n="10.4">ceci</w> <w n="10.5">n</w>’<w n="10.6">est</w> <w n="10.7">pas</w> <w n="10.8">une</w> <w n="10.9">chimère</w> !)</l>
						<l n="11" num="1.11"><w n="11.1">m</w>’<w n="11.2">avait</w> <w n="11.3">porté</w> <w n="11.4">sept</w> <w n="11.5">ans</w> <w n="11.6">entiers</w>. <w n="11.7">Encore</w> <w n="11.8">enfant</w></l>
						<l n="12" num="1.12"><w n="12.1">j</w>’<w n="12.2">assommai</w> <w n="12.3">d</w>’<w n="12.4">une</w> <w n="12.5">chiquenaude</w> <w n="12.6">un</w> <w n="12.7">éléphant</w>.</l>
						<l n="13" num="1.13"><w n="13.1">Chaque</w> <w n="13.2">jour</w> <w n="13.3">huit</w> <w n="13.4">pendus</w> <w n="13.5">à</w> <w n="13.6">face</w> <w n="13.7">de</w> <w n="13.8">Gorgonne</w></l>
						<l n="14" num="1.14"><w n="14.1">grimaçaient</w> <w n="14.2">aux</w> <w n="14.3">huit</w> <w n="14.4">coins</w> <w n="14.5">de</w> <w n="14.6">ma</w> <w n="14.7">tour</w> <w n="14.8">octogone</w>,</l>
						<l n="15" num="1.15"><w n="15.1">et</w> <w n="15.2">j</w>’<w n="15.3">eus</w> <w n="15.4">pour</w> <w n="15.5">précepteur</w> <w n="15.6">cet</w> <w n="15.7">illustre</w> <w n="15.8">Sarcey</w></l>
						<l n="16" num="1.16"><w n="16.1">qui</w> <w n="16.2">semble</w> <w n="16.3">un</w> <w n="16.4">fruit</w> <w n="16.5">trop</w> <w n="16.6">mûr</w> <w n="16.7">de</w> <w n="16.8">cucurbitacé</w>,</l>
						<l n="17" num="1.17"><w n="17.1">mais</w> <w n="17.2">qui</w> <w n="17.3">sait</w> <w n="17.4">tout</w>, <w n="17.5">ayant</w> <w n="17.6">lu</w> <w n="17.7">plusieurs</w> <w n="17.8">fois</w> <w n="17.9">Larousse</w> !</l>
						<l n="18" num="1.18"><w n="18.1">Mon</w> <w n="18.2">parrain</w> <w n="18.3">se</w> <w n="18.4">nommait</w> <w n="18.5">Frédéric</w> <w n="18.6">Barberousse</w>.</l>
						<l n="19" num="1.19"><w n="19.1">Quand</w> <w n="19.2">j</w>’<w n="19.3">atteignis</w> <w n="19.4">quinze</w> <w n="19.5">ans</w> : <w n="19.6">le</w> <w n="19.7">Cid</w> <w n="19.8">Campeador</w>,</l>
						<l n="20" num="1.20"><w n="20.1">pour</w> <w n="20.2">m</w>’<w n="20.3">offrir</w> <w n="20.4">sa</w> <w n="20.5">tueuse</w> <w n="20.6">et</w> <w n="20.7">ses</w> <w n="20.8">éperons</w> <w n="20.9">d</w>’<w n="20.10">or</w>,</l>
						<l n="21" num="1.21"><w n="21.1">sortit</w> <w n="21.2">de</w> <w n="21.3">son</w> <w n="21.4">tombeau</w> ; <w n="21.5">d</w>’<w n="21.6">une</w> <w n="21.7">voix</w> <w n="21.8">surhumaine</w> :</l>
						<l n="22" num="1.22">« — <w n="22.1">Ami</w>, <w n="22.2">veux</w>-<w n="22.3">tu</w> <w n="22.4">coucher</w>, <w n="22.5">dit</w>-<w n="22.6">il</w>, <w n="22.7">avec</w> <w n="22.8">Chimène</w> !</l>
						<l n="23" num="1.23"><w n="23.1">Moi</w>, <w n="23.2">je</w> <w n="23.3">lui</w> <w n="23.4">répondis</w> : « <w n="23.5">Zut</w> ! » <w n="23.6">et</w> « <w n="23.7">Bran</w> ! » <w n="23.8">Par</w> <w n="23.9">façon</w></l>
						<l n="24" num="1.24"><w n="24.1">de</w> <w n="24.2">divertissement</w>, <w n="24.3">d</w>’<w n="24.4">un</w> <w n="24.5">coup</w> <w n="24.6">d</w>’<w n="24.7">estramaçon</w></l>
						<l n="25" num="1.25"><w n="25.1">j</w>’<w n="25.2">éventrai</w> <w n="25.3">l</w>’<w n="25.4">Empereur</w> ; <w n="25.5">puis</w> <w n="25.6">je</w> <w n="25.7">châtrai</w> <w n="25.8">le</w> <w n="25.9">Pape</w></l>
						<l n="26" num="1.26"><w n="26.1">et</w> <w n="26.2">son</w> <w n="26.3">grand</w> <w n="26.4">moutardier</w> ; <w n="26.5">je</w> <w n="26.6">dérobai</w> <w n="26.7">sa</w> <w n="26.8">chape</w></l>
						<l n="27" num="1.27"><w n="27.1">d</w>’<w n="27.2">or</w>, <w n="27.3">sa</w> <w n="27.4">tiare</w> <w n="27.5">d</w>’<w n="27.6">or</w> <w n="27.7">et</w> <w n="27.8">son</w> <w n="27.9">grand</w> <w n="27.10">ostensoir</w></l>
						<l n="28" num="1.28"><w n="28.1">d</w>’<w n="28.2">or</w> <w n="28.3">pareil</w> <w n="28.4">au</w> <w n="28.5">soleil</w> <w n="28.6">vermeil</w> <w n="28.7">dans</w> <w n="28.8">l</w>’<w n="28.9">or</w> <w n="28.10">du</w> <w n="28.11">soir</w> !</l>
						<l n="29" num="1.29"><w n="29.1">Des</w> <w n="29.2">cardinaux</w> <w n="29.3">traînaient</w> <w n="29.4">mon</w> <w n="29.5">char</w>, <w n="29.6">à</w> <w n="29.7">quatre</w> <w n="29.8">pattes</w></l>
						<l n="30" num="1.30"><w n="30.1">et</w> <w n="30.2">je</w> <w n="30.3">gravis</w> <w n="30.4">ainsi</w>, <w n="30.5">sept</w> <w n="30.6">fois</w>, <w n="30.7">les</w> <w n="30.8">monts</w> <w n="30.9">Karpates</w>.</l>
						<l n="31" num="1.31"><w n="31.1">Je</w> <w n="31.2">dis</w> <w n="31.3">au</w> <w n="31.4">Padishah</w> : « <w n="31.5">Vous</w> <w n="31.6">n</w>’<w n="31.7">êtes</w> <w n="31.8">qu</w>’<w n="31.9">un</w> <w n="31.10">faquin</w> ! »</l>
						<l n="32" num="1.32"><w n="32.1">pour</w> <w n="32.2">ma</w> <w n="32.3">couche</w> <w n="32.4">le</w> <w n="32.5">fils</w> <w n="32.6">de</w> <w n="32.7">l</w>’<w n="32.8">Amorabaquin</w></l>
						<l n="33" num="1.33"><w n="33.1">m</w>’<w n="33.2">offrit</w> <w n="33.3">ses</w> <w n="33.4">trente</w> <w n="33.5">sœurs</w> <w n="33.6">et</w> <w n="33.7">ses</w> <w n="33.8">quatre</w>-<w n="33.9">vingts</w> <w n="33.10">femmes</w>,</l>
						<l n="34" num="1.34"><w n="34.1">et</w> <w n="34.2">je</w> <w n="34.3">me</w> <w n="34.4">suis</w> <w n="34.5">grisé</w> <w n="34.6">de</w> <w n="34.7">voluptés</w> <w n="34.8">infâmes</w></l>
						<l n="35" num="1.35"><w n="35.1">parmi</w> <w n="35.2">les</w> <w n="35.3">icoglans</w> <w n="35.4">du</w> <w n="35.5">grand</w> <w n="35.6">Kaïmakan</w> !</l>
						<l n="36" num="1.36"><w n="36.1">Les</w> <w n="36.2">Boyards</w> <w n="36.3">de</w> <w n="36.4">Russie</w> <w n="36.5">au</w> <w n="36.6">manteau</w> <w n="36.7">d</w>’<w n="36.8">astrakan</w></l>
						<l n="37" num="1.37"><w n="37.1">décrottaient</w> <w n="37.2">mes</w> <w n="37.3">souliers</w>. <w n="37.4">L</w>’<w n="37.5">Empereur</w> <w n="37.6">de</w> <w n="37.7">la</w> <w n="37.8">Chine</w>,</l>
						<l n="38" num="1.38"><w n="38.1">pour</w> <w n="38.2">monter</w> <w n="38.3">à</w> <w n="38.4">cheval</w> <w n="38.5">me</w> <w n="38.6">prêtant</w> <w n="38.7">son</w> <w n="38.8">échine</w>,</l>
						<l n="39" num="1.39"><w n="39.1">osa</w> <w n="39.2">me</w> <w n="39.3">dire</w> <w n="39.4">un</w> <w n="39.5">mot</w> <w n="39.6">sans</w> <w n="39.7">ôter</w> <w n="39.8">son</w> <w n="39.9">chapeau</w> :</l>
						<l n="40" num="1.40"><w n="40.1">je</w> <w n="40.2">l</w>’<w n="40.3">écorchai</w> <w n="40.4">tout</w> <w n="40.5">vif</w> <w n="40.6">et</w> <w n="40.7">revendis</w> <w n="40.8">sa</w> <w n="40.9">peau</w></l>
						<l n="41" num="1.41"><w n="41.1">très</w> <w n="41.2">cher</w> <w n="41.3">à</w> <w n="41.4">Félix</w> <w n="41.5">Faure</w> ! <w n="41.6">Encore</w> <w n="41.7">qu</w>’<w n="41.8">impubère</w></l>
						<l n="42" num="1.42">(<w n="42.1">on</w> <w n="42.2">me</w> <w n="42.3">voit</w> <w n="42.4">tous</w> <w n="42.5">les</w> <w n="42.6">goûts</w> <w n="42.7">de</w> <w n="42.8">feu</w> <w n="42.9">César</w> <w n="42.10">Tibère</w>)</l>
						<l n="43" num="1.43"><w n="43.1">je</w> <w n="43.2">déflorai</w> <w n="43.3">la</w> <w n="43.4">sœur</w> <w n="43.5">du</w> <w n="43.6">Taïkoun</w> ; <w n="43.7">je</w> <w n="43.8">crois</w></l>
						<l n="44" num="1.44"><w n="44.1">qu</w>’<w n="44.2">il</w> <w n="44.3">voulut</w> <w n="44.4">rouspéter</w> : <w n="44.5">je</w> <w n="44.6">fis</w> <w n="44.7">clouer</w> <w n="44.8">en</w> <w n="44.9">croix</w></l>
						<l n="45" num="1.45"><w n="45.1">ce</w> <w n="45.2">bélître</w>, <w n="45.3">piller</w>, <w n="45.4">huit</w> <w n="45.5">jours</w>, <w n="45.6">sa</w> <w n="45.7">capitale</w></l>
						<l n="46" num="1.46"><w n="46.1">et</w> <w n="46.2">dévorer</w> <w n="46.3">son</w> <w n="46.4">fils</w> <w n="46.5">par</w> <w n="46.6">un</w> <w n="46.7">onocrotale</w> !</l>
						<l n="47" num="1.47"><w n="47.1">Ayant</w> <w n="47.2">sodomisé</w> <w n="47.3">Brunetière</w> <w n="47.4">et</w> <w n="47.5">Barrès</w>,</l>
						<l n="48" num="1.48"><w n="48.1">j</w>’<w n="48.2">exterminai</w> <w n="48.3">les</w> <w n="48.4">phansegars</w> <w n="48.5">de</w> <w n="48.6">Bénarés</w> !</l>
						<l n="49" num="1.49"><w n="49.1">À</w> <w n="49.2">Byzance</w> <w n="49.3">qu</w>’<w n="49.4">on</w> <w n="49.5">nommme</w> <w n="49.6">aussi</w> <w n="49.7">Constantinople</w>,</l>
						<l n="50" num="1.50"><w n="50.1">ô</w> <w n="50.2">Mahomet</w>, <w n="50.3">je</w> <w n="50.4">pris</w> <w n="50.5">ton</w> <w n="50.6">drapeau</w> <w n="50.7">de</w> <w n="50.8">sinople</w></l>
						<l n="51" num="1.51"><w n="51.1">pour</w> <w n="51.2">m</w>’<w n="51.3">absterger</w> <w n="51.4">le</w> <w n="51.5">fondement</w> <w n="51.6">et</w> <w n="51.7">j</w>’<w n="51.8">empalais</w>,</l>
						<l n="52" num="1.52"><w n="52.1">chaque</w> <w n="52.2">soir</w>, <w n="52.3">un</w> <w n="52.4">vizir</w> <w n="52.5">au</w> <w n="52.6">seuil</w> <w n="52.7">de</w> <w n="52.8">mon</w> <w n="52.9">palais</w> !</l>
						<l n="53" num="1.53"><w n="53.1">Ma</w> <w n="53.2">dague</w>, <w n="53.3">messeigneurs</w>, <w n="53.4">n</w>’<w n="53.5">est</w> <w n="53.6">pas</w> <w n="53.7">fille</w> <w n="53.8">des</w> <w n="53.9">rues</w> :</l>
						<l n="54" num="1.54"><w n="54.1">elle</w> <w n="54.2">a</w> <w n="54.3">trente</w>-<w n="54.4">et</w>-<w n="54.5">un</w> <w n="54.6">jours</w> <w n="54.7">dans</w> <w n="54.8">le</w> <w n="54.9">mois</w> <w n="54.10">ses</w> <w n="54.11">menstrues</w> !</l>
						<l n="55" num="1.55"><w n="55.1">En</w> <w n="55.2">pissant</w> <w n="55.3">j</w>’<w n="55.4">éteignis</w> <w n="55.5">le</w> <w n="55.6">Vésuve</w> <w n="55.7">et</w> <w n="55.8">l</w>’<w n="55.9">Hekla</w> ;</l>
						<l n="56" num="1.56"><w n="56.1">le</w> <w n="56.2">mont</w> <w n="56.3">Kinchinjinga</w> <w n="56.4">devant</w> <w n="56.5">moi</w> <w n="56.6">recula</w> !</l>
						<l n="57" num="1.57"><w n="57.1">Voulant</w> <w n="57.2">un</w> <w n="57.3">héritier</w>, <w n="57.4">sur</w> <w n="57.5">les</w> <w n="57.6">bords</w> <w n="57.7">du</w> <w n="57.8">Zambèze</w></l>
						<l n="58" num="1.58"><w n="58.1">Où</w> <w n="58.2">nage</w> <w n="58.3">en</w> <w n="58.4">reniflant</w> <w n="58.5">l</w>’<w n="58.6">hippopotame</w> <w n="58.7">obèse</w>,</l>
						<l n="59" num="1.59"><w n="59.1">dans</w> <w n="59.2">la</w> <w n="59.3">forêt</w>, <w n="59.4">séjour</w> <w n="59.5">du</w> <w n="59.6">mandrill</w> <w n="59.7">au</w> <w n="59.8">nez</w> <w n="59.9">bleu</w>,</l>
						<l n="60" num="1.60"><w n="60.1">sous</w> <w n="60.2">le</w> <w n="60.3">ciel</w> <w n="60.4">coruscant</w> <w n="60.5">et</w> <w n="60.6">les</w> <w n="60.7">rayons</w> <w n="60.8">de</w> <w n="60.9">feu</w></l>
						<l n="61" num="1.61"><w n="61.1">d</w>’<w n="61.2">un</w> <w n="61.3">soleil</w> <w n="61.4">infernal</w> <w n="61.5">que</w> <w n="61.6">le</w> <w n="61.7">Dyable</w> <w n="61.8">tisone</w>,</l>
						<l n="62" num="1.62"><w n="62.1">j</w>’<w n="62.2">eus</w> <w n="62.3">quatorze</w> <w n="62.4">bâtard</w> <w n="62.5">jumeaux</w> <w n="62.6">d</w>’<w n="62.7">une</w> <w n="62.8">Amazone</w>.</l>
						<l n="63" num="1.63"><w n="63.1">Parmi</w> <w n="63.2">ces</w> <w n="63.3">négrillons</w>, <w n="63.4">j</w>’<w n="63.5">élus</w> <w n="63.6">pour</w> <w n="63.7">mettre</w> <w n="63.8">à</w> <w n="63.9">part</w></l>
						<l n="64" num="1.64"><w n="64.1">le</w> <w n="64.2">plus</w> <w n="64.3">foncé</w>, <w n="64.4">jetant</w> <w n="64.5">le</w> <w n="64.6">reste</w> <w n="64.7">à</w> <w n="64.8">mon</w> <w n="64.9">chat</w>-<w n="64.10">pard</w> !</l>
						<l n="65" num="1.65"><w n="65.1">La</w> <w n="65.2">Reine</w> <w n="65.3">de</w> <w n="65.4">Saba</w>, <w n="65.5">misérable</w> <w n="65.6">femelle</w>,</l>
						<l n="66" num="1.66"><w n="66.1">voulut</w> <w n="66.2">me</w> <w n="66.3">résister</w> : <w n="66.4">je</w> <w n="66.5">coupai</w> <w n="66.6">sa</w> <w n="66.7">mamelle</w></l>
						<l n="67" num="1.67"><w n="67.1">senestre</w> <w n="67.2">pour</w> <w n="67.3">m</w>’<w n="67.4">en</w> <w n="67.5">faire</w> <w n="67.6">une</w> <w n="67.7">blague</w> <w n="67.8">et</w>, <w n="67.9">depuis</w>,</l>
						<l n="68" num="1.68"><w n="68.1">je</w> <w n="68.2">fis</w> <w n="68.3">coudre</w> <w n="68.4">en</w> <w n="68.5">un</w> <w n="68.6">sac</w> <w n="68.7">et</w> <w n="68.8">jeter</w> <w n="68.9">en</w> <w n="68.10">un</w> <w n="68.11">puits</w></l>
						<l n="69" num="1.69"><w n="69.1">la</w> <w n="69.2">fille</w> <w n="69.3">d</w>’<w n="69.4">un</w> <w n="69.5">rajah</w> <w n="69.6">parce</w> <w n="69.7">que</w> <w n="69.8">son</w> <w n="69.9">haleine</w></l>
						<l n="70" num="1.70"><w n="70.1">était</w> <w n="70.2">forte</w> <w n="70.3">et</w> <w n="70.4">je</w> <w n="70.5">fus</w> <w n="70.6">aimé</w> <w n="70.7">d</w>’<w n="70.8">une</w> <w n="70.9">baleine</w></l>
						<l n="71" num="1.71"><w n="71.1">géante</w> <w n="71.2">au</w> <w n="71.3">Pôle</w> <w n="71.4">Nord</w> (<w n="71.5">palsambleu</w> ! <w n="71.6">c</w>’<w n="71.7">est</w> <w n="71.8">assez</w></l>
						<l n="72" num="1.72"><w n="72.1">pervers</w>, <w n="72.2">qu</w>’<w n="72.3">en</w> <w n="72.4">dites</w>-<w n="72.5">vous</w> ? <w n="72.6">l</w>’<w n="72.7">amour</w> <w n="72.8">des</w> <w n="72.9">cétacés</w> !)</l>
						<l n="73" num="1.73"><w n="73.1">Fort</w> <w n="73.2">peu</w> <w n="73.3">de</w> <w n="73.4">temps</w> <w n="73.5">avant</w> <w n="73.6">que</w> <w n="73.7">je</w> <w n="73.8">ne</w> <w n="73.9">massacrasse</w></l>
						<l n="74" num="1.74"><w n="74.1">l</w>’<w n="74.2">affreux</w> <w n="74.3">Zéomébuch</w> <w n="74.4">et</w> <w n="74.5">tous</w> <w n="74.6">ceux</w> <w n="74.7">de</w> <w n="74.8">sa</w> <w n="74.9">race</w>,</l>
						<l n="75" num="1.75"><w n="75.1">dans</w> <w n="75.2">la</w> <w n="75.3">jungle</w> <w n="75.4">où</w> <w n="75.5">saignaient</w> <w n="75.6">des</w> <w n="75.7">fleurs</w> <w n="75.8">d</w>’<w n="75.9">alonzoas</w></l>
						<l n="76" num="1.76"><w n="76.1">je</w> <w n="76.2">dévorai</w> <w n="76.3">tout</w> <w n="76.4">crus</w> <w n="76.5">huit</w> <w n="76.6">cent</w> <w n="76.7">mille</w> <w n="76.8">boas</w>,</l>
						<l n="77" num="1.77"><w n="77.1">et</w> <w n="77.2">je</w> <w n="77.3">bus</w> <w n="77.4">du</w> <w n="77.5">venin</w> <w n="77.6">de</w> <w n="77.7">trigonocéphale</w> !</l>
						<l n="78" num="1.78"><w n="78.1">La</w> <w n="78.2">rafale</w> <w n="78.3">hurlait</w> ! <w n="78.4">je</w> <w n="78.5">dis</w> <w n="78.6">à</w> <w n="78.7">la</w> <w n="78.8">rafale</w> :</l>
						<l n="79" num="1.79">« — <w n="79.1">Qu</w>’<w n="79.2">on</w> <w n="79.3">se</w> <w n="79.4">taise</w> ! <w n="79.5">ou</w> <w n="79.6">mordieu</w>… »… <hi rend="ital"><w n="79.7">La</w> <w n="79.8">rafale</w> <w n="79.9">se</w> <w n="79.10">tut</w></hi> !</l>
						<l n="80" num="1.80"><w n="80.1">Répondez</w> ! <w n="80.2">Répondez</w>, <w n="80.3">bonzes</w> <w n="80.4">de</w> <w n="80.5">l</w>’<w n="80.6">Institut</w> :</l>
						<l n="81" num="1.81"><w n="81.1">mon</w> <hi rend="ital"><w n="81.2">Quos</w> <w n="81.3">ego</w></hi> <w n="81.4">vaut</w>-<w n="81.5">il</w> <w n="81.6">celui</w> <w n="81.7">du</w> <w n="81.8">sieur</w> <w n="81.9">Virgile</w> ?</l>
						<l n="82" num="1.82"><w n="82.1">Or</w> — <w n="82.2">j</w>’<w n="82.3">atteste</w> <w n="82.4">ceci</w> <w n="82.5">la</w> <w n="82.6">main</w> <w n="82.7">sur</w> <w n="82.8">l</w>’<w n="82.9">Évangile</w> ! —</l>
						<l n="83" num="1.83"><w n="83.1">un</w> <w n="83.2">matin</w>, <w n="83.3">il</w> <w n="83.4">me</w> <w n="83.5">plut</w> <w n="83.6">de</w> <w n="83.7">descendre</w> <w n="83.8">en</w> <w n="83.9">enfer</w></l>
						<l n="84" num="1.84"><w n="84.1">avant</w> <w n="84.2">le</w> <w n="84.3">déjeûner</w> ; <w n="84.4">mon</w> <w n="84.5">cousin</w> <w n="84.6">Lucifer</w></l>
						<l n="85" num="1.85"><w n="85.1">me</w> <w n="85.2">reçut</w> <w n="85.3">noblement</w> <w n="85.4">et</w> <w n="85.5">me</w> <w n="85.6">donna</w> <w n="85.7">mille</w> <w n="85.8">âmes</w></l>
						<l n="86" num="1.86"><w n="86.1">de</w> <w n="86.2">Juifs</w> <w n="86.3">à</w> <w n="86.4">torturer</w> ! <w n="86.5">Ensemble</w> <w n="86.6">nous</w> <w n="86.7">parlâmes</w></l>
						<l n="87" num="1.87"><w n="87.1">politique</w>, <w n="87.2">beaux</w>-<w n="87.3">arts</w> <w n="87.4">et</w> <w n="87.5">cætera</w>, <w n="87.6">je</w> <w n="87.7">vis</w></l>
						<l n="88" num="1.88"><w n="88.1">qu</w>’<w n="88.2">il</w> <w n="88.3">avait</w> <w n="88.4">du</w> <w n="88.5">bon</w> <w n="88.6">sens</w> : <w n="88.7">il</w> <w n="88.8">fut</w> <w n="88.9">de</w> <w n="88.10">mon</w> <w n="88.11">avis</w></l>
						<l n="89" num="1.89"><w n="89.1">en</w> <w n="89.2">tout</w> ; <w n="89.3">et</w> <w n="89.4">j</w>’<w n="89.5">urinai</w> <w n="89.6">dans</w> <w n="89.7">les</w> <w n="89.8">cent</w> <w n="89.9">trente</w> <w n="89.10">bouches</w></l>
						<l n="90" num="1.90"><w n="90.1">du</w> <w n="90.2">grand</w> <w n="90.3">Baal</w>-<w n="90.4">Zebub</w>, <w n="90.5">archi</w>-<w n="90.6">baron</w> <w n="90.7">des</w> <w n="90.8">mouches</w> !</l>
						<l n="91" num="1.91"><w n="91.1">L</w>’<w n="91.2">Océan</w> <w n="91.3">Pacifique</w> <w n="91.4">a</w> <w n="91.5">vu</w>, <w n="91.6">plus</w> <w n="91.7">d</w>’<w n="91.8">une</w> <w n="91.9">fois</w>,</l>
						<l n="92" num="1.92"><w n="92.1">son</w> <w n="92.2">flux</w> <w n="92.3">et</w> <w n="92.4">son</w> <w n="92.5">reflux</w> <w n="92.6">s</w>’<w n="92.7">arrêter</w> <w n="92.8">à</w> <w n="92.9">ma</w> <w n="92.10">voix</w> !</l>
						<l n="93" num="1.93"><w n="93.1">À</w> <w n="93.2">ma</w> <w n="93.3">voix</w>, <w n="93.4">les</w> <w n="93.5">pendus</w> <w n="93.6">chantaient</w> <w n="93.7">à</w> <w n="93.8">la</w> <w n="93.9">potence</w>…</l>
					</lg>
					<lg n="2">
						<l n="94" num="2.1"><w n="94.1">Or</w>, <w n="94.2">ayant</w> <w n="94.3">tout</w> <w n="94.4">rangé</w> <w n="94.5">sous</w> <w n="94.6">mon</w> <w n="94.7">omnipotence</w>,</l>
						<l n="95" num="2.2"><w n="95.1">les</w> <w n="95.2">Rois</w>, <w n="95.3">les</w> <w n="95.4">empereurs</w>, <w n="95.5">les</w> <w n="95.6">Dieux</w>, <w n="95.7">les</w> <w n="95.8">Éléments</w>,</l>
						<l n="96" num="2.3"><w n="96.1">servi</w> <w n="96.2">par</w> <w n="96.3">les</w> <w n="96.4">sorciers</w> <w n="96.5">et</w> <w n="96.6">par</w> <w n="96.7">les</w> <w n="96.8">nigromants</w>,</l>
						<l n="97" num="2.4"><w n="97.1">je</w> <w n="97.2">compris</w> <w n="97.3">que</w> <w n="97.4">la</w> <w n="97.5">vie</w> <w n="97.6">est</w> <w n="97.7">une</w> <w n="97.8">farce</w> <w n="97.9">amère</w></l>
						<l n="98" num="2.5"><w n="98.1">et</w>, <w n="98.2">pensif</w>, <w n="98.3">conculcant</w> <w n="98.4">les</w> <w n="98.5">cinq</w> <w n="98.6">mondes</w> <w n="98.7">vautrés</w></l>
						<l n="99" num="2.6"><w n="99.1">à</w> <w n="99.2">mes</w> <w n="99.3">pieds</w>, <w n="99.4">je</w> <w n="99.5">revins</w>, <w n="99.6">près</w> <w n="99.7">de</w> <w n="99.8">ma</w> <w n="99.9">vieille</w> <w n="99.10">mère</w>,</l>
						<l n="100" num="2.7"><w n="100.1">deviner</w> <w n="100.2">les</w> <w n="100.3">rébus</w> <w n="100.4">des</w> <w n="100.5">journaux</w> <w n="100.6">illustrés</w> !</l>
					</lg>
				</div></body></text></TEI>