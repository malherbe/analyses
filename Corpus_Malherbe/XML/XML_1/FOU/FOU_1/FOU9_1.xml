<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA NÉGRESSE BLONDE</title>
				<title type="medium">Édition électronique</title>
				<author key="FOU">
					<name>
						<forename>Georges</forename>
						<surname>FOUREST</surname>
					</name>
					<date from="1864" to="1945">1864-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1172 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref> 
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">FOU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>La Négresse blonde</title>
						<author>Georges Fourest</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/La_N%C3%A9gresse_blonde_(recueil)</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>La Négresse blonde</title>
								<author>Georges Fourest</author>
								<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k1269960g</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A MESSEIN</publisher>
									<date when="1909">1909</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1909">1909</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-08-02" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-08-02" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LA SINGESSE</head><div type="poem" key="FOU9">
					<head type="main">LA SINGESSE</head>
					<opener>
						<epigraph>
							<cit>
								<quote>
									I cannot conceive you to be human creatures <lb></lb>
									but a sort of species hardly a degree above a <lb></lb>
									monkey ; who has more diverting tricks than <lb></lb>
									any of you, is an animal less mischievous and <lb></lb>
									expensive.
								</quote>
								<bibl>
									<name>SWIFT</name> (Letter to a very young lady).
								</bibl>
							</cit>
						</epigraph>
					</opener>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Donc</w> <w n="1.2">voici</w> ! <w n="1.3">Moi</w>, <w n="1.4">Poète</w>, <w n="1.5">en</w> <w n="1.6">ma</w> <w n="1.7">haute</w> <w n="1.8">sagesse</w></l>
						<l n="2" num="1.2"><w n="2.1">Respuant</w> <w n="2.2">l</w>’<w n="2.3">Ève</w> <w n="2.4">à</w> <w n="2.5">qui</w> <w n="2.6">le</w> <w n="2.7">Père</w> <w n="2.8">succomba</w></l>
						<l n="3" num="1.3"><w n="3.1">J</w>’<w n="3.2">ai</w> <w n="3.3">choisi</w> <w n="3.4">pour</w> <w n="3.5">l</w>’<w n="3.6">aimer</w> <w n="3.7">une</w> <w n="3.8">jeune</w> <w n="3.9">singesse</w></l>
						<l n="4" num="1.4"><w n="4.1">Au</w> <w n="4.2">pays</w> <w n="4.3">noir</w> <w n="4.4">dans</w> <w n="4.5">la</w> <w n="4.6">forêt</w> <w n="4.7">de</w> <w n="4.8">Mayummba</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Fille</w> <w n="5.2">des</w> <w n="5.3">mandrills</w> <w n="5.4">verts</w>, <w n="5.5">ô</w> <w n="5.6">guenuche</w> <w n="5.7">d</w>’<w n="5.8">Afrique</w>,</l>
						<l n="6" num="2.2"><w n="6.1">Je</w> <w n="6.2">te</w> <w n="6.3">proclame</w> <w n="6.4">ici</w> <w n="6.5">la</w> <w n="6.6">reine</w> <w n="6.7">et</w> <w n="6.8">la</w> <w n="6.9">Vénus</w></l>
						<l n="7" num="2.3"><w n="7.1">Quadrumane</w>, <w n="7.2">et</w> <w n="7.3">je</w> <w n="7.4">bous</w> <w n="7.5">d</w>’<w n="7.6">une</w> <w n="7.7">ardeur</w> <w n="7.8">hystérique</w></l>
						<l n="8" num="2.4"><w n="8.1">Pour</w> <w n="8.2">les</w> <w n="8.3">callosités</w> <w n="8.4">qui</w> <w n="8.5">bordent</w> <w n="8.6">ton</w> <w n="8.7">anus</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">J</w>’<w n="9.2">aime</w> <w n="9.3">ton</w> <w n="9.4">cul</w> <w n="9.5">pelé</w>, <w n="9.6">tes</w> <w n="9.7">rides</w>, <w n="9.8">tes</w> <w n="9.9">bajoues</w></l>
						<l n="10" num="3.2"><w n="10.1">Et</w> <w n="10.2">je</w> <w n="10.3">proclamerai</w> <w n="10.4">devant</w> <w n="10.5">maintes</w> <w n="10.6">et</w> <w n="10.7">maints</w>,</l>
						<l n="11" num="3.3"><w n="11.1">Devant</w> <w n="11.2">monsieur</w> <w n="11.3">Reyer</w>, <w n="11.4">mordieu</w> ! <w n="11.5">que</w> <w n="11.6">tu</w> <w n="11.7">ne</w> <w n="11.8">joues</w></l>
						<l n="12" num="3.4"><w n="12.1">Oncques</w> <w n="12.2">du</w> <w n="12.3">piano</w> <w n="12.4">malgré</w> <w n="12.5">tes</w> <w n="12.6">quatre</w> <w n="12.7">mains</w> ;</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Et</w> <w n="13.2">comme</w> <w n="13.3">Salomon</w> <w n="13.4">pour</w> <w n="13.5">l</w>’<w n="13.6">enfant</w> <w n="13.7">sémitique</w>,</l>
						<l n="14" num="4.2"><w n="14.1">La</w> <w n="14.2">perle</w> <w n="14.3">d</w>’<w n="14.4">Issachar</w> <w n="14.5">offerte</w> <w n="14.6">au</w> <w n="14.7">bien</w>-<w n="14.8">aimé</w>,</l>
						<l n="15" num="4.3"><w n="15.1">J</w>’<w n="15.2">entonnerai</w> <w n="15.3">pour</w> <w n="15.4">toi</w> <w n="15.5">l</w>’<w n="15.6">énamouré</w> <w n="15.7">cantique</w>,</l>
						<l n="16" num="4.4"><w n="16.1">Ô</w> <w n="16.2">ma</w> <w n="16.3">tour</w> <w n="16.4">de</w> <w n="16.5">David</w>, <w n="16.6">ô</w> <w n="16.7">mon</w> <w n="16.8">jardin</w> <w n="16.9">fermé</w>…</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">C</w>’<w n="17.2">était</w> <w n="17.3">dans</w> <w n="17.4">la</w> <w n="17.5">forêt</w> <w n="17.6">vierge</w> <w n="17.7">sous</w> <w n="17.8">les</w> <w n="17.9">tropiques</w></l>
						<l n="18" num="5.2"><w n="18.1">Où</w> <w n="18.2">s</w>’<w n="18.3">ouvre</w> <w n="18.4">en</w> <w n="18.5">éventail</w> <w n="18.6">le</w> <w n="18.7">palmier</w> <w n="18.8">chamœrops</w> ;</l>
						<l n="19" num="5.3"><w n="19.1">Dans</w> <w n="19.2">le</w> <w n="19.3">soir</w> <w n="19.4">alangui</w> <w n="19.5">d</w>’<w n="19.6">effluves</w> <w n="19.7">priapiques</w></l>
						<l n="20" num="5.4"><w n="20.1">Stridait</w>, <w n="20.2">rauque</w>, <w n="20.3">le</w> <w n="20.4">cri</w> <w n="20.5">des</w> <w n="20.6">nyctalomerops</w> ;</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">L</w>’<w n="21.2">heure</w> <w n="21.3">glissait</w>, <w n="21.4">nocturne</w>, <w n="21.5">où</w> <w n="21.6">gazelles</w>, <w n="21.7">girafes</w>,</l>
						<l n="22" num="6.2"><w n="22.1">Couaggas</w>, <w n="22.2">éléphants</w>, <w n="22.3">zèbres</w>, <w n="22.4">zébus</w>, <w n="22.5">springbocks</w> <ref target="1" type="noteAnchor">1</ref></l>
						<l n="23" num="6.3"><w n="23.1">Vont</w> <w n="23.2">boire</w> <w n="23.3">aux</w> <w n="23.4">zihouas</w> <w n="23.5">sans</w> <w n="23.6">verres</w> <w n="23.7">ni</w> <w n="23.8">carafes</w></l>
						<l n="24" num="6.4"><w n="24.1">Laissant</w> <w n="24.2">l</w>’<w n="24.3">homme</w> <w n="24.4">pervers</w> <w n="24.5">s</w>’<w n="24.6">intoxiquer</w> <w n="24.7">de</w> <w n="24.8">bocks</w> ;</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">Sous</w> <w n="25.2">les</w> <w n="25.3">cactus</w> <w n="25.4">en</w> <w n="25.5">feu</w> <w n="25.6">tout</w> <w n="25.7">droits</w> <w n="25.8">comme</w> <w n="25.9">des</w> <w n="25.10">cierges</w></l>
						<l n="26" num="7.2"><w n="26.1">Des</w> <w n="26.2">lianes</w> <w n="26.3">rampaient</w> (<w n="26.4">nullement</w> <w n="26.5">de</w> <w n="26.6">Pougy</w>) ;</l>
						<l n="27" num="7.3"><w n="27.1">Autant</w> <w n="27.2">que</w> <w n="27.3">la</w> <w n="27.4">forêt</w> <w n="27.5">ma</w> <w n="27.6">Singesse</w> <w n="27.7">était</w> <w n="27.8">vierge</w> ;</l>
						<l n="28" num="7.4"><w n="28.1">De</w> <w n="28.2">son</w> <w n="28.3">sang</w> <w n="28.4">virginal</w> <w n="28.5">l</w>’<w n="28.6">humus</w> <w n="28.7">était</w> <w n="28.8">rougi</w>.</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1"><w n="29.1">Le</w> <w n="29.2">premier</w>, <w n="29.3">j</w>’<w n="29.4">écartai</w> <w n="29.5">ses</w> <w n="29.6">lèvres</w> <w n="29.7">de</w> <w n="29.8">pucelle</w></l>
						<l n="30" num="8.2"><w n="30.1">En</w> <w n="30.2">un</w> <w n="30.3">rut</w> <w n="30.4">triomphal</w>, <w n="30.5">oublieux</w> <w n="30.6">de</w> <w n="30.7">Malthus</w>,</l>
						<l n="31" num="8.3"><w n="31.1">Et</w> <w n="31.2">des</w> <w n="31.3">parfums</w> <w n="31.4">salés</w> <w n="31.5">montaient</w> <w n="31.6">de</w> <w n="31.7">son</w> <w n="31.8">aisselle</w></l>
						<l n="32" num="8.4"><w n="32.1">Et</w> <w n="32.2">des</w> <w n="32.3">parfums</w> <w n="32.4">pleuvaient</w> <w n="32.5">des</w> <w n="32.6">larysacanthus</w> ;</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1"><w n="33.1">Elle</w> <w n="33.2">se</w> <w n="33.3">redressa</w>, <w n="33.4">fière</w> <w n="33.5">de</w> <w n="33.6">sa</w> <w n="33.7">blessure</w>,</l>
						<l n="34" num="9.2"><w n="34.1">À</w> <w n="34.2">demi</w> <w n="34.3">souriante</w> <w n="34.4">et</w> <w n="34.5">confuse</w> <w n="34.6">à</w> <w n="34.7">demi</w> ;</l>
						<l n="35" num="9.3"><w n="35.1">Le</w> <w n="35.2">rugissement</w> <w n="35.3">fou</w> <w n="35.4">de</w> <w n="35.5">notre</w> <w n="35.6">jouissure</w></l>
						<l n="36" num="9.4"><w n="36.1">Arrachait</w> <w n="36.2">au</w> <w n="36.3">repos</w> <w n="36.4">le</w> <w n="36.5">chacal</w> <w n="36.6">endormi</w>.</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1"><w n="37.1">Sept</w> <w n="37.2">fois</w> <w n="37.3">je</w> <w n="37.4">la</w> <w n="37.5">repris</w>, <w n="37.6">lascive</w> : <w n="37.7">son</w> <w n="37.8">œil</w> <w n="37.9">jaune</w></l>
						<l n="38" num="10.2"><w n="38.1">Clignottait</w>, <w n="38.2">langoureux</w>, <w n="38.3">tour</w> <w n="38.4">à</w> <w n="38.5">tour</w>, <w n="38.6">et</w> <w n="38.7">mutin</w> ;</l>
						<l n="39" num="10.3"><w n="39.1">La</w> <w n="39.2">Dryade</w> <w n="39.3">amoureuse</w> <w n="39.4">aux</w> <w n="39.5">bras</w> <w n="39.6">du</w> <w n="39.7">jeune</w> <w n="39.8">Faune</w></l>
						<l n="40" num="10.4"><w n="40.1">A</w> <w n="40.2">moins</w> <w n="40.3">d</w>’<w n="40.4">amour</w> <w n="40.5">en</w> <w n="40.6">fleurs</w> <w n="40.7">et</w> <w n="40.8">d</w>’<w n="40.9">esprit</w> <w n="40.10">libertin</w> !</l>
					</lg>
					<lg n="11">
						<l n="41" num="11.1"><w n="41.1">Toi</w>, <w n="41.2">Fille</w> <w n="41.3">des</w> <w n="41.4">humains</w>, <w n="41.5">triste</w> <w n="41.6">poupée</w> <w n="41.7">humaine</w></l>
						<l n="42" num="11.2"><w n="42.1">Au</w> <w n="42.2">ventre</w> <w n="42.3">plein</w> <w n="42.4">de</w> <w n="42.5">son</w>, <w n="42.6">tondeuse</w> <w n="42.7">de</w> <w n="42.8">Samson</w>,</l>
						<l n="43" num="11.3"><w n="43.1">Dalila</w>, <w n="43.2">Bovary</w>, <w n="43.3">Marneffe</w> <w n="43.4">ou</w> <w n="43.5">Celimène</w>,</l>
						<l n="44" num="11.4"><w n="44.1">Contemple</w> <w n="44.2">mon</w> <w n="44.3">épouse</w> <w n="44.4">et</w> <w n="44.5">retiens</w> <w n="44.6">sa</w> <w n="44.7">leçon</w> :</l>
					</lg>
					<lg n="12">
						<l n="45" num="12.1"><w n="45.1">Mon</w> <w n="45.2">épouse</w> <w n="45.3">est</w> <w n="45.4">loyale</w> <w n="45.5">et</w> <w n="45.6">très</w> <w n="45.7">chaste</w> <w n="45.8">et</w> <w n="45.9">soumise</w></l>
						<l n="46" num="12.2"><w n="46.1">Et</w> <w n="46.2">j</w>’<w n="46.3">adore</w> <w n="46.4">la</w> <w n="46.5">voir</w>, <w n="46.6">aux</w> <w n="46.7">matins</w> <w n="46.8">ingénus</w>,</l>
						<l n="47" num="12.3"><w n="47.1">Le</w> <w n="47.2">cœur</w> <w n="47.3">sans</w> <w n="47.4">artifice</w> <w n="47.5">et</w> <w n="47.6">le</w> <w n="47.7">corps</w> <w n="47.8">sans</w> <w n="47.9">chemise</w>,</l>
						<l n="48" num="12.4"><w n="48.1">Au</w> <w n="48.2">soleil</w> <w n="48.3">tropical</w>, <w n="48.4">montrer</w> <w n="48.5">ses</w> <w n="48.6">charmes</w> <w n="48.7">nus</w> ;</l>
					</lg>
					<lg n="13">
						<l n="49" num="13.1"><w n="49.1">Elle</w> <w n="49.2">sait</w> <w n="49.3">me</w> <w n="49.4">choisir</w> <w n="49.5">ignames</w> <w n="49.6">et</w> <w n="49.7">goyaves</w> ;</l>
						<l n="50" num="13.2"><w n="50.1">Lorsque</w> <w n="50.2">nous</w> <w n="50.3">cheminons</w> <w n="50.4">par</w> <w n="50.5">les</w> <w n="50.6">sentiers</w> <w n="50.7">étroits</w>,</l>
						<l n="51" num="13.3"><w n="51.1">Ses</w> <w n="51.2">mains</w> <w n="51.3">aux</w> <w n="51.4">doigts</w> <w n="51.5">velus</w> <w n="51.6">écartent</w> <w n="51.7">les</w> <w n="51.8">agaves</w>,</l>
						<l n="52" num="13.4"><w n="52.1">Tel</w> <w n="52.2">un</w> <w n="52.3">page</w> <w n="52.4">attentif</w> <w n="52.5">marchant</w> <w n="52.6">devant</w> <w n="52.7">les</w> <w n="52.8">rois</w>,</l>
					</lg>
					<lg n="14">
						<l n="53" num="14.1"><w n="53.1">Puis</w> <w n="53.2">dans</w> <w n="53.3">ma</w> <w n="53.4">chevelure</w> <w n="53.5">oublieuse</w> <w n="53.6">du</w> <w n="53.7">peigne</w></l>
						<l n="54" num="14.2"><w n="54.1">Avec</w> <w n="54.2">précaution</w> <w n="54.3">elle</w> <w n="54.4">cherche</w> <w n="54.5">les</w> <w n="54.6">poux</w></l>
						<l n="55" num="14.3"><w n="55.1">Satisfaite</w> <w n="55.2">pourvu</w> <w n="55.3">que</w> <w n="55.4">d</w>’<w n="55.5">un</w> <w n="55.6">sourire</w> <w n="55.7">daigne</w></l>
						<l n="56" num="14.4"><w n="56.1">La</w> <w n="56.2">payer</w>, <w n="56.3">une</w> <w n="56.4">fois</w>, <w n="56.5">le</w> <w n="56.6">Seigneur</w> <w n="56.7">et</w> <w n="56.8">l</w>’<w n="56.9">Époux</w>.</l>
					</lg>
					<lg n="15">
						<l n="57" num="15.1"><w n="57.1">Si</w> <w n="57.2">quelque</w> <w n="57.3">souvenir</w> <w n="57.4">de</w> <w n="57.5">souleur</w> <w n="57.6">morte</w> <w n="57.7">amasse</w></l>
						<l n="58" num="15.2"><w n="58.1">Des</w> <w n="58.2">rides</w> <w n="58.3">sur</w> <w n="58.4">mon</w> <w n="58.5">front</w> <w n="58.6">que</w> <w n="58.7">l</w>’<w n="58.8">ennui</w> <w n="58.9">foudroya</w>,</l>
						<l n="59" num="15.3"><w n="59.1">Pour</w> <w n="59.2">divertir</w> <w n="59.3">son</w> <w n="59.4">maître</w> <w n="59.5">elle</w> <w n="59.6">fait</w> <w n="59.7">la</w> <w n="59.8">grimace</w>,</l>
						<l n="60" num="15.4"><w n="60.1">Grotesque</w> <w n="60.2">et</w> <w n="60.3">fantastique</w> <w n="60.4">à</w> <w n="60.5">délecter</w> <w n="60.6">Goya</w> !</l>
					</lg>
					<lg n="16">
						<l n="61" num="16.1"><w n="61.1">Un</w> <w n="61.2">étrange</w> <w n="61.3">rictus</w> <w n="61.4">tord</w> <w n="61.5">sa</w> <w n="61.6">narine</w> <w n="61.7">bleue</w>,</l>
						<l n="62" num="16.2"><w n="62.1">Elle</w> <w n="62.2">se</w> <w n="62.3">gratte</w> <w n="62.4">d</w>’<w n="62.5">un</w> <w n="62.6">geste</w> <w n="62.7">obscène</w> <w n="62.8">et</w> <w n="62.9">joli</w></l>
						<l n="63" num="16.3"><w n="63.1">La</w> <w n="63.2">fesse</w> <w n="63.3">puis</w> <w n="63.4">s</w>’<w n="63.5">accroche</w> <w n="63.6">aux</w> <w n="63.7">branches</w> <w n="63.8">par</w> <w n="63.9">la</w> <w n="63.10">queue</w></l>
						<l n="64" num="16.4"><w n="64.1">En</w> <w n="64.2">bondissant</w>, <w n="64.3">Footit</w>, <w n="64.4">Littl</w>-<w n="64.5">Tich</w>. <w n="64.6">Hanlon</w>-<w n="64.7">Lee</w> !</l>
					</lg>
					<lg n="17">
						<l n="65" num="17.1"><w n="65.1">Mais</w> <w n="65.2">soudain</w> <w n="65.3">la</w> <w n="65.4">voilà</w> <w n="65.5">très</w> <w n="65.6">grave</w> ! <w n="65.7">Sa</w> <w n="65.8">mimique</w></l>
						<l n="66" num="17.2"><w n="66.1">Me</w> <w n="66.2">dicte</w> <w n="66.3">et</w> <w n="66.4">je</w> <w n="66.5">sais</w> <w n="66.6">lire</w> <w n="66.7">en</w> <w n="66.8">ses</w> <w n="66.9">regards</w> <w n="66.10">profonds</w></l>
						<l n="67" num="17.3"><w n="67.1">Des</w> <w n="67.2">vocables</w> <w n="67.3">muets</w> <w n="67.4">au</w> <w n="67.5">sens</w> <w n="67.6">métaphysique</w></l>
						<l n="68" num="17.4"><w n="68.1">Je</w> <w n="68.2">comprends</w> <w n="68.3">son</w> <w n="68.4">langage</w> <w n="68.5">et</w> <w n="68.6">nous</w> <w n="68.7">philosophons</w> :</l>
					</lg>
					<lg n="18">
						<l n="69" num="18.1"><w n="69.1">Elle</w> <w n="69.2">croit</w> <w n="69.3">en</w> <w n="69.4">un</w> <w n="69.5">Dieu</w> <w n="69.6">par</w> <w n="69.7">qui</w> <w n="69.8">le</w> <w n="69.9">soleil</w> <w n="69.10">brille</w></l>
						<l n="70" num="18.2"><w n="70.1">Qui</w> <w n="70.2">créa</w> <w n="70.3">l</w>’<w n="70.4">univers</w> <w n="70.5">pour</w> <w n="70.6">le</w> <w n="70.7">bon</w> <w n="70.8">chimpanzé</w></l>
						<l n="71" num="18.3"><w n="71.1">Puis</w> <w n="71.2">dont</w> <w n="71.3">le</w> <w n="71.4">Fils</w>-<w n="71.5">Unique</w>, <w n="71.6">un</w> <w n="71.7">jour</w> <w n="71.8">s</w>’<w n="71.9">est</w> <w n="71.10">fait</w> <w n="71.11">gorille</w></l>
						<l n="72" num="18.4"><w n="72.1">Pour</w> <w n="72.2">ravir</w> <w n="72.3">le</w> <w n="72.4">pécheur</w> <w n="72.5">à</w> <w n="72.6">l</w>’<w n="72.7">enfer</w> <w n="72.8">embrasé</w> !</l>
					</lg>
					<lg n="19">
						<l n="73" num="19.1"><w n="73.1">Simiesque</w> <w n="73.2">Iaveh</w> <w n="73.3">de</w> <w n="73.4">la</w> <w n="73.5">forêt</w> <w n="73.6">immense</w></l>
						<l n="74" num="19.2"><w n="74.1">Ô</w> <w n="74.2">Zeus</w> <w n="74.3">omnipotent</w> <w n="74.4">de</w> <w n="74.5">l</w>’<w n="74.6">Animalité</w>,</l>
						<l n="75" num="19.3"><w n="75.1">Fais</w> <w n="75.2">germer</w> <w n="75.3">en</w> <w n="75.4">ses</w> <w n="75.5">flancs</w> <w n="75.6">et</w> <w n="75.7">croître</w> <w n="75.8">ma</w> <w n="75.9">semence</w>,</l>
						<l n="76" num="19.4"><w n="76.1">Ouvre</w> <w n="76.2">son</w> <w n="76.3">utérus</w> <w n="76.4">à</w> <w n="76.5">la</w> <w n="76.6">maternité</w></l>
					</lg>
					<lg n="20">
						<l n="77" num="20.1"><w n="77.1">Car</w> <w n="77.2">je</w> <w n="77.3">veux</w> <w n="77.4">voir</w> <w n="77.5">issus</w> <w n="77.6">de</w> <w n="77.7">sa</w> <w n="77.8">vulve</w> <w n="77.9">féconde</w></l>
						<l n="78" num="20.2"><w n="78.1">Nos</w> <w n="78.2">enfants</w> <w n="78.3">libérés</w> <w n="78.4">d</w>’<w n="78.5">atavismes</w> <w n="78.6">humains</w></l>
						<l n="79" num="20.3"><w n="79.1">Aux</w> <w n="79.2">obroontchoas</w> <w n="79.3">que</w> <w n="79.4">la</w> <w n="79.5">serpe</w> <w n="79.6">n</w>’<w n="79.7">émonde</w></l>
						<l n="80" num="20.4"><w n="80.1">Jamais</w> <w n="80.2">en</w> <w n="80.3">grimaçant</w> <w n="80.4">grimper</w> <w n="80.5">à</w> <w n="80.6">quatre</w> <w n="80.7">mains</w> !…</l>
					</lg>
					<lg n="21">
						<l n="81" num="21.1"><w n="81.1">Et</w> <w n="81.2">dans</w> <w n="81.3">l</w>’<w n="81.4">espoir</w> <w n="81.5">sacré</w> <w n="81.6">d</w>’<w n="81.7">une</w> <w n="81.8">progéniture</w></l>
						<l n="82" num="21.2"><w n="82.1">Sans</w> <w n="82.2">lois</w>, <w n="82.3">sans</w> <w n="82.4">préjugés</w>, <w n="82.5">sans</w> <w n="82.6">rêves</w> <w n="82.7">décevants</w></l>
						<l n="83" num="21.3"><w n="83.1">Nous</w> <w n="83.2">offrons</w> <w n="83.3">notre</w> <w n="83.4">amour</w> <w n="83.5">à</w> <w n="83.6">la</w> <w n="83.7">grande</w> <w n="83.8">Nature</w>,</l>
						<l n="84" num="21.4"><w n="84.1">Fiers</w> <w n="84.2">comme</w> <w n="84.3">les</w> <w n="84.4">palmiers</w>, <w n="84.5">libres</w> <w n="84.6">comme</w> <w n="84.7">les</w> <w n="84.8">vents</w> ! ! !</l>
					</lg>
					<closer>
						<note type="footnote" id="1">
							Etc., etc. <lb></lb>
							(Note de l’Auteur.)
						</note>
					</closer>
				</div></body></text></TEI>