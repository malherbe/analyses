<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES DIVERSES</title>
				<title type="medium">Une édition électronique</title>
				<author key="CHA">
					<name>
						<forename>François-René</forename>
						<nameLink>de</nameLink>
						<surname>CHATEAUBRIAND</surname>
					</name>
					<date from="1768" to="1848">1768-1848</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>735 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">CHA_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies diverses</title>
						<author>François-René de Chateaubriand</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">https://www.poesies.net/chateaubriandpoesiesdiverses.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition papier d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1797" to="1827">1797-1827</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface n’est pas incluse.</p>
				<p>Les notes de l’auteur ont été intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>Des corrections métriques ont été apportées sans autres attestations publiées.</p>
				<p>Les vers de la strophe illisible du poème "SERMENT — LES NOIRS DEVANT LE GIBET DE JOHN BROWN" ont été marqués comme inanalysables.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
 					<p>L’orthographe a été vérifiée avec le correcteur Aspell.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="CHA17">
				<head type="number">VII</head>
				<head type="main">Ballade de l’Abencerage</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Le</w> <w n="1.2">roi</w> <w n="1.3">don</w> <w n="1.4">Juan</w></l>
					<l n="2" num="1.2"><w n="2.1">Un</w> <w n="2.2">jour</w> <w n="2.3">chevauchant</w></l>
					<l n="3" num="1.3"><w n="3.1">Vit</w> <w n="3.2">sur</w> <w n="3.3">la</w> <w n="3.4">montagne</w></l>
					<l n="4" num="1.4"><w n="4.1">Grenade</w> <w n="4.2">d</w>’<w n="4.3">Espagne</w> ;</l>
					<l n="5" num="1.5"><w n="5.1">Il</w> <w n="5.2">lui</w> <w n="5.3">dit</w> <w n="5.4">soudain</w> :</l>
					<l n="6" num="1.6"><w n="6.1">Cité</w> <w n="6.2">mignonne</w>,</l>
					<l n="7" num="1.7"><w n="7.1">Mon</w> <w n="7.2">cœur</w> <w n="7.3">te</w> <w n="7.4">donne</w></l>
					<l n="8" num="1.8"><w n="8.1">Avec</w> <w n="8.2">ma</w> <w n="8.3">main</w>.</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1"><w n="9.1">Je</w> <w n="9.2">t</w>’<w n="9.3">épouserai</w>,</l>
					<l n="10" num="2.2"><w n="10.1">Puis</w> <w n="10.2">apporterai</w></l>
					<l n="11" num="2.3"><w n="11.1">En</w> <w n="11.2">dons</w> <w n="11.3">à</w> <w n="11.4">ta</w> <w n="11.5">ville</w></l>
					<l n="12" num="2.4"><w n="12.1">Cordoue</w> <w n="12.2">et</w> <w n="12.3">Séville</w>.</l>
					<l n="13" num="2.5"><w n="13.1">Superbes</w> <w n="13.2">atours</w></l>
					<l n="14" num="2.6"><w n="14.1">Et</w> <w n="14.2">perles</w> <w n="14.3">fines</w></l>
					<l n="15" num="2.7"><w n="15.1">Je</w> <w n="15.2">te</w> <w n="15.3">destine</w></l>
					<l n="16" num="2.8"><w n="16.1">Pour</w> <w n="16.2">nos</w> <w n="16.3">amours</w>.</l>
				</lg>
				<lg n="3">
					<l n="17" num="3.1"><w n="17.1">Grenade</w> <w n="17.2">répond</w> :</l>
					<l n="18" num="3.2"><w n="18.1">Grand</w> <w n="18.2">roi</w> <w n="18.3">de</w> <w n="18.4">Léon</w>,</l>
					<l n="19" num="3.3"><w n="19.1">Au</w> <w n="19.2">Maure</w> <w n="19.3">liée</w>,</l>
					<l n="20" num="3.4"><w n="20.1">Je</w> <w n="20.2">suis</w> <w n="20.3">mariée</w>.</l>
					<l n="21" num="3.5"><w n="21.1">Garde</w> <w n="21.2">tes</w> <w n="21.3">présents</w> :</l>
					<l n="22" num="3.6"><w n="22.1">J</w>’<w n="22.2">ai</w> <w n="22.3">pour</w> <w n="22.4">parure</w></l>
					<l n="23" num="3.7"><w n="23.1">Riche</w> <w n="23.2">ceinture</w></l>
					<l n="24" num="3.8"><w n="24.1">Et</w> <w n="24.2">beaux</w> <w n="24.3">enfants</w>.</l>
				</lg>
				<lg n="4">
					<l n="25" num="4.1"><w n="25.1">Ainsi</w> <w n="25.2">tu</w> <w n="25.3">disais</w> ;</l>
					<l n="26" num="4.2"><w n="26.1">Ainsi</w> <w n="26.2">tu</w> <w n="26.3">mentais</w>.</l>
					<l n="27" num="4.3"><w n="27.1">O</w> <w n="27.2">mortelle</w> <w n="27.3">injure</w> !</l>
					<l n="28" num="4.4"><w n="28.1">Grenade</w> <w n="28.2">est</w> <w n="28.3">parjure</w> !</l>
					<l n="29" num="4.5"><w n="29.1">Un</w> <w n="29.2">chrétien</w> <w n="29.3">maudit</w></l>
					<l n="30" num="4.6"><w n="30.1">D</w>’<w n="30.2">Abencerage</w></l>
					<l n="31" num="4.7"><w n="31.1">Tient</w> <w n="31.2">l</w>’<w n="31.3">héritage</w> :</l>
					<l n="32" num="4.8"><w n="32.1">C</w>’<w n="32.2">était</w> <w n="32.3">écrit</w> !</l>
				</lg>
				<lg n="5">
					<l n="33" num="5.1"><w n="33.1">Jamais</w> <w n="33.2">le</w> <w n="33.3">chameau</w></l>
					<l n="34" num="5.2"><w n="34.1">N</w>’<w n="34.2">apporte</w> <w n="34.3">au</w> <w n="34.4">tombeau</w>,</l>
					<l n="35" num="5.3"><w n="35.1">Près</w> <w n="35.2">de</w> <w n="35.3">la</w> <w n="35.4">piscine</w>,</l>
					<l n="36" num="5.4"><w n="36.1">L</w>’<w n="36.2">haggi</w> <w n="36.3">de</w> <w n="36.4">Médine</w>.</l>
					<l n="37" num="5.5"><w n="37.1">Un</w> <w n="37.2">chrétien</w> <w n="37.3">maudit</w></l>
					<l n="38" num="5.6"><w n="38.1">D</w>’<w n="38.2">Abencerage</w></l>
					<l n="39" num="5.7"><w n="39.1">Tient</w> <w n="39.2">l</w>’<w n="39.3">héritage</w> :</l>
					<l n="40" num="5.8"><w n="40.1">C</w>’<w n="40.2">était</w> <w n="40.3">écrit</w> !</l>
				</lg>
				<lg n="6">
					<l n="41" num="6.1"><w n="41.1">O</w> <w n="41.2">bel</w> <w n="41.3">Alhambra</w> !</l>
					<l n="42" num="6.2"><w n="42.1">O</w> <w n="42.2">palais</w> <w n="42.3">d</w>’<w n="42.4">Allah</w> !</l>
					<l n="43" num="6.3"><w n="43.1">Cité</w> <w n="43.2">des</w> <w n="43.3">fontaines</w> !</l>
					<l n="44" num="6.4"><w n="44.1">Fleuve</w> <w n="44.2">aux</w> <w n="44.3">vertes</w> <w n="44.4">plaines</w> !</l>
					<l n="45" num="6.5"><w n="45.1">Un</w> <w n="45.2">chrétien</w> <w n="45.3">maudit</w></l>
					<l n="46" num="6.6"><w n="46.1">D</w>’<w n="46.2">Abencerage</w></l>
					<l n="47" num="6.7"><w n="47.1">Tient</w> <w n="47.2">l</w>’<w n="47.3">héritage</w> :</l>
					<l n="48" num="6.8"><w n="48.1">C</w>’<w n="48.2">était</w> <w n="48.3">écrit</w> !</l>
				</lg>
			</div></body></text></TEI>