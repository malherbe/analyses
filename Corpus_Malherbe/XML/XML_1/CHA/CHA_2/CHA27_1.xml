<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES DIVERSES</title>
				<title type="medium">Une édition électronique</title>
				<author key="CHA">
					<name>
						<forename>François-René</forename>
						<nameLink>de</nameLink>
						<surname>CHATEAUBRIAND</surname>
					</name>
					<date from="1768" to="1848">1768-1848</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>735 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">CHA_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies diverses</title>
						<author>François-René de Chateaubriand</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">https://www.poesies.net/chateaubriandpoesiesdiverses.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition papier d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1797" to="1827">1797-1827</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface n’est pas incluse.</p>
				<p>Les notes de l’auteur ont été intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>Des corrections métriques ont été apportées sans autres attestations publiées.</p>
				<p>Les vers de la strophe illisible du poème "SERMENT — LES NOIRS DEVANT LE GIBET DE JOHN BROWN" ont été marqués comme inanalysables.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
 					<p>L’orthographe a été vérifiée avec le correcteur Aspell.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="CHA27">
				<head type="number">XVII</head>
				<head type="main">Les Alpes ou l’Italie</head>
				<opener>
					<dateline>
						<date when="1822">1822</date>.
					</dateline>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Donc</w> <w n="1.2">reconnaissez</w>-<w n="1.3">vous</w> <w n="1.4">au</w> <w n="1.5">fond</w> <w n="1.6">de</w> <w n="1.7">vos</w> <w n="1.8">abîmes</w></l>
					<l n="2" num="1.2"><w n="2.1">Ce</w> <w n="2.2">voyageur</w> <w n="2.3">pensif</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Au</w> <w n="3.2">cœur</w> <w n="3.3">triste</w>, <w n="3.4">aux</w> <w n="3.5">cheveux</w> <w n="3.6">blanchis</w> <w n="3.7">comme</w> <w n="3.8">vos</w> <w n="3.9">cimes</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Au</w> <w n="4.2">pas</w> <w n="4.3">lent</w> <w n="4.4">et</w> <w n="4.5">tardif</w> ?</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Jadis</w> <w n="5.2">de</w> <w n="5.3">ce</w> <w n="5.4">vieux</w> <w n="5.5">bois</w>, <w n="5.6">où</w> <w n="5.7">fait</w> <w n="5.8">une</w> <w n="5.9">eau</w> <w n="5.10">limpide</w>,</l>
					<l n="6" num="2.2"><w n="6.1">Je</w> <w n="6.2">sondais</w> <w n="6.3">l</w>’<w n="6.4">épaisseur</w></l>
					<l n="7" num="2.3"><w n="7.1">Hardi</w> <w n="7.2">comme</w> <w n="7.3">un</w> <w n="7.4">aiglon</w>, <w n="7.5">comme</w> <w n="7.6">un</w> <w n="7.7">chevreuil</w> <w n="7.8">rapide</w>,</l>
					<l n="8" num="2.4"><w n="8.1">Et</w> <w n="8.2">gai</w> <w n="8.3">comme</w> <w n="8.4">un</w> <w n="8.5">chasseur</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Alpes</w>, <w n="9.2">vous</w> <w n="9.3">n</w>’<w n="9.4">avez</w> <w n="9.5">point</w> <w n="9.6">subi</w> <w n="9.7">mes</w> <w n="9.8">destinées</w> !</l>
					<l n="10" num="3.2"><w n="10.1">Le</w> <w n="10.2">temps</w> <w n="10.3">ne</w> <w n="10.4">vous</w> <w n="10.5">peut</w> <w n="10.6">rien</w>.</l>
					<l n="11" num="3.3"><w n="11.1">Vos</w> <w n="11.2">fronts</w> <w n="11.3">légèrement</w> <w n="11.4">ont</w> <w n="11.5">porté</w> <w n="11.6">les</w> <w n="11.7">années</w></l>
					<l n="12" num="3.4"><w n="12.1">Qui</w> <w n="12.2">pèsent</w> <w n="12.3">sur</w> <w n="12.4">le</w> <w n="12.5">mien</w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Pour</w> <w n="13.2">la</w> <w n="13.3">première</w> <w n="13.4">fois</w>, <w n="13.5">quand</w>, <w n="13.6">rempli</w> <w n="13.7">d</w>’<w n="13.8">espérance</w>,</l>
					<l n="14" num="4.2"><w n="14.1">Je</w> <w n="14.2">franchis</w> <w n="14.3">vos</w> <w n="14.4">remparts</w>,</l>
					<l n="15" num="4.3"><w n="15.1">Ainsi</w> <w n="15.2">que</w> <w n="15.3">l</w>’<w n="15.4">horizon</w>, <w n="15.5">un</w> <w n="15.6">avenir</w> <w n="15.7">immense</w></l>
					<l n="16" num="4.4"><w n="16.1">S</w>’<w n="16.2">ouvrait</w> <w n="16.3">à</w> <w n="16.4">mes</w> <w n="16.5">regards</w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">L</w>’<w n="17.2">Italie</w> <w n="17.3">à</w> <w n="17.4">mes</w> <w n="17.5">pieds</w>, <w n="17.6">et</w> <w n="17.7">devant</w> <w n="17.8">moi</w> <w n="17.9">le</w> <w n="17.10">monde</w>,</l>
					<l n="18" num="5.2"><w n="18.1">Quel</w> <w n="18.2">champ</w> <w n="18.3">pour</w> <w n="18.4">mes</w> <w n="18.5">désirs</w> !</l>
					<l n="19" num="5.3"><w n="19.1">Je</w> <w n="19.2">volai</w>, <w n="19.3">j</w>’<w n="19.4">évoquai</w> <w n="19.5">cette</w> <w n="19.6">Rome</w> <w n="19.7">féconde</w></l>
					<l n="20" num="5.4"><w n="20.1">En</w> <w n="20.2">puissants</w> <w n="20.3">souvenirs</w>.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">Du</w> <w n="21.2">Tasse</w> <w n="21.3">une</w> <w n="21.4">autre</w> <w n="21.5">fois</w> <w n="21.6">je</w> <w n="21.7">revis</w> <w n="21.8">la</w> <w n="21.9">patrie</w> :</l>
					<l n="22" num="6.2"><w n="22.1">Imitant</w> <w n="22.2">Godefroi</w>,</l>
					<l n="23" num="6.3"><w n="23.1">Chrétien</w> <w n="23.2">et</w> <w n="23.3">chevalier</w>, <w n="23.4">j</w>’<w n="23.5">allais</w> <w n="23.6">vers</w> <w n="23.7">la</w> <w n="23.8">Syrie</w></l>
					<l n="24" num="6.4"><w n="24.1">Plein</w> <w n="24.2">d</w>’<w n="24.3">ardeur</w> <w n="24.4">et</w> <w n="24.5">de</w> <w n="24.6">foi</w>.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">Ils</w> <w n="25.2">ne</w> <w n="25.3">sont</w> <w n="25.4">plus</w> <w n="25.5">ces</w> <w n="25.6">jours</w> <w n="25.7">que</w> <w n="25.8">point</w> <w n="25.9">mon</w> <w n="25.10">cœur</w> <w n="25.11">n</w>’<w n="25.12">oublie</w>,</l>
					<l n="26" num="7.2"><w n="26.1">Et</w> <w n="26.2">ce</w> <w n="26.3">cœur</w> <w n="26.4">aujourd</w>’<w n="26.5">hui</w></l>
					<l n="27" num="7.3"><w n="27.1">Sous</w> <w n="27.2">le</w> <w n="27.3">brillant</w> <w n="27.4">soleil</w> <w n="27.5">de</w> <w n="27.6">la</w> <w n="27.7">belle</w> <w n="27.8">Italie</w></l>
					<l n="28" num="7.4"><w n="28.1">Ne</w> <w n="28.2">sent</w> <w n="28.3">plus</w> <w n="28.4">que</w> <w n="28.5">l</w>’<w n="28.6">ennui</w>.</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1">Pompeux</w> <w n="29.2">ambassadeurs</w> <w n="29.3">que</w> <w n="29.4">la</w> <w n="29.5">faveur</w> <w n="29.6">caresse</w>,</l>
					<l n="30" num="8.2"><w n="30.1">Ministres</w>, <w n="30.2">valez</w>-<w n="30.3">vous</w></l>
					<l n="31" num="8.3"><w n="31.1">Les</w> <w n="31.2">obscurs</w> <w n="31.3">compagnons</w> <w n="31.4">de</w> <w n="31.5">ma</w> <w n="31.6">vive</w> <w n="31.7">jeunesse</w></l>
					<l n="32" num="8.4"><w n="32.1">Et</w> <w n="32.2">mes</w> <w n="32.3">plaisirs</w> <w n="32.4">si</w> <w n="32.5">doux</w> ?</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1"><w n="33.1">Vos</w> <w n="33.2">noms</w> <w n="33.3">aux</w> <w n="33.4">bords</w> <w n="33.5">riants</w> <w n="33.6">que</w> <w n="33.7">l</w>’<w n="33.8">Adige</w> <w n="33.9">décore</w></l>
					<l n="34" num="9.2"><w n="34.1">Du</w> <w n="34.2">temps</w> <w n="34.3">seront</w> <w n="34.4">vaincus</w>,</l>
					<l n="35" num="9.3"><w n="35.1">Que</w> <w n="35.2">Catulle</w> <w n="35.3">et</w> <w n="35.4">Lesbie</w> <w n="35.5">enchanteront</w> <w n="35.6">encore</w></l>
					<l n="36" num="9.4"><w n="36.1">Les</w> <w n="36.2">flots</w> <w n="36.3">du</w> <w n="36.4">Bénacus</w>.</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1"><w n="37.1">Politiques</w>, <w n="37.2">guerriers</w>, <w n="37.3">vous</w> <w n="37.4">qui</w> <w n="37.5">prétendez</w> <w n="37.6">vivre</w></l>
					<l n="38" num="10.2"><w n="38.1">Dans</w> <w n="38.2">la</w> <w n="38.3">postérité</w>,</l>
					<l n="39" num="10.3"><w n="39.1">J</w>’<w n="39.2">y</w> <w n="39.3">consens</w> : <w n="39.4">mais</w> <w n="39.5">on</w> <w n="39.6">peut</w> <w n="39.7">arriver</w> <w n="39.8">sans</w> <w n="39.9">vous</w> <w n="39.10">suivre</w>,</l>
					<l n="40" num="10.4"><w n="40.1">A</w> <w n="40.2">l</w>’<w n="40.3">immortalité</w>.</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1"><w n="41.1">J</w>’<w n="41.2">ai</w> <w n="41.3">vu</w> <w n="41.4">ces</w> <w n="41.5">fiers</w> <w n="41.6">sentiers</w> <w n="41.7">tracés</w> <w n="41.8">par</w> <w n="41.9">la</w> <w n="41.10">Victoire</w>,</l>
					<l n="42" num="11.2"><w n="42.1">Au</w> <w n="42.2">milieu</w> <w n="42.3">des</w> <w n="42.4">frimas</w>,</l>
					<l n="43" num="11.3"><w n="43.1">Ces</w> <w n="43.2">rochers</w> <w n="43.3">du</w> <w n="43.4">Simplon</w> <w n="43.5">que</w> <w n="43.6">le</w> <w n="43.7">bras</w> <w n="43.8">de</w> <w n="43.9">la</w> <w n="43.10">Gloire</w></l>
					<l n="44" num="11.4"><w n="44.1">Pendit</w> <w n="44.2">pour</w> <w n="44.3">nos</w> <w n="44.4">soldats</w> :</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1"><w n="45.1">Ouvrage</w> <w n="45.2">d</w>’<w n="45.3">un</w> <w n="45.4">géant</w>, <w n="45.5">monument</w> <w n="45.6">du</w> <w n="45.7">génie</w>,</l>
					<l n="46" num="12.2"><w n="46.1">Serez</w>-<w n="46.2">vous</w> <w n="46.3">plus</w> <w n="46.4">connus</w></l>
					<l n="47" num="12.3"><w n="47.1">Que</w> <w n="47.2">la</w> <w n="47.3">roche</w> <w n="47.4">où</w> <w n="47.5">Saint</w>-<w n="47.6">Preux</w> <w n="47.7">contait</w> <w n="47.8">à</w> <w n="47.9">Meillerie</w></l>
					<l n="48" num="12.4"><w n="48.1">Les</w> <w n="48.2">tourments</w> <w n="48.3">de</w> <w n="48.4">Vénus</w> ?</l>
				</lg>
				<lg n="13">
					<l n="49" num="13.1"><w n="49.1">Je</w> <w n="49.2">vous</w> <w n="49.3">peignis</w> <w n="49.4">aussi</w>, <w n="49.5">chimère</w> <w n="49.6">enchanteresse</w>,</l>
					<l n="50" num="13.2"><w n="50.1">Fictions</w> <w n="50.2">des</w> <w n="50.3">amours</w> !</l>
					<l n="51" num="13.3"><w n="51.1">Aux</w> <w n="51.2">tristes</w> <w n="51.3">vérités</w> <w n="51.4">le</w> <w n="51.5">temps</w>, <w n="51.6">qui</w> <w n="51.7">fuit</w> <w n="51.8">sans</w> <w n="51.9">cesse</w>,</l>
					<l n="52" num="13.4"><w n="52.1">Livre</w> <w n="52.2">à</w> <w n="52.3">présent</w> <w n="52.4">mes</w> <w n="52.5">jours</w>.</l>
				</lg>
				<lg n="14">
					<l n="53" num="14.1"><w n="53.1">L</w>’<w n="53.2">histoire</w> <w n="53.3">et</w> <w n="53.4">le</w> <w n="53.5">roman</w> <w n="53.6">font</w> <w n="53.7">deux</w> <w n="53.8">parts</w> <w n="53.9">de</w> <w n="53.10">la</w> <w n="53.11">vie</w>,</l>
					<l n="54" num="14.2"><w n="54.1">Qui</w> <w n="54.2">si</w> <w n="54.3">tôt</w> <w n="54.4">se</w> <w n="54.5">ternit</w> :</l>
					<l n="55" num="14.3"><w n="55.1">Le</w> <w n="55.2">roman</w> <w n="55.3">la</w> <w n="55.4">commence</w>, <w n="55.5">et</w> <w n="55.6">lorsqu</w>’<w n="55.7">elle</w> <w n="55.8">est</w> <w n="55.9">flétrie</w></l>
					<l n="56" num="14.4"><w n="56.1">L</w>’<w n="56.2">histoire</w> <w n="56.3">la</w> <w n="56.4">finit</w>.</l>
				</lg>
			</div></body></text></TEI>