<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES DIVERSES</title>
				<title type="medium">Une édition électronique</title>
				<author key="CHA">
					<name>
						<forename>François-René</forename>
						<nameLink>de</nameLink>
						<surname>CHATEAUBRIAND</surname>
					</name>
					<date from="1768" to="1848">1768-1848</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>735 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">CHA_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies diverses</title>
						<author>François-René de Chateaubriand</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">https://www.poesies.net/chateaubriandpoesiesdiverses.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition papier d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1797" to="1827">1797-1827</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface n’est pas incluse.</p>
				<p>Les notes de l’auteur ont été intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>Des corrections métriques ont été apportées sans autres attestations publiées.</p>
				<p>Les vers de la strophe illisible du poème "SERMENT — LES NOIRS DEVANT LE GIBET DE JOHN BROWN" ont été marqués comme inanalysables.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
 					<p>L’orthographe a été vérifiée avec le correcteur Aspell.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="CHA23">
				<head type="number">XIII</head>
				<head type="main">Vers</head>
				<head type="sub">trouvés sur le pont du Rhône</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Il</w> <w n="1.2">est</w> <w n="1.3">minuit</w>, <w n="1.4">et</w> <w n="1.5">tu</w> <w n="1.6">sommeilles</w> ;</l>
					<l n="2" num="1.2"><w n="2.1">Tu</w> <w n="2.2">dors</w>, <w n="2.3">et</w> <w n="2.4">moi</w> <w n="2.5">je</w> <w n="2.6">vais</w> <w n="2.7">mourir</w>.</l>
					<l n="3" num="1.3"><w n="3.1">Que</w> <w n="3.2">dis</w>-<w n="3.3">je</w>, <w n="3.4">hélas</w> ! <w n="3.5">peut</w>-<w n="3.6">être</w> <w n="3.7">que</w> <w n="3.8">tu</w> <w n="3.9">veilles</w> !</l>
					<l n="4" num="1.4"><w n="4.1">Pour</w> <w n="4.2">qui</w> ?... <w n="4.3">L</w>’<w n="4.4">enfer</w> <w n="4.5">me</w> <w n="4.6">fera</w> <w n="4.7">moins</w> <w n="4.8">souffrir</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Demain</w> <w n="5.2">quand</w>, <w n="5.3">appuyée</w> <w n="5.4">au</w> <w n="5.5">bras</w> <w n="5.6">de</w> <w n="5.7">ta</w> <w n="5.8">conquête</w>,</l>
					<l n="6" num="2.2"><w n="6.1">Lasse</w> <w n="6.2">de</w> <w n="6.3">trop</w> <w n="6.4">d</w>’<w n="6.5">amour</w> <w n="6.6">et</w> <w n="6.7">cherchant</w> <w n="6.8">le</w> <w n="6.9">repos</w>,</l>
					<l n="7" num="2.3"><w n="7.1">Tu</w> <w n="7.2">passeras</w> <w n="7.3">ce</w> <w n="7.4">fleuve</w>, <w n="7.5">avance</w> <w n="7.6">un</w> <w n="7.7">peu</w> <w n="7.8">la</w> <w n="7.9">tête</w></l>
					<l n="8" num="2.4"><w n="8.1">Et</w> <w n="8.2">regarde</w> <w n="8.3">couler</w> <w n="8.4">ces</w> <w n="8.5">flots</w>.</l>
				</lg>
			</div></body></text></TEI>