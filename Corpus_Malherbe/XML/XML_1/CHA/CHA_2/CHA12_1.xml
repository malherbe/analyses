<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES DIVERSES</title>
				<title type="medium">Une édition électronique</title>
				<author key="CHA">
					<name>
						<forename>François-René</forename>
						<nameLink>de</nameLink>
						<surname>CHATEAUBRIAND</surname>
					</name>
					<date from="1768" to="1848">1768-1848</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>735 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">CHA_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies diverses</title>
						<author>François-René de Chateaubriand</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">https://www.poesies.net/chateaubriandpoesiesdiverses.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition papier d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1797" to="1827">1797-1827</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface n’est pas incluse.</p>
				<p>Les notes de l’auteur ont été intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>Des corrections métriques ont été apportées sans autres attestations publiées.</p>
				<p>Les vers de la strophe illisible du poème "SERMENT — LES NOIRS DEVANT LE GIBET DE JOHN BROWN" ont été marqués comme inanalysables.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
 					<p>L’orthographe a été vérifiée avec le correcteur Aspell.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="CHA12">
				<head type="number">II</head>
				<head type="main">A Lydie</head>
				<head type="form">Imitation d’Alcée, poète grec</head>
				<opener>
					<dateline>
						<placeName>Londres</placeName>,
						<date when="1797">1797</date>.
					</dateline>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Lydie</w>, <w n="1.2">es</w>-<w n="1.3">tu</w> <w n="1.4">sincère</w> ? <w n="1.5">Excuse</w> <w n="1.6">mes</w> <w n="1.7">alarmes</w> :</l>
					<l n="2" num="1.2"><w n="2.1">Tu</w> <w n="2.2">t</w>’<w n="2.3">embellis</w> <w n="2.4">en</w> <w n="2.5">accroissant</w> <w n="2.6">mes</w> <w n="2.7">feux</w> ;</l>
					<l n="3" num="1.3"><w n="3.1">Et</w> <w n="3.2">le</w> <w n="3.3">même</w> <w n="3.4">moment</w> <w n="3.5">qui</w> <w n="3.6">t</w>’<w n="3.7">apporte</w> <w n="3.8">des</w> <w n="3.9">charmes</w></l>
					<l n="4" num="1.4"><w n="4.1">Ride</w> <w n="4.2">mon</w> <w n="4.3">front</w> <w n="4.4">et</w> <w n="4.5">blanchit</w> <w n="4.6">mes</w> <w n="4.7">cheveux</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Au</w> <w n="5.2">matin</w> <w n="5.3">de</w> <w n="5.4">tes</w> <w n="5.5">ans</w>, <w n="5.6">de</w> <w n="5.7">la</w> <w n="5.8">foule</w> <w n="5.9">chérie</w>,</l>
					<l n="6" num="2.2"><w n="6.1">Tout</w> <w n="6.2">est</w> <w n="6.3">pour</w> <w n="6.4">toi</w> <w n="6.5">joie</w>, <w n="6.6">espérance</w>, <w n="6.7">amour</w> ;</l>
					<l n="7" num="2.3"><w n="7.1">Et</w> <w n="7.2">moi</w>, <w n="7.3">vieux</w> <w n="7.4">voyageur</w>, <w n="7.5">sur</w> <w n="7.6">ta</w> <w n="7.7">route</w> <w n="7.8">fleurie</w></l>
					<l n="8" num="2.4"><w n="8.1">Je</w> <w n="8.2">marche</w> <w n="8.3">seul</w> <w n="8.4">et</w> <w n="8.5">vois</w> <w n="8.6">finir</w> <w n="8.7">le</w> <w n="8.8">jour</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Ainsi</w> <w n="9.2">qu</w>’<w n="9.3">un</w> <w n="9.4">doux</w> <w n="9.5">rayon</w> <w n="9.6">quand</w> <w n="9.7">ton</w> <w n="9.8">regard</w> <w n="9.9">humide</w></l>
					<l n="10" num="3.2"><w n="10.1">Pénètre</w> <w n="10.2">au</w> <w n="10.3">fond</w> <w n="10.4">de</w> <w n="10.5">mon</w> <w n="10.6">cœur</w> <w n="10.7">ranimé</w>,</l>
					<l n="11" num="3.3"><w n="11.1">J</w>’<w n="11.2">ose</w> <w n="11.3">à</w> <w n="11.4">peine</w> <w n="11.5">effleurer</w> <w n="11.6">d</w>’<w n="11.7">une</w> <w n="11.8">lèvre</w> <w n="11.9">timide</w></l>
					<l n="12" num="3.4"><w n="12.1">De</w> <w n="12.2">ton</w> <w n="12.3">beau</w> <w n="12.4">front</w> <w n="12.5">le</w> <w n="12.6">voile</w> <w n="12.7">parfumé</w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Tout</w> <w n="13.2">à</w> <w n="13.3">la</w> <w n="13.4">fois</w> <w n="13.5">honteux</w> <w n="13.6">et</w> <w n="13.7">fier</w> <w n="13.8">de</w> <w n="13.9">ton</w> <w n="13.10">caprice</w>,</l>
					<l n="14" num="4.2"><w n="14.1">Sans</w> <w n="14.2">croire</w> <w n="14.3">en</w> <w n="14.4">toi</w>, <w n="14.5">je</w> <w n="14.6">m</w>’<w n="14.7">en</w> <w n="14.8">laisse</w> <w n="14.9">enivrer</w>.</l>
					<l n="15" num="4.3"><w n="15.1">J</w>’<w n="15.2">adore</w> <w n="15.3">tes</w> <w n="15.4">attraits</w>, <w n="15.5">mais</w> <w n="15.6">je</w> <w n="15.7">me</w> <w n="15.8">rends</w> <w n="15.9">justice</w> :</l>
					<l n="16" num="4.4"><w n="16.1">Je</w> <w n="16.2">sens</w> <w n="16.3">l</w>’<w n="16.4">amour</w> <w n="16.5">et</w> <w n="16.6">ne</w> <w n="16.7">puis</w> <w n="16.8">l</w>’<w n="16.9">inspirer</w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">Par</w> <w n="17.2">quel</w> <w n="17.3">enchantement</w> <w n="17.4">ai</w>-<w n="17.5">je</w> <w n="17.6">pu</w> <w n="17.7">te</w> <w n="17.8">séduire</w> ?</l>
					<l n="18" num="5.2"><w n="18.1">N</w>’<w n="18.2">aurais</w>-<w n="18.3">tu</w> <w n="18.4">point</w> <w n="18.5">dans</w> <w n="18.6">mon</w> <w n="18.7">dernier</w> <w n="18.8">soleil</w></l>
					<l n="19" num="5.3"><w n="19.1">Cherché</w> <w n="19.2">l</w>’<w n="19.3">astre</w> <w n="19.4">de</w> <w n="19.5">feu</w> <w n="19.6">qui</w> <w n="19.7">sur</w> <w n="19.8">moi</w> <w n="19.9">semblait</w> <w n="19.10">luire</w></l>
					<l n="20" num="5.4"><w n="20.1">Quand</w> <w n="20.2">de</w> <w n="20.3">Sapho</w> <w n="20.4">je</w> <w n="20.5">chantais</w> <w n="20.6">le</w> <w n="20.7">réveil</w> ?</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">Je</w> <w n="21.2">n</w>’<w n="21.3">ai</w> <w n="21.4">point</w> <w n="21.5">le</w> <w n="21.6">talent</w> <w n="21.7">qu</w>’<w n="21.8">on</w> <w n="21.9">encense</w> <w n="21.10">au</w> <w n="21.11">Parnasse</w>.</l>
					<l n="22" num="6.2"><w n="22.1">Eussé</w>-<w n="22.2">je</w> <w n="22.3">un</w> <w n="22.4">temple</w> <w n="22.5">au</w> <w n="22.6">sommet</w> <w n="22.7">d</w>’<w n="22.8">Hélicon</w>,</l>
					<l n="23" num="6.3"><w n="23.1">Le</w> <w n="23.2">talent</w> <w n="23.3">ne</w> <w n="23.4">rend</w> <w n="23.5">point</w> <w n="23.6">ce</w> <w n="23.7">que</w> <w n="23.8">le</w> <w n="23.9">temps</w> <w n="23.10">efface</w> ;</l>
					<l n="24" num="6.4"><w n="24.1">La</w> <w n="24.2">gloire</w>, <w n="24.3">hélas</w> ! <w n="24.4">ne</w> <w n="24.5">rajeunit</w> <w n="24.6">qu</w>’<w n="24.7">un</w> <w n="24.8">nom</w>.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">Le</w> <hi rend="ital"><w n="25.2">Guerrier</w> <w n="25.3">de</w> <w n="25.4">Samos</w></hi>, <w n="25.5">le</w> <hi rend="ital"><w n="25.6">Berger</w> <w n="25.7">d</w>’<w n="25.8">Aphélie</w></hi><ref target="1" type="noteAnchor">1</ref></l>
					<l n="26" num="7.2"><w n="26.1">Mes</w> <w n="26.2">fils</w> <w n="26.3">ingrats</w>, <w n="26.4">m</w>’<w n="26.5">ont</w>-<w n="26.6">ils</w> <w n="26.7">ravi</w> <w n="26.8">ta</w> <w n="26.9">foi</w> ?</l>
					<l n="27" num="7.3"><w n="27.1">Ton</w> <w n="27.2">admiration</w> <w n="27.3">me</w> <w n="27.4">blesse</w> <w n="27.5">et</w> <w n="27.6">m</w>’<w n="27.7">humilie</w> :</l>
					<l n="28" num="7.4"><w n="28.1">Le</w> <w n="28.2">croirais</w>-<w n="28.3">tu</w> ? <w n="28.4">je</w> <w n="28.5">suis</w> <w n="28.6">jaloux</w> <w n="28.7">de</w> <w n="28.8">moi</w>.</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1">Que</w> <w n="29.2">m</w>’<w n="29.3">importe</w> <w n="29.4">de</w> <w n="29.5">vivre</w> <w n="29.6">au</w> <w n="29.7">delà</w> <w n="29.8">de</w> <w n="29.9">ma</w> <w n="29.10">vie</w> ?</l>
					<l n="30" num="8.2"><w n="30.1">Qu</w>’<w n="30.2">importe</w> <w n="30.3">un</w> <w n="30.4">nom</w> <w n="30.5">par</w> <w n="30.6">la</w> <w n="30.7">mort</w> <w n="30.8">publié</w> ?</l>
					<l n="31" num="8.3"><w n="31.1">Pour</w> <w n="31.2">moi</w>-<w n="31.3">même</w> <w n="31.4">un</w> <w n="31.5">moment</w> <w n="31.6">aime</w>-<w n="31.7">moi</w>, <w n="31.8">ma</w> <w n="31.9">Lydie</w>,</l>
					<l n="32" num="8.4"><w n="32.1">Et</w> <w n="32.2">que</w> <w n="32.3">je</w> <w n="32.4">sois</w> <w n="32.5">à</w> <w n="32.6">jamais</w> <w n="32.7">oublié</w> !</l>
				</lg>
				<closer>
					<note type="footnote" id="1">Deux ouvrages d’Alcée.</note>
				</closer>
			</div></body></text></TEI>