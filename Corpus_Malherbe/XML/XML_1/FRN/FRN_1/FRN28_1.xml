<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Inattentions et Sollicitudes</title>
				<title type="medium">Édition électronique</title>
				<author key="FRN">
					<name>
						<forename>Maurice Étienne</forename>
						<surname>LEGRAND</surname>
						<addName type="pen_name">FRANC-NOHAIN</addName>
					</name>
					<date from="1872" to="1934">1872-1934</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>649 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">FRN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Les Inattentions et Sollicitudes</title>
						<author>Franc-Nohain</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k5438658j.texteImage</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Inattentions et Sollicitudes</title>
								<author>Franc-Nohain</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>L. VANIER</publisher>
									<date when="1894">1894</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1894">1894</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ) ont été restituées.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-06-04" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="FRN28">
		<head type="main">LA RENOVATION SPIRITUELLE <lb></lb>DU POÈTE FRANC-NOHAIN</head>
			<div type="section" n="1">
			<head type="sub">I. — EXODE</head>
			<lg n="1">
				<l n="1" num="1.1"><w n="1.1">Triste</w> <w n="1.2">pécheur</w>, <w n="1.3">je</w> <w n="1.4">suis</w> <w n="1.5">parti</w>,</l>
				<l n="2" num="1.2"><w n="2.1">Je</w> <w n="2.2">suis</w> <w n="2.3">parti</w> <w n="2.4">vers</w> <w n="2.5">la</w> <w n="2.6">sainte</w> <w n="2.7">ville</w>,</l>
				<l n="3" num="1.3"><w n="3.1">Pour</w> <w n="3.2">mes</w> <w n="3.3">péchés</w>, <w n="3.4">les</w> <w n="3.5">gros</w> <w n="3.6">péchés</w> <w n="3.7">que</w> <w n="3.8">j</w>’<w n="3.9">ai</w> <w n="3.10">commis</w></l>
				<l n="4" num="1.4"><w n="4.1">Avec</w> <w n="4.2">des</w> <w n="4.3">filles</w>,</l>
				<l n="5" num="1.5"><w n="5.1">Vers</w> <w n="5.2">la</w> <w n="5.3">sainte</w> <w n="5.4">ville</w>, <w n="5.5">vers</w> <w n="5.6">Rome</w>, <w n="5.7">je</w> <w n="5.8">suis</w> <w n="5.9">parti</w>.</l>
			</lg>
			<lg n="2">
				<l n="6" num="2.1"><w n="6.1">J</w>’<w n="6.2">ai</w> <w n="6.3">pris</w> <w n="6.4">le</w> <w n="6.5">train</w></l>
				<l n="7" num="2.2"><w n="7.1">Des</w> <w n="7.2">pèlerins</w>,</l>
				<l n="8" num="2.3"><w n="8.1">Les</w> <w n="8.2">pèlerins</w> <w n="8.3">passionnés</w> <w n="8.4">des</w> <w n="8.5">Pâques</w> <w n="8.6">fleuries</w> :</l>
				<l n="9" num="2.4"><w n="9.1">Mes</w> <w n="9.2">Pâques</w> <w n="9.3">fleuries</w>,</l>
				<l n="10" num="2.5"><w n="10.1">Je</w> <w n="10.2">viens</w> <w n="10.3">vous</w> <w n="10.4">faire</w> <w n="10.5">en</w> <w n="10.6">Italie</w>.</l>
			</lg>
			</div>
			<div type="section" n="2">
			<head type="sub">II. — RÉCIT</head>
			<lg n="1">
				<l n="11" num="1.1"><w n="11.1">Dans</w> <w n="11.2">les</w> <w n="11.3">basiliques</w> <w n="11.4">de</w> <w n="11.5">marbre</w> <w n="11.6">et</w> <w n="11.7">d</w>’<w n="11.8">or</w>,</l>
				<l n="12" num="1.2"><w n="12.1">Les</w> <w n="12.2">mains</w> <w n="12.3">jointes</w> <w n="12.4">et</w> <w n="12.5">avec</w> <w n="12.6">une</w> <w n="12.7">grande</w> <w n="12.8">humilité</w>,</l>
				<l n="13" num="1.3"><w n="13.1">Les</w> <w n="13.2">mains</w> <w n="13.3">jointes</w>, <w n="13.4">et</w> <w n="13.5">chantant</w> <w n="13.6">les</w> <w n="13.7">psaumes</w>, <w n="13.8">j</w>’<w n="13.9">ai</w> <w n="13.10">marché</w></l>
				<l n="14" num="1.4"><w n="14.1">Dans</w> <w n="14.2">les</w> <w n="14.3">basiliques</w> <w n="14.4">de</w> <w n="14.5">marbre</w> <w n="14.6">et</w> <w n="14.7">d</w>’<w n="14.8">or</w>.</l>
			</lg>
			<lg n="2">
				<l n="15" num="2.1"><w n="15.1">Je</w> <w n="15.2">me</w> <w n="15.3">suis</w> <w n="15.4">agenouillé</w> <w n="15.5">devant</w> <w n="15.6">les</w> <w n="15.7">images</w>,</l>
				<l n="16" num="2.2"><w n="16.1">Mes</w> <w n="16.2">membres</w> <w n="16.3">impurs</w> <w n="16.4">ont</w> <w n="16.5">touché</w> <w n="16.6">les</w> <w n="16.7">saintes</w> <w n="16.8">reliques</w>,</l>
				<l n="17" num="2.3"><w n="17.1">Et</w> <w n="17.2">je</w> <w n="17.3">me</w> <w n="17.4">suis</w> <w n="17.5">livré</w> <w n="17.6">à</w> <w n="17.7">toutes</w> <w n="17.8">les</w> <w n="17.9">pratiques</w></l>
				<l n="18" num="2.4"><w n="18.1">Les</w> <w n="18.2">plus</w> <w n="18.3">généralement</w> <w n="18.4">en</w> <w n="18.5">usage</w>.</l>
			</lg>
			<lg n="3">
				<l n="19" num="3.1"><w n="19.1">Et</w> <w n="19.2">je</w> <w n="19.3">n</w>’<w n="19.4">ai</w> <w n="19.5">point</w> <w n="19.6">fait</w> <w n="19.7">attention</w> <w n="19.8">aux</w> <w n="19.9">petites</w> <w n="19.10">Anglaises</w></l>
				<l n="20" num="3.2"><w n="20.1">Très</w> <w n="20.2">jolies</w>, <w n="20.3">leurs</w> <w n="20.4">cheveux</w> <w n="20.5">rouges</w> <w n="20.6">ébouriffés</w>,</l>
				<l n="21" num="3.3"><w n="21.1">Qui</w> <w n="21.2">me</w> <w n="21.3">regardaient</w> <w n="21.4">avec</w> <w n="21.5">de</w> <w n="21.6">petits</w> <w n="21.7">rires</w> <w n="21.8">étouffés</w> :</l>
				<l n="22" num="3.4"><w n="22.1">J</w>’<w n="22.2">ai</w> <w n="22.3">banni</w> <w n="22.4">loin</w> <w n="22.5">de</w> <w n="22.6">moi</w> <w n="22.7">les</w> <w n="22.8">pensées</w> <w n="22.9">mauvaises</w>.</l>
			</lg>
			</div>
			<div type="section" n="3">
			<head type="sub">III. — PRIÈRE</head>
			<lg n="1">
				<l n="23" num="1.1"><w n="23.1">Pied</w> <w n="23.2">de</w> <w n="23.3">saint</w> <w n="23.4">Pierre</w>, <w n="23.5">pied</w> <w n="23.6">de</w> <w n="23.7">saint</w> <w n="23.8">Pierre</w>,</l>
				<l n="24" num="1.2"><w n="24.1">Toi</w> <w n="24.2">dont</w> <w n="24.3">le</w> <w n="24.4">bronze</w> <w n="24.5">fond</w> <w n="24.6">lentement</w> <w n="24.7">sous</w> <w n="24.8">les</w> <w n="24.9">pleur</w> :</l>
				<l n="25" num="1.3"><w n="25.1">Et</w> <w n="25.2">sous</w> <w n="25.3">les</w> <w n="25.4">baisers</w> <w n="25.5">des</w> <w n="25.6">pécheurs</w>,</l>
				<l n="26" num="1.4"><w n="26.1">A</w> <w n="26.2">toi</w> <w n="26.3">j</w>’<w n="26.4">adresse</w> <w n="26.5">ma</w> <w n="26.6">prière</w>,</l>
				<l n="27" num="1.5"><w n="27.1">Pied</w> <w n="27.2">de</w> <w n="27.3">saint</w> <w n="27.4">Pierre</w> !</l>
			</lg>
			<lg n="2">
				<l n="28" num="2.1"><w n="28.1">Ô</w> <w n="28.2">toi</w> <w n="28.3">qui</w> <w n="28.4">lus</w> <w n="28.5">plus</w> <w n="28.6">près</w> <w n="28.7">de</w> <w n="28.8">nous</w>,</l>
				<l n="29" num="2.2"><w n="29.1">Car</w> <w n="29.2">nous</w> <w n="29.3">ne</w> <w n="29.4">sommes</w> <w n="29.5">que</w> <w n="29.6">poussière</w>,</l>
				<l n="30" num="2.3"><w n="30.1">À</w> <w n="30.2">toi</w> <w n="30.3">j</w>’<w n="30.4">adresse</w> <w n="30.5">ma</w> <w n="30.6">prière</w>,</l>
				<l n="31" num="2.4"><w n="31.1">Pied</w> <w n="31.2">de</w> <w n="31.3">saint</w> <w n="31.4">Pierre</w>,</l>
				<l n="32" num="2.5"><w n="32.1">Je</w> <w n="32.2">te</w> <w n="32.3">supplie</w> <w n="32.4">à</w> <w n="32.5">deux</w> <w n="32.6">genoux</w> :</l>
				<l n="33" num="2.6"><w n="33.1">Rachète</w>-<w n="33.2">nous</w>.</l>
			</lg>
			</div>
			<div type="section" n="4">
			<head type="sub">IV. — RÉDEMPTION</head>
			<lg n="1">
				<l n="34" num="1.1"><w n="34.1">Et</w> <w n="34.2">maintenant</w></l>
				<l n="35" num="1.2"><w n="35.1">Il</w> <w n="35.2">en</w> <w n="35.3">est</w> <w n="35.4">temps</w>,</l>
				<l n="36" num="1.3"><w n="36.1">Grands</w> <w n="36.2">de</w> <w n="36.3">la</w> <w n="36.4">terre</w> <w n="36.5">inclinez</w> <w n="36.6">la</w> <w n="36.7">tête</w> :</l>
				<l n="37" num="1.4"><w n="37.1">Voici</w> <w n="37.2">le</w> <w n="37.3">grand</w> <w n="37.4">pénitencier</w> <w n="37.5">et</w> <w n="37.6">sa</w> <w n="37.7">baguette</w> ;</l>
				<l n="38" num="1.5"><w n="38.1">Allons</w> <w n="38.2">vite</w> <w n="38.3">que</w> <w n="38.4">l</w>’<w n="38.5">on</w> <w n="38.6">s</w>’<w n="38.7">avance</w></l>
				<l n="39" num="1.6"><w n="39.1">Au</w> <w n="39.2">tribunal</w> <w n="39.3">de</w> <w n="39.4">pénitence</w>.</l>
				<l n="40" num="1.7">(<w n="40.1">C</w>’<w n="40.2">est</w> <w n="40.3">la</w> <w n="40.4">corde</w> <w n="40.5">au</w> <w n="40.6">cou</w> <w n="40.7">que</w> <w n="40.8">j</w>’<w n="40.9">eusse</w> <w n="40.10">voulu</w></l>
				<l n="41" num="1.8"><w n="41.1">M</w>’<w n="41.2">y</w> <w n="41.3">rendre</w>, <w n="41.4">en</w> <w n="41.5">chemise</w> <w n="41.6">et</w> <w n="41.7">pieds</w> <w n="41.8">nus</w>,</l>
				<l n="42" num="1.9"><w n="42.1">Mais</w> <w n="42.2">il</w> <w n="42.3">paraît</w> <w n="42.4">que</w> <w n="42.5">ça</w> <w n="42.6">ne</w> <w n="42.7">se</w> <w n="42.8">fait</w> <w n="42.9">plus</w>.)</l>
			</lg>
			<lg n="2">
				<l n="43" num="2.1"><w n="43.1">Après</w> <w n="43.2">tout</w>, <w n="43.3">qu</w>’<w n="43.4">importe</w></l>
				<l n="44" num="2.2"><w n="44.1">L</w>’<w n="44.2">habit</w> <w n="44.3">que</w> <w n="44.4">l</w>’<w n="44.5">on</w> <w n="44.6">porte</w>,</l>
				<l n="45" num="2.3"><w n="45.1">Pourvu</w> <w n="45.2">que</w> <w n="45.3">notre</w> <w n="45.4">âme</w></l>
				<l n="46" num="2.4"><w n="46.1">Soit</w> <w n="46.2">sauvée</w> <w n="46.3">des</w> <w n="46.4">flammes</w> :</l>
				<l n="47" num="2.5"><w n="47.1">Marchons</w>, <w n="47.2">marchons</w></l>
				<l n="48" num="2.6"><w n="48.1">À</w> <w n="48.2">la</w> <w n="48.3">rédemption</w> !</l>
			</lg>
			</div>
			<div type="section" n="5">
			<head type="sub">V. — EXTASE</head>
			<lg n="1">
				<l n="49" num="1.1"><w n="49.1">Et</w> <w n="49.2">alors</w> <w n="49.3">est</w> <w n="49.4">venue</w> <w n="49.5">l</w>’<w n="49.6">heure</w> <w n="49.7">des</w> <w n="49.8">béatitudes</w> ;</l>
				<l n="50" num="1.2"><w n="50.1">Voix</w> <w n="50.2">célestes</w>, <w n="50.3">chants</w> <w n="50.4">des</w> <w n="50.5">séraphins</w>,</l>
				<l n="51" num="1.3"><w n="51.1">Aimables</w> <w n="51.2">préludes</w></l>
				<l n="52" num="1.4"><w n="52.1">Des</w> <w n="52.2">plaisirs</w> <w n="52.3">divins</w> :</l>
				<l n="53" num="1.5"><w n="53.1">Ce</w> <w n="53.2">fut</w> <w n="53.3">l</w>’<w n="53.4">heure</w> <w n="53.5">des</w> <w n="53.6">béatitudes</w>.</l>
			</lg>
			<lg n="2">
				<l n="54" num="2.1"><w n="54.1">Cardinaux</w> <w n="54.2">rouges</w>,</l>
				<l n="55" num="2.2"><w n="55.1">Évêques</w> <w n="55.2">violets</w>,</l>
				<l n="56" num="2.3"><w n="56.1">Moines</w> <w n="56.2">en</w> <w n="56.3">cagoule</w></l>
				<l n="57" num="2.4"><w n="57.1">Nu</w>-<w n="57.2">pieds</w> ;</l>
				<l n="58" num="2.5"><w n="58.1">Vatican</w>,</l>
				<l n="59" num="2.6"><w n="59.1">Saint</w>-<w n="59.2">Jean</w></l>
				<l n="60" num="2.7"><w n="60.1">De</w> <w n="60.2">Latran</w> ;</l>
				<l n="61" num="2.8"><w n="61.1">Bonheur</w>, <w n="61.2">ivresse</w>,</l>
				<l n="62" num="2.9"><w n="62.1">J</w>’<w n="62.2">ai</w> <w n="62.3">vu</w> <w n="62.4">Léon</w> <w n="62.5">Treize</w> :</l>
				<l n="63" num="2.10"><hi rend="smallcap"><w n="63.1">J</w>’<w n="63.2">AI</w> <w n="63.3">VU</w> <w n="63.4">LE</w> <w n="63.5">PAPE</w></hi>.</l>
			</lg>
			</div>
			<div type="section" n="6">
			<head type="sub">VI. — PARTIE ANECDOTIQUE</head>
			<lg n="1">
				<l n="64" num="1.1"><w n="64.1">On</w> <w n="64.2">m</w>’<w n="64.3">a</w> <w n="64.4">donne</w> <w n="64.5">aussi</w> <w n="64.6">sur</w> <w n="64.7">les</w> <w n="64.8">petits</w> <w n="64.9">jeunes</w> <w n="64.10">gens</w></l>
				<l n="65" num="1.2"><w n="65.1">Qui</w> <w n="65.2">fréquentent</w> <w n="65.3">assidûment</w> <w n="65.4">la</w> <w n="65.5">chapelle</w> <w n="65.6">Sixtine</w>,</l>
				<l n="66" num="1.3"><w n="66.1">Des</w> <w n="66.2">détails</w> <w n="66.3">d</w>’<w n="66.4">un</w> <w n="66.5">ordre</w> <w n="66.6">plutôt</w> <w n="66.7">intime</w></l>
				<l n="67" num="1.4"><w n="67.1">Et</w> <w n="67.2">qui</w> <w n="67.3">ne</w> <w n="67.4">manquent</w> <w n="67.5">point</w> <w n="67.6">d</w>’<w n="67.7">un</w> <w n="67.8">certain</w> <w n="67.9">piquant</w>.</l>
				<l n="68" num="1.5"><w n="68.1">Mais</w> <w n="68.2">je</w> <w n="68.3">n</w>’<w n="68.4">ai</w> <w n="68.5">pas</w> <w n="68.6">pu</w> <w n="68.7">les</w> <w n="68.8">vérifier</w>, <w n="68.9">faute</w> <w n="68.10">de</w> <w n="68.11">temps</w>.</l>
			</lg>
			</div>
		</div></body></text></TEI>