<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Inattentions et Sollicitudes</title>
				<title type="medium">Édition électronique</title>
				<author key="FRN">
					<name>
						<forename>Maurice Étienne</forename>
						<surname>LEGRAND</surname>
						<addName type="pen_name">FRANC-NOHAIN</addName>
					</name>
					<date from="1872" to="1934">1872-1934</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>649 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">FRN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Les Inattentions et Sollicitudes</title>
						<author>Franc-Nohain</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k5438658j.texteImage</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Inattentions et Sollicitudes</title>
								<author>Franc-Nohain</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>L. VANIER</publisher>
									<date when="1894">1894</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1894">1894</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ) ont été restituées.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-06-04" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2023-06-25" who="RR" type="analyse">Étape 7 de l'analyse automatique du corpus : Traitement des rimes, des strophes et de la forme globale.</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="FRN6">
		<head type="main">SENSATIONS<lb></lb>DES ENDROITS DE LUXURE</head>
			<lg n="1">
				<l n="1" num="1.1"><w n="1.1">Très</w> <w n="1.2">douce</w> <w n="1.3">et</w> <w n="1.4">très</w> <w n="1.5">mélancolique</w>, <w n="1.6">la</w> <w n="1.7">Négresse</w>,</l>
				<l n="2" num="1.2"><w n="2.1">Fredonnant</w> <w n="2.2">quelque</w> <w n="2.3">lascive</w> <w n="2.4">bamboula</w>, <w n="2.5">déroule</w> <w n="2.6">ses</w> <w n="2.7">tresses</w>.</l>
			</lg>
			<lg n="2">
				<l n="3" num="2.1"><space unit="char" quantity="12"></space><w n="3.1">Do</w>, <w n="3.2">ré</w>, <w n="3.3">mi</w>, <w n="3.4">do</w>,</l>
				<l n="4" num="2.2"><space unit="char" quantity="12"></space><w n="4.1">Pianos</w>, <w n="4.2">pianos</w>.</l>
				<l n="5" num="2.3"><w n="5.1">Mes</w> <w n="5.2">sœurs</w>, <w n="5.3">mes</w> <w n="5.4">sœurs</w>, <w n="5.5">allumons</w> <w n="5.6">les</w> <w n="5.7">bougies</w>,</l>
				<l n="6" num="2.4"><w n="6.1">Nous</w> <w n="6.2">allons</w> <w n="6.3">commencer</w> <w n="6.4">l</w>’<w n="6.5">orgie</w>.</l>
				<l n="7" num="2.5">— <w n="7.1">Holà</w>, <w n="7.2">ho</w> ! <w n="7.3">piquante</w> <w n="7.4">soubrette</w>,</l>
				<l n="8" num="2.6"><w n="8.1">Ne</w> <w n="8.2">nous</w> <w n="8.3">apporteras</w>-<w n="8.4">tu</w> <w n="8.5">pas</w> <w n="8.6">quelque</w> <w n="8.7">menthe</w> <w n="8.8">verte</w> ?</l>
			</lg>
			<lg n="3">
				<l n="9" num="3.1"><w n="9.1">C</w>’<w n="9.2">est</w> <w n="9.3">vraiment</w> <w n="9.4">un</w> <w n="9.5">très</w> <w n="9.6">beau</w> <w n="9.7">coup</w> <w n="9.8">d</w>’<w n="9.9">œil</w>,</l>
				<l n="10" num="3.2"><w n="10.1">Tous</w> <w n="10.2">ces</w> <w n="10.3">divans</w>, <w n="10.4">tous</w> <w n="10.5">ces</w> <w n="10.6">fauteuils</w>,</l>
				<l n="11" num="3.3"><w n="11.1">Et</w> <w n="11.2">de</w> <w n="11.3">tous</w> <w n="11.4">les</w> <w n="11.5">côtés</w> <w n="11.6">des</w> <w n="11.7">glaces</w>,</l>
				<l n="12" num="3.4"><w n="12.1">De</w> <w n="12.2">dos</w>, <w n="12.3">de</w> <w n="12.4">trois</w> <w n="12.5">quarts</w> <w n="12.6">et</w> <w n="12.7">de</w> <w n="12.8">face</w> ;</l>
				<l n="13" num="3.5"><w n="13.1">Et</w> <w n="13.2">ces</w> <w n="13.3">peintures</w>, <w n="13.4">badines</w> <w n="13.5">plutôt</w>,</l>
				<l n="14" num="3.6"><w n="14.1">Où</w> <w n="14.2">se</w> <w n="14.3">complut</w> <w n="14.4">le</w> <w n="14.5">pinceau</w> <w n="14.6">de</w> <w n="14.7">quelque</w> <w n="14.8">Bouguereau</w>.</l>
			</lg>
			<lg n="4">
				<l n="15" num="4.1"><w n="15.1">Très</w> <w n="15.2">douce</w> <w n="15.3">et</w> <w n="15.4">très</w> <w n="15.5">mélancolique</w>, <w n="15.6">la</w> <w n="15.7">Négresse</w>,</l>
				<l n="16" num="4.2"><w n="16.1">Fredonnant</w> <w n="16.2">quelque</w> <w n="16.3">lascive</w> <w n="16.4">bamboula</w>, <w n="16.5">déroule</w> <w n="16.6">ses</w> <w n="16.7">tresses</w>.</l>
			</lg>
			<lg n="5">
				<l n="17" num="5.1"><w n="17.1">Certes</w>, <w n="17.2">ce</w> <w n="17.3">sont</w> <w n="17.4">des</w> <w n="17.5">raffinements</w>, <w n="17.6">des</w> <w n="17.7">luxes</w>,</l>
				<l n="18" num="5.2"><w n="18.1">Que</w> <w n="18.2">ne</w> <w n="18.3">soupçonnèrent</w> <w n="18.4">jamais</w> <w n="18.5">les</w> <w n="18.6">peuples</w> <w n="18.7">étrusques</w> ;</l>
				<l n="19" num="5.3"><w n="19.1">Et</w> <w n="19.2">pourtant</w>, <w n="19.3">quand</w> <w n="19.4">on</w> <w n="19.5">réfléchit</w> <w n="19.6">bien</w>,</l>
				<l n="20" num="5.4"><w n="20.1">On</w> <w n="20.2">trouve</w> <w n="20.3">que</w> <w n="20.4">les</w> <w n="20.5">peuples</w> <w n="20.6">étrusques</w> <w n="20.7">n</w>’<w n="20.8">y</w> <w n="20.9">perdaient</w> <w n="20.10">rien</w>.</l>
				<l n="21" num="5.5"><w n="21.1">Ces</w> <w n="21.2">populations</w> <w n="21.3">primitives</w></l>
				<l n="22" num="5.6"><w n="22.1">Avaient</w> <w n="22.2">le</w> <w n="22.3">cœur</w> <w n="22.4">naïf</w>, <w n="22.5">avaient</w> <w n="22.6">l</w>’<w n="22.7">âme</w> <w n="22.8">naïve</w> ;</l>
				<l n="23" num="5.7"><w n="23.1">Dans</w> <w n="23.2">les</w> <w n="23.3">maisons</w> <w n="23.4">de</w> <w n="23.5">prostitution</w>,</l>
				<l n="24" num="5.8"><w n="24.1">C</w>’<w n="24.2">est</w> <w n="24.3">l</w>’<w n="24.4">AMOUR</w> <w n="24.5">que</w> <w n="24.6">nous</w> <w n="24.7">prostituons</w> —</l>
				<l n="25" num="5.9">(<w n="25.1">Probablement</w>.) —</l>
			</lg>
			<lg n="6">
				<l n="26" num="6.1"><w n="26.1">Très</w> <w n="26.2">douce</w> <w n="26.3">et</w> <w n="26.4">très</w> <w n="26.5">mélancolique</w>, <w n="26.6">la</w> <w n="26.7">Négresse</w></l>
				<l n="27" num="6.2"><w n="27.1">Fredonnant</w> <w n="27.2">quelque</w> <w n="27.3">lascive</w> <w n="27.4">bamboula</w>, <w n="27.5">déroule</w> <w n="27.6">ses</w> <w n="27.7">tresses</w>.</l>
			</lg>
	</div></body></text></TEI>