<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Inattentions et Sollicitudes</title>
				<title type="medium">Édition électronique</title>
				<author key="FRN">
					<name>
						<forename>Maurice Étienne</forename>
						<surname>LEGRAND</surname>
						<addName type="pen_name">FRANC-NOHAIN</addName>
					</name>
					<date from="1872" to="1934">1872-1934</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>649 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">FRN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Les Inattentions et Sollicitudes</title>
						<author>Franc-Nohain</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k5438658j.texteImage</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Inattentions et Sollicitudes</title>
								<author>Franc-Nohain</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>L. VANIER</publisher>
									<date when="1894">1894</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1894">1894</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ) ont été restituées.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-06-04" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="FRN19">
		<head type="main">BALLADE <lb></lb>DES CANNES ET DES PARAPLUIES</head>
			<div type="section" n="1">
			<head type="number">I</head>
			<lg n="1">
				<l n="1" num="1.1"><w n="1.1">La</w> <w n="1.2">Banlieue</w> <w n="1.3">a</w> <w n="1.4">pris</w></l>
				<l n="2" num="1.2"><w n="2.1">Son</w> <w n="2.2">beau</w> <w n="2.3">parapluie</w>,</l>
				<l n="3" num="1.3"><w n="3.1">Et</w> <w n="3.2">vers</w> <w n="3.3">Paris</w></l>
				<l n="4" num="1.4"><w n="4.1">S</w>’<w n="4.2">en</w> <w n="4.3">est</w> <w n="4.4">allée</w> : —</l>
			</lg>
			<lg n="2">
				<l n="5" num="2.1"><w n="5.1">Toute</w> <w n="5.2">la</w> <w n="5.3">journée</w>, <w n="5.4">toute</w> <w n="5.5">la</w> <w n="5.6">journée</w>,</l>
				<l n="6" num="2.2"><w n="6.1">Au</w> <w n="6.2">ciel</w> <w n="6.3">radieux</w> <w n="6.4">le</w> <w n="6.5">soleil</w> <w n="6.6">a</w> <w n="6.7">lui</w>.</l>
			</lg>
			</div>
			<div type="section" n="2">
			<head type="number">II</head>
			<lg n="1">
				<l n="7" num="1.1"><w n="7.1">Naturellement</w>, <w n="7.2">le</w> <w n="7.3">dimanche</w> <w n="7.4">suivant</w>,</l>
				<l n="8" num="1.2"><w n="8.1">La</w> <w n="8.2">Banlieue</w> <w n="8.3">a</w> <w n="8.4">dit</w> : <w n="8.5">Puisqu</w>’<w n="8.6">il</w> <w n="8.7">fait</w> <w n="8.8">beau</w> <w n="8.9">temps</w>,</l>
				<l n="9" num="1.3"><w n="9.1">Prenons</w> <w n="9.2">notre</w> <w n="9.3">canne</w> <w n="9.4">a</w> <w n="9.5">pomme</w> <w n="9.6">d</w>’<w n="9.7">argent</w> ; —</l>
			</lg>
			<lg n="2">
				<l n="10" num="2.1"><w n="10.1">Tout</w> <w n="10.2">le</w> <w n="10.3">long</w> <w n="10.4">du</w> <w n="10.5">jour</w>, <w n="10.6">il</w> <w n="10.7">a</w> <w n="10.8">plu</w> <w n="10.9">si</w> <w n="10.10">fort</w></l>
				<l n="11" num="2.2"><w n="11.1">Que</w> <w n="11.2">l</w>’<w n="11.3">on</w> <w n="11.4">n</w>’<w n="11.5">aurait</w> <w n="11.6">pas</w> <w n="11.7">mis</w> <w n="11.8">Monsieur</w> <w n="11.9">Carnot</w> <w n="11.10">dehors</w>.</l>
			</lg>
			</div>
			<div type="section" n="3">
			<head type="number">III</head>
			<lg n="1">
				<l n="12" num="1.1"><w n="12.1">Alors</w> <w n="12.2">la</w> <w n="12.3">Banlieue</w>, <w n="12.4">toujours</w> <w n="12.5">si</w> <w n="12.6">pratique</w>,</l>
				<l n="13" num="1.2"><w n="13.1">Pour</w> <w n="13.2">éviter</w> <w n="13.3">un</w></l>
				<l n="14" num="1.3"><w n="14.1">Contretemps</w> <w n="14.2">fâcheux</w>,</l>
				<l n="15" num="1.4"><w n="15.1">A</w> <w n="15.2">pris</w> <w n="15.3">à</w> <w n="15.4">la</w> <w n="15.5">fois</w> <w n="15.6">parapluie</w> <w n="15.7">et</w> <w n="15.8">stick</w>,</l>
			</lg>
			<lg n="2">
				<l n="16" num="2.1"><w n="16.1">Et</w> <w n="16.2">les</w> <w n="16.3">a</w> <w n="16.4">perdus</w> <w n="16.5">tous</w> <w n="16.6">deux</w></l>
				<l n="17" num="2.2"><w n="17.1">Dans</w> <w n="17.2">le</w> <w n="17.3">train</w>.</l>
			</lg>
			</div>
	</div></body></text></TEI>