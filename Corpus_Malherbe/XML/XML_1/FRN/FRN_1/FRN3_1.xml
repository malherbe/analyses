<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Inattentions et Sollicitudes</title>
				<title type="medium">Édition électronique</title>
				<author key="FRN">
					<name>
						<forename>Maurice Étienne</forename>
						<surname>LEGRAND</surname>
						<addName type="pen_name">FRANC-NOHAIN</addName>
					</name>
					<date from="1872" to="1934">1872-1934</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>649 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">FRN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Les Inattentions et Sollicitudes</title>
						<author>Franc-Nohain</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k5438658j.texteImage</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Inattentions et Sollicitudes</title>
								<author>Franc-Nohain</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>L. VANIER</publisher>
									<date when="1894">1894</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1894">1894</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ) ont été restituées.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-06-04" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2023-06-25" who="RR" type="analyse">Étape 7 de l'analyse automatique du corpus : Traitement des rimes, des strophes et de la forme globale.</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="FRN3">
		<head type="main">CANTILÈNE DES TRAINS<lb></lb>QU’ON MANQUE</head>
			<lg n="1">
				<l n="1" num="1.1"><w n="1.1">Ce</w> <w n="1.2">sont</w> <w n="1.3">les</w> <w n="1.4">gares</w>, <w n="1.5">les</w> <w n="1.6">lointaines</w> <w n="1.7">gares</w>,</l>
				<l n="2" num="1.2"><w n="2.1">Où</w> <w n="2.2">l</w>’<w n="2.3">on</w> <w n="2.4">arrive</w> <w n="2.5">toujours</w> <w n="2.6">trop</w> <w n="2.7">tard</w>.</l>
			</lg>
			<lg n="2">
				<l n="3" num="2.1"><w n="3.1">Belle</w>-<w n="3.2">maman</w>, <w n="3.3">embrassez</w>-<w n="3.4">moi</w>,</l>
				<l n="4" num="2.2"><w n="4.1">Embrassez</w>-<w n="4.2">moi</w> <w n="4.3">encore</w> <w n="4.4">une</w> <w n="4.5">fois</w>,</l>
				<l n="5" num="2.3"><w n="5.1">Et</w> <w n="5.2">empilons</w>-<w n="5.3">nous</w> <w n="5.4">comme</w> <w n="5.5">des</w> <w n="5.6">anchois</w></l>
				<l n="6" num="2.4"><w n="6.1">Dans</w> <w n="6.2">le</w> <w n="6.3">vieil</w> <w n="6.4">omnibus</w> <w n="6.5">bourgeois</w>.</l>
			</lg>
			<lg n="3">
				<l n="7" num="3.1"><w n="7.1">Ouf</w>, <w n="7.2">brouf</w> !</l>
				<l n="8" num="3.2"><w n="8.1">Waterpoofs</w> !</l>
				<l n="9" num="3.3"><w n="9.1">Cannes</w> <w n="9.2">et</w> <w n="9.3">parapluies</w>…</l>
				<l n="10" num="3.4"><w n="10.1">Je</w> <w n="10.2">ne</w> <w n="10.3">sais</w> <w n="10.4">plus</w> <w n="10.5">du</w> <w n="10.6">tout</w> <w n="10.7">où</w> <w n="10.8">j</w>’<w n="10.9">en</w> <w n="10.10">suis</w>.</l>
			</lg>
			<lg n="4">
				<l n="11" num="4.1"><w n="11.1">Voici</w> <w n="11.2">venir</w> <w n="11.3">les</w> <w n="11.4">hommes</w> <w n="11.5">d</w>’<w n="11.6">équipe</w></l>
				<l n="12" num="4.2"><w n="12.1">Qui</w> <w n="12.2">regardent</w> <w n="12.3">béatement</w> <w n="12.4">en</w> <w n="12.5">fumant</w> <w n="12.6">leurs</w> <w n="12.7">pipes</w>.</l>
			</lg>
			<lg n="5">
				<l n="13" num="5.1"><w n="13.1">Le</w> <w n="13.2">train</w>, <w n="13.3">le</w> <w n="13.4">train</w> <w n="13.5">que</w> <w n="13.6">j</w>’<w n="13.7">entends</w> !</l>
				<l n="14" num="5.2"><w n="14.1">Nous</w> <w n="14.2">n</w>’<w n="14.3">arriverons</w> <w n="14.4">jamais</w> <w n="14.5">à</w> <w n="14.6">temps</w>.</l>
				<l n="15" num="5.3">(<w n="15.1">Certainement</w>.)</l>
			</lg>
			<lg n="6">
				<l n="16" num="6.1">— <w n="16.1">Monsieur</w>, <w n="16.2">on</w> <w n="16.3">ne</w> <w n="16.4">peut</w> <w n="16.5">plus</w> <w n="16.6">enregistrer</w> <w n="16.7">vos</w> <w n="16.8">bagages</w> :</l>
				<l n="17" num="6.2"><w n="17.1">C</w>’<w n="17.2">est</w> <w n="17.3">vraiment</w> <w n="17.4">dommage</w>.</l>
			</lg>
			<lg n="7">
				<l n="18" num="7.1"><w n="18.1">La</w> <w n="18.2">cloche</w> <w n="18.3">du</w> <w n="18.4">départ</w>, <w n="18.5">écoutez</w> <w n="18.6">la</w> <w n="18.7">cloche</w> :</l>
				<l n="19" num="7.2"><w n="19.1">Le</w> <w n="19.2">mécanicien</w> <w n="19.3">et</w> <w n="19.4">le</w> <w n="19.5">chauffeur</w> <w n="19.6">ont</w> <w n="19.7">un</w> <w n="19.8">cœur</w> <w n="19.9">de</w> <w n="19.10">roche</w> ;</l>
				<l n="20" num="7.3"><w n="20.1">Alors</w>, <w n="20.2">inutile</w> <w n="20.3">d</w>’<w n="20.4">agiter</w> <w n="20.5">notre</w> <w n="20.6">mouchoir</w> <w n="20.7">de</w> <w n="20.8">poche</w> ?</l>
			</lg>
			<lg n="8">
				<l n="21" num="8.1"><w n="21.1">Ainsi</w> <w n="21.2">les</w> <w n="21.3">trains</w> <w n="21.4">s</w>’<w n="21.5">en</w> <w n="21.6">vont</w> <w n="21.7">rapides</w> <w n="21.8">et</w> <w n="21.9">discrets</w> :</l>
				<l n="22" num="8.2"><w n="22.1">Et</w> <w n="22.2">l</w>’<w n="22.3">on</w> <w n="22.4">est</w> <w n="22.5">très</w> <w n="22.6">embêté</w> <w n="22.7">après</w>.</l>
			</lg>
	</div></body></text></TEI>