<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Inattentions et Sollicitudes</title>
				<title type="medium">Édition électronique</title>
				<author key="FRN">
					<name>
						<forename>Maurice Étienne</forename>
						<surname>LEGRAND</surname>
						<addName type="pen_name">FRANC-NOHAIN</addName>
					</name>
					<date from="1872" to="1934">1872-1934</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>649 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">FRN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Les Inattentions et Sollicitudes</title>
						<author>Franc-Nohain</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k5438658j.texteImage</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Inattentions et Sollicitudes</title>
								<author>Franc-Nohain</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>L. VANIER</publisher>
									<date when="1894">1894</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1894">1894</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ) ont été restituées.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-06-04" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2023-06-23" who="RR" type="analyse">Étape 7 de l'analyse automatique du corpus : Traitement des rimes, des strophes et de la forme globale.</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="FRN24">
		<head type="main">CHAPITRE DES CHAPEAUX</head>
			<head type="sub_1">QUON RENCONTRE LE JOUR DU 1<hi rend="sup">er</hi> JANVIER</head>
			<lg n="1">
				<l n="1" num="1.1"><w n="1.1">Du</w> <w n="1.2">fond</w> <w n="1.3">des</w> <w n="1.4">familiales</w> <w n="1.5">armoires</w>,</l>
				<l n="2" num="1.2"><w n="2.1">C</w>’<w n="2.2">est</w> <w n="2.3">ce</w> <w n="2.4">jour</w>-<w n="2.5">là</w> <w n="2.6">qu</w>’<w n="2.7">on</w> <w n="2.8">fait</w> <w n="2.9">sortir</w> <w n="2.10">les</w> <w n="2.11">chapeaux</w> <w n="2.12">noirs</w>.</l>
			</lg>
			<lg n="2">
				<l n="3" num="2.1"><w n="3.1">Quelle</w> <w n="3.2">que</w> <w n="3.3">soit</w> <w n="3.4">la</w> <w n="3.5">température</w>,</l>
				<l n="4" num="2.2"><w n="4.1">Pluie</w> <w n="4.2">ou</w> <w n="4.3">vent</w>, <w n="4.4">dégel</w> <w n="4.5">ou</w> <w n="4.6">froidure</w>,</l>
				<l n="5" num="2.3"><w n="5.1">Que</w> <w n="5.2">les</w> <w n="5.3">chapeaux</w> <w n="5.4">noirs</w> <w n="5.5">ont</w> <w n="5.6">bon</w> <w n="5.7">air</w></l>
				<l n="6" num="2.4"><w n="6.1">Sur</w> <w n="6.2">le</w> <w n="6.3">crâne</w> <w n="6.4">des</w> <w n="6.5">fonctionnaires</w></l>
				<l n="7" num="2.5"><w n="7.1">Qui</w> <w n="7.2">vont</w> <w n="7.3">à</w> <w n="7.4">la</w> <w n="7.5">sous</w>-<w n="7.6">préfecture</w> !</l>
			</lg>
			<lg n="3">
				<l n="8" num="3.1"><w n="8.1">Par</w> <w n="8.2">bandes</w> <w n="8.3">de</w> <w n="8.4">trois</w> <w n="8.5">ou</w> <w n="8.6">de</w> <w n="8.7">six</w>,</l>
				<l n="9" num="3.2"><w n="9.1">Quelquefois</w> <w n="9.2">plus</w>, <w n="9.3">ou</w> <w n="9.4">moins</w> <w n="9.5">aussi</w>,</l>
				<l n="10" num="3.3"><w n="10.1">On</w> <w n="10.2">dirait</w> <w n="10.3">d</w>’<w n="10.4">un</w> <w n="10.5">vol</w> <w n="10.6">d</w>’<w n="10.7">hirondelles</w></l>
				<l n="11" num="3.4"><w n="11.1">Passant</w>, <w n="11.2">avec</w> <w n="11.3">de</w> <w n="11.4">petits</w> <w n="11.5">cris</w>,</l>
				<l n="12" num="3.5"><w n="12.1">À</w> <w n="12.2">tire</w>-<w n="12.3">d</w>’<w n="12.4">aile</w> — <w n="12.5">et</w> <w n="12.6">quelles</w> <w n="12.7">ailes</w> ! —</l>
			</lg>
			<lg n="4">
				<l n="13" num="4.1"><w n="13.1">Car</w> <w n="13.2">les</w> <w n="13.3">ailes</w> <w n="13.4">des</w> <w n="13.5">chapeaux</w> <w n="13.6">noirs</w></l>
				<l n="14" num="4.2"><w n="14.1">Toutes</w> <w n="14.2">nous</w> <w n="14.3">content</w> <w n="14.4">quelque</w> <w n="14.5">attendrissante</w> <w n="14.6">histoire</w> :</l>
			</lg>
			<lg n="5">
				<l n="15" num="5.1">— <w n="15.1">Chapeau</w> <w n="15.2">aux</w> <w n="15.3">larges</w> <w n="15.4">bords</w>, <w n="15.5">quand</w> <w n="15.6">donc</w> <w n="15.7">pris</w>-<w n="15.8">tu</w> <w n="15.9">ton</w> <w n="15.10">vol</w>,</l>
				<l n="16" num="5.2">— <w n="16.1">Pour</w> <w n="16.2">le</w> <w n="16.3">baptême</w> <w n="16.4">du</w> <w n="16.5">petit</w> <w n="16.6">Paul</w> ?</l>
			</lg>
			<lg n="6">
				<l n="17" num="6.1">— <w n="17.1">Bords</w> <w n="17.2">étroits</w>, <w n="17.3">de</w> <w n="17.4">quoi</w> <w n="17.5">nous</w> <w n="17.6">faites</w>-<w n="17.7">vous</w> <w n="17.8">souvenir</w>,</l>
				<l n="18" num="6.2">— <w n="18.1">De</w> <w n="18.2">la</w> <w n="18.3">fois</w> <w n="18.4">où</w> <w n="18.5">les</w> <w n="18.6">Ministres</w> <w n="18.7">devaient</w> <w n="18.8">venir</w> ?</l>
			</lg>
			<lg n="7">
				<l n="19" num="7.1"><w n="19.1">Et</w> <w n="19.2">c</w>’<w n="19.3">est</w> <w n="19.4">ainsi</w> <w n="19.5">qu</w>’<w n="19.6">en</w> <w n="19.7">rangs</w> <w n="19.8">serrés</w>,</l>
				<l n="20" num="7.2"><w n="20.1">Ils</w> <w n="20.2">vont</w>, <w n="20.3">soigneusement</w> <w n="20.4">lustrés</w></l>
				<l n="21" num="7.3"><w n="21.1">Par</w> <w n="21.2">la</w> <w n="21.3">main</w> <w n="21.4">des</w> <w n="21.5">femmes</w> <w n="21.6">aimantes</w>,</l>
				<l n="22" num="7.4"><w n="22.1">Qui</w> <w n="22.2">de</w> <w n="22.3">loin</w> <w n="22.4">regardent</w> <w n="22.5">aux</w> <w n="22.6">carreaux</w>,</l>
				<l n="23" num="7.5"><w n="23.1">Et</w> <w n="23.2">trouvent</w> <w n="23.3">que</w> <w n="23.4">leurs</w> <w n="23.5">maris</w> <w n="23.6">ont</w> <w n="23.7">des</w> <w n="23.8">chapeaux</w></l>
				<l n="24" num="7.6"><w n="24.1">D</w>’<w n="24.2">une</w> <w n="24.3">forme</w> <w n="24.4">véritablement</w> <w n="24.5">élégante</w>.</l>
				<l n="25" num="7.7">(<w n="25.1">Pourtant</w>, <w n="25.2">il</w> <w n="25.3">faut</w> <w n="25.4">avouer</w> <w n="25.5">que</w> <w n="25.6">le</w> <w n="25.7">chapeau</w> <w n="25.8">du</w> <w n="25.9">surnuméraire</w>,</l>
				<l n="26" num="7.8"><w n="26.1">Le</w> <w n="26.2">surnuméraire</w>, <w n="26.3">bien</w> <w n="26.4">entendu</w>, <w n="26.5">de</w> <w n="26.6">l</w>’<w n="26.7">enregistrement</w>,</l>
				<l n="27" num="7.9">— <w n="27.1">Un</w> <w n="27.2">jeune</w> <w n="27.3">homme</w> <w n="27.4">charmant</w>,</l>
				<l n="28" num="7.10"><w n="28.1">Ma</w> <w n="28.2">chère</w> ! —</l>
				<l n="29" num="7.11"><w n="29.1">Vous</w> <w n="29.2">a</w> <w n="29.3">encore</w> <w n="29.4">une</w> <w n="29.5">allure</w> <w n="29.6">particulière</w> ;</l>
				<l n="30" num="7.12">— <w n="30.1">Un</w> <w n="30.2">chapeau</w> <w n="30.3">qui</w> <w n="30.4">vient</w> <w n="30.5">de</w> <w n="30.6">chez</w> ? ? <w n="30.7">Charles</w> ? ?</l>
				<l n="31" num="7.13"><w n="31.1">Tu</w> <w n="31.2">parles</w> !… )</l>
			</lg>
			<lg n="8">
				<l n="32" num="8.1"><w n="32.1">Lorsque</w> <w n="32.2">sera</w> <w n="32.3">tombée</w> <w n="32.4">la</w> <w n="32.5">nuit</w>,</l>
				<l n="33" num="8.2"><w n="33.1">Après</w> <w n="33.2">avoir</w> <w n="33.3">fait</w> <w n="33.4">deux</w> <w n="33.5">ou</w> <w n="33.6">trois</w> <w n="33.7">tours</w> <w n="33.8">de</w> <w n="33.9">ville</w>,</l>
				<l n="34" num="8.3"><w n="34.1">On</w> <w n="34.2">remettra</w> <w n="34.3">les</w> <w n="34.4">chapeaux</w> <w n="34.5">noirs</w> <w n="34.6">dans</w> <w n="34.7">leurs</w> <w n="34.8">étuis</w>,</l>
				<l n="35" num="8.4"><w n="35.1">Où</w> <w n="35.2">ils</w> <w n="35.3">se</w> <w n="35.4">rendormiront</w> <w n="35.5">bien</w> <w n="35.6">tranquilles</w>.</l>
			</lg>
			<lg n="9">
				<l n="36" num="9.1"><w n="36.1">On</w> <w n="36.2">les</w> <w n="36.3">ressortira</w> <w n="36.4">pour</w> <w n="36.5">le</w> <w n="36.6">Quatorze</w> <w n="36.7">Juillet</w>,</l>
				<l n="37" num="9.2"><w n="37.1">Ou</w> <w n="37.2">même</w> <w n="37.3">avant</w>, <w n="37.4">si</w> <w n="37.5">l</w>’<w n="37.6">on</w> <w n="37.7">change</w> <w n="37.8">de</w> <w n="37.9">sous</w>-<w n="37.10">préfet</w>,</l>
				<l n="38" num="9.3"><w n="38.1">Ou</w> <w n="38.2">si</w> <w n="38.3">Monsieur</w> <w n="38.4">Carnot</w> <w n="38.5">passe</w> <w n="38.6">à</w> <w n="38.7">la</w> <w n="38.8">gare</w> ;</l>
				<l n="39" num="9.4"><w n="39.1">Ou</w> <w n="39.2">encore</w> <w n="39.3">si</w> <w n="39.4">le</w> <w n="39.5">Directeur</w></l>
				<l n="40" num="9.5"><w n="40.1">Venait</w> <w n="40.2">à</w> <w n="40.3">mourir</w>, <w n="40.4">par</w> <w n="40.5">bonheur</w>,</l>
				<l n="41" num="9.6"><w n="41.1">Sans</w> <w n="41.2">crier</w> <w n="41.3">gare</w>,</l>
				<l n="42" num="9.7">— <w n="42.1">Ce</w> <w n="42.2">qui</w> <w n="42.3">serait</w> <w n="42.4">excellent</w></l>
				<l n="43" num="9.8"><w n="43.1">Au</w> <w n="43.2">point</w> <w n="43.3">de</w> <w n="43.4">vue</w> <w n="43.5">de</w> <w n="43.6">l</w>’<w n="43.7">avancement</w>.</l>
			</lg>
	</div></body></text></TEI>