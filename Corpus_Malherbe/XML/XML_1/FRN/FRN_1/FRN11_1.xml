<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Inattentions et Sollicitudes</title>
				<title type="medium">Édition électronique</title>
				<author key="FRN">
					<name>
						<forename>Maurice Étienne</forename>
						<surname>LEGRAND</surname>
						<addName type="pen_name">FRANC-NOHAIN</addName>
					</name>
					<date from="1872" to="1934">1872-1934</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>649 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">FRN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Les Inattentions et Sollicitudes</title>
						<author>Franc-Nohain</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k5438658j.texteImage</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Inattentions et Sollicitudes</title>
								<author>Franc-Nohain</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>L. VANIER</publisher>
									<date when="1894">1894</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1894">1894</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ) ont été restituées.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-06-04" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="FRN11">
		<head type="main">LA RONDE DES NEVEUX <lb></lb>INATTENTIONNÉS</head>
			<div type="section" n="1">
			<head type="number">I</head>
			<lg n="1">
				<l n="1" num="1.1"><w n="1.1">Nous</w> <w n="1.2">sommes</w> <w n="1.3">allés</w> <w n="1.4">dans</w> <w n="1.5">des</w> <w n="1.6">gares</w> <w n="1.7">de</w> <w n="1.8">la</w> <w n="1.9">ceinture</w> ;</l>
				<l n="2" num="1.2"><w n="2.1">Nous</w> <w n="2.2">avons</w> <w n="2.3">parcouru</w> <w n="2.4">des</w> <w n="2.5">plaines</w> <w n="2.6">et</w> <w n="2.7">des</w> <w n="2.8">coteaux</w> ;</l>
				<l n="3" num="1.3"><w n="3.1">Nous</w> <w n="3.2">avons</w> <w n="3.3">vu</w> <w n="3.4">stopper</w> <w n="3.5">des</w> <w n="3.6">bateaux</w>,</l>
				<l n="4" num="1.4"><w n="4.1">Et</w> <w n="4.2">nous</w> <w n="4.3">avons</w> <w n="4.4">vu</w> <w n="4.5">s</w>’<w n="4.6">arrêter</w> <w n="4.7">des</w> <w n="4.8">voitures</w> :</l>
				<l n="5" num="1.5"><w n="5.1">Mais</w> <w n="5.2">les</w> <w n="5.3">bateaux</w> <w n="5.4">sont</w> <w n="5.5">repartis</w></l>
				<l n="6" num="1.6"><w n="6.1">Et</w> <w n="6.2">les</w> <w n="6.3">voitures</w> <w n="6.4">sont</w> <w n="6.5">reparties</w> <w n="6.6">aussi</w>.</l>
				<l n="7" num="1.7"><space unit="char" quantity="14"></space><w n="7.1">Sous</w> <w n="7.2">les</w> <w n="7.3">quinconces</w>,</l>
				<l n="8" num="1.8"><space unit="char" quantity="8"></space><w n="8.1">Nous</w> <w n="8.2">ne</w> <w n="8.3">retrouvons</w> <w n="8.4">pas</w> <w n="8.5">nos</w> <w n="8.6">oncles</w>.</l>
			</lg>
			</div>
			<div type="section" n="2">
			<head type="number">II</head>
			<lg n="1">
				<l n="9" num="1.1"><w n="9.1">Nous</w> <w n="9.2">y</w> <w n="9.3">sommes</w> <w n="9.4">allés</w> <w n="9.5">bien</w> <w n="9.6">des</w> <w n="9.7">dimanches</w>,</l>
				<l n="10" num="1.2"><w n="10.1">Nous</w> <w n="10.2">y</w> <w n="10.3">sommes</w> <w n="10.4">allés</w> <w n="10.5">bien</w> <w n="10.6">des</w> <w n="10.7">lundis</w> ;</l>
				<l n="11" num="1.3"><w n="11.1">Mardis</w>, <w n="11.2">mercredis</w>, <w n="11.3">jeudis</w>, <w n="11.4">vendredis</w>,</l>
				<l n="12" num="1.4"><w n="12.1">Ça</w> <w n="12.2">n</w>’<w n="12.3">a</w> <w n="12.4">pas</w> <w n="12.5">été</w> <w n="12.6">une</w> <w n="12.7">autre</w> <w n="12.8">paire</w> <w n="12.9">de</w> <w n="12.10">manches</w>.</l>
				<l n="13" num="1.5"><w n="13.1">Il</w> <w n="13.2">est</w> <w n="13.3">probable</w> <w n="13.4">que</w> <w n="13.5">nous</w> <w n="13.6">y</w> <w n="13.7">serions</w> <w n="13.8">allés</w> <w n="13.9">les</w> <w n="13.10">samedis</w>,</l>
				<l n="14" num="1.6"><w n="14.1">Ça</w> <w n="14.2">aurait</w> <w n="14.3">été</w> <w n="14.4">la</w> <w n="14.5">même</w> <w n="14.6">chose</w> <w n="14.7">aussi</w>.</l>
				<l n="15" num="1.7"><space unit="char" quantity="14"></space><w n="15.1">Sous</w> <w n="15.2">les</w> <w n="15.3">quinconces</w></l>
				<l n="16" num="1.8"><space unit="char" quantity="8"></space><w n="16.1">Nous</w> <w n="16.2">ne</w> <w n="16.3">retrouvons</w> <w n="16.4">pas</w> <w n="16.5">nos</w> <w n="16.6">oncles</w>.</l>
			</lg>
			</div>
			<div type="section" n="3">
			<head type="number">III</head>
			<lg n="1">
				<l n="17" num="1.1"><w n="17.1">Alors</w> <w n="17.2">vous</w> <w n="17.3">comprenez</w>, <w n="17.4">ça</w> <w n="17.5">nous</w> <w n="17.6">dégoûte</w> :</l>
				<l n="18" num="1.2"><w n="18.1">S</w>’<w n="18.2">ils</w> <w n="18.3">ont</w> <w n="18.4">fait</w> <w n="18.5">la</w> <w n="18.6">fête</w> <w n="18.7">dans</w> <w n="18.8">les</w> <w n="18.9">fortifications</w>,</l>
				<l n="19" num="1.3"><w n="19.1">Et</w> <w n="19.2">qu</w>’<w n="19.3">ils</w> <w n="19.4">ont</w> <w n="19.5">oublié</w> <w n="19.6">le</w> <w n="19.7">numéro</w> <w n="19.8">de</w> <w n="19.9">leur</w> <w n="19.10">maison</w>,</l>
				<l n="20" num="1.4"><w n="20.1">Qu</w>’<w n="20.2">est</w>-<w n="20.3">ce</w> <w n="20.4">que</w> <w n="20.5">vous</w> <w n="20.6">voulez</w> <w n="20.7">que</w> <w n="20.8">nous</w>, <w n="20.9">on</w> <w n="20.10">y</w> <w n="20.11">foute</w> !</l>
				<l n="21" num="1.5"><w n="21.1">Ne</w> <w n="21.2">vaudrait</w>-<w n="21.3">il</w> <w n="21.4">pas</w> <w n="21.5">mieux</w>, <w n="21.6">tout</w> <w n="21.7">bêtement</w>,</l>
				<l n="22" num="1.6"><w n="22.1">S</w>’<w n="22.2">adresser</w> <w n="22.3">à</w> <w n="22.4">quelque</w> <w n="22.5">agence</w> <w n="22.6">de</w> <w n="22.7">renseignements</w> ?</l>
				<l n="23" num="1.7"><space unit="char" quantity="14"></space><w n="23.1">Sous</w> <w n="23.2">les</w> <w n="23.3">quinconces</w></l>
				<l n="24" num="1.8"><space unit="char" quantity="8"></space><w n="24.1">Nous</w> <w n="24.2">ne</w> <w n="24.3">retrouvons</w> <w n="24.4">pas</w> <w n="24.5">nos</w> <w n="24.6">oncles</w>.</l>
			</lg>
			</div>
	</div></body></text></TEI>