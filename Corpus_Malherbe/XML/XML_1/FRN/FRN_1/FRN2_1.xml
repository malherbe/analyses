<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Inattentions et Sollicitudes</title>
				<title type="medium">Édition électronique</title>
				<author key="FRN">
					<name>
						<forename>Maurice Étienne</forename>
						<surname>LEGRAND</surname>
						<addName type="pen_name">FRANC-NOHAIN</addName>
					</name>
					<date from="1872" to="1934">1872-1934</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>649 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">FRN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Les Inattentions et Sollicitudes</title>
						<author>Franc-Nohain</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k5438658j.texteImage</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Inattentions et Sollicitudes</title>
								<author>Franc-Nohain</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>L. VANIER</publisher>
									<date when="1894">1894</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1894">1894</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ) ont été restituées.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-06-04" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="FRN2">
		<head type="main">LA MAÎTRESSE QUE JE PRENDRAI</head>
			<lg n="1">
				<l n="1" num="1.1"><w n="1.1">La</w> <w n="1.2">maîtresse</w> <w n="1.3">que</w> <w n="1.4">je</w> <w n="1.5">prendrai</w> <w n="1.6">sera</w> <w n="1.7">très</w> <w n="1.8">bète</w> :</l>
				<l n="2" num="1.2"><w n="2.1">Les</w> <w n="2.2">femmes</w> <w n="2.3">d</w>’<w n="2.4">esprit</w>, <w n="2.5">ça</w> <w n="2.6">nous</w> <w n="2.7">fait</w> <w n="2.8">trop</w> <w n="2.9">mal</w> <w n="2.10">à</w> <w n="2.11">la</w> <w n="2.12">tète</w>.</l>
			</lg>
			<lg n="2">
				<l n="3" num="2.1"><w n="3.1">Elle</w> <w n="3.2">ne</w> <w n="3.3">sera</w> <w n="3.4">pas</w> <w n="3.5">même</w> <w n="3.6">de</w> <w n="3.7">la</w> <w n="3.8">Société</w> <w n="3.9">des</w> <w n="3.10">Gens</w> <w n="3.11">de</w> <w n="3.12">lettres</w>,</l>
				<l n="4" num="2.2"><w n="4.1">Et</w> <w n="4.2">elle</w> <w n="4.3">n</w>’<w n="4.4">aura</w> <w n="4.5">jamais</w> <w n="4.6">été</w> <w n="4.7">institutrice</w>, <w n="4.8">peut</w>-<w n="4.9">être</w>.</l>
			</lg>
			<lg n="3">
				<l n="5" num="3.1"><w n="5.1">Et</w> <w n="5.2">alors</w> <w n="5.3">elle</w> <w n="5.4">ne</w> <w n="5.5">m</w>’<w n="5.6">appellera</w> <w n="5.7">pas</w> <w n="5.8">son</w> <w n="5.9">cher</w> <w n="5.10">poète</w>,</l>
				<l n="6" num="3.2"><w n="6.1">Et</w> <w n="6.2">elle</w> <w n="6.3">ne</w> <w n="6.4">recopiera</w> <w n="6.5">pas</w> <w n="6.6">mes</w> <w n="6.7">vers</w> <w n="6.8">à</w> <w n="6.9">l</w>’<w n="6.10">encre</w> <w n="6.11">violette</w>.</l>
			</lg>
			<lg n="4">
				<l n="7" num="4.1"><w n="7.1">Mais</w> <w n="7.2">je</w> <w n="7.3">me</w> <w n="7.4">complairai</w>, <w n="7.5">en</w> <w n="7.6">d</w>’<w n="7.7">exquises</w> <w n="7.8">délices</w>, <w n="7.9">à</w> <w n="7.10">reconnaître</w></l>
				<l n="8" num="4.2"><w n="8.1">Qu</w>’<w n="8.2">elle</w> <w n="8.3">manque</w> <w n="8.4">totalement</w>, <w n="8.5">oh</w> ! <w n="8.6">mais</w> <w n="8.7">totalement</w>, <w n="8.8">de</w> <w n="8.9">lettres</w>.</l>
			</lg>
			<lg n="5">
				<l n="9" num="5.1"><w n="9.1">Et</w> <w n="9.2">nous</w> <w n="9.3">ferons</w> <w n="9.4">très</w> <w n="9.5">gentiment</w> <w n="9.6">tous</w> <w n="9.7">les</w> <w n="9.8">deux</w> <w n="9.9">la</w> <w n="9.10">petite</w> <w n="9.11">fête</w>,</l>
				<l n="10" num="5.2"><w n="10.1">Sans</w> <w n="10.2">dépasser</w> <w n="10.3">d</w>’<w n="10.4">ailleurs</w>, <w n="10.5">bien</w> <w n="10.6">entendu</w>, <w n="10.7">les</w> <w n="10.8">bornes</w> <w n="10.9">honnêtes</w>.</l>
			</lg>
			<lg n="6">
				<l n="11" num="6.1"><w n="11.1">Et</w> <w n="11.2">ainsi</w> <w n="11.3">nous</w> <w n="11.4">nous</w> <w n="11.5">serons</w> <w n="11.6">aimés</w> <w n="11.7">bêtement</w>, <w n="11.8">comme</w> <w n="11.9">les</w> <w n="11.10">bêtes</w> :</l>
				<l n="12" num="6.2"><w n="12.1">Et</w> <w n="12.2">puis</w>, <w n="12.3">après</w> <w n="12.4">tout</w>, <w n="12.5">ça</w> <w n="12.6">n</w>’<w n="12.7">est</w> <w n="12.8">pas</w> <w n="12.9">déjà</w> <w n="12.10">si</w> <w n="12.11">bête</w>.</l>
			</lg>
	</div></body></text></TEI>