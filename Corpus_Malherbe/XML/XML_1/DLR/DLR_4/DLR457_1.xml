<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FIGURE DE PROUE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2836 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/delaruemardruepoesies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">La Figure de Proue</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/stream/lafiguredeproue00dela/lafiguredeproue00dela_djvu.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>La Figure de Proue</title>
								<author>Lucie Delarue-Mardrus</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Fasquelle</publisher>
									<date when="1908">1908</date>
								</imprint>
							</monogr>
							<note>Source des poèmes ajoutés et édition de référence pour les corrections métriques.</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1908">1908</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
				<p>Des poèmes ont été ajoutés afin d’être conforme à l’édition de référence (source : archive.org)</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">AU PORT</head><div type="poem" key="DLR457">
					<head type="main">UN CHANT DE RETOUR</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Honfleur</w>, <w n="1.2">ma</w> <w n="1.3">ville</w> <w n="1.4">de</w> <w n="1.5">naissance</w></l>
						<l n="2" num="1.2"><w n="2.1">Que</w> <w n="2.2">j</w>’<w n="2.3">aime</w> <w n="2.4">plus</w> <w n="2.5">que</w> <w n="2.6">de</w> <w n="2.7">raison</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Je</w> <w n="3.2">te</w> <w n="3.3">reviens</w> <w n="3.4">de</w> <w n="3.5">l</w>’<w n="3.6">horizon</w></l>
						<l n="4" num="1.4"><w n="4.1">Ayant</w> <w n="4.2">mené</w> <w n="4.3">loin</w> <w n="4.4">mon</w> <w n="4.5">enfance</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Je</w> <w n="5.2">t</w>’<w n="5.3">avais</w> <w n="5.4">dans</w> <w n="5.5">l</w>’<w n="5.6">âme</w> <w n="5.7">et</w> <w n="5.8">la</w> <w n="5.9">chair</w>,</l>
						<l n="6" num="2.2"><w n="6.1">Et</w> <w n="6.2">si</w> <w n="6.3">j</w>’<w n="6.4">ai</w> <w n="6.5">quitté</w> <w n="6.6">ta</w> <w n="6.7">jetée</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Ce</w> <w n="7.2">n</w>’<w n="7.3">est</w> <w n="7.4">qu</w>’<w n="7.5">à</w> <w n="7.6">tout</w> <w n="7.7">jamais</w> <w n="7.8">hantée</w></l>
						<l n="8" num="2.4"><w n="8.1">Par</w> <w n="8.2">ta</w> <w n="8.3">grisaille</w> <w n="8.4">sur</w> <w n="8.5">la</w> <w n="8.6">mer</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Ailleurs</w>, <w n="9.2">il</w> <w n="9.3">fait</w> <w n="9.4">parfois</w> <w n="9.5">bon</w> <w n="9.6">vivre</w>,</l>
						<l n="10" num="3.2"><w n="10.1">Mais</w> <w n="10.2">toujours</w>, <w n="10.3">ville</w> <w n="10.4">des</w> <w n="10.5">prés</w> <w n="10.6">verts</w>,</l>
						<l n="11" num="3.3"><w n="11.1">On</w> <w n="11.2">est</w> <w n="11.3">un</w> <w n="11.4">peu</w> <w n="11.5">ton</w> <w n="11.6">marin</w> <w n="11.7">ivre</w></l>
						<l n="12" num="3.4"><w n="12.1">Qui</w> <w n="12.2">tangue</w> <w n="12.3">à</w> <w n="12.4">travers</w> <w n="12.5">l</w>’<w n="12.6">univers</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Qui</w> <w n="13.2">sait</w> <w n="13.3">quels</w> <w n="13.4">calmes</w>, <w n="13.5">quelles</w> <w n="13.6">rages</w></l>
						<l n="14" num="4.2"><w n="14.1">On</w> <w n="14.2">a</w> <w n="14.3">vu</w> <w n="14.4">loin</w> <w n="14.5">de</w> <w n="14.6">toi</w>, <w n="14.7">Honfleur</w> ?</l>
						<l n="15" num="4.3"><w n="15.1">Quels</w> <w n="15.2">continents</w> <w n="15.3">couleur</w> <w n="15.4">de</w> <w n="15.5">fleur</w>.</l>
						<l n="16" num="4.4"><w n="16.1">Et</w> <w n="16.2">qui</w> <w n="16.3">sait</w> <w n="16.4">même</w> <w n="16.5">quels</w> <w n="16.6">naufrages</w> ?</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Nul</w> <w n="17.2">ne</w> <w n="17.3">saura</w> <w n="17.4">jamais</w> <w n="17.5">jusqu</w>’<w n="17.6">où</w></l>
						<l n="18" num="5.2"><w n="18.1">On</w> <w n="18.2">a</w> <w n="18.3">pu</w> <w n="18.4">conduire</w> <w n="18.5">sa</w> <w n="18.6">barque</w>.</l>
						<l n="19" num="5.3"><w n="19.1">Mais</w> <w n="19.2">vois</w>-<w n="19.3">tu</w> <w n="19.4">quand</w> <w n="19.5">on</w> <w n="19.6">naît</w> <w n="19.7">monarque</w>,</l>
						<l n="20" num="5.4"><w n="20.1">Monarque</w> <w n="20.2">on</w> <w n="20.3">reste</w> <w n="20.4">jusqu</w>’<w n="20.5">au</w> <w n="20.6">bout</w>.</l>
					</lg>
				</div></body></text></TEI>