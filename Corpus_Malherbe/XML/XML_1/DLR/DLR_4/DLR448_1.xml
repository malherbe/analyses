<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FIGURE DE PROUE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2836 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/delaruemardruepoesies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">La Figure de Proue</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/stream/lafiguredeproue00dela/lafiguredeproue00dela_djvu.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>La Figure de Proue</title>
								<author>Lucie Delarue-Mardrus</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Fasquelle</publisher>
									<date when="1908">1908</date>
								</imprint>
							</monogr>
							<note>Source des poèmes ajoutés et édition de référence pour les corrections métriques.</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1908">1908</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
				<p>Des poèmes ont été ajoutés afin d’être conforme à l’édition de référence (source : archive.org)</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">POÈMES ORANAIS ET KABYLES</head><div type="poem" key="DLR448">
					<head type="main">MISSIVE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Tout</w> <w n="1.2">au</w> <w n="1.3">travers</w> <w n="1.4">des</w> <w n="1.5">sombres</w> <w n="1.6">monts</w> <w n="1.7">du</w> <w n="1.8">Thababor</w></l>
						<l n="2" num="1.2"><w n="2.1">Où</w> <w n="2.2">tournoyait</w> <w n="2.3">l</w>’<w n="2.4">aigle</w> <w n="2.5">kabyle</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Écoute</w> : <w n="3.2">j</w>’<w n="3.3">ai</w> <w n="3.4">passé</w> <w n="3.5">sur</w> <w n="3.6">mon</w> <w n="3.7">cheval</w> <w n="3.8">habile</w></l>
						<l n="4" num="1.4"><w n="4.1">A</w> <w n="4.2">poser</w> <w n="4.3">ses</w> <w n="4.4">pieds</w> <w n="4.5">fins</w> <w n="4.6">sur</w> <w n="4.7">les</w> <w n="4.8">sentiers</w> <w n="4.9">sans</w> <w n="4.10">bords</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Vois</w>-<w n="5.2">tu</w> <w n="5.3">rôder</w>, <w n="5.4">sur</w> <w n="5.5">les</w> <w n="5.6">sommets</w>, <w n="5.7">ces</w> <w n="5.8">brumes</w> <w n="5.9">blanches</w></l>
						<l n="6" num="2.2"><w n="6.1">Elles</w> <w n="6.2">s</w>’<w n="6.3">ouvrent</w> <w n="6.4">parfois</w>, <w n="6.5">laissant</w> <w n="6.6">à</w> <w n="6.7">découvert</w>.</l>
						<l n="7" num="2.3"><w n="7.1">Entre</w> <w n="7.2">la</w> <w n="7.3">torsion</w> <w n="7.4">des</w> <w n="7.5">branches</w>,</l>
						<l n="8" num="2.4"><w n="8.1">Tout</w> <w n="8.2">le</w> <w n="8.3">beau</w> <w n="8.4">mois</w> <w n="8.5">de</w> <w n="8.6">mai</w> <w n="8.7">d</w>’<w n="8.8">en</w> <w n="8.9">bas</w>, <w n="8.10">puissant</w> <w n="8.11">et</w> <w n="8.12">vert</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Là</w>, <w n="9.2">blessés</w> <w n="9.3">par</w> <w n="9.4">le</w> <w n="9.5">drame</w> <w n="9.6">ancien</w> <w n="9.7">des</w> <w n="9.8">orages</w>,</l>
						<l n="10" num="3.2"><w n="10.1">Les</w> <w n="10.2">vieux</w> <w n="10.3">arbres</w> <w n="10.4">haussaient</w> <w n="10.5">l</w>’<w n="10.6">azur</w> <w n="10.7">à</w> <w n="10.8">bout</w> <w n="10.9">de</w> <w n="10.10">bras</w>,</l>
						<l n="11" num="3.3"><w n="11.1">Et</w> <w n="11.2">leurs</w> <w n="11.3">faîtes</w> <w n="11.4">cardaient</w> <w n="11.5">la</w> <w n="11.6">fuite</w> <w n="11.7">des</w> <w n="11.8">nuages</w>,</l>
						<l n="12" num="3.4"><w n="12.1">Les</w> <w n="12.2">jours</w> <w n="12.3">de</w> <w n="12.4">vent</w> <w n="12.5">et</w> <w n="12.6">de</w> <w n="12.7">ciel</w> <w n="12.8">bas</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">J</w>’<w n="13.2">ai</w> <w n="13.3">bu</w> <w n="13.4">dans</w> <w n="13.5">le</w> <w n="13.6">soleil</w> <w n="13.7">les</w> <w n="13.8">sources</w> <w n="13.9">éternelles</w></l>
						<l n="14" num="4.2"><w n="14.1">Qui</w> <w n="14.2">débordent</w>, <w n="14.3">suivant</w> <w n="14.4">leur</w> <w n="14.5">pente</w>, <w n="14.6">de</w> <w n="14.7">partout</w>,</l>
						<l n="15" num="4.3"><w n="15.1">Et</w> <w n="15.2">qui</w> <w n="15.3">gardent</w> <w n="15.4">le</w> <w n="15.5">petit</w> <w n="15.6">goût</w></l>
						<l n="16" num="4.4"><w n="16.1">Des</w> <w n="16.2">fougères</w> <w n="16.3">en</w> <w n="16.4">Heurs</w> <w n="16.5">qui</w> <w n="16.6">détrempent</w> <w n="16.7">en</w> <w n="16.8">elles</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Dans</w> <w n="17.2">la</w> <w n="17.3">neige</w> <w n="17.4">d</w>’<w n="17.5">en</w> <w n="17.6">haut</w>, <w n="17.7">quelque</w> <w n="17.8">gibier</w> <w n="17.9">caché</w></l>
						<l n="18" num="5.2"><w n="18.1">Se</w> <w n="18.2">laissait</w> <w n="18.3">surprendre</w> <w n="18.4">à</w> <w n="18.5">la</w> <w n="18.6">trace</w>.</l>
						<l n="19" num="5.3"><w n="19.1">Et</w>, <w n="19.2">dans</w> <w n="19.3">les</w> <w n="19.4">morceaux</w> <w n="19.5">chauds</w> <w n="19.6">du</w> <w n="19.7">pays</w>, <w n="19.8">les</w> <w n="19.9">rochers</w></l>
						<l n="20" num="5.4"><w n="20.1">Avaient</w> <w n="20.2">des</w> <w n="20.3">singes</w> <w n="20.4">gais</w> <w n="20.5">qui</w> <w n="20.6">faisaient</w> <w n="20.7">la</w> <w n="20.8">grimace</w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Et</w>, <w n="21.2">comprends</w>-<w n="21.3">tu</w> ?… <w n="21.4">Serrée</w> <w n="21.5">au</w> <w n="21.6">pied</w> <w n="21.7">d</w>’<w n="21.8">un</w> <w n="21.9">chêne</w> <w n="21.10">fort</w></l>
						<l n="22" num="6.2"><w n="22.1">Et</w> <w n="22.2">mouillé</w>, <w n="22.3">j</w>’<w n="22.4">oubliais</w> <w n="22.5">la</w> <w n="22.6">grande</w> <w n="22.7">horreur</w> <w n="22.8">du</w> <w n="22.9">sable</w>,</l>
						<l n="23" num="6.3"><w n="23.1">Dans</w> <w n="23.2">la</w> <w n="23.3">bonne</w> <w n="23.4">forêt</w> <w n="23.5">qui</w> <w n="23.6">porte</w>, <w n="23.7">invariable</w>.</l>
						<l n="24" num="6.4"><w n="24.1">Sa</w> <w n="24.2">mousse</w> <w n="24.3">du</w> <w n="24.4">côté</w> <w n="24.5">du</w> <w n="24.6">Nord</w>.</l>
					</lg>
				</div></body></text></TEI>