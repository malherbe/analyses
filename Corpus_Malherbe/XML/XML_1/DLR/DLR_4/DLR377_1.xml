<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FIGURE DE PROUE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2836 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/delaruemardruepoesies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">La Figure de Proue</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/stream/lafiguredeproue00dela/lafiguredeproue00dela_djvu.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>La Figure de Proue</title>
								<author>Lucie Delarue-Mardrus</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Fasquelle</publisher>
									<date when="1908">1908</date>
								</imprint>
							</monogr>
							<note>Source des poèmes ajoutés et édition de référence pour les corrections métriques.</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1908">1908</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
				<p>Des poèmes ont été ajoutés afin d’être conforme à l’édition de référence (source : archive.org)</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">BARBARESQUES</head><div type="poem" key="DLR377">
					<head type="main">SIRÈNE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">L</w>’<w n="1.2">habitante</w> <w n="1.3">des</w> <w n="1.4">mers</w> <w n="1.5">tièdes</w> <w n="1.6">et</w> <w n="1.7">sans</w> <w n="1.8">marée</w></l>
						<l n="2" num="1.2"><w n="2.1">Qui</w> <w n="2.2">cligne</w> <w n="2.3">doucement</w> <w n="2.4">des</w> <w n="2.5">cils</w> <w n="2.6">orientaux</w></l>
						<l n="3" num="1.3"><w n="3.1">Saura</w>-<w n="3.2">t</w>-<w n="3.3">elle</w> <w n="3.4">abolir</w> <w n="3.5">la</w> <w n="3.6">voix</w> <w n="3.7">désespérée</w></l>
						<l n="4" num="1.4"><w n="4.1">De</w> <w n="4.2">celle</w> <w n="4.3">assise</w> <w n="4.4">au</w> <w n="4.5">cœur</w> <w n="4.6">de</w> <w n="4.7">mes</w> <w n="4.8">natives</w> <w n="4.9">eaux</w> ?</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Ne</w> <w n="5.2">le</w> <w n="5.3">dis</w> <w n="5.4">pas</w> ! <w n="5.5">je</w> <w n="5.6">sais</w> <w n="5.7">que</w> <w n="5.8">ta</w> <w n="5.9">face</w> <w n="5.10">est</w> <w n="5.11">très</w> <w n="5.12">pâle</w></l>
						<l n="6" num="2.2"><w n="6.1">Et</w> <w n="6.2">si</w> <w n="6.3">tristes</w> <w n="6.4">tes</w> <w n="6.5">yeux</w> <w n="6.6">qu</w>’<w n="6.7">ils</w> <w n="6.8">ont</w> <w n="6.9">pleuré</w> <w n="6.10">la</w> <w n="6.11">mer</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Certes</w> <w n="7.2">point</w> <w n="7.3">le</w> <w n="7.4">bain</w> <w n="7.5">bleu</w> <w n="7.6">que</w> <w n="7.7">nourrit</w> <w n="7.8">ce</w> <w n="7.9">ciel</w> <w n="7.10">clair</w></l>
						<l n="8" num="2.4"><w n="8.1">Mais</w> <w n="8.2">la</w> <w n="8.3">mer</w> <w n="8.4">rétractile</w> <w n="8.5">et</w> <w n="8.6">septentrionale</w>,</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">La</w> <w n="9.2">grise</w> <w n="9.3">mer</w>, <w n="9.4">ma</w> <w n="9.5">glauque</w>, <w n="9.6">où</w> <w n="9.7">les</w> <w n="9.8">couchants</w> <w n="9.9">sont</w> <w n="9.10">longs</w></l>
						<l n="10" num="3.2"><w n="10.1">Et</w> <w n="10.2">violents</w> <w n="10.3">parmi</w> <w n="10.4">la</w> <w n="10.5">détresse</w> <w n="10.6">des</w> <w n="10.7">brumes</w>.</l>
						<l n="11" num="3.3"><w n="11.1">Et</w>, <w n="11.2">jusque</w> <w n="11.3">sur</w> <w n="11.4">le</w> <w n="11.5">bord</w>, <w n="11.6">empourprent</w> <w n="11.7">les</w> <w n="11.8">écumes</w>.</l>
						<l n="12" num="3.4"><w n="12.1">Comme</w> <w n="12.2">d</w>’<w n="12.3">avoir</w> <w n="12.4">noyé</w> <w n="12.5">tes</w> <w n="12.6">cheveux</w> <w n="12.7">roux</w> <w n="12.8">et</w> <w n="12.9">blonds</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Au</w> <w n="13.2">plus</w> <w n="13.3">chaud</w> <w n="13.4">du</w> <w n="13.5">soleil</w> <w n="13.6">africain</w> <w n="13.7">qui</w> <w n="13.8">m</w>’<w n="13.9">abuse</w>,</l>
						<l n="14" num="4.2"><w n="14.1">Je</w> <w n="14.2">sens</w> <w n="14.3">jusqu</w>’<w n="14.4">à</w> <w n="14.5">mon</w> <w n="14.6">cœur</w> <w n="14.7">se</w> <w n="14.8">glisser</w> <w n="14.9">ton</w> <w n="14.10">corps</w> <w n="14.11">froid</w>,</l>
						<l n="15" num="4.3"><w n="15.1">Translucide</w>, <w n="15.2">et</w> <w n="15.3">plus</w> <w n="15.4">pâle</w> <w n="15.5">et</w> <w n="15.6">beau</w> <w n="15.7">qu</w>’<w n="15.8">une</w> <w n="15.9">méduse</w>.</l>
						<l n="16" num="4.4"><w n="16.1">Et</w> <w n="16.2">tout</w> <w n="16.3">le</w> <w n="16.4">souvenir</w> <w n="16.5">se</w> <w n="16.6">colle</w> <w n="16.7">contre</w> <w n="16.8">moi</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Et</w> <w n="17.2">c</w>’<w n="17.3">est</w> <w n="17.4">lorsqu</w>’<w n="17.5">un</w> <w n="17.6">grand</w> <w n="17.7">cri</w> <w n="17.8">perce</w> <w n="17.9">les</w> <w n="17.10">étendues</w></l>
						<l n="18" num="5.2"><w n="18.1">Et</w> <w n="18.2">m</w>’<w n="18.3">atteint</w>, — <w n="18.4">moi</w> <w n="18.5">qui</w> <w n="18.6">sais</w> <w n="18.7">tout</w> <w n="18.8">le</w> <w n="18.9">secret</w> <w n="18.10">des</w> <w n="18.11">mers</w>.</l>
						<l n="19" num="5.3"><w n="19.1">Venu</w>, <w n="19.2">non</w> <w n="19.3">du</w> <w n="19.4">port</w> <w n="19.5">blanc</w> <w n="19.6">d</w>’<w n="19.7">où</w> <w n="19.8">partent</w> <w n="19.9">les</w> <w n="19.10">steamers</w>.</l>
						<l n="20" num="5.4"><w n="20.1">Mais</w> <w n="20.2">du</w> <w n="20.3">plus</w> <w n="20.4">désolé</w> <w n="20.5">de</w> <w n="20.6">mes</w> <w n="20.7">plages</w> <w n="20.8">perdues</w>…</l>
					</lg>
				</div></body></text></TEI>