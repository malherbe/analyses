<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FIGURE DE PROUE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2836 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/delaruemardruepoesies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">La Figure de Proue</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/stream/lafiguredeproue00dela/lafiguredeproue00dela_djvu.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>La Figure de Proue</title>
								<author>Lucie Delarue-Mardrus</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Fasquelle</publisher>
									<date when="1908">1908</date>
								</imprint>
							</monogr>
							<note>Source des poèmes ajoutés et édition de référence pour les corrections métriques.</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1908">1908</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
				<p>Des poèmes ont été ajoutés afin d’être conforme à l’édition de référence (source : archive.org)</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">EN KROUMIRIE</head><div type="poem" key="DLR390">
					<head type="main">MOMENT NOCTURNE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Nous</w> <w n="1.2">qui</w> <w n="1.3">ne</w> <w n="1.4">portons</w> <w n="1.5">point</w> <w n="1.6">le</w> <w n="1.7">joug</w> <w n="1.8">bas</w> <w n="1.9">des</w> <w n="1.10">aînés</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Qui</w> <w n="2.2">ne</w> <w n="2.3">connaissons</w> <w n="2.4">plus</w> <w n="2.5">dans</w> <w n="2.6">quel</w> <w n="2.7">monde</w> <w n="2.8">nous</w> <w n="2.9">sommes</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Nous</w> <w n="3.2">savons</w> <w n="3.3">la</w> <w n="3.4">splendeur</w> <w n="3.5">des</w> <w n="3.6">soirs</w> <w n="3.7">déracinés</w></l>
						<l n="4" num="1.4"><w n="4.1">Où</w> <w n="4.2">l</w>’<w n="4.3">on</w> <w n="4.4">est</w> <w n="4.5">seulement</w> <w n="4.6">des</w> <w n="4.7">femmes</w> <w n="4.8">et</w> <w n="4.9">des</w> <w n="4.10">hommes</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Nuits</w> <w n="5.2">d</w>’<w n="5.3">Afrique</w> ! <w n="5.4">Tenant</w> <w n="5.5">nos</w> <w n="5.6">nuques</w> <w n="5.7">dans</w> <w n="5.8">nos</w> <w n="5.9">mains</w>,</l>
						<l n="6" num="2.2"><w n="6.1">Nous</w> <w n="6.2">avons</w> <w n="6.3">respiré</w> <w n="6.4">Tété</w> <w n="6.5">comme</w> <w n="6.6">des</w> <w n="6.7">plantes</w>.</l>
						<l n="7" num="2.3"><w n="7.1">Alors</w> <w n="7.2">que</w>, <w n="7.3">sur</w> <w n="7.4">nos</w> <w n="7.5">yeux</w> <w n="7.6">restés</w> <w n="7.7">à</w> <w n="7.8">peine</w> <w n="7.9">humains</w>.</l>
						<l n="8" num="2.4"><w n="8.1">Le</w> <w n="8.2">ciel</w> <w n="8.3">laissait</w> <w n="8.4">tomber</w> <w n="8.5">ses</w> <w n="8.6">étoiles</w> <w n="8.7">filantes</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Ah</w> ! <w n="9.2">qui</w> <w n="9.3">saura</w> <w n="9.4">les</w> <w n="9.5">dieux</w> <w n="9.6">que</w> <w n="9.7">nous</w> <w n="9.8">avons</w> <w n="9.9">été</w></l>
						<l n="10" num="3.2"><w n="10.1">Quand</w> <w n="10.2">toute</w> <w n="10.3">la</w> <w n="10.4">forêt</w> <w n="10.5">craquait</w> <w n="10.6">comme</w> <w n="10.7">une</w> <w n="10.8">écorce</w></l>
						<l n="11" num="3.3"><w n="11.1">Et</w> <w n="11.2">qu</w>’<w n="11.3">animale</w> <w n="11.4">en</w> <w n="11.5">nous</w> <w n="11.6">s</w>’<w n="11.7">étirait</w> <w n="11.8">notre</w> <w n="11.9">force</w></l>
						<l n="12" num="3.4"><w n="12.1">Dans</w> <w n="12.2">un</w> <w n="12.3">instant</w> <w n="12.4">plus</w> <w n="12.5">grand</w> <w n="12.6">que</w> <w n="12.7">notre</w> <w n="12.8">éternité</w> ?</l>
					</lg>
				</div></body></text></TEI>