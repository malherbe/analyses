<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FIGURE DE PROUE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2836 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/delaruemardruepoesies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">La Figure de Proue</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/stream/lafiguredeproue00dela/lafiguredeproue00dela_djvu.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>La Figure de Proue</title>
								<author>Lucie Delarue-Mardrus</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Fasquelle</publisher>
									<date when="1908">1908</date>
								</imprint>
							</monogr>
							<note>Source des poèmes ajoutés et édition de référence pour les corrections métriques.</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1908">1908</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
				<p>Des poèmes ont été ajoutés afin d’être conforme à l’édition de référence (source : archive.org)</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">EN KROUMIRIE</head><div type="poem" key="DLR393">
					<head type="main">RÉVEILS</head>
					<div type="section" n="1">
						<head type="number">I</head>
						<head type="sub">VILLAGE</head>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">En</w> <w n="1.2">Afrique</w> <w n="1.3">mineure</w>, <w n="1.4">on</w> <w n="1.5">retrouve</w> <w n="1.6">au</w> <w n="1.7">passage</w></l>
							<l n="2" num="1.2"><w n="2.1">Un</w> <w n="2.2">bout</w> <w n="2.3">d</w>’<w n="2.4">Europe</w> <w n="2.5">au</w> <w n="2.6">flanc</w> <w n="2.7">d</w>’<w n="2.8">un</w> <w n="2.9">mont</w>, <w n="2.10">dans</w> <w n="2.11">un</w> <w n="2.12">village</w></l>
							<l n="3" num="1.3"><w n="3.1">Traversé</w>. <w n="3.2">C</w>’<w n="3.3">est</w>, <w n="3.4">au</w> <w n="3.5">vol</w>, <w n="3.6">le</w> <w n="3.7">réveil</w> <w n="3.8">chassieux</w></l>
							<l n="4" num="1.4"><w n="4.1">Des</w> <w n="4.2">êtres</w> <w n="4.3">dont</w> <w n="4.4">l</w>’<w n="4.5">aurore</w> <w n="4.6">ouvre</w> <w n="4.7">les</w> <w n="4.8">pauvres</w> <w n="4.9">yeux</w>.</l>
							<l n="5" num="1.5"><w n="5.1">C</w>’<w n="5.2">est</w>, <w n="5.3">au</w> <w n="5.4">sortir</w> <w n="5.5">de</w> <w n="5.6">la</w> <w n="5.7">ténèbre</w> <w n="5.8">et</w> <w n="5.9">du</w> <w n="5.10">silence</w>,</l>
							<l n="6" num="1.6"><w n="6.1">Le</w> <w n="6.2">bruit</w> <w n="6.3">et</w> <w n="6.4">la</w> <w n="6.5">couleur</w> <w n="6.6">du</w> <w n="6.7">jour</w> <w n="6.8">qui</w> <w n="6.9">recommencent</w>.</l>
							<l n="7" num="1.7"><w n="7.1">C</w>’<w n="7.2">est</w> <w n="7.3">un</w> <w n="7.4">homme</w> <w n="7.5">qui</w> <w n="7.6">baille</w> <w n="7.7">en</w> <w n="7.8">étirant</w> <w n="7.9">ses</w> <w n="7.10">bras</w></l>
							<l n="8" num="1.8"><w n="8.1">Sans</w> <w n="8.2">sourire</w>. <w n="8.3">C</w>’<w n="8.4">est</w> <w n="8.5">un</w> <w n="8.6">cheval</w> <w n="8.7">osseux</w> <w n="8.8">et</w> <w n="8.9">las</w> ;</l>
							<l n="9" num="1.9"><w n="9.1">On</w> <w n="9.2">lui</w> <w n="9.3">remet</w>, <w n="9.4">alors</w> <w n="9.5">que</w> <w n="9.6">clignotent</w> <w n="9.7">ses</w> <w n="9.8">taies</w>.</l>
							<l n="10" num="1.10"><w n="10.1">Son</w> <w n="10.2">collier</w> <w n="10.3">de</w> <w n="10.4">misère</w> <w n="10.5">au</w> <w n="10.6">creux</w> <w n="10.7">des</w> <w n="10.8">mêmes</w> <w n="10.9">plaies</w> ;</l>
							<l n="11" num="1.11"><w n="11.1">Et</w> <w n="11.2">c</w>’<w n="11.3">est</w> <w n="11.4">vivre</w>. <w n="11.5">Et</w> <w n="11.6">la</w> <w n="11.7">bête</w> <w n="11.8">est</w> <w n="11.9">triste</w> <w n="11.10">immensément</w>.</l>
							<l n="12" num="1.12"><w n="12.1">Autant</w> <w n="12.2">sans</w> <w n="12.3">doute</w>, <w n="12.4">ou</w> <w n="12.5">plus</w> <w n="12.6">encore</w> <w n="12.7">que</w> <w n="12.8">les</w> <w n="12.9">gens</w>…</l>
						</lg>
					</div>
					<div type="section" n="2">
						<head type="number">II</head>
						<head type="sub">MONTAGNE</head>
						<lg n="1">
							<l n="13" num="1.1"><w n="13.1">C</w>’<w n="13.2">est</w> <w n="13.3">le</w> <w n="13.4">réveil</w> <w n="13.5">de</w> <w n="13.6">la</w> <w n="13.7">montagne</w> <w n="13.8">sombre</w> <w n="13.9">et</w> <w n="13.10">claire</w></l>
							<l n="14" num="1.2"><w n="14.1">Qui</w> <w n="14.2">garde</w> <w n="14.3">encor</w> <w n="14.4">la</w> <w n="14.5">nuit</w> <w n="14.6">sur</w> <w n="14.7">un</w> <w n="14.8">de</w> <w n="14.9">ses</w> <w n="14.10">côtés</w>.</l>
						</lg>
						<lg n="2">
							<l n="15" num="2.1"><w n="15.1">Elle</w> <w n="15.2">reprend</w> <w n="15.3">sa</w> <w n="15.4">bienheureuse</w> <w n="15.5">éternité</w>,</l>
							<l n="16" num="2.2"><w n="16.1">Massivement</w>, <w n="16.2">face</w> <w n="16.3">à</w> <w n="16.4">l</w>’<w n="16.5">aurore</w> <w n="16.6">millénaire</w>.</l>
						</lg>
						<lg n="3">
							<l n="17" num="3.1"><w n="17.1">Et</w>, <w n="17.2">pour</w> <w n="17.3">être</w> <w n="17.4">pareille</w> <w n="17.5">au</w> <w n="17.6">bel</w> <w n="17.7">Arabe</w> <w n="17.8">lent</w></l>
							<l n="18" num="3.2"><w n="18.1">Qui</w> <w n="18.2">se</w> <w n="18.3">lève</w> <w n="18.4">dans</w> <w n="18.5">son</w> <w n="18.6">manteau</w> <w n="18.7">fatal</w> <w n="18.8">et</w> <w n="18.9">blanc</w>,</l>
						</lg>
						<lg n="4">
							<l n="19" num="4.1"><w n="19.1">Elle</w> <w n="19.2">écarte</w> <w n="19.3">la</w> <w n="19.4">brume</w> <w n="19.5">et</w> <w n="19.6">sort</w> <w n="19.7">de</w> <w n="19.8">ses</w> <w n="19.9">nuages</w>,</l>
							<l n="20" num="4.2"><w n="20.1">Et</w> <w n="20.2">crève</w> <w n="20.3">le</w> <w n="20.4">ciel</w> <w n="20.5">rose</w> <w n="20.6">avec</w> <w n="20.7">son</w> <w n="20.8">grand</w> <w n="20.9">visage</w> ;</l>
						</lg>
					</div>
				</div></body></text></TEI>