<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FIGURE DE PROUE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2836 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/delaruemardruepoesies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">La Figure de Proue</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/stream/lafiguredeproue00dela/lafiguredeproue00dela_djvu.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>La Figure de Proue</title>
								<author>Lucie Delarue-Mardrus</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Fasquelle</publisher>
									<date when="1908">1908</date>
								</imprint>
							</monogr>
							<note>Source des poèmes ajoutés et édition de référence pour les corrections métriques.</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1908">1908</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
				<p>Des poèmes ont été ajoutés afin d’être conforme à l’édition de référence (source : archive.org)</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PREMIER ISLAM</head><div type="poem" key="DLR362">
					<head type="main">UTIQUE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Le</w> <w n="1.2">bonheur</w> <w n="1.3">monotone</w> <w n="1.4">et</w> <w n="1.5">grave</w> <w n="1.6">de</w> <w n="1.7">l</w>’<w n="1.8">espace</w></l>
						<l n="2" num="1.2"><w n="2.1">Nous</w> <w n="2.2">laissa</w> <w n="2.3">souvent</w> <w n="2.4">seuls</w> <w n="2.5">avec</w> <w n="2.6">les</w> <w n="2.7">horizons</w></l>
						<l n="3" num="1.3"><w n="3.1">Où</w> <w n="3.2">rôdait</w> <w n="3.3">au</w> <w n="3.4">couchant</w> <w n="3.5">notre</w> <w n="3.6">âme</w> <w n="3.7">jamais</w> <w n="3.8">lasse</w></l>
						<l n="4" num="1.4"><w n="4.1">De</w> <w n="4.2">voirie</w> <w n="4.3">beau</w> <w n="4.4">soleil</w> <w n="4.5">sombrer</w> <w n="4.6">dans</w> <w n="4.7">les</w> <w n="4.8">moissons</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Le</w> <w n="5.2">soir</w> <w n="5.3">nous</w> <w n="5.4">attirait</w> <w n="5.5">vers</w> <w n="5.6">les</w> <w n="5.7">plaines</w> <w n="5.8">d</w>’<w n="5.9">Utique</w></l>
						<l n="6" num="2.2"><w n="6.1">Où</w> <w n="6.2">les</w> <w n="6.3">blés</w> <w n="6.4">infinis</w> <w n="6.5">se</w> <w n="6.6">mouraient</w> <w n="6.7">de</w> <w n="6.8">chaleur</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Où</w>, <w n="7.2">le</w> <w n="7.3">long</w> <w n="7.4">des</w> <w n="7.5">sentiers</w>, <w n="7.6">le</w> <w n="7.7">sol</w> <w n="7.8">trois</w> <w n="7.9">fois</w> <w n="7.10">antique</w></l>
						<l n="8" num="2.4"><w n="8.1">Ne</w> <w n="8.2">nourrissait</w> <w n="8.3">plus</w> <w n="8.4">rien</w> <w n="8.5">que</w> <w n="8.6">des</w> <w n="8.7">chardons</w> <w n="8.8">en</w> <w n="8.9">fleur</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Et</w>, <w n="9.2">quand</w> <w n="9.3">la</w> <w n="9.4">nuit</w> <w n="9.5">subite</w> <w n="9.6">avait</w> <w n="9.7">éteint</w> <w n="9.8">la</w> <w n="9.9">plaine</w>.</l>
						<l n="10" num="3.2"><w n="10.1">En</w> <w n="10.2">rentrant</w> <w n="10.3">on</w> <w n="10.4">voyait</w> <w n="10.5">dans</w> <w n="10.6">le</w> <w n="10.7">faux</w> <w n="10.8">poivrier</w></l>
						<l n="11" num="3.3"><w n="11.1">Qui</w> <w n="11.2">longe</w> <w n="11.3">la</w> <w n="11.4">maison</w> <w n="11.5">solitaire</w>, <w n="11.6">briller</w></l>
						<l n="12" num="3.4"><w n="12.1">En</w> <w n="12.2">face</w> <w n="12.3">du</w> <w n="12.4">couchant</w> <w n="12.5">fini</w>, <w n="12.6">la</w> <w n="12.7">lune</w> <w n="12.8">pleine</w>…</l>
					</lg>
				</div></body></text></TEI>