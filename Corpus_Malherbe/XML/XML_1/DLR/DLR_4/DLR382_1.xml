<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FIGURE DE PROUE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2836 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/delaruemardruepoesies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">La Figure de Proue</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/stream/lafiguredeproue00dela/lafiguredeproue00dela_djvu.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>La Figure de Proue</title>
								<author>Lucie Delarue-Mardrus</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Fasquelle</publisher>
									<date when="1908">1908</date>
								</imprint>
							</monogr>
							<note>Source des poèmes ajoutés et édition de référence pour les corrections métriques.</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1908">1908</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
				<p>Des poèmes ont été ajoutés afin d’être conforme à l’édition de référence (source : archive.org)</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">BARBARESQUES</head><div type="poem" key="DLR382">
					<head type="main">A LA LOUANGE DES PORTS DE MER</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Vous</w> <w n="1.2">vivez</w> <w n="1.3">en</w> <w n="1.4">mon</w> <w n="1.5">cœur</w>, <w n="1.6">ports</w> <w n="1.7">de</w> <w n="1.8">mer</w>, <w n="1.9">ports</w> <w n="1.10">de</w> <w n="1.11">mer</w></l>
						<l n="2" num="1.2"><w n="2.1">Arrondis</w> <w n="2.2">et</w> <w n="2.3">calmés</w> <w n="2.4">devant</w> <w n="2.5">le</w> <w n="2.6">large</w> <w n="2.7">amer</w>.</l>
					</lg>
					<lg n="2">
						<l n="3" num="2.1"><w n="3.1">Autour</w> <w n="3.2">des</w> <w n="3.3">paquebots</w> <w n="3.4">arrêtés</w> <w n="3.5">sur</w> <w n="3.6">leurs</w> <w n="3.7">quilles</w>,</l>
						<l n="4" num="2.2"><w n="4.1">Cette</w> <w n="4.2">odeur</w> <w n="4.3">de</w> <w n="4.4">goudron</w>, <w n="4.5">d</w>’<w n="4.6">ordure</w> <w n="4.7">et</w> <w n="4.8">de</w> <w n="4.9">coquilles</w>,</l>
					</lg>
					<lg n="3">
						<l n="5" num="3.1"><w n="5.1">Cette</w> <w n="5.2">odeur</w> <w n="5.3">rude</w> <w n="5.4">du</w> <w n="5.5">départ</w> <w n="5.6">et</w> <w n="5.7">du</w> <w n="5.8">retour</w>,</l>
						<l n="6" num="3.2"><w n="6.1">Je</w> <w n="6.2">la</w> <w n="6.3">respire</w>, <w n="6.4">sur</w> <w n="6.5">vos</w> <w n="6.6">quais</w>, <w n="6.7">avec</w> <w n="6.8">amour</w>.</l>
					</lg>
					<lg n="4">
						<l n="7" num="4.1"><w n="7.1">J</w>’<w n="7.2">aime</w> <w n="7.3">le</w> <w n="7.4">clapotis</w> <w n="7.5">qui</w> <w n="7.6">berce</w> <w n="7.7">et</w> <w n="7.8">qui</w> <w n="7.9">soulève</w></l>
						<l n="8" num="4.2"><w n="8.1">En</w> <w n="8.2">vous</w>, <w n="8.3">tant</w> <w n="8.4">de</w> <w n="8.5">reflets</w>, <w n="8.6">de</w> <w n="8.7">commerce</w> <w n="8.8">et</w> <w n="8.9">de</w> <w n="8.10">rêve</w>,</l>
					</lg>
					<lg n="5">
						<l n="9" num="5.1"><w n="9.1">Et</w> <w n="9.2">l</w>’<w n="9.3">esprit</w> <w n="9.4">du</w> <w n="9.5">voyage</w> <w n="9.6">erre</w> <w n="9.7">à</w> <w n="9.8">travers</w> <w n="9.9">vos</w> <w n="9.10">mâts</w></l>
						<l n="10" num="5.2"><w n="10.1">Dont</w> <w n="10.2">craquent</w> <w n="10.3">doucement</w> <w n="10.4">les</w> <w n="10.5">sous</w>-<w n="10.6">bois</w> <w n="10.7">délicats</w>.</l>
					</lg>
					<lg n="6">
						<l n="11" num="6.1"><w n="11.1">Beaux</w> <w n="11.2">ports</w>, <w n="11.3">beaux</w> <w n="11.4">ports</w> <w n="11.5">de</w> <w n="11.6">mer</w> <w n="11.7">de</w> <w n="11.8">mes</w> <w n="11.9">villes</w> <w n="11.10">diverses</w></l>
						<l n="12" num="6.2"><w n="12.1">Dans</w> <w n="12.2">le</w> <w n="12.3">bleu</w> <w n="12.4">méridional</w> <w n="12.5">ou</w> <w n="12.6">les</w> <w n="12.7">averses</w>,</l>
					</lg>
					<lg n="7">
						<l n="13" num="7.1"><w n="13.1">Beaux</w> <w n="13.2">ports</w> <w n="13.3">où</w> <w n="13.4">Ton</w> <w n="13.5">peut</w> <w n="13.6">voir</w> <w n="13.7">se</w> <w n="13.8">balancer</w> <w n="13.9">de</w> <w n="13.10">près</w></l>
						<l n="14" num="7.2"><w n="14.1">Le</w> <w n="14.2">soleil</w> <w n="14.3">pris</w>, <w n="14.4">le</w> <w n="14.5">soir</w>, <w n="14.6">dans</w> <w n="14.7">le</w> <w n="14.8">haut</w> <w n="14.9">des</w> <w n="14.10">agrès</w>,</l>
					</lg>
					<lg n="8">
						<l n="15" num="8.1"><w n="15.1">Je</w> <w n="15.2">vous</w> <w n="15.3">chéris</w> <w n="15.4">du</w> <w n="15.5">fond</w> <w n="15.6">de</w> <w n="15.7">ma</w> <w n="15.8">première</w> <w n="15.9">enfance</w></l>
						<l n="16" num="8.2"><w n="16.1">Qui</w> <w n="16.2">devinait</w> <w n="16.3">déjà</w> <w n="16.4">la</w> <w n="16.5">joie</w> <w n="16.6">et</w> <w n="16.7">la</w> <w n="16.8">souffrance</w>.</l>
					</lg>
					<lg n="9">
						<l n="17" num="9.1"><w n="17.1">Les</w> <w n="17.2">barques</w> <w n="17.3">de</w> <w n="17.4">Honfleur</w> <w n="17.5">qui</w> <w n="17.6">se</w> <w n="17.7">marquent</w> <subst hand="RR" reason="analysis" type="phonemization"><add rend="hidden"><w n="17.8">ach</w></add><del>H</del></subst>. <w n="17.9">O</w>.</l>
						<l n="18" num="9.2"><w n="18.1">Partaient</w> <w n="18.2">sans</w> <w n="18.3">bruit</w> <w n="18.4">à</w> <w n="18.5">l</w>’<w n="18.6">heure</w> <w n="18.7">où</w> <w n="18.8">la</w> <w n="18.9">mer</w> <w n="18.10">monte</w> <w n="18.11">haut</w>.</l>
					</lg>
					<lg n="10">
						<l n="19" num="10.1"><w n="19.1">Elles</w> <w n="19.2">partaient</w> <w n="19.3">vers</w> <w n="19.4">l</w>’<w n="19.5">inconnu</w> <w n="19.6">qui</w> <w n="19.7">tente</w> <w n="19.8">et</w> <w n="19.9">brille</w>.</l>
						<l n="20" num="10.2"><w n="20.1">Avec</w> <w n="20.2">l</w>’<w n="20.3">obscur</w> <w n="20.4">désir</w> <w n="20.5">d</w>’<w n="20.6">une</w> <w n="20.7">petite</w> <w n="20.8">fille</w>,</l>
					</lg>
					<lg n="11">
						<l n="21" num="11.1"><w n="21.1">Alors</w> <w n="21.2">que</w> <w n="21.3">j</w>’<w n="21.4">ignorais</w> <w n="21.5">encor</w> <w n="21.6">que</w> <w n="21.7">mon</w> <w n="21.8">destin</w></l>
						<l n="22" num="11.2"><w n="22.1">Me</w> <w n="22.2">donnerait</w> <w n="22.3">la</w> <w n="22.4">mer</w>, <w n="22.5">le</w> <w n="22.6">risque</w> <w n="22.7">et</w> <w n="22.8">le</w> <w n="22.9">butin</w>.</l>
					</lg>
					<lg n="12">
						<l n="23" num="12.1"><w n="23.1">Or</w>, <w n="23.2">puisque</w> <w n="23.3">maintenant</w> <w n="23.4">se</w> <w n="23.5">gonfle</w> <w n="23.6">ma</w> <w n="23.7">poitrine</w></l>
						<l n="24" num="12.2"><w n="24.1">De</w> <w n="24.2">grand</w> <w n="24.3">enthousiasme</w> <w n="24.4">et</w> <w n="24.5">de</w> <w n="24.6">brise</w> <w n="24.7">marine</w>.</l>
					</lg>
					<lg n="13">
						<l n="25" num="13.1"><w n="25.1">Salut</w> <w n="25.2">à</w> <w n="25.3">vous</w> ! <w n="25.4">J</w>’<w n="25.5">ai</w> <w n="25.6">pris</w> <w n="25.7">aussi</w> <w n="25.8">mon</w> <w n="25.9">large</w> <w n="25.10">vol</w></l>
						<l n="26" num="13.2"><w n="26.1">Devers</w> <w n="26.2">un</w> <w n="26.3">autre</w> <w n="26.4">ciel</w>, <w n="26.5">devers</w> <w n="26.6">un</w> <w n="26.7">autre</w> <w n="26.8">sol</w>,</l>
					</lg>
					<lg n="14">
						<l n="27" num="14.1"><w n="27.1">O</w> <w n="27.2">vous</w> <w n="27.3">qui</w> <w n="27.4">m</w>’<w n="27.5">accueillez</w> <w n="27.6">au</w> <w n="27.7">bout</w> <w n="27.8">de</w> <w n="27.9">tout</w> <w n="27.10">voyage</w>,</l>
						<l n="28" num="14.2"><w n="28.1">Beaux</w> <w n="28.2">ports</w>, <w n="28.3">beaux</w> <w n="28.4">ports</w> <w n="28.5">de</w> <w n="28.6">mon</w> <w n="28.7">bonheur</w> <w n="28.8">et</w> <w n="28.9">de</w> <w n="28.10">mon</w> <w n="28.11">âge</w></l>
					</lg>
				</div></body></text></TEI>