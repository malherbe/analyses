<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FIGURE DE PROUE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2836 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/delaruemardruepoesies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">La Figure de Proue</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/stream/lafiguredeproue00dela/lafiguredeproue00dela_djvu.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>La Figure de Proue</title>
								<author>Lucie Delarue-Mardrus</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Fasquelle</publisher>
									<date when="1908">1908</date>
								</imprint>
							</monogr>
							<note>Source des poèmes ajoutés et édition de référence pour les corrections métriques.</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1908">1908</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
				<p>Des poèmes ont été ajoutés afin d’être conforme à l’édition de référence (source : archive.org)</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PREMIER ISLAM</head><div type="poem" key="DLR364">
					<head type="main">MALARIA</head>
					<div type="section" n="1">
						<head type="number">I</head>
							<lg n="1">
							<l n="1" num="1.1"><w n="1.1">Situ</w> <w n="1.2">veux</w> <w n="1.3">bien</w>, <w n="1.4">rentrons</w>. <w n="1.5">Il</w> <w n="1.6">ne</w> <w n="1.7">fait</w> <w n="1.8">plus</w> <w n="1.9">très</w> <w n="1.10">clair</w>.</l>
							<l n="2" num="1.2"><w n="2.1">Le</w> <w n="2.2">soleil</w> <w n="2.3">dans</w> <w n="2.4">les</w> <w n="2.5">blés</w> <w n="2.6">meurt</w> <w n="2.7">comme</w> <w n="2.8">dans</w> <w n="2.9">la</w> <w n="2.10">mer</w>.</l>
							<l n="3" num="1.3"><w n="3.1">Vers</w> <w n="3.2">la</w> <w n="3.3">source</w> <w n="3.4">d</w>’<w n="3.5">eau</w> <w n="3.6">chaude</w> <w n="3.7">où</w> <w n="3.8">des</w> <w n="3.9">palmes</w> <w n="3.10">se</w> <w n="3.11">mouillent</w>,</l>
							<l n="4" num="1.4"><w n="4.1">La</w> <w n="4.2">terre</w> <w n="4.3">est</w> <w n="4.4">craquelée</w> <w n="4.5">et</w> <w n="4.6">grouille</w> <w n="4.7">de</w> <w n="4.8">grenouilles</w>.</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1"><w n="5.1">La</w> <w n="5.2">plaine</w> <w n="5.3">serait</w>-<w n="5.4">elle</w> <w n="5.5">un</w> <w n="5.6">marais</w> <w n="5.7">desséché</w> ?</l>
							<l n="6" num="2.2"><w n="6.1">Quel</w> <w n="6.2">est</w> <w n="6.3">l</w>’<w n="6.4">instinct</w> <w n="6.5">qui</w> <w n="6.6">fait</w> <w n="6.7">que</w> <w n="6.8">nous</w> <w n="6.9">serrons</w> <w n="6.10">les</w> <w n="6.11">lèvres</w> ?</l>
							<l n="7" num="2.3"><w n="7.1">Ah</w> ! <w n="7.2">j</w>’<w n="7.3">ai</w> <w n="7.4">peur</w> ! <w n="7.5">Pourquoi</w> <w n="7.6">donc</w> <w n="7.7">tremblons</w>-<w n="7.8">nous</w>, <w n="7.9">si</w> <w n="7.10">la</w> <w n="7.11">Fièvre</w></l>
							<l n="8" num="2.4"><w n="8.1">Ne</w> <w n="8.2">rampe</w> <w n="8.3">pas</w> <w n="8.4">vers</w> <w n="8.5">nous</w> <w n="8.6">comme</w> <w n="8.7">un</w> <w n="8.8">monstre</w> <w n="8.9">caché</w> ?…</l>
						</lg>
					</div>
					<div type="section" n="2">
						<head type="number">II</head>
						<lg n="1">
							<l n="9" num="1.1"><w n="9.1">Enfermez</w> <w n="9.2">les</w> <w n="9.3">enfants</w>, <w n="9.4">voici</w> <w n="9.5">le</w> <w n="9.6">crépuscule</w>…</l>
							<l n="10" num="1.2"><w n="10.1">Dans</w> <w n="10.2">les</w> <w n="10.3">blés</w>, <w n="10.4">le</w> <w n="10.5">soleil</w> <w n="10.6">est</w> <w n="10.7">presque</w> <w n="10.8">trépassé</w>.</l>
							<l n="11" num="1.3"><w n="11.1">Un</w> <w n="11.2">monstre</w> <w n="11.3">doucereux</w> <w n="11.4">sort</w> <w n="11.5">du</w> <w n="11.6">sol</w> <w n="11.7">crevassé</w> :</l>
							<l n="12" num="1.4"><w n="12.1">C</w>’<w n="12.2">est</w> <w n="12.3">l</w>’<w n="12.4">heure</w>… <w n="12.5">Sentez</w>-<w n="12.6">vous</w> <w n="12.7">la</w> <w n="12.8">fièvre</w> <w n="12.9">qui</w> <w n="12.10">circule</w> ?</l>
							<l n="13" num="1.5">— <w n="13.1">Enfermez</w> <w n="13.2">les</w> <w n="13.3">enfants</w>, <w n="13.4">voici</w> <w n="13.5">le</w> <w n="13.6">crépuscule</w>.</l>
						</lg>
						<lg n="2">
							<l n="14" num="2.1"><w n="14.1">Les</w> <w n="14.2">enfants</w> <w n="14.3">resteront</w> <w n="14.4">derrière</w> <w n="14.5">les</w> <w n="14.6">carreaux</w></l>
							<l n="15" num="2.2"><w n="15.1">A</w> <w n="15.2">regarder</w> <w n="15.3">de</w> <w n="15.4">leurs</w> <w n="15.5">grands</w> <w n="15.6">yeux</w>, <w n="15.7">brûler</w> <w n="15.8">la</w> <w n="15.9">plaine</w>.</l>
							<l n="16" num="2.3"><w n="16.1">Fermez</w> <w n="16.2">tout</w> ! <w n="16.3">L</w>’<w n="16.4">été</w> <w n="16.5">souffle</w> <w n="16.6">une</w> <w n="16.7">terrible</w> <w n="16.8">haleine</w> !</l>
							<l n="17" num="2.4">— <w n="17.1">Sans</w> <w n="17.2">jouer</w>, <w n="17.3">sans</w> <w n="17.4">parler</w>, <w n="17.5">menacés</w> <w n="17.6">dans</w> <w n="17.7">leurs</w> <w n="17.8">os</w>.</l>
							<l n="18" num="2.5"><w n="18.1">Les</w> <w n="18.2">enfants</w> <w n="18.3">resteront</w> <w n="18.4">derrière</w> <w n="18.5">les</w> <w n="18.6">carreaux</w>…</l>
						</lg>
					</div>
				</div></body></text></TEI>