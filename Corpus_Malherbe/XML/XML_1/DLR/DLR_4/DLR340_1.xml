<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FIGURE DE PROUE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2836 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/delaruemardruepoesies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">La Figure de Proue</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/stream/lafiguredeproue00dela/lafiguredeproue00dela_djvu.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>La Figure de Proue</title>
								<author>Lucie Delarue-Mardrus</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Fasquelle</publisher>
									<date when="1908">1908</date>
								</imprint>
							</monogr>
							<note>Source des poèmes ajoutés et édition de référence pour les corrections métriques.</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1908">1908</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
				<p>Des poèmes ont été ajoutés afin d’être conforme à l’édition de référence (source : archive.org)</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PREMIER ISLAM</head><div type="poem" key="DLR340">
					<head type="main">ÉGYPTIENNE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Dans</w> <w n="1.2">le</w> <w n="1.3">luth</w>, <w n="1.4">dans</w> <w n="1.5">les</w> <w n="1.6">coups</w> <w n="1.7">de</w> <w n="1.8">la</w> <w n="1.9">darabouka</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Dans</w> <w n="2.2">le</w> <w n="2.3">chalumeau</w> <w n="2.4">peint</w>, <w n="2.5">criard</w> <w n="2.6">et</w> <w n="2.7">ineffable</w></l>
						<l n="3" num="1.3"><w n="3.1">Rythmant</w> <w n="3.2">à</w> <w n="3.3">contretemps</w> <w n="3.4">tout</w> <w n="3.5">le</w> <w n="3.6">pays</w> <w n="3.7">arabe</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Revit</w> <w n="4.2">pour</w> <w n="4.3">moi</w> <w n="4.4">la</w> <w n="4.5">mémoire</w> <w n="4.6">de</w> <w n="4.7">Wassila</w>,</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">De</w> <w n="5.2">sa</w> <w n="5.3">face</w> <w n="5.4">d</w>’<w n="5.5">Égypte</w> <w n="5.6">inspirée</w> <w n="5.7">et</w> <w n="5.8">foncée</w>,</l>
						<l n="6" num="2.2"><w n="6.1">Qui</w> <w n="6.2">véhémentement</w> <w n="6.3">se</w> <w n="6.4">détournait</w> <w n="6.5">de</w> <w n="6.6">nous</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Lorsque</w>, <w n="7.2">le</w> <w n="7.3">cœur</w> <w n="7.4">battant</w>, <w n="7.5">les</w> <w n="7.6">paupières</w> <w n="7.7">baissées</w>,</l>
						<l n="8" num="2.4"><w n="8.1">Elle</w>-<w n="8.2">même</w> <w n="8.3">souffrait</w> <w n="8.4">de</w> <w n="8.5">son</w> <w n="8.6">chant</w> <w n="8.7">rauque</w> <w n="8.8">et</w> <w n="8.9">doux</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Contre</w> <w n="9.2">son</w> <w n="9.3">luth</w> <w n="9.4">profond</w>, <w n="9.5">la</w> <w n="9.6">revoir</w> <w n="9.7">comme</w> <w n="9.8">morte</w></l>
						<l n="10" num="3.2"><w n="10.1">D</w>’<w n="10.2">avoir</w> <w n="10.3">trop</w> <w n="10.4">sangloté</w> <w n="10.5">ce</w> <w n="10.6">monotone</w> <w n="10.7">amour</w></l>
						<l n="11" num="3.3"><w n="11.1">Qui</w> <w n="11.2">passait</w> <w n="11.3">dans</w> <w n="11.4">mon</w> <w n="11.5">âme</w> <w n="11.6">étrangère</w>, <w n="11.7">plus</w> <w n="11.8">sourd</w>,</l>
						<l n="12" num="3.4"><w n="12.1">Plus</w> <w n="12.2">triste</w> <w n="12.3">et</w> <w n="12.4">plus</w> <w n="12.5">obscur</w> <w n="12.6">que</w> <w n="12.7">le</w> <w n="12.8">vent</w> <w n="12.9">dans</w> <w n="12.10">les</w> <w n="12.11">portes</w> !…</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">J</w>’<w n="13.2">avais</w> <w n="13.3">sans</w> <w n="13.4">le</w> <w n="13.5">savoir</w> <w n="13.6">un</w> <w n="13.7">peu</w> <w n="13.8">de</w> <w n="13.9">passion</w></l>
						<l n="14" num="4.2"><w n="14.1">Pour</w> <w n="14.2">ton</w> <w n="14.3">profil</w> <w n="14.4">à</w> <w n="14.5">cheveux</w> <w n="14.6">courts</w> <w n="14.7">de</w> <w n="14.8">Pharaon</w>,</l>
						<l n="15" num="4.3"><w n="15.1">Ton</w> <w n="15.2">sombre</w> <w n="15.3">contralto</w>, <w n="15.4">tes</w> <w n="15.5">lèvres</w> <w n="15.6">violettes</w>…</l>
						<l n="16" num="4.4"><w n="16.1">Et</w> <w n="16.2">maintenant</w>, <w n="16.3">ton</w> <w n="16.4">visage</w> <w n="16.5">lointain</w>, <w n="16.6">ton</w> <w n="16.7">nom</w>,</l>
						<l n="17" num="4.5"><w n="17.1">Ta</w> <w n="17.2">voix</w>, <w n="17.3">sont</w> <w n="17.4">sur</w> <w n="17.5">mon</w> <w n="17.6">cœur</w> <w n="17.7">comme</w> <w n="17.8">des</w> <w n="17.9">amulettes</w>.</l>
					</lg>
				</div></body></text></TEI>