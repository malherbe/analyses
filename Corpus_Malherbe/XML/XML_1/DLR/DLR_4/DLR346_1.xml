<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FIGURE DE PROUE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2836 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/delaruemardruepoesies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">La Figure de Proue</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/stream/lafiguredeproue00dela/lafiguredeproue00dela_djvu.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>La Figure de Proue</title>
								<author>Lucie Delarue-Mardrus</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Fasquelle</publisher>
									<date when="1908">1908</date>
								</imprint>
							</monogr>
							<note>Source des poèmes ajoutés et édition de référence pour les corrections métriques.</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1908">1908</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
				<p>Des poèmes ont été ajoutés afin d’être conforme à l’édition de référence (source : archive.org)</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PREMIER ISLAM</head><div type="poem" key="DLR346">
					<head type="main">ENSEIGNEMENT</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Aujourd</w>’<w n="1.2">hui</w>, <w n="1.3">sur</w> <w n="1.4">le</w> <w n="1.5">bord</w> <w n="1.6">de</w> <w n="1.7">la</w> <w n="1.8">mer</w> <w n="1.9">dans</w> <w n="1.10">marée</w></l>
						<l n="2" num="1.2"><w n="2.1">Et</w> <w n="2.2">si</w> <w n="2.3">claire</w> <w n="2.4">au</w> <w n="2.5">soleil</w> <w n="2.6">qu</w>’<w n="2.7">on</w> <w n="2.8">la</w> <w n="2.9">voit</w> <w n="2.10">jusqu</w>’<w n="2.11">au</w> <w n="2.12">cœur</w>,</l>
						<l n="3" num="1.3"><w n="3.1">J</w>’<w n="3.2">adore</w> <w n="3.3">en</w> <w n="3.4">mon</w> <w n="3.5">esprit</w> <w n="3.6">Sapho</w> <w n="3.7">désespérée</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Qui</w>, <w n="4.2">lasse</w>, <w n="4.3">y</w> <w n="4.4">abîma</w> <w n="4.5">sa</w> <w n="4.6">joie</w> <w n="4.7">et</w> <w n="4.8">sa</w> <w n="4.9">douleur</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Écoutant</w> <w n="5.2">jusqu</w>’<w n="5.3">à</w> <w n="5.4">moi</w> <w n="5.5">gronder</w> <w n="5.6">l</w>’<w n="5.7">ode</w> <w n="5.8">éternelle</w></l>
						<l n="6" num="2.2"><w n="6.1">De</w> <w n="6.2">l</w>’<w n="6.3">eau</w> <w n="6.4">bleue</w> <w n="6.5">où</w> <w n="6.6">tous</w> <w n="6.7">les</w> <w n="6.8">tourments</w> <w n="6.9">sont</w> <w n="6.10">confondus</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Je</w> <w n="7.2">crois</w> <w n="7.3">que</w> <w n="7.4">cette</w> <w n="7.5">mer</w> <w n="7.6">m</w>’<w n="7.7">apprend</w> <w n="7.8">les</w> <w n="7.9">chants</w> <w n="7.10">perdus</w></l>
						<l n="8" num="2.4"><w n="8.1">De</w> <w n="8.2">la</w> <w n="8.3">lyre</w> <w n="8.4">saphique</w> <w n="8.5">encor</w> <w n="8.6">vivante</w> <w n="8.7">en</w> <w n="8.8">elle</w>…</l>
					</lg>
				</div></body></text></TEI>