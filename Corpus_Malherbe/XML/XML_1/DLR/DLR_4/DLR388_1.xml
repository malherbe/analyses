<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FIGURE DE PROUE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2836 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/delaruemardruepoesies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">La Figure de Proue</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/stream/lafiguredeproue00dela/lafiguredeproue00dela_djvu.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>La Figure de Proue</title>
								<author>Lucie Delarue-Mardrus</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Fasquelle</publisher>
									<date when="1908">1908</date>
								</imprint>
							</monogr>
							<note>Source des poèmes ajoutés et édition de référence pour les corrections métriques.</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1908">1908</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
				<p>Des poèmes ont été ajoutés afin d’être conforme à l’édition de référence (source : archive.org)</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">EN KROUMIRIE</head><div type="poem" key="DLR388">
					<head type="main">RENCONTRE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">L</w>’<w n="1.2">aventure</w> <w n="1.3">à</w> <w n="1.4">travers</w> <w n="1.5">les</w> <w n="1.6">pays</w> <w n="1.7">parcourus</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Les</w> <w n="2.2">plaines</w> <w n="2.3">sans</w> <w n="2.4">verdure</w> <w n="2.5">où</w>, <w n="2.6">nue</w> <w n="2.7">et</w> <w n="2.8">torse</w>, <w n="2.9">brille</w></l>
						<l n="3" num="1.3"><w n="3.1">La</w> <w n="3.2">Medjerda</w>, <w n="3.3">comme</w> <w n="3.4">une</w> <w n="3.5">anguille</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Dans</w> <w n="4.2">la</w> <w n="4.3">douleur</w> <w n="4.4">du</w> <w n="4.5">soleil</w> <w n="4.6">cru</w> ;</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Les</w> <w n="5.2">jardins</w> <w n="5.3">des</w> <w n="5.4">villes</w> <w n="5.5">arabes</w></l>
						<l n="6" num="2.2"><w n="6.1">Et</w> <w n="6.2">d</w>’<w n="6.3">autres</w> ; <w n="6.4">les</w> <w n="6.5">cités</w> <w n="6.6">en</w> <w n="6.7">ruine</w> <w n="6.8">ou</w> <w n="6.9">debout</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Carthage</w> <w n="7.2">trois</w> <w n="7.3">fois</w> <w n="7.4">morte</w> <w n="7.5">où</w> <w n="7.6">l</w>’<w n="7.7">orge</w> <w n="7.8">garde</w> <w n="7.9">un</w> <w n="7.10">goût</w></l>
						<l n="8" num="2.4"><w n="8.1">Des</w> <w n="8.2">cendres</w> <w n="8.3">immémoriales</w> ;</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">La</w> <w n="9.2">forêt</w> <w n="9.3">des</w> <w n="9.4">pays</w> <w n="9.5">Kroumirs</w></l>
						<l n="10" num="3.2"><w n="10.1">Où</w> <w n="10.2">nous</w> <w n="10.3">galopions</w>, <w n="10.4">une</w> <w n="10.5">rose</w> <w n="10.6">à</w> <w n="10.7">l</w>’<w n="10.8">oreille</w>.</l>
						<l n="11" num="3.3"><w n="11.1">Sur</w> <w n="11.2">nos</w> <w n="11.3">belles</w> <w n="11.4">mules</w> <w n="11.5">pareilles</w>.</l>
						<l n="12" num="3.4"><w n="12.1">Nous</w> <w n="12.2">délivrant</w> <w n="12.3">de</w> <w n="12.4">tout</w>, <w n="12.5">même</w> <w n="12.6">du</w> <w n="12.7">souvenir</w> ;</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">L</w>’<w n="13.2">aurore</w> <w n="13.3">couleur</w> <w n="13.4">d</w>’<w n="13.5">abricot</w>,</l>
						<l n="14" num="4.2"><w n="14.1">Le</w> <w n="14.2">midi</w>, <w n="14.3">le</w> <w n="14.4">couchant</w>, <w n="14.5">la</w> <w n="14.6">lune</w> <w n="14.7">ronde</w> <w n="14.8">et</w> <w n="14.9">haute</w></l>
						<l n="15" num="4.3"><w n="15.1">Sur</w> <w n="15.2">ces</w> <w n="15.3">forêts</w> <w n="15.4">où</w>, <w n="15.5">côte</w> <w n="15.6">à</w> <w n="15.7">côte</w>.</l>
						<l n="16" num="4.4"><w n="16.1">Nous</w> <w n="16.2">vivions</w> <w n="16.3">glorieux</w>, <w n="16.4">seuls</w> <w n="16.5">avec</w> <w n="16.6">notre</w> <w n="16.7">écho</w> ;</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Les</w> <w n="17.2">soirs</w> <w n="17.3">d</w>’<w n="17.4">immense</w> <w n="17.5">rêverie</w></l>
						<l n="18" num="5.2"><w n="18.1">Sur</w> <w n="18.2">le</w> <w n="18.3">plus</w> <w n="18.4">haut</w> <w n="18.5">des</w> <w n="18.6">monts</w> <w n="18.7">du</w> <w n="18.8">pays</w> <w n="18.9">vert</w> <w n="18.10">et</w> <w n="18.11">roux</w>.</l>
						<l n="19" num="5.3"><w n="19.1">Lorsque</w>, <w n="19.2">du</w> <w n="19.3">fond</w> <w n="19.4">de</w> <w n="19.5">l</w>’<w n="19.6">Algérie</w>,</l>
						<l n="20" num="5.4"><w n="20.1">Les</w> <w n="20.2">sommets</w> <w n="20.3">successifs</w> <w n="20.4">déferlaient</w> <w n="20.5">contre</w> <w n="20.6">nous</w> ;</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Tout</w> <w n="21.2">cela</w> <w n="21.3">qui</w>’<w n="21.4">berçait</w> <w n="21.5">notre</w> <w n="21.6">vie</w> <w n="21.7">ineffable</w>.</l>
						<l n="22" num="6.2"><w n="22.1">Pour</w> <w n="22.2">un</w> <w n="22.3">moment</w>, <w n="22.4">en</w> <w n="22.5">moi</w>, <w n="22.6">fut</w> <w n="22.7">comme</w> <w n="22.8">n</w>’<w n="22.9">étant</w> <w n="22.10">plus</w>.</l>
						<l n="23" num="6.3"><w n="23.1">Le</w> <w n="23.2">jour</w> <w n="23.3">que</w>, <w n="23.4">sans</w> <w n="23.5">savoir</w>, <w n="23.6">nous</w> <w n="23.7">sommes</w> <w n="23.8">descendus</w></l>
						<l n="24" num="6.4"><w n="24.1">A</w> <w n="24.2">Tabarka</w>, <w n="24.3">ville</w> <w n="24.4">marine</w> <w n="24.5">dans</w> <w n="24.6">le</w> <w n="24.7">sable</w>,</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">Parce</w> <w n="25.2">que</w> <w n="25.3">la</w> <w n="25.4">mer</w> <w n="25.5">s</w>’<w n="25.6">y</w> <w n="25.7">répand</w></l>
						<l n="26" num="7.2"><w n="26.1">Verte</w>, <w n="26.2">lumineuse</w> <w n="26.3">et</w> <w n="26.4">foncée</w>,</l>
						<l n="27" num="7.3"><w n="27.1">Et</w> <w n="27.2">qu</w>’<w n="27.3">au</w> <w n="27.4">cœur</w> <w n="27.5">du</w> <w n="27.6">large</w> <w n="27.7">bleu</w>-<w n="27.8">paon</w>.</l>
						<l n="28" num="7.4"><w n="28.1">Toute</w> <w n="28.2">mon</w> <w n="28.3">âme</w> <w n="28.4">s</w>’<w n="28.5">est</w> <w n="28.6">en</w> <w n="28.7">silence</w> <w n="28.8">élancée</w></l>
						<l n="29" num="7.5"><w n="29.1">Vers</w> <w n="29.2">plus</w> <w n="29.3">loin</w>, <w n="29.4">vers</w> <w n="29.5">plus</w> <w n="29.6">beau</w>, <w n="29.7">vers</w> <w n="29.8">plus</w> <w n="29.9">pur</w>, <w n="29.10">vers</w> <w n="29.11">plus</w> <w n="29.12">grand</w></l>
						<l n="30" num="7.6"><w n="30.1">Où</w> <w n="30.2">nous</w> <w n="30.3">n</w>’<w n="30.4">atteindrons</w> <w n="30.5">pas</w>, <w n="30.6">même</w> <w n="30.7">par</w> <w n="30.8">la</w> <w n="30.9">pensée</w>…</l>
					</lg>
				</div></body></text></TEI>