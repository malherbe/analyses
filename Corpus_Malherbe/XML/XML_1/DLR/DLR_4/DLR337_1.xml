<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FIGURE DE PROUE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2836 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/delaruemardruepoesies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">La Figure de Proue</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/stream/lafiguredeproue00dela/lafiguredeproue00dela_djvu.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>La Figure de Proue</title>
								<author>Lucie Delarue-Mardrus</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Fasquelle</publisher>
									<date when="1908">1908</date>
								</imprint>
							</monogr>
							<note>Source des poèmes ajoutés et édition de référence pour les corrections métriques.</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1908">1908</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
				<p>Des poèmes ont été ajoutés afin d’être conforme à l’édition de référence (source : archive.org)</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PREMIER ISLAM</head><div type="poem" key="DLR337">
					<head type="main">PRIÈRE MARINE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">A</w> <w n="1.2">travers</w> <w n="1.3">des</w> <w n="1.4">chemins</w> <w n="1.5">nuptiaux</w> <w n="1.6">d</w>’<w n="1.7">orangers</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Je</w> <w n="2.2">suis</w> <w n="2.3">venue</w> <w n="2.4">à</w> <w n="2.5">toi</w>, <w n="2.6">mer</w> <w n="2.7">Méditerranée</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Et</w> <w n="3.2">me</w> <w n="3.3">voici</w> <w n="3.4">debout</w>, <w n="3.5">face</w> <w n="3.6">à</w> <w n="3.7">face</w>, <w n="3.8">étonnée</w></l>
						<l n="4" num="1.4"><w n="4.1">D</w>’<w n="4.2">ouvrir</w> <w n="4.3">sur</w> <w n="4.4">ta</w> <w n="4.5">splendeur</w> <w n="4.6">mes</w> <w n="4.7">regards</w> <w n="4.8">étrangers</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Ce</w> <w n="5.2">soir</w>, <w n="5.3">ce</w> <w n="5.4">premier</w> <w n="5.5">soir</w>, <w n="5.6">t</w>’<w n="5.7">es</w>-<w n="5.8">tu</w> <w n="5.9">faite</w> <w n="5.10">si</w> <w n="5.11">pâle</w></l>
						<l n="6" num="2.2"><w n="6.1">Pour</w> <w n="6.2">ne</w> <w n="6.3">pas</w> <w n="6.4">m</w>’<w n="6.5">offenser</w> <w n="6.6">de</w> <w n="6.7">tes</w> <w n="6.8">bleus</w> <w n="6.9">inouïs</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Toi</w> <w n="7.2">qui</w> <w n="7.3">n</w>’<w n="7.4">es</w> <w n="7.5">pas</w> <w n="7.6">l</w>’<w n="7.7">horizon</w> <w n="7.8">gris</w> <w n="7.9">de</w> <w n="7.10">mon</w> <w n="7.11">pays</w>,</l>
						<l n="8" num="2.4"><w n="8.1">Mer</w> <w n="8.2">éternellement</w>, <w n="8.3">rythmiquement</w> <w n="8.4">étale</w> ?</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Je</w> <w n="9.2">tremble</w> <w n="9.3">de</w> <w n="9.4">venir</w> <w n="9.5">à</w> <w n="9.6">toi</w>, <w n="9.7">de</w> <w n="9.8">t</w>’<w n="9.9">apporter</w></l>
						<l n="10" num="3.2"><w n="10.1">Toute</w> <w n="10.2">mon</w> <w n="10.3">âme</w> <w n="10.4">où</w> <w n="10.5">crie</w> <w n="10.6">et</w> <w n="10.7">chante</w> <w n="10.8">l</w>’<w n="10.9">Innommable</w>…</l>
						<l n="11" num="3.3"><w n="11.1">Quoique</w> <w n="11.2">fille</w> <w n="11.3">d</w>’<w n="11.4">ailleurs</w>, <w n="11.5">voudras</w>-<w n="11.6">tu</w> <w n="11.7">m</w>’<w n="11.8">adopter</w>,</l>
						<l n="12" num="3.4"><w n="12.1">M</w>’<w n="12.2">enseigner</w> <w n="12.3">le</w> <w n="12.4">secret</w> <w n="12.5">de</w> <w n="12.6">tes</w> <w n="12.7">eaux</w> <w n="12.8">sur</w> <w n="12.9">ton</w> <w n="12.10">sable</w> ?</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Ah</w> ! <w n="13.2">berce</w>-<w n="13.3">moi</w>, <w n="13.4">beau</w> <w n="13.5">flot</w> <w n="13.6">qui</w> <w n="13.7">ne</w> <w n="13.8">me</w> <w n="13.9">connais</w> <w n="13.10">point</w>,</l>
						<l n="14" num="4.2"><w n="14.1">Moi</w> <w n="14.2">qui</w> <w n="14.3">suis</w> <w n="14.4">veuve</w> <w n="14.5">de</w> <w n="14.6">ma</w> <w n="14.7">mer</w> <w n="14.8">et</w> <w n="14.9">de</w> <w n="14.10">ma</w> <w n="14.11">terre</w>,</l>
						<l n="15" num="4.3"><w n="15.1">Moi</w> <w n="15.2">qui</w> <w n="15.3">t</w>’<w n="15.4">aime</w> <w n="15.5">déjà</w>, <w n="15.6">moi</w> <w n="15.7">qui</w> <w n="15.8">viens</w> <w n="15.9">de</w> <w n="15.10">si</w> <w n="15.11">loin</w>,</l>
						<l n="16" num="4.4"><w n="16.1">Moi</w> <w n="16.2">qui</w> <w n="16.3">voudrais</w> <w n="16.4">commettre</w> <w n="16.5">avec</w> <w n="16.6">toi</w> <w n="16.7">l</w>’<w n="16.8">adultère</w> !</l>
					</lg>
				</div></body></text></TEI>