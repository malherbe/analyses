<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FIGURE DE PROUE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2836 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/delaruemardruepoesies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">La Figure de Proue</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/stream/lafiguredeproue00dela/lafiguredeproue00dela_djvu.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>La Figure de Proue</title>
								<author>Lucie Delarue-Mardrus</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Fasquelle</publisher>
									<date when="1908">1908</date>
								</imprint>
							</monogr>
							<note>Source des poèmes ajoutés et édition de référence pour les corrections métriques.</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1908">1908</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
				<p>Des poèmes ont été ajoutés afin d’être conforme à l’édition de référence (source : archive.org)</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PREMIER ISLAM</head><div type="poem" key="DLR342">
					<head type="main">TEMPÊTE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Toi</w> <w n="1.2">si</w> <w n="1.3">douce</w>, <w n="1.4">si</w> <w n="1.5">bleue</w> <w n="1.6">au</w> <w n="1.7">bout</w> <w n="1.8">de</w> <w n="1.9">tout</w> <w n="1.10">chemin</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Mer</w>, <w n="2.2">tu</w> ’<w n="2.3">es</w> <w n="2.4">plus</w> <w n="2.5">ce</w> <w n="2.6">soir</w> <w n="2.7">qu</w>’<w n="2.8">une</w> <w n="2.9">ombre</w> <w n="2.10">qui</w> <w n="2.11">déferle</w></l>
						<l n="3" num="1.3"><w n="3.1">Dans</w> <w n="3.2">l</w>’<w n="3.3">orage</w> <w n="3.4">couleur</w> <w n="3.5">de</w> <w n="3.6">perle</w>.</l>
					</lg>
					<lg n="2">
						<l n="4" num="2.1"><w n="4.1">J</w>’<w n="4.2">entends</w> <w n="4.3">au</w> <w n="4.4">loin</w> <w n="4.5">crier</w>, <w n="4.6">la</w> <w n="4.7">bouche</w> <w n="4.8">à</w> <w n="4.9">leurs</w> <w n="4.10">deux</w> <w n="4.11">mains</w>,</l>
						<l n="5" num="2.2"><w n="5.1">Les</w> <w n="5.2">millions</w> <w n="5.3">surgis</w> <w n="5.4">de</w> <w n="5.5">sirènes</w> <w n="5.6">mêlées</w></l>
						<l n="6" num="2.3"><w n="6.1">De</w> <w n="6.2">tes</w> <w n="6.3">vagues</w> <w n="6.4">échevelées</w>.</l>
					</lg>
					<lg n="3">
						<l n="7" num="3.1"><w n="7.1">Veux</w>-<w n="7.2">tu</w> <w n="7.3">de</w> <w n="7.4">moi</w> ? <w n="7.5">J</w>’<w n="7.6">irai</w> <w n="7.7">jusqu</w>’<w n="7.8">à</w> <w n="7.9">toi</w>, <w n="7.10">cette</w> <w n="7.11">nuit</w>.</l>
						<l n="8" num="3.2"><w n="8.1">Tes</w> <w n="8.2">passions</w> <w n="8.3">avec</w> <w n="8.4">leurs</w> <w n="8.5">dégâts</w> <w n="8.6">et</w> <w n="8.7">leur</w> <w n="8.8">bruit</w></l>
						<l n="9" num="3.3"><w n="9.1">Ne</w> <w n="9.2">grondent</w> <w n="9.3">pas</w> <w n="9.4">plus</w> <w n="9.5">que</w> <w n="9.6">les</w> <w n="9.7">miennes</w>.</l>
					</lg>
					<lg n="4">
						<l n="10" num="4.1"><w n="10.1">J</w>’<w n="10.2">irai</w> ! <w n="10.3">Ce</w> <w n="10.4">souffle</w> <w n="10.5">rauque</w> <w n="10.6">est</w> <w n="10.7">celui</w> <w n="10.8">qu</w>’<w n="10.9">il</w> <w n="10.10">me</w> <w n="10.11">faut</w>,</l>
						<l n="11" num="4.2"><w n="11.1">Et</w> <w n="11.2">vous</w> <w n="11.3">vous</w> <w n="11.4">souviendrez</w> <w n="11.5">des</w> <w n="11.6">râles</w> <w n="11.7">de</w> <w n="11.8">Sapho</w>,</l>
						<l n="12" num="4.3"><w n="12.1">Fureurs</w> <w n="12.2">méditerranéennes</w> !</l>
					</lg>
				</div></body></text></TEI>