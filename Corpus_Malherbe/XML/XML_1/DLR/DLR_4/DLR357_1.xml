<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FIGURE DE PROUE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2836 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/delaruemardruepoesies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">La Figure de Proue</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/stream/lafiguredeproue00dela/lafiguredeproue00dela_djvu.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>La Figure de Proue</title>
								<author>Lucie Delarue-Mardrus</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Fasquelle</publisher>
									<date when="1908">1908</date>
								</imprint>
							</monogr>
							<note>Source des poèmes ajoutés et édition de référence pour les corrections métriques.</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1908">1908</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
				<p>Des poèmes ont été ajoutés afin d’être conforme à l’édition de référence (source : archive.org)</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PREMIER ISLAM</head><div type="poem" key="DLR357">
					<head type="main">SOUDANAIS</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Notre</w> <w n="1.2">ordre</w> <w n="1.3">impérieux</w> <w n="1.4">à</w> <w n="1.5">l</w>’<w n="1.6">insolite</w> <w n="1.7">nègre</w></l>
						<l n="2" num="1.2"><w n="2.1">L</w>’<w n="2.2">a</w> <w n="2.3">descendu</w> <w n="2.4">soudain</w> <w n="2.5">de</w> <w n="2.6">son</w> <w n="2.7">vieil</w> <w n="2.8">âne</w> <w n="2.9">maigre</w>.</l>
					</lg>
					<lg n="2">
						<l n="3" num="2.1"><w n="3.1">Sa</w> <w n="3.2">jupe</w> <w n="3.3">de</w> <w n="3.4">chacals</w> <w n="3.5">vole</w>, <w n="3.6">et</w> <w n="3.7">son</w> <w n="3.8">tambour</w> <w n="3.9">peint</w></l>
						<l n="4" num="2.2"><w n="4.1">Gronde</w>, <w n="4.2">et</w> <w n="4.3">son</w> <w n="4.4">masque</w> <w n="4.5">est</w> <w n="4.6">fait</w> <w n="4.7">d</w>’<w n="4.8">une</w> <w n="4.9">peau</w> <w n="4.10">de</w> <w n="4.11">lapin</w>.</l>
					</lg>
					<lg n="3">
						<l n="5" num="3.1"><w n="5.1">Les</w> <w n="5.2">cent</w> <w n="5.3">miroirs</w> <w n="5.4">cousus</w> <w n="5.5">à</w> <w n="5.6">son</w> <w n="5.7">bonnet</w> <w n="5.8">sauvage</w></l>
						<l n="6" num="3.2"><w n="6.1">Éclatent</w> <w n="6.2">au</w> <w n="6.3">soleil</w>, <w n="6.4">au</w> <w n="6.5">rythme</w> <w n="6.6">de</w> <w n="6.7">sa</w> <w n="6.8">rage</w>.</l>
					</lg>
					<lg n="4">
						<l n="7" num="4.1"><w n="7.1">Il</w> <w n="7.2">semble</w> <w n="7.3">ainsi</w>, <w n="7.4">du</w> <w n="7.5">haut</w> <w n="7.6">de</w> <w n="7.7">ses</w> <w n="7.8">contorsions</w>,</l>
						<l n="8" num="4.2"><w n="8.1">Jeter</w> <w n="8.2">autour</w> <w n="8.3">de</w> <w n="8.4">lui</w> <w n="8.5">des</w> <w n="8.6">constellations</w>.</l>
					</lg>
					<lg n="5">
						<l n="9" num="5.1"><w n="9.1">Tout</w> <w n="9.2">le</w> <w n="9.3">voyage</w> <w n="9.4">au</w> <w n="9.5">loin</w> <w n="9.6">danse</w> <w n="9.7">avec</w> <w n="9.8">ce</w> <w n="9.9">nègre</w> <w n="9.10">ivre</w>.</l>
						<l n="10" num="5.2"><w n="10.1">Ai</w>-<w n="10.2">je</w> <w n="10.3">enfin</w> <w n="10.4">vu</w> <w n="10.5">de</w> <w n="10.6">près</w> <w n="10.7">ce</w> <w n="10.8">qu</w>’<w n="10.9">on</w> <w n="10.10">lit</w> <w n="10.11">dans</w> <w n="10.12">les</w> <w n="10.13">livres</w> ?</l>
					</lg>
				</div></body></text></TEI>