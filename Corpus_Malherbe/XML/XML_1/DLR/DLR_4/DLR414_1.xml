<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FIGURE DE PROUE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2836 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/delaruemardruepoesies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">La Figure de Proue</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/stream/lafiguredeproue00dela/lafiguredeproue00dela_djvu.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>La Figure de Proue</title>
								<author>Lucie Delarue-Mardrus</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Fasquelle</publisher>
									<date when="1908">1908</date>
								</imprint>
							</monogr>
							<note>Source des poèmes ajoutés et édition de référence pour les corrections métriques.</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1908">1908</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
				<p>Des poèmes ont été ajoutés afin d’être conforme à l’édition de référence (source : archive.org)</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">DE FRANCE</head><div type="poem" key="DLR414">
					<head type="main">INVOCATION</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">O</w> <w n="1.2">vous</w>, <w n="1.3">production</w> <w n="1.4">étrange</w> <w n="1.5">et</w> <w n="1.6">naturelle</w></l>
						<l n="2" num="1.2"><w n="2.1">De</w> <w n="2.2">ce</w> <w n="2.3">sang</w> <w n="2.4">rouge</w> <w n="2.5">et</w> <w n="2.6">bleu</w>, <w n="2.7">rusé</w>, <w n="2.8">savant</w>, <w n="2.9">ardent</w></l>
						<l n="3" num="1.3"><w n="3.1">Et</w> <w n="3.2">magnifique</w> <w n="3.3">d</w>’<w n="3.4">Occident</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Notre</w>-<w n="4.2">Dame</w>, <w n="4.3">éternel</w>, <w n="4.4">immobile</w> <w n="4.5">coup</w> <w n="4.6">d</w>’<w n="4.7">aile</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Vous</w> <w n="5.2">qui</w> <w n="5.3">vous</w> <w n="5.4">compliquez</w> <w n="5.5">comme</w> <w n="5.6">notre</w> <w n="5.7">cerveau</w></l>
						<l n="6" num="2.2"><w n="6.1">D</w>’<w n="6.2">imagination</w> <w n="6.3">à</w> <w n="6.4">jamais</w> <w n="6.5">fatigante</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Nous</w> <w n="7.2">ne</w> <w n="7.3">comprenons</w> <w n="7.4">point</w> <w n="7.5">à</w> <w n="7.6">fond</w> <w n="7.7">ce</w> <w n="7.8">qui</w> <w n="7.9">vous</w> <w n="7.10">hante</w>,</l>
						<l n="8" num="2.4"><w n="8.1">Quel</w> <w n="8.2">esprit</w> <w n="8.3">satanique</w> <w n="8.4">autant</w> <w n="8.5">qu</w>’<w n="8.6">il</w> <w n="8.7">est</w> <w n="8.8">dévot</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Mais</w> <w n="9.2">nous</w> <w n="9.3">nous</w> <w n="9.4">retrouvons</w> <w n="9.5">dans</w> <w n="9.6">vos</w> <w n="9.7">rosaces</w> <w n="9.8">folles</w></l>
						<l n="10" num="3.2"><w n="10.1">Où</w> <w n="10.2">s</w>’<w n="10.3">emprisonne</w> <w n="10.4">et</w> <w n="10.5">luit</w> <w n="10.6">un</w> <w n="10.7">univers</w> <w n="10.8">vermeil</w>.</l>
						<l n="11" num="3.3"><w n="11.1">Nous</w> <w n="11.2">qui</w> <w n="11.3">ne</w> <w n="11.4">pouvons</w> <w n="11.5">voir</w> <w n="11.6">qu</w>’<w n="11.7">à</w> <w n="11.8">travers</w> <w n="11.9">des</w> <w n="11.10">symboles</w></l>
						<l n="12" num="3.4"><w n="12.1">Le</w> <w n="12.2">jour</w> <w n="12.3">tout</w> <w n="12.4">simple</w> <w n="12.5">du</w> <w n="12.6">soleil</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Orage</w> <w n="13.2">d</w>’<w n="13.3">harmonie</w>, <w n="13.4">ô</w> <w n="13.5">muette</w> <w n="13.6">musique</w>,</l>
						<l n="14" num="4.2"><w n="14.1">Votre</w> <w n="14.2">flèche</w> <w n="14.3">elle</w>-<w n="14.4">même</w>, <w n="14.5">en</w> <w n="14.6">son</w> <w n="14.7">vol</w> <w n="14.8">sans</w> <w n="14.9">défaut</w>,</l>
						<l n="15" num="4.3"><w n="15.1">Ne</w> <w n="15.2">peut</w> <w n="15.3">monter</w> <w n="15.4">tout</w> <w n="15.5">droit</w> <w n="15.6">vers</w> <w n="15.7">la</w> <w n="15.8">métaphysique</w></l>
						<l n="16" num="4.4"><w n="16.1">Qu</w>’<w n="16.2">en</w> <w n="16.3">se</w> <w n="16.4">chargeant</w> <w n="16.5">encor</w> <w n="16.6">d</w>’<w n="16.7">ornements</w> <w n="16.8">Jusqu</w>’<w n="16.9">en</w> <w n="16.10">haut</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Donc</w>, <w n="17.2">pierre</w> <w n="17.3">et</w> <w n="17.4">verre</w> <w n="17.5">forme</w> <w n="17.6">alourdie</w> <w n="17.7">et</w> <w n="17.8">légère</w>.</l>
						<l n="18" num="5.2"><w n="18.1">Loin</w> <w n="18.2">des</w> <w n="18.3">Jérusalem</w>, <w n="18.4">Nazareth</w> <w n="18.5">et</w> <w n="18.6">Sion</w></l>
						<l n="19" num="5.3"><w n="19.1">Qui</w> <w n="19.2">dressent</w> <w n="19.3">dans</w> <w n="19.4">les</w> <w n="19.5">Suds</w> <w n="19.6">leur</w> <w n="19.7">blancheur</w> <w n="19.8">étrangère</w>,</l>
						<l n="20" num="5.4"><w n="20.1">O</w> <w n="20.2">fantasque</w> ! <w n="20.3">soyez</w> <w n="20.4">notre</w> <w n="20.5">habitation</w> !</l>
					</lg>
				</div></body></text></TEI>