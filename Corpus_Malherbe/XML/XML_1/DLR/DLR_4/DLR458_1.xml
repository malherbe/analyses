<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FIGURE DE PROUE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2836 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/delaruemardruepoesies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">La Figure de Proue</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/stream/lafiguredeproue00dela/lafiguredeproue00dela_djvu.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>La Figure de Proue</title>
								<author>Lucie Delarue-Mardrus</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Fasquelle</publisher>
									<date when="1908">1908</date>
								</imprint>
							</monogr>
							<note>Source des poèmes ajoutés et édition de référence pour les corrections métriques.</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1908">1908</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
				<p>Des poèmes ont été ajoutés afin d’être conforme à l’édition de référence (source : archive.org)</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">AU PORT</head><div type="poem" key="DLR458">
					<head type="main">DE HONFLEUR</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Honfleur</w>, <w n="1.2">ma</w> <w n="1.3">ville</w>, <w n="1.4">je</w> <w n="1.5">te</w> <w n="1.6">vois</w></l>
						<l n="2" num="1.2"><w n="2.1">Du</w> <w n="2.2">haut</w> <w n="2.3">de</w> <w n="2.4">ta</w> <w n="2.5">colline</w>, <w n="2.6">ô</w> <w n="2.7">pluvieuse</w>, <w n="2.8">ô</w> <w n="2.9">grise</w>.</l>
						<l n="3" num="1.3"><w n="3.1">Entre</w> <w n="3.2">les</w> <w n="3.3">flots</w> <w n="3.4">pressés</w> <w n="3.5">de</w> <w n="3.6">ta</w> <w n="3.7">mer</w> <w n="3.8">qui</w> <w n="3.9">se</w> <w n="3.10">brise</w></l>
						<l n="4" num="1.4"><w n="4.1">Et</w> <w n="4.2">le</w> <w n="4.3">moutonnement</w> <w n="4.4">terrien</w> <w n="4.5">de</w> <w n="4.6">tes</w> <w n="4.7">bois</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Que</w> <w n="5.2">de</w> <w n="5.3">fois</w>, <w n="5.4">devant</w> <w n="5.5">d</w>’<w n="5.6">autre</w> <w n="5.7">villes</w>,</l>
						<l n="6" num="2.2"><w n="6.1">J</w>’<w n="6.2">évoquai</w> <w n="6.3">tes</w> <w n="6.4">contours</w> <w n="6.5">tout</w> <w n="6.6">immatériels</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Parmi</w> <w n="7.2">l</w>’<w n="7.3">Afrique</w> <w n="7.4">fauve</w> <w n="7.5">et</w> <w n="7.6">ses</w> <w n="7.7">blancheurs</w> <w n="7.8">faciles</w>.</l>
						<l n="8" num="2.4"><w n="8.1">Te</w> <w n="8.2">voici</w> <w n="8.3">donc</w> <w n="8.4">enfin</w> <w n="8.5">devant</w> <w n="8.6">mes</w> <w n="8.7">yeux</w> <w n="8.8">réels</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Ma</w> <w n="9.2">cité</w>, <w n="9.3">combien</w> <w n="9.4">sont</w> <w n="9.5">tes</w> <w n="9.6">plages</w></l>
						<l n="10" num="3.2"><w n="10.1">Tristes</w>, <w n="10.2">ton</w> <w n="10.3">estuaire</w> <w n="10.4">évasif</w> <w n="10.5">et</w> <w n="10.6">navré</w> !</l>
						<l n="11" num="3.3"><w n="11.1">Mais</w> <w n="11.2">que</w> <w n="11.3">sont</w> <w n="11.4">gais</w> <w n="11.5">et</w> <w n="11.6">sains</w> <w n="11.7">et</w> <w n="11.8">riches</w> <w n="11.9">tes</w> <w n="11.10">herbages</w>.</l>
						<l n="12" num="3.4"><w n="12.1">Tes</w> <w n="12.2">arbres</w> <w n="12.3">lourds</w> <w n="12.4">de</w> <w n="12.5">fruits</w> <w n="12.6">et</w> <w n="12.7">d</w>’<w n="12.8">automne</w> <w n="12.9">doré</w> !</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Parmi</w> <w n="13.2">tes</w> <w n="13.3">clochers</w> <w n="13.4">et</w> <w n="13.5">tes</w> <w n="13.6">phares</w></l>
						<l n="14" num="4.2"><w n="14.1">Tu</w> <w n="14.2">sens</w> <w n="14.3">toujours</w> <w n="14.4">le</w> <w n="14.5">foin</w>, <w n="14.6">la</w> <w n="14.7">vase</w> <w n="14.8">et</w> <w n="14.9">le</w> <w n="14.10">goudron</w>,</l>
						<l n="15" num="4.3"><w n="15.1">Et</w> <w n="15.2">tes</w> <w n="15.3">barques</w> <w n="15.4">toujours</w> <w n="15.5">tirent</w> <w n="15.6">sur</w> <w n="15.7">leurs</w> <w n="15.8">amarres</w></l>
						<l n="16" num="4.4"><w n="16.1">Et</w> <w n="16.2">tes</w> <w n="16.3">oiseaux</w> <w n="16.4">de</w> <w n="16.5">mer</w> <w n="16.6">tournent</w> <w n="16.7">toujours</w> <w n="16.8">en</w> <w n="16.9">rond</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Le</w> <w n="17.2">temps</w> <w n="17.3">où</w> <w n="17.4">l</w>’<w n="17.5">on</w> <w n="17.6">allait</w> <w n="17.7">aux</w> <w n="17.8">Iles</w></l>
						<l n="18" num="5.2"><w n="18.1">Persiste</w> <w n="18.2">en</w> <w n="18.3">toi</w>, <w n="18.4">parmi</w> <w n="18.5">quelque</w> <w n="18.6">quartier</w> <w n="18.7">noirci</w>.</l>
						<l n="19" num="5.3"><w n="19.1">Moi</w> <w n="19.2">qui</w> <w n="19.3">reviens</w> <w n="19.4">de</w> <w n="19.5">loin</w>, <w n="19.6">ô</w> <w n="19.7">ville</w> <w n="19.8">entre</w> <w n="19.9">les</w> <w n="19.10">villes</w>,</l>
						<l n="20" num="5.4"><w n="20.1">Je</w> <w n="20.2">sais</w> <w n="20.3">bien</w> <w n="20.4">que</w>, <w n="20.5">tous</w> <w n="20.6">les</w> <w n="20.7">voyages</w>, <w n="20.8">c</w>’<w n="20.9">est</w> <w n="20.10">ici</w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Et</w> <w n="21.2">sur</w> <w n="21.3">ton</w> <w n="21.4">profil</w> <w n="21.5">de</w> <w n="21.6">bitume</w></l>
						<l n="22" num="6.2"><w n="22.1">Et</w> <w n="22.2">d</w>’<w n="22.3">opale</w>, <w n="22.4">montant</w> <w n="22.5">de</w> <w n="22.6">l</w>’<w n="22.7">amas</w> <w n="22.8">sombre</w> <w n="22.9">et</w> <w n="22.10">clair</w>.</l>
						<l n="23" num="6.3"><w n="23.1">Je</w> <w n="23.2">regarde</w> <w n="23.3">s</w>’<w n="23.4">étendre</w> <w n="23.5">en</w> <w n="23.6">biais</w> <w n="23.7">vers</w> <w n="23.8">la</w> <w n="23.9">mer</w></l>
						<l n="24" num="6.4"><w n="24.1">Cette</w> <w n="24.2">grande</w> <w n="24.3">fumée</w> <w n="24.4">ou</w> <w n="24.5">cette</w> <w n="24.6">grande</w> <w n="24.7">brume</w>.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">Phantasme</w> <w n="25.2">traversé</w> <w n="25.3">d</w>’<w n="25.4">oiseaux</w>.</l>
						<l n="26" num="7.2"><w n="26.1">Cette</w> <w n="26.2">fumée</w> <w n="26.3">ou</w> <w n="26.4">cette</w> <w n="26.5">brume</w> <w n="26.6">qui</w> <w n="26.7">s</w>’<w n="26.8">élève</w>.</l>
						<l n="27" num="7.3"><w n="27.1">N</w>’<w n="27.2">est</w>-<w n="27.3">ce</w> <w n="27.4">pas</w>, <w n="27.5">élancé</w> <w n="27.6">de</w> <w n="27.7">ma</w> <w n="27.8">ville</w> <w n="27.9">de</w> <w n="27.10">rêve</w>.</l>
						<l n="28" num="7.4"><w n="28.1">Mon</w> <w n="28.2">esprit</w> <w n="28.3">qui</w> <w n="28.4">s</w>’<w n="28.5">épand</w> <w n="28.6">sur</w> <w n="28.7">la</w> <w n="28.8">terre</w> <w n="28.9">et</w> <w n="28.10">les</w> <w n="28.11">eaux</w> ?…</l>
					</lg>
				</div></body></text></TEI>