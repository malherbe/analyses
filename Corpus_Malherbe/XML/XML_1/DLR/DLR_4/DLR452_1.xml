<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FIGURE DE PROUE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2836 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/delaruemardruepoesies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">La Figure de Proue</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/stream/lafiguredeproue00dela/lafiguredeproue00dela_djvu.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>La Figure de Proue</title>
								<author>Lucie Delarue-Mardrus</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Fasquelle</publisher>
									<date when="1908">1908</date>
								</imprint>
							</monogr>
							<note>Source des poèmes ajoutés et édition de référence pour les corrections métriques.</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1908">1908</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
				<p>Des poèmes ont été ajoutés afin d’être conforme à l’édition de référence (source : archive.org)</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">POÈMES ORANAIS ET KABYLES</head><div type="poem" key="DLR452">
					<head type="main">D’UNE FENÊTRE SUR LA RADE</head>
					<div type="section" n="1">
						<head type="number">I</head>
						<head type="sub">ENVOL</head>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">Dans</w> <w n="1.2">le</w> <w n="1.3">creux</w> <w n="1.4">de</w> <w n="1.5">ces</w> <w n="1.6">huit</w> <w n="1.7">montagnes</w> <w n="1.8">orageuses</w>,</l>
							<l n="2" num="1.2"><w n="2.1">La</w> <w n="2.2">baie</w> <w n="2.3">au</w> <w n="2.4">soir</w> <w n="2.5">tombant</w> <w n="2.6">est</w> <w n="2.7">comme</w> <w n="2.8">un</w> <w n="2.9">bol</w> <w n="2.10">de</w> <w n="2.11">lait</w>.</l>
						</lg>
						<lg n="2">
							<l n="3" num="2.1"><w n="3.1">Viens</w> <w n="3.2">t</w>’<w n="3.3">accouder</w> <w n="3.4">devant</w> <w n="3.5">le</w> <w n="3.6">port</w>, <w n="3.7">puisqu</w>’<w n="3.8">il</w> <w n="3.9">te</w> <w n="3.10">plaît</w></l>
							<l n="4" num="2.2"><w n="4.1">De</w> <w n="4.2">voir</w> <w n="4.3">évoluer</w> <w n="4.4">les</w> <w n="4.5">coques</w> <w n="4.6">voyageuses</w>.</l>
						</lg>
						<lg n="3">
							<l n="5" num="3.1"><w n="5.1">Tu</w> <w n="5.2">ne</w> <w n="5.3">sais</w> <w n="5.4">pas</w> <w n="5.5">le</w> <w n="5.6">mal</w> <w n="5.7">et</w> <w n="5.8">le</w> <w n="5.9">bien</w> <w n="5.10">que</w> <w n="5.11">te</w> <w n="5.12">font</w></l>
							<l n="6" num="3.2"><w n="6.1">Ce</w> <w n="6.2">soir</w> <w n="6.3">tombant</w>, <w n="6.4">ce</w> <w n="6.5">ciel</w>, <w n="6.6">ce</w> <w n="6.7">port</w>, <w n="6.8">ces</w> <w n="6.9">promenades</w>,</l>
						</lg>
						<lg n="4">
							<l n="7" num="4.1"><w n="7.1">Toi</w> <w n="7.2">dont</w> <w n="7.3">l</w>’<w n="7.4">âme</w> <w n="7.5">d</w>’<w n="7.6">oiseau</w> <w n="7.7">de</w> <w n="7.8">mer</w>, <w n="7.9">devant</w> <w n="7.10">les</w> <w n="7.11">rades</w>.</l>
							<l n="8" num="4.2"><w n="8.1">Tourne</w> <w n="8.2">en</w> <w n="8.3">criant</w> <w n="8.4">autour</w> <w n="8.5">des</w> <w n="8.6">bateaux</w> <w n="8.7">qui</w> <w n="8.8">s</w>’<w n="8.9">en</w> <w n="8.10">vont</w>.</l>
						</lg>
					</div>
					<div type="section" n="2">
						<head type="number">II</head>
						<head type="sub">ÉLAN</head>
						<lg n="1">
							<l n="9" num="1.1"><w n="9.1">Personne</w> <w n="9.2">ne</w> <w n="9.3">pourra</w> <w n="9.4">sur</w> <w n="9.5">la</w> <w n="9.6">terre</w> <w n="9.7">savoir</w></l>
							<l n="10" num="1.2"><w n="10.1">Combien</w> <w n="10.2">j</w>’<w n="10.3">aime</w> <w n="10.4">les</w> <w n="10.5">silhouettes</w></l>
							<l n="11" num="1.3"><w n="11.1">Des</w> <w n="11.2">puissants</w> <w n="11.3">paquebots</w> <w n="11.4">ancrés</w>, <w n="11.5">rouges</w> <w n="11.6">et</w> <w n="11.7">noirs</w>,</l>
							<l n="12" num="1.4"><w n="12.1">Dans</w> <w n="12.2">les</w> <w n="12.3">ports</w> <w n="12.4">bleus</w> <w n="12.5">d</w>’<w n="12.6">Afrique</w> <w n="12.7">où</w> <w n="12.8">tournoient</w> <w n="12.9">les</w> <w n="12.10">mouettes</w>.</l>
						</lg>
						<lg n="2">
							<l n="13" num="2.1"><w n="13.1">O</w> <w n="13.2">mes</w> <w n="13.3">chers</w> <w n="13.4">paquebots</w> <w n="13.5">pour</w> <w n="13.6">un</w> <w n="13.7">jour</w> <w n="13.8">à</w> <w n="13.9">l</w>’<w n="13.10">écart</w></l>
							<l n="14" num="2.2"><w n="14.1">Du</w> <w n="14.2">large</w> <w n="14.3">où</w> <w n="14.4">le</w> <w n="14.5">destin</w> <w n="14.6">se</w> <w n="14.7">joue</w>.</l>
							<l n="15" num="2.3"><w n="15.1">Que</w> <w n="15.2">soit</w> <w n="15.3">ma</w> <w n="15.4">face</w> <w n="15.5">au</w> <w n="15.6">vent</w> <w n="15.7">la</w> <w n="15.8">figure</w> <w n="15.9">de</w> <w n="15.10">proue</w></l>
							<l n="16" num="2.4"><w n="16.1">De</w> <w n="16.2">vos</w> <w n="16.3">avants</w> <w n="16.4">tournés</w> <w n="16.5">du</w> <w n="16.6">côté</w> <w n="16.7">du</w> <w n="16.8">départ</w> !…</l>
						</lg>
					</div>
					<div type="section" n="3">
						<head type="number">III</head>
						<head type="sub">VEILLÉE</head>
						<lg n="1">
							<l n="17" num="1.1"><w n="17.1">A</w> <w n="17.2">la</w> <w n="17.3">fenêtre</w> <w n="17.4">lumineuse</w> <w n="17.5">de</w> <w n="17.6">la</w> <w n="17.7">chambre</w>,</l>
							<l n="18" num="1.2"><w n="18.1">Le</w> <w n="18.2">clair</w> <w n="18.3">de</w> <w n="18.4">lune</w>, <w n="18.5">peu</w> <w n="18.6">à</w> <w n="18.7">peu</w>, <w n="18.8">devient</w> <w n="18.9">le</w> <w n="18.10">jour</w>.</l>
							<l n="19" num="1.3"><w n="19.1">Qu</w>’<w n="19.2">est</w>-<w n="19.3">ce</w> <w n="19.4">donc</w>, <w n="19.5">dans</w> <w n="19.6">ton</w> <w n="19.7">âme</w> <w n="19.8">obscure</w>, <w n="19.9">qui</w> <w n="19.10">se</w> <w n="19.11">cambre</w></l>
							<l n="20" num="1.4"><w n="20.1">Et</w> <w n="20.2">qui</w> <w n="20.3">s</w>’<w n="20.4">affaisse</w> <w n="20.5">tour</w> <w n="20.6">à</w> <w n="20.7">tour</w> ?</l>
						</lg>
						<lg n="2">
							<l n="21" num="2.1"><w n="21.1">Pourquoi</w> <w n="21.2">donc</w> <w n="21.3">cette</w> <w n="21.4">nuit</w> <w n="21.5">de</w> <w n="21.6">veillée</w> <w n="21.7">inquiète</w> ?</l>
							<l n="22" num="2.2"><w n="22.1">Faut</w>-<w n="22.2">il</w>, <w n="22.3">faut</w>-<w n="22.4">il</w>, <w n="22.5">alors</w> <w n="22.6">que</w> <w n="22.7">le</w> <w n="22.8">monde</w> <w n="22.9">est</w> <w n="22.10">blafard</w></l>
							<l n="23" num="2.3"><w n="23.1">Et</w> <w n="23.2">mort</w>, <w n="23.3">qu</w>’<w n="23.4">un</w> <w n="23.5">bateau</w> <w n="23.6">sombre</w> <w n="23.7">attende</w> <w n="23.8">quelque</w> <w n="23.9">part</w>,</l>
							<l n="24" num="2.4"><w n="24.1">Et</w> <w n="24.2">que</w> <w n="24.3">soit</w> <w n="24.4">ton</w> <w n="24.5">repos</w>, <w n="24.6">comme</w> <w n="24.7">d</w>’<w n="24.8">une</w> <w n="24.9">mouette</w>,</l>
							<l n="25" num="2.5"><w n="25.1">Égratigné</w> <w n="25.2">parla</w> <w n="25.3">grande</w> <w n="25.4">aile</w> <w n="25.5">du</w> <w n="25.6">départ</w> ?…</l>
						</lg>
					</div>
				</div></body></text></TEI>