<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FIGURE DE PROUE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2836 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/delaruemardruepoesies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">La Figure de Proue</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/stream/lafiguredeproue00dela/lafiguredeproue00dela_djvu.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>La Figure de Proue</title>
								<author>Lucie Delarue-Mardrus</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Fasquelle</publisher>
									<date when="1908">1908</date>
								</imprint>
							</monogr>
							<note>Source des poèmes ajoutés et édition de référence pour les corrections métriques.</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1908">1908</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
				<p>Des poèmes ont été ajoutés afin d’être conforme à l’édition de référence (source : archive.org)</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">AU PORT</head><div type="poem" key="DLR471">
					<head type="main">DÉCLARATION</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Moi</w> <w n="1.2">qui</w> <w n="1.3">viens</w> <w n="1.4">des</w> <w n="1.5">gens</w> <w n="1.6">que</w> <w n="1.7">tu</w> <w n="1.8">parques</w></l>
						<l n="2" num="1.2"><w n="2.1">Entre</w> <w n="2.2">ton</w> <w n="2.3">port</w> <w n="2.4">et</w> <w n="2.5">ton</w> <w n="2.6">clocher</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Qui</w> <w n="3.2">pourra</w> <w n="3.3">jamais</w> <w n="3.4">arracher</w></l>
						<l n="4" num="1.4"><w n="4.1">Mon</w> <w n="4.2">cœur</w> <w n="4.3">de</w> <w n="4.4">toi</w>, <w n="4.5">ville</w> <w n="4.6">des</w> <w n="4.7">barques</w> ?</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">De</w> <w n="5.2">jour</w> <w n="5.3">et</w> <w n="5.4">de</w> <w n="5.5">nuit</w>, <w n="5.6">combien</w> <w n="5.7">j</w>’<w n="5.8">aime</w></l>
						<l n="6" num="2.2"><w n="6.1">Les</w> <w n="6.2">voir</w> <w n="6.3">gagner</w> <w n="6.4">les</w> <w n="6.5">horizons</w>,</l>
						<l n="7" num="2.3"><w n="7.1">A</w> <w n="7.2">la</w> <w n="7.3">fois</w> <w n="7.4">oiseaux</w> <w n="7.5">et</w> <w n="7.6">poissons</w>.</l>
						<l n="8" num="2.4"><w n="8.1">Ces</w> <w n="8.2">barques</w> <w n="8.3">que</w> <w n="8.4">le</w> <w n="8.5">vent</w> <w n="8.6">essaime</w> !</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Honfleur</w>, <w n="9.2">ô</w> ,<w n="9.3">ma</w> <w n="9.4">ville</w>, <w n="9.5">ô</w> <w n="9.6">ma</w> <w n="9.7">barque</w>.</l>
						<l n="10" num="3.2"><w n="10.1">Au</w> <w n="10.2">pays</w> <w n="10.3">froid</w>, <w n="10.4">au</w> <w n="10.5">pays</w> <w n="10.6">chaud</w>.</l>
						<l n="11" num="3.3"><w n="11.1">Je</w> <w n="11.2">porte</w> <w n="11.3">dans</w> <w n="11.4">l</w>’<w n="11.5">âme</w> <w n="11.6">la</w> <w n="11.7">marque</w></l>
						<l n="12" num="3.4"><w n="12.1">De</w> <w n="12.2">tes</w> <w n="12.3">voiles</w> <w n="12.4">rudes</w> : <subst hand="RR" reason="analysis" type="phonemization"><add rend="hidden"><w n="12.5">ach</w></add><del>H</del></subst>. <w n="12.6">O</w>.</l>
					</lg>
				</div></body></text></TEI>