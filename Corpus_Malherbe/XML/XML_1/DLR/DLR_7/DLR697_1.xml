<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">A MAMAN</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1250 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_7</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title type="main">A MAMAN</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LIBRAIRIE CHARPENTIER ET FASQUELLE</publisher>
							<date when="1920">1920</date>
						</imprint>
					</monogr>
					<note>
						Édition numérisée.
						Merci à la bibliothèque de l’université de Denver (USA)
						qui a bien voulu prêté un exemplaire de cet ouvrage ; aucune bibliothèque
						de France n’ayant accepté le prêt ou la reproduction sur place.
					</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1920">1920</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>.....</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-11-28" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">VI</head><head type="main_part">MÉDITATIONS</head><div type="poem" key="DLR697">
					<head type="main">L’ÉNIGME</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Je</w> <w n="1.2">n</w>’<w n="1.3">ai</w> <w n="1.4">pas</w> <w n="1.5">senti</w>, <w n="1.6">moi</w>, <w n="1.7">le</w> <w n="1.8">jour</w> <w n="1.9">où</w> <w n="1.10">je</w> <w n="1.11">naissais</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Que</w> <w n="2.2">le</w> <w n="2.3">vaste</w> <w n="2.4">néant</w> <w n="2.5">me</w> <w n="2.6">jetait</w> <w n="2.7">sur</w> <w n="2.8">la</w> <w n="2.9">terre</w>.</l>
						<l n="3" num="1.3"><w n="3.1">Tu</w> <w n="3.2">n</w>’<w n="3.3">as</w> <w n="3.4">pas</w> <w n="3.5">senti</w>, <w n="3.6">toi</w>, <w n="3.7">lorsque</w> <w n="3.8">tu</w> <w n="3.9">trépassais</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Que</w> <w n="4.2">le</w> <w n="4.3">vaste</w> <w n="4.4">néant</w> <w n="4.5">te</w> <w n="4.6">prenait</w> <w n="4.7">tout</w> <w n="4.8">entière</w></l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">La</w> <w n="5.2">naissance</w> <w n="5.3">et</w> <w n="5.4">la</w> <w n="5.5">mort</w> <w n="5.6">sont</w> <w n="5.7">un</w> <w n="5.8">même</w> <w n="5.9">mystère</w></l>
					</lg>
					<lg n="3">
						<l n="6" num="3.1"><w n="6.1">Tu</w> <w n="6.2">me</w> <w n="6.3">vis</w> <w n="6.4">naître</w>, <w n="6.5">toi</w> ; <w n="6.6">moi</w>, <w n="6.7">je</w> <w n="6.8">te</w> <w n="6.9">vis</w> <w n="6.10">mourir</w>.</l>
						<l n="7" num="3.2"><w n="7.1">Nous</w> <w n="7.2">fûmes</w> <w n="7.3">le</w> <w n="7.4">témoin</w> <w n="7.5">des</w> <w n="7.6">deux</w> <w n="7.7">énigmes</w> <w n="7.8">sombres</w>.</l>
						<l n="8" num="3.3"><w n="8.1">La</w> <w n="8.2">vie</w> ? <w n="8.3">Un</w> <w n="8.4">nombre</w> <w n="8.5">obscur</w> <w n="8.6">de</w> <w n="8.7">jours</w> <w n="8.8">à</w> <w n="8.9">parcourir</w>,</l>
						<l n="9" num="3.4"><w n="9.1">Pauvre</w> <w n="9.2">instant</w> <w n="9.3">de</w> <w n="9.4">clarté</w> <w n="9.5">douteuse</w> <w n="9.6">entre</w> <w n="9.7">deux</w> <w n="9.8">ombres</w>…</l>
					</lg>
					<lg n="4">
						<l n="10" num="4.1"><w n="10.1">Oh</w> ! <w n="10.2">savoir</w> <w n="10.3">à</w> <w n="10.4">quel</w> <w n="10.5">rythme</w> <w n="10.6">obéissent</w> <w n="10.7">ces</w> <w n="10.8">nombres</w> !</l>
					</lg>
				</div></body></text></TEI>