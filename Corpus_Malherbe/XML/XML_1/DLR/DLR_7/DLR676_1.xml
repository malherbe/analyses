<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">A MAMAN</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1250 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_7</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title type="main">A MAMAN</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LIBRAIRIE CHARPENTIER ET FASQUELLE</publisher>
							<date when="1920">1920</date>
						</imprint>
					</monogr>
					<note>
						Édition numérisée.
						Merci à la bibliothèque de l’université de Denver (USA)
						qui a bien voulu prêté un exemplaire de cet ouvrage ; aucune bibliothèque
						de France n’ayant accepté le prêt ou la reproduction sur place.
					</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1920">1920</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>.....</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-11-28" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">III</head><head type="main_part">EN DEUIL</head><div type="poem" key="DLR676">
					<head type="main">ILS DISENT…</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Ils</w> <w n="1.2">disent</w> : « <w n="1.3">Tout</w> <w n="1.4">le</w> <w n="1.5">monde</w>, <w n="1.6">après</w> <w n="1.7">tout</w>, <w n="1.8">perd</w> <w n="1.9">sa</w> <w n="1.10">mère</w></l>
						<l n="2" num="1.2"><space unit="char" quantity="8"></space><w n="2.1">Ce</w> <w n="2.2">n</w>’<w n="2.3">est</w> <w n="2.4">pas</w> <w n="2.5">extraordinaire</w>. »</l>
						<l n="3" num="1.3"><w n="3.1">Et</w>, <w n="3.2">devant</w> <w n="3.3">le</w> <w n="3.4">regard</w> <w n="3.5">de</w> <w n="3.6">leur</w> <w n="3.7">banalité</w>,</l>
						<l n="4" num="1.4"><space unit="char" quantity="8"></space><w n="4.1">On</w> <w n="4.2">songe</w> : « <w n="4.3">Oui</w> !… <w n="4.4">Formalité</w>. »</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Ainsi</w> <w n="5.2">donc</w> <w n="5.3">il</w> <w n="5.4">n</w>’<w n="5.5">est</w> <w n="5.6">pas</w> <w n="5.7">d</w>’<w n="5.8">éternelle</w> <w n="5.9">épouvante</w></l>
						<l n="6" num="2.2"><space unit="char" quantity="8"></space><w n="6.1">Au</w> <w n="6.2">fond</w> <w n="6.3">de</w> <w n="6.4">votre</w> <w n="6.5">âme</w> <w n="6.6">vivante</w> ?</l>
						<l n="7" num="2.3"><w n="7.1">Vos</w> <w n="7.2">mères</w>, <w n="7.3">lorsque</w> <w n="7.4">l</w>’<w n="7.5">âge</w> <w n="7.6">a</w> <w n="7.7">raidi</w> <w n="7.8">leurs</w> <w n="7.9">genoux</w>,</l>
						<l n="8" num="2.4"><space unit="char" quantity="8"></space><w n="8.1">Sont</w> <w n="8.2">mortes</w> <w n="8.3">aussi</w> <w n="8.4">devant</w> <w n="8.5">vous</w> ?</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Ce</w> <w n="9.2">n</w>’<w n="9.3">est</w> <w n="9.4">sans</w> <w n="9.5">doute</w> <w n="9.6">pas</w> <w n="9.7">pour</w> <w n="9.8">nous</w> <w n="9.9">la</w> <w n="9.10">même</w> <w n="9.11">chose</w></l>
						<l n="10" num="3.2"><space unit="char" quantity="8"></space>— <w n="10.1">Frêle</w>, <w n="10.2">humble</w> <w n="10.3">et</w> <w n="10.4">jolie</w>, <w n="10.5">une</w> <w n="10.6">rose</w>,</l>
						<l n="11" num="3.3"><w n="11.1">Rose</w> <w n="11.2">toujours</w> <w n="11.3">cachée</w> <w n="11.4">et</w> <w n="11.5">toujours</w> <w n="11.6">parfumant</w>,</l>
						<l n="12" num="3.4"><space unit="char" quantity="8"></space><w n="12.1">Voilà</w> <w n="12.2">ce</w> <w n="12.3">qu</w>’<w n="12.4">elle</w> <w n="12.5">était</w>, <w n="12.6">maman</w></l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Rougissante</w> <w n="13.2">d</w>’<w n="13.3">avoir</w> <w n="13.4">en</w> <w n="13.5">elle</w> <w n="13.6">tous</w> <w n="13.7">les</w> <w n="13.8">doutes</w>,</l>
						<l n="14" num="4.2"><space unit="char" quantity="8"></space><w n="14.1">Elle</w> <w n="14.2">était</w> <w n="14.3">notre</w> <w n="14.4">enfant</w> <w n="14.5">à</w> <w n="14.6">toutes</w>,</l>
						<l n="15" num="4.3"><w n="15.1">Et</w>, <w n="15.2">chacune</w> <w n="15.3">de</w> <w n="15.4">nous</w>, <w n="15.5">l</w>’<w n="15.6">aimant</w> <w n="15.7">à</w> <w n="15.8">sa</w> <w n="15.9">façon</w>,</l>
						<l n="16" num="4.4"><space unit="char" quantity="8"></space><w n="16.1">Lui</w> <w n="16.2">faisait</w>, <w n="16.3">je</w> <w n="16.4">crois</w>, <w n="16.5">la</w> <w n="16.6">leçon</w></l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Si</w> <w n="17.2">petite</w> <w n="17.3">au</w> <w n="17.4">milieu</w> <w n="17.5">de</w> <w n="17.6">tant</w> <w n="17.7">de</w> <w n="17.8">grandes</w> <w n="17.9">filles</w></l>
						<l n="18" num="5.2"><space unit="char" quantity="8"></space><w n="18.1">Qui</w> <w n="18.2">recommençaient</w> <w n="18.3">des</w> <w n="18.4">familles</w>,</l>
						<l n="19" num="5.3"><w n="19.1">On</w> <w n="19.2">n</w>’<w n="19.3">aura</w> <w n="19.4">jamais</w> <w n="19.5">su</w> <w n="19.6">le</w> <w n="19.7">fond</w> <w n="19.8">mystérieux</w></l>
						<l n="20" num="5.4"><space unit="char" quantity="8"></space><w n="20.1">De</w> <w n="20.2">son</w> <w n="20.3">regard</w> <w n="20.4">aux</w> <w n="20.5">larges</w> <w n="20.6">yeux</w></l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Intimidée</w> <w n="21.2">et</w> <w n="21.3">douce</w>, <w n="21.4">un</w> <w n="21.5">peu</w> <w n="21.6">froide</w>, <w n="21.7">humble</w> <w n="21.8">et</w> <w n="21.9">bonne</w>,</l>
						<l n="22" num="6.2"><space unit="char" quantity="8"></space><w n="22.1">Était</w>-<w n="22.2">ce</w> <w n="22.3">une</w> <w n="22.4">grande</w> <w n="22.5">personne</w> ?</l>
						<l n="23" num="6.3"><w n="23.1">Quant</w> <w n="23.2">à</w> <w n="23.3">moi</w>, <w n="23.4">je</w> <w n="23.5">sais</w> <w n="23.6">bien</w> <w n="23.7">comme</w> <w n="23.8">mon</w> <w n="23.9">cœur</w> <w n="23.10">se</w> <w n="23.11">fend</w>,</l>
						<l n="24" num="6.4"><space unit="char" quantity="8"></space><w n="24.1">Et</w> <w n="24.2">que</w> <w n="24.3">j</w>’<w n="24.4">ai</w> <w n="24.5">perdu</w> <w n="24.6">mon</w> <w n="24.7">enfant</w>.</l>
					</lg>
				</div></body></text></TEI>