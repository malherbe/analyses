<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">A MAMAN</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1250 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_7</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title type="main">A MAMAN</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LIBRAIRIE CHARPENTIER ET FASQUELLE</publisher>
							<date when="1920">1920</date>
						</imprint>
					</monogr>
					<note>
						Édition numérisée.
						Merci à la bibliothèque de l’université de Denver (USA)
						qui a bien voulu prêté un exemplaire de cet ouvrage ; aucune bibliothèque
						de France n’ayant accepté le prêt ou la reproduction sur place.
					</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1920">1920</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>.....</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-11-28" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">I</head><head type="main_part">DÉDICACE</head><div type="poem" key="DLR662">
					<head type="main">DÉDICACE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Le</w> <w n="1.2">chagrin</w> <w n="1.3">pleure</w> <w n="1.4">et</w> <w n="1.5">chante</w>. <w n="1.6">Il</w> <w n="1.7">ressemble</w> <w n="1.8">à</w> <w n="1.9">l</w>’<w n="1.10">amour</w></l>
						<l n="2" num="1.2"><space unit="char" quantity="8"></space><w n="2.1">Il</w> <w n="2.2">nous</w> <w n="2.3">exalte</w>. <w n="2.4">Il</w> <w n="2.5">nous</w> <w n="2.6">enivre</w>.</l>
						<l n="3" num="1.3"><w n="3.1">C</w>’<w n="3.2">est</w> <w n="3.3">avec</w> <w n="3.4">mon</w> <w n="3.5">chagrin</w> <w n="3.6">que</w> <w n="3.7">je</w> <w n="3.8">t</w>’<w n="3.9">ai</w> <w n="3.10">fait</w> <w n="3.11">ce</w> <w n="3.12">livre</w>,</l>
						<l n="4" num="1.4"><space unit="char" quantity="8"></space><w n="4.1">Afin</w> <w n="4.2">qu</w>’<w n="4.3">il</w> <w n="4.4">te</w> <w n="4.5">fasse</w> <w n="4.6">revivre</w></l>
						<l n="5" num="1.5"><w n="5.1">Malgré</w> <w n="5.2">la</w> <w n="5.3">terre</w> <w n="5.4">épaisse</w> <w n="5.5">et</w> <w n="5.6">le</w> <w n="5.7">sépulcre</w> <w n="5.8">lourd</w>.</l>
					</lg>
					<lg n="2">
						<l n="6" num="2.1"><w n="6.1">Voici</w> <w n="6.2">ce</w> <w n="6.3">qu</w>’<w n="6.4">aujourd</w>’<w n="6.5">hui</w> <w n="6.6">tendrement</w> <w n="6.7">je</w> <w n="6.8">t</w>’<w n="6.9">élève</w></l>
						<l n="7" num="2.2"><space unit="char" quantity="8"></space><w n="7.1">Pour</w> <w n="7.2">te</w> <w n="7.3">tirer</w> <w n="7.4">d</w>’<w n="7.5">entre</w> <w n="7.6">les</w> <w n="7.7">morts</w>.</l>
						<l n="8" num="2.3"><w n="8.1">Au</w> <w n="8.2">temps</w> <w n="8.3">où</w>, <w n="8.4">lentement</w>, <w n="8.5">tu</w> <w n="8.6">me</w> <w n="8.7">donnais</w> <w n="8.8">ta</w> <w n="8.9">sève</w>,</l>
						<l n="9" num="2.4"><space unit="char" quantity="8"></space><w n="9.1">Quand</w> <w n="9.2">tu</w> <w n="9.3">me</w> <w n="9.4">portais</w> <w n="9.5">dans</w> <w n="9.6">ton</w> <w n="9.7">corps</w>,</l>
						<l n="10" num="2.5"><w n="10.1">Sentais</w>-<w n="10.2">tu</w> <w n="10.3">que</w> <w n="10.4">j</w>’<w n="10.5">étais</w> <w n="10.6">cette</w> <w n="10.7">fille</w> <w n="10.8">de</w> <w n="10.9">rêve</w> ?</l>
					</lg>
					<lg n="3">
						<l n="11" num="3.1"><w n="11.1">O</w> <w n="11.2">ma</w> <w n="11.3">mère</w> ! <w n="11.4">Déjà</w> <w n="11.5">n</w>’<w n="11.6">étais</w>-<w n="11.7">je</w> <w n="11.8">pas</w> <w n="11.9">pour</w> <w n="11.10">toi</w>,</l>
						<l n="12" num="3.2"><space unit="char" quantity="8"></space><w n="12.1">Merveilleusement</w>, <w n="12.2">ton</w> <w n="12.3">poète</w> ?</l>
						<l n="13" num="3.3"><w n="13.1">Non</w>. <w n="13.2">Ma</w> <w n="13.3">naissance</w> <w n="13.4">à</w> <w n="13.5">moi</w> <w n="13.6">ne</w> <w n="13.7">fut</w> <w n="13.8">pas</w> <w n="13.9">une</w> <w n="13.10">fête</w>.</l>
						<l n="14" num="3.4"><space unit="char" quantity="8"></space><w n="14.1">Tu</w> <w n="14.2">n</w>’<w n="14.3">as</w> <w n="14.4">pas</w> <w n="14.5">vu</w> <w n="14.6">briller</w> <w n="14.7">ma</w> <w n="14.8">tête</w>.</l>
						<l n="15" num="3.5"><w n="15.1">Je</w> <w n="15.2">n</w>’<w n="15.3">étais</w> <w n="15.4">qu</w>’<w n="15.5">une</w> <w n="15.6">enfant</w> <w n="15.7">de</w> <w n="15.8">plus</w> <w n="15.9">sous</w> <w n="15.10">le</w> <w n="15.11">grand</w> <w n="15.12">toit</w></l>
					</lg>
					<lg n="4">
						<l n="16" num="4.1"><w n="16.1">Je</w> <w n="16.2">venais</w> <w n="16.3">à</w> <w n="16.4">mon</w> <w n="16.5">tour</w> <w n="16.6">après</w> <w n="16.7">mes</w> <w n="16.8">sœurs</w> <w n="16.9">aînées</w>,</l>
						<l n="17" num="4.2"><space unit="char" quantity="8"></space><w n="17.1">Et</w> <w n="17.2">nul</w>, <w n="17.3">quand</w> <w n="17.4">je</w> <w n="17.5">me</w> <w n="17.6">tins</w> <w n="17.7">debout</w>,</l>
						<l n="18" num="4.3"><w n="18.1">Ne</w> <w n="18.2">vit</w> <w n="18.3">derrière</w> <w n="18.4">moi</w> <w n="18.5">comme</w> <w n="18.6">un</w> <w n="18.7">grand</w> <w n="18.8">ange</w> <w n="18.9">fou</w>.</l>
						<l n="19" num="4.4"><space unit="char" quantity="8"></space><w n="19.1">J</w>’<w n="19.2">étais</w> <w n="19.3">la</w> <w n="19.4">dernière</w>, <w n="19.5">et</w> <w n="19.6">c</w>’<w n="19.7">est</w> <w n="19.8">tout</w>,</l>
						<l n="20" num="4.5"><w n="20.1">Et</w> <w n="20.2">je</w> <w n="20.3">n</w>’<w n="20.4">avais</w> <w n="20.5">pour</w> <w n="20.6">moi</w> <w n="20.7">que</w> <w n="20.8">mes</w> <w n="20.9">courtes</w> <w n="20.10">années</w></l>
					</lg>
					<lg n="5">
						<l n="21" num="5.1"><w n="21.1">La</w> <w n="21.2">plus</w> <w n="21.3">petite</w>, <w n="21.4">oui</w> ! <w n="21.5">toujours</w>, <w n="21.6">jusqu</w>’<w n="21.7">à</w> <w n="21.8">la</w> <w n="21.9">fin</w> ;</l>
						<l n="22" num="5.2"><space unit="char" quantity="8"></space><w n="22.1">La</w> <w n="22.2">plus</w> <w n="22.3">petite</w> <w n="22.4">et</w> <w n="22.5">la</w> <w n="22.6">plus</w> <w n="22.7">douce</w>.</l>
						<l n="23" num="5.3"><w n="23.1">Toutes</w>, <w n="23.2">tu</w> <w n="23.3">nous</w> <w n="23.4">soignais</w> <w n="23.5">comme</w> <w n="23.6">un</w> <w n="23.7">jardin</w> <w n="23.8">qui</w> <w n="23.9">pousse</w></l>
						<l n="24" num="5.4"><space unit="char" quantity="8"></space><w n="24.1">Comme</w> <w n="24.2">des</w> <w n="24.3">oiseaux</w> <w n="24.4">dans</w> <w n="24.5">la</w> <w n="24.6">mousse</w>.</l>
						<l n="25" num="5.5"><w n="25.1">Mais</w> <w n="25.2">aujourd</w>’<w n="25.3">hui</w> <w n="25.4">c</w>’<w n="25.5">est</w> <w n="25.6">moi</w> <w n="25.7">qui</w> <w n="25.8">suis</w> <w n="25.9">l</w>’<w n="25.10">aînée</w>, <w n="25.11">enfin</w> !</l>
					</lg>
					<lg n="6">
						<l n="26" num="6.1"><w n="26.1">J</w>’<w n="26.2">étais</w> <w n="26.3">la</w> <w n="26.4">plus</w> <w n="26.5">petite</w> <w n="26.6">et</w> <w n="26.7">j</w>’<w n="26.8">en</w> <w n="26.9">étais</w> <w n="26.10">heureuse</w>.</l>
						<l n="27" num="6.2"><space unit="char" quantity="8"></space><w n="27.1">Ceci</w> <w n="27.2">n</w>’<w n="27.3">est</w> <w n="27.4">pas</w> <w n="27.5">un</w> <w n="27.6">cri</w> <w n="27.7">d</w>’<w n="27.8">orgueil</w>.</l>
						<l n="28" num="6.3"><w n="28.1">Ceci</w> <w n="28.2">n</w>’<w n="28.3">est</w> <w n="28.4">que</w> <w n="28.5">ferveur</w>, <w n="28.6">grande</w> <w n="28.7">tendresse</w>, <w n="28.8">deuil</w>,</l>
						<l n="29" num="6.4"><space unit="char" quantity="8"></space><w n="29.1">Et</w>, <w n="29.2">devant</w> <w n="29.3">le</w> <w n="29.4">suprême</w> <w n="29.5">seuil</w>,</l>
						<l n="30" num="6.5"><w n="30.1">Qu</w>’<w n="30.2">un</w> <w n="30.3">chant</w> <w n="30.4">pour</w> <w n="30.5">que</w> <w n="30.6">ta</w> <w n="30.7">mort</w> <w n="30.8">paraisse</w> <w n="30.9">moins</w> <w n="30.10">affreuse</w></l>
					</lg>
					<lg n="7">
						<l n="31" num="7.1"><w n="31.1">Un</w> <w n="31.2">poète</w> ! <w n="31.3">Pouvoir</w>, <w n="31.4">dans</w> <w n="31.5">ce</w> <w n="31.6">langage</w>-<w n="31.7">là</w>,</l>
						<l n="32" num="7.2"><space unit="char" quantity="8"></space><w n="32.1">Dire</w> <w n="32.2">un</w> <w n="32.3">peu</w> <w n="32.4">ta</w> <w n="32.5">bonté</w>, <w n="32.6">ton</w> <w n="32.7">charme</w>,</l>
						<l n="33" num="7.3"><w n="33.1">Tout</w> <w n="33.2">ce</w> <w n="33.3">qu</w>’<w n="33.4">aimèrent</w> <w n="33.5">ceux</w> <w n="33.6">que</w> <w n="33.7">le</w> <w n="33.8">destin</w> <w n="33.9">désarme</w>,</l>
						<l n="34" num="7.4"><space unit="char" quantity="8"></space><w n="34.1">Parer</w> <w n="34.2">ta</w> <w n="34.3">mort</w> <w n="34.4">de</w> <w n="34.5">cette</w> <w n="34.6">larme</w>,</l>
						<l n="35" num="7.5"><w n="35.1">Voilà</w> <w n="35.2">tout</w> <w n="35.3">mon</w> <w n="35.4">orgueil</w> <w n="35.5">du</w> <w n="35.6">livre</w> <w n="35.7">que</w> <w n="35.8">voilà</w></l>
					</lg>
					<lg n="8">
						<l n="36" num="8.1"><w n="36.1">Ce</w> <w n="36.2">livre</w> <w n="36.3">filial</w> <w n="36.4">né</w> <w n="36.5">de</w> <w n="36.6">mon</w> <w n="36.7">âme</w> <w n="36.8">amère</w>,</l>
						<l n="37" num="8.2"><space unit="char" quantity="8"></space><w n="37.1">Ce</w> <w n="37.2">poème</w>, <w n="37.3">gerbe</w> <w n="37.4">de</w> <w n="37.5">fleurs</w>,</l>
						<l n="38" num="8.3"><w n="38.1">C</w>’<w n="38.2">est</w> <w n="38.3">toute</w> <w n="38.4">ma</w> <w n="38.5">tristesse</w> <w n="38.6">et</w> <w n="38.7">celle</w> <w n="38.8">de</w> <w n="38.9">mes</w> <w n="38.10">sœurs</w>…</l>
						<l n="39" num="8.4"><space unit="char" quantity="8"></space><w n="39.1">Et</w> <w n="39.2">puisse</w>-<w n="39.3">t</w>-<w n="39.4">il</w> <w n="39.5">bercer</w> <w n="39.6">les</w> <w n="39.7">cœurs</w></l>
						<l n="40" num="8.5"><w n="40.1">De</w> <w n="40.2">ceux</w> <w n="40.3">qui</w>, <w n="40.4">comme</w> <w n="40.5">nous</w>, <w n="40.6">pleurent</w> <w n="40.7">tout</w> <w n="40.8">bas</w> <w n="40.9">leur</w> <w n="40.10">mère</w>.</l>
					</lg>
				</div></body></text></TEI>