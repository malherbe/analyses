<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">A MAMAN</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1250 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_7</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title type="main">A MAMAN</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LIBRAIRIE CHARPENTIER ET FASQUELLE</publisher>
							<date when="1920">1920</date>
						</imprint>
					</monogr>
					<note>
						Édition numérisée.
						Merci à la bibliothèque de l’université de Denver (USA)
						qui a bien voulu prêté un exemplaire de cet ouvrage ; aucune bibliothèque
						de France n’ayant accepté le prêt ou la reproduction sur place.
					</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1920">1920</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>.....</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-11-28" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">VI</head><head type="main_part">MÉDITATIONS</head><div type="poem" key="DLR699">
					<head type="main">HANTISE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Toujours</w>, <w n="1.2">toujours</w> <w n="1.3">je</w> <w n="1.4">pense</w> <w n="1.5">aux</w> <w n="1.6">pauvres</w> <w n="1.7">vieilles</w> <w n="1.8">femmes</w></l>
						<l n="2" num="1.2"><w n="2.1">Qui</w> <w n="2.2">s</w>’<w n="2.3">en</w> <w n="2.4">vont</w> <w n="2.5">tristement</w> <w n="2.6">finir</w> <w n="2.7">à</w> <w n="2.8">l</w>’<w n="2.9">hôpital</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Qui</w> <w n="3.2">pendant</w> <w n="3.3">de</w> <w n="3.4">longs</w> <w n="3.5">jours</w>, <w n="3.6">et</w> <w n="3.7">quand</w> <w n="3.8">elles</w> <w n="3.9">ont</w> <w n="3.10">mal</w>,</l>
						<l n="4" num="1.4"><space unit="char" quantity="8"></space><w n="4.1">N</w>’<w n="4.2">ont</w> <w n="4.3">personne</w> <w n="4.4">autour</w> <w n="4.5">de</w> <w n="4.6">leurs</w> <w n="4.7">âmes</w></l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">O</w> <w n="5.2">pauvres</w> <w n="5.3">corps</w> <w n="5.4">jetés</w> <w n="5.5">dans</w> <w n="5.6">les</w> <w n="5.7">lits</w> <w n="5.8">douloureux</w>,</l>
						<l n="6" num="2.2"><w n="6.1">Rien</w>, <w n="6.2">rien</w> <w n="6.3">qu</w>’<w n="6.4">indifférence</w> <w n="6.5">autour</w> <w n="6.6">de</w> <w n="6.7">vos</w> <w n="6.8">supplices</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Et</w> <w n="7.2">lorsque</w> <w n="7.3">vous</w> <w n="7.4">avez</w> <w n="7.5">de</w> <w n="7.6">ces</w> <w n="7.7">petits</w> <w n="7.8">caprices</w>,</l>
						<l n="8" num="2.4"><space unit="char" quantity="8"></space><w n="8.1">Pas</w> <w n="8.2">d</w>’<w n="8.3">enfant</w> <w n="8.4">penché</w> <w n="8.5">sur</w> <w n="8.6">vos</w> <w n="8.7">yeux</w></l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Pas</w> <w n="9.2">de</w> <w n="9.3">cœur</w> <w n="9.4">déchiré</w> <w n="9.5">près</w> <w n="9.6">de</w> <w n="9.7">votre</w> <w n="9.8">agonie</w>,</l>
						<l n="10" num="3.2"><w n="10.1">Personne</w> <w n="10.2">qui</w> <w n="10.3">sanglote</w> <w n="10.4">en</w> <w n="10.5">appelant</w> : « <w n="10.6">Maman</w> ».</l>
						<l n="11" num="3.3"><w n="11.1">Nulles</w> <w n="11.2">lèvres</w>, <w n="11.3">aux</w> <w n="11.4">mains</w> <w n="11.5">d</w>’<w n="11.6">une</w> <w n="11.7">mère</w> <w n="11.8">bénie</w>,</l>
						<l n="12" num="3.4"><space unit="char" quantity="8"></space><w n="12.1">Les</w> <w n="12.2">baisant</w> <w n="12.3">passionnément</w></l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Nul</w> <w n="13.2">visage</w> <w n="13.3">au</w> <w n="13.4">chevet</w> <w n="13.5">L</w>’<w n="13.6">infirmière</w> <w n="13.7">marâtre</w>,</l>
						<l n="14" num="4.2"><w n="14.1">Qui</w>, <w n="14.2">lorsqu</w>’<w n="14.3">enfin</w> <w n="14.4">la</w> <w n="14.5">mort</w> <w n="14.6">aura</w> <w n="14.7">calmé</w> <w n="14.8">vos</w> <w n="14.9">traits</w></l>
						<l n="15" num="4.3"><w n="15.1">Ne</w> <w n="15.2">songera</w> <w n="15.3">pour</w> <w n="15.4">vous</w> <w n="15.5">qu</w>’<w n="15.6">à</w> <w n="15.7">d</w>’<w n="15.8">horribles</w> <w n="15.9">apprêts</w></l>
						<l n="16" num="4.4"><space unit="char" quantity="8"></space><w n="16.1">Pour</w> <w n="16.2">la</w> <w n="16.3">morgue</w> <w n="16.4">ou</w> <w n="16.5">l</w>’<w n="16.6">amphithéâtre</w>…</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Toujours</w>, <w n="17.2">toujours</w> <w n="17.3">je</w> <w n="17.4">pense</w> <w n="17.5">en</w> <w n="17.6">pleurant</w> <w n="17.7">à</w> <w n="17.8">cela</w>,</l>
						<l n="18" num="5.2"><w n="18.1">Depuis</w> <w n="18.2">que</w> <w n="18.3">j</w>’<w n="18.4">ai</w> <w n="18.5">bercé</w> <w n="18.6">ma</w> <w n="18.7">mère</w> <w n="18.8">agonisante</w>.</l>
						<l n="19" num="5.3"><w n="19.1">Et</w>, <w n="19.2">dans</w> <w n="19.3">un</w> <w n="19.4">rêve</w> <w n="19.5">affreux</w> <w n="19.6">qui</w> <w n="19.7">sans</w> <w n="19.8">cesse</w> <w n="19.9">me</w> <w n="19.10">hante</w>,</l>
						<l n="20" num="5.4"><space unit="char" quantity="8"></space><w n="20.1">Je</w> <w n="20.2">vois</w> <w n="20.3">mourir</w> <w n="20.4">ces</w> <w n="20.5">vieilles</w>-<w n="20.6">là</w>.</l>
					</lg>
				</div></body></text></TEI>