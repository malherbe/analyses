<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">A MAMAN</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1250 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_7</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title type="main">A MAMAN</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LIBRAIRIE CHARPENTIER ET FASQUELLE</publisher>
							<date when="1920">1920</date>
						</imprint>
					</monogr>
					<note>
						Édition numérisée.
						Merci à la bibliothèque de l’université de Denver (USA)
						qui a bien voulu prêté un exemplaire de cet ouvrage ; aucune bibliothèque
						de France n’ayant accepté le prêt ou la reproduction sur place.
					</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1920">1920</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>.....</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-11-28" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">II</head><head type="main_part">DE PROFUNDIS</head><div type="poem" key="DLR672">
					<head type="main">SAINT SYLVESTRE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Voici</w> : <w n="1.2">Pour</w> <w n="1.3">la</w> <w n="1.4">première</w> <w n="1.5">fois</w></l>
						<l n="2" num="1.2"><w n="2.1">Je</w> <w n="2.2">ne</w> <w n="2.3">t</w>’<w n="2.4">ai</w> <w n="2.5">pas</w> <w n="2.6">dit</w> : « <w n="2.7">Bonne</w> <w n="2.8">Année</w> ! »</l>
						<l n="3" num="1.3"><w n="3.1">Cette</w> <w n="3.2">couronne</w> <w n="3.3">lourde</w> <w n="3.4">aux</w> <w n="3.5">doigts</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Sans</w> <w n="4.2">parler</w> <w n="4.3">je</w> <w n="4.4">te</w> <w n="4.5">l</w>’<w n="4.6">ai</w> <w n="4.7">donnée</w></l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Seule</w> <w n="5.2">et</w> <w n="5.3">noire</w> <w n="5.4">dans</w> <w n="5.5">l</w>’<w n="5.6">hiver</w> <w n="5.7">blanc</w>,</l>
						<l n="6" num="2.2"><w n="6.1">Sur</w> <w n="6.2">ta</w> <w n="6.3">tombe</w> <w n="6.4">où</w> <w n="6.5">l</w>’<w n="6.6">arbre</w> <w n="6.7">s</w>’<w n="6.8">incline</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Je</w> <w n="7.2">venais</w>, <w n="7.3">pour</w> <w n="7.4">le</w> <w n="7.5">nouvel</w> <w n="7.6">an</w>,</l>
						<l n="8" num="2.4"><w n="8.1">Pleurer</w> <w n="8.2">mes</w> <w n="8.3">larmes</w> <w n="8.4">d</w>’<w n="8.5">orpheline</w></l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">La</w> <w n="9.2">mort</w> <w n="9.3">habitait</w> <w n="9.4">les</w> <w n="9.5">tombeaux</w>,</l>
						<l n="10" num="3.2"><w n="10.1">L</w>’<w n="10.2">hiver</w> <w n="10.3">mettait</w> <w n="10.4">dessus</w> <w n="10.5">sa</w> <w n="10.6">neige</w>.</l>
						<l n="11" num="3.3"><w n="11.1">Et</w> <w n="11.2">moi</w>, <w n="11.3">retenant</w> <w n="11.4">mes</w> <w n="11.5">sanglots</w>,</l>
						<l n="12" num="3.4">« <w n="12.1">C</w>’<w n="12.2">est</w> <w n="12.3">le</w> <w n="12.4">froid</w> <w n="12.5">partout</w>, <w n="12.6">me</w> <w n="12.7">disais</w>-<w n="12.8">je</w>. »</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Cet</w> <w n="13.2">an</w> <w n="13.3">qui</w> <w n="13.4">meurt</w>, <w n="13.5">c</w>’<w n="13.6">était</w> <w n="13.7">encor</w></l>
						<l n="14" num="4.2"><w n="14.1">Un</w> <w n="14.2">morceau</w> <w n="14.3">de</w> <w n="14.4">ton</w> <w n="14.5">existence</w>.</l>
						<l n="15" num="4.3"><w n="15.1">C</w>’<w n="15.2">était</w> <w n="15.3">la</w> <w n="15.4">date</w> <w n="15.5">de</w> <w n="15.6">ta</w> <w n="15.7">mort</w>…</l>
						<l n="16" num="4.4"><w n="16.1">Demain</w>, <w n="16.2">demain</w>, <w n="16.3">quelle</w> <w n="16.4">distance</w> !</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Demain</w> <w n="17.2">commence</w> <w n="17.3">l</w>’<w n="17.4">an</w> <w n="17.5">nouveau</w>.</l>
						<l n="18" num="5.2"><w n="18.1">Pour</w> <w n="18.2">lequel</w> <w n="18.3">tu</w> <w n="18.4">n</w>’<w n="18.5">es</w> <w n="18.6">qu</w>’<w n="18.7">une</w> <w n="18.8">morte</w>.</l>
						<l n="19" num="5.3"><w n="19.1">Vœux</w> ? <w n="19.2">Souhaits</w> ?… <w n="19.3">Plus</w> <w n="19.4">rien</w>-<w n="19.5">ne</w> <w n="19.6">t</w>’<w n="19.7">importe</w></l>
						<l n="20" num="5.4"><w n="20.1">Quêta</w> <w n="20.2">place</w> <w n="20.3">dans</w> <w n="20.4">ton</w> <w n="20.5">caveau</w></l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Comme</w> <w n="21.2">tout</w> <w n="21.3">mon</w> <w n="21.4">être</w> <w n="21.5">se</w> <w n="21.6">cabre</w> !</l>
						<l n="22" num="6.2"><w n="22.1">Là</w>, <w n="22.2">sous</w> <w n="22.3">cette</w> <w n="22.4">pierre</w>, <w n="22.5">maman</w>,</l>
						<l n="23" num="6.3"><w n="23.1">En</w> <w n="23.2">somme</w> <w n="23.3">peu</w> <w n="23.4">profondément</w>,</l>
						<l n="24" num="6.4"><w n="24.1">Tu</w> <w n="24.2">n</w>’<w n="24.3">es</w> <w n="24.4">plus</w> <w n="24.5">qu</w>’<w n="24.6">un</w> <w n="24.7">objet</w> <w n="24.8">macabre</w></l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">Tu</w> <w n="25.2">ne</w> <w n="25.3">me</w> <w n="25.4">verras</w> <w n="25.5">jamais</w> <w n="25.6">plus</w>.</l>
						<l n="26" num="7.2"><w n="26.1">Pour</w> <w n="26.2">toi</w> <w n="26.3">ma</w> <w n="26.4">vie</w> <w n="26.5">est</w> <w n="26.6">terminée</w>.</l>
						<l n="27" num="7.3"><w n="27.1">Tant</w> <w n="27.2">de</w> <w n="27.3">jours</w>, <w n="27.4">ce</w> <w n="27.5">soir</w> <w n="27.6">révolus</w>,</l>
						<l n="28" num="7.4"><w n="28.1">C</w>’<w n="28.2">est</w>, <w n="28.3">pour</w> <w n="28.4">toi</w>, <w n="28.5">ma</w> <w n="28.6">dernière</w> <w n="28.7">année</w></l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1"><w n="29.1">L</w>’<w n="29.2">an</w> <w n="29.3">neuf</w> <w n="29.4">qui</w> <w n="29.5">te</w> <w n="29.6">reste</w> <w n="29.7">étranger</w></l>
						<l n="30" num="8.2"><w n="30.1">Sera</w> <w n="30.2">donc</w> <w n="30.3">le</w> <w n="30.4">début</w> <w n="30.5">d</w>’<w n="30.6">une</w> <w n="30.7">ère</w>.</l>
						<l n="31" num="8.3"><w n="31.1">Je</w> <w n="31.2">vivrai</w> <w n="31.3">maintenant</w> <w n="31.4">sans</w> <w n="31.5">mère</w>,</l>
						<l n="32" num="8.4"><w n="32.1">Tu</w> <w n="32.2">ne</w> <w n="32.3">me</w> <w n="32.4">verras</w> <w n="32.5">plus</w> <w n="32.6">changer</w></l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1"><w n="33.1">Adieu</w>, <w n="33.2">maman</w> ! <w n="33.3">Sans</w> <w n="33.4">nulle</w> <w n="33.5">envie</w></l>
						<l n="34" num="9.2"><w n="34.1">Je</w> <w n="34.2">vais</w> <w n="34.3">vers</w> <w n="34.4">la</w> <w n="34.5">maturité</w>.</l>
						<l n="35" num="9.3"><w n="35.1">Ce</w> <w n="35.2">soir</w>, <w n="35.3">la</w> <w n="35.4">moitié</w> <w n="35.5">de</w> <w n="35.6">ma</w> <w n="35.7">vie</w></l>
						<l n="36" num="9.4"><w n="36.1">S</w>’<w n="36.2">engouffre</w> <w n="36.3">dans</w> <w n="36.4">l</w>’<w n="36.5">éternité</w></l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1"><w n="37.1">Adieu</w> <w n="37.2">maman</w>, <w n="37.3">tombe</w> <w n="37.4">carrée</w> !</l>
						<l n="38" num="10.2"> <w n="38.1">Hélas</w> ! <w n="38.2">Quand</w> <w n="38.3">minuit</w> <w n="38.4">sonnera</w>,</l>
						<l n="39" num="10.3"><w n="39.1">Froide</w>, <w n="39.2">noire</w>, <w n="39.3">commencera</w></l>
						<l n="40" num="10.4"><w n="40.1">Ta</w> <w n="40.2">première</w> <w n="40.3">année</w> <w n="40.4">enterrée</w>…</l>
					</lg>
				</div></body></text></TEI>