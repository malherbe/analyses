<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CHOIX DE POÈMES</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1917 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">DLR_13</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Choix de poèmes</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie Alphonse Lemerre</publisher>
							<date when="1951">1951</date>
						</imprint>
					</monogr>
					<note>Édition numérisée à partir d’un ouvrage prêté par la BPU de neuchatel</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1951">1951</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La partie en prose (FRAGMENT D’UN TEXTE DE L’AUTEUR PRÉFAÇANT DEUX POÈMES D’EDGAR POE) n’est incluse.</p>
				<p>Les indications de la première édition d’un poème données dans la table des matières ont été reportées comme sous-titre des poèmes. L’année d’édition a été ajoutée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell (correction d’erreur d’océrisation).</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-01-14" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-01-14" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CHAPITRE II</head><head type="main_part">Honfleur</head><div type="poem" key="DLR1057">
					<head type="main">Adieu</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">J</w>’<w n="1.2">ai</w> <w n="1.3">vendu</w> <w n="1.4">ma</w> <w n="1.5">maison</w> <w n="1.6">d</w>’<w n="1.7">où</w> <w n="1.8">me</w> <w n="1.9">chassait</w> <w n="1.10">la</w> <w n="1.11">vie</w>,</l>
						<l n="2" num="1.2"><space unit="char" quantity="8"></space><w n="2.1">Et</w> <w n="2.2">j</w>’<w n="2.3">ai</w> <w n="2.4">froid</w> <w n="2.5">jusque</w> <w n="2.6">dans</w> <w n="2.7">le</w> <w n="2.8">cœur</w>.</l>
						<l n="3" num="1.3"><space unit="char" quantity="8"></space><w n="3.1">Il</w> <w n="3.2">m</w>’<w n="3.3">arrive</w> <w n="3.4">ce</w> <w n="3.5">grand</w> <w n="3.6">malheur</w>,</l>
						<l n="4" num="1.4"><space unit="char" quantity="8"></space><w n="4.1">Ma</w> <w n="4.2">route</w> <w n="4.3">bifurque</w>  — <w n="4.4">ou</w> <w n="4.5">dévie</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">D</w>’<w n="5.2">autres</w> <w n="5.3">vont</w> <w n="5.4">donc</w> <w n="5.5">hanter</w> <w n="5.6">le</w> <w n="5.7">magnifique</w> <w n="5.8">lieu</w></l>
						<l n="6" num="2.2"><space unit="char" quantity="8"></space><w n="6.1">Où</w> <w n="6.2">je</w> <w n="6.3">promenais</w> <w n="6.4">ma</w> <w n="6.5">tristesse</w>.</l>
						<l n="7" num="2.3"><space unit="char" quantity="8"></space><w n="7.1">Belle</w> <w n="7.2">maison</w> <w n="7.3">aimée</w>, <w n="7.4">adieu</w> !</l>
						<l n="8" num="2.4"><space unit="char" quantity="8"></space><w n="8.1">Sois</w> <w n="8.2">le</w> <w n="8.3">tombeau</w> <w n="8.4">de</w> <w n="8.5">ma</w> <w n="8.6">jeunesse</w> !</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">L</w>’<w n="9.2">âge</w>, <w n="9.3">la</w> <w n="9.4">malveillance</w> <w n="9.5">et</w> <w n="9.6">tout</w> <w n="9.7">le</w> <w n="9.8">reste</w> <w n="9.9">ont</w> <w n="9.10">fait</w></l>
						<l n="10" num="3.2"><space unit="char" quantity="8"></space><w n="10.1">Que</w> <w n="10.2">je</w> <w n="10.3">ne</w> <w n="10.4">pouvais</w> <w n="10.5">plus</w> <w n="10.6">y</w> <w n="10.7">vivre</w>.</l>
						<l n="11" num="3.3"><space unit="char" quantity="8"></space><w n="11.1">Mais</w> <w n="11.2">à</w> <w n="11.3">présent</w> <w n="11.4">quel</w> <w n="11.5">chemin</w> <w n="11.6">suivre</w> ?</l>
						<l n="12" num="3.4"><space unit="char" quantity="8"></space><w n="12.1">Devant</w> <w n="12.2">mes</w> <w n="12.3">yeux</w> <w n="12.4">tout</w> <w n="12.5">se</w> <w n="12.6">défait</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Ce</w> <w n="13.2">fut</w> <w n="13.3">l</w>’<w n="13.4">enfance</w>, <w n="13.5">et</w> <w n="13.6">puis</w> <w n="13.7">ce</w> <w n="13.8">fut</w> <w n="13.9">l</w>’<w n="13.10">adolescence</w></l>
						<l n="14" num="4.2"><space unit="char" quantity="8"></space><w n="14.1">Et</w> <w n="14.2">la</w> <w n="14.3">jeunesse</w>. <w n="14.4">Maintenant</w>,</l>
						<l n="15" num="4.3"><space unit="char" quantity="8"></space><w n="15.1">J</w>’<w n="15.2">en</w> <w n="15.3">arrive</w> <w n="15.4">au</w> <w n="15.5">dernier</w> <w n="15.6">tournant</w> ;</l>
						<l n="16" num="4.4"><space unit="char" quantity="8"></space><w n="16.1">C</w>’<w n="16.2">est</w> <w n="16.3">déjà</w> <w n="16.4">la</w> <w n="16.5">mort</w> <w n="16.6">qui</w> <w n="16.7">commence</w>.</l>
					</lg>
				</div></body></text></TEI>