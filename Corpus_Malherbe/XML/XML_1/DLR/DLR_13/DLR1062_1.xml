<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CHOIX DE POÈMES</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1917 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">DLR_13</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Choix de poèmes</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie Alphonse Lemerre</publisher>
							<date when="1951">1951</date>
						</imprint>
					</monogr>
					<note>Édition numérisée à partir d’un ouvrage prêté par la BPU de neuchatel</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1951">1951</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La partie en prose (FRAGMENT D’UN TEXTE DE L’AUTEUR PRÉFAÇANT DEUX POÈMES D’EDGAR POE) n’est incluse.</p>
				<p>Les indications de la première édition d’un poème données dans la table des matières ont été reportées comme sous-titre des poèmes. L’année d’édition a été ajoutée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell (correction d’erreur d’océrisation).</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-01-14" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-01-14" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CHAPITRE III</head><head type="main_part">Chevauchées</head><div type="poem" key="DLR1062">
					<head type="main">Chasse</head>
					<head type="sub_2">SOUFFLES DE TEMPÊTE, Fasquelle, 1918</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Invisible</w> <w n="1.2">cerf</w> <w n="1.3">que</w> <w n="1.4">je</w> <w n="1.5">veux</w> <w n="1.6">forcer</w></l>
						<l n="2" num="1.2"><w n="2.1">Dans</w> <w n="2.2">l</w>’<w n="2.3">automne</w> <w n="2.4">d</w>’<w n="2.5">or</w> <w n="2.6">flamboyante</w> <w n="2.7">et</w> <w n="2.8">morte</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Invisibles</w> <w n="3.2">chiens</w>, <w n="3.3">invisible</w> <w n="3.4">escorte</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Quels</w> <w n="4.2">yeux</w> <w n="4.3">que</w> <w n="4.4">les</w> <w n="4.5">miens</w> <w n="4.6">vous</w> <w n="4.7">verront</w> <w n="4.8">passer</w> ?</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Mon</w> <w n="5.2">cheval</w> <w n="5.3">réel</w> <w n="5.4">a</w> <w n="5.5">peur</w> <w n="5.6">des</w> <w n="5.7">fantômes</w>,</l>
						<l n="6" num="2.2"><w n="6.1">Moi</w>, <w n="6.2">presque</w> <w n="6.3">un</w> <w n="6.4">esprit</w>, <w n="6.5">j</w>’<w n="6.6">ai</w> <w n="6.7">peur</w> <w n="6.8">des</w> <w n="6.9">vivants</w>.</l>
						<l n="7" num="2.3"><w n="7.1">Qui</w> <w n="7.2">verra</w> <w n="7.3">la</w> <w n="7.4">reine</w> <w n="7.5">aux</w> <w n="7.6">yeux</w> <w n="7.7">émouvants</w></l>
						<l n="8" num="2.4"><w n="8.1">Parcourir</w> <w n="8.2">son</w> <w n="8.3">rêve</w> <w n="8.4">aux</w> <w n="8.5">vastes</w> <w n="8.6">royaumes</w> ?</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Les</w> <w n="9.2">traces</w> <w n="9.3">du</w> <w n="9.4">cerf</w> <w n="9.5">sont</w> <w n="9.6">dans</w> <w n="9.7">le</w> <w n="9.8">hallier</w>.</l>
						<l n="10" num="3.2"><w n="10.1">Il</w> <w n="10.2">y</w> <w n="10.3">a</w> <w n="10.4">du</w> <w n="10.5">sang</w> <w n="10.6">jusque</w> <w n="10.7">sur</w> <w n="10.8">ce</w> <w n="10.9">hêtre</w>.</l>
						<l n="11" num="3.3"><w n="11.1">J</w>’<w n="11.2">entends</w> <w n="11.3">alentour</w> <w n="11.4">les</w> <w n="11.5">chiens</w> <w n="11.6">aboyer</w>.</l>
						<l n="12" num="3.4"><w n="12.1">Ma</w> <w n="12.2">meute</w> <w n="12.3">glapit</w>… <w n="12.4">C</w>’<w n="12.5">est</w> <w n="12.6">le</w> <w n="12.7">vent</w> <w n="12.8">peut</w>-<w n="12.9">être</w> ?</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Quelqu</w>’<w n="13.2">un</w>, <w n="13.3">il</w> <w n="13.4">me</w> <w n="13.5">semble</w>, <w n="13.6">a</w> <w n="13.7">sonné</w> <w n="13.8">du</w> <w n="13.9">cor</w>.</l>
						<l n="14" num="4.2"><w n="14.1">Est</w>-<w n="14.2">ce</w> <w n="14.3">pour</w> <w n="14.4">la</w> <w n="14.5">vue</w> <w n="14.6">ou</w> <w n="14.7">pour</w> <w n="14.8">la</w> <w n="14.9">curée</w> ?</l>
						<l n="15" num="4.3"><w n="15.1">Le</w> <w n="15.2">cerf</w> <w n="15.3">n</w>’<w n="15.4">est</w> <w n="15.5">pas</w> <w n="15.6">là</w>, <w n="15.7">le</w> <w n="15.8">cerf</w> <w n="15.9">n</w>’<w n="15.10">est</w> <w n="15.11">pas</w> <w n="15.12">mort</w>,</l>
						<l n="16" num="4.4"><w n="16.1">Le</w> <w n="16.2">cerf</w> <w n="16.3">court</w> <w n="16.4">toujours</w> <w n="16.5">l</w>’<w n="16.6">automne</w> <w n="16.7">empourprée</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Taïaut</w> ! <w n="17.2">Dans</w> <w n="17.3">le</w> <w n="17.4">soir</w> <w n="17.5">je</w> <w n="17.6">l</w>’<w n="17.7">ai</w> <w n="17.8">vu</w>, <w n="17.9">je</w> <w n="17.10">crois</w> !</l>
						<l n="18" num="5.2"><w n="18.1">Était</w>-<w n="18.2">ce</w> <w n="18.3">sa</w> <w n="18.4">tête</w> ? <w n="18.5">Était</w>-<w n="18.6">ce</w> <w n="18.7">une</w> <w n="18.8">branche</w> ?</l>
						<l n="19" num="5.3"><w n="19.1">Il</w> <w n="19.2">portait</w> <w n="19.3">au</w> <w n="19.4">vent</w>, <w n="19.5">recourbée</w> <w n="19.6">et</w> <w n="19.7">blanche</w>,</l>
						<l n="20" num="5.4"><w n="20.1">La</w> <w n="20.2">nouvelle</w> <w n="20.3">lune</w> <w n="20.4">entre</w> <w n="20.5">ses</w> <w n="20.6">deux</w> <w n="20.7">bois</w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Vite</w> ! <w n="21.2">Lancez</w>-<w n="21.3">vous</w>, <w n="21.4">mes</w> <w n="21.5">grands</w> <w n="21.6">chiens</w> <w n="21.7">sauvages</w> !</l>
						<l n="22" num="6.2"><w n="22.1">Vite</w>, <w n="22.2">mon</w> <w n="22.3">cheval</w> ! <w n="22.4">Galopons</w> <w n="22.5">sur</w> <w n="22.6">lui</w> !</l>
						<l n="23" num="6.3"><w n="23.1">Vite</w>, <w n="23.2">mon</w> <w n="23.3">escorte</w> ! <w n="23.4">Avec</w> <w n="23.5">les</w> <w n="23.6">nuages</w>,</l>
						<l n="24" num="6.4"><w n="24.1">A</w> <w n="24.2">travers</w> <w n="24.3">les</w> <w n="24.4">bois</w>, <w n="24.5">courons</w> <w n="24.6">dans</w> <w n="24.7">la</w> <w n="24.8">nuit</w> !</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">Le</w> <w n="25.2">cerf</w> <w n="25.3">disparaît</w>, <w n="25.4">la</w> <w n="25.5">lune</w> <w n="25.6">s</w>’<w n="25.7">efface</w>,</l>
						<l n="26" num="7.2"><w n="26.1">Le</w> <w n="26.2">silence</w> <w n="26.3">noir</w> <w n="26.4">règne</w> <w n="26.5">sur</w> <w n="26.6">le</w> <w n="26.7">val</w>.</l>
						<l n="27" num="7.3"><w n="27.1">Sous</w> <w n="27.2">le</w> <w n="27.3">ciel</w> <w n="27.4">d</w>’<w n="27.5">orage</w> <w n="27.6">où</w> <w n="27.7">s</w>’<w n="27.8">enfuit</w> <w n="27.9">la</w> <w n="27.10">chasse</w>,</l>
						<l n="28" num="7.4"><w n="28.1">Je</w> <w n="28.2">suis</w> <w n="28.3">toute</w> <w n="28.4">seule</w> <w n="28.5">avec</w> <w n="28.6">mon</w> <w n="28.7">cheval</w>.</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1"><w n="29.1">Dans</w> <w n="29.2">l</w>’<w n="29.3">automne</w> <w n="29.4">d</w>’<w n="29.5">or</w> <w n="29.6">flamboyante</w> <w n="29.7">et</w> <w n="29.8">morte</w>,</l>
						<l n="30" num="8.2"><w n="30.1">Invisible</w> <w n="30.2">cerf</w> <w n="30.3">que</w> <w n="30.4">je</w> <w n="30.5">veux</w> <w n="30.6">forcer</w>,</l>
						<l n="31" num="8.3"><w n="31.1">Invisibles</w> <w n="31.2">chiens</w>, <w n="31.3">invisible</w> <w n="31.4">escorte</w>,</l>
						<l n="32" num="8.4"><w n="32.1">Quels</w> <w n="32.2">yeux</w> <w n="32.3">que</w> <w n="32.4">les</w> <w n="32.5">miens</w> <w n="32.6">vous</w> <w n="32.7">verront</w> <w n="32.8">passer</w> ?…</l>
					</lg>
				</div></body></text></TEI>