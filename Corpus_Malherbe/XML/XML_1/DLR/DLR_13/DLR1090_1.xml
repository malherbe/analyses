<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CHOIX DE POÈMES</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1917 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">DLR_13</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Choix de poèmes</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie Alphonse Lemerre</publisher>
							<date when="1951">1951</date>
						</imprint>
					</monogr>
					<note>Édition numérisée à partir d’un ouvrage prêté par la BPU de neuchatel</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1951">1951</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La partie en prose (FRAGMENT D’UN TEXTE DE L’AUTEUR PRÉFAÇANT DEUX POÈMES D’EDGAR POE) n’est incluse.</p>
				<p>Les indications de la première édition d’un poème données dans la table des matières ont été reportées comme sous-titre des poèmes. L’année d’édition a été ajoutée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell (correction d’erreur d’océrisation).</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-01-14" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-01-14" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CHAPITRE VII</head><head type="main_part">Rencontres</head><div type="poem" key="DLR1090">
					<head type="main">Romance</head>
					<head type="number">I</head>
					<lg n="1">
						<l n="1" num="1.1"><space unit="char" quantity="8"></space><w n="1.1">J</w>’<w n="1.2">ai</w>, <w n="1.3">dans</w> <w n="1.4">ma</w> <w n="1.5">gorge</w> <w n="1.6">et</w> ’<w n="1.7">dans</w> <w n="1.8">mon</w> <w n="1.9">âme</w>,</l>
						<l n="2" num="1.2"><space unit="char" quantity="12"></space><w n="2.1">Le</w> <w n="2.2">sanglot</w> <w n="2.3">du</w> <w n="2.4">printemps</w></l>
						<l n="3" num="1.3"><space unit="char" quantity="8"></space><w n="3.1">Et</w> <w n="3.2">le</w> <w n="3.3">souvenir</w> <w n="3.4">de</w> <w n="3.5">la</w> <w n="3.6">femme</w></l>
						<l n="4" num="1.4"><space unit="char" quantity="8"></space><w n="4.1">Que</w> <w n="4.2">j</w>’<w n="4.3">aimais</w> <w n="4.4">quand</w> <w n="4.5">j</w>’<w n="4.6">avais</w> <w n="4.7">vingt</w> <w n="4.8">ans</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><space unit="char" quantity="8"></space><w n="5.1">Pourquoi</w>, <w n="5.2">tandis</w> <w n="5.3">que</w> <w n="5.4">refleurissent</w></l>
						<l n="6" num="2.2"><w n="6.1">Les</w> <w n="6.2">arbres</w> <w n="6.3">morts</w> <w n="6.4">chargés</w> <w n="6.5">des</w> <w n="6.6">plus</w> <w n="6.7">tendres</w> <w n="6.8">couleurs</w>,</l>
						<l n="7" num="2.3"><space unit="char" quantity="8"></space><w n="7.1">Faut</w>-<w n="7.2">il</w> <w n="7.3">que</w> <w n="7.4">les</w> <w n="7.5">amours</w> <w n="7.6">périssent</w></l>
						<l n="8" num="2.4"><space unit="char" quantity="8"></space><w n="8.1">Et</w> <w n="8.2">ne</w> <w n="8.3">refassent</w> <w n="8.4">plus</w> <w n="8.5">de</w> <w n="8.6">fleurs</w> ?</l>
					</lg>
				</div></body></text></TEI>