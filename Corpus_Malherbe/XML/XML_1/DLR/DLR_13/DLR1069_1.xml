<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CHOIX DE POÈMES</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1917 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">DLR_13</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Choix de poèmes</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie Alphonse Lemerre</publisher>
							<date when="1951">1951</date>
						</imprint>
					</monogr>
					<note>Édition numérisée à partir d’un ouvrage prêté par la BPU de neuchatel</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1951">1951</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La partie en prose (FRAGMENT D’UN TEXTE DE L’AUTEUR PRÉFAÇANT DEUX POÈMES D’EDGAR POE) n’est incluse.</p>
				<p>Les indications de la première édition d’un poème données dans la table des matières ont été reportées comme sous-titre des poèmes. L’année d’édition a été ajoutée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell (correction d’erreur d’océrisation).</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-01-14" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-01-14" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CHAPITRE V</head><head type="main_part">Intimité</head><div type="poem" key="DLR1069">
					<head type="main">Déclaration</head>
					<head type="sub_2">HORIZONS, Fasquelle, 1905</head>
					<opener>
						<salute>Au Docteur J.-C. Mardrus</salute>
					</opener>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Tu</w> <w n="1.2">as</w> <w n="1.3">lavé</w> <w n="1.4">tu</w> <w n="1.5">as</w> <w n="1.6">drainé</w> <w n="1.7">mon</w> <w n="1.8">âme</w> <w n="1.9">lâche</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Où</w> <w n="2.2">la</w> <w n="2.3">mélancolie</w> <w n="2.4">avait</w> <w n="2.5">mis</w> <w n="2.6">son</w> <w n="2.7">baiser</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Et</w> <w n="3.2">raclé</w> <w n="3.3">dans</w> <w n="3.4">leur</w> <w n="3.5">mal</w> <w n="3.6">mes</w> <w n="3.7">os</w> <w n="3.8">civilisés</w></l>
						<l n="4" num="1.4"><w n="4.1">Avec</w> <w n="4.2">ta</w> <w n="4.3">dureté</w> <w n="4.4">pareille</w> <w n="4.5">à</w> <w n="4.6">une</w> <w n="4.7">hache</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Tes</w> <w n="5.2">mains</w> <w n="5.3">ont</w> <w n="5.4">libéré</w> <w n="5.5">toutes</w> <w n="5.6">mes</w> <w n="5.7">passions</w>,</l>
						<l n="6" num="2.2"><w n="6.1">Décourbé</w> <w n="6.2">rudement</w> <w n="6.3">l</w>’<w n="6.4">ankylose</w> <w n="6.5">peureuse</w></l>
						<l n="7" num="2.3"><w n="7.1">Des</w> <w n="7.2">siècles</w>, <w n="7.3">arraché</w> <w n="7.4">mes</w> <w n="7.5">libres</w> <w n="7.6">douloureuses</w></l>
						<l n="8" num="2.4"><w n="8.1">Du</w> <w n="8.2">terrain</w> <w n="8.3">de</w> <w n="8.4">la</w> <w n="8.5">race</w> <w n="8.6">et</w> <w n="8.7">des</w> <w n="8.8">traditions</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Maintenant</w>, <w n="9.2">je</w> <w n="9.3">connais</w> <w n="9.4">et</w> <w n="9.5">je</w> <w n="9.6">peux</w>. <w n="9.7">Mon</w> <w n="9.8">écorce</w></l>
						<l n="10" num="3.2"><w n="10.1">Éclatée</w> <w n="10.2">a</w> <w n="10.3">laissé</w> <w n="10.4">toute</w> <w n="10.5">mon</w> <w n="10.6">âme</w> <w n="10.7">à</w> <w n="10.8">nu</w>.</l>
						<l n="11" num="3.3"><w n="11.1">Je</w> <w n="11.2">pousse</w> <w n="11.3">vers</w> <w n="11.4">la</w> <w n="11.5">vie</w> <w n="11.6">un</w> <w n="11.7">tel</w> <w n="11.8">cri</w> <w n="11.9">suraigu</w></l>
						<l n="12" num="3.4"><w n="12.1">Qu</w>’<w n="12.2">elle</w> <w n="12.3">aura</w> <w n="12.4">peur</w> <w n="12.5">de</w> <w n="12.6">moi</w>, <w n="12.7">peut</w>-<w n="12.8">être</w>, <w n="12.9">et</w> <w n="12.10">de</w> <w n="12.11">ma</w> <w n="12.12">force</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Je</w> <w n="13.2">veux</w> <w n="13.3">vivre</w> ! <w n="13.4">La</w> <w n="13.5">mort</w> <w n="13.6">rôdera</w> <w n="13.7">vainement</w> :</l>
						<l n="14" num="4.2"><w n="14.1">Elle</w> <w n="14.2">fut</w> <w n="14.3">mon</w> <w n="14.4">espoir</w>, <w n="14.5">elle</w> <w n="14.6">devient</w> <w n="14.7">ma</w> <w n="14.8">crainte</w>.</l>
						<l n="15" num="4.3">— <w n="15.1">Donne</w> <w n="15.2">ta</w> <w n="15.3">main</w> ! <w n="15.4">Allons</w>, <w n="15.5">d</w>’<w n="15.6">une</w> <w n="15.7">énergique</w> <w n="15.8">étreinte</w>,</l>
						<l n="16" num="4.4"><w n="16.1">Prendre</w> <w n="16.2">la</w> <w n="16.3">Destinée</w> <w n="16.4">avec</w> <w n="16.5">des</w> <w n="16.6">bras</w> <w n="16.7">d</w>’<w n="16.8">amant</w> ;</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Donne</w> <w n="17.2">ta</w> <w n="17.3">main</w> ! <w n="17.4">Je</w> <w n="17.5">veux</w> <w n="17.6">y</w> <w n="17.7">cramponner</w> <w n="17.8">mon</w> <w n="17.9">geste</w>,</l>
						<l n="18" num="5.2"><w n="18.1">Et</w>, <w n="18.2">défiant</w> <w n="18.3">la</w> <w n="18.4">vie</w> <w n="18.5">et</w> <w n="18.6">défiant</w> <w n="18.7">la</w> <w n="18.8">mort</w>,</l>
						<l n="19" num="5.3"><w n="19.1">M</w>’<w n="19.2">enfermer</w> <w n="19.3">en</w> <w n="19.4">toi</w> <w n="19.5">seul</w> <w n="19.6">comme</w> <w n="19.7">en</w> <w n="19.8">un</w> <w n="19.9">château</w> <w n="19.10">fort</w></l>
						<l n="20" num="5.4"><w n="20.1">Pour</w> <w n="20.2">dominer</w> <w n="20.3">l</w>’<w n="20.4">abîme</w> <w n="20.5">ouvert</w> <w n="20.6">de</w> <w n="20.7">tout</w> <w n="20.8">le</w> <w n="20.9">reste</w>.</l>
					</lg>
				</div></body></text></TEI>