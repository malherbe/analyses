<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CHOIX DE POÈMES</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1917 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">DLR_13</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Choix de poèmes</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie Alphonse Lemerre</publisher>
							<date when="1951">1951</date>
						</imprint>
					</monogr>
					<note>Édition numérisée à partir d’un ouvrage prêté par la BPU de neuchatel</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1951">1951</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La partie en prose (FRAGMENT D’UN TEXTE DE L’AUTEUR PRÉFAÇANT DEUX POÈMES D’EDGAR POE) n’est incluse.</p>
				<p>Les indications de la première édition d’un poème données dans la table des matières ont été reportées comme sous-titre des poèmes. L’année d’édition a été ajoutée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell (correction d’erreur d’océrisation).</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-01-14" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-01-14" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CHAPITRE VI</head><head type="main_part">Religion</head><div type="poem" key="DLR1085">
					<head type="number">II</head>
					<head type="main">Aux Derviches Mewlewi</head>
					<head type="sub_2">PAR VENTS ET MARÉES, Fasquelle, 1910</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Je</w> <w n="1.2">garde</w> <w n="1.3">ce</w> <w n="1.4">bonheur</w> <w n="1.5">entre</w> <w n="1.6">tous</w> <w n="1.7">les</w> <w n="1.8">bonheurs</w></l>
						<l n="2" num="1.2"><space unit="char" quantity="8"></space><w n="2.1">D</w>’<w n="2.2">avoir</w> <w n="2.3">connu</w> <w n="2.4">la</w> <w n="2.5">descendance</w></l>
						<l n="3" num="1.3"><w n="3.1">Platonique</w>, <w n="3.2">la</w> <w n="3.3">seule</w>, <w n="3.4">en</w> <w n="3.5">ces</w> <w n="3.6">divins</w> <w n="3.7">tourneurs</w></l>
						<l n="4" num="1.4"><space unit="char" quantity="8"></space><w n="4.1">Pâlis</w> <w n="4.2">de</w> <w n="4.3">musique</w> <w n="4.4">et</w> <w n="4.5">de</w> <w n="4.6">danse</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Une</w> <w n="5.2">flûte</w> <w n="5.3">blessée</w> <w n="5.4">à</w> <w n="5.5">voix</w> <w n="5.6">de</w> <w n="5.7">rossignol</w></l>
						<l n="6" num="2.2"><space unit="char" quantity="8"></space><w n="6.1">Accompagne</w> <w n="6.2">des</w> <w n="6.3">tambours</w> <w n="6.4">frêles</w> ;</l>
						<l n="7" num="2.3"><w n="7.1">Et</w>, <w n="7.2">pour</w> <w n="7.3">que</w> <w n="7.4">vingt</w> <w n="7.5">soufis</w> <w n="7.6">prennent</w> <w n="7.7">soudain</w> <w n="7.8">leur</w> <w n="7.9">vol</w>,</l>
						<l n="8" num="2.4"><space unit="char" quantity="8"></space><w n="8.1">Les</w> <w n="8.2">bras</w> <w n="8.3">s</w>’<w n="8.4">ouvrent</w> <w n="8.5">comme</w> <w n="8.6">des</w> <w n="8.7">ailes</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Ils</w> <w n="9.2">tournent</w> ! <w n="9.3">Je</w> <w n="9.4">te</w> <w n="9.5">vois</w>, <w n="9.6">cercle</w> <w n="9.7">passionné</w>,</l>
						<l n="10" num="3.2"><space unit="char" quantity="8"></space><w n="10.1">Et</w> <w n="10.2">je</w> <w n="10.3">te</w> <w n="10.4">sens</w>, <w n="10.5">spasme</w> <w n="10.6">de</w> <w n="10.7">l</w>’<w n="10.8">âme</w> !</l>
						<l n="11" num="3.3"><w n="11.1">Au</w> <w n="11.2">grand</w> <w n="11.3">rythme</w> <w n="11.4">muet</w> <w n="11.5">de</w> <w n="11.6">ces</w> <w n="11.7">jupes</w> <w n="11.8">de</w> <w n="11.9">femmes</w>,</l>
						<l n="12" num="3.4"><space unit="char" quantity="8"></space><w n="12.1">Tout</w> <w n="12.2">mon</w> <w n="12.3">être</w> <w n="12.4">aussi</w> <w n="12.5">veut</w> <w n="12.6">tourner</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Chœur</w> <w n="13.2">d</w>’<w n="13.3">esprits</w> <w n="13.4">qui</w> <w n="13.5">glissez</w> <w n="13.6">comme</w> <w n="13.7">jadis</w> <w n="13.8">les</w> <w n="13.9">anges</w></l>
						<l n="14" num="4.2"><space unit="char" quantity="8"></space><w n="14.1">Sur</w> <w n="14.2">un</w> <w n="14.3">signe</w> <w n="14.4">de</w> <w n="14.5">Gabriel</w>,</l>
						<l n="15" num="4.3"><w n="15.1">Chacun</w> <w n="15.2">de</w> <w n="15.3">vous</w>, <w n="15.4">blanc</w> <w n="15.5">papillon</w> <w n="15.6">surnaturel</w>,</l>
						<l n="16" num="4.4"><space unit="char" quantity="8"></space><w n="16.1">Se</w> <w n="16.2">multiplie</w> <w n="16.3">en</w> <w n="16.4">pas</w> <w n="16.5">étranges</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">C</w>’<w n="17.2">est</w> <w n="17.3">la</w> <w n="17.4">ronde</w> <w n="17.5">de</w> <w n="17.6">rêve</w> <w n="17.7">et</w> <w n="17.8">de</w> <w n="17.9">réflexion</w>.</l>
						<l n="18" num="5.2"><space unit="char" quantity="8"></space><w n="18.1">Une</w> <w n="18.2">main</w> <w n="18.3">jette</w>, <w n="18.4">et</w> <w n="18.5">l</w>’<w n="18.6">autre</w> <w n="18.7">accepte</w>.</l>
						<l n="19" num="5.3"><w n="19.1">Votre</w> <w n="19.2">hypostase</w> <w n="19.3">danse</w> <w n="19.4">et</w> <w n="19.5">redit</w> <w n="19.6">le</w> <w n="19.7">précepte</w></l>
						<l n="20" num="5.4"><space unit="char" quantity="8"></space><w n="20.1">D</w>’<w n="20.2">éternelle</w> <w n="20.3">giration</w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Le</w> <w n="21.2">tournoiement</w> <w n="21.3">sans</w> <w n="21.4">bruit</w> <w n="21.5">de</w> <w n="21.6">vos</w> <w n="21.7">candides</w> <w n="21.8">voiles</w></l>
						<l n="22" num="6.2"><space unit="char" quantity="8"></space><w n="22.1">Évente</w> <w n="22.2">le</w> <w n="22.3">mystique</w> <w n="22.4">lieu</w>,</l>
						<l n="23" num="6.3"><w n="23.1">Et</w> <w n="23.2">vous</w> <w n="23.3">perpétuez</w>, <w n="23.4">ô</w> <w n="23.5">frères</w> <w n="23.6">des</w> <w n="23.7">étoiles</w>,</l>
						<l n="24" num="6.4"><space unit="char" quantity="8"></space><w n="24.1">Le</w> <w n="24.2">mouvement</w> <w n="24.3">qui</w> <w n="24.4">plaît</w> <w n="24.5">à</w> <w n="24.6">Dieu</w>.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">Soufis</w> ! <w n="25.2">Le</w> <w n="25.3">beau</w> <w n="25.4">désir</w> <w n="25.5">de</w> <w n="25.6">voler</w> <w n="25.7">vous</w> <w n="25.8">emporte</w> !</l>
						<l n="26" num="7.2"><space unit="char" quantity="8"></space><w n="26.1">Dans</w> <w n="26.2">un</w> <w n="26.3">geste</w> <w n="26.4">crucifié</w>,</l>
						<l n="27" num="7.3"><w n="27.1">Vous</w> <w n="27.2">tournez</w>, <w n="27.3">les</w> <w n="27.4">bras</w> <w n="27.5">étendus</w>, <w n="27.6">la</w> <w n="27.7">face</w> <w n="27.8">morte</w></l>
						<l n="28" num="7.4"><space unit="char" quantity="8"></space><w n="28.1">Et</w> <w n="28.2">le</w> <w n="28.3">souffle</w> <w n="28.4">raréfié</w>.</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1"><w n="29.1">Vous</w> <w n="29.2">tournez</w>, <w n="29.3">vous</w> <w n="29.4">tournez</w>, <w n="29.5">enivrés</w> <w n="29.6">de</w> <w n="29.7">vertige</w>,</l>
						<l n="30" num="8.2"><space unit="char" quantity="8"></space><w n="30.1">Heureux</w> <w n="30.2">jusques</w> <w n="30.3">à</w> <w n="30.4">la</w> <w n="30.5">douleur</w>,</l>
						<l n="31" num="8.3"><w n="31.1">Et</w> <w n="31.2">votre</w> <w n="31.3">robe</w> <w n="31.4">semble</w>, <w n="31.5">arrachée</w> <w n="31.6">à</w> <w n="31.7">sa</w> <w n="31.8">tige</w>,</l>
						<l n="32" num="8.4"><space unit="char" quantity="8"></space><w n="32.1">Une</w> <w n="32.2">immense</w> <w n="32.3">et</w> <w n="32.4">démente</w> <w n="32.5">fleur</w>.</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1"><w n="33.1">Le</w> <w n="33.2">vol</w> <w n="33.3">silencieux</w> ! <w n="33.4">La</w> <w n="33.5">fraîcheur</w> <w n="33.6">d</w>’<w n="33.7">ailes</w> <w n="33.8">blanches</w> !</l>
						<l n="34" num="9.2"><space unit="char" quantity="8"></space><w n="34.1">Ah</w> ! <w n="34.2">que</w> <w n="34.3">chaque</w> <w n="34.4">pas</w>, <w n="34.5">chaque</w> <w n="34.6">tour</w>,</l>
						<l n="35" num="9.3"><w n="35.1">Que</w> <w n="35.2">chaque</w> <w n="35.3">glissement</w> <w n="35.4">des</w> <w n="35.5">pieds</w> <w n="35.6">nus</w> <w n="35.7">sur</w> <w n="35.8">les</w> <w n="35.9">planches</w></l>
						<l n="36" num="9.4"><space unit="char" quantity="8"></space><w n="36.1">Répète</w> : <w n="36.2">Amour</w> ! <w n="36.3">Amour</w> ! <w n="36.4">Amour</w> !…</l>
					</lg>
				</div></body></text></TEI>