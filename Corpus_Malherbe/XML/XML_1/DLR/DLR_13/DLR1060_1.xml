<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CHOIX DE POÈMES</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1917 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">DLR_13</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Choix de poèmes</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie Alphonse Lemerre</publisher>
							<date when="1951">1951</date>
						</imprint>
					</monogr>
					<note>Édition numérisée à partir d’un ouvrage prêté par la BPU de neuchatel</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1951">1951</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La partie en prose (FRAGMENT D’UN TEXTE DE L’AUTEUR PRÉFAÇANT DEUX POÈMES D’EDGAR POE) n’est incluse.</p>
				<p>Les indications de la première édition d’un poème données dans la table des matières ont été reportées comme sous-titre des poèmes. L’année d’édition a été ajoutée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell (correction d’erreur d’océrisation).</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-01-14" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-01-14" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CHAPITRE III</head><head type="main_part">Chevauchées</head><div type="poem" key="DLR1060">
					<head type="main">Éloge de mon Cheval</head>
					<head type="sub_2">LA FIGURE DE PROUE, Fasquelle, 1908</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Mon</w> <w n="1.2">cheval</w> <w n="1.3">au</w> <w n="1.4">poitrail</w> <w n="1.5">solide</w>, <w n="1.6">à</w> <w n="1.7">l</w>’<w n="1.8">œil</w> <w n="1.9">de</w> <w n="1.10">feu</w>,</l>
						<l n="2" num="1.2"><space unit="char" quantity="4"></space><w n="2.1">Frère</w> <w n="2.2">joyeux</w> <w n="2.3">de</w> <w n="2.4">mon</w> <w n="2.5">âme</w> <w n="2.6">animale</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Ton</w> <w n="3.2">sang</w> <w n="3.3">arabe</w> <w n="3.4">bout</w> <w n="3.5">comme</w> <w n="3.6">le</w> <w n="3.7">mien</w>, <w n="3.8">beau</w> <w n="3.9">mâle</w>,</l>
						<l n="4" num="1.4"><space unit="char" quantity="8"></space><w n="4.1">Et</w> <w n="4.2">tu</w> <w n="4.3">comprends</w> <w n="4.4">si</w> <w n="4.5">bien</w> <w n="4.6">le</w> <w n="4.7">jeu</w> !</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Voici</w> <w n="5.2">notre</w> <w n="5.3">statue</w> <w n="5.4">haute</w> <w n="5.5">et</w> <w n="5.6">momentanée</w>.</l>
						<l n="6" num="2.2"><space unit="char" quantity="4"></space><w n="6.1">Chaque</w> <w n="6.2">jour</w> <w n="6.3">pour</w> <w n="6.4">nous</w> <w n="6.5">est</w> <w n="6.6">le</w> <w n="6.7">jour</w> <w n="6.8">des</w> <w n="6.9">bonds</w></l>
						<l n="7" num="2.3"><space unit="char" quantity="8"></space><w n="7.1">Et</w> <w n="7.2">des</w> <w n="7.3">caprices</w> <w n="7.4">furibonds</w></l>
						<l n="8" num="2.4"><space unit="char" quantity="4"></space><w n="8.1">Vite</w> <w n="8.2">oubliés</w> <w n="8.3">au</w> <w n="8.4">bout</w> <w n="8.5">de</w> <w n="8.6">la</w> <w n="8.7">journée</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Ton</w> <w n="9.2">galop</w> <w n="9.3">violent</w> <w n="9.4">obéit</w> <w n="9.5">à</w> <w n="9.6">mon</w> <w n="9.7">cri</w>,</l>
						<l n="10" num="3.2"><space unit="char" quantity="8"></space><w n="10.1">Nous</w> <w n="10.2">vivons</w> <w n="10.3">d</w>’<w n="10.4">ivresses</w> <w n="10.5">pareilles</w> ;</l>
						<l n="11" num="3.3"><w n="11.1">Et</w> <w n="11.2">je</w> <w n="11.3">vois</w> <w n="11.4">l</w>’<w n="11.5">existence</w> <w n="11.6">entre</w> <w n="11.7">tes</w> <w n="11.8">deux</w> <w n="11.9">oreilles</w>,</l>
						<l n="12" num="3.4"><space unit="char" quantity="4"></space><w n="12.1">Sensibles</w>. <w n="12.2">à</w> <w n="12.3">tout</w> <w n="12.4">comme</w> <w n="12.5">mon</w> <w n="12.6">esprit</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">La</w> <w n="13.2">même</w> <w n="13.3">passion</w> <w n="13.4">passe</w> <w n="13.5">dans</w> <w n="13.6">nos</w> <w n="13.7">narines</w>,</l>
						<l n="14" num="4.2"><space unit="char" quantity="8"></space><w n="14.1">Le</w> <w n="14.2">même</w> <w n="14.3">vent</w> <w n="14.4">dans</w> <w n="14.5">nos</w> <w n="14.6">cheveux</w>.</l>
						<l n="15" num="4.3"><w n="15.1">Je</w> <w n="15.2">fais</w> <w n="15.3">ce</w> <w n="15.4">qui</w> <w n="15.5">te</w> <w n="15.6">plaît</w> <w n="15.7">et</w> <w n="15.8">toi</w> <w n="15.9">ce</w> <w n="15.10">que</w> <w n="15.11">je</w> <w n="15.12">veux</w>,</l>
						<l n="16" num="4.4"><space unit="char" quantity="4"></space><w n="16.1">Et</w> <w n="16.2">la</w> <w n="16.3">liberté</w> <w n="16.4">gonfle</w> <w n="16.5">nos</w> <w n="16.6">poitrines</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Le</w> <w n="17.2">tout</w> <w n="17.3">puissant</w> <w n="17.4">pouvoir</w> <w n="17.5">s</w>’<w n="17.6">équilibre</w> <w n="17.7">entre</w> <w n="17.8">nous</w> :</l>
						<l n="18" num="5.2"><space unit="char" quantity="4"></space><w n="18.1">Ma</w> <w n="18.2">vie</w> <w n="18.3">est</w> <w n="18.4">livrée</w> <w n="18.5">à</w> <w n="18.6">ton</w> <w n="18.7">dos</w> <w n="18.8">farouche</w>,</l>
						<l n="19" num="5.3"><space unit="char" quantity="8"></space><w n="19.1">Ma</w> <w n="19.2">volonté</w> <w n="19.3">mate</w> <w n="19.4">ta</w> <w n="19.5">bouche</w>,</l>
						<l n="20" num="5.4"><space unit="char" quantity="4"></space><w n="20.1">Et</w> <w n="20.2">ta</w> <w n="20.3">force</w> <w n="20.4">est</w> <w n="20.5">prise</w> <w n="20.6">entre</w> <w n="20.7">mes</w> <w n="20.8">genoux</w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Que</w> <w n="21.2">si</w>, <w n="21.3">présentement</w>, <w n="21.4">l</w>’<w n="21.5">ombre</w> <w n="21.6">multiple</w> <w n="21.7">et</w> <w n="21.8">une</w></l>
						<l n="22" num="6.2"><space unit="char" quantity="8"></space><w n="22.1">Descend</w> <w n="22.2">avec</w> <w n="22.3">le</w> <w n="22.4">feu</w> <w n="22.5">des</w> <w n="22.6">soirs</w>,</l>
						<l n="23" num="6.3"><w n="23.1">Dis</w> ? <w n="23.2">Prenons</w> <w n="23.3">notre</w> <w n="23.4">trot</w> <w n="23.5">vers</w> <w n="23.6">la</w> <w n="23.7">nouvelle</w> <w n="23.8">lune</w></l>
						<l n="24" num="6.4"><space unit="char" quantity="4"></space><w n="24.1">Cornue</w> <w n="24.2">au</w>-<w n="24.3">dessus</w> <w n="24.4">des</w> <w n="24.5">bois</w> <w n="24.6">déjà</w> <w n="24.7">noirs</w>.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">Rythmons</w> <w n="25.2">des</w> <w n="25.3">quatre</w> <w n="25.4">pieds</w> <w n="25.5">notre</w> <w n="25.6">vol</w> <w n="25.7">qui</w> <w n="25.8">s</w>’<w n="25.9">élance</w>,</l>
						<l n="26" num="7.2"><space unit="char" quantity="4"></space><w n="26.1">Si</w> <w n="26.2">tu</w> <w n="26.3">veux</w> <w n="26.4">gagner</w> <w n="26.5">le</w> <w n="26.6">but</w> <w n="26.7">d</w>’<w n="26.8">un</w> <w n="26.9">seul</w> <w n="26.10">trait</w>,</l>
						<l n="27" num="7.3"><w n="27.1">Et</w> <w n="27.2">battons</w> <w n="27.3">vivement</w> <w n="27.4">la</w> <w n="27.5">mesure</w> <w n="27.6">en</w> <w n="27.7">silence</w></l>
						<l n="28" num="7.4"><space unit="char" quantity="8"></space><w n="28.1">Dans</w> <w n="28.2">les</w> <w n="28.3">sentiers</w> <w n="28.4">de</w> <w n="28.5">la</w> <w n="28.6">forêt</w>.</l>
					</lg>
				</div></body></text></TEI>