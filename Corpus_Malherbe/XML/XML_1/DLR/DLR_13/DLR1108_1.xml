<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CHOIX DE POÈMES</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1917 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">DLR_13</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Choix de poèmes</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie Alphonse Lemerre</publisher>
							<date when="1951">1951</date>
						</imprint>
					</monogr>
					<note>Édition numérisée à partir d’un ouvrage prêté par la BPU de neuchatel</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1951">1951</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La partie en prose (FRAGMENT D’UN TEXTE DE L’AUTEUR PRÉFAÇANT DEUX POÈMES D’EDGAR POE) n’est incluse.</p>
				<p>Les indications de la première édition d’un poème données dans la table des matières ont été reportées comme sous-titre des poèmes. L’année d’édition a été ajoutée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell (correction d’erreur d’océrisation).</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-01-14" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-01-14" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CHAPITRE IX</head><head type="main_part">Derniers Poèmes</head><head type="sub_part">INÉDITS</head><div type="poem" key="DLR1108">
					<head type="main">Ne Varietur</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">J</w>’<w n="1.2">ai</w> <w n="1.3">besoin</w> <w n="1.4">chaque</w> <w n="1.5">jour</w> <w n="1.6">de</w> <w n="1.7">revoir</w> <w n="1.8">dans</w> <w n="1.9">la</w> <w n="1.10">glace</w></l>
						<l n="2" num="1.2"><space unit="char" quantity="8"></space><w n="2.1">La</w> <w n="2.2">triste</w> <w n="2.3">maigreur</w> <w n="2.4">de</w> <w n="2.5">mon</w> <w n="2.6">corps</w>,</l>
						<l n="3" num="1.3"><w n="3.1">De</w> <w n="3.2">regarder</w> <w n="3.3">mes</w> <w n="3.4">mains</w> <w n="3.5">dont</w> <w n="3.6">les</w> <w n="3.7">gestes</w> <w n="3.8">sont</w> <w n="3.9">morts</w></l>
						<l n="4" num="1.4"><w n="4.1">Et</w> <w n="4.2">de</w> <w n="4.3">sentir</w> <w n="4.4">qu</w>’<w n="4.5">en</w> <w n="4.6">mes</w> <w n="4.7">genoux</w> <w n="4.8">la</w> <w n="4.9">force</w> <w n="4.10">casse</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">J</w>’<w n="5.2">ai</w> <w n="5.3">besoin</w> <w n="5.4">de</w> <w n="5.5">cela</w> <w n="5.6">pour</w> <w n="5.7">savoir</w> <w n="5.8">qu</w>’<w n="5.9">à</w> <w n="5.10">présent</w></l>
						<l n="6" num="2.2"><space unit="char" quantity="8"></space><w n="6.1">Je</w> <w n="6.2">ne</w> <w n="6.3">suis</w> <w n="6.4">qu</w>’<w n="6.5">une</w> <w n="6.6">vieille</w> <w n="6.7">femme</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Car</w> <w n="7.2">rien</w> <w n="7.3">n</w>’<w n="7.4">a</w> <w n="7.5">révélé</w> <w n="7.6">jusqu</w>’<w n="7.7">ici</w> <w n="7.8">dans</w> <w n="7.9">mon</w> <w n="7.10">âme</w></l>
						<l n="8" num="2.4"><w n="8.1">Ni</w> <w n="8.2">même</w> <w n="8.3">sur</w> <w n="8.4">mes</w> <w n="8.5">traits</w> <w n="8.6">cet</w> <w n="8.7">âge</w> <w n="8.8">déplaisant</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Je</w> <w n="9.2">suis</w> <w n="9.3">jeune</w> <w n="9.4">en</w> <w n="9.5">esprit</w> <w n="9.6">et</w> <w n="9.7">presque</w> <w n="9.8">de</w> <w n="9.9">visage</w>,</l>
						<l n="10" num="3.2"><space unit="char" quantity="8"></space><w n="10.1">Jeune</w> <w n="10.2">de</w> <w n="10.3">mes</w> <w n="10.4">cheveux</w> <w n="10.5">foncés</w>,</l>
						<l n="11" num="3.3"><w n="11.1">Jeune</w> <w n="11.2">surtout</w> <w n="11.3">d</w>’<w n="11.4">avoir</w>, <w n="11.5">en</w> <w n="11.6">dedans</w>, <w n="11.7">le</w> <w n="11.8">même</w> <w n="11.9">âge</w>,</l>
						<l n="12" num="3.4"><w n="12.1">Le</w> <w n="12.2">même</w> <w n="12.3">flamboiement</w> <w n="12.4">qu</w>’<w n="12.5">en</w> <w n="12.6">mes</w> <w n="12.7">plus</w> <w n="12.8">beaux</w> <w n="12.9">passés</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">C</w>’<w n="13.2">est</w> <w n="13.3">ainsi</w>. <w n="13.4">Quelquefois</w>, <w n="13.5">oubliant</w> <w n="13.6">l</w>’<w n="13.7">existence</w></l>
						<l n="14" num="4.2"><space unit="char" quantity="8"></space><w n="14.1">Qui</w> <w n="14.2">m</w>’<w n="14.3">est</w> <w n="14.4">faite</w> <w n="14.5">dorénavant</w>,</l>
						<l n="15" num="4.3"><w n="15.1">Je</w> <w n="15.2">crois</w> <w n="15.3">pouvoir</w> <w n="15.4">bondir</w> <w n="15.5">à</w> <w n="15.6">cheval</w> <w n="15.7">dans</w> <w n="15.8">le</w> <w n="15.9">vent</w>,</l>
						<l n="16" num="4.4"><w n="16.1">Car</w> <w n="16.2">tout</w> <w n="16.3">mon</w> <w n="16.4">être</w> <w n="16.5">reste</w>, <w n="16.6">à</w> <w n="16.7">jamais</w>, <w n="16.8">en</w> <w n="16.9">partance</w>.</l>
					</lg>
				</div></body></text></TEI>