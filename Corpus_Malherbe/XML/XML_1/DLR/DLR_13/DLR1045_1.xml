<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CHOIX DE POÈMES</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1917 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">DLR_13</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Choix de poèmes</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie Alphonse Lemerre</publisher>
							<date when="1951">1951</date>
						</imprint>
					</monogr>
					<note>Édition numérisée à partir d’un ouvrage prêté par la BPU de neuchatel</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1951">1951</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La partie en prose (FRAGMENT D’UN TEXTE DE L’AUTEUR PRÉFAÇANT DEUX POÈMES D’EDGAR POE) n’est incluse.</p>
				<p>Les indications de la première édition d’un poème données dans la table des matières ont été reportées comme sous-titre des poèmes. L’année d’édition a été ajoutée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell (correction d’erreur d’océrisation).</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-01-14" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-01-14" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CHAPITRE PREMIER</head><head type="main_part">L’Estuaire</head><head type="sub_part">SOUFFLES DE TEMPÊTE, Fasquelle, 1918</head><div type="poem" key="DLR1045">
					<head type="main">La Figure de Proue</head>
					<head type="sub_2">LA FIGURE DE PROUE, Fasquelle, 1908</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">La</w> <w n="1.2">ligure</w> <w n="1.3">de</w> <w n="1.4">proue</w> <w n="1.5">allongée</w> <w n="1.6">à</w> <w n="1.7">l</w>’<w n="1.8">étrave</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Vers</w> <w n="2.2">les</w> <w n="2.3">quatre</w> <w n="2.4">infinis</w>, <w n="2.5">le</w> <w n="2.6">visage</w> <w n="2.7">en</w> <w n="2.8">avant</w></l>
						<l n="3" num="1.3"><w n="3.1">S</w>’<w n="3.2">élance</w> ; <w n="3.3">et</w>, <w n="3.4">magnifique</w>, <w n="3.5">enorgueilli</w> <w n="3.6">de</w> <w n="3.7">vent</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Le</w> <w n="4.2">bateau</w> <w n="4.3">tout</w> <w n="4.4">entier</w> <w n="4.5">la</w> <w n="4.6">suit</w> <w n="4.7">comme</w> <w n="4.8">un</w> <w n="4.9">esclave</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Ses</w> <w n="5.2">yeux</w> <w n="5.3">ont</w> <w n="5.4">la</w> <w n="5.5">couleur</w> <w n="5.6">du</w> <w n="5.7">large</w> <w n="5.8">doux</w>-<w n="5.9">amer</w>,</l>
						<l n="6" num="2.2"><w n="6.1">Mille</w> <w n="6.2">relents</w> <w n="6.3">salins</w> <w n="6.4">ont</w> <w n="6.5">gonflé</w> <w n="6.6">ses</w> <w n="6.7">narines</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Sa</w> <w n="7.2">poitrine</w> <w n="7.3">a</w> <w n="7.4">humé</w> <w n="7.5">mille</w> <w n="7.6">brises</w> <w n="7.7">marines</w>,</l>
						<l n="8" num="2.4"><w n="8.1">Et</w> <w n="8.2">sa</w> <w n="8.3">bouche</w> <w n="8.4">entr</w>’<w n="8.5">ouverte</w> <w n="8.6">a</w> <w n="8.7">bu</w> <w n="8.8">toute</w> <w n="8.9">la</w> <w n="8.10">mer</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Lors</w> <w n="9.2">de</w> <w n="9.3">son</w> <w n="9.4">premier</w> <w n="9.5">choc</w> <w n="9.6">contre</w> <w n="9.7">la</w> <w n="9.8">vague</w> <w n="9.9">ronde</w>,</l>
						<l n="10" num="3.2"><w n="10.1">Quand</w>, <w n="10.2">neuve</w>, <w n="10.3">elle</w> <w n="10.4">quitta</w> <w n="10.5">le</w> <w n="10.6">premier</w> <w n="10.7">de</w> <w n="10.8">ses</w> <w n="10.9">ports</w>,</l>
						<l n="11" num="3.3"><w n="11.1">Elle</w> <w n="11.2">mit</w>, <w n="11.3">pour</w> <w n="11.4">voler</w>, <w n="11.5">toutes</w> <w n="11.6">voiles</w> <w n="11.7">dehors</w>,</l>
						<l n="12" num="3.4"><w n="12.1">Et</w> <w n="12.2">ses</w> <w n="12.3">jeunes</w> <w n="12.4">marins</w> <w n="12.5">criaient</w> : « <w n="12.6">Au</w> <w n="12.7">nord</w> <w n="12.8">du</w> <w n="12.9">monde</w> ! »</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Ce</w> <w n="13.2">jour</w> <w n="13.3">la</w> <w n="13.4">mariait</w>, <w n="13.5">vierge</w>, <w n="13.6">avec</w> <w n="13.7">l</w>’<w n="13.8">inconnu</w>.</l>
						<l n="14" num="4.2"><w n="14.1">Le</w> <w n="14.2">hasard</w>, <w n="14.3">désormais</w>, <w n="14.4">la</w> <w n="14.5">guette</w> <w n="14.6">à</w> <w n="14.7">chaque</w> <w n="14.8">rive</w>,</l>
						<l n="15" num="4.3"><w n="15.1">Car</w>, <w n="15.2">sur</w> <w n="15.3">la</w> <w n="15.4">proue</w> <w n="15.5">aiguë</w> <w n="15.6">où</w> <w n="15.7">son</w> <w n="15.8">destin</w>. <w n="15.9">la</w> <w n="15.10">rive</w>,</l>
						<l n="16" num="4.4"><w n="16.1">Qui</w> <w n="16.2">sait</w> <w n="16.3">quels</w> <w n="16.4">océans</w> <w n="16.5">laveront</w> <w n="16.6">son</w> <w n="16.7">front</w> <w n="16.8">nu</w> ?</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Elle</w> <w n="17.2">naviguera</w> <w n="17.3">dans</w> <w n="17.4">l</w>’<w n="17.5">oubli</w> <w n="17.6">des</w> <w n="17.7">tempêtes</w></l>
						<l n="18" num="5.2"><w n="18.1">Sur</w> <w n="18.2">l</w>’<w n="18.3">argent</w> <w n="18.4">des</w> <w n="18.5">minuits</w> <w n="18.6">et</w> <w n="18.7">sur</w> <w n="18.8">l</w>’<w n="18.9">or</w> <w n="18.10">des</w> <w n="18.11">midis</w>,</l>
						<l n="19" num="5.3"><w n="19.1">Et</w> <w n="19.2">ses</w> <w n="19.3">yeux</w> <w n="19.4">pleureront</w> <w n="19.5">les</w> <w n="19.6">havres</w> <w n="19.7">arrondis</w></l>
						<l n="20" num="5.4"><w n="20.1">Quand</w> <w n="20.2">les</w> <w n="20.3">lames</w> <w n="20.4">l</w>’<w n="20.5">attaqueront</w> <w n="20.6">comme</w> <w n="20.7">des</w> <w n="20.8">bêtes</w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Elle</w> <w n="21.2">saura</w> <w n="21.3">tous</w> <w n="21.4">les</w> <w n="21.5">aspects</w>, <w n="21.6">tous</w> <w n="21.7">les</w> <w n="21.8">climats</w>,</l>
						<l n="22" num="6.2"><w n="22.1">La</w> <w n="22.2">chaleur</w> <w n="22.3">et</w> <w n="22.4">le</w> <w n="22.5">froid</w>, <w n="22.6">l</w>’<w n="22.7">Équateur</w> <w n="22.8">et</w> <w n="22.9">les</w> <w n="22.10">pôles</w> ;</l>
						<l n="23" num="6.3"><w n="23.1">Elle</w> <w n="23.2">rapportera</w> <w n="23.3">sur</w> <w n="23.4">ses</w> <w n="23.5">frêles</w> <w n="23.6">épaules</w></l>
						<l n="24" num="6.4"><w n="24.1">Le</w> <w n="24.2">monde</w>, <w n="24.3">et</w> <w n="24.4">tous</w> <w n="24.5">les</w> <w n="24.6">ciels</w> <w n="24.7">aux</w> <w n="24.8">pointes</w> <w n="24.9">de</w> <w n="24.10">ses</w> <w n="24.11">mâts</w>.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">Et</w> <w n="25.2">toujours</w>, <w n="25.3">face</w> <w n="25.4">au</w> <w n="25.5">large</w> <w n="25.6">où</w> <w n="25.7">neigent</w> <w n="25.8">des</w> <w n="25.9">mouettes</w>,</l>
						<l n="26" num="7.2"><w n="26.1">Dans</w> <w n="26.2">la</w> <w n="26.3">sécurité</w> <w n="26.4">comme</w> <w n="26.5">dans</w> <w n="26.6">le</w> <w n="26.7">péril</w>,</l>
						<l n="27" num="7.3"><w n="27.1">Seule</w>, <w n="27.2">elle</w> <w n="27.3">mènera</w> <w n="27.4">son</w> <w n="27.5">vaisseau</w> <w n="27.6">vers</w> <w n="27.7">l</w>’<w n="27.8">exil</w></l>
						<l n="28" num="7.4"><w n="28.1">Où</w> <w n="28.2">s</w>’<w n="28.3">en</w> <w n="28.4">vont</w> <w n="28.5">à</w> <w n="28.6">jamais</w> <w n="28.7">les</w> <w n="28.8">désirs</w> <w n="28.9">des</w> <w n="28.10">poètes</w> ;</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1"><w n="29.1">Seule</w>, <w n="29.2">elle</w> <w n="29.3">affrontera</w> <w n="29.4">les</w> <w n="29.5">assauts</w> <w n="29.6">furibonds</w></l>
						<l n="30" num="8.2"><w n="30.1">De</w> <w n="30.2">l</w>’<w n="30.3">ennemie</w> <w n="30.4">énigmatique</w> <w n="30.5">et</w> <w n="30.6">ses</w> <w n="30.7">grands</w> <w n="30.8">calmes</w> ;</l>
						<l n="31" num="8.3"><w n="31.1">Seule</w>, <w n="31.2">à</w> <w n="31.3">son</w> <w n="31.4">front</w>, <w n="31.5">elle</w> <w n="31.6">ceindra</w>, <w n="31.7">telles</w> <w n="31.8">des</w> <w n="31.9">palmes</w>,</l>
						<l n="32" num="8.4"><w n="32.1">Les</w> <w n="32.2">souvenirs</w> <w n="32.3">de</w> <w n="32.4">tant</w> <w n="32.5">de</w> <w n="32.6">sommeils</w> <w n="32.7">et</w> <w n="32.8">de</w> <w n="32.9">bonds</w>.</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1"><w n="33.1">Et</w> <w n="33.2">quand</w>, <w n="33.3">ayant</w> <w n="33.4">blessé</w> <w n="33.5">les</w> <w n="33.6">flots</w> <w n="33.7">de</w> <w n="33.8">son</w> <w n="33.9">sillage</w>,</l>
						<l n="34" num="9.2"><w n="34.1">Le</w> <w n="34.2">chef</w> <w n="34.3">coiffé</w> <w n="34.4">de</w> <w n="34.5">goëmons</w>, <w n="34.6">sauvagement</w>,</l>
						<l n="35" num="9.3"><w n="35.1">Elle</w> <w n="35.2">s</w>’<w n="35.3">en</w> <w n="35.4">reviendra</w> <w n="35.5">comme</w> <w n="35.6">vers</w> <w n="35.7">un</w> <w n="35.8">aimant</w></l>
						<l n="36" num="9.4"><w n="36.1">A</w> <w n="36.2">son</w> <w n="36.3">port</w>, <w n="36.4">le</w> <w n="36.5">col</w> <w n="36.6">ceint</w> <w n="36.7">des</w> <w n="36.8">perles</w> <w n="36.9">du</w> <w n="36.10">voyage</w>,</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1"><w n="37.1">Parmi</w> <w n="37.2">toutes</w> <w n="37.3">les</w> <w n="37.4">mers</w> <w n="37.5">qui</w> <w n="37.6">baignent</w> <w n="37.7">les</w> <w n="37.8">pays</w>,</l>
						<l n="38" num="10.2"><w n="38.1">Le</w> <w n="38.2">mirage</w> <w n="38.3">profond</w> <w n="38.4">de</w> <w n="38.5">sa</w> <w n="38.6">face</w> <w n="38.7">effarée</w></l>
						<l n="39" num="10.3"><w n="39.1">Aura</w> <w n="39.2">divinement</w> <w n="39.3">repeuplé</w> <w n="39.4">la</w> <w n="39.5">marée</w></l>
						<l n="40" num="10.4"><w n="40.1">D</w>’<w n="40.2">une</w> <w n="40.3">ultime</w> <w n="40.4">sirène</w> <w n="40.5">aux</w> <w n="40.6">regards</w> <w n="40.7">inouïs</w>.</l>
					</lg>
					<lg n="11">
						<l n="41" num="11.1">… <w n="41.1">J</w>’<w n="41.2">ai</w> <w n="41.3">voulu</w> <w n="41.4">le</w> <w n="41.5">destin</w> <w n="41.6">des</w> <w n="41.7">figures</w> <w n="41.8">de</w> <w n="41.9">proue</w></l>
						<l n="42" num="11.2"><w n="42.1">Qui</w> <w n="42.2">tôt</w> <w n="42.3">quittent</w> <w n="42.4">le</w> <w n="42.5">port</w> <w n="42.6">et</w> <w n="42.7">qui</w> <w n="42.8">reviennent</w> <w n="42.9">tard</w>.</l>
						<l n="43" num="11.3"><w n="43.1">Je</w> <w n="43.2">suis</w> <w n="43.3">jalouse</w> <w n="43.4">du</w> <w n="43.5">retour</w> <w n="43.6">et</w> <w n="43.7">du</w> <w n="43.8">départ</w></l>
						<l n="44" num="11.4"><w n="44.1">Et</w> <w n="44.2">des</w> <w n="44.3">coraux</w> <w n="44.4">mouillés</w> <w n="44.5">dont</w> <w n="44.6">leur</w> <w n="44.7">gorge</w> <w n="44.8">se</w> <w n="44.9">noue</w>.</l>
					</lg>
					<lg n="12">
						<l n="45" num="12.1"><w n="45.1">J</w>’<w n="45.2">affronterai</w> <w n="45.3">les</w> <w n="45.4">mornes</w> <w n="45.5">gris</w>, <w n="45.6">les</w> <w n="45.7">brûlants</w> <w n="45.8">bleus</w></l>
						<l n="46" num="12.2"><w n="46.1">De</w> <w n="46.2">la</w> <w n="46.3">mer</w> <w n="46.4">figurée</w> <w n="46.5">et</w> <w n="46.6">de</w> <w n="46.7">la</w> <w n="46.8">mer</w> <w n="46.9">réelle</w>,</l>
						<l n="47" num="12.3"><w n="47.1">Puisque</w>, <w n="47.2">du</w> <w n="47.3">fond</w> <w n="47.4">du</w> <w n="47.5">risque</w>, <w n="47.6">on</w> <w n="47.7">s</w>’<w n="47.8">en</w> <w n="47.9">revient</w> <w n="47.10">plus</w> <w n="47.11">belle</w>,</l>
						<l n="48" num="12.4"><w n="48.1">Rapportant</w> <w n="48.2">un</w> <w n="48.3">visage</w> <w n="48.4">ardent</w> <w n="48.5">et</w> <w n="48.6">fabuleux</w>.</l>
					</lg>
					<lg n="13">
						<l n="49" num="13.1"><w n="49.1">Je</w> <w n="49.2">serai</w> <w n="49.3">celle</w>-<w n="49.4">là</w>, <w n="49.5">de</w> <w n="49.6">son</w> <w n="49.7">vaisseau</w> <w n="49.8">suivie</w>,</l>
						<l n="50" num="13.2"><w n="50.1">Qui</w> <w n="50.2">lève</w> <w n="50.3">haut</w> <w n="50.4">un</w> <w n="50.5">front</w> <w n="50.6">des</w> <w n="50.7">houles</w> <w n="50.8">baptisé</w>,</l>
						<l n="51" num="13.3"><w n="51.1">Et</w> <w n="51.2">dont</w> <w n="51.3">le</w> <w n="51.4">cœur</w>, <w n="51.5">jusqu</w>’<w n="51.6">à</w> <w n="51.7">la</w> <w n="51.8">mort</w> <w n="51.9">inapaisé</w>,</l>
						<l n="52" num="13.4"><w n="52.1">Traverse</w> <w n="52.2">bravement</w> <w n="52.3">le</w> <w n="52.4">voyage</w> <w n="52.5">et</w> <w n="52.6">la</w> <w n="52.7">vie</w>.</l>
					</lg>
				</div></body></text></TEI>