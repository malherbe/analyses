<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CHOIX DE POÈMES</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1917 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">DLR_13</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Choix de poèmes</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie Alphonse Lemerre</publisher>
							<date when="1951">1951</date>
						</imprint>
					</monogr>
					<note>Édition numérisée à partir d’un ouvrage prêté par la BPU de neuchatel</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1951">1951</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La partie en prose (FRAGMENT D’UN TEXTE DE L’AUTEUR PRÉFAÇANT DEUX POÈMES D’EDGAR POE) n’est incluse.</p>
				<p>Les indications de la première édition d’un poème données dans la table des matières ont été reportées comme sous-titre des poèmes. L’année d’édition a été ajoutée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell (correction d’erreur d’océrisation).</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-01-14" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-01-14" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">TRADUCTIONS <lb></lb>en vers français <lb></lb>par <lb></lb>LUCIE DELARUE-MARDRUS</head><head type="main_subpart">Six Poèmes d’Emily Brontë</head><div type="poem" key="DLR1119">
						<head type="main">Martyre de l’Honneur</head>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">La</w> <w n="1.2">lune</w> <w n="1.3">est</w> <w n="1.4">pleine</w>, <w n="1.5">cette</w> <w n="1.6">nuit</w>.</l>
							<l n="2" num="1.2"><space unit="char" quantity="4"></space><w n="2.1">Peu</w> <w n="2.2">d</w>’<w n="2.3">étoiles</w>, <w n="2.4">mais</w> <w n="2.5">claires</w>.</l>
							<l n="3" num="1.3"><w n="3.1">Sur</w> <w n="3.2">les</w> <w n="3.3">carreaux</w> <w n="3.4">le</w> <w n="3.5">givre</w> <w n="3.6">luit</w></l>
							<l n="4" num="1.4"><space unit="char" quantity="4"></space><w n="4.1">Imitant</w> <w n="4.2">des</w> <w n="4.3">fougères</w>.</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1"><w n="5.1">Par</w> <w n="5.2">la</w> <w n="5.3">persienne</w> <w n="5.4">des</w> <w n="5.5">lueurs</w></l>
							<l n="6" num="2.2"><space unit="char" quantity="4"></space><w n="6.1">De</w> <w n="6.2">jour</w> <w n="6.3">baignent</w> <w n="6.4">la</w> <w n="6.5">chambre</w>.</l>
							<l n="7" num="2.3"><w n="7.1">Vous</w> <w n="7.2">passez</w> <w n="7.3">là</w>, <w n="7.4">malgré</w> <w n="7.5">décembre</w>,</l>
							<l n="8" num="2.4"><space unit="char" quantity="4"></space><w n="8.1">Des</w> <w n="8.2">heures</w> <w n="8.3">de</w> <w n="8.4">douceur</w>,</l>
						</lg>
						<lg n="3">
							<l n="9" num="3.1"><w n="9.1">Tandis</w> <w n="9.2">que</w>, <w n="9.3">domptant</w> <w n="9.4">avec</w> <w n="9.5">peine</w></l>
							<l n="10" num="3.2"><space unit="char" quantity="4"></space><w n="10.1">Cette</w> <w n="10.2">angoisse</w> <w n="10.3">que</w> <w n="10.4">j</w>’<w n="10.5">ai</w>,</l>
							<l n="11" num="3.3"><w n="11.1">J</w>’<w n="11.2">arpente</w> <w n="11.3">la</w> <w n="11.4">maison</w> <w n="11.5">sereine</w></l>
							<l n="12" num="3.4"><space unit="char" quantity="4"></space><w n="12.1">Sans</w> <w n="12.2">pouvoir</w> <w n="12.3">reposer</w>.</l>
						</lg>
						<lg n="4">
							<l n="13" num="4.1"><w n="13.1">Dans</w> <w n="13.2">le</w> <w n="13.3">hall</w> <w n="13.4">l</w>’<w n="13.5">horloge</w> <w n="13.6">ancienne</w></l>
							<l n="14" num="4.2"><space unit="char" quantity="4"></space><w n="14.1">D</w>’<w n="14.2">heure</w> <w n="14.3">en</w> <w n="14.4">heure</w> <w n="14.5">s</w>’<w n="14.6">entend</w>.</l>
							<l n="15" num="4.3"><w n="15.1">Il</w> <w n="15.2">semble</w> <w n="15.3">que</w> <w n="15.4">ses</w> <w n="15.5">coups</w> <w n="15.6">reviennent</w></l>
							<l n="16" num="4.4"><space unit="char" quantity="4"></space><w n="16.1">Toujours</w> <w n="16.2">plus</w> <w n="16.3">lentement</w>.</l>
						</lg>
						<lg n="5">
							<l n="17" num="5.1"><w n="17.1">Que</w> <w n="17.2">longue</w>. <w n="17.3">l</w>’<w n="17.4">étoile</w> <w n="17.5">qui</w> <w n="17.6">tremble</w>.</l>
							<l n="18" num="5.2"><space unit="char" quantity="4"></space><w n="18.1">A</w> <w n="18.2">faire</w> <w n="18.3">son</w> <w n="18.4">chemin</w> !</l>
							<l n="19" num="5.3"><w n="19.1">Quoi</w> ! <w n="19.2">Toujours</w> <w n="19.3">là</w> ?… <w n="19.4">Jamais</w>. <w n="19.5">il</w> <w n="19.6">semble</w>,</l>
							<l n="20" num="5.4"><space unit="char" quantity="4"></space><w n="20.1">Ne</w> <w n="20.2">luira</w> <w n="20.3">le</w> <w n="20.4">matin</w>.</l>
						</lg>
						<lg n="6">
							<l n="21" num="6.1"><w n="21.1">Je</w> <w n="21.2">suis</w> <w n="21.3">debout</w> <w n="21.4">à</w> <w n="21.5">votre</w> <w n="21.6">porte</w>.</l>
							<l n="22" num="6.2"><space unit="char" quantity="4"></space><w n="22.1">Mon</w> <w n="22.2">amour</w>, <w n="22.3">dormez</w>-<w n="22.4">vous</w> ?</l>
							<l n="23" num="6.3"><w n="23.1">Mon</w> <w n="23.2">cœur</w>, <w n="23.3">sous</w> <w n="23.4">la</w> <w n="23.5">main</w> <w n="23.6">que</w> <w n="23.7">j</w>’<w n="23.8">y</w> <w n="23.9">porte</w>,</l>
							<l n="24" num="6.4"><space unit="char" quantity="4"></space><w n="24.1">N</w>’<w n="24.2">a</w> <w n="24.3">presque</w> <w n="24.4">plus</w> <w n="24.5">de</w> <w n="24.6">coups</w>.</l>
						</lg>
						<lg n="7">
							<l n="25" num="7.1"><w n="25.1">Froid</w>, <w n="25.2">froid</w> <w n="25.3">le</w> <w n="25.4">vent</w> <w n="25.5">d</w>’<w n="25.6">est</w> <w n="25.7">qui</w> <w n="25.8">sanglote</w>,</l>
							<l n="26" num="7.2"><space unit="char" quantity="4"></space><w n="26.1">Éloignant</w> <w n="26.2">peu</w> <w n="26.3">à</w> <w n="26.4">peu</w></l>
							<l n="27" num="7.3"><w n="27.1">La</w> <w n="27.2">cloche</w> <w n="27.3">des</w> <w n="27.4">tours</w>, <w n="27.5">dont</w> <w n="27.6">la</w> <w n="27.7">note</w></l>
							<l n="28" num="7.4"><space unit="char" quantity="4"></space><w n="28.1">Meurt</w> <w n="28.2">comme</w> <w n="28.3">mon</w> <w n="28.4">adieu</w>.</l>
						</lg>
						<lg n="8">
							<l n="29" num="8.1"><w n="29.1">Sur</w> <w n="29.2">moi</w>, <w n="29.3">demain</w>, <w n="29.4">la</w> <w n="29.5">flétrissure</w>,</l>
							<l n="30" num="8.2"><space unit="char" quantity="4"></space><w n="30.1">La</w> <w n="30.2">haine</w> <w n="30.3">en</w> <w n="30.4">tous</w> <w n="30.5">les</w> <w n="30.6">yeux</w> :</l>
							<l n="31" num="8.3"><w n="31.1">Je</w> <w n="31.2">porterai</w> <w n="31.3">les</w> <w n="31.4">noms</w> <w n="31.5">honteux</w></l>
							<l n="32" num="8.4"><space unit="char" quantity="4"></space><w n="32.1">De</w> <w n="32.2">traître</w> <w n="32.3">et</w> <w n="32.4">de</w> <w n="32.5">parjure</w>.</l>
						</lg>
						<lg n="9">
							<l n="33" num="9.1"><w n="33.1">Mes</w> <w n="33.2">faux</w> <w n="33.3">amis</w> <w n="33.4">ricaneront</w>,</l>
							<l n="34" num="9.2"><space unit="char" quantity="4"></space><w n="34.1">Les</w> <w n="34.2">vrais</w> <w n="34.3">me</w> <w n="34.4">voudront</w> <w n="34.5">morte</w>.</l>
							<l n="35" num="9.3"><w n="35.1">Les</w> <w n="35.2">pleurs</w> <w n="35.3">que</w> <w n="35.4">mes</w> <w n="35.5">yeux</w> <w n="35.6">verseront</w></l>
							<l n="36" num="9.4"><space unit="char" quantity="4"></space><w n="36.1">Seront</w> <w n="36.2">d</w>’<w n="36.3">amère</w> <w n="36.4">sorte</w>.</l>
						</lg>
						<lg n="10">
							<l n="37" num="10.1"><w n="37.1">Votre</w> <w n="37.2">race</w> <w n="37.3">de</w> <w n="37.4">hors</w> <w n="37.5">la</w> <w n="37.6">loi</w></l>
							<l n="38" num="10.2"><space unit="char" quantity="4"></space><w n="38.1">Malgré</w> <w n="38.2">sa</w> <w n="38.3">trace</w> <w n="38.4">noire</w></l>
							<l n="39" num="10.3"><w n="39.1">Verra</w> <w n="39.2">pardonner</w> <w n="39.3">son</w> <w n="39.4">histoire</w></l>
							<l n="40" num="10.4"><space unit="char" quantity="4"></space><w n="40.1">Hormis</w> <w n="40.2">mon</w> <w n="40.3">crime</w> <w n="40.4">à</w> <w n="40.5">moi</w>.</l>
						</lg>
						<lg n="11">
							<l n="41" num="11.1"><w n="41.1">Car</w> <w n="41.2">qui</w> <w n="41.3">donc</w> <w n="41.4">pardonne</w> <w n="41.5">à</w> <w n="41.6">ce</w> <w n="41.7">crime</w> :</l>
							<l n="42" num="11.2"><space unit="char" quantity="4"></space><w n="42.1">La</w> <w n="42.2">lâche</w> <w n="42.3">fausseté</w> ?</l>
							<l n="43" num="11.3"><w n="43.1">Champion</w> <w n="43.2">de</w> <w n="43.3">la</w> <w n="43.4">liberté</w>,</l>
							<l n="44" num="11.4"><space unit="char" quantity="4"></space><w n="44.1">La</w> <w n="44.2">révolte</w> <w n="44.3">est</w> <w n="44.4">sublime</w> ;</l>
						</lg>
						<lg n="12">
							<l n="45" num="12.1"><w n="45.1">Pour</w> <w n="45.2">certaines</w> <w n="45.3">haines</w> <w n="45.4">qu</w>’<w n="45.5">on</w> <w n="45.6">a</w>,</l>
							<l n="46" num="12.2"><space unit="char" quantity="4"></space><w n="46.1">Juste</w> <w n="46.2">est</w> <w n="46.3">le</w> <w n="46.4">poignard</w> <w n="46.5">même</w>.</l>
							<l n="47" num="12.3"><w n="47.1">Mais</w> <w n="47.2">traître</w>, « <w n="47.3">traître</w> », <w n="47.4">ce</w> <w n="47.5">mot</w> <w n="47.6">là</w></l>
							<l n="48" num="12.4"><space unit="char" quantity="4"></space><w n="48.1">Soulève</w> <w n="48.2">l</w>’<w n="48.3">anathème</w>.</l>
						</lg>
						<lg n="13">
							<l n="49" num="13.1"><w n="49.1">Plutôt</w> <w n="49.2">que</w> <w n="49.3">de</w> <w n="49.4">perdre</w> <w n="49.5">l</w>’<w n="49.6">honneur</w></l>
							<l n="50" num="13.2"><space unit="char" quantity="4"></space><w n="50.1">Oh</w> ! <w n="50.2">être</w> <w n="50.3">déchirée</w> !</l>
							<l n="51" num="13.3"><w n="51.1">J</w>’<w n="51.2">aime</w> <w n="51.3">mieux</w> <w n="51.4">pourtant</w> <w n="51.5">la</w> <w n="51.6">curée</w></l>
							<l n="52" num="13.4"><space unit="char" quantity="4"></space><w n="52.1">Que</w> <w n="52.2">mentir</w> <w n="52.3">à</w> <w n="52.4">mon</w> <w n="52.5">cœur</w>.</l>
						</lg>
						<lg n="14">
							<l n="53" num="14.1"><w n="53.1">Moi</w> <w n="53.2">tromper</w> <w n="53.3">mon</w> <w n="53.4">cher</w> <w n="53.5">amour</w>, <w n="53.6">même</w></l>
							<l n="54" num="14.2"><space unit="char" quantity="4"></space><w n="54.1">Pour</w> <w n="54.2">vous</w> <w n="54.3">garder</w> <w n="54.4">à</w> <w n="54.5">moi</w> ?</l>
							<l n="55" num="14.3"><w n="55.1">Non</w> ! <w n="55.2">L</w>’<w n="55.3">avenir</w>, <w n="55.4">preuve</w> <w n="55.5">suprême</w>,</l>
							<l n="56" num="14.4"><space unit="char" quantity="4"></space><w n="56.1">Vous</w> <w n="56.2">fera</w> <w n="56.3">croire</w> <w n="56.4">en</w> <w n="56.5">moi</w>.</l>
						</lg>
						<lg n="15">
							<l n="57" num="15.1"><w n="57.1">Je</w> <w n="57.2">sais</w>, <w n="57.3">moi</w>, <w n="57.4">que</w> <w n="57.5">la</w> <w n="57.6">juste</w> <w n="57.7">voie</w></l>
							<l n="58" num="15.2"><space unit="char" quantity="4"></space><w n="58.1">Est</w> <w n="58.2">celle</w> <w n="58.3">que</w> <w n="58.4">je</w> <w n="58.5">suis</w>.</l>
							<l n="59" num="15.3"><w n="59.1">Ce</w> <w n="59.2">devoir</w> <w n="59.3">dont</w> <w n="59.4">je</w> <w n="59.5">suis</w> <w n="59.6">la</w> <w n="59.7">proie</w></l>
							<l n="60" num="15.4"><space unit="char" quantity="4"></space><w n="60.1">M</w>’<w n="60.2">abîme</w> <w n="60.3">dans</w> <w n="60.4">la</w> <w n="60.5">nuit</w>,</l>
						</lg>
						<lg n="16">
							<l n="61" num="16.1"><w n="61.1">Et</w> <w n="61.2">que</w> <w n="61.3">la</w> <w n="61.4">honte</w> <w n="61.5">universelle</w></l>
							<l n="62" num="16.2"><space unit="char" quantity="4"></space><w n="62.1">Me</w> <w n="62.2">retire</w> <w n="62.3">l</w>’<w n="62.4">honneur</w>,</l>
							<l n="63" num="16.3"><w n="63.1">Qu</w>’<w n="63.2">importe</w> ! <w n="63.3">Dans</w> <w n="63.4">mon</w> <w n="63.5">propre</w> <w n="63.6">cœur</w></l>
							<l n="64" num="16.4"><space unit="char" quantity="4"></space><w n="64.1">Je</w> <w n="64.2">me</w> <w n="64.3">sais</w>, <w n="64.4">moi</w>, <w n="64.5">fidèle</w>.</l>
						</lg>
					</div></body></text></TEI>