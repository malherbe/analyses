<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CHOIX DE POÈMES</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1917 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">DLR_13</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Choix de poèmes</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie Alphonse Lemerre</publisher>
							<date when="1951">1951</date>
						</imprint>
					</monogr>
					<note>Édition numérisée à partir d’un ouvrage prêté par la BPU de neuchatel</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1951">1951</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La partie en prose (FRAGMENT D’UN TEXTE DE L’AUTEUR PRÉFAÇANT DEUX POÈMES D’EDGAR POE) n’est incluse.</p>
				<p>Les indications de la première édition d’un poème données dans la table des matières ont été reportées comme sous-titre des poèmes. L’année d’édition a été ajoutée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell (correction d’erreur d’océrisation).</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-01-14" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-01-14" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CHAPITRE IX</head><head type="main_part">Derniers Poèmes</head><head type="sub_part">INÉDITS</head><div type="poem" key="DLR1107">
					<head type="main">Printemps</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Un</w> <w n="1.2">arbre</w> <w n="1.3">que</w> <w n="1.4">Je</w> <w n="1.5">vois</w> <w n="1.6">de</w> <w n="1.7">loin</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Un</w> <w n="2.2">oiseau</w> <w n="2.3">que</w> <w n="2.4">j</w>’<w n="2.5">entends</w> <w n="2.6">à</w> <w n="2.7">peine</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Le</w> <w n="3.2">feu</w> <w n="3.3">dont</w> <w n="3.4">je</w> <w n="3.5">ne</w> <w n="3.6">prends</w> <w n="3.7">plus</w> <w n="3.8">soin</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Sur</w> <w n="4.2">mon</w> <w n="4.3">dos</w> <w n="4.4">un</w> <w n="4.5">peu</w> <w n="4.6">moins</w> <w n="4.7">de</w> <w n="4.8">laine</w>,</l>
						<l n="5" num="1.5"><w n="5.1">Le</w> <w n="5.2">printemps</w>, <w n="5.3">pour</w> <w n="5.4">moi</w>, <w n="5.5">c</w>’<w n="5.6">est</w> <w n="5.7">cela_</w></l>
					</lg>
					<lg n="2">
						<l n="6" num="2.1"><w n="6.1">Puisqu</w>’<w n="6.2">il</w> <w n="6.3">faut</w> <w n="6.4">que</w> <w n="6.5">je</w> <w n="6.6">souffre</w> <w n="6.7">là</w></l>
						<l n="7" num="2.2"><w n="7.1">Toujours</w> <w n="7.2">en</w> <w n="7.3">dehors</w> <w n="7.4">de</w> <w n="7.5">la</w> <w n="7.6">danse</w>,</l>
						<l n="8" num="2.3"><w n="8.1">Mon</w> <w n="8.2">Dieu</w> ! <w n="8.3">Que</w> <w n="8.4">me</w> <w n="8.5">resterait</w>-<w n="8.6">il</w></l>
						<l n="9" num="2.4"><w n="9.1">De</w> <w n="9.2">la</w> <w n="9.3">nature</w> <w n="9.4">en</w> <w n="9.5">plein</w> <w n="9.6">avril</w></l>
						<l n="10" num="2.5"><w n="10.1">Si</w> <w n="10.2">je</w> <w n="10.3">n</w>’<w n="10.4">avais</w> <w n="10.5">la</w> <w n="10.6">souvenance</w> ?</l>
					</lg>
				</div></body></text></TEI>