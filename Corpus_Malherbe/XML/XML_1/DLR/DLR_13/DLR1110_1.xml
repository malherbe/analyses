<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CHOIX DE POÈMES</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1917 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">DLR_13</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Choix de poèmes</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie Alphonse Lemerre</publisher>
							<date when="1951">1951</date>
						</imprint>
					</monogr>
					<note>Édition numérisée à partir d’un ouvrage prêté par la BPU de neuchatel</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1951">1951</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La partie en prose (FRAGMENT D’UN TEXTE DE L’AUTEUR PRÉFAÇANT DEUX POÈMES D’EDGAR POE) n’est incluse.</p>
				<p>Les indications de la première édition d’un poème données dans la table des matières ont été reportées comme sous-titre des poèmes. L’année d’édition a été ajoutée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell (correction d’erreur d’océrisation).</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-01-14" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-01-14" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CHAPITRE IX</head><head type="main_part">Derniers Poèmes</head><head type="sub_part">INÉDITS</head><div type="poem" key="DLR1110">
					<head type="main">Par ma fenêtre ouverte…</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Par</w> <w n="1.2">ma</w> <w n="1.3">fenêtre</w> <w n="1.4">ouverte</w> <w n="1.5">où</w> <w n="1.6">la</w> <w n="1.7">clarté</w> <w n="1.8">s</w>’<w n="1.9">attarde</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Dans</w> <w n="2.2">la</w> <w n="2.3">douceur</w> <w n="2.4">du</w> <w n="2.5">soir</w> <w n="2.6">printanier</w>, <w n="2.7">je</w> <w n="2.8">regarde</w>…</l>
					</lg>
					<lg n="2">
						<l n="3" num="2.1"><w n="3.1">Chaque</w> <w n="3.2">arbre</w>, <w n="3.3">chaque</w> <w n="3.4">toit</w> <w n="3.5">qui</w> <w n="3.6">s</w>’<w n="3.7">élance</w> <w n="3.8">dans</w> <w n="3.9">l</w>’<w n="3.10">air</w>,</l>
						<l n="4" num="2.2"><w n="4.1">Tel</w> <w n="4.2">le</w> <w n="4.3">roc</w> <w n="4.4">qui</w> <w n="4.5">finit</w> <w n="4.6">où</w> <w n="4.7">commence</w> <w n="4.8">la</w> <w n="4.9">mer</w>,</l>
						<l n="5" num="2.3"><w n="5.1">Marque</w> <w n="5.2">la</w> <w n="5.3">fin</w> <w n="5.4">d</w>’<w n="5.5">un</w> <w n="5.6">monde</w> <w n="5.7">au</w> <w n="5.8">bord</w> <w n="5.9">d</w>’<w n="5.10">un</w> <w n="5.11">autre</w> <w n="5.12">monde</w>.</l>
						<l n="6" num="2.4"><w n="6.1">Ici</w> <w n="6.2">la</w> <w n="6.3">terre</w> <w n="6.4">et</w> <w n="6.5">là</w> <w n="6.6">le</w> <w n="6.7">vide</w> <w n="6.8">où</w>, <w n="6.9">toute</w> <w n="6.10">ronde</w>,</l>
						<l n="7" num="2.5"><w n="7.1">Cette</w> <w n="7.2">terre</w>, <w n="7.3">toupie</w> <w n="7.4">en</w> <w n="7.5">marche</w> <w n="7.6">dans</w> <w n="7.7">l</w>’<w n="7.8">éther</w>,</l>
						<l n="8" num="2.6"><space unit="char" quantity="8"></space><w n="8.1">Sans</w> <w n="8.2">sa</w> <w n="8.3">pauvre</w> <w n="8.4">ceinture</w> <w n="8.5">d</w>’<w n="8.6">air</w></l>
						<l n="9" num="2.7"><w n="9.1">Ne</w> <w n="9.2">serait</w> <w n="9.3">à</w> <w n="9.4">son</w> <w n="9.5">tour</w> <w n="9.6">qu</w>’<w n="9.7">une</w> <w n="9.8">lune</w> <w n="9.9">inféconde</w>.</l>
					</lg>
					<lg n="3">
						<l n="10" num="3.1"><w n="10.1">Je</w> <w n="10.2">contemple</w> <w n="10.3">ce</w> <w n="10.4">toit</w> <w n="10.5">et</w> <w n="10.6">cet</w> <w n="10.7">arbre</w>, <w n="10.8">montés</w></l>
						<l n="11" num="3.2"><w n="11.1">Vers</w> <w n="11.2">l</w>’<w n="11.3">insondable</w> <w n="11.4">énigme</w> <w n="11.5">et</w> <w n="11.6">ses</w> <w n="11.7">immensités</w>.</l>
						<l n="12" num="3.3"><w n="12.1">En</w> <w n="12.2">bas</w>, <w n="12.3">la</w> <w n="12.4">rue</w> <w n="12.5">est</w> <w n="12.6">calme</w> <w n="12.7">et</w> <w n="12.8">le</w> <w n="12.9">printemps</w> <w n="12.10">tranquille</w>.</l>
						<l n="13" num="3.4"><w n="13.1">Rien</w> <w n="13.2">ne</w> <w n="13.3">trouble</w> <w n="13.4">la</w> <w n="13.5">paix</w> <w n="13.6">de</w> <w n="13.7">la</w> <w n="13.8">petite</w> <w n="13.9">ville</w>.</l>
						<l n="14" num="3.5"><w n="14.1">On</w> <w n="14.2">entend</w> <w n="14.3">au</w> <w n="14.4">lointain</w> <w n="14.5">un</w> <w n="14.6">merle</w>. <w n="14.7">Il</w> <w n="14.8">fait</w> <w n="14.9">très</w> <w n="14.10">beau</w>.</l>
						<l part="I" n="15" num="3.6"><w n="15.1">C</w>’<w n="15.2">est</w> <w n="15.3">tout</w>. </l>
						<l part="F" n="15" num="3.6">— <w n="15.4">Pourquoi</w> <w n="15.5">mes</w> <w n="15.6">yeux</w> <w n="15.7">regardent</w>-<w n="15.8">ils</w> <w n="15.9">si</w> <w n="15.10">haut</w> ?</l>
					</lg>
				</div></body></text></TEI>