<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CHOIX DE POÈMES</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1917 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">DLR_13</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Choix de poèmes</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie Alphonse Lemerre</publisher>
							<date when="1951">1951</date>
						</imprint>
					</monogr>
					<note>Édition numérisée à partir d’un ouvrage prêté par la BPU de neuchatel</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1951">1951</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La partie en prose (FRAGMENT D’UN TEXTE DE L’AUTEUR PRÉFAÇANT DEUX POÈMES D’EDGAR POE) n’est incluse.</p>
				<p>Les indications de la première édition d’un poème données dans la table des matières ont été reportées comme sous-titre des poèmes. L’année d’édition a été ajoutée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell (correction d’erreur d’océrisation).</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-01-14" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-01-14" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CHAPITRE VI</head><head type="main_part">Religion</head><div type="poem" key="DLR1083">
					<head type="main">Ode aux Juifs</head>
					<head type="sub_2">LA FIGURE DE PROUE, Fasquelle, 1908</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Je</w> <w n="1.2">vous</w> <w n="1.3">ai</w> <w n="1.4">vus</w>, <w n="1.5">les</w> <w n="1.6">Juifs</w>, <w n="1.7">dans</w> <w n="1.8">l</w>’<w n="1.9">horreur</w> <w n="1.10">du</w> <w n="1.11">ghetto</w></l>
						<l n="2" num="1.2"><w n="2.1">De</w> <w n="2.2">vos</w> <w n="2.3">pays</w> <w n="2.4">originels</w>, <w n="2.5">soleil</w> <w n="2.6">et</w> <w n="2.7">sable</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Vivre</w> <w n="3.2">à</w> <w n="3.3">l</w>’<w n="3.4">écart</w> <w n="3.5">votre</w> <w n="3.6">existence</w> <w n="3.7">misérable</w></l>
						<l n="4" num="1.4"><w n="4.1">Sur</w> <w n="4.2">quoi</w> <w n="4.3">le</w> <w n="4.4">monde</w> <w n="4.5">a</w> <w n="4.6">mis</w> <w n="4.7">un</w> <w n="4.8">éternel</w> <w n="4.9">veto</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">J</w>’<w n="5.2">ai</w> <w n="5.3">vu</w> <w n="5.4">monter</w> <w n="5.5">la</w> <w n="5.6">garde</w> <w n="5.7">ironique</w> <w n="5.8">et</w> <w n="5.9">cruelle</w></l>
						<l n="6" num="2.2"><w n="6.1">De</w> <w n="6.2">l</w>’<w n="6.3">Arabe</w>, <w n="6.4">mortel</w> <w n="6.5">ennemi</w> <w n="6.6">de</w> !’<w n="6.7">Hébreu</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Dont</w> <w n="7.2">l</w>’<w n="7.3">orgueil</w> <w n="7.4">bédouin</w> <w n="7.5">maintenait</w> <w n="7.6">en</w> <w n="7.7">tutelle</w></l>
						<l n="8" num="2.4"><w n="8.1">Votre</w> <w n="8.2">caste</w> <w n="8.3">maudite</w> <w n="8.4">et</w> <w n="8.5">destinée</w> <w n="8.6">au</w> <w n="8.7">feu</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Le</w> <w n="9.2">long</w> <w n="9.3">de</w> <w n="9.4">vos</w> <w n="9.5">taudis</w> <w n="9.6">où</w> <w n="9.7">la</w> <w n="9.8">tête</w> <w n="9.9">se</w> <w n="9.10">cogne</w>,</l>
						<l n="10" num="3.2"><w n="10.1">La</w> <w n="10.2">vermine</w>, <w n="10.3">la</w> <w n="10.4">puanteur</w>, <w n="10.5">l</w>’<w n="10.6">obscurité</w></l>
						<l n="11" num="3.3"><w n="11.1">Grouillaient</w> <w n="11.2">atrocement</w> <w n="11.3">dans</w> <w n="11.4">l</w>’<w n="11.5">immuable</w> <w n="11.6">été</w></l>
						<l n="12" num="3.4"><w n="12.1">Du</w> <w n="12.2">Sud</w>, <w n="12.3">comme</w> <w n="12.4">une</w> <w n="12.5">immense</w> <w n="12.6">et</w> <w n="12.7">multiple</w> <w n="12.8">charogne</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Et</w> <w n="13.2">vous</w> <w n="13.3">célébriez</w> <w n="13.4">la</w> <w n="13.5">Pâque</w> <w n="13.6">sans</w> <w n="13.7">bonheur</w></l>
						<l n="14" num="4.2"><w n="14.1">Par</w> <w n="14.2">les</w> <w n="14.3">chants</w> <w n="14.4">étouffés</w> <w n="14.5">de</w> <w n="14.6">votre</w> <w n="14.7">foule</w> <w n="14.8">vile</w>,</l>
						<l n="15" num="4.3"><w n="15.1">Et</w> <w n="15.2">vos</w> <w n="15.3">enfants</w> <w n="15.4">riaient</w> <w n="15.5">sous</w> <w n="15.6">les</w> <w n="15.7">roses</w> <w n="15.8">et</w> <w n="15.9">l</w>’<w n="15.10">huile</w>,</l>
						<l n="16" num="4.4"><w n="16.1">Avec</w> <w n="16.2">des</w> <w n="16.3">yeux</w> <w n="16.4">humiliés</w> <w n="16.5">et</w> <w n="16.6">pleins</w> <w n="16.7">de</w> <w n="16.8">peur</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Mais</w> <w n="17.2">dans</w> <w n="17.3">ces</w> <w n="17.4">yeux</w> <w n="17.5">de</w> <w n="17.6">velours</w> <w n="17.7">noir</w> <w n="17.8">ou</w> <w n="17.9">de</w> <w n="17.10">pervenche</w></l>
						<l n="18" num="5.2"><w n="18.1">Une</w> <w n="18.2">sourde</w> <w n="18.3">éloquence</w> <w n="18.4">allumait</w> <w n="18.5">le</w> <w n="18.6">regard</w>,</l>
						<l n="19" num="5.3"><w n="19.1">Et</w> <w n="19.2">ces</w> <w n="19.3">yeux</w> <w n="19.4">nous</w> <w n="19.5">disaient</w> <w n="19.6">au</w> <w n="19.7">passage</w> : « <w n="19.8">Plus</w> <w n="19.9">tard</w> !</l>
						<l n="20" num="5.4">« <w n="20.1">Ne</w> <w n="20.2">connaissez</w>-<w n="20.3">vous</w> <w n="20.4">pas</w> <w n="20.5">déjà</w> <w n="20.6">notre</w> <w n="20.7">revanche</w> ?</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1">« <w n="21.1">Vous</w> <w n="21.2">savez</w> <w n="21.3">bien</w>, <w n="21.4">pourtant</w>, <w n="21.5">où</w> <w n="21.6">vivent</w> <w n="21.7">nos</w> <w n="21.8">aînés</w> !</l>
						<l n="22" num="6.2">« <w n="22.1">Votre</w> <w n="22.2">race</w>, <w n="22.3">au</w> <w n="22.4">delà</w> <w n="22.5">des</w> <w n="22.6">mers</w>, <w n="22.7">en</w> <w n="22.8">est</w> <w n="22.9">enceinte</w>.</l>
						<l n="23" num="6.3">« <w n="23.1">Vous</w> <w n="23.2">avez</w> <w n="23.3">dans</w> <w n="23.4">le</w> <w n="23.5">sang</w> <w n="23.6">l</w>’<w n="23.7">ineffaçable</w> <w n="23.8">empreinte</w></l>
						<l n="24" num="6.4">« <w n="24.1">De</w> <w n="24.2">leur</w> <w n="24.3">bouche</w> <w n="24.4">lippue</w> <w n="24.5">et</w> <w n="24.6">de</w> <w n="24.7">leur</w> <w n="24.8">puissant</w> <w n="24.9">nez</w>.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1">« <w n="25.1">Regardez</w>-<w n="25.2">les</w> <w n="25.3">de</w> <w n="25.4">près</w>, <w n="25.5">nos</w> <w n="25.6">yeux</w> <w n="25.7">opiniâtres</w> !</l>
						<l n="26" num="7.2">« <w n="26.1">Oui</w>, <w n="26.2">nous</w> <w n="26.3">sommes</w> <w n="26.4">hués</w>, <w n="26.5">méprisés</w>, <w n="26.6">avilis</w>,</l>
						<l n="27" num="7.3">« <w n="27.1">Mais</w> <w n="27.2">nous</w> <w n="27.3">posséderons</w> <w n="27.4">vos</w> <w n="27.5">trônes</w> <w n="27.6">et</w> <w n="27.7">vos</w> <w n="27.8">lits</w>,</l>
						<l n="28" num="7.4">« <w n="28.1">Vos</w> <w n="28.2">commerces</w>, <w n="28.3">vos</w> <w n="28.4">lupanars</w> <w n="28.5">et</w> <w n="28.6">vos</w> <w n="28.7">théâtres</w>.</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1">« <w n="29.1">Nous</w> <w n="29.2">serons</w> <w n="29.3">accroupis</w> <w n="29.4">au</w> <w n="29.5">fond</w> <w n="29.6">de</w> <w n="29.7">tout</w>. <w n="29.8">Bien</w> <w n="29.9">mieux</w> !</l>
						<l n="30" num="8.2">« <w n="30.1">Pour</w> <w n="30.2">finir</w> <w n="30.3">la</w> <w n="30.4">vengeance</w> <w n="30.5">effroyable</w> <w n="30.6">et</w> <w n="30.7">rusée</w>,</l>
						<l n="31" num="8.3">« <w n="31.1">Nous</w>, <w n="31.2">sangs</w> <w n="31.3">purs</w> <w n="31.4">fourvoyés</w> <w n="31.5">dans</w> <w n="31.6">votre</w> <w n="31.7">foule</w> <w n="31.8">usée</w>,</l>
						<l n="32" num="8.4">« <w n="32.1">Nous</w> <w n="32.2">vous</w> <w n="32.3">enfanterons</w> <w n="32.4">sournoisement</w> <w n="32.5">des</w> <w n="32.6">dieux</w>.</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1">« <w n="33.1">C</w>’<w n="33.2">est</w> <w n="33.3">pour</w> <w n="33.4">un</w> <w n="33.5">Juif</w> <w n="33.6">divin</w> <w n="33.7">sorti</w> <w n="33.8">de</w> <w n="33.9">nos</w> <w n="33.10">étables</w></l>
						<l n="34" num="9.2">« <w n="34.1">Que</w> <w n="34.2">vos</w> <w n="34.3">orgues</w> <w n="34.4">s</w>’<w n="34.5">enrouent</w> <w n="34.6">et</w> <w n="34.7">que</w> <w n="34.8">dansent</w> <w n="34.9">vos</w> <w n="34.10">fleurs</w>.</l>
						<l n="35" num="9.3">« <w n="35.1">A</w> <w n="35.2">nous</w> <w n="35.3">les</w> <w n="35.4">papes</w> <w n="35.5">blancs</w>, <w n="35.6">l</w>’<w n="35.7">encens</w>, <w n="35.8">les</w> <w n="35.9">saintes</w> <w n="35.10">tables</w>,</l>
						<l n="36" num="9.4">« <w n="36.1">Toutes</w> <w n="36.2">les</w> <w n="36.3">Notre</w>-<w n="36.4">Dame</w> <w n="36.5">et</w> <w n="36.6">tous</w> <w n="36.7">les</w> <w n="36.8">Sacré</w>-<w n="36.9">Cœur</w> !</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1">« <w n="37.1">Le</w> <w n="37.2">Ghetto</w> !… <w n="37.3">N</w>’<w n="37.4">est</w>-<w n="37.5">ce</w> <w n="37.6">pas</w> <w n="37.7">pour</w> <w n="37.8">la</w> <w n="37.9">petite</w> <w n="37.10">Juive</w>,</l>
						<l n="38" num="10.2">« <w n="38.1">Pour</w> <w n="38.2">cette</w> <w n="38.3">Myriam</w> <w n="38.4">de</w> <w n="38.5">chez</w> <w n="38.6">nous</w>, <w n="38.7">cependant</w>,</l>
						<l n="39" num="10.3">« <w n="39.1">Que</w> <w n="39.2">tant</w> <w n="39.3">d</w>’<w n="39.4">architecture</w> <w n="39.5">inouïe</w> <w n="39.6">et</w> <w n="39.7">naïve</w></l>
						<l n="40" num="10.4">« <w n="40.1">Se</w> <w n="40.2">dresse</w> <w n="40.3">sur</w> <w n="40.4">l</w>’<w n="40.5">amas</w> <w n="40.6">des</w> <w n="40.7">villes</w> <w n="40.8">d</w>’<w n="40.9">Occident</w> ?</l>
					</lg>
					<lg n="11">
						<l n="41" num="11.1">« <w n="41.1">C</w>’<w n="41.2">est</w> <w n="41.3">nous</w>, <w n="41.4">votre</w> <w n="41.5">au</w>-<w n="41.6">delà</w>, <w n="41.7">vos</w> <w n="41.8">terreurs</w>, <w n="41.9">tous</w> <w n="41.10">vos</w> <w n="41.11">râles</w>,</l>
						<l n="42" num="11.2">« <w n="42.1">Nous</w> <w n="42.2">vous</w> <w n="42.3">avons</w> <w n="42.4">tordus</w> <w n="42.5">du</w> <w n="42.6">fond</w> <w n="42.7">de</w> <w n="42.8">notre</w> <w n="42.9">Sud</w>,</l>
						<l n="43" num="11.3">« <w n="43.1">Et</w> <w n="43.2">nous</w> <w n="43.3">chantons</w> <w n="43.4">sur</w> <w n="43.5">vos</w> <w n="43.6">cités</w> <w n="43.7">notre</w> <w n="43.8">Talmud</w>,</l>
						<l n="44" num="11.4">« <w n="44.1">Et</w> <w n="44.2">vous</w> <w n="44.3">nous</w> <w n="44.4">bâtirez</w> <w n="44.5">encor</w> <w n="44.6">des</w> <w n="44.7">cathédrales</w>.</l>
					</lg>
					<lg n="12">
						<l n="45" num="12.1">« <w n="45.1">Que</w> <w n="45.2">les</w> <w n="45.3">deux</w> <w n="45.4">Orients</w> <w n="45.5">et</w> <w n="45.6">les</w> <w n="45.7">deux</w> <w n="45.8">Occidents</w></l>
						<l n="46" num="12.2">« <w n="46.1">Nous</w> <w n="46.2">gardent</w> ! <w n="46.3">Nous</w> <w n="46.4">saurons</w> <w n="46.5">trouver</w> <w n="46.6">notre</w> <w n="46.7">royaume</w>,</l>
						<l n="47" num="12.3">« <w n="47.1">Et</w> <w n="47.2">nous</w> <w n="47.3">regarderons</w> <w n="47.4">tourner</w> <w n="47.5">dans</w> <w n="47.6">notre</w> <w n="47.7">paume</w></l>
						<l n="48" num="12.4">« <w n="48.1">Le</w> <w n="48.2">monde</w>. <w n="48.3">Et</w> <w n="48.4">c</w>’<w n="48.5">est</w> <w n="48.6">pourquoi</w> <w n="48.7">nous</w> <w n="48.8">rions</w> <w n="48.9">en</w> <w n="48.10">dedans</w>. »</l>
					</lg>
					<lg n="13">
						<l n="49" num="13.1">— <w n="49.1">Ainsi</w>, <w n="49.2">dans</w> <w n="49.3">le</w> <w n="49.4">soleil</w> <w n="49.5">et</w> <w n="49.6">le</w> <w n="49.7">sable</w>, <w n="49.8">au</w> <w n="49.9">passage</w>,</l>
						<l n="50" num="13.2"><w n="50.1">J</w>’<w n="50.2">écoutais</w> <w n="50.3">ce</w> <w n="50.4">regard</w> <w n="50.5">au</w> <w n="50.6">langage</w> <w n="50.7">muet</w></l>
						<l n="51" num="13.3"><w n="51.1">Chargé</w> <w n="51.2">de</w> <w n="51.3">patience</w> <w n="51.4">infinie</w> <w n="51.5">et</w> <w n="51.6">de</w> <w n="51.7">rage</w>,</l>
						<l n="52" num="13.4"><w n="52.1">Qui</w>, <w n="52.2">d</w>’<w n="52.3">entre</w> <w n="52.4">les</w> <w n="52.5">longs</w> <w n="52.6">cils</w> <w n="52.7">hypocrites</w>, <w n="52.8">fluait</w>.</l>
					</lg>
					<lg n="14">
						<l n="53" num="14.1"><w n="53.1">Et</w> <w n="53.2">je</w> <w n="53.3">voyais</w>, <w n="53.4">en</w> <w n="53.5">vérité</w>, <w n="53.6">tout</w> <w n="53.7">le</w> <w n="53.8">Possible</w></l>
						<l n="54" num="14.2"><w n="54.1">Qui</w> <w n="54.2">guette</w> <w n="54.3">dans</w> <w n="54.4">vos</w> <w n="54.5">yeux</w> <w n="54.6">pleins</w> <w n="54.7">de</w> <w n="54.8">honte</w> <w n="54.9">et</w> <w n="54.10">de</w> <w n="54.11">peur</w>,</l>
						<l n="55" num="14.3"><w n="55.1">Et</w> <w n="55.2">moi</w> <w n="55.3">qui</w> <w n="55.4">ne</w> <w n="55.5">suis</w> <w n="55.6">point</w>, <w n="55.7">Israël</w>, <w n="55.8">votre</w> <w n="55.9">sœur</w>,</l>
						<l n="56" num="14.4"><w n="56.1">Je</w> <w n="56.2">vous</w> <w n="56.3">ai</w> <w n="56.4">salué</w> <w n="56.5">tout</w> <w n="56.6">bas</w>, <w n="56.7">peuple</w> <w n="56.8">terrible</w> !</l>
					</lg>
				</div></body></text></TEI>