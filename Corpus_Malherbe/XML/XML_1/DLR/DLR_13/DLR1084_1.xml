<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CHOIX DE POÈMES</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1917 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">DLR_13</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Choix de poèmes</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie Alphonse Lemerre</publisher>
							<date when="1951">1951</date>
						</imprint>
					</monogr>
					<note>Édition numérisée à partir d’un ouvrage prêté par la BPU de neuchatel</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1951">1951</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La partie en prose (FRAGMENT D’UN TEXTE DE L’AUTEUR PRÉFAÇANT DEUX POÈMES D’EDGAR POE) n’est incluse.</p>
				<p>Les indications de la première édition d’un poème données dans la table des matières ont été reportées comme sous-titre des poèmes. L’année d’édition a été ajoutée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell (correction d’erreur d’océrisation).</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-01-14" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-01-14" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CHAPITRE VI</head><head type="main_part">Religion</head><div type="poem" key="DLR1084">
					<head type="number">I</head>
					<head type="main">Visitation</head>
					<head type="sub_2">SOUFFLES DE TEMPÊTE, Fasquelle, 1918</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Cathédrale</w> <w n="1.2">debout</w> <w n="1.3">sur</w> <w n="1.4">l</w>’<w n="1.5">horrible</w> <w n="1.6">présent</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Légèreté</w> <w n="2.2">de</w> <w n="2.3">pierre</w> <w n="2.4">aux</w> <w n="2.5">longues</w> <w n="2.6">avenues</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Orgue</w> <w n="3.2">à</w> <w n="3.3">mille</w> <w n="3.4">tuyaux</w> <w n="3.5">du</w> <w n="3.6">silence</w> <w n="3.7">écrasant</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Avec</w> <w n="4.2">ton</w> <w n="4.3">verre</w> <w n="4.4">en</w> <w n="4.5">feu</w> <w n="4.6">pris</w> <w n="4.7">dans</w> <w n="4.8">tes</w> <w n="4.9">pierres</w> <w n="4.10">nues</w>,</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Allant</w> <w n="5.2">au</w> <w n="5.3">rouge</w> <w n="5.4">et</w> <w n="5.5">bleu</w> <w n="5.6">de</w> <w n="5.7">tes</w> <w n="5.8">vitraux</w> <w n="5.9">foncés</w>,</l>
						<l n="6" num="2.2"><w n="6.1">Parmi</w> <w n="6.2">ton</w> <w n="6.3">ombre</w>, <w n="6.4">enfin</w>, <w n="6.5">mes</w> <w n="6.6">âmes</w> <w n="6.7">sont</w> <w n="6.8">chez</w> <w n="6.9">elles</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Telle</w> <w n="7.2">une</w> <w n="7.3">légion</w> <w n="7.4">d</w>’<w n="7.5">archanges</w> <w n="7.6">offensés</w></l>
						<l n="8" num="2.4"><w n="8.1">Qui</w> <w n="8.2">retrouvent</w> <w n="8.3">ici</w> <w n="8.4">la</w> <w n="8.5">place</w> <w n="8.6">de</w> <w n="8.7">leurs</w> <w n="8.8">ailes</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Mes</w> <w n="9.2">yeux</w> <w n="9.3">comptent</w> <w n="9.4">tes</w> <w n="9.5">rangs</w> <w n="9.6">de</w> <w n="9.7">colonnes</w> <w n="9.8">qui</w> <w n="9.9">vont</w></l>
						<l n="10" num="3.2"><w n="10.1">Une</w> <w n="10.2">à</w> <w n="10.3">une</w>, <w n="10.4">faisceaux</w> <w n="10.5">serrés</w>, <w n="10.6">paquets</w> <w n="10.7">de</w> <w n="10.8">cierges</w>,</l>
						<l n="11" num="3.3"><w n="11.1">Rejoindre</w> <w n="11.2">avec</w> <w n="11.3">l</w>’<w n="11.4">encens</w> <w n="11.5">la</w> <w n="11.6">nuit</w> <w n="11.7">de</w> <w n="11.8">ton</w> <w n="11.9">plafond</w></l>
						<l n="12" num="3.4"><w n="12.1">Où</w> <w n="12.2">flottent</w> <w n="12.3">doucement</w> <w n="12.4">les</w> <w n="12.5">saintes</w> <w n="12.6">et</w> <w n="12.7">les</w> <w n="12.8">vierges</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Grand</w> <w n="13.2">passé</w>, <w n="13.3">moyen</w> <w n="13.4">âge</w> <w n="13.5">hermétique</w> <w n="13.6">et</w> <w n="13.7">fleuri</w>,</l>
						<l n="14" num="4.2"><w n="14.1">Satanique</w>, <w n="14.2">angélique</w>, <w n="14.3">ô</w> <w n="14.4">très</w> <w n="14.5">pure</w>, <w n="14.6">ô</w> <w n="14.7">terrible</w>,</l>
						<l n="15" num="4.3"><w n="15.1">Cathédrale</w>, <w n="15.2">tu</w> <w n="15.3">n</w>’<w n="15.4">es</w> <w n="15.5">tout</w> <w n="15.6">entière</w> <w n="15.7">qu</w>’<w n="15.8">un</w> <w n="15.9">cri</w></l>
						<l n="16" num="4.4"><w n="16.1">Jeté</w> <w n="16.2">par</w> <w n="16.3">les</w> <w n="16.4">humains</w> <w n="16.5">perdus</w> <w n="16.6">vers</w> <w n="16.7">l</w>’<w n="16.8">invisible</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Cri</w> <w n="17.2">de</w> <w n="17.3">ma</w> <w n="17.4">race</w>, <w n="17.5">cri</w> <w n="17.6">de</w> <w n="17.7">mon</w> <w n="17.8">être</w> <w n="17.9">qui</w> <w n="17.10">court</w>,</l>
						<l n="18" num="5.2"><w n="18.1">Aveugle</w> <w n="18.2">et</w> <w n="18.3">les</w> <w n="18.4">bras</w> <w n="18.5">fous</w>, <w n="18.6">vers</w> <w n="18.7">le</w> <w n="18.8">ciel</w> <w n="18.9">ou</w> <w n="18.10">l</w>’<w n="18.11">abîme</w>,</l>
						<l n="19" num="5.3"><w n="19.1">Je</w> <w n="19.2">meurs</w> <w n="19.3">de</w> <w n="19.4">t</w>’<w n="19.5">adorer</w>, <w n="19.6">moi</w> <w n="19.7">perdue</w>, <w n="19.8">ô</w> <w n="19.9">sublime</w>,</l>
						<l n="20" num="5.4"><w n="20.1">O</w> <w n="20.2">Exaltation</w>, <w n="20.3">amour</w>, <w n="20.4">amour</w>, <w n="20.5">amour</w> !…</l>
					</lg>
				</div></body></text></TEI>