<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CHOIX DE POÈMES</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1917 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">DLR_13</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Choix de poèmes</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie Alphonse Lemerre</publisher>
							<date when="1951">1951</date>
						</imprint>
					</monogr>
					<note>Édition numérisée à partir d’un ouvrage prêté par la BPU de neuchatel</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1951">1951</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La partie en prose (FRAGMENT D’UN TEXTE DE L’AUTEUR PRÉFAÇANT DEUX POÈMES D’EDGAR POE) n’est incluse.</p>
				<p>Les indications de la première édition d’un poème données dans la table des matières ont été reportées comme sous-titre des poèmes. L’année d’édition a été ajoutée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell (correction d’erreur d’océrisation).</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-01-14" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-01-14" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">TRADUCTIONS <lb></lb>en vers français <lb></lb>par <lb></lb>LUCIE DELARUE-MARDRUS</head><head type="main_subpart">Poèmes de Anna Wickham</head><head type="sub_subpart">traduits en vers libres et réguliers</head><div type="poem" key="DLR1127">
						<head type="main">Celui qui revint</head>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">Dix</w> <w n="1.2">ans</w> <w n="1.3">j</w>’<w n="1.4">attendis</w> <w n="1.5">dans</w> <w n="1.6">le</w> <w n="1.7">coin</w></l>
							<l n="2" num="1.2"><w n="2.1">Qui</w> <w n="2.2">fut</w> <w n="2.3">ma</w> <w n="2.4">maison</w> <w n="2.5">minuscule</w>,</l>
							<l n="3" num="1.3"><w n="3.1">Guettant</w> <w n="3.2">de</w> <w n="3.3">l</w>’<w n="3.4">aube</w> <w n="3.5">au</w> <w n="3.6">crépuscule</w></l>
							<l n="4" num="1.4"><w n="4.1">S</w>’<w n="4.2">il</w> <w n="4.3">n</w>’<w n="4.4">allait</w> <w n="4.5">pas</w> <w n="4.6">venir</w> <w n="4.7">au</w> <w n="4.8">loin</w>.</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1"><w n="5.1">Il</w> <w n="5.2">vint</w> ! <w n="5.3">Mais</w>, <w n="5.4">dix</w> <w n="5.5">ans</w>, <w n="5.6">par</w> <w n="5.7">la</w> <w n="5.8">suite</w>,</l>
							<l n="6" num="2.2"><w n="6.1">O</w> <w n="6.2">malédiction</w> <w n="6.3">du</w> <w n="6.4">sort</w> !</l>
							<l n="7" num="2.3"><w n="7.1">A</w> <w n="7.2">table</w>, <w n="7.3">au</w> <w n="7.4">lit</w>, <w n="7.5">toujours</w> <w n="7.6">au</w> <w n="7.7">gîte</w>,</l>
							<l n="8" num="2.4"><w n="8.1">Celui</w> <w n="8.2">que</w> <w n="8.3">j</w>’<w n="8.4">aimais</w> <w n="8.5">était</w> <w n="8.6">mort</w>.</l>
						</lg>
						<lg n="3">
							<l n="9" num="3.1"><w n="9.1">Dans</w> <w n="9.2">la</w> <w n="9.3">montagne</w>, <w n="9.4">un</w> <w n="9.5">soir</w> <w n="9.6">d</w>’<w n="9.7">orage</w>,</l>
							<l n="10" num="3.2"><w n="10.1">Il</w> <w n="10.2">tomba</w>. <w n="10.3">Rapporté</w> <w n="10.4">chez</w> <w n="10.5">nous</w>,</l>
							<l n="11" num="3.3"><w n="11.1">Sans</w> <w n="11.2">chagrin</w> <w n="11.3">je</w> <w n="11.4">fus</w> <w n="11.5">à</w> <w n="11.6">genoux</w>.</l>
							<l n="12" num="3.4"><w n="12.1">Puis</w> <w n="12.2">je</w> <w n="12.3">regardai</w> <w n="12.4">son</w> <w n="12.5">visage</w>.</l>
						</lg>
						<lg n="4">
							<l n="13" num="4.1"><w n="13.1">Alors</w> <w n="13.2">mes</w> <w n="13.3">bras</w> <w n="13.4">passionnés</w></l>
							<l n="14" num="4.2"><w n="14.1">Serrèrent</w> <w n="14.2">ce</w> <w n="14.3">corps</w> <w n="14.4">sans</w> <w n="14.5">rien</w> <w n="14.6">dire</w>,</l>
							<l n="15" num="4.3"><w n="15.1">Car</w> <w n="15.2">sa</w> <w n="15.3">mort</w> <w n="15.4">avait</w> <w n="15.5">le</w> <w n="15.6">sourire</w></l>
							<l n="16" num="4.4"><w n="16.1">De</w> <w n="16.2">celui</w> <w n="16.3">que</w> <w n="16.4">j</w>’<w n="16.5">avais</w> <w n="16.6">aimé</w> !</l>
						</lg>
						<lg n="5">
							<l n="17" num="5.1"><w n="17.1">O</w> <w n="17.2">vous</w>, <w n="17.3">veuves</w> ! <w n="17.4">Prêtez</w>-<w n="17.5">moi</w>, <w n="17.6">dites</w>,</l>
							<l n="18" num="5.2"><w n="18.1">Vos</w> <w n="18.2">pleurs</w> <w n="18.3">pour</w> <w n="18.4">pleurer</w> <w n="18.5">mon</w> <w n="18.6">ami</w> :</l>
							<l n="19" num="5.3"><w n="19.1">Le</w> <w n="19.2">revoici</w>, <w n="19.3">lui</w> <w n="19.4">qui</w> <w n="19.5">dormit</w></l>
							<l n="20" num="5.4"><w n="20.1">Dix</w> <w n="20.2">ans</w> <w n="20.3">d</w>’<w n="20.4">existence</w> <w n="20.5">maudite</w>.</l>
						</lg>
					</div></body></text></TEI>