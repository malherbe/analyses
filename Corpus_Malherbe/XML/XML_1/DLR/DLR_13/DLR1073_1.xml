<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CHOIX DE POÈMES</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1917 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">DLR_13</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Choix de poèmes</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie Alphonse Lemerre</publisher>
							<date when="1951">1951</date>
						</imprint>
					</monogr>
					<note>Édition numérisée à partir d’un ouvrage prêté par la BPU de neuchatel</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1951">1951</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La partie en prose (FRAGMENT D’UN TEXTE DE L’AUTEUR PRÉFAÇANT DEUX POÈMES D’EDGAR POE) n’est incluse.</p>
				<p>Les indications de la première édition d’un poème données dans la table des matières ont été reportées comme sous-titre des poèmes. L’année d’édition a été ajoutée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell (correction d’erreur d’océrisation).</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-01-14" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-01-14" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CHAPITRE V</head><head type="main_part">Intimité</head><div type="poem" key="DLR1073">
					<head type="main">Ballade du Feu</head>
					<head type="sub_2">TEMPS PRÉSENTS, Paul Mourousy, 1939</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">La</w> <w n="1.2">cheminée</w> <w n="1.3">est</w> <w n="1.4">un</w> <w n="1.5">théâtre</w></l>
						<l n="2" num="1.2"><w n="2.1">Où</w> <w n="2.2">l</w>’<w n="2.3">on</w> <w n="2.4">voit</w> <w n="2.5">le</w> <w n="2.6">drame</w> <w n="2.7">du</w> <w n="2.8">feu</w>.</l>
						<l n="3" num="1.3"><w n="3.1">La</w> <w n="3.2">nuit</w>, <w n="3.3">assise</w> <w n="3.4">au</w> <w n="3.5">coin</w> <w n="3.6">de</w> <w n="3.7">l</w>’<w n="3.8">âtre</w>,</l>
						<l n="4" num="1.4"><w n="4.1">J</w>’<w n="4.2">assiste</w>, <w n="4.3">pensive</w>, <w n="4.4">à</w> <w n="4.5">ce</w> <w n="4.6">jeu</w>.</l>
						<l n="5" num="1.5"><w n="5.1">C</w>’<w n="5.2">est</w> <w n="5.3">tout</w> <w n="5.4">un</w> <w n="5.5">enfer</w> <w n="5.6">qui</w> <w n="5.7">se</w> <w n="5.8">meut</w>,</l>
						<l n="6" num="1.6"><w n="6.1">C</w>’<w n="6.2">est</w> <w n="6.3">tout</w> <w n="6.4">un</w> <w n="6.5">orage</w> <w n="6.6">qui</w> <w n="6.7">tonne</w>,</l>
						<l n="7" num="1.7"><w n="7.1">Et</w> <w n="7.2">voici</w>, <w n="7.3">brûlant</w> <w n="7.4">camaïeu</w>,</l>
						<l n="8" num="1.8"><w n="8.1">Les</w> <w n="8.2">grandes</w> <w n="8.3">couleurs</w> <w n="8.4">de</w> <w n="8.5">l</w>’<w n="8.6">automne</w>.</l>
					</lg>
					<lg n="2">
						<l n="9" num="2.1"><w n="9.1">Par</w> <w n="9.2">ici</w> <w n="9.3">le</w> <w n="9.4">ballet</w> <w n="9.5">folâtre</w></l>
						<l n="10" num="2.2"><w n="10.1">De</w> <w n="10.2">plus</w> <w n="10.3">d</w>’<w n="10.4">un</w> <w n="10.5">petit</w> <w n="10.6">esprit</w> <w n="10.7">bleu</w>,</l>
						<l n="11" num="2.3"><w n="11.1">Par</w> <w n="11.2">li</w>, <w n="11.3">le</w> <w n="11.4">bois</w> <w n="11.5">opiniâtre</w></l>
						<l n="12" num="2.4"><w n="12.1">Qui</w> <w n="12.2">se</w> <w n="12.3">fend</w> <w n="12.4">soudain</w> <w n="12.5">au</w> <w n="12.6">milieu</w>.</l>
						<l n="13" num="2.5"><w n="13.1">Un</w> <w n="13.2">follet</w> <w n="13.3">siffle</w> <w n="13.4">tant</w> <w n="13.5">qu</w>’<w n="13.6">il</w> <w n="13.7">peut</w> !</l>
						<l n="14" num="2.6"><w n="14.1">D</w>’<w n="14.2">un</w> <w n="14.3">fil</w> <w n="14.4">d</w>’<w n="14.5">or</w> <w n="14.6">plus</w> <w n="14.7">fin</w> <w n="14.8">que</w> <w n="14.9">cheveu</w></l>
						<l n="15" num="2.7"><w n="15.1">La</w> <w n="15.2">bûche</w>, <w n="15.3">tout</w> <w n="15.4">doux</w>, <w n="15.5">se</w> <w n="15.6">festonne</w>.</l>
						<l n="16" num="2.8"><w n="16.1">Au</w> <w n="16.2">centre</w>, <w n="16.3">s</w>’<w n="16.4">embrouille</w> <w n="16.5">le</w> <w n="16.6">nœud</w></l>
						<l n="17" num="2.9"><w n="17.1">Des</w> <w n="17.2">grandes</w> <w n="17.3">couleurs</w> <w n="17.4">de</w> <w n="17.5">l</w>’<w n="17.6">automne</w>.</l>
					</lg>
					<lg n="3">
						<l n="18" num="3.1"><w n="18.1">Silencieuse</w> <w n="18.2">comme</w> <w n="18.3">un</w> <w n="18.4">pâtre</w>,</l>
						<l n="19" num="3.2"><w n="19.1">Toute</w> <w n="19.2">seule</w> <w n="19.3">et</w> <w n="19.4">pareille</w>, <w n="19.5">un</w> <w n="19.6">peu</w>,</l>
						<l n="20" num="3.3"><w n="20.1">A</w> <w n="20.2">quelque</w> <w n="20.3">croyant</w> <w n="20.4">idolâtre</w>,</l>
						<l n="21" num="3.4"><w n="21.1">Moi</w>, <w n="21.2">je</w> <w n="21.3">déchiffre</w> <w n="21.4">cet</w> <w n="21.5">hébreu</w>.</l>
						<l n="22" num="3.5"><w n="22.1">Dehors</w>, <w n="22.2">la</w> <w n="22.3">lune</w> ; <w n="22.4">ou</w> <w n="22.5">bien</w> <w n="22.6">il</w> <w n="22.7">pleut</w>.</l>
						<l n="23" num="3.6"><w n="23.1">Mon</w> <w n="23.2">rêve</w> <w n="23.3">doucement</w> <w n="23.4">mitonne</w></l>
						<l n="24" num="3.7"><w n="24.1">Et</w> <w n="24.2">revoit</w>, <w n="24.3">oubliant</w> <w n="24.4">le</w> <w n="24.5">lieu</w>,</l>
						<l n="25" num="3.8"><w n="25.1">Les</w> <w n="25.2">grandes</w> <w n="25.3">couleurs</w> <w n="25.4">de</w> <w n="25.5">l</w>’<w n="25.6">automne</w>.</l>
					</lg>
					<lg n="4">
						<head type="form">ENVOI</head>
						<l n="26" num="4.1"><w n="26.1">Va</w> ! <w n="26.2">Si</w> <w n="26.3">mon</w> <w n="26.4">cœur</w> <w n="26.5">brûle</w> <w n="26.6">et</w> <w n="26.7">s</w>’<w n="26.8">émeut</w></l>
						<l n="27" num="4.2"><w n="27.1">Comme</w> <w n="27.2">toi</w>, <w n="27.3">mon</w> <w n="27.4">feu</w> <w n="27.5">monotone</w>,</l>
						<l n="28" num="4.3"><w n="28.1">C</w>’<w n="28.2">est</w> <w n="28.3">qu</w>’<w n="28.4">il</w> <w n="28.5">porte</w> <w n="28.6">aussi</w>, <w n="28.7">plein</w> <w n="28.8">d</w>’<w n="28.9">adieu</w>,</l>
						<l n="29" num="4.4"><w n="29.1">Les</w> <w n="29.2">grandes</w> <w n="29.3">couleurs</w> <w n="29.4">de</w> <w n="29.5">l</w>’<w n="29.6">automne</w>.</l>
					</lg>
				</div></body></text></TEI>