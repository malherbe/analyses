<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CHOIX DE POÈMES</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1917 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">DLR_13</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Choix de poèmes</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie Alphonse Lemerre</publisher>
							<date when="1951">1951</date>
						</imprint>
					</monogr>
					<note>Édition numérisée à partir d’un ouvrage prêté par la BPU de neuchatel</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1951">1951</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La partie en prose (FRAGMENT D’UN TEXTE DE L’AUTEUR PRÉFAÇANT DEUX POÈMES D’EDGAR POE) n’est incluse.</p>
				<p>Les indications de la première édition d’un poème données dans la table des matières ont été reportées comme sous-titre des poèmes. L’année d’édition a été ajoutée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell (correction d’erreur d’océrisation).</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-01-14" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-01-14" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CHAPITRE PREMIER</head><head type="main_part">L’Estuaire</head><head type="sub_part">SOUFFLES DE TEMPÊTE, Fasquelle, 1918</head><div type="poem" key="DLR1044">
					<head type="main">L’Estuaire</head>
					<head type="sub_2">SOUFFLES DE TEMPÊTE, Fasquelle, 1918</head>
					<lg n="1">
						<l n="1" num="1.1">,<w n="1.1">J</w>’<w n="1.2">aime</w> <w n="1.3">toujours</w> <w n="1.4">revoir</w> <w n="1.5">l</w>’<w n="1.6">estuaire</w>, <w n="1.7">ses</w> <w n="1.8">eaux</w></l>
						<l n="2" num="1.2">,<w n="2.1">Hybrides</w>, <w n="2.2">où</w> <w n="2.3">la</w> <w n="2.4">mer</w> <w n="2.5">au</w> <w n="2.6">fleuve</w> <w n="2.7">se</w> <w n="2.8">mélange</w>.</l>
						<l n="3" num="1.3">,<w n="3.1">C</w>’<w n="3.2">est</w> <w n="3.3">là</w> <w n="3.4">que</w> <w n="3.5">j</w>’<w n="3.6">ai</w> <w n="3.7">senti</w> <w n="3.8">naître</w> <w n="3.9">et</w> <w n="3.10">grandir</w> <w n="3.11">cet</w> <w n="3.12">ange</w></l>
						<l n="4" num="1.4">,<w n="4.1">Qui</w>, <w n="4.2">jusques</w> <w n="4.3">à</w> <w n="4.4">ma</w> <w n="4.5">mort</w>, <w n="4.6">tourmentera</w> <w n="4.7">mes</w> <w n="4.8">os</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">,<w n="5.1">Je</w> <w n="5.2">porte</w> <w n="5.3">au</w> <w n="5.4">fond</w> <w n="5.5">de</w> <w n="5.6">moi</w> <w n="5.7">l</w>’<w n="5.8">estuaire</w> <w n="5.9">complexe</w>,</l>
						<l n="6" num="2.2">,<w n="6.1">Son</w> <w n="6.2">eau</w> <w n="6.3">douce</w> <w n="6.4">mêlée</w> <w n="6.5">à</w> <w n="6.6">tant</w> <w n="6.7">de</w> <w n="6.8">sel</w> <w n="6.9">amer</w>.</l>
						<l n="7" num="2.3">,<w n="7.1">Quelque</w> <w n="7.2">chose</w>, <w n="7.3">en</w> <w n="7.4">mon</w> <w n="7.5">âme</w> <w n="7.6">à</w> <w n="7.7">tout</w> <w n="7.8">jamais</w> <w n="7.9">perplexe</w>,</l>
						<l n="8" num="2.4"><w n="8.1">A</w> <w n="8.2">fini</w> <w n="8.3">d</w>’<w n="8.4">être</w> <w n="8.5">fleuve</w> <w n="8.6">et</w> <w n="8.7">n</w>’<w n="8.8">est</w> <w n="8.9">pas</w> <choice reason="analysis" type="false_verse" hand="RR"><sic>encore</sic><corr source="edition_1918"><w n="8.10">encor</w></corr></choice> <w n="8.11">mer</w>.</l>
					</lg>
				</div></body></text></TEI>