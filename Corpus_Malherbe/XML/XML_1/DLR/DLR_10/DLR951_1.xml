<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">MORT ET PRINTEMPS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2448 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_10</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruemortetprintemps.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>MORT ET PRINTEMPS</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Albert Messein</publisher>
							<date when="1932">1932</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques.</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1932">1932</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les titres des poèmes ont été corrigés en fonction de l’édition de référence.</p>
				<p>Les parties manquantes des poèmes "À SAINT GERMAIN" et "À LA NUIT" ont été ajoutées</p>
				<p>Des corrections métriques qui s’imposent ont été faites (erreurs d’édition).</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les retraits introduits automatiquement ont été modifiés conformément à l’édition de référence.</p>
				<p>Des corrections métriques ont été faites ; introduction de licences poétiques ; certe, encor...</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-09-02" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-09-02" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLR951">
				<head type="main">VOYAGES</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Mon</w> <w n="1.2">premier</w> <w n="1.3">âge</w> <w n="1.4">a</w> <w n="1.5">voyagé</w></l>
					<l n="2" num="1.2"><w n="2.1">Vers</w> <w n="2.2">les</w> <w n="2.3">îles</w> <w n="2.4">imaginaires</w></l>
					<l n="3" num="1.3"><w n="3.1">Qui</w> <w n="3.2">sentaient</w> <w n="3.3">si</w> <w n="3.4">bon</w> <w n="3.5">l</w>’<w n="3.6">oranger</w></l>
					<l n="4" num="1.4"><w n="4.1">Au</w> <w n="4.2">bout</w> <w n="4.3">des</w> <w n="4.4">vagues</w> <w n="4.5">ordinaires</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Vagues</w> <w n="5.2">de</w> <w n="5.3">mon</w> <w n="5.4">vieux</w> <w n="5.5">petit</w> <w n="5.6">port</w></l>
					<l n="6" num="2.2"><w n="6.1">Roulé</w> <w n="6.2">dans</w> <w n="6.3">son</w> <w n="6.4">odeur</w> <w n="6.5">de</w> <w n="6.6">caque</w></l>
					<l n="7" num="2.3"><w n="7.1">Sur</w> <w n="7.2">l</w>’<w n="7.3">estuaire</w>, <w n="7.4">grande</w> <w n="7.5">flaque</w></l>
					<l n="8" num="2.4"><w n="8.1">Que</w> <w n="8.2">touche</w> <w n="8.3">le</w> <w n="8.4">ciel</w> <w n="8.5">bas</w> <w n="8.6">et</w> <w n="8.7">saur</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Plus</w> <w n="9.2">tard</w>, <w n="9.3">certes</w>, <w n="9.4">je</w> <w n="9.5">suis</w> <w n="9.6">allée</w></l>
					<l n="10" num="3.2"><w n="10.1">Voir</w> <w n="10.2">de</w> <w n="10.3">près</w> <w n="10.4">mes</w> <w n="10.5">rêves</w> <w n="10.6">premiers</w></l>
					<l n="11" num="3.3"><w n="11.1">Bien</w> <w n="11.2">loin</w> <w n="11.3">de</w> <w n="11.4">ma</w> <w n="11.5">ville</w> <w n="11.6">salée</w>,</l>
					<l n="12" num="3.4"><w n="12.1">De</w> <w n="12.2">mes</w> <w n="12.3">prés</w> <w n="12.4">et</w> <w n="12.5">de</w> <w n="12.6">mes</w> <w n="12.7">pommiers</w>,</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">J</w>’<w n="13.2">ai</w> <w n="13.3">bourlingué</w> <w n="13.4">dans</w> <w n="13.5">des</w> <w n="13.6">voyages</w>,</l>
					<l n="14" num="4.2"><w n="14.1">Bourlingué</w> <w n="14.2">dans</w> <w n="14.3">la</w> <w n="14.4">vie</w> <w n="14.5">aussi</w>.</l>
					<l n="15" num="4.3"><w n="15.1">Maintenant</w> <w n="15.2">je</w> <w n="15.3">retrouve</w> <w n="15.4">ici</w></l>
					<l n="16" num="4.4"><w n="16.1">Mon</w> <w n="16.2">enfance</w> <w n="16.3">et</w> <w n="16.4">ses</w> <w n="16.5">paysages</w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">Et</w>, <w n="17.2">quand</w> <w n="17.3">je</w> <w n="17.4">regarde</w>, <w n="17.5">le</w> <w n="17.6">soir</w>,</l>
					<l n="18" num="5.2"><w n="18.1">Les</w> <w n="18.2">longs</w> <w n="18.3">couchants</w> <w n="18.4">de</w> <w n="18.5">l</w>’<w n="18.6">estuaire</w></l>
					<l n="19" num="5.3"><w n="19.1">Recréer</w> <w n="19.2">tout</w> <w n="19.3">l</w>’<w n="19.4">imaginaire</w></l>
					<l n="20" num="5.4"><w n="20.1">Que</w> <w n="20.2">je</w> <w n="20.3">voulais</w> <w n="20.4">toucher</w> <w n="20.5">et</w> <w n="20.6">voir</w>,</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">Je</w> <w n="21.2">sais</w> <w n="21.3">que</w> <w n="21.4">la</w> <w n="21.5">terre</w> <w n="21.6">rêvée</w></l>
					<l n="22" num="6.2"><w n="22.1">Est</w> <w n="22.2">là</w> <w n="22.3">dans</w> <w n="22.4">le</w> <w n="22.5">soir</w> <w n="22.6">émouvant</w></l>
					<l n="23" num="6.3"><w n="23.1">Et</w> <w n="23.2">que</w> <w n="23.3">je</w> <w n="23.4">suis</w>, <w n="23.5">dorénavant</w>,</l>
					<l n="24" num="6.4"><w n="24.1">Mieux</w> <w n="24.2">que</w> <w n="24.3">revenue</w> : <w n="24.4">arrivée</w>.</l>
				</lg>
			</div></body></text></TEI>