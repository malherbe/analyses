<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">MORT ET PRINTEMPS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2448 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_10</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruemortetprintemps.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>MORT ET PRINTEMPS</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Albert Messein</publisher>
							<date when="1932">1932</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques.</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1932">1932</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les titres des poèmes ont été corrigés en fonction de l’édition de référence.</p>
				<p>Les parties manquantes des poèmes "À SAINT GERMAIN" et "À LA NUIT" ont été ajoutées</p>
				<p>Des corrections métriques qui s’imposent ont été faites (erreurs d’édition).</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les retraits introduits automatiquement ont été modifiés conformément à l’édition de référence.</p>
				<p>Des corrections métriques ont été faites ; introduction de licences poétiques ; certe, encor...</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-09-02" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-09-02" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLR917">
				<head type="main">LA CHIMÈRE</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Être</w> <w n="1.2">seule</w> <w n="1.3">avec</w> <w n="1.4">quelques</w> <w n="1.5">âmes</w></l>
					<l n="2" num="1.2"><w n="2.1">D</w>’<w n="2.2">amis</w> <w n="2.3">que</w> <w n="2.4">j</w>’<w n="2.5">aime</w> <w n="2.6">à</w> <w n="2.7">l</w>’<w n="2.8">horizon</w></l>
					<l n="3" num="1.3"><w n="3.1">Mais</w> <w n="3.2">personne</w> <w n="3.3">dans</w> <w n="3.4">la</w> <w n="3.5">maison</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Seule</w> <w n="4.2">avec</w> <w n="4.3">le</w> <w n="4.4">bruit</w> <w n="4.5">de</w> <w n="4.6">mes</w> <w n="4.7">femmes</w></l>
					<l n="5" num="1.5"><w n="5.1">Allant</w> <w n="5.2">et</w> <w n="5.3">venant</w> <w n="5.4">quelque</w> <w n="5.5">part</w>,</l>
					<l n="6" num="1.6"><w n="6.1">Présence</w> <w n="6.2">humaine</w>, <w n="6.3">voix</w> <w n="6.4">vivantes</w></l>
					<l n="7" num="1.7"><w n="7.1">Et</w> <w n="7.2">que</w> <w n="7.3">j</w>’<w n="7.4">entends</w>, <w n="7.5">douces</w> <w n="7.6">servantes</w></l>
					<l n="8" num="1.8"><w n="8.1">Qui</w> <w n="8.2">dorment</w> <w n="8.3">dès</w> <w n="8.4">qu</w>’<w n="8.5">il</w> <w n="8.6">se</w> <w n="8.7">fait</w> <w n="8.8">tard</w>,</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1"><w n="9.1">Me</w> <w n="9.2">sentir</w> <w n="9.3">très</w> <w n="9.4">grave</w> <w n="9.5">ou</w> <w n="9.6">très</w> <w n="9.7">gaie</w>,</l>
					<l n="10" num="2.2"><w n="10.1">Avoir</w> <w n="10.2">cent</w> <w n="10.3">ans</w> <w n="10.4">ou</w> <w n="10.5">bien</w> <w n="10.6">dix</w> <w n="10.7">ans</w>,</l>
					<l n="11" num="2.3"><w n="11.1">Et</w>, <w n="11.2">quelque</w> <w n="11.3">caprice</w> <w n="11.4">que</w> <w n="11.5">j</w>’<w n="11.6">aie</w>,</l>
					<l n="12" num="2.4"><w n="12.1">Rêves</w> <w n="12.2">funèbres</w> <w n="12.3">ou</w> <w n="12.4">plaisants</w>,</l>
					<l n="13" num="2.5"><w n="13.1">Peinture</w>, <w n="13.2">musique</w>, <w n="13.3">écriture</w>,</l>
					<l n="14" num="2.6"><w n="14.1">Mon</w> <w n="14.2">chien</w>, <w n="14.3">mon</w> <w n="14.4">chat</w> <w n="14.5">ou</w> <w n="14.6">la</w> <w n="14.7">nature</w>,</l>
					<l n="15" num="2.7"><w n="15.1">N</w>’<w n="15.2">avoir</w> <w n="15.3">pas</w> <w n="15.4">à</w> <w n="15.5">m</w>’<w n="15.6">en</w> <w n="15.7">expliquer</w>,</l>
				</lg>
				<lg n="3">
					<l n="16" num="3.1"><w n="16.1">Ce</w> <w n="16.2">bonheur</w> <w n="16.3">si</w> <w n="16.4">peu</w> <w n="16.5">compliqué</w></l>
					<l n="17" num="3.2"><w n="17.1">Que</w> <w n="17.2">j</w>’<w n="17.3">ai</w> <w n="17.4">connu</w> <w n="17.5">jusqu</w>’<w n="17.6">à</w> <w n="17.7">l</w>’<w n="17.8">ivresse</w>,</l>
					<l n="18" num="3.3"><w n="18.1">Il</w> <w n="18.2">est</w> (<w n="18.3">interrompu</w> <w n="18.4">sans</w> <w n="18.5">cesse</w>),</l>
					<l n="19" num="3.4"><w n="19.1">La</w> <w n="19.2">chimère</w> <w n="19.3">qu</w>’<w n="19.4">on</w> <w n="19.5">a</w> <w n="19.6">tout</w> <w n="19.7">bas</w></l>
					<l n="20" num="3.5"><w n="20.1">Et</w> <w n="20.2">qu</w>’<w n="20.3">on</w> <w n="20.4">ne</w> <w n="20.5">réalise</w> <w n="20.6">pas</w>.</l>
				</lg>
			</div></body></text></TEI>