<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">MORT ET PRINTEMPS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2448 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_10</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruemortetprintemps.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>MORT ET PRINTEMPS</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Albert Messein</publisher>
							<date when="1932">1932</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques.</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1932">1932</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les titres des poèmes ont été corrigés en fonction de l’édition de référence.</p>
				<p>Les parties manquantes des poèmes "À SAINT GERMAIN" et "À LA NUIT" ont été ajoutées</p>
				<p>Des corrections métriques qui s’imposent ont été faites (erreurs d’édition).</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les retraits introduits automatiquement ont été modifiés conformément à l’édition de référence.</p>
				<p>Des corrections métriques ont été faites ; introduction de licences poétiques ; certe, encor...</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-09-02" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-09-02" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">JACQUELINE</head><div type="poem" key="DLR965">
					<head type="number">XIII</head>
					<lg n="1">
						<l n="1" num="1.1"><space unit="char" quantity="10"></space><w n="1.1">Tu</w> <w n="1.2">n</w>’<w n="1.3">es</w> <w n="1.4">pas</w> <w n="1.5">encore</w> <w n="1.6">ai</w> <w n="1.7">passé</w>.</l>
						<l n="2" num="1.2"><w n="2.1">Ta</w> <w n="2.2">présence</w> <w n="2.3">est</w> <w n="2.4">toujours</w> <w n="2.5">près</w> <w n="2.6">de</w> <w n="2.7">nous</w>, <w n="2.8">bien</w> <w n="2.9">vivante</w>,</l>
						<l n="3" num="1.3"><space unit="char" quantity="10"></space><w n="3.1">Et</w>, <w n="3.2">comme</w> <w n="3.3">un</w> <w n="3.4">trésor</w> <w n="3.5">amassé</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Nous</w> <w n="4.2">t</w>’<w n="4.3">avons</w> <w n="4.4">là</w>, <w n="4.5">debout</w> <w n="4.6">dans</w> <w n="4.7">ta</w> <w n="4.8">force</w> <w n="4.9">émouvante</w> ?</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><space unit="char" quantity="10"></space><w n="5.1">Mais</w> <w n="5.2">l</w>’<w n="5.3">année</w> <w n="5.4">avance</w> <w n="5.5">sans</w> <w n="5.6">toi</w>.</l>
						<l n="6" num="2.2"><w n="6.1">Quand</w> <w n="6.2">elle</w> <w n="6.3">finira</w>, <w n="6.4">ce</w> <w n="6.5">sera</w> <w n="6.6">la</w> <w n="6.7">dernière</w></l>
						<l n="7" num="2.3"><space unit="char" quantity="10"></space><w n="7.1">Où</w> <w n="7.2">tu</w> <w n="7.3">vécus</w>… <w n="7.4">Dans</w> <w n="7.5">quelle</w> <w n="7.6">ornière</w></l>
						<l n="8" num="2.4"><w n="8.1">Es</w>-<w n="8.2">tu</w> <w n="8.3">couchée</w>, <w n="8.4">au</w> <w n="8.5">fond</w> <w n="8.6">du</w> <w n="8.7">grand</w> <w n="8.8">silence</w> <w n="8.9">froid</w> ?</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><space unit="char" quantity="10"></space><w n="9.1">L</w>’<w n="9.2">an</w> <w n="9.3">prochain</w>, <w n="9.4">reviendra</w> <w n="9.5">la</w> <w n="9.6">date</w></l>
						<l n="10" num="3.2"><w n="10.1">De</w> <w n="10.2">ta</w> <w n="10.3">mort</w>. <w n="10.4">ce</w> <w n="10.5">sera</w> <w n="10.6">déjà</w> <w n="10.7">bien</w> <w n="10.8">plus</w> <w n="10.9">lointain</w>.</l>
						<l n="11" num="3.3"><space unit="char" quantity="10"></space><w n="11.1">Et</w>, <w n="11.2">puisqu</w>’<w n="11.3">il</w> <w n="11.4">faut</w> <w n="11.5">qu</w>’<w n="11.6">on</w> <w n="11.7">s</w>’<w n="11.8">acclimate</w>,</l>
						<l n="12" num="3.4"><w n="12.1">Plus</w> <w n="12.2">de</w> <w n="12.3">surprise</w> <w n="12.4">au</w> <w n="12.5">fond</w> <w n="12.6">de</w> <w n="12.7">notre</w> <w n="12.8">cœur</w> <w n="12.9">atteint</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><space unit="char" quantity="10"></space><w n="13.1">Le</w> <w n="13.2">temps</w> <w n="13.3">passera</w>. <w n="13.4">L</w>’<w n="13.5">habitude</w></l>
						<l n="14" num="4.2"><w n="14.1">Est</w> <w n="14.2">un</w> <w n="14.3">miracle</w> <w n="14.4">lent</w> <w n="14.5">qui</w> <w n="14.6">réussit</w> <w n="14.7">toujours</w>.</l>
						<l n="15" num="4.3"><space unit="char" quantity="10"></space><w n="15.1">Par</w> <w n="15.2">quelque</w> <w n="15.3">soir</w> <w n="15.4">de</w> <w n="15.5">lassitude</w></l>
						<l n="16" num="4.4"><w n="16.1">Nous</w> <w n="16.2">dirons</w> : « <w n="16.3">Jacqueline</w> <w n="16.4">et</w> <w n="16.5">les</w> <w n="16.6">anciens</w> <w n="16.7">beaux</w> <w n="16.8">jours</w> »</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><space unit="char" quantity="10"></space><w n="17.1">Puis</w> <w n="17.2">cela</w> <w n="17.3">deviendra</w> : <hi rend="ital"><w n="17.4">naguère</w></hi>.</l>
						<l n="18" num="5.2"><w n="18.1">Nous</w> <w n="18.2">ne</w> <w n="18.3">saurons</w> <w n="18.4">plus</w> <w n="18.5">bien</w> <w n="18.6">la</w> <w n="18.7">date</w>. <w n="18.8">C</w>’<w n="18.9">est</w> <w n="18.10">ainsi</w>.</l>
						<l n="19" num="5.3"><space unit="char" quantity="10"></space><w n="19.1">Et</w> <w n="19.2">puis</w> <w n="19.3">plus</w> <w n="19.4">rien</w>. <w n="19.5">Car</w>, <w n="19.6">nous</w> <w n="19.7">aussi</w>,</l>
						<l n="20" num="5.4"><w n="20.1">Nous</w> <w n="20.2">serons</w> <w n="20.3">allongés</w> <w n="20.4">dans</w> <w n="20.5">le</w> <w n="20.6">froid</w> <w n="20.7">de</w> <w n="20.8">l</w>’<w n="20.9">ornière</w>.</l>
					</lg>
				</div></body></text></TEI>