<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">MORT ET PRINTEMPS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2448 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_10</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruemortetprintemps.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>MORT ET PRINTEMPS</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Albert Messein</publisher>
							<date when="1932">1932</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques.</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1932">1932</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les titres des poèmes ont été corrigés en fonction de l’édition de référence.</p>
				<p>Les parties manquantes des poèmes "À SAINT GERMAIN" et "À LA NUIT" ont été ajoutées</p>
				<p>Des corrections métriques qui s’imposent ont été faites (erreurs d’édition).</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les retraits introduits automatiquement ont été modifiés conformément à l’édition de référence.</p>
				<p>Des corrections métriques ont été faites ; introduction de licences poétiques ; certe, encor...</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-09-02" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-09-02" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLR935">
				<head type="main">LA CHIMÈRE</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">La</w> <w n="1.2">chimère</w> <w n="1.3">est</w> <w n="1.4">au</w> <w n="1.5">fond</w> <w n="1.6">de</w> <w n="1.7">l</w>’<w n="1.8">homme</w></l>
					<l n="2" num="1.2"><w n="2.1">Dès</w> <w n="2.2">le</w> <w n="2.3">moment</w> <w n="2.4">qu</w>’<w n="2.5">on</w> <w n="2.6">l</w>’<w n="2.7">a</w> <w n="2.8">vêtu</w>.</l>
					<l n="3" num="1.3"><w n="3.1">Le</w> <w n="3.2">petit</w> <w n="3.3">humain</w> <w n="3.4">rose</w> <w n="3.5">et</w> <w n="3.6">nu</w></l>
					<l n="4" num="1.4"><w n="4.1">Naquit</w> <w n="4.2">animal</w>, <w n="4.3">ou</w> <w n="4.4">tout</w> <w n="4.5">comme</w>.</l>
					<l n="5" num="1.5"><w n="5.1">Son</w> <w n="5.2">premier</w> <w n="5.3">maillot</w>, <w n="5.4">c</w>’<w n="5.5">est</w> : <w n="5.6">la</w> <w n="5.7">loi</w>,</l>
					<l n="6" num="1.6"><w n="6.1">La</w> <w n="6.2">convention</w>, <w n="6.3">le</w> <w n="6.4">mensonge</w>,</l>
					<l n="7" num="1.7"><w n="7.1">L</w>’<w n="7.2">artificiel</w>, <w n="7.3">donne</w> <w n="7.4">le</w> <w n="7.5">songe</w> ;</l>
					<l n="8" num="1.8"><w n="8.1">C</w>’<w n="8.2">est</w> <w n="8.3">le</w> <w n="8.4">chiffre</w> <w n="8.5">invisible</w> <w n="8.6">et</w> <w n="8.7">roi</w> ;</l>
					<l n="9" num="1.9"><w n="9.1">C</w>’<w n="9.2">est</w> <w n="9.3">la</w> <w n="9.4">science</w> <w n="9.5">qui</w> <w n="9.6">remplace</w></l>
					<l n="10" num="1.10"><w n="10.1">Ce</w> <w n="10.2">que</w> <w n="10.3">nous</w> <w n="10.4">ne</w> <w n="10.5">possédons</w> <w n="10.6">point</w> ;</l>
					<l n="11" num="1.11"><w n="11.1">C</w>’<w n="11.2">est</w> <w n="11.3">aller</w> <w n="11.4">plus</w> <w n="11.5">vite</w> <w n="11.6">et</w> <w n="11.7">plus</w> <w n="11.8">loin</w></l>
					<l n="12" num="1.12"><w n="12.1">Que</w> <w n="12.2">ne</w> <w n="12.3">le</w> <w n="12.4">permet</w> <w n="12.5">notre</w> <w n="12.6">race</w> ;</l>
					<l n="13" num="1.13"><w n="13.1">Être</w> <w n="13.2">le</w> <w n="13.3">monstre</w> <w n="13.4">universel</w></l>
					<l n="14" num="1.14"><w n="14.1">Toujours</w> <w n="14.2">prêt</w> <w n="14.3">à</w> <w n="14.4">tuer</w> <w n="14.5">et</w> <w n="14.6">prendre</w> ;</l>
					<l n="15" num="1.15"><w n="15.1">Sans</w> <w n="15.2">cesse</w> <w n="15.3">monter</w> <w n="15.4">et</w> <w n="15.5">descendre</w>,</l>
					<l n="16" num="1.16">(<w n="16.1">Acrobate</w> <w n="16.2">entre</w> <w n="16.3">terre</w> <w n="16.4">et</w> <w n="16.5">ciel</w>,</l>
					<l n="17" num="1.17"><w n="17.1">Course</w> <w n="17.2">inutile</w> <w n="17.3">et</w> <w n="17.4">dangereuse</w>),</l>
					<l n="18" num="1.18"><w n="18.1">Sur</w> <w n="18.2">l</w>’<w n="18.3">échelle</w> <w n="18.4">mystérieuse</w>…</l>
					<l n="19" num="1.19"><w n="19.1">La</w> <w n="19.2">chimère</w> ?… <w n="19.3">Le</w> <w n="19.4">rustaud</w> <w n="19.5">lourd</w></l>
					<l n="20" num="1.20"><w n="20.1">Qui</w> <w n="20.2">parle</w> <w n="20.3">quand</w> <w n="20.4">même</w> <w n="20.5">d</w>’<w n="20.6">amour</w></l>
					<l n="21" num="1.21"><w n="21.1">Quand</w> <w n="21.2">le</w> <w n="21.3">chien</w> <w n="21.4">prend</w>, <w n="21.5">muet</w>, <w n="21.6">sa</w> <w n="21.7">chienne</w>.</l>
					<l n="22" num="1.22"><w n="22.1">La</w> <w n="22.2">chimère</w> ?… <w n="22.3">Un</w> <w n="22.4">tombeau</w> <w n="22.5">qui</w> <w n="22.6">dit</w></l>
					<l n="23" num="1.23"><w n="23.1">A</w> <w n="23.2">l</w>’<w n="23.3">étranger</w> : « <w n="23.4">Qu</w>’<w n="23.5">il</w> <w n="23.6">vous</w> <w n="23.7">souvienne</w> ! »</l>
					<l n="24" num="1.24"><w n="24.1">La</w> <w n="24.2">cathédrale</w> <w n="24.3">qui</w> <w n="24.4">prédit</w></l>
					<l n="25" num="1.25"><w n="25.1">Des</w> <w n="25.2">contes</w> <w n="25.3">bleus</w> <w n="25.4">pour</w> <w n="25.5">nos</w> <w n="25.6">charognes</w>,</l>
					<l n="26" num="1.26"><w n="26.1">L</w>’<w n="26.2">ambition</w> <w n="26.3">et</w> <w n="26.4">ses</w> <w n="26.5">besognes</w>,</l>
					<l n="27" num="1.27"><w n="27.1">Le</w> <w n="27.2">crime</w> <w n="27.3">et</w> <w n="27.4">son</w> <w n="27.5">absurde</w> <w n="27.6">but</w>.</l>
					<l n="28" num="1.28"><w n="28.1">Quoi</w> ? <w n="28.2">N</w>’<w n="28.3">avait</w>-<w n="28.4">il</w> <w n="28.5">pas</w> <w n="28.6">mangé</w>, <w n="28.7">bu</w> ?…</l>
					<l n="29" num="1.29"><w n="29.1">Tuer</w> ! <w n="29.2">Tuer</w> <w n="29.3">pour</w> <w n="29.4">une</w> <w n="29.5">idée</w>,</l>
					<l n="30" num="1.30"><w n="30.1">O</w> <w n="30.2">jungle</w> ! <w n="30.3">Et</w> <w n="30.4">voici</w> <w n="30.5">qu</w>’<w n="30.6">accoudée</w></l>
					<l n="31" num="1.31"><w n="31.1">La</w> <w n="31.2">poésie</w>, <w n="31.3">en</w> <w n="31.4">même</w> <w n="31.5">temps</w>,</l>
					<l n="32" num="1.32"><w n="32.1">Parle</w> <w n="32.2">de</w> <w n="32.3">rêve</w> <w n="32.4">et</w> <w n="32.5">de</w> <w n="32.6">printemps</w> !</l>
				</lg>
				<lg n="2">
					<l n="33" num="2.1"><w n="33.1">Art</w>, <w n="33.2">musique</w>, <w n="33.3">mots</w> — <w n="33.4">et</w> <w n="33.5">les</w> <w n="33.6">roses</w>,</l>
					<l n="34" num="2.2"><w n="34.1">Églantines</w> <w n="34.2">qu</w>’<w n="34.3">on</w> <w n="34.4">fausse</w>, — <w n="34.5">choses</w></l>
					<l n="35" num="2.3"><w n="35.1">Faites</w> <w n="35.2">pour</w> <w n="35.3">quelque</w> <w n="35.4">ange</w> <w n="35.5">étranger</w>,</l>
					<l n="36" num="2.4"><w n="36.1">Qui</w> <w n="36.2">ne</w> <w n="36.3">sont</w> <w n="36.4">point</w> <w n="36.5">boire</w>, <w n="36.6">manger</w>,</l>
					<l n="37" num="2.5"><w n="37.1">Dormir</w> <w n="37.2">et</w> <w n="37.3">reproduire</w>, <w n="37.4">seules</w></l>
					<l n="38" num="2.6"><w n="38.1">Nécessaires</w> <w n="38.2">et</w> <w n="38.3">lourdes</w> <w n="38.4">meules</w></l>
					<l n="39" num="2.7"><w n="39.1">Faisant</w> <w n="39.2">tourner</w> <w n="39.3">le</w> <w n="39.4">grand</w> <w n="39.5">moulin</w> ;</l>
					<l n="40" num="2.8"><w n="40.1">Tout</w> <w n="40.2">cela</w>, <w n="40.3">travail</w> <w n="40.4">sibyllin</w></l>
					<l n="41" num="2.9"><w n="41.1">De</w> <w n="41.2">l</w>’<w n="41.3">espèce</w> <w n="41.4">humaine</w> <w n="41.5">démente</w>,</l>
					<l n="42" num="2.10"><w n="42.1">Heureuse</w> <w n="42.2">pourvu</w> <w n="42.3">qu</w>’<w n="42.4">elle</w> <w n="42.5">mente</w> ;</l>
					<l n="43" num="2.11"><w n="43.1">L</w>’<w n="43.2">amour</w> <w n="43.3">maternel</w> <w n="43.4">respecté</w></l>
					<l n="44" num="2.12"><w n="44.1">Chez</w> <w n="44.2">la</w> <w n="44.3">femelle</w>, <w n="44.4">ver</w> <w n="44.5">coupé</w></l>
					<l n="45" num="2.13"><w n="45.1">Épris</w> <w n="45.2">de</w> <w n="45.3">son</w> <w n="45.4">tronçon</w> <w n="45.5">qui</w> <w n="45.6">bouge</w> ;</l>
					<l n="46" num="2.14"><w n="46.1">La</w> <w n="46.2">patrie</w> <w n="46.3">et</w> <w n="46.4">tout</w> <w n="46.5">le</w> <w n="46.6">sol</w> <w n="46.7">rouge</w>,</l>
					<l n="47" num="2.15"><w n="47.1">Follement</w>, <w n="47.2">de</w> <w n="47.3">sang</w> <w n="47.4">maternel</w> ;</l>
					<l n="48" num="2.16"><w n="48.1">Le</w> <w n="48.2">bien</w> <w n="48.3">et</w> <w n="48.4">le</w> <w n="48.5">mal</w>, <w n="48.6">éternel</w></l>
					<l n="49" num="2.17"><w n="49.1">Décret</w> <w n="49.2">qui</w> <w n="49.3">fait</w> <w n="49.4">courber</w> <w n="49.5">nos</w> <w n="49.6">têtes</w>,</l>
					<l n="50" num="2.18"><w n="50.1">Ignorée</w> <w n="50.2">à</w> <w n="50.3">jamais</w> <w n="50.4">des</w> <w n="50.5">bêtes</w>,</l>
					<l n="51" num="2.19"><w n="51.1">La</w> <w n="51.2">chimère</w>, <w n="51.3">c</w>’<w n="51.4">est</w> <w n="51.5">tout</w> <w n="51.6">cela</w> !</l>
				</lg>
				<lg n="3">
					<l n="52" num="3.1"><w n="52.1">Alors</w>, <w n="52.2">pourquoi</w>, <w n="52.3">tant</w> <w n="52.4">que</w> <w n="52.5">nous</w> <w n="52.6">sommes</w>,</l>
					<l n="53" num="3.2"><w n="53.1">Race</w> <w n="53.2">chimérique</w> <w n="53.3">des</w> <w n="53.4">hommes</w>,</l>
					<l n="54" num="3.3"><w n="54.1">Ne</w> <w n="54.2">pas</w> <w n="54.3">tous</w> <w n="54.4">croire</w> <w n="54.5">en</w> <w n="54.6">l</w>’<w n="54.7">au</w>-<w n="54.8">delà</w> ?</l>
				</lg>
			</div></body></text></TEI>