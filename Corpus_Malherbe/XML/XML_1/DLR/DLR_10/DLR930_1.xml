<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">MORT ET PRINTEMPS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2448 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_10</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruemortetprintemps.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>MORT ET PRINTEMPS</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Albert Messein</publisher>
							<date when="1932">1932</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques.</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1932">1932</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les titres des poèmes ont été corrigés en fonction de l’édition de référence.</p>
				<p>Les parties manquantes des poèmes "À SAINT GERMAIN" et "À LA NUIT" ont été ajoutées</p>
				<p>Des corrections métriques qui s’imposent ont été faites (erreurs d’édition).</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les retraits introduits automatiquement ont été modifiés conformément à l’édition de référence.</p>
				<p>Des corrections métriques ont été faites ; introduction de licences poétiques ; certe, encor...</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-09-02" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-09-02" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLR930">
				<head type="main">BOUTONS D’OR</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">La</w> <w n="1.2">montée</w> <w n="1.3">à</w> <w n="1.4">travers</w> <w n="1.5">les</w> <w n="1.6">prés</w></l>
					<l n="2" num="1.2"><w n="2.1">Porte</w>, <w n="2.2">en</w> <w n="2.3">mai</w>, <w n="2.4">de</w> <w n="2.5">vastes</w> <w n="2.6">carrés</w></l>
					<l n="3" num="1.3"><w n="3.1">De</w> <w n="3.2">boutons</w> <w n="3.3">d</w>’<w n="3.4">or</w> <w n="3.5">hauts</w> <w n="3.6">et</w> <w n="3.7">serrés</w>.</l>
				</lg>
				<lg n="2">
					<l n="4" num="2.1"><w n="4.1">Cela</w> <w n="4.2">tremble</w> <w n="4.3">dans</w> <w n="4.4">l</w>’<w n="4.5">ombre</w> <w n="4.6">bleue</w>,</l>
					<l n="5" num="2.2"><w n="5.1">Comète</w> <w n="5.2">d</w>’<w n="5.3">or</w> <w n="5.4">à</w> <w n="5.5">longue</w> <w n="5.6">queue</w> ;</l>
					<l n="6" num="2.3"><w n="6.1">On</w> <w n="6.2">en</w> <w n="6.3">a</w> <w n="6.4">bien</w> <w n="6.5">pour</w> <w n="6.6">une</w> <w n="6.7">lieue</w>.</l>
				</lg>
				<lg n="3">
					<l n="7" num="3.1"><w n="7.1">En</w> <w n="7.2">passant</w> <w n="7.3">par</w> <w n="7.4">cet</w> <w n="7.5">or</w>, <w n="7.6">on</w> <w n="7.7">croit</w></l>
					<l n="8" num="3.2"><w n="8.1">Qu</w>’<w n="8.2">on</w> <w n="8.3">foule</w>, <w n="8.4">dans</w> <w n="8.5">l</w>’<w n="8.6">ait</w> <w n="8.7">un</w> <w n="8.8">peu</w> <w n="8.9">froid</w>,</l>
					<l n="9" num="3.3"><w n="9.1">Le</w> <w n="9.2">grand</w> <w n="9.3">manteau</w> <w n="9.4">du</w> <w n="9.5">printemps</w>-<w n="9.6">roi</w>.</l>
				</lg>
			</div></body></text></TEI>