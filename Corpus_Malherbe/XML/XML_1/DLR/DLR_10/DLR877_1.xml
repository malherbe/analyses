<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">MORT ET PRINTEMPS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2448 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_10</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruemortetprintemps.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>MORT ET PRINTEMPS</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Albert Messein</publisher>
							<date when="1932">1932</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques.</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1932">1932</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les titres des poèmes ont été corrigés en fonction de l’édition de référence.</p>
				<p>Les parties manquantes des poèmes "À SAINT GERMAIN" et "À LA NUIT" ont été ajoutées</p>
				<p>Des corrections métriques qui s’imposent ont été faites (erreurs d’édition).</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les retraits introduits automatiquement ont été modifiés conformément à l’édition de référence.</p>
				<p>Des corrections métriques ont été faites ; introduction de licences poétiques ; certe, encor...</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-09-02" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-09-02" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">THE SKULL</head><div type="poem" key="DLR877">
					<head type="number">VII</head>
					<head type="main">RÉVÉLATION</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Sourd</w>, <w n="1.2">muet</w> — <w n="1.3">et</w> <w n="1.4">la</w> <w n="1.5">cécité</w>.</l>
						<l n="2" num="1.2"><w n="2.1">Ce</w> <w n="2.2">crâne</w>, <w n="2.3">pourtant</w>, <w n="2.4">nous</w> <w n="2.5">renseigne</w>.</l>
						<l n="3" num="1.3"><w n="3.1">L</w>’<w n="3.2">ossature</w> <w n="3.3">est</w> <w n="3.4">d</w>’<w n="3.5">un</w> <w n="3.6">autre</w> <w n="3.7">règne</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Il</w> <w n="4.2">semble</w>, <w n="4.3">dans</w> <w n="4.4">sa</w> <w n="4.5">dureté</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">J</w>’<w n="5.2">ai</w> <w n="5.3">voulu</w> <w n="5.4">redonner</w> <w n="5.5">ses</w> <w n="5.6">formes</w></l>
						<l n="6" num="2.2"><w n="6.1">A</w> <w n="6.2">la</w> <w n="6.3">vieille</w> <w n="6.4">tête</w> <w n="6.5">de</w> <w n="6.6">mort</w>.</l>
						<l n="7" num="2.3"><w n="7.1">Un</w> <w n="7.2">peu</w> <w n="7.3">de</w> <w n="7.4">cire</w>, <w n="7.5">un</w> <w n="7.6">peu</w> <w n="7.7">d</w>’<w n="7.8">effort</w></l>
						<l n="8" num="2.4"><w n="8.1">Comblent</w> <w n="8.2">les</w> <w n="8.3">orbites</w> <w n="8.4">énormes</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Mes</w> <w n="9.2">doigts</w> <w n="9.3">recouvrent</w> <w n="9.4">le</w> <w n="9.5">nez</w> <w n="9.6">creux</w>,</l>
						<l n="10" num="3.2"><w n="10.1">Et</w> <w n="10.2">la</w> <w n="10.3">mâchoire</w> <w n="10.4">et</w> <w n="10.5">la</w> <w n="10.6">pommette</w>,</l>
						<l n="11" num="3.3"><w n="11.1">Et</w>, <w n="11.2">sur</w> <w n="11.3">cette</w> <w n="11.4">armature</w> <w n="11.5">nette</w>,</l>
						<l n="12" num="3.4"><w n="12.1">La</w> <w n="12.2">tête</w> <w n="12.3">se</w> <w n="12.4">modèle</w> <w n="12.5">au</w> <w n="12.6">mieux</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">O</w> <w n="13.2">surprise</w> ! <w n="13.3">O</w> <w n="13.4">face</w> <w n="13.5">inconnue</w></l>
						<l n="14" num="4.2"><w n="14.1">Qui</w> <w n="14.2">se</w> <w n="14.3">sculpte</w> <w n="14.4">en</w> <w n="14.5">dépit</w> <w n="14.6">de</w> <w n="14.7">moi</w> !</l>
						<l n="15" num="4.3"><w n="15.1">Sur</w> <w n="15.2">ce</w> <w n="15.3">masque</w> <w n="15.4">et</w> <w n="15.5">sur</w> <w n="15.6">son</w> <w n="15.7">effroi</w></l>
						<l n="16" num="4.4"><w n="16.1">Surgit</w> <w n="16.2">une</w> <w n="16.3">figure</w> <w n="16.4">nue</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Les</w> <w n="17.2">yeux</w>, <w n="17.3">qui</w> <w n="17.4">paraissaient</w> <w n="17.5">si</w> <w n="17.6">grands</w>,</l>
						<l n="18" num="5.2"><w n="18.1">Sont</w> <w n="18.2">petits</w>. <w n="18.3">Ce</w> <w n="18.4">n</w>’<w n="18.5">est</w> <w n="18.6">pas</w> <w n="18.7">ma</w> <w n="18.8">faute</w>.</l>
						<l n="19" num="5.3"><w n="19.1">Le</w> <w n="19.2">front</w> <w n="19.3">est</w> <w n="19.4">bas</w>, <w n="19.5">la</w> <w n="19.6">joue</w> <w n="19.7">est</w> <w n="19.8">haute</w> ;</l>
						<l n="20" num="5.4"><w n="20.1">C</w>’<w n="20.2">est</w> <w n="20.3">un</w> <w n="20.4">type</w> <w n="20.5">des</w> <w n="20.6">plus</w> <w n="20.7">courants</w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Ainsi</w> <w n="21.2">donc</w> <w n="21.3">un</w> <w n="21.4">banal</w> <w n="21.5">visage</w></l>
						<l n="22" num="6.2"><w n="22.1">Recouvrait</w> <w n="22.2">l</w>’<w n="22.3">affreux</w> <w n="22.4">cauchemar</w> ;</l>
						<l n="23" num="6.3"><w n="23.1">Ces</w> <w n="23.2">trous</w> <w n="23.3">vides</w>, <w n="23.4">ce</w> <w n="23.5">nez</w> <w n="23.6">camard</w></l>
						<l n="24" num="6.4"><w n="24.1">N</w>’<w n="24.2">en</w> <w n="24.3">voulaient</w> <w n="24.4">dire</w> <w n="24.5">davantage</w>.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">Un</w> <w n="25.2">insignifiant</w> <w n="25.3">humain</w></l>
						<l n="26" num="7.2"><w n="26.1">Remplace</w> <w n="26.2">le</w> <w n="26.3">crâne</w> <w n="26.4">ascétique</w></l>
						<l n="27" num="7.3"><w n="27.1">Se</w> <w n="27.2">substitue</w> <w n="27.3">au</w> <w n="27.4">fantastique</w></l>
						<l n="28" num="7.4"><w n="28.1">Que</w> <w n="28.2">je</w> <w n="28.3">regardais</w> <w n="28.4">dans</w> <w n="28.5">ma</w> <w n="28.6">main</w>.</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1"><w n="29.1">Qu</w>’<w n="29.2">était</w>-<w n="29.3">il</w> ? <w n="29.4">Un</w> <w n="29.5">homme</w> ? <w n="29.6">Une</w> <w n="29.7">femme</w> ?</l>
						<l n="30" num="8.2"><w n="30.1">Il</w> <w n="30.2">allait</w>, <w n="30.3">suivant</w> <w n="30.4">le</w> <w n="30.5">troupeau</w>,</l>
						<l n="31" num="8.3"><w n="31.1">Sans</w> <w n="31.2">se</w> <w n="31.3">douter</w> <w n="31.4">que</w>, <w n="31.5">sous</w> <w n="31.6">sa</w> <w n="31.7">peau</w>,</l>
						<l n="32" num="8.4"><w n="32.1">Tranquille</w>, <w n="32.2">il</w> <w n="32.3">portait</w> <w n="32.4">un</w> <w n="32.5">tel</w> <w n="32.6">drame</w>.</l>
					</lg>
				</div></body></text></TEI>