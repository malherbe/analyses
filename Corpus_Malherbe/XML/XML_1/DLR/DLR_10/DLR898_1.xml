<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">MORT ET PRINTEMPS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2448 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_10</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruemortetprintemps.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>MORT ET PRINTEMPS</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Albert Messein</publisher>
							<date when="1932">1932</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques.</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1932">1932</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les titres des poèmes ont été corrigés en fonction de l’édition de référence.</p>
				<p>Les parties manquantes des poèmes "À SAINT GERMAIN" et "À LA NUIT" ont été ajoutées</p>
				<p>Des corrections métriques qui s’imposent ont été faites (erreurs d’édition).</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les retraits introduits automatiquement ont été modifiés conformément à l’édition de référence.</p>
				<p>Des corrections métriques ont été faites ; introduction de licences poétiques ; certe, encor...</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-09-02" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-09-02" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLR898">
				<head type="main">ARRIVÉE</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Dans</w> <w n="1.2">la</w> <w n="1.3">fente</w> <w n="1.4">de</w> <w n="1.5">mes</w> <w n="1.6">rideaux</w></l>
					<l n="2" num="1.2"><w n="2.1">Je</w> <w n="2.2">vois</w> <w n="2.3">remuer</w> <w n="2.4">des</w> <w n="2.5">étoiles</w>.</l>
					<l n="3" num="1.3"><w n="3.1">L</w>’<w n="3.2">estuaire</w> <w n="3.3">est</w> <w n="3.4">plein</w> <w n="3.5">de</w> <w n="3.6">bateaux</w></l>
					<l n="4" num="1.4"><w n="4.1">Qui</w> <w n="4.2">s</w>’<w n="4.3">en</w> <w n="4.4">vont</w> <w n="4.5">sous</w> <w n="4.6">leurs</w> <w n="4.7">quatre</w> <w n="4.8">voiles</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Je</w> <w n="5.2">suis</w> <w n="5.3">seule</w> <w n="5.4">au</w> <w n="5.5">coin</w> <w n="5.6">de</w> <w n="5.7">mon</w> <w n="5.8">feu</w>.</l>
					<l n="6" num="2.2"><w n="6.1">Printemps</w>, <w n="6.2">campagne</w>, <w n="6.3">grandes</w> <w n="6.4">bûches</w>.</l>
					<l n="7" num="2.3"><w n="7.1">Le</w> <w n="7.2">silence</w> <w n="7.3">est</w> <w n="7.4">chargé</w> <w n="7.5">d</w>’<w n="7.6">embûches</w></l>
					<l n="8" num="2.4"><w n="8.1">Et</w> <w n="8.2">tel</w> <w n="8.3">qu</w>’<w n="8.4">il</w> <w n="8.5">m</w>’<w n="8.6">étourdit</w> <w n="8.7">un</w> <w n="8.8">peu</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Et</w> <w n="9.2">cependant</w> <w n="9.3">Paris</w> <w n="9.4">existe</w></l>
					<l n="10" num="3.2"><w n="10.1">Au</w> <w n="10.2">loin</w>, <w n="10.3">et</w> <w n="10.4">son</w> <w n="10.5">immense</w> <w n="10.6">bruit</w>.</l>
					<l n="11" num="3.3"><w n="11.1">A</w> <w n="11.2">Paris</w>, <w n="11.3">mon</w> <w n="11.4">Dieu</w>, <w n="11.5">que</w> <w n="11.6">c</w>’<w n="11.7">est</w> <w n="11.8">triste</w></l>
					<l n="12" num="3.4"><w n="12.1">De</w> <w n="12.2">n</w>’<w n="12.3">être</w> <w n="12.4">pas</w> <w n="12.5">seul</w> <w n="12.6">dans</w> <w n="12.7">la</w> <w n="12.8">nuit</w> !</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">J</w>’<w n="13.2">évoque</w>, <w n="13.3">et</w> <w n="13.4">mon</w> <w n="13.5">front</w> <w n="13.6">dans</w> <w n="13.7">mes</w> <w n="13.8">paumes</w></l>
					<l n="14" num="4.2"><w n="14.1">Est</w> <w n="14.2">lourd</w> <w n="14.3">de</w> <w n="14.4">rêves</w> <w n="14.5">enchanteurs</w>,</l>
					<l n="15" num="4.3"><w n="15.1">Les</w> <w n="15.2">maisons</w> <w n="15.3">à</w> <w n="15.4">radiateurs</w></l>
					<l n="16" num="4.4"><w n="16.1">Qui</w> <w n="16.2">sont</w> <w n="16.3">sans</w> <w n="16.4">rêves</w> <w n="16.5">ni</w> <w n="16.6">fantômes</w>.</l>
				</lg>
			</div></body></text></TEI>