<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">MORT ET PRINTEMPS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2448 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_10</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruemortetprintemps.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>MORT ET PRINTEMPS</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Albert Messein</publisher>
							<date when="1932">1932</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques.</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1932">1932</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les titres des poèmes ont été corrigés en fonction de l’édition de référence.</p>
				<p>Les parties manquantes des poèmes "À SAINT GERMAIN" et "À LA NUIT" ont été ajoutées</p>
				<p>Des corrections métriques qui s’imposent ont été faites (erreurs d’édition).</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les retraits introduits automatiquement ont été modifiés conformément à l’édition de référence.</p>
				<p>Des corrections métriques ont été faites ; introduction de licences poétiques ; certe, encor...</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-09-02" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-09-02" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLR907">
				<head type="main">A SAINT-GERMAIN</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Le</w> <w n="1.2">trot</w> <w n="1.3">d</w>’<w n="1.4">un</w> <w n="1.5">vieux</w> <w n="1.6">cheval</w> <w n="1.7">de</w> <w n="1.8">fiacre</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Vivant</w> <w n="2.2">vestige</w> <w n="2.3">d</w>’<w n="2.4">autrefois</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Parcourait</w> <w n="3.2">au</w> <w n="3.3">couchant</w> <w n="3.4">les</w> <w n="3.5">bois</w></l>
					<l n="4" num="1.4"><w n="4.1">Où</w> <w n="4.2">l</w>’<w n="4.3">air</w> <w n="4.4">d</w>’<w n="4.5">avril</w> <w n="4.6">reste</w> <w n="4.7">encore</w> <w n="4.8">âcre</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Il</w> <w n="5.2">allait</w> <w n="5.3">à</w> <w n="5.4">travers</w> <w n="5.5">le</w> <w n="5.6">temps</w></l>
					<l n="6" num="2.2"><w n="6.1">Aussi</w> <w n="6.2">bien</w> <w n="6.3">qu</w>’<w n="6.4">à</w> <w n="6.5">travers</w> <w n="6.6">l</w>’<w n="6.7">espace</w>.</l>
					<l n="7" num="2.3"><w n="7.1">Nous</w> <w n="7.2">avions</w> <w n="7.3">quitté</w> <w n="7.4">la</w> <w n="7.5">terrasse</w></l>
					<l n="8" num="2.4"><w n="8.1">Qui</w> <w n="8.2">me</w> <w n="8.3">vit</w> <w n="8.4">avant</w> <w n="8.5">mon</w> <w n="8.6">printemps</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Je</w> <w n="9.2">suis</w> <w n="9.3">l</w>’<w n="9.4">assez</w> <w n="9.5">jeune</w> <w n="9.6">grand</w>’<w n="9.7">mère</w></l>
					<l n="10" num="3.2"><w n="10.1">D</w>’<w n="10.2">une</w> <w n="10.3">fillette</w> <w n="10.4">de</w> <w n="10.5">jadis</w>.</l>
					<l n="11" num="3.3"><w n="11.1">Mon</w> <w n="11.2">âge</w> <w n="11.3">alors</w> ? <w n="11.4">Neuf</w> <w n="11.5">ans</w> <w n="11.6">ou</w> <w n="11.7">dix</w>,</l>
					<l n="12" num="3.4"><w n="12.1">Long</w> <w n="12.2">souvenir</w>, <w n="12.3">temps</w> <w n="12.4">éphémère</w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Le</w> <w n="13.2">cœur</w>, <w n="13.3">d</w>’<w n="13.4">ordinaire</w>, <w n="13.5">se</w> <w n="13.6">fend</w>,</l>
					<l n="14" num="4.2"><w n="14.1">Quand</w> <w n="14.2">on</w> <w n="14.3">revit</w> <w n="14.4">ces</w> <w n="14.5">heures</w> <w n="14.6">mortes</w>.</l>
					<l n="15" num="4.3"><w n="15.1">Non</w>. <w n="15.2">Par</w> <w n="15.3">les</w> <w n="15.4">mêmes</w> <w n="15.5">routes</w> <w n="15.6">tortes</w>,</l>
					<l n="16" num="4.4"><w n="16.1">Je</w> <w n="16.2">me</w> <w n="16.3">sentais</w> <w n="16.4">toujours</w> <w n="16.5">enfant</w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">Rien</w> <w n="17.2">de</w> <w n="17.3">bien</w> <w n="17.4">changé</w> <w n="17.5">dans</w> <w n="17.6">ma</w> <w n="17.7">tête</w>.</l>
					<l n="18" num="5.2"><w n="18.1">Je</w> <w n="18.2">restais</w> <w n="18.3">un</w> <w n="18.4">être</w> <w n="18.5">pareil</w>,</l>
					<l n="19" num="5.3"><w n="19.1">Étant</w>, <w n="19.2">sous</w> <w n="19.3">le</w> <w n="19.4">même</w> <w n="19.5">soleil</w>,</l>
					<l n="20" num="5.4"><w n="20.1">Le</w> <w n="20.2">même</w> <w n="20.3">identique</w> <w n="20.4">poète</w>.</l>
				</lg>
			</div></body></text></TEI>