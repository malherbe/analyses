<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">MORT ET PRINTEMPS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2448 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_10</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruemortetprintemps.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>MORT ET PRINTEMPS</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Albert Messein</publisher>
							<date when="1932">1932</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques.</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1932">1932</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les titres des poèmes ont été corrigés en fonction de l’édition de référence.</p>
				<p>Les parties manquantes des poèmes "À SAINT GERMAIN" et "À LA NUIT" ont été ajoutées</p>
				<p>Des corrections métriques qui s’imposent ont été faites (erreurs d’édition).</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les retraits introduits automatiquement ont été modifiés conformément à l’édition de référence.</p>
				<p>Des corrections métriques ont été faites ; introduction de licences poétiques ; certe, encor...</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-09-02" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-09-02" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">JACQUELINE</head><div type="poem" key="DLR963">
					<head type="number">XI</head>
					<lg n="1">
						<l n="1" num="1.1"><space unit="char" quantity="8"></space><w n="1.1">J</w>’<w n="1.2">avais</w> <w n="1.3">encor</w> <w n="1.4">tant</w> <w n="1.5">à</w> <w n="1.6">te</w> <w n="1.7">dire</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Tant</w> <w n="2.2">à</w> <w n="2.3">te</w> <w n="2.4">demander</w> <w n="2.5">et</w> <w n="2.6">tant</w> <w n="2.7">à</w> <w n="2.8">te</w> <w n="2.9">donner</w>…</l>
						<l n="3" num="1.3"><space unit="char" quantity="8"></space><w n="3.1">Mais</w> <w n="3.2">tu</w> <w n="3.3">meurs</w>. <w n="3.4">Tout</w> <w n="3.5">est</w> <w n="3.6">terminé</w>.</l>
						<l n="4" num="1.4"><space unit="char" quantity="8"></space><w n="4.1">J</w>’<w n="4.2">avais</w> <w n="4.3">encor</w> <w n="4.4">tant</w> <w n="4.5">à</w> <w n="4.6">te</w> <w n="4.7">dire</w> !</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><space unit="char" quantity="8"></space><w n="5.1">Puisque</w> <w n="5.2">tu</w> <w n="5.3">n</w>’<w n="5.4">as</w> <w n="5.5">plus</w> <w n="5.6">d</w>’<w n="5.7">avenir</w>,</l>
						<l n="6" num="2.2"><w n="6.1">Ce</w> <w n="6.2">n</w>’<w n="6.3">est</w> <w n="6.4">donc</w> <w n="6.5">rien</w> <w n="6.6">que</w> <w n="6.7">du</w> <w n="6.8">passé</w> <w n="6.9">qu</w>’<w n="6.10">il</w> <w n="6.11">me</w> <w n="6.12">faut</w> <w n="6.13">vivre</w>,</l>
						<l n="7" num="2.3"><space unit="char" quantity="8"></space><w n="7.1">Tome</w> <w n="7.2">premier</w> <w n="7.3">sans</w> <w n="7.4">second</w> <w n="7.5">livre</w>,</l>
						<l n="8" num="2.4"><space unit="char" quantity="8"></space><w n="8.1">Puisque</w> <w n="8.2">tu</w> <w n="8.3">n</w>’<w n="8.4">as</w> <w n="8.5">plus</w> <w n="8.6">d</w>’<w n="8.7">avenir</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><space unit="char" quantity="8"></space><w n="9.1">Nous</w> <w n="9.2">étions</w> <w n="9.3">deux</w>, <w n="9.4">me</w> <w n="9.5">voici</w> <w n="9.6">seule</w>.</l>
						<l n="10" num="3.2"><w n="10.1">Maintenant</w> <w n="10.2">tu</w> <w n="10.3">ne</w> <w n="10.4">sais</w> <w n="10.5">même</w> <w n="10.6">pas</w> <w n="10.7">que</w> <w n="10.8">tu</w> <w n="10.9">fus</w>.</l>
						<l n="11" num="3.3"><space unit="char" quantity="8"></space><w n="11.1">Néant</w>, <w n="11.2">tu</w> <w n="11.3">ne</w> <w n="11.4">me</w> <w n="11.5">connais</w> <w n="11.6">plus</w>.</l>
						<l n="12" num="3.4"><space unit="char" quantity="8"></space><w n="12.1">Nous</w> <w n="12.2">étions</w> <w n="12.3">deux</w>, <w n="12.4">me</w> <w n="12.5">voici</w> <w n="12.6">seule</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><space unit="char" quantity="8"></space><w n="13.1">Mon</w> <w n="13.2">cœur</w> <w n="13.3">à</w> <w n="13.4">moi</w> <w n="13.5">n</w>’<w n="13.6">a</w> <w n="13.7">pas</w> <w n="13.8">changé</w>.</l>
						<l n="14" num="4.2"><w n="14.1">Toi</w>, <w n="14.2">tu</w> <w n="14.3">n</w>’<w n="14.4">es</w> <w n="14.5">qu</w>’<w n="14.6">un</w> <w n="14.7">tombeau</w>, <w n="14.8">mais</w> <w n="14.9">ma</w> <w n="14.10">tendresse</w> <w n="14.11">reste</w>,</l>
						<l n="15" num="4.3"><space unit="char" quantity="8"></space><w n="15.1">Inutile</w>, <w n="15.2">sans</w> <w n="15.3">mots</w>, <w n="15.4">sans</w> <w n="15.5">geste</w>,.</l>
						<l n="16" num="4.4"><space unit="char" quantity="8"></space>— <w n="16.1">Mon</w> <w n="16.2">cœur</w> <w n="16.3">à</w> <w n="16.4">moi</w> <w n="16.5">n</w>’<w n="16.6">a</w> <w n="16.7">pas</w> <w n="16.8">changé</w>.</l>
					</lg>
				</div></body></text></TEI>