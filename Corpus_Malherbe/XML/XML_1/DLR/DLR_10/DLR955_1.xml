<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">MORT ET PRINTEMPS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2448 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_10</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruemortetprintemps.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>MORT ET PRINTEMPS</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Albert Messein</publisher>
							<date when="1932">1932</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques.</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1932">1932</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les titres des poèmes ont été corrigés en fonction de l’édition de référence.</p>
				<p>Les parties manquantes des poèmes "À SAINT GERMAIN" et "À LA NUIT" ont été ajoutées</p>
				<p>Des corrections métriques qui s’imposent ont été faites (erreurs d’édition).</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les retraits introduits automatiquement ont été modifiés conformément à l’édition de référence.</p>
				<p>Des corrections métriques ont été faites ; introduction de licences poétiques ; certe, encor...</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-09-02" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-09-02" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">JACQUELINE</head><div type="poem" key="DLR955">
					<head type="number">III</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Je</w> <w n="1.2">te</w> <w n="1.3">disais</w>, <w n="1.4">lorsque</w> <w n="1.5">vivante</w>,</l>
						<l n="2" num="1.2">« <w n="2.1">Bonjour</w>, <w n="2.2">petit</w> <w n="2.3">Jacquelinot</w> ! »</l>
						<l n="3" num="1.3"><w n="3.1">Je</w> <w n="3.2">le</w> <w n="3.3">redis</w> <w n="3.4">dans</w> <w n="3.5">l</w>’<w n="3.6">épouvante</w>.</l>
						<l n="4" num="1.4"><w n="4.1">Te</w> <w n="4.2">voilà</w> <w n="4.3">si</w> <w n="4.4">bas</w> — <w n="4.5">ou</w> <w n="4.6">si</w> <w n="4.7">haut</w> !</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Disparue</w> <w n="5.2">à</w> <w n="5.3">jamais</w>. <w n="5.4">En</w> <w n="5.5">proie</w></l>
						<l n="6" num="2.2"><w n="6.1">A</w> <w n="6.2">la</w> <w n="6.3">décomposition</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Toi</w>, <w n="7.2">toi</w> !… <w n="7.3">Sans</w> <w n="7.4">douleur</w> <w n="7.5">et</w> <w n="7.6">ans</w> <w n="7.7">joie</w>,</l>
						<l n="8" num="2.4"><w n="8.1">Sans</w> <w n="8.2">espoir</w> <w n="8.3">et</w> <w n="8.4">sans</w> <w n="8.5">passion</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Objet</w> <w n="9.2">dans</w> <w n="9.3">la</w> <w n="9.4">terre</w>, <w n="9.5">squelette</w></l>
						<l n="10" num="3.2"><w n="10.1">Qui</w> <w n="10.2">travaille</w> <w n="10.3">à</w> <w n="10.4">se</w> <w n="10.5">délivrer</w></l>
						<l n="11" num="3.3"><w n="11.1">DE</w> <w n="11.2">ce</w> <w n="11.3">fardeau</w> <w n="11.4">pâle</w> <w n="11.5">et</w> <w n="11.6">doré</w></l>
						<l n="12" num="3.4"><w n="12.1">Qui</w> <w n="12.2">composait</w> <w n="12.3">ta</w> <w n="12.4">silhouette</w>,</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Tes</w> <w n="13.2">yeux</w> <w n="13.3">qui</w> <w n="13.4">plongeaient</w> <w n="13.5">dans</w> <w n="13.6">mes</w> <w n="13.7">yeux</w></l>
						<l n="14" num="4.2"><w n="14.1">Et</w> <w n="14.2">que</w> <w n="14.3">révulsa</w> <w n="14.4">l</w>’<w n="14.5">agonie</w>,</l>
						<l n="15" num="4.3"><w n="15.1">Ces</w> <w n="15.2">deux</w> <w n="15.3">beaux</w> <w n="15.4">joyaux</w> <w n="15.5">verts</w> <w n="15.6">et</w> <w n="15.7">bleus</w></l>
						<l n="16" num="4.4"><w n="16.1">Sont</w> <w n="16.2">déjà</w> <w n="16.3">dans</w> <w n="16.4">l</w>’<w n="16.5">ignominie</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Ta</w> <w n="17.2">bouche</w>… <w n="17.3">tes</w> <w n="17.4">mains</w>… <w n="17.5">tout</w> <w n="17.6">ton</w> <w n="17.7">corps</w>,</l>
						<l n="18" num="5.2"><w n="18.1">Tout</w> <w n="18.2">cela</w> <w n="18.3">dans</w> <w n="18.4">la</w> <w n="18.5">tombe</w> <w n="18.6">close</w></l>
						<l n="19" num="5.3"><w n="19.1">Existe</w> <w n="19.2">encore</w>, <w n="19.3">longue</w> <w n="19.4">chose</w></l>
						<l n="20" num="5.4"><w n="20.1">Qu</w>’<w n="20.2">on</w> <w n="20.3">cacha</w> <w n="20.4">comme</w> <w n="20.5">les</w> <w n="20.6">trésors</w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Et</w> <w n="21.2">tout</w> <w n="21.3">cela</w> <w n="21.4">va</w> <w n="21.5">disparaître</w></l>
						<l n="22" num="6.2"><w n="22.1">Lentement</w> <w n="22.2">dans</w> <w n="22.3">l</w>’<w n="22.4">obscurité</w>.</l>
						<l n="23" num="6.3"><w n="23.1">Une</w> <w n="23.2">fille</w> <w n="23.3">si</w> <w n="23.4">jeune</w>, <w n="23.5">un</w> <w n="23.6">être</w></l>
						<l n="24" num="6.4"><w n="24.1">Dont</w> <w n="24.2">il</w> <w n="24.3">ne</w> <w n="24.4">va</w> <w n="24.5">plus</w> <w n="24.6">rien</w> <w n="24.7">rester</w>.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">Brûlante</w> <w n="25.2">encore</w> <w n="25.3">est</w> <w n="25.4">ta</w> <w n="25.5">présence</w>,</l>
						<l n="26" num="7.2"><w n="26.1">Ta</w> <w n="26.2">place</w> <w n="26.3">dans</w> <w n="26.4">ce</w> <w n="26.5">monde</w> <w n="26.6">ci</w>.</l>
						<l n="27" num="7.3"><w n="27.1">Je</w> <w n="27.2">te</w> <w n="27.3">revois</w> <w n="27.4">partout</w> : <w n="27.5">ici</w>,</l>
						<l n="28" num="7.4"><w n="28.1">Là</w> ; <w n="28.2">c</w>’<w n="28.3">est</w> <w n="28.4">ton</w> <w n="28.5">regard</w>, <w n="28.6">ta</w> <w n="28.7">cadence</w>,</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1"><w n="29.1">Et</w> <w n="29.2">rien</w> ! <w n="29.3">tu</w> <w n="29.4">ne</w> <w n="29.5">reviendra</w> <w n="29.6">pas</w>,</l>
						<l n="30" num="8.2"><w n="30.1">Je</w> <w n="30.2">ne</w> <w n="30.3">saurai</w> <w n="30.4">plus</w> <w n="30.5">rien</w>. <w n="30.6">J</w>’<w n="30.7">ignore</w></l>
						<l n="31" num="8.3"><w n="31.1">Qui</w> <w n="31.2">tu</w> <w n="31.3">es</w>, <w n="31.4">si</w> <w n="31.5">tu</w> <w n="31.6">vis</w> <w n="31.7">encore</w></l>
						<l n="32" num="8.4"><w n="32.1">Dans</w> <w n="32.2">on</w> <w n="32.3">ne</w> <w n="32.4">ait</w> <w n="32.5">quel</w> <w n="32.6">grand</w> <w n="32.7">là</w>-<w n="32.8">bas</w>.</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1"><w n="33.1">Je</w> <w n="33.2">répète</w> <w n="33.3">mon</w> <w n="33.4">appel</w> <w n="33.5">tendre</w>.</l>
						<l n="34" num="9.2"><w n="34.1">Est</w>-<w n="34.2">ce</w> <w n="34.3">que</w> <w n="34.4">je</w> <w n="34.5">parle</w> <w n="34.6">à</w> <w n="34.7">quelqu</w>’<w n="34.8">un</w> ?</l>
						<l n="35" num="9.3"><w n="35.1">Je</w> <w n="35.2">ne</w> <w n="35.3">sais</w> <w n="35.4">si</w> <w n="35.5">tu</w> <w n="35.6">peux</w> <w n="35.7">m</w>’<w n="35.8">entendre</w>.</l>
						<l n="36" num="9.4"><w n="36.1">Des</w> <w n="36.2">signes</w> ? <w n="36.3">Tu</w> <w n="36.4">n</w>’<w n="36.5">en</w> <w n="36.6">fais</w> <w n="36.7">aucun</w>.</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1"><w n="37.1">Ah</w> ! <w n="37.2">la</w> <w n="37.3">mort</w> ! <w n="37.4">Cette</w> <w n="37.5">indifférence</w></l>
						<l n="38" num="10.2"><w n="38.1">De</w> <w n="38.2">l</w>’<w n="38.3">être</w> <w n="38.4">qui</w> <w n="38.5">s</w>’<w n="38.6">en</w> <w n="38.7">est</w> <w n="38.8">allé</w> !</l>
						<l n="39" num="10.3"><w n="39.1">Mes</w> <w n="39.2">ennemis</w> <w n="39.3">peuvent</w> <w n="39.4">parler</w>,</l>
						<l n="40" num="10.4"><w n="40.1">Tu</w> <w n="40.2">ne</w> <w n="40.3">prendras</w> <w n="40.4">plus</w> <w n="40.5">ma</w> <w n="40.6">défense</w>.</l>
					</lg>
					<lg n="11">
						<l n="41" num="11.1"><w n="41.1">Moi</w> <w n="41.2">je</w> <w n="41.3">ne</w> <w n="41.4">suis</w> <w n="41.5">plus</w> <w n="41.6">rien</w> <w n="41.7">pour</w> <w n="41.8">toi</w>.</l>
						<l n="42" num="11.2"><w n="42.1">Tu</w> <w n="42.2">te</w> <w n="42.3">reposes</w> <w n="42.4">dans</w> <w n="42.5">ta</w> <w n="42.6">terre</w>,</l>
						<l n="43" num="11.3"><w n="43.1">Plus</w> <w n="43.2">inerte</w> <w n="43.3">qu</w>’<w n="43.4">un</w> <w n="43.5">caillou</w> <w n="43.6">froid</w>,</l>
						<l n="44" num="11.4"><w n="44.1">Têtue</w>, <w n="44.2">occupée</w> <w n="44.3">à</w> <w n="44.4">te</w> <w n="44.5">taire</w>.</l>
					</lg>
					<lg n="12">
						<l n="45" num="12.1"><w n="45.1">Ce</w> <w n="45.2">surnom</w> <w n="45.3">qui</w> <w n="45.4">fut</w> <w n="45.5">toi</w>, <w n="45.6">pourtant</w>,</l>
						<l n="46" num="12.2"><w n="46.1">Jacquelinot</w>, <w n="46.2">je</w> <w n="46.3">le</w> <w n="46.4">répète</w>.</l>
						<l n="47" num="12.3"><w n="47.1">Mais</w> <w n="47.2">ma</w> <w n="47.3">pauvre</w> <w n="47.4">voix</w> <w n="47.5">inquiète</w></l>
						<l n="48" num="12.4"><w n="48.1">N</w>’<w n="48.2">appelle</w> <w n="48.3">plus</w> <w n="48.4">que</w> <w n="48.5">du</w> <w n="48.6">néant</w>.</l>
					</lg>
					<lg n="13">
						<l n="49" num="13.1"><w n="49.1">L</w>’<w n="49.2">entends</w>-<w n="49.3">tu</w>, <w n="49.4">ma</w> <w n="49.5">vois</w> <w n="49.6">douloureuse</w> ?</l>
						<l n="50" num="13.2">— <w n="50.1">Jacquelinot</w>, <w n="50.2">notre</w> <w n="50.3">amitié</w> !</l>
						<l n="51" num="13.3"><w n="51.1">Non</w> ! <w n="51.2">Tu</w> <w n="51.3">n</w>’<w n="51.4">est</w> <w n="51.5">qu</w>’<w n="51.6">une</w> <w n="51.7">mort</w> <w n="51.8">affreuse</w></l>
						<l n="52" num="13.4"><w n="52.1">Qui</w> <w n="52.2">dort</w>, <w n="52.3">ayant</w> <w n="52.4">tout</w> <w n="52.5">oublié</w>.</l>
					</lg>
				</div></body></text></TEI>