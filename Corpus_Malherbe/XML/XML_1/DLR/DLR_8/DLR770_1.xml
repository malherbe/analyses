<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÈMES MIGNONS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1223 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_8</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÈMES MIGNONS</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruepoesies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÈMES MIGNONS</title>
								<author>Lucie Delarue-Mardrus</author>
								<imprint>
									<pubPlace>Honfleur</pubPlace>
									<publisher>Éditions de la Lieutenance</publisher>
									<date when="2002">2002</date>
								</imprint>
							</monogr>
							<note>L’édition de 2002 est la source de cette version électronique comme le prouve le choix des poèmes et les erreurs d’édition.</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
				<sourceDesc>
					<biblStruct>
						<monogr>
							<title>POÈMES MIGNONS</title>
							<author>Lucie Delarue-Mardrus</author>
							<imprint>
								<pubPlace>Paris</pubPlace>
								<publisher>LIBRAIRIE GEDALGE</publisher>
								<date when="1929">1929</date>
							</imprint>
						</monogr>
						<note>Édition de référence (numérisée)</note>
					</biblStruct>
				</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1929">1929</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le texte a été mis en conformité avec l’édition originale de 1929.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les retraits introduits automatiquement ont été modifiés conformément à l’édition de référence.</p>
				<p>Les poèmes manquants de l’édition 2002 qui est à l’origine de la version électronique disponible on été ajoutés afin d’être conforme à l’édition de 1929.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLR770">
				<head type="main">Le Vent</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Dans</w> <w n="1.2">le</w> <w n="1.3">jardin</w>, <w n="1.4">le</w> <w n="1.5">vent</w> <w n="1.6">nous</w> <w n="1.7">pousse</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Et</w> <w n="2.2">pourtant</w> <w n="2.3">on</w> <w n="2.4">ne</w> <w n="2.5">le</w> <w n="2.6">voit</w> <w n="2.7">pas</w>.</l>
					<l n="3" num="1.3"><w n="3.1">Nous</w> <w n="3.2">avons</w> <w n="3.3">beau</w> <w n="3.4">lui</w> <w n="3.5">crier</w> : « <w n="3.6">Pouce</w> ! »</l>
					<l n="4" num="1.4"><w n="4.1">Rien</w> <w n="4.2">n</w>’<w n="4.3">arrête</w> <w n="4.4">son</w> <w n="4.5">branle</w>-<w n="4.6">bas</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Quand</w> <w n="5.2">nous</w> <w n="5.3">courons</w> <w n="5.4">sur</w> <w n="5.5">les</w> <w n="5.6">pelouses</w>,</l>
					<l n="6" num="2.2"><w n="6.1">Il</w> <w n="6.2">court</w> <w n="6.3">encor</w> <w n="6.4">plus</w> <w n="6.5">fort</w> <w n="6.6">que</w> <w n="6.7">nous</w>.</l>
					<l n="7" num="2.3"><w n="7.1">Tantôt</w> <w n="7.2">il</w> <w n="7.3">colle</w> <w n="7.4">à</w> <w n="7.5">nos</w> <w n="7.6">genoux</w>,</l>
					<l n="8" num="2.4"><w n="8.1">Tantôt</w> <w n="8.2">il</w> <w n="8.3">nous</w> <w n="8.4">gonfle</w> <w n="8.5">nos</w> <w n="8.6">blouses</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Aussi</w> <w n="9.2">brusque</w> <w n="9.3">que</w> <w n="9.4">les</w> <w n="9.5">garçons</w>,</l>
					<l n="10" num="3.2"><w n="10.1">Il</w> <w n="10.2">tire</w> <w n="10.3">les</w> <w n="10.4">robes</w> <w n="10.5">des</w> <w n="10.6">filles</w>.</l>
					<l n="11" num="3.3"><w n="11.1">Il</w> <w n="11.2">nous</w> <w n="11.3">roule</w> <w n="11.4">comme</w> <w n="11.5">des</w> <w n="11.6">billes</w>,</l>
					<l n="12" num="3.4"><w n="12.1">Il</w> <w n="12.2">a</w> <w n="12.3">de</w> <w n="12.4">mauvaises</w> <w n="12.5">façons</w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Pourtant</w> <w n="13.2">quelle</w> <w n="13.3">amusante</w> <w n="13.4">chose</w></l>
					<l n="14" num="4.2"><w n="14.1">De</w> <w n="14.2">sauter</w> <w n="14.3">à</w> <w n="14.4">travers</w> <w n="14.5">le</w> <w n="14.6">vent</w> !</l>
					<l n="15" num="4.3"><w n="15.1">On</w> <w n="15.2">a</w> <w n="15.3">la</w> <w n="15.4">figure</w> <w n="15.5">plus</w> <w n="15.6">rose</w></l>
					<l n="16" num="4.4"><w n="16.1">Et</w> <w n="16.2">tous</w> <w n="16.3">ses</w> <w n="16.4">cheveux</w> <w n="16.5">par</w> <w n="16.6">devant</w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">On</w> <w n="17.2">est</w> <w n="17.3">comme</w> <w n="17.4">l</w>’<w n="17.5">herbe</w> <w n="17.6">et</w> <w n="17.7">la</w> <w n="17.8">branche</w>,</l>
					<l n="18" num="5.2"><w n="18.1">Tout</w> <w n="18.2">malmené</w>, <w n="18.3">tout</w> <w n="18.4">secoué</w>.</l>
					<l n="19" num="5.3"><w n="19.1">On</w> <w n="19.2">n</w>’<w n="19.3">a</w> <w n="19.4">pas</w> <w n="19.5">besoin</w> <w n="19.6">de</w> <w n="19.7">jouer</w>.</l>
					<l n="20" num="5.4">‒ <w n="20.1">Oh</w> ! <w n="20.2">Qu</w>’<w n="20.3">il</w> <w n="20.4">fasse</w> <w n="20.5">du</w> <w n="20.6">vent</w> <w n="20.7">dimanche</w> !</l>
				</lg>
			</div></body></text></TEI>