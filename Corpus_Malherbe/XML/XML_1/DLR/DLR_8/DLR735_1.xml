<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÈMES MIGNONS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1223 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_8</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÈMES MIGNONS</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruepoesies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÈMES MIGNONS</title>
								<author>Lucie Delarue-Mardrus</author>
								<imprint>
									<pubPlace>Honfleur</pubPlace>
									<publisher>Éditions de la Lieutenance</publisher>
									<date when="2002">2002</date>
								</imprint>
							</monogr>
							<note>L’édition de 2002 est la source de cette version électronique comme le prouve le choix des poèmes et les erreurs d’édition.</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
				<sourceDesc>
					<biblStruct>
						<monogr>
							<title>POÈMES MIGNONS</title>
							<author>Lucie Delarue-Mardrus</author>
							<imprint>
								<pubPlace>Paris</pubPlace>
								<publisher>LIBRAIRIE GEDALGE</publisher>
								<date when="1929">1929</date>
							</imprint>
						</monogr>
						<note>Édition de référence (numérisée)</note>
					</biblStruct>
				</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1929">1929</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le texte a été mis en conformité avec l’édition originale de 1929.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les retraits introduits automatiquement ont été modifiés conformément à l’édition de référence.</p>
				<p>Les poèmes manquants de l’édition 2002 qui est à l’origine de la version électronique disponible on été ajoutés afin d’être conforme à l’édition de 1929.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLR735">
				<head type="main">Monsieur Polichinelle</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Monsieur</w> <w n="1.2">Polichinelle</w></l>
					<l n="2" num="1.2"><w n="2.1">De</w> <w n="2.2">soie</w> <w n="2.3">ou</w> <w n="2.4">de</w> <w n="2.5">flanelle</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Les</w> <w n="3.2">deux</w> <w n="3.3">bosses</w> <w n="3.4">qu</w>’<w n="3.5">il</w> <w n="3.6">a</w></l>
					<l n="4" num="1.4"><w n="4.1">L</w>’<w n="4.2">empêchent</w> <w n="4.3">d</w>’<w n="4.4">être</w> <w n="4.5">plat</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Ces</w> <w n="5.2">deux</w> <w n="5.3">sacs</w> <w n="5.4">à</w> <w n="5.5">malice</w></l>
					<l n="6" num="2.2"><w n="6.1">Embêtent</w> <w n="6.2">la</w> <w n="6.3">police</w>.</l>
					<l n="7" num="2.3"><w n="7.1">Ce</w> <w n="7.2">qu</w>’<w n="7.3">il</w> <w n="7.4">en</w> <w n="7.5">fait</w>, <w n="7.6">des</w> <w n="7.7">tours</w>,</l>
					<l n="8" num="2.4"><w n="8.1">En</w> <w n="8.2">s</w>’<w n="8.3">esquivant</w> <w n="8.4">toujours</w> !</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Au</w> <w n="9.2">fond</w>, <w n="9.3">c</w>’<w n="9.4">est</w> <w n="9.5">un</w> <w n="9.6">apache</w>.</l>
					<l n="10" num="3.2"><w n="10.1">Pourtant</w>, <w n="10.2">bien</w> <w n="10.3">qu</w>’<w n="10.4">on</w> <w n="10.5">le</w> <w n="10.6">sache</w>,</l>
					<l n="11" num="3.3"><w n="11.1">A</w> <w n="11.2">Guignol</w>, <w n="11.3">on</w> <w n="11.4">se</w> <w n="11.5">tord</w>,</l>
					<l n="12" num="3.4"><w n="12.1">Et</w> <w n="12.2">chacun</w> <w n="12.3">crie</w> : « <w n="12.4">Encor</w> ! »</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Il</w> <w n="13.2">n</w>’<w n="13.3">y</w> <w n="13.4">a</w> <w n="13.5">pas</w> <w n="13.6">à</w> <w n="13.7">dire</w>,</l>
					<l n="14" num="4.2"><w n="14.1">Il</w> <w n="14.2">nous</w> <w n="14.3">a</w> <w n="14.4">fait</w> <w n="14.5">tant</w> <w n="14.6">rire</w>,</l>
					<l n="15" num="4.3"><w n="15.1">Il</w> <w n="15.2">a</w> <w n="15.3">tant</w> <w n="15.4">de</w> <w n="15.5">bagout</w></l>
					<l n="16" num="4.4"><w n="16.1">Qu</w>’<w n="16.2">on</w> <w n="16.3">lui</w> <w n="16.4">pardonne</w> <w n="16.5">tout</w>.</l>
				</lg>
			</div></body></text></TEI>