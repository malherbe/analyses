<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÈMES MIGNONS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1223 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_8</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÈMES MIGNONS</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruepoesies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÈMES MIGNONS</title>
								<author>Lucie Delarue-Mardrus</author>
								<imprint>
									<pubPlace>Honfleur</pubPlace>
									<publisher>Éditions de la Lieutenance</publisher>
									<date when="2002">2002</date>
								</imprint>
							</monogr>
							<note>L’édition de 2002 est la source de cette version électronique comme le prouve le choix des poèmes et les erreurs d’édition.</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
				<sourceDesc>
					<biblStruct>
						<monogr>
							<title>POÈMES MIGNONS</title>
							<author>Lucie Delarue-Mardrus</author>
							<imprint>
								<pubPlace>Paris</pubPlace>
								<publisher>LIBRAIRIE GEDALGE</publisher>
								<date when="1929">1929</date>
							</imprint>
						</monogr>
						<note>Édition de référence (numérisée)</note>
					</biblStruct>
				</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1929">1929</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le texte a été mis en conformité avec l’édition originale de 1929.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les retraits introduits automatiquement ont été modifiés conformément à l’édition de référence.</p>
				<p>Les poèmes manquants de l’édition 2002 qui est à l’origine de la version électronique disponible on été ajoutés afin d’être conforme à l’édition de 1929.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLR743">
				<head type="main">Petit Garçon et petite Fille</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Petit</w> <w n="1.2">garçon</w></l>
					<l n="2" num="1.2"><w n="2.1">Sera</w> <w n="2.2">soldat</w>,</l>
					<l n="3" num="1.3"><w n="3.1">C</w>’<w n="3.2">est</w> <w n="3.3">la</w> <w n="3.4">rançon</w></l>
					<l n="4" num="1.4"><w n="4.1">Que</w> <w n="4.2">veut</w> <w n="4.3">l</w>’<w n="4.4">État</w></l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Petite</w>-<w n="5.2">fille</w></l>
					<l n="6" num="2.2"><w n="6.1">Sera</w> <w n="6.2">maman</w>,</l>
					<l n="7" num="2.3"><w n="7.1">C</w>’<w n="7.2">est</w> <w n="7.3">l</w>’<w n="7.4">élément</w></l>
					<l n="8" num="2.4"><w n="8.1">De</w> <w n="8.2">la</w> <w n="8.3">famille</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Sabre</w> <w n="9.2">de</w> <w n="9.3">bois</w> !</l>
					<l n="10" num="3.2"><w n="10.1">Défend</w> <w n="10.2">les</w> <w n="10.3">lois</w>,</l>
					<l n="11" num="3.3"><w n="11.1">Petit</w> <w n="11.2">garçon</w>,</l>
					<l n="12" num="3.4"><w n="12.1">C</w>’<w n="12.2">est</w> <w n="12.3">la</w> <w n="12.4">rançon</w> !</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Âme</w> <w n="13.2">occupée</w></l>
					<l n="14" num="4.2"><w n="14.1">Par</w> <w n="14.2">ta</w> <w n="14.3">poupée</w>,</l>
					<l n="15" num="4.3"><w n="15.1">Petite</w> <w n="15.2">fille</w></l>
					<l n="16" num="4.4"><w n="16.1">Sois</w> <w n="16.2">la</w> <w n="16.3">famille</w> !</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">Futur</w> <w n="17.2">soldat</w>,</l>
					<l n="18" num="5.2"><w n="18.1">Songe</w> <w n="18.2">à</w> <w n="18.3">l</w>’<w n="18.4">État</w></l>
					<l n="19" num="5.3"><w n="19.1">Maman</w> <w n="19.2">prochaine</w>,</l>
					<l n="20" num="5.4"><w n="20.1">Songe</w> <w n="20.2">à</w> <w n="20.3">ta</w> <w n="20.4">chaîne</w> !</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1">‒ <w n="21.1">Que</w> <w n="21.2">ma</w> <w n="21.3">chanson</w></l>
					<l n="22" num="6.2"><w n="22.1">Reste</w> <w n="22.2">en</w> <w n="22.3">votre</w> <w n="22.4">âme</w>,</l>
					<l n="23" num="6.3"><w n="23.1">Petit</w> <w n="23.2">garçon</w>,</l>
					<l n="24" num="6.4"><w n="24.1">Petite</w> <w n="24.2">femme</w>.</l>
				</lg>
			</div></body></text></TEI>