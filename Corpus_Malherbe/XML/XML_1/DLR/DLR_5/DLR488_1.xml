<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">PAR VENTS ET MARÉES</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>Delarue-Mardrus</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, OCR, Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1987 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2020</date>
				<idno type="local">DLR_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title type="main">PAR VENTS ET MARÉES</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>BIBLIOTHÈQUE-CHARPENTIER</publisher>
							<date when="1910">1910</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1910">1910</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les retraits introduits automatiquement ont été modifiés conformément à l’édition de référence.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-09-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-09-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LA MER</head><div type="poem" key="DLR488">
					<head type="main">POÈME DU HARENG</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">L</w>’<w n="1.2">hiver</w> <w n="1.3">est</w> <w n="1.4">revenu</w>, <w n="1.5">l</w>’<w n="1.6">horizon</w> <w n="1.7">est</w> <w n="1.8">plus</w> <w n="1.9">grand</w>,</l>
						<l n="2" num="1.2"><w n="2.1">La</w> <w n="2.2">tempête</w> <w n="2.3">nous</w> <w n="2.4">mord</w> <w n="2.5">de</w> <w n="2.6">ses</w> <w n="2.7">gueules</w> <w n="2.8">ouvertes</w>.</l>
						<l n="3" num="1.3"><space unit="char" quantity="8"></space><w n="3.1">Mais</w>, <w n="3.2">à</w> <w n="3.3">travers</w> <w n="3.4">les</w> <w n="3.5">vagues</w> <w n="3.6">vertes</w>,</l>
						<l n="4" num="1.4"><space unit="char" quantity="8"></space><w n="4.1">Par</w> <w n="4.2">milliers</w> <w n="4.3">passe</w> <w n="4.4">le</w> <w n="4.5">hareng</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Le</w> <w n="5.2">hareng</w> <w n="5.3">passe</w> ! <w n="5.4">Au</w> <w n="5.5">large</w> ! <w n="5.6">Allons</w> <w n="5.7">à</w> <w n="5.8">sa</w> <w n="5.9">conquête</w> !</l>
						<l n="6" num="2.2"><w n="6.1">Il</w> <w n="6.2">n</w>’<w n="6.3">est</w> <w n="6.4">pas</w> <w n="6.5">trop</w> <w n="6.6">des</w> <w n="6.7">nuits</w>, <w n="6.8">il</w> <w n="6.9">n</w>’<w n="6.10">est</w> <w n="6.11">pas</w> <w n="6.12">trop</w> <w n="6.13">dès</w> <w n="6.14">jours</w> !</l>
						<l n="7" num="2.3"><space unit="char" quantity="8"></space><w n="7.1">Nous</w> <w n="7.2">avons</w> <w n="7.3">le</w> <w n="7.4">cœur</w> <w n="7.5">plus</w> <w n="7.6">en</w> <w n="7.7">fête</w></l>
						<l n="8" num="2.4"><space unit="char" quantity="8"></space><w n="8.1">Que</w> <w n="8.2">ceux</w> <w n="8.3">qui</w> <w n="8.4">vont</w> <w n="8.5">vers</w> <w n="8.6">leurs</w> <w n="8.7">amours</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Oh</w> ! <w n="9.2">quand</w> <w n="9.3">la</w> <w n="9.4">voile</w> <w n="9.5">pend</w>, <w n="9.6">couleur</w> <w n="9.7">de</w> <w n="9.8">feuille</w> <w n="9.9">morte</w>,</l>
						<l n="10" num="3.2"><w n="10.1">Quand</w> <w n="10.2">la</w> <w n="10.3">barque</w>, <w n="10.4">le</w> <w n="10.5">long</w> <w n="10.6">du</w> <w n="10.7">port</w> <w n="10.8">noir</w> <w n="10.9">et</w> <w n="10.10">mouillé</w>,</l>
						<l n="11" num="3.3"><space unit="char" quantity="8"></space><w n="11.1">Sous</w> <w n="11.2">le</w> <w n="11.3">hareng</w> <w n="11.4">frais</w> <w n="11.5">quelle</w> <w n="11.6">porte</w></l>
						<l n="12" num="3.4"><space unit="char" quantity="8"></space><w n="12.1">Luit</w> <w n="12.2">comme</w> <w n="12.3">l</w>’<w n="12.4">argent</w> <w n="12.5">monnayé</w> !</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">C</w>’<w n="13.2">est</w> <w n="13.3">alors</w> <w n="13.4">que</w> <w n="13.5">le</w> <w n="13.6">quai</w> <w n="13.7">jusqu</w>’<w n="13.8">à</w> <w n="13.9">la</w> <w n="13.10">nuit</w> <w n="13.11">travaille</w></l>
						<l n="14" num="4.2"><w n="14.1">Au</w> <w n="14.2">bruit</w> <w n="14.3">sourd</w> <w n="14.4">des</w> <w n="14.5">marteaux</w> <w n="14.6">sur</w> <w n="14.7">les</w> <w n="14.8">caisses</w> <w n="14.9">de</w> <w n="14.10">bois</w></l>
						<l n="15" num="4.3"><space unit="char" quantity="8"></space><w n="15.1">Parmi</w> <w n="15.2">le</w> <w n="15.3">grouillement</w> <w n="15.4">des</w> <w n="15.5">voix</w></l>
						<l n="16" num="4.4"><space unit="char" quantity="8"></space><w n="16.1">Et</w> <w n="16.2">des</w> <w n="16.3">pieds</w> <w n="16.4">glissant</w> <w n="16.5">sur</w> <w n="16.6">l</w>’<w n="16.7">écaille</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Les</w> <w n="17.2">commères</w> <w n="17.3">d</w>’<w n="17.4">Honfleur</w> <w n="17.5">qui</w> <w n="17.6">lavent</w> <w n="17.7">le</w> <w n="17.8">poisson</w></l>
						<l n="18" num="5.2"><w n="18.1">Penchant</w> <w n="18.2">vers</w> <w n="18.3">les</w> <w n="18.4">baquets</w> <w n="18.5">leurs</w> <w n="18.6">larges</w> <w n="18.7">gorges</w> <w n="18.8">mûres</w></l>
						<l n="19" num="5.3"><space unit="char" quantity="8"></space><w n="19.1">Ont</w> <w n="19.2">à</w> <w n="19.3">chaque</w> <w n="19.4">ongle</w> <w n="19.5">un</w> <w n="19.6">fin</w> <w n="19.7">glaçon</w></l>
						<l n="20" num="5.4"><space unit="char" quantity="8"></space><w n="20.1">Et</w> <w n="20.2">la</w> <w n="20.3">bouche</w> <w n="20.4">pleine</w> <w n="20.5">d</w>’<w n="20.6">injures</w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Il</w> <w n="21.2">faut</w> <w n="21.3">bien</w> <w n="21.4">avouer</w> <w n="21.5">qu</w>’<w n="21.6">elles</w> <w n="21.7">ont</w> <w n="21.8">un</w> <w n="21.9">peu</w> <w n="21.10">bu</w></l>
						<l n="22" num="6.2"><w n="22.1">Ces</w> <w n="22.2">poissardes</w> <w n="22.3">du</w> <w n="22.4">quai</w> <w n="22.5">courageuses</w> <w n="22.6">et</w> <w n="22.7">rauques</w>.</l>
						<l n="23" num="6.3"><space unit="char" quantity="8"></space><w n="23.1">Mais</w> <w n="23.2">c</w>’<w n="23.3">est</w> <w n="23.4">qu</w>’<w n="23.5">elles</w> <w n="23.6">en</w> <w n="23.7">ont</w> <w n="23.8">tant</w> <w n="23.9">vu</w></l>
						<l n="24" num="6.4"><space unit="char" quantity="8"></space><w n="24.1">Depuis</w> <w n="24.2">que</w> <w n="24.3">s</w>’<w n="24.4">ouvrent</w> <w n="24.5">leurs</w> <w n="24.6">yeux</w> <w n="24.7">glauques</w> !</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">Quand</w> <w n="25.2">la</w> <w n="25.3">mer</w> <w n="25.4">nous</w> <w n="25.5">roulait</w> <w n="25.6">à</w> <w n="25.7">travers</w> <w n="25.8">les</w> <w n="25.9">hasards</w>,</l>
						<l n="26" num="7.2"><w n="26.1">Nous</w> <w n="26.2">les</w> <w n="26.3">mâles</w> <w n="26.4">brutaux</w>, <w n="26.5">abreuvés</w> <w n="26.6">de</w> <w n="26.7">rogommes</w>,</l>
						<l n="27" num="7.3"><space unit="char" quantity="8"></space><w n="27.1">Nos</w> <w n="27.2">campagnes</w> <w n="27.3">ont</w> <w n="27.4">fait</w> <w n="27.5">des</w> <w n="27.6">hommes</w></l>
						<l n="28" num="7.4"><space unit="char" quantity="8"></space><w n="28.1">Et</w> <w n="28.2">peuplé</w> <w n="28.3">le</w> <w n="28.4">pays</w> <w n="28.5">de</w> <w n="28.6">gars</w>.</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1"><w n="29.1">Au</w> <w n="29.2">travail</w> <w n="29.3">donc</w>, <w n="29.4">nous</w> <w n="29.5">tous</w>, <w n="29.6">les</w> <w n="29.7">gueux</w> <w n="29.8">avec</w> <w n="29.9">les</w> <w n="29.10">gueuses</w></l>
						<l n="30" num="8.2"><w n="30.1">Tous</w> <w n="30.2">les</w> <w n="30.3">chaluts</w>, <w n="30.4">par</w> <w n="30.5">le</w> <w n="30.6">chenal</w>, <w n="30.7">rentrent</w> <w n="30.8">en</w> <w n="30.9">rang</w>,</l>
						<l n="31" num="8.3"><space unit="char" quantity="8"></space><w n="31.1">Soyons</w> <w n="31.2">gais</w>, <w n="31.3">puisque</w> <w n="31.4">le</w> <w n="31.5">hareng</w></l>
						<l n="32" num="8.4"><space unit="char" quantity="8"></space><w n="32.1">Fait</w> <w n="32.2">nos</w> <w n="32.3">pèches</w> <w n="32.4">si</w> <w n="32.5">poissonneuses</w>.</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1"><w n="33.1">La</w> <w n="33.2">vie</w> <w n="33.3">est</w> <w n="33.4">rude</w> <w n="33.5">à</w> <w n="33.6">vivre</w> <w n="33.7">au</w> <w n="33.8">vent</w> <w n="33.9">cassant</w> <w n="33.10">et</w> <w n="33.11">clair</w>,</l>
						<l n="34" num="9.2"><w n="34.1">Oui</w> ! <w n="34.2">mais</w> <w n="34.3">chacun</w> <w n="34.4">de</w> <w n="34.5">nous</w>, <w n="34.6">la</w> <w n="34.7">poche</w>-<w n="34.8">presque</w> <w n="34.9">pleine</w>.</l>
						<l n="35" num="9.3"><space unit="char" quantity="8"></space><w n="35.1">Peut</w> <w n="35.2">avoir</w> <w n="35.3">un</w> <w n="35.4">peu</w> <w n="35.5">moins</w> <w n="35.6">de</w> <w n="35.7">haine</w></l>
						<l n="36" num="9.4"><space unit="char" quantity="8"></space><w n="36.1">Pour</w> <w n="36.2">l</w>’<w n="36.3">ennemie</w> <w n="36.4">au</w> <w n="36.5">loin</w>, <w n="36.6">la</w> <w n="36.7">mer</w> !</l>
					</lg>
				</div></body></text></TEI>