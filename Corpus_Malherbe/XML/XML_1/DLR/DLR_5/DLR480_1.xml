<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">PAR VENTS ET MARÉES</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>Delarue-Mardrus</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, OCR, Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1987 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2020</date>
				<idno type="local">DLR_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title type="main">PAR VENTS ET MARÉES</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>BIBLIOTHÈQUE-CHARPENTIER</publisher>
							<date when="1910">1910</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1910">1910</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les retraits introduits automatiquement ont été modifiés conformément à l’édition de référence.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-09-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-09-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLR480">
				<lg n="1">
					<l n="1" num="1.1"><hi rend="ital"><w n="1.1">O</w> <w n="1.2">mer</w> ! <w n="1.3">Fond</w> <w n="1.4">de</w> <w n="1.5">ténèbre</w> <w n="1.6">et</w> <w n="1.7">surface</w> <w n="1.8">qui</w> <w n="1.9">brilles</w>,</hi></l>
					<l n="2" num="1.2"><hi rend="ital"><w n="2.1">Toi</w> <w n="2.2">qu</w>’<w n="2.3">on</w> <w n="2.4">ne</w> <w n="2.5">peut</w> <w n="2.6">marquer</w> <w n="2.7">d</w>’<w n="2.8">un</w> <w n="2.9">durable</w> <w n="2.10">sillon</w>,</hi></l>
					<l n="3" num="1.3"><hi rend="ital"><w n="3.1">Je</w> <w n="3.2">viens</w> <w n="3.3">ici</w>, <w n="3.4">vindicative</w> <w n="3.5">entre</w> <w n="3.6">tes</w> <w n="3.7">filles</w>,</hi></l>
					<l n="4" num="1.4"><hi rend="ital"><w n="4.1">Adorer</w> <w n="4.2">ton</w> <w n="4.3">caprice</w> <w n="4.4">et</w> <w n="4.5">ta</w> <w n="4.6">rébellion</w>.</hi></l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><hi rend="ital"><w n="5.1">Mer</w> <w n="5.2">changeante</w> <w n="5.3">à</w> <w n="5.4">jamais</w>, <w n="5.5">pourtant</w> <w n="5.6">toujours</w> <w n="5.7">toi</w>-<w n="5.8">même</w></hi></l>
					<l n="6" num="2.2"><hi rend="ital"><w n="6.1">Depuis</w> <w n="6.2">les</w> <w n="6.3">premiers</w> <w n="6.4">jours</w> <w n="6.5">du</w> <w n="6.6">temps</w> <w n="6.7">illimité</w>,</hi></l>
					<l n="7" num="2.3"><hi rend="ital"><w n="7.1">Mer</w> <w n="7.2">sans</w> <w n="7.3">maître</w>, <w n="7.4">éternel</w> <w n="7.5">conseil</w> <w n="7.6">de</w> <w n="7.7">liberté</w>,</hi></l>
					<l n="8" num="2.4"><hi rend="ital"><w n="8.1">Permets</w> <w n="8.2">qu</w>’<w n="8.3">un</w>-<w n="8.4">cœur</w> <w n="8.5">humain</w> <w n="8.6">s</w>’<w n="8.7">enthousiasme</w> <w n="8.8">et</w> <w n="8.9">t</w>’<w n="8.10">aime</w>.</hi></l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><hi rend="ital"><w n="9.1">Mon</w> <w n="9.2">désir</w> <w n="9.3">ne</w> <w n="9.4">peut</w> <w n="9.5">pas</w> <w n="9.6">me</w> <w n="9.7">creuser</w> <w n="9.8">un</w> <w n="9.9">chemin</w></hi></l>
					<l n="10" num="3.2"><hi rend="ital"><w n="10.1">Jusques</w> <w n="10.2">au</w> <w n="10.3">plus</w> <w n="10.4">caché</w> <w n="10.5">de</w> <w n="10.6">ton</w> <w n="10.7">secret</w> <w n="10.8">royaume</w>,</hi></l>
					<l n="11" num="3.3"><hi rend="ital"><w n="11.1">Mais</w> <w n="11.2">je</w> <w n="11.3">puis</w>, <w n="11.4">me</w> <w n="11.5">penchant</w>, <w n="11.6">te</w> <w n="11.7">prendre</w> <w n="11.8">dans</w> <w n="11.9">ma</w> <w n="11.10">paume</w></hi></l>
					<l n="12" num="3.4"><hi rend="ital"><w n="12.1">Et</w> <w n="12.2">boire</w> <w n="12.3">Vin</w> <w n="12.4">fini</w> <w n="12.5">dans</w> <w n="12.6">le</w> <w n="12.7">fond</w> <w n="12.8">de</w> <w n="12.9">ma</w> <w n="12.10">main</w>.</hi></l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><hi rend="ital"><w n="13.1">Mon</w> <w n="13.2">désir</w> <w n="13.3">ne</w> <w n="13.4">peut</w> <w n="13.5">pas</w> <w n="13.6">me</w> <w n="13.7">creuser</w> <w n="13.8">un</w> <w n="13.9">chemin</w></hi></l>
					<l n="14" num="4.2"><hi rend="ital"><w n="14.1">Jusque</w> <w n="14.2">s</w> <w n="14.3">au</w> <w n="14.4">plus</w> <w n="14.5">caché</w> <w n="14.6">de</w> <w n="14.7">ton</w> <w n="14.8">secret</w> <w n="14.9">royaume</w>,</hi></l>
					<l n="15" num="4.3"><hi rend="ital"><w n="15.1">Mais</w> <w n="15.2">je</w> <w n="15.3">puis</w>, <w n="15.4">me</w> <w n="15.5">penchant</w>, <w n="15.6">te</w> <w n="15.7">prendre</w> <w n="15.8">dans</w> <w n="15.9">ma</w> <w n="15.10">paume</w></hi></l>
					<l n="16" num="4.4"><hi rend="ital"><w n="16.1">Et</w> <w n="16.2">boire</w> <w n="16.3">Vin</w> <w n="16.4">fini</w> <w n="16.5">dans</w> <w n="16.6">le</w> <w n="16.7">fond</w> <w n="16.8">de</w> <w n="16.9">ma</w> <w n="16.10">main</w>.</hi></l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><hi rend="ital"><w n="17.1">Ce</w> <w n="17.2">sel</w> <w n="17.3">contre</w> <w n="17.4">mes</w> <w n="17.5">dents</w>, <w n="17.6">c</w>’<w n="17.7">est</w> <w n="17.8">celui</w> <w n="17.9">des</w> <w n="17.10">eaux</w> <w n="17.11">pâles</w></hi></l>
					<l n="18" num="5.2"><hi rend="ital"><w n="18.1">Qui</w> <w n="18.2">vont</w> <w n="18.3">s</w>’<w n="18.4">assombrissant</w> <w n="18.5">jusqu</w>’<w n="18.6">au</w> <w n="18.7">domaine</w> <w n="18.8">obscur</w></hi></l>
					<l n="19" num="5.3"><hi rend="ital"><w n="19.1">Où</w> <w n="19.2">tu</w> <w n="19.3">n</w>’<w n="19.4">es</w> <w n="19.5">plus</w>, <w n="19.6">miroir</w> <w n="19.7">du</w> <w n="19.8">ciel</w>, <w n="19.9">gouffre</w> <w n="19.10">d</w>’<w n="19.11">azur</w>,</hi></l>
					<l n="20" num="5.4"><hi rend="ital"><w n="20.1">Qu</w>’<w n="20.2">un</w> <w n="20.3">flot</w> <w n="20.4">de</w> <w n="20.5">nuit</w> <w n="20.6">hanté</w> <w n="20.7">par</w> <w n="20.8">de</w> <w n="20.9">lumineux</w> <w n="20.10">squales</w></hi></l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><hi rend="ital"><w n="21.1">Nous</w> <w n="21.2">contenterons</w>-<w n="21.3">nous</w> <w n="21.4">de</w> <w n="21.5">l</w>’<w n="21.6">esprit</w> <w n="21.7">inconnu</w></hi></l>
					<l n="22" num="6.2"><hi rend="ital"><w n="22.1">Qui</w> <w n="22.2">mène</w> <w n="22.3">ta</w> <w n="22.4">surface</w> <w n="22.5">à</w> <w n="22.6">jamais</w> <choice reason="analysis" type="false_verse" hand="RR" resp="RR"><sic>flucteuse</sic><corr source="none"><w n="22.7">fluctueuse</w></corr></choice> ?</hi></l>
					<l n="23" num="6.3"><hi rend="ital"><w n="23.1">Nous</w> <w n="23.2">contenterons</w>-<w n="23.3">nous</w> <w n="23.4">que</w> <w n="23.5">ta</w> <w n="23.6">vague</w> <w n="23.7">trop</w> <w n="23.8">creuse</w></hi></l>
					<l n="24" num="6.4"><hi rend="ital"><w n="24.1">Soit</w> <w n="24.2">la</w> <w n="24.3">place</w> <w n="24.4">d</w>’<w n="24.5">un</w> <w n="24.6">corps</w> <w n="24.7">énigmatique</w> <w n="24.8">et</w> <w n="24.9">nu</w> ?</hi></l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><hi rend="ital"><w n="25.1">Laisse</w>-<w n="25.2">nous</w> <w n="25.3">oublier</w> <w n="25.4">que</w> <w n="25.5">de</w> <w n="25.6">grands</w> <w n="25.7">paysages</w></hi></l>
					<l n="26" num="7.2"><hi rend="ital"><w n="26.1">Nocturnes</w> <w n="26.2">sont</w> <w n="26.3">en</w> <w n="26.4">toi</w> <w n="26.5">pour</w> <w n="26.6">toujours</w> <w n="26.7">submergés</w>.</hi></l>
					<l n="27" num="7.3"><hi rend="ital"><w n="27.1">O</w> <w n="27.2">mer</w> ! <w n="27.3">Jette</w> <w n="27.4">à</w> <w n="27.5">nos</w> <w n="27.6">pieds</w> <w n="27.7">tes</w> <w n="27.8">plus</w> <w n="27.9">clairs</w> <w n="27.10">coquillages</w>,</hi></l>
					<l n="28" num="7.4"><hi rend="ital"><w n="28.1">Que</w> <w n="28.2">nous</w> <w n="28.3">ne</w> <w n="28.4">songions</w> <w n="28.5">plus</w> <w n="28.6">à</w> <w n="28.7">tes</w> <w n="28.8">sombres</w> <w n="28.9">vergers</w>.</hi></l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><hi rend="ital"><w n="29.1">Te</w> <w n="29.2">voici</w> <w n="29.3">dans</w> <w n="29.4">ta</w> <w n="29.5">grâce</w> <w n="29.6">et</w> <w n="29.7">ta</w> <w n="29.8">géométrie</w>,</hi></l>
					<l n="30" num="8.2"><hi rend="ital"><w n="30.1">Dans</w> <w n="30.2">ta</w> <w n="30.3">furie</w> <w n="30.4">horrible</w> <w n="30.5">et</w> <w n="30.6">ton</w> <w n="30.7">auguste</w> <w n="30.8">paix</w>.</hi></l>
					<l n="31" num="8.3"><hi rend="ital"><w n="31.1">Errons</w> <w n="31.2">parmi</w> <w n="31.3">ton</w> <w n="31.4">sable</w>, <w n="31.5">ivres</w> <w n="31.6">de</w> <w n="31.7">tes</w> <w n="31.8">aspects</w>,</hi></l>
					<l n="32" num="8.4"><hi rend="ital"><w n="32.1">Sans</w> <w n="32.2">plus</w> <w n="32.3">pleurer</w> <w n="32.4">ta</w> <w n="32.5">profondeur</w>, <w n="32.6">cette</w> <w n="32.7">patrie</w>.</hi></l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1"><hi rend="ital"><w n="33.1">Entre</w> <w n="33.2">des</w> <w n="33.3">cyprès</w> <w n="33.4">noirs</w> <w n="33.5">et</w>-<w n="33.6">des</w> <w n="33.7">orangers</w> <w n="33.8">blancs</w>,</hi></l>
					<l n="34" num="9.2"><hi rend="ital"><w n="34.1">Je</w> <w n="34.2">t</w>’<w n="34.3">ai</w> <w n="34.4">connue</w> <w n="34.5">étale</w> <w n="34.6">et</w> <w n="34.7">méditerranée</w>,</hi></l>
					<l n="35" num="9.3"><hi rend="ital"><w n="35.1">Et</w> <w n="35.2">tu</w> <w n="35.3">te</w> <w n="35.4">souvenais</w> <w n="35.5">dans</w> <w n="35.6">tes</w> <w n="35.7">calmes</w> <w n="35.8">élans</w></hi></l>
					<l n="36" num="9.4"><hi rend="ital"><w n="36.1">D</w>’<w n="36.2">avoir</w> <w n="36.3">baigné</w> <w n="36.4">jadis</w> <w n="36.5">l</w>’<w n="36.6">hellade</w>, <w n="36.7">cette</w> <w n="36.8">aînée</w>.</hi></l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1"><hi rend="ital"><w n="37.1">A</w> <w n="37.2">travers</w> <w n="37.3">des</w> <w n="37.4">buissons</w> <w n="37.5">de</w> <w n="37.6">ronces</w> <w n="37.7">et</w> <w n="37.8">de</w> <w n="37.9">houx</w>,</hi></l>
					<l n="38" num="10.2"><hi rend="ital"><w n="38.1">Au</w> <w n="38.2">rythme</w> <w n="38.3">dur</w> <w n="38.4">des</w> <w n="38.5">flux</w> <w n="38.6">et</w> <w n="38.7">reflux</w> <w n="38.8">retractiles</w>,</hi></l>
					<l n="39" num="10.3"><hi rend="ital"><w n="39.1">Je</w> <w n="39.2">t</w>’<w n="39.3">ai</w> <w n="39.4">connue</w> <w n="39.5">au</w> <w n="39.6">nord</w>, <w n="39.7">comme</w> <w n="39.8">au</w> <w n="39.9">temps</w> <w n="39.10">où</w> <w n="39.11">les</w> <w n="39.12">Roux</w>,</hi></l>
					<l n="40" num="10.4"><hi rend="ital"><w n="40.1">Les</w> <w n="40.2">grands</w> <w n="40.3">iarles</w>, <w n="40.4">quittaient</w> <w n="40.5">leurs</w> <w n="40.6">neigeuses</w> <w n="40.7">presqu</w>’<w n="40.8">îles</w>.</hi></l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1"><hi rend="ital"><w n="41.1">Je</w> <w n="41.2">t</w>’<w n="41.3">ai</w> <w n="41.4">connue</w> <w n="41.5">ici</w>, <w n="41.6">là</w>-<w n="41.7">bas</w>, <w n="41.8">de</w> <w n="41.9">loin</w>, <w n="41.10">de</w> <w n="41.11">près</w>,</hi></l>
					<l n="42" num="11.2"><hi rend="ital"><w n="42.1">J</w>’<w n="42.2">ai</w> <w n="42.3">revêtu</w> <w n="42.4">le</w> <w n="42.5">voile</w> <w n="42.6">ardent</w> <w n="42.7">de</w> <w n="42.8">ta</w> <w n="42.9">nuance</w>.</hi></l>
					<l n="43" num="11.3"><hi rend="ital"><w n="43.1">C</w>’<w n="43.2">est</w> <w n="43.3">encor</w> <w n="43.4">toi</w> <w n="43.5">que</w> <w n="43.6">j</w>’<w n="43.7">aime</w> <w n="43.8">et</w> <w n="43.9">travers</w> <w n="43.10">les</w> <w n="43.11">forêts</w>,</hi></l>
					<l n="44" num="11.4"><hi rend="ital"><w n="44.1">Quand</w> <w n="44.2">l</w>’<w n="44.3">ouragan</w> <w n="44.4">reprend</w> <w n="44.5">la</w> <w n="44.6">voix</w> <w n="44.7">de</w> <w n="44.8">ta</w> <w n="44.9">démence</w>.</hi></l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1"><hi rend="ital"><w n="45.1">Dans</w> <w n="45.2">tout</w> <w n="45.3">ce</w> <w n="45.4">qui</w> <w n="45.5">m</w> <w n="45.6">émeut</w> <w n="45.7">de</w> <w n="45.8">faible</w> <w n="45.9">ou</w> <w n="45.10">de</w> <w n="45.11">puissant</w>,</hi></l>
					<l n="46" num="12.2"><hi rend="ital"><w n="46.1">Je</w> <w n="46.2">me</w> <w n="46.3">souviens</w> <w n="46.4">de</w> <w n="46.5">toi</w> <w n="46.6">qui</w> <w n="46.7">hantes</w> <w n="46.8">ma</w> <w n="46.9">pensée</w>.</hi></l>
					<l n="47" num="12.3"><hi rend="ital"><w n="47.1">Ta</w> <w n="47.2">mémoire</w> <w n="47.3">est</w> <w n="47.4">en</w> <w n="47.5">moi</w> <w n="47.6">pour</w> <w n="47.7">toujours</w> <w n="47.8">enfoncée</w>,</hi></l>
					<l n="48" num="12.4"><hi rend="ital"><w n="48.1">Ton</w> <w n="48.2">rythme</w> <w n="48.3">multiforme</w> <w n="48.4">est</w> <w n="48.5">resté</w> <w n="48.6">dans</w> <w n="48.7">mon</w> <w n="48.8">sang</w>.</hi></l>
				</lg>
				<lg n="13">
					<l n="49" num="13.1"><hi rend="ital"><w n="49.1">Pour</w> <w n="49.2">moi</w>, <w n="49.3">la</w> <w n="49.4">vie</w> <w n="49.5">a</w> <w n="49.6">ta</w> <w n="49.7">douceur</w> <w n="49.8">ou</w> <w n="49.9">ta</w> <w n="49.10">tempête</w>,</hi></l>
					<l n="50" num="13.2"><hi rend="ital"><w n="50.1">Ton</w> <w n="50.2">royaume</w> <w n="50.3">secret</w>, <w n="50.4">ton</w> <w n="50.5">flux</w> <w n="50.6">et</w> <w n="50.7">ton</w> <w n="50.8">reflux</w>.</hi></l>
					<l n="51" num="13.3"><hi rend="ital"><w n="51.1">Parfois</w> <w n="51.2">mon</w> <w n="51.3">désespoir</w> <w n="51.4">est</w> <w n="51.5">une</w> <w n="51.6">grande</w> <w n="51.7">fête</w></hi></l>
					<l n="52" num="13.4"><hi rend="ital"><w n="52.1">Où</w> <w n="52.2">tournent</w> <w n="52.3">tes</w> <w n="52.4">oiseaux</w> <w n="52.5">marins</w> <w n="52.6">aux</w> <w n="52.7">cris</w> <w n="52.8">aigus</w>.</hi></l>
				</lg>
				<lg n="14">
					<l n="53" num="14.1"><hi rend="ital"><w n="53.1">Je</w> <w n="53.2">t</w>’<w n="53.3">atteste</w> <w n="53.4">en</w> <w n="53.5">mon</w> <w n="53.6">cœur</w>, <w n="53.7">possession</w> <w n="53.8">immense</w> !</hi></l>
					<l n="54" num="14.2"><hi rend="ital"><w n="54.1">Toi</w> <w n="54.2">seule</w> <w n="54.3">je</w> <w n="54.4">te</w> <w n="54.5">peux</w> <w n="54.6">reconnaître</w> <w n="54.7">et</w> <w n="54.8">chérir</w>,</hi></l>
					<l n="55" num="14.3"><hi rend="ital"><w n="55.1">Mer</w>, <w n="55.2">éternelle</w> <w n="55.3">joie</w>, <w n="55.4">éternelle</w> <w n="55.5">souffrance</w>,</hi></l>
					<l n="56" num="14.4"><hi rend="ital"><w n="56.1">Mer</w>, <w n="56.2">dont</w> <w n="56.3">je</w> <w n="56.4">ne</w> <w n="56.5">peux</w> <w n="56.6">pas</w> <w n="56.7">et</w> <w n="56.8">ne</w> <w n="56.9">veux</w> <w n="56.10">pas</w> <w n="56.11">guérir</w> !</hi></l>
				</lg>
			</div></body></text></TEI>