<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">PAR VENTS ET MARÉES</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>Delarue-Mardrus</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, OCR, Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1987 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2020</date>
				<idno type="local">DLR_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title type="main">PAR VENTS ET MARÉES</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>BIBLIOTHÈQUE-CHARPENTIER</publisher>
							<date when="1910">1910</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1910">1910</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les retraits introduits automatiquement ont été modifiés conformément à l’édition de référence.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-09-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-09-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CHEZ NOUS</head><div type="poem" key="DLR552">
					<head type="main">DÉFI</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Que</w> <w n="1.2">la</w> <w n="1.3">haine</w> <w n="1.4">anonyme</w> <w n="1.5">et</w> <w n="1.6">que</w> <w n="1.7">l</w>’<w n="1.8">envie</w> <w n="1.9">esclave</w></l>
						<l n="2" num="1.2"><w n="2.1">Environnent</w> <w n="2.2">mon</w> <w n="2.3">cœur</w> <w n="2.4">de</w> <w n="2.5">leurs</w> <w n="2.6">traits</w> <w n="2.7">incessants</w> !</l>
						<l n="3" num="1.3"><w n="3.1">Je</w> <w n="3.2">regarde</w> <w n="3.3">à</w> <w n="3.4">mes</w> <w n="3.5">pieds</w> <w n="3.6">éclaboussés</w> <w n="3.7">de</w> <w n="3.8">bave</w></l>
						<l n="4" num="1.4"><space unit="char" quantity="8"></space><w n="4.1">La</w> <w n="4.2">révolte</w> <w n="4.3">des</w> <w n="4.4">impuissants</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Votre</w> <w n="5.2">encre</w> <w n="5.3">empoisonnée</w> <w n="5.4">est</w> <w n="5.5">vaine</w>. <w n="5.6">Votre</w> <w n="5.7">proie</w></l>
						<l n="6" num="2.2"><w n="6.1">Ce</w> <w n="6.2">n</w>’<w n="6.3">est</w> <w n="6.4">pas</w> <w n="6.5">moi</w> ! <w n="6.6">Qui</w> <w n="6.7">donc</w> <w n="6.8">va</w> <w n="6.9">me</w> <w n="6.10">tordre</w> <w n="6.11">le</w> <w n="6.12">col</w> ?</l>
						<l n="7" num="2.3"><w n="7.1">Mon</w> <w n="7.2">visage</w> <w n="7.3">se</w> <w n="7.4">rit</w> <w n="7.5">de</w> <w n="7.6">votre</w> <w n="7.7">vitriol</w>,</l>
						<l n="8" num="2.4"><space unit="char" quantity="8"></space><w n="8.1">Vous</w> <w n="8.2">n</w>’<w n="8.3">atteindrez</w> <w n="8.4">jamais</w> <w n="8.5">ma</w> <w n="8.6">joie</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Contre</w> <w n="9.2">mes</w> <w n="9.3">vrais</w> <w n="9.4">trésors</w> <w n="9.5">nul</w> <w n="9.6">de</w> <w n="9.7">vous</w> <w n="9.8">ne</w> <w n="9.9">peut</w> <w n="9.10">rien</w>.</l>
						<l n="10" num="3.2"><w n="10.1">Ma</w> <w n="10.2">pensée</w> <w n="10.3">est</w> <w n="10.4">à</w> <w n="10.5">moi</w> <w n="10.6">comme</w> <w n="10.7">est</w> <w n="10.8">à</w> <w n="10.9">moi</w> <w n="10.10">mon</w> <w n="10.11">homme</w>,</l>
						<l n="11" num="3.3"><w n="11.1">Comme</w> <w n="11.2">est</w> <w n="11.3">à</w> <w n="11.4">moi</w> <w n="11.5">mon</w> <w n="11.6">temps</w>, <w n="11.7">comme</w> <w n="11.8">est</w> <w n="11.9">à</w> <w n="11.10">moi</w> <w n="11.11">mon</w> <w n="11.12">bien</w></l>
						<l n="12" num="3.4"><space unit="char" quantity="8"></space><w n="12.1">Additionnez</w> ! <w n="12.2">Je</w> <w n="12.3">fais</w> <w n="12.4">la</w> <w n="12.5">somme</w> !</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Non</w> ! <w n="13.2">Vous</w> <w n="13.3">n</w>’<w n="13.4">entrerez</w> <w n="13.5">pas</w> <w n="13.6">chez</w> <w n="13.7">moi</w>, <w n="13.8">dans</w> <w n="13.9">ma</w> <w n="13.10">maison</w>,</l>
						<l n="14" num="4.2"><w n="14.1">Vous</w> <w n="14.2">ne</w> <w n="14.3">briserez</w> <w n="14.4">pas</w> <w n="14.5">mes</w> <w n="14.6">vitres</w> <w n="14.7">sur</w> <w n="14.8">ma</w> <w n="14.9">Seine</w>,</l>
						<l n="15" num="4.3"><w n="15.1">Vous</w> <w n="15.2">ne</w> <w n="15.3">m</w>’<w n="15.4">ôterez</w> <w n="15.5">pas</w> <w n="15.6">Paris</w>. <w n="15.7">Non</w>, <w n="15.8">votre</w> <w n="15.9">haine</w></l>
						<l n="16" num="4.4"><space unit="char" quantity="8"></space><w n="16.1">Ne</w> <w n="16.2">barre</w> <w n="16.3">pas</w> <w n="16.4">mon</w> <w n="16.5">horizon</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Vous</w> <w n="17.2">n</w>’<w n="17.3">envahirez</w> <w n="17.4">pas</w> <w n="17.5">ma</w> <w n="17.6">campagne</w> <w n="17.7">natale</w>,</l>
						<l n="18" num="5.2"><w n="18.1">La</w> <w n="18.2">terre</w> <w n="18.3">où</w>, <w n="18.4">doucement</w>, <w n="18.5">les</w> <w n="18.6">miens</w> <w n="18.7">sont</w> <w n="18.8">enfouis</w>.</l>
						<l n="19" num="5.3"><w n="19.1">Il</w> <w n="19.2">n</w>’<w n="19.3">est</w> <w n="19.4">aucun</w> <w n="19.5">relent</w> <w n="19.6">de</w> <w n="19.7">votre</w> <w n="19.8">haleine</w> <w n="19.9">sale</w>,</l>
						<l n="20" num="5.4"><space unit="char" quantity="8"></space><w n="20.1">Dans</w> <w n="20.2">mon</w> <w n="20.3">pays</w>, <w n="20.4">clans</w> <w n="20.5">mon</w> <w n="20.6">pays</w> !</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Mon</w> <w n="21.2">pays</w> <w n="21.3">où</w> <w n="21.4">je</w> <w n="21.5">fus</w> <w n="21.6">une</w> <w n="21.7">enfant</w> <w n="21.8">avant</w> <w n="21.9">d</w>’<w n="21.10">être</w></l>
						<l n="22" num="6.2"><w n="22.1">La</w> <w n="22.2">femme</w> <w n="22.3">d</w>’<w n="22.4">aujourd</w>’<w n="22.5">hui</w> <w n="22.6">que</w> <w n="22.7">visent</w> <w n="22.8">tant</w> <w n="22.9">de</w> <w n="22.10">coups</w>,</l>
						<l n="23" num="6.3"><w n="23.1">Où</w> <w n="23.2">des</w> <w n="23.3">vieilles</w> <w n="23.4">que</w> <w n="23.5">j</w>’<w n="23.6">aime</w> <w n="23.7">et</w> <w n="23.8">qui</w> <w n="23.9">me</w> <w n="23.10">virent</w> <w n="23.11">naître</w></l>
						<l n="24" num="6.4"><space unit="char" quantity="8"></space><w n="24.1">Me</w> <w n="24.2">bercèrent</w> <w n="24.3">sur</w> <w n="24.4">leurs</w> <w n="24.5">genoux</w>.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">Non</w> ! <w n="25.2">Vous</w> <w n="25.3">ne</w> <w n="25.4">serez</w> <w n="25.5">pas</w> <w n="25.6">la</w> <w n="25.7">sombre</w> <w n="25.8">Jacquerie</w></l>
						<l n="26" num="7.2"><w n="26.1">En</w> <w n="26.2">route</w> <w n="26.3">vers</w> <w n="26.4">le</w> <w n="26.5">fier</w> <w n="26.6">château</w> <w n="26.7">de</w> <w n="26.8">mon</w> <w n="26.9">bonheur</w>.</l>
						<l n="27" num="7.3"><w n="27.1">Car</w> <w n="27.2">j</w>’<w n="27.3">ai</w>, <w n="27.4">pour</w> <w n="27.5">me</w> <w n="27.6">garder</w> <w n="27.7">contre</w> <w n="27.8">le</w> <w n="27.9">flot</w> <w n="27.10">hurleur</w>,</l>
						<l n="28" num="7.4"><space unit="char" quantity="8"></space><w n="28.1">Ma</w> <w n="28.2">haie</w> <w n="28.3">autour</w> <w n="28.4">de</w> <w n="28.5">ma</w> <w n="28.6">prairie</w>.</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1"><w n="29.1">Venez</w> ! <w n="29.2">Et</w> <w n="29.3">franchissez</w> <w n="29.4">mes</w> <w n="29.5">herbages</w> <w n="29.6">bourbeux</w> !</l>
						<l n="30" num="8.2"><w n="30.1">Vous</w> <w n="30.2">y</w> <w n="30.3">serez</w> <w n="30.4">reçus</w>, <w n="30.5">croyez</w>-<w n="30.6">le</w>, <w n="30.7">haut</w> <w n="30.8">et</w> <w n="30.9">ferme</w>,</l>
						<l n="31" num="8.3"><w n="31.1">Sur</w> <w n="31.2">les</w> <w n="31.3">fourches</w> <w n="31.4">de</w> <w n="31.5">fer</w> <w n="31.6">des</w> <w n="31.7">hommes</w> <w n="31.8">de</w> <w n="31.9">ma</w> <w n="31.10">ferme</w></l>
						<l n="32" num="8.4"><space unit="char" quantity="8"></space><w n="32.1">Et</w> <w n="32.2">sur</w> <w n="32.3">les</w> <w n="32.4">cornes</w> <w n="32.5">de</w> <w n="32.6">mes</w> <w n="32.7">bœufs</w> !</l>
					</lg>
				</div></body></text></TEI>