<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">PAR VENTS ET MARÉES</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>Delarue-Mardrus</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, OCR, Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1987 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2020</date>
				<idno type="local">DLR_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title type="main">PAR VENTS ET MARÉES</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>BIBLIOTHÈQUE-CHARPENTIER</publisher>
							<date when="1910">1910</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1910">1910</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les retraits introduits automatiquement ont été modifiés conformément à l’édition de référence.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-09-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-09-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CHEZ NOUS</head><div type="poem" key="DLR541">
					<head type="main">NEIGE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Il</w> <w n="1.2">neige</w>. <w n="1.3">Du</w> <w n="1.4">ciel</w> <w n="1.5">noir</w> <w n="1.6">tombe</w> <w n="1.7">de</w> <w n="1.8">la</w> <w n="1.9">blancheur</w>.</l>
						<l n="2" num="1.2"><w n="2.1">Rapidement</w>, <w n="2.2">les</w> <w n="2.3">prés</w> <w n="2.4">seront</w> <w n="2.5">ensevelis</w>.</l>
						<l n="3" num="1.3"><w n="3.1">Il</w> <w n="3.2">monte</w> <w n="3.3">de</w> <w n="3.4">partout</w> <w n="3.5">une</w> <w n="3.6">vaste</w> <w n="3.7">lueur</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Et</w> <w n="4.2">la</w> <w n="4.3">vallée</w> <w n="4.4">est</w> <w n="4.5">creuse</w> <w n="4.6">et</w> <w n="4.7">blanche</w> <w n="4.8">comme</w> <w n="4.9">un</w> <w n="4.10">lys</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Chaque</w> <w n="5.2">flocon</w> <w n="5.3">s</w>’<w n="5.4">allume</w>. <w n="5.5">Il</w> <w n="5.6">fait</w> <w n="5.7">clair</w> ! <w n="5.8">Il</w> <w n="5.9">fait</w> <w n="5.10">clair</w></l>
						<l n="6" num="2.2"><w n="6.1">De</w> <w n="6.2">ce</w> <w n="6.3">luxe</w> <w n="6.4">inouï</w> <w n="6.5">la</w> <w n="6.6">nature</w> <w n="6.7">se</w> <w n="6.8">vêt</w>.</l>
						<l n="7" num="2.3"><w n="7.1">Le</w> <w n="7.2">dehors</w> <w n="7.3">est</w> <w n="7.4">plus</w> <w n="7.5">beau</w> <w n="7.6">que</w> <w n="7.7">tout</w> <w n="7.8">ce</w> <w n="7.9">qu</w>’<w n="7.10">on</w> <w n="7.11">rêvait</w>.</l>
						<l n="8" num="2.4">— <w n="8.1">Salut</w> <w n="8.2">à</w> <w n="8.3">la</w> <w n="8.4">féerie</w> <w n="8.5">immense</w> <w n="8.6">de</w> <w n="8.7">l</w>’<w n="8.8">hiver</w> !</l>
					</lg>
				</div></body></text></TEI>