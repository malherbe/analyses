<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">PAR VENTS ET MARÉES</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>Delarue-Mardrus</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, OCR, Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1987 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2020</date>
				<idno type="local">DLR_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title type="main">PAR VENTS ET MARÉES</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>BIBLIOTHÈQUE-CHARPENTIER</publisher>
							<date when="1910">1910</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1910">1910</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les retraits introduits automatiquement ont été modifiés conformément à l’édition de référence.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-09-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-09-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CHEZ NOUS</head><div type="poem" key="DLR535">
					<head type="main">TEMPÊTE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">On</w> <w n="1.2">dirait</w> <w n="1.3">que</w>, <w n="1.4">ce</w> <w n="1.5">soir</w>, <w n="1.6">tout</w> <w n="1.7">l</w>’<w n="1.8">automne</w> <w n="1.9">est</w> <w n="1.10">venu</w></l>
						<l n="2" num="1.2"><w n="2.1">S</w>’<w n="2.2">abattre</w> <w n="2.3">avec</w> <w n="2.4">le</w> <w n="2.5">vent</w> <w n="2.6">sur</w> <w n="2.7">notre</w> <w n="2.8">toit</w> <w n="2.9">qu</w>’<w n="2.10">il</w> <w n="2.11">cogne</w>.</l>
						<l n="3" num="1.3"><w n="3.1">Dehors</w>, <w n="3.2">le</w> <w n="3.3">mauvais</w> <w n="3.4">temps</w> <w n="3.5">comme</w> <w n="3.6">un</w> <w n="3.7">loup</w>-<w n="3.8">garou</w> <w n="3.9">grogne</w>.</l>
						<l n="4" num="1.4"><w n="4.1">Il</w> <w n="4.2">pleut</w> <w n="4.3">obliquement</w> <w n="4.4">sur</w> <w n="4.5">l</w>’<w n="4.6">estuaire</w> <w n="4.7">nu</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Il</w> <w n="5.2">fait</w> <w n="5.3">triste</w>, <w n="5.4">il</w> <w n="5.5">fait</w> <w n="5.6">froid</w>. <w n="5.7">Le</w> <w n="5.8">cœur</w> <w n="5.9">est</w> <w n="5.10">mal</w> <w n="5.11">à</w> <w n="5.12">l</w>’<w n="5.13">aise</w>.</l>
						<l n="6" num="2.2"><w n="6.1">On</w> <w n="6.2">attend</w> <w n="6.3">malgré</w> <w n="6.4">soi</w> <w n="6.5">les</w> <w n="6.6">fantômes</w> <w n="6.7">du</w> <w n="6.8">soir</w>.</l>
						<l n="7" num="2.3"><w n="7.1">On</w> <w n="7.2">frissonne</w>, <w n="7.3">on</w> <w n="7.4">a</w> <w n="7.5">peur</w>. <w n="7.6">Il</w> <w n="7.7">semble</w> <w n="7.8">qu</w>’<w n="7.9">on</w> <w n="7.10">va</w> <w n="7.11">voir</w></l>
						<l n="8" num="2.4"><w n="8.1">Dès</w> <w n="8.2">faces</w> <w n="8.3">se</w> <w n="8.4">coller</w> <w n="8.5">aux</w> <w n="8.6">vitres</w> <w n="8.7">Louis</w>-<w n="8.8">Seize</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Le</w> <w n="9.2">cimetière</w> <w n="9.3">proche</w> <w n="9.4">et</w> <w n="9.5">visible</w> <w n="9.6">est</w> <w n="9.7">en</w> <w n="9.8">bas</w>.</l>
						<l n="10" num="3.2"><w n="10.1">À</w> <w n="10.2">l</w>’<w n="10.3">horizon</w> <w n="10.4">lointain</w>, <w n="10.5">la</w> <w n="10.6">mer</w> <w n="10.7">crie</w> <w n="10.8">à</w> <w n="10.9">voie</w> <w n="10.10">haute</w>.</l>
						<l n="11" num="3.3"><w n="11.1">Taisons</w>-<w n="11.2">nous</w>… <w n="11.3">Écoutons</w>… <w n="11.4">Guettons</w>…. <w n="11.5">On</w> <w n="11.6">ne</w> <w n="11.7">sait</w> <w n="11.8">pas</w>.</l>
						<l n="12" num="3.4"><w n="12.1">Peut</w>-<w n="12.2">être</w> <w n="12.3">que</w> <w n="12.4">les</w> <w n="12.5">morts</w> <w n="12.6">escaladent</w> <w n="12.7">la</w> <w n="12.8">côte</w>.</l>
					</lg>
				</div></body></text></TEI>