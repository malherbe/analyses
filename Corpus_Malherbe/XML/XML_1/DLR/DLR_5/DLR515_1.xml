<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">PAR VENTS ET MARÉES</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>Delarue-Mardrus</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, OCR, Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1987 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2020</date>
				<idno type="local">DLR_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title type="main">PAR VENTS ET MARÉES</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>BIBLIOTHÈQUE-CHARPENTIER</publisher>
							<date when="1910">1910</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1910">1910</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les retraits introduits automatiquement ont été modifiés conformément à l’édition de référence.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-09-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-09-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LA ROUTE</head><div type="poem" key="DLR515">
					<head type="main">VENISE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">La</w> <w n="1.2">gondole</w> <w n="1.3">qu</w>’<w n="1.4">étoile</w> <w n="1.5">un</w> <w n="1.6">célèbre</w> <w n="1.7">falot</w></l>
						<l n="2" num="1.2"><space unit="char" quantity="8"></space><w n="2.1">Nous</w> <w n="2.2">mène</w>, <w n="2.3">noire</w> <w n="2.4">et</w> <w n="2.5">funéraire</w>.</l>
						<l n="3" num="1.3"><w n="3.1">La</w> <w n="3.2">route</w> <w n="3.3">de</w> <w n="3.4">minuit</w> <w n="3.5">est</w> <w n="3.6">muette</w> <w n="3.7">et</w> <w n="3.8">si</w> <w n="3.9">claire</w>…</l>
						<l n="4" num="1.4"><w n="4.1">C</w>’<w n="4.2">est</w> <w n="4.3">ici</w> <w n="4.4">le</w> <w n="4.5">pays</w> <w n="4.6">de</w> <w n="4.7">la</w> <w n="4.8">lune</w> <w n="4.9">et</w> <w n="4.10">de</w> <w n="4.11">l</w>’<w n="4.12">eau</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Venise</w> <w n="5.2">autour</w> <w n="5.3">de</w> <w n="5.4">nous</w>, <w n="5.5">ville</w> <w n="5.6">paradoxale</w>,</l>
						<l n="6" num="2.2"><space unit="char" quantity="8"></space><w n="6.1">Dresse</w> <w n="6.2">un</w> <w n="6.3">contour</w> <w n="6.4">pesant</w> <w n="6.5">et</w> <w n="6.6">mol</w>.</l>
						<l n="7" num="2.3"><w n="7.1">Ah</w> ! <w n="7.2">n</w>’<w n="7.3">y</w> <w n="7.4">avait</w>-<w n="7.5">il</w> <w n="7.6">plus</w> <w n="7.7">de</w> <w n="7.8">place</w> <w n="7.9">sur</w> <w n="7.10">le</w> <w n="7.11">sol</w></l>
						<l n="8" num="2.4"><w n="8.1">Pour</w> <w n="8.2">te</w> <w n="8.3">choisir</w>, <w n="8.4">lagune</w> <w n="8.5">étroite</w> <w n="8.6">et</w> <w n="8.7">sépulcrale</w> ?</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Voici</w> <w n="9.2">l</w>’<w n="9.3">enfantement</w> <w n="9.4">monstrueux</w> <w n="9.5">de</w> <w n="9.6">la</w> <w n="9.7">peur</w>,</l>
						<l n="10" num="3.2"><space unit="char" quantity="8"></space><w n="10.1">La</w> <w n="10.2">cité</w> <w n="10.3">qui</w> <w n="10.4">fuit</w> <w n="10.5">les</w> <w n="10.6">Barbares</w>.</l>
						<l n="11" num="3.3"><w n="11.1">Qu</w>’<w n="11.2">elle</w> <w n="11.3">soit</w> <w n="11.4">donc</w> <w n="11.5">l</w>’<w n="11.6">asile</w>, <w n="11.7">en</w> <w n="11.8">sa</w> <w n="11.9">vaine</w> <w n="11.10">splendeur</w>,</l>
						<l n="12" num="3.4"><w n="12.1">Des</w> <w n="12.2">rêves</w> <w n="12.3">compliqués</w> <w n="12.4">et</w> <w n="12.5">des</w> <w n="12.6">amours</w> <w n="12.7">bizarres</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Musiciens</w> <w n="13.2">pervers</w>, <w n="13.3">littérateurs</w> <w n="13.4">fiévreux</w>,</l>
						<l n="14" num="4.2"><space unit="char" quantity="8"></space><w n="14.1">Oui</w>, <w n="14.2">qu</w>’<w n="14.3">à</w> <w n="14.4">Venise</w> <w n="14.5">se</w> <w n="14.6">dédie</w></l>
						<l n="15" num="4.3"><w n="15.1">Tout</w> <w n="15.2">sentiment</w> <w n="15.3">où</w> <w n="15.4">reste</w> <w n="15.5">un</w> <w n="15.6">peu</w> <w n="15.7">de</w> <w n="15.8">comédie</w>,</l>
						<l n="16" num="4.4"><w n="16.1">Tout</w> <w n="16.2">visage</w> <w n="16.3">qui</w> <w n="16.4">porte</w> <w n="16.5">un</w> <w n="16.6">masque</w> <w n="16.7">autour</w> <w n="16.8">des</w> <w n="16.9">yeux</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Je</w> <w n="17.2">vous</w> <w n="17.3">le</w> <w n="17.4">dis</w>, <w n="17.5">Venise</w> <w n="17.6">est</w> <w n="17.7">la</w> <w n="17.8">ville</w> <w n="17.9">des</w> <w n="17.10">masques</w> !</l>
						<l n="18" num="5.2"><space unit="char" quantity="8"></space><w n="18.1">Notre</w> <w n="18.2">temps</w>, <w n="18.3">visage</w> <w n="18.4">réel</w>,</l>
						<l n="19" num="5.3"><w n="19.1">N</w>’<w n="19.2">amène</w> <w n="19.3">plus</w> <w n="19.4">ceux</w> <w n="19.5">qu</w>’<w n="19.6">il</w> <w n="19.7">faudrait</w> <w n="19.8">parmi</w> <w n="19.9">ce</w> <w n="19.10">ciel</w>,</l>
						<l n="20" num="5.4"><w n="20.1">Parmi</w> <w n="20.2">cette</w> <w n="20.3">eau</w>, <w n="20.4">ce</w> <w n="20.5">marbre</w> <w n="20.6">et</w> <w n="20.7">ces</w> <w n="20.8">détours</w> <w n="20.9">fantasques</w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">C</w>’<w n="21.2">est</w> <w n="21.3">peut</w>-<w n="21.4">être</w> <w n="21.5">pourquoi</w> <w n="21.6">la</w> <w n="21.7">gondole</w> <w n="21.8">est</w> <w n="21.9">en</w> <w n="21.10">deuil</w>,</l>
						<l n="22" num="6.2"><space unit="char" quantity="8"></space><w n="22.1">Ignorant</w> <w n="22.2">vers</w> <w n="22.3">quoi</w> <w n="22.4">va</w> <w n="22.5">sa</w> <w n="22.6">rame</w></l>
						<l n="23" num="6.3"><w n="23.1">Loin</w> <w n="23.2">du</w> <w n="23.3">doge</w> <w n="23.4">doré</w>, <w n="23.5">loin</w> <w n="23.6">de</w> <w n="23.7">la</w> <w n="23.8">sombre</w> <w n="23.9">dame</w></l>
						<l n="24" num="6.4"><w n="24.1">Qui</w> <w n="24.2">se</w> <w n="24.3">berçaient</w> <w n="24.4">jadis</w> <w n="24.5">clans</w> <w n="24.6">son</w> <w n="24.7">flottant</w> <w n="24.8">cercueil</w>.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">Venise</w> <w n="25.2">meurt</w> <w n="25.3">debout</w>, <w n="25.4">les</w> <w n="25.5">pieds</w> <w n="25.6">dans</w> <w n="25.7">l</w>’<w n="25.8">eau</w> <w n="25.9">malsaine</w>.</l>
						<l n="26" num="7.2"><space unit="char" quantity="8"></space><w n="26.1">Que</w> <w n="26.2">venons</w>-<w n="26.3">nous</w> <w n="26.4">y</w> <w n="26.5">faire</w> <w n="26.6">encor</w> ?</l>
						<l n="27" num="7.3"><w n="27.1">Nous</w> <w n="27.2">qui</w>, <w n="27.3">brutalement</w>, <w n="27.4">envahissons</w> <w n="27.5">la</w> <w n="27.6">scène</w>,</l>
						<l n="28" num="7.4"><w n="28.1">Les</w> <w n="28.2">acteurs</w> <w n="28.3">sont</w> <w n="28.4">partis</w>, <w n="28.5">s</w>’<w n="28.6">il</w> <w n="28.7">reste</w> <w n="28.8">le</w> <w n="28.9">décor</w>.</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1"><w n="29.1">Ville</w> <w n="29.2">sinistre</w>, <w n="29.3">empuantie</w>, <w n="29.4">où</w>, <w n="29.5">dans</w> <w n="29.6">la</w> <w n="29.7">vase</w>,</l>
						<l n="30" num="8.2"><space unit="char" quantity="8"></space><w n="30.1">Pourrit</w> <w n="30.2">le</w> <w n="30.3">marbre</w> <w n="30.4">mariné</w> !</l>
						<l n="31" num="8.3"><w n="31.1">Églises</w> <w n="31.2">et</w> <w n="31.3">palais</w> <w n="31.4">sont</w> <w n="31.5">rongés</w> <w n="31.6">par</w> <w n="31.7">la</w> <w n="31.8">base</w>,</l>
						<l n="32" num="8.4"><w n="32.1">Et</w> <w n="32.2">tel</w> <w n="32.3">qui</w> <w n="32.4">brille</w> <w n="32.5">encore</w> <w n="32.6">est</w> <w n="32.7">déjà</w> <w n="32.8">ruiné</w>.</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1"><w n="33.1">Ah</w> ! <w n="33.2">respectons</w> <w n="33.3">cette</w> <w n="33.4">agonie</w> <w n="33.5">et</w> <w n="33.6">ses</w> <w n="33.7">symptômes</w> !</l>
						<l n="34" num="9.2"><space unit="char" quantity="8"></space><w n="34.1">Tout</w> <w n="34.2">est</w> <w n="34.3">mort</w> <w n="34.4">de</w> <w n="34.5">ce</w> <w n="34.6">qui</w> <w n="34.7">passait</w></l>
						<l n="35" num="9.3"><w n="35.1">Sur</w> <w n="35.2">les</w> <w n="35.3">canaux</w> <w n="35.4">restreints</w>, <w n="35.5">où</w> <w n="35.6">pleurent</w>, <w n="35.7">doux</w> <w n="35.8">fantômes</w>,</l>
						<l n="36" num="9.4"><w n="36.1">Ces</w> <w n="36.2">deux</w> <w n="36.3">derniers</w> <w n="36.4">venus</w>, <w n="36.5">George</w> <w n="36.6">Sand</w> <w n="36.7">et</w> <w n="36.8">Musset</w>.</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1"><w n="37.1">Laissons</w> <w n="37.2">cette</w> <w n="37.3">cité</w> <w n="37.4">s</w>’<w n="37.5">écrouler</w> <w n="37.6">d</w>’<w n="37.7">elle</w>-<w n="37.8">même</w> !</l>
						<l n="38" num="10.2"><space unit="char" quantity="8"></space><w n="38.1">Pitié</w> <w n="38.2">pour</w> <w n="38.3">son</w> <w n="38.4">dernier</w> <w n="38.5">sanglot</w> !</l>
						<l n="39" num="10.3"><w n="39.1">Elle</w> <w n="39.2">connaît</w> <w n="39.3">déjà</w> <w n="39.4">la</w> <w n="39.5">sentence</w> <w n="39.6">suprême</w></l>
						<l n="40" num="10.4"><w n="40.1">Qui</w> <w n="40.2">la</w> <w n="40.3">condamne</w> <w n="40.4">à</w> <w n="40.5">mort</w> <w n="40.6">par</w> <w n="40.7">le</w> <w n="40.8">poison</w> <w n="40.9">de</w> <w n="40.10">l</w>’<w n="40.11">eau</w>.</l>
					</lg>
					<lg n="11">
						<l n="41" num="11.1"><w n="41.1">Nos</w> <w n="41.2">pieds</w> <w n="41.3">sonnent</w> <w n="41.4">si</w> <w n="41.5">faux</w> <w n="41.6">sur</w> <w n="41.7">le</w> <w n="41.8">marbre</w> <w n="41.9">des</w> <w n="41.10">marches</w></l>
						<l n="42" num="11.2"><space unit="char" quantity="8"></space><w n="42.1">Laissons</w> <w n="42.2">Venise</w> <w n="42.3">à</w> <w n="42.4">son</w> <w n="42.5">passé</w>,</l>
						<l n="43" num="11.3"><w n="43.1">Laissons</w> <w n="43.2">finir</w> <w n="43.3">en</w> <w n="43.4">paix</w> <w n="43.5">ses</w> <w n="43.6">palais</w> <w n="43.7">et</w> <w n="43.8">ses</w> <w n="43.9">arches</w> :</l>
						<l n="44" num="11.4"><hi rend="ital"><w n="44.1">Requiescat</w></hi> <w n="44.2">sur</w> <w n="44.3">elle</w>, <w n="44.4">à</w> <w n="44.5">jamais</w>, <hi rend="ital"><w n="44.6">in</w> <w n="44.7">pace</w></hi> !</l>
					</lg>
				</div></body></text></TEI>