<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">PAR VENTS ET MARÉES</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>Delarue-Mardrus</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, OCR, Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1987 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2020</date>
				<idno type="local">DLR_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title type="main">PAR VENTS ET MARÉES</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>BIBLIOTHÈQUE-CHARPENTIER</publisher>
							<date when="1910">1910</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1910">1910</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les retraits introduits automatiquement ont été modifiés conformément à l’édition de référence.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-09-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-09-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LA MER</head><div type="poem" key="DLR492">
					<head type="main">IN MEMORIAM</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Vous</w>, <w n="1.2">pêcheurs</w> <w n="1.3">morts</w> <w n="1.4">tombés</w> <w n="1.5">hors</w> <w n="1.6">de</w> <w n="1.7">votre</w> <w n="1.8">bateau</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Dans</w> <w n="2.2">l</w>’<w n="2.3">estuaire</w> <w n="2.4">gris</w> <w n="2.5">où</w> <w n="2.6">la</w> <w n="2.7">Seine</w> <w n="2.8">s</w>’<w n="2.9">évase</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Vous</w> <w n="3.2">qu</w>’<w n="3.3">on</w> <w n="3.4">ne</w> <w n="3.5">peut</w> <w n="3.6">trouver</w>, <w n="3.7">enfouis</w> <w n="3.8">dans</w> <w n="3.9">la</w> <w n="3.10">vase</w>,</l>
						<l n="4" num="1.4"><space unit="char" quantity="8"></space><w n="4.1">Vous</w> <w n="4.2">serez</w> <w n="4.3">oubliés</w> <w n="4.4">bientôt</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Le</w> <w n="5.2">souvenir</w> <w n="5.3">est</w> <w n="5.4">court</w> <w n="5.5">chez</w> <w n="5.6">les</w> <w n="5.7">enfants</w>. <w n="5.8">La</w> <w n="5.9">veuve</w> ?</l>
						<l n="6" num="2.2"><w n="6.1">Les</w> <w n="6.2">coups</w> <w n="6.3">lui</w> <w n="6.4">manqueront</w>, <w n="6.5">elle</w> <w n="6.6">se</w> <w n="6.7">mariera</w>.</l>
						<l n="7" num="2.3"><w n="7.1">Les</w> <w n="7.2">amis</w> ? <w n="7.3">ils</w> <w n="7.4">en</w> <w n="7.5">ont</w> <w n="7.6">tant</w> <w n="7.7">vu</w> ! <w n="7.8">Ton</w> <w n="7.9">corps</w> <w n="7.10">sera</w></l>
						<l n="8" num="2.4"><space unit="char" quantity="8"></space><w n="8.1">En</w> <w n="8.2">pleine</w> <w n="8.3">mer</w> <w n="8.4">ou</w> <w n="8.5">dans</w> <w n="8.6">le</w> <w n="8.7">fleuve</w>…</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">« <w n="9.1">Eh</w> <w n="9.2">quoi</w> ! <w n="9.3">Je</w> <w n="9.4">n</w>’<w n="9.5">aurai</w> <w n="9.6">rien</w>, <w n="9.7">prière</w> <w n="9.8">ni</w> <w n="9.9">tombeau</w>,</l>
						<l n="10" num="3.2"><w n="10.1">Moi</w> <w n="10.2">pauvre</w> <w n="10.3">travailleur</w> <w n="10.4">mort</w> <w n="10.5">en</w> <w n="10.6">pleine</w> <w n="10.7">besogne</w> ?</l>
						<l n="11" num="3.3"><w n="11.1">Quand</w> <w n="11.2">il</w> <w n="11.3">fera</w> <w n="11.4">mauvais</w> <w n="11.5">et</w> <w n="11.6">quand</w> <w n="11.7">il</w> <w n="11.8">fera</w> <w n="11.9">beau</w></l>
						<l n="12" num="3.4"><space unit="char" quantity="8"></space><w n="12.1">Je</w> <w n="12.2">ne</w> <w n="12.3">serai</w> <w n="12.4">qu</w>’<w n="12.5">une</w> <w n="12.6">charogne</w> ? »</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Pêcheur</w>, <w n="13.2">pêcheur</w>, <w n="13.3">quelqu</w>’<w n="13.4">un</w>, <w n="13.5">à</w> <w n="13.6">l</w>’<w n="13.7">heure</w> <w n="13.8">où</w> <w n="13.9">la</w> <w n="13.10">nuit</w> <w n="13.11">vient</w></l>
						<l n="14" num="4.2"><w n="14.1">T</w>’<w n="14.2">écoutera</w> <w n="14.3">parler</w> <w n="14.4">avec</w> <w n="14.5">la</w> <w n="14.6">vague</w> <w n="14.7">oblique</w>.</l>
						<l n="15" num="4.3"><w n="15.1">Repose</w> <w n="15.2">dans</w> <w n="15.3">mon</w> <w n="15.4">cœur</w>, <w n="15.5">tombeau</w> <w n="15.6">mélancolique</w> :</l>
						<l n="16" num="4.4"><space unit="char" quantity="8"></space><w n="16.1">Le</w> <w n="16.2">poète</w>, <w n="16.3">lui</w>, <w n="16.4">se</w> <w n="16.5">souvient</w>.</l>
					</lg>
				</div></body></text></TEI>