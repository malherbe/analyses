<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">PAR VENTS ET MARÉES</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>Delarue-Mardrus</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, OCR, Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1987 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2020</date>
				<idno type="local">DLR_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title type="main">PAR VENTS ET MARÉES</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>BIBLIOTHÈQUE-CHARPENTIER</publisher>
							<date when="1910">1910</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1910">1910</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les retraits introduits automatiquement ont été modifiés conformément à l’édition de référence.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-09-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-09-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CHEZ NOUS</head><div type="poem" key="DLR543">
					<head type="main">DON D’HIVER</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Le</w> <w n="1.2">premier</w> <w n="1.3">gui</w> <w n="1.4">cueilli</w> <w n="1.5">chez</w> <w n="1.6">nous</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Je</w> <w n="2.2">te</w> <w n="2.3">l</w>’<w n="2.4">apporte</w> <w n="2.5">avec</w> <w n="2.6">le</w> <w n="2.7">givre</w> ;</l>
						<l n="3" num="1.3"><w n="3.1">Grand</w>’<w n="3.2">mère</w> <w n="3.3">qui</w> <w n="3.4">ne</w> <w n="3.5">peux</w> <w n="3.6">plus</w> <w n="3.7">vivre</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Je</w> <w n="4.2">te</w> <w n="4.3">l</w>’<w n="4.4">apporte</w> <w n="4.5">avec</w> <w n="4.6">le</w> <w n="4.7">houx</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Ton</w> <w n="5.2">âme</w>, <w n="5.3">au</w> <w n="5.4">fond</w> <w n="5.5">de</w> <w n="5.6">tes</w> <w n="5.7">yeux</w> <w n="5.8">roux</w>,</l>
						<l n="6" num="2.2"><w n="6.1">S</w>’<w n="6.2">ouvrait</w> <w n="6.3">comme</w> <w n="6.4">un</w> <w n="6.5">étrange</w> <w n="6.6">livre</w>.</l>
						<l n="7" num="2.3"><w n="7.1">Le</w> <w n="7.2">premier</w> <w n="7.3">gui</w> <w n="7.4">cueilli</w> <w n="7.5">chez</w> <w n="7.6">nous</w>,</l>
						<l n="8" num="2.4"><w n="8.1">Je</w> <w n="8.2">te</w> <w n="8.3">l</w>’<w n="8.4">apporte</w> <w n="8.5">avec</w> <w n="8.6">le</w> <w n="8.7">givre</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Lèvent</w> <w n="9.2">nous</w> <w n="9.3">glace</w> <w n="9.4">de</w>. <w n="9.5">ses</w>-<w n="9.6">coups</w>,</l>
						<l n="10" num="3.2"><w n="10.1">Mais</w> <w n="10.2">la</w> <w n="10.3">jeunesse</w> <w n="10.4">nous</w> <w n="10.5">enivre</w>.</l>
						<l n="11" num="3.3"><w n="11.1">Toi</w>, <w n="11.2">froideur</w> <w n="11.3">que</w> <w n="11.4">rien</w> <w n="11.5">ne</w> <w n="11.6">délivre</w>,</l>
						<l n="12" num="3.4"><w n="12.1">O</w> <w n="12.2">morte</w> ! <w n="12.3">je</w> <w n="12.4">t</w>’<w n="12.5">offre</w> <w n="12.6">à</w> <w n="12.7">genoux</w></l>
						<l n="13" num="3.5"><w n="13.1">Le</w> <w n="13.2">premier</w> <w n="13.3">gui</w> <w n="13.4">cueilli</w> <w n="13.5">chez</w> <w n="13.6">nous</w>.</l>
					</lg>
				</div></body></text></TEI>