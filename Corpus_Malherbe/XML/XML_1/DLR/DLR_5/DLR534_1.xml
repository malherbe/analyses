<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">PAR VENTS ET MARÉES</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>Delarue-Mardrus</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, OCR, Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1987 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2020</date>
				<idno type="local">DLR_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title type="main">PAR VENTS ET MARÉES</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>BIBLIOTHÈQUE-CHARPENTIER</publisher>
							<date when="1910">1910</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1910">1910</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les retraits introduits automatiquement ont été modifiés conformément à l’édition de référence.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-09-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-09-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CHEZ NOUS</head><div type="poem" key="DLR534">
					<head type="main">GRAVÉ EN SONGE</head>
					<head type="sub">SUR UNE PIERRE DE MA PAROISSE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Au</w> <w n="1.2">bout</w> <w n="1.3">des</w> <w n="1.4">tours</w> <w n="1.5">et</w> <w n="1.6">des</w> <w n="1.7">détours</w></l>
						<l n="2" num="1.2"><w n="2.1">Où</w> <w n="2.2">se</w> <w n="2.3">croisent</w> <w n="2.4">les</w> <w n="2.5">carrefours</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Haute</w> <w n="3.2">et</w> <w n="3.3">vieille</w>, <w n="3.4">voici</w> <w n="3.5">l</w>’<w n="3.6">église</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Cœur</w> <w n="4.2">doré</w> <w n="4.3">de</w> <w n="4.4">la</w> <w n="4.5">ville</w> <w n="4.6">grise</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Voici</w> <w n="5.2">l</w>’<w n="5.3">encens</w>, <w n="5.4">l</w>’<w n="5.5">alleluia</w></l>
						<l n="6" num="2.2"><w n="6.1">Et</w> <w n="6.2">l</w>’<w n="6.3">or</w> <w n="6.4">qui</w>, <w n="6.5">des</w> <w n="6.6">siècles</w>, <w n="6.7">brilla</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Et</w> <w n="7.2">les</w> <w n="7.3">vitraux</w> <w n="7.4">et</w> <w n="7.5">le</w> <w n="7.6">grand</w> <w n="7.7">orgue</w>,</l>
						<l n="8" num="2.4"><w n="8.1">Et</w> <w n="8.2">de</w> <w n="8.3">la</w> <w n="8.4">grâce</w> <w n="8.5">et</w> <w n="8.6">de</w> <w n="8.7">la</w> <w n="8.8">morgue</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Au</w> <w n="9.2">loin</w>, <w n="9.3">c</w>’<w n="9.4">est</w> <w n="9.5">le</w> <w n="9.6">port</w>, <w n="9.7">c</w>’<w n="9.8">est</w> <w n="9.9">la</w> <w n="9.10">mer</w>.</l>
						<l n="10" num="3.2"><w n="10.1">L</w>’<w n="10.2">humanité</w>, <w n="10.3">bétail</w> <w n="10.4">amer</w>,</l>
						<l n="11" num="3.3"><w n="11.1">Remplit</w> <w n="11.2">d</w>’<w n="11.3">une</w> <w n="11.4">vie</w> <w n="11.5">humble</w> <w n="11.6">et</w> <w n="11.7">dure</w></l>
						<l n="12" num="3.4"><w n="12.1">Les</w> <w n="12.2">maisons</w> <w n="12.3">où</w> <w n="12.4">le</w> <w n="12.5">malheur</w> <w n="12.6">dure</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">La</w> <w n="13.2">douleur</w> <w n="13.3">et</w> <w n="13.4">la</w> <w n="13.5">pauvreté</w></l>
						<l n="14" num="4.2"><w n="14.1">Se</w> <w n="14.2">parent</w> <w n="14.3">d</w>’<w n="14.4">un</w> <w n="14.5">peu</w> <w n="14.6">de</w> <w n="14.7">beauté</w>…</l>
						<l n="15" num="4.3"><w n="15.1">Debout</w> <w n="15.2">sur</w> <w n="15.3">l</w>’<w n="15.4">existence</w> <w n="15.5">brève</w>,</l>
						<l n="16" num="4.4"><w n="16.1">C</w>’<w n="16.2">est</w> <w n="16.3">l</w>’<w n="16.4">église</w>, <w n="16.5">l</w>’<w n="16.6">éternel</w> <w n="16.7">rêve</w>.</l>
					</lg>
				</div></body></text></TEI>