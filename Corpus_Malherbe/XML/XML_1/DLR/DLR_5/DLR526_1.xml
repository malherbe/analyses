<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">PAR VENTS ET MARÉES</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>Delarue-Mardrus</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, OCR, Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1987 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2020</date>
				<idno type="local">DLR_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title type="main">PAR VENTS ET MARÉES</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>BIBLIOTHÈQUE-CHARPENTIER</publisher>
							<date when="1910">1910</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1910">1910</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les retraits introduits automatiquement ont été modifiés conformément à l’édition de référence.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-09-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-09-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CHEZ NOUS</head><div type="poem" key="DLR526">
					<head type="main">SANGLOT</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Au</w> <w n="1.2">premier</w> <w n="1.3">chant</w> <w n="1.4">du</w> <w n="1.5">merle</w> <w n="1.6">dans</w> <w n="1.7">le</w> <w n="1.8">lierre</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Je</w> <w n="2.2">me</w> <w n="2.3">suis</w> <w n="2.4">éveillée</w> <w n="2.5">en</w> <w n="2.6">sanglots</w> <w n="2.7">haletants</w>.</l>
						<l n="3" num="1.3"><w n="3.1">Quelle</w> <w n="3.2">douleur</w> <w n="3.3">sauvage</w>, <w n="3.4">inconsolable</w>, <w n="3.5">entière</w></l>
						<l n="4" num="1.4"><w n="4.1">D</w>’<w n="4.2">avoir</w> <w n="4.3">reconnu</w> <w n="4.4">la</w> <w n="4.5">voix</w> <w n="4.6">du</w> <w n="4.7">printemps</w> !</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">O</w> <w n="5.2">gosier</w> <w n="5.3">du</w> <w n="5.4">merle</w>, <w n="5.5">ô</w> <w n="5.6">réminiscence</w>,</l>
						<l n="6" num="2.2"><w n="6.1">Comme</w> <w n="6.2">tu</w> <w n="6.3">fais</w> <w n="6.4">sombrer</w> <w n="6.5">ma</w> <w n="6.6">tête</w> <w n="6.7">clans</w> <w n="6.8">mes</w> <w n="6.9">doigts</w></l>
						<l n="7" num="2.3"><w n="7.1">Tu</w> <w n="7.2">chantes</w> <w n="7.3">le</w> <w n="7.4">chant</w> <w n="7.5">de</w> <w n="7.6">l</w>’<w n="7.7">adolescence</w>,</l>
						<l n="8" num="2.4"><w n="8.1">Non</w> <w n="8.2">le</w> <w n="8.3">printemps</w> <w n="8.4">présent</w>, <w n="8.5">mais</w> <w n="8.6">celui</w> <w n="8.7">d</w>’<w n="8.8">autrefois</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Je</w> <w n="9.2">reconnais</w> <w n="9.3">bien</w> <w n="9.4">mon</w> <w n="9.5">ancienne</w> <w n="9.6">âme</w></l>
						<l n="10" num="3.2"><w n="10.1">Fraîche</w>, <w n="10.2">double</w>, <w n="10.3">pareille</w> <w n="10.4">à</w> <w n="10.5">Daphnis</w> <w n="10.6">et</w> <w n="10.7">Chloé</w>…</l>
						<l n="11" num="3.3"><w n="11.1">Tant</w> <w n="11.2">de</w> <w n="11.3">choses</w> <w n="11.4">ont</w> <w n="11.5">en</w> <w n="11.6">elle</w> <w n="11.7">afflué</w></l>
						<l n="12" num="3.4"><w n="12.1">Vraiment</w>, <w n="12.2">pour</w> <w n="12.3">que</w> <w n="12.4">je</w> <w n="12.5">sois</w> <w n="12.6">aujourd</w>’<w n="12.7">hui</w> <w n="12.8">cette</w> <w n="12.9">femme</w></l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">À</w> <w n="13.2">ce</w> <w n="13.3">chant</w> <w n="13.4">soudain</w> <w n="13.5">du</w> <w n="13.6">merle</w>, <w n="13.7">il</w> <w n="13.8">me</w> <w n="13.9">vient</w></l>
						<l n="14" num="4.2"><w n="14.1">Une</w> <w n="14.2">mélancolie</w> <w n="14.3">absurde</w> <w n="14.4">et</w> <w n="14.5">déchirante</w>.</l>
						<l n="15" num="4.3"><w n="15.1">Comment</w> <w n="15.2">m</w>’<w n="15.3">expliquer</w> <w n="15.4">ce</w> <w n="15.5">qui</w> <w n="15.6">parfois</w> <w n="15.7">hante</w></l>
						<l n="16" num="4.4"><w n="16.1">Mes</w> <w n="16.2">rêves</w>, <w n="16.3">nonobstant</w> <w n="16.4">tout</w> <w n="16.5">le</w> <w n="16.6">bon</w>, <w n="16.7">tout</w> <w n="16.8">le</w> <w n="16.9">bien</w> ?…</l>
					</lg>
				</div></body></text></TEI>