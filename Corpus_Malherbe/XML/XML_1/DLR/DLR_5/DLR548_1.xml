<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">PAR VENTS ET MARÉES</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>Delarue-Mardrus</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, OCR, Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1987 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2020</date>
				<idno type="local">DLR_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title type="main">PAR VENTS ET MARÉES</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>BIBLIOTHÈQUE-CHARPENTIER</publisher>
							<date when="1910">1910</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1910">1910</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les retraits introduits automatiquement ont été modifiés conformément à l’édition de référence.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-09-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-09-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CHEZ NOUS</head><div type="poem" key="DLR548">
					<head type="main">LE VENT NOCTURNE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Comme</w> <w n="1.2">quelqu</w>’<w n="1.3">un</w>, <w n="1.4">par</w> <w n="1.5">la</w> <w n="1.6">fenêtre</w> <w n="1.7">grande</w> <w n="1.8">ouverte</w></l>
						<l n="2" num="1.2"><space unit="char" quantity="8"></space><w n="2.1">Cette</w> <w n="2.2">nuit</w> <w n="2.3">grondeuse</w>, <w n="2.4">est</w> <w n="2.5">entré</w> <w n="2.6">le</w> <w n="2.7">vent</w>.</l>
						<l n="3" num="1.3"><space unit="char" quantity="8"></space><w n="3.1">Dans</w> <w n="3.2">mon</w> <w n="3.3">lit</w> <w n="3.4">je</w> <w n="3.5">gisais</w>, <w n="3.6">inerte</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Lazare</w> <w n="4.2">du</w> <w n="4.3">sommeil</w>, <w n="4.4">qui</w> <w n="4.5">redevient</w> <w n="4.6">vivant</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Il</w> <w n="5.2">réveilla</w> <w n="5.3">le</w> <w n="5.4">chœur</w> <w n="5.5">d</w>’<w n="5.6">esprits</w> <w n="5.7">qui</w> <w n="5.8">me</w> <w n="5.9">possède</w>.</l>
						<l n="6" num="2.2"><space unit="char" quantity="8"></space><w n="6.1">Sa</w> <w n="6.2">voix</w> <w n="6.3">me</w> <w n="6.4">donnait</w> <w n="6.5">un</w> <w n="6.6">ardent</w> <w n="6.7">conseil</w></l>
						<l n="7" num="2.3"><space unit="char" quantity="8"></space>— <w n="7.1">Debout</w>, <w n="7.2">démon</w>, <w n="7.3">rôdeuse</w>, <w n="7.4">aède</w> !</l>
						<l n="8" num="2.4"><w n="8.1">Viens</w> <w n="8.2">voler</w> <w n="8.3">avec</w> <w n="8.4">moi</w> <w n="8.5">le</w> <w n="8.6">grand</w> <w n="8.7">vent</w>, <w n="8.8">ton</w> <w n="8.9">pareil</w> !</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Il</w> <w n="9.2">disait</w> : « <w n="9.3">Tu</w> <w n="9.4">fuiras</w> <w n="9.5">dans</w> <w n="9.6">la</w> <w n="9.7">nuit</w>, <w n="9.8">invisible</w>,</l>
						<l n="10" num="3.2"><space unit="char" quantity="8"></space><w n="10.1">Comme</w> <w n="10.2">la</w> <w n="10.3">bourrasque</w> <w n="10.4">et</w> <w n="10.5">comme</w> <w n="10.6">les</w> <w n="10.7">morts</w>.</l>
						<l n="11" num="3.3"><space unit="char" quantity="8"></space><w n="11.1">Tu</w> <w n="11.2">serais</w> <w n="11.3">tes</w> <w n="11.4">aïeux</w> <w n="11.5">sans</w> <w n="11.6">corps</w></l>
						<l n="12" num="3.4"><w n="12.1">Et</w> <w n="12.2">ton</w> <w n="12.3">père</w>, <w n="12.4">ce</w> <w n="12.5">mort</w> <w n="12.6">d</w>’<w n="12.7">hier</w>, <w n="12.8">encor</w> <w n="12.9">sensible</w>. »</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Il</w> <w n="13.2">disait</w> : « <w n="13.3">Tu</w> <w n="13.4">serais</w> <w n="13.5">hiboux</w> <w n="13.6">et</w> <w n="13.7">revenants</w>,</l>
						<l n="14" num="4.2"><space unit="char" quantity="8"></space><w n="14.1">Tout</w> <w n="14.2">ce</w> <w n="14.3">qui</w> <w n="14.4">se</w> <w n="14.5">plaint</w> <w n="14.6">sans</w> <w n="14.7">vouloir</w> <w n="14.8">se</w> <w n="14.9">taire</w>,</l>
						<l n="15" num="4.3"><space unit="char" quantity="8"></space><w n="15.1">Tu</w> <w n="15.2">serais</w> <w n="15.3">le</w> <w n="15.4">cri</w> <w n="15.5">du</w> <w n="15.6">mystère</w></l>
						<l n="16" num="4.4"><w n="16.1">Qui</w> <w n="16.2">s</w>’<w n="16.3">élève</w>, <w n="16.4">les</w> <w n="16.5">nuits</w>, <w n="16.6">sur</w> <w n="16.7">des</w> <w n="16.8">rythmes</w> <w n="16.9">traînants</w>. »</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Il</w> <w n="17.2">disait</w> : « <w n="17.3">Lève</w>-<w n="17.4">toi</w> ! <w n="17.5">debout</w>, <w n="17.6">fille</w> <w n="17.7">nocturne</w> !</l>
						<l n="18" num="5.2"><space unit="char" quantity="8"></space><w n="18.1">Tu</w> <w n="18.2">portes</w> <w n="18.3">un</w> <w n="18.4">masque</w> <w n="18.5">au</w> <w n="18.6">cœur</w>, <w n="18.7">ôte</w>-<w n="18.8">le</w>,</l>
						<l n="19" num="5.3"><space unit="char" quantity="8"></space><w n="19.1">Ensauvagée</w> <w n="19.2">et</w> <w n="19.3">taciturne</w></l>
						<l n="20" num="5.4"><w n="20.1">Sœur</w> <w n="20.2">de</w> <w n="20.3">l</w>’<w n="20.4">air</w> <w n="20.5">et</w> <w n="20.6">de</w> <w n="20.7">l</w>’<w n="20.8">eau</w>, <w n="20.9">de</w> <w n="20.10">la</w> <w n="20.11">terre</w> <w n="20.12">et</w> <w n="20.13">du</w> <w n="20.14">feu</w> ! »</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Il</w> <w n="21.2">disait</w> : « <w n="21.3">Fais</w> <w n="21.4">semblant</w>, <w n="21.5">au</w> <w n="21.6">soleil</w>, <w n="21.7">d</w>’<w n="21.8">être</w> <w n="21.9">humaine</w>,</l>
						<l n="22" num="6.2"><space unit="char" quantity="8"></space><w n="22.1">Mais</w>, <w n="22.2">quand</w> <w n="22.3">vient</w> <w n="22.4">la</w> <w n="22.5">nuit</w>, <w n="22.6">subis</w> <w n="22.7">ton</w> <w n="22.8">destin</w>.</l>
						<l n="23" num="6.3"><space unit="char" quantity="8"></space><w n="23.1">Âme</w> <w n="23.2">sans</w> <w n="23.3">bornes</w>, <w n="23.4">je</w> <w n="23.5">t</w>’<w n="23.6">entraîne</w>,</l>
						<l n="24" num="6.4"><w n="24.1">Sois</w> <w n="24.2">le</w> <w n="24.3">vent</w>, <w n="24.4">le</w> <w n="24.5">grand</w> <w n="24.6">vent</w>, <w n="24.7">le</w> <w n="24.8">vent</w> <w n="24.9">jusqu</w>’<w n="24.10">au</w> <w n="24.11">matin</w> !…»</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">Lors</w> <w n="25.2">j</w>’<w n="25.3">ai</w> <w n="25.4">pris</w> <w n="25.5">l</w>’<w n="25.6">envergure</w> <w n="25.7">informe</w> <w n="25.8">et</w> <w n="25.9">gigantesque</w></l>
						<l n="26" num="7.2"><space unit="char" quantity="8"></space><w n="26.1">Espace</w>, <w n="26.2">dis</w>-<w n="26.3">nous</w>, <w n="26.4">ô</w> <w n="26.5">toi</w>, <w n="26.6">notre</w> <w n="26.7">amant</w>,</l>
						<l n="27" num="7.3"><space unit="char" quantity="8"></space><w n="27.1">Si</w> <w n="27.2">ce</w> <w n="27.3">fut</w> <w n="27.4">tout</w>, <w n="27.5">à</w> <w n="27.6">fait</w> <w n="27.7">ou</w> <w n="27.8">presque</w></l>
						<l n="28" num="7.4"><w n="28.1">Que</w> <w n="28.2">régna</w> <w n="28.3">la</w> <w n="28.4">tempête</w>, <w n="28.5">épouvantablement</w> ?</l>
					</lg>
				</div></body></text></TEI>