<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">OCCIDENT</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3151 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Occident</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">http://www.poesies.net/luciedelaruemardrueoccident.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Occident</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LA REVUE BLANCHE</publisher>
							<date when="1901">1901</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1901">1901</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
					<p>Certains retraits de vers ont été supprimés conformément à l’édition de référence.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">EN PLEIN VENT</head><div type="poem" key="DLR13">
					<head type="main">HAIES D’OCTOBRE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Premier</w> <w n="1.2">frisson</w> <w n="1.3">de</w> <w n="1.4">jaune</w> <w n="1.5">aux</w> <w n="1.6">verdures</w> ; <w n="1.7">les</w> <w n="1.8">haies</w></l>
						<l n="2" num="1.2"><w n="2.1">Sont</w> <w n="2.2">riches</w> <w n="2.3">d</w>’<w n="2.4">avoir</w> <w n="2.5">fait</w> <w n="2.6">éclore</w> <w n="2.7">tant</w> <w n="2.8">de</w> <w n="2.9">baies</w>,</l>
						<l n="3" num="1.3"><space unit="char" quantity="16"></space><w n="3.1">Rouges</w> <w n="3.2">de</w> <w n="3.3">baies</w></l>
						<l n="4" num="1.4"><w n="4.1">Et</w> <w n="4.2">rouges</w> <w n="4.3">d</w>’<w n="4.4">oiseaux</w> <w n="4.5">roux</w> <w n="4.6">hôtes</w> <w n="4.7">nombreux</w> <w n="4.8">des</w> <w n="4.9">haies</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Rouges</w>-<w n="5.2">gorges</w> <w n="5.3">tachés</w> <w n="5.4">de</w> <w n="5.5">rouge</w> <w n="5.6">sous</w> <w n="5.7">les</w> <w n="5.8">cous</w>,</l>
						<l n="6" num="2.2"><w n="6.1">Moineaux</w> <w n="6.2">cuivrés</w> <w n="6.3">parmi</w> <w n="6.4">le</w> <w n="6.5">sorbier</w> <w n="6.6">et</w> <w n="6.7">le</w> <w n="6.8">houx</w>,</l>
						<l n="7" num="2.3"><space unit="char" quantity="16"></space><w n="7.1">Rouge</w> <w n="7.2">le</w> <w n="7.3">houx</w></l>
						<l n="8" num="2.4"><w n="8.1">Et</w> <w n="8.2">rouges</w> <w n="8.3">ces</w> <w n="8.4">oiseaux</w> <w n="8.5">tachés</w> <w n="8.6">de</w> <w n="8.7">rouge</w> <w n="8.8">aux</w> <w n="8.9">cous</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Sureaux</w> <w n="9.2">aussi</w>, <w n="9.3">régal</w> <w n="9.4">des</w> <w n="9.5">becs</w> <w n="9.6">gourmands</w>, <w n="9.7">et</w> <w n="9.8">mûres</w></l>
						<l n="10" num="3.2"><w n="10.1">En</w> <w n="10.2">grappes</w>, <w n="10.3">et</w> <w n="10.4">traînant</w>, <w n="10.5">trempant</w> <w n="10.6">dans</w> <w n="10.7">les</w> <w n="10.8">murmures</w>,</l>
						<l n="11" num="3.3"><space unit="char" quantity="16"></space><w n="11.1">Sureaux</w> <w n="11.2">et</w> <w n="11.3">mûres</w>,</l>
						<l n="12" num="3.4"><w n="12.1">Des</w> <w n="12.2">ruisselets</w> <w n="12.3">courants</w> <w n="12.4">pleins</w> <w n="12.5">de</w> <w n="12.6">menus</w> <w n="12.7">murmures</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Eau</w> <w n="13.2">qui</w> <w n="13.3">clapote</w>, <w n="13.4">envols</w> <w n="13.5">et</w> <w n="13.6">petits</w> <w n="13.7">cris</w> <w n="13.8">piquants</w>,</l>
						<l n="14" num="4.2"><w n="14.1">Arbrisseaux</w>, <w n="14.2">ruisselets</w>, <w n="14.3">oiseaux</w>, <w n="14.4">petits</w> <w n="14.5">cancans</w></l>
						<l n="15" num="4.3"><space unit="char" quantity="16"></space><w n="15.1">En</w> <w n="15.2">cris</w> <w n="15.3">piquants</w>,</l>
						<l n="16" num="4.4"><w n="16.1">Grains</w> <w n="16.2">dispersés</w> <w n="16.3">du</w> <w n="16.4">bec</w>, <w n="16.5">poursuites</w> <w n="16.6">et</w> <w n="16.7">cancans</w>,</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Détournement</w> <w n="17.2">du</w> <w n="17.3">sombre</w> <w n="17.4">où</w> <w n="17.5">le</w> <w n="17.6">rêve</w> <w n="17.7">se</w> <w n="17.8">plonge</w>,</l>
						<l n="18" num="5.2"><w n="18.1">Tout</w> <w n="18.2">le</w> <w n="18.3">long</w> <w n="18.4">de</w> <w n="18.5">la</w> <w n="18.6">haie</w> <w n="18.7">en</w> <w n="18.8">tumulte</w> <w n="18.9">qu</w>’<w n="18.10">on</w> <w n="18.11">longe</w></l>
						<l n="19" num="5.3"><space unit="char" quantity="16"></space><w n="19.1">Et</w> <w n="19.2">qu</w>’<w n="19.3">on</w> <w n="19.4">relonge</w>,</l>
						<l n="20" num="5.4"><w n="20.1">Ce</w> <w n="20.2">petit</w> <w n="20.3">monde</w> <w n="20.4">intime</w> <w n="20.5">et</w> <w n="20.6">naïf</w> <w n="20.7">où</w> <w n="20.8">l</w>’<w n="20.9">œil</w> <w n="20.10">plonge</w> !…</l>
					</lg>
				</div></body></text></TEI>