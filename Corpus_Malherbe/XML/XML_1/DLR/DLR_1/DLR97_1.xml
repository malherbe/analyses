<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">OCCIDENT</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3151 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Occident</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">http://www.poesies.net/luciedelaruemardrueoccident.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Occident</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LA REVUE BLANCHE</publisher>
							<date when="1901">1901</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1901">1901</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
					<p>Certains retraits de vers ont été supprimés conformément à l’édition de référence.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">VESPÉRALES</head><div type="poem" key="DLR97">
					<head type="main">VESPER</head>
					<lg n="1">
						<l n="1" num="1.1"><space unit="char" quantity="4"></space><w n="1.1">Ce</w> <w n="1.2">jour</w> <w n="1.3">d</w>’<w n="1.4">été</w> <w n="1.5">qui</w> <w n="1.6">ne</w> <w n="1.7">finissait</w> <w n="1.8">pas</w></l>
						<l n="2" num="1.2"><w n="2.1">Sombre</w> <w n="2.2">enfin</w> <w n="2.3">dans</w> <w n="2.4">un</w> <w n="2.5">crépuscule</w> <w n="2.6">magnifique</w>,</l>
						<l n="3" num="1.3"><space unit="char" quantity="4"></space><w n="3.1">Ce</w> <w n="3.2">jour</w> <w n="3.3">d</w>’<w n="3.4">été</w> <w n="3.5">qui</w> <w n="3.6">ne</w> <w n="3.7">finissait</w> <w n="3.8">pas</w>.</l>
					</lg>
					<lg n="2">
						<l n="4" num="2.1"><space unit="char" quantity="4"></space><w n="4.1">Viens</w> <w n="4.2">ma</w> <w n="4.3">camarade</w> <w n="4.4">mélancolique</w>,</l>
						<l n="5" num="2.2"><w n="5.1">Mon</w> <w n="5.2">âme</w> ! <w n="5.3">Et</w> <w n="5.4">hantons</w> <w n="5.5">les</w> <w n="5.6">campagnes</w> <w n="5.7">pas</w> <w n="5.8">à</w> <w n="5.9">pas</w>.</l>
						<l n="6" num="2.3"><space unit="char" quantity="4"></space><w n="6.1">Viens</w> <w n="6.2">ma</w> <w n="6.3">camarade</w> <w n="6.4">mélancolique</w> !</l>
					</lg>
					<lg n="3">
						<l n="7" num="3.1"><space unit="char" quantity="4"></space><w n="7.1">La</w> <w n="7.2">crudité</w> <w n="7.3">des</w> <w n="7.4">matins</w> <w n="7.5">et</w> <w n="7.6">midis</w></l>
						<l n="8" num="3.2"><w n="8.1">Éclaboussait</w> <w n="8.2">de</w> <w n="8.3">trop</w> <w n="8.4">de</w> <w n="8.5">soleil</w> <w n="8.6">notre</w> <w n="8.7">rêve</w>,</l>
						<l n="9" num="3.3"><space unit="char" quantity="4"></space><w n="9.1">La</w> <w n="9.2">crudité</w> <w n="9.3">des</w> <w n="9.4">matins</w> <w n="9.5">et</w> <w n="9.6">midis</w>.</l>
					</lg>
					<lg n="4">
						<l n="10" num="4.1"><space unit="char" quantity="4"></space><w n="10.1">Mais</w> <w n="10.2">voici</w> <w n="10.3">qu</w>’<w n="10.4">une</w> <w n="10.5">aube</w> <w n="10.6">étrange</w> <w n="10.7">se</w> <w n="10.8">lève</w></l>
						<l n="11" num="4.2"><w n="11.1">Au</w> <w n="11.2">cœur</w> <w n="11.3">de</w> <w n="11.4">l</w>’<w n="11.5">horizon</w> <w n="11.6">où</w> <w n="11.7">le</w> <w n="11.8">soleil</w> <w n="11.9">descend</w> ;</l>
						<l n="12" num="4.3"><space unit="char" quantity="4"></space><w n="12.1">Mais</w> <w n="12.2">voici</w> <w n="12.3">qu</w>’<w n="12.4">une</w> <w n="12.5">aube</w> <w n="12.6">étrange</w> <w n="12.7">se</w> <w n="12.8">lève</w> !</l>
					</lg>
					<lg n="5">
						<l n="13" num="5.1"><space unit="char" quantity="4"></space><w n="13.1">Son</w> <w n="13.2">jour</w> <w n="13.3">est</w> <w n="13.4">une</w> <w n="13.5">allusion</w> <w n="13.6">au</w> <w n="13.7">sang</w> ;</l>
						<l n="14" num="5.2"><w n="14.1">C</w>’<w n="14.2">est</w> <w n="14.3">une</w> <w n="14.4">pourpre</w> <w n="14.5">douce</w> <w n="14.6">et</w> <w n="14.7">tiède</w> <w n="14.8">qui</w> <w n="14.9">nous</w> <w n="14.10">baigne</w> ;</l>
						<l n="15" num="5.3"><space unit="char" quantity="4"></space><w n="15.1">Son</w> <w n="15.2">jour</w> <w n="15.3">est</w> <w n="15.4">une</w> <w n="15.5">allusion</w> <w n="15.6">au</w> <w n="15.7">sang</w>.</l>
					</lg>
					<lg n="6">
						<l n="16" num="6.1"><space unit="char" quantity="4"></space><w n="16.1">C</w>’<w n="16.2">est</w> <w n="16.3">de</w> <w n="16.4">l</w>’<w n="16.5">ombre</w> <w n="16.6">qui</w> <w n="16.7">s</w>’<w n="16.8">étale</w> <w n="16.9">et</w> <w n="16.10">qui</w> <w n="16.11">saigne</w>,</l>
						<l n="17" num="6.2"><w n="17.1">Où</w> <w n="17.2">s</w>’<w n="17.3">éveille</w> <w n="17.4">le</w> <w n="17.5">chœur</w> <w n="17.6">de</w> <w n="17.7">tous</w> <w n="17.8">les</w> <w n="17.9">lamentos</w> ;</l>
						<l n="18" num="6.3"><space unit="char" quantity="4"></space><w n="18.1">C</w>’<w n="18.2">est</w> <w n="18.3">de</w> <w n="18.4">l</w>’<w n="18.5">ombre</w> <w n="18.6">qui</w> <w n="18.7">s</w>’<w n="18.8">étale</w> <w n="18.9">et</w> <w n="18.10">qui</w> <w n="18.11">saigne</w>.</l>
					</lg>
					<lg n="7">
						<l n="19" num="7.1"><space unit="char" quantity="4"></space><w n="19.1">Les</w> <w n="19.2">revenants</w> <w n="19.3">y</w> <w n="19.4">traînent</w> <w n="19.5">leurs</w> <w n="19.6">manteaux</w>,</l>
						<l n="20" num="7.2"><w n="20.1">On</w> <w n="20.2">y</w> <w n="20.3">entend</w> <w n="20.4">un</w> <w n="20.5">frôlement</w> <w n="20.6">de</w> <w n="20.7">bêtes</w> <w n="20.8">tristes</w> ;</l>
						<l n="21" num="7.3"><space unit="char" quantity="4"></space><w n="21.1">Les</w> <w n="21.2">revenants</w> <w n="21.3">y</w> <w n="21.4">traînent</w> <w n="21.5">leurs</w> <w n="21.6">manteaux</w>.</l>
					</lg>
					<lg n="8">
						<l n="22" num="8.1"><space unit="char" quantity="4"></space><w n="22.1">Oh</w> ! <w n="22.2">les</w> <w n="22.3">crapauds</w> <w n="22.4">et</w> <w n="22.5">leurs</w> <w n="22.6">goîtres</w> <w n="22.7">flûtistes</w></l>
						<l n="23" num="8.2"><w n="23.1">Et</w> <w n="23.2">les</w> <w n="23.3">chats</w>-<w n="23.4">huants</w> <w n="23.5">y</w> <w n="23.6">appelant</w> <w n="23.7">au</w> <w n="23.8">secours</w> !</l>
						<l n="24" num="8.3"><space unit="char" quantity="4"></space><w n="24.1">Oh</w> ! <w n="24.2">les</w> <w n="24.3">crapauds</w> <w n="24.4">et</w> <w n="24.5">leurs</w> <w n="24.6">goîtres</w> <w n="24.7">flûtistes</w> !</l>
					</lg>
					<lg n="9">
						<l n="25" num="9.1"><space unit="char" quantity="4"></space><w n="25.1">C</w>’<w n="25.2">est</w> <w n="25.3">le</w> <w n="25.4">réveil</w> <w n="25.5">des</w> <w n="25.6">bizarres</w> <w n="25.7">amours</w>,</l>
						<l n="26" num="9.2"><w n="26.1">Des</w> <w n="26.2">sourds</w> <w n="26.3">repentirs</w>, <w n="26.4">des</w> <w n="26.5">solitaires</w> <w n="26.6">suicides</w>,</l>
						<l n="27" num="9.3"><space unit="char" quantity="4"></space><w n="27.1">C</w>’<w n="27.2">est</w> <w n="27.3">le</w> <w n="27.4">réveil</w> <w n="27.5">des</w> <w n="27.6">bizarres</w> <w n="27.7">amours</w>.</l>
					</lg>
					<lg n="10">
						<l n="28" num="10.1"><space unit="char" quantity="4"></space><w n="28.1">L</w>’<w n="28.2">aube</w> <w n="28.3">inverse</w> <w n="28.4">des</w> <w n="28.5">rêveurs</w> <w n="28.6">illucides</w></l>
						<l n="29" num="10.2"><w n="29.1">Pleurant</w> <w n="29.2">des</w> <w n="29.3">chagrins</w> <w n="29.4">faux</w> <w n="29.5">avec</w> <w n="29.6">des</w> <w n="29.7">sanglots</w> <w n="29.8">vrais</w>,</l>
						<l n="30" num="10.3"><space unit="char" quantity="4"></space><w n="30.1">L</w>’<w n="30.2">aube</w> <w n="30.3">inverse</w> <w n="30.4">des</w> <w n="30.5">rêveurs</w> <w n="30.6">illucides</w>.</l>
					</lg>
					<lg n="11">
						<l n="31" num="11.1"><w n="31.1">Et</w> <w n="31.2">nous</w>, <w n="31.3">nous</w> <w n="31.4">y</w> <w n="31.5">crierons</w>, <w n="31.6">qui</w> <w n="31.7">tordons</w> <w n="31.8">vers</w> <w n="31.9">l</w>’<w n="31.10">Après</w></l>
						<l n="32" num="11.2"><w n="32.1">Dans</w> <w n="32.2">un</w> <w n="32.3">geste</w> <w n="32.4">impuissant</w> <w n="32.5">nos</w> <w n="32.6">poignes</w> <w n="32.7">déicides</w> ;</l>
						<l n="33" num="11.3"><w n="33.1">Et</w> <w n="33.2">nous</w>, <w n="33.3">nous</w> <w n="33.4">y</w> <w n="33.5">crierons</w>, <w n="33.6">qui</w> <w n="33.7">tordons</w> <w n="33.8">vers</w> <w n="33.9">l</w>’<w n="33.10">Après</w></l>
					</lg>
					<lg n="12">
						<l n="34" num="12.1"><w n="34.1">Notre</w> <w n="34.2">cœur</w> <w n="34.3">gros</w> <w n="34.4">d</w>’<w n="34.5">angoisse</w> <w n="34.6">et</w> <w n="34.7">de</w> <w n="34.8">mauvais</w> <w n="34.9">secrets</w>.</l>
					</lg>
				</div></body></text></TEI>