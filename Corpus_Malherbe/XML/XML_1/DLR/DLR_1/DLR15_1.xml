<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">OCCIDENT</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3151 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Occident</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">http://www.poesies.net/luciedelaruemardrueoccident.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Occident</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LA REVUE BLANCHE</publisher>
							<date when="1901">1901</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1901">1901</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
					<p>Certains retraits de vers ont été supprimés conformément à l’édition de référence.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">EN PLEIN VENT</head><div type="poem" key="DLR15">
					<head type="main">CONSEILS</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Si</w> <w n="1.2">tu</w> <w n="1.3">viens</w> <w n="1.4">à</w> <w n="1.5">hanter</w> <w n="1.6">le</w> <w n="1.7">parc</w> <w n="1.8">atteint</w> <w n="1.9">d</w>’<w n="1.10">automne</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Passe</w> <w n="2.2">les</w> <w n="2.3">yeux</w> <w n="2.4">mi</w>-<w n="2.5">clos</w> <w n="2.6">sans</w> <w n="2.7">chercher</w> <w n="2.8">le</w> <w n="2.9">détail</w></l>
						<l n="3" num="1.3"><w n="3.1">De</w> <w n="3.2">chaque</w> <w n="3.3">feuille</w> <w n="3.4">chue</w> <w n="3.5">ouverte</w> <w n="3.6">en</w> <w n="3.7">éventail</w></l>
						<l n="4" num="1.4"><w n="4.1">Sous</w> <w n="4.2">les</w> <w n="4.3">lourds</w> <w n="4.4">marronniers</w> <w n="4.5">où</w> <w n="4.6">plus</w> <w n="4.7">rien</w> <w n="4.8">ne</w> <w n="4.9">chantonne</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Ne</w> <w n="5.2">suis</w> <w n="5.3">pas</w> <w n="5.4">du</w> <w n="5.5">profond</w> <w n="5.6">de</w> <w n="5.7">l</w>’<w n="5.8">œil</w>, <w n="5.9">sous</w> <w n="5.10">d</w>’<w n="5.11">autres</w> <w n="5.12">troncs</w>,</l>
						<l n="6" num="2.2"><w n="6.1">D</w>’<w n="6.2">autres</w> <w n="6.3">envols</w> <w n="6.4">muets</w> <w n="6.5">dans</w> <w n="6.6">les</w> <w n="6.7">clairières</w> <w n="6.8">tues</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Ni</w> <w n="7.2">l</w>’<w n="7.3">arabesque</w> <w n="7.4">au</w> <w n="7.5">ciel</w> <w n="7.6">des</w> <w n="7.7">branches</w> <w n="7.8">dévêtues</w>,</l>
						<l n="8" num="2.4"><w n="8.1">Ni</w> <w n="8.2">n</w>’<w n="8.3">entends</w> <w n="8.4">les</w> <w n="8.5">corbeaux</w> <w n="8.6">crier</w> <w n="8.7">aux</w> <w n="8.8">environs</w> ;</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Ne</w> <w n="9.2">te</w> <w n="9.3">retourne</w> <w n="9.4">pas</w> <w n="9.5">vers</w> <w n="9.6">l</w>’<w n="9.7">allée</w> <w n="9.8">où</w> <w n="9.9">peut</w>-<w n="9.10">être</w></l>
						<l n="10" num="3.2"><w n="10.1">Ta</w> <w n="10.2">traîne</w> <w n="10.3">fit</w> <w n="10.4">parmi</w> <w n="10.5">les</w> <w n="10.6">feuilles</w> <w n="10.7">un</w> <w n="10.8">sillon</w>,</l>
						<l n="11" num="3.3"><w n="11.1">Et</w> <w n="11.2">ne</w> <w n="11.3">t</w>’<w n="11.4">attarde</w> <w n="11.5">pas</w> <w n="11.6">en</w> <w n="11.7">quelque</w> <w n="11.8">station</w></l>
						<l n="12" num="3.4"><w n="12.1">Trop</w> <w n="12.2">pensive</w> <w n="12.3">à</w> <w n="12.4">graver</w> <w n="12.5">ton</w> <w n="12.6">chiffre</w> <w n="12.7">sur</w> <w n="12.8">un</w> <w n="12.9">hêtre</w> ;</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Puis</w>, <w n="13.2">ne</w> <w n="13.3">te</w> <w n="13.4">penche</w> <w n="13.5">pas</w> <w n="13.6">à</w> <w n="13.7">voir</w> <w n="13.8">dans</w> <w n="13.9">les</w> <w n="13.10">étangs</w></l>
						<l n="14" num="4.2"><w n="14.1">T</w>’<w n="14.2">apparaître</w> <w n="14.3">à</w> <w n="14.4">rebours</w> <w n="14.5">le</w> <w n="14.6">spectre</w> <w n="14.7">de</w> <w n="14.8">toi</w>-<w n="14.9">même</w></l>
						<l n="15" num="4.3"><w n="15.1">Dans</w> <w n="15.2">l</w>’<w n="15.3">eau</w> <w n="15.4">menteuse</w> <w n="15.5">où</w> <w n="15.6">dort</w> <w n="15.7">un</w> <w n="15.8">paysage</w> <w n="15.9">blême</w></l>
						<l n="16" num="4.4"><w n="16.1">Que</w> <w n="16.2">dérange</w> <w n="16.3">le</w> <w n="16.4">vent</w> <w n="16.5">par</w> <w n="16.6">coups</w> <w n="16.7">intermittents</w> ;</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Même</w>, <w n="17.2">n</w>’<w n="17.3">allonge</w> <w n="17.4">pas</w> <w n="17.5">vers</w> <w n="17.6">les</w> <w n="17.7">dernières</w> <w n="17.8">roses</w></l>
						<l n="18" num="5.2"><w n="18.1">Ta</w> <w n="18.2">main</w> <w n="18.3">qui</w> <w n="18.4">les</w> <w n="18.5">voudrait</w> <w n="18.6">à</w> <w n="18.7">ta</w> <w n="18.8">bouche</w> <w n="18.9">ou</w> <w n="18.10">ton</w> <w n="18.11">cœur</w>,</l>
						<l n="19" num="5.3"><w n="19.1">Plus</w> <w n="19.2">douces</w> <w n="19.3">d</w>’<w n="19.4">avoir</w> <w n="19.5">fait</w> <w n="19.6">éclater</w> <w n="19.7">leur</w> <w n="19.8">fraîcheur</w></l>
						<l n="20" num="5.4"><w n="20.1">Quand</w> <w n="20.2">le</w> <w n="20.3">dehors</w> <w n="20.4">n</w>’<w n="20.5">a</w> <w n="20.6">plus</w> <w n="20.7">que</w> <w n="20.8">des</w> <w n="20.9">choses</w> <w n="20.10">décloses</w> ;</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Et</w> <w n="21.2">quand</w> <w n="21.3">tu</w> <w n="21.4">reviendras</w> <w n="21.5">t</w>’<w n="21.6">asseoir</w> <w n="21.7">au</w> <w n="21.8">coin</w> <w n="21.9">du</w> <w n="21.10">feu</w>,</l>
						<l n="22" num="6.2"><w n="22.1">N</w>’<w n="22.2">y</w> <w n="22.3">reste</w> <w n="22.4">pas</w> <w n="22.5">songeuse</w> <w n="22.6">et</w> <w n="22.7">la</w> <w n="22.8">tête</w> <w n="22.9">pendante</w> ;</l>
						<l n="23" num="6.3"><w n="23.1">Rouvre</w> <w n="23.2">le</w> <w n="23.3">tome</w> <w n="23.4">clos</w> <w n="23.5">sur</w> <w n="23.6">la</w> <w n="23.7">page</w> <w n="23.8">pédante</w>,</l>
						<l n="24" num="6.4"><w n="24.1">Et</w> <w n="24.2">que</w> <w n="24.3">tout</w> <w n="24.4">ton</w> <w n="24.5">esprit</w> <w n="24.6">s</w>’<w n="24.7">y</w> <w n="24.8">plonge</w> <w n="24.9">peu</w> <w n="24.10">à</w> <w n="24.11">peu</w>.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">Car</w> <w n="25.2">si</w> <w n="25.3">ton</w> <w n="25.4">pas</w> <w n="25.5">s</w>’<w n="25.6">attarde</w> <w n="25.7">et</w> <w n="25.8">si</w> <w n="25.9">tu</w> <w n="25.10">te</w> <w n="25.11">reposes</w>,</l>
						<l n="26" num="7.2"><w n="26.1">Si</w> <w n="26.2">tes</w> <w n="26.3">prunelles</w> <w n="26.4">voient</w>, <w n="26.5">si</w> <w n="26.6">ton</w> <w n="26.7">oreille</w> <w n="26.8">entend</w>,</l>
						<l n="27" num="7.3"><w n="27.1">Si</w> <w n="27.2">ta</w> <w n="27.3">face</w> <w n="27.4">se</w> <w n="27.5">penche</w> <w n="27.6">au</w>-<w n="27.7">dessus</w> <w n="27.8">de</w> <w n="27.9">l</w>’<w n="27.10">étang</w></l>
						<l n="28" num="7.4"><w n="28.1">Et</w> <w n="28.2">si</w> <w n="28.3">ton</w> <w n="28.4">geste</w> <w n="28.5">va</w> <w n="28.6">vers</w> <w n="28.7">les</w> <w n="28.8">dernières</w> <w n="28.9">roses</w>,</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1"><w n="29.1">Et</w> <w n="29.2">si</w>, <w n="29.3">près</w> <w n="29.4">du</w> <w n="29.5">foyer</w> <w n="29.6">où</w> <w n="29.7">le</w> <w n="29.8">silence</w> <w n="29.9">dort</w>,</l>
						<l n="30" num="8.2"><w n="30.1">Tu</w> <w n="30.2">restes</w> <w n="30.3">le</w> <w n="30.4">front</w> <w n="30.5">bas</w> <w n="30.6">et</w> <w n="30.7">les</w> <w n="30.8">mains</w> <w n="30.9">désœuvrées</w></l>
						<l n="31" num="8.3"><w n="31.1">A</w> <w n="31.2">voir</w> <w n="31.3">l</w>’<w n="31.4">allusion</w> <w n="31.5">dans</w> <w n="31.6">les</w> <w n="31.7">flammes</w> <w n="31.8">zébrées</w></l>
						<l n="32" num="8.4"><w n="32.1">Des</w> <w n="32.2">feuilles</w> <w n="32.3">de</w> <w n="32.4">novembre</w> <w n="32.5">aux</w> <w n="32.6">larges</w> <w n="32.7">chutes</w> <w n="32.8">d</w>’<w n="32.9">or</w>,</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1"><w n="33.1">Tu</w> <w n="33.2">sentiras</w> <w n="33.3">la</w> <w n="33.4">fin</w> <w n="33.5">s</w>’<w n="33.6">achever</w> <w n="33.7">dans</w> <w n="33.8">ton</w> <w n="33.9">âme</w></l>
						<l n="34" num="9.2"><w n="34.1">De</w> <w n="34.2">tout</w> <w n="34.3">ce</w> <w n="34.4">qui</w> <w n="34.5">se</w> <w n="34.6">meurt</w> <w n="34.7">au</w> <w n="34.8">dehors</w> : <w n="34.9">le</w> <w n="34.10">beau</w> <w n="34.11">temps</w>,</l>
						<l n="35" num="9.3"><w n="35.1">La</w> <w n="35.2">verdure</w>, <w n="35.3">les</w> <w n="35.4">champs</w>, <w n="35.5">le</w> <w n="35.6">chaud</w> <w n="35.7">et</w> <w n="35.8">tes</w> <w n="35.9">vingt</w> <w n="35.10">ans</w></l>
						<l n="36" num="9.4"><w n="36.1">Plus</w> <w n="36.2">pesants</w> <w n="36.3">à</w> <w n="36.4">ton</w> <w n="36.5">cœur</w> <w n="36.6">qu</w>’<w n="36.7">âge</w> <w n="36.8">de</w> <w n="36.9">vieille</w> <w n="36.10">femme</w> ;</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1"><w n="37.1">Et</w> <w n="37.2">vers</w> <w n="37.3">la</w> <w n="37.4">lune</w> <w n="37.5">à</w> <w n="37.6">l</w>’<w n="37.7">heure</w> <w n="37.8">où</w> <w n="37.9">sans</w> <w n="37.10">bruit</w> <w n="37.11">elle</w> <w n="37.12">sort</w></l>
						<l n="38" num="10.2"><w n="38.1">Du</w> <w n="38.2">bois</w> <w n="38.3">que</w> <w n="38.4">lentement</w> <w n="38.5">le</w> <w n="38.6">lent</w> <w n="38.7">automne</w> <w n="38.8">oxyde</w>,</l>
						<l n="39" num="10.3"><w n="39.1">Tu</w> <w n="39.2">lèveras</w> <w n="39.3">des</w> <w n="39.4">yeux</w> <w n="39.5">remplis</w> <w n="39.6">de</w> <w n="39.7">suicide</w></l>
						<l n="40" num="10.4"><w n="40.1">Et</w> <w n="40.2">des</w> <w n="40.3">bras</w> <w n="40.4">déjà</w> <w n="40.5">fous</w> <w n="40.6">du</w> <w n="40.7">geste</w> <w n="40.8">de</w> <w n="40.9">la</w> <w n="40.10">mort</w>.</l>
					</lg>
				</div></body></text></TEI>