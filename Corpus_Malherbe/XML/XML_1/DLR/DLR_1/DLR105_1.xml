<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">OCCIDENT</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3151 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Occident</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">http://www.poesies.net/luciedelaruemardrueoccident.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Occident</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LA REVUE BLANCHE</publisher>
							<date when="1901">1901</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1901">1901</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
					<p>Certains retraits de vers ont été supprimés conformément à l’édition de référence.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">VESPÉRALES</head><div type="poem" key="DLR105">
					<head type="main">BERCEUSE PUÉRILE</head>
					<opener>
						<salute>A Ma Sœur Suzanne.</salute>
					</opener>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Dormez</w>, <w n="1.2">bébé</w> ; <w n="1.3">la</w> <w n="1.4">nuit</w> <w n="1.5">est</w> <w n="1.6">sombre</w>.</l>
						<l n="2" num="1.2"><w n="2.1">Voici</w> <w n="2.2">rôder</w>, <w n="2.3">passant</w> <w n="2.4">dans</w> <w n="2.5">l</w>’<w n="2.6">ombre</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Les</w> <w n="3.2">grands</w> <w n="3.3">loups</w> <w n="3.4">au</w> <w n="3.5">pelage</w> <w n="3.6">noir</w>.</l>
						<l n="4" num="1.4"><w n="4.1">Avec</w> <w n="4.2">leur</w> <w n="4.3">voix</w> <w n="4.4">étrange</w> <w n="4.5">et</w> <w n="4.6">forte</w></l>
						<l n="5" num="1.5"><w n="5.1">Ils</w> <w n="5.2">viendront</w> <w n="5.3">crier</w> <w n="5.4">à</w> <w n="5.5">la</w> <w n="5.6">porte</w>,</l>
						<l n="6" num="1.6"><w n="6.1">Si</w> <w n="6.2">bébé</w> <w n="6.3">ne</w> <w n="6.4">dort</w> <w n="6.5">pas</w> <w n="6.6">ce</w> <w n="6.7">soir</w>.</l>
					</lg>
					<lg n="2">
						<l n="7" num="2.1"><w n="7.1">Dormez</w>, <w n="7.2">bébé</w> ; <w n="7.3">le</w> <w n="7.4">temps</w> <w n="7.5">fait</w> <w n="7.6">rage</w>.</l>
						<l n="8" num="2.2"><w n="8.1">Voici</w> <w n="8.2">passer</w> <w n="8.3">un</w> <w n="8.4">grand</w> <w n="8.5">orage</w>,</l>
						<l n="9" num="2.3"><w n="9.1">On</w> <w n="9.2">l</w>’<w n="9.3">entend</w> <w n="9.4">tout</w> <w n="9.5">là</w>-<w n="9.6">bas</w> <w n="9.7">gémir</w>.</l>
						<l n="10" num="2.4"><w n="10.1">Sur</w> <w n="10.2">son</w> <w n="10.3">aile</w> <w n="10.4">le</w> <w n="10.5">vent</w> <w n="10.6">l</w>’<w n="10.7">apporte</w>,</l>
						<l n="11" num="2.5"><w n="11.1">Il</w> <w n="11.2">viendra</w> <w n="11.3">secouer</w> <w n="11.4">la</w> <w n="11.5">porte</w></l>
						<l n="12" num="2.6"><w n="12.1">Si</w> <w n="12.2">bébé</w> <w n="12.3">ne</w> <w n="12.4">veut</w> <w n="12.5">pas</w> <w n="12.6">dormir</w>.</l>
					</lg>
					<lg n="3">
						<l n="13" num="3.1"><w n="13.1">Dormez</w>, <w n="13.2">bébé</w> ; <w n="13.3">l</w>’<w n="13.4">ange</w> <w n="13.5">vous</w> <w n="13.6">veille</w>.</l>
						<l n="14" num="3.2"><w n="14.1">Quand</w> <w n="14.2">l</w>’<w n="14.3">enfant</w> <w n="14.4">doucement</w> <w n="14.5">sommeille</w></l>
						<l n="15" num="3.3"><w n="15.1">Il</w> <w n="15.2">lui</w> <w n="15.3">dit</w> <w n="15.4">sa</w> <w n="15.5">chanson</w> <w n="15.6">tout</w> <w n="15.7">bas</w> ;</l>
						<l n="16" num="3.4"><w n="16.1">Mais</w> <w n="16.2">il</w> <w n="16.3">serait</w> <w n="16.4">triste</w> <w n="16.5">et</w>, <w n="16.6">sur</w> <w n="16.7">l</w>’<w n="16.8">heure</w>,</l>
						<l n="17" num="3.5"><w n="17.1">S</w>’<w n="17.2">envolerait</w> <w n="17.3">de</w> <w n="17.4">sa</w> <w n="17.5">demeure</w>,</l>
						<l n="18" num="3.6"><w n="18.1">Si</w> <w n="18.2">bébé</w> <w n="18.3">ne</w> <w n="18.4">s</w>’<w n="18.5">endormait</w> <w n="18.6">pas</w>.</l>
					</lg>
				</div></body></text></TEI>