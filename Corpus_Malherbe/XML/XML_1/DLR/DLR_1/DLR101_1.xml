<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">OCCIDENT</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3151 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Occident</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">http://www.poesies.net/luciedelaruemardrueoccident.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Occident</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LA REVUE BLANCHE</publisher>
							<date when="1901">1901</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1901">1901</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
					<p>Certains retraits de vers ont été supprimés conformément à l’édition de référence.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">VESPÉRALES</head><div type="poem" key="DLR101">
					<head type="main">PARFUM AIMÉ</head>
					<opener>
						<salute>A Ma Sœur Alice</salute>
					</opener>
					<lg n="1">
						<l n="1" num="1.1"><space unit="char" quantity="8"></space><w n="1.1">Parfum</w> <w n="1.2">évocateur</w> <w n="1.3">d</w>’<w n="1.4">absents</w></l>
						<l n="2" num="1.2"><w n="2.1">En</w> <w n="2.2">qui</w> <w n="2.3">l</w>’<w n="2.4">être</w> <w n="2.5">bien</w> <w n="2.6">loin</w> <w n="2.7">qu</w>’<w n="2.8">on</w> <w n="2.9">l</w>’<w n="2.10">appelle</w> <w n="2.11">repasse</w></l>
						<l n="3" num="1.3"><space unit="char" quantity="8"></space><w n="3.1">Comme</w> <w n="3.2">un</w> <w n="3.3">fantôme</w> <w n="3.4">plein</w> <w n="3.5">de</w> <w n="3.6">grâce</w></l>
						<l n="4" num="1.4"><w n="4.1">Et</w> <w n="4.2">nous</w> <w n="4.3">reprend</w> <w n="4.4">d</w>’<w n="4.5">un</w> <w n="4.6">coup</w> <w n="4.7">notre</w> <w n="4.8">esprit</w> <w n="4.9">et</w> <w n="4.10">nos</w> <w n="4.11">sens</w>,</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><space unit="char" quantity="8"></space><w n="5.1">Parfum</w>, <w n="5.2">d</w>’<w n="5.3">une</w> <w n="5.4">seule</w> <w n="5.5">fleurée</w></l>
						<l n="6" num="2.2"><w n="6.1">Redisant</w> <w n="6.2">tout</w>, <w n="6.3">regard</w>, <w n="6.4">voix</w>, <w n="6.5">geste</w>, <w n="6.6">allure</w>, <w n="6.7">pas</w>,</l>
						<l n="7" num="2.3"><space unit="char" quantity="8"></space><w n="7.1">Et</w> <w n="7.2">ce</w> <w n="7.3">qui</w> <w n="7.4">ne</w> <w n="7.5">s</w>’<w n="7.6">exprime</w> <w n="7.7">pas</w></l>
						<l n="8" num="2.4"><w n="8.1">Du</w> <w n="8.2">charme</w> <w n="8.3">qu</w>’<w n="8.4">émanait</w> <w n="8.5">sa</w> <w n="8.6">personne</w> <w n="8.7">adorée</w>,</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><space unit="char" quantity="8"></space><w n="9.1">Parfum</w>, <w n="9.2">perle</w> <w n="9.3">en</w> <w n="9.4">laquelle</w> <w n="9.5">tient</w></l>
						<l n="10" num="3.2"><w n="10.1">Mieux</w> <w n="10.2">encore</w> <w n="10.3">que</w> <w n="10.4">ce</w> <w n="10.5">quelqu</w>’<w n="10.6">un</w>, <w n="10.7">son</w> <w n="10.8">ambiance</w>,</l>
						<l n="11" num="3.3"><space unit="char" quantity="8"></space><w n="11.1">Toute</w> <w n="11.2">l</w>’<w n="11.3">époque</w> <w n="11.4">d</w>’<w n="11.5">existence</w></l>
						<l n="12" num="3.4"><w n="12.1">Qu</w>’<w n="12.2">on</w> <w n="12.3">y</w> <w n="12.4">vécut</w>, <w n="12.5">passé</w> <w n="12.6">concentré</w> <w n="12.7">qui</w> <w n="12.8">revient</w>,</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><space unit="char" quantity="8"></space><w n="13.1">Parfum</w>, <w n="13.2">goutte</w> <w n="13.3">sur</w> <w n="13.4">la</w> <w n="13.5">vêture</w>,</l>
						<l n="14" num="4.2"><w n="14.1">Tiédeur</w> <w n="14.2">d</w>’<w n="14.3">une</w> <w n="14.4">présence</w>, <w n="14.5">haleine</w> <w n="14.6">d</w>’<w n="14.7">un</w> <w n="14.8">baiser</w>,</l>
						<l n="15" num="4.3"><space unit="char" quantity="8"></space><w n="15.1">O</w> <w n="15.2">toi</w> <w n="15.3">qui</w> <w n="15.4">sais</w> <w n="15.5">nous</w> <w n="15.6">abuser</w>,</l>
						<l n="16" num="4.4"><w n="16.1">Rends</w>-<w n="16.2">nous</w> <w n="16.3">pour</w> <w n="16.4">un</w> <w n="16.5">instant</w> <w n="16.6">la</w> <w n="16.7">chère</w> <w n="16.8">créature</w>,</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><space unit="char" quantity="8"></space><w n="17.1">Parfum</w>, <w n="17.2">bonheur</w> <w n="17.3">qui</w> <w n="17.4">te</w> <w n="17.5">promets</w></l>
						<l n="18" num="5.2"><w n="18.1">Aux</w> <w n="18.2">cœurs</w> <w n="18.3">heureux</w> <w n="18.4">pour</w> <w n="18.5">qui</w> <w n="18.6">l</w>’<w n="18.7">absence</w> <w n="18.8">sera</w> <w n="18.9">brève</w>,</l>
						<l n="19" num="5.3"><space unit="char" quantity="8"></space><w n="19.1">Parfum</w>, <w n="19.2">hélas</w> ! <w n="19.3">qui</w> <w n="19.4">parachève</w></l>
						<l n="20" num="5.4"><w n="20.1">Le</w> <w n="20.2">lourd</w> <w n="20.3">regret</w> <w n="20.4">des</w> <w n="20.5">cœurs</w> <w n="20.6">séparés</w> <w n="20.7">à</w> <w n="20.8">jamais</w> !</l>
					</lg>
				</div></body></text></TEI>