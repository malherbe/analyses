<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">OCCIDENT</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3151 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Occident</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">http://www.poesies.net/luciedelaruemardrueoccident.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Occident</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LA REVUE BLANCHE</publisher>
							<date when="1901">1901</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1901">1901</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
					<p>Certains retraits de vers ont été supprimés conformément à l’édition de référence.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">L’ÂME DES RUES</head><div type="poem" key="DLR76">
					<head type="main">COUP D’ŒIL</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">J</w>’<w n="1.2">ai</w> <w n="1.3">regardé</w> <w n="1.4">la</w> <w n="1.5">ville</w> <w n="1.6">au</w> <w n="1.7">loin</w> <w n="1.8">de</w> <w n="1.9">tous</w> <w n="1.10">mes</w> <w n="1.11">yeux</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Lourde</w> <w n="2.2">de</w> <w n="2.3">dômes</w> <w n="2.4">ronds</w>, <w n="2.5">lourde</w> <w n="2.6">d</w>’<w n="2.7">arcs</w> <w n="2.8">orgueilleux</w>,</l>
						<l n="3" num="1.3"><space unit="char" quantity="8"></space><w n="3.1">Légère</w> <w n="3.2">de</w> <w n="3.3">son</w> <w n="3.4">trop</w> <w n="3.5">de</w> <w n="3.6">flèches</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Crever</w> <w n="4.2">le</w> <w n="4.3">couchant</w> <w n="4.4">rouge</w> <w n="4.5">avec</w> <w n="4.6">ses</w> <w n="4.7">pointes</w> <w n="4.8">sèches</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">De</w> <w n="5.2">la</w> <w n="5.3">pierre</w> !… <w n="5.4">Le</w> <w n="5.5">fleuve</w> <w n="5.6">en</w> <w n="5.7">passant</w> <w n="5.8">sous</w> <w n="5.9">les</w> <w n="5.10">ponts</w></l>
						<l n="6" num="2.2"><w n="6.1">Redisait</w> <w n="6.2">cette</w> <w n="6.3">pierre</w> <w n="6.4">en</w> <w n="6.5">ses</w> <w n="6.6">fonds</w> <w n="6.7">et</w> <w n="6.8">tréfonds</w>,</l>
						<l n="7" num="2.3"><space unit="char" quantity="8"></space><w n="7.1">Ah</w> ! <w n="7.2">de</w> <w n="7.3">la</w> <w n="7.4">pierre</w>, <w n="7.5">ah</w> ! <w n="7.6">de</w> <w n="7.7">la</w> <w n="7.8">pierre</w> !</l>
						<l n="8" num="2.4"><w n="8.1">Toute</w> <w n="8.2">la</w> <w n="8.3">pierre</w>, <w n="8.4">étau</w> <w n="8.5">géant</w> <w n="8.6">et</w> <w n="8.7">tumulaire</w>,</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Tombale</w> <w n="9.2">ville</w>, <w n="9.3">étau</w> <w n="9.4">des</w> <w n="9.5">rêves</w> <w n="9.6">qui</w> <w n="9.7">s</w>’<w n="9.8">enfuient</w></l>
						<l n="10" num="3.2"><w n="10.1">Dans</w> <w n="10.2">les</w> <w n="10.3">couchants</w> <w n="10.4">avec</w> <w n="10.5">les</w> <w n="10.6">regards</w> <w n="10.7">qui</w> <w n="10.8">s</w>’<w n="10.9">ennuient</w></l>
						<l n="11" num="3.3"><space unit="char" quantity="8"></space><w n="11.1">Des</w> <w n="11.2">enfances</w> <w n="11.3">mélancoliques</w>,</l>
						<l n="12" num="3.4"><w n="12.1">Songeant</w> <w n="12.2">des</w> <w n="12.3">jardins</w> <w n="12.4">verts</w> <w n="12.5">sous</w> <w n="12.6">des</w> <w n="12.7">ciels</w> <w n="12.8">bucoliques</w> !…</l>
					</lg>
				</div></body></text></TEI>