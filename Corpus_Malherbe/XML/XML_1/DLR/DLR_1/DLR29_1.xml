<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">OCCIDENT</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3151 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Occident</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">http://www.poesies.net/luciedelaruemardrueoccident.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Occident</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LA REVUE BLANCHE</publisher>
							<date when="1901">1901</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1901">1901</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
					<p>Certains retraits de vers ont été supprimés conformément à l’édition de référence.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">L’ÂME ET LA MER</head><div type="poem" key="DLR29">
					<head type="main">HYMNE MARIN</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Que</w> <w n="1.2">ta</w> <w n="1.3">toute</w>-<w n="1.4">puissance</w> <w n="1.5">en</w> <w n="1.6">colère</w> <w n="1.7">déferle</w></l>
						<l n="2" num="1.2"><w n="2.1">Ou</w> <w n="2.2">se</w> <w n="2.3">meure</w> <w n="2.4">en</w> <w n="2.5">pâleur</w> <w n="2.6">dans</w> <w n="2.7">les</w> <w n="2.8">couchants</w> <w n="2.9">éteints</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Laiteuse</w> <w n="3.2">dans</w> <w n="3.3">les</w> <w n="3.4">soirs</w> <w n="3.5">comme</w> <w n="3.6">une</w> <w n="3.7">grande</w> <w n="3.8">perle</w></l>
						<l n="4" num="1.4"><w n="4.1">Ou</w> <w n="4.2">bleue</w> <w n="4.3">ou</w> <w n="4.4">noire</w> <w n="4.5">au</w> <w n="4.6">cœur</w> <w n="4.7">des</w> <w n="4.8">midis</w> <w n="4.9">et</w> <w n="4.10">matins</w>,</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">O</w> <w n="5.2">Toi</w> <w n="5.3">matinale</w> ! <w n="5.4">ô</w> <w n="5.5">vespérale</w> ! <w n="5.6">ô</w> <w n="5.7">nocturne</w> !</l>
						<l n="6" num="2.2"><w n="6.1">Quelle</w> <w n="6.2">que</w> <w n="6.3">tu</w> <w n="6.4">sois</w>, <w n="6.5">flux</w> <w n="6.6">qui</w> <w n="6.7">monte</w> <w n="6.8">ou</w> <w n="6.9">redescend</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Toi</w> <w n="7.2">que</w> <w n="7.3">j</w>’<w n="7.4">ai</w> <w n="7.5">mariée</w> <w n="7.6">à</w> <w n="7.7">mon</w> <w n="7.8">cœur</w> <w n="7.9">taciturne</w>,</l>
						<l n="8" num="2.4"><w n="8.1">A</w> <w n="8.2">tout</w> <w n="8.3">ce</w> <w n="8.4">qui</w> <w n="8.5">me</w> <w n="8.6">bat</w> <w n="8.7">dans</w> <w n="8.8">l</w>’<w n="8.9">âme</w> <w n="8.10">et</w> <w n="8.11">dans</w> <w n="8.12">le</w> <w n="8.13">sang</w>,</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Manche</w> <w n="9.2">française</w>, <w n="9.3">mer</w> <w n="9.4">normande</w>, <w n="9.5">mer</w> <w n="9.6">natale</w> !</l>
						<l n="10" num="3.2"><w n="10.1">Grisaille</w> <w n="10.2">coutumière</w> <w n="10.3">au</w> <w n="10.4">bout</w> <w n="10.5">des</w> <w n="10.6">horizons</w>,</l>
						<l n="11" num="3.3"><w n="11.1">Douce</w> <w n="11.2">à</w> <w n="11.3">la</w> <w n="11.4">chair</w> <w n="11.5">et</w> <w n="11.6">douce</w> <w n="11.7">à</w> <w n="11.8">l</w>’<w n="11.9">âme</w> <w n="11.10">occidentale</w>,</l>
						<l n="12" num="3.4"><w n="12.1">Chère</w> <w n="12.2">à</w> <w n="12.3">nos</w> <w n="12.4">prés</w> <w n="12.5">verts</w>, <w n="12.6">souffle</w> <w n="12.7">et</w> <w n="12.8">voix</w> <w n="12.9">de</w> <w n="12.10">nos</w> <w n="12.11">maisons</w>,</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Je</w> <w n="13.2">t</w>’<w n="13.3">aime</w>, <w n="13.4">dans</w> <w n="13.5">ta</w> <w n="13.6">grande</w> <w n="13.7">et</w> <w n="13.8">mystérieuse</w> <w n="13.9">œuvre</w></l>
						<l n="14" num="4.2"><w n="14.1">De</w> <w n="14.2">houle</w> <w n="14.3">et</w> <w n="14.4">de</w> <w n="14.5">repos</w> <w n="14.6">alternants</w>, <w n="14.7">et</w> <w n="14.8">je</w> <w n="14.9">viens</w>,</l>
						<l n="15" num="4.3"><w n="15.1">Je</w> <w n="15.2">cours</w> <w n="15.3">à</w> <w n="15.4">toi</w> <w n="15.5">qui</w> <w n="15.6">loin</w>, <w n="15.7">qui</w> <w n="15.8">près</w> <w n="15.9">m</w>’<w n="15.10">attires</w>, <w n="15.11">pieuvre</w> !</l>
						<l n="16" num="4.4"><w n="16.1">Glauque</w> <w n="16.2">étreinte</w> <w n="16.3">qui</w> <w n="16.4">veux</w> <w n="16.5">nos</w> <w n="16.6">corps</w> <w n="16.7">pour</w> <w n="16.8">tes</w> <w n="16.9">liens</w> !</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">J</w>’<w n="17.2">aime</w> <w n="17.3">en</w> <w n="17.4">la</w> <w n="17.5">flore</w> <w n="17.6">dont</w> <w n="17.7">ta</w> <w n="17.8">profondeur</w> <w n="17.9">regorge</w></l>
						<l n="18" num="5.2"><w n="18.1">La</w> <w n="18.2">sirène</w> <w n="18.3">des</w> <w n="18.4">clairs</w> <w n="18.5">et</w> <w n="18.6">froids</w> <w n="18.7">septentrions</w></l>
						<l n="19" num="5.3"><w n="19.1">Étrange</w> <w n="19.2">et</w> <w n="19.3">toute</w> <w n="19.4">prête</w> <w n="19.5">à</w> <w n="19.6">nous</w> <w n="19.7">prendre</w> <w n="19.8">à</w> <w n="19.9">la</w> <w n="19.10">gorge</w>,</l>
						<l n="20" num="5.4"><w n="20.1">Qui</w> <w n="20.2">rit</w> <w n="20.3">à</w> <w n="20.4">fleur</w> <w n="20.5">des</w> <w n="20.6">eaux</w> <w n="20.7">pour</w> <w n="20.8">que</w> <w n="20.9">nous</w> <w n="20.10">y</w> <w n="20.11">courions</w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">J</w>’<w n="21.2">aime</w> <w n="21.3">l</w>’<w n="21.4">existence</w> <w n="21.5">alme</w> <w n="21.6">et</w> <w n="21.7">grouillante</w> <w n="21.8">et</w> <w n="21.9">si</w> <w n="21.10">chaste</w></l>
						<l n="22" num="6.2"><w n="22.1">Dont</w> <w n="22.2">tu</w> <w n="22.3">vis</w> <w n="22.4">chastement</w>, <w n="22.5">pudique</w> <w n="22.6">qui</w> <w n="22.7">te</w> <w n="22.8">fonds</w>,</l>
						<l n="23" num="6.3"><w n="23.1">Gaie</w> <w n="23.2">ou</w> <w n="23.3">mineure</w>, <w n="23.4">en</w> <w n="23.5">chants</w>, <w n="23.6">dans</w> <w n="23.7">le</w> <w n="23.8">registre</w> <w n="23.9">vaste</w></l>
						<l n="24" num="6.4"><w n="24.1">De</w> <w n="24.2">cette</w> <w n="24.3">sphinge</w> <w n="24.4">occulte</w> <w n="24.5">au</w> <w n="24.6">guet</w> <w n="24.7">sous</w> <w n="24.8">tes</w> <w n="24.9">tréfonds</w>.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">Je</w> <w n="25.2">t</w>’<w n="25.3">aime</w>, <w n="25.4">âme</w> <w n="25.5">des</w> <w n="25.6">yeux</w>, <w n="25.7">regard</w> <w n="25.8">clair</w> <w n="25.9">des</w> <w n="25.10">prunelles</w>,</l>
						<l n="26" num="7.2"><w n="26.1">Comme</w> <w n="26.2">j</w>’<w n="26.3">aime</w> <w n="26.4">les</w> <w n="26.5">yeux</w>, <w n="26.6">comme</w> <w n="26.7">j</w>’<w n="26.8">aime</w> <hi rend="ital"><w n="26.9">des</w></hi> <w n="26.10">yeux</w></l>
						<l n="27" num="7.3"><w n="27.1">Féminins</w> <w n="27.2">que</w> <w n="27.3">j</w>’<w n="27.4">ai</w> <w n="27.5">vus</w>, <w n="27.6">dans</w> <w n="27.7">des</w> <w n="27.8">faces</w> <w n="27.9">charnelles</w>,</l>
						<l n="28" num="7.4"><w n="28.1">Ruisseler</w> <w n="28.2">tous</w> <w n="28.3">tes</w> <w n="28.4">verts</w>, <w n="28.5">tous</w> <w n="28.6">tes</w> <w n="28.7">gris</w>, <w n="28.8">tous</w> <w n="28.9">tes</w> <w n="28.10">bleus</w>.</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1"><w n="29.1">Et</w> <w n="29.2">tu</w> <w n="29.3">es</w> <w n="29.4">belle</w>, <w n="29.5">ampleur</w> <w n="29.6">rude</w> <w n="29.7">et</w> <w n="29.8">préhistorique</w>,</l>
						<l n="30" num="8.2"><w n="30.1">Pareil</w> <w n="30.2">spectacle</w> <w n="30.3">aux</w> <w n="30.4">sens</w> <w n="30.5">modernes</w> <w n="30.6">qu</w>’<w n="30.7">aux</w> <w n="30.8">anciens</w>,</l>
						<l n="31" num="8.3"><w n="31.1">Splendeur</w> <w n="31.2">invariée</w>, <w n="31.3">incorruptible</w> <w n="31.4">rite</w>,</l>
						<l n="32" num="8.4"><w n="32.1">Caprice</w> <w n="32.2">que</w> <w n="32.3">n</w>’<w n="32.4">a</w> <w n="32.5">pu</w> <w n="32.6">l</w>’<w n="32.7">homme</w> <w n="32.8">plier</w> <w n="32.9">aux</w> <w n="32.10">siens</w>.</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1"><w n="33.1">Primordiale</w>, <w n="33.2">libre</w>, <w n="33.3">ô</w> <w n="33.4">libre</w> ! <w n="33.5">et</w> <w n="33.6">toujours</w> <w n="33.7">vierge</w>,</l>
						<l n="34" num="9.2"><w n="34.1">Je</w> <w n="34.2">veux</w> <w n="34.3">t</w>’<w n="34.4">aimer</w>, <w n="34.5">je</w> <w n="34.6">veux</w> <w n="34.7">te</w> <w n="34.8">hanter</w> <w n="34.9">pour</w> <w n="34.10">t</w>’<w n="34.11">aimer</w></l>
						<l n="35" num="9.3"><w n="35.1">Et</w> <w n="35.2">coucher</w> <w n="35.3">mollement</w> <w n="35.4">le</w> <w n="35.5">rêve</w>, <w n="35.6">sur</w> <w n="35.7">ta</w> <w n="35.8">berge</w>,</l>
						<l n="36" num="9.4"><w n="36.1">Que</w> <w n="36.2">tu</w> <w n="36.3">sais</w> <w n="36.4">si</w> <w n="36.5">bien</w> <w n="36.6">mettre</w> <w n="36.7">en</w> <w n="36.8">musique</w> <w n="36.9">et</w> <w n="36.10">rythmer</w> !</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1"><w n="37.1">O</w> <w n="37.2">toi</w> <w n="37.3">qui</w> <w n="37.4">nous</w> <w n="37.5">endors</w> <w n="37.6">du</w> <w n="37.7">sommeil</w> <w n="37.8">hypnotique</w></l>
						<l n="38" num="10.2"><w n="38.1">Des</w> <w n="38.2">iris</w> <w n="38.3">large</w> <w n="38.4">ouverts</w> <w n="38.5">parmi</w> <w n="38.6">tes</w> <w n="38.7">mouvements</w>,</l>
						<l n="39" num="10.3"><w n="39.1">O</w> <w n="39.2">toi</w> <w n="39.3">flot</w> <w n="39.4">de</w> <w n="39.5">l</w>’<w n="39.6">enfance</w>, <w n="39.7">ô</w> <w n="39.8">toi</w> <w n="39.9">berceuse</w> <w n="39.10">antique</w></l>
						<l n="40" num="10.4"><w n="40.1">Où</w> <w n="40.2">rauque</w> <w n="40.3">sourdement</w> <w n="40.4">l</w>’<w n="40.5">âme</w> <w n="40.6">des</w> <w n="40.7">éléments</w> !</l>
					</lg>
				</div></body></text></TEI>