<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">OCCIDENT</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3151 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Occident</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">http://www.poesies.net/luciedelaruemardrueoccident.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Occident</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LA REVUE BLANCHE</publisher>
							<date when="1901">1901</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1901">1901</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
					<p>Certains retraits de vers ont été supprimés conformément à l’édition de référence.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">VESPÉRALES</head><div type="poem" key="DLR126">
					<ab type="star">*</ab>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">L</w>’<w n="1.2">enfance</w> <w n="1.3">gardera</w> <w n="1.4">le</w> <w n="1.5">secret</w> <w n="1.6">de</w> <w n="1.7">ses</w> <w n="1.8">songes</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Son</w> <w n="2.2">petit</w> <w n="2.3">cœur</w> <w n="2.4">profond</w> <w n="2.5">qui</w> <w n="2.6">ne</w> <w n="2.7">s</w>’<w n="2.8">explique</w> <w n="2.9">pas</w> ;</l>
						<l n="3" num="1.3"><w n="3.1">L</w>’<w n="3.2">e</w>,<w n="3.3">fa</w>,<w n="3.4">ce</w> <w n="3.5">ne</w> <w n="3.6">sait</w> <w n="3.7">point</w> <w n="3.8">les</w> <w n="3.9">mots</w> <w n="3.10">et</w> <w n="3.11">leurs</w> <w n="3.12">mensonges</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Elle</w> <w n="4.2">sent</w>, <w n="4.3">elle</w> <w n="4.4">écoute</w>, <w n="4.5">et</w> <w n="4.6">regarde</w> <w n="4.7">tout</w> <w n="4.8">bas</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Quand</w> <w n="5.2">les</w> <w n="5.3">yeux</w> <w n="5.4">puérils</w> <w n="5.5">éteignent</w> <w n="5.6">leur</w> <w n="5.7">lumière</w></l>
						<l n="6" num="2.2"><w n="6.1">Sous</w> <w n="6.2">le</w> <w n="6.3">voile</w> <w n="6.4">rusé</w> <w n="6.5">des</w> <w n="6.6">cils</w> <w n="6.7">adolescents</w>,</l>
						<l n="7" num="2.3"><w n="7.1">L</w>’<w n="7.2">enfance</w> <w n="7.3">morte</w> <w n="7.4">emporte</w> <w n="7.5">au</w> <w n="7.6">passé</w> <w n="7.7">funéraire</w></l>
						<l n="8" num="2.4"><w n="8.1">Son</w> <w n="8.2">trésor</w> <w n="8.3">ignoré</w> <w n="8.4">dans</w> <w n="8.5">ses</w> <w n="8.6">doigts</w> <w n="8.7">innocents</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Mais</w> <w n="9.2">l</w>’<w n="9.3">adulte</w> <w n="9.4">rancœur</w> <w n="9.5">de</w> <w n="9.6">la</w> <w n="9.7">vie</w> <w n="9.8">hypocrite</w></l>
						<l n="10" num="3.2"><w n="10.1">Se</w> <w n="10.2">souvient</w> <w n="10.3">en</w> <w n="10.4">pleurant</w> <w n="10.5">de</w> <w n="10.6">ces</w> <w n="10.7">rêves</w> <w n="10.8">partis</w></l>
						<l n="11" num="3.3"><w n="11.1">Comme</w> <w n="11.2">font</w>, <w n="11.3">sanglotant</w> <w n="11.4">leur</w> <w n="11.5">douleur</w> <w n="11.6">émérite</w>,</l>
						<l n="12" num="3.4"><w n="12.1">Les</w> <w n="12.2">mères</w> <w n="12.3">qui</w> <w n="12.4">s</w>’<w n="12.5">en</w> <w n="12.6">vont</w> <w n="12.7">aux</w> <w n="12.8">tombeaux</w> <w n="12.9">des</w> <w n="12.10">petits</w>…</l>
					</lg>
				</div></body></text></TEI>