<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">OCCIDENT</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3151 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Occident</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">http://www.poesies.net/luciedelaruemardrueoccident.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Occident</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LA REVUE BLANCHE</publisher>
							<date when="1901">1901</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1901">1901</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
					<p>Certains retraits de vers ont été supprimés conformément à l’édition de référence.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">EN PLEIN VENT</head><div type="poem" key="DLR20">
					<head type="main">PRINTEMPS</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Je</w> <w n="1.2">voudrais</w> <w n="1.3">évoquer</w> <w n="1.4">à</w> <w n="1.5">cause</w> <w n="1.6">du</w> <w n="1.7">printemps</w></l>
						<l n="2" num="1.2"><w n="2.1">Quelque</w> <w n="2.2">rêve</w> <w n="2.3">fleurant</w> <w n="2.4">la</w> <w n="2.5">joie</w> <w n="2.6">et</w> <w n="2.7">la</w> <w n="2.8">tendresse</w></l>
						<l n="3" num="1.3"><w n="3.1">Où</w> <w n="3.2">flâneraient</w> <w n="3.3">des</w> <w n="3.4">pas</w> <w n="3.5">d</w>’<w n="3.6">amant</w> <w n="3.7">et</w> <w n="3.8">de</w> <w n="3.9">maîtresse</w></l>
						<l n="4" num="1.4"><w n="4.1">Ivres</w> <w n="4.2">de</w> <w n="4.3">leur</w> <w n="4.4">amour</w> <w n="4.5">et</w> <w n="4.6">de</w> <w n="4.7">leurs</w> <w n="4.8">beaux</w> <w n="4.9">vingt</w> <w n="4.10">ans</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Car</w> <w n="5.2">voici</w> <w n="5.3">sur</w> <w n="5.4">le</w> <w n="5.5">bleu</w> <w n="5.6">des</w> <w n="5.7">ciels</w> <w n="5.8">les</w> <w n="5.9">aubépines</w>,</l>
						<l n="6" num="2.2"><w n="6.1">Roses</w> <w n="6.2">bouquets</w> <w n="6.3">perdant</w> <w n="6.4">au</w> <w n="6.5">vent</w> <w n="6.6">par</w> <w n="6.7">millions</w></l>
						<l n="7" num="2.3"><w n="7.1">Leurs</w> <w n="7.2">pétales</w> <w n="7.3">mêlés</w> <w n="7.4">au</w> <w n="7.5">vol</w> <w n="7.6">des</w> <w n="7.7">papillons</w></l>
						<l n="8" num="2.4"><w n="8.1">Légers</w> <w n="8.2">plus</w> <w n="8.3">follement</w> <w n="8.4">qu</w>’<w n="8.5">un</w> <w n="8.6">pas</w> <w n="8.7">de</w> <w n="8.8">ballerines</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Car</w> <w n="9.2">voici</w> <w n="9.3">susurrer</w> <w n="9.4">les</w> <w n="9.5">sursauts</w> <w n="9.6">clapotants</w></l>
						<l n="10" num="3.2"><w n="10.1">Des</w> <w n="10.2">ruisseaux</w> <w n="10.3">clairs</w> <w n="10.4">en</w> <w n="10.5">qui</w> <w n="10.6">ne</w> <w n="10.7">dort</w> <w n="10.8">aucune</w> <w n="10.9">lie</w>,</l>
						<l n="11" num="3.3"><w n="11.1">Et</w> <w n="11.2">se</w> <w n="11.3">mirer</w> <w n="11.4">déjà</w> <w n="11.5">quelque</w> <w n="11.6">longue</w> <w n="11.7">ancolie</w></l>
						<l n="12" num="3.4"><w n="12.1">Comme</w> <w n="12.2">une</w> <w n="12.3">étoile</w> <w n="12.4">au</w> <w n="12.5">fond</w> <w n="12.6">du</w> <w n="12.7">glauque</w> <w n="12.8">des</w> <w n="12.9">étangs</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Car</w> <w n="13.2">voici</w> <w n="13.3">les</w> <w n="13.4">pigeons</w> <w n="13.5">aux</w> <w n="13.6">saluts</w> <w n="13.7">réciproques</w>,</l>
						<l n="14" num="4.2"><w n="14.1">Le</w> <w n="14.2">cou</w> <w n="14.3">gonflé</w> <w n="14.4">d</w>’<w n="14.5">amour</w> <w n="14.6">et</w> <w n="14.7">de</w> <w n="14.8">roucoulement</w>,</l>
						<l n="15" num="4.3"><w n="15.1">Et</w>, <w n="15.2">comme</w> <w n="15.3">un</w> <w n="15.4">éventail</w> <w n="15.5">étalé</w> <w n="15.6">largement</w>,</l>
						<l n="16" num="4.4"><w n="16.1">Ouvrir</w> <w n="16.2">leur</w> <w n="16.3">roue</w> <w n="16.4">énorme</w> <w n="16.5">et</w> <w n="16.6">riche</w>, <w n="16.7">les</w> <w n="16.8">paons</w> <w n="16.9">rauques</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Car</w> <w n="17.2">les</w> <w n="17.3">échos</w> <w n="17.4">moqueurs</w> <w n="17.5">aux</w> <w n="17.6">gorges</w> <w n="17.7">des</w> <w n="17.8">coucous</w></l>
						<l n="18" num="5.2"><w n="18.1">Et</w> <w n="18.2">les</w> <w n="18.3">rires</w> <w n="18.4">aigus</w> <w n="18.5">d</w>’<w n="18.6">hirondelles</w> <w n="18.7">alertes</w></l>
						<l n="19" num="5.3"><w n="19.1">Et</w> <w n="19.2">les</w> <w n="19.3">cris</w> <w n="19.4">des</w> <w n="19.5">gibiers</w> <w n="19.6">au</w> <w n="19.7">sein</w> <w n="19.8">des</w> <w n="19.9">ombres</w> <w n="19.10">vertes</w>,</l>
						<l n="20" num="5.4"><w n="20.1">Tous</w> <w n="20.2">les</w> <w n="20.3">refrains</w> <w n="20.4">qui</w> <w n="20.5">sont</w> <w n="20.6">au</w> <w n="20.7">fond</w> <w n="20.8">de</w> <w n="20.9">tous</w> <w n="20.10">les</w> <w n="20.11">cous</w>,</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Tout</w> <w n="21.2">ceci</w>, <w n="21.3">tout</w> <w n="21.4">cela</w>, <w n="21.5">l</w>’<w n="21.6">eau</w> <w n="21.7">qui</w> <w n="21.8">court</w>, <w n="21.9">ce</w> <w n="21.10">qui</w> <w n="21.11">passe</w>,</l>
						<l n="22" num="6.2"><w n="22.1">Le</w> <w n="22.2">vent</w> <w n="22.3">et</w> <w n="22.4">la</w> <w n="22.5">nuée</w> <w n="22.6">en</w> <w n="22.7">haut</w> <w n="22.8">et</w> <w n="22.9">le</w> <w n="22.10">sous</w>-<w n="22.11">bois</w></l>
						<l n="23" num="6.3"><w n="23.1">Et</w> <w n="23.2">les</w> <w n="23.3">champs</w> <w n="23.4">et</w> <w n="23.5">la</w> <w n="23.6">route</w> <w n="23.7">et</w> <w n="23.8">les</w> <w n="23.9">fleurs</w> <w n="23.10">et</w> <w n="23.11">les</w> <w n="23.12">voix</w>,</l>
						<l n="24" num="6.4"><w n="24.1">Toute</w> <w n="24.2">cette</w> <w n="24.3">harmonie</w> <w n="24.4">et</w> <w n="24.5">toute</w> <w n="24.6">cette</w> <w n="24.7">grâce</w>,</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">C</w>’<w n="25.2">est</w> <w n="25.3">l</w>’<w n="25.4">accompagnement</w> <w n="25.5">haut</w> <w n="25.6">et</w> <w n="25.7">bas</w> <w n="25.8">tour</w> <w n="25.9">à</w> <w n="25.10">tour</w></l>
						<l n="26" num="7.2"><w n="26.1">Qui</w> <w n="26.2">soutient</w> <w n="26.3">le</w> <w n="26.4">duo</w> <w n="26.5">de</w> <w n="26.6">l</w>’<w n="26.7">homme</w> <w n="26.8">et</w> <w n="26.9">de</w> <w n="26.10">la</w> <w n="26.11">femme</w>,</l>
						<l n="27" num="7.3"><w n="27.1">C</w>’<w n="27.2">est</w> <w n="27.3">tout</w> <w n="27.4">le</w> <w n="27.5">renouveau</w> <w n="27.6">chantant</w> <w n="27.7">l</w>’<w n="27.8">épithalame</w></l>
						<l n="28" num="7.4"><w n="28.1">Pour</w> <w n="28.2">l</w>’<w n="28.3">auguste</w> <w n="28.4">union</w> <w n="28.5">d</w>’<w n="28.6">un</w> <w n="28.7">couple</w> <w n="28.8">dans</w> <w n="28.9">l</w>’<w n="28.10">amour</w> !</l>
					</lg>
				</div></body></text></TEI>