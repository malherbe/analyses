<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">OCCIDENT</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3151 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Occident</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">http://www.poesies.net/luciedelaruemardrueoccident.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Occident</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LA REVUE BLANCHE</publisher>
							<date when="1901">1901</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1901">1901</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
					<p>Certains retraits de vers ont été supprimés conformément à l’édition de référence.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PAROLES I</head><head type="main_subpart">TRILOGIE DU PARTI A PRENDRE</head><div type="poem" key="DLR42">
						<head type="number">I</head>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">Un</w> <w n="1.2">jour</w>, <w n="1.3">tu</w> <w n="1.4">sentiras</w> <w n="1.5">ton</w> <w n="1.6">âme</w> <w n="1.7">fiancée</w></l>
							<l n="2" num="1.2"><w n="2.1">Au</w> <w n="2.2">grand</w>, <w n="2.3">avec</w> <w n="2.4">l</w>’<w n="2.5">instinct</w> <w n="2.6">dès</w> <w n="2.7">lors</w> <w n="2.8">jamais</w> <w n="2.9">ôté</w></l>
							<l n="3" num="1.3"><w n="3.1">D</w>’<w n="3.2">aller</w> <w n="3.3">se</w> <w n="3.4">consoler</w> <w n="3.5">au</w> <w n="3.6">sein</w> <w n="3.7">de</w> <w n="3.8">la</w> <w n="3.9">beauté</w></l>
							<l n="4" num="1.4"><w n="4.1">De</w> <w n="4.2">tout</w> <w n="4.3">ce</w> <w n="4.4">qui</w> <w n="4.5">la</w> <w n="4.6">fit</w> <w n="4.7">douloureuse</w> <w n="4.8">et</w> <w n="4.9">lassée</w>.</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1"><w n="5.1">Or</w>, <w n="5.2">tu</w> <w n="5.3">verras</w> <w n="5.4">le</w> <w n="5.5">monde</w> <w n="5.6">autour</w> <w n="5.7">de</w> <w n="5.8">toi</w> ; <w n="5.9">poussée</w></l>
							<l n="6" num="2.2"><w n="6.1">Coude</w> <w n="6.2">à</w> <w n="6.3">coude</w>, <w n="6.4">cohue</w> <w n="6.5">où</w> <w n="6.6">la</w> <w n="6.7">banalité</w></l>
							<l n="7" num="2.3"><w n="7.1">Domine</w>, <w n="7.2">où</w> <w n="7.3">l</w>’<w n="7.4">implacable</w> <w n="7.5">et</w> <w n="7.6">froise</w> <w n="7.7">vanité</w></l>
							<l n="8" num="2.4"><w n="8.1">Finit</w> <w n="8.2">par</w> <w n="8.3">pervertir</w> <w n="8.4">toute</w> <w n="8.5">haute</w> <w n="8.6">pensée</w>.</l>
						</lg>
						<lg n="3">
							<l n="9" num="3.1"><w n="9.1">Alors</w> <w n="9.2">tu</w> <w n="9.3">rêveras</w> <w n="9.4">de</w> <w n="9.5">quelque</w> <w n="9.6">grand</w> <w n="9.7">lointain</w></l>
							<l n="10" num="3.2"><w n="10.1">Tranquille</w>, <w n="10.2">grave</w>, <w n="10.3">pur</w> <w n="10.4">d</w>’<w n="10.5">ambitions</w> <w n="10.6">pareilles</w>,</l>
							<l n="11" num="3.3"><w n="11.1">Où</w> <w n="11.2">fuir</w> <w n="11.3">les</w> <w n="11.4">yeux</w> <w n="11.5">fermés</w>, <w n="11.6">les</w> <w n="11.7">paumes</w> <w n="11.8">aux</w> <w n="11.9">oreilles</w>,</l>
						</lg>
						<lg n="4">
							<l n="12" num="4.1"><w n="12.1">Pour</w>, <w n="12.2">heureux</w> <w n="12.3">du</w> <w n="12.4">Refuge</w> <w n="12.5">à</w> <w n="12.6">tout</w> <w n="12.7">jamais</w> <w n="12.8">atteint</w>,</l>
							<l n="13" num="4.2"><w n="13.1">Y</w> <w n="13.2">reposer</w> <w n="13.3">ton</w> <w n="13.4">cœur</w> <w n="13.5">et</w> <w n="13.6">ton</w> <w n="13.7">âme</w> <w n="13.8">amoureuse</w></l>
							<l n="14" num="4.3"><w n="14.1">De</w> <w n="14.2">calme</w>, <w n="14.3">hors</w> <w n="14.4">du</w> <w n="14.5">bruit</w> <w n="14.6">de</w> <w n="14.7">l</w>’<w n="14.8">existence</w> <w n="14.9">creuse</w>.</l>
						</lg>
					</div></body></text></TEI>