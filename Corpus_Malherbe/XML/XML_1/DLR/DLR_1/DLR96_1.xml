<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">OCCIDENT</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3151 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Occident</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">http://www.poesies.net/luciedelaruemardrueoccident.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Occident</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LA REVUE BLANCHE</publisher>
							<date when="1901">1901</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1901">1901</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
					<p>Certains retraits de vers ont été supprimés conformément à l’édition de référence.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">L’ENCENSOIR</head><div type="poem" key="DLR96">
					<head type="main">SALUT</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Hélène</w>, <w n="1.2">je</w> <w n="1.3">n</w>’<w n="1.4">ai</w> <w n="1.5">pas</w> <w n="1.6">comme</w> <w n="1.7">vous</w> <w n="1.8">dans</w> <w n="1.9">ma</w> <w n="1.10">vie</w></l>
						<l n="2" num="1.2"><space unit="char" quantity="8"></space><w n="2.1">La</w> <w n="2.2">Muse</w> <w n="2.3">en</w> <w n="2.4">noir</w> <w n="2.5">des</w> <w n="2.6">amours</w> <w n="2.7">morts</w> ;</l>
						<l n="3" num="1.3"><w n="3.1">Jamais</w> <w n="3.2">nulle</w> <w n="3.3">hantise</w> <w n="3.4">encor</w> <w n="3.5">ne</w> <w n="3.6">m</w>’<w n="3.7">a</w> <w n="3.8">suivie</w>,</l>
						<l n="4" num="1.4"><space unit="char" quantity="8"></space><w n="4.1">Ni</w> <w n="4.2">le</w> <w n="4.3">regret</w>, <w n="4.4">ni</w> <w n="4.5">le</w> <w n="4.6">remords</w> ;</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Mon</w> <w n="5.2">cœur</w> <w n="5.3">est</w> <w n="5.4">vierge</w> <w n="5.5">en</w> <w n="5.6">moi</w> <w n="5.7">comme</w> <w n="5.8">ma</w> <w n="5.9">chair</w> <w n="5.10">est</w> <w n="5.11">vierge</w>,</l>
						<l n="6" num="2.2"><space unit="char" quantity="8"></space><w n="6.1">Il</w> <w n="6.2">ne</w> <w n="6.3">porte</w> <w n="6.4">aucun</w> <w n="6.5">secret</w> <w n="6.6">deuil</w>,</l>
						<l n="7" num="2.3"><w n="7.1">L</w>’<w n="7.2">amour</w> <w n="7.3">en</w> <w n="7.4">passant</w> <w n="7.5">frappe</w> <w n="7.6">à</w> <w n="7.7">cette</w> <w n="7.8">tour</w> <w n="7.9">d</w>’<w n="7.10">orgueil</w></l>
						<l n="8" num="2.4"><space unit="char" quantity="8"></space><w n="8.1">Qui</w> <w n="8.2">ne</w> <w n="8.3">se</w> <w n="8.4">livre</w> <w n="8.5">ni</w> <w n="8.6">n</w>’<w n="8.7">héberge</w> ;</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Si</w> <w n="9.2">je</w> <w n="9.3">hurle</w> <w n="9.4">d</w>’<w n="9.5">angoisse</w> <w n="9.6">et</w> <w n="9.7">clame</w> <w n="9.8">de</w> <w n="9.9">désir</w>,</l>
						<l n="10" num="3.2"><space unit="char" quantity="8"></space><w n="10.1">C</w>’<w n="10.2">est</w> <w n="10.3">que</w> <w n="10.4">l</w>’<w n="10.5">existence</w> <w n="10.6">est</w> <w n="10.7">méchante</w>,</l>
						<l n="11" num="3.3"><w n="11.1">C</w>’<w n="11.2">est</w>, <w n="11.3">devant</w> <w n="11.4">l</w>’<w n="11.5">infini</w> <w n="11.6">que</w> <w n="11.7">l</w>’<w n="11.8">on</w> <w n="11.9">ne</w> <w n="11.10">peut</w> <w n="11.11">saisir</w>,</l>
						<l n="12" num="3.4"><space unit="char" quantity="8"></space><w n="12.1">Que</w> <w n="12.2">je</w> <w n="12.3">suis</w> <w n="12.4">toujours</w> <w n="12.5">impuissante</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Et</w> <w n="13.2">mes</w> <w n="13.3">rythmes</w> <w n="13.4">n</w>’<w n="13.5">étant</w> <w n="13.6">pour</w> <w n="13.7">aucun</w> <w n="13.8">bien</w>-<w n="13.9">aimé</w></l>
						<l n="14" num="4.2"><space unit="char" quantity="8"></space><w n="14.1">S</w>’<w n="14.2">en</w> <w n="14.3">vont</w> <w n="14.4">vers</w> <w n="14.5">la</w> <w n="14.6">beauté</w> <w n="14.7">des</w> <w n="14.8">choses</w>,</l>
						<l n="15" num="4.3"><w n="15.1">Vers</w> <w n="15.2">la</w> <w n="15.3">mer</w>, <w n="15.4">vers</w> <w n="15.5">les</w> <w n="15.6">bois</w>, <w n="15.7">vers</w> <w n="15.8">les</w> <w n="15.9">grands</w> <w n="15.10">couchants</w> <w n="15.11">roses</w></l>
						<l n="16" num="4.4"><space unit="char" quantity="8"></space><w n="16.1">En</w> <w n="16.2">qui</w> <w n="16.3">seuls</w> <w n="16.4">vit</w> <w n="16.5">mon</w> <w n="16.6">cœur</w> <w n="16.7">fermé</w> ;</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Et</w> <w n="17.2">ma</w> <w n="17.3">Muse</w> <w n="17.4">nourrie</w> <w n="17.5">à</w> <w n="17.6">la</w> <w n="17.7">même</w> <w n="17.8">mamelle</w></l>
						<l n="18" num="5.2"><space unit="char" quantity="8"></space><w n="18.1">Ignorante</w> <w n="18.2">du</w> <w n="18.3">tendre</w> <w n="18.4">émoi</w></l>
						<l n="19" num="5.3"><w n="19.1">Marche</w> <w n="19.2">violemment</w> <w n="19.3">le</w> <w n="19.4">même</w> <w n="19.5">pas</w> <w n="19.6">que</w> <w n="19.7">moi</w></l>
						<l n="20" num="5.4"><space unit="char" quantity="8"></space><w n="20.1">Comme</w> <w n="20.2">une</w> <w n="20.3">farouche</w> <w n="20.4">jumelle</w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Mais</w> <w n="21.2">la</w> <w n="21.3">virilité</w> <w n="21.4">de</w> <w n="21.5">ce</w> <w n="21.6">souffle</w> <w n="21.7">me</w> <w n="21.8">fait</w></l>
						<l n="22" num="6.2"><space unit="char" quantity="8"></space><w n="22.1">Aimer</w> <w n="22.2">justement</w> <w n="22.3">la</w> <w n="22.4">tendresse</w></l>
						<l n="23" num="6.3"><w n="23.1">Le</w> <w n="23.2">charme</w> <w n="23.3">féminin</w>, <w n="23.4">la</w> <w n="23.5">douceur</w>, <w n="23.6">la</w> <w n="23.7">caresse</w></l>
						<l n="24" num="6.4"><space unit="char" quantity="8"></space><w n="24.1">Que</w> <w n="24.2">contient</w> <w n="24.3">votre</w> <w n="24.4">œuvre</w> <w n="24.5">à</w> <w n="24.6">souhait</w>,</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">Et</w> <w n="25.2">comme</w> <w n="25.3">au</w> <w n="25.4">temps</w> <w n="25.5">jadis</w> <w n="25.6">s</w>’<w n="25.7">inclinait</w> <w n="25.8">jusqu</w>’<w n="25.9">à</w> <w n="25.10">terre</w></l>
						<l n="26" num="7.2"><space unit="char" quantity="8"></space><w n="26.1">Pour</w> <w n="26.2">la</w> <w n="26.3">dame</w> <w n="26.4">le</w> <w n="26.5">chevalier</w>,</l>
						<l n="27" num="7.3"><w n="27.1">Incliner</w> <w n="27.2">devant</w> <w n="27.3">vous</w> <w n="27.4">ma</w> <w n="27.5">tête</w> <w n="27.6">volontaire</w></l>
						<l n="28" num="7.4"><space unit="char" quantity="8"></space><w n="28.1">Que</w> <w n="28.2">l</w>’<w n="28.3">amour</w> <w n="28.4">ne</w> <w n="28.5">sut</w> <w n="28.6">point</w> <w n="28.7">plier</w>.</l>
					</lg>
				</div></body></text></TEI>