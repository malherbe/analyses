<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">OCCIDENT</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3151 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Occident</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">http://www.poesies.net/luciedelaruemardrueoccident.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Occident</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LA REVUE BLANCHE</publisher>
							<date when="1901">1901</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1901">1901</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
					<p>Certains retraits de vers ont été supprimés conformément à l’édition de référence.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">EN PLEIN VENT</head><div type="poem" key="DLR12">
					<head type="main">HEURE AUTOMNALE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Je</w> <w n="1.2">me</w> <w n="1.3">suis</w> <w n="1.4">accoudée</w> <w n="1.5">à</w> <w n="1.6">voir</w> <w n="1.7">mourir</w> <w n="1.8">l</w>’<w n="1.9">automne</w></l>
						<l n="2" num="1.2"><w n="2.1">A</w> <w n="2.2">mon</w> <w n="2.3">petit</w> <w n="2.4">mur</w> <w n="2.5">bas</w>, <w n="2.6">près</w> <w n="2.7">du</w> <w n="2.8">bruit</w> <w n="2.9">monotone</w></l>
						<l n="3" num="1.3"><w n="3.1">De</w> <w n="3.2">la</w> <w n="3.3">source</w> <w n="3.4">tombant</w> <w n="3.5">dans</w> <w n="3.6">le</w> <w n="3.7">vert</w> <w n="3.8">de</w> <w n="3.9">l</w>’<w n="3.10">étang</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Et</w> <w n="4.2">d</w>’<w n="4.3">où</w> <w n="4.4">je</w> <w n="4.5">vois</w> <w n="4.6">les</w> <w n="4.7">prés</w> <w n="4.8">au</w> <w n="4.9">bout</w> <w n="4.10">desquels</w> <w n="4.11">s</w>’<w n="4.12">étend</w></l>
						<l n="5" num="1.5"><w n="5.1">La</w> <w n="5.2">clarté</w> <w n="5.3">de</w> <w n="5.4">la</w> <w n="5.5">mer</w> <w n="5.6">entre</w> <w n="5.7">les</w> <w n="5.8">branches</w> <w n="5.9">rousses</w>.</l>
						<l n="6" num="1.6"><w n="6.1">Les</w> <w n="6.2">oiseaux</w> <w n="6.3">ont</w> <w n="6.4">encor</w> <w n="6.5">quelques</w> <w n="6.6">notes</w> <w n="6.7">très</w> <w n="6.8">douces</w></l>
						<l n="7" num="1.7"><w n="7.1">Et</w> <w n="7.2">remplissent</w> <w n="7.3">tout</w> <w n="7.4">seuls</w> <w n="7.5">avec</w> <w n="7.6">leur</w> <w n="7.7">petit</w> <w n="7.8">chant</w></l>
						<l n="8" num="1.8"><w n="8.1">La</w> <w n="8.2">campagne</w> <w n="8.3">où</w> <w n="8.4">le</w> <w n="8.5">jour</w> <w n="8.6">traîne</w> <w n="8.7">comme</w> <w n="8.8">un</w> <w n="8.9">couchant</w>,</l>
						<l n="9" num="1.9"><w n="9.1">Tant</w> <w n="9.2">y</w> <w n="9.3">stagne</w> <w n="9.4">d</w>’<w n="9.5">ennui</w> <w n="9.6">morne</w> <w n="9.7">et</w> <w n="9.8">de</w> <w n="9.9">léthargie</w> ;</l>
						<l n="10" num="1.10"><w n="10.1">Et</w>, <w n="10.2">près</w> <w n="10.3">de</w> <w n="10.4">moi</w>, <w n="10.5">l</w>’<w n="10.6">été</w> <w n="10.7">parti</w> <w n="10.8">se</w> <w n="10.9">réfugie</w></l>
						<l n="11" num="1.11"><w n="11.1">Tout</w> <w n="11.2">entier</w> <w n="11.3">dans</w> <w n="11.4">le</w> <w n="11.5">cœur</w> <w n="11.6">d</w>’<w n="11.7">une</w> <w n="11.8">dernière</w> <w n="11.9">fleur</w></l>
						<l n="12" num="1.12"><w n="12.1">Que</w> <w n="12.2">pousse</w> <w n="12.3">un</w> <w n="12.4">dahlia</w> <w n="12.5">simple</w>, <w n="12.6">comme</w> <w n="12.7">vainqueur</w></l>
						<l n="13" num="1.13"><w n="13.1">D</w>’<w n="13.2">être</w>, <w n="13.3">seule</w> <w n="13.4">fraîcheur</w> <w n="13.5">du</w> <w n="13.6">dehors</w> <w n="13.7">en</w> <w n="13.8">désastre</w>,</l>
						<l n="14" num="1.14"><w n="14.1">Plus</w> <w n="14.2">rouge</w> <w n="14.3">qu</w>’<w n="14.4">une</w> <w n="14.5">bouche</w>, <w n="14.6">éclatant</w> <w n="14.7">comme</w> <w n="14.8">un</w> <w n="14.9">astre</w>.</l>
					</lg>
				</div></body></text></TEI>