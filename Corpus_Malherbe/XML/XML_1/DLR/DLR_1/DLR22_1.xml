<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">OCCIDENT</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3151 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Occident</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">http://www.poesies.net/luciedelaruemardrueoccident.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Occident</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LA REVUE BLANCHE</publisher>
							<date when="1901">1901</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1901">1901</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
					<p>Certains retraits de vers ont été supprimés conformément à l’édition de référence.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">L’ÂME ET LA MER</head><div type="poem" key="DLR22">
					<head type="main">OMBRES SUR LA MER</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Les</w> <w n="1.2">nuages</w> <w n="1.3">sereins</w> <w n="1.4">stagnant</w> <w n="1.5">au</w>-<w n="1.6">dessus</w> <w n="1.7">d</w>’<w n="1.8">elle</w></l>
						<l n="2" num="1.2"><w n="2.1">L</w>’<w n="2.2">ont</w> <w n="2.3">couverte</w>, <w n="2.4">la</w> <w n="2.5">mer</w>, <w n="2.6">tout</w> <w n="2.7">le</w> <w n="2.8">long</w> <w n="2.9">de</w> <w n="2.10">ses</w> <w n="2.11">eaux</w> :</l>
						<l n="3" num="1.3"><space unit="char" quantity="8"></space><w n="3.1">Là</w>, <w n="3.2">d</w>’<w n="3.3">une</w> <w n="3.4">ombre</w> <w n="3.5">de</w> <w n="3.6">citadelle</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Plus</w> <w n="4.2">loin</w>, <w n="4.3">de</w> <w n="4.4">mille</w> <w n="4.5">toits</w>, <w n="4.6">tours</w>, <w n="4.7">temples</w> <w n="4.8">et</w> <w n="4.9">châteaux</w> ;</l>
						<l n="5" num="1.5"><w n="5.1">Et</w> <w n="5.2">la</w> <w n="5.3">mer</w>, <w n="5.4">qu</w>’<w n="5.5">elle</w> <w n="5.6">soit</w> <w n="5.7">basse</w>, <w n="5.8">haute</w> <w n="5.9">ou</w> <w n="5.10">étale</w>,</l>
						<l n="6" num="1.6"><space unit="char" quantity="8"></space><w n="6.1">Berce</w> <w n="6.2">une</w> <w n="6.3">ville</w> <w n="6.4">capitale</w></l>
						<l n="7" num="1.7"><w n="7.1">En</w> <w n="7.2">ombre</w> <w n="7.3">et</w> <w n="7.4">que</w> <w n="7.5">jamais</w> <w n="7.6">les</w> <w n="7.7">yeux</w> <w n="7.8">ne</w> <w n="7.9">pourront</w> <w n="7.10">voir</w>.</l>
					</lg>
					<lg n="2">
						<l n="8" num="2.1"><w n="8.1">Et</w> <w n="8.2">les</w> <w n="8.3">grands</w> <w n="8.4">peupliers</w> <w n="8.5">ont</w> <w n="8.6">une</w> <w n="8.7">ombre</w> <w n="8.8">si</w> <w n="8.9">grande</w></l>
						<l n="9" num="2.2"><space unit="char" quantity="8"></space><w n="9.1">Dans</w> <w n="9.2">l</w>’<w n="9.3">herbe</w> <w n="9.4">où</w> <w n="9.5">va</w> <w n="9.6">ton</w> <w n="9.7">nonchaloir</w>,</l>
						<l n="10" num="2.3"><w n="10.1">Qu</w>’<w n="10.2">elle</w> <w n="10.3">suit</w> <w n="10.4">tout</w> <w n="10.5">le</w> <w n="10.6">pré</w> <w n="10.7">de</w> <w n="10.8">son</w> <w n="10.9">étroite</w> <w n="10.10">bande</w>,</l>
						<l n="11" num="2.4"><w n="11.1">Descend</w> <w n="11.2">la</w> <w n="11.3">falaise</w> <w n="11.4">et</w> <w n="11.5">s</w>’<w n="11.6">allonge</w> <w n="11.7">dans</w> <w n="11.8">la</w> <w n="11.9">mer</w></l>
						<l n="12" num="2.5"><w n="12.1">Où</w> <w n="12.2">tremble</w> <w n="12.3">ainsi</w>, <w n="12.4">eux</w> <w n="12.5">si</w> <w n="12.6">lointains</w>, <w n="12.7">leur</w> <w n="12.8">faîte</w> <w n="12.9">vert</w>.</l>
					</lg>
					<lg n="3">
						<l n="13" num="3.1"><w n="13.1">Et</w> <w n="13.2">ton</w> <w n="13.3">ombre</w> <w n="13.4">est</w> <w n="13.5">couchée</w> <w n="13.6">aussi</w> <w n="13.7">parmi</w> <w n="13.8">les</w> <w n="13.9">vagues</w>,</l>
						<l n="14" num="3.2"><w n="14.1">Sirène</w> <w n="14.2">noire</w> <w n="14.3">et</w> <w n="14.4">qui</w> <w n="14.5">tend</w> <w n="14.6">à</w> <w n="14.7">tes</w> <w n="14.8">bras</w> <w n="14.9">tendus</w>,</l>
						<l n="15" num="3.3"><w n="15.1">Comme</w> <w n="15.2">une</w> <w n="15.3">allusion</w> <w n="15.4">aux</w> <w n="15.5">baisers</w> <w n="15.6">défendus</w>,</l>
						<l n="16" num="3.4"><w n="16.1">L</w>’<w n="16.2">impossible</w> <w n="16.3">plaisir</w> <w n="16.4">de</w> <w n="16.5">ses</w> <w n="16.6">étreintes</w> <w n="16.7">vagues</w>…</l>
					</lg>
				</div></body></text></TEI>