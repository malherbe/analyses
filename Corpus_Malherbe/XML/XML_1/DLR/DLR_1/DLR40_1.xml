<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">OCCIDENT</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3151 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Occident</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">http://www.poesies.net/luciedelaruemardrueoccident.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Occident</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LA REVUE BLANCHE</publisher>
							<date when="1901">1901</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1901">1901</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
					<p>Certains retraits de vers ont été supprimés conformément à l’édition de référence.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PAROLES I</head><head type="main_subpart">TRILOGIE DU SIGNE DE CROIX</head><div type="poem" key="DLR40">
						<head type="main">LE FILS</head>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">Mais</w> <w n="1.2">voici</w> <w n="1.3">que</w>, <w n="1.4">parmi</w> <w n="1.5">l</w>’<w n="1.6">horrible</w> <w n="1.7">bacchanal</w>,</l>
							<l n="2" num="1.2"><w n="2.1">Le</w> <w n="2.2">Père</w> <w n="2.3">engendre</w> <w n="2.4">un</w> <w n="2.5">Fils</w> <w n="2.6">infiniment</w> <w n="2.7">auguste</w></l>
							<l n="3" num="1.3"><w n="3.1">Dans</w> <w n="3.2">le</w> <w n="3.3">but</w> <w n="3.4">de</w> <w n="3.5">laisser</w> <w n="3.6">à</w> <w n="3.7">l</w>’<w n="3.8">humanité</w> <w n="3.9">fruste</w></l>
							<l n="4" num="1.4"><w n="4.1">L</w>’<w n="4.2">exemple</w> <w n="4.3">de</w> <w n="4.4">son</w> <w n="4.5">être</w> <w n="4.6">aimable</w> <w n="4.7">et</w> <w n="4.8">virginal</w>.</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1"><w n="5.1">Les</w> <w n="5.2">yeux</w> <w n="5.3">levés</w>, <w n="5.4">au</w> <w n="5.5">cours</w> <w n="5.6">de</w> <w n="5.7">son</w> <w n="5.8">chemin</w> <w n="5.9">banal</w>,</l>
							<l n="6" num="2.2"><w n="6.1">Au</w> <w n="6.2">ciel</w> <w n="6.3">d</w>’<w n="6.4">où</w> <w n="6.5">semble</w> <w n="6.6">choir</w> <w n="6.7">chaque</w> <w n="6.8">hasard</w> <w n="6.9">injuste</w>,</l>
							<l n="7" num="2.3"><w n="7.1">Il</w> <w n="7.2">travaille</w>, <w n="7.3">ridant</w> <w n="7.4">ses</w> <w n="7.5">mains</w>, <w n="7.6">ployant</w> <w n="7.7">son</w> <w n="7.8">buste</w></l>
							<l n="8" num="2.4"><w n="8.1">Voués</w> <w n="8.2">pour</w> <w n="8.3">récompense</w> <w n="8.4">au</w> <w n="8.5">supplice</w> <w n="8.6">final</w>,</l>
						</lg>
						<lg n="3">
							<l n="9" num="3.1"><w n="9.1">Voulant</w>, <w n="9.2">hôte</w> <w n="9.3">du</w> <w n="9.4">monde</w> <w n="9.5">haineux</w>, <w n="9.6">vil</w>, <w n="9.7">triste</w>, <w n="9.8">obscène</w>,</l>
							<l n="10" num="3.2"><w n="10.1">Traversant</w>, <w n="10.2">os</w> <w n="10.3">et</w> <w n="10.4">chair</w>, <w n="10.5">sa</w> <w n="10.6">scandaleuse</w> <w n="10.7">scène</w>,</l>
							<l n="11" num="3.3"><w n="11.1">Douer</w> <w n="11.2">d</w>’<w n="11.3">âme</w> <w n="11.4">et</w> <w n="11.5">de</w> <w n="11.6">cœur</w> <w n="11.7">ses</w> <w n="11.8">vivants</w> <w n="11.9">mannequins</w></l>
						</lg>
						<lg n="4">
							<l n="12" num="4.1"><w n="12.1">En</w> <w n="12.2">leur</w> <w n="12.3">montrant</w>, <w n="12.4">par</w> <w n="12.5">sa</w> <w n="12.6">douceur</w> <w n="12.7">à</w> <w n="12.8">leur</w> <w n="12.9">souffrance</w>,</l>
							<l n="13" num="4.2"><w n="13.1">La</w> <w n="13.2">part</w> <w n="13.3">docile</w> <w n="13.4">prise</w> <w n="13.5">à</w> <w n="13.6">leurs</w> <w n="13.7">labeurs</w> <w n="13.8">mesquins</w>,</l>
							<l n="14" num="4.3"><w n="14.1">Comment</w> <w n="14.2">vivre</w> <w n="14.3">en</w> <w n="14.4">courage</w>, <w n="14.5">en</w> <w n="14.6">bien</w>, <w n="14.7">en</w> <w n="14.8">espérance</w>.</l>
						</lg>
					</div></body></text></TEI>