<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">OCCIDENT</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3151 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Occident</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">http://www.poesies.net/luciedelaruemardrueoccident.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Occident</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LA REVUE BLANCHE</publisher>
							<date when="1901">1901</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1901">1901</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
					<p>Certains retraits de vers ont été supprimés conformément à l’édition de référence.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">VESPÉRALES</head><div type="poem" key="DLR127">
					<head type="main">BELLE NUIT</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">La</w> <w n="1.2">lune</w> <w n="1.3">était</w> <w n="1.4">aux</w> <w n="1.5">cieux</w> <w n="1.6">à</w> <w n="1.7">l</w>’<w n="1.8">heure</w> <w n="1.9">de</w> <w n="1.10">minuit</w></l>
						<l n="2" num="1.2"><w n="2.1">Comme</w> <w n="2.2">une</w> <w n="2.3">grande</w> <w n="2.4">perle</w> <w n="2.5">au</w> <w n="2.6">front</w> <w n="2.7">noir</w> <w n="2.8">de</w> <w n="2.9">la</w> <w n="2.10">nuit</w>.</l>
						<l n="3" num="1.3"><w n="3.1">Tout</w> <w n="3.2">dormait</w> <w n="3.3">et</w> <w n="3.4">j</w>’<w n="3.5">étais</w> <w n="3.6">comme</w> <w n="3.7">seule</w> <w n="3.8">sur</w> <w n="3.9">terre</w>.</l>
						<l n="4" num="1.4"><w n="4.1">J</w>’<w n="4.2">ai</w> <w n="4.3">regardé</w> <w n="4.4">la</w> <w n="4.5">lune</w> <w n="4.6">étrange</w> <w n="4.7">et</w> <w n="4.8">solitaire</w></l>
						<l n="5" num="1.5"><w n="5.1">Sur</w> <w n="5.2">laquelle</w> , <w n="5.3">Sapho</w>, <w n="5.4">se</w> <w n="5.5">sont</w> <w n="5.6">fixés</w> <w n="5.7">tes</w> <w n="5.8">yeux</w></l>
						<l n="6" num="1.6"><w n="6.1">Aux</w> <w n="6.2">temps</w> <w n="6.3">antiques</w> <w n="6.4">quand</w>, <w n="6.5">de</w> <w n="6.6">ton</w> <w n="6.7">pas</w> <w n="6.8">orgueilleux</w>,</l>
						<l n="7" num="1.7"><w n="7.1">Tu</w> <w n="7.2">hantais</w> <w n="7.3">par</w> <w n="7.4">les</w> <w n="7.5">nuits</w> <w n="7.6">l</w>’’<w n="7.7">île</w> <w n="7.8">coloniale</w>,</l>
						<l n="8" num="1.8"><w n="8.1">Toute</w> <w n="8.2">seule</w>, <w n="8.3">levant</w> <w n="8.4">ta</w> <w n="8.5">tête</w> <w n="8.6">géniale</w></l>
						<l n="9" num="1.9"><w n="9.1">Vers</w> <w n="9.2">le</w> <w n="9.3">ciel</w> <w n="9.4">où</w> <w n="9.5">mettait</w> <w n="9.6">l</w>’<w n="9.7">astre</w> <w n="9.8">son</w> <w n="9.9">pâle</w> <w n="9.10">jour</w>.</l>
						<l n="10" num="1.10"><w n="10.1">C</w>’<w n="10.2">est</w> <w n="10.3">alors</w> <w n="10.4">qu</w>’<w n="10.5">à</w> <w n="10.6">ta</w> <w n="10.7">lyre</w>, <w n="10.8">ô</w> <w n="10.9">Muse</w> <w n="10.10">de</w> <w n="10.11">l</w>’<w n="10.12">amour</w> !</l>
						<l n="11" num="1.11"><w n="11.1">O</w> <w n="11.2">Muse</w> <w n="11.3">du</w> <w n="11.4">désir</w> <w n="11.5">et</w> <w n="11.6">des</w> <w n="11.7">folles</w> <w n="11.8">tendresses</w>,</l>
						<l n="12" num="1.12"><w n="12.1">Frissonnaient</w> <w n="12.2">tes</w> <w n="12.3">beaux</w> <w n="12.4">doigts</w> <w n="12.5">habiles</w> <w n="12.6">aux</w> <w n="12.7">caresses</w></l>
						<l n="13" num="1.13"><w n="13.1">Et</w> <w n="13.2">que</w> <w n="13.3">chantait</w> <w n="13.4">parmi</w> <w n="13.5">la</w> <w n="13.6">marée</w> <w n="13.7">et</w> <w n="13.8">les</w> <w n="13.9">vents</w></l>
						<l n="14" num="1.14"><w n="14.1">Ta</w> <w n="14.2">bouche</w> <w n="14.3">ivre</w> <w n="14.4">aux</w> <w n="14.5">baisers</w> <w n="14.6">complexes</w> <w n="14.7">et</w> <w n="14.8">savants</w>…</l>
						<l n="15" num="1.15"><w n="15.1">Oh</w> ! <w n="15.2">de</w> <w n="15.3">songer</w> <w n="15.4">tout</w> <w n="15.5">bas</w> <w n="15.6">qu</w>’<w n="15.7">à</w> <w n="15.8">cette</w> <w n="15.9">lune</w> <w n="15.10">blême</w></l>
						<l n="16" num="1.16"><w n="16.1">Tes</w> <w n="16.2">yeux</w> <w n="16.3">s</w>’<w n="16.4">étaient</w> <w n="16.5">rivés</w>, <w n="16.6">grande</w> <w n="16.7">Sapho</w>, <w n="16.8">de</w> <w n="16.9">même</w></l>
						<l n="17" num="1.17"><w n="17.1">Que</w> <w n="17.2">les</w> <w n="17.3">miens</w> <w n="17.4">quand</w>, <w n="17.5">parmi</w> <w n="17.6">le</w> <w n="17.7">sommeil</w> <w n="17.8">de</w> <w n="17.9">la</w> <w n="17.10">nuit</w>,</l>
						<l n="18" num="1.18"><w n="18.1">Je</w> <w n="18.2">veillais</w> <w n="18.3">seule</w> <w n="18.4">avec</w> <w n="18.5">mon</w> <w n="18.6">éternel</w> <w n="18.7">ennui</w> !</l>
						<l n="19" num="1.19"><w n="19.1">Prêtresse</w> <w n="19.2">de</w> <w n="19.3">l</w>’<w n="19.4">amour</w> <w n="19.5">qu</w>’<w n="19.6">ils</w> <w n="19.7">appellent</w> <w n="19.8">infâme</w>,</l>
						<l n="20" num="1.20"><w n="20.1">O</w> <w n="20.2">Sapho</w> ! <w n="20.3">qu</w>’<w n="20.4">a</w> <w n="20.5">donc</w> <w n="20.6">pu</w> <w n="20.7">devenir</w> <w n="20.8">ta</w> <w n="20.9">grande</w> <w n="20.10">âme</w> ?</l>
						<l n="21" num="1.21"><w n="21.1">Sous</w> <w n="21.2">la</w> <w n="21.3">lune</w> <w n="21.4">qui</w> <w n="21.5">vit</w> <w n="21.6">ta</w> <w n="21.7">joie</w> <w n="21.8">et</w> <w n="21.9">ta</w> <w n="21.10">douleur</w>,</l>
						<l n="22" num="1.22"><w n="22.1">Je</w> <w n="22.2">t</w>’<w n="22.3">ai</w> <w n="22.4">chantée</w>, <w n="22.5">aimée</w>, <w n="22.6">admirée</w> <w n="22.7">en</w> <w n="22.8">mon</w> <w n="22.9">cœur</w>,</l>
						<l n="23" num="1.23"><w n="23.1">Moi</w> <w n="23.2">poétesse</w> <w n="23.3">vierge</w>, <w n="23.4">ô</w> <w n="23.5">toi</w> <w n="23.6">la</w> <w n="23.7">poétesse</w></l>
						<l n="24" num="1.24"><w n="24.1">Courtisane</w>, <w n="24.2">ô</w> <w n="24.3">toi</w> <w n="24.4">l</w>’<w n="24.5">aigle</w> <w n="24.6">orgueilleuse</w>, <w n="24.7">l</w>’<w n="24.8">Altesse</w> !</l>
					</lg>
				</div></body></text></TEI>