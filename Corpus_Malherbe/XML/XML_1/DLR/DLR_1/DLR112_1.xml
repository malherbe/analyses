<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">OCCIDENT</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3151 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Occident</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">http://www.poesies.net/luciedelaruemardrueoccident.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Occident</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LA REVUE BLANCHE</publisher>
							<date when="1901">1901</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1901">1901</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
					<p>Certains retraits de vers ont été supprimés conformément à l’édition de référence.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">VESPÉRALES</head><div type="poem" key="DLR112">
					<head type="main">DEUXIÈME NOCTURNE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Je</w> <w n="1.2">rêve</w> <w n="1.3">par</w> <w n="1.4">les</w> <w n="1.5">nuits</w> <w n="1.6">à</w> <w n="1.7">l</w>’<w n="1.8">amour</w> <w n="1.9">qui</w> <w n="1.10">sanglote</w>,</l>
						<l n="2" num="1.2"><space unit="char" quantity="8"></space><w n="2.1">Amours</w> <w n="2.2">non</w> <w n="2.3">sus</w>, <w n="2.4">amours</w> <w n="2.5">trahis</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Amants</w> <w n="3.2">qu</w>’<w n="3.3">on</w> <w n="3.4">n</w>’<w n="3.5">aime</w> <w n="3.6">pas</w>, <w n="3.7">ou</w> <w n="3.8">pire</w>, <w n="3.9">amants</w> <w n="3.10">haïs</w>,</l>
						<l n="4" num="1.4"><space unit="char" quantity="8"></space><w n="4.1">Pleurs</w> <w n="4.2">de</w> <w n="4.3">paria</w>, <w n="4.4">pleurs</w> <w n="4.5">d</w>’<w n="4.6">ilote</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Je</w> <w n="5.2">rêve</w> <w n="5.3">à</w> <w n="5.4">vous</w>, <w n="5.5">cheveux</w> <w n="5.6">blanchissants</w> <w n="5.7">sur</w> <w n="5.8">les</w> <w n="5.9">fronts</w>,</l>
						<l n="6" num="2.2"><space unit="char" quantity="8"></space><w n="6.1">Poings</w> <w n="6.2">qu</w>’<w n="6.3">on</w> <w n="6.4">enfonce</w> <w n="6.5">sans</w> <w n="6.6">les</w> <w n="6.7">bouches</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Détresses</w> <w n="7.2">dans</w> <w n="7.3">secours</w> <w n="7.4">qui</w> <w n="7.5">pâmez</w> <w n="7.6">sur</w> <w n="7.7">des</w> <w n="7.8">couches</w>,</l>
						<l n="8" num="2.4"><space unit="char" quantity="8"></space><w n="8.1">Cœurs</w> <w n="8.2">qui</w> <w n="8.3">saignez</w> <w n="8.4">dans</w> <w n="8.5">des</w> <w n="8.6">girons</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">A</w> <w n="9.2">toi</w>, <w n="9.3">lente</w> <w n="9.4">insomnie</w> <w n="9.5">ouvreuse</w> <w n="9.6">de</w> <w n="9.7">prunelles</w></l>
						<l n="10" num="3.2"><space unit="char" quantity="8"></space><w n="10.1">Dans</w> <w n="10.2">la</w> <w n="10.3">muette</w> <w n="10.4">obscurité</w>,</l>
						<l n="11" num="3.3"><w n="11.1">Où</w> <w n="11.2">la</w> <w n="11.3">mort</w> <w n="11.4">qui</w> <w n="11.5">sourit</w> <w n="11.6">tend</w> <w n="11.7">ses</w> <w n="11.8">mains</w> <w n="11.9">fraternelles</w></l>
						<l n="12" num="3.4"><space unit="char" quantity="8"></space><w n="12.1">Et</w> <w n="12.2">tout</w> <w n="12.3">bas</w> <w n="12.4">parle</w> <w n="12.5">du</w> <w n="12.6">Léthé</w> !</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Et</w> <w n="13.2">telle</w> <w n="13.3">que</w> <w n="13.4">serait</w> <w n="13.5">quelque</w> <w n="13.6">oraison</w> <w n="13.7">nocturne</w></l>
						<l n="14" num="4.2"><space unit="char" quantity="8"></space><w n="14.1">Sans</w> <w n="14.2">signe</w> <w n="14.3">de</w> <w n="14.4">croix</w> <w n="14.5">et</w> <w n="14.6">sans</w> <w n="14.7">mots</w>,</l>
						<l n="15" num="4.3"><w n="15.1">Ma</w> <w n="15.2">pitié</w> <w n="15.3">va</w> <w n="15.4">roder</w> <w n="15.5">à</w> <w n="15.6">l</w>’<w n="15.7">entour</w> <w n="15.8">de</w> <w n="15.9">ces</w> <w n="15.10">maux</w></l>
						<l n="16" num="4.4"><space unit="char" quantity="8"></space><w n="16.1">Comme</w> <w n="16.2">un</w> <w n="16.3">fantôme</w> <w n="16.4">taciturne</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">… <w n="17.1">Ah</w> ! <w n="17.2">qu</w>’<w n="17.3">au</w> <w n="17.4">moins</w>, <w n="17.5">ah</w> ! <w n="17.6">qu</w>’<w n="17.7">au</w> <w n="17.8">moins</w> <w n="17.9">dans</w> <w n="17.10">leur</w> <w n="17.11">être</w> <w n="17.12">béant</w></l>
						<l n="18" num="5.2"><space unit="char" quantity="8"></space><w n="18.1">Où</w> <w n="18.2">le</w> <w n="18.3">glas</w> <w n="18.4">de</w> <w n="18.5">la</w> <w n="18.6">mort</w> <w n="18.7">bourdonne</w>,</l>
						<l n="19" num="5.3"><w n="19.1">Le</w> <w n="19.2">sommeil</w> <w n="19.3">un</w> <w n="19.4">instant</w> <w n="19.5">descende</w>, <w n="19.6">puisqu</w>’<w n="19.7">il</w> <w n="19.8">donne</w></l>
						<l n="20" num="5.4"><space unit="char" quantity="8"></space><w n="20.1">Comme</w> <w n="20.2">un</w> <w n="20.3">avant</w> <w n="20.4">goût</w> <w n="20.5">du</w> <w n="20.6">néant</w> !</l>
					</lg>
				</div></body></text></TEI>