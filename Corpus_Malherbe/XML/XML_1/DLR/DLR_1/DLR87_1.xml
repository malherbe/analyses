<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">OCCIDENT</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3151 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Occident</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">http://www.poesies.net/luciedelaruemardrueoccident.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Occident</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LA REVUE BLANCHE</publisher>
							<date when="1901">1901</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1901">1901</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
					<p>Certains retraits de vers ont été supprimés conformément à l’édition de référence.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">L’ENCENSOIR</head><div type="poem" key="DLR87">
					<head type="main">BUSTE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Ce</w> <w n="1.2">marbre</w> <w n="1.3">dormait</w>, <w n="1.4">brut</w>, <w n="1.5">depuis</w> <w n="1.6">l</w>’<w n="1.7">antiquité</w></l>
						<l n="2" num="1.2"><w n="2.1">Et</w> <w n="2.2">les</w> <w n="2.3">temps</w> <w n="2.4">étaient</w> <w n="2.5">loin</w> <w n="2.6">des</w> <w n="2.7">dieux</w> <w n="2.8">et</w> <w n="2.9">des</w> <w n="2.10">érèbes</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Qu</w>’<w n="3.2">il</w> <w n="3.3">attendait</w> <w n="3.4">encore</w>, <w n="3.5">enfoui</w> <w n="3.6">dans</w> <w n="3.7">les</w> <w n="3.8">glèbes</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Qu</w>’<w n="4.2">un</w> <w n="4.3">maître</w> <w n="4.4">en</w> <w n="4.5">fît</w> <w n="4.6">jaillir</w> <w n="4.7">quelque</w> <w n="4.8">divinité</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Et</w> <w n="5.2">les</w> <w n="5.3">siècles</w>, <w n="5.4">passant</w> <w n="5.5">sur</w> <w n="5.6">lui</w>, <w n="5.7">l</w>’<w n="5.8">ont</w> <w n="5.9">respecté</w></l>
						<l n="6" num="2.2"><w n="6.1">Afin</w> <w n="6.2">qu</w>’<w n="6.3">un</w> <w n="6.4">jour</w>, <w n="6.5">livrée</w> <w n="6.6">à</w> <w n="6.7">l</w>’<w n="6.8">amour</w> <w n="6.9">des</w> <w n="6.10">éphèbes</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Des</w> <w n="7.2">mûrs</w> <w n="7.3">et</w> <w n="7.4">des</w> <w n="7.5">vieillards</w> <w n="7.6">et</w> <w n="7.7">dominant</w> <w n="7.8">les</w> <w n="7.9">plèbes</w>,</l>
						<l n="8" num="2.4"><w n="8.1">L</w>’<w n="8.2">art</w> <w n="8.3">y</w> <w n="8.4">pétrifiât</w> <w n="8.5">pour</w> <w n="8.6">toujours</w> <w n="8.7">ta</w> <w n="8.8">beauté</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Et</w> <w n="9.2">le</w> <w n="9.3">temps</w> <w n="9.4">qui</w> <w n="9.5">détruit</w> <w n="9.6">la</w> <w n="9.7">splendeur</w> <w n="9.8">des</w> <w n="9.9">visages</w></l>
						<l n="10" num="3.2"><w n="10.1">Peut</w>, <w n="10.2">sur</w> <w n="10.3">ton</w> <w n="10.4">effigie</w>, <w n="10.5">entasser</w> <w n="10.6">tous</w> <w n="10.7">ses</w> <w n="10.8">âges</w> :</l>
						<l n="11" num="3.3"><w n="11.1">De</w> <w n="11.2">même</w> <w n="11.3">que</w> <w n="11.4">la</w> <w n="11.5">gloire</w> <w n="11.6">au</w> <w n="11.7">socle</w> <w n="11.8">des</w> <w n="11.9">héros</w></l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1"><w n="12.1">L</w>’<w n="12.2">hommage</w> <w n="12.3">ira</w> <w n="12.4">toujours</w>, <w n="12.5">ainsi</w> <w n="12.6">qu</w>’<w n="12.7">une</w> <w n="12.8">corolle</w></l>
						<l n="13" num="4.2"><w n="13.1">Fleurir</w> <w n="13.2">et</w> <w n="13.3">caresser</w> <w n="13.4">cette</w> <w n="13.5">chair</w> <w n="13.6">de</w> <w n="13.7">Paros</w></l>
						<l n="14" num="4.3"><w n="14.1">Où</w> <w n="14.2">demeure</w> <w n="14.3">à</w> <w n="14.4">jamais</w> <w n="14.5">ton</w> <w n="14.6">sourire</w> <w n="14.7">d</w>’<w n="14.8">idole</w>.</l>
					</lg>
				</div></body></text></TEI>