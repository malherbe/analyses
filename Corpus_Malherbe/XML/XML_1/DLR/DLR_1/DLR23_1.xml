<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">OCCIDENT</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3151 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Occident</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">http://www.poesies.net/luciedelaruemardrueoccident.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Occident</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LA REVUE BLANCHE</publisher>
							<date when="1901">1901</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1901">1901</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
					<p>Certains retraits de vers ont été supprimés conformément à l’édition de référence.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">L’ÂME ET LA MER</head><div type="poem" key="DLR23">
					<head type="main">BERCEUSE MARINE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Mer</w>, <w n="1.2">je</w> <w n="1.3">t</w>’<w n="1.4">entends</w> <w n="1.5">monter</w> <w n="1.6">du</w> <w n="1.7">fond</w> <w n="1.8">de</w> <w n="1.9">l</w>’<w n="1.10">horizon</w>,</l>
						<l n="2" num="1.2"><space unit="char" quantity="8"></space><w n="2.1">Comme</w> <w n="2.2">pour</w> <w n="2.3">engloutir</w> <w n="2.4">le</w> <w n="2.5">monde</w> ;</l>
						<l n="3" num="1.3"><w n="3.1">Mer</w>, <w n="3.2">je</w> <w n="3.3">t</w>’<w n="3.4">entends</w> <w n="3.5">monter</w> <w n="3.6">du</w> <w n="3.7">fond</w> <w n="3.8">de</w> <w n="3.9">l</w>’<w n="3.10">horizon</w> !</l>
					</lg>
					<lg n="2">
						<l n="4" num="2.1"><w n="4.1">Grosse</w> <w n="4.2">de</w> <w n="4.3">la</w> <w n="4.4">fureur</w> <w n="4.5">que</w> <w n="4.6">chaque</w> <w n="4.7">lame</w> <w n="4.8">gronde</w>,</l>
						<l n="5" num="2.2"><w n="5.1">Ta</w> <w n="5.2">grande</w> <w n="5.3">voix</w> <w n="5.4">nocturne</w> <w n="5.5">a</w> <w n="5.6">franchi</w> <w n="5.7">la</w> <w n="5.8">maison</w>,</l>
						<l n="6" num="2.3"><w n="6.1">Grosse</w> <w n="6.2">de</w> <w n="6.3">la</w> <w n="6.4">fureur</w> <w n="6.5">que</w> <w n="6.6">chaque</w> <w n="6.7">lame</w> <w n="6.8">gronde</w>.</l>
					</lg>
					<lg n="3">
						<l n="7" num="3.1"><w n="7.1">Tandis</w> <w n="7.2">que</w> <w n="7.3">le</w> <w n="7.4">sommeil</w> <w n="7.5">vient</w> <w n="7.6">pire</w> <w n="7.7">qu</w>’<w n="7.8">un</w> <w n="7.9">poison</w></l>
						<l n="8" num="3.2"><space unit="char" quantity="8"></space><w n="8.1">M</w>’<w n="8.2">apporte</w> <w n="8.3">ses</w> <w n="8.4">plus</w> <w n="8.5">affreux</w> <w n="8.6">rêves</w> ;</l>
						<l n="9" num="3.3"><w n="9.1">Tandis</w> <w n="9.2">que</w> <w n="9.3">le</w> <w n="9.4">sommeil</w> <w n="9.5">vient</w> <w n="9.6">pire</w> <w n="9.7">qu</w>’<w n="9.8">un</w> <w n="9.9">poison</w>,</l>
					</lg>
					<lg n="4">
						<l n="10" num="4.1"><w n="10.1">O</w> <w n="10.2">toi</w> ! <w n="10.3">hurle</w> <w n="10.4">plus</w> <w n="10.5">fort</w> <w n="10.6">encore</w> <w n="10.7">sur</w> <w n="10.8">les</w> <w n="10.9">grèves</w>,</l>
						<l n="11" num="4.2"><w n="11.1">Que</w> <w n="11.2">je</w> <w n="11.3">t</w>’<w n="11.4">entende</w> <w n="11.5">même</w> <w n="11.6">au</w> <w n="11.7">fond</w> <w n="11.8">de</w> <w n="11.9">l</w>’<w n="11.10">oreiller</w>,</l>
						<l n="12" num="4.3"><w n="12.1">O</w> <w n="12.2">toi</w> ! <w n="12.3">hurle</w> <w n="12.4">plus</w> <w n="12.5">fort</w> <w n="12.6">encore</w> <w n="12.7">sur</w> <w n="12.8">les</w> <w n="12.9">grèves</w> !</l>
					</lg>
					<lg n="5">
						<l n="13" num="5.1"><w n="13.1">Car</w> <w n="13.2">moi</w> <w n="13.3">je</w> <w n="13.4">vais</w> <w n="13.5">dormir</w> <w n="13.6">et</w> <w n="13.7">toi</w> <w n="13.8">tu</w> <w n="13.9">vas</w> <w n="13.10">veiller</w>,</l>
						<l n="14" num="5.2"><space unit="char" quantity="8"></space><w n="14.1">Chantant</w> <w n="14.2">de</w> <w n="14.3">toute</w> <w n="14.4">ta</w> <w n="14.5">marée</w>,</l>
						<l n="15" num="5.3"><w n="15.1">Car</w> <w n="15.2">moi</w> <w n="15.3">je</w> <w n="15.4">vais</w> <w n="15.5">dormir</w> <w n="15.6">et</w> <w n="15.7">toi</w> <w n="15.8">tu</w> <w n="15.9">vas</w> <w n="15.10">veiller</w>.</l>
					</lg>
					<lg n="6">
						<l n="16" num="6.1"><w n="16.1">Ta</w> <w n="16.2">berceuse</w> <w n="16.3">sera</w> <w n="16.4">rude</w> <w n="16.5">et</w> <w n="16.6">désespérée</w> ;</l>
						<l n="17" num="6.2"><w n="17.1">Soufflant</w> <w n="17.2">l</w>’<w n="17.3">horreur</w> <w n="17.4">sans</w> <w n="17.5">trêve</w> <w n="17.6">et</w> <w n="17.7">sans</w> <w n="17.8">rémissions</w>,</l>
						<l n="18" num="6.3"><w n="18.1">Ta</w> <w n="18.2">berceuse</w> <w n="18.3">sera</w> <w n="18.4">rude</w> <w n="18.5">et</w> <w n="18.6">désespérée</w>,</l>
					</lg>
					<lg n="7">
						<l n="19" num="7.1"><w n="19.1">Racontant</w> <w n="19.2">les</w> <w n="19.3">Saphos</w> <w n="19.4">sanglotant</w> <w n="19.5">leurs</w> <w n="19.6">Phaons</w></l>
						<l n="20" num="7.2"><space unit="char" quantity="8"></space><w n="20.1">Du</w> <w n="20.2">haut</w> <w n="20.3">de</w> <w n="20.4">Leucades</w> <w n="20.5">farouches</w>,</l>
						<l n="21" num="7.3"><w n="21.1">Racontant</w> <w n="21.2">les</w> <w n="21.3">Saphos</w> <w n="21.4">sanglotant</w> <w n="21.5">leurs</w> <w n="21.6">Phaons</w>,</l>
					</lg>
					<lg n="8">
						<l n="22" num="8.1"><w n="22.1">Les</w> <w n="22.2">veuvages</w> <w n="22.3">en</w> <w n="22.4">deuil</w> <w n="22.5">criant</w> <w n="22.6">par</w> <w n="22.7">mille</w> <w n="22.8">bouches</w>,</l>
						<l n="23" num="8.2"><w n="23.1">Les</w> <w n="23.2">croix</w> <w n="23.3">de</w> <w n="23.4">mort</w> <w n="23.5">le</w> <w n="23.6">long</w> <w n="23.7">de</w> <w n="23.8">tes</w> <w n="23.9">rivages</w> <w n="23.10">roux</w>,</l>
						<l n="24" num="8.3"><w n="24.1">Les</w> <w n="24.2">veuvages</w> <w n="24.3">en</w> <w n="24.4">deuil</w> <w n="24.5">criant</w> <w n="24.6">par</w> <w n="24.7">mille</w> <w n="24.8">bouches</w>,</l>
					</lg>
					<lg n="9">
						<l n="25" num="9.1"><w n="25.1">Les</w> <w n="25.2">naufrages</w> <w n="25.3">et</w> <w n="25.4">les</w> <w n="25.5">dégâts</w>, <w n="25.6">tous</w> <w n="25.7">tes</w> <w n="25.8">courroux</w>,</l>
						<l n="26" num="9.2"><space unit="char" quantity="8"></space><w n="26.1">Toute</w> <w n="26.2">ta</w> <w n="26.3">sombre</w> <w n="26.4">souvenance</w>,</l>
						<l n="27" num="9.3"><w n="27.1">Les</w> <w n="27.2">naufrages</w> <w n="27.3">et</w> <w n="27.4">les</w> <w n="27.5">dégâts</w>, <w n="27.6">tous</w> <w n="27.7">tes</w> <w n="27.8">courroux</w> !</l>
					</lg>
					<lg n="10">
						<l n="28" num="10.1"><w n="28.1">Chante</w> <w n="28.2">et</w> <w n="28.3">je</w> <w n="28.4">dormirai</w> <w n="28.5">comme</w> <w n="28.6">au</w> <w n="28.7">temps</w> <w n="28.8">de</w> <w n="28.9">l</w>’<w n="28.10">enfance</w></l>
						<l n="29" num="10.2"><w n="29.1">Avec</w> <w n="29.2">ton</w> <w n="29.3">chant</w> <w n="29.4">barbare</w> <w n="29.5">enflant</w> <w n="29.6">l</w>’<w n="29.7">obscurité</w>,</l>
						<l n="30" num="10.3"><w n="30.1">Chante</w> <w n="30.2">et</w> <w n="30.3">je</w> <w n="30.4">dormirai</w> <w n="30.5">comme</w> <w n="30.6">au</w> <w n="30.7">temps</w> <w n="30.8">de</w> <w n="30.9">l</w>’<w n="30.10">enfance</w>,</l>
					</lg>
					<lg n="11">
						<l n="31" num="11.1"><w n="31.1">Avec</w> <w n="31.2">l</w>’<w n="31.3">illusion</w> <w n="31.4">d</w>’<w n="31.5">une</w> <w n="31.6">âme</w> <w n="31.7">à</w> <w n="31.8">mon</w> <w n="31.9">côté</w>.</l>
					</lg>
				</div></body></text></TEI>