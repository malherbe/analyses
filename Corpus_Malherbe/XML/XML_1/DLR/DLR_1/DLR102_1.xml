<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">OCCIDENT</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3151 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Occident</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">http://www.poesies.net/luciedelaruemardrueoccident.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Occident</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LA REVUE BLANCHE</publisher>
							<date when="1901">1901</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1901">1901</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
					<p>Certains retraits de vers ont été supprimés conformément à l’édition de référence.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">VESPÉRALES</head><div type="poem" key="DLR102">
					<ab type="star">*</ab>
					<lg n="1">
						<l n="1" num="1.1"><space unit="char" quantity="6"></space><w n="1.1">Les</w> <w n="1.2">beaux</w> <w n="1.3">jours</w> <w n="1.4">qui</w> <w n="1.5">se</w> <w n="1.6">meurent</w> <w n="1.7">si</w> <w n="1.8">tard</w></l>
						<l n="2" num="1.2"><space unit="char" quantity="6"></space><w n="2.1">Dans</w> <w n="2.2">les</w> <w n="2.3">couchants</w> <w n="2.4">mélancoliques</w>,</l>
						<l n="3" num="1.3"><space unit="char" quantity="6"></space><w n="3.1">Ah</w> ! <w n="3.2">ces</w> <w n="3.3">après</w>-<w n="3.4">midi</w> <w n="3.5">bucoliques</w></l>
						<l n="4" num="1.4"><space unit="char" quantity="6"></space><w n="4.1">Où</w> <w n="4.2">nous</w> <w n="4.3">traînons</w> <w n="4.4">notre</w> <w n="4.5">regard</w></l>
						<l n="5" num="1.5"><space unit="char" quantity="6"></space><w n="5.1">Et</w> <w n="5.2">notre</w> <w n="5.3">pas</w> <w n="5.4">et</w> <w n="5.5">notre</w> <w n="5.6">geste</w></l>
						<l n="6" num="1.6"><space unit="char" quantity="6"></space><w n="6.1">Et</w> <w n="6.2">notre</w> <w n="6.3">contenance</w> <w n="6.4">et</w> <w n="6.5">le</w> <w n="6.6">reste</w>,</l>
						<l n="7" num="1.7"><space unit="char" quantity="6"></space><w n="7.1">La</w> <w n="7.2">marionnette</w> <w n="7.3">et</w> <w n="7.4">tout</w> <w n="7.5">son</w> <w n="7.6">fard</w> !</l>
						<l n="8" num="1.8"><space unit="char" quantity="6"></space><w n="8.1">Les</w> <w n="8.2">beaux</w> <w n="8.3">jours</w> <w n="8.4">qui</w> <w n="8.5">se</w> <w n="8.6">meurent</w> <w n="8.7">si</w> <w n="8.8">tard</w>,</l>
						<l n="9" num="1.9"><space unit="char" quantity="6"></space><w n="9.1">Ces</w> <w n="9.2">beaux</w> <w n="9.3">jours</w> <w n="9.4">d</w>’<w n="9.5">été</w>, <w n="9.6">toute</w> <w n="9.7">leur</w> <w n="9.8">joie</w></l>
						<l n="10" num="1.10"><space unit="char" quantity="6"></space><w n="10.1">Est</w> <w n="10.2">pour</w> <w n="10.3">les</w> <w n="10.4">purs</w> <w n="10.5">et</w> <w n="10.6">simples</w> <w n="10.7">cœurs</w>,</l>
						<l n="11" num="1.11"><space unit="char" quantity="6"></space><w n="11.1">Pour</w> <w n="11.2">les</w> <w n="11.3">croyances</w> <w n="11.4">et</w> <w n="11.5">les</w> <w n="11.6">vigueurs</w> ;</l>
						<l n="12" num="1.12"><space unit="char" quantity="6"></space><w n="12.1">Mais</w> <w n="12.2">pas</w> <w n="12.3">pour</w> <w n="12.4">nous</w> <w n="12.5">que</w> <w n="12.6">l</w>’<w n="12.7">angoisse</w> <w n="12.8">noie</w> !</l>
					</lg>
					<lg n="2">
						<l n="13" num="2.1"><space unit="char" quantity="6"></space>… <w n="13.1">Et</w> <w n="13.2">parce</w> <w n="13.3">qu</w>’<w n="13.4">en</w> <w n="13.5">nous</w> <w n="13.6">rien</w> <w n="13.7">n</w>’<w n="13.8">est</w> <w n="13.9">pareil</w></l>
						<l n="14" num="2.2"><space unit="char" quantity="6"></space><w n="14.1">A</w> <w n="14.2">cette</w> <w n="14.3">fête</w> <w n="14.4">du</w> <w n="14.5">beau</w> <w n="14.6">soleil</w>,</l>
						<l n="15" num="2.3"><space unit="char" quantity="6"></space><w n="15.1">La</w> <w n="15.2">fleur</w> <w n="15.3">si</w> <w n="15.4">triste</w> <w n="15.5">de</w> <w n="15.6">nos</w> <w n="15.7">névroses</w></l>
						<l n="16" num="2.4"><w n="16.1">N</w>’<w n="16.2">épousera</w> <w n="16.3">jamais</w> <w n="16.4">la</w> <w n="16.5">chair</w> <w n="16.6">fraîche</w> <w n="16.7">des</w> <w n="16.8">roses</w>.</l>
					</lg>
				</div></body></text></TEI>