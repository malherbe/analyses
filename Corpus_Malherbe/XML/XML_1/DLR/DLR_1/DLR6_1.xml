<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">OCCIDENT</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3151 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Occident</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">http://www.poesies.net/luciedelaruemardrueoccident.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Occident</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LA REVUE BLANCHE</publisher>
							<date when="1901">1901</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1901">1901</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
					<p>Certains retraits de vers ont été supprimés conformément à l’édition de référence.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">EN PLEIN VENT</head><div type="poem" key="DLR6">
					<head type="main">LES POULES</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">J</w>’<w n="1.2">arpente</w> <w n="1.3">bien</w> <w n="1.4">souvent</w> <w n="1.5">l</w>’<w n="1.6">honnête</w> <w n="1.7">basse</w>-<w n="1.8">cour</w></l>
						<l n="2" num="1.2"><w n="2.1">Pour</w> <w n="2.2">en</w> <w n="2.3">rire</w> <w n="2.4">à</w> <w n="2.5">part</w> <w n="2.6">moi</w>. <w n="2.7">Comme</w> <w n="2.8">des</w> <w n="2.9">gens</w> <w n="2.10">de</w> <w n="2.11">Cour</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Les</w> <w n="3.2">poules</w>, <w n="3.3">au</w> <w n="3.4">soleil</w>, <w n="3.5">s</w>’<w n="3.6">en</w> <w n="3.7">vont</w> <w n="3.8">en</w> <w n="3.9">caravane</w>.</l>
						<l n="4" num="1.4"><w n="4.1">Parmi</w> <w n="4.2">les</w> <w n="4.3">boutons</w> <w n="4.4">d</w>’<w n="4.5">or</w> <w n="4.6">chacune</w> <w n="4.7">se</w> <w n="4.8">pavane</w>,</l>
						<l n="5" num="1.5"><w n="5.1">Jacasse</w>, <w n="5.2">met</w> <w n="5.3">son</w> <w n="5.4">mot</w> <w n="5.5">au</w> <w n="5.6">bruyant</w> <w n="5.7">hourvari</w>,</l>
						<l n="6" num="1.6"><w n="6.1">Et</w>, <w n="6.2">soulevant</w> <w n="6.3">le</w> <w n="6.4">chef</w> <w n="6.5">d</w>’<w n="6.6">un</w> <w n="6.7">geste</w> <w n="6.8">renchéri</w>,</l>
						<l n="7" num="1.7"><w n="7.1">Guette</w> <w n="7.2">d</w>’<w n="7.3">un</w> <w n="7.4">œil</w> <w n="7.5">tout</w> <w n="7.6">rond</w>, <w n="7.7">prête</w> <w n="7.8">à</w> <w n="7.9">prendre</w> <w n="7.10">l</w>’<w n="7.11">alarme</w>,</l>
						<l n="8" num="1.8"><w n="8.1">Le</w> <w n="8.2">gros</w> <w n="8.3">coq</w> <w n="8.4">qui</w> <w n="8.5">s</w>’<w n="8.6">avance</w> <w n="8.7">à</w> <w n="8.8">grands</w> <w n="8.9">pas</w> <w n="8.10">de</w> <w n="8.11">gendarme</w>.</l>
						<l n="9" num="1.9"><w n="9.1">Lui</w>, <w n="9.2">secouant</w> <w n="9.3">au</w> <w n="9.4">vent</w> <w n="9.5">ses</w> <w n="9.6">plumes</w> <w n="9.7">camaïeu</w>,</l>
						<l n="10" num="1.10"><w n="10.1">En</w> <w n="10.2">maître</w> <w n="10.3">de</w> <w n="10.4">sérail</w> <w n="10.5">se</w> <w n="10.6">plante</w> <w n="10.7">au</w> <w n="10.8">beau</w> <w n="10.9">milieu</w>,</l>
						<l n="11" num="1.11"><w n="11.1">Courtise</w> <w n="11.2">tour</w> <w n="11.3">à</w> <w n="11.4">tour</w> <w n="11.5">chaque</w> <w n="11.6">dame</w> <w n="11.7">touffue</w></l>
						<l n="12" num="1.12"><w n="12.1">Et</w>, <w n="12.2">frappant</w> <w n="12.3">le</w> <w n="12.4">sol</w> <w n="12.5">dur</w> <w n="12.6">de</w> <w n="12.7">sa</w> <w n="12.8">patte</w> <w n="12.9">griffue</w>,</l>
						<l n="13" num="1.13"><w n="13.1">Lance</w> <w n="13.2">le</w> <w n="13.3">fier</w> <w n="13.4">appel</w> <w n="13.5">de</w> <w n="13.6">son</w> <w n="13.7">cokeriko</w>.</l>
						<l n="14" num="1.14"><w n="14.1">A</w> <w n="14.2">son</w> <w n="14.3">côté</w>, <w n="14.4">lissant</w> <w n="14.5">sa</w> <w n="14.6">robe</w> <w n="14.7">rococo</w>,</l>
						<l n="15" num="1.15"><w n="15.1">La</w> <w n="15.2">favorite</w> <w n="15.3">cherche</w> <w n="15.4">une</w> <w n="15.5">graine</w> <w n="15.6">oubliée</w> ;</l>
						<l n="16" num="1.16"><w n="16.1">Baissant</w> <w n="16.2">piteusement</w> <w n="16.3">sa</w> <w n="16.4">tête</w> <w n="16.5">humiliée</w>,</l>
						<l n="17" num="1.17"><w n="17.1">Un</w> <w n="17.2">poulet</w>, <w n="17.3">qu</w>’<w n="17.4">un</w> <w n="17.5">essai</w> <w n="17.6">trouva</w> <w n="17.7">trop</w> <w n="17.8">jeune</w> <w n="17.9">encor</w>,</l>
						<l n="18" num="1.18"><w n="18.1">Admire</w> <w n="18.2">le</w> <w n="18.3">pacha</w> <w n="18.4">qu</w>’<w n="18.5">il</w> <w n="18.6">fixe</w> <w n="18.7">d</w>’<w n="18.8">un</w> <w n="18.9">œil</w> <w n="18.10">d</w>’<w n="18.11">or</w></l>
						<l n="19" num="1.19"><w n="19.1">Et</w> <w n="19.2">cherche</w> <w n="19.3">quel</w> <w n="19.4">moyen</w> <w n="19.5">la</w> <w n="19.6">nature</w> <w n="19.7">vous</w> <w n="19.8">prête</w></l>
						<l n="20" num="1.20"><w n="20.1">Pour</w> <w n="20.2">se</w> <w n="20.3">faire</w> <w n="20.4">pousser</w> <w n="20.5">très</w> <w n="20.6">vivement</w> <w n="20.7">la</w> <w n="20.8">crête</w>.</l>
						<l n="21" num="1.21"><w n="21.1">Une</w> <w n="21.2">mère</w>, <w n="21.3">affairée</w> <w n="21.4">à</w> <w n="21.5">ce</w> <w n="21.6">rôle</w> <w n="21.7">tout</w> <w n="21.8">neuf</w>,</l>
						<l n="22" num="1.22"><w n="22.1">Suit</w> <w n="22.2">ses</w> <w n="22.3">poussins</w> <w n="22.4">encor</w> <w n="22.5">ronds</w> <w n="22.6">du</w> <w n="22.7">moule</w> <w n="22.8">de</w> <w n="22.9">l</w>’<w n="22.10">œuf</w>,</l>
						<l n="23" num="1.23"><w n="23.1">Tandis</w> <w n="23.2">qu</w>’<w n="23.3">avec</w> <w n="23.4">cent</w> <w n="23.5">tours</w>, <w n="23.6">cent</w> <w n="23.7">courses</w> <w n="23.8">insensées</w>,</l>
						<l n="24" num="1.24"><w n="24.1">Cou</w> <w n="24.2">tordu</w>, <w n="24.3">bec</w> <w n="24.4">ouvert</w>, <w n="24.5">deux</w> <w n="24.6">poules</w> <w n="24.7">hérissées</w></l>
						<l n="25" num="1.25"><w n="25.1">Cherchent</w> <w n="25.2">à</w> <w n="25.3">se</w> <w n="25.4">reprendre</w> <w n="25.5">une</w> <w n="25.6">croûte</w> <w n="25.7">de</w> <w n="25.8">pain</w></l>
						<l n="26" num="1.26"><w n="26.1">Qu</w>’<w n="26.2">une</w> <w n="26.3">d</w>’<w n="26.4">elles</w> <w n="26.5">vola</w> <w n="26.6">dans</w> <w n="26.7">la</w> <w n="26.8">niche</w> <w n="26.9">au</w> <w n="26.10">lapin</w>.</l>
					</lg>
					<lg n="2">
						<l n="27" num="2.1"><w n="27.1">Et</w> <w n="27.2">le</w> <w n="27.3">soleil</w>, <w n="27.4">luisant</w> <w n="27.5">sur</w> <w n="27.6">tout</w> <w n="27.7">ce</w> <w n="27.8">petit</w> <w n="27.9">monde</w>,</l>
						<l n="28" num="2.2"><w n="28.1">Gambade</w> <w n="28.2">par</w> <w n="28.3">les</w> <w n="28.4">prés</w>, <w n="28.5">s</w>’<w n="28.6">étale</w> <w n="28.7">comme</w> <w n="28.8">une</w> <w n="28.9">onde</w>,</l>
						<l n="29" num="2.3"><w n="29.1">Et</w>, <w n="29.2">tout</w> <w n="29.3">en</w> <w n="29.4">entraînant</w> <w n="29.5">après</w> <w n="29.6">soi</w> <w n="29.7">l</w>’<w n="29.8">univers</w>,</l>
						<l n="30" num="2.4"><w n="30.1">Garde</w> <w n="30.2">de</w> <w n="30.3">chauds</w> <w n="30.4">rayons</w> <w n="30.5">aux</w> <w n="30.6">moindres</w> <w n="30.7">recoins</w> <w n="30.8">verts</w>.</l>
					</lg>
				</div></body></text></TEI>