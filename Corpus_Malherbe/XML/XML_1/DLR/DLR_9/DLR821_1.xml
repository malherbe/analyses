<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">LES SEPT DOULEURS D’OCTOBRE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>Delarue-Mardrus</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2316 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_9</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Sept douleurs d’Octobre</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/luciedelaruemardrueferveur.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Sept douleurs d’Octobre</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>J. Ferenczi et fils, éditeurs</publisher>
							<date when="1930">1930</date>
						</imprint>
					</monogr>
					<note>Édition de référence</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1930">1930</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Il manque de très nombreux poèmes dans le texte source.</p>
				<p>Les nombreux poèmes manquants ont été ajoutés à partir d’une numérisation de l’édition de référence.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-11-28" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">V</head><head type="main_part">CI-GÎT</head><div type="poem" key="DLR821">
					<head type="main">D’AMIENS</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Amiens</w>, <w n="1.2">ta</w> <w n="1.3">cathédrale</w> <w n="1.4">arborescente</w> <w n="1.5">et</w> <w n="1.6">claire</w>,</l>
						<l n="2" num="1.2"><w n="2.1">A</w> <w n="2.2">milieu</w> <w n="2.3">des</w> <w n="2.4">dégâts</w> <w n="2.5">qui</w> <w n="2.6">racontent</w> <w n="2.7">la</w> <w n="2.8">guerre</w>,</l>
						<l n="3" num="1.3"><space unit="char" quantity="16"></space><w n="3.1">Dit</w> <w n="3.2">sa</w> <w n="3.3">prière</w>.</l>
					</lg>
					<lg n="2">
						<l n="4" num="2.1"><w n="4.1">Dehors</w>, <w n="4.2">la</w> <w n="4.3">pluie</w> <w n="4.4">immense</w> <w n="4.5">et</w> <w n="4.6">grise</w> <w n="4.7">fait</w> <w n="4.8">des</w> <w n="4.9">fleuves</w>.</l>
						<l n="5" num="2.2"><w n="5.1">Trous</w> <w n="5.2">d</w>’<w n="5.3">obus</w> <w n="5.4">des</w> <w n="5.5">maisons</w> <w n="5.6">grimaçantes</w> <w n="5.7">et</w> <w n="5.8">veuves</w>,</l>
						<l n="6" num="2.3"><space unit="char" quantity="16"></space><w n="6.1">Ruines</w> <w n="6.2">neuves</w> !</l>
					</lg>
					<lg n="3">
						<l n="7" num="3.1"><w n="7.1">Or</w>, <w n="7.2">dans</w> <w n="7.3">la</w> <w n="7.4">calme</w> <w n="7.5">nef</w>, <w n="7.6">à</w> <w n="7.7">deux</w> <w n="7.8">pas</w> <w n="7.9">des</w> <w n="7.10">désastres</w>,</l>
						<l n="8" num="3.2"><w n="8.1">Il</w> <w n="8.2">y</w> <w n="8.3">a</w> <w n="8.4">doucement</w>, <w n="8.5">derrière</w> <w n="8.6">des</w> <w n="8.7">pilastres</w>,</l>
						<l n="9" num="3.3"><space unit="char" quantity="16"></space><w n="9.1">Des</w> <w n="9.2">levers</w> <w n="9.3">d</w>’<w n="9.4">astres</w>.</l>
					</lg>
					<lg n="4">
						<l n="10" num="4.1"><w n="10.1">L</w>’<w n="10.2">esprit</w> <w n="10.3">qui</w> <w n="10.4">mi</w> <w n="10.5">en</w> <w n="10.6">nous</w> <w n="10.7">tant</w> <w n="10.8">de</w> <w n="10.9">ciel</w> <w n="10.10">et</w> <w n="10.11">de</w> <w n="10.12">fange</w></l>
						<l n="11" num="4.2"><w n="11.1">Aurait</w>-<w n="11.2">il</w> <w n="11.3">donc</w> <w n="11.4">permis</w> <w n="11.5">le</w> <w n="11.6">mariage</w> <w n="11.7">étrange</w></l>
						<l n="12" num="4.3"><space unit="char" quantity="16"></space><w n="12.1">Du</w> <w n="12.2">démon</w> <w n="12.3">avec</w> <w n="12.4">l</w>’<w n="12.5">ange</w>,</l>
					</lg>
					<lg n="5">
						<l n="13" num="5.1"><w n="13.1">Que</w> <w n="13.2">les</w> <w n="13.3">mêmes</w> <w n="13.4">humains</w> <w n="13.5">dont</w> <w n="13.6">l</w>’<w n="13.7">âme</w> <w n="13.8">s</w>’<w n="13.9">évertue</w></l>
						<l n="14" num="5.2"><w n="14.1">Aient</w>, <w n="14.2">après</w> <w n="14.3">toi</w>, <w n="14.4">créé</w>, <w n="14.5">cathédrale</w> <w n="14.6">têtue</w>,</l>
						<l n="15" num="5.3"><space unit="char" quantity="16"></space><w n="15.1">L</w>’<w n="15.2">obus</w> <w n="15.3">qui</w> <w n="15.4">tue</w> ?…</l>
					</lg>
				</div></body></text></TEI>