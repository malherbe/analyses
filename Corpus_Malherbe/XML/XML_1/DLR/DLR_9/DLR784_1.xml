<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">LES SEPT DOULEURS D’OCTOBRE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>Delarue-Mardrus</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2316 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_9</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Sept douleurs d’Octobre</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/luciedelaruemardrueferveur.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Sept douleurs d’Octobre</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>J. Ferenczi et fils, éditeurs</publisher>
							<date when="1930">1930</date>
						</imprint>
					</monogr>
					<note>Édition de référence</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1930">1930</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Il manque de très nombreux poèmes dans le texte source.</p>
				<p>Les nombreux poèmes manquants ont été ajoutés à partir d’une numérisation de l’édition de référence.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-11-28" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">I</head><head type="main_part">L’AUTOMNE</head><div type="poem" key="DLR784">
					<head type="main">LITANIES</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Automne</w> <w n="1.2">aux</w> <w n="1.3">jours</w> <w n="1.4">trop</w> <w n="1.5">courts</w>, <w n="1.6">aux</w> <w n="1.7">nuits</w> <w n="1.8">trop</w> <w n="1.9">tôt</w> <w n="1.10">venues</w></l>
						<l n="2" num="1.2"><space unit="char" quantity="8"></space><w n="2.1">Au</w> <w n="2.2">bout</w> <w n="2.3">des</w> <w n="2.4">longues</w> <w n="2.5">avenues</w>,</l>
					</lg>
					<lg n="2">
						<l n="3" num="2.1"><w n="3.1">Automne</w>, <w n="3.2">sept</w> <w n="3.3">couleurs</w> <w n="3.4">au</w> <w n="3.5">fond</w> <w n="3.6">du</w> <w n="3.7">brouillard</w> <w n="3.8">bleu</w>,</l>
						<l n="4" num="2.2"><space unit="char" quantity="8"></space><w n="4.1">Descente</w> <w n="4.2">des</w> <w n="4.3">langues</w> <w n="4.4">de</w> <w n="4.5">feu</w>,</l>
					</lg>
					<lg n="3">
						<l n="5" num="3.1"><w n="5.1">Automne</w> <w n="5.2">brune</w> <w n="5.3">et</w> <w n="5.4">jaune</w> <w n="5.5">avec</w> <w n="5.6">des</w> <w n="5.7">taches</w> <w n="5.8">rouges</w></l>
						<l n="6" num="3.2"><space unit="char" quantity="8"></space><w n="6.1">Parmi</w> <w n="6.2">le</w> <w n="6.3">vent</w> <w n="6.4">tiède</w> <w n="6.5">où</w> <w n="6.6">tu</w> <w n="6.7">bouges</w>,</l>
					</lg>
					<lg n="4">
						<l n="7" num="4.1"><w n="7.1">Automne</w> <w n="7.2">aux</w> <w n="7.3">feuilles</w> <w n="7.4">d</w>’<w n="7.5">or</w> <w n="7.6">plus</w> <w n="7.7">belles</w> <w n="7.8">que</w> <w n="7.9">des</w> <w n="7.10">fleurs</w>,</l>
						<l n="8" num="4.2"><space unit="char" quantity="8"></space><w n="8.1">Mère</w> <w n="8.2">des</w> <w n="8.3">fruits</w> <w n="8.4">et</w> <w n="8.5">des</w> <w n="8.6">couleurs</w>,</l>
					</lg>
					<lg n="5">
						<l n="9" num="5.1"><w n="9.1">Automne</w> <w n="9.2">du</w> <w n="9.3">mirage</w> <w n="9.4">et</w> <w n="9.5">des</w> <w n="9.6">métamorphoses</w>,</l>
						<l n="10" num="5.2"><space unit="char" quantity="8"></space><w n="10.1">Sourire</w> <w n="10.2">des</w> <w n="10.3">dernières</w> <w n="10.4">roses</w>,</l>
					</lg>
					<lg n="6">
						<l n="11" num="6.1"><w n="11.1">Automne</w>, <w n="11.2">conte</w> <w n="11.3">bleu</w>, <w n="11.4">spectacle</w> <w n="11.5">sans</w> <w n="11.6">pareil</w>,</l>
						<l n="12" num="6.2"><space unit="char" quantity="8"></space><w n="12.1">Ardeur</w> <w n="12.2">dernière</w> <w n="12.3">du</w> <w n="12.4">soleil</w>,</l>
					</lg>
					<lg n="7">
						<l n="13" num="7.1"><w n="13.1">Automne</w> <w n="13.2">claire</w> <w n="13.3">et</w> <w n="13.4">sombre</w>, <w n="13.5">ivre</w> <w n="13.6">et</w> <w n="13.7">désenchantée</w>,</l>
						<l n="14" num="7.2"><space unit="char" quantity="8"></space><w n="14.1">Splendeur</w> <w n="14.2">amèrement</w> <w n="14.3">hantée</w>,</l>
					</lg>
					<lg n="8">
						<l n="15" num="8.1"><w n="15.1">Automne</w> <w n="15.2">dont</w> <w n="15.3">la</w> <w n="15.4">chute</w> <w n="15.5">ouvre</w> <w n="15.6">des</w> <w n="15.7">horizons</w>,</l>
						<l n="16" num="8.2"><space unit="char" quantity="8"></space><w n="16.1">Neige</w> <w n="16.2">en</w> <w n="16.3">flamme</w> <w n="16.4">autour</w> <w n="16.5">des</w> <w n="16.6">maisons</w>,</l>
					</lg>
					<lg n="9">
						<l n="17" num="9.1"><w n="17.1">Automne</w> <w n="17.2">au</w> <w n="17.3">désespoir</w>, <w n="17.4">automne</w> <w n="17.5">aux</w> <w n="17.6">beaux</w> <w n="17.7">désastres</w>,</l>
						<l n="18" num="9.2"><space unit="char" quantity="8"></space><w n="18.1">Dont</w> <w n="18.2">les</w> <w n="18.3">branches</w> <w n="18.4">pleurent</w> <w n="18.5">des</w> <w n="18.6">astres</w>,</l>
					</lg>
					<lg n="10">
						<l n="19" num="10.1"><w n="19.1">Automne</w> <w n="19.2">à</w> <w n="19.3">l</w>’<w n="19.4">abandon</w>, <w n="19.5">automne</w> <w n="19.6">d</w>’<w n="19.7">ombre</w> <w n="19.8">et</w> <w n="19.9">d</w>’<w n="19.10">or</w></l>
						<l n="20" num="10.2"><space unit="char" quantity="8"></space><w n="20.1">Où</w> <w n="20.2">craque</w> <w n="20.3">le</w> <w n="20.4">pas</w> <w n="20.5">de</w> <w n="20.6">la</w> <w n="20.7">Mort</w>,</l>
					</lg>
					<lg n="11">
						<l n="21" num="11.1"><w n="21.1">Automne</w> <w n="21.2">des</w> <w n="21.3">rêveurs</w>, <w n="21.4">automne</w> <w n="21.5">des</w> <w n="21.6">poètes</w>,</l>
						<l n="22" num="11.2"><space unit="char" quantity="8"></space><w n="22.1">Jette</w> <w n="22.2">tes</w> <w n="22.3">feuilles</w> <w n="22.4">sur</w> <w n="22.5">leurs</w> <w n="22.6">têtes</w>.</l>
					</lg>
					<lg n="12">
						<l n="23" num="12.1"><w n="23.1">Automne</w> <w n="23.2">rose</w> <w n="23.3">et</w> <w n="23.4">rousse</w> <w n="23.5">aux</w> <w n="23.6">reflets</w> <w n="23.7">violets</w>,</l>
						<l n="24" num="12.2"><space unit="char" quantity="8"></space><w n="24.1">Dans</w> <w n="24.2">ta</w> <w n="24.3">pourpre</w> <w n="24.4">ensevelis</w>-<w n="24.5">les</w>.</l>
					</lg>
					<lg n="13">
						<l n="25" num="13.1"><w n="25.1">Leurs</w> <w n="25.2">cœurs</w> <w n="25.3">se</w> <w n="25.4">sont</w> <w n="25.5">perdus</w> <w n="25.6">au</w> <w n="25.7">fond</w> <w n="25.8">de</w> <w n="25.9">tes</w> <w n="25.10">grisailles</w>,</l>
						<l n="26" num="13.2"><space unit="char" quantity="8"></space><w n="26.1">Fais</w>-<w n="26.2">leur</w> <w n="26.3">de</w> <w n="26.4">belles</w> <w n="26.5">funérailles</w>.</l>
					</lg>
					<lg n="14">
						<l n="27" num="14.1"><w n="27.1">Leurs</w> <w n="27.2">cœurs</w> <w n="27.3">se</w> <w n="27.4">sont</w> <w n="27.5">perdus</w> <w n="27.6">dans</w> <w n="27.7">ton</w> <w n="27.8">néant</w> <w n="27.9">doré</w>,</l>
						<l n="28" num="14.2"><space unit="char" quantity="8"></space><w n="28.1">Chante</w>-<w n="28.2">leur</w> <w n="28.3">ton</w> <hi rend="ital"><w n="28.4">dies</w> <w n="28.5">iræ</w></hi>.</l>
					</lg>
					<lg n="15">
						<l n="29" num="15.1"><w n="29.1">Leurs</w> <w n="29.2">cœurs</w> <w n="29.3">se</w> <w n="29.4">sont</w> <w n="29.5">perdus</w> <w n="29.6">dans</w> <w n="29.7">tes</w> <w n="29.8">douceurs</w> <w n="29.9">cruelles</w>,</l>
						<l n="30" num="15.2"><space unit="char" quantity="8"></space><w n="30.1">Automne</w>, <w n="30.2">croise</w>-<w n="30.3">leur</w> <w n="30.4">les</w> <w n="30.5">ailes</w>,</l>
					</lg>
					<lg n="16">
						<l n="31" num="16.1"><w n="31.1">Et</w> <w n="31.2">que</w> <w n="31.3">dorment</w> <w n="31.4">en</w> <w n="31.5">paix</w> <w n="31.6">leurs</w> <w n="31.7">pauvres</w> <w n="31.8">cœurs</w> <w n="31.9">lassés</w></l>
						<l n="32" num="16.2"><space unit="char" quantity="8"></space><w n="32.1">Sous</w> <w n="32.2">les</w> <w n="32.3">feuilles</w> <w n="32.4">de</w> <w n="32.5">tes</w> <w n="32.6">fossés</w>.</l>
					</lg>
				</div></body></text></TEI>