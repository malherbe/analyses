<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">LES SEPT DOULEURS D’OCTOBRE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>Delarue-Mardrus</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2316 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_9</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Sept douleurs d’Octobre</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/luciedelaruemardrueferveur.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Sept douleurs d’Octobre</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>J. Ferenczi et fils, éditeurs</publisher>
							<date when="1930">1930</date>
						</imprint>
					</monogr>
					<note>Édition de référence</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1930">1930</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Il manque de très nombreux poèmes dans le texte source.</p>
				<p>Les nombreux poèmes manquants ont été ajoutés à partir d’une numérisation de l’édition de référence.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-11-28" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">VI</head><head type="main_part">SPECTRES</head><div type="poem" key="DLR836">
					<head type="main">LE CERCUEIL VIVANT</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Elle</w>-<w n="1.2">même</w> <w n="1.3">l</w>’<w n="1.4">avait</w> <w n="1.5">choisie</w>,</l>
						<l n="2" num="1.2"><space unit="char" quantity="8"></space>(<w n="2.1">O</w> <w n="2.2">fantaisie</w> !)</l>
						<l n="3" num="1.3"><w n="3.1">Cette</w> <w n="3.2">bière</w> <w n="3.3">d</w>’<w n="3.4">un</w> <w n="3.5">travail</w> <w n="3.6">fin</w></l>
						<l n="4" num="1.4"><w n="4.1">Que</w> <w n="4.2">l</w>’<w n="4.3">on</w> <w n="4.4">vient</w> <w n="4.5">de</w> <w n="4.6">fermer</w> <w n="4.7">enfin</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Elle</w> <w n="5.2">n</w>’<w n="5.3">était</w> <w n="5.4">pas</w> <w n="5.5">étrangère</w>.</l>
						<l n="6" num="2.2"><space unit="char" quantity="8"></space><w n="6.1">Vide</w> <w n="6.2">et</w> <w n="6.3">légère</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Parmi</w> <w n="7.2">d</w>’<w n="7.3">autres</w> <w n="7.4">meubles</w> <w n="7.5">pesants</w></l>
						<l n="8" num="2.4"><w n="8.1">Elle</w> <w n="8.2">attendait</w> <w n="8.3">depuis</w> <w n="8.4">des</w> <w n="8.5">ans</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">La</w> <w n="9.2">funèbre</w> <w n="9.3">plaisanterie</w>,</l>
						<l n="10" num="3.2"><space unit="char" quantity="8"></space><w n="10.1">Sans</w> <w n="10.2">qu</w>’<w n="10.3">on</w> <w n="10.4">en</w> <w n="10.5">rie</w></l>
						<l n="11" num="3.3">— <w n="11.1">Car</w> <w n="11.2">qui</w> <w n="11.3">donc</w> <w n="11.4">y</w> <w n="11.5">pensait</w> <w n="11.6">encor</w> ? —</l>
						<l n="12" num="3.4"><w n="12.1">Tout</w> <w n="12.2">à</w> <w n="12.3">coup</w> <w n="12.4">redevient</w> <w n="12.5">la</w> <w n="12.6">mort</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">U</w> <w n="13.2">n</w> <w n="13.3">matin</w> <w n="13.4">voici</w> <w n="13.5">que</w> <w n="13.6">l</w>’<w n="13.7">on</w> <w n="13.8">visse</w></l>
						<l n="14" num="4.2"><space unit="char" quantity="8"></space><w n="14.1">Le</w> <w n="14.2">bois</w> <w n="14.3">qui</w> <w n="14.4">crisse</w>,</l>
						<l n="15" num="4.3"><w n="15.1">Long</w> <w n="15.2">bruit</w> <w n="15.3">qui</w> <w n="15.4">fait</w> <w n="15.5">grincer</w> <w n="15.6">des</w> <w n="15.7">dents</w>,</l>
						<l n="16" num="4.4">— <w n="16.1">Et</w> <w n="16.2">que</w> <w n="16.3">le</w> <w n="16.4">cadavre</w> <w n="16.5">est</w> <w n="16.6">dedans</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Jadis</w>, <w n="17.2">l</w>’<w n="17.3">artiste</w> <w n="17.4">bien</w> <w n="17.5">vivante</w></l>
						<l n="18" num="5.2"><space unit="char" quantity="8"></space><w n="18.1">Et</w> <w n="18.2">qui</w> <w n="18.3">se</w> <w n="18.4">vante</w></l>
						<l n="19" num="5.3"><w n="19.1">De</w> <w n="19.2">n</w>’<w n="19.3">avoir</w> <w n="19.4">jamais</w> <w n="19.5">peur</w> <w n="19.6">de</w> <w n="19.7">rien</w>,</l>
						<l n="20" num="5.4"><w n="20.1">S</w>’<w n="20.2">y</w> <w n="20.3">allonge</w> <w n="20.4">et</w> <w n="20.5">s</w>’<w n="20.6">y</w> <w n="20.7">trouve</w> <w n="20.8">bien</w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Que</w> <w n="21.2">fut</w> <w n="21.3">alors</w> <w n="21.4">sa</w> <w n="21.5">rêverie</w> ?</l>
						<l n="22" num="6.2"><space unit="char" quantity="8"></space><w n="22.1">Toute</w> <w n="22.2">fleurie</w></l>
						<l n="23" num="6.3"><w n="23.1">Elle</w> <w n="23.2">faisait</w> <w n="23.3">la</w> <w n="23.4">morte</w>, <w n="23.5">mais</w></l>
						<l n="24" num="6.4"><w n="24.1">Son</w> <w n="24.2">instinct</w> <w n="24.3">lui</w> <w n="24.4">disait</w> : « <w n="24.5">Jamais</w> ! »</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">Puis</w>, <w n="25.2">dans</w> <w n="25.3">la</w> <w n="25.4">bière</w> <w n="25.5">parfumée</w></l>
						<l n="26" num="7.2"><space unit="char" quantity="8"></space><w n="26.1">Eut</w> <w n="26.2">inhumée</w>,</l>
						<l n="27" num="7.3"><w n="27.1">Afin</w> <w n="27.2">d</w>’<w n="27.3">être</w> <w n="27.4">relue</w> <w n="27.5">un</w> <w n="27.6">jour</w>,</l>
						<l n="28" num="7.4"><w n="28.1">La</w> <w n="28.2">correspondance</w> <w n="28.3">d</w>’<w n="28.4">amour</w>.</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1"><w n="29.1">Or</w>, <w n="29.2">à</w> <w n="29.3">présent</w> <w n="29.4">que</w> <w n="29.5">dans</w> <w n="29.6">l</w>’<w n="29.7">étroite</w></l>
						<l n="30" num="8.2"><space unit="char" quantity="8"></space><w n="30.1">Et</w> <w n="30.2">longue</w> <w n="30.3">boîte</w></l>
						<l n="31" num="8.3"><w n="31.1">La</w> <w n="31.2">défunte</w> <w n="31.3">au</w> <w n="31.4">masque</w> <w n="31.5">sculpté</w></l>
						<l n="32" num="8.4"><w n="32.1">Repose</w> <w n="32.2">pour</w> <w n="32.3">l</w>’<w n="32.4">éternité</w>,</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1"><w n="33.1">Quatre</w> <w n="33.2">planches</w> <w n="33.3">de</w> <w n="33.4">bois</w> <w n="33.5">de</w> <w n="33.6">rose</w>,</l>
						<l n="34" num="9.2"><space unit="char" quantity="8"></space><w n="34.1">Dans</w> <w n="34.2">l</w>’<w n="34.3">ombre</w> <w n="34.4">close</w></l>
						<l n="35" num="9.3"><w n="35.1">Où</w> <w n="35.2">sa</w> <w n="35.3">robe</w> <w n="35.4">est</w> <w n="35.5">comme</w> <w n="35.6">un</w> <w n="35.7">grand</w> <w n="35.8">lys</w>,</l>
						<l n="36" num="9.4"><w n="36.1">Se</w> <w n="36.2">souviennent</w> <w n="36.3">du</w> <w n="36.4">temps</w> <w n="36.5">jadis</w>.</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1">« <w n="37.1">Te</w> <w n="37.2">rappelles</w>-<w n="37.3">tu</w>, <w n="37.4">disent</w>-<w n="37.5">elles</w>,</l>
						<l n="38" num="10.2"><space unit="char" quantity="8"></space><w n="38.1">Comme</w> <w n="38.2">étaient</w> <w n="38.3">belles</w></l>
						<l n="39" num="10.3"><w n="39.1">Tes</w> <w n="39.2">formes</w> <w n="39.3">pleines</w> <w n="39.4">de</w> <w n="39.5">secrets</w> ?…</l>
						<l n="40" num="10.4"><w n="40.1">De</w> <w n="40.2">doux</w> <w n="40.3">bruit</w> <w n="40.4">quand</w> <w n="40.5">tu</w> <w n="40.6">respirais</w> !</l>
					</lg>
					<lg n="11">
						<l n="41" num="11.1">« <w n="41.1">Nous</w> <w n="41.2">et</w> <w n="41.3">toi</w> <w n="41.4">répétions</w> <w n="41.5">nos</w> <w n="41.6">rôles</w>.</l>
						<l n="42" num="11.2"><space unit="char" quantity="8"></space><w n="42.1">Oui</w>, <w n="42.2">tes</w> <w n="42.3">épaules</w></l>
						<l n="43" num="11.3"><w n="43.1">Nous</w> <w n="43.2">en</w> <w n="43.3">connaissons</w> <w n="43.4">le</w> <w n="43.5">contour</w>,</l>
						<l n="44" num="11.4"><w n="44.1">Et</w> <w n="44.2">tout</w> <w n="44.3">ton</w> <w n="44.4">frêle</w> <w n="44.5">corps</w> <w n="44.6">d</w>’<w n="44.7">amour</w>.</l>
					</lg>
					<lg n="12">
						<l n="45" num="12.1">« <w n="45.1">Plus</w> <w n="45.2">tard</w>, <w n="45.3">oh</w> ! <w n="45.4">ces</w> <w n="45.5">lettres</w> ! <w n="45.6">Ces</w> <w n="45.7">lettres</w></l>
						<l n="46" num="12.2"><space unit="char" quantity="8"></space><w n="46.1">D</w>’<w n="46.2">enfants</w>, <w n="46.3">de</w> <w n="46.4">maîtres</w> !</l>
						<l n="47" num="12.3"><w n="47.1">Nous</w> <w n="47.2">t</w>’<w n="47.3">en</w> <w n="47.4">redirons</w> <w n="47.5">la</w> <w n="47.6">teneur</w>,</l>
						<l n="48" num="12.4"><w n="48.1">Nous</w> <w n="48.2">les</w> <w n="48.3">savons</w> <w n="48.4">toutes</w> <w n="48.5">par</w> <w n="48.6">cœur</w> !</l>
					</lg>
					<lg n="13">
						<l n="49" num="13.1">« <w n="49.1">Écoute</w>-<w n="49.2">les</w> <w n="49.3">parler</w> : <hi rend="ital"><w n="49.4">Quand</w> <w n="49.5">même</w></hi> !</l>
						<l n="50" num="13.2"><space unit="char" quantity="8"></space><w n="50.1">Et</w> <w n="50.2">puis</w> : « <w n="50.3">Je</w> <w n="50.4">t</w>’<w n="50.5">aime</w> ! »</l>
						<l n="51" num="13.3"><w n="51.1">Écoute</w>, <w n="51.2">écoute</w> <w n="51.3">te</w> <w n="51.4">bercer</w></l>
						<l n="52" num="13.4"><w n="52.1">La</w> <w n="52.2">voix</w> <w n="52.3">du</w> <w n="52.4">fabuleux</w> <w n="52.5">passé</w>.</l>
					</lg>
					<lg n="14">
						<l n="53" num="14.1">« <w n="53.1">Ton</w> <w n="53.2">heure</w> <w n="53.3">a</w> <w n="53.4">sonné</w>. <w n="53.5">Mais</w> <w n="53.6">qu</w>’<w n="53.7">importe</w></l>
						<l n="54" num="14.2"><space unit="char" quantity="8"></space><w n="54.1">Que</w> <w n="54.2">tu</w> <w n="54.3">sois</w> <w n="54.4">morte</w> ?</l>
						<l n="55" num="14.3"><w n="55.1">Plus</w> <w n="55.2">familière</w> <w n="55.3">qu</w>’<w n="55.4">un</w> <w n="55.5">toit</w></l>
						<l n="56" num="14.4"><w n="56.1">Ta</w> <w n="56.2">bière</w> <w n="56.3">demeure</w> <w n="56.4">avec</w> <w n="56.5">toi</w>.</l>
					</lg>
					<lg n="15">
						<l n="57" num="15.1">« <w n="57.1">Le</w> <w n="57.2">tombeau</w> <w n="57.3">lourd</w> <w n="57.4">comme</w> <w n="57.5">une</w> <w n="57.6">meule</w></l>
						<l n="58" num="15.2"><space unit="char" quantity="8"></space><w n="58.1">Te</w> <w n="58.2">laisse</w> <w n="58.3">seule</w> ;</l>
						<l n="59" num="15.3"><w n="59.1">Mais</w>, <w n="59.2">pour</w> <w n="59.3">charmer</w> <w n="59.4">ton</w> <w n="59.5">au</w>-<w n="59.6">delà</w>,</l>
						<l n="60" num="15.4"><w n="60.1">Moi</w>, <w n="60.2">ton</w> <w n="60.3">vieux</w> <w n="60.4">cercueil</w>, <w n="60.5">je</w> <w n="60.6">suis</w> <w n="60.7">là</w> !</l>
					</lg>
					<lg n="16">
						<l n="61" num="16.1">« <w n="61.1">La</w> <w n="61.2">grande</w> <w n="61.3">foule</w> <w n="61.4">va</w> <w n="61.5">se</w> <w n="61.6">taire</w>.</l>
						<l n="62" num="16.2"><space unit="char" quantity="8"></space><w n="62.1">Dans</w> <w n="62.2">le</w> <w n="62.3">mystère</w></l>
						<l n="63" num="16.3"><w n="63.1">Nous</w>, <w n="63.2">nous</w> <w n="63.3">parlerons</w> <w n="63.4">nuit</w> <w n="63.5">et</w> <w n="63.6">jour</w></l>
						<l n="64" num="16.4"><w n="64.1">De</w> <w n="64.2">beauté</w>, <w n="64.3">de</w> <w n="64.4">gloire</w> <w n="64.5">et</w> <w n="64.6">d</w>’<w n="64.7">amour</w> ! »</l>
					</lg>
					<lg n="17">
						<l n="65" num="17.1"><w n="65.1">Et</w> <w n="65.2">c</w>’<w n="65.3">est</w> <w n="65.4">ainsi</w>, <w n="65.5">lorsque</w> <w n="65.6">tout</w> <w n="65.7">cesse</w>,</l>
						<l n="66" num="17.2"><space unit="char" quantity="8"></space><w n="66.1">Que</w>, <w n="66.2">dans</w> <w n="66.3">la</w> <w n="66.4">caisse</w></l>
						<l n="67" num="17.3"><w n="67.1">Terrible</w> <w n="67.2">où</w> <w n="67.3">sa</w> <w n="67.4">dépouille</w> <w n="67.5">dort</w>,</l>
						<l n="68" num="17.4"><w n="68.1">Sarah</w> <w n="68.2">Bernhardt</w>, <w n="68.3">malgré</w> <w n="68.4">la</w> <w n="68.5">mort</w>,</l>
					</lg>
					<lg n="18">
						<l n="69" num="18.1"><w n="69.1">Est</w> <w n="69.2">couchée</w> <w n="69.3">avec</w> <w n="69.4">sa</w> <w n="69.5">jeunesse</w>.</l>
					</lg>
				</div></body></text></TEI>