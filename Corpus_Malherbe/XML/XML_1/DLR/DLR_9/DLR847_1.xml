<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">LES SEPT DOULEURS D’OCTOBRE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>Delarue-Mardrus</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2316 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_9</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Sept douleurs d’Octobre</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/luciedelaruemardrueferveur.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Sept douleurs d’Octobre</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>J. Ferenczi et fils, éditeurs</publisher>
							<date when="1930">1930</date>
						</imprint>
					</monogr>
					<note>Édition de référence</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1930">1930</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Il manque de très nombreux poèmes dans le texte source.</p>
				<p>Les nombreux poèmes manquants ont été ajoutés à partir d’une numérisation de l’édition de référence.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-11-28" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">VII</head><head type="main_part">L’ANGOISSE</head><div type="poem" key="DLR847">
					<head type="main">NON CREDO</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">C</w>’<w n="1.2">est</w> <w n="1.3">à</w> <w n="1.4">toi</w>, <w n="1.5">noire</w> <w n="1.6">mort</w> <w n="1.7">des</w> <w n="1.8">matérialistes</w></l>
						<l n="2" num="1.2"><space unit="char" quantity="8"></space><w n="2.1">Que</w> <w n="2.2">vont</w> <w n="2.3">mes</w> <w n="2.4">souhaits</w> <w n="2.5">de</w> <w n="2.6">ce</w> <w n="2.7">soir</w>.</l>
						<l n="3" num="1.3"><w n="3.1">C</w>’<w n="3.2">est</w> <w n="3.3">toi</w> <w n="3.4">que</w> <w n="3.5">je</w> <w n="3.6">voudrais</w> <w n="3.7">pour</w> <w n="3.8">fermer</w> <w n="3.9">mes</w> <w n="3.10">yeux</w> <w n="3.11">tristes</w>,</l>
						<l n="4" num="1.4"><space unit="char" quantity="8"></space><w n="4.1">Faire</w> <w n="4.2">cesser</w> <w n="4.3">mon</w> <w n="4.4">désespoir</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Si</w> <w n="5.2">je</w> <w n="5.3">savais</w> <w n="5.4">qu</w>’<w n="5.5">au</w> <w n="5.6">creux</w> <w n="5.7">de</w> <w n="5.8">lai</w> <w n="5.9">fosse</w> <w n="5.10">profonde</w></l>
						<l n="6" num="2.2"><space unit="char" quantity="8"></space><w n="6.1">Il</w> <w n="6.2">n</w>’<w n="6.3">est</w> <w n="6.4">plus</w> <w n="6.5">rien</w> <w n="6.6">que</w> <w n="6.7">du</w> <w n="6.8">néant</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Si</w> <w n="7.2">la</w> <w n="7.3">lourde</w> <w n="7.4">descente</w> <w n="7.5">au</w> <w n="7.6">fond</w> <w n="7.7">du</w> <w n="7.8">trou</w> <w n="7.9">béant</w></l>
						<l n="8" num="2.4"><space unit="char" quantity="8"></space><w n="8.1">Était</w> <w n="8.2">pour</w> <w n="8.3">moi</w> <w n="8.4">la</w> <w n="8.5">fin</w> <w n="8.6">du</w> <w n="8.7">monde</w>,</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Peut</w>-<w n="9.2">être</w> <w n="9.3">cette</w> <w n="9.4">foi</w> <w n="9.5">qui</w> <w n="9.6">vaut</w> <w n="9.7">l</w>’<w n="9.8">autre</w>, <w n="9.9">après</w> <w n="9.10">tout</w>,</l>
						<l n="10" num="3.2"><space unit="char" quantity="8"></space><w n="10.1">Me</w> <w n="10.2">redonnerait</w>-<w n="10.3">elle</w> <w n="10.4">envie</w></l>
						<l n="11" num="3.3"><w n="11.1">De</w> <w n="11.2">copier</w> <w n="11.3">autrui</w>, <w n="11.4">de</w> <w n="11.5">vivre</w> <w n="11.6">cette</w> <w n="11.7">vie</w></l>
						<l n="12" num="3.4"><space unit="char" quantity="8"></space><w n="12.1">Telle</w> <w n="12.2">qu</w>’<w n="12.3">elle</w> <w n="12.4">est</w>, <w n="12.5">et</w> <w n="12.6">jusqu</w>’<w n="12.7">au</w> <w n="12.8">bout</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Mais</w> <w n="13.2">ceci</w> <w n="13.3">ni</w> <w n="13.4">cela</w> <w n="13.5">pour</w> <w n="13.6">moi</w> <w n="13.7">ne</w> <w n="13.8">sont</w> <w n="13.9">croyance</w>.</l>
						<l n="14" num="4.2"><space unit="char" quantity="8"></space><w n="14.1">Je</w> <w n="14.2">ne</w> <w n="14.3">sais</w> <w n="14.4">pas</w> <w n="14.5">ce</w> <w n="14.6">qu</w>’<w n="14.7">on</w> <w n="14.8">me</w> <w n="14.9">veut</w>.</l>
						<l n="15" num="4.3"><w n="15.1">Être</w> <w n="15.2">une</w> <w n="15.3">athée</w> ! <w n="15.4">Ou</w> <w n="15.5">bien</w> <w n="15.6">avoir</w> <w n="15.7">l</w>’<w n="15.8">outrecuidance</w>,</l>
						<l n="16" num="4.4"><space unit="char" quantity="8"></space><w n="16.1">Comme</w> <w n="16.2">d</w>’<w n="16.3">autres</w>, <w n="16.4">de</w> <w n="16.5">croire</w> <w n="16.6">en</w> <w n="16.7">Dieu</w> !</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">On</w> <w n="17.2">n</w>’<w n="17.3">aura</w> <w n="17.4">jamais</w> <w n="17.5">su</w>, <w n="17.6">personne</w> <w n="17.7">ni</w> <w n="17.8">moi</w>-<w n="17.9">même</w>,</l>
						<l n="18" num="5.2"><space unit="char" quantity="8"></space><w n="18.1">Pourquoi</w> <w n="18.2">ce</w> <w n="18.3">courage</w> <w n="18.4">que</w> <w n="18.5">j</w>’<w n="18.6">ai</w>,</l>
						<l n="19" num="5.3"><w n="19.1">Pourquoi</w> <w n="19.2">je</w> <w n="19.3">vis</w>, <w n="19.4">pourquoi</w> <w n="19.5">ma</w> <w n="19.6">bataille</w> <w n="19.7">suprême</w>,</l>
						<l n="20" num="5.4"><space unit="char" quantity="8"></space><w n="20.1">Ayant</w> <w n="20.2">ce</w> <w n="20.3">cœur</w> <w n="20.4">découragé</w>.</l>
					</lg>
				</div></body></text></TEI>