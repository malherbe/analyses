<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">LES SEPT DOULEURS D’OCTOBRE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>Delarue-Mardrus</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2316 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_9</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Sept douleurs d’Octobre</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/luciedelaruemardrueferveur.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Sept douleurs d’Octobre</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>J. Ferenczi et fils, éditeurs</publisher>
							<date when="1930">1930</date>
						</imprint>
					</monogr>
					<note>Édition de référence</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1930">1930</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Il manque de très nombreux poèmes dans le texte source.</p>
				<p>Les nombreux poèmes manquants ont été ajoutés à partir d’une numérisation de l’édition de référence.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-11-28" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">V</head><head type="main_part">CI-GÎT</head><div type="poem" key="DLR826">
					<head type="main">GAIE</head>
					<lg n="1">
						<l n="1" num="1.1"><space unit="char" quantity="8"></space><w n="1.1">Gaie</w> ?… <w n="1.2">Et</w> <w n="1.3">comment</w> <w n="1.4">pourrais</w>-<w n="1.5">je</w> <w n="1.6">l</w>’<w n="1.7">être</w></l>
						<l n="2" num="1.2"><space unit="char" quantity="8"></space><w n="2.1">Quand</w> <w n="2.2">la</w> <w n="2.3">terre</w> <w n="2.4">palpite</w> <w n="2.5">encor</w>,</l>
						<l n="3" num="1.3"><space unit="char" quantity="18"></space><w n="3.1">Champ</w> <w n="3.2">de</w> <w n="3.3">mort</w></l>
						<l n="4" num="1.4"><w n="4.1">Où</w> <w n="4.2">des</w> <w n="4.3">spectres</w> <w n="4.4">devaient</w>, <w n="4.5">par</w> <w n="4.6">milliers</w>, <w n="4.7">apparaître</w> ?</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><space unit="char" quantity="8"></space><w n="5.1">Après</w> <w n="5.2">l</w>’<w n="5.3">hiver</w> <w n="5.4">vient</w> <w n="5.5">le</w> <w n="5.6">printemps</w></l>
						<l n="6" num="2.2"><space unit="char" quantity="8"></space><w n="6.1">Je</w> <w n="6.2">sais</w> <w n="6.3">bien</w>, <w n="6.4">la</w> <w n="6.5">jeunesse</w> <w n="6.6">danse</w>,</l>
						<l n="7" num="2.3"><space unit="char" quantity="18"></space><w n="7.1">Mais</w> <w n="7.2">je</w> <w n="7.3">pense</w></l>
						<l n="8" num="2.4"><w n="8.1">Aux</w> <w n="8.2">garçons</w> <w n="8.3">enfouis</w> <w n="8.4">qui</w> <w n="8.5">n</w>’<w n="8.6">avaient</w> <w n="8.7">que</w> <w n="8.8">vingt</w> <w n="8.9">ans</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><space unit="char" quantity="8"></space><w n="9.1">Gaie</w> ?… <w n="9.2">Il</w> <w n="9.3">y</w> <w n="9.4">a</w> <w n="9.5">l</w>’<w n="9.6">irréparable</w></l>
						<l n="10" num="3.2"><space unit="char" quantity="8"></space><w n="10.1">Et</w> <w n="10.2">les</w> <w n="10.3">pas</w> <w n="10.4">sanglants</w> <w n="10.5">que</w> <w n="10.6">je</w> <w n="10.7">vois</w></l>
						<l n="11" num="3.3"><space unit="char" quantity="18"></space><w n="11.1">Sur</w> <w n="11.2">le</w> <w n="11.3">sable</w></l>
						<l n="12" num="3.4"><w n="12.1">Ne</w> <w n="12.2">pourront</w> <w n="12.3">s</w>’<w n="12.4">effacer</w> <w n="12.5">avant</w> <w n="12.6">longtemps</w>, <w n="12.7">je</w> <w n="12.8">crois</w>.</l>
					</lg>
				</div></body></text></TEI>