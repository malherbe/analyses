<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">LES SEPT DOULEURS D’OCTOBRE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>Delarue-Mardrus</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2316 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_9</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Sept douleurs d’Octobre</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/luciedelaruemardrueferveur.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Sept douleurs d’Octobre</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>J. Ferenczi et fils, éditeurs</publisher>
							<date when="1930">1930</date>
						</imprint>
					</monogr>
					<note>Édition de référence</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1930">1930</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Il manque de très nombreux poèmes dans le texte source.</p>
				<p>Les nombreux poèmes manquants ont été ajoutés à partir d’une numérisation de l’édition de référence.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-11-28" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">I</head><head type="main_part">L’AUTOMNE</head><div type="poem" key="DLR787">
					<head type="main">CINQ PETITS TABLEAUX</head>
					<div n="1" type="section">
						<head type="number">I</head>
						<lg n="1">
							<l n="1" num="1.1"><space unit="char" quantity="8"></space><w n="1.1">Sur</w> <w n="1.2">les</w> <w n="1.3">arbres</w> <w n="1.4">et</w> <w n="1.5">sur</w> <w n="1.6">le</w> <w n="1.7">sol</w>,</l>
							<l n="2" num="1.2"><space unit="char" quantity="14"></space><w n="2.1">Des</w> <w n="2.2">feuilles</w>, <w n="2.3">des</w> <w n="2.4">feuilles</w> !</l>
							<l n="3" num="1.3"><space unit="char" quantity="8"></space><w n="3.1">Tout</w> <w n="3.2">jaune</w>, <w n="3.3">un</w> <w n="3.4">petit</w> <w n="3.5">arbre</w> <w n="3.6">fol</w></l>
							<l n="4" num="1.4"><space unit="char" quantity="8"></space><w n="4.1">Perd</w> <w n="4.2">d</w>’<w n="4.3">un</w> <w n="4.4">seul</w> <w n="4.5">coup</w> <w n="4.6">plus</w> <w n="4.7">de</w> <w n="4.8">cent</w> <w n="4.9">feuilles</w>.</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1"><space unit="char" quantity="8"></space><w n="5.1">Rouges</w>, <w n="5.2">jaunes</w>, <w n="5.3">mauves</w> <w n="5.4">et</w> <w n="5.5">roux</w>,</l>
							<l n="6" num="2.2"><space unit="char" quantity="14"></space><w n="6.1">O</w> <w n="6.2">palette</w> <w n="6.3">claire</w> !</l>
							<l n="7" num="2.3"><space unit="char" quantity="8"></space><w n="7.1">Le</w> <w n="7.2">grand</w> <w n="7.3">vert</w> <w n="7.4">des</w> <w n="7.5">prés</w> <w n="7.6">s</w>’<w n="7.7">exaspère</w></l>
							<l n="8" num="2.4"><space unit="char" quantity="8"></space><w n="8.1">Sous</w> <w n="8.2">les</w> <w n="8.3">branchages</w> <w n="8.4">noirs</w> <w n="8.5">et</w> <w n="8.6">roux</w>,</l>
						</lg>
						<lg n="3">
							<l n="9" num="3.1"><space unit="char" quantity="8"></space><w n="9.1">Et</w> <w n="9.2">ce</w> <w n="9.3">petit</w> <w n="9.4">bouton</w> <w n="9.5">de</w> <w n="9.6">rose</w></l>
							<l n="10" num="3.2"><space unit="char" quantity="14"></space><w n="10.1">Qui</w> <w n="10.2">fleurit</w> <w n="10.3">trop</w> <w n="10.4">tard</w>,</l>
							<l n="11" num="3.3"><space unit="char" quantity="8"></space><w n="11.1">Brille</w> <w n="11.2">dans</w> <w n="11.3">un</w> <w n="11.4">peu</w> <w n="11.5">de</w> <w n="11.6">brouillard</w>,</l>
							<l n="12" num="3.4"><space unit="char" quantity="8"></space><w n="12.1">Cœur</w> <w n="12.2">frileux</w> <w n="12.3">de</w> <w n="12.4">l</w>’<w n="12.5">automne</w> <w n="12.6">rose</w>.</l>
						</lg>
					</div>
					<div n="2" type="section">
						<head type="number">II</head>
						<lg n="1">
							<l n="13" num="1.1"><space unit="char" quantity="8"></space><w n="13.1">A</w> <w n="13.2">travers</w> <w n="13.3">prés</w>, <w n="13.4">à</w> <w n="13.5">travers</w> <w n="13.6">bois</w>,</l>
							<l n="14" num="1.2"><w n="14.1">Commence</w> <w n="14.2">la</w> <w n="14.3">féerie</w> <w n="14.4">étrange</w> <w n="14.5">de</w> <w n="14.6">l</w>’<w n="14.7">année</w>.</l>
							<l n="15" num="1.3"><space unit="char" quantity="8"></space><w n="15.1">Partout</w> <w n="15.2">où</w> <w n="15.3">vont</w> <w n="15.4">mes</w> <w n="15.5">yeux</w>, <w n="15.6">je</w> <w n="15.7">vois</w></l>
							<l n="16" num="1.4"><space unit="char" quantity="8"></space><w n="16.1">La</w> <w n="16.2">grande</w> <w n="16.3">automne</w> <w n="16.4">empoisonnée</w>.</l>
						</lg>
						<lg n="2">
							<l n="17" num="2.1"><space unit="char" quantity="8"></space><w n="17.1">Les</w> <w n="17.2">branchages</w> <w n="17.3">tordus</w> <w n="17.4">et</w> <w n="17.5">noirs</w></l>
							<l n="18" num="2.2"><w n="18.1">Sont</w> <w n="18.2">lentement</w> <w n="18.3">en</w> <w n="18.4">proie</w> <w n="18.5">à</w> <w n="18.6">toutes</w> <w n="18.7">les</w> <w n="18.8">chimies</w>.</l>
							<l n="19" num="2.3"><space unit="char" quantity="8"></space><w n="19.1">Les</w> <w n="19.2">dernières</w> <w n="19.3">roses</w>, <w n="19.4">blémies</w>,</l>
							<l n="20" num="2.4"><space unit="char" quantity="8"></space><w n="20.1">Fleurissent</w> <w n="20.2">sur</w> <w n="20.3">des</w> <w n="20.4">désespoirs</w>.</l>
						</lg>
						<lg n="3">
							<l n="21" num="3.1"><space unit="char" quantity="8"></space><w n="21.1">Dans</w> <w n="21.2">la</w> <w n="21.3">jonchée</w> <w n="21.4">épaisse</w> <w n="21.5">et</w> <w n="21.6">rose</w>,</l>
							<l n="22" num="3.2"><w n="22.1">Je</w> <w n="22.2">m</w>’<w n="22.3">avance</w>, <w n="22.4">et</w> <w n="22.5">mes</w> <w n="22.6">pieds</w> <w n="22.7">font</w> <w n="22.8">un</w> <w n="22.9">étroit</w> <w n="22.10">chemin</w>.</l>
							<l n="23" num="3.3"><space unit="char" quantity="8"></space><w n="23.1">Et</w> <w n="23.2">toute</w> <w n="23.3">tremblante</w>, <w n="23.4">à</w> <w n="23.5">ma</w> <w n="23.6">main</w>,</l>
							<l n="24" num="3.4"><space unit="char" quantity="8"></space><w n="24.1">Une</w> <w n="24.2">feuille</w> <w n="24.3">se</w> <w n="24.4">décompose</w>.</l>
						</lg>
					</div>
					<div n="3" type="section">
						<head type="number">III</head>
						<lg n="1">
							<l n="25" num="1.1"><w n="25.1">Je</w> <w n="25.2">te</w> <w n="25.3">retrouve</w> <w n="25.4">donc</w>, <w n="25.5">solitude</w> <w n="25.6">fleurie</w></l>
							<l n="26" num="1.2"><space unit="char" quantity="8"></space><w n="26.1">Où</w> <w n="26.2">l</w>’<w n="26.3">on</w> <w n="26.4">aime</w> <w n="26.5">parler</w> <w n="26.6">tout</w> <w n="26.7">bas</w> !</l>
							<l n="27" num="1.3"><space unit="char" quantity="8"></space><w n="27.1">Voici</w> <w n="27.2">l</w>’<w n="27.3">effrayante</w> <w n="27.4">féerie</w>,</l>
							<l n="28" num="1.4"><space unit="char" quantity="8"></space><w n="28.1">Les</w> <w n="28.2">soirs</w> <w n="28.3">où</w> <w n="28.4">la</w> <w n="28.5">chouette</w> <w n="28.6">crie</w>,</l>
							<l n="29" num="1.5"><space unit="char" quantity="8"></space><w n="29.1">Où</w> <w n="29.2">l</w>’<w n="29.3">automne</w> <w n="29.4">sonne</w> <w n="29.5">le</w> <w n="29.6">glas</w>.</l>
						</lg>
						<lg n="2">
							<l n="30" num="2.1"><w n="30.1">Disparate</w>, <w n="30.2">autrefois</w>, <w n="30.3">d</w>’<w n="30.4">être</w> <w n="30.5">une</w> <w n="30.6">jeune</w> <w n="30.7">femme</w></l>
							<l n="31" num="2.2"><space unit="char" quantity="8"></space><w n="31.1">Parmi</w> <w n="31.2">la</w> <w n="31.3">funèbre</w> <w n="31.4">couleur</w>,</l>
							<l n="32" num="2.3"><space unit="char" quantity="8"></space><w n="32.1">Malgré</w> <w n="32.2">ce</w> <w n="32.3">front</w> <w n="32.4">triste</w> <w n="32.5">et</w> <w n="32.6">rêveur</w></l>
							<l n="33" num="2.4"><space unit="char" quantity="8"></space><w n="33.1">J</w>’<w n="33.2">avais</w> <w n="33.3">tout</w> <w n="33.4">l</w>’<w n="33.5">été</w> <w n="33.6">dans</w> <w n="33.7">mon</w> <w n="33.8">cœur</w>.</l>
							<l n="34" num="2.5"><space unit="char" quantity="8"></space>‒ <w n="34.1">Maintenant</w>, <w n="34.2">voici</w> <w n="34.3">le</w> <w n="34.4">vrai</w> <w n="34.5">drame</w>.</l>
						</lg>
						<lg n="3">
							<l n="35" num="3.1"><w n="35.1">Il</w> <w n="35.2">faudra</w> <w n="35.3">lentement</w> <w n="35.4">me</w> <w n="35.5">faire</w> <w n="35.6">une</w> <w n="35.7">raison</w>,</l>
							<l n="36" num="3.2"><space unit="char" quantity="8"></space><w n="36.1">Hélas</w> ! <w n="36.2">et</w> <w n="36.3">que</w> <w n="36.4">mon</w> <w n="36.5">cœur</w> <w n="36.6">connaisse</w></l>
							<l n="37" num="3.3"><space unit="char" quantity="8"></space><w n="37.1">Qu</w>’<w n="37.2">il</w> <w n="37.3">tombe</w>, <w n="37.4">autour</w> <w n="37.5">de</w> <w n="37.6">ma</w> <w n="37.7">maison</w>,</l>
							<l n="38" num="3.4"><space unit="char" quantity="8"></space><w n="38.1">Et</w> <w n="38.2">les</w> <w n="38.3">feuilles</w> <w n="38.4">de</w> <w n="38.5">la</w> <w n="38.6">saison</w></l>
							<l n="39" num="3.5"><space unit="char" quantity="8"></space><w n="39.1">Et</w> <w n="39.2">les</w> <w n="39.3">feuilles</w> <w n="39.4">de</w> <w n="39.5">ma</w> <w n="39.6">jeunesse</w>.</l>
						</lg>
					</div>
					<div n="4" type="section">
						<head type="number">IV</head>
						<lg n="1">
							<l n="40" num="1.1"><w n="40.1">L</w>’<w n="40.2">avenue</w> <w n="40.3">au</w> <w n="40.4">matin</w>, <w n="40.5">cathédrale</w> <w n="40.6">d</w>’<w n="40.7">automne</w>,</l>
							<l n="41" num="1.2"><w n="41.1">Découpe</w> <w n="41.2">sur</w> <w n="41.3">le</w> <w n="41.4">ciel</w> <w n="41.5">des</w> <w n="41.6">vitraux</w> <w n="41.7">flamboyants</w>.</l>
							<l n="42" num="1.3"><w n="42.1">Une</w> <w n="42.2">épaisse</w> <w n="42.3">jonchée</w> <w n="42.4">est</w> <w n="42.5">aux</w> <w n="42.6">deux</w> <w n="42.7">bouts</w> <w n="42.8">fuyants</w>,</l>
							<l n="43" num="1.4"><w n="43.1">Rouge</w> <w n="43.2">et</w> <w n="43.3">jaune</w> <w n="43.4">lueur</w> <w n="43.5">dont</w> <w n="43.6">le</w> <w n="43.7">regard</w> <w n="43.8">s</w>’<w n="43.9">étonne</w>.</l>
						</lg>
						<lg n="2">
							<l n="44" num="2.1"><w n="44.1">Je</w> <w n="44.2">m</w>’<w n="44.3">avance</w> <w n="44.4">sans</w> <w n="44.5">bruit</w> <w n="44.6">dans</w> <w n="44.7">ce</w> <w n="44.8">monde</w> <w n="44.9">vermeil</w>,</l>
							<l n="45" num="2.2"><w n="45.1">Et</w>, <w n="45.2">sous</w> <w n="45.3">les</w> <w n="45.4">hauts</w> <w n="45.5">tilleuls</w> <w n="45.6">dont</w> <w n="45.7">la</w> <w n="45.8">masse</w> <w n="45.9">s</w>’<w n="45.10">allège</w>,</l>
							<l n="46" num="2.3"><w n="46.1">Je</w> <w n="46.2">regarde</w> <w n="46.3">tomber</w> <w n="46.4">partout</w>, <w n="46.5">comme</w> <w n="46.6">une</w> <w n="46.7">neige</w>,</l>
							<l n="47" num="2.4"><w n="47.1">Les</w> <w n="47.2">rondes</w> <w n="47.3">feuilles</w> <w n="47.4">d</w>’<w n="47.5">or</w> <w n="47.6">et</w> <w n="47.7">les</w> <w n="47.8">ronds</w> <w n="47.9">de</w> <w n="47.10">soleil</w>.</l>
						</lg>
						<lg n="3">
							<l n="48" num="3.1"><w n="48.1">Menant</w> <w n="48.2">ainsi</w> <w n="48.3">dans</w> <w n="48.4">l</w>’<w n="48.5">ombre</w> <w n="48.6">une</w> <w n="48.7">marche</w> <w n="48.8">étouffée</w>,</l>
							<l n="49" num="3.2"><w n="49.1">Je</w> <w n="49.2">trace</w> <w n="49.3">dans</w> <w n="49.4">cet</w> <w n="49.5">or</w> <w n="49.6">tant</w> <w n="49.7">de</w> <w n="49.8">minces</w> <w n="49.9">sentiers</w></l>
							<l n="50" num="3.3"><w n="50.1">Que</w> <w n="50.2">je</w> <w n="50.3">crois</w> <w n="50.4">en</w> <w n="50.5">rentrant</w> <w n="50.6">voir</w> <w n="50.7">briller</w> <w n="50.8">à</w> <w n="50.9">mes</w> <w n="50.10">pieds</w>,</l>
							<l n="51" num="3.4"><w n="51.1">Miraculeusement</w>, <w n="51.2">des</w> <w n="51.3">bottines</w> <w n="51.4">de</w> <w n="51.5">fée</w>.</l>
						</lg>
					</div>
					<div n="5" type="section">
						<head type="number">V</head>
						<lg n="1">
							<l n="52" num="1.1"><w n="52.1">Sous</w> <w n="52.2">ce</w> <w n="52.3">ciel</w> <w n="52.4">pluvieux</w> <w n="52.5">et</w> <w n="52.6">rapide</w>, <w n="52.7">l</w>’<w n="52.8">automne</w></l>
							<l n="53" num="1.2"><space unit="char" quantity="8"></space><w n="53.1">Reste</w> <w n="53.2">flamboyante</w>. <w n="53.3">Et</w> <w n="53.4">le</w> <w n="53.5">soir</w></l>
							<l n="54" num="1.3"><w n="54.1">Qui</w> <w n="54.2">vient</w> <w n="54.3">et</w> <w n="54.4">fait</w> <w n="54.5">cesser</w> <w n="54.6">cet</w> <w n="54.7">oiseau</w> <w n="54.8">qui</w> <w n="54.9">chantonne</w>,</l>
							<l n="55" num="1.4"><space unit="char" quantity="8"></space><w n="55.1">Le</w> <w n="55.2">soir</w> <w n="55.3">ne</w> <w n="55.4">peut</w> <w n="55.5">devenir</w> <w n="55.6">noir</w>.</l>
						</lg>
						<lg n="2">
							<l n="56" num="2.1"><w n="56.1">Comme</w> <w n="56.2">un</w> <w n="56.3">vaste</w> <w n="56.4">incendie</w> <w n="56.5">allumé</w> <w n="56.6">par</w> <w n="56.7">les</w> <w n="56.8">hommes</w>,</l>
							<l n="57" num="2.2"><space unit="char" quantity="8"></space><w n="57.1">Le</w> <w n="57.2">paysage</w> <w n="57.3">est</w> <w n="57.4">empourpré</w>.</l>
							<l n="58" num="2.3"><w n="58.1">Et</w> <w n="58.2">tout</w> <w n="58.3">le</w> <w n="58.4">soleil</w> <w n="58.5">reste</w> <w n="58.6">en</w> <w n="58.7">ce</w> <w n="58.8">panier</w> <w n="58.9">de</w> <w n="58.10">pommes</w></l>
							<l n="59" num="2.4"><space unit="char" quantity="8"></space><w n="59.1">Qui</w> <w n="59.2">rutile</w> <w n="59.3">au</w> <w n="59.4">milieu</w> <w n="59.5">du</w> <w n="59.6">pré</w>.</l>
						</lg>
						<lg n="3">
							<l n="60" num="3.1"><w n="60.1">Rouge</w>, <w n="60.2">rousse</w>, <w n="60.3">orangée</w> <w n="60.4">et</w> <w n="60.5">jaune</w>, <w n="60.6">et</w> <w n="60.7">qui</w> <w n="60.8">insiste</w>,</l>
							<l n="61" num="3.2"><space unit="char" quantity="8"></space><w n="61.1">La</w> <w n="61.2">couleur</w> <w n="61.3">ne</w> <w n="61.4">veut</w> <w n="61.5">pas</w> <w n="61.6">mourir</w>.</l>
							<l n="62" num="3.3"><w n="62.1">Parmi</w> <w n="62.2">ce</w> <w n="62.3">soir</w> <w n="62.4">en</w> <w n="62.5">flamme</w> <w n="62.6">où</w> <w n="62.7">j</w>’<w n="62.8">aime</w> <w n="62.9">tant</w> <w n="62.10">courir</w>,</l>
							<l n="63" num="3.4"><space unit="char" quantity="8"></space><w n="63.1">Mon</w> <w n="63.2">Dieu</w>, <w n="63.3">comme</w> <w n="63.4">mon</w> <w n="63.5">cœur</w> <w n="63.6">est</w> <w n="63.7">triste</w>…</l>
						</lg>
					</div>
				</div></body></text></TEI>