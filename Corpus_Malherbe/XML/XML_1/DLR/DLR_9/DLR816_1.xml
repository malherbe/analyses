<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">LES SEPT DOULEURS D’OCTOBRE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>Delarue-Mardrus</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2316 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_9</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Sept douleurs d’Octobre</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/luciedelaruemardrueferveur.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Sept douleurs d’Octobre</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>J. Ferenczi et fils, éditeurs</publisher>
							<date when="1930">1930</date>
						</imprint>
					</monogr>
					<note>Édition de référence</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1930">1930</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Il manque de très nombreux poèmes dans le texte source.</p>
				<p>Les nombreux poèmes manquants ont été ajoutés à partir d’une numérisation de l’édition de référence.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-11-28" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">IV</head><head type="main_part">DE LA VIE A LA MORT</head><div type="poem" key="DLR816">
					<head type="main">A MON CŒUR</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Cette</w> <w n="1.2">nuit</w>, <w n="1.3">je</w> <w n="1.4">m</w>’<w n="1.5">endors</w> <w n="1.6">dans</w> <w n="1.7">la</w> <w n="1.8">chambre</w> <w n="1.9">où</w> <w n="1.10">tout</w> <w n="1.11">dort</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Mais</w> <w n="2.2">le</w> <w n="2.3">repos</w> <w n="2.4">n</w>’<w n="2.5">est</w> <w n="2.6">pas</w> <w n="2.7">parfait</w>. <w n="2.8">Qu</w>’<w n="2.9">est</w>-<w n="2.10">ce</w> <w n="2.11">qui</w> <w n="2.12">veille</w> ?</l>
						<l n="3" num="1.3"><w n="3.1">Mon</w> <w n="3.2">cœur</w> ! <w n="3.3">Ses</w> <w n="3.4">grands</w> <w n="3.5">coups</w> <w n="3.6">sourds</w> <w n="3.7">vivent</w> <w n="3.8">dans</w> <w n="3.9">mon</w> <w n="3.10">oreille</w>.</l>
						<l n="4" num="1.4"><w n="4.1">Certes</w>, <w n="4.2">la</w> <w n="4.3">vie</w> <w n="4.4">est</w> <w n="4.5">plus</w> <w n="4.6">étrange</w> <w n="4.7">que</w> <w n="4.8">la</w> <w n="4.9">mort</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Cœur</w>, <w n="5.2">ô</w> <w n="5.3">cœur</w> <w n="5.4">si</w> <w n="5.5">pressé</w> <w n="5.6">qui</w> <w n="5.7">sans</w> <w n="5.8">cesse</w> <w n="5.9">travailles</w></l>
						<l n="6" num="2.2"><w n="6.1">Avec</w> <w n="6.2">cette</w> <w n="6.3">énergie</w> <w n="6.4">âpre</w> <w n="6.5">de</w> <w n="6.6">forgeron</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Cœur</w> <w n="7.2">tout</w> <w n="7.3">vivant</w>, <w n="7.4">fruit</w> <w n="7.5">remuant</w> <w n="7.6">de</w> <w n="7.7">mes</w> <w n="7.8">entrailles</w>,</l>
						<l n="8" num="2.4"><w n="8.1">Qui</w> <w n="8.2">bats</w> <w n="8.3">dans</w> <w n="8.4">tout</w> <w n="8.5">mon</w> <w n="8.6">corps</w>, <w n="8.7">des</w> <w n="8.8">pieds</w> <w n="8.9">jusques</w> <w n="8.10">au</w> <w n="8.11">front</w>,</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Nuit</w> <w n="9.2">et</w> <w n="9.3">jour</w> <w n="9.4">au</w> <w n="9.5">labeur</w>, <w n="9.6">quelle</w> <w n="9.7">est</w> <w n="9.8">ta</w> <w n="9.9">résistance</w> ?</l>
						<l n="10" num="3.2"><w n="10.1">Quand</w> <w n="10.2">je</w> <w n="10.3">repose</w>, <w n="10.4">toi</w>, <w n="10.5">même</w> <w n="10.6">pas</w> <w n="10.7">assoupi</w>,</l>
						<l n="11" num="3.3"><w n="11.1">Inlassable</w> <w n="11.2">tu</w> <w n="11.3">suis</w> <w n="11.4">ton</w> <w n="11.5">rythme</w> <w n="11.6">sans</w> <w n="11.7">répit</w>.</l>
						<l n="12" num="3.4"><w n="12.1">Combien</w> <w n="12.2">de</w> <w n="12.3">coups</w> <w n="12.4">frappés</w>, <w n="12.5">au</w> <w n="12.6">cours</w> <w n="12.7">d</w>’<w n="12.8">une</w> <w n="12.9">existence</w> ?</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Bête</w> <w n="13.2">vivante</w>, <w n="13.3">enfant</w> <w n="13.4">dont</w> <w n="13.5">on</w> <w n="13.6">n</w>’<w n="13.7">accouche</w> <w n="13.8">pas</w>,</l>
						<l n="14" num="4.2"><w n="14.1">Battant</w> <w n="14.2">de</w> <w n="14.3">cette</w> <w n="14.4">cloche</w> <w n="14.5">creuse</w>, <w n="14.6">la</w> <w n="14.7">poitrine</w>,</l>
						<l n="15" num="4.3"><w n="15.1">Cœur</w> <w n="15.2">d</w>’<w n="15.3">où</w> <w n="15.4">s</w>’<w n="15.5">échappe</w> <w n="15.6">à</w> <w n="15.7">flots</w> <w n="15.8">la</w> <w n="15.9">source</w> <w n="15.10">purpurine</w></l>
						<l n="16" num="4.4"><w n="16.1">Du</w> <w n="16.2">sang</w> <w n="16.3">intérieur</w> <w n="16.4">courant</w> <w n="16.5">de</w> <w n="16.6">haut</w> <w n="16.7">en</w> <w n="16.8">bas</w>,</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Cœur</w>, <w n="17.2">moteur</w> <w n="17.3">acharné</w> <w n="17.4">de</w> <w n="17.5">nos</w> <w n="17.6">faibles</w> <w n="17.7">personnes</w>,</l>
						<l n="18" num="5.2"><w n="18.1">Intime</w> <w n="18.2">balancier</w> <w n="18.3">qui</w> <w n="18.4">mesures</w> <w n="18.5">le</w> <w n="18.6">temps</w>,</l>
						<l n="19" num="5.3"><w n="19.1">Cœur</w> <w n="19.2">qui</w> <w n="19.3">cognes</w> <w n="19.4">si</w> <w n="19.5">fort</w> <w n="19.6">aux</w> <w n="19.7">instants</w> <w n="19.8">éclatants</w>,</l>
						<l n="20" num="5.4"><w n="20.1">Glas</w> <w n="20.2">annonciateur</w> <w n="20.3">qui</w> <w n="20.4">sonnes</w>, <w n="20.5">sonnes</w>, <w n="20.6">sonnes</w>,</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Cœur</w> <w n="21.2">des</w> <w n="21.3">terrestres</w>, <w n="21.4">cœur</w> <w n="21.5">des</w> <w n="21.6">bêtes</w> <w n="21.7">et</w> <w n="21.8">des</w> <w n="21.9">gens</w></l>
						<l n="22" num="6.2"><w n="22.1">Qui</w> <w n="22.2">ne</w> <w n="22.3">reposeras</w> <w n="22.4">jamais</w> <w n="22.5">que</w> <w n="22.6">dans</w> <w n="22.7">la</w> <w n="22.8">terre</w>,</l>
						<l n="23" num="6.3"><w n="23.1">Quand</w>, <w n="23.2">après</w> <w n="23.3">tant</w> <w n="23.4">de</w> <w n="23.5">zèle</w> <w n="23.6">et</w> <w n="23.7">de</w> <w n="23.8">soins</w> <w n="23.9">diligents</w>,</l>
						<l n="24" num="6.4"><w n="24.1">Tu</w> <w n="24.2">seras</w> <w n="24.3">aussi</w> <w n="24.4">simple</w> <w n="24.5">et</w> <w n="24.6">sage</w> <w n="24.7">qu</w>’<w n="24.8">une</w> <w n="24.9">pierre</w>,</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">O</w> <w n="25.2">cœur</w> <w n="25.3">indépendant</w> <w n="25.4">de</w> <w n="25.5">mon</w> <w n="25.6">vouloir</w>, <w n="25.7">émoi</w></l>
						<l n="26" num="7.2"><w n="26.1">Perpétuel</w>, <w n="26.2">énigme</w> <w n="26.3">éternelle</w> <w n="26.4">de</w> <w n="26.5">l</w>’<w n="26.6">être</w>,</l>
						<l n="27" num="7.3"><w n="27.1">O</w> <w n="27.2">cœur</w>, <w n="27.3">monstre</w> <w n="27.4">caché</w>, <w n="27.5">tu</w> <w n="27.6">me</w> <w n="27.7">fais</w> <w n="27.8">peur</w>, <w n="27.9">mon</w> <w n="27.10">maître</w>,</l>
						<l n="28" num="7.4"><w n="28.1">Lorsque</w> <w n="28.2">j</w>’<w n="28.3">entends</w>, <w n="28.4">la</w> <w n="28.5">nuit</w>, <w n="28.6">ton</w> <w n="28.7">frappement</w> <w n="28.8">en</w> <w n="28.9">moi</w>.</l>
					</lg>
				</div></body></text></TEI>