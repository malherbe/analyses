<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">LES SEPT DOULEURS D’OCTOBRE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>Delarue-Mardrus</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2316 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_9</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Sept douleurs d’Octobre</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/luciedelaruemardrueferveur.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Sept douleurs d’Octobre</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>J. Ferenczi et fils, éditeurs</publisher>
							<date when="1930">1930</date>
						</imprint>
					</monogr>
					<note>Édition de référence</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1930">1930</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Il manque de très nombreux poèmes dans le texte source.</p>
				<p>Les nombreux poèmes manquants ont été ajoutés à partir d’une numérisation de l’édition de référence.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-11-28" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">II</head><head type="main_part">L’AVENUE</head><div type="poem" key="DLR795">
					<head type="main">AT HOME</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Voici</w> <w n="1.2">les</w> <w n="1.3">cinq</w> <w n="1.4">belles</w> <w n="1.5">fenêtres</w></l>
						<l n="2" num="1.2"><w n="2.1">Du</w> <w n="2.2">salon</w> <w n="2.3">vieillot</w> <w n="2.4">où</w> <w n="2.5">je</w> <w n="2.6">vis</w></l>
						<l n="3" num="1.3"><w n="3.1">En</w> <w n="3.2">plein</w> <w n="3.3">herbage</w>, <w n="3.4">loin</w> <w n="3.5">des</w> <w n="3.6">êtres</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Haut</w>, <w n="4.2">comme</w> <w n="4.3">au</w> <w n="4.4">temps</w> <w n="4.5">des</w> <w n="4.6">ponts</w>-<w n="4.7">levis</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Dans</w> <w n="5.2">la</w> <w n="5.3">première</w> <w n="5.4">est</w> <w n="5.5">la</w> <w n="5.6">vallée</w>,</l>
						<l n="6" num="2.2"><w n="6.1">Dans</w> <w n="6.2">l</w>’<w n="6.3">autre</w> <w n="6.4">la</w> <w n="6.5">ville</w> <w n="6.6">bleu</w> <w n="6.7">noir</w> ;</l>
						<l n="7" num="2.3"><w n="7.1">Deux</w> <w n="7.2">autres</w> <w n="7.3">ont</w> <w n="7.4">un</w> <w n="7.5">bout</w> <w n="7.6">d</w>’<w n="7.7">allée</w></l>
						<l n="8" num="2.4"><w n="8.1">Et</w> <w n="8.2">l</w>’<w n="8.3">estuaire</w> <w n="8.4">où</w> <w n="8.5">meurt</w> <w n="8.6">le</w> <w n="8.7">soir</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Mais</w>, <w n="9.2">dans</w> <w n="9.3">la</w> <w n="9.4">tragique</w> <w n="9.5">cinquième</w>,</l>
						<l n="10" num="3.2"><w n="10.1">Plein</w> <w n="10.2">les</w> <w n="10.3">seize</w> <w n="10.4">petits</w> <w n="10.5">carreaux</w>,</l>
						<l n="11" num="3.3"><w n="11.1">Il</w> <w n="11.2">n</w>’<w n="11.3">est</w> <w n="11.4">rien</w> <w n="11.5">qu</w>’<w n="11.6">un</w> <w n="11.7">grand</w> <w n="11.8">vide</w> <w n="11.9">blême</w></l>
						<l n="12" num="3.4"><w n="12.1">Au</w>-<w n="12.2">dessus</w> <w n="12.3">d</w>’<w n="12.4">espaces</w> <w n="12.5">ruraux</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Ces</w> <w n="13.2">éléments</w> <w n="13.3">frôlent</w> <w n="13.4">mes</w> <w n="13.5">vitres</w>,</l>
						<l n="14" num="4.2"><w n="14.1">Ils</w> <w n="14.2">se</w> <w n="14.3">mêlent</w> <w n="14.4">au</w> <w n="14.5">mobilier</w>,</l>
						<l n="15" num="4.3"><w n="15.1">Et</w>, <w n="15.2">sur</w> <w n="15.3">un</w> <w n="15.4">mode</w> <w n="15.5">familier</w>,</l>
						<l n="16" num="4.4"><w n="16.1">Déroulent</w> <w n="16.2">leurs</w> <w n="16.3">vastes</w> <w n="16.4">chapitres</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Je</w> <w n="17.2">vois</w> <w n="17.3">à</w> <w n="17.4">travers</w> <w n="17.5">l</w>’<w n="17.6">infini</w></l>
						<l n="18" num="5.2"><w n="18.1">Monter</w> <w n="18.2">le</w> <w n="18.3">monde</w> <w n="18.4">des</w> <w n="18.5">nuages</w>,</l>
						<l n="19" num="5.3"><w n="19.1">La</w> <w n="19.2">formation</w> <w n="19.3">des</w> <w n="19.4">orages</w></l>
						<l n="20" num="5.4"><w n="20.1">Ou</w> <w n="20.2">le</w> <w n="20.3">beau</w> <w n="20.4">temps</w> <w n="20.5">bleu</w> <w n="20.6">dans</w> <w n="20.7">son</w> <w n="20.8">nid</w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Et</w> <w n="21.2">parfois</w>, <w n="21.3">relevant</w> <w n="21.4">la</w> <w n="21.5">tête</w></l>
						<l n="22" num="6.2"><w n="22.1">Au</w> <w n="22.2">coin</w> <w n="22.3">de</w> <w n="22.4">mon</w> <w n="22.5">âtre</w> <w n="22.6">embrasé</w>,</l>
						<l n="23" num="6.3"><w n="23.1">Je</w> <w n="23.2">vois</w> <w n="23.3">accourir</w> <w n="23.4">la</w> <w n="23.5">tempête</w></l>
						<l n="24" num="6.4"><w n="24.1">Ainsi</w> <w n="24.2">qu</w>’<w n="24.3">un</w> <w n="24.4">monstre</w> <w n="24.5">apprivoisé</w>.</l>
					</lg>
				</div></body></text></TEI>