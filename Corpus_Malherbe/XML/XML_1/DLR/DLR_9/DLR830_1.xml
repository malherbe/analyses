<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">LES SEPT DOULEURS D’OCTOBRE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>Delarue-Mardrus</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2316 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_9</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Sept douleurs d’Octobre</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/luciedelaruemardrueferveur.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Sept douleurs d’Octobre</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>J. Ferenczi et fils, éditeurs</publisher>
							<date when="1930">1930</date>
						</imprint>
					</monogr>
					<note>Édition de référence</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1930">1930</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Il manque de très nombreux poèmes dans le texte source.</p>
				<p>Les nombreux poèmes manquants ont été ajoutés à partir d’une numérisation de l’édition de référence.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-11-28" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">VI</head><head type="main_part">SPECTRES</head><div type="poem" key="DLR830">
					<head type="main">A MES QUATRE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">L</w>’<w n="1.2">Archange</w> <w n="1.3">va</w> <w n="1.4">devant</w>. (<w n="1.5">Je</w> <w n="1.6">ne</w> <w n="1.7">dis</w> <w n="1.8">pas</w> <w n="1.9">de</w> <w n="1.10">nom</w>.)</l>
						<l n="2" num="1.2"><space unit="char" quantity="8"></space><w n="2.1">L</w>’<w n="2.2">Archange</w>, <w n="2.3">premier</w> <w n="2.4">de</w> <w n="2.5">mes</w> <w n="2.6">doubles</w>.</l>
						<l n="3" num="1.3"><space unit="char" quantity="8"></space><w n="3.1">A</w> <w n="3.2">ma</w> <w n="3.3">droite</w> <w n="3.4">va</w> <w n="3.5">le</w> <w n="3.6">second</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Le</w> <w n="4.2">troisième</w> <w n="4.3">à</w> <w n="4.4">ma</w> <w n="4.5">gauche</w>. <w n="4.6">Et</w>, <w n="4.7">baissant</w> <w n="4.8">ses</w> <w n="4.9">yeux</w> <w n="4.10">troubles</w>,</l>
						<l n="5" num="1.5"><w n="5.1">Quatrième</w> <w n="5.2">et</w> <w n="5.3">dernier</w> <w n="5.4">me</w> <w n="5.5">suit</w> <w n="5.6">le</w> <w n="5.7">plus</w> <w n="5.8">petit</w>.</l>
						<l n="6" num="1.6"><space unit="char" quantity="8"></space><w n="6.1">Et</w> <w n="6.2">lorsqu</w>’<w n="6.3">ils</w> <w n="6.4">sont</w> <w n="6.5">là</w> <w n="6.6">tous</w> <w n="6.7">les</w> <w n="6.8">quatre</w>,</l>
						<l n="7" num="1.7"><w n="7.1">Alors</w> <w n="7.2">et</w> <w n="7.3">seulement</w> <w n="7.4">je</w> <w n="7.5">sens</w> <w n="7.6">mon</w> <w n="7.7">cœur</w> <w n="7.8">bien</w> <w n="7.9">battre</w>.</l>
					</lg>
					<lg n="2">
						<l n="8" num="2.1"><space unit="char" quantity="8"></space><w n="8.1">Trop</w> <w n="8.2">souvent</w> <w n="8.3">l</w>’<w n="8.4">un</w> <w n="8.5">d</w>’<w n="8.6">eux</w> <w n="8.7">est</w> <w n="8.8">parti</w>,</l>
						<l n="9" num="2.2"><w n="9.1">Trop</w> <w n="9.2">souvent</w> <w n="9.3">je</w> <w n="9.4">m</w>’<w n="9.5">en</w> <w n="9.6">vais</w> <w n="9.7">par</w> <w n="9.8">la</w> <w n="9.9">vie</w>, <w n="9.10">inquiète</w>,</l>
						<l n="10" num="2.3"><space unit="char" quantity="16"></space><w n="10.1">Triste</w>, <w n="10.2">incomplète</w>,</l>
						<l n="11" num="2.4"><w n="11.1">Avec</w> <w n="11.2">le</w> <w n="11.3">vide</w> <w n="11.4">en</w> <w n="11.5">moi</w> <w n="11.6">laissé</w> <w n="11.7">par</w> <w n="11.8">cet</w> <w n="11.9">absent</w>.</l>
					</lg>
					<lg n="3">
						<l n="12" num="3.1"><space unit="char" quantity="12"></space><w n="12.1">Mais</w>, <w n="12.2">âme</w> <w n="12.3">de</w> <w n="12.4">mon</w> <w n="12.5">âme</w>,</l>
						<l n="13" num="3.2"><space unit="char" quantity="16"></space><w n="13.1">Sang</w> <w n="13.2">de</w> <w n="13.3">mon</w> <w n="13.4">sang</w>,</l>
						<l n="14" num="3.3"><w n="14.1">O</w> <w n="14.2">toi</w> <w n="14.3">qui</w> <w n="14.4">me</w> <w n="14.5">créas</w> <w n="14.6">plus</w> <w n="14.7">qu</w>’<w n="14.8">homme</w> <w n="14.9">et</w> <w n="14.10">plus</w> <w n="14.11">que</w> <w n="14.12">femme</w>,</l>
						<l n="15" num="3.4"><space unit="char" quantity="16"></space><w n="15.1">Sois</w> <w n="15.2">devant</w> <w n="15.3">moi</w>,</l>
						<l n="16" num="3.5"><w n="16.1">Amour</w>, <w n="16.2">grandeur</w>, <w n="16.3">beauté</w>, <w n="16.4">surnaturel</w> <w n="16.5">émoi</w>,</l>
						<l n="17" num="3.6"><space unit="char" quantity="16"></space><w n="17.1">Sois</w> <w n="17.2">devant</w> <w n="17.3">moi</w></l>
						<l n="18" num="3.7"><w n="18.1">Toujours</w>, <w n="18.2">toujours</w>, <w n="18.3">ô</w> <w n="18.4">toi</w> <w n="18.5">qui</w> <w n="18.6">dans</w> <w n="18.7">l</w>’<w n="18.8">ombre</w> <w n="18.9">étincelles</w>,</l>
						<l n="19" num="3.8"><space unit="char" quantity="16"></space><w n="19.1">Sois</w> <w n="19.2">devant</w> <w n="19.3">moi</w>,</l>
						<l n="20" num="3.9"><w n="20.1">Que</w> <w n="20.2">je</w> <w n="20.3">suive</w> <w n="20.4">à</w> <w n="20.5">jamais</w> <w n="20.6">la</w> <w n="20.7">traîne</w> <w n="20.8">de</w> <w n="20.9">tes</w> <w n="20.10">ailes</w> !</l>
					</lg>
				</div></body></text></TEI>