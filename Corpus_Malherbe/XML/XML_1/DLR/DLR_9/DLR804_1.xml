<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">LES SEPT DOULEURS D’OCTOBRE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>Delarue-Mardrus</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2316 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_9</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Sept douleurs d’Octobre</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/luciedelaruemardrueferveur.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Sept douleurs d’Octobre</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>J. Ferenczi et fils, éditeurs</publisher>
							<date when="1930">1930</date>
						</imprint>
					</monogr>
					<note>Édition de référence</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1930">1930</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Il manque de très nombreux poèmes dans le texte source.</p>
				<p>Les nombreux poèmes manquants ont été ajoutés à partir d’une numérisation de l’édition de référence.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-11-28" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">III</head><head type="main_part">MAI, JOLI MAI !</head><div type="poem" key="DLR804">
					<head type="main">PRIMAVERA</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Le</w> <w n="1.2">printemps</w> <w n="1.3">reparu</w>, <w n="1.4">rythme</w> <w n="1.5">toujours</w> <w n="1.6">le</w> <w n="1.7">même</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Primevères</w> <w n="2.2">dans</w> <w n="2.3">l</w>’<w n="2.4">ombre</w> <w n="2.5">et</w> <w n="2.6">merles</w> <w n="2.7">en</w> <w n="2.8">gaieté</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Herbe</w> <w n="3.2">neuve</w> <w n="3.3">et</w> <w n="3.4">ciel</w> <w n="3.5">bleu</w>, <w n="3.6">tout</w> <w n="3.7">ce</w> <w n="3.8">qui</w> <w n="3.9">chante</w> : « <w n="3.10">J</w>’<w n="3.11">aime</w> ! »</l>
						<l n="4" num="1.4"><w n="4.1">Se</w> <w n="4.2">croit</w> <w n="4.3">innocemment</w> <w n="4.4">miracle</w> <w n="4.5">et</w> <w n="4.6">nouveauté</w> !</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Partout</w> <w n="5.2">on</w> <w n="5.3">voit</w>, <w n="5.4">pliant</w> <w n="5.5">sous</w> <w n="5.6">leurs</w> <w n="5.7">fleurs</w> <w n="5.8">sans</w> <w n="5.9">feuillage</w>,</l>
						<l n="6" num="2.2"><w n="6.1">Cerisiers</w> <w n="6.2">et</w> <w n="6.3">poiriers</w> <w n="6.4">gonflés</w> <w n="6.5">et</w> <w n="6.6">luxueux</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Et</w> <w n="7.2">l</w>’<w n="7.3">on</w> <w n="7.4">croit</w>, <w n="7.5">si</w> <w n="7.6">l</w>’<w n="7.7">azur</w> <w n="7.8">berce</w> <w n="7.9">un</w> <w n="7.10">petit</w> <w n="7.11">nuage</w>,</l>
						<l n="8" num="2.4"><w n="8.1">Qu</w>’<w n="8.2">un</w> <w n="8.3">de</w> <w n="8.4">ces</w> <w n="8.5">arbres</w> <w n="8.6">blancs</w> <w n="8.7">vient</w> <w n="8.8">de</w> <w n="8.9">monter</w> <w n="8.10">aux</w> <w n="8.11">cieux</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Dans</w> <w n="9.2">l</w>’<w n="9.3">herbage</w>, <w n="9.4">des</w> <w n="9.5">millions</w> <w n="9.6">de</w> <w n="9.7">pâquerettes</w></l>
						<l n="10" num="3.2"><w n="10.1">Ressemblent</w> <w n="10.2">gentiment</w> <w n="10.3">à</w> <w n="10.4">des</w> <w n="10.5">gouttes</w> <w n="10.6">de</w> <w n="10.7">lait</w>.</l>
						<l n="11" num="3.3"><w n="11.1">On</w> <w n="11.2">dirait</w> <w n="11.3">que</w> <w n="11.4">la</w> <w n="11.5">vache</w> <w n="11.6">rousse</w>, <w n="11.7">après</w> <w n="11.8">les</w> <w n="11.9">traites</w>,</l>
						<l n="12" num="3.4"><w n="12.1">A</w> <w n="12.2">semé</w> <w n="12.3">tout</w> <w n="12.4">cela</w> <w n="12.5">juste</w> <w n="12.6">comme</w> <w n="12.7">il</w> <w n="12.8">fallait</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">O</w> <w n="13.2">printemps</w> <w n="13.3">de</w> <w n="13.4">chez</w> <w n="13.5">nous</w>, <w n="13.6">naïve</w> <w n="13.7">imagerie</w>,</l>
						<l n="14" num="4.2"><w n="14.1">Fraîcheur</w>, <w n="14.2">rosée</w>, <w n="14.3">oiseaux</w>, <w n="14.4">bouquets</w>, <w n="14.5">longueur</w> <w n="14.6">des</w> <w n="14.7">jours</w>,</l>
						<l n="15" num="4.3"><w n="15.1">Laisse</w>-<w n="15.2">moi</w>, <w n="15.3">quand</w> <w n="15.4">je</w> <w n="15.5">cours</w>, <w n="15.6">ivre</w>, <w n="15.7">dans</w> <w n="15.8">ma</w> <w n="15.9">prairie</w>,</l>
						<l n="16" num="4.4"><w n="16.1">Croire</w> <w n="16.2">que</w>, <w n="16.3">cette</w> <w n="16.4">fois</w>, <w n="16.5">te</w> <w n="16.6">voilà</w> <w n="16.7">pour</w> <w n="16.8">toujours</w> !</l>
					</lg>
				</div></body></text></TEI>