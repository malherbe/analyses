<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">LES SEPT DOULEURS D’OCTOBRE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>Delarue-Mardrus</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2316 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_9</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Sept douleurs d’Octobre</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/luciedelaruemardrueferveur.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Sept douleurs d’Octobre</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>J. Ferenczi et fils, éditeurs</publisher>
							<date when="1930">1930</date>
						</imprint>
					</monogr>
					<note>Édition de référence</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1930">1930</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Il manque de très nombreux poèmes dans le texte source.</p>
				<p>Les nombreux poèmes manquants ont été ajoutés à partir d’une numérisation de l’édition de référence.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-11-28" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">III</head><head type="main_part">MAI, JOLI MAI !</head><div type="poem" key="DLR808">
					<head type="main">MAI</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Mon</w> <w n="1.2">âme</w> <w n="1.3">sans</w> <w n="1.4">désirs</w> <w n="1.5">repose</w> <w n="1.6">en</w> <w n="1.7">plein</w> <w n="1.8">printemps</w>.</l>
						<l n="2" num="1.2"><w n="2.1">Oh</w> ! <w n="2.2">gosiers</w> <w n="2.3">des</w> <w n="2.4">oiseaux</w>, <w n="2.5">petits</w> <w n="2.6">rires</w> <w n="2.7">de</w> <w n="2.8">joie</w> !</l>
						<l n="3" num="1.3"><w n="3.1">Les</w> <w n="3.2">pâquerettes</w> <w n="3.3">font</w> <w n="3.4">dans</w> <w n="3.5">l</w>’<w n="3.6">herbage</w> <w n="3.7">une</w> <w n="3.8">voie</w></l>
						<l n="4" num="1.4"><w n="4.1">Lactée</w>, <w n="4.2">et</w> <w n="4.3">tous</w> <w n="4.4">les</w> <w n="4.5">coins</w> <w n="4.6">où</w> <w n="4.7">l</w>’<w n="4.8">on</w> <w n="4.9">va</w> <w n="4.10">sont</w> <w n="4.11">tentants</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Les</w> <w n="5.2">pommiers</w> <w n="5.3">tortueux</w>, <w n="5.4">crochus</w>, <w n="5.5">gonflés</w> <w n="5.6">de</w> <w n="5.7">bosses</w>,</l>
						<l n="6" num="2.2"><w n="6.1">Redeviennent</w> <w n="6.2">chacun</w> <w n="6.3">un</w> <w n="6.4">panier</w> <w n="6.5">parfumé</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Virginale</w> <w n="7.2">blancheur</w>, <w n="7.3">long</w> <w n="7.4">cortège</w> <w n="7.5">des</w> <w n="7.6">noces</w></l>
						<l n="8" num="2.4"><w n="8.1">De</w> <w n="8.2">la</w> <w n="8.3">terre</w> <w n="8.4">normande</w> <w n="8.5">avec</w> <w n="8.6">le</w> <w n="8.7">mois</w> <w n="8.8">de</w> <w n="8.9">Mai</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Dans</w> <w n="9.2">un</w> <w n="9.3">peu</w> <w n="9.4">d</w>’<w n="9.5">ombre</w> <w n="9.6">il</w> <w n="9.7">traîne</w> <w n="9.8">encore</w> <w n="9.9">des</w> <w n="9.10">jacinthes</w>.</l>
						<l n="10" num="3.2"><w n="10.1">Un</w> <w n="10.2">grand</w> <w n="10.3">mystère</w> <w n="10.4">couve</w> <w n="10.5">au</w> <w n="10.6">tournant</w> <w n="10.7">du</w> <w n="10.8">sous</w>-<w n="10.9">bois</w>.</l>
						<l n="11" num="3.3"><w n="11.1">Les</w> <w n="11.2">arbres</w>, <w n="11.3">brouillard</w> <w n="11.4">vert</w>, <w n="11.5">sont</w> <w n="11.6">roucoulants</w> <w n="11.7">de</w> <w n="11.8">plaintes</w>.</l>
						<l n="12" num="3.4">— <w n="12.1">Faut</w>-<w n="12.2">il</w> <w n="12.3">que</w> <w n="12.4">le</w> <w n="12.5">printemps</w> <w n="12.6">m</w>’<w n="12.7">étonne</w> <w n="12.8">à</w> <w n="12.9">chaque</w> <w n="12.10">fois</w> !</l>
					</lg>
				</div></body></text></TEI>