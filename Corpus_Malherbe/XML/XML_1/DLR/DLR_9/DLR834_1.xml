<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">LES SEPT DOULEURS D’OCTOBRE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>Delarue-Mardrus</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2316 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_9</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Sept douleurs d’Octobre</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/luciedelaruemardrueferveur.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Sept douleurs d’Octobre</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>J. Ferenczi et fils, éditeurs</publisher>
							<date when="1930">1930</date>
						</imprint>
					</monogr>
					<note>Édition de référence</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1930">1930</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Il manque de très nombreux poèmes dans le texte source.</p>
				<p>Les nombreux poèmes manquants ont été ajoutés à partir d’une numérisation de l’édition de référence.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-11-28" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">VI</head><head type="main_part">SPECTRES</head><div type="poem" key="DLR834">
					<head type="main">FINALEMENT</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">J</w>’<w n="1.2">errais</w> <w n="1.3">au</w> <w n="1.4">soir</w> <w n="1.5">tombant</w> <w n="1.6">dans</w> <w n="1.7">la</w> <w n="1.8">région</w> <w n="1.9">pâle</w></l>
						<l n="2" num="1.2"><space unit="char" quantity="8"></space><w n="2.1">Au</w> <w n="2.2">bord</w> <w n="2.3">sombrement</w> <w n="2.4">violet</w></l>
						<l n="3" num="1.3"><space unit="char" quantity="8"></space><w n="3.1">De</w> <w n="3.2">la</w> <w n="3.3">vase</w> <w n="3.4">couleur</w> <w n="3.5">d</w>’<w n="3.6">opale</w></l>
						<l n="4" num="1.4"><w n="4.1">Où</w> <w n="4.2">l</w>’<w n="4.3">on</w> <w n="4.4">entend</w> <w n="4.5">au</w> <w n="4.6">loin</w> <w n="4.7">appeler</w> <w n="4.8">le</w> <w n="4.9">sifflet</w></l>
						<l n="5" num="1.5"><space unit="char" quantity="8"></space><w n="5.1">Triste</w> <w n="5.2">et</w> <w n="5.3">doux</w> <w n="5.4">d</w>’<w n="5.5">oiseaux</w> <w n="5.6">invisibles</w>.</l>
						<l n="6" num="1.6"><space unit="char" quantity="8"></space><w n="6.1">Ici</w>, <w n="6.2">lai</w> <w n="6.3">mer</w> <w n="6.4">qu</w>’<w n="6.5">on</w> <w n="6.6">ne</w> <w n="6.7">voit</w> <w n="6.8">pas</w>,</l>
						<l n="7" num="1.7"><w n="7.1">Perdue</w> <w n="7.2">à</w> <w n="7.3">l</w>’<w n="7.4">horizon</w> <w n="7.5">dans</w> <w n="7.6">des</w> <w n="7.7">gris</w> <w n="7.8">indicibles</w>,</l>
						<l n="8" num="1.8"><w n="8.1">Semble</w> <w n="8.2">devoir</w> <w n="8.3">toujours</w>, <w n="8.4">toujours</w> <w n="8.5">rester</w> <w n="8.6">là</w>-<w n="8.7">bas</w></l>
						<l n="9" num="1.9"><space unit="char" quantity="8"></space><w n="9.1">Où</w> <w n="9.2">la</w> <w n="9.3">mit</w> <w n="9.4">la</w> <w n="9.5">basse</w> <w n="9.6">marée</w>.</l>
						<l n="10" num="1.10"><w n="10.1">Et</w> <w n="10.2">la</w> <w n="10.3">couleur</w> <w n="10.4">du</w> <w n="10.5">ciel</w>, <w n="10.6">dans</w> <w n="10.7">la</w> <w n="10.8">vase</w> <w n="10.9">mirée</w>,</l>
						<l n="11" num="1.11"><w n="11.1">Étend</w> <w n="11.2">un</w> <w n="11.3">firmament</w> <w n="11.4">de</w> <w n="11.5">nacre</w> <w n="11.6">sous</w> <w n="11.7">les</w> <w n="11.8">pas</w>.</l>
						<l n="12" num="1.12"><space unit="char" quantity="8"></space><w n="12.1">De</w> <w n="12.2">nuage</w> <w n="12.3">est</w> <w n="12.4">pareil</w> <w n="12.5">à</w> <w n="12.6">l</w>’<w n="12.7">onde</w>,</l>
						<l n="13" num="1.13"><w n="13.1">La</w> <w n="13.2">terre</w> <w n="13.3">devient</w> <w n="13.4">vase</w> <w n="13.5">et</w> <w n="13.6">le</w> <w n="13.7">fleuve</w> <w n="13.8">océan</w>,</l>
						<l n="14" num="1.14"><w n="14.1">Et</w> <w n="14.2">tout</w> <w n="14.3">semble</w> <w n="14.4">sortir</w> <w n="14.5">lentement</w> <w n="14.6">du</w> <w n="14.7">néant</w>,</l>
						<l n="15" num="1.15"><space unit="char" quantity="8"></space><w n="15.1">Comme</w> <w n="15.2">au</w> <w n="15.3">commencement</w> <w n="15.4">du</w> <w n="15.5">monde</w>.</l>
						<l n="16" num="1.16"><w n="16.1">Fleuve</w> <w n="16.2">et</w> <w n="16.3">mer</w> ! <w n="16.4">Estuaire</w> <w n="16.5">imprécis</w> <w n="16.6">et</w> <w n="16.7">salé</w> !</l>
						<l n="17" num="1.17"><w n="17.1">C</w>’<w n="17.2">est</w> <w n="17.3">là</w> <w n="17.4">qu</w>’<w n="17.5">est</w> <w n="17.6">mon</w> <w n="17.7">pays</w> <w n="17.8">fécond</w> <w n="17.9">et</w> <w n="17.10">désolé</w>,</l>
						<l n="18" num="1.18"><w n="18.1">Là</w> <w n="18.2">que</w> <w n="18.3">j</w>’<w n="18.4">attends</w> <w n="18.5">le</w> <w n="18.6">flot</w> <w n="18.7">lorsqu</w>’<w n="18.8">il</w> <w n="18.9">s</w>’<w n="18.10">en</w> <w n="18.11">est</w> <w n="18.12">allé</w>,</l>
						<l n="19" num="1.19"><w n="19.1">Sachant</w> <w n="19.2">qu</w>’<w n="19.3">il</w> <w n="19.4">reviendra</w> <w n="19.5">sur</w> <w n="19.6">sa</w> <w n="19.7">déserte</w> <w n="19.8">grève</w>.</l>
						<l n="20" num="1.20"><space unit="char" quantity="8"></space><w n="20.1">C</w>’<w n="20.2">est</w> <w n="20.3">là</w> <w n="20.4">que</w> <w n="20.5">mon</w> <w n="20.6">obsédant</w> <w n="20.7">rêve</w></l>
						<l n="21" num="1.21"><w n="21.1">Guette</w>, <w n="21.2">guette</w> <w n="21.3">à</w> <w n="21.4">jamais</w> <w n="21.5">la</w> <w n="21.6">sirène</w> <w n="21.7">du</w> <w n="21.8">Nord</w></l>
						<l n="22" num="1.22"><w n="22.1">Qui</w> <w n="22.2">chanta</w> <w n="22.3">tout</w> <w n="22.4">un</w> <w n="22.5">jour</w>, <w n="22.6">autrefois</w>, <w n="22.7">dans</w> <w n="22.8">le</w> <w n="22.9">port</w>.</l>
						<l n="23" num="1.23"><w n="23.1">Et</w> <w n="23.2">le</w> <w n="23.3">ciel</w>, <w n="23.4">l</w>’<w n="23.5">eau</w>, <w n="23.6">le</w> <w n="23.7">sol</w>, <w n="23.8">la</w> <w n="23.9">vase</w> <w n="23.10">aux</w> <w n="23.11">pâles</w> <w n="23.12">flammes</w></l>
						<l n="24" num="1.24"><w n="24.1">Sont</w>, <w n="24.2">pour</w> <w n="24.3">mes</w> <w n="24.4">yeux</w> <w n="24.5">remplis</w> <w n="24.6">par</w> <w n="24.7">un</w> <w n="24.8">songe</w> <w n="24.9">éternel</w>,</l>
						<l n="25" num="1.25"><space unit="char" quantity="8"></space><w n="25.1">Le</w> <w n="25.2">vivant</w> <w n="25.3">monstre</w> <w n="25.4">maternel</w></l>
						<l n="26" num="1.26"><w n="26.1">D</w>’<w n="26.2">où</w> <w n="26.3">mon</w> <w n="26.4">être</w> <w n="26.5">naquit</w> <w n="26.6">avec</w> <w n="26.7">toutes</w> <w n="26.8">ses</w> <w n="26.9">âmes</w>.</l>
					</lg>
				</div></body></text></TEI>