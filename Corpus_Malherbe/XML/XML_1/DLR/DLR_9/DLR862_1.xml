<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">LES SEPT DOULEURS D’OCTOBRE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>Delarue-Mardrus</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2316 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_9</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Sept douleurs d’Octobre</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/luciedelaruemardrueferveur.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Sept douleurs d’Octobre</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>J. Ferenczi et fils, éditeurs</publisher>
							<date when="1930">1930</date>
						</imprint>
					</monogr>
					<note>Édition de référence</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1930">1930</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Il manque de très nombreux poèmes dans le texte source.</p>
				<p>Les nombreux poèmes manquants ont été ajoutés à partir d’une numérisation de l’édition de référence.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-11-28" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">VIII</head><head type="main_part">BONHEURS</head><div type="poem" key="DLR862">
					<head type="main">MODES</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Je</w> <w n="1.2">vais</w> <w n="1.3">en</w> <w n="1.4">bas</w> <w n="1.5">couleur</w> <w n="1.6">de</w> <w n="1.7">jambes</w>, <w n="1.8">fins</w> <w n="1.9">souliers</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Robe</w> <w n="2.2">droite</w> <w n="2.3">à</w> <w n="2.4">la</w> <w n="2.5">taille</w> <w n="2.6">excessivement</w> <w n="2.7">basse</w></l>
						<l n="3" num="1.3"><w n="3.1">Et</w> <w n="3.2">si</w> <w n="3.3">courte</w> <w n="3.4">qu</w>’<w n="3.5">on</w> <w n="3.6">voit</w> <w n="3.7">mes</w> <w n="3.8">mollets</w> <w n="3.9">déliés</w>.</l>
						<l n="4" num="1.4"><w n="4.1">Mes</w> <w n="4.2">cheveux</w> <w n="4.3">sont</w> <w n="4.4">coupés</w> <w n="4.5">sous</w> <w n="4.6">mon</w> <w n="4.7">chapeau</w> <w n="4.8">cocasse</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Je</w> <w n="5.2">suis</w> <w n="5.3">la</w> <w n="5.4">mode</w> <w n="5.5">assez</w> <w n="5.6">pour</w> <w n="5.7">ne</w> <w n="5.8">détonner</w> <w n="5.9">point</w> :</l>
						<l n="6" num="2.2"><w n="6.1">Fourreaux</w> <w n="6.2">de</w> <w n="6.3">satin</w> <w n="6.4">noir</w>, <w n="6.5">foulards</w> <w n="6.6">de</w> <w n="6.7">teintes</w> <w n="6.8">crues</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Manteaux</w> <w n="7.2">rudes</w> <w n="7.3">de</w> <w n="7.4">poils</w> <w n="7.5">rayés</w> ; <w n="7.6">de</w> <w n="7.7">près</w>, <w n="7.8">de</w> <w n="7.9">loin</w>,</l>
						<l n="8" num="2.4"><w n="8.1">Mon</w> <w n="8.2">passage</w> <w n="8.3">élégant</w> <w n="8.4">n</w>’<w n="8.5">étonne</w> <w n="8.6">pas</w> <w n="8.7">les</w> <w n="8.8">rues</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Le</w> <w n="9.2">seul</w> <w n="9.3">étonnement</w> <w n="9.4">est</w> <w n="9.5">en</w> <w n="9.6">moi</w> — <w n="9.7">Car</w> <w n="9.8">je</w> <w n="9.9">vois</w></l>
						<l n="10" num="3.2"><w n="10.1">Cette</w> <w n="10.2">époque</w> <w n="10.3">où</w> <w n="10.4">je</w> <w n="10.5">vis</w> <w n="10.6">d</w>’<w n="10.7">avance</w> <w n="10.8">décédée</w>.</l>
						<l n="11" num="3.3"><w n="11.1">Tout</w> <w n="11.2">change</w>. <w n="11.3">En</w> <w n="11.4">souriant</w> <w n="11.5">ils</w> <w n="11.6">diront</w> : « <w n="11.7">Autrefois</w> ! »</l>
						<l n="12" num="3.4"><w n="12.1">Ma</w> <w n="12.2">toilette</w>, <w n="12.3">à</w> <w n="12.4">mes</w> <w n="12.5">yeux</w>, <w n="12.6">est</w> <w n="12.7">déjà</w> <w n="12.8">démodée</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">On</w> <w n="13.2">représentera</w> <w n="13.3">sur</w> <w n="13.4">des</w> <w n="13.5">scènes</w> <w n="13.6">cela</w>,</l>
						<l n="14" num="4.2"><w n="14.1">Et</w> <w n="14.2">vous</w> <w n="14.3">soupirerez</w>, <w n="14.4">jeunes</w> <w n="14.5">femmes</w> <w n="14.6">câlines</w>,</l>
						<l n="15" num="4.3"><w n="15.1">Et</w> <w n="15.2">songerez</w> : « <w n="15.3">O</w> <w n="15.4">charme</w> <w n="15.5">exquis</w> <w n="15.6">de</w> <w n="15.7">ce</w> <w n="15.8">temps</w>-<w n="15.9">là</w> ! »</l>
						<l n="16" num="4.4">— <w n="16.1">Comme</w> <w n="16.2">quand</w> <w n="16.3">nous</w> <w n="16.4">voyons</w> <w n="16.5">danser</w> <w n="16.6">des</w> <w n="16.7">crinolines</w>.</l>
					</lg>
				</div></body></text></TEI>