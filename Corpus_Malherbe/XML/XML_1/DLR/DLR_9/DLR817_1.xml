<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">LES SEPT DOULEURS D’OCTOBRE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>Delarue-Mardrus</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2316 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_9</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Sept douleurs d’Octobre</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/luciedelaruemardrueferveur.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Sept douleurs d’Octobre</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>J. Ferenczi et fils, éditeurs</publisher>
							<date when="1930">1930</date>
						</imprint>
					</monogr>
					<note>Édition de référence</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1930">1930</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Il manque de très nombreux poèmes dans le texte source.</p>
				<p>Les nombreux poèmes manquants ont été ajoutés à partir d’une numérisation de l’édition de référence.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-11-28" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">IV</head><head type="main_part">DE LA VIE A LA MORT</head><div type="poem" key="DLR817">
					<head type="main">MOURIR</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Mourir</w>… <w n="1.2">Parole</w> <w n="1.3">d</w>’<w n="1.4">épouvante</w></l>
						<l n="2" num="1.2"><space unit="char" quantity="10"></space><w n="2.1">Ou</w> <w n="2.2">d</w>’<w n="2.3">espoir</w>.</l>
						<l n="3" num="1.3"><w n="3.1">L</w>’<w n="3.2">âme</w> <w n="3.3">croit</w> <w n="3.4">s</w>’<w n="3.5">en</w> <w n="3.6">aller</w> <w n="3.7">vivante</w></l>
						<l n="4" num="1.4"><space unit="char" quantity="10"></space><w n="4.1">Dans</w> <w n="4.2">le</w> <w n="4.3">noir</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Fin</w> <w n="5.2">d</w>’<w n="5.3">une</w> <w n="5.4">fête</w> <w n="5.5">ou</w> <w n="5.6">d</w>’<w n="5.7">un</w> <w n="5.8">martyre</w>,</l>
						<l n="6" num="2.2"><space unit="char" quantity="10"></space><w n="6.1">Vaste</w> <w n="6.2">oubli</w>,</l>
						<l n="7" num="2.3"><w n="7.1">La</w> <w n="7.2">mort</w>, <w n="7.3">on</w> <w n="7.4">croirait</w>, <w n="7.5">nous</w> <w n="7.6">attire</w></l>
						<l n="8" num="2.4"><space unit="char" quantity="10"></space><w n="8.1">Dans</w> <w n="8.2">son</w> <w n="8.3">lit</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">La</w> <w n="9.2">vie</w> <w n="9.3">est</w> <w n="9.4">d</w>’<w n="9.5">un</w> <w n="9.6">coup</w> <w n="9.7">renversée</w></l>
						<l n="10" num="3.2"><space unit="char" quantity="10"></space><w n="10.1">Sous</w> <w n="10.2">l</w>’<w n="10.3">assaut</w>…</l>
						<l n="11" num="3.3"><w n="11.1">Que</w> <w n="11.2">non</w> ! <w n="11.3">La</w> <w n="11.4">mort</w> <w n="11.5">est</w> <w n="11.6">commencée</w></l>
						<l n="12" num="3.4"><space unit="char" quantity="10"></space><w n="12.1">Au</w> <w n="12.2">berceau</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Et</w> <w n="13.2">c</w>’<w n="13.3">est</w> <w n="13.4">sans</w> <w n="13.5">cesse</w> <w n="13.6">qu</w>’<w n="13.7">elle</w> <w n="13.8">pille</w></l>
						<l n="14" num="4.2"><space unit="char" quantity="10"></space><w n="14.1">Les</w> <w n="14.2">vivants</w>.</l>
						<l n="15" num="4.3"><w n="15.1">Où</w> <w n="15.2">donc</w> <w n="15.3">est</w> <w n="15.4">la</w> <w n="15.5">petite</w> <w n="15.6">fille</w></l>
						<l n="16" num="4.4"><space unit="char" quantity="10"></space><w n="16.1">A</w> <w n="16.2">tous</w> <w n="16.3">vents</w>,</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Où</w> <w n="17.2">l</w>’<w n="17.3">adolescente</w> <w n="17.4">aux</w> <w n="17.5">yeux</w> <w n="17.6">sombres</w></l>
						<l n="18" num="5.2"><space unit="char" quantity="10"></space><w n="18.1">Que</w> <w n="18.2">je</w> <w n="18.3">fus</w> ?</l>
						<l n="19" num="5.3"><w n="19.1">Hélas</w> ! <w n="19.2">Toutes</w> <w n="19.3">deux</w> <w n="19.4">ne</w> <w n="19.5">sont</w> <w n="19.6">plus</w></l>
						<l n="20" num="5.4"><space unit="char" quantity="10"></space><w n="20.1">Que</w> <w n="20.2">des</w> <w n="20.3">ombres</w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Qui</w> <w n="21.2">donc</w> <w n="21.3">a</w> <w n="21.4">sangloté</w>, <w n="21.5">pourtant</w>,</l>
						<l n="22" num="6.2"><space unit="char" quantity="10"></space><w n="22.1">Autour</w> <w n="22.2">d</w>’<w n="22.3">elles</w>,</l>
						<l n="23" num="6.3"><w n="23.1">Gagnant</w> <w n="23.2">avec</w> <w n="23.3">un</w> <w n="23.4">doux</w> <w n="23.5">bruit</w> <w n="23.6">d</w>’<w n="23.7">ailes</w></l>
						<l n="24" num="6.4"><space unit="char" quantity="10"></space><w n="24.1">Le</w> <w n="24.2">néant</w> ?</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">Partout</w> <w n="25.2">s</w>’<w n="25.3">infiltre</w> <w n="25.4">la</w> <w n="25.5">ténèbre</w></l>
						<l n="26" num="7.2"><space unit="char" quantity="10"></space><w n="26.1">De</w> <w n="26.2">la</w> <w n="26.3">mort</w>.</l>
						<l n="27" num="7.3"><w n="27.1">Chaque</w> <w n="27.2">soir</w>, <w n="27.3">le</w> <w n="27.4">lit</w> <w n="27.5">où</w> <w n="27.6">l</w>’<w n="27.7">on</w> <w n="27.8">dort</w></l>
						<l n="28" num="7.4"><space unit="char" quantity="10"></space><w n="28.1">Est</w> <w n="28.2">funèbre</w>.</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1"><w n="29.1">Mourir</w>, <w n="29.2">c</w>’<w n="29.3">est</w> <w n="29.4">cesser</w> <w n="29.5">de</w> <w n="29.6">mourir</w>,</l>
						<l n="30" num="8.2"><space unit="char" quantity="10"></space><w n="30.1">A</w> <w n="30.2">quelque</w> <w n="30.3">âge</w></l>
						<l n="31" num="8.3"><w n="31.1">Que</w> <w n="31.2">la</w> <w n="31.3">mort</w> <w n="31.4">nous</w> <w n="31.5">fasse</w> <w n="31.6">atterrir</w></l>
						<l n="32" num="8.4"><space unit="char" quantity="10"></space><w n="32.1">Au</w> <w n="32.2">rivage</w>.</l>
					</lg>
				</div></body></text></TEI>