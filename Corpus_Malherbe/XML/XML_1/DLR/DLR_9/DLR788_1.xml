<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">LES SEPT DOULEURS D’OCTOBRE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>Delarue-Mardrus</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2316 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_9</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Sept douleurs d’Octobre</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/luciedelaruemardrueferveur.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Sept douleurs d’Octobre</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>J. Ferenczi et fils, éditeurs</publisher>
							<date when="1930">1930</date>
						</imprint>
					</monogr>
					<note>Édition de référence</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1930">1930</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Il manque de très nombreux poèmes dans le texte source.</p>
				<p>Les nombreux poèmes manquants ont été ajoutés à partir d’une numérisation de l’édition de référence.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-11-28" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">I</head><head type="main_part">L’AUTOMNE</head><div type="poem" key="DLR788">
					<head type="main">TEMPÊTE D’OCTOBRE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Pour</w> <w n="1.2">remplacer</w> <w n="1.3">partout</w> <w n="1.4">l</w>’<w n="1.5">ancienne</w> <w n="1.6">verdure</w></l>
						<l n="2" num="1.2"><space unit="char" quantity="8"></space><w n="2.1">Par</w> <w n="2.2">on</w> <w n="2.3">ne</w> <w n="2.4">sait</w> <w n="2.5">quel</w> <w n="2.6">iodure</w>,</l>
					</lg>
					<lg n="2">
						<l n="3" num="2.1"><w n="3.1">Les</w> <w n="3.2">feuilles</w> <w n="3.3">mortes</w> <w n="3.4">ont</w> <w n="3.5">de</w> <w n="3.6">si</w> <w n="3.7">belles</w> <w n="3.8">couleurs</w></l>
						<l n="4" num="2.2"><space unit="char" quantity="8"></space><w n="4.1">Qu</w>’<w n="4.2">on</w> <w n="4.3">peut</w> <w n="4.4">croire</w> <w n="4.5">qu</w>’<w n="4.6">il</w> <w n="4.7">pleut</w> <w n="4.8">des</w> <w n="4.9">fleurs</w>.</l>
					</lg>
					<lg n="3">
						<l n="5" num="3.1"><w n="5.1">Le</w> <w n="5.2">féroce</w> <w n="5.3">chasseur</w> <w n="5.4">de</w> <w n="5.5">la</w> <w n="5.6">vieille</w> <w n="5.7">ballade</w></l>
						<l n="6" num="3.2"><space unit="char" quantity="8"></space><w n="6.1">Parcourt</w> <w n="6.2">cette</w> <w n="6.3">pourpre</w> <w n="6.4">malade</w>.</l>
					</lg>
					<lg n="4">
						<l n="7" num="4.1"><w n="7.1">Taïaut</w> ! <w n="7.2">C</w>’<w n="7.3">est</w> <w n="7.4">la</w> <w n="7.5">tempête</w>, <w n="7.6">au</w> <w n="7.7">fond</w> <w n="7.8">du</w> <w n="7.9">lointain</w> <w n="7.10">d</w>’<w n="7.11">or</w>,</l>
						<l n="8" num="4.2"><space unit="char" quantity="8"></space><w n="8.1">Qui</w> <w n="8.2">passe</w> <w n="8.3">et</w> <w n="8.4">qui</w> <w n="8.5">sonne</w> <w n="8.6">du</w> <w n="8.7">cor</w>.</l>
					</lg>
					<lg n="5">
						<l n="9" num="5.1"><w n="9.1">Tout</w> <w n="9.2">s</w>’<w n="9.3">effeuille</w>, <w n="9.4">se</w> <w n="9.5">tord</w>, <w n="9.6">s</w>’<w n="9.7">enfuit</w>. <w n="9.8">La</w> <w n="9.9">forêt</w> <w n="9.10">bronche</w>,</l>
						<l n="10" num="5.2"><space unit="char" quantity="8"></space><w n="10.1">Le</w> <w n="10.2">vent</w> <w n="10.3">immense</w> <w n="10.4">arrache</w> <w n="10.5">et</w> <w n="10.6">jonche</w>.</l>
					</lg>
					<lg n="6">
						<l n="11" num="6.1"><w n="11.1">On</w> <w n="11.2">dirait</w> <w n="11.3">que</w> <w n="11.4">plus</w> <w n="11.5">rien</w> <w n="11.6">ne</w> <w n="11.7">va</w> <w n="11.8">rester</w> <w n="11.9">debout</w></l>
						<l n="12" num="6.2"><space unit="char" quantity="8"></space><w n="12.1">Dans</w> <w n="12.2">cette</w> <w n="12.3">grande</w> <w n="12.4">fin</w> <w n="12.5">de</w> <w n="12.6">tout</w>.</l>
					</lg>
					<lg n="7">
						<l n="13" num="7.1"><w n="13.1">Et</w> <w n="13.2">l</w>’<w n="13.3">on</w> <w n="13.4">s</w>’<w n="13.5">en</w> <w n="13.6">va</w> <w n="13.7">parmi</w> <w n="13.8">cette</w> <w n="13.9">ivresse</w> <w n="13.10">farouche</w></l>
						<l n="14" num="7.2"><space unit="char" quantity="8"></space><w n="14.1">En</w> <w n="14.2">courant</w>, <w n="14.3">en</w> <w n="14.4">ouvrant</w> <w n="14.5">la</w> <w n="14.6">bouche</w>,</l>
					</lg>
					<lg n="8">
						<l n="15" num="8.1"><w n="15.1">Avec</w> <w n="15.2">l</w>’<w n="15.3">âpre</w> <w n="15.4">désir</w>, <w n="15.5">dans</w> <w n="15.6">ces</w> <w n="15.7">tourbillons</w> <w n="15.8">d</w>’<w n="15.9">or</w>,</l>
						<l n="16" num="8.2"><space unit="char" quantity="8"></space><w n="16.1">De</w> <w n="16.2">voler</w> <w n="16.3">comme</w> <w n="16.4">eux</w> <w n="16.5">à</w> <w n="16.6">la</w> <w n="16.7">mort</w>.</l>
					</lg>
				</div></body></text></TEI>