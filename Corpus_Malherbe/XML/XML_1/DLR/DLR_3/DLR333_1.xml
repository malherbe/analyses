<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">HORIZONS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1794 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Horizons</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>BIBLIOTHÈQUE-CHARPENTIER, EUGÈNE FASQUELLE, ÉDITEUR</publisher>
							<date when="1905">1905</date>
						</imprint>
					</monogr>
					<note>Édition numérisée sur demande (BnF)</note>
				</biblStruct>
			</sourceDesc>
			</fileDesc>
		<profileDesc>
			<creation>
				<date when="1905">1905</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">DE MOI-MÊME</head><div type="poem" key="DLR333">
					<head type="main">CALME</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Tu</w> <w n="1.2">regardes</w> <w n="1.3">changer</w> <w n="1.4">la</w> <w n="1.5">couleur</w> <w n="1.6">des</w> <w n="1.7">heures</w></l>
						<l n="2" num="1.2"><w n="2.1">Entre</w> <w n="2.2">les</w> <w n="2.3">branches</w> <w n="2.4">d</w>’<w n="2.5">or</w> <w n="2.6">du</w> <w n="2.7">jardin</w> <w n="2.8">automnal</w>…</l>
						<l n="3" num="1.3">— <w n="3.1">Tes</w> <w n="3.2">rêves</w> <w n="3.3">t</w>’<w n="3.4">auront</w> <w n="3.5">fait</w> <w n="3.6">tant</w> <w n="3.7">de</w> <w n="3.8">bien</w> <w n="3.9">et</w> <w n="3.10">de</w> <w n="3.11">mal</w> !</l>
						<l n="4" num="1.4"><w n="4.1">Mais</w> <w n="4.2">aujourd</w>’<w n="4.3">hui</w> <w n="4.4">plus</w> <w n="4.5">rien</w> <w n="4.6">n</w>’<w n="4.7">en</w> <w n="4.8">demeure</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Tes</w> <w n="5.2">bras</w> <w n="5.3">sont</w> <w n="5.4">retombés</w> <w n="5.5">qui</w> <w n="5.6">s</w>’<w n="5.7">ouvraient</w> <w n="5.8">vers</w> <w n="5.9">la</w> <w n="5.10">mer</w>,</l>
						<l n="6" num="2.2"><w n="6.1">Vers</w> <w n="6.2">la</w> <w n="6.3">possession</w> <w n="6.4">d</w>’<w n="6.5">extases</w> <w n="6.6">inouïes</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Vers</w> <w n="7.2">une</w> <w n="7.3">gloire</w> <w n="7.4">éclatant</w> <w n="7.5">en</w> <w n="7.6">trompettes</w> <w n="7.7">claires</w>,</l>
						<l n="8" num="2.4"><w n="8.1">Vers</w> <w n="8.2">l</w>’<w n="8.3">espace</w> <w n="8.4">où</w> <w n="8.5">brûlait</w> <w n="8.6">l</w>’<w n="8.7">esprit</w> <w n="8.8">d</w>’<w n="8.9">Adonaï</w> !</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Te</w> <w n="9.2">voici</w> <w n="9.3">vivre</w> <w n="9.4">dans</w> <w n="9.5">la</w> <w n="9.6">docilité</w></l>
						<l n="10" num="3.2"><w n="10.1">D</w>’<w n="10.2">une</w> <w n="10.3">plante</w> <w n="10.4">poussée</w> <w n="10.5">au</w> <w n="10.6">soleil</w> <w n="10.7">avec</w> <w n="10.8">joie</w></l>
						<l n="11" num="3.3"><w n="11.1">Et</w> <w n="11.2">qui</w> <w n="11.3">se</w> <w n="11.4">berce</w> <w n="11.5">un</w> <w n="11.6">peu</w> <w n="11.7">et</w> <w n="11.8">ploie</w></l>
						<l n="12" num="3.4"><w n="12.1">Sous</w> <w n="12.2">le</w> <w n="12.3">vent</w> <w n="12.4">d</w>’<w n="12.5">automne</w> <w n="12.6">ou</w> <w n="12.7">d</w>’<w n="12.8">été</w>,</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Et</w> <w n="13.2">cela</w> <w n="13.3">suffit</w> <w n="13.4">sans</w> <w n="13.5">doute</w></l>
						<l n="14" num="4.2"><w n="14.1">D</w>’<w n="14.2">être</w> <w n="14.3">une</w> <w n="14.4">femme</w> <w n="14.5">tendre</w> <w n="14.6">au</w> <w n="14.7">bras</w> <w n="14.8">de</w> <w n="14.9">son</w> <w n="14.10">ami</w>,</l>
						<l n="15" num="4.3"><w n="15.1">Qui</w> <w n="15.2">marche</w> <w n="15.3">dans</w> <w n="15.4">la</w> <w n="15.5">vie</w> <w n="15.6">en</w> <w n="15.7">rêvant</w> <w n="15.8">à</w> <w n="15.9">demi</w></l>
						<l n="16" num="4.4"><w n="16.1">Sans</w> <w n="16.2">plus</w> <w n="16.3">sentir</w> <w n="16.4">ses</w> <w n="16.5">pieds</w> <w n="16.6">se</w> <w n="16.7">meurtrir</w> <w n="16.8">sur</w> <w n="16.9">les</w> <w n="16.10">routes</w>…</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">— <w n="17.1">Mais</w> <w n="17.2">peut</w>-<w n="17.3">être</w> <w n="17.4">qu</w>’<w n="17.5">il</w> <w n="17.6">vit</w> <w n="17.7">encore</w>, <w n="17.8">ton</w> <w n="17.9">désir</w></l>
						<l n="18" num="5.2"><w n="18.1">D</w>’<w n="18.2">aller</w> <w n="18.3">vers</w> <w n="18.4">les</w> <w n="18.5">couchants</w> <w n="18.6">où</w> <w n="18.7">saigne</w> <w n="18.8">l</w>’<w n="18.9">Au</w> <w n="18.10">delà</w> ?</l>
						<l n="19" num="5.3"><w n="19.1">Car</w> <w n="19.2">l</w>’<w n="19.3">âme</w> <w n="19.4">qui</w> <w n="19.5">palpite</w> <w n="19.6">en</w> <w n="19.7">toi</w>, <w n="19.8">folle</w> <w n="19.9">ou</w> <w n="19.10">paisible</w>,</l>
						<l n="20" num="5.4"><w n="20.1">Tu</w> <w n="20.2">ne</w> <w n="20.3">la</w> <w n="20.4">connais</w> <w n="20.5">pas</w> ! <w n="20.6">Tu</w> <w n="20.7">ne</w> <w n="20.8">la</w> <w n="20.9">connais</w> <w n="20.10">pas</w> !</l>
					</lg>
				</div></body></text></TEI>