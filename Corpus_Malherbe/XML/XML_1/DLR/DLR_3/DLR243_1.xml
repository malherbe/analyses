<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">HORIZONS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1794 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Horizons</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>BIBLIOTHÈQUE-CHARPENTIER, EUGÈNE FASQUELLE, ÉDITEUR</publisher>
							<date when="1905">1905</date>
						</imprint>
					</monogr>
					<note>Édition numérisée sur demande (BnF)</note>
				</biblStruct>
			</sourceDesc>
			</fileDesc>
		<profileDesc>
			<creation>
				<date when="1905">1905</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">TENDRESSES</head><div type="poem" key="DLR243">
					<head type="main">ACCUEIL</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Pour</w> <w n="1.2">accueillir</w> (<w n="1.3">a</w> <w n="1.4">figure</w> <w n="1.5">d</w>’<w n="1.6">ivoire</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Je</w> <w n="2.2">me</w> <w n="2.3">tiendrai</w> <w n="2.4">debout</w> <w n="2.5">devant</w> <w n="2.6">le</w> <w n="2.7">seuil</w></l>
						<l n="3" num="1.3"><w n="3.1">Et</w> <w n="3.2">tu</w> <w n="3.3">viendras</w> <w n="3.4">vers</w> <w n="3.5">moi</w> <w n="3.6">du</w> <w n="3.7">fond</w> <w n="3.8">des</w> <w n="3.9">feuilles</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Une</w> <w n="4.2">restée</w> <w n="4.3">entre</w> <w n="4.4">tes</w> <w n="4.5">cheveux</w> <w n="4.6">noirs</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Tu</w> <w n="5.2">me</w> <w n="5.3">verras</w> ! <w n="5.4">de</w> <w n="5.5">velours</w> <w n="5.6">et</w> <w n="5.7">de</w> <w n="5.8">soie</w>,</l>
						<l n="6" num="2.2"><w n="6.1">Je</w> <w n="6.2">t</w>’<w n="6.3">attendrai</w> <w n="6.4">d</w>’<w n="6.5">un</w> <w n="6.6">geste</w> <w n="6.7">grave</w> <w n="6.8">et</w> <w n="6.9">mol</w> ;</l>
						<l n="7" num="2.3"><w n="7.1">Je</w> <w n="7.2">t</w>’<w n="7.3">attendrai</w> <w n="7.4">d</w>’<w n="7.5">un</w> <w n="7.6">sourire</w> <w n="7.7">d</w>’<w n="7.8">idole</w>,</l>
						<l n="8" num="2.4"><w n="8.1">Je</w> <w n="8.2">t</w>’<w n="8.3">offrirai</w> <w n="8.4">mes</w> <w n="8.5">bagues</w> <w n="8.6">et</w> <w n="8.7">mes</w> <w n="8.8">doigts</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Nous</w> <w n="9.2">entrerons</w>, <w n="9.3">toi</w> <w n="9.4">marchant</w>, <w n="9.5">moi</w> <w n="9.6">portée</w>…</l>
						<l n="10" num="3.2"><w n="10.1">Et</w> <w n="10.2">doucement</w> <w n="10.3">les</w> <w n="10.4">roses</w> <w n="10.5">du</w> <w n="10.6">dehors</w></l>
						<l n="11" num="3.3"><w n="11.1">Se</w> <w n="11.2">répandront</w> <w n="11.3">en</w> <w n="11.4">odeurs</w> <w n="11.5">d</w>’<w n="11.6">ambre</w> <w n="11.7">et</w> <w n="11.8">d</w>’<w n="11.9">or</w></l>
						<l n="12" num="3.4"><w n="12.1">Sur</w> <w n="12.2">notre</w> <w n="12.3">amour</w> <w n="12.4">et</w> <w n="12.5">sur</w> <w n="12.6">notre</w> <w n="12.7">beauté</w>.</l>
					</lg>
				</div></body></text></TEI>