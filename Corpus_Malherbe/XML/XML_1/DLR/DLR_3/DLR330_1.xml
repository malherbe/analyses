<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">HORIZONS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1794 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Horizons</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>BIBLIOTHÈQUE-CHARPENTIER, EUGÈNE FASQUELLE, ÉDITEUR</publisher>
							<date when="1905">1905</date>
						</imprint>
					</monogr>
					<note>Édition numérisée sur demande (BnF)</note>
				</biblStruct>
			</sourceDesc>
			</fileDesc>
		<profileDesc>
			<creation>
				<date when="1905">1905</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">DE MOI-MÊME</head><div type="poem" key="DLR330">
					<head type="main">LA VAILLANCE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">La</w> <w n="1.2">vie</w> <w n="1.3">est</w> <w n="1.4">triste</w> ; <w n="1.5">l</w>’<w n="1.6">espoir</w> <w n="1.7">meugle</w></l>
						<l n="2" num="1.2"><w n="2.1">Comme</w> <w n="2.2">un</w> <w n="2.3">bœuf</w> <w n="2.4">condamné</w> <w n="2.5">qui</w> <w n="2.6">marche</w> <w n="2.7">à</w> <w n="2.8">l</w>’<w n="2.9">abattoir</w></l>
						<l n="3" num="1.3">— <w n="3.1">Crains</w>, <w n="3.2">autant</w> <w n="3.3">que</w> <w n="3.4">l</w>’<w n="3.5">horreur</w> <w n="3.6">de</w> <w n="3.7">devenir</w> <w n="3.8">aveugle</w>,</l>
						<l n="4" num="1.4"><w n="4.1">La</w> <w n="4.2">possible</w> <w n="4.3">contagion</w> <w n="4.4">d</w>’<w n="4.5">un</w> <w n="4.6">autre</w> <w n="4.7">espoir</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Ne</w> <w n="5.2">t</w>’<w n="5.3">arrête</w> <w n="5.4">jamais</w> <w n="5.5">sur</w> <w n="5.6">la</w> <w n="5.7">route</w> <w n="5.8">de</w> <w n="5.9">vivre</w></l>
						<l n="6" num="2.2"><w n="6.1">Avec</w> <w n="6.2">un</w> <w n="6.3">regard</w> <w n="6.4">derrière</w> <w n="6.5">toi</w></l>
						<l n="7" num="2.3"><w n="7.1">Mesurant</w> <w n="7.2">le</w> <w n="7.3">chemin</w> <w n="7.4">que</w> <w n="7.5">tu</w> <w n="7.6">viens</w> <w n="7.7">de</w> <w n="7.8">suivre</w></l>
						<l n="8" num="2.4"><w n="8.1">Entre</w> <w n="8.2">l</w>’<w n="8.3">aujourd</w>’<w n="8.4">hui</w> <w n="8.5">mûr</w> <w n="8.6">et</w> <w n="8.7">le</w> <w n="8.8">vert</w> <w n="8.9">autrefois</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Fuis</w> <w n="9.2">les</w> <w n="9.3">orgues</w>, <w n="9.4">les</w> <w n="9.5">cors</w> <w n="9.6">dans</w> <w n="9.7">le</w> <w n="9.8">lointain</w>, <w n="9.9">les</w> <w n="9.10">cloches</w></l>
						<l n="10" num="3.2"><w n="10.1">N</w>’<w n="10.2">écoute</w> <w n="10.3">pas</w>. <w n="10.4">Ne</w> <w n="10.5">sache</w> <w n="10.6">pas</w>. <w n="10.7">Ne</w> <w n="10.8">veuille</w> <w n="10.9">pas</w>.</l>
						<l n="11" num="3.3"><w n="11.1">Toute</w> <w n="11.2">l</w>’<w n="11.3">enfance</w> <w n="11.4">est</w> <w n="11.5">encore</w> <w n="11.6">là</w>-<w n="11.7">bas</w>,</l>
						<l n="12" num="3.4"><w n="12.1">Elle</w> <w n="12.2">te</w> <w n="12.3">reprendra</w> <w n="12.4">ton</w> <w n="12.5">cœur</w> <w n="12.6">si</w> <w n="12.7">tu</w> <w n="12.8">t</w>’<w n="12.9">approches</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Marche</w> <w n="13.2">sur</w> <w n="13.3">l</w>’<w n="13.4">avenir</w> <w n="13.5">toujours</w> <w n="13.6">plus</w> <w n="13.7">durement</w>,</l>
						<l n="14" num="4.2"><w n="14.1">De</w> <w n="14.2">peur</w> <w n="14.3">de</w> <w n="14.4">perdre</w> <w n="14.5">l</w>’<w n="14.6">habitude</w> <w n="14.7">acquise</w> <w n="14.8">à</w> <w n="14.9">peine</w></l>
						<l n="15" num="4.3"><w n="15.1">De</w> <w n="15.2">vivre</w> <w n="15.3">pour</w> <w n="15.4">la</w> <w n="15.5">vie</w>, <w n="15.6">en</w> <w n="15.7">comptant</w> <w n="15.8">seulement</w></l>
						<l n="16" num="4.4"><w n="16.1">Mourir</w> <w n="16.2">le</w> <w n="16.3">plus</w> <w n="16.4">tard</w> <w n="16.5">possible</w> <w n="16.6">à</w> <w n="16.7">la</w> <w n="16.8">peine</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Va</w> ! <w n="17.2">chante</w>, <w n="17.3">danse</w>, <w n="17.4">crie</w>, <w n="17.5">en</w> <w n="17.6">marchant</w>, <w n="17.7">ta</w> <w n="17.8">gaieté</w></l>
						<l n="18" num="5.2"><w n="18.1">De</w> <w n="18.2">ne</w> <w n="18.3">plus</w> <w n="18.4">rien</w> <w n="18.5">attendre</w> <w n="18.6">et</w> <w n="18.7">de</w> <w n="18.8">ne</w> <w n="18.9">plus</w> <w n="18.10">rien</w> <w n="18.11">croire</w> ;</l>
						<l n="19" num="5.3"><w n="19.1">Cela</w> <w n="19.2">seul</w> <w n="19.3">est</w>, <w n="19.4">au</w> <w n="19.5">bout</w> <w n="19.6">de</w> <w n="19.7">toute</w> <w n="19.8">obscurité</w>,</l>
						<l n="20" num="5.4"><w n="20.1">La</w> <w n="20.2">lézarde</w> <w n="20.3">de</w> <w n="20.4">jour</w> <w n="20.5">qui</w> <w n="20.6">fend</w> <w n="20.7">l</w>’<w n="20.8">impasse</w> <w n="20.9">noire</w>.</l>
					</lg>
				</div></body></text></TEI>