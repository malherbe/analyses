<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">HORIZONS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1794 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Horizons</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>BIBLIOTHÈQUE-CHARPENTIER, EUGÈNE FASQUELLE, ÉDITEUR</publisher>
							<date when="1905">1905</date>
						</imprint>
					</monogr>
					<note>Édition numérisée sur demande (BnF)</note>
				</biblStruct>
			</sourceDesc>
			</fileDesc>
		<profileDesc>
			<creation>
				<date when="1905">1905</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">DE MOI-MÊME</head><div type="poem" key="DLR325">
					<head type="main">AMIE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Mon</w> <w n="1.2">regard</w> <w n="1.3">vainement</w> <w n="1.4">attentif</w> <w n="1.5">et</w> <w n="1.6">subtil</w></l>
						<l n="2" num="1.2"><w n="2.1">S</w>’<w n="2.2">est</w> <w n="2.3">plongé</w> <w n="2.4">dans</w> <w n="2.5">les</w> <w n="2.6">yeux</w> <w n="2.7">mystérieux</w> <w n="2.8">des</w> <w n="2.9">femmes</w>.</l>
						<l n="3" num="1.3"><w n="3.1">O</w> <w n="3.2">mon</w> <w n="3.3">regard</w> <w n="3.4">lassé</w> ! <w n="3.5">se</w> <w n="3.6">peut</w>-<w n="3.7">il</w>, <w n="3.8">se</w> <w n="3.9">peut</w>-<w n="3.10">il</w></l>
						<l n="4" num="1.4"><w n="4.1">Que</w> <w n="4.2">le</w> <w n="4.3">silence</w> <w n="4.4">soit</w> <w n="4.5">si</w> <w n="4.6">rare</w> <w n="4.7">au</w> <w n="4.8">fond</w> <w n="4.9">des</w> <w n="4.10">âmes</w> ?…</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Mais</w> <w n="5.2">les</w> <w n="5.3">lacs</w> <w n="5.4">troubles</w> <w n="5.5">sont</w> <w n="5.6">plus</w> <w n="5.7">profonds</w> <w n="5.8">que</w> <w n="5.9">les</w> <w n="5.10">yeux</w>,</l>
						<l n="6" num="2.2"><w n="6.1">Et</w> <w n="6.2">voici</w> <w n="6.3">dans</w> <w n="6.4">les</w> <w n="6.5">eaux</w> <w n="6.6">la</w> <w n="6.7">commençante</w> <w n="6.8">automne</w>.</l>
						<l n="7" num="2.3"><w n="7.1">Allons</w> <w n="7.2">voir</w> <w n="7.3">luire</w> <w n="7.4">au</w> <w n="7.5">cœur</w> <w n="7.6">de</w> <w n="7.7">ses</w> <w n="7.8">bois</w> <w n="7.9">qui</w> <w n="7.10">moutonnent</w></l>
						<l n="8" num="2.4"><w n="8.1">Des</w> <w n="8.2">morceaux</w> <w n="8.3">de</w> <w n="8.4">couchant</w> <w n="8.5">entre</w> <w n="8.6">les</w> <w n="8.7">sapins</w> <w n="8.8">bleus</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Elle</w> <w n="9.2">meurt</w> <w n="9.3">chaque</w> <w n="9.4">jour</w> <w n="9.5">tragiquement</w> <w n="9.6">au</w> <w n="9.7">monde</w> ;</l>
						<l n="10" num="3.2"><w n="10.1">Ses</w> <w n="10.2">saules</w> <w n="10.3">sont</w> <w n="10.4">plus</w> <w n="10.5">beaux</w> <w n="10.6">que</w> <w n="10.7">des</w> <w n="10.8">cheveux</w> <w n="10.9">défaits</w>…</l>
						<l n="11" num="3.3"><w n="11.1">Ah</w> ! <w n="11.2">qui</w> <w n="11.3">vaudra</w> <w n="11.4">jamais</w> <w n="11.5">cette</w> <w n="11.6">grande</w> <w n="11.7">âme</w> <w n="11.8">blonde</w></l>
						<l n="12" num="3.4"><w n="12.1">Qui</w> <w n="12.2">s</w>’<w n="12.3">effeuille</w> <w n="12.4">sur</w> <w n="12.5">nous</w> <w n="12.6">au</w> <w n="12.7">passage</w> <w n="12.8">et</w> <w n="12.9">se</w> <w n="12.10">tait</w> ?</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">— <w n="13.1">Tu</w> <w n="13.2">seras</w> <w n="13.3">mon</w> <w n="13.4">amie</w>, <w n="13.5">automne</w> <w n="13.6">ardente</w> <w n="13.7">et</w> <w n="13.8">sobre</w>,</l>
						<l n="14" num="4.2"><w n="14.1">Et</w> <w n="14.2">j</w>’<w n="14.3">appuierai</w> <w n="14.4">mon</w> <w n="14.5">cœur</w> <w n="14.6">contre</w> <w n="14.7">ton</w> <w n="14.8">cœur</w> <w n="14.9">brisé</w>,</l>
						<l n="15" num="4.3"><w n="15.1">Et</w> <w n="15.2">tu</w> <w n="15.3">me</w> <w n="15.4">donneras</w> <w n="15.5">tes</w> <w n="15.6">lèvres</w> <w n="15.7">à</w> <w n="15.8">baiser</w></l>
						<l n="16" num="4.4"><w n="16.1">Sur</w> <w n="16.2">l</w>’<w n="16.3">humide</w> <w n="16.4">douceur</w> <w n="16.5">de</w> <w n="16.6">tes</w> <w n="16.7">roses</w> <w n="16.8">d</w>’<w n="16.9">octobre</w>.</l>
					</lg>
				</div></body></text></TEI>