<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">HORIZONS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1794 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Horizons</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>BIBLIOTHÈQUE-CHARPENTIER, EUGÈNE FASQUELLE, ÉDITEUR</publisher>
							<date when="1905">1905</date>
						</imprint>
					</monogr>
					<note>Édition numérisée sur demande (BnF)</note>
				</biblStruct>
			</sourceDesc>
			</fileDesc>
		<profileDesc>
			<creation>
				<date when="1905">1905</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LE LONG DES JARDINS ET DE L’EAU</head><div type="poem" key="DLR267">
					<head type="main">LE VENT</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Le</w> <w n="1.2">vent</w> <w n="1.3">tombé</w> <w n="1.4">revient</w> <w n="1.5">comme</w> <w n="1.6">un</w> <w n="1.7">raz</w> <w n="1.8">de</w> <w n="1.9">marée</w></l>
						<l n="2" num="1.2"><w n="2.1">Tourmenter</w> <w n="2.2">les</w> <w n="2.3">villes</w> <w n="2.4">carrées</w></l>
						<l n="3" num="1.3"><w n="3.1">Et</w> <w n="3.2">les</w> <w n="3.3">arbres</w> <w n="3.4">de</w> <w n="3.5">leurs</w> <w n="3.6">jardins</w>.</l>
					</lg>
					<lg n="2">
						<l n="4" num="2.1"><w n="4.1">Il</w> <w n="4.2">déferle</w> <w n="4.3">sur</w> <w n="4.4">nous</w> <w n="4.5">par</w> <w n="4.6">gigantesques</w> <w n="4.7">lames</w>,</l>
						<l n="5" num="2.2"><w n="5.1">Il</w> <w n="5.2">voudrait</w> <w n="5.3">nous</w> <w n="5.4">arracher</w> <w n="5.5">l</w>’<w n="5.6">âme</w></l>
						<l n="6" num="2.3"><w n="6.1">Gomme</w> <w n="6.2">les</w> <w n="6.3">feuilles</w> <w n="6.4">des</w> <w n="6.5">jardins</w>.</l>
					</lg>
					<lg n="3">
						<l n="7" num="3.1"><w n="7.1">Ah</w> ! <w n="7.2">livrons</w> <w n="7.3">notre</w> <w n="7.4">orgueil</w> <w n="7.5">et</w> <w n="7.6">notre</w> <w n="7.7">véhémence</w></l>
						<l n="8" num="3.2"><w n="8.1">Au</w> <w n="8.2">flot</w> <w n="8.3">qui</w> <w n="8.4">meurt</w> <w n="8.5">et</w> <w n="8.6">recommence</w></l>
						<l n="9" num="3.3"><w n="9.1">De</w> <w n="9.2">cette</w> <w n="9.3">mer</w> <w n="9.4">qu</w>’<w n="9.5">on</w> <w n="9.6">ne</w> <w n="9.7">voit</w> <w n="9.8">pas</w>,</l>
					</lg>
					<lg n="4">
						<l n="10" num="4.1"><w n="10.1">Et</w> <w n="10.2">que</w> <w n="10.3">notre</w> <w n="10.4">pensée</w>, <w n="10.5">âpre</w>, <w n="10.6">agile</w> <w n="10.7">et</w> <w n="10.8">muette</w>,</l>
						<l n="11" num="4.2"><w n="11.1">Domine</w> <w n="11.2">ainsi</w> <w n="11.3">qu</w>’<w n="11.4">une</w> <w n="11.5">mouette</w></l>
						<l n="12" num="4.3"><w n="12.1">Tant</w> <w n="12.2">d</w>’<w n="12.3">énergie</w> <w n="12.4">et</w> <w n="12.5">de</w> <w n="12.6">fracas</w> !</l>
					</lg>
					<lg n="5">
						<l n="13" num="5.1">— <w n="13.1">Le</w> <w n="13.2">vent</w> ! <w n="13.3">Le</w> <w n="13.4">vent</w> ! <w n="13.5">Voici</w> <w n="13.6">que</w> <w n="13.7">le</w> <w n="13.8">monde</w> <w n="13.9">s</w>’<w n="13.10">écroule</w></l>
						<l n="14" num="5.2"><w n="14.1">Ivre</w> <w n="14.2">d</w>’<w n="14.3">un</w> <w n="14.4">souvenir</w> <w n="14.5">de</w> <w n="14.6">houle</w>,</l>
						<l n="15" num="5.3"><w n="15.1">D</w>’<w n="15.2">écume</w>, <w n="15.3">d</w>’<w n="15.4">embrun</w> <w n="15.5">et</w> <w n="15.6">de</w> <w n="15.7">sel</w>,</l>
					</lg>
					<lg n="6">
						<l n="16" num="6.1"><w n="16.1">Je</w> <w n="16.2">foncerai</w>, <w n="16.3">les</w> <w n="16.4">yeux</w> <w n="16.5">fermés</w>, <w n="16.6">sur</w> <w n="16.7">la</w> <w n="16.8">tempête</w>,</l>
						<l n="17" num="6.2"><w n="17.1">Pour</w> <w n="17.2">croire</w> <w n="17.3">que</w> <w n="17.4">je</w> <w n="17.5">troue</w> <w n="17.6">et</w> <w n="17.7">brise</w> <w n="17.8">avec</w> <w n="17.9">ma</w> <w n="17.10">tête</w></l>
						<l n="18" num="6.3"><w n="18.1">Le</w> <w n="18.2">tourbillon</w> <w n="18.3">universel</w> !</l>
					</lg>
				</div></body></text></TEI>