<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">HORIZONS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1794 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Horizons</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>BIBLIOTHÈQUE-CHARPENTIER, EUGÈNE FASQUELLE, ÉDITEUR</publisher>
							<date when="1905">1905</date>
						</imprint>
					</monogr>
					<note>Édition numérisée sur demande (BnF)</note>
				</biblStruct>
			</sourceDesc>
			</fileDesc>
		<profileDesc>
			<creation>
				<date when="1905">1905</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">TENDRESSES</head><div type="poem" key="DLR246">
					<head type="main">LA CONSOLATION</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Je</w> <w n="1.2">t</w>’<w n="1.3">apporte</w> <w n="1.4">en</w> <w n="1.5">pleurant</w> <w n="1.6">mon</w> <w n="1.7">âme</w> <w n="1.8">de</w> <w n="1.9">ce</w> <w n="1.10">soir</w> :</l>
						<l n="2" num="1.2"><w n="2.1">On</w> <w n="2.2">l</w>’<w n="2.3">a</w> <w n="2.4">blessée</w> ! <w n="2.5">On</w> <w n="2.6">l</w>’<w n="2.7">a</w> <w n="2.8">blessée</w> !…</l>
					</lg>
					<lg n="2">
						<l n="3" num="2.1"><w n="3.1">Toi</w> <w n="3.2">qui</w> <w n="3.3">m</w>’<w n="3.4">aimes</w>, <w n="3.5">berce</w>-<w n="3.6">moi</w> <w n="3.7">contre</w> <w n="3.8">toi</w>,</l>
						<l n="4" num="2.2"><w n="4.1">Berce</w>-<w n="4.2">moi</w>, <w n="4.3">peureuse</w> <w n="4.4">et</w> <w n="4.5">tassée</w>,</l>
						<l n="5" num="2.3"><w n="5.1">Et</w>, <w n="5.2">sur</w> <w n="5.3">ta</w> <w n="5.4">large</w> <w n="5.5">épaule</w> <w n="5.6">où</w> <w n="5.7">je</w> <w n="5.8">me</w> <w n="5.9">sens</w> <w n="5.10">si</w> <w n="5.11">bien</w>,</l>
						<l n="6" num="2.4"><w n="6.1">Garde</w>-<w n="6.2">moi</w> <w n="6.3">sans</w> <w n="6.4">me</w> <w n="6.5">dire</w> <w n="6.6">rien</w>.</l>
					</lg>
					<lg n="3">
						<l n="7" num="3.1"><w n="7.1">Il</w> <w n="7.2">fait</w> <w n="7.3">bon</w> <w n="7.4">contre</w> <w n="7.5">toi</w> <w n="7.6">quand</w> <w n="7.7">je</w> <w n="7.8">souffre</w>… <w n="7.9">Ah</w> <w n="7.10">qu</w>’<w n="7.11">importe</w></l>
						<l n="8" num="3.2"><w n="8.1">Que</w> <w n="8.2">je</w> <w n="8.3">souffre</w> ! <w n="8.4">Ou</w> <w n="8.5">plutôt</w>, <w n="8.6">tant</w> <w n="8.7">mieux</w> ! <w n="8.8">Ta</w> <w n="8.9">douce</w> <w n="8.10">et</w> <w n="8.11">forte</w></l>
						<l n="9" num="3.3"><w n="9.1">Et</w> <w n="9.2">si</w> <w n="9.3">chaude</w> <w n="9.4">poitrine</w> <w n="9.5">en</w> <w n="9.6">est</w> <w n="9.7">meilleure</w> <w n="9.8">encore</w>,</l>
					</lg>
					<lg n="4">
						<l n="10" num="4.1"><w n="10.1">Car</w> <w n="10.2">voici</w> <w n="10.3">qu</w>’<w n="10.4">alanguie</w> <w n="10.5">et</w> <w n="10.6">toute</w> <w n="10.7">morte</w></l>
						<l n="11" num="4.2"><w n="11.1">De</w> <w n="11.2">tendresse</w>, <w n="11.3">mon</w> <w n="11.4">âme</w> <w n="11.5">amère</w> <w n="11.6">s</w>’<w n="11.7">y</w> <w n="11.8">endort</w></l>
						<l n="12" num="4.3"><w n="12.1">Comme</w> <w n="12.2">un</w> <w n="12.3">petit</w> <w n="12.4">enfant</w> <w n="12.5">sur</w> <w n="12.6">le</w> <w n="12.7">bras</w> <w n="12.8">qui</w> <w n="12.9">le</w> <w n="12.10">porte</w></l>
					</lg>
				</div></body></text></TEI>