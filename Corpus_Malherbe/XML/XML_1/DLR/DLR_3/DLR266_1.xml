<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">HORIZONS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1794 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Horizons</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>BIBLIOTHÈQUE-CHARPENTIER, EUGÈNE FASQUELLE, ÉDITEUR</publisher>
							<date when="1905">1905</date>
						</imprint>
					</monogr>
					<note>Édition numérisée sur demande (BnF)</note>
				</biblStruct>
			</sourceDesc>
			</fileDesc>
		<profileDesc>
			<creation>
				<date when="1905">1905</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LE LONG DES JARDINS ET DE L’EAU</head><div type="poem" key="DLR266">
					<head type="main">ESSAI</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Nous</w> <w n="1.2">irons</w> <w n="1.3">sur</w> <w n="1.4">le</w> <w n="1.5">bord</w> <w n="1.6">des</w> <w n="1.7">eaux</w></l>
						<l n="2" num="1.2"><w n="2.1">D</w>’<w n="2.2">avant</w> <w n="2.3">Paris</w>, <w n="2.4">claires</w> <w n="2.5">et</w> <w n="2.6">belles</w>.</l>
						<l n="3" num="1.3"><w n="3.1">Nous</w> <w n="3.2">aurons</w> <w n="3.3">par</w> <w n="3.4">les</w> <w n="3.5">prés</w> <w n="3.6">l</w>’<w n="3.7">âme</w> <w n="3.8">des</w> <w n="3.9">bestiaux</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Leur</w> <w n="4.2">bon</w> <w n="4.3">regard</w> <w n="4.4">dans</w> <w n="4.5">les</w> <w n="4.6">prunelles</w>.</l>
						<l n="5" num="1.5"><w n="5.1">Et</w> <w n="5.2">nous</w> <w n="5.3">serons</w> <w n="5.4">aussi</w> <w n="5.5">contents</w></l>
						<l n="6" num="1.6"><w n="6.1">Devant</w> <w n="6.2">l</w>’<w n="6.3">ampleur</w> <w n="6.4">des</w> <w n="6.5">lignes</w> <w n="6.6">naturelles</w></l>
						<l n="7" num="1.7"><w n="7.1">De</w> <w n="7.2">la</w> <w n="7.3">Seine</w> <w n="7.4">qui</w> <w n="7.5">tourne</w> <w n="7.6">autour</w> <w n="7.7">de</w> <w n="7.8">ses</w> <w n="7.9">coteaux</w>,</l>
						<l n="8" num="1.8"><w n="8.1">Que</w> <w n="8.2">de</w> <w n="8.3">voir</w> <w n="8.4">de</w> <w n="8.5">tout</w> <w n="8.6">près</w>, <w n="8.7">bercé</w> <w n="8.8">dans</w> <w n="8.9">le</w> <w n="8.10">beau</w> <w n="8.11">temps</w>,</l>
						<l n="9" num="1.9"><w n="9.1">Un</w> <w n="9.2">bourdon</w> <w n="9.3">roux</w> <w n="9.4">sur</w> <w n="9.5">une</w> <w n="9.6">ombelle</w>.</l>
					</lg>
					<lg n="2">
						<l n="10" num="2.1"><w n="10.1">Ainsi</w>, <w n="10.2">nous</w> <w n="10.3">irons</w> <w n="10.4">lentement</w> <w n="10.5">et</w> <w n="10.6">loin</w></l>
						<l n="11" num="2.2"><w n="11.1">Selon</w> <w n="11.2">le</w> <w n="11.3">trèfle</w>, <w n="11.4">le</w> <w n="11.5">sainfoin</w>,</l>
						<l n="12" num="2.3"><w n="12.1">La</w> <w n="12.2">belladone</w> <w n="12.3">et</w> <w n="12.4">la</w> <w n="12.5">marguerite</w> <w n="12.6">champêtre</w>,</l>
						<l n="13" num="2.4"><w n="13.1">Cueillant</w> <w n="13.2">de</w> <w n="13.3">beaux</w> <w n="13.4">bouquets</w>, <w n="13.5">et</w> <w n="13.6">roses</w> <w n="13.7">de</w> <w n="13.8">bien</w>-<w n="13.9">être</w>.</l>
						<l n="14" num="2.5"><w n="14.1">Et</w> <w n="14.2">sans</w> <w n="14.3">doute</w> <w n="14.4">oublierons</w>-<w n="14.5">nous</w> <w n="14.6">un</w> <w n="14.7">peu</w></l>
						<l n="15" num="2.6"><w n="15.1">A</w> <w n="15.2">la</w> <w n="15.3">longue</w>, <w n="15.4">au</w> <w n="15.5">bout</w> <w n="15.6">de</w> <w n="15.7">cette</w> <w n="15.8">herbe</w> <w n="15.9">tranquille</w>,</l>
						<l n="16" num="2.7"><w n="16.1">De</w> <w n="16.2">ces</w> <w n="16.3">beaux</w> <w n="16.4">peupliers</w> <w n="16.5">en</w> <w n="16.6">file</w>,</l>
						<l n="17" num="2.8"><w n="17.1">De</w> <w n="17.2">cette</w> <w n="17.3">Seine</w> <w n="17.4">au</w> <w n="17.5">tournant</w> <w n="17.6">bleu</w>,</l>
						<l n="18" num="2.9"><w n="18.1">Au</w> <w n="18.2">bout</w> <w n="18.3">de</w> <w n="18.4">tant</w> <w n="18.5">d</w>’<w n="18.6">odeurs</w>, <w n="18.7">au</w> <w n="18.8">bout</w> <w n="18.9">de</w> <w n="18.10">tant</w> <w n="18.11">de</w> <w n="18.12">fleurs</w></l>
						<l n="19" num="2.10"><w n="19.1">Où</w> <w n="19.2">l</w>’<w n="19.3">heure</w> <w n="19.4">change</w> <w n="19.5">de</w> <w n="19.6">couleur</w>,</l>
						<l n="20" num="2.11">— <w n="20.1">Monstrueusement</w> <w n="20.2">belle</w> <w n="20.3">et</w> <w n="20.4">proche</w> — <w n="20.5">notre</w> <w n="20.6">ville</w> ?</l>
					</lg>
				</div></body></text></TEI>