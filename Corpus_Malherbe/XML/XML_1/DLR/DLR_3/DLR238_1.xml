<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">HORIZONS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1794 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Horizons</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>BIBLIOTHÈQUE-CHARPENTIER, EUGÈNE FASQUELLE, ÉDITEUR</publisher>
							<date when="1905">1905</date>
						</imprint>
					</monogr>
					<note>Édition numérisée sur demande (BnF)</note>
				</biblStruct>
			</sourceDesc>
			</fileDesc>
		<profileDesc>
			<creation>
				<date when="1905">1905</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">TENDRESSES</head><div type="poem" key="DLR238">
					<head type="main">UN JOUR</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Nous</w> <w n="1.2">avons</w> <w n="1.3">écrit</w>, <w n="1.4">lu</w>, — <w n="1.5">travaillé</w>, <w n="1.6">en</w> <w n="1.7">somme</w>.</l>
						<l n="2" num="1.2"><w n="2.1">Nous</w> <w n="2.2">voici</w> <w n="2.3">jouant</w> <w n="2.4">à</w> <w n="2.5">travers</w> <w n="2.6">la</w> <w n="2.7">maison</w>.</l>
						<l n="3" num="1.3"><w n="3.1">Par</w> <w n="3.2">la</w> <w n="3.3">fenêtre</w>, — <w n="3.4">lourde</w> <w n="3.5">et</w> <w n="3.6">belle</w>, —</l>
						<l n="4" num="1.4"><w n="4.1">Entre</w> <w n="4.2">une</w> <w n="4.3">rose</w> <w n="4.4">de</w> <w n="4.5">saison</w>.</l>
						<l n="5" num="1.5"><w n="5.1">Ma</w> <w n="5.2">voix</w> <w n="5.3">de</w> <w n="5.4">femme</w>, <w n="5.5">en</w> <w n="5.6">bas</w>, <w n="5.7">répond</w> <w n="5.8">à</w> <w n="5.9">ta</w> <w n="5.10">voix</w> <w n="5.11">d</w>’<w n="5.12">homme</w></l>
						<l n="6" num="1.6"><w n="6.1">En</w> <w n="6.2">haut</w> <w n="6.3">sur</w> <w n="6.4">l</w>’<w n="6.5">escalier</w>. <w n="6.6">Nous</w> <w n="6.7">sommes</w></l>
						<l n="7" num="1.7"><w n="7.1">Gomme</w> <w n="7.2">des</w> <w n="7.3">perdrix</w> <w n="7.4">qui</w> <w n="7.5">rappellent</w>.</l>
					</lg>
					<lg n="2">
						<l n="8" num="2.1"><w n="8.1">Le</w> <w n="8.2">chat</w> <w n="8.3">assis</w> <w n="8.4">regarde</w> <w n="8.5">avec</w> <w n="8.6">l</w>’<w n="8.7">air</w> <w n="8.8">étonné</w></l>
						<l n="9" num="2.2"><w n="9.1">Qu</w>’<w n="9.2">ont</w> <w n="9.3">les</w> <w n="9.4">animaux</w> <w n="9.5">de</w> <w n="9.6">faïence</w> ;</l>
						<l n="10" num="2.3"><w n="10.1">La</w> <w n="10.2">chèvre</w> <w n="10.3">bêle</w> <w n="10.4">dans</w> <w n="10.5">son</w> <w n="10.6">parc</w> ; <w n="10.7">les</w> <w n="10.8">poules</w> <w n="10.9">lancent</w></l>
						<l n="11" num="2.4"><w n="11.1">Le</w> <w n="11.2">cri</w> <w n="11.3">de</w> <w n="11.4">l</w>’<w n="11.5">œuf</w>, <w n="11.6">blanche</w> <w n="11.7">gésine</w>.</l>
						<l n="12" num="2.5"><w n="12.1">Toute</w> <w n="12.2">seule</w> <w n="12.3">dans</w> <w n="12.4">la</w> <w n="12.5">cuisine</w>,</l>
						<l n="13" num="2.6"><w n="13.1">La</w> <w n="13.2">servante</w> <w n="13.3">fait</w> <w n="13.4">le</w> <w n="13.5">dîner</w>.</l>
					</lg>
					<lg n="3">
						<l n="14" num="3.1"><w n="14.1">Je</w> <w n="14.2">t</w>’<w n="14.3">aime</w> <w n="14.4">bien</w>, <w n="14.5">tu</w> <w n="14.6">m</w>’<w n="14.7">aimes</w> <w n="14.8">bien</w>,</l>
						<l n="15" num="3.2"><w n="15.1">Nous</w> <w n="15.2">ne</w> <w n="15.3">nous</w> <w n="15.4">attendons</w> <w n="15.5">à</w> <w n="15.6">rien</w>…</l>
						<l n="16" num="3.3"><w n="16.1">Bonne</w> <w n="16.2">journée</w>. <w n="16.3">Bonne</w> <w n="16.4">journée</w> !</l>
					</lg>
				</div></body></text></TEI>