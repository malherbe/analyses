<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">HORIZONS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1794 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Horizons</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>BIBLIOTHÈQUE-CHARPENTIER, EUGÈNE FASQUELLE, ÉDITEUR</publisher>
							<date when="1905">1905</date>
						</imprint>
					</monogr>
					<note>Édition numérisée sur demande (BnF)</note>
				</biblStruct>
			</sourceDesc>
			</fileDesc>
		<profileDesc>
			<creation>
				<date when="1905">1905</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LE LONG DES JARDINS ET DE L’EAU</head><div type="poem" key="DLR274">
					<head type="main">LE SOLEIL DANS LA BRUME…</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Le</w> <w n="1.2">soleil</w> <w n="1.3">dans</w> <w n="1.4">la</w> <w n="1.5">brume</w> <w n="1.6">est</w> <w n="1.7">rouge</w> <w n="1.8">et</w> <w n="1.9">sans</w> <w n="1.10">halo</w></l>
						<l n="2" num="1.2"><w n="2.1">Au</w>-<w n="2.2">dessus</w> <w n="2.3">de</w> <w n="2.4">la</w> <w n="2.5">berge</w> <w n="2.6">et</w> <w n="2.7">des</w> <w n="2.8">ponts</w> <w n="2.9">et</w> <w n="2.10">de</w> <w n="2.11">l</w>’<w n="2.12">eau</w></l>
						<l n="3" num="1.3"><w n="3.1">Qui</w> <w n="3.2">s</w>’<w n="3.3">abaisse</w> <w n="3.4">et</w> <w n="3.5">qui</w> <w n="3.6">se</w> <w n="3.7">soulève</w>.</l>
						<l n="4" num="1.4"><w n="4.1">Pour</w> <w n="4.2">toi</w>, <w n="4.3">ne</w> <w n="4.4">reprends</w> <w n="4.5">pas</w> <w n="4.6">ton</w> <w n="4.7">indicible</w> <w n="4.8">rêve</w>,</l>
						<l n="5" num="1.5"><w n="5.1">Pendant</w> <w n="5.2">que</w> <w n="5.3">le</w> <w n="5.4">bateau</w> <w n="5.5">glisse</w> <w n="5.6">et</w> <w n="5.7">que</w> <w n="5.8">te</w> <w n="5.9">soulève</w></l>
						<l n="6" num="1.6"><w n="6.1">La</w> <w n="6.2">respiration</w> <w n="6.3">lourde</w> <w n="6.4">et</w> <w n="6.5">molle</w> <w n="6.6">de</w> <w n="6.7">l</w>’<w n="6.8">eau</w>.</l>
					</lg>
					<lg n="2">
						<l n="7" num="2.1"><w n="7.1">Ne</w> <w n="7.2">sois</w> <w n="7.3">pas</w> <w n="7.4">sombre</w> <w n="7.5">avec</w> <w n="7.6">ton</w> <w n="7.7">front</w> <w n="7.8">chaud</w> <w n="7.9">dans</w> <w n="7.10">ta</w> <w n="7.11">main</w>,</l>
						<l n="8" num="2.2"><w n="8.1">Songeant</w> <w n="8.2">à</w> <w n="8.3">la</w> <w n="8.4">douleur</w> <w n="8.5">possible</w> <w n="8.6">de</w> <w n="8.7">demain</w>,</l>
						<l n="9" num="2.3"><w n="9.1">A</w> <w n="9.2">celle</w> <w n="9.3">d</w>’<w n="9.4">hier</w>, <w n="9.5">à</w> <w n="9.6">la</w> <w n="9.7">vie</w>…</l>
						<l n="10" num="2.4"><w n="10.1">Mais</w> <w n="10.2">vois</w> <w n="10.3">se</w> <w n="10.4">refermer</w> <w n="10.5">à</w> <w n="10.6">même</w> <w n="10.7">l</w>’<w n="10.8">eau</w> <w n="10.9">suivie</w></l>
						<l n="11" num="2.5"><w n="11.1">Le</w> <w n="11.2">sillage</w> <w n="11.3">profond</w> <w n="11.4">et</w> <w n="11.5">semblable</w> <w n="11.6">à</w> <w n="11.7">la</w> <w n="11.8">vie</w></l>
						<l n="12" num="2.6"><w n="12.1">Qui</w> <w n="12.2">creuse</w> <w n="12.3">en</w> <w n="12.4">nous</w> <w n="12.5">tant</w> <w n="12.6">de</w> <w n="12.7">douleur</w> <w n="12.8">sans</w> <w n="12.9">lendemain</w>.</l>
					</lg>
				</div></body></text></TEI>