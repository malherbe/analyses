<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">HORIZONS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1794 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Horizons</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>BIBLIOTHÈQUE-CHARPENTIER, EUGÈNE FASQUELLE, ÉDITEUR</publisher>
							<date when="1905">1905</date>
						</imprint>
					</monogr>
					<note>Édition numérisée sur demande (BnF)</note>
				</biblStruct>
			</sourceDesc>
			</fileDesc>
		<profileDesc>
			<creation>
				<date when="1905">1905</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LE LONG DES JARDINS ET DE L’EAU</head><div type="poem" key="DLR272">
					<head type="main">TEMPÊTE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Par</w> <w n="1.2">la</w> <w n="1.3">nuit</w> <w n="1.4">dramatique</w> <w n="1.5">et</w> <w n="1.6">le</w> <w n="1.7">jardin</w> <w n="1.8">amer</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Montant</w> <w n="2.2">les</w> <w n="2.3">toits</w>, <w n="2.4">cognant</w> <w n="2.5">aux</w> <w n="2.6">murs</w>, <w n="2.7">sifflant</w> <w n="2.8">aux</w> <w n="2.9">fentes</w>,</l>
						<l n="3" num="1.3"><w n="3.1">La</w> <w n="3.2">tempête</w> <w n="3.3">d</w>’<w n="3.4">octobre</w>, <w n="3.5">obscure</w> <w n="3.6">et</w> <w n="3.7">véhémente</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Déferle</w> <w n="4.2">avec</w> <w n="4.3">l</w>’<w n="4.4">éclat</w> <w n="4.5">d</w>’<w n="4.6">une</w> <w n="4.7">mauvaise</w> <w n="4.8">mer</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Pourquoi</w> <w n="5.2">faut</w>-<w n="5.3">il</w> <w n="5.4">du</w> <w n="5.5">fond</w> <w n="5.6">de</w> <w n="5.7">moi</w> <w n="5.8">qu</w>’<w n="5.9">une</w> <w n="5.10">gorgone</w></l>
						<l n="6" num="2.2"><w n="6.1">Ruée</w> <w n="6.2">aux</w> <w n="6.3">vitres</w> <w n="6.4">cherche</w> <w n="6.5">à</w> <w n="6.6">sortir</w>, <w n="6.7">à</w> <w n="6.8">sortir</w> !</l>
						<l n="7" num="2.3"><w n="7.1">Pour</w> <w n="7.2">ajouter</w> <w n="7.3">l</w>’<w n="7.4">horreur</w> <w n="7.5">de</w> <w n="7.6">sa</w> <w n="7.7">face</w> <w n="7.8">à</w> <w n="7.9">l</w>’<w n="7.10">automne</w></l>
						<l n="8" num="2.4"><w n="8.1">Et</w> <w n="8.2">hurler</w>, <w n="8.3">et</w> <w n="8.4">rouler</w> <w n="8.5">au</w> <w n="8.6">vent</w>, <w n="8.7">et</w> <w n="8.8">s</w>’<w n="8.9">en</w> <w n="8.10">vêtir</w> ?…</l>
					</lg>
				</div></body></text></TEI>