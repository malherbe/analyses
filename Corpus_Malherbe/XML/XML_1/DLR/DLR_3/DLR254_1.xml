<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">HORIZONS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1794 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Horizons</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>BIBLIOTHÈQUE-CHARPENTIER, EUGÈNE FASQUELLE, ÉDITEUR</publisher>
							<date when="1905">1905</date>
						</imprint>
					</monogr>
					<note>Édition numérisée sur demande (BnF)</note>
				</biblStruct>
			</sourceDesc>
			</fileDesc>
		<profileDesc>
			<creation>
				<date when="1905">1905</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LE LONG DES JARDINS ET DE L’EAU</head><div type="poem" key="DLR254">
					<head type="main">LES CHALANDS</head>
					<opener>
						<salute>A Ch. Th. Féret.</salute>
					</opener>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Aux</w> <w n="1.2">tournants</w> <w n="1.3">troubles</w> <w n="1.4">de</w> <w n="1.5">la</w> <w n="1.6">Seine</w>, <w n="1.7">mes</w> <w n="1.8">chalands</w></l>
						<l n="2" num="1.2"><w n="2.1">Avec</w> <w n="2.2">leurs</w> <w n="2.3">mariniers</w> <w n="2.4">blonds</w> <w n="2.5">et</w> <w n="2.6">roux</w> <w n="2.7">à</w> <w n="2.8">l</w>’<w n="2.9">arrière</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Défilent</w> <w n="3.2">sous</w> <w n="3.3">mes</w> <w n="3.4">yeux</w>, <w n="3.5">à</w> <w n="3.6">la</w> <w n="3.7">remorque</w>, <w n="3.8">lents</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Un</w> <w n="4.2">pot</w> <w n="4.3">de</w> <w n="4.4">fleurs</w> <w n="4.5">à</w> <w n="4.6">leurs</w> <w n="4.7">fenêtres</w> <w n="4.8">batelières</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">J</w>’<w n="5.2">aime</w> <w n="5.3">les</w> <w n="5.4">regarder</w>, <w n="5.5">bien</w> <w n="5.6">chargés</w>, <w n="5.7">bien</w> <w n="5.8">fournis</w>.</l>
						<l n="6" num="2.2"><w n="6.1">Ils</w> <w n="6.2">sont</w> <w n="6.3">assis</w> <w n="6.4">sur</w> <w n="6.5">leur</w> <w n="6.6">reflet</w> <w n="6.7">quand</w> <w n="6.8">ils</w> <w n="6.9">s</w>’<w n="6.10">arrêtent</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Et</w> <w n="7.2">l</w>’<w n="7.3">eau</w> <w n="7.4">douce</w> <w n="7.5">vient</w> <w n="7.6">caresser</w> <w n="7.7">comme</w> <w n="7.8">une</w> <w n="7.9">bête</w></l>
						<l n="8" num="2.4"><w n="8.1">Et</w> <w n="8.2">faire</w> <w n="8.3">respirer</w> <w n="8.4">leurs</w> <w n="8.5">beaux</w> <w n="8.6">ventres</w> <w n="8.7">vernis</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">La</w> <w n="9.2">Seine</w> <w n="9.3">de</w> <w n="9.4">Paris</w> <w n="9.5">sans</w> <w n="9.6">verdure</w> <w n="9.7">et</w> <w n="9.8">sans</w> <w n="9.9">grève</w>,</l>
						<l n="10" num="3.2"><w n="10.1">Je</w> <w n="10.2">voudrais</w> <w n="10.3">la</w> <w n="10.4">quitter</w> <w n="10.5">pour</w> <w n="10.6">m</w>’<w n="10.7">en</w> <w n="10.8">aller</w> <w n="10.9">comme</w> <w n="10.10">eux</w>,</l>
						<l n="11" num="3.3">— <w n="11.1">Passant</w> <w n="11.2">au</w> <w n="11.3">fil</w> <w n="11.4">de</w> <w n="11.5">l</w>’<w n="11.6">eau</w> <w n="11.7">par</w> <w n="11.8">Rouen</w> <w n="11.9">et</w> <w n="11.10">la</w> <w n="11.11">Hève</w>, —</l>
						<l n="12" num="3.4"><w n="12.1">Regagner</w> <w n="12.2">l</w>’<w n="12.3">estuaire</w> <w n="12.4">avec</w> <w n="12.5">son</w> <w n="12.6">cap</w> <w n="12.7">brumeux</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Car</w> <w n="13.2">ils</w> <w n="13.3">vont</w> <w n="13.4">jusqu</w>’<w n="13.5">au</w> <w n="13.6">bout</w> <w n="13.7">de</w> <w n="13.8">ma</w> <w n="13.9">Seine</w> <w n="13.10">normande</w>,</l>
						<l n="14" num="4.2"><w n="14.1">Et</w> <w n="14.2">moi</w>, <w n="14.3">certains</w> <w n="14.4">soirs</w> <w n="14.5">lourds</w> <w n="14.6">ou</w> <w n="14.7">certains</w> <w n="14.8">matins</w> <w n="14.9">clair</w></l>
						<l n="15" num="4.3"><w n="15.1">Je</w> <w n="15.2">sens</w>, <w n="15.3">rien</w> <w n="15.4">qu</w>’<w n="15.5">à</w> <w n="15.6">les</w> <w n="15.7">voir</w>, <w n="15.8">que</w> <w n="15.9">mon</w> <w n="15.10">âme</w> <w n="15.11">demande</w></l>
						<l n="16" num="4.4"><w n="16.1">Quelque</w> <w n="16.2">chose</w>… <w n="16.3">Et</w> <w n="16.4">je</w> <w n="16.5">suis</w> <w n="16.6">en</w> <w n="16.7">peine</w> <w n="16.8">de</w> <w n="16.9">la</w> <w n="16.10">mer</w>.</l>
					</lg>
				</div></body></text></TEI>