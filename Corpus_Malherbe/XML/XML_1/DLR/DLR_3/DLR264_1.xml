<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">HORIZONS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1794 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Horizons</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>BIBLIOTHÈQUE-CHARPENTIER, EUGÈNE FASQUELLE, ÉDITEUR</publisher>
							<date when="1905">1905</date>
						</imprint>
					</monogr>
					<note>Édition numérisée sur demande (BnF)</note>
				</biblStruct>
			</sourceDesc>
			</fileDesc>
		<profileDesc>
			<creation>
				<date when="1905">1905</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LE LONG DES JARDINS ET DE L’EAU</head><div type="poem" key="DLR264">
					<head type="main">JUILLET</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">L</w>’<w n="1.2">air</w> <w n="1.3">brûlant</w> <w n="1.4">fait</w> <w n="1.5">vibrer</w> <w n="1.6">les</w> <w n="1.7">horizons</w> <w n="1.8">ruraux</w> ;</l>
						<l n="2" num="1.2"><w n="2.1">Un</w> <w n="2.2">excessif</w> <w n="2.3">soleil</w> <w n="2.4">rend</w> <w n="2.5">l</w>’<w n="2.6">ombre</w> <w n="2.7">plus</w> <w n="2.8">profonde</w>.</l>
						<l n="3" num="1.3"><w n="3.1">Dans</w> <w n="3.2">notre</w> <w n="3.3">jardin</w> <w n="3.4">calme</w> <w n="3.5">où</w> <w n="3.6">la</w> <w n="3.7">verdure</w> <w n="3.8">abonde</w>,</l>
						<l n="4" num="1.4"><w n="4.1">L</w>’<w n="4.2">après</w>-<w n="4.3">midi</w> <w n="4.4">sommeille</w> <w n="4.5">au</w> <w n="4.6">cœur</w> <w n="4.7">des</w> <w n="4.8">bosquets</w> <w n="4.9">chauds</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Voici</w> <w n="5.2">le</w> <w n="5.3">lierre</w> <w n="5.4">sombre</w> <w n="5.5">où</w> <w n="5.6">reluit</w> <w n="5.7">une</w> <w n="5.8">abeille</w>,</l>
						<l n="6" num="2.2"><w n="6.1">La</w> <w n="6.2">vigne</w>, <w n="6.3">les</w> <w n="6.4">rosiers</w>, <w n="6.5">les</w> <w n="6.6">fruits</w> <w n="6.7">déjà</w> <w n="6.8">joufflus</w>.</l>
						<l n="7" num="2.3"><w n="7.1">L</w>’<w n="7.2">herbe</w> <w n="7.3">couchée</w> <w n="7.4">encor</w> <w n="7.5">par</w> <w n="7.6">le</w> <w n="7.7">vent</w> <w n="7.8">de</w> <w n="7.9">la</w> <w n="7.10">veille</w>,</l>
						<l n="8" num="2.4"><w n="8.1">Et</w> <w n="8.2">toi</w> !… <w n="8.3">tout</w> <w n="8.4">ce</w> <w n="8.5">que</w> <w n="8.6">j</w>’<w n="8.7">aime</w> <w n="8.8">au</w> <w n="8.9">monde</w>, <w n="8.10">et</w> <w n="8.11">rien</w> <w n="8.12">de</w> <w n="8.13">plus</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Pas</w> <w n="9.2">un</w> <w n="9.3">souffle</w> <w n="9.4">n</w>’<w n="9.5">atteint</w> <w n="9.6">les</w> <w n="9.7">roses</w> <w n="9.8">éclatées</w> ;</l>
						<l n="10" num="3.2"><w n="10.1">Les</w> <w n="10.2">arbres</w> <w n="10.3">inégaux</w> <w n="10.4">sculptent</w> <w n="10.5">le</w> <w n="10.6">grand</w> <w n="10.7">ciel</w> <w n="10.8">clair</w>…</l>
						<l n="11" num="3.3"><w n="11.1">Ah</w> ! <w n="11.2">nous</w> <w n="11.3">demeurerons</w> <w n="11.4">souvent</w> <w n="11.5">jusqu</w>’<w n="11.6">aux</w> <w n="11.7">nuitées</w></l>
						<l n="12" num="3.4"><w n="12.1">Sous</w> <w n="12.2">la</w> <w n="12.3">tonnelle</w> <w n="12.4">ronde</w> <w n="12.5">où</w> <w n="12.6">filtre</w> <w n="12.7">un</w> <w n="12.8">jour</w> <w n="12.9">si</w> <w n="12.10">vert</w> !</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Ne</w> <w n="13.2">disons</w> <w n="13.3">rien</w>. <w n="13.4">Là</w>-<w n="13.5">bas</w> <w n="13.6">parle</w> <w n="13.7">une</w> <w n="13.8">voie</w> <w n="13.9">décrue</w> ;</l>
						<l n="14" num="4.2"><w n="14.1">Tout</w> <w n="14.2">à</w> <w n="14.3">l</w>’<w n="14.4">heure</w>, <w n="14.5">un</w> <w n="14.6">lointain</w> <w n="14.7">chariot</w> <w n="14.8">cahotait</w> ;</l>
						<l n="15" num="4.3"><w n="15.1">On</w> <w n="15.2">ne</w> <w n="15.3">sait</w> <w n="15.4">presque</w> <w n="15.5">plus</w> <w n="15.6">qu</w>’<w n="15.7">on</w> <w n="15.8">existe</w>. <w n="15.9">On</w> <w n="15.10">se</w> <w n="15.11">tait</w></l>
						<l n="16" num="4.4"><w n="16.1">Parmi</w> <w n="16.2">cette</w> <w n="16.3">rumeur</w> <w n="16.4">quelconque</w> <w n="16.5">de</w> <w n="16.6">la</w> <w n="16.7">rue</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Ne</w> <w n="17.2">disons</w> <w n="17.3">rien</w>. <w n="17.4">Il</w> <w n="17.5">fait</w> <w n="17.6">indifférent</w> <w n="17.7">et</w> <w n="17.8">bon</w> ;</l>
						<l n="18" num="5.2"><w n="18.1">Je</w> <w n="18.2">viens</w> <w n="18.3">de</w> <w n="18.4">voir</w> <w n="18.5">tomber</w> <w n="18.6">une</w> <w n="18.7">rose</w> <w n="18.8">fanée</w>…</l>
						<l n="19" num="5.3"><w n="19.1">Écoutons</w>, <w n="19.2">assoupis</w> <w n="19.3">de</w> <w n="19.4">satisfaction</w>,</l>
						<l n="20" num="5.4"><w n="20.1">Battre</w> <w n="20.2">tout</w> <w n="20.3">doucement</w> <w n="20.4">le</w> <w n="20.5">cœur</w> <w n="20.6">de</w> <w n="20.7">la</w> <w n="20.8">journée</w></l>
					</lg>
				</div></body></text></TEI>