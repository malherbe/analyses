<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">HORIZONS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1794 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Horizons</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>BIBLIOTHÈQUE-CHARPENTIER, EUGÈNE FASQUELLE, ÉDITEUR</publisher>
							<date when="1905">1905</date>
						</imprint>
					</monogr>
					<note>Édition numérisée sur demande (BnF)</note>
				</biblStruct>
			</sourceDesc>
			</fileDesc>
		<profileDesc>
			<creation>
				<date when="1905">1905</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">TENDRESSES</head><div type="poem" key="DLR239">
					<head type="main">CHEZ NOUS</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Les</w> <w n="1.2">volets</w> <w n="1.3">sont</w> <w n="1.4">déjà</w> <w n="1.5">fermés</w>, <w n="1.6">la</w> <w n="1.7">nuit</w> <w n="1.8">est</w> <w n="1.9">faite</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Et</w> <w n="2.2">tendrement</w>, <w n="2.3">autour</w> <w n="2.4">de</w> <w n="2.5">la</w> <w n="2.6">lampe</w>, <w n="2.7">s</w>’<w n="2.8">apprête</w></l>
						<l n="3" num="1.3"><w n="3.1">L</w>’<w n="3.2">heure</w> <w n="3.3">de</w> <w n="3.4">tous</w> <w n="3.5">les</w> <w n="3.6">soirs</w>, <w n="3.7">ineffable</w> <w n="3.8">et</w> <w n="3.9">secrète</w>,</l>
					</lg>
					<lg n="2">
						<l n="4" num="2.1"><w n="4.1">Si</w> <w n="4.2">douce</w> ! <w n="4.3">où</w> <w n="4.4">l</w>’<w n="4.5">on</w> <w n="4.6">se</w> <w n="4.7">serre</w> <w n="4.8">un</w> <w n="4.9">peu</w> <w n="4.10">l</w>’<w n="4.11">un</w> <w n="4.12">contre</w> <w n="4.13">l</w>’<w n="4.14">autre</w></l>
						<l n="5" num="2.2"><w n="5.1">Pour</w> <w n="5.2">lire</w> <w n="5.3">ou</w> <w n="5.4">pour</w> <w n="5.5">causer</w> <w n="5.6">un</w> <w n="5.7">moment</w> <w n="5.8">côte</w> <w n="5.9">à</w> <w n="5.10">côte</w>,</l>
						<l n="6" num="2.3"><w n="6.1">Mais</w> <w n="6.2">presque</w> <w n="6.3">bas</w>, <w n="6.4">craignant</w> <w n="6.5">le</w> <w n="6.6">bruit</w> <w n="6.7">de</w> <w n="6.8">la</w> <w n="6.9">voix</w> <w n="6.10">haute</w>.</l>
					</lg>
					<lg n="3">
						<l n="7" num="3.1"><w n="7.1">La</w> <w n="7.2">bonne</w> <w n="7.3">épaule</w> <w n="7.4">s</w>’<w n="7.5">offre</w> <w n="7.6">au</w> <w n="7.7">front</w> <w n="7.8">câlin</w> <w n="7.9">qui</w> <w n="7.10">plie</w> ;</l>
						<l n="8" num="3.2"><w n="8.1">On</w> <w n="8.2">sent</w> <w n="8.3">qu</w>’<w n="8.4">on</w> <w n="8.5">va</w> <w n="8.6">pleurer</w> <w n="8.7">de</w> <w n="8.8">tout</w> <w n="8.9">ce</w> <w n="8.10">qui</w> <w n="8.11">vous</w> <w n="8.12">lie</w>…</l>
						<l n="9" num="3.3"><w n="9.1">Ah</w> ! <w n="9.2">comme</w> <w n="9.3">on</w> <w n="9.4">s</w>’<w n="9.5">aime</w> <w n="9.6">bien</w> ! <w n="9.7">Quel</w> <w n="9.8">charme</w> <w n="9.9">que</w> <w n="9.10">la</w> <w n="9.11">vie</w> !</l>
					</lg>
					<lg n="4">
						<l n="10" num="4.1"><w n="10.1">Il</w> <w n="10.2">fait</w> <w n="10.3">calme</w>, <w n="10.4">i</w> <w n="10.5">l</w> <w n="10.6">fait</w> <w n="10.7">chaud</w>. <w n="10.8">L</w>’<w n="10.9">âme</w> <w n="10.10">heureuse</w> <w n="10.11">se</w> <w n="10.12">laisse</w></l>
						<l n="11" num="4.2"><w n="11.1">Aller</w>. <w n="11.2">La</w> <w n="11.3">lampe</w> <w n="11.4">est</w> <w n="11.5">douce</w> <w n="11.6">ainsi</w> <w n="11.7">qu</w>’<w n="11.8">un</w> <w n="11.9">jour</w> <w n="11.10">qui</w> <w n="11.11">baisse</w>.</l>
						<l n="12" num="4.3">— <w n="12.1">Cette</w> <w n="12.2">heure</w> <w n="12.3">est</w> <w n="12.4">le</w> <w n="12.5">bonheur</w> <w n="12.6">et</w> <w n="12.7">toute</w> <w n="12.8">la</w> <w n="12.9">sagesse</w>,</l>
					</lg>
					<lg n="5">
						<l n="13" num="5.1"><w n="13.1">Car</w>, <w n="13.2">s</w>’<w n="13.3">il</w> <w n="13.4">en</w> <w n="13.5">fut</w>, <w n="13.6">s</w>’<w n="13.7">il</w> <w n="13.8">doit</w> <w n="13.9">en</w> <w n="13.10">être</w> <w n="13.11">de</w> <w n="13.12">plus</w> <w n="13.13">folles</w>,</l>
						<l n="14" num="5.2"><w n="14.1">Rien</w> <w n="14.2">ne</w> <w n="14.3">vaudra</w> <w n="14.4">la</w> <w n="14.5">paix</w> <w n="14.6">de</w> <w n="14.7">ces</w> <w n="14.8">pénombres</w> <w n="14.9">molle</w>,</l>
						<l n="15" num="5.3"><w n="15.1">Et</w> <w n="15.2">ce</w> <w n="15.3">simple</w> <w n="15.4">regard</w>, <w n="15.5">et</w> <w n="15.6">ces</w> <w n="15.7">quelques</w> <w n="15.8">paroles</w>.</l>
					</lg>
				</div></body></text></TEI>