<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Nos Secrètes Amours</title>
				<title type="sub">édition "Les Iles", 1951</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>754 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_12</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Nos Secrètes Amours</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher></publisher>
						<idno type="URL">http://romanslesbiens.canalblog.com/archives/2007/11/10/6841446.html</idno>
					</publicationStmt>
					<sourceDesc>
						<p>édition "Les Iles", Paris, Imprimerie Nicolas — Niort, 1951, n° 14 sur 700 exemplaires. 48 pages.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Nos Secrètes Amours</title>
						<author>Lucie Delarue-Mardrus</author>
						<editor>texte établi et annoté par Mirande Lucien</editor>
						<edition>Deuxième tirage revu (première édition : 2008)</edition>
						<imprint>
							<pubPlace>Cassaniouze</pubPlace>
							<publisher>ErosOnyx</publisher>
							<date when="2018">2018</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1902" to="1905">1902-1905</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La présente édition électronique est a priori conforme avec l’édition de Natalie Barney de 1951.</p>
				<p>Une vérification avec un exemplaire imprimé ou numérisé de l’édition de 1951 ne serait pas superflue.</p>
				<p>Des vers ont été retirés pour avoir une édition conforme avec celle de 1951 selon les notes de l’éditeur de l’édition de 2008/2018 (poème : Contradiction).</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Des retraits introduits automatiquement ont été supprimés conformément au document de l’édition source et selon l’édition imprimée de 2018.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-09-02" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-12-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
				<change when="2020-12-03" who="RR">Vérification du texte avec l’édition de 2018.</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLR1002">
				<head type="main">MIROIR</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Je</w> <w n="1.2">me</w> <w n="1.3">regarde</w> <w n="1.4">en</w> <w n="1.5">toi</w> <w n="1.6">comme</w> <w n="1.7">jadis</w> <w n="1.8">au</w> <w n="1.9">fond</w></l>
					<l n="2" num="1.2"><w n="2.1">Des</w> <w n="2.2">eaux</w> <w n="2.3">douces</w> <w n="2.4">des</w> <w n="2.5">prés</w>, <w n="2.6">ombreuses</w> <w n="2.7">et</w> <w n="2.8">dormantes</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Où</w> <w n="3.2">naissait</w> <w n="3.3">sans</w> <w n="3.4">trembler</w> <w n="3.5">mon</w> <w n="3.6">mirage</w> <w n="3.7">profond</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Comme</w> <w n="4.2">une</w> <w n="4.3">nymphe</w> <w n="4.4">qui</w> <w n="4.5">se</w> <w n="4.6">baigne</w> <w n="4.7">entre</w> <w n="4.8">les</w> <w n="4.9">plantes</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Car</w> <w n="5.2">tu</w> <w n="5.3">es</w> <w n="5.4">douce</w> <w n="5.5">ainsi</w> <w n="5.6">qu</w>’<w n="5.7">un</w> <w n="5.8">reflet</w> <w n="5.9">dans</w> <w n="5.10">de</w> <w n="5.11">l</w>’<w n="5.12">eau</w>,</l>
					<l n="6" num="2.2"><w n="6.1">Et</w> <w n="6.2">tes</w> <w n="6.3">yeux</w> <w n="6.4">bleus</w> <w n="6.5">sont</w> <w n="6.6">mes</w> <w n="6.7">yeux</w> <w n="6.8">noirs</w> <w n="6.9">devenus</w> <w n="6.10">vagues</w></l>
					<l n="7" num="2.3"><w n="7.1">Et</w> <w n="7.2">tes</w> <w n="7.3">lointains</w> <w n="7.4">cheveux</w> <w n="7.5">de</w> <w n="7.6">rêve</w> <w n="7.7">et</w> <w n="7.8">de</w> <w n="7.9">halo</w></l>
					<l n="8" num="2.4"><w n="8.1">Sont</w> <w n="8.2">mes</w> <w n="8.3">roux</w> <w n="8.4">cheveux</w> <w n="8.5">bruns</w> <w n="8.6">réfléchis</w>, <w n="8.7">et</w> <w n="8.8">tes</w> <w n="8.9">bagues</w></l>
					<l n="9" num="2.5"><w n="9.1">Sont</w> <w n="9.2">le</w> <w n="9.3">renvoi</w> <w n="9.4">de</w> <w n="9.5">mes</w> <w n="9.6">chatons</w> <w n="9.7">rouges</w> <w n="9.8">et</w> <w n="9.9">bleus</w>…</l>
				</lg>
				<lg n="3">
					<l n="10" num="3.1"><w n="10.1">Je</w> <w n="10.2">te</w> <w n="10.3">prendrai</w> <w n="10.4">contre</w> <w n="10.5">mon</w> <w n="10.6">âme</w>, <w n="10.7">si</w> <w n="10.8">tu</w> <w n="10.9">veux</w>,</l>
					<l n="11" num="3.2"><w n="11.1">Puisque</w> <w n="11.2">notre</w> <w n="11.3">beauté</w> <w n="11.4">diverse</w> <w n="11.5">coïncide</w> ;</l>
					<l n="12" num="3.3"><w n="12.1">Puisqu</w>’<w n="12.2">en</w> <w n="12.3">toi</w> <w n="12.4">j</w>’<w n="12.5">ai</w> <w n="12.6">trouvé</w>, <w n="12.7">corporelle</w> <w n="12.8">et</w> <w n="12.9">lucide</w>,</l>
					<l n="13" num="3.4"><w n="13.1">La</w> <w n="13.2">nymphe</w> <w n="13.3">qui</w> <w n="13.4">troublait</w> <w n="13.5">les</w> <w n="13.6">eaux</w> <w n="13.7">de</w> <w n="13.8">mon</w> <w n="13.9">passé</w>,</l>
					<l n="14" num="3.5"><w n="14.1">Et</w> <w n="14.2">puisque</w>, <w n="14.3">sous</w> <w n="14.4">l</w>’<w n="14.5">argent</w> <w n="14.6">défait</w> <w n="14.7">des</w> <w n="14.8">boucles</w> <w n="14.9">blondes</w>,</l>
					<l n="15" num="3.6"><w n="15.1">Ton</w> <w n="15.2">corps</w> <w n="15.3">entre</w> <w n="15.4">mes</w> <w n="15.5">bras</w> <w n="15.6">péremptoires</w> <w n="15.7">pressé</w>,</l>
					<l n="16" num="3.7"><w n="16.1">Est</w> <w n="16.2">demeuré</w> <w n="16.3">subtil</w> <w n="16.4">et</w> <w n="16.5">fuyant</w> <w n="16.6">comme</w> <w n="16.7">l</w>’<w n="16.8">onde</w>…</l>
				</lg>
			</div></body></text></TEI>