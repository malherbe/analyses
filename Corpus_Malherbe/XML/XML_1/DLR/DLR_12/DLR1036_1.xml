<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Nos Secrètes Amours</title>
				<title type="sub">édition "Les Iles", 1951</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>754 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_12</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Nos Secrètes Amours</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher></publisher>
						<idno type="URL">http://romanslesbiens.canalblog.com/archives/2007/11/10/6841446.html</idno>
					</publicationStmt>
					<sourceDesc>
						<p>édition "Les Iles", Paris, Imprimerie Nicolas — Niort, 1951, n° 14 sur 700 exemplaires. 48 pages.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Nos Secrètes Amours</title>
						<author>Lucie Delarue-Mardrus</author>
						<editor>texte établi et annoté par Mirande Lucien</editor>
						<edition>Deuxième tirage revu (première édition : 2008)</edition>
						<imprint>
							<pubPlace>Cassaniouze</pubPlace>
							<publisher>ErosOnyx</publisher>
							<date when="2018">2018</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1902" to="1905">1902-1905</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La présente édition électronique est a priori conforme avec l’édition de Natalie Barney de 1951.</p>
				<p>Une vérification avec un exemplaire imprimé ou numérisé de l’édition de 1951 ne serait pas superflue.</p>
				<p>Des vers ont été retirés pour avoir une édition conforme avec celle de 1951 selon les notes de l’éditeur de l’édition de 2008/2018 (poème : Contradiction).</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Des retraits introduits automatiquement ont été supprimés conformément au document de l’édition source et selon l’édition imprimée de 2018.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-09-02" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-12-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
				<change when="2020-12-03" who="RR">Vérification du texte avec l’édition de 2018.</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLR1036">
				<head type="main">HURLEMENT</head>
				<lg n="1">
					<l n="1" num="1.1"><space unit="char" quantity="8"></space><w n="1.1">Je</w> <w n="1.2">connais</w> <w n="1.3">l</w>’<w n="1.4">angoisse</w> <w n="1.5">de</w> <w n="1.6">voir</w></l>
					<l n="2" num="1.2"><w n="2.1">Sa</w> <w n="2.2">taille</w> <w n="2.3">se</w> <w n="2.4">cambrer</w> <w n="2.5">dans</w> <w n="2.6">mes</w> <w n="2.7">mains</w> <w n="2.8">insensées</w>,</l>
					<l n="3" num="1.3"><space unit="char" quantity="8"></space><w n="3.1">Ses</w> <w n="3.2">yeux</w> <w n="3.3">bleus</w> <w n="3.4">qui</w> <w n="3.5">tournent</w> <w n="3.6">au</w> <w n="3.7">noir</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Son</w> <w n="4.2">sourire</w> <w n="4.3">vaincu</w>, <w n="4.4">ses</w> <w n="4.5">narines</w> <w n="4.6">pincées</w>,</l>
					<l n="5" num="1.5">— <w n="5.1">Et</w> <w n="5.2">de</w> <w n="5.3">vouloir</w> <w n="5.4">l</w>’<w n="5.5">étreindre</w> <w n="5.6">et</w> <w n="5.7">de</w> <w n="5.8">ne</w> <w n="5.9">pas</w> <w n="5.10">pouvoir</w>.</l>
				</lg>
				<lg n="2">
					<l n="6" num="2.1"><w n="6.1">Je</w> <w n="6.2">mourrai</w> <w n="6.3">de</w> <w n="6.4">son</w> <w n="6.5">corps</w> <w n="6.6">qui</w> <w n="6.7">se</w> <w n="6.8">donne</w> <w n="6.9">et</w> <w n="6.10">qui</w> <w n="6.11">ploie</w> ;</l>
					<l n="7" num="2.2"><space unit="char" quantity="8"></space><w n="7.1">Je</w> <w n="7.2">mourrai</w> <w n="7.3">de</w> <w n="7.4">sa</w> <w n="7.5">profondeur</w></l>
					<l n="8" num="2.3"><w n="8.1">Étroite</w> <w n="8.2">et</w> <w n="8.3">douce</w> <w n="8.4">ainsi</w> <w n="8.5">qu</w>’<w n="8.6">une</w> <w n="8.7">amphore</w> <w n="8.8">de</w> <w n="8.9">joie</w>,</l>
					<l n="9" num="2.4"><w n="9.1">Et</w> <w n="9.2">que</w> <w n="9.3">je</w> <w n="9.4">ne</w> <w n="9.5">puis</w> <w n="9.6">pas</w> <w n="9.7">blesser</w> <w n="9.8">comme</w> <w n="9.9">une</w> <w n="9.10">proie</w></l>
					<l n="10" num="2.5"><space unit="char" quantity="8"></space><w n="10.1">Sous</w> <w n="10.2">mon</w> <w n="10.3">baiser</w> <w n="10.4">dominateur</w>.</l>
				</lg>
				<lg n="3">
					<l n="11" num="3.1">— <w n="11.1">Sur</w> <w n="11.2">ton</w> <w n="11.3">funèbre</w> <w n="11.4">lit</w> <w n="11.5">d</w>’<w n="11.6">ivresse</w> <w n="11.7">et</w> <w n="11.8">de</w> <w n="11.9">douleur</w>,</l>
					<l n="12" num="3.2"><w n="12.1">A</w> <w n="12.2">jamais</w> <w n="12.3">dévouée</w> <w n="12.4">à</w> <w n="12.5">ton</w> <w n="12.6">spasme</w> <w n="12.7">terrible</w>,</l>
					<l n="13" num="3.3"><w n="13.1">Je</w> <w n="13.2">mourrai</w> <w n="13.3">de</w> <w n="13.4">ton</w> <w n="13.5">mal</w>, <w n="13.6">Impossible</w>, <w n="13.7">Impossible</w> !…</l>
					<l n="14" num="3.4"><w n="14.1">Avec</w> <w n="14.2">tous</w> <w n="14.3">les</w> <w n="14.4">sanglots</w> <w n="14.5">de</w> <w n="14.6">Sapho</w> <w n="14.7">dans</w> <w n="14.8">le</w> <w n="14.9">cœur</w> !</l>
				</lg>
			</div></body></text></TEI>