<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Nos Secrètes Amours</title>
				<title type="sub">édition "Les Iles", 1951</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>754 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_12</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Nos Secrètes Amours</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher></publisher>
						<idno type="URL">http://romanslesbiens.canalblog.com/archives/2007/11/10/6841446.html</idno>
					</publicationStmt>
					<sourceDesc>
						<p>édition "Les Iles", Paris, Imprimerie Nicolas — Niort, 1951, n° 14 sur 700 exemplaires. 48 pages.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Nos Secrètes Amours</title>
						<author>Lucie Delarue-Mardrus</author>
						<editor>texte établi et annoté par Mirande Lucien</editor>
						<edition>Deuxième tirage revu (première édition : 2008)</edition>
						<imprint>
							<pubPlace>Cassaniouze</pubPlace>
							<publisher>ErosOnyx</publisher>
							<date when="2018">2018</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1902" to="1905">1902-1905</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La présente édition électronique est a priori conforme avec l’édition de Natalie Barney de 1951.</p>
				<p>Une vérification avec un exemplaire imprimé ou numérisé de l’édition de 1951 ne serait pas superflue.</p>
				<p>Des vers ont été retirés pour avoir une édition conforme avec celle de 1951 selon les notes de l’éditeur de l’édition de 2008/2018 (poème : Contradiction).</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Des retraits introduits automatiquement ont été supprimés conformément au document de l’édition source et selon l’édition imprimée de 2018.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-09-02" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-12-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
				<change when="2020-12-03" who="RR">Vérification du texte avec l’édition de 2018.</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLR1015">
				<head type="main">LA BÊTE</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Nous</w> <w n="1.2">pencherons</w> <w n="1.3">sur</w> <w n="1.4">toi</w> <w n="1.5">notre</w> <w n="1.6">corps</w> <w n="1.7">et</w> <w n="1.8">notre</w> <w n="1.9">âme</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Bouche</w> <w n="2.2">intime</w>, <w n="2.3">nudité</w> <w n="2.4">de</w> <w n="2.5">la</w> <w n="2.6">nudité</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Tendre</w> <w n="3.2">et</w> <w n="3.3">mystérieux</w> <w n="3.4">repli</w> <w n="3.5">de</w> <w n="3.6">la</w> <w n="3.7">beauté</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Rose</w> <w n="4.2">coquille</w> <w n="4.3">où</w> <w n="4.4">vit</w> <w n="4.5">la</w> <w n="4.6">passion</w> <w n="4.7">des</w> <w n="4.8">femmes</w> !</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Lorsque</w>, <w n="5.2">pour</w> <w n="5.3">t</w>’<w n="5.4">adorer</w>, <w n="5.5">nous</w> <w n="5.6">plions</w> <w n="5.7">le</w> <w n="5.8">genou</w>,</l>
					<l n="6" num="2.2"><w n="6.1">L</w>’<w n="6.2">odeur</w> <w n="6.3">de</w> <w n="6.4">tout</w> <w n="6.5">l</w>’<w n="6.6">amour</w> <w n="6.7">exalte</w> <w n="6.8">nos</w> <w n="6.9">narines</w>,</l>
					<l n="7" num="2.3"><w n="7.1">Et</w>, <w n="7.2">sous</w> <w n="7.3">notre</w> <w n="7.4">baiser</w>, <w n="7.5">ton</w> <w n="7.6">plaisir</w> <w n="7.7">a</w> <w n="7.8">le</w> <w n="7.9">goût</w></l>
					<l n="8" num="2.4"><w n="8.1">Des</w> <w n="8.2">goémons</w> <w n="8.3">mouillés</w> <w n="8.4">et</w> <w n="8.5">des</w> <w n="8.6">bêtes</w> <w n="8.7">marines</w>,</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Toi</w> <w n="9.2">de</w> <w n="9.3">chair</w> <w n="9.4">délicate</w> <w n="9.5">et</w> <w n="9.6">crue</w>, <w n="9.7">étrange</w> <w n="9.8">cœur</w></l>
					<l n="10" num="3.2"><w n="10.1">Du</w> <w n="10.2">monde</w>, <w n="10.3">rétractile</w> <w n="10.4">et</w> <w n="10.5">secrète</w> <w n="10.6">gencive</w>,</l>
					<l n="11" num="3.3"><w n="11.1">Bête</w> <w n="11.2">terrible</w>, <w n="11.3">bête</w> <w n="11.4">au</w> <w n="11.5">guet</w>, <w n="11.6">bête</w> <w n="11.7">lascive</w>,</l>
					<l n="12" num="3.4"><w n="12.1">Bête</w> <w n="12.2">éternelle</w>, — <w n="12.3">O</w> <w n="12.4">joie</w> !… <w n="12.5">O</w> <w n="12.6">douleur</w> !… <w n="12.7">O</w> <w n="12.8">douceur</w> !…</l>
				</lg>
			</div></body></text></TEI>