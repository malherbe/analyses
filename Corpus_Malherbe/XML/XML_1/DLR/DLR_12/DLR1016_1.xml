<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Nos Secrètes Amours</title>
				<title type="sub">édition "Les Iles", 1951</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>754 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_12</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Nos Secrètes Amours</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher></publisher>
						<idno type="URL">http://romanslesbiens.canalblog.com/archives/2007/11/10/6841446.html</idno>
					</publicationStmt>
					<sourceDesc>
						<p>édition "Les Iles", Paris, Imprimerie Nicolas — Niort, 1951, n° 14 sur 700 exemplaires. 48 pages.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Nos Secrètes Amours</title>
						<author>Lucie Delarue-Mardrus</author>
						<editor>texte établi et annoté par Mirande Lucien</editor>
						<edition>Deuxième tirage revu (première édition : 2008)</edition>
						<imprint>
							<pubPlace>Cassaniouze</pubPlace>
							<publisher>ErosOnyx</publisher>
							<date when="2018">2018</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1902" to="1905">1902-1905</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La présente édition électronique est a priori conforme avec l’édition de Natalie Barney de 1951.</p>
				<p>Une vérification avec un exemplaire imprimé ou numérisé de l’édition de 1951 ne serait pas superflue.</p>
				<p>Des vers ont été retirés pour avoir une édition conforme avec celle de 1951 selon les notes de l’éditeur de l’édition de 2008/2018 (poème : Contradiction).</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Des retraits introduits automatiquement ont été supprimés conformément au document de l’édition source et selon l’édition imprimée de 2018.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-09-02" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-12-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
				<change when="2020-12-03" who="RR">Vérification du texte avec l’édition de 2018.</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLR1016">
				<head type="main">POSSESSION</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Un</w> <w n="1.2">frôlement</w> <w n="1.3">suffit</w> <w n="1.4">pour</w> <w n="1.5">abattre</w> <w n="1.6">ma</w> <w n="1.7">force</w>,</l>
					<l n="2" num="1.2"><space unit="char" quantity="8"></space><w n="2.1">Un</w> <w n="2.2">frôlement</w> <w n="2.3">de</w> <w n="2.4">mon</w> <w n="2.5">amante</w>.</l>
					<l n="3" num="1.3"><w n="3.1">Quand</w> <w n="3.2">sa</w> <w n="3.3">bouche</w> <w n="3.4">frémit</w> <w n="3.5">sur</w> <w n="3.6">ma</w> <w n="3.7">bouche</w> <w n="3.8">dormante</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Son</w> <w n="4.2">baiser</w> <w n="4.3">entre</w> <w n="4.4">en</w> <w n="4.5">moi</w> <w n="4.6">comme</w> <w n="4.7">une</w> <w n="4.8">lame</w> <w n="4.9">torse</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Mais</w>, <w n="5.2">par</w> <w n="5.3">certaines</w> <w n="5.4">nuits</w>, <w n="5.5">si</w> <w n="5.6">nous</w> <w n="5.7">couchons</w> <w n="5.8">ensemble</w>,</l>
					<l n="6" num="2.2"><space unit="char" quantity="8"></space><w n="6.1">Je</w> <w n="6.2">ne</w> <w n="6.3">suis</w> <w n="6.4">plus</w> <w n="6.5">rien</w> <w n="6.6">qu</w>’<w n="6.7">une</w> <w n="6.8">proie</w></l>
					<l n="7" num="2.3"><w n="7.1">Qui</w> <w n="7.2">se</w> <w n="7.3">débat</w> <w n="7.4">contre</w> <w n="7.5">elle</w> <w n="7.6">et</w> <w n="7.7">rit</w> <w n="7.8">et</w> <w n="7.9">pleure</w> <w n="7.10">et</w> <w n="7.11">tremble</w>,</l>
					<l n="8" num="2.4"><w n="8.1">Et</w> <w n="8.2">va</w> <w n="8.3">mourir</w> <w n="8.4">de</w> <w n="8.5">joie</w>, <w n="8.6">et</w> <w n="8.7">va</w> <w n="8.8">mourir</w> <w n="8.9">de</w> <w n="8.10">joie</w> !…</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Elle</w> <w n="9.2">est</w> <w n="9.3">belle</w>… <w n="9.4">Je</w> <w n="9.5">l</w>’<w n="9.6">aime</w>… <w n="9.7">Ah</w> ! <w n="9.8">quelle</w> <w n="9.9">chose</w> <w n="9.10">au</w> <w n="9.11">monde</w></l>
					<l n="10" num="3.2"><space unit="char" quantity="8"></space><w n="10.1">Pourrait</w> <w n="10.2">m</w>’<w n="10.3">arracher</w> <w n="10.4">d</w>’<w n="10.5">elle</w></l>
					<l n="11" num="3.3"><w n="11.1">Qui</w> <w n="11.2">tendit</w> <w n="11.3">à</w> <w n="11.4">jamais</w> <w n="11.5">cette</w> <w n="11.6">corde</w> <w n="11.7">profonde</w></l>
					<l n="12" num="3.4"><w n="12.1">Dans</w> <w n="12.2">mon</w> <w n="12.3">âme</w> <w n="12.4">d</w>’<w n="12.5">orgueil</w> <w n="12.6">si</w> <w n="12.7">sombre</w> <w n="12.8">et</w> <w n="12.9">si</w> <w n="12.10">charnelle</w> ?…</l>
				</lg>
			</div></body></text></TEI>