<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Nos Secrètes Amours</title>
				<title type="sub">édition "Les Iles", 1951</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>754 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_12</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Nos Secrètes Amours</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher></publisher>
						<idno type="URL">http://romanslesbiens.canalblog.com/archives/2007/11/10/6841446.html</idno>
					</publicationStmt>
					<sourceDesc>
						<p>édition "Les Iles", Paris, Imprimerie Nicolas — Niort, 1951, n° 14 sur 700 exemplaires. 48 pages.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Nos Secrètes Amours</title>
						<author>Lucie Delarue-Mardrus</author>
						<editor>texte établi et annoté par Mirande Lucien</editor>
						<edition>Deuxième tirage revu (première édition : 2008)</edition>
						<imprint>
							<pubPlace>Cassaniouze</pubPlace>
							<publisher>ErosOnyx</publisher>
							<date when="2018">2018</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1902" to="1905">1902-1905</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La présente édition électronique est a priori conforme avec l’édition de Natalie Barney de 1951.</p>
				<p>Une vérification avec un exemplaire imprimé ou numérisé de l’édition de 1951 ne serait pas superflue.</p>
				<p>Des vers ont été retirés pour avoir une édition conforme avec celle de 1951 selon les notes de l’éditeur de l’édition de 2008/2018 (poème : Contradiction).</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Des retraits introduits automatiquement ont été supprimés conformément au document de l’édition source et selon l’édition imprimée de 2018.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-09-02" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-12-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
				<change when="2020-12-03" who="RR">Vérification du texte avec l’édition de 2018.</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLR1001">
				<head type="main">POUR TOI</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Toujours</w> <w n="1.2">je</w> <w n="1.3">me</w> <w n="1.4">transforme</w> <w n="1.5">et</w> <w n="1.6">suis</w> <w n="1.7">toujours</w> <w n="1.8">la</w> <w n="1.9">même</w></l>
					<l n="2" num="1.2"><w n="2.1">Comme</w> <w n="2.2">la</w> <w n="2.3">mer</w> <w n="2.4">multiple</w> <w n="2.5">et</w> <w n="2.6">une</w> <w n="2.7">d</w>’<w n="2.8">où</w> <w n="2.9">je</w> <w n="2.10">sors</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Qui</w> <w n="3.2">recule</w> <w n="3.3">et</w> <w n="3.4">se</w> <w n="3.5">rue</w> <w n="3.6">à</w> <w n="3.7">jamais</w> <w n="3.8">et</w> <w n="3.9">quand</w> <w n="3.10">même</w></l>
					<l n="4" num="1.4"><w n="4.1">Vers</w> <w n="4.2">la</w> <w n="4.3">possession</w> <w n="4.4">des</w> <w n="4.5">villes</w> <w n="4.6">et</w> <w n="4.7">des</w> <w n="4.8">ports</w></l>
					<l n="5" num="1.5"><w n="5.1">Et</w> <w n="5.2">des</w> <w n="5.3">terres</w> <w n="5.4">avec</w> <w n="5.5">leurs</w> <w n="5.6">prés</w> <w n="5.7">et</w> <w n="5.8">leurs</w> <w n="5.9">bois</w> <w n="5.10">tors</w></l>
					<l n="6" num="1.6"><w n="6.1">Qu</w>’<w n="6.2">Elle</w> <w n="6.3">n</w>’<w n="6.4">atteindra</w> <w n="6.5">pas</w> <w n="6.6">de</w> <w n="6.7">son</w> <w n="6.8">spasme</w> <w n="6.9">suprême</w>.</l>
				</lg>
				<lg n="2">
					<l n="7" num="2.1"><w n="7.1">Car</w> <w n="7.2">longtemps</w> <w n="7.3">j</w>’<w n="7.4">ai</w> <w n="7.5">longé</w> <w n="7.6">les</w> <w n="7.7">vagues</w> <w n="7.8">d</w>’<w n="7.9">autrefois</w></l>
					<l n="8" num="2.2"><w n="8.1">Qui</w> <w n="8.2">gardent</w> <w n="8.3">le</w> <w n="8.4">contour</w> <w n="8.5">des</w> <w n="8.6">sirènes</w> <w n="8.7">en</w> <w n="8.8">elles</w>,</l>
					<l n="9" num="2.3"><w n="9.1">Et</w> <w n="9.2">jeté</w> <w n="9.3">dans</w> <w n="9.4">le</w> <w n="9.5">vent</w> <w n="9.6">la</w> <w n="9.7">force</w> <w n="9.8">de</w> <w n="9.9">ma</w> <w n="9.10">voix</w>,</l>
					<l n="10" num="2.4"><w n="10.1">Et</w> <w n="10.2">pressé</w> <w n="10.3">ma</w> <w n="10.4">poitrine</w> <w n="10.5">énergique</w> <w n="10.6">et</w> <w n="10.7">charnelle</w></l>
					<l n="11" num="2.5"><w n="11.1">Comme</w> <w n="11.2">pour</w> <w n="11.3">maintenir</w> <w n="11.4">sous</w> <w n="11.5">l</w>’<w n="11.6">effort</w> <w n="11.7">des</w> <w n="11.8">dix</w> <w n="11.9">doigts</w></l>
					<l n="12" num="2.6"><w n="12.1">Toutes</w> <w n="12.2">les</w> <w n="12.3">passions</w> <w n="12.4">de</w> <w n="12.5">mon</w> <w n="12.6">âme</w> <w n="12.7">éternelle</w>.</l>
				</lg>
				<lg n="3">
					<l n="13" num="3.1">— <w n="13.1">Ah</w> ! <w n="13.2">ne</w> <w n="13.3">regrette</w> <w n="13.4">pas</w> <w n="13.5">l</w>’<w n="13.6">horreur</w> <w n="13.7">et</w> <w n="13.8">la</w> <w n="13.9">beauté</w></l>
					<l n="14" num="3.2"><w n="14.1">De</w> <w n="14.2">la</w> <w n="14.3">mer</w>, <w n="14.4">demeurée</w> <w n="14.5">au</w> <w n="14.6">cœur</w> <w n="14.7">de</w> <w n="14.8">ma</w> <w n="14.9">chair</w> <w n="14.10">lasse</w> !</l>
					<l n="15" num="3.3"><w n="15.1">Si</w>, <w n="15.2">frénétiquement</w>, <w n="15.3">mon</w> <w n="15.4">désir</w> <w n="15.5">te</w> <w n="15.6">dépasse</w>,</l>
					<l n="16" num="3.4"><w n="16.1">C</w>’<w n="16.2">est</w> <w n="16.3">que</w>, <w n="16.4">brisant</w> <w n="16.5">le</w> <w n="16.6">sceau</w> <w n="16.7">de</w> <w n="16.8">notre</w> <w n="16.9">humanité</w>,</l>
					<l n="17" num="3.5"><w n="17.1">Mon</w> <w n="17.2">être</w> <w n="17.3">se</w> <w n="17.4">débat</w> <w n="17.5">sous</w> <w n="17.6">le</w> <w n="17.7">dieu</w> <w n="17.8">qui</w> <w n="17.9">l</w>’<w n="17.10">embrasse</w> !</l>
				</lg>
			</div></body></text></TEI>