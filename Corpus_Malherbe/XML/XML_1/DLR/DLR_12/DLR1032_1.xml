<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Nos Secrètes Amours</title>
				<title type="sub">édition "Les Iles", 1951</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>754 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_12</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Nos Secrètes Amours</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher></publisher>
						<idno type="URL">http://romanslesbiens.canalblog.com/archives/2007/11/10/6841446.html</idno>
					</publicationStmt>
					<sourceDesc>
						<p>édition "Les Iles", Paris, Imprimerie Nicolas — Niort, 1951, n° 14 sur 700 exemplaires. 48 pages.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Nos Secrètes Amours</title>
						<author>Lucie Delarue-Mardrus</author>
						<editor>texte établi et annoté par Mirande Lucien</editor>
						<edition>Deuxième tirage revu (première édition : 2008)</edition>
						<imprint>
							<pubPlace>Cassaniouze</pubPlace>
							<publisher>ErosOnyx</publisher>
							<date when="2018">2018</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1902" to="1905">1902-1905</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La présente édition électronique est a priori conforme avec l’édition de Natalie Barney de 1951.</p>
				<p>Une vérification avec un exemplaire imprimé ou numérisé de l’édition de 1951 ne serait pas superflue.</p>
				<p>Des vers ont été retirés pour avoir une édition conforme avec celle de 1951 selon les notes de l’éditeur de l’édition de 2008/2018 (poème : Contradiction).</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Des retraits introduits automatiquement ont été supprimés conformément au document de l’édition source et selon l’édition imprimée de 2018.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-09-02" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-12-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
				<change when="2020-12-03" who="RR">Vérification du texte avec l’édition de 2018.</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLR1032">
				<head type="main">BLESSURE</head>
				<lg n="1">
					<l n="1" num="1.1"><space unit="char" quantity="10"></space><w n="1.1">Mon</w> <w n="1.2">enfant</w>, <w n="1.3">ma</w> <w n="1.4">blanche</w> <w n="1.5">douceur</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Je</w> <w n="2.2">te</w> <w n="2.3">quitte</w> <w n="2.4">ce</w> <w n="2.5">soir</w> <w n="2.6">comme</w> <w n="2.7">à</w> <w n="2.8">l</w>’<w n="2.9">accoutumée</w>…</l>
					<l n="3" num="1.3">— <w n="3.1">Ah</w> ! <w n="3.2">ne</w> <w n="3.3">devine</w> <w n="3.4">pas</w> <w n="3.5">encore</w>, <w n="3.6">bien</w> <w n="3.7">aimée</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Quel</w> <w n="4.2">adieu</w> <w n="4.3">sanglotant</w> <w n="4.4">ce</w> <w n="4.5">soir</w> <w n="4.6">est</w> <w n="4.7">dans</w> <w n="4.8">mon</w> <w n="4.9">cœur</w>,</l>
					<l n="5" num="1.5"><space unit="char" quantity="10"></space><w n="5.1">Quelle</w> <w n="5.2">porte</w> <w n="5.3">s</w>’<w n="5.4">est</w> <w n="5.5">refermée</w> !</l>
				</lg>
				<lg n="2">
					<l n="6" num="2.1"><space unit="char" quantity="10"></space><w n="6.1">Trop</w> <w n="6.2">de</w> <w n="6.3">passés</w> <w n="6.4">sont</w> <w n="6.5">entre</w> <w n="6.6">nous</w></l>
					<l n="7" num="2.2"><w n="7.1">Et</w> <w n="7.2">ton</w> <w n="7.3">âme</w> <w n="7.4">jamais</w> <w n="7.5">n</w>’<w n="7.6">épousera</w> <w n="7.7">la</w> <w n="7.8">mienne</w>.</l>
					<l n="8" num="2.3"><w n="8.1">Puisqu</w>’<w n="8.2">il</w> <w n="8.3">ne</w> <w n="8.4">se</w> <w n="8.5">peut</w> <w n="8.6">pas</w> <w n="8.7">que</w> <w n="8.8">rien</w> <w n="8.9">ne</w> <w n="8.10">nous</w> <w n="8.11">souvienne</w>,</l>
					<l n="9" num="2.4"><w n="9.1">Désunissons</w> <w n="9.2">nos</w> <w n="9.3">bras</w> <w n="9.4">enlacés</w> <w n="9.5">à</w> <w n="9.6">nos</w> <w n="9.7">cous</w>,</l>
					<l n="10" num="2.5"><space unit="char" quantity="10"></space><w n="10.1">Retourne</w> <w n="10.2">à</w> <w n="10.3">ton</w> <w n="10.4">amour</w> <w n="10.5">ancienne</w>.</l>
				</lg>
				<lg n="3">
					<l n="11" num="3.1"><space unit="char" quantity="10"></space><w n="11.1">Le</w> <w n="11.2">souvenir</w> <w n="11.3">est</w> <w n="11.4">plus</w> <w n="11.5">que</w> <w n="11.6">moi</w>…</l>
					<l n="12" num="3.2">— <w n="12.1">Debout</w>, <w n="12.2">je</w> <w n="12.3">piétinais</w> <w n="12.4">ma</w> <w n="12.5">grande</w> <w n="12.6">lassitude</w></l>
					<l n="13" num="3.3"><w n="13.1">Pour</w> <w n="13.2">suivre</w> <w n="13.3">ta</w> <w n="13.4">beauté</w> <w n="13.5">passante</w> <w n="13.6">au</w> <w n="13.7">regard</w> <w n="13.8">froid</w>.</l>
					<l n="14" num="3.4"><w n="14.1">Mais</w> <w n="14.2">voici</w> : <w n="14.3">je</w> <w n="14.4">reprends</w>, <w n="14.5">ce</w> <w n="14.6">soir</w>, <w n="14.7">ma</w> <w n="14.8">solitude</w>,</l>
					<l n="15" num="3.5"><space unit="char" quantity="10"></space><w n="15.1">La</w> <w n="15.2">route</w> <w n="15.3">de</w> <w n="15.4">ma</w> <w n="15.5">solitude</w>.</l>
				</lg>
				<lg n="4">
					<l n="16" num="4.1"><space unit="char" quantity="10"></space><w n="16.1">Tu</w> <w n="16.2">n</w>’<w n="16.3">étais</w> <w n="16.4">pas</w> <w n="16.5">pour</w> <w n="16.6">moi</w>… <w n="16.7">Jamais</w></l>
					<l n="17" num="4.2"><w n="17.1">Je</w> <w n="17.2">ne</w> <w n="17.3">romprai</w> <w n="17.4">le</w> <w n="17.5">sceau</w> <w n="17.6">de</w> <w n="17.7">songe</w> <w n="17.8">et</w> <w n="17.9">de</w> <w n="17.10">silence</w></l>
					<l n="18" num="4.3"><w n="18.1">Qui</w> <w n="18.2">fermera</w> <w n="18.3">ma</w> <w n="18.4">bouche</w> <w n="18.5">à</w> <w n="18.6">l</w>’<w n="18.7">amour</w>, <w n="18.8">désormais</w>,</l>
					<l n="19" num="4.4"><w n="19.1">Car</w> <w n="19.2">je</w> <w n="19.3">ne</w> <w n="19.4">retrouverai</w> <w n="19.5">l</w>’<w n="19.6">horreur</w> <w n="19.7">et</w> <w n="19.8">l</w>’<w n="19.9">opulence</w></l>
					<l n="20" num="4.5"><space unit="char" quantity="10"></space><w n="20.1">De</w> <w n="20.2">l</w>’<w n="20.3">autrefois</w> <w n="20.4">sans</w> <w n="20.5">espérance</w>.</l>
				</lg>
				<lg n="5">
					<l n="21" num="5.1"><space unit="char" quantity="10"></space><w n="21.1">Souris</w> <w n="21.2">en</w> <w n="21.3">me</w> <w n="21.4">quittant</w>, <w n="21.5">ce</w> <w n="21.6">soir</w>,</l>
					<l n="22" num="5.2"><w n="22.1">Et</w> <w n="22.2">donne</w>-<w n="22.3">moi</w> <w n="22.4">tes</w> <w n="22.5">mains</w>, <w n="22.6">et</w> <w n="22.7">donne</w>-<w n="22.8">moi</w> <w n="22.9">ta</w> <w n="22.10">bouche</w>.</l>
					<l n="23" num="5.3">— <w n="23.1">Mais</w> <w n="23.2">que</w> <w n="23.3">ton</w> <w n="23.4">tendre</w> <w n="23.5">corps</w> <w n="23.6">doucement</w> <w n="23.7">se</w> <w n="23.8">recouche</w>,</l>
					<l n="24" num="5.4"><w n="24.1">Toi</w> <w n="24.2">qui</w> <w n="24.3">ne</w> <w n="24.4">peux</w> <w n="24.5">sentir</w>, <w n="24.6">squelette</w> <w n="24.7">dans</w> <w n="24.8">le</w> <w n="24.9">noir</w>,</l>
					<l n="25" num="5.5"><space unit="char" quantity="10"></space><w n="25.1">Quelle</w> <w n="25.2">mort</w> <w n="25.3">se</w> <w n="25.4">penche</w> <w n="25.5">et</w> <w n="25.6">te</w> <w n="25.7">touche</w>.</l>
				</lg>
			</div></body></text></TEI>