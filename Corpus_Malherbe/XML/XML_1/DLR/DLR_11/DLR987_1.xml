<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">TEMPS PRÉSENTS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>818 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_11</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title type="main">TEMPS PRÉSENTS</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Les Cahiers d’art et d’amitié, P. Mourousy</publisher>
							<date when="1939">1939</date>
						</imprint>
					</monogr>
					<note>Édition numérisée sur demande (BnF)</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1939">1939</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>.....</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLR987">
				<head type="main">BALLADE BERTHE</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Berthe</w>, <w n="1.2">ma</w> <w n="1.3">fidèle</w> <w n="1.4">servante</w></l>
					<l n="2" num="1.2"><w n="2.1">Qui</w> <w n="2.2">me</w> <w n="2.3">traitez</w> <w n="2.4">en</w> <w n="2.5">nourrisson</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Et</w>, <w n="3.2">qu</w>’<w n="3.3">il</w> <w n="3.4">fasse</w> <w n="3.5">beau</w>, <w n="3.6">pleuve</w> <w n="3.7">ou</w> <w n="3.8">vente</w></l>
					<l n="4" num="1.4"><w n="4.1">Me</w> <w n="4.2">cuisinez</w> <w n="4.3">chair</w> <w n="4.4">et</w> <w n="4.5">poisson</w>,</l>
					<l n="5" num="1.5"><w n="5.1">Qui</w> <w n="5.2">toujours</w> <w n="5.3">êtes</w> <w n="5.4">là</w>, <w n="5.5">fervente</w>,</l>
					<l n="6" num="1.6"><w n="6.1">Afin</w> <w n="6.2">que</w> <w n="6.3">je</w> <w n="6.4">m</w>’<w n="6.5">en</w> <w n="6.6">trouve</w> <w n="6.7">mieux</w>,</l>
					<l n="7" num="1.7"><w n="7.1">Je</w> <w n="7.2">souhaite</w>, <w n="7.3">longtemps</w> <w n="7.4">vivante</w>,</l>
					<l n="8" num="1.8"><w n="8.1">Qu</w>’<w n="8.2">un</w> <w n="8.3">jour</w> <w n="8.4">vous</w> <w n="8.5">me</w> <w n="8.6">fermiez</w> <w n="8.7">les</w> <w n="8.8">yeux</w>.</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1"><w n="9.1">Vous</w> <w n="9.2">n</w>’<w n="9.3">êtes</w> <w n="9.4">pas</w> <w n="9.5">femme</w> <w n="9.6">savante</w>.</l>
					<l n="10" num="2.2"><w n="10.1">Ce</w> <w n="10.2">n</w>’<w n="10.3">est</w> <w n="10.4">point</w> <w n="10.5">là</w> <w n="10.6">votre</w> <w n="10.7">façon</w>.</l>
					<l n="11" num="2.3"><w n="11.1">Telle</w> <w n="11.2">qu</w>’<w n="11.3">elle</w> <w n="11.4">est</w>, <w n="11.5">votre</w> <w n="11.6">chanson</w></l>
					<l n="12" num="2.4"><w n="12.1">M</w>’<w n="12.2">agrée</w>, <w n="12.3">et</w>, <w n="12.4">certes</w>, <w n="12.5">je</w> <w n="12.6">vous</w> <w n="12.7">vante</w></l>
					<l n="13" num="2.5"><w n="13.1">De</w> <w n="13.2">n</w>’<w n="13.3">être</w> <w n="13.4">rien</w> <w n="13.5">que</w> <w n="13.6">la</w> <w n="13.7">suivante</w></l>
					<l n="14" num="2.6"><w n="14.1">Autour</w> <w n="14.2">de</w> <w n="14.3">mon</w> <w n="14.4">front</w> <w n="14.5">studieux</w>.</l>
					<l n="15" num="2.7"><w n="15.1">Un</w> <w n="15.2">jour</w> <w n="15.3">viendra</w> <w n="15.4">l</w>’<w n="15.5">heure</w> <w n="15.6">émouvante</w></l>
					<l n="16" num="2.8"><w n="16.1">Où</w> <w n="16.2">vous</w> <w n="16.3">me</w> <w n="16.4">fermerez</w> <w n="16.5">les</w> <w n="16.6">yeux</w>.</l>
				</lg>
				<lg n="3">
					<l n="17" num="3.1"><w n="17.1">Parfois</w> <w n="17.2">la</w> <w n="17.3">vie</w> <w n="17.4">est</w> <w n="17.5">énervante</w>,</l>
					<l n="18" num="3.2"><w n="18.1">Et</w> <w n="18.2">je</w> <w n="18.3">gronde</w> <w n="18.4">et</w> <w n="18.5">fais</w> <w n="18.6">la</w> <w n="18.7">leçon</w>.</l>
					<l n="19" num="3.3"><w n="19.1">Certains</w> <w n="19.2">jours</w>, <w n="19.3">une</w> <w n="19.4">humeur</w> <w n="19.5">mouvante</w></l>
					<l n="20" num="3.4"><w n="20.1">Me</w> <w n="20.2">fait</w> <w n="20.3">sacrer</w> <w n="20.4">comme</w> <w n="20.5">un</w> <w n="20.6">garçon</w>.</l>
					<l n="21" num="3.5"><w n="21.1">L</w>’<w n="21.2">amitié</w> <w n="21.3">n</w>’<w n="21.4">est</w> <w n="21.5">pas</w> <w n="21.6">décevante</w></l>
					<l n="22" num="3.6"><w n="22.1">Entre</w> <w n="22.2">nous</w>, <w n="22.3">pourtant</w>. <w n="22.4">Toutes</w> <w n="22.5">deux</w></l>
					<l n="23" num="3.7"><w n="23.1">Faisons</w> <w n="23.2">la</w> <w n="23.3">paix</w> <w n="23.4">l</w>’<w n="23.5">heure</w> <w n="23.6">suivante</w>,</l>
					<l n="24" num="3.8"><w n="24.1">Avant</w> <w n="24.2">que</w> <w n="24.3">de</w> <w n="24.4">fermer</w> <w n="24.5">les</w> <w n="24.6">yeux</w>.</l>
				</lg>
				<lg n="4">
					<head type="main">ENVOI</head>
					<l n="25" num="4.1"><w n="25.1">O</w> <w n="25.2">bonne</w> <w n="25.3">présence</w> <w n="25.4">qui</w> <w n="25.5">hante</w></l>
					<l n="26" num="4.2"><w n="26.1">Ma</w> <w n="26.2">solitude</w>, <w n="26.3">soins</w> <w n="26.4">pieux</w>,</l>
					<l n="27" num="4.3"><w n="27.1">Quand</w> <w n="27.2">viendra</w> <w n="27.3">la</w> <w n="27.4">grande</w> <w n="27.5">épouvante</w>,</l>
					<l n="28" num="4.4"><w n="28.1">C</w>’<w n="28.2">est</w> <w n="28.3">vous</w> <w n="28.4">qui</w> <w n="28.5">fermerez</w> <w n="28.6">mes</w> <w n="28.7">yeux</w>.</l>
				</lg>
			</div></body></text></TEI>