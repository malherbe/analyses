<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">TEMPS PRÉSENTS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>818 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_11</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title type="main">TEMPS PRÉSENTS</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Les Cahiers d’art et d’amitié, P. Mourousy</publisher>
							<date when="1939">1939</date>
						</imprint>
					</monogr>
					<note>Édition numérisée sur demande (BnF)</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1939">1939</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>.....</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLR971">
				<head type="main">BALLADE DE QUELQUES DAMES LÉGIONNAIRES</head>
				<head type="sub_1">(avec mon respect à beaucoup d’autres)</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Sous</w> <w n="1.2">les</w> <w n="1.3">yeux</w> <w n="1.4">ronds</w> <w n="1.5">des</w> <w n="1.6">peuples</w> <w n="1.7">étonnés</w>,</l>
					<l n="2" num="1.2"><w n="2.1">La</w> <w n="2.2">France</w> <w n="2.3">met</w> <w n="2.4">au</w> <w n="2.5">féminin</w> <w n="2.6">ses</w> <w n="2.7">gloires</w>.</l>
					<l n="3" num="1.3"><w n="3.1">Or</w> <w n="3.2">Bonaparte</w> <w n="3.3">et</w> <w n="3.4">tous</w> <w n="3.5">ses</w> <w n="3.6">galonnés</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Voyant</w> <w n="4.2">passer</w> <w n="4.3">tant</w> <w n="4.4">de</w> <w n="4.5">dames</w> <w n="4.6">notoires</w></l>
					<l n="5" num="1.5"><w n="5.1">Se</w> <w n="5.2">doutent</w>-<w n="5.3">ils</w>, <w n="5.4">outre</w> <w n="5.5">quelques</w> <w n="5.6">pourboires</w>,</l>
					<l n="6" num="1.6"><w n="6.1">Qu</w>’<w n="6.2">elles</w> <w n="6.3">ont</w> <w n="6.4">dû</w>, <w n="6.5">pour</w> <w n="6.6">gagner</w> <w n="6.7">ces</w> <w n="6.8">faveurs</w>,</l>
					<l n="7" num="1.7"><w n="7.1">Longtemps</w> <w n="7.2">servir</w>, <w n="7.3">travaux</w> <w n="7.4">préparatoires</w>,</l>
					<l n="8" num="1.8"><w n="8.1">Sainte</w> <w n="8.2">Putain</w>, <w n="8.3">patronne</w> <w n="8.4">des</w> <w n="8.5">honneurs</w> ?</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1"><w n="9.1">O</w> <w n="9.2">grands</w> <w n="9.3">soldats</w> <w n="9.4">qui</w> <w n="9.5">furent</w> <w n="9.6">les</w> <w n="9.7">aînés</w>,</l>
					<l n="10" num="2.2"><w n="10.1">La</w> <w n="10.2">femme</w> <w n="10.3">ayant</w> <w n="10.4">impudeurs</w> <w n="10.5">péremptoires</w>,</l>
					<l n="11" num="2.3"><w n="11.1">Leur</w> <w n="11.2">boutonnière</w> <w n="11.3">est</w> <w n="11.4">où</w> <w n="11.5">vous</w> <w n="11.6">devinez</w>.</l>
					<l n="12" num="2.4"><w n="12.1">Vous</w> <w n="12.2">bombiez</w>, <w n="12.3">vous</w>, <w n="12.4">plastrons</w> <w n="12.5">ostentatoires</w>,</l>
					<l n="13" num="2.5"><w n="13.1">Mais</w> <w n="13.2">notre</w> <w n="13.3">siècle</w> <w n="13.4">a</w> <w n="13.5">changé</w> <w n="13.6">de</w> <w n="13.7">victoires</w>,</l>
					<l n="14" num="2.6"><w n="14.1">Et</w> <hi rend="ital"><w n="14.2">Haut</w> <w n="14.3">les</w> <w n="14.4">Culs</w></hi> <w n="14.5">remplace</w> <hi rend="ital"><w n="14.6">Haut</w> <w n="14.7">les</w> <w n="14.8">Cœurs</w></hi>.</l>
					<l n="15" num="2.7">— <w n="15.1">Ah</w> ! <w n="15.2">que</w> <w n="15.3">tu</w> <w n="15.4">sais</w> <w n="15.5">de</w> <w n="15.6">petites</w> <w n="15.7">histoires</w>,</l>
					<l n="16" num="2.8"><w n="16.1">Sainte</w> <w n="16.2">Putain</w>, <w n="16.3">patronne</w> <w n="16.4">des</w> <w n="16.5">honneurs</w> !</l>
				</lg>
				<lg n="3">
					<l n="17" num="3.1"><w n="17.1">Les</w> <w n="17.2">cheveux</w> <w n="17.3">teints</w> <w n="17.4">et</w> <w n="17.5">les</w> <w n="17.6">appas</w> <w n="17.7">fanés</w>,</l>
					<l n="18" num="3.2"><w n="18.1">Qu</w>’<w n="18.2">importe</w> !… <w n="18.3">Un</w> <w n="18.4">jour</w> <w n="18.5">ministres</w> <w n="18.6">provisoires</w></l>
					<l n="19" num="3.3"><w n="19.1">Dans</w> <w n="19.2">ces</w> <w n="19.3">restants</w> <w n="19.4">viennent</w> <w n="19.5">fourrer</w> <w n="19.6">leur</w> <w n="19.7">nez</w>.</l>
					<l n="20" num="3.4"><w n="20.1">Poches</w> <w n="20.2">à</w> <w n="20.3">fiel</w>, <w n="20.4">sous</w> <w n="20.5">les</w> <w n="20.6">lacrymatoires</w>,</l>
					<l n="21" num="3.5"><w n="21.1">Tout</w> <w n="21.2">aussitôt</w> <w n="21.3">de</w> <w n="21.4">verser</w> <w n="21.5">gouttes</w> <w n="21.6">noires</w>,</l>
					<l n="22" num="3.6"><w n="22.1">Larmes</w> <w n="22.2">de</w> <w n="22.3">joie</w> <w n="22.4">où</w> <w n="22.5">le</w> <w n="22.6">fard</w> <w n="22.7">joint</w> <w n="22.8">ses</w> <w n="22.9">pleurs</w>.</l>
					<l n="23" num="3.7">— <w n="23.1">Va</w>, <w n="23.2">ruban</w> <w n="23.3">rouge</w>, <w n="23.4">orne</w> <w n="23.5">leurs</w> <w n="23.6">promontoires</w></l>
					<l n="24" num="3.8"><w n="24.1">Sainte</w> <w n="24.2">Putain</w>, <w n="24.3">patronne</w> <w n="24.4">les</w> <w n="24.5">honneurs</w>.</l>
				</lg>
				<lg n="4">
					<head type="main">ENVOI</head>
					<l n="25" num="4.1"><w n="25.1">Héros</w> <w n="25.2">obscurs</w>, <w n="25.3">ô</w> <w n="25.4">mères</w>, <w n="25.5">quels</w> <w n="25.6">prétoires</w></l>
					<l n="26" num="4.2"><w n="26.1">Honoreront</w> <w n="26.2">vos</w> <w n="26.3">courages</w>, <w n="26.4">mes</w> <w n="26.5">sœurs</w> ?</l>
					<l n="27" num="4.3"><w n="27.1">Ah</w> ! <w n="27.2">prends</w> <w n="27.3">pitié</w> <w n="27.4">des</w> <w n="27.5">malheureuses</w> <w n="27.6">poires</w>,</l>
					<l n="28" num="4.4"><w n="28.1">Sainte</w> <w n="28.2">Putain</w>, <w n="28.3">patronne</w> <w n="28.4">des</w> <w n="28.5">honneurs</w> !</l>
				</lg>
			</div></body></text></TEI>