<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">TEMPS PRÉSENTS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>818 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_11</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title type="main">TEMPS PRÉSENTS</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Les Cahiers d’art et d’amitié, P. Mourousy</publisher>
							<date when="1939">1939</date>
						</imprint>
					</monogr>
					<note>Édition numérisée sur demande (BnF)</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1939">1939</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>.....</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLR992">
				<head type="main">BALLADE A LA MÊME</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Sous</w> <w n="1.2">les</w> <w n="1.3">ampoules</w> <w n="1.4">électriques</w></l>
					<l n="2" num="1.2"><w n="2.1">Où</w> <w n="2.2">règne</w> <w n="2.3">le</w> <w n="2.4">plus</w> <w n="2.5">pur</w> <w n="2.6">pompier</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Pour</w> <w n="3.2">attirer</w> <w n="3.3">les</w> <w n="3.4">Amériques</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Aubaine</w> <w n="4.2">du</w> <w n="4.3">futur</w> <w n="4.4">fripier</w>,</l>
					<l n="5" num="1.5"><w n="5.1">Poussent</w> <w n="5.2">les</w> <w n="5.3">roses</w> <w n="5.4">en</w> <w n="5.5">papier</w>.</l>
					<l n="6" num="1.6"><w n="6.1">Pauvre</w> <w n="6.2">sainte</w>, <w n="6.3">hors</w> <w n="6.4">de</w> <w n="6.5">ta</w> <w n="6.6">niche</w>,</l>
					<l n="7" num="1.7"><w n="7.1">Que</w> <w n="7.2">viens</w>-<w n="7.3">tu</w> <w n="7.4">faire</w> <w n="7.5">en</w> <w n="7.6">ce</w> <w n="7.7">guêpier</w>,</l>
					<l n="8" num="1.8"><w n="8.1">Sous</w> <w n="8.2">ta</w> <w n="8.3">châsse</w> <w n="8.4">de</w> <w n="8.5">nouveau</w> <w n="8.6">riche</w> ?</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1"><w n="9.1">Les</w> <w n="9.2">croix</w>, <w n="9.3">les</w> <w n="9.4">bûchers</w> <w n="9.5">et</w> <w n="9.6">les</w> <w n="9.7">triques</w></l>
					<l n="10" num="2.2"><w n="10.1">Que</w> <w n="10.2">demandaient</w> <w n="10.3">pour</w> <w n="10.4">marchepied</w></l>
					<l n="11" num="2.3"><w n="11.1">Tes</w> <w n="11.2">désirs</w> <w n="11.3">les</w> <w n="11.4">plus</w> <w n="11.5">chimériques</w></l>
					<l n="12" num="2.4"><w n="12.1">N</w>’<w n="12.2">étaient</w> <w n="12.3">rien</w>, <w n="12.4">près</w> <w n="12.5">de</w> <w n="12.6">ce</w> <w n="12.7">trépied</w>,</l>
					<l n="13" num="2.5"><w n="13.1">Devant</w> <w n="13.2">quoi</w> <w n="13.3">bobonne</w> <w n="13.4">et</w> <w n="13.5">troupier</w></l>
					<l n="14" num="2.6"><w n="14.1">Viennent</w> <w n="14.2">oublier</w> <w n="14.3">leur</w> <w n="14.4">matchiche</w>.</l>
					<l n="15" num="2.7"><w n="15.1">O</w> <w n="15.2">ton</w> <w n="15.3">beau</w> <w n="15.4">songe</w> <w n="15.5">estropié</w></l>
					<l n="16" num="2.8"><w n="16.1">Sous</w> <w n="16.2">ta</w> <w n="16.3">châsse</w> <w n="16.4">de</w> <w n="16.5">nouveau</w> <w n="16.6">riche</w> !</l>
				</lg>
				<lg n="3">
					<l n="17" num="3.1"><w n="17.1">Bazar</w> <w n="17.2">de</w> <w n="17.3">nonnes</w> <w n="17.4">hystériques</w>,</l>
					<l n="18" num="3.2"><w n="18.1">Billets</w> <w n="18.2">de</w> <w n="18.3">banque</w> <w n="18.4">de</w> <w n="18.5">croupier</w>,</l>
					<l n="19" num="3.3"><w n="19.1">Drapeaux</w> <w n="19.2">qui</w> <w n="19.3">dansent</w> <w n="19.4">des</w> <w n="19.5">pyrrhiques</w>,</l>
					<l n="20" num="3.4"><w n="20.1">Quel</w> <w n="20.2">mal</w> <w n="20.3">te</w> <w n="20.4">fait</w>-<w n="20.5">on</w> <w n="20.6">expier</w> ?</l>
					<l n="21" num="3.5"><w n="21.1">L</w>’<w n="21.2">ancien</w> <w n="21.3">se</w> <w n="21.4">pouvait</w> <w n="21.5">copier</w></l>
					<l n="22" num="3.6"><w n="22.1">Puisque</w> <w n="22.2">le</w> <w n="22.3">génie</w> <w n="22.4">est</w> <w n="22.5">en</w> <w n="22.6">friche</w> !</l>
					<l n="23" num="3.7"><w n="23.1">Las</w> ! <w n="23.2">Te</w> <w n="23.3">voir</w> <w n="23.4">bête</w> <w n="23.5">comme</w> <w n="23.6">pied</w></l>
					<l n="24" num="3.8"><w n="24.1">Sous</w> <w n="24.2">ta</w> <w n="24.3">châsse</w> <w n="24.4">de</w> <w n="24.5">nouveau</w> <w n="24.6">riche</w> !</l>
				</lg>
				<lg n="4">
					<head type="main">ENVOI</head>
					<l n="25" num="4.1"><w n="25.1">Ne</w> <w n="25.2">sens</w>-<w n="25.3">tu</w> <w n="25.4">pas</w>, <w n="25.5">puant</w> <w n="25.6">clapier</w>,</l>
					<l n="26" num="4.2"><w n="26.1">Foule</w> <w n="26.2">que</w> <w n="26.3">tant</w> <w n="26.4">d</w>’<w n="26.5">horreur</w> <w n="26.6">entiche</w>,</l>
					<l n="27" num="4.3"><w n="27.1">La</w> <w n="27.2">sainte</w> <w n="27.3">triste</w> <w n="27.4">t</w>’<w n="27.5">épier</w></l>
					<l n="28" num="4.4"><w n="28.1">Sous</w> <w n="28.2">sa</w> <w n="28.3">châsse</w> <w n="28.4">de</w> <w n="28.5">nouveau</w> <w n="28.6">riche</w> ?</l>
				</lg>
			</div></body></text></TEI>