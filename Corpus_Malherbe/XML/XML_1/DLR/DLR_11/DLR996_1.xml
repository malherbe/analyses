<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">TEMPS PRÉSENTS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>818 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_11</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title type="main">TEMPS PRÉSENTS</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Les Cahiers d’art et d’amitié, P. Mourousy</publisher>
							<date when="1939">1939</date>
						</imprint>
					</monogr>
					<note>Édition numérisée sur demande (BnF)</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1939">1939</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>.....</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLR996">
				<head type="main">BALLADE VILLON</head>
				<head type="sub_1">(EN HOMMAGE A CELUI DONT J’EMPRUNTAI LA FORME)</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Or</w> <w n="1.2">çà</w>, <w n="1.3">maître</w> <w n="1.4">François</w> <w n="1.5">Villon</w>,</l>
					<l n="2" num="1.2"><w n="2.1">On</w> <w n="2.2">te</w> <w n="2.3">connaît</w> <w n="2.4">plus</w> <w n="2.5">d</w>’<w n="2.6">une</w> <w n="2.7">tache</w> !</l>
					<l n="3" num="1.3"><w n="3.1">N</w>’<w n="3.2">ayant</w> <w n="3.3">pas</w> <w n="3.4">vaillant</w> <w n="3.5">un</w> <w n="3.6">oignon</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Le</w> <w n="4.2">vol</w> <w n="4.3">fut</w> <w n="4.4">bel</w> <w n="4.5">et</w> <w n="4.6">bien</w> <w n="4.7">ta</w> <w n="4.8">tâche</w>.</l>
					<l n="5" num="1.5"><w n="5.1">Tu</w> <w n="5.2">n</w>’<w n="5.3">étais</w> <w n="5.4">après</w> <w n="5.5">tout</w> <w n="5.6">qu</w>’<w n="5.7">apache</w>,</l>
					<l n="6" num="1.6"><w n="6.1">Mais</w> <w n="6.2">tes</w> <w n="6.3">blanches</w> <w n="6.4">neiges</w> <w n="6.5">d</w>’<w n="6.6">antan</w></l>
					<l n="7" num="1.7"><w n="7.1">Empêchent</w> <w n="7.2">toujours</w> <w n="7.3">qu</w>’<w n="7.4">on</w> <w n="7.5">se</w> <w n="7.6">fâche</w>.</l>
					<l n="8" num="1.8"><w n="8.1">Voleur</w> ? <w n="8.2">Grand</w> <w n="8.3">poète</w>, <w n="8.4">pourtant</w> !</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1"><w n="9.1">Ta</w> <w n="9.2">muse</w> <w n="9.3">jamais</w> <w n="9.4">n</w>’<w n="9.5">a</w> <w n="9.6">bâillon</w>,</l>
					<l n="10" num="2.2"><w n="10.1">Même</w> <w n="10.2">au</w> <w n="10.3">cachot</w> <w n="10.4">où</w> <w n="10.5">l</w>’<w n="10.6">on</w> <w n="10.7">te</w> <w n="10.8">cache</w>.</l>
					<l n="11" num="2.3"><w n="11.1">Rimant</w> <w n="11.2">ta</w> <w n="11.3">mort</w> <w n="11.4">sous</w> <w n="11.5">le</w> <w n="11.6">haillon</w>,</l>
					<l n="12" num="2.4"><w n="12.1">Certes</w>, <w n="12.2">tu</w> <w n="12.3">ne</w> <w n="12.4">fus</w> <w n="12.5">pas</w> <w n="12.6">un</w> <w n="12.7">lâche</w> !</l>
					<l n="13" num="2.5"><w n="13.1">Paillard</w>, <w n="13.2">maquereau</w>, <w n="13.3">voire</w> <w n="13.4">vache</w>,</l>
					<l n="14" num="2.6"><w n="14.1">Changeant</w> <w n="14.2">en</w> <w n="14.3">poème</w> <w n="14.4">épatant</w></l>
					<l n="15" num="2.7"><w n="15.1">Jusqu</w>’<w n="15.2">au</w> <w n="15.3">pet</w> <w n="15.4">que</w> <w n="15.5">Margot</w> <w n="15.6">te</w> <w n="15.7">lâche</w>,</l>
					<l n="16" num="2.8"><w n="16.1">Voleur</w> ? <w n="16.2">Grand</w> <w n="16.3">poète</w>, <w n="16.4">pourtant</w> !</l>
				</lg>
				<lg n="3">
					<l n="17" num="3.1"><w n="17.1">Au</w> <w n="17.2">déduit</w> <w n="17.3">avec</w> <w n="17.4">ta</w> <w n="17.5">souillon</w>,</l>
					<l n="18" num="3.2"><w n="18.1">Ou</w> <w n="18.2">lorsque</w> <w n="18.3">ta</w> <w n="18.4">mère</w> <w n="18.5">t</w>’<w n="18.6">arrache</w></l>
					<l n="19" num="3.3"><w n="19.1">Ces</w> <w n="19.2">vers</w> <w n="19.3">pleins</w> <w n="19.4">d</w>’<w n="19.5">un</w> <w n="19.6">pieux</w> <w n="19.7">rayon</w>,</l>
					<l n="20" num="3.4"><w n="20.1">Faut</w>-<w n="20.2">il</w> <w n="20.3">que</w> <w n="20.4">ton</w> <w n="20.5">âme</w> <w n="20.6">ne</w> <w n="20.7">sache</w></l>
					<l n="21" num="3.5"><w n="21.1">Voir</w> <w n="21.2">le</w> <w n="21.3">futur</w> <w n="21.4">qui</w> <w n="21.5">s</w>’<w n="21.6">amourache</w></l>
					<l n="22" num="3.6"><w n="22.1">De</w> <w n="22.2">toi</w>, <w n="22.3">l</w>’<w n="22.4">ivrogne</w> <w n="22.5">hoquetant</w>,</l>
					<l n="23" num="3.7"><w n="23.1">Et</w>, <w n="23.2">tes</w> <w n="23.3">vers</w>, <w n="23.4">les</w> <w n="23.5">mâche</w> <w n="23.6">et</w> <w n="23.7">remâche</w> ?…</l>
					<l n="24" num="3.8"><w n="24.1">Voleur</w> ? <w n="24.2">Grand</w> <w n="24.3">poète</w>, <w n="24.4">pourtant</w> !</l>
				</lg>
				<lg n="4">
					<head type="main">ENVOI</head>
					<l n="25" num="4.1"><w n="25.1">Va</w> ! <w n="25.2">Bois</w>, <w n="25.3">et</w> <w n="25.4">baise</w>, <w n="25.5">et</w> <w n="25.6">vole</w>, <w n="25.7">et</w> <w n="25.8">gâche</w> !</l>
					<l n="26" num="4.2"><w n="26.1">En</w> <w n="26.2">avoir</w> <w n="26.3">commis</w> <w n="26.4">tant</w> <w n="26.5">et</w> <w n="26.6">tant</w> !</l>
					<l n="27" num="4.3"><w n="27.1">Corde</w> <w n="27.2">et</w> <w n="27.3">prison</w> — <w n="27.4">mais</w> <w n="27.5">quel</w> <w n="27.6">panache</w></l>
					<l n="28" num="4.4">— <w n="28.1">Voleur</w> ? <w n="28.2">Grand</w> <w n="28.3">poète</w>, <w n="28.4">pourtant</w> !</l>
				</lg>
			</div></body></text></TEI>