<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">TEMPS PRÉSENTS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>818 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_11</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title type="main">TEMPS PRÉSENTS</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Les Cahiers d’art et d’amitié, P. Mourousy</publisher>
							<date when="1939">1939</date>
						</imprint>
					</monogr>
					<note>Édition numérisée sur demande (BnF)</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1939">1939</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>.....</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLR968">
				<head type="main">PRÉFACE</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Ne</w> <w n="1.2">pas</w> <w n="1.3">croire</w> <w n="1.4">surtout</w> <w n="1.5">de</w> <w n="1.6">Mardrus</w>-<w n="1.7">Delarue</w></l>
					<l n="2" num="1.2"><w n="2.1">Que</w> <w n="2.2">ce</w> <w n="2.3">petit</w> <w n="2.4">bouquin</w> <w n="2.5">fut</w> <w n="2.6">écrit</w> <w n="2.7">tout</w> <w n="2.8">de</w> <w n="2.9">go</w>.</l>
					<l n="3" num="1.3"><w n="3.1">Bien</w> <w n="3.2">au</w> <w n="3.3">contraire</w> ! <w n="3.4">Étant</w> <w n="3.5">de</w> <w n="3.6">purisme</w> <w n="3.7">férue</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Elle</w> <w n="4.2">exige</w> <w n="4.3">qu</w>’<w n="4.4">un</w> <w n="4.5">vers</w> <w n="4.6">soit</w> <w n="4.7">de</w> <w n="4.8">l</w>’<w n="4.9">or</w> <w n="4.10">en</w> <w n="4.11">lingot</w></l>
					<l n="5" num="1.5"><w n="5.1">Et</w> <w n="5.2">ne</w> <w n="5.3">peut</w> <w n="5.4">supporter</w> <w n="5.5">le</w> <w n="5.6">style</w> <w n="5.7">visigoth</w>,</l>
					<l n="6" num="1.6"><w n="6.1">Ni</w> <w n="6.2">qu</w>’<w n="6.3">un</w> <w n="6.4">poème</w> <w n="6.5">soit</w>, <w n="6.6">de</w> <w n="6.7">nos</w> <w n="6.8">jours</w>, <w n="6.9">une</w> <w n="6.10">loque</w>.</l>
					<l n="7" num="1.7">… <w n="7.1">Et</w>, <w n="7.2">maintenant</w>, <w n="7.3">à</w> <w n="7.4">nous</w> <w n="7.5">et</w> <w n="7.6">l</w>’<hi rend="ital"><w n="7.7">item</w></hi> <w n="7.8">et</w> <w n="7.9">l</w>’<hi rend="ital"><w n="7.10">ergo</w></hi>,</l>
					<l n="8" num="1.8"><w n="8.1">Car</w> <w n="8.2">ces</w> <w n="8.3">ballades</w>-<w n="8.4">ci</w> <w n="8.5">parlent</w> <w n="8.6">de</w> <w n="8.7">notre</w> <w n="8.8">époque</w>.</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1"><w n="9.1">Politique</w>, <w n="9.2">impôts</w>, <w n="9.3">sports</w>, <w n="9.4">le</w> <w n="9.5">giton</w> <w n="9.6">et</w> <w n="9.7">la</w> <w n="9.8">grue</w>,</l>
					<l n="10" num="2.2"><w n="10.1">Les</w> <w n="10.2">juifs</w>, <w n="10.3">et</w> <w n="10.4">la</w> <w n="10.5">rombière</w> <w n="10.6">au</w> <w n="10.7">profil</w> <w n="10.8">de</w> <w n="10.9">magot</w>,</l>
					<l n="11" num="2.3"><w n="11.1">L</w>’<w n="11.2">avion</w> <w n="11.3">dans</w> <w n="11.4">le</w> <w n="11.5">ciel</w>, <w n="11.6">les</w> <w n="11.7">croquants</w> <w n="11.8">dans</w> <w n="11.9">la</w> <w n="11.10">rue</w>,</l>
					<l n="12" num="2.4"><w n="12.1">Tout</w> <w n="12.2">un</w> <w n="12.3">stock</w> <w n="12.4">va</w> <w n="12.5">vers</w> <w n="12.6">vous</w> <w n="12.7">voguer</w> <w n="12.8">à</w> <w n="12.9">plein</w> <w n="12.10">cargo</w></l>
					<l n="13" num="2.5"><w n="13.1">Sur</w> <w n="13.2">quoi</w> <w n="13.3">mettra</w> <w n="13.4">la</w> <w n="13.5">mort</w> <w n="13.6">le</w> <w n="13.7">suprême</w> <w n="13.8">embargo</w>.</l>
					<l n="14" num="2.6"><w n="14.1">Que</w> <w n="14.2">de</w> <w n="14.3">sujets</w> <w n="14.4">offerts</w> <w n="14.5">à</w> <w n="14.6">quiconque</w> <w n="14.7">se</w> <w n="14.8">moque</w> !</l>
					<l n="15" num="2.7"><w n="15.1">Plus</w> <w n="15.2">d</w>’<w n="15.3">un</w> <w n="15.4">couplet</w>, <w n="15.5">d</w>’<w n="15.6">ailleurs</w>, <w n="15.7">sentira</w> <w n="15.8">le</w> <w n="15.9">fagot</w>,</l>
					<l n="16" num="2.8"><w n="16.1">Car</w> <w n="16.2">ces</w> <w n="16.3">ballades</w>-<w n="16.4">ci</w> <w n="16.5">parlent</w> <w n="16.6">de</w> <w n="16.7">notre</w> <w n="16.8">époque</w>.</l>
				</lg>
				<lg n="3">
					<l n="17" num="3.1"><w n="17.1">La</w> <w n="17.2">ballade</w> <w n="17.3">a</w> <w n="17.4">ceci</w> <w n="17.5">pour</w> <w n="17.6">elle</w>, <w n="17.7">accorte</w> <w n="17.8">et</w> <w n="17.9">drue</w>,</l>
					<l n="18" num="3.2"><w n="18.1">Qu</w>’<w n="18.2">elle</w> <w n="18.3">admet</w> <w n="18.4">au</w> <w n="18.5">besoin</w> <w n="18.6">le</w> <w n="18.7">gros</w> <w n="18.8">mot</w> <w n="18.9">et</w> <w n="18.10">l</w>’<w n="18.11">argot</w>.</l>
					<l n="19" num="3.3"><w n="19.1">Ironique</w> <w n="19.2">avant</w> <w n="19.3">tout</w> <w n="19.4">et</w> <w n="19.5">parfois</w> <w n="19.6">incongrue</w>,</l>
					<l n="20" num="3.4"><w n="20.1">Cette</w> <w n="20.2">perle</w> <w n="20.3">n</w>’<w n="20.4">est</w> <w n="20.5">point</w> <w n="20.6">pour</w> <w n="20.7">les</w> <w n="20.8">pieds</w> <w n="20.9">du</w> <w n="20.10">nigaud</w>.</l>
					<l n="21" num="3.5"><w n="21.1">Sa</w> <w n="21.2">science</w>, <w n="21.3">aux</w> <w n="21.4">rimeurs</w>, <w n="21.5">donne</w> <w n="21.6">le</w> <w n="21.7">vertigo</w> ?…</l>
					<l n="22" num="3.6"><w n="22.1">Nonobstant</w> <w n="22.2">catachrèse</w> <w n="22.3">et</w> <w n="22.4">malgré</w> <w n="22.5">synecdoque</w>,</l>
					<l n="23" num="3.7"><w n="23.1">Tant</w> <w n="23.2">d</w>’<w n="23.3">art</w>, <w n="23.4">heureusement</w>, <w n="23.5">n</w>’<w n="23.6">exclut</w> <w n="23.7">aucun</w> <w n="23.8">ragot</w>,</l>
					<l n="24" num="3.8"><w n="24.1">Car</w> <w n="24.2">ces</w> <w n="24.3">ballades</w>-<w n="24.4">ci</w> <w n="24.5">parlent</w> <w n="24.6">de</w> <w n="24.7">noire</w> <w n="24.8">époque</w>.</l>
				</lg>
				<lg n="4">
					<head type="main">ENVOI</head>
					<l n="25" num="4.1"><w n="25.1">Que</w> <w n="25.2">le</w> <w n="25.3">présent</w> <w n="25.4">ouvrage</w>, <w n="25.5">ô</w> <w n="25.6">lecteur</w>, <w n="25.7">ne</w> <w n="25.8">te</w> <w n="25.9">choque</w> !</l>
					<l n="26" num="4.2"><w n="26.1">Il</w> <w n="26.2">use</w>, <w n="26.3">j</w>’<w n="26.4">en</w> <w n="26.5">conviens</w>, <w n="26.6">du</w> <w n="26.7">bec</w> <w n="26.8">et</w> <w n="26.9">de</w> <w n="26.10">l</w>’<w n="26.11">ergot</w></l>
					<l n="27" num="4.3"><w n="27.1">Et</w> <w n="27.2">n</w>’<w n="27.3">épargne</w> <w n="27.4">bourgeois</w>, <w n="27.5">canaille</w> <w n="27.6">ni</w> <w n="27.7">gogo</w>…</l>
					<l n="28" num="4.4">— <w n="28.1">Mais</w> <w n="28.2">ces</w> <w n="28.3">ballades</w>-<w n="28.4">ci</w> <w n="28.5">parlent</w> <w n="28.6">de</w> <w n="28.7">notre</w> <w n="28.8">époque</w>.</l>
				</lg>
			</div></body></text></TEI>