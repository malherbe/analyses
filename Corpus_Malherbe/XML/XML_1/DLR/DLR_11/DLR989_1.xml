<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">TEMPS PRÉSENTS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>818 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_11</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title type="main">TEMPS PRÉSENTS</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Les Cahiers d’art et d’amitié, P. Mourousy</publisher>
							<date when="1939">1939</date>
						</imprint>
					</monogr>
					<note>Édition numérisée sur demande (BnF)</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1939">1939</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>.....</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLR989">
				<head type="main">BALLADE DES ÉCHECS</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Sur</w> <w n="1.2">l</w>’<w n="1.3">échiquier</w>, <w n="1.4">luisant</w> <w n="1.5">miroir</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Quand</w> <w n="2.2">brillent</w>, <w n="2.3">rangés</w> <w n="2.4">en</w> <w n="2.5">bataille</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Deux</w> <w n="3.2">peuples</w> : <w n="3.3">l</w>’<w n="3.4">un</w> <w n="3.5">du</w> <w n="3.6">plus</w> <w n="3.7">beau</w> <w n="3.8">noir</w>,</l>
					<l n="4" num="1.4"><w n="4.1">L</w>’<w n="4.2">autre</w> <w n="4.3">du</w> <w n="4.4">plus</w> <w n="4.5">beau</w> <w n="4.6">jaune</w> <w n="4.7">paille</w>,</l>
					<l n="5" num="1.5"><w n="5.1">Quand</w>, <w n="5.2">redressant</w> <w n="5.3">leur</w> <w n="5.4">haute</w> <w n="5.5">taille</w>,</l>
					<l n="6" num="1.6"><w n="6.1">La</w> <w n="6.2">reine</w> <w n="6.3">et</w> <w n="6.4">le</w> <w n="6.5">roi</w>, <w n="6.6">couple</w> <w n="6.7">fat</w>,</l>
					<l n="7" num="1.7"><w n="7.1">Se</w> <w n="7.2">rengorgent</w> <w n="7.3">comme</w> <w n="7.4">à</w> <w n="7.5">Versaille</w>,</l>
					<l n="8" num="1.8"><w n="8.1">Qui</w> <w n="8.2">va</w> <w n="8.3">donner</w> <w n="8.4">l</w>’<w n="8.5">échec</w> <w n="8.6">et</w> <w n="8.7">mat</w> ?</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1"><w n="9.1">Chacun</w> <w n="9.2">fera</w> <w n="9.3">tout</w> <w n="9.4">son</w> <w n="9.5">devoir</w></l>
					<l n="10" num="2.2"><w n="10.1">Comme</w> <w n="10.2">il</w> <w n="10.3">pourra</w>, <w n="10.4">vaille</w> <w n="10.5">que</w> <w n="10.6">vaille</w>.</l>
					<l n="11" num="2.3"><w n="11.1">Le</w> <w n="11.2">roi</w> <w n="11.3">tremble</w> <w n="11.4">en</w> <w n="11.5">son</w> <w n="11.6">étouffoir</w>.</l>
					<l n="12" num="2.4"><w n="12.1">Fous</w>, <w n="12.2">chevaux</w>, <w n="12.3">tours</w> <w n="12.4">et</w> <w n="12.5">valetaille</w>,</l>
					<l n="13" num="2.5"><w n="13.1">Tout</w> <w n="13.2">le</w> <w n="13.3">monde</w> <w n="13.4">bientôt</w> <w n="13.5">s</w>’<w n="13.6">égaille</w>,</l>
					<l n="14" num="2.6"><w n="14.1">L</w>’<w n="14.2">action</w> <w n="14.3">s</w>’<w n="14.4">engage</w>… <w n="14.5">A</w> <w n="14.6">dieu</w> <w n="14.7">vat</w> !</l>
					<l n="15" num="2.7"><w n="15.1">L</w>’<w n="15.2">un</w> <w n="15.3">se</w> <w n="15.4">défend</w> <w n="15.5">et</w> <w n="15.6">l</w>’<w n="15.7">autre</w> <w n="15.8">assaille</w> ;</l>
					<l n="16" num="2.8"><w n="16.1">Qui</w> <w n="16.2">va</w> <w n="16.3">donner</w> <w n="16.4">l</w>’<w n="16.5">échec</w> <w n="16.6">et</w> <w n="16.7">mat</w> ?</l>
				</lg>
				<lg n="3">
					<l n="17" num="3.1"><w n="17.1">Les</w> <w n="17.2">pions</w> <w n="17.3">vont</w> <w n="17.4">à</w> <w n="17.5">l</w>’<w n="17.6">abbatoir</w>.</l>
					<l n="18" num="3.2"><w n="18.1">Le</w> <w n="18.2">cheval</w> <w n="18.3">rue</w> <w n="18.4">et</w> <w n="18.5">le</w> <w n="18.6">fou</w> <w n="18.7">raille</w>,</l>
					<l n="19" num="3.3"><w n="19.1">Tandis</w> <w n="19.2">que</w>, <w n="19.3">lente</w> <w n="19.4">à</w> <w n="19.5">s</w>’<w n="19.6">émouvoir</w>,</l>
					<l n="20" num="3.4"><w n="20.1">La</w> <w n="20.2">tour</w>, <w n="20.3">ronde</w> <w n="20.4">comme</w> <w n="20.5">futaille</w>,</l>
					<l n="21" num="3.5"><w n="21.1">Attend</w>, <w n="21.2">pour</w> <w n="21.3">lancer</w> <w n="21.4">sa</w> <w n="21.5">mitraille</w>,</l>
					<l n="22" num="3.6"><w n="22.1">L</w>’<w n="22.2">occasion</w> <w n="22.3">d</w>’<w n="22.4">un</w> <w n="22.5">exeat</w>.</l>
					<l n="23" num="3.7">— <w n="23.1">Échec</w> <w n="23.2">au</w> <w n="23.3">roi</w> ! — <w n="23.4">Bien</w> ! <w n="23.5">Qu</w>’<w n="23.6">il</w> <w n="23.7">s</w>’<w n="23.8">en</w> <w n="23.9">aille</w> !</l>
					<l n="24" num="3.8">… <w n="24.1">Qui</w> <w n="24.2">va</w> <w n="24.3">donner</w> <w n="24.4">l</w>’<w n="24.5">échec</w> <w n="24.6">et</w> <w n="24.7">mat</w> ?</l>
				</lg>
				<lg n="4">
					<head type="main">ENVOI</head>
					<l n="25" num="4.1"><w n="25.1">Reine</w> <w n="25.2">dont</w> <w n="25.3">l</w>’<w n="25.4">orgueil</w> <w n="25.5">ne</w> <w n="25.6">défaille</w>,</l>
					<l n="26" num="4.2"><w n="26.1">Plus</w> <w n="26.2">puissante</w> <w n="26.3">que</w> <w n="26.4">Goliath</w>,</l>
					<l n="27" num="4.3"><w n="27.1">Crains</w> <w n="27.2">le</w> <w n="27.3">pion</w>, <w n="27.4">humble</w> <w n="27.5">canaille</w></l>
					<l n="28" num="4.4"><w n="28.1">Qui</w> <w n="28.2">va</w> <w n="28.3">donner</w> <w n="28.4">l</w>’<w n="28.5">échec</w> <w n="28.6">et</w> <w n="28.7">mat</w>.</l>
				</lg>
			</div></body></text></TEI>