<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">TEMPS PRÉSENTS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>818 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_11</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title type="main">TEMPS PRÉSENTS</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Les Cahiers d’art et d’amitié, P. Mourousy</publisher>
							<date when="1939">1939</date>
						</imprint>
					</monogr>
					<note>Édition numérisée sur demande (BnF)</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1939">1939</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>.....</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLR975">
				<head type="main">BALLADE DE L’ASTROLOGIE</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Levant</w> <w n="1.2">les</w> <w n="1.3">yeux</w> <w n="1.4">devers</w> <w n="1.5">le</w> <w n="1.6">ciel</w> <w n="1.7">nocturne</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Loin</w> <w n="2.2">d</w>’<w n="2.3">admirer</w>, <w n="2.4">à</w> <w n="2.5">l</w>’<w n="2.6">entour</w> <w n="2.7">du</w> <w n="2.8">croissant</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Tant</w> <w n="3.2">d</w>’<w n="3.3">astres</w> <w n="3.4">d</w>’<w n="3.5">or</w> <w n="3.6">répandus</w> <w n="3.7">à</w> <w n="3.8">pleine</w> <w n="3.9">urne</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Cherchons</w> <w n="4.2">plutôt</w> <w n="4.3">ce</w> <w n="4.4">qui</w> <w n="4.5">pour</w> <w n="4.6">nous</w> <w n="4.7">descend</w></l>
					<l n="5" num="1.5"><w n="5.1">Ou</w> <w n="5.2">descendra</w> <w n="5.3">de</w> <w n="5.4">ce</w> <w n="5.5">ciel</w> <w n="5.6">menaçant</w>,</l>
					<l n="6" num="1.6"><w n="6.1">Car</w>, <w n="6.2">notre</w> <w n="6.3">terre</w> <w n="6.4">étant</w> <w n="6.5">par</w> <w n="6.6">lui</w> <w n="6.7">régie</w>,</l>
					<l n="7" num="1.7"><w n="7.1">Nous</w> <w n="7.2">apprenons</w> <w n="7.3">qu</w>’<w n="7.4">ancien</w> <w n="7.5">ou</w> <w n="7.6">récent</w></l>
					<l n="8" num="1.8"><w n="8.1">Notre</w> <w n="8.2">malheur</w>, <w n="8.3">c</w>’<w n="8.4">est</w> <w n="8.5">de</w> <w n="8.6">l</w>’<w n="8.7">astrologie</w>.</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1"><w n="9.1">Le</w> <w n="9.2">monde</w>, <w n="9.3">las</w> <w n="9.4">de</w> <w n="9.5">tremper</w> <w n="9.6">son</w> <w n="9.7">cothurne</w></l>
					<l n="10" num="2.2"><w n="10.1">Dans</w> <w n="10.2">un</w> <w n="10.3">ruisseau</w> <w n="10.4">de</w> <w n="10.5">larmes</w> <w n="10.6">et</w> <w n="10.7">de</w> <w n="10.8">sang</w>,</l>
					<l n="11" num="2.3"><w n="11.1">Rêvant</w> <w n="11.2">de</w> <w n="11.3">paix</w>, <w n="11.4">espère</w>, <w n="11.5">taciturne</w>,</l>
					<l n="12" num="2.4"><w n="12.1">Voir</w> <w n="12.2">s</w>’<w n="12.3">arrêter</w> <w n="12.4">ce</w> <w n="12.5">massacre</w> <w n="12.6">incessant</w>.</l>
					<l n="13" num="2.5"><w n="13.1">Mais</w>, <w n="13.2">sans</w> <w n="13.3">répit</w> <w n="13.4">et</w> <w n="13.5">partout</w> <w n="13.6">entassant</w></l>
					<l n="14" num="2.6"><w n="14.1">Cadavres</w> <w n="14.2">chauds</w> <w n="14.3">sur</w> <w n="14.4">la</w> <w n="14.5">terre</w> <w n="14.6">rougie</w>,</l>
					<l n="15" num="2.7"><w n="15.1">Mort</w> <w n="15.2">n</w>’<w n="15.3">ouït</w> <w n="15.4">point</w> <w n="15.5">noire</w> <w n="15.6">tragique</w> <w n="15.7">accent</w>.</l>
					<l n="16" num="2.8"><w n="16.1">Notre</w> <w n="16.2">malheur</w>, <w n="16.3">c</w>’<w n="16.4">est</w> <w n="16.5">de</w> <w n="16.6">l</w>’<w n="16.7">astrologie</w>.</l>
				</lg>
				<lg n="3">
					<l n="17" num="3.1"><w n="17.1">Guerre</w> <w n="17.2">partout</w> ? <w n="17.3">Il</w> <w n="17.4">paraît</w> <w n="17.5">que</w> <w n="17.6">Saturne</w>,</l>
					<l n="18" num="3.2"><w n="18.1">Pris</w> <w n="18.2">tout</w> <w n="18.3">soudain</w> <w n="18.4">d</w>’<w n="18.5">un</w> <w n="18.6">désir</w> <w n="18.7">indécent</w>,</l>
					<l n="19" num="3.3"><w n="19.1">De</w> <w n="19.2">son</w> <w n="19.3">anneau</w> <w n="19.4">trop</w> <w n="19.5">étroit</w>, <w n="19.6">cette</w> <w n="19.7">turne</w>,</l>
					<l n="20" num="3.4"><w n="20.1">Veut</w> <w n="20.2">s</w>’<w n="20.3">échapper</w>, <w n="20.4">étalon</w> <w n="20.5">cent</w> <w n="20.6">pour</w> <w n="20.7">cent</w>,</l>
					<l n="21" num="3.5"><w n="21.1">Pour</w> <w n="21.2">conter</w> <w n="21.3">flamme</w> <w n="21.4">à</w> <w n="21.5">planètes</w> <w n="21.6">passant</w>,</l>
					<l n="22" num="3.6"><w n="22.1">Dont</w> <w n="22.2">il</w> <w n="22.3">appert</w>, <w n="22.4">martyrs</w> <w n="22.5">de</w> <w n="22.6">cette</w> <w n="22.7">orgie</w>,</l>
					<l n="23" num="3.7"><w n="23.1">Que</w>, <w n="23.2">nonobstant</w> <w n="23.3">nos</w> <w n="23.4">raisons</w>, (<w n="23.5">c</w>’<w n="23.6">est</w> <w n="23.7">vexant</w> !)</l>
					<l n="24" num="3.8"><w n="24.1">Notre</w> <w n="24.2">malheur</w>, <w n="24.3">c</w>’<w n="24.4">est</w> <w n="24.5">de</w> <w n="24.6">l</w>’<w n="24.7">astrologie</w>.</l>
				</lg>
				<lg n="4">
					<head type="main">ENVOI</head>
					<l n="25" num="4.1"><w n="25.1">Veuille</w> <w n="25.2">souffler</w> <w n="25.3">comme</w> <w n="25.4">simple</w> <w n="25.5">bougie</w></l>
					<l n="26" num="4.2"><w n="26.1">Ces</w> <w n="26.2">excités</w> <w n="26.3">du</w> <w n="26.4">ciel</w>, <w n="26.5">ô</w> <w n="26.6">Tout</w> <w n="26.7">Puissant</w> !</l>
					<l n="27" num="4.3"><w n="27.1">Puisque</w> (<w n="27.2">fais</w> <w n="27.3">vite</w>, <w n="27.4">ô</w> <w n="27.5">Seigneur</w>, <w n="27.6">c</w>’<w n="27.7">est</w> <w n="27.8">pressant</w> !)</l>
					<l n="28" num="4.4"><w n="28.1">Notre</w> <w n="28.2">malheur</w>, <w n="28.3">c</w>’<w n="28.4">est</w> <w n="28.5">de</w> <w n="28.6">l</w>’<w n="28.7">astrologie</w>.</l>
				</lg>
			</div></body></text></TEI>