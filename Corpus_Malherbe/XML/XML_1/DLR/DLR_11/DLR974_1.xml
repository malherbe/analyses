<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">TEMPS PRÉSENTS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>818 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_11</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title type="main">TEMPS PRÉSENTS</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Les Cahiers d’art et d’amitié, P. Mourousy</publisher>
							<date when="1939">1939</date>
						</imprint>
					</monogr>
					<note>Édition numérisée sur demande (BnF)</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1939">1939</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>.....</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLR974">
				<head type="main">BALLADE DE LA POLITIQUE</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">En</w> <w n="1.2">même</w> <w n="1.3">temps</w> <w n="1.4">que</w> <w n="1.5">bluff</w>, <w n="1.6">publicité</w>, <w n="1.7">combine</w></l>
					<l n="2" num="1.2"><w n="2.1">Auront</w> <w n="2.2">donné</w> <w n="2.3">sa</w> <w n="2.4">marque</w> <w n="2.5">à</w> <w n="2.6">l</w>’<w n="2.7">âge</w> <w n="2.8">du</w> <w n="2.9">moteur</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Pour</w> <w n="3.2">faire</w> <w n="3.3">voir</w> <w n="3.4">ses</w> <w n="3.5">crocs</w> <w n="3.6">retroussant</w> <w n="3.7">sa</w> <w n="3.8">babine</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Chaque</w> <w n="4.2">peuple</w>, <w n="4.3">aujourd</w>’<w n="4.4">hui</w>, <w n="4.5">se</w> <w n="4.6">veut</w> <w n="4.7">gladiateur</w>.</l>
					<l n="5" num="1.5"><w n="5.1">Qui</w> <w n="5.2">n</w>’<w n="5.3">a</w> <w n="5.4">pas</w> <w n="5.5">désormais</w> <w n="5.6">son</w> <w n="5.7">petit</w> <w n="5.8">dictateur</w> ?</l>
					<l n="6" num="1.6"><w n="6.1">Le</w> <w n="6.2">communisme</w> <w n="6.3">seul</w> <w n="6.4">n</w>’<w n="6.5">envisage</w> <w n="6.6">qu</w>’<w n="6.7">apôtres</w>.</l>
					<l n="7" num="1.7"><w n="7.1">Égalité</w> <w n="7.2">partout</w> <w n="7.3">et</w> <w n="7.4">pas</w> <w n="7.5">de</w> <w n="7.6">chef</w>. <w n="7.7">Menteur</w> !</l>
					<l n="8" num="1.8"><w n="8.1">Ils</w> <w n="8.2">sont</w> <w n="8.3">tous</w> <w n="8.4">emmerdants</w>, <w n="8.5">les</w> <w n="8.6">uns</w> <w n="8.7">comme</w> <w n="8.8">les</w> <w n="8.9">autres</w>.</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1"><w n="9.1">Hitler</w> <w n="9.2">qui</w> <w n="9.3">va</w> <w n="9.4">trop</w> <w n="9.5">fort</w>, <w n="9.6">la</w> <w n="9.7">France</w> <w n="9.8">qui</w> <w n="9.9">lambine</w>,</l>
					<l n="10" num="2.2"><w n="10.1">Tourbillon</w> <w n="10.2">d</w>’<w n="10.3">une</w> <w n="10.4">part</w> <w n="10.5">et</w> <w n="10.6">de</w> <w n="10.7">l</w>’<w n="10.8">autre</w> <w n="10.9">lenteur</w>,</l>
					<l n="11" num="2.3"><w n="11.1">Va</w>-<w n="11.2">t</w>-<w n="11.3">on</w> <w n="11.4">longtemps</w> <w n="11.5">encor</w> <w n="11.6">s</w>’<w n="11.7">offrir</w> <w n="11.8">notre</w> <w n="11.9">bobine</w> ?</l>
					<l n="12" num="2.4"><w n="12.1">Guerres</w>, <w n="12.2">grèves</w>, <w n="12.3">impôts</w>, <w n="12.4">décrets</w>, <w n="12.5">O</w> <w n="12.6">Créateur</w></l>
					<l n="13" num="2.5"><w n="13.1">Vous</w> <w n="13.2">qui</w> <w n="13.3">de</w> <w n="13.4">nos</w> <w n="13.5">tourments</w> <w n="13.6">êtes</w> <w n="13.7">le</w> <w n="13.8">spectateur</w>,</l>
					<l n="14" num="2.6"><w n="14.1">En</w> <w n="14.2">ce</w> <w n="14.3">siècle</w> <w n="14.4">agité</w> <w n="14.5">quels</w> <w n="14.6">desseins</w> <w n="14.7">sont</w> <w n="14.8">les</w> <w n="14.9">vôtres</w> ?</l>
					<l n="15" num="2.7"><w n="15.1">Ici</w> <w n="15.2">la</w> <w n="15.3">liberté</w>, <w n="15.4">là</w> <w n="15.5">le</w> <w n="15.6">libérateur</w>,</l>
					<l n="16" num="2.8"><w n="16.1">Ils</w> <w n="16.2">sont</w> <w n="16.3">tous</w> <w n="16.4">emmerdants</w>, <w n="16.5">les</w> <w n="16.6">uns</w> <w n="16.7">comme</w> <w n="16.8">les</w> <w n="16.9">autres</w>.</l>
				</lg>
				<lg n="3">
					<l n="17" num="3.1"><w n="17.1">De</w> <w n="17.2">faillite</w> <w n="17.3">en</w> <w n="17.4">terreur</w>, <w n="17.5">on</w> <w n="17.6">ronchonne</w>, <w n="17.7">on</w> <w n="17.8">turbine</w> ;</l>
					<l n="18" num="3.2"><w n="18.1">Ah</w> ! <w n="18.2">qu</w>’<w n="18.3">on</w> <w n="18.4">aimerait</w> <w n="18.5">mieux</w> <w n="18.6">ouïr</w> <w n="18.7">quelque</w> <w n="18.8">chanteur</w></l>
					<l n="19" num="3.3"><w n="19.1">Ou</w> <w n="19.2">bien</w> <w n="19.3">voir</w> <w n="19.4">Arlequin</w> <w n="19.5">courtiser</w> <w n="19.6">Colombine</w> !</l>
					<l n="20" num="3.4"><w n="20.1">Du</w> <w n="20.2">bien</w>-<w n="20.3">vivre</w>, <w n="20.4">après</w> <w n="20.5">tout</w>, <w n="20.6">chacun</w> <w n="20.7">est</w> <w n="20.8">amateur</w>,</l>
					<l n="21" num="3.5"><w n="21.1">Et</w> <w n="21.2">le</w> <w n="21.3">joli</w> <w n="21.4">printemps</w> <w n="21.5">a</w> <w n="21.6">toujours</w> <w n="21.7">sa</w> <w n="21.8">senteur</w>.</l>
					<l n="22" num="3.6"><w n="22.1">Mais</w> <w n="22.2">où</w> <w n="22.3">qu</w>’<w n="22.4">on</w> <w n="22.5">aille</w>, <w n="22.6">hélas</w> ! <w n="22.7">canon</w> <w n="22.8">ou</w> <w n="22.9">patenôtres</w>,</l>
					<l n="23" num="3.7"><w n="23.1">Faucille</w> <w n="23.2">du</w> <w n="23.3">soviet</w> <w n="23.4">ou</w> <w n="23.5">faisceau</w> <w n="23.6">du</w> <w n="23.7">licteur</w>,</l>
					<l n="24" num="3.8"><w n="24.1">Ils</w> <w n="24.2">sont</w> <w n="24.3">tous</w> <w n="24.4">emmerdants</w>, <w n="24.5">les</w> <w n="24.6">uns</w> <w n="24.7">comme</w> <w n="24.8">les</w> <w n="24.9">autres</w>.</l>
				</lg>
				<lg n="4">
					<head type="main">ENVOI</head>
					<l n="25" num="4.1"><w n="25.1">O</w> <w n="25.2">Paix</w> ! <w n="25.3">Beau</w> <w n="25.4">champ</w> <w n="25.5">de</w> <w n="25.6">blé</w> <w n="25.7">qui</w> <w n="25.8">n</w>’<w n="25.9">es</w> <w n="25.10">plus</w> <w n="25.11">rien</w> <w n="25.12">qu</w>’<w n="25.13">épeautres</w>,</l>
					<l n="26" num="4.2"><w n="26.1">Quel</w> <w n="26.2">démon</w> <w n="26.3">t</w>’<w n="26.4">envoya</w> <w n="26.5">ce</w> <w n="26.6">souffle</w> <w n="26.7">destructeur</w> ?</l>
					<l n="27" num="4.3"><w n="27.1">Mais</w>, <w n="27.2">à</w> <w n="27.3">la</w> <w n="27.4">fin</w>, <w n="27.5">pourquoi</w> <w n="27.6">nous</w> <w n="27.7">donner</w> <w n="27.8">un</w> <w n="27.9">tuteur</w> ?</l>
					<l n="28" num="4.4"><w n="28.1">Ils</w> <w n="28.2">sont</w> <w n="28.3">tous</w> <w n="28.4">emmerdants</w>, <w n="28.5">les</w> <w n="28.6">uns</w> <w n="28.7">comme</w> <w n="28.8">les</w> <w n="28.9">autres</w>.</l>
				</lg>
			</div></body></text></TEI>