<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">FERVEUR</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2031 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Ferveur</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">http://www.poesies.net/luciedelaruemardrueferveur.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Ferveur</title>
						<author>Lucie Delarue-Mardrus</author>
						<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k9691679h?rk=21459;2</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LA REVUE BLANCHE</publisher>
							<date when="1902">1902</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1902">1902</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">POÈMES TERRESTRES</head><div type="poem" key="DLR158">
					<head type="main">QUATRIÈME DÉCEMBRALE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Puisqu</w>’<w n="1.2">il</w> <w n="1.3">vient</w> <w n="1.4">à</w> <w n="1.5">passer</w>, <w n="1.6">sur</w> <w n="1.7">l</w>’<w n="1.8">horreur</w> <w n="1.9">décembrale</w></l>
						<l n="2" num="1.2"><w n="2.1">Du</w> <w n="2.2">dehors</w>, <w n="2.3">ce</w> <w n="2.4">frisson</w> <w n="2.5">de</w> <w n="2.6">tiédeur</w> <w n="2.7">anormale</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Je</w> <w n="3.2">hanterai</w> <w n="3.3">tes</w> <w n="3.4">troncs</w> <w n="3.5">épais</w> <w n="3.6">comme</w> <w n="3.7">des</w> <w n="3.8">tours</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Jardin</w> <w n="4.2">crochu</w>, <w n="4.3">jardin</w> <w n="4.4">trempé</w> <w n="4.5">des</w> <w n="4.6">mauvais</w> <w n="4.7">jours</w>,</l>
						<l n="5" num="1.5"><w n="5.1">Où</w> <w n="5.2">chaque</w> <w n="5.3">rameau</w> <w n="5.4">noir</w> <w n="5.5">allonge</w> <w n="5.6">un</w> <w n="5.7">tentacule</w></l>
						<l n="6" num="1.6"><w n="6.1">Pour</w> <w n="6.2">retenir</w> <choice hand="RR" reason="analysis" type="licence"><sic rend="hidden">encore</sic><corr source="none"><w n="6.3">encor</w></corr></choice> <w n="6.4">le</w> <w n="6.5">fuyant</w> <w n="6.6">crépuscule</w> !</l>
						<l n="7" num="1.7"><w n="7.1">Tout</w> <w n="7.2">ce</w> <w n="7.3">coucher</w>, <w n="7.4">captif</w> <w n="7.5">des</w> <w n="7.6">arbres</w> <w n="7.7">frémissants</w></l>
						<l n="8" num="1.8"><w n="8.1">Je</w> <w n="8.2">le</w> <w n="8.3">posséderai</w> <w n="8.4">jusqu</w>’<w n="8.5">au</w> <w n="8.6">fond</w> <w n="8.7">de</w> <w n="8.8">mes</w> <w n="8.9">sens</w>,</l>
						<l n="9" num="1.9"><w n="9.1">Et</w> <w n="9.2">que</w> <w n="9.3">t</w>’<w n="9.4">importe</w>, <w n="9.5">alors</w>, <w n="9.6">ô</w> <w n="9.7">ma</w> <w n="9.8">soif</w> <w n="9.9">éperdue</w></l>
						<l n="10" num="1.10"><w n="10.1">De</w> <w n="10.2">vivre</w>, <w n="10.3">si</w>, <w n="10.4">dans</w> <w n="10.5">l</w>’<w n="10.6">air</w> <w n="10.7">qui</w> <w n="10.8">passe</w>, <w n="10.9">répandue</w>,</l>
						<l n="11" num="1.11"><w n="11.1">L</w>’<w n="11.2">odeur</w> <w n="11.3">morne</w> <w n="11.4">d</w>’<w n="11.5">un</w> <w n="11.6">coin</w> <w n="11.7">fraîchement</w> <w n="11.8">labouré</w></l>
						<l n="12" num="1.12"><w n="12.1">Monte</w> <w n="12.2">à</w> <w n="12.3">moi</w> <w n="12.4">tout</w> <w n="12.5">à</w> <w n="12.6">coup</w> <w n="12.7">comme</w> <w n="12.8">un</w> « <w n="12.9">dies</w> <w n="12.10">iræ</w> » ?</l>
					</lg>
				</div></body></text></TEI>