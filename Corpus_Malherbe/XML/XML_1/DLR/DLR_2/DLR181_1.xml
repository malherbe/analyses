<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">FERVEUR</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2031 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Ferveur</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">http://www.poesies.net/luciedelaruemardrueferveur.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Ferveur</title>
						<author>Lucie Delarue-Mardrus</author>
						<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k9691679h?rk=21459;2</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LA REVUE BLANCHE</publisher>
							<date when="1902">1902</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1902">1902</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">L’ÂME DES CHOSES</head><div type="poem" key="DLR181">
					<head type="main">A UNE PETITE STATUE CHALDÉO-<lb></lb>ASSYRIENNE QUE J’AI</head>
					<opener>
						<salute>Pour Séb. Ch. Leconte.</salute>
					</opener>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Toi</w> <w n="1.2">qui</w> <w n="1.3">n</w>’<w n="1.4">eus</w> <w n="1.5">qu</w>’<w n="1.6">un</w> <w n="1.7">passé</w> <w n="1.8">d</w>’<w n="1.9">idole</w> <w n="1.10">humble</w> <w n="1.11">et</w> <w n="1.12">fragile</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Voici</w> <w n="2.2">que</w>, <w n="2.3">dans</w> <w n="2.4">la</w> <w n="2.5">gaine</w> <w n="2.6">étroite</w> <w n="2.7">des</w> <w n="2.8">contours</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Nourris</w> <w n="3.2">de</w> <w n="3.3">l</w>’<w n="3.4">or</w> <w n="3.5">des</w> <w n="3.6">dieux</w> <w n="3.7">et</w> <w n="3.8">du</w> <w n="3.9">granit</w> <w n="3.10">des</w> <w n="3.11">tours</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Les</w> <w n="4.2">siècles</w> <w n="4.3">font</w> <w n="4.4">un</w> <w n="4.5">socle</w> <w n="4.6">à</w> <w n="4.7">ta</w> <w n="4.8">gloire</w> <w n="4.9">d</w>’<w n="4.10">argile</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Clouée</w> <w n="5.2">au</w> <w n="5.3">mur</w> <w n="5.4">sourit</w> <w n="5.5">ton</w> <w n="5.6">apparition</w>,</l>
						<l n="6" num="2.2"><w n="6.1">Et</w> <w n="6.2">le</w> <w n="6.3">regard</w> <w n="6.4">songeur</w> <w n="6.5">et</w> <w n="6.6">pieux</w> <w n="6.7">qui</w> <w n="6.8">t</w>’<w n="6.9">englobe</w></l>
						<l n="7" num="2.3"><w n="7.1">Croit</w> <w n="7.2">voir</w>, <w n="7.3">dans</w> <w n="7.4">les</w> <w n="7.5">pâleurs</w> <w n="7.6">de</w> <w n="7.7">l</w>’<w n="7.8">inhumation</w>,</l>
						<l n="8" num="2.4"><w n="8.1">La</w> <w n="8.2">terre</w> <w n="8.3">de</w> <w n="8.4">Chaldée</w> <w n="8.5">encore</w> <w n="8.6">sur</w> <w n="8.7">ta</w> <w n="8.8">robe</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Le</w> <w n="9.2">fantôme</w> <w n="9.3">du</w> <w n="9.4">sombre</w> <w n="9.5">et</w> <w n="9.6">royal</w> <w n="9.7">Orient</w></l>
						<l n="10" num="3.2"><w n="10.1">S</w>’<w n="10.2">est</w> <w n="10.3">roidi</w> <w n="10.4">dans</w> <w n="10.5">l</w>’<w n="10.6">horreur</w> <w n="10.7">tranquille</w> <w n="10.8">de</w> <w n="10.9">ta</w> <w n="10.10">pose</w>,</l>
						<l n="11" num="3.3"><w n="11.1">Et</w>, <w n="11.2">comme</w> <w n="11.3">en</w> <w n="11.4">ceux</w> <w n="11.5">des</w> <w n="11.6">sphinx</w> <w n="11.7">que</w> <w n="11.8">crispe</w> <w n="11.9">l</w>’<w n="11.10">ankylose</w>,</l>
						<l n="12" num="3.4"><w n="12.1">Dans</w> <w n="12.2">tes</w> <w n="12.3">yeux</w> <w n="12.4">émoussés</w> <w n="12.5">regarde</w> <w n="12.6">le</w> <w n="12.7">Néant</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Mais</w> <w n="13.2">ton</w> <w n="13.3">sourire</w> <w n="13.4">vit</w>, <w n="13.5">ô</w> <w n="13.6">roi</w>, <w n="13.7">prêtre</w> <w n="13.8">barbare</w></l>
						<l n="14" num="4.2"><w n="14.1">Des</w> <w n="14.2">longues</w> <w n="14.3">voluptés</w> <w n="14.4">du</w> <w n="14.5">passé</w> <w n="14.6">fabuleux</w> !</l>
						<l n="15" num="4.3"><w n="15.1">Babylone</w> <w n="15.2">te</w> <w n="15.3">coiffe</w> <w n="15.4">ainsi</w> <w n="15.5">qu</w>’<w n="15.6">une</w> <w n="15.7">tiare</w></l>
						<l n="16" num="4.4"><w n="16.1">Et</w> <w n="16.2">brûle</w> <w n="16.3">en</w> <w n="16.4">cette</w> <w n="16.5">moue</w> <w n="16.6">au</w> <w n="16.7">sens</w> <w n="16.8">mystérieux</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Et</w> <w n="17.2">je</w> <w n="17.3">veux</w> <w n="17.4">élever</w> <w n="17.5">mes</w> <w n="17.6">mains</w> <w n="17.7">sacerdotales</w></l>
						<l n="18" num="5.2"><w n="18.1">Dont</w> <w n="18.2">reluit</w> <w n="18.3">chaque</w> <w n="18.4">doigt</w> <w n="18.5">quadruplement</w> <w n="18.6">bagué</w>,</l>
						<l n="19" num="5.3"><w n="19.1">Pour</w> <w n="19.2">que</w> <w n="19.3">l</w>’<w n="19.4">air</w> <w n="19.5">mécréant</w> <w n="19.6">de</w> <w n="19.7">ce</w> <w n="19.8">temps</w> <w n="19.9">soit</w> <w n="19.10">nargué</w></l>
						<l n="20" num="5.4"><w n="20.1">Par</w> <w n="20.2">leur</w> <w n="20.3">chair</w> <w n="20.4">toute</w> <w n="20.5">nue</w> <w n="20.6">où</w> <w n="20.7">pèsent</w> <w n="20.8">des</w> <w n="20.9">opales</w>,</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Par</w> <w n="21.2">leur</w> <w n="21.3">chair</w> <w n="21.4">d</w>’<w n="21.5">aujourd</w>’<w n="21.6">hui</w> <w n="21.7">dont</w> <w n="21.8">le</w> <w n="21.9">geste</w> <w n="21.10">incrusté</w></l>
						<l n="22" num="6.2"><w n="22.1">S</w>’<w n="22.2">offre</w>, <w n="22.3">dans</w> <w n="22.4">la</w> <w n="22.5">douceur</w> <w n="22.6">d</w>’<w n="22.7">un</w> <w n="22.8">rite</w> <w n="22.9">énigmatique</w>,</l>
						<l n="23" num="6.3"><w n="23.1">Au</w> <w n="23.2">calme</w>, <w n="23.3">à</w> <w n="23.4">m</w>’<w n="23.5">ironie</w>, <w n="23.6">à</w> <w n="23.7">la</w> <w n="23.8">perversité</w></l>
						<l n="24" num="6.4"><w n="24.1">Que</w> <w n="24.2">retrousse</w> <w n="24.3">à</w> <w n="24.4">demi</w> <w n="24.5">ta</w> <w n="24.6">bouche</w> <w n="24.7">asiatique</w>…</l>
					</lg>
				</div></body></text></TEI>