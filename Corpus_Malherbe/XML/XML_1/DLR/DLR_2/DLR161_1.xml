<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">FERVEUR</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2031 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Ferveur</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">http://www.poesies.net/luciedelaruemardrueferveur.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Ferveur</title>
						<author>Lucie Delarue-Mardrus</author>
						<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k9691679h?rk=21459;2</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LA REVUE BLANCHE</publisher>
							<date when="1902">1902</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1902">1902</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PRONES I</head><div type="poem" key="DLR161">
					<head type="main">SUPRÉMATIE</head>
					<opener>
						<salute>A Octave Mirbeau.</salute>
					</opener>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Pour</w> <w n="1.2">le</w> <w n="1.3">désœuvrement</w> <w n="1.4">cultivé</w> <w n="1.5">des</w> <w n="1.6">cinq</w> <w n="1.7">sens</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Toute</w> <w n="2.2">une</w> <w n="2.3">humanité</w> <w n="2.4">de</w> <w n="2.5">bagne</w> <w n="2.6">sue</w> <w n="2.7">et</w> <w n="2.8">râle</w>…</l>
						<l n="3" num="1.3"><w n="3.1">Dandinons</w> <w n="3.2">nos</w> <w n="3.3">santés</w> <w n="3.4">ointes</w> <w n="3.5">d</w>’<w n="3.6">huile</w> <w n="3.7">et</w> <w n="3.8">d</w>’<w n="3.9">encens</w></l>
						<l n="4" num="1.4"><w n="4.1">Vers</w> <w n="4.2">les</w> <w n="4.3">quartiers</w> <w n="4.4">où</w> <w n="4.5">vit</w> <w n="4.6">sa</w> <w n="4.7">déchéance</w> <w n="4.8">pâle</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">La</w> <w n="5.2">robe</w> <w n="5.3">haut</w> <w n="5.4">levée</w> <w n="5.5">a</w> <w n="5.6">peur</w> <w n="5.7">des</w> <w n="5.8">détritus</w> ;</l>
						<l n="6" num="2.2"><w n="6.1">L</w>’<w n="6.2">esprit</w> <w n="6.3">pervers</w> <w n="6.4">tendu</w> <w n="6.5">vers</w> <w n="6.6">un</w> <w n="6.7">charme</w> <w n="6.8">équivoque</w></l>
						<l n="7" num="2.3"><w n="7.1">Guette</w> <w n="7.2">sournoisement</w> <w n="7.3">pour</w> <w n="7.4">en</w> <w n="7.5">saisir</w> <w n="7.6">les</w> <w n="7.7">us</w></l>
						<l n="8" num="2.4"><w n="8.1">La</w> <w n="8.2">sinistre</w> <w n="8.3">beauté</w> <w n="8.4">du</w> <w n="8.5">vice</w> <w n="8.6">et</w> <w n="8.7">de</w> <w n="8.8">la</w> <w n="8.9">loque</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Un</w> <w n="9.2">goût</w> <w n="9.3">de</w> <w n="9.4">barbarie</w> <w n="9.5">anémique</w> <w n="9.6">est</w> <w n="9.7">dans</w> <w n="9.8">l</w>’<w n="9.9">air</w> :</l>
						<l n="10" num="3.2"><w n="10.1">Seules</w> <w n="10.2">hordes</w> <w n="10.3">des</w> <w n="10.4">temps</w> <w n="10.5">présents</w> <w n="10.6">gorgés</w> <w n="10.7">d</w>’<w n="10.8">absinthe</w>,</l>
						<l n="11" num="3.3"><w n="11.1">Morne</w> <w n="11.2">argot</w> <w n="11.3">pourrissant</w> <w n="11.4">le</w> <w n="11.5">dialecte</w> <w n="11.6">clair</w>,</l>
						<l n="12" num="3.4"><w n="12.1">Brutes</w> <w n="12.2">sans</w> <w n="12.3">innocence</w> <w n="12.4">et</w> <w n="12.5">que</w> <w n="12.6">la</w> <w n="12.7">vie</w> <w n="12.8">éreinte</w>,</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Leur</w> <w n="13.2">destin</w> <w n="13.3">de</w> <w n="13.4">laideur</w>, <w n="13.5">d</w>’<w n="13.6">esclavage</w> <w n="13.7">et</w> <w n="13.8">de</w> <w n="13.9">maux</w></l>
						<l n="14" num="4.2"><w n="14.1">Tatoue</w> <w n="14.2">aux</w> <w n="14.3">quatre</w> <w n="14.4">coins</w> <w n="14.5">leur</w> <w n="14.6">pâleur</w> <w n="14.7">faciale</w>,</l>
						<l n="15" num="4.3"><w n="15.1">Et</w> <w n="15.2">cette</w> <w n="15.3">horreur</w>, <w n="15.4">déjà</w> <w n="15.5">transmise</w> <w n="15.6">à</w> <w n="15.7">leurs</w> <w n="15.8">marmots</w>,</l>
						<l n="16" num="4.4"><w n="16.1">En</w> <w n="16.2">pleine</w> <w n="16.3">gourme</w> <w n="16.4">inscrit</w> <w n="16.5">sa</w> <w n="16.6">lettre</w> <w n="16.7">sociale</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Or</w>, <w n="17.2">songeons</w> <w n="17.3">que</w> <w n="17.4">ce</w> <w n="17.5">sont</w> <w n="17.6">ceux</w>-<w n="17.7">là</w> <w n="17.8">qui</w> <w n="17.9">remueront</w></l>
						<l n="18" num="5.2"><w n="18.1">Silencieusement</w> <w n="18.2">des</w> <w n="18.3">masses</w> <w n="18.4">et</w> <w n="18.5">des</w> <w n="18.6">masses</w>,</l>
						<l n="19" num="5.3"><w n="19.1">Quand</w> <w n="19.2">un</w> <w n="19.3">souffle</w> <w n="19.4">assez</w> <w n="19.5">fort</w> <w n="19.6">emplira</w> <w n="19.7">le</w> <w n="19.8">clairon</w></l>
						<l n="20" num="5.4"><w n="20.1">Qui</w> <w n="20.2">saura</w> <w n="20.3">secouer</w> <w n="20.4">les</w> <w n="20.5">haines</w> <w n="20.6">et</w> <w n="20.7">les</w> <w n="20.8">crasses</w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Alors</w>, <w n="21.2">tel</w> <w n="21.3">un</w> <w n="21.4">terrain</w> <w n="21.5">inconnu</w> <w n="21.6">d</w>’<w n="21.7">en</w> <w n="21.8">dessous</w></l>
						<l n="22" num="6.2"><w n="22.1">Lèverait</w> <w n="22.2">sur</w> <w n="22.3">les</w> <w n="22.4">fleurs</w> <w n="22.5">ses</w> <w n="22.6">mottes</w> <w n="22.7">assassines</w>,</l>
						<l n="23" num="6.3"><w n="23.1">Tous</w> <w n="23.2">courront</w>, <w n="23.3">danseront</w>, <w n="23.4">gueuleront</w> <w n="23.5">contre</w> <w n="23.6">nous</w></l>
					</lg>
					<lg n="7">
						<l n="24" num="7.1"><w n="24.1">Avec</w> <w n="24.2">le</w> <w n="24.3">bon</w> <w n="24.4">droit</w> <w n="24.5">plein</w> <w n="24.6">leurs</w> <w n="24.7">poings</w> <w n="24.8">et</w> <w n="24.9">leurs</w> <w n="24.10">poitrines</w>.</l>
					</lg>
					<lg n="8">
						<l n="25" num="8.1"><w n="25.1">Mais</w> <w n="25.2">si</w> <w n="25.3">nos</w> <w n="25.4">doigts</w> <w n="25.5">dorés</w> <w n="25.6">craquent</w> <w n="25.7">sous</w> <w n="25.8">leurs</w> <w n="25.9">calus</w>,</l>
						<l n="26" num="8.2"><w n="26.1">Dans</w> <w n="26.2">leur</w> <w n="26.3">regard</w> <w n="26.4">vitreux</w> <w n="26.5">où</w> <w n="26.6">la</w> <w n="26.7">bête</w> <w n="26.8">se</w> <w n="26.9">vautre</w></l>
						<l n="27" num="8.3"><w n="27.1">Nous</w> <w n="27.2">surprendrons</w> <w n="27.3">leur</w> <w n="27.4">rêve</w> <w n="27.5">avec</w> <w n="27.6">nos</w> <w n="27.7">yeux</w> <w n="27.8">d</w>’<w n="27.9">élus</w>,</l>
						<l n="28" num="8.4"><w n="28.1">Tandis</w> <w n="28.2">qu</w>’<w n="28.3">eux</w>, <w n="28.4">à</w> <w n="28.5">jamais</w>, <w n="28.6">ignoreront</w> <w n="28.7">le</w> <w n="28.8">nôtre</w>…</l>
					</lg>
				</div></body></text></TEI>