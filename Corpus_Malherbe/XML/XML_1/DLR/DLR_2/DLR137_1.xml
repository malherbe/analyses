<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">FERVEUR</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2031 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Ferveur</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">http://www.poesies.net/luciedelaruemardrueferveur.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Ferveur</title>
						<author>Lucie Delarue-Mardrus</author>
						<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k9691679h?rk=21459;2</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LA REVUE BLANCHE</publisher>
							<date when="1902">1902</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1902">1902</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">POÈMES TERRESTRES</head><div type="poem" key="DLR137">
					<head type="main">AU CRÉPUSCULE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Quoique</w> <w n="1.2">les</w> <w n="1.3">branches</w> <w n="1.4">soient</w> <w n="1.5">encore</w> <w n="1.6">toutes</w> <w n="1.7">nues</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Le</w> <w n="2.2">printemps</w> <w n="2.3">est</w> <w n="2.4">déjà</w> <w n="2.5">dans</w> <w n="2.6">l</w>’<w n="2.7">air</w> <w n="2.8">des</w> <w n="2.9">avenues</w>.</l>
					</lg>
					<lg n="2">
						<l n="3" num="2.1"><w n="3.1">Pas</w> <w n="3.2">une</w> <w n="3.3">fleur</w> <w n="3.4">n</w>’<w n="3.5">émeut</w> <w n="3.6">les</w> <w n="3.7">gazons</w> <w n="3.8">des</w> <w n="3.9">talus</w>,</l>
						<l n="4" num="2.2"><w n="4.1">Les</w> <w n="4.2">bois</w> <w n="4.3">sont</w> <w n="4.4">durs</w> <w n="4.5">et</w> <w n="4.6">noirs</w> ; <w n="4.7">pourtant</w> <w n="4.8">l</w>’<w n="4.9">hiver</w> <w n="4.10">n</w>’<w n="4.11">est</w> <w n="4.12">plus</w>.</l>
					</lg>
					<lg n="3">
						<l n="5" num="3.1"><w n="5.1">Prenons</w>-<w n="5.2">nous</w> <w n="5.3">par</w> <w n="5.4">la</w> <w n="5.5">main</w> <w n="5.6">et</w> <w n="5.7">marchons</w> <w n="5.8">sans</w> <w n="5.9">secousse</w>.</l>
						<l n="6" num="3.2"><w n="6.1">Notre</w> <w n="6.2">cœur</w> <w n="6.3">sans</w> <w n="6.4">parole</w> <w n="6.5">est</w> <w n="6.6">triste</w>, <w n="6.7">et</w> <w n="6.8">l</w>’<w n="6.9">heure</w> <w n="6.10">est</w> <w n="6.11">douce</w>.</l>
					</lg>
					<lg n="4">
						<l n="7" num="4.1"><w n="7.1">Aimons</w>, <w n="7.2">dans</w> <w n="7.3">le</w> <w n="7.4">silence</w> <w n="7.5">où</w> <w n="7.6">s</w>’<w n="7.7">étouffent</w> <w n="7.8">nos</w> <w n="7.9">pas</w>,</l>
						<l n="8" num="4.2"><w n="8.1">La</w> <w n="8.2">beauté</w> <w n="8.3">des</w> <w n="8.4">couchants</w> <w n="8.5">qui</w> <w n="8.6">ne</w> <w n="8.7">nous</w> <w n="8.8">trompe</w> <w n="8.9">pas</w>.</l>
					</lg>
					<lg n="5">
						<l n="9" num="5.1"><w n="9.1">Goûtons</w> <w n="9.2">tout</w> <w n="9.3">le</w> <w n="9.4">bonheur</w> <w n="9.5">de</w> <w n="9.6">notre</w> <w n="9.7">solitude</w> :</l>
						<l n="10" num="5.2"><w n="10.1">Une</w> <w n="10.2">voix</w> <w n="10.3">bercera</w> <w n="10.4">cette</w> <w n="10.5">béatitude</w> ;</l>
					</lg>
					<lg n="6">
						<l n="11" num="6.1"><w n="11.1">Car</w> <w n="11.2">un</w> <w n="11.3">pur</w> <w n="11.4">rossignol</w> <w n="11.5">que</w> <w n="11.6">nous</w> <w n="11.7">ne</w> <w n="11.8">pouvons</w> <w n="11.9">voir</w></l>
						<l n="12" num="6.2"><w n="12.1">Chante</w> <w n="12.2">à</w> <w n="12.3">mourir</w> <w n="12.4">au</w> <w n="12.5">fond</w> <w n="12.6">des</w> <w n="12.7">arbres</w> <w n="12.8">et</w> <w n="12.9">du</w> <w n="12.10">soir</w>,</l>
					</lg>
					<lg n="7">
						<l n="13" num="7.1"><w n="13.1">Et</w> <w n="13.2">sa</w> <w n="13.3">grande</w> <w n="13.4">âme</w>, <w n="13.5">vers</w> <w n="13.6">la</w> <w n="13.7">lune</w> <w n="13.8">qui</w> <w n="13.9">se</w> <w n="13.10">lève</w>,</l>
						<l n="14" num="7.2"><w n="14.1">Monte</w> <w n="14.2">de</w> <w n="14.3">ses</w> <w n="14.4">poumons</w> <w n="14.5">plus</w> <w n="14.6">menus</w> <w n="14.7">qu</w>’<w n="14.8">une</w> <w n="14.9">fève</w>…</l>
					</lg>
				</div></body></text></TEI>