<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">FERVEUR</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2031 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Ferveur</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">http://www.poesies.net/luciedelaruemardrueferveur.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Ferveur</title>
						<author>Lucie Delarue-Mardrus</author>
						<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k9691679h?rk=21459;2</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LA REVUE BLANCHE</publisher>
							<date when="1902">1902</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1902">1902</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PRONES II</head><div type="poem" key="DLR170">
					<head type="main">LE POÈME DE L’ÉTERNELLE ÉGLISE</head>
					<opener>
						<salute>A Raymond Berger.</salute>
					</opener>
					<div type="section" n="1">
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">En</w> <w n="1.2">écoutant</w> <w n="1.3">la</w> <w n="1.4">Ville</w>, <w n="1.5">ayant</w> <w n="1.6">fermé</w> <w n="1.7">les</w> <w n="1.8">yeux</w>,</l>
							<l n="2" num="1.2"><w n="2.1">J</w>’<w n="2.2">ai</w> <w n="2.3">vu</w>, <w n="2.4">dans</w> <w n="2.5">la</w> <w n="2.6">ténèbre</w> <w n="2.7">intime</w> <w n="2.8">des</w> <w n="2.9">paupières</w>,</l>
							<l n="3" num="1.3"><w n="3.1">Mes</w> <w n="3.2">rougeurs</w> <w n="3.3">d</w>’<w n="3.4">incendie</w> <w n="3.5">et</w> <w n="3.6">les</w> <w n="3.7">chutes</w> <w n="3.8">de</w> <w n="3.9">pierres</w></l>
							<l n="4" num="1.4"><w n="4.1">Du</w> <w n="4.2">Demain</w> <w n="4.3">préparé</w> <w n="4.4">par</w> <w n="4.5">d</w>’<w n="4.6">inouïs</w> <w n="4.7">aïeux</w>.</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1"><w n="5.1">Le</w> <w n="5.2">sol</w> <w n="5.3">crevé</w> <w n="5.4">tremblait</w> <w n="5.5">sous</w> <w n="5.6">les</w> <w n="5.7">hordes</w> <w n="5.8">carrées</w></l>
							<l n="6" num="2.2"><w n="6.1">Des</w> <w n="6.2">esclaves</w> <w n="6.3">d</w>’<w n="6.4">hier</w>, <w n="6.5">blêmes</w> <w n="6.6">de</w> <w n="6.7">passion</w>,</l>
							<l n="7" num="2.3"><w n="7.1">Levant</w> <w n="7.2">cent</w> <w n="7.3">mille</w> <w n="7.4">bras</w> <w n="7.5">vers</w> <w n="7.6">la</w> <w n="7.7">destruction</w>,</l>
							<l n="8" num="2.4"><w n="8.1">Et</w> <w n="8.2">gonflant</w> <w n="8.3">d</w>’<w n="8.4">hymnes</w> <w n="8.5">leurs</w> <w n="8.6">poitrines</w> <w n="8.7">libérées</w>.</l>
						</lg>
						<lg n="3">
							<l n="9" num="3.1"><w n="9.1">Et</w> <w n="9.2">le</w> <w n="9.3">rire</w>, <w n="9.4">tragique</w> <w n="9.5">et</w> <w n="9.6">fou</w> <w n="9.7">comme</w> <w n="9.8">un</w> <w n="9.9">sanglot</w>,</l>
							<l n="10" num="3.2"><w n="10.1">Y</w> <w n="10.2">secouait</w> <w n="10.3">les</w> <w n="10.4">seins</w> <w n="10.5">ivres</w> <w n="10.6">des</w> <w n="10.7">filles</w> <w n="10.8">folles</w></l>
							<l n="11" num="3.3"><w n="11.1">Qui</w>, <w n="11.2">béantes</w>, <w n="11.3">hurlaient</w> <w n="11.4">aussi</w> <w n="11.5">les</w> <w n="11.6">carmagnoles</w>,</l>
							<l n="12" num="3.4"><w n="12.1">Dont</w> <w n="12.2">se</w> <w n="12.3">rythmait</w> <w n="12.4">au</w> <w n="12.5">vent</w> <w n="12.6">le</w> <w n="12.7">sinistre</w> <w n="12.8">galop</w>.</l>
						</lg>
						<lg n="4">
							<l n="13" num="4.1"><w n="13.1">Mais</w> <w n="13.2">un</w> <w n="13.3">grand</w> <w n="13.4">rêve</w>, <w n="13.5">issu</w> <w n="13.6">de</w> <w n="13.7">cette</w> <w n="13.8">immense</w> <w n="13.9">crise</w>,</l>
							<l n="14" num="4.2"><w n="14.1">Dans</w> <w n="14.2">le</w> <w n="14.3">ciel</w> <w n="14.4">clair</w> <w n="14.5">de</w> <w n="14.6">l</w>’<w n="14.7">ordre</w> <w n="14.8">et</w> <w n="14.9">de</w> <w n="14.10">la</w> <w n="14.11">liberté</w></l>
							<l n="15" num="4.3"><w n="15.1">Faisait</w> <w n="15.2">déjà</w> <w n="15.3">monter</w> <w n="15.4">de</w> <w n="15.5">terre</w> <w n="15.6">une</w> <w n="15.7">cité</w></l>
							<l n="16" num="4.4"><w n="16.1">Virginale</w>, <w n="16.2">sans</w> <w n="16.3">Tribunal</w> <w n="16.4">et</w> <w n="16.5">sans</w> <w n="16.6">Église</w>.</l>
						</lg>
						<lg n="5">
							<l n="17" num="5.1"><w n="17.1">Et</w> <w n="17.2">si</w> <w n="17.3">l</w>’<w n="17.4">on</w> <w n="17.5">se</w> <w n="17.6">heurtait</w> <w n="17.7">des</w> <w n="17.8">pieds</w> <w n="17.9">à</w> <w n="17.10">quelque</w> <w n="17.11">bloc</w>,</l>
							<l n="18" num="5.2">‒ <w n="18.1">Survivant</w> <w n="18.2">oublié</w> <w n="18.3">de</w> <w n="18.4">l</w>’<w n="18.5">ancienne</w> <w n="18.6">pensée</w>, ‒</l>
							<l n="19" num="5.3"><w n="19.1">La</w> <w n="19.2">cohue</w> <w n="19.3">achevait</w> <w n="19.4">ce</w> <w n="19.5">vestige</w>, <w n="19.6">pressée</w></l>
							<l n="20" num="5.4"><w n="20.1">Et</w> <w n="20.2">dure</w>, <w n="20.3">et</w> <w n="20.4">poursuivant</w> <w n="20.5">son</w> <w n="20.6">sillon</w> <w n="20.7">comme</w> <w n="20.8">un</w> <w n="20.9">soc</w>.</l>
						</lg>
						<lg n="6">
							<l n="21" num="6.1">… <w n="21.1">Soudain</w>, <w n="21.2">rompant</w> <w n="21.3">l</w>’<w n="21.4">assaut</w> <w n="21.5">de</w> <w n="21.6">la</w> <w n="21.7">ruine</w> <w n="21.8">grise</w>,</l>
							<l n="22" num="6.2"><w n="22.1">Le</w> <w n="22.2">rauque</w> <w n="22.3">bataillon</w> <w n="22.4">recule</w> <w n="22.5">et</w> <w n="22.6">reste</w> <w n="22.7">coi</w>,</l>
							<l n="23" num="6.3"><w n="23.1">Écumant</w> <w n="23.2">de</w> <w n="23.3">silence</w> <w n="23.4">et</w> <w n="23.5">ne</w> <w n="23.6">sachant</w> <w n="23.7">pourquoi</w></l>
							<l n="24" num="6.4"><w n="24.1">Un</w> <w n="24.2">simple</w> <w n="24.3">mendiant</w> <w n="24.4">surgi</w> <w n="24.5">l</w>’<w n="24.6">immobilise</w>.</l>
						</lg>
						<lg n="7">
							<l n="25" num="7.1"><w n="25.1">Un</w> <w n="25.2">soleil</w> <w n="25.3">ignoré</w> <w n="25.4">brûle</w> <w n="25.5">dans</w> <w n="25.6">ses</w> <w n="25.7">cheveux</w>,</l>
							<l n="26" num="7.2"><w n="26.1">Éclairant</w> <w n="26.2">dans</w> <w n="26.3">le</w> <w n="26.4">soir</w> <w n="26.5">la</w> <w n="26.6">pierre</w> <w n="26.7">à</w> <w n="26.8">l</w>’<w n="26.9">agonie</w>,</l>
							<l n="27" num="7.3"><w n="27.1">Et</w>, <w n="27.2">géniale</w>, <w n="27.3">bleue</w> <w n="27.4">et</w> <w n="27.5">fixe</w>, <w n="27.6">l</w>’<w n="27.7">ironie</w></l>
							<l n="28" num="7.4"><w n="28.1">Tombe</w> <w n="28.2">des</w> <w n="28.3">calmes</w> <w n="28.4">cils</w> <w n="28.5">du</w> <w n="28.6">pauvre</w> <w n="28.7">lumineux</w>.</l>
						</lg>
						<lg n="8">
							<l n="29" num="8.1"><w n="29.1">Il</w> <w n="29.2">va</w> <w n="29.3">parler</w>. <w n="29.4">Il</w> <w n="29.5">meut</w> <w n="29.6">sa</w> <w n="29.7">grâce</w> <w n="29.8">solennelle</w> ;</l>
							<l n="30" num="8.2"><w n="30.1">Et</w> <w n="30.2">la</w> <w n="30.3">foule</w>, <w n="30.4">devant</w> <w n="30.5">ce</w> <w n="30.6">fragile</w> <w n="30.7">rival</w>,</l>
							<l n="31" num="8.3"><w n="31.1">Obéit</w> <w n="31.2">toute</w>, <w n="31.3">avec</w> <w n="31.4">des</w> <w n="31.5">regards</w> <w n="31.6">d</w>’<w n="31.7">animal</w>,</l>
							<l n="32" num="8.4"><w n="32.1">A</w> <w n="32.2">l</w>’<w n="32.3">ordre</w> <w n="32.4">de</w> <w n="32.5">l</w>’<w n="32.6">index</w> <w n="32.7">qui</w> <w n="32.8">s</w>’<w n="32.9">est</w> <w n="32.10">levé</w> <w n="32.11">sur</w> <w n="32.12">elle</w>.</l>
						</lg>
					</div>
					<div type="section" n="2">
						<head type="main">LE PAUVRE</head>
						<lg n="1">
							<l n="33" num="1.1"><w n="33.1">La</w> <w n="33.2">paix</w> <w n="33.3">sur</w> <w n="33.4">vous</w> !… <w n="33.5">Comment</w> <w n="33.6">pouvez</w>-<w n="33.7">vous</w> <w n="33.8">croire</w> <w n="33.9">à</w> <w n="33.10">bas</w></l>
							<l n="34" num="1.2"><w n="34.1">L</w>’<w n="34.2">Église</w> ?… <w n="34.3">Vous</w> <w n="34.4">dansez</w> <w n="34.5">sur</w> <w n="34.6">la</w> <w n="34.7">tour</w> <w n="34.8">renversée</w>,</l>
							<l n="35" num="1.3"><w n="35.1">Ignorant</w> <w n="35.2">que</w> <w n="35.3">le</w> <w n="35.4">marbre</w> <w n="35.5">meurt</w>, <w n="35.6">non</w> <w n="35.7">la</w> <w n="35.8">Pensée</w> ;</l>
							<l n="36" num="1.4"><w n="36.1">Mais</w> <w n="36.2">l</w>’<w n="36.3">Église</w> <w n="36.4">est</w>, <w n="36.5">parmi</w> <w n="36.6">le</w> <w n="36.7">sang</w> <w n="36.8">et</w> <w n="36.9">les</w> <w n="36.10">dégâts</w>,</l>
							<l n="37" num="1.5"><w n="37.1">Tout</w> <w n="37.2">debout</w> ! <w n="37.3">Et</w> <w n="37.4">jamais</w> <w n="37.5">elle</w> <w n="37.6">ne</w> <w n="37.7">fut</w> <w n="37.8">plus</w> <w n="37.9">fière</w> !</l>
						</lg>
						<lg n="2">
							<l n="38" num="2.1"><w n="38.1">Pourquoi</w> <w n="38.2">vomissez</w>-<w n="38.3">vous</w>, <w n="38.4">de</w> <w n="38.5">haine</w>, <w n="38.6">une</w> <w n="38.7">chanson</w> ?</l>
							<l n="39" num="2.2"><w n="39.1">Ne</w> <w n="39.2">voyez</w>-<w n="39.3">vous</w> <w n="39.4">grandir</w> <w n="39.5">sa</w> <w n="39.6">force</w> <w n="39.7">pierre</w> <w n="39.8">à</w> <w n="39.9">pierre</w> ?…</l>
							<l n="40" num="2.3"><w n="40.1">O</w> <w n="40.2">vous</w> <w n="40.3">tous</w> ! <w n="40.4">l</w>’<w n="40.5">heure</w> <w n="40.6">vient</w> <w n="40.7">de</w> <w n="40.8">couper</w> <w n="40.9">la</w> <w n="40.10">moisson</w>,</l>
							<l n="41" num="2.4"><w n="41.1">L</w>’<w n="41.2">heure</w> <w n="41.3">vient</w> <w n="41.4">de</w> <w n="41.5">compter</w> <w n="41.6">les</w> <w n="41.7">pierres</w> <w n="41.8">angulaires</w> !</l>
						</lg>
					</div>
					<div type="section" n="3">
						<head type="main">LA FOULE</head>
						<lg n="1">
							<l n="42" num="1.1"><w n="42.1">Nous</w> <w n="42.2">ne</w> <w n="42.3">comprenons</w> <w n="42.4">point</w> <w n="42.5">ce</w> <w n="42.6">que</w> <w n="42.7">fixent</w> <w n="42.8">là</w>-<w n="42.9">bas</w>,</l>
							<l n="43" num="1.2"><w n="43.1">Plus</w> <w n="43.2">loin</w> <w n="43.3">que</w> <w n="43.4">tous</w> <w n="43.5">nos</w> <w n="43.6">yeux</w> <w n="43.7">tes</w> <w n="43.8">étranges</w> <w n="43.9">prunelles</w>.</l>
							<l n="44" num="1.3"><w n="44.1">Mais</w> <w n="44.2">apprends</w>-<w n="44.3">nous</w> <w n="44.4">où</w> <w n="44.5">sont</w>, <w n="44.6">attendant</w> <w n="44.7">les</w> <w n="44.8">truelles</w>,</l>
							<l n="45" num="1.4"><w n="45.1">Ces</w> <w n="45.2">pierres</w> <w n="45.3">que</w> <w n="45.4">tu</w> <w n="45.5">dis</w> <w n="45.6">et</w> <w n="45.7">que</w> <w n="45.8">l</w>’<w n="45.9">on</w> <w n="45.10">ne</w> <w n="45.11">voit</w> <w n="45.12">pas</w>.</l>
						</lg>
					</div>
					<div type="section" n="4">
						<head type="main">LE PAUVRE</head>
						<lg n="1">
							<l part="I" n="46" num="1.1"><w n="46.1">Dans</w> <w n="46.2">vos</w> <w n="46.3">poitrines</w> !… </l>
							<l part="F" n="46" num="1.1"><w n="46.4">Cœur</w> <w n="46.5">sanglant</w>, <w n="46.6">ô</w> <w n="46.7">cœur</w> <w n="46.8">de</w> <w n="46.9">l</w>’<w n="46.10">homme</w> !</l>
							<l n="47" num="1.2"><w n="47.1">Gouffre</w> <w n="47.2">que</w> <w n="47.3">n</w>’<w n="47.4">emplit</w> <w n="47.5">pas</w> <w n="47.6">l</w>’<w n="47.7">océan</w> <w n="47.8">qu</w>’<w n="47.9">on</w> <w n="47.10">te</w> <w n="47.11">doit</w>,</l>
							<l n="48" num="1.3"><w n="48.1">Cœur</w> <w n="48.2">intact</w> <w n="48.3">sous</w> <w n="48.4">le</w> <w n="48.5">poids</w> <w n="48.6">de</w> <w n="48.7">tout</w> <w n="48.8">ce</w> <w n="48.9">qui</w> <w n="48.10">t</w>’<w n="48.11">assomme</w>,</l>
							<l n="49" num="1.4"><w n="49.1">O</w> <w n="49.2">cœur</w>, <w n="49.3">écoute</w>-<w n="49.4">moi</w> <w n="49.5">quand</w> <w n="49.6">je</w> <w n="49.7">te</w> <w n="49.8">touche</w> <w n="49.9">au</w> <w n="49.10">doigt</w>,</l>
							<l n="50" num="1.5"><w n="50.1">Quand</w> <w n="50.2">je</w> <w n="50.3">te</w> <w n="50.4">dis</w> : « <w n="50.5">Tu</w> <w n="50.6">es</w> <w n="50.7">pierre</w> <w n="50.8">et</w> <w n="50.9">sur</w> <w n="50.10">cette</w> <w n="50.11">pierre</w></l>
							<l part="I" n="51" num="1.6"><w n="51.1">J</w>’<w n="51.2">ai</w> <w n="51.3">bâti</w> <w n="51.4">mon</w> <w n="51.5">Église</w> <w n="51.6">à</w> <w n="51.7">jamais</w> ! » </l>
						</lg>
					</div>
					<div type="section" n="5">
						<head type="main">LA FOULE</head>
						<lg n="1">
							<l part="F" n="51"><w n="51.8">Qui</w> <w n="51.9">es</w>-<w n="51.10">tu</w>,</l>
							<l n="52" num="1.1"><w n="52.1">Toi</w> <w n="52.2">qui</w> <w n="52.3">redis</w> <w n="52.4">les</w> <w n="52.5">mots</w> <w n="52.6">d</w>’<w n="52.7">exécrable</w> <w n="52.8">vertu</w></l>
							<l n="53" num="1.2"><w n="53.1">Du</w> <w n="53.2">denier</w> <w n="53.3">dieu</w> <w n="53.4">que</w> <w n="53.5">piétina</w> <w n="53.6">notre</w> <w n="53.7">colère</w> ?</l>
							<l n="54" num="1.3"><w n="54.1">Avant</w> <w n="54.2">de</w> <w n="54.3">t</w>’<w n="54.4">en</w> <w n="54.5">aller</w> <w n="54.6">en</w> <w n="54.7">lambeaux</w> <w n="54.8">retrouver</w></l>
							<l n="55" num="1.4"><w n="55.1">Les</w> <w n="55.2">prêtres</w> <w n="55.3">abolis</w> <w n="55.4">de</w> <w n="55.5">Christ</w> <w n="55.6">ou</w> <w n="55.7">d</w>’<w n="55.8">Iaveh</w>,</l>
							<l n="56" num="1.5"><w n="56.1">Expire</w> <w n="56.2">donc</w> <w n="56.3">d</w>’<w n="56.4">abord</w> <w n="56.5">de</w> <w n="56.6">honte</w> <w n="56.7">pour</w> <w n="56.8">tes</w> <w n="56.9">dires</w>,</l>
							<l n="57" num="1.6"><w n="57.1">Submergé</w> <w n="57.2">par</w> <w n="57.3">le</w> <w n="57.4">fleuve</w> <w n="57.5">énorme</w> <w n="57.6">de</w> <w n="57.7">nos</w> <w n="57.8">rires</w> !</l>
						</lg>
					</div>
					<div type="section" n="6">
						<head type="main">LE PAUVRE</head>
						<lg n="1">
							<l n="58" num="1.1"><w n="58.1">Arrière</w> !… <w n="58.2">Je</w> <w n="58.3">connais</w> <w n="58.4">déjà</w> <w n="58.5">votre</w> <w n="58.6">gaîté</w> :</l>
							<l n="59" num="1.2"><w n="59.1">Elle</w> <w n="59.2">vêtit</w> <w n="59.3">mon</w> <w n="59.4">corps</w> <w n="59.5">et</w> <w n="59.6">couronna</w> <w n="59.7">ma</w> <w n="59.8">tête</w>,</l>
							<l n="60" num="1.3"><w n="60.1">Et</w> <w n="60.2">je</w> <w n="60.3">suis</w> <w n="60.4">pâle</w> <w n="60.5">encor</w> <w n="60.6">du</w> <w n="60.7">trépas</w> <w n="60.8">insulté</w>.</l>
							<l n="61" num="1.4"><w n="61.1">Mais</w> <w n="61.2">aujourd</w>’<w n="61.3">hui</w> <w n="61.4">mon</w> <w n="61.5">règne</w> <w n="61.6">arrive</w>, <w n="61.7">c</w>’<w n="61.8">est</w> <w n="61.9">ma</w> <w n="61.10">fête</w></l>
							<l n="62" num="1.5"><w n="62.1">Plus</w> <w n="62.2">qu</w>’<w n="62.3">à</w> <w n="62.4">Jérusalem</w> <w n="62.5">tout</w> <w n="62.6">en</w> <w n="62.7">palmes</w> ! <w n="62.8">Le</w> <w n="62.9">jour</w></l>
							<l n="63" num="1.6"><w n="63.1">Se</w> <w n="63.2">lève</w> ! <w n="63.3">C</w>’<w n="63.4">est</w> <w n="63.5">l</w>’<w n="63.6">aurore</w> <w n="63.7">en</w> <w n="63.8">flammes</w> <w n="63.9">de</w> <w n="63.10">l</w>’<w n="63.11">Amour</w> !…</l>
						</lg>
					</div>
					<div type="section" n="7">
						<head type="main">LA FOULE</head>
						<lg n="1">
							<l n="64" num="1.1"><w n="64.1">O</w> <w n="64.2">folie</w> ! <w n="64.3">O</w> <w n="64.4">dégoût</w> <w n="64.5">des</w> <w n="64.6">anciennes</w> <w n="64.7">nausées</w> !</l>
							<l n="65" num="1.2"><w n="65.1">Qu</w>’<w n="65.2">il</w> <w n="65.3">meure</w> <w n="65.4">sous</w> <w n="65.5">les</w> <w n="65.6">coups</w>, <w n="65.7">la</w> <w n="65.8">haine</w> <w n="65.9">et</w> <w n="65.10">les</w> <w n="65.11">risées</w> :</l>
							<l n="66" num="1.3"><w n="66.1">C</w>’<w n="66.2">est</w> <w n="66.3">le</w> <w n="66.4">Christ</w> ! <w n="66.5">C</w>’<w n="66.6">est</w> <w n="66.7">le</w> <w n="66.8">Christ</w> <w n="66.9">lui</w>-<w n="66.10">même</w>… <w n="66.11">C</w>’<w n="66.12">est</w> <w n="66.13">Jésus</w> !…</l>
						</lg>
					</div>
					<div type="section" n="8">
						<head type="main">LE PAUVRE</head>
						<lg n="1">
							<l part="I" n="67" num="1.1"><w n="67.1">Vous</w> <w n="67.2">l</w>’<w n="67.3">avez</w> <w n="67.4">dit</w> ! </l>
						</lg>
					</div>
					<div type="section" n="9">
						<head type="main">LA FOULE</head>
						<lg n="1">
							<l part="M" n="67"><w n="67.5">A</w> <w n="67.6">mort</w> ! </l>
						</lg>
					</div>
					<div type="section" n="10">
						<head type="main">LE PAUVRE</head>
						<lg n="1">
							<l part="F" n="67"><w n="67.7">Faudra</w>-<w n="67.8">t</w>-<w n="67.9">il</w> <w n="67.10">que</w> <w n="67.11">je</w> <w n="67.12">meure</w></l>
							<l n="68" num="1.1"><w n="68.1">Alors</w> <w n="68.2">que</w> <w n="68.3">sont</w> <w n="68.4">venus</w> <w n="68.5">ceux</w> <w n="68.6">de</w> <w n="68.7">la</w> <w n="68.8">dernière</w> <w n="68.9">heure</w>,</l>
							<l n="69" num="1.2"><w n="69.1">Virginaux</w>, <w n="69.2">dépouillés</w> <w n="69.3">des</w> <w n="69.4">rites</w> <w n="69.5">et</w> <w n="69.6">des</w> <w n="69.7">us</w> ?</l>
							<l n="70" num="1.3"><w n="70.1">Quand</w> <w n="70.2">voici</w> <w n="70.3">mûre</w> <w n="70.4">enfin</w> <w n="70.5">ma</w> <w n="70.6">récolte</w> <w n="70.7">tardive</w> ?…</l>
							<l n="71" num="1.4"><w n="71.1">O</w> <w n="71.2">mes</w> <w n="71.3">enfants</w>, <w n="71.4">je</w> <w n="71.5">vous</w> <w n="71.6">le</w> <w n="71.7">dis</w>, <w n="71.8">mon</w> <w n="71.9">règne</w> <w n="71.10">arrive</w> !</l>
							<l n="72" num="1.5"><w n="72.1">Car</w> <w n="72.2">voyez</w>-<w n="72.3">moi</w> : <w n="72.4">debout</w> <w n="72.5">dans</w> <w n="72.6">mon</w> <w n="72.7">simple</w> <w n="72.8">haillon</w>,</l>
							<l n="73" num="1.6"><w n="73.1">Ouvrant</w> <w n="73.2">au</w> <w n="73.3">ciel</w> <w n="73.4">du</w> <w n="73.5">soir</w> <w n="73.6">ma</w> <w n="73.7">bouche</w> <w n="73.8">sans</w> <w n="73.9">bâillon</w>,</l>
							<l n="74" num="1.7"><w n="74.1">En</w> <w n="74.2">une</w> <w n="74.3">liberté</w> <w n="74.4">d</w>’<w n="74.5">épaules</w>, <w n="74.6">hors</w> <w n="74.7">la</w> <w n="74.8">cangue</w></l>
							<l n="75" num="1.8"><w n="75.1">Des</w> <w n="75.2">manteaux</w> <w n="75.3">d</w>’<w n="75.4">or</w>, <w n="75.5">je</w> <w n="75.6">mêle</w> <w n="75.7">au</w> <w n="75.8">vent</w> <w n="75.9">cette</w> <w n="75.10">harangue</w> !</l>
							<l n="76" num="1.9"><w n="76.1">Ah</w> ! <w n="76.2">c</w>’<w n="76.3">est</w> <w n="76.4">la</w> <w n="76.5">fin</w>, <w n="76.6">ce</w> <w n="76.7">soir</w>, <w n="76.8">des</w> <w n="76.9">efforts</w> <w n="76.10">impuissants</w> !</l>
							<l n="77" num="1.10"><w n="77.1">Je</w> <w n="77.2">sens</w> <w n="77.3">que</w> <w n="77.4">le</w> <w n="77.5">plein</w> <w n="77.6">air</w> <w n="77.7">me</w> <w n="77.8">lave</w> <w n="77.9">des</w> <w n="77.10">encens</w>,</l>
							<l n="78" num="1.11"><w n="78.1">Des</w> <w n="78.2">cires</w>, <w n="78.3">des</w> <w n="78.4">foisons</w> <w n="78.5">de</w> <w n="78.6">lis</w>, <w n="78.7">des</w> <w n="78.8">girandoles</w>,</l>
							<l n="79" num="1.12"><w n="79.1">Et</w> <w n="79.2">de</w> <w n="79.3">tout</w> <w n="79.4">aliment</w> <w n="79.5">de</w> <w n="79.6">l</w>’<w n="79.7">orgueil</w> <w n="79.8">des</w> <w n="79.9">idoles</w> !</l>
							<l n="80" num="1.13"><w n="80.1">Qu</w>’<w n="80.2">étaient</w> <w n="80.3">donc</w> <w n="80.4">le</w> <w n="80.5">manteau</w> <w n="80.6">de</w> <w n="80.7">pourpre</w> <w n="80.8">et</w> <w n="80.9">le</w> <w n="80.10">roseau</w></l>
							<l n="81" num="1.14"><w n="81.1">Près</w> <w n="81.2">de</w> <w n="81.3">la</w> <w n="81.4">honte</w> <w n="81.5">d</w>’<w n="81.6">être</w>, <w n="81.7">avec</w> <w n="81.8">le</w> <w n="81.9">lourd</w> <w n="81.10">boisseau</w></l>
							<l n="82" num="1.15"><w n="82.1">De</w> <w n="82.2">la</w> <w n="82.3">tiare</w> <w n="82.4">sur</w> <w n="82.5">ma</w> <w n="82.6">tête</w> <w n="82.7">de</w> <w n="82.8">lumière</w>,</l>
							<l n="83" num="1.16"><w n="83.1">Assis</w> <w n="83.2">près</w> <w n="83.3">des</w> <w n="83.4">veaux</w> <w n="83.5">d</w>’<w n="83.6">or</w> <w n="83.7">et</w> <w n="83.8">des</w> <w n="83.9">dragons</w> <w n="83.10">de</w> <w n="83.11">pierre</w> ?</l>
							<l n="84" num="1.17"><w n="84.1">Qu</w>’<w n="84.2">était</w> <w n="84.3">la</w> <w n="84.4">mort</w>, <w n="84.5">qu</w>’<w n="84.6">était</w> <w n="84.7">toute</w> <w n="84.8">ma</w> <w n="84.9">passion</w></l>
							<l n="85" num="1.18"><w n="85.1">Au</w> <w n="85.2">prix</w> <w n="85.3">de</w> <w n="85.4">ton</w> <w n="85.5">horreur</w>, <w n="85.6">Déification</w> ?…</l>
							<l n="86" num="1.19"><w n="86.1">Mais</w> <w n="86.2">voici</w> !… <w n="86.3">Délivrant</w> <w n="86.4">mon</w> <w n="86.5">ascension</w> <w n="86.6">claire</w>,</l>
							<l n="87" num="1.20"><w n="87.1">Votre</w> <w n="87.2">geste</w> <w n="87.3">a</w> <w n="87.4">brisé</w> <w n="87.5">le</w> <w n="87.6">marbre</w> <w n="87.7">tumulaire</w>,</l>
							<l n="88" num="1.21"><w n="88.1">Et</w>, <w n="88.2">parmi</w> <w n="88.3">les</w> <w n="88.4">débris</w> <w n="88.5">dispersés</w>, <w n="88.6">je</w> <w n="88.7">surgis</w></l>
							<l n="89" num="1.22"><w n="89.1">Pour</w> <w n="89.2">vous</w> <w n="89.3">redire</w>, <w n="89.4">libéré</w> <w n="89.5">des</w> <w n="89.6">paradis</w></l>
							<l n="90" num="1.23"><w n="90.1">Et</w> <w n="90.2">des</w> <w n="90.3">enfers</w> : « <w n="90.4">Il</w> <w n="90.5">faut</w> <w n="90.6">s</w>’<w n="90.7">aimer</w> <w n="90.8">les</w> <w n="90.9">uns</w> <w n="90.10">les</w> <w n="90.11">autres</w> !… »</l>
							<l n="91" num="1.24">‒ <w n="91.1">Et</w> <w n="91.2">c</w>’<w n="91.3">est</w> <w n="91.4">là</w> <w n="91.5">l</w>’<w n="91.6">Éternelle</w> <w n="91.7">Église</w>, <w n="91.8">ô</w> <w n="91.9">mes</w> <w n="91.10">apôtres</w> !</l>
						</lg>
					</div>
					<div type="section" n="11">
						<head type="main">LA FOULE</head>
						<lg n="1">
							<l n="92" num="1.1"><w n="92.1">Qui</w> <w n="92.2">donc</w> <w n="92.3">est</w>-<w n="92.4">il</w> ? <w n="92.5">son</w> <w n="92.6">front</w> <w n="92.7">inexplicable</w> <w n="92.8">luit</w> ;</l>
							<l n="93" num="1.2"><w n="93.1">Son</w> <w n="93.2">manteau</w>, <w n="93.3">comme</w> <w n="93.4">une</w> <w n="93.5">envergure</w>, <w n="93.6">le</w> <w n="93.7">soulève</w>,</l>
							<l n="94" num="1.3"><w n="94.1">Et</w> <w n="94.2">nos</w> <w n="94.3">vierges</w> <w n="94.4">instincts</w> <w n="94.5">nous</w> <w n="94.6">emportent</w> <w n="94.7">vers</w> <w n="94.8">lui</w>.</l>
							<l n="95" num="1.4"><w n="95.1">Sa</w> <w n="95.2">parole</w> <w n="95.3">ressemble</w> <w n="95.4">à</w> <w n="95.5">notre</w> <w n="95.6">plus</w> <w n="95.7">beau</w> <w n="95.8">rêve</w> :</l>
							<l n="96" num="1.5"><w n="96.1">Franchissant</w> <w n="96.2">deux</w> <w n="96.3">mille</w> <w n="96.4">ans</w> <w n="96.5">sur</w> <w n="96.6">elle</w> <w n="96.7">révolus</w>,</l>
							<l n="97" num="1.6"><w n="97.1">Elle</w> <w n="97.2">dépasse</w> <w n="97.3">d</w>’<w n="97.4">un</w> <w n="97.5">seul</w> <w n="97.6">coup</w> <w n="97.7">toutes</w> <w n="97.8">les</w> <w n="97.9">bornes</w>,</l>
							<l n="98" num="1.7"><w n="98.1">Et</w> <w n="98.2">les</w> <w n="98.3">âges</w> <w n="98.4">futurs</w> <w n="98.5">ne</w> <w n="98.6">diront</w> <w n="98.7">rien</w> <w n="98.8">de</w> <w n="98.9">plus</w>.</l>
						</lg>
						<lg n="2">
							<l n="99" num="2.1">… <w n="99.1">O</w> <w n="99.2">Passant</w> <w n="99.3">de</w> <w n="99.4">clarté</w> <w n="99.5">qui</w> <w n="99.6">viens</w> <w n="99.7">en</w> <w n="99.8">loques</w> <w n="99.9">mornes</w></l>
							<l n="100" num="2.2"><w n="100.1">Secouer</w> <w n="100.2">sur</w> <w n="100.3">nos</w> <w n="100.4">cœurs</w> <w n="100.5">les</w> <w n="100.6">mots</w> <w n="100.7">que</w> <w n="100.8">nous</w> <w n="100.9">voulons</w>,</l>
							<l n="101" num="2.3"><w n="101.1">Toi</w>, <w n="101.2">prêtre</w> <w n="101.3">qui</w> <w n="101.4">n</w>’<w n="101.5">as</w> <w n="101.6">point</w> <w n="101.7">mitré</w> <w n="101.8">tes</w> <w n="101.9">cheveux</w> <w n="101.10">blonds</w>,</l>
							<l n="102" num="2.4"><w n="102.1">Dont</w> <w n="102.2">la</w> <w n="102.3">main</w> <w n="102.4">ne</w> <w n="102.5">tend</w> <w n="102.6">pas</w> <w n="102.7">la</w> <w n="102.8">sébile</w> <w n="102.9">de</w> <w n="102.10">Rome</w>,</l>
							<l part="I" n="103" num="2.5"><w n="103.1">Qui</w> <w n="103.2">donc</w> <w n="103.3">es</w>-<w n="103.4">tu</w> ? <w n="103.5">Le</w> <w n="103.6">Fils</w> <w n="103.7">de</w> <w n="103.8">Dieu</w> ?… </l>
						</lg>
					</div>
					<div type="section" n="12">
						<head type="main">LE PAUVRE</head>
						<lg n="1">
							<l part="F" n="103"><w n="103.9">Le</w> <w n="103.10">Fils</w> <w n="103.11">de</w> <w n="103.12">l</w>’<w n="103.13">homme</w> !</l>
						</lg>
					</div>
				</div></body></text></TEI>