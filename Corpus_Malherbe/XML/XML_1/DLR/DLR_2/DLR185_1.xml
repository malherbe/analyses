<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">FERVEUR</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2031 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Ferveur</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">http://www.poesies.net/luciedelaruemardrueferveur.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Ferveur</title>
						<author>Lucie Delarue-Mardrus</author>
						<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k9691679h?rk=21459;2</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LA REVUE BLANCHE</publisher>
							<date when="1902">1902</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1902">1902</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">FUMÉES</head><div type="poem" key="DLR185">
					<ab type="dot">*</ab>
					<lg n="1">
						<l rhyme="none" n="1" num="1.1"><w n="1.1">Mon</w> <w n="1.2">anxiété</w> <w n="1.3">va</w> <w n="1.4">sans</w> <w n="1.5">cesse</w> <w n="1.6">vers</w> <w n="1.7">la</w> <w n="1.8">Ville</w>…</l>
					</lg>
					<lg n="2">
						<l n="2" num="2.1"><w n="2.1">Avec</w> <w n="2.2">mes</w> <w n="2.3">deux</w> <w n="2.4">yeux</w> <w n="2.5">las</w> <w n="2.6">trépassés</w> <w n="2.7">de</w> <w n="2.8">paresse</w>,</l>
						<l n="3" num="2.2"><w n="3.1">Je</w> <w n="3.2">la</w> <w n="3.3">regarde</w> <w n="3.4">au</w> <w n="3.5">loin</w> <w n="3.6">parmi</w> <w n="3.7">ses</w> <w n="3.8">brumes</w> <w n="3.9">pâles</w></l>
						<l n="4" num="2.3"><w n="4.1">Et</w>, <w n="4.2">sur</w> <w n="4.3">la</w> <w n="4.4">vitre</w> <w n="4.5">où</w> <w n="4.6">mon</w> <w n="4.7">désœuvrement</w> <w n="4.8">se</w> <w n="4.9">dresse</w>,</l>
						<l n="5" num="2.4"><w n="5.1">Je</w> <w n="5.2">raisonne</w> <w n="5.3">au</w> <w n="5.4">travers</w> <w n="5.5">de</w> <w n="5.6">mes</w> <w n="5.7">bagues</w> <w n="5.8">royales</w>.</l>
					</lg>
					<lg n="3">
						<l n="6" num="3.1"><w n="6.1">Mais</w> <w n="6.2">je</w> <w n="6.3">ne</w> <w n="6.4">sais</w> <w n="6.5">pourquoi</w>, <w n="6.6">dans</w> <w n="6.7">le</w> <w n="6.8">chien</w> <w n="6.9">et</w> <w n="6.10">loup</w> <w n="6.11">triste</w>,</l>
						<l n="7" num="3.2"><w n="7.1">Derrière</w> <w n="7.2">cette</w> <w n="7.3">vitre</w> <w n="7.4">où</w> <w n="7.5">mon</w> <w n="7.6">haleine</w> <w n="7.7">marque</w>,</l>
						<l n="8" num="3.3"><w n="8.1">Je</w> <w n="8.2">m</w>’<w n="8.3">assombris</w> <w n="8.4">avec</w> <w n="8.5">cette</w> <w n="8.6">âme</w> <w n="8.7">d</w>’<w n="8.8">anarchiste</w>,</l>
						<l n="9" num="3.4"><w n="9.1">Sourdement</w>, <w n="9.2">à</w> <w n="9.3">travers</w> <w n="9.4">ces</w> <w n="9.5">dix</w> <w n="9.6">doigts</w> <w n="9.7">de</w> <w n="9.8">monarque</w>,</l>
					</lg>
					<lg n="4">
						<l n="10" num="4.1"><w n="10.1">Ni</w> <w n="10.2">pourquoi</w> <w n="10.3">crient</w> <w n="10.4">si</w> <w n="10.5">fort</w> <w n="10.6">vers</w> <w n="10.7">mon</w> <w n="10.8">aise</w> <w n="10.9">inutile</w>,</l>
						<l n="11" num="4.2"><w n="11.1">Par</w> <w n="11.2">delà</w> <w n="11.3">la</w> <w n="11.4">fenêtre</w> <w n="11.5">étroite</w> <w n="11.6">de</w> <w n="11.7">ma</w> <w n="11.8">joie</w>,</l>
						<l n="12" num="4.3"><w n="12.1">Ceux</w> <w n="12.2">qui</w> <w n="12.3">n</w>’<w n="12.4">en</w> <w n="12.5">peuvent</w> <w n="12.6">plus</w>, <w n="12.7">par</w> <w n="12.8">centaines</w> <w n="12.9">de</w> <w n="12.10">mille</w>,</l>
						<l n="13" num="4.4"><w n="13.1">Ni</w> <w n="13.2">pourquoi</w>, <w n="13.3">hors</w> <w n="13.4">ce</w> <w n="13.5">cœur</w> <w n="13.6">profond</w> <w n="13.7">qui</w> <w n="13.8">s</w>’<w n="13.9">apitoie</w>,</l>
					</lg>
					<lg n="5">
						<l rhyme="none" n="14" num="5.1"><w n="14.1">Mon</w> <w n="14.2">anxiété</w> <w n="14.3">va</w> <w n="14.4">sans</w> <w n="14.5">cesse</w> <w n="14.6">vers</w> <w n="14.7">la</w> <w n="14.8">Ville</w>…</l>
					</lg>
				</div></body></text></TEI>