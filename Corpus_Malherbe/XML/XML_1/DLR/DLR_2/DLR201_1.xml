<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">FERVEUR</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2031 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Ferveur</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">http://www.poesies.net/luciedelaruemardrueferveur.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Ferveur</title>
						<author>Lucie Delarue-Mardrus</author>
						<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k9691679h?rk=21459;2</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LA REVUE BLANCHE</publisher>
							<date when="1902">1902</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1902">1902</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">FUMÉES</head><div type="poem" key="DLR201">
					<head type="main">CHANSON</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Avant</w> <w n="1.2">l</w>’<w n="1.3">heure</w> <w n="1.4">où</w> <w n="1.5">la</w> <w n="1.6">nuit</w> <w n="1.7">est</w> <w n="1.8">noire</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Tout</w> <w n="2.2">au</w> <w n="2.3">bout</w> <w n="2.4">des</w> <w n="2.5">soirs</w> <w n="2.6">inouïs</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Les</w> <w n="3.2">couchants</w> <w n="3.3">nous</w> <w n="3.4">font</w> <w n="3.5">toujours</w> <w n="3.6">croire</w></l>
						<l n="4" num="1.4"><w n="4.1">A</w> <w n="4.2">de</w> <w n="4.3">miraculeux</w> <w n="4.4">pays</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Nos</w> <w n="5.2">cœurs</w> <w n="5.3">leurrés</w> <w n="5.4">et</w> <w n="5.5">pleins</w> <w n="5.6">de</w> <w n="5.7">fable</w></l>
						<l n="6" num="2.2"><w n="6.1">Chantent</w> <w n="6.2">l</w>’<w n="6.3">instant</w> <w n="6.4">enfin</w> <w n="6.5">venu</w></l>
						<l n="7" num="2.3"><w n="7.1">De</w> <w n="7.2">gonfler</w> <w n="7.3">la</w> <w n="7.4">voile</w> <w n="7.5">ineffable</w></l>
						<l n="8" num="2.4"><w n="8.1">Qui</w> <w n="8.2">mène</w> <w n="8.3">à</w> <w n="8.4">ce</w> <w n="8.5">monde</w> <w n="8.6">inconnu</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Allons</w> <w n="9.2">vers</w> <w n="9.3">la</w> <w n="9.4">joie</w> <w n="9.5">et</w> <w n="9.6">la</w> <w n="9.7">gloire</w> !</l>
						<l n="10" num="3.2"><w n="10.1">La</w> <w n="10.2">terre</w> <w n="10.3">est</w> <w n="10.4">pleine</w> <w n="10.5">de</w> <w n="10.6">malheurs</w> :</l>
						<l n="11" num="3.3"><w n="11.1">Partons</w> ! <w n="11.2">Partons</w> !… <w n="11.3">O</w> <w n="11.4">pleurs</w> ! <w n="11.5">pleurs</w> ! <w n="11.6">pleurs</w> !</l>
						<l n="12" num="3.4"><w n="12.1">Restons</w> <w n="12.2">ici</w>, <w n="12.3">la</w> <w n="12.4">nuit</w> <w n="12.5">est</w> <w n="12.6">noire</w>…</l>
					</lg>
				</div></body></text></TEI>