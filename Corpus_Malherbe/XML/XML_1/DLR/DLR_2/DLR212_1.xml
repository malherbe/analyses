<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">FERVEUR</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2031 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Ferveur</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">http://www.poesies.net/luciedelaruemardrueferveur.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Ferveur</title>
						<author>Lucie Delarue-Mardrus</author>
						<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k9691679h?rk=21459;2</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LA REVUE BLANCHE</publisher>
							<date when="1902">1902</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1902">1902</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">DÉCLARATIONS</head><div type="poem" key="DLR212">
					<head type="main">VADE RETRO</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">O</w> <w n="1.2">toi</w>, <w n="1.3">naissance</w>, <w n="1.4">sœur</w> <w n="1.5">jumelle</w> <w n="1.6">de</w> <w n="1.7">la</w> <w n="1.8">mort</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Race</w> <w n="2.2">obscure</w> <w n="2.3">dans</w> <w n="2.4">notre</w> <w n="2.5">geste</w> <w n="2.6">confinée</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Deviendrons</w>-<w n="3.2">nous</w>, <w n="3.3">en</w> <w n="3.4">assistant</w> <w n="3.5">ton</w> <w n="3.6">sourd</w> <w n="3.7">effort</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Complices</w> <w n="4.2">du</w> <w n="4.3">vouloir</w> <w n="4.4">d</w>’<w n="4.5">où</w> <w n="4.6">sort</w> <w n="4.7">la</w> <w n="4.8">destinée</w> ?</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Je</w> <w n="5.2">n</w>’<w n="5.3">accepterai</w> <w n="5.4">pas</w> <w n="5.5">en</w> <w n="5.6">mon</w> <w n="5.7">humanité</w></l>
						<l n="6" num="2.2"><w n="6.1">Animale</w>, <w n="6.2">où</w> <w n="6.3">l</w>’<w n="6.4">esprit</w> <w n="6.5">n</w>’<w n="6.6">est</w> <w n="6.7">point</w>, <w n="6.8">ta</w> <w n="6.9">magie</w> <w n="6.10">noire</w> ;</l>
						<l n="7" num="2.3"><w n="7.1">Ton</w> <w n="7.2">égoïste</w> <w n="7.3">événement</w> <w n="7.4">dans</w> <w n="7.5">notre</w> <w n="7.6">histoire</w>,</l>
						<l n="8" num="2.4"><w n="8.1">Je</w> <w n="8.2">le</w> <w n="8.3">repousse</w> <w n="8.4">avec</w> <w n="8.5">toute</w> <w n="8.6">ma</w> <w n="8.7">charité</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Loin</w> <w n="9.2">de</w> <w n="9.3">moi</w> <w n="9.4">donc</w> <w n="9.5">le</w> <w n="9.6">faix</w> <w n="9.7">de</w> <w n="9.8">ton</w> <w n="9.9">œuvre</w> <w n="9.10">incertaine</w>,</l>
						<l n="10" num="3.2"><w n="10.1">Et</w> <w n="10.2">que</w> <w n="10.3">puisse</w> <w n="10.4">la</w> <w n="10.5">Vie</w> <w n="10.6">oublier</w> <w n="10.7">l</w>’<w n="10.8">œuf</w> <w n="10.9">caché</w></l>
						<l n="11" num="3.3"><w n="11.1">Où</w> <w n="11.2">couverait</w>, <w n="11.3">ainsi</w> <w n="11.4">qu</w>’<w n="11.5">un</w> <w n="11.6">monstrueux</w> <w n="11.7">péché</w>,</l>
						<l n="12" num="3.4"><w n="12.1">Dans</w> <w n="12.2">mes</w> <w n="12.3">flancs</w>, <w n="12.4">malgré</w> <w n="12.5">moi</w>, <w n="12.6">l</w>’<w n="12.7">horreur</w> <w n="12.8">d</w>’<w n="12.9">une</w> <w n="12.10">âme</w> <w n="12.11">humaine</w> !</l>
					</lg>
				</div></body></text></TEI>