<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">FERVEUR</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2031 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Ferveur</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">http://www.poesies.net/luciedelaruemardrueferveur.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Ferveur</title>
						<author>Lucie Delarue-Mardrus</author>
						<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k9691679h?rk=21459;2</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LA REVUE BLANCHE</publisher>
							<date when="1902">1902</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1902">1902</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PRONES II</head><div type="poem" key="DLR171">
					<head type="main">LE POÈME SUR LA MONTAGNE</head>
					<div type="section" n="1">
						<head type="main">LE POÈTE</head>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">Allons</w> <w n="1.2">voir</w> <w n="1.3">se</w> <w n="1.4">faner</w> <w n="1.5">l</w>’<w n="1.6">été</w> <w n="1.7">que</w> <w n="1.8">nous</w> <w n="1.9">aimons</w>,</l>
							<l n="2" num="1.2"><w n="2.1">Mon</w> <w n="2.2">âme</w> ! <w n="2.3">et</w> <w n="2.4">méditer</w> <w n="2.5">en</w> <w n="2.6">paix</w> <w n="2.7">au</w> <w n="2.8">pied</w> <w n="2.9">des</w> <w n="2.10">monts</w> :</l>
							<l n="3" num="1.3"><w n="3.1">L</w>’<w n="3.2">air</w> <w n="3.3">automnal</w> <w n="3.4">y</w> <w n="3.5">est</w> <w n="3.6">sans</w> <w n="3.7">goût</w>, <w n="3.8">comme</w> <w n="3.9">l</w>’<w n="3.10">eau</w> <w n="3.11">pure</w>,</l>
							<l n="4" num="1.4"><w n="4.1">Et</w> <w n="4.2">les</w> <w n="4.3">quatre</w> <w n="4.4">horizons</w> <w n="4.5">haussent</w> <w n="4.6">l</w>’<w n="4.7">architecture</w></l>
							<l n="5" num="1.5"><w n="5.1">Inégale</w> <w n="5.2">de</w> <w n="5.3">leurs</w> <w n="5.4">sommets</w> <w n="5.5">noyés</w> <w n="5.6">de</w> <w n="5.7">bleu</w>.</l>
							<l n="6" num="1.6"><w n="6.1">Notre</w> <w n="6.2">force</w> <w n="6.3">écrasée</w> <w n="6.4">erre</w> <w n="6.5">parmi</w> <w n="6.6">ce</w> <w n="6.7">lieu</w></l>
							<l n="7" num="1.7"><w n="7.1">Où</w> <w n="7.2">l</w>’<w n="7.3">horreur</w> <w n="7.4">des</w> <w n="7.5">sapins</w> <w n="7.6">durement</w> <w n="7.7">se</w> <w n="7.8">balance</w>,</l>
							<l n="8" num="1.8"><w n="8.1">Où</w> <w n="8.2">notre</w> <w n="8.3">pas</w> <w n="8.4">émeut</w> <w n="8.5">un</w> <w n="8.6">éternel</w> <w n="8.7">silence</w>…</l>
							<l n="9" num="1.9"><w n="9.1">Mais</w> <w n="9.2">chaque</w> <w n="9.3">pic</w>, <w n="9.4">dressé</w> <w n="9.5">contre</w> <w n="9.6">l</w>’<w n="9.7">espace</w> <w n="9.8">nu</w>,</l>
							<l n="10" num="1.10"><w n="10.1">Menace</w> <w n="10.2">de</w> <w n="10.3">sa</w> <w n="10.4">corne</w> <w n="10.5">immense</w> <w n="10.6">l</w>’<w n="10.7">Inconnu</w>,</l>
							<l n="11" num="1.11"><w n="11.1">Et</w> <w n="11.2">tout</w> <w n="11.3">notre</w> <w n="11.4">orgueilleux</w> <w n="11.5">désir</w> <w n="11.6">qui</w> <w n="11.7">l</w>’<w n="11.8">accompagne</w></l>
							<l n="12" num="1.12"><w n="12.1">Épouse</w> <w n="12.2">la</w> <w n="12.3">grande</w> <w n="12.4">âme</w> <w n="12.5">âpre</w> <w n="12.6">de</w> <w n="12.7">la</w> <w n="12.8">montagne</w>.</l>
						</lg>
					</div>
					<div type="section" n="2">
						<head type="main">LA MONTAGNE</head>
						<lg n="1">
							<l n="13" num="1.1"><w n="13.1">Viens</w> <w n="13.2">à</w> <w n="13.3">moi</w> ! <w n="13.4">Mes</w> <w n="13.5">beaux</w> <w n="13.6">ciels</w> <w n="13.7">sont</w> <w n="13.8">frais</w> <w n="13.9">comme</w> <w n="13.10">l</w>’<w n="13.11">émail</w>,</l>
							<l n="14" num="1.2"><w n="14.1">L</w>’<w n="14.2">herbe</w> <w n="14.3">pleine</w> <w n="14.4">de</w> <w n="14.5">fleurs</w> <w n="14.6">de</w> <w n="14.7">ma</w> <w n="14.8">première</w> <w n="14.9">pente</w></l>
							<l n="15" num="1.3"><w n="15.1">Est</w> <w n="15.2">tendre</w> <w n="15.3">sous</w> <w n="15.4">les</w> <w n="15.5">pieds</w> <w n="15.6">du</w> <w n="15.7">passant</w> <w n="15.8">qui</w> <w n="15.9">la</w> <w n="15.10">hante</w> ;</l>
							<l n="16" num="1.4"><w n="16.1">Un</w> <w n="16.2">peu</w> <w n="16.3">plus</w> <w n="16.4">haut</w>, <w n="16.5">voici</w> <w n="16.6">que</w> <w n="16.7">sonne</w> <w n="16.8">mon</w> <w n="16.9">bétail</w> ;</l>
							<l n="17" num="1.5"><w n="17.1">Puis</w>, <w n="17.2">écumeux</w> <w n="17.3">et</w> <w n="17.4">fous</w> <w n="17.5">comme</w> <w n="17.6">une</w> <w n="17.7">écluse</w> <w n="17.8">ouverte</w>,</l>
							<l n="18" num="1.6"><w n="18.1">Mes</w> <w n="18.2">torrents</w> <w n="18.3">vont</w> <w n="18.4">remplir</w> <w n="18.5">le</w> <w n="18.6">paysage</w> <w n="18.7">inerte</w>,</l>
							<l n="19" num="1.7"><w n="19.1">Et</w> <w n="19.2">tu</w> <w n="19.3">pourra</w> <w n="19.4">surprendre</w>, <w n="19.5">incliné</w> <w n="19.6">vers</w> <w n="19.7">leurs</w> <w n="19.8">flots</w>,</l>
							<l n="20" num="1.8">‒ <w n="20.1">Qui</w> <w n="20.2">sur</w> <w n="20.3">la</w> <w n="20.4">même</w> <w n="20.5">roche</w> <w n="20.6">et</w> <w n="20.7">pour</w> <w n="20.8">la</w> <w n="20.9">rendre</w> <w n="20.10">ronde</w></l>
							<l n="21" num="1.9"><w n="21.1">Rebondissent</w> <w n="21.2">depuis</w> <w n="21.3">les</w> <w n="21.4">premiers</w> <w n="21.5">temps</w> <w n="21.6">du</w> <w n="21.7">monde</w>, ‒</l>
							<l n="22" num="1.10"><w n="22.1">Le</w> <w n="22.2">long</w> <w n="22.3">et</w> <w n="22.4">dur</w> <w n="22.5">roman</w> <w n="22.6">de</w> <w n="22.7">a</w> <w n="22.8">pierre</w> <w n="22.9">et</w> <w n="22.10">des</w> <w n="22.11">eaux</w>.</l>
							<l n="23" num="1.11"><w n="23.1">Monte</w> <w n="23.2">encore</w> ! <w n="23.3">Mes</w> <w n="23.4">flancs</w> <w n="23.5">par</w> <w n="23.6">l</w>’<w n="23.7">Immobilité</w> ;</l>
							<l n="24" num="1.12"><w n="24.1">Mais</w> <w n="24.2">virginale</w>, <w n="24.3">tout</w> <w n="24.4">en</w> <w n="24.5">haut</w>, <w n="24.6">la</w> <w n="24.7">neige</w> <w n="24.8">innée</w></l>
							<l n="25" num="1.13"><w n="25.1">T</w>’<w n="25.2">attend</w> <w n="25.3">dans</w> <w n="25.4">la</w> <w n="25.5">blancheur</w> <w n="25.6">de</w> <w n="25.7">sa</w> <w n="25.8">stérilité</w> !</l>
						</lg>
					</div>
					<div type="section" n="3">
						<head type="main">LE POÈTE</head>
						<lg n="1">
							<l n="26" num="1.1"><w n="26.1">Comment</w> <w n="26.2">résisterais</w>-<w n="26.3">je</w> <w n="26.4">à</w> <w n="26.5">cette</w> <w n="26.6">fiancée</w></l>
							<l n="27" num="1.2"><w n="27.1">Immarcessiblement</w> <w n="27.2">pure</w> <w n="27.3">de</w> <w n="27.4">ma</w> <w n="27.5">Pensée</w> ?…</l>
							<l n="28" num="1.3"><w n="28.1">O</w> <w n="28.2">neige</w> ! <w n="28.3">c</w>’<w n="28.4">est</w> <w n="28.5">vers</w> <w n="28.6">toi</w> <w n="28.7">que</w> <w n="28.8">je</w> <w n="28.9">vais</w> <w n="28.10">en</w> <w n="28.11">chantant</w> !</l>
							<l n="29" num="1.4"><w n="29.1">L</w>’<w n="29.2">orgueil</w> <w n="29.3">de</w> <w n="29.4">la</w> <w n="29.5">montée</w> <w n="29.6">aide</w> <w n="29.7">mon</w> <w n="29.8">pas</w> <w n="29.9">content</w>,</l>
							<l n="30" num="1.5"><w n="30.1">Et</w> <w n="30.2">je</w> <w n="30.3">me</w> <w n="30.4">sens</w> <w n="30.5">grandir</w> <w n="30.6">de</w> <w n="30.7">toute</w> <w n="30.8">l</w>’<w n="30.9">étendue</w></l>
							<l n="31" num="1.6"><w n="31.1">Dévorée</w> <w n="31.2">au</w> <w n="31.3">hasard</w> <w n="31.4">de</w> <w n="31.5">ma</w> <w n="31.6">course</w> <w n="31.7">éperdue</w>…</l>
							<l n="32" num="1.7"><w n="32.1">Ce</w> <w n="32.2">faîte</w> <w n="32.3">à</w> <w n="32.4">qui</w> <w n="32.5">je</w> <w n="32.6">tends</w> <w n="32.7">mes</w> <w n="32.8">bras</w> <w n="32.9">de</w> <w n="32.10">passion</w>,</l>
							<l n="33" num="1.8"><w n="33.1">Qu</w>’<w n="33.2">il</w> <w n="33.3">m</w>’<w n="33.4">emporte</w> <w n="33.5">en</w> <w n="33.6">sa</w> <w n="33.7">lourde</w> <w n="33.8">et</w> <w n="33.9">blanche</w> <w n="33.10">assomption</w></l>
							<l n="34" num="1.9"><w n="34.1">Comme</w> <w n="34.2">un</w> <w n="34.3">signe</w> <w n="34.4">géant</w> <w n="34.5">sur</w> <w n="34.6">ses</w> <w n="34.7">ailes</w> <w n="34.8">tranquilles</w> !</l>
							<l n="35" num="1.10"><w n="35.1">Le</w> <w n="35.2">monde</w> <w n="35.3">est</w> <w n="35.4">loin</w> ! <w n="35.5">Le</w> <w n="35.6">roc</w> <w n="35.7">sonne</w> <w n="35.8">sous</w> <w n="35.9">mon</w> <w n="35.10">épieu</w>,</l>
							<l n="36" num="1.11"><w n="36.1">Et</w>, <w n="36.2">déjà</w>, <w n="36.3">regardant</w> <w n="36.4">avec</w> <w n="36.5">des</w> <w n="36.6">yeux</w> <w n="36.7">de</w> <w n="36.8">dieu</w>,</l>
							<l n="37" num="1.12"><w n="37.1">Je</w> <w n="37.2">vois</w> <w n="37.3">les</w> <w n="37.4">champs</w>, <w n="37.5">je</w> <w n="37.6">vois</w> <w n="37.7">les</w> <w n="37.8">prés</w>… <w n="37.9">Je</w> <w n="37.10">vois</w> <w n="37.11">les</w> <w n="37.12">Villes</w> !</l>
							<l n="38" num="1.13"><w n="38.1">Villes</w> ! <w n="38.2">Villes</w> ! <w n="38.3">Noirceur</w> <w n="38.4">de</w> <w n="38.5">l</w>’<w n="38.6">horizon</w> <w n="38.7">serein</w>,</l>
							<l n="39" num="1.14"><w n="39.1">Du</w> <w n="39.2">haut</w> <w n="39.3">de</w> <w n="39.4">mon</w> <w n="39.5">sublime</w> <w n="39.6">et</w> <w n="39.7">sourcilleux</w> <w n="39.8">refuge</w>,</l>
							<l n="40" num="1.15"><w n="40.1">Je</w> <w n="40.2">ne</w> <w n="40.3">darderai</w> <w n="40.4">pas</w> <w n="40.5">sur</w> <w n="40.6">vous</w> <w n="40.7">des</w> <w n="40.8">yeux</w> <w n="40.9">de</w> <w n="40.10">Juge</w>,</l>
							<l n="41" num="1.16"><w n="41.1">Car</w> <w n="41.2">je</w> <w n="41.3">vous</w> <w n="41.4">aime</w> <w n="41.5">avec</w> <w n="41.6">le</w> <w n="41.7">cœur</w> <w n="41.8">du</w> <w n="41.9">pèlerin</w></l>
							<l n="42" num="1.17"><w n="42.1">Qui</w> <w n="42.2">va</w> <w n="42.3">vers</w> <w n="42.4">la</w> <w n="42.5">blancheur</w> <w n="42.6">la</w> <w n="42.7">paix</w> <w n="42.8">et</w> <w n="42.9">l</w>’<w n="42.10">altitude</w> !</l>
							<l n="43" num="1.18"><w n="43.1">Et</w> <w n="43.2">que</w> <w n="43.3">ne</w> <w n="43.4">puis</w>-<w n="43.5">je</w> <w n="43.6">aussi</w>, <w n="43.7">baigné</w> <w n="43.8">de</w> <w n="43.9">solitude</w>,</l>
							<l n="44" num="1.19"><w n="44.1">Faire</w> <w n="44.2">tomber</w>, <w n="44.3">du</w> <w n="44.4">bout</w> <w n="44.5">d</w>’<w n="44.6">un</w> <w n="44.7">geste</w> <w n="44.8">de</w> <w n="44.9">bonté</w>,</l>
							<l n="45" num="1.20"><w n="45.1">Un</w> <w n="45.2">peu</w> <w n="45.3">de</w> <w n="45.4">mon</w> <w n="45.5">bonheur</w> <w n="45.6">sur</w> <w n="45.7">votre</w> <w n="45.8">humanité</w> ?…</l>
						</lg>
					</div>
					<div type="section" n="4">
						<head type="main">LA MONTAGNE</head>
						<lg n="1">
							<l n="46" num="1.1"><w n="46.1">Vont</w>-<w n="46.2">elles</w>, <w n="46.3">ces</w> <w n="46.4">cités</w>, <w n="46.5">attarder</w> <w n="46.6">ta</w> <w n="46.7">tendresse</w>,</l>
							<l n="47" num="1.2"><w n="47.1">Lorsque</w>, <w n="47.2">d</w>’<w n="47.3">un</w> <w n="47.4">souffle</w> <w n="47.5">froid</w>, <w n="47.6">séchant</w> <w n="47.7">déjà</w> <w n="47.8">le</w> <w n="47.9">sel</w></l>
							<l n="48" num="1.3"><w n="48.1">De</w> <w n="48.2">tes</w> <w n="48.3">yeux</w> <w n="48.4">qui</w> <w n="48.5">s</w>’<w n="48.6">apitoyaient</w>, <w n="48.7">ma</w> <w n="48.8">cime</w> <w n="48.9">dresse</w></l>
							<l n="49" num="1.4"><w n="49.1">Vers</w> <w n="49.2">les</w> <w n="49.3">infinis</w> <w n="49.4">bleus</w> <w n="49.5">son</w> <w n="49.6">lis</w> <w n="49.7">surnaturel</w> ?</l>
						</lg>
					</div>
					<div type="section" n="5">
						<head type="main">LE POÈTE</head>
						<lg n="1">
							<l n="50" num="1.1"><w n="50.1">Je</w> <w n="50.2">ne</w> <w n="50.3">tournerai</w> <w n="50.4">plus</w> <w n="50.5">mes</w> <w n="50.6">yeux</w> <w n="50.7">vers</w> <w n="50.8">la</w> <w n="50.9">vallée</w>,</l>
							<l n="51" num="1.2"><w n="51.1">Mais</w> <w n="51.2">que</w> <w n="51.3">seul</w> <w n="51.4">le</w> <w n="51.5">sommet</w> <w n="51.6">m</w>’<w n="51.7">attache</w> <w n="51.8">à</w> <w n="51.9">son</w> <w n="51.10">éclat</w> !</l>
							<l n="52" num="1.3"><w n="52.1">Je</w> <w n="52.2">vais</w> !… <w n="52.3">Mes</w> <w n="52.4">doigts</w> <w n="52.5">blessés</w> <w n="52.6">saignent</w>… <w n="52.7">La</w> <w n="52.8">neige</w> <w n="52.9">est</w> <w n="52.10">là</w> !</l>
							<l n="53" num="1.4"><w n="53.1">Je</w> <w n="53.2">vois</w> <w n="53.3">le</w> <w n="53.4">but</w> ! <w n="53.5">J</w>’<w n="53.6">atteins</w> <w n="53.7">la</w> <w n="53.8">tour</w> <w n="53.9">immaculée</w>…</l>
							<l n="54" num="1.5"><w n="54.1">O</w> <w n="54.2">Pureté</w> !… <w n="54.3">Splendeur</w> !… <w n="54.4">Silence</w> !… <w n="54.5">M</w>’<w n="54.6">y</w> <w n="54.7">voici</w> !</l>
						</lg>
					</div>
					<div type="section" n="6">
						<head type="main">LA MONTAGNE</head>
						<lg n="1">
							<l n="55" num="1.1"><w n="55.1">Repose</w>-<w n="55.2">toi</w>. <w n="55.3">Tes</w> <w n="55.4">mains</w> <w n="55.5">n</w>’<w n="55.6">ont</w> <w n="55.7">plus</w> <w n="55.8">d</w>’<w n="55.9">autre</w> <w n="55.10">souci</w></l>
							<l n="56" num="1.2"><w n="56.1">Que</w> <w n="56.2">de</w> <w n="56.3">se</w> <w n="56.4">joindre</w> <w n="56.5">sur</w> <w n="56.6">ton</w> <w n="56.7">cœur</w> <w n="56.8">gonflé</w> <w n="56.9">de</w> <w n="56.10">joie</w> ;</l>
							<l n="57" num="1.3"><w n="57.1">Parmi</w> <w n="57.2">l</w>’<w n="57.3">éternité</w> <w n="57.4">des</w> <w n="57.5">hivers</w> <w n="57.6">inouïs</w>,</l>
							<l n="58" num="1.4"><w n="58.1">La</w> <w n="58.2">neige</w> <w n="58.3">est</w> <w n="58.4">ton</w> <w n="58.5">esclave</w> <w n="58.6">et</w> <w n="58.7">l</w>’<w n="58.8">espace</w> <w n="58.9">est</w> <w n="58.10">ta</w> <w n="58.11">proie</w> ;</l>
							<l n="59" num="1.5"><w n="59.1">Un</w> <w n="59.2">nuage</w> <w n="59.3">à</w> <w n="59.4">tes</w> <w n="59.5">pieds</w>, <w n="59.6">plus</w> <w n="59.7">vaste</w> <w n="59.8">qu</w>’<w n="59.9">un</w> <w n="59.10">pays</w>,</l>
							<l n="60" num="1.6"><w n="60.1">T</w>’<w n="60.2">a</w> <w n="60.3">caché</w> <w n="60.4">la</w> <w n="60.5">douleur</w> <w n="60.6">des</w> <w n="60.7">villes</w> <w n="60.8">de</w> <w n="60.9">la</w> <w n="60.10">terre</w> ;</l>
							<l n="61" num="1.7"><w n="61.1">L</w>’<w n="61.2">océan</w> <w n="61.3">de</w> <w n="61.4">ma</w> <w n="61.5">pureté</w> <w n="61.6">te</w> <w n="61.7">désaltère</w></l>
							<l n="62" num="1.8"><w n="62.1">Et</w> <w n="62.2">t</w>’<w n="62.3">arrache</w> <w n="62.4">à</w> <w n="62.5">jamais</w> <w n="62.6">à</w> <w n="62.7">ton</w> <w n="62.8">humanité</w> !</l>
						</lg>
					</div>
					<div type="section" n="7">
						<head type="main">LE POÈTE</head>
						<lg n="1">
							<l n="63" num="1.1"><w n="63.1">Je</w> <w n="63.2">suis</w> <w n="63.3">seul</w> <w n="63.4">comme</w> <w n="63.5">un</w> <w n="63.6">aigle</w> <w n="63.7">avec</w> <w n="63.8">l</w>’<w n="63.9">immensité</w> ;</l>
							<l n="64" num="1.2"><w n="64.1">J</w>’<w n="64.2">ai</w> <w n="64.3">dominé</w> <w n="64.4">l</w>’<w n="64.5">orgueil</w> <w n="64.6">du</w> <w n="64.7">Mont</w>, <w n="64.8">telle</w> <w n="64.9">une</w> <w n="64.10">bête</w></l>
							<l n="65" num="1.3"><w n="65.1">Fabuleuse</w> <w n="65.2">sur</w> <w n="65.3">qui</w> <w n="65.4">sont</w> <w n="65.5">mes</w> <w n="65.6">deux</w> <w n="65.7">pieds</w> <w n="65.8">de</w> <w n="65.9">roi</w> :</l>
							<l n="66" num="1.4"><w n="66.1">Mais</w> <w n="66.2">le</w> <w n="66.3">vertigineux</w> <w n="66.4">azur</w> <w n="66.5">touche</w> <w n="66.6">ma</w> <w n="66.7">tête</w>…</l>
							<l n="67" num="1.5"><w n="67.1">Ai</w>-<w n="67.2">je</w> <w n="67.3">soif</w> <w n="67.4">de</w> <w n="67.5">ce</w> <w n="67.6">ciel</w> <w n="67.7">qui</w> <w n="67.8">déferle</w> <w n="67.9">sur</w> <w n="67.10">moi</w> ?</l>
						</lg>
						<lg n="2">
							<l n="68" num="2.1"><w n="68.1">Non</w> ! <w n="68.2">Mon</w> <w n="68.3">rêve</w> <w n="68.4">toujours</w> <w n="68.5">inapaisé</w> <w n="68.6">dévie</w></l>
							<l n="69" num="2.2"><w n="69.1">Vers</w> <w n="69.2">le</w> <w n="69.3">vide</w> <w n="69.4">de</w> <w n="69.5">croire</w> <w n="69.6">une</w> <w n="69.7">âme</w> <w n="69.8">à</w> <w n="69.9">ce</w> <w n="69.10">ciel</w> <w n="69.11">bleu</w>.</l>
							<l n="70" num="2.3"><w n="70.1">Tu</w> <w n="70.2">m</w>’<w n="70.3">as</w> <w n="70.4">hissé</w> <w n="70.5">trop</w> <w n="70.6">haut</w> <w n="70.7">au</w>-<w n="70.8">dessus</w> <w n="70.9">de</w> <w n="70.10">la</w> <w n="70.11">vie</w>,</l>
							<l n="71" num="2.4"><w n="71.1">Neige</w> <w n="71.2">vers</w> <w n="71.3">qui</w>, <w n="71.4">chantant</w>, <w n="71.5">j</w>’<w n="71.6">ai</w> <w n="71.7">monté</w> <w n="71.8">peu</w> <w n="71.9">à</w> <w n="71.10">peu</w>,</l>
							<l n="72" num="2.5"><w n="72.1">Et</w> <w n="72.2">j</w>’<w n="72.3">écoute</w> <w n="72.4">en</w> <w n="72.5">mon</w> <w n="72.6">cœur</w> <w n="72.7">ton</w> <w n="72.8">blanc</w> <w n="72.9">néant</w> <w n="72.10">se</w> <w n="72.11">taire</w>…</l>
						</lg>
						<lg n="3">
							<l n="73" num="3.1"><w n="73.1">Ah</w>, <w n="73.2">retourner</w> <w n="73.3">vers</w> <w n="73.4">vous</w>, <w n="73.5">cris</w> <w n="73.6">ivres</w> <w n="73.7">de</w> <w n="73.8">la</w> <w n="73.9">terre</w> !</l>
						</lg>
					</div>
				</div></body></text></TEI>