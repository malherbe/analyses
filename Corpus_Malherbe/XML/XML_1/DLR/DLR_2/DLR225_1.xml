<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">FERVEUR</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2031 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Ferveur</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">http://www.poesies.net/luciedelaruemardrueferveur.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Ferveur</title>
						<author>Lucie Delarue-Mardrus</author>
						<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k9691679h?rk=21459;2</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LA REVUE BLANCHE</publisher>
							<date when="1902">1902</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1902">1902</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">HEURES INTIMES</head><div type="poem" key="DLR225">
					<head type="main">L’ACCUEIL</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Nous</w> <w n="1.2">partirons</w> <w n="1.3">tous</w> <w n="1.4">deux</w> <w n="1.5">vers</w> <w n="1.6">la</w> <w n="1.7">vieille</w> <w n="1.8">maison</w></l>
						<l n="2" num="1.2"><w n="2.1">A</w> <w n="2.2">qui</w> <w n="2.3">le</w> <w n="2.4">passé</w> <w n="2.5">fit</w> <w n="2.6">une</w> <w n="2.7">âme</w> ; <w n="2.8">où</w> <w n="2.9">nous</w> <w n="2.10">accueille</w></l>
						<l n="3" num="1.3"><w n="3.1">Le</w> <w n="3.2">jardin</w> <w n="3.3">dévoré</w> <w n="3.4">d</w>’<w n="3.5">automne</w> <w n="3.6">feuille</w> <w n="3.7">à</w> <w n="3.8">feuille</w>,</l>
						<l n="4" num="1.4"><w n="4.1">En</w> <w n="4.2">lequel</w>, <w n="4.3">tristement</w>, <w n="4.4">s</w>’<w n="4.5">envole</w> <w n="4.6">la</w> <w n="4.7">saison</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Car</w> <w n="5.2">de</w> <w n="5.3">tous</w> <w n="5.4">tes</w> <w n="5.5">gazons</w> <w n="5.6">restés</w> <w n="5.7">aromatiques</w>,</l>
						<l n="6" num="2.2"><w n="6.1">De</w> <w n="6.2">tous</w> <w n="6.3">tes</w> <w n="6.4">arbres</w> <w n="6.5">fous</w>, <w n="6.6">jardin</w> ! <w n="6.7">tu</w> <w n="6.8">nous</w> <w n="6.9">attends</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Ainsi</w> <w n="7.2">que</w> <w n="7.3">toi</w>, <w n="7.4">maison</w> <w n="7.5">tranquille</w> <w n="7.6">du</w> <w n="7.7">vieux</w> <w n="7.8">temps</w>,</l>
						<l n="8" num="2.4"><w n="8.1">De</w> <w n="8.2">tous</w> <w n="8.3">tes</w> <w n="8.4">petits</w> <w n="8.5">coins</w> <w n="8.6">et</w> <w n="8.7">recoins</w> <w n="8.8">illogiques</w>,</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Afin</w> <w n="9.2">que</w> <w n="9.3">nous</w> <w n="9.4">hantions</w> <w n="9.5">ces</w> <w n="9.6">pénates</w> <w n="9.7">à</w> <w n="9.8">deux</w>,</l>
						<l n="10" num="3.2"><w n="10.1">Que</w>, <w n="10.2">double</w>, <w n="10.3">pour</w> <w n="10.4">longtemps</w> <w n="10.5">notre</w> <w n="10.6">ombre</w> <w n="10.7">s</w>’<w n="10.8">y</w> <w n="10.9">étende</w></l>
						<l n="11" num="3.3"><w n="11.1">Et</w> <w n="11.2">pour</w> <w n="11.3">qu</w>’<w n="11.4">y</w> <w n="11.5">chante</w> <w n="11.6">aussi</w> <w n="11.7">sa</w> <w n="11.8">joie</w> <w n="11.9">et</w> <w n="11.10">s</w>’<w n="11.11">y</w> <w n="11.12">entende</w></l>
						<l n="12" num="3.4"><w n="12.1">Le</w> <w n="12.2">pigeon</w> <w n="12.3">qui</w> <w n="12.4">roucoule</w> <w n="12.5">en</w> <w n="12.6">nos</w> <w n="12.7">cous</w> <w n="12.8">amoureux</w>.</l>
					</lg>
				</div></body></text></TEI>