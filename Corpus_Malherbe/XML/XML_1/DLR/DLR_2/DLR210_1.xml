<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">FERVEUR</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2031 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Ferveur</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">http://www.poesies.net/luciedelaruemardrueferveur.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Ferveur</title>
						<author>Lucie Delarue-Mardrus</author>
						<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k9691679h?rk=21459;2</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LA REVUE BLANCHE</publisher>
							<date when="1902">1902</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1902">1902</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">DÉCLARATIONS</head><div type="poem" key="DLR210">
					<head type="main">J’AIME SONGER…</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">J</w>’<w n="1.2">aime</w> <w n="1.3">songer</w> <w n="1.4">aux</w> <w n="1.5">mains</w> <w n="1.6">de</w> <w n="1.7">mon</w> <w n="1.8">âme</w>, <w n="1.9">filant</w></l>
						<l n="2" num="1.2"><w n="2.1">A</w> <w n="2.2">l</w>’<w n="2.3">aveugle</w>, <w n="2.4">en</w> <w n="2.5">ces</w> <w n="2.6">temps</w> <w n="2.7">de</w> <w n="2.8">mon</w> <w n="2.9">passé</w>, <w n="2.10">le</w> <w n="2.11">lent</w></l>
						<l n="3" num="1.3"><w n="3.1">Ouvrage</w> <w n="3.2">où</w> <w n="3.3">s</w>’<w n="3.4">accomplit</w> <w n="3.5">la</w> <w n="3.6">capture</w> <w n="3.7">inouïe</w></l>
						<l n="4" num="1.4"><w n="4.1">De</w> <w n="4.2">ton</w> <w n="4.3">cœur</w>, ‒ <w n="4.4">oiseau</w> <w n="4.5">lourd</w> <w n="4.6">de</w> <w n="4.7">roue</w> <w n="4.8">épanouie</w></l>
						<l n="5" num="1.5"><w n="5.1">Et</w> <w n="5.2">que</w> <w n="5.3">cent</w> <w n="5.4">réseaux</w> <w n="5.5">d</w>’<w n="5.6">or</w> <w n="5.7">retiennent</w> <w n="5.8">à</w> <w n="5.9">jamais</w>, ‒</l>
						<l n="6" num="1.6"><w n="6.1">Quand</w> <w n="6.2">ce</w> <w n="6.3">que</w> <w n="6.4">je</w> <w n="6.5">pleurais</w> <w n="6.6">et</w> <w n="6.7">quand</w> <w n="6.8">ce</w> <w n="6.9">que</w> <w n="6.10">j</w>’<w n="6.11">aimais</w>,</l>
						<l n="7" num="1.7"><w n="7.1">Le</w> <w n="7.2">trouble</w>, <w n="7.3">la</w> <w n="7.4">beauté</w>, <w n="7.5">le</w> <w n="7.6">doute</w> <w n="7.7">et</w> <w n="7.8">ses</w> <w n="7.9">batailles</w>,</l>
						<l n="8" num="1.8"><w n="8.1">Toute</w> <w n="8.2">l</w>’<w n="8.3">inquiétude</w>, <w n="8.4">ô</w> <w n="8.5">minutes</w>, <w n="8.6">ô</w> <w n="8.7">mailles</w> !</l>
						<l n="9" num="1.9"><w n="9.1">Dans</w> <w n="9.2">l</w>’<w n="9.3">enchevêtrement</w> <w n="9.4">inexpliqué</w> <w n="9.5">des</w> <w n="9.6">jours</w></l>
						<l n="10" num="1.10"><w n="10.1">Ainsi</w> <w n="10.2">parachevait</w>, <w n="10.3">en</w> <w n="10.4">ses</w> <w n="10.5">tours</w> <w n="10.6">et</w> <w n="10.7">détours</w>,</l>
						<l n="11" num="1.11"><w n="11.1">L</w>’<w n="11.2">ample</w> <w n="11.3">filet</w> <w n="11.4">de</w> <w n="11.5">charme</w> <w n="11.6">et</w> <w n="11.7">d</w>’<w n="11.8">âpre</w> <w n="11.9">sapience</w></l>
						<l n="12" num="1.12"><w n="12.1">Où</w> <w n="12.2">se</w> <w n="12.3">devait</w> <w n="12.4">un</w> <w n="12.5">soir</w> <w n="12.6">prendre</w> <w n="12.7">ta</w> <w n="12.8">violence</w>.</l>
					</lg>
				</div></body></text></TEI>