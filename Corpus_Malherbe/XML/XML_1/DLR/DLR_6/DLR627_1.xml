<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">SOUFFLES DE TEMPÊTE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3271 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_6</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruesoufledetempete.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poèmes de guerre extraits de Souffles de tempêtes (1918)</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poetesses.blog4ever.com</publisher>
						<idno type="URL">https://poetesses.blog4ever.com/nouveaux-poemes-de-guerre-dans-souffles-de-tempete-1918</idno>
					</publicationStmt>
					<sourceDesc>
						<p>Textes complémentaires à ceux du site précédent</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title>SOUFFLES DE TEMPÊTE</title>
					<author>Lucie Delarue-Mardrus</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>E. Fasquelle</publisher>
						<date when="1918">1918</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1918">1918</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes manquants ( "Mes Bonnes routes" -> fin du recueil") ont été ajoutés à partir des poèmes du site : https://poetesses.blog4ever.com.</p>
				<p>Les textes des deux sources numériques comportent de nombreuses erreurs de numérisation ; une relecture complète s’impose.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les corrections métriques ont été faites à partir d’une version numérisée du texte effectuée par la Bibliothèque de Caen le mer.</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-01-31" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-01-31" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">VII</head><head type="main_part">LA GUERRE</head><div type="poem" key="DLR627">
					<head type="main">RESPONSABILITÉS</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Comme</w> <w n="1.2">une</w> <w n="1.3">immense</w> <w n="1.4">mer</w> <w n="1.5">qui</w> <w n="1.6">monte</w> <w n="1.7">sa</w> <w n="1.8">marée</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Nous</w> <w n="2.2">entendons</w> <w n="2.3">la</w> <w n="2.4">guerre</w> <w n="2.5">autour</w> <w n="2.6">de</w> <w n="2.7">nous</w> <w n="2.8">grandir</w>.</l>
						<l n="3" num="1.3"><w n="3.1">Pouvons</w>-<w n="3.2">nous</w> <w n="3.3">vraiment</w> <w n="3.4">voir</w> <w n="3.5">sans</w> <w n="3.6">crier</w> <w n="3.7">et</w> <w n="3.8">bondir</w></l>
						<l n="4" num="1.4"><space unit="char" quantity="8"></space><w n="4.1">Tant</w> <w n="4.2">de</w> <w n="4.3">jeunesse</w> <w n="4.4">massacrée</w> !</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">C</w>’<w n="5.2">est</w> <w n="5.3">la</w> <w n="5.4">guerre</w> <w n="5.5">qui</w> <w n="5.6">règne</w> <w n="5.7">et</w> <w n="5.8">conduit</w> <w n="5.9">le</w> <w n="5.10">destin</w>.</l>
						<l n="6" num="2.2"><w n="6.1">La</w> <w n="6.2">folie</w> <w n="6.3">en</w> <w n="6.4">un</w> <w n="6.5">jour</w> <w n="6.6">s</w>’<w n="6.7">empare</w> <w n="6.8">de</w> <w n="6.9">l</w>’<w n="6.10">Europe</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Et</w> <w n="7.2">la</w> <w n="7.3">mort</w> <w n="7.4">qui</w> <w n="7.5">partout</w>, <w n="7.6">se</w> <w n="7.7">dépêche</w> <w n="7.8">et</w> <w n="7.9">galope</w>,</l>
						<l n="8" num="2.4"><space unit="char" quantity="8"></space><w n="8.1">Ne</w> <w n="8.2">peut</w> <w n="8.3">plus</w> <w n="8.4">compter</w> <w n="8.5">son</w> <w n="8.6">butin</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Nous</w> <w n="9.2">avons</w> <w n="9.3">tout</w> <w n="9.4">laissé</w> : <w n="9.5">pensée</w>, <w n="9.6">art</w>, <w n="9.7">rêve</w>, <w n="9.8">éthique</w>.</l>
						<l n="10" num="3.2"><w n="10.1">Il</w> <w n="10.2">n</w>’<w n="10.3">y</w> <w n="10.4">a</w> <w n="10.5">plus</w> <w n="10.6">d</w>’<w n="10.7">humains</w>, <w n="10.8">il</w> <w n="10.9">y</w> <w n="10.10">a</w> <w n="10.11">des</w> <w n="10.12">fusils</w>.</l>
						<l n="11" num="3.3"><w n="11.1">Dans</w> <w n="11.2">le</w> <w n="11.3">vent</w> <w n="11.4">des</w> <w n="11.5">canons</w>, <w n="11.6">les</w> <w n="11.7">peuples</w> <w n="11.8">sont</w> <w n="11.9">saisis</w></l>
						<l n="12" num="3.4"><space unit="char" quantity="8"></space><w n="12.1">D</w>’<w n="12.2">une</w> <w n="12.3">rage</w> <w n="12.4">apocalyptique</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Les</w> <w n="13.2">fleuves</w> <w n="13.3">par</w> <w n="13.4">les</w> <w n="13.5">champs</w> <w n="13.6">coulent</w> <w n="13.7">rouges</w>. <w n="13.8">Le</w> <w n="13.9">feu</w></l>
						<l n="14" num="4.2"><w n="14.1">Dévore</w> <w n="14.2">les</w> <w n="14.3">cités</w> <w n="14.4">calmes</w> <w n="14.5">et</w> <w n="14.6">magistrales</w>,</l>
						<l n="15" num="4.3"><w n="15.1">La</w> <w n="15.2">ruine</w> <w n="15.3">en</w> <w n="15.4">une</w> <w n="15.5">heure</w> <w n="15.6">atteint</w> <w n="15.7">les</w> <w n="15.8">cathédrales</w>,</l>
						<l n="16" num="4.4"><space unit="char" quantity="8"></space><w n="16.1">Tout</w> <w n="16.2">saigne</w> <w n="16.3">et</w> <w n="16.4">meurt</w> <w n="16.5">sous</w> <w n="16.6">le</w> <w n="16.7">ciel</w> <w n="16.8">bleu</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">De</w> <w n="17.2">cette</w> <w n="17.3">addition</w>, <w n="17.4">quelqu</w>’<w n="17.5">un</w> <w n="17.6">fera</w> <w n="17.7">la</w> <w n="17.8">somme</w>.</l>
						<l n="18" num="5.2"><w n="18.1">Quels</w> <w n="18.2">monstres</w> <w n="18.3">de</w> <w n="18.4">l</w>’<w n="18.5">enfer</w> <w n="18.6">ont</w> <w n="18.7">déchaîné</w> <w n="18.8">cela</w> ?</l>
						<l n="19" num="5.3"><w n="19.1">Qu</w>’<w n="19.2">est</w>-<w n="19.3">ce</w> <w n="19.4">qui</w> <w n="19.5">fit</w> <w n="19.6">surgir</w> <w n="19.7">cette</w> <w n="19.8">horreur</w> <w n="19.9">que</w> <w n="19.10">voilà</w> ?…</l>
						<l n="20" num="5.4"><space unit="char" quantity="8"></space>‒ <w n="20.1">Quelques</w> <w n="20.2">paroles</w> <w n="20.3">d</w>’<w n="20.4">un</w> <w n="20.5">seul</w> <w n="20.6">homme</w>.</l>
					</lg>
				</div></body></text></TEI>