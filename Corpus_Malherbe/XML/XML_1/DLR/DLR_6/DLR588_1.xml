<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">SOUFFLES DE TEMPÊTE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3271 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_6</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruesoufledetempete.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poèmes de guerre extraits de Souffles de tempêtes (1918)</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poetesses.blog4ever.com</publisher>
						<idno type="URL">https://poetesses.blog4ever.com/nouveaux-poemes-de-guerre-dans-souffles-de-tempete-1918</idno>
					</publicationStmt>
					<sourceDesc>
						<p>Textes complémentaires à ceux du site précédent</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title>SOUFFLES DE TEMPÊTE</title>
					<author>Lucie Delarue-Mardrus</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>E. Fasquelle</publisher>
						<date when="1918">1918</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1918">1918</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes manquants ( "Mes Bonnes routes" -> fin du recueil") ont été ajoutés à partir des poèmes du site : https://poetesses.blog4ever.com.</p>
				<p>Les textes des deux sources numériques comportent de nombreuses erreurs de numérisation ; une relecture complète s’impose.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les corrections métriques ont été faites à partir d’une version numérisée du texte effectuée par la Bibliothèque de Caen le mer.</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-01-31" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-01-31" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">IV</head><head type="main_part">CHEVAUX DE LA MER</head><div type="poem" key="DLR588">
					<head type="main">PROMENADES</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Alors</w> <w n="1.2">que</w> <w n="1.3">nous</w> <w n="1.4">longeons</w> <w n="1.5">la</w> <w n="1.6">grève</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Au</w> <w n="2.2">soir</w>, <w n="2.3">j</w>’<w n="2.4">écoute</w> <w n="2.5">immensément</w></l>
						<l n="3" num="1.3"><w n="3.1">Toutes</w> <w n="3.2">les</w> <w n="3.3">cavales</w> <w n="3.4">du</w> <w n="3.5">rêve</w></l>
						<l n="4" num="1.4"><w n="4.1">Galoper</w> <w n="4.2">avec</w> <w n="4.3">ma</w> <w n="4.4">jument</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Le</w> <w n="5.2">soleil</w> <w n="5.3">rouge</w> <w n="5.4">et</w> <w n="5.5">rond</w> <w n="5.6">se</w> <w n="5.7">couche</w></l>
						<l n="6" num="2.2"><w n="6.1">A</w> <w n="6.2">l</w>’<w n="6.3">horizon</w>, <w n="6.4">au</w> <w n="6.5">bout</w> <w n="6.6">du</w> <w n="6.7">flot</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Et</w> <w n="7.2">le</w> <w n="7.3">vent</w>, <w n="7.4">en</w> <w n="7.5">passant</w> <w n="7.6">sur</w> <w n="7.7">l</w>’<w n="7.8">eau</w>,</l>
						<l n="8" num="2.4"><w n="8.1">Met</w> <w n="8.2">du</w> <w n="8.3">sel</w> <w n="8.4">jusque</w> <w n="8.5">dans</w> <w n="8.6">ma</w> <w n="8.7">bouche</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Au</w> <w n="9.2">rythme</w> <w n="9.3">d</w>’<w n="9.4">un</w> <w n="9.5">souffle</w> <w n="9.6">pareil</w></l>
						<l n="10" num="3.2"><w n="10.1">Volent</w> <w n="10.2">la</w> <w n="10.3">crinière</w> <w n="10.4">et</w> <w n="10.5">l</w>’<w n="10.6">écume</w>.</l>
						<l n="11" num="3.3"><w n="11.1">N</w>’<w n="11.2">est</w>-<w n="11.3">ce</w> <w n="11.4">pas</w> <w n="11.5">un</w> <w n="11.6">cheval</w> <w n="11.7">de</w> <w n="11.8">brume</w></l>
						<l n="12" num="3.4"><w n="12.1">Que</w> <w n="12.2">je</w> <w n="12.3">pousse</w> <w n="12.4">vers</w> <w n="12.5">le</w> <w n="12.6">soleil</w> ?</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Songeuse</w> <w n="13.2">et</w> <w n="13.3">sauvage</w> <w n="13.4">amazone</w></l>
						<l n="14" num="4.2"><w n="14.1">Égarée</w> <w n="14.2">au</w> <w n="14.3">pays</w> <w n="14.4">amer</w></l>
						<l n="15" num="4.3"><w n="15.1">De</w> <w n="15.2">l</w>’<w n="15.3">eau</w> <w n="15.4">bleue</w> <w n="15.5">et</w> <w n="15.6">du</w> <w n="15.7">sable</w> <w n="15.8">jaune</w>,</l>
						<l n="16" num="4.4"><w n="16.1">Je</w> <w n="16.2">crois</w> <w n="16.3">que</w> <w n="16.4">je</w> <w n="16.5">monte</w> <w n="16.6">la</w> <w n="16.7">mer</w>.</l>
					</lg>
				</div></body></text></TEI>