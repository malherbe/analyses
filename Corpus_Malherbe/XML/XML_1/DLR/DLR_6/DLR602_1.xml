<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">SOUFFLES DE TEMPÊTE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3271 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_6</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruesoufledetempete.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poèmes de guerre extraits de Souffles de tempêtes (1918)</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poetesses.blog4ever.com</publisher>
						<idno type="URL">https://poetesses.blog4ever.com/nouveaux-poemes-de-guerre-dans-souffles-de-tempete-1918</idno>
					</publicationStmt>
					<sourceDesc>
						<p>Textes complémentaires à ceux du site précédent</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title>SOUFFLES DE TEMPÊTE</title>
					<author>Lucie Delarue-Mardrus</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>E. Fasquelle</publisher>
						<date when="1918">1918</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1918">1918</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes manquants ( "Mes Bonnes routes" -> fin du recueil") ont été ajoutés à partir des poèmes du site : https://poetesses.blog4ever.com.</p>
				<p>Les textes des deux sources numériques comportent de nombreuses erreurs de numérisation ; une relecture complète s’impose.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les corrections métriques ont été faites à partir d’une version numérisée du texte effectuée par la Bibliothèque de Caen le mer.</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-01-31" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-01-31" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">V</head><head type="main_part">ARRIÈRES-SAISONS</head><div type="poem" key="DLR602">
					<head type="main">AUMÔNE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Nulle</w> <w n="1.2">ivresse</w> <w n="1.3">ne</w> <w n="1.4">m</w>’<w n="1.5">est</w> <w n="1.6">venue</w></l>
						<l n="2" num="1.2"><w n="2.1">D</w>’<w n="2.2">avoir</w> <w n="2.3">fréquenté</w> <w n="2.4">les</w> <w n="2.5">humains</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Étonnés</w> <w n="3.2">par</w> <w n="3.3">mon</w> <w n="3.4">âme</w> <w n="3.5">nue</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Ils</w> <w n="4.2">ne</w> <w n="4.3">me</w> <w n="4.4">tendent</w> <w n="4.5">pas</w> <w n="4.6">les</w> <w n="4.7">mains</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Moi</w>, <w n="5.2">je</w> <w n="5.3">venais</w>, <w n="5.4">pleine</w> <w n="5.5">de</w> <w n="5.6">grâce</w>,</l>
						<l n="6" num="2.2"><w n="6.1">Leur</w> <w n="6.2">offrir</w> <w n="6.3">mon</w> <w n="6.4">butin</w> <w n="6.5">doré</w>.</l>
						<l n="7" num="2.3"><w n="7.1">Presque</w> <w n="7.2">tous</w> <w n="7.3">m</w>’<w n="7.4">ont</w> <w n="7.5">fait</w> <w n="7.6">la</w> <w n="7.7">grimace</w></l>
						<l n="8" num="2.4"><w n="8.1">Ou</w> <w n="8.2">se</w> <w n="8.3">sont</w> <w n="8.4">tus</w> <w n="8.5">pour</w> <w n="8.6">m</w>’<w n="8.7">ignorer</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Si</w> <w n="9.2">parfois</w> <w n="9.3">une</w> <w n="9.4">heure</w> <w n="9.5">de</w> <w n="9.6">charme</w></l>
						<l n="10" num="3.2"><w n="10.1">Me</w> <w n="10.2">fut</w> <w n="10.3">donnée</w> <w n="10.4">au</w> <w n="10.5">milieu</w> <w n="10.6">d</w>’<w n="10.7">eux</w>,</l>
						<l n="11" num="3.3"><w n="11.1">Je</w> <w n="11.2">leur</w> <w n="11.3">dois</w> <w n="11.4">tant</w> <w n="11.5">de</w> <w n="11.6">jours</w> <w n="11.7">hideux</w></l>
						<l n="12" num="3.4"><w n="12.1">Que</w> <w n="12.2">mon</w> <w n="12.3">courage</w> <w n="12.4">enfin</w> <w n="12.5">désarme</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Je</w> <w n="13.2">ne</w> <w n="13.3">veux</w> <w n="13.4">plus</w> <w n="13.5">rien</w> <w n="13.6">de</w> <w n="13.7">ceux</w>-<w n="13.8">là</w></l>
						<l n="14" num="4.2"><w n="14.1">Qu</w>’<w n="14.2">il</w> <w n="14.3">faut</w> <w n="14.4">appeler</w> <w n="14.5">mes</w> <w n="14.6">semblables</w>.</l>
						<l n="15" num="4.3"><w n="15.1">Monde</w> <w n="15.2">haineux</w>, <w n="15.3">peureux</w> <w n="15.4">et</w> <w n="15.5">plat</w>,</l>
						<l n="16" num="4.4"><w n="16.1">Nos</w> <w n="16.2">lois</w> <w n="16.3">n</w>’<w n="16.4">ont</w> <w n="16.5">pas</w> <w n="16.6">les</w> <w n="16.7">mêmes</w> <w n="16.8">tables</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">On</w> <w n="17.2">peut</w> <w n="17.3">être</w> <w n="17.4">heureux</w> <w n="17.5">sans</w> <w n="17.6">amis</w>,</l>
						<l n="18" num="5.2"><w n="18.1">Les</w> <w n="18.2">choses</w> <w n="18.3">valent</w> <w n="18.4">qu</w>’<w n="18.5">on</w> <w n="18.6">les</w> <w n="18.7">aime</w>.</l>
						<l n="19" num="5.3"><w n="19.1">Mon</w> <w n="19.2">bonheur</w> <w n="19.3">à</w> <w n="19.4">moi</w>, <w n="19.5">je</w> <w n="19.6">l</w>’<w n="19.7">ai</w> <w n="19.8">mis</w></l>
						<l n="20" num="5.4"><w n="20.1">Dans</w> <w n="20.2">tout</w> <w n="20.3">ce</w> <w n="20.4">qui</w> <w n="20.5">vient</w> <w n="20.6">de</w> <w n="20.7">moi</w>-<w n="20.8">même</w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Ce</w> <w n="21.2">qu</w>’<w n="21.3">on</w> <w n="21.4">appelle</w> <w n="21.5">le</w> <w n="21.6">labeur</w></l>
						<l n="22" num="6.2"><w n="22.1">Et</w> <w n="22.2">ce</w> <w n="22.3">que</w> <w n="22.4">j</w>’<w n="22.5">appelle</w> <w n="22.6">ma</w> <w n="22.7">Muse</w>,</l>
						<l n="23" num="6.3"><w n="23.1">Et</w> <w n="23.2">tout</w> <w n="23.3">le</w> <w n="23.4">reste</w>, <w n="23.5">qui</w> <w n="23.6">m</w>’<w n="23.7">amuse</w>,</l>
						<l n="24" num="6.4"><w n="24.1">Tout</w> <w n="24.2">cela</w> <w n="24.3">suffit</w> <w n="24.4">à</w> <w n="24.5">mon</w> <w n="24.6">cœur</w>.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">J</w>’<w n="25.2">ai</w> <w n="25.3">Paris</w> <w n="25.4">et</w> <w n="25.5">ma</w> <w n="25.6">Normandie</w></l>
						<l n="26" num="7.2"><w n="26.1">Où</w> <w n="26.2">je</w> <w n="26.3">me</w> <w n="26.4">sens</w> <w n="26.5">si</w> <w n="26.6">bien</w> <w n="26.7">chez</w> <w n="26.8">moi</w>,</l>
						<l n="27" num="7.3"><w n="27.1">Du</w> <w n="27.2">bruit</w> <w n="27.3">pour</w> <w n="27.4">mon</w> <w n="27.5">âme</w> <w n="27.6">hardie</w>,</l>
						<l n="28" num="7.4"><w n="28.1">Ou</w> <w n="28.2">du</w> <w n="28.3">silence</w> <w n="28.4">plein</w> <w n="28.5">d</w>’<w n="28.6">émoi</w>.</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1"><w n="29.1">J</w>’<w n="29.2">ai</w> <w n="29.3">mon</w> <w n="29.4">beau</w> <w n="29.5">cheval</w> <w n="29.6">qui</w> <w n="29.7">galope</w></l>
						<l n="30" num="8.2"><w n="30.1">Dans</w> <w n="30.2">le</w> <w n="30.3">même</w> <w n="30.4">sens</w> <w n="30.5">que</w> <w n="30.6">le</w> <w n="30.7">vent</w>,</l>
						<l n="31" num="8.3"><w n="31.1">Par</w> <w n="31.2">les</w> <w n="31.3">roux</w> <w n="31.4">automnes</w> <w n="31.5">d</w>’<w n="31.6">Europe</w>,</l>
						<l n="32" num="8.4"><w n="32.1">Sous</w> <w n="32.2">un</w> <w n="32.3">ciel</w> <w n="32.4">bas</w>, <w n="32.5">gris</w> <w n="32.6">et</w> <w n="32.7">mouvant</w>.</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1"><w n="33.1">J</w>’<w n="33.2">ai</w> <w n="33.3">ma</w> <w n="33.4">musique</w> <w n="33.5">et</w> <w n="33.6">mon</w> <w n="33.7">grimoire</w>,</l>
						<l n="34" num="9.2"><w n="34.1">Mon</w> <w n="34.2">doux</w> <w n="34.3">piano</w> <w n="34.4">reposant</w>,</l>
						<l n="35" num="9.3"><w n="35.1">Ma</w> <w n="35.2">grammaire</w> <w n="35.3">d</w>’<w n="35.4">arabisant</w>,</l>
						<l n="36" num="9.4"><w n="36.1">Même</w> <w n="36.2">mon</w> <w n="36.3">violon</w>. ‒ <w n="36.4">ma</w> <w n="36.5">gloire</w> !</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1"><w n="37.1">J</w>’<w n="37.2">ai</w> <w n="37.3">mes</w> <w n="37.4">pinceaux</w> <w n="37.5">et</w> <w n="37.6">mes</w> <w n="37.7">crayons</w></l>
						<l n="38" num="10.2"><w n="38.1">Pour</w> <w n="38.2">les</w> <w n="38.3">jours</w> <w n="38.4">où</w> <w n="38.5">je</w> <w n="38.6">me</w> <w n="38.7">sens</w> <w n="38.8">peintre</w>…</l>
						<l n="39" num="10.3"><w n="39.1">Puis</w> <w n="39.2">j</w>’<w n="39.3">ai</w> <w n="39.4">mon</w> <w n="39.5">rêve</w> <w n="39.6">qui</w> <w n="39.7">me</w> <w n="39.8">cintre</w></l>
						<l n="40" num="10.4"><w n="40.1">D</w>’<w n="40.2">une</w> <w n="40.3">auréole</w> <w n="40.4">de</w> <w n="40.5">rayons</w>.</l>
					</lg>
					<lg n="11">
						<l n="41" num="11.1"><w n="41.1">Dans</w> <w n="41.2">le</w> <w n="41.3">visible</w> <w n="41.4">et</w> <w n="41.5">l</w>’<w n="41.6">invisible</w>,</l>
						<l n="42" num="11.2"><w n="42.1">Je</w> <w n="42.2">me</w> <w n="42.3">promène</w> <w n="42.4">en</w> <w n="42.5">souriant</w>.</l>
						<l n="43" num="11.3"><w n="43.1">Mon</w> <w n="43.2">destin</w> <w n="43.3">n</w>’<w n="43.4">a</w> <w n="43.5">rien</w> <w n="43.6">d</w>’<w n="43.7">effrayant</w>.</l>
						<l n="44" num="11.4"><w n="44.1">Je</w> <w n="44.2">suis</w> <w n="44.3">seule</w>, <w n="44.4">mais</w> <w n="44.5">je</w> <w n="44.6">suis</w> <w n="44.7">libre</w>.</l>
					</lg>
					<lg n="12">
						<l n="45" num="12.1"><w n="45.1">Parmi</w> <w n="45.2">vous</w>, <w n="45.3">décevants</w> <w n="45.4">humains</w>,</l>
						<l n="46" num="12.2"><w n="46.1">Déjà</w> <w n="46.2">pareille</w> <w n="46.3">à</w> <w n="46.4">mon</w> <w n="46.5">fantôme</w>,</l>
						<l n="47" num="12.3"><w n="47.1">J</w>’<w n="47.2">aime</w> <w n="47.3">mieux</w> <w n="47.4">mon</w> <w n="47.5">grave</w> <w n="47.6">royaume</w></l>
						<l n="48" num="12.4"><w n="48.1">Que</w> <w n="48.2">vos</w> <w n="48.3">bonheurs</w> <w n="48.4">sans</w> <w n="48.5">lendemains</w>.</l>
					</lg>
					<lg n="13">
						<l n="49" num="13.1"><w n="49.1">Au</w> <w n="49.2">jour</w> <w n="49.3">venu</w>, <w n="49.4">que</w> <w n="49.5">l</w>’<w n="49.6">heure</w> <w n="49.7">sonne</w></l>
						<l n="50" num="13.2"><w n="50.1">Où</w> <w n="50.2">l</w>’<w n="50.3">on</w> <w n="50.4">doit</w> <w n="50.5">renoncer</w> <w n="50.6">à</w> <w n="50.7">tout</w> !</l>
						<l n="51" num="13.3"><w n="51.1">Je</w> <w n="51.2">ne</w> <w n="51.3">devrai</w> <w n="51.4">rien</w> <w n="51.5">à</w> <w n="51.6">personne</w></l>
						<l n="52" num="13.4"><w n="52.1">Et</w> <w n="52.2">chacun</w> <w n="52.3">me</w> <w n="52.4">devra</w> <w n="52.5">beaucoup</w>.</l>
					</lg>
					<lg n="14">
						<l n="53" num="14.1"><w n="53.1">Car</w> <w n="53.2">toutes</w> <w n="53.3">ces</w> <w n="53.4">belles</w> <w n="53.5">années</w></l>
						<l n="54" num="14.2"><w n="54.1">A</w> <w n="54.2">l</w>’<w n="54.3">écart</w> <w n="54.4">de</w> <w n="54.5">vos</w> <w n="54.6">tristes</w> <w n="54.7">bruits</w></l>
						<l n="55" num="14.3"><w n="55.1">Auront</w> <w n="55.2">encor</w> <w n="55.3">nourri</w> <w n="55.4">mes</w> <w n="55.5">fruits</w>,</l>
						<l n="56" num="14.4">‒ <w n="56.1">Et</w> <w n="56.2">je</w> <w n="56.3">vous</w> <w n="56.4">les</w> <w n="56.5">aurai</w> <w n="56.6">données</w>.</l>
					</lg>
				</div></body></text></TEI>