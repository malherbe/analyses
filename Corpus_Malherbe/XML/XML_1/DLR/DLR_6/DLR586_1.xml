<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">SOUFFLES DE TEMPÊTE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3271 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_6</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruesoufledetempete.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poèmes de guerre extraits de Souffles de tempêtes (1918)</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poetesses.blog4ever.com</publisher>
						<idno type="URL">https://poetesses.blog4ever.com/nouveaux-poemes-de-guerre-dans-souffles-de-tempete-1918</idno>
					</publicationStmt>
					<sourceDesc>
						<p>Textes complémentaires à ceux du site précédent</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title>SOUFFLES DE TEMPÊTE</title>
					<author>Lucie Delarue-Mardrus</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>E. Fasquelle</publisher>
						<date when="1918">1918</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1918">1918</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes manquants ( "Mes Bonnes routes" -> fin du recueil") ont été ajoutés à partir des poèmes du site : https://poetesses.blog4ever.com.</p>
				<p>Les textes des deux sources numériques comportent de nombreuses erreurs de numérisation ; une relecture complète s’impose.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les corrections métriques ont été faites à partir d’une version numérisée du texte effectuée par la Bibliothèque de Caen le mer.</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-01-31" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-01-31" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">IV</head><head type="main_part">CHEVAUX DE LA MER</head><div type="poem" key="DLR586">
					<head type="main">PRÉSENCE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Que</w> <w n="1.2">les</w> <w n="1.3">pommiers</w> <w n="1.4">d</w>’<w n="1.5">avril</w> <w n="1.6">ouvrent</w> <w n="1.7">parmi</w> <w n="1.8">l</w>’<w n="1.9">aurore</w></l>
						<l n="2" num="1.2"><w n="2.1">Leurs</w> <w n="2.2">mille</w> <w n="2.3">blanches</w> <w n="2.4">fleurs</w> <w n="2.5">au</w> <w n="2.6">cœur</w> <w n="2.7">incarnadin</w></l>
						<l n="3" num="1.3"><w n="3.1">Ou</w> <w n="3.2">que</w> <w n="3.3">règne</w> <w n="3.4">l</w>’<w n="3.5">hiver</w> <w n="3.6">sans</w> <w n="3.7">feuilles</w> <w n="3.8">et</w> <w n="3.9">sans</w> <w n="3.10">flore</w>,</l>
						<l n="4" num="1.4"><space unit="char" quantity="8"></space><w n="4.1">La</w> <w n="4.2">mer</w> <w n="4.3">est</w> <w n="4.4">au</w> <w n="4.5">bout</w> <w n="4.6">du</w> <w n="4.7">jardin</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">On</w> <w n="5.2">l</w>’<w n="5.3">entend</w> <w n="5.4">de</w> <w n="5.5">partout</w>, <w n="5.6">furieuse</w> <w n="5.7">ou</w> <w n="5.8">câline</w>,</l>
						<l n="6" num="2.2"><w n="6.1">Léchant</w> <w n="6.2">doucement</w> <w n="6.3">l</w>’<w n="6.4">herbe</w> <w n="6.5">ou</w> <w n="6.6">mangeant</w> <w n="6.7">le</w> <w n="6.8">terrain</w>.</l>
						<l n="7" num="2.3"><w n="7.1">Elle</w> <w n="7.2">est</w> <w n="7.3">là</w>, <w n="7.4">vivant</w> <w n="7.5">monstre</w> <w n="7.6">impatient</w> <w n="7.7">du</w> <w n="7.8">frein</w> ;</l>
						<l n="8" num="2.4"><space unit="char" quantity="8"></space><w n="8.1">La</w> <w n="8.2">marée</w> <w n="8.3">est</w> <w n="8.4">sa</w> <w n="8.5">discipline</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Oui</w>, <w n="9.2">la</w> <w n="9.3">mer</w> <w n="9.4">est</w> <w n="9.5">au</w> <w n="9.6">bout</w> <w n="9.7">du</w> <w n="9.8">jardin</w> ! <w n="9.9">Incessant</w>,</l>
						<l n="10" num="3.2"><w n="10.1">Son</w> <w n="10.2">rythme</w>, <w n="10.3">nuit</w> <w n="10.4">et</w> <w n="10.5">jour</w>, <w n="10.6">entre</w> <w n="10.7">par</w> <w n="10.8">les</w> <w n="10.9">fenêtres</w>.</l>
						<l n="11" num="3.3"><w n="11.1">On</w> <w n="11.2">ne</w> <w n="11.3">peut</w> <w n="11.4">oublier</w>, <w n="11.5">au</w> <w n="11.6">plus</w> <w n="11.7">profond</w> <w n="11.8">des</w> <w n="11.9">aîtres</w>,</l>
						<l n="12" num="3.4"><space unit="char" quantity="8"></space><w n="12.1">Ce</w> <w n="12.2">voisinage</w> <w n="12.3">menaçant</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Le</w> <w n="13.2">raclement</w> <w n="13.3">profond</w> <w n="13.4">des</w> <w n="13.5">grèves</w> <w n="13.6">qu</w>’<w n="13.7">elle</w> <w n="13.8">drague</w></l>
						<l n="14" num="4.2"><w n="14.1">Berce</w> <w n="14.2">tous</w> <w n="14.3">les</w> <w n="14.4">sommeils</w> <w n="14.5">couchés</w> <w n="14.6">au</w> <w n="14.7">creux</w> <w n="14.8">des</w> <w n="14.9">lits</w>,</l>
						<l n="15" num="4.3"><w n="15.1">Et</w> <w n="15.2">l</w>’<w n="15.3">on</w> <w n="15.4">devine</w> <w n="15.5">au</w> <w n="15.6">loin</w> <w n="15.7">ses</w> <w n="15.8">plis</w> <w n="15.9">et</w> <w n="15.10">ses</w> <w n="15.11">replis</w></l>
						<l n="16" num="4.4"><space unit="char" quantity="8"></space><w n="16.1">Et</w> <w n="16.2">la</w> <w n="16.3">forme</w> <w n="16.4">de</w> <w n="16.5">chaque</w> <w n="16.6">vague</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">… <w n="17.1">Vers</w> <w n="17.2">elle</w> <w n="17.3">nous</w> <w n="17.4">irons</w>, <w n="17.5">de</w> <w n="17.6">gradin</w> <w n="17.7">en</w> <w n="17.8">gradin</w>,</l>
						<l n="18" num="5.2"><w n="18.1">Par</w> <w n="18.2">les</w> <w n="18.3">matins</w> <w n="18.4">de</w> <w n="18.5">joie</w> <w n="18.6">et</w> <w n="18.7">par</w> <w n="18.8">les</w> <w n="18.9">nuits</w> <w n="18.10">pleurées</w>.</l>
						<l n="19" num="5.3">‒ <w n="19.1">O</w> <w n="19.2">vie</w> <w n="19.3">humaine</w>, $<w n="19.4">o</w> <w n="19.5">sœur</w> <w n="19.6">tragique</w> <w n="19.7">des</w> <w n="19.8">marées</w>,</l>
						<l n="20" num="5.4"><space unit="char" quantity="8"></space><w n="20.1">La</w> <w n="20.2">mer</w> <w n="20.3">est</w> <w n="20.4">au</w> <w n="20.5">bout</w> <w n="20.6">du</w> <w n="20.7">jardin</w> !</l>
					</lg>
				</div></body></text></TEI>