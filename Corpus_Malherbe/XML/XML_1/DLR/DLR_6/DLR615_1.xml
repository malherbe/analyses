<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">SOUFFLES DE TEMPÊTE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3271 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_6</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruesoufledetempete.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poèmes de guerre extraits de Souffles de tempêtes (1918)</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poetesses.blog4ever.com</publisher>
						<idno type="URL">https://poetesses.blog4ever.com/nouveaux-poemes-de-guerre-dans-souffles-de-tempete-1918</idno>
					</publicationStmt>
					<sourceDesc>
						<p>Textes complémentaires à ceux du site précédent</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title>SOUFFLES DE TEMPÊTE</title>
					<author>Lucie Delarue-Mardrus</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>E. Fasquelle</publisher>
						<date when="1918">1918</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1918">1918</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes manquants ( "Mes Bonnes routes" -> fin du recueil") ont été ajoutés à partir des poèmes du site : https://poetesses.blog4ever.com.</p>
				<p>Les textes des deux sources numériques comportent de nombreuses erreurs de numérisation ; une relecture complète s’impose.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les corrections métriques ont été faites à partir d’une version numérisée du texte effectuée par la Bibliothèque de Caen le mer.</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-01-31" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-01-31" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">VI</head><head type="main_part">PROPHÉTIQUES</head><div type="poem" key="DLR615">
					<head type="main">LIÈGE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Je</w> <w n="1.2">me</w> <w n="1.3">rappellerai</w> <w n="1.4">cette</w> <w n="1.5">verte</w> <w n="1.6">hauteur</w></l>
						<l n="2" num="1.2"><w n="2.1">D</w>’<w n="2.2">où</w> <w n="2.3">j</w>’<w n="2.4">ai</w> <w n="2.5">vu</w> <w n="2.6">s</w>’<w n="2.7">étager</w> <w n="2.8">la</w> <w n="2.9">travailleuse</w> <w n="2.10">Liège</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Fantastique</w> <w n="3.2">cité</w>, <w n="3.3">récit</w> <w n="3.4">de</w> <w n="3.5">vieux</w> <w n="3.6">conteur</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Sous</w> <w n="4.2">un</w> <w n="4.3">nuage</w> <w n="4.4">immense</w> <w n="4.5">et</w> <w n="4.6">changeant</w> <w n="4.7">qui</w> <w n="4.8">l</w>’<w n="4.9">assiège</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Le</w> <w n="5.2">soleil</w> <w n="5.3">disparu</w>, <w n="5.4">sous</w> <w n="5.5">un</w> <w n="5.6">mince</w> <w n="5.7">croissant</w></l>
						<l n="6" num="2.2"><w n="6.1">Qui</w> <w n="6.2">tient</w> <w n="6.3">seul</w>, <w n="6.4">dans</w> <w n="6.5">le</w> <w n="6.6">ciel</w>, <w n="6.7">la</w> <w n="6.8">place</w> <w n="6.9">encore</w> <w n="6.10">rouge</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Lune</w> <w n="7.2">en</w> <w n="7.3">profil</w>, <w n="7.4">bijou</w> <w n="7.5">du</w> <w n="7.6">soir</w>, <w n="7.7">unique</w> <w n="7.8">accent</w></l>
						<l n="8" num="2.4"><w n="8.1">Du</w> <w n="8.2">décor</w> <w n="8.3">ardoisé</w>, <w n="8.4">vaporeux</w> <w n="8.5">et</w> <w n="8.6">qui</w> <w n="8.7">bouge</w>,</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Tout</w> <w n="9.2">ce</w> <w n="9.3">qui</w> <w n="9.4">fume</w> <w n="9.5">est</w> <w n="9.6">là</w>, <w n="9.7">blanc</w>, <w n="9.8">gris</w>, <w n="9.9">et</w> <w n="9.10">se</w> <w n="9.11">confond</w></l>
						<l n="10" num="3.2"><w n="10.1">Avec</w> <w n="10.2">le</w> <w n="10.3">grand</w> <w n="10.4">nuage</w>, <w n="10.5">avec</w> <w n="10.6">la</w> <w n="10.7">Meuse</w> <w n="10.8">pâle</w>.</l>
						<l n="11" num="3.3"><w n="11.1">Noire</w>, <w n="11.2">une</w> <w n="11.3">flèche</w> <w n="11.4">dit</w> <w n="11.5">l</w>’<w n="11.6">église</w> <w n="11.7">cathédrale</w>.</l>
						<l n="12" num="3.4"><w n="12.1">D</w>’<w n="12.2">autres</w> <w n="12.3">pointes</w> <w n="12.4">encor</w> <w n="12.5">percent</w> <w n="12.6">le</w> <w n="12.7">vague</w> <w n="12.8">fond</w></l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Quatre</w> <w n="13.2">plans</w> <w n="13.3">de</w> <w n="13.4">maisons</w> <w n="13.5">pas</w> <w n="13.6">encore</w> <w n="13.7">allumées</w>.</l>
						<l n="14" num="4.2"><w n="14.1">Hauts</w> <w n="14.2">fourneaux</w> <w n="14.3">et</w> <w n="14.4">clochers</w> <w n="14.5">hérissent</w> <w n="14.6">les</w> <w n="14.7">bouillards</w> ;</l>
						<l n="15" num="4.3"><w n="15.1">Et</w>, <w n="15.2">dans</w> <w n="15.3">le</w> <w n="15.4">même</w> <w n="15.5">sens</w>, <w n="15.6">vont</w> <w n="15.7">toutes</w> <w n="15.8">les</w> <w n="15.9">fumées</w>,</l>
						<l n="16" num="4.4"><w n="16.1">Rejoindre</w> <w n="16.2">on</w> <w n="16.3">ne</w> <w n="16.4">sait</w> <w n="16.5">où</w> <w n="16.6">les</w> <w n="16.7">nuages</w> <w n="16.8">fuyards</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">L</w>’<w n="17.2">ombre</w> <w n="17.3">tombe</w>, <w n="17.4">alentour</w>, <w n="17.5">sur</w> <w n="17.6">la</w> <w n="17.7">douce</w> <w n="17.8">campagne</w>.</l>
						<l n="18" num="5.2"><w n="18.1">Mis</w> <w n="18.2">je</w> <w n="18.3">sais</w> <w n="18.4">deux</w> <w n="18.5">points</w> <w n="18.6">noirs</w> <w n="18.7">de</w> <w n="18.8">l</w>’<w n="18.9">espace</w> <w n="18.10">endormi</w> :</l>
						<l n="19" num="5.3"><w n="19.1">Ici</w> <w n="19.2">c</w>’<w n="19.3">est</w> <w n="19.4">la</w> <w n="19.5">Hollande</w>, <w n="19.6">et</w> <w n="19.7">là</w> <w n="19.8">c</w>’<w n="19.9">est</w> <w n="19.10">l</w>’<w n="19.11">Allemagne</w>,</l>
						<l n="20" num="5.4"><w n="20.1">Les</w> <w n="20.2">frontières</w> <w n="20.3">déjà</w>, <w n="20.4">l</w>’<w n="20.5">étranger</w>, <w n="20.6">l</w>’<w n="20.7">ennemi</w>…</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Pays</w> <w n="21.2">flamands</w>, <w n="21.3">pays</w> <w n="21.4">wallons</w>, <w n="21.5">petites</w> <w n="21.6">Frances</w>,</l>
						<l n="22" num="6.2"><w n="22.1">N</w>’<w n="22.2">oubliez</w> <w n="22.3">pas</w> <w n="22.4">de</w> <w n="22.5">qui</w> <w n="22.6">vous</w> <w n="22.7">êtes</w> <w n="22.8">les</w> <w n="22.9">enfants</w>,</l>
						<l n="23" num="6.3"><w n="23.1">Ni</w> <w n="23.2">toi</w> <w n="23.3">qui</w>, <w n="23.4">des</w> <w n="23.5">voisins</w> <w n="23.6">trop</w> <w n="23.7">proches</w>, <w n="23.8">te</w> <w n="23.9">défends</w>,</l>
						<l n="24" num="6.4"><w n="24.1">O</w> <w n="24.2">Liège</w>, <w n="24.3">capitale</w> <w n="24.4">étrange</w> <w n="24.5">des</w> <w n="24.6">nuances</w> !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1912">Novembre 1912</date>
						</dateline>
					</closer>
				</div></body></text></TEI>