<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">SOUFFLES DE TEMPÊTE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3271 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_6</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruesoufledetempete.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poèmes de guerre extraits de Souffles de tempêtes (1918)</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poetesses.blog4ever.com</publisher>
						<idno type="URL">https://poetesses.blog4ever.com/nouveaux-poemes-de-guerre-dans-souffles-de-tempete-1918</idno>
					</publicationStmt>
					<sourceDesc>
						<p>Textes complémentaires à ceux du site précédent</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title>SOUFFLES DE TEMPÊTE</title>
					<author>Lucie Delarue-Mardrus</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>E. Fasquelle</publisher>
						<date when="1918">1918</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1918">1918</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes manquants ( "Mes Bonnes routes" -> fin du recueil") ont été ajoutés à partir des poèmes du site : https://poetesses.blog4ever.com.</p>
				<p>Les textes des deux sources numériques comportent de nombreuses erreurs de numérisation ; une relecture complète s’impose.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les corrections métriques ont été faites à partir d’une version numérisée du texte effectuée par la Bibliothèque de Caen le mer.</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-01-31" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-01-31" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">III</head><head type="main_part">LE SPHINX</head><div type="poem" key="DLR581">
					<head type="main">D’ALEXANDRIE</head>
					<lg n="1">
						<l n="1" num="1.1"><space unit="char" quantity="8"></space><w n="1.1">Rochers</w> <w n="1.2">obscurs</w> <w n="1.3">sur</w> <w n="1.4">la</w> <w n="1.5">mer</w> <w n="1.6">bleue</w></l>
						<l n="2" num="1.2"><space unit="char" quantity="16"></space><w n="2.1">D</w>’<w n="2.2">un</w> <w n="2.3">bleu</w> <w n="2.4">de</w> <w n="2.5">nuit</w>.</l>
						<l n="3" num="1.3"><w n="3.1">Le</w> <w n="3.2">ciel</w> <w n="3.3">orangé</w> <w n="3.4">du</w> <w n="3.5">couchant</w>, <w n="3.6">sur</w> <w n="3.7">la</w> <w n="3.8">mer</w> <w n="3.9">bleue</w>,</l>
						<l n="4" num="1.4"><space unit="char" quantity="8"></space><w n="4.1">Étend</w> <w n="4.2">son</w> <w n="4.3">immense</w> <w n="4.4">circuit</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><space unit="char" quantity="8"></space><w n="5.1">Sombre</w> <w n="5.2">est</w> <w n="5.3">la</w> <w n="5.4">proue</w> <w n="5.5">aiguë</w> <w n="5.6">et</w> <w n="5.7">courbe</w></l>
						<l n="6" num="2.2"><space unit="char" quantity="16"></space><w n="6.1">Que</w>, <w n="6.2">doucement</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Berce</w> <w n="7.2">une</w> <w n="7.3">barque</w>, <w n="7.4">au</w> <w n="7.5">creux</w> <w n="7.6">de</w> <w n="7.7">cette</w> <w n="7.8">crique</w> <w n="7.9">courbe</w>,</l>
						<l n="8" num="2.4"><space unit="char" quantity="8"></space><w n="8.1">Au</w> <w n="8.2">gré</w> <w n="8.3">d</w>’<w n="8.4">un</w> <w n="8.5">pêcheur</w> <w n="8.6">musulman</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><space unit="char" quantity="8"></space><w n="9.1">Quelques</w> <w n="9.2">palmiers</w> <w n="9.3">lèvent</w> <w n="9.4">leurs</w> <w n="9.5">têtes</w>,</l>
						<l n="10" num="3.2"><space unit="char" quantity="16"></space><w n="10.1">Minces</w> <w n="10.2">profils</w>,</l>
						<l n="11" num="3.3"><w n="11.1">ET</w> <w n="11.2">l</w>’<w n="11.3">on</w> <w n="11.4">voit</w> <w n="11.5">l</w>’<w n="11.6">horizon</w> <w n="11.7">nocturne</w> <w n="11.8">entre</w> <w n="11.9">leurs</w> <w n="11.10">têtes</w></l>
						<l n="12" num="3.4"><space unit="char" quantity="8"></space><w n="12.1">Qui</w> <w n="12.2">s</w>’<w n="12.3">échevèlent</w> <w n="12.4">en</w> <w n="12.5">longs</w> <w n="12.6">fils</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><space unit="char" quantity="8"></space><w n="13.1">La</w> <w n="13.2">ville</w> <w n="13.3">neuve</w> <w n="13.4">égyptienne</w></l>
						<l n="14" num="4.2"><space unit="char" quantity="16"></space><w n="14.1">S</w>’<w n="14.2">allume</w> <w n="14.3">au</w> <w n="14.4">loin</w> ;</l>
						<l n="15" num="4.3"><w n="15.1">Mais</w> <w n="15.2">parmi</w> <w n="15.3">l</w>’<w n="15.4">ombre</w>, <w n="15.5">l</w>’<w n="15.6">âme</w> <w n="15.7">antique</w> <w n="15.8">égyptienne</w></l>
						<l n="16" num="4.4"><space unit="char" quantity="8"></space><w n="16.1">Demeure</w> <w n="16.2">en</w> <w n="16.3">ce</w> <w n="16.4">tout</w> <w n="16.5">petit</w> <w n="16.6">coin</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><space unit="char" quantity="8"></space><w n="17.1">Et</w> <w n="17.2">l</w>’<w n="17.3">esprit</w> <w n="17.4">d</w>’<w n="17.5">or</w> <w n="17.6">de</w> <w n="17.7">Cléopâtre</w>,</l>
						<l n="18" num="5.2"><space unit="char" quantity="16"></space><w n="18.1">Au</w> <w n="18.2">fond</w> <w n="18.3">du</w> <w n="18.4">soir</w>,</l>
						<l n="19" num="5.3"><w n="19.1">Navigue</w> <w n="19.2">et</w> <w n="19.3">chante</w>, <w n="19.4">l</w>’<w n="19.5">esprit</w> <w n="19.6">d</w>’<w n="19.7">or</w> <w n="19.8">de</w> <w n="19.9">Cléopâtre</w>,</l>
						<l n="20" num="5.4"><space unit="char" quantity="8"></space><w n="20.1">Face</w> <w n="20.2">au</w> <w n="20.3">vingtième</w> <w n="20.4">siècle</w> <w n="20.5">noir</w>.</l>
					</lg>
				</div></body></text></TEI>