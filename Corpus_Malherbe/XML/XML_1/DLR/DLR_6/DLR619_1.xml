<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">SOUFFLES DE TEMPÊTE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3271 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_6</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruesoufledetempete.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poèmes de guerre extraits de Souffles de tempêtes (1918)</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poetesses.blog4ever.com</publisher>
						<idno type="URL">https://poetesses.blog4ever.com/nouveaux-poemes-de-guerre-dans-souffles-de-tempete-1918</idno>
					</publicationStmt>
					<sourceDesc>
						<p>Textes complémentaires à ceux du site précédent</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title>SOUFFLES DE TEMPÊTE</title>
					<author>Lucie Delarue-Mardrus</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>E. Fasquelle</publisher>
						<date when="1918">1918</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1918">1918</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes manquants ( "Mes Bonnes routes" -> fin du recueil") ont été ajoutés à partir des poèmes du site : https://poetesses.blog4ever.com.</p>
				<p>Les textes des deux sources numériques comportent de nombreuses erreurs de numérisation ; une relecture complète s’impose.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les corrections métriques ont été faites à partir d’une version numérisée du texte effectuée par la Bibliothèque de Caen le mer.</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-01-31" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-01-31" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">VII</head><head type="main_part">LA GUERRE</head><div type="poem" key="DLR619">
					<head type="main">BALLADE DU MOBILISÉ</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Le</w> <w n="1.2">garçon</w> <w n="1.3">s</w>’<w n="1.4">en</w> <w n="1.5">revient</w> <w n="1.6">des</w> <w n="1.7">champs</w>.</l>
						<l n="2" num="1.2"><w n="2.1">Il</w> <w n="2.2">mène</w> <w n="2.3">la</w> <w n="2.4">haute</w> <w n="2.5">charrette</w></l>
						<l n="3" num="1.3"><w n="3.1">Où</w> <w n="3.2">la</w> <w n="3.3">récolte</w>, <w n="3.4">en</w> <w n="3.5">tas</w> <w n="3.6">penchants</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Va</w> <w n="4.2">vers</w> <w n="4.3">la</w> <w n="4.4">grange</w> <w n="4.5">toute</w> <w n="4.6">prête</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">C</w>’<w n="5.2">est</w> <w n="5.3">le</w> <w n="5.4">beau</w> <w n="5.5">mois</w> <w n="5.6">de</w> <w n="5.7">la</w> <w n="5.8">moisson</w>.</l>
						<l n="6" num="2.2"><w n="6.1">Le</w> <w n="6.2">garçon</w>, <w n="6.3">légèrement</w> <w n="6.4">ivre</w>,</l>
						<l n="7" num="2.3"><w n="7.1">A</w> <w n="7.2">sur</w> <w n="7.3">la</w> <w n="7.4">bouche</w> <w n="7.5">une</w> <w n="7.6">chanson</w></l>
						<l n="8" num="2.4"><w n="8.1">Qui</w> <w n="8.2">dit</w> <w n="8.3">tout</w> <w n="8.4">son</w> <w n="8.5">bonheur</w> <w n="8.6">de</w> <w n="8.7">vivre</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Devant</w> <w n="9.2">le</w> <w n="9.3">premier</w> <w n="9.4">des</w> <w n="9.5">hameaux</w></l>
						<l n="10" num="3.2"><w n="10.1">Il</w> <w n="10.2">entend</w> <w n="10.3">le</w> <w n="10.4">tambour</w> <w n="10.5">qui</w> <w n="10.6">sonne</w></l>
						<l n="11" num="3.3"><w n="11.1">Et</w>, <w n="11.2">soudain</w> <w n="11.3">arrêté</w>, <w n="11.4">frissonne</w>,</l>
						<l n="12" num="3.4"><w n="12.1">Doutant</w> <w n="12.2">s</w>’<w n="12.3">il</w> <w n="12.4">a</w> <w n="12.5">compris</w> <w n="12.6">ces</w> <w n="12.7">mots</w> :</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">« <w n="13.1">Amis</w>, <w n="13.2">la</w> <w n="13.3">guerre</w> <w n="13.4">est</w> <w n="13.5">déclarée</w>,</l>
						<l n="14" num="4.2"><w n="14.1">Que</w> <w n="14.2">chacun</w> <w n="14.3">rejoigne</w> <w n="14.4">son</w> <w n="14.5">corps</w>.</l>
						<l n="15" num="4.3"><w n="15.1">Venez</w> <w n="15.2">tous</w> <w n="15.3">en</w> <w n="15.4">troupe</w> <w n="15.5">serrée</w>,</l>
						<l n="16" num="4.4"><w n="16.1">A</w> <w n="16.2">nous</w> <w n="16.3">les</w> <w n="16.4">jeunes</w> <w n="16.5">et</w> <w n="16.6">les</w> <w n="16.7">forts</w> ! »</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Aussitôt</w> <w n="17.2">une</w> <w n="17.3">sainte</w> <w n="17.4">transe</w></l>
						<l n="18" num="5.2"><w n="18.1">S</w>’<w n="18.2">empare</w> <w n="18.3">en</w> <w n="18.4">même</w> <w n="18.5">temps</w> <w n="18.6">de</w> <w n="18.7">tous</w> ;</l>
						<l n="19" num="5.3"><w n="19.1">Un</w> <w n="19.2">seul</w> <w n="19.3">cri</w> <w n="19.4">dit</w> : « <w n="19.5">Vive</w> <w n="19.6">la</w> <w n="19.7">France</w> !</l>
						<l n="20" num="5.4"><w n="20.1">L</w>’<w n="20.2">Alsace</w> <w n="20.3">et</w> <w n="20.4">la</w> <w n="20.5">Lorraine</w> <w n="20.6">à</w> <w n="20.7">nous</w> ! »</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Le</w> <w n="21.2">garçon</w> <w n="21.3">a</w> <w n="21.4">repris</w> <w n="21.5">sa</w> <w n="21.6">route</w></l>
						<l n="22" num="6.2"><w n="22.1">Au</w> <w n="22.2">pas</w> <w n="22.3">pesant</w> <w n="22.4">de</w> <w n="22.5">ses</w> <w n="22.6">chevaux</w>.</l>
						<l n="23" num="6.3"><w n="23.1">Son</w> <w n="23.2">âme</w> <w n="23.3">se</w> <w n="23.4">soulève</w> <w n="23.5">toute</w></l>
						<l n="24" num="6.4"><w n="24.1">Et</w> <w n="24.2">s</w>’<w n="24.3">élance</w> <w n="24.4">par</w> <w n="24.5">monts</w> <w n="24.6">et</w> <w n="24.7">vaux</w>.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">A</w> <w n="25.2">la</w> <w n="25.3">ferme</w>, <w n="25.4">il</w> <w n="25.5">entre</w> : « <w n="25.6">Mon</w> <w n="25.7">père</w>,</l>
						<l n="26" num="7.2"><w n="26.1">Ma</w> <w n="26.2">mère</w>, <w n="26.3">et</w> <w n="26.4">toi</w>, <w n="26.5">ma</w> <w n="26.6">sœur</w>, <w n="26.7">je</w> <w n="26.8">pars</w>.</l>
						<l n="27" num="7.3"><w n="27.1">Voici</w> <w n="27.2">l</w>’<w n="27.3">heure</w> <w n="27.4">des</w> <w n="27.5">grands</w> <w n="27.6">départs</w>,</l>
						<l n="28" num="7.4"><w n="28.1">Vive</w> <w n="28.2">la</w> <w n="28.3">France</w> ! <w n="28.4">C</w>’<w n="28.5">est</w> <w n="28.6">la</w> <w n="28.7">guerre</w> ! »</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1"><w n="29.1">Après</w> <w n="29.2">la</w> <w n="29.3">première</w> <w n="29.4">stupeur</w>,</l>
						<l n="30" num="8.2"><w n="30.1">Chacun</w> <w n="30.2">en</w> <w n="30.3">sanglotant</w> <w n="30.4">l</w>’<w n="30.5">embrasse</w>,</l>
						<l n="31" num="8.3"><w n="31.1">Mais</w> <w n="31.2">lui</w>, <w n="31.3">garçon</w> <w n="31.4">de</w> <w n="31.5">bonne</w> <w n="31.6">race</w>,</l>
						<l n="32" num="8.4"><w n="32.1">Leur</w> <w n="32.2">dit</w> : « <w n="32.3">Je</w> <w n="32.4">reviendrai</w> <w n="32.5">vainqueur</w> !</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1">« <w n="33.1">Au</w> <w n="33.2">revoir</w>, <w n="33.3">ô</w> <w n="33.4">ma</w> <w n="33.5">belle</w> <w n="33.6">ferme</w>,</l>
						<l n="34" num="9.2"><w n="34.1">Au</w> <w n="34.2">revoir</w>, <w n="34.3">ô</w> <w n="34.4">miennes</w> <w n="34.5">et</w> <w n="34.6">miens</w> !</l>
						<l n="35" num="9.3"><w n="35.1">Après</w> <w n="35.2">avoir</w> <w n="35.3">combattu</w> <w n="35.4">ferme</w>,</l>
						<l n="36" num="9.4"><w n="36.1">Dans</w> <w n="36.2">quelques</w> <w n="36.3">mois</w> <w n="36.4">je</w> <w n="36.5">vous</w> <w n="36.6">reviens</w>.</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1">« <w n="37.1">Toi</w>, <w n="37.2">mère</w>, <w n="37.3">et</w> <w n="37.4">toi</w>, <w n="37.5">ma</w> <w n="37.6">sœur</w> <w n="37.7">Marie</w>,</l>
						<l n="38" num="10.2"><w n="38.1">Pour</w> <w n="38.2">moi</w> <w n="38.3">récitez</w> <w n="38.4">un</w> <hi rend="ital"><w n="38.5">Ave</w></hi>.</l>
						<l n="39" num="10.3"><w n="39.1">Allons</w>, <w n="39.2">enfants</w> <w n="39.3">de</w> <w n="39.4">la</w> <w n="39.5">Patrie</w>,</l>
						<l n="40" num="10.4"><w n="40.1">Le</w> <w n="40.2">jour</w> <w n="40.3">de</w> <w n="40.4">gloire</w> <w n="40.5">est</w> <w n="40.6">arrivé</w> ! »</l>
					</lg>
					<closer>
						<dateline>
							<placeName>Honfleur</placeName>,
							<date when="1914">5 août 1914</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>