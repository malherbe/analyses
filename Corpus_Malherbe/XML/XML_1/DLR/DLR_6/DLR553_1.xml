<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">SOUFFLES DE TEMPÊTE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3271 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_6</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruesoufledetempete.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poèmes de guerre extraits de Souffles de tempêtes (1918)</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poetesses.blog4ever.com</publisher>
						<idno type="URL">https://poetesses.blog4ever.com/nouveaux-poemes-de-guerre-dans-souffles-de-tempete-1918</idno>
					</publicationStmt>
					<sourceDesc>
						<p>Textes complémentaires à ceux du site précédent</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title>SOUFFLES DE TEMPÊTE</title>
					<author>Lucie Delarue-Mardrus</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>E. Fasquelle</publisher>
						<date when="1918">1918</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1918">1918</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes manquants ( "Mes Bonnes routes" -> fin du recueil") ont été ajoutés à partir des poèmes du site : https://poetesses.blog4ever.com.</p>
				<p>Les textes des deux sources numériques comportent de nombreuses erreurs de numérisation ; une relecture complète s’impose.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les corrections métriques ont été faites à partir d’une version numérisée du texte effectuée par la Bibliothèque de Caen le mer.</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-01-31" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-01-31" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">I</head><head type="main_part">L’AUTOMNE A CHEVAL</head><div type="poem" key="DLR553">
					<head type="main">LA RENCONTRE D’AUTOMNE</head>
					<lg n="1">
						<l n="1" num="1.1"><space unit="char" quantity="8"></space><w n="1.1">J</w>’<w n="1.2">ai</w> <w n="1.3">rencontré</w> <w n="1.4">parmi</w> <w n="1.5">l</w>’<w n="1.6">automne</w></l>
						<l n="2" num="1.2"><w n="2.1">Qui</w>, <w n="2.2">jusqu</w>’<w n="2.3">à</w> <w n="2.4">l</w>’<w n="2.5">horizon</w>, <w n="2.6">se</w> <w n="2.7">déploie</w> <w n="2.8">et</w> <w n="2.9">moutonne</w>,</l>
						<l n="3" num="1.3"><w n="3.1">J</w>’<w n="3.2">ai</w> <w n="3.3">rencontré</w> <w n="3.4">parmi</w> <w n="3.5">la</w> <w n="3.6">finale</w> <w n="3.7">douceur</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Rencontré</w> <w n="4.2">tout</w> <w n="4.3">à</w> <w n="4.4">coup</w> <w n="4.5">ma</w> <w n="4.6">première</w> <w n="4.7">jeunesse</w>,</l>
						<l n="5" num="1.5"><space unit="char" quantity="8"></space><w n="5.1">Et</w>, <w n="5.2">fière</w> <w n="5.3">de</w> <w n="5.4">mon</w> <w n="5.5">droit</w> <w n="5.6">d</w>’<w n="5.7">aînesse</w>,</l>
						<l n="6" num="1.6"><w n="6.1">J</w>’<w n="6.2">ai</w> <w n="6.3">dit</w> <w n="6.4">hautainement</w> : « <w n="6.5">Salut</w>, <w n="6.6">petite</w> <w n="6.7">sœur</w> ! »</l>
					</lg>
					<lg n="2">
						<l n="7" num="2.1"><space unit="char" quantity="8"></space><w n="7.1">Dans</w> <w n="7.2">l</w>’<w n="7.3">ombre</w> <w n="7.4">d</w>’<w n="7.5">une</w> <w n="7.6">branche</w> <w n="7.7">oblique</w>,</l>
						<l n="8" num="2.2"><w n="8.1">Sur</w> <w n="8.2">mon</w> <w n="8.3">jeune</w> <w n="8.4">cheval</w>, <w n="8.5">essoufflée</w>, <w n="8.6">héroïque</w>,</l>
						<l n="9" num="2.3"><w n="9.1">Arrêtée</w> <w n="9.2">au</w> <w n="9.3">milieu</w> <w n="9.4">d</w>’<w n="9.5">un</w> <w n="9.6">furieux</w> <w n="9.7">galop</w>,</l>
						<l n="10" num="2.4"><w n="10.1">Toute</w> <w n="10.2">ironie</w> <w n="10.3">amère</w> <w n="10.4">en</w> <w n="10.5">mon</w> <w n="10.6">cœur</w> <w n="10.7">endormie</w>,</l>
						<l n="11" num="2.5"><space unit="char" quantity="8"></space><w n="11.1">J</w>’<w n="11.2">étais</w> <w n="11.3">simple</w>, <w n="11.4">enivrée</w>, <w n="11.5">amie</w></l>
						<l n="12" num="2.6"><w n="12.1">De</w> <w n="12.2">l</w>’<w n="12.3">automne</w>, <w n="12.4">des</w> <w n="12.5">bois</w>, <w n="12.6">du</w> <w n="12.7">vent</w>, <w n="12.8">du</w> <w n="12.9">ciel</w>, <w n="12.10">de</w> <w n="12.11">l</w>’<w n="12.12">eau</w>.</l>
					</lg>
					<lg n="3">
						<l n="13" num="3.1"><space unit="char" quantity="8"></space><w n="13.1">J</w>’<w n="13.2">ai</w> <w n="13.3">dit</w> : « <w n="13.4">Enfant</w> <w n="13.5">triste</w> <w n="13.6">et</w> <w n="13.7">muette</w>,</l>
						<l n="14" num="3.2"><w n="14.1">Je</w> <w n="14.2">vois</w> <w n="14.3">derrière</w> <w n="14.4">toi</w> <w n="14.5">tes</w> <w n="14.6">ailes</w> <w n="14.7">de</w> <w n="14.8">mouette</w>,</l>
						<l n="15" num="3.3"><w n="15.1">Et</w> <w n="15.2">je</w> <w n="15.3">sais</w> <w n="15.4">ce</w> <w n="15.5">que</w> <w n="15.6">dit</w> <w n="15.7">ton</w> <w n="15.8">sourire</w> <w n="15.9">accablé</w>.</l>
						<l n="16" num="3.4"><w n="16.1">Réponds</w>-<w n="16.2">moi</w>, <w n="16.3">demoiselle</w> <w n="16.4">entre</w> <w n="16.5">les</w> <w n="16.6">demoiselles</w> !</l>
						<l n="17" num="3.5"><space unit="char" quantity="8"></space><w n="17.1">A</w> <w n="17.2">l</w>’<w n="17.3">envergure</w> <w n="17.4">de</w> <w n="17.5">ses</w> <w n="17.6">ailes</w>,</l>
						<l n="18" num="3.6"><w n="18.1">Ne</w> <w n="18.2">reconnais</w>-<w n="18.3">tu</w> <w n="18.4">pas</w> <w n="18.5">ce</w> <w n="18.6">grand</w> <w n="18.7">cheval</w> <w n="18.8">ailé</w> ? »</l>
					</lg>
					<lg n="4">
						<l n="19" num="4.1"><space unit="char" quantity="8"></space><w n="19.1">Elle</w> : « <w n="19.2">Ces</w> <w n="19.3">ailes</w> <w n="19.4">que</w> <w n="19.5">tu</w> <w n="19.6">portes</w></l>
						<l n="20" num="4.2"><w n="20.1">Faisaient</w>, <w n="20.2">derrière</w> <w n="20.3">toi</w>, <w n="20.4">voler</w> <w n="20.5">les</w> <w n="20.6">feuilles</w> <w n="20.7">mortes</w>,</l>
						<l n="21" num="4.3"><w n="21.1">Et</w> <w n="21.2">cependant</w> <w n="21.3">mon</w> <w n="21.4">cœur</w> <w n="21.5">ne</w> <w n="21.6">te</w> <w n="21.7">reconnaît</w> <w n="21.8">pas</w>.</l>
						<l n="22" num="4.4"><w n="22.1">Ton</w> <w n="22.2">sourire</w> <w n="22.3">est</w> <w n="22.4">trop</w> <w n="22.5">calme</w> <w n="22.6">et</w> <w n="22.7">trop</w> <w n="22.8">jeune</w> <w n="22.9">ton</w> <w n="22.10">âme</w>.</l>
						<l n="23" num="4.5"><space unit="char" quantity="8"></space><w n="23.1">Je</w> <w n="23.2">suis</w> <w n="23.3">vierge</w> <w n="23.4">et</w> <w n="23.5">tu</w> <w n="23.6">n</w>’<w n="23.7">es</w> <w n="23.8">que</w> <w n="23.9">femme</w>,</l>
						<l n="24" num="4.6"><w n="24.1">Souffre</w>, <w n="24.2">de</w> <w n="24.3">ton</w> <w n="24.4">chemin</w>, <w n="24.5">que</w> <w n="24.6">j</w>’<w n="24.7">écarte</w> <w n="24.8">mes</w> <w n="24.9">pas</w>.</l>
					</lg>
					<lg n="5">
						<l n="25" num="5.1"><space unit="char" quantity="8"></space>« <w n="25.1">Moi</w>, <w n="25.2">sans</w> <w n="25.3">jamais</w> <w n="25.4">que</w> <w n="25.5">rien</w> <w n="25.6">m</w>’<w n="25.7">endorme</w>,</l>
						<l n="26" num="5.2"><w n="26.1">J</w>’<w n="26.2">appelle</w> <w n="26.3">en</w> <w n="26.4">vain</w> <w n="26.5">l</w>’<w n="26.6">amant</w> <w n="26.7">impossible</w> <w n="26.8">et</w> <w n="26.9">sans</w> <w n="26.10">forme</w></l>
						<l n="27" num="5.3"><w n="27.1">Qui</w> <w n="27.2">fait</w> <w n="27.3">le</w> <w n="27.4">ciel</w>, <w n="27.5">les</w> <w n="27.6">bois</w> <w n="27.7">et</w> <w n="27.8">le</w> <w n="27.9">vent</w> <w n="27.10">sensuels</w>.</l>
						<l n="28" num="5.4"><w n="28.1">Triste</w> <w n="28.2">avant</w> <w n="28.3">l</w>’<w n="28.4">âge</w>, <w n="28.5">sage</w>, <w n="28.6">inavouée</w> <w n="28.7">et</w> <w n="28.8">double</w>,</l>
						<l n="29" num="5.5"><space unit="char" quantity="8"></space><w n="29.1">Je</w> <w n="29.2">rêve</w>, <w n="29.3">alors</w> <w n="29.4">que</w> <w n="29.5">toi</w>, <w n="29.6">sans</w> <w n="29.7">trouble</w>,</l>
						<l n="30" num="5.6"><w n="30.1">Tu</w> <w n="30.2">satisfais</w> <w n="30.3">ton</w> <w n="30.4">sang</w> <w n="30.5">de</w> <w n="30.6">bonheurs</w> <w n="30.7">visuels</w>.</l>
					</lg>
					<lg n="6">
						<l n="31" num="6.1"><space unit="char" quantity="8"></space>« <w n="31.1">D</w>’<w n="31.2">où</w> <w n="31.3">vient</w> <w n="31.4">que</w> <w n="31.5">tu</w> <w n="31.6">bondis</w> <w n="31.7">encore</w></l>
						<l n="32" num="6.2"><w n="32.1">Si</w> <w n="32.2">le</w> <w n="32.3">divin</w> <w n="32.4">désir</w> <w n="32.5">jamais</w> <w n="32.6">ne</w> <w n="32.7">te</w> <w n="32.8">dévore</w>,</l>
						<l n="33" num="6.3"><w n="33.1">Et</w> <w n="33.2">que</w> <w n="33.3">poursuis</w>-<w n="33.4">tu</w> <w n="33.5">donc</w> <w n="33.6">sur</w> <w n="33.7">ton</w> <w n="33.8">jeune</w> <w n="33.9">cheval</w></l>
						<l n="34" num="6.4"><w n="34.1">Si</w> <w n="34.2">tu</w> <w n="34.3">ne</w> <w n="34.4">te</w> <w n="34.5">meurs</w> <w n="34.6">pas</w> <w n="34.7">de</w> <w n="34.8">soif</w>, <w n="34.9">ô</w> <w n="34.10">mon</w> <w n="34.11">aînée</w> ?</l>
						<l n="35" num="6.5"><space unit="char" quantity="8"></space><w n="35.1">Vois</w>, <w n="35.2">comme</w> <w n="35.3">je</w> <w n="35.4">me</w> <w n="35.5">suis</w> <w n="35.6">fanée</w></l>
						<l n="36" num="6.6"><w n="36.1">Dans</w> <w n="36.2">l</w>’<w n="36.3">attente</w> <w n="36.4">d</w>’<w n="36.5">un</w> <w n="36.6">dieu</w> <w n="36.7">qui</w> <w n="36.8">calmerait</w> <w n="36.9">mon</w> <w n="36.10">mal</w> ! »</l>
					</lg>
					<lg n="7">
						<l n="37" num="7.1"><space unit="char" quantity="8"></space>« <w n="37.1">J</w>’<w n="37.2">ai</w> <w n="37.3">dit</w> : » <w n="37.4">O</w> <w n="37.5">mon</w> <w n="37.6">ancienne</w> <w n="37.7">âme</w> !</l>
						<l n="38" num="7.2"><w n="38.1">Toi</w> <w n="38.2">tu</w> <w n="38.3">n</w>’<w n="38.4">es</w> <w n="38.5">qu</w>’<w n="38.6">une</w> <w n="38.7">vierge</w> <w n="38.8">et</w> <w n="38.9">je</w> <w n="38.10">suis</w> <w n="38.11">une</w> <w n="38.12">femme</w></l>
						<l n="39" num="7.3"><w n="39.1">Toute</w> <w n="39.2">la</w> <w n="39.3">gloire</w> <w n="39.4">et</w> <w n="39.5">tout</w> <w n="39.6">l</w>’<w n="39.7">amour</w> <w n="39.8">je</w> <w n="39.9">les</w> <w n="39.10">connais</w>,</l>
						<l n="40" num="7.4"><w n="40.1">Et</w> <w n="40.2">je</w> <w n="40.3">sais</w> <w n="40.4">maintenant</w> <w n="40.5">que</w> <w n="40.6">le</w> <w n="40.7">plaisir</w> <w n="40.8">de</w> <w n="40.9">vivre</w></l>
						<l n="41" num="7.5"><space unit="char" quantity="8"></space><w n="41.1">C</w>’<w n="41.2">est</w> <w n="41.3">de</w> <w n="41.4">n</w>’<w n="41.5">avoir</w> <w n="41.6">rien</w> <w n="41.7">à</w> <w n="41.8">poursuivre</w>,</l>
						<l n="42" num="7.6"><w n="42.1">Sinon</w> <w n="42.2">le</w> <w n="42.3">vent</w> <w n="42.4">qui</w> <w n="42.5">passe</w> <w n="42.6">à</w> <w n="42.7">travers</w> <w n="42.8">les</w> <w n="42.9">genêts</w>. »</l>
					</lg>
					<lg n="8">
						<l n="43" num="8.1"><space unit="char" quantity="8"></space>« <w n="43.1">Mon</w> <w n="43.2">rêve</w> <w n="43.3">n</w>’<w n="43.4">en</w> <w n="43.5">est</w> <w n="43.6">pas</w> <w n="43.7">moins</w> <w n="43.8">vaste</w>,</l>
						<l n="44" num="8.2"><w n="44.1">Mais</w>, <w n="44.2">pour</w> <w n="44.3">avoir</w> <w n="44.4">vécu</w>, <w n="44.5">combien</w> <w n="44.6">je</w> <w n="44.7">me</w> <w n="44.8">sens</w> <w n="44.9">chaste</w></l>
						<l n="45" num="8.3"><w n="45.1">Près</w> <w n="45.2">des</w> <w n="45.3">songes</w> <w n="45.4">secrets</w> <w n="45.5">de</w> <w n="45.6">la</w> <w n="45.7">virginité</w> !</l>
						<l n="46" num="8.4"><w n="46.1">Si</w> <w n="46.2">je</w> <w n="46.3">reprends</w> <w n="46.4">mon</w> <w n="46.5">vol</w> <w n="46.6">parmi</w> <w n="46.7">l</w>’<w n="46.8">automne</w> <w n="46.9">blonde</w>,</l>
						<l n="47" num="8.5"><space unit="char" quantity="8"></space><w n="47.1">C</w>’<w n="47.2">est</w> <w n="47.3">pour</w> <w n="47.4">fuir</w> <w n="47.5">l</w>’<w n="47.6">amour</w> <w n="47.7">et</w> <w n="47.8">le</w> <w n="47.9">monde</w>,</l>
						<l n="48" num="8.6"><w n="48.1">Car</w> <w n="48.2">le</w> <w n="48.3">monde</w> <w n="48.4">est</w> <w n="48.5">bassesse</w> <w n="48.6">et</w> <w n="48.7">l</w>’<w n="48.8">amour</w> <w n="48.9">pauvreté</w>. »</l>
					</lg>
					<lg n="9">
						<l n="49" num="9.1"><space unit="char" quantity="8"></space>« <w n="49.1">Ouvre</w> <w n="49.2">ton</w> <w n="49.3">regard</w> <w n="49.4">qui</w> <w n="49.5">s</w>’<w n="49.6">étonne</w>.</l>
						<l n="50" num="9.2"><w n="50.1">Contemple</w> <w n="50.2">en</w> <w n="50.3">moi</w> <w n="50.4">l</w>’<w n="50.5">esprit</w> <w n="50.6">tragique</w> <w n="50.7">de</w> <w n="50.8">l</w>’<w n="50.9">automne</w></l>
						<l n="51" num="9.3"><w n="51.1">Et</w> <w n="51.2">la</w> <w n="51.3">simplicité</w> <w n="51.4">des</w> <w n="51.5">feuilles</w> <w n="51.6">dans</w> <w n="51.7">le</w> <w n="51.8">vent</w>.</l>
						<l n="52" num="9.4"><w n="52.1">Ton</w> <w n="52.2">dieu</w>, <w n="52.3">je</w> <w n="52.4">l</w>’<w n="52.5">ai</w> <w n="52.6">trouvé</w> <w n="52.7">parmi</w> <w n="52.8">les</w> <w n="52.9">solitudes</w>.</l>
						<l n="53" num="9.5"><space unit="char" quantity="8"></space><w n="53.1">Et</w>, <w n="53.2">dans</w> <w n="53.3">mes</w> <w n="53.4">mains</w> <w n="53.5">fines</w> <w n="53.6">et</w> <w n="53.7">rudes</w>,</l>
						<l n="54" num="9.6"><w n="54.1">Je</w> <w n="54.2">porte</w> <w n="54.3">un</w> <w n="54.4">univers</w> <w n="54.5">éternel</w> <w n="54.6">et</w> <w n="54.7">vivant</w>. »</l>
					</lg>
					<lg n="10">
						<l n="55" num="10.1"><space unit="char" quantity="8"></space>« <w n="55.1">Face</w> <w n="55.2">à</w> <w n="55.3">face</w> <w n="55.4">avec</w> <w n="55.5">le</w> <w n="55.6">mystère</w>,</l>
						<l n="56" num="10.2"><w n="56.1">Je</w> <w n="56.2">respire</w>, <w n="56.3">anxieuse</w>, <w n="56.4">ivre</w>, <w n="56.5">entre</w> <w n="56.6">ciel</w> <w n="56.7">et</w> <w n="56.8">terre</w> ;</l>
						<l n="57" num="10.3"><w n="57.1">La</w> <w n="57.2">joie</w> <w n="57.3">et</w> <w n="57.4">la</w> <w n="57.5">douleur</w> <w n="57.6">me</w> <w n="57.7">donnent</w> <w n="57.8">leur</w> <w n="57.9">parfum</w> ;</l>
						<l n="58" num="10.4"><w n="58.1">L</w>’<w n="58.2">étonnement</w> <w n="58.3">de</w> <w n="58.4">vivre</w> <w n="58.5">occupe</w> <w n="58.6">ma</w> <w n="58.7">pensée</w>…</l>
						<l n="59" num="10.5"><space unit="char" quantity="8"></space><w n="59.1">Présente</w>, <w n="59.2">future</w> <w n="59.3">et</w> <w n="59.4">passée</w>,</l>
						<l n="60" num="10.6"><w n="60.1">Suis</w>-<w n="60.2">je</w> <w n="60.3">un</w> <w n="60.4">fantôme</w> <w n="60.5">errant</w> ? <w n="60.6">Suis</w>-<w n="60.7">je</w> <w n="60.8">encore</w> <w n="60.9">quelqu</w>’<w n="60.10">un</w> ? »</l>
					</lg>
					<lg n="11">
						<l n="61" num="11.1"><space unit="char" quantity="8"></space>« <w n="61.1">Voici</w>. <w n="61.2">Je</w> <w n="61.3">n</w>’<w n="61.4">ai</w> <w n="61.5">plus</w> <w n="61.6">rien</w> <w n="61.7">à</w> <w n="61.8">dire</w>.</l>
						<l n="62" num="11.2"><w n="62.1">Regarde</w> <w n="62.2">seulement</w> <w n="62.3">cette</w> <w n="62.4">identique</w> <w n="62.5">lyre</w></l>
						<l n="63" num="11.3"><w n="63.1">Que</w> <w n="63.2">je</w> <w n="63.3">tiens</w> <w n="63.4">comme</w> <w n="63.5">toi</w> <w n="63.6">dans</w> <w n="63.7">de</w> <w n="63.8">pieuses</w> <w n="63.9">mains</w>.</l>
						<l n="64" num="11.4"><w n="64.1">Je</w> <w n="64.2">veux</w> <w n="64.3">chanter</w> <w n="64.4">encor</w> <w n="64.5">jusqu</w>’<w n="64.6">au</w> <w n="64.7">jour</w> <w n="64.8">de</w> <w n="64.9">la</w> <w n="64.10">cendre</w>.</l>
						<l n="65" num="11.5"><space unit="char" quantity="8"></space><w n="65.1">Et</w> <w n="65.2">nul</w> <w n="65.3">ne</w> <w n="65.4">peut</w> <w n="65.5">venir</w> <w n="65.6">me</w> <w n="65.7">prendre</w>,</l>
						<l n="66" num="11.6"><w n="66.1">Sauvage</w>, <w n="66.2">libre</w> <w n="66.3">et</w> <w n="66.4">fier</w>, <w n="66.5">mon</w> <w n="66.6">bonheur</w> <w n="66.7">sans</w> <w n="66.8">humains</w>. »</l>
					</lg>
					<lg n="12">
						<l n="67" num="12.1"><space unit="char" quantity="8"></space><w n="67.1">J</w>’<w n="67.2">ai</w> <w n="67.3">vu</w> <w n="67.4">sa</w> <w n="67.5">tête</w> <w n="67.6">détournée</w>.</l>
						<l n="68" num="12.2"><w n="68.1">Elle</w> <w n="68.2">m</w>’<w n="68.3">a</w> <w n="68.4">dit</w> : « <w n="68.5">Adieu</w> ! <w n="68.6">Va</w> <w n="68.7">vers</w> <w n="68.8">ta</w> <w n="68.9">destinée</w> !</l>
						<l n="69" num="12.3"><w n="69.1">Moi</w>, <w n="69.2">je</w> <w n="69.3">demeurerai</w> <w n="69.4">seule</w> <w n="69.5">avec</w> <w n="69.6">mon</w> <w n="69.7">sanglot</w> ! »</l>
						<l n="70" num="12.4">« <w n="70.1">Adieu</w>, <w n="70.2">criai</w>-<w n="70.3">je</w>, <w n="70.4">adieu</w>, <w n="70.5">moi</w>-<w n="70.6">même</w>, <w n="70.7">ô</w> <w n="70.8">triste</w> ! <w n="70.9">O</w> <w n="70.10">pâle</w> ! »</l>
						<l n="71" num="12.5"><space unit="char" quantity="8"></space><w n="71.1">Et</w>, <w n="71.2">parmi</w> <w n="71.3">la</w> <w n="71.4">pourpre</w> <w n="71.5">automnale</w>,</l>
						<l n="72" num="12.6"><w n="72.1">J</w>’<w n="72.2">ai</w> <w n="72.3">salué</w> <w n="72.4">ma</w> <w n="72.5">sœur</w> <w n="72.6">dans</w> <w n="72.7">le</w> <w n="72.8">vent</w> <w n="72.9">du</w> <w n="72.10">galop</w>.</l>
					</lg>
					<closer>
						<dateline>
							<date when="1912">1912</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>