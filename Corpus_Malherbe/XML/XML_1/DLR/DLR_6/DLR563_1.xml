<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">SOUFFLES DE TEMPÊTE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3271 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_6</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruesoufledetempete.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poèmes de guerre extraits de Souffles de tempêtes (1918)</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poetesses.blog4ever.com</publisher>
						<idno type="URL">https://poetesses.blog4ever.com/nouveaux-poemes-de-guerre-dans-souffles-de-tempete-1918</idno>
					</publicationStmt>
					<sourceDesc>
						<p>Textes complémentaires à ceux du site précédent</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title>SOUFFLES DE TEMPÊTE</title>
					<author>Lucie Delarue-Mardrus</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>E. Fasquelle</publisher>
						<date when="1918">1918</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1918">1918</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes manquants ( "Mes Bonnes routes" -> fin du recueil") ont été ajoutés à partir des poèmes du site : https://poetesses.blog4ever.com.</p>
				<p>Les textes des deux sources numériques comportent de nombreuses erreurs de numérisation ; une relecture complète s’impose.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les corrections métriques ont été faites à partir d’une version numérisée du texte effectuée par la Bibliothèque de Caen le mer.</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-01-31" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-01-31" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">II</head><head type="main_part">ADMIRATIONS</head><div type="poem" key="DLR563">
					<head type="main">EX-VOTO</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Notre</w>-<w n="1.2">Dame</w> <w n="1.3">de</w> <w n="1.4">Grâce</w> <w n="1.5">où</w> <w n="1.6">c</w>’<w n="1.7">est</w> <w n="1.8">toujours</w> <w n="1.9">dimanche</w>,</l>
						<l n="2" num="1.2"><space unit="char" quantity="8"></space><w n="2.1">Riche</w> <w n="2.2">d</w>’<w n="2.3">encens</w>, <w n="2.4">d</w>’<w n="2.5">ombre</w> <w n="2.6">et</w> <w n="2.7">de</w> <w n="2.8">feu</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Tes</w> <w n="3.2">tout</w> <w n="3.3">petits</w> <w n="3.4">trois</w>-<w n="3.5">mâts</w> <w n="3.6">dans</w> <w n="3.7">leur</w> <w n="3.8">bouteille</w> <w n="3.9">blanche</w></l>
						<l n="4" num="1.4"><space unit="char" quantity="8"></space><w n="4.1">Naviguaient</w> <w n="4.2">sur</w> <w n="4.3">un</w> <w n="4.4">peu</w> <w n="4.5">de</w> <w n="4.6">bleu</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Parmi</w> <w n="5.2">la</w> <w n="5.3">bonne</w> <w n="5.4">odeur</w> <w n="5.5">d</w>’<w n="5.6">une</w> <w n="5.7">éternelle</w> <w n="5.8">messe</w>,</l>
						<l n="6" num="2.2"><space unit="char" quantity="8"></space><w n="6.1">Pleine</w> <w n="6.2">d</w>’<w n="6.3">étonnement</w> <w n="6.4">muet</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Mon</w> <w n="7.2">enfance</w> <w n="7.3">craintive</w> <w n="7.4">a</w> <w n="7.5">guetté</w> <w n="7.6">ce</w> <w n="7.7">jouet</w></l>
						<l n="8" num="2.4"><space unit="char" quantity="8"></space><w n="8.1">Qui</w> <w n="8.2">n</w>’<w n="8.3">est</w> <w n="8.4">que</w> <w n="8.5">pieuse</w> <w n="8.6">promesse</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Joujou</w> <w n="9.2">dans</w> <w n="9.3">la</w> <w n="9.4">chapelle</w>, <w n="9.5">étonnant</w> <w n="9.6">ex</w>-<w n="9.7">voto</w>,</l>
						<l n="10" num="3.2"><space unit="char" quantity="8"></space><w n="10.1">Incompréhensible</w> <w n="10.2">merveille</w>,</l>
						<l n="11" num="3.3"><w n="11.1">Je</w> <w n="11.2">te</w> <w n="11.3">tiens</w> <w n="11.4">dans</w> <w n="11.5">mes</w> <w n="11.6">mains</w>, <w n="11.7">ce</w> <w n="11.8">soir</w>, <w n="11.9">petit</w> <w n="11.10">bateau</w></l>
						<l n="12" num="3.4"><space unit="char" quantity="8"></space><w n="12.1">Enfermé</w> <w n="12.2">dans</w> <w n="12.3">une</w> <w n="12.4">bouteille</w> !</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Ainsi</w> <w n="13.2">mon</w> <w n="13.3">âge</w> <w n="13.4">à</w> <w n="13.5">moi</w> <w n="13.6">comble</w> <w n="13.7">les</w> <w n="13.8">vœux</w> <w n="13.9">ardents</w></l>
						<l n="14" num="4.2"><space unit="char" quantity="8"></space><w n="14.1">Que</w> <w n="14.2">fit</w> <w n="14.3">une</w> <w n="14.4">gamine</w> <w n="14.5">ancienne</w>.</l>
						<l n="15" num="4.3"><w n="15.1">La</w> <w n="15.2">bouteille</w> <w n="15.3">dévote</w> <w n="15.4">et</w> <w n="15.5">son</w> <w n="15.6">bateau</w> <w n="15.7">dedans</w></l>
						<l n="16" num="4.4"><space unit="char" quantity="8"></space><w n="16.1">Aujourd</w>’<w n="16.2">hui</w> <w n="16.3">seulement</w> <w n="16.4">est</w> <w n="16.5">mienne</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Mon</w> <w n="17.2">enfance</w> <w n="17.3">a</w> <w n="17.4">jeté</w> <w n="17.5">la</w> <w n="17.6">bouteille</w> <w n="17.7">à</w> <w n="17.8">la</w> <w n="17.9">mer</w>,</l>
						<l n="18" num="5.2"><space unit="char" quantity="8"></space><w n="18.1">Le</w> <w n="18.2">temps</w> <w n="18.3">enfin</w> <w n="18.4">me</w> <w n="18.5">la</w> <w n="18.6">ramène</w>.</l>
						<l n="19" num="5.3"><w n="19.1">Voici</w>, <w n="19.2">pour</w> <w n="19.3">oublier</w> <w n="19.4">le</w> <w n="19.5">présent</w> <w n="19.6">doux</w> <w n="19.7">amer</w>,</l>
						<l n="20" num="5.4"><space unit="char" quantity="8"></space><w n="20.1">Tout</w> <w n="20.2">le</w> <w n="20.3">charme</w> <w n="20.4">dont</w> <w n="20.5">elle</w> <w n="20.6">est</w> <w n="20.7">pleine</w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Car</w> <w n="21.2">la</w> <w n="21.3">même</w> <w n="21.4">surprise</w> <w n="21.5">étrange</w> <w n="21.6">qu</w>’<w n="21.7">autrefois</w></l>
						<l n="22" num="6.2"><space unit="char" quantity="8"></space><w n="22.1">Me</w> <w n="22.2">tient</w> <w n="22.3">devant</w> <w n="22.4">cette</w> <w n="22.5">relique</w></l>
						<l n="23" num="6.3"><w n="23.1">Qui</w> <w n="23.2">couche</w> <w n="23.3">avec</w> <w n="23.4">grand</w> <w n="23.5">soin</w>, <w n="23.6">légèrement</w> <w n="23.7">oblique</w>,</l>
						<l n="24" num="6.4"><space unit="char" quantity="8"></space><w n="24.1">Sur</w> <w n="24.2">son</w> <w n="24.3">pied</w> <w n="24.4">poussiéreux</w> <w n="24.5">de</w> <w n="24.6">bois</w>.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">Le</w> <w n="25.2">trois</w>-<w n="25.3">mâts</w> <w n="25.4">compliqué</w>, <w n="25.5">dans</w> <w n="25.6">la</w> <w n="25.7">bouteille</w> <w n="25.8">ronde</w></l>
						<l n="26" num="7.2"><space unit="char" quantity="8"></space><w n="26.1">Dont</w> <w n="26.2">les</w> <w n="26.3">reflets</w> <w n="26.4">imitent</w> <w n="26.5">l</w>’<w n="26.6">eau</w>,</l>
						<l n="27" num="7.3"><w n="27.1">Semble</w>, <w n="27.2">l</w>’<w n="27.3">avant</w> <w n="27.4">tourné</w> <w n="27.5">du</w> <w n="27.6">côté</w> <w n="27.7">du</w> <w n="27.8">goulot</w>,</l>
						<l n="28" num="7.4"><space unit="char" quantity="8"></space><w n="28.1">Faire</w> <w n="28.2">à</w> <w n="28.3">jamais</w> <w n="28.4">le</w> <w n="28.5">tour</w> <w n="28.6">du</w> <w n="28.7">monde</w>.</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1"><w n="29.1">Oui</w>, <w n="29.2">je</w> <w n="29.3">sais</w> ! <w n="29.4">Le</w> <w n="29.5">miracle</w> <w n="29.6">est</w> <w n="29.7">artificiel</w>…</l>
						<l n="30" num="8.2"><space unit="char" quantity="8"></space><w n="30.1">Qu</w>’<w n="30.2">importe</w> ! <w n="30.3">Dans</w> <w n="30.4">l</w>’<w n="30.5">étroit</w> <w n="30.6">espace</w>,</l>
						<l n="31" num="8.3"><w n="31.1">N</w>’<w n="31.2">a</w>-<w n="31.3">t</w>-<w n="31.4">on</w> <w n="31.5">pas</w> <w n="31.6">fait</w> <w n="31.7">entrer</w>, <w n="31.8">ô</w> <w n="31.9">chapelle</w> <w n="31.10">de</w> <w n="31.11">Grâce</w>,</l>
						<l n="32" num="8.4"><space unit="char" quantity="8"></space><w n="32.1">Toute</w> <w n="32.2">le</w> <w n="32.3">mer</w> <w n="32.4">et</w> <w n="32.5">tout</w> <w n="32.6">le</w> <w n="32.7">ciel</w> ?</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1"><w n="33.1">Je</w> <w n="33.2">regarde</w> <w n="33.3">sans</w> <w n="33.4">fin</w>, <w n="33.5">perplexe</w>, <w n="33.6">émue</w> <w n="33.7">et</w> <w n="33.8">sage</w>,</l>
						<l n="34" num="9.2"><space unit="char" quantity="8"></space><w n="34.1">Cet</w> <w n="34.2">objet</w> <w n="34.3">que</w> <w n="34.4">je</w> <w n="34.5">désirais</w>.</l>
						<l n="35" num="9.3"><w n="35.1">Dans</w> <w n="35.2">le</w> <w n="35.3">verre</w> <w n="35.4">exigu</w> <w n="35.5">je</w> <w n="35.6">fais</w> <w n="35.7">un</w> <w n="35.8">long</w> <w n="35.9">voyage</w></l>
						<l n="36" num="9.4"><space unit="char" quantity="8"></space><w n="36.1">Bien</w> <w n="36.2">plus</w> <w n="36.3">beau</w>, <w n="36.4">certes</w> <w n="36.5">que</w> <w n="36.6">les</w> <w n="36.7">vrais</w>.</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1"><w n="37.1">J</w>’<w n="37.2">oublie</w>, <w n="37.3">à</w> <w n="37.4">contempler</w>, <w n="37.5">les</w> <w n="37.6">Méditerranées</w>,</l>
						<l n="38" num="10.2"><space unit="char" quantity="8"></space><w n="38.1">Les</w> <w n="38.2">îles</w>, <w n="38.3">les</w> <w n="38.4">ports</w> <w n="38.5">inouïs</w></l>
						<l n="39" num="10.3"><w n="39.1">Où</w> <w n="39.2">mes</w> <w n="39.3">ambitions</w> <w n="39.4">ont</w> <w n="39.5">été</w> <w n="39.6">promenées</w></l>
						<l n="40" num="10.4"><space unit="char" quantity="8"></space><w n="40.1">De</w> <w n="40.2">connaître</w> <w n="40.3">tous</w> <w n="40.4">les</w> <w n="40.5">pays</w>.</l>
					</lg>
					<lg n="11">
						<l n="41" num="11.1"><w n="41.1">Dans</w> <w n="41.2">ma</w> <w n="41.3">bouteille</w> <w n="41.4">fée</w> <w n="41.5">on</w> <w n="41.6">voit</w> <w n="41.7">tempêtes</w> <w n="41.8">noires</w></l>
						<l n="42" num="11.2"><space unit="char" quantity="8"></space><w n="42.1">Ou</w> <w n="42.2">bien</w> <w n="42.3">grands</w> <w n="42.4">calmes</w> <w n="42.5">sans</w> <w n="42.6">couleur</w>,</l>
						<l n="43" num="11.3"><w n="43.1">Des</w> <w n="43.2">apparitions</w>, <w n="43.3">et</w> <w n="43.4">toutes</w> <w n="43.5">les</w> <w n="43.6">histoires</w></l>
						<l n="44" num="11.4"><space unit="char" quantity="8"></space><w n="44.1">Des</w> <w n="44.2">vieux</w> <w n="44.3">matelots</w> <w n="44.4">de</w> <w n="44.5">Honfleur</w>.</l>
					</lg>
					<lg n="12">
						<l n="45" num="12.1"><w n="45.1">On</w> <w n="45.2">voit</w> <w n="45.3">les</w> <w n="45.4">continents</w> <w n="45.5">désirés</w> <w n="45.6">sur</w> <w n="45.7">la</w> <w n="45.8">carte</w>,</l>
						<l n="46" num="12.2"><space unit="char" quantity="8"></space><w n="46.1">Les</w> <w n="46.2">océans</w>, <w n="46.3">tout</w> <w n="46.4">ce</w> <w n="46.5">qu</w>’<w n="46.6">on</w> <w n="46.7">veut</w>.</l>
						<l n="47" num="12.3"><w n="47.1">Pour</w> <w n="47.2">que</w> <w n="47.3">l</w>’<w n="47.4">esprit</w> <w n="47.5">s</w>’<w n="47.6">embarque</w> <w n="47.7">à</w> <w n="47.8">toute</w> <w n="47.9">voile</w> <w n="47.10">et</w> <w n="47.11">parte</w>,</l>
						<l n="48" num="12.4"><space unit="char" quantity="8"></space><w n="48.1">Il</w> <w n="48.2">suffit</w> <w n="48.3">de</w> <w n="48.4">ce</w> <w n="48.5">peu</w> <w n="48.6">de</w> <w n="48.7">bleu</w>.</l>
					</lg>
					<lg n="13">
						<l n="49" num="13.1"><w n="49.1">Car</w>, <w n="49.2">voyage</w> <w n="49.3">au</w> <w n="49.4">long</w> <w n="49.5">cours</w> <w n="49.6">qui</w> <w n="49.7">jamais</w> <w n="49.8">ne</w> <w n="49.9">s</w>’<w n="49.10">achève</w>,</l>
						<l n="50" num="13.2"><space unit="char" quantity="8"></space><w n="50.1">Croisière</w> <w n="50.2">qui</w> <w n="50.3">ne</w> <w n="50.4">bouge</w> <w n="50.5">pas</w>,</l>
						<l n="51" num="13.3"><w n="51.1">Au</w> <w n="51.2">creux</w> <w n="51.3">du</w> <w n="51.4">clair</w> <w n="51.5">flacon</w>, <w n="51.6">mon</w> <w n="51.7">tout</w> <w n="51.8">petit</w> <w n="51.9">trois</w>-<w n="51.10">mâts</w></l>
						<l n="52" num="13.4"><space unit="char" quantity="8"></space><w n="52.1">Navigue</w> <w n="52.2">sans</w> <w n="52.3">fin</w> <w n="52.4">dans</w> <w n="52.5">le</w> <w n="52.6">rêve</w>.</l>
					</lg>
				</div></body></text></TEI>