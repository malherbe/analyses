<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">SOUFFLES DE TEMPÊTE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3271 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_6</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruesoufledetempete.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poèmes de guerre extraits de Souffles de tempêtes (1918)</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poetesses.blog4ever.com</publisher>
						<idno type="URL">https://poetesses.blog4ever.com/nouveaux-poemes-de-guerre-dans-souffles-de-tempete-1918</idno>
					</publicationStmt>
					<sourceDesc>
						<p>Textes complémentaires à ceux du site précédent</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title>SOUFFLES DE TEMPÊTE</title>
					<author>Lucie Delarue-Mardrus</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>E. Fasquelle</publisher>
						<date when="1918">1918</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1918">1918</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes manquants ( "Mes Bonnes routes" -> fin du recueil") ont été ajoutés à partir des poèmes du site : https://poetesses.blog4ever.com.</p>
				<p>Les textes des deux sources numériques comportent de nombreuses erreurs de numérisation ; une relecture complète s’impose.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les corrections métriques ont été faites à partir d’une version numérisée du texte effectuée par la Bibliothèque de Caen le mer.</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-01-31" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-01-31" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">I</head><head type="main_part">L’AUTOMNE A CHEVAL</head><div type="poem" key="DLR556">
					<head type="main">AU TROT</head>
					<lg n="1">
						<l n="1" num="1.1"><space unit="char" quantity="8"></space><w n="1.1">Sans</w> <w n="1.2">fin</w>, <w n="1.3">sans</w> <w n="1.4">trêve</w>, <w n="1.5">au</w> <w n="1.6">trot</w>,</l>
						<l n="2" num="1.2"><w n="2.1">J</w>’<w n="2.2">écarte</w> <w n="2.3">l</w>’<w n="2.4">automne</w> <w n="2.5">avec</w> <w n="2.6">mon</w> <w n="2.7">visage</w>,</l>
						<l n="3" num="1.3"><space unit="char" quantity="8"></space><w n="3.1">Fuis</w> <w n="3.2">la</w> <w n="3.3">branche</w> <w n="3.4">au</w> <w n="3.5">passage</w>,</l>
						<l n="4" num="1.4"><w n="4.1">A</w> <w n="4.2">cheval</w>, <w n="4.3">le</w> <w n="4.4">front</w> <w n="4.5">contre</w> <w n="4.6">le</w> <w n="4.7">garrot</w>,</l>
						<l n="5" num="1.5"><space unit="char" quantity="8"></space><w n="5.1">Sans</w> <w n="5.2">fin</w>, <w n="5.3">sans</w> <w n="5.4">trêve</w>, <w n="5.5">au</w> <w n="5.6">trot</w>.</l>
					</lg>
					<lg n="2">
						<l n="6" num="2.1"><space unit="char" quantity="8"></space><w n="6.1">Il</w> <w n="6.2">pleut</w>, <w n="6.3">ce</w> <w n="6.4">semble</w>, <w n="6.5">un</w> <w n="6.6">peu</w>.</l>
						<l n="7" num="2.2"><w n="7.1">Une</w> <w n="7.2">feuille</w> <w n="7.3">au</w> <w n="7.4">vol</w> <w n="7.5">me</w> <w n="7.6">fouette</w> <w n="7.7">la</w> <w n="7.8">joue</w>…</l>
						<l n="8" num="2.3"><space unit="char" quantity="8"></space><w n="8.1">Comme</w> <w n="8.2">octobre</w> <w n="8.3">est</w> <w n="8.4">en</w> <w n="8.5">feu</w> !</l>
						<l n="9" num="2.4"><w n="9.1">Sur</w> <w n="9.2">mon</w> <w n="9.3">cou</w> <w n="9.4">penché</w> <w n="9.5">le</w> <w n="9.6">bois</w> <w n="9.7">se</w> <w n="9.8">secoue</w>.</l>
						<l n="10" num="2.5"><space unit="char" quantity="8"></space><w n="10.1">Il</w> <w n="10.2">pleut</w>, <w n="10.3">ce</w> <w n="10.4">semble</w>, <w n="10.5">un</w> <w n="10.6">peu</w>.</l>
					</lg>
					<lg n="3">
						<l n="11" num="3.1"><space unit="char" quantity="8"></space><w n="11.1">Trottons</w> ! <w n="11.2">Rions</w> ! <w n="11.3">Allons</w> !</l>
						<l n="12" num="3.2"><w n="12.1">Par</w> <w n="12.2">bruyère</w> <w n="12.3">sèche</w> <w n="12.4">et</w> <w n="12.5">par</w> <w n="12.6">feuilles</w> <w n="12.7">mortes</w>,</l>
						<l n="13" num="3.3"><space unit="char" quantity="8"></space><w n="13.1">Couleurs</w> <w n="13.2">de</w> <w n="13.3">toutes</w> <w n="13.4">sortes</w>,</l>
						<l n="14" num="3.4"><w n="14.1">Fougères</w> <w n="14.2">d</w>’<w n="14.3">or</w> <w n="14.4">pâle</w> <w n="14.5">et</w> <w n="14.6">chênes</w> <w n="14.7">trop</w> <w n="14.8">blonds</w>,</l>
						<l n="15" num="3.5"><space unit="char" quantity="8"></space><w n="15.1">Trottons</w>, <w n="15.2">rions</w>, <w n="15.3">allons</w> !…</l>
					</lg>
				</div></body></text></TEI>