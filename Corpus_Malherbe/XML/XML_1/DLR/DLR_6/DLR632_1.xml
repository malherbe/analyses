<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">SOUFFLES DE TEMPÊTE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3271 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_6</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruesoufledetempete.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poèmes de guerre extraits de Souffles de tempêtes (1918)</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poetesses.blog4ever.com</publisher>
						<idno type="URL">https://poetesses.blog4ever.com/nouveaux-poemes-de-guerre-dans-souffles-de-tempete-1918</idno>
					</publicationStmt>
					<sourceDesc>
						<p>Textes complémentaires à ceux du site précédent</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title>SOUFFLES DE TEMPÊTE</title>
					<author>Lucie Delarue-Mardrus</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>E. Fasquelle</publisher>
						<date when="1918">1918</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1918">1918</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes manquants ( "Mes Bonnes routes" -> fin du recueil") ont été ajoutés à partir des poèmes du site : https://poetesses.blog4ever.com.</p>
				<p>Les textes des deux sources numériques comportent de nombreuses erreurs de numérisation ; une relecture complète s’impose.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les corrections métriques ont été faites à partir d’une version numérisée du texte effectuée par la Bibliothèque de Caen le mer.</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-01-31" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-01-31" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">VII</head><head type="main_part">LA GUERRE</head><div type="poem" key="DLR632">
					<head type="main">AU JARDIN DE MAI</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Le</w> <w n="1.2">printemps</w>, <w n="1.3">au</w> <w n="1.4">jardin</w> <w n="1.5">de</w> <w n="1.6">mai</w>, <w n="1.7">nous</w> <w n="1.8">faisait</w> <w n="1.9">fête</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Et</w> <w n="2.2">nos</w> <w n="2.3">pieds</w> <w n="2.4">étaient</w> <w n="2.5">prêts</w> <w n="2.6">pour</w> <w n="2.7">la</w> <w n="2.8">course</w> <w n="2.9">et</w> <w n="2.10">le</w> <w n="2.11">bond</w>.</l>
						<l n="3" num="1.3"><space unit="char" quantity="8"></space><w n="3.1">Des</w> <w n="3.2">arbres</w> <w n="3.3">entiers</w> <w n="3.4">sentaient</w> <w n="3.5">bon</w>.</l>
						<l n="4" num="1.4"><space unit="char" quantity="8"></space><w n="4.1">Nous</w> <w n="4.2">en</w> <w n="4.3">pensions</w> <w n="4.4">perdre</w> <w n="4.5">la</w> <w n="4.6">tête</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Nous</w> <w n="5.2">allions</w>, <w n="5.3">nous</w> <w n="5.4">tenant</w> <w n="5.5">la</w> <w n="5.6">main</w>, <w n="5.7">comme</w> <w n="5.8">deux</w> <w n="5.9">sœurs</w>,</l>
						<l n="6" num="2.2"><w n="6.1">Sans</w> <w n="6.2">presque</w> <w n="6.3">nous</w> <w n="6.4">parler</w>, <w n="6.5">à</w> <w n="6.6">grands</w> <w n="6.7">pas</w>, <w n="6.8">bouche</w> <w n="6.9">bée</w>.</l>
						<l n="7" num="2.3"><space unit="char" quantity="8"></space><w n="7.1">Une</w> <w n="7.2">frêle</w> <w n="7.3">pluie</w> <w n="7.4">est</w> <w n="7.5">tombée</w></l>
						<l n="8" num="2.4"><space unit="char" quantity="8"></space><w n="8.1">Qui</w> <w n="8.2">semblait</w> <w n="8.3">parfumée</w> <w n="8.4">aux</w> <w n="8.5">fleurs</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Les</w> <w n="9.2">marronniers</w> <w n="9.3">illuminés</w>, <w n="9.4">tout</w> <w n="9.5">blancs</w>, <w n="9.6">tout</w> <w n="9.7">roses</w>,</l>
						<l n="10" num="3.2"><w n="10.1">Portaient</w> <w n="10.2">leurs</w> <w n="10.3">fleurs</w> <w n="10.4">ainsi</w> <w n="10.5">que</w> <w n="10.6">de</w> <w n="10.7">légers</w> <w n="10.8">flambeaux</w>.</l>
						<l n="11" num="3.3"><space unit="char" quantity="8"></space><w n="11.1">Des</w> <w n="11.2">lilas</w> <w n="11.3">étaient</w> <w n="11.4">lourds</w> <w n="11.5">et</w> <w n="11.6">beaux</w>.</l>
						<l n="12" num="3.4"><space unit="char" quantity="8"></space><w n="12.1">Nous</w> <w n="12.2">y</w> <w n="12.3">fîmes</w> <w n="12.4">de</w> <w n="12.5">longues</w> <w n="12.6">pauses</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">L</w>’<w n="13.2">herbe</w> <w n="13.3">montait</w> <w n="13.4">à</w> <w n="13.5">l</w>’<w n="13.6">arbre</w>, <w n="13.7">et</w> <w n="13.8">l</w>’<w n="13.9">arbre</w> <w n="13.10">descendait</w></l>
						<l n="14" num="4.2"><w n="14.1">A</w> <w n="14.2">l</w>’<w n="14.3">herbe</w> ; <w n="14.4">et</w> <w n="14.5">les</w> <w n="14.6">gazons</w> <w n="14.7">berçaient</w> <w n="14.8">des</w> <w n="14.9">ombres</w> <w n="14.10">rondes</w>.</l>
						<l n="15" num="4.3"><space unit="char" quantity="8"></space><w n="15.1">Une</w> <w n="15.2">branche</w> <w n="15.3">basse</w> <w n="15.4">pendait</w>,</l>
						<l n="16" num="4.4"><space unit="char" quantity="8"></space><w n="16.1">Offrant</w> <w n="16.2">des</w> <w n="16.3">corolles</w> <w n="16.4">profondes</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Nous</w> <w n="17.2">disions</w> <w n="17.3">qu</w>’<w n="17.4">on</w> <w n="17.5">ne</w> <w n="17.6">peut</w> <w n="17.7">s</w>’<w n="17.8">habituer</w> <w n="17.9">jamais</w></l>
						<l n="18" num="5.2"><w n="18.1">Au</w> <w n="18.2">printemps</w>, <w n="18.3">cette</w> <w n="18.4">histoire</w> <w n="18.5">irréelle</w> <w n="18.6">de</w> <w n="18.7">fées</w>.</l>
						<l n="19" num="5.3"><space unit="char" quantity="8"></space><w n="19.1">Ivres</w>, <w n="19.2">par</w> <w n="19.3">vaux</w> <w n="19.4">et</w> <w n="19.5">par</w> <w n="19.6">sommets</w>,</l>
						<l n="20" num="5.4"><space unit="char" quantity="8"></space><w n="20.1">Nous</w> <w n="20.2">voulions</w> <w n="20.3">vivre</w> <w n="20.4">décoiffées</w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Pour</w> <w n="21.2">un</w> <w n="21.3">poète</w> <w n="21.4">vrai</w> <w n="21.5">qui</w>, <w n="21.6">passionnément</w>,</l>
						<l n="22" num="6.2"><w n="22.1">Parcourt</w> <w n="22.2">d</w>’<w n="22.3">un</w> <w n="22.4">pied</w> <w n="22.5">léger</w> <w n="22.6">la</w> <w n="22.7">saison</w> <w n="22.8">la</w> <w n="22.9">plus</w> <w n="22.10">belle</w>,</l>
						<l n="23" num="6.3"><space unit="char" quantity="8"></space><w n="23.1">C</w>’<w n="23.2">est</w> <w n="23.3">toujours</w> <w n="23.4">un</w> <w n="23.5">étonnement</w></l>
						<l n="24" num="6.4"><space unit="char" quantity="8"></space><w n="24.1">Que</w> <w n="24.2">la</w> <w n="24.3">rencontre</w> <w n="24.4">d</w>’<w n="24.5">une</w> <w n="24.6">ombelle</w>.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">C</w>’<w n="25.2">est</w> <w n="25.3">toujours</w> <w n="25.4">une</w> <w n="25.5">offrande</w>, <w n="25.6">et</w> <w n="25.7">c</w>’<w n="25.8">est</w> <w n="25.9">toujours</w> <w n="25.10">un</w> <w n="25.11">don</w></l>
						<l n="26" num="7.2"><w n="26.1">Qu</w>’<w n="26.2">un</w> <w n="26.3">nuage</w>, <w n="26.4">un</w> <w n="26.5">reflet</w>, <w n="26.6">un</w> <w n="26.7">rayon</w>, <w n="26.8">un</w> <w n="26.9">coin</w> <w n="26.10">sombre</w>,</l>
						<l n="27" num="7.3"><space unit="char" quantity="8"></space><w n="27.1">Et</w> <w n="27.2">c</w>’<w n="27.3">est</w> <w n="27.4">un</w> <w n="27.5">trésor</w> <w n="27.6">qu</w>’<w n="27.7">un</w> <w n="27.8">bourdon</w></l>
						<l n="28" num="7.4"><space unit="char" quantity="8"></space><w n="28.1">Qui</w> <w n="28.2">survole</w> <w n="28.3">l</w>’<w n="28.4">herbe</w>, <w n="28.5">dans</w> <w n="28.6">l</w>’<w n="28.7">ombre</w>.</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1"><w n="29.1">Nos</w> <w n="29.2">cœurs</w> <w n="29.3">battaient</w> <w n="29.4">de</w> <w n="29.5">joie</w>, <w n="29.6">ô</w> <w n="29.7">printemps</w> ! <w n="29.8">ô</w> <w n="29.9">printemps</w> !</l>
						<l n="30" num="8.2"><w n="30.1">Tout</w> <w n="30.2">était</w> <w n="30.3">bonne</w> <w n="30.4">odeur</w>, <w n="30.5">douce</w> <w n="30.6">couleur</w>, <w n="30.7">musique</w>,</l>
						<l n="31" num="8.3"><space unit="char" quantity="8"></space><w n="31.1">Jeunesse</w>, <w n="31.2">allégresse</w> <w n="31.3">physique</w>.</l>
						<l n="32" num="8.4"><space unit="char" quantity="8"></space>‒ <w n="32.1">Mais</w> <w n="32.2">nos</w> <w n="32.3">fronts</w> <w n="32.4">étaient</w> <w n="32.5">mécontents</w>.</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1"><w n="33.1">Que</w> <w n="33.2">fait</w>-<w n="33.3">on</w> <w n="33.4">quelque</w> <w n="33.5">part</w>, <w n="33.6">qu</w>’<w n="33.7">invente</w>-<w n="33.8">t</w>-<w n="33.9">on</w> <w n="33.10">d</w>’<w n="33.11">horrible</w>,</l>
						<l n="34" num="9.2"><w n="34.1">Dans</w> <w n="34.2">le</w> <w n="34.3">même</w> <w n="34.4">moment</w> <w n="34.5">qu</w>’<w n="34.6">au</w> <w n="34.7">sein</w> <w n="34.8">du</w> <w n="34.9">printemps</w> <w n="34.10">clair</w></l>
						<l n="35" num="9.3"><space unit="char" quantity="8"></space><w n="35.1">Le</w> <w n="35.2">bourgeon</w> <w n="35.3">le</w> <w n="35.4">plus</w> <w n="35.5">insensible</w></l>
						<l n="36" num="9.4"><space unit="char" quantity="8"></space><w n="36.1">Cède</w> <w n="36.2">à</w> <w n="36.3">la</w> <w n="36.4">caresse</w> <w n="36.5">de</w> <w n="36.6">l</w>’<w n="36.7">air</w> ?</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1"><w n="37.1">La</w> <w n="37.2">nature</w> <w n="37.3">fleurit</w>, <w n="37.4">bourdonne</w>, <w n="37.5">encense</w>, <w n="37.6">bouge</w> ;</l>
						<l n="38" num="10.2"><w n="38.1">Partout</w> <w n="38.2">brille</w>, <w n="38.3">innocent</w>, <w n="38.4">le</w> <w n="38.5">paradis</w> <w n="38.6">de</w> <w n="38.7">mai</w> ;</l>
						<l n="39" num="10.3"><space unit="char" quantity="8"></space><w n="39.1">Le</w> <w n="39.2">sol</w> <w n="39.3">même</w> <w n="39.4">espère</w> <w n="39.5">et</w> <w n="39.6">promet</w>.</l>
						<l n="40" num="10.4"><space unit="char" quantity="8"></space>…<w n="40.1">Sauf</w> <w n="40.2">aux</w> <w n="40.3">lieux</w> <w n="40.4">où</w> <w n="40.5">la</w> <w n="40.6">terre</w> <w n="40.7">est</w> <w n="40.8">rouge</w>.</l>
					</lg>
					<lg n="11">
						<l n="41" num="11.1"><w n="41.1">Un</w> <w n="41.2">épouvantement</w> <w n="41.3">barre</w> <w n="41.4">chaque</w> <w n="41.5">horizon</w>.</l>
						<l n="42" num="11.2"><w n="42.1">Le</w> <w n="42.2">monstre</w> <w n="42.3">de</w> <w n="42.4">la</w> <w n="42.5">guerre</w> <w n="42.6">est</w> <w n="42.7">là</w>, <w n="42.8">qui</w> <w n="42.9">boit</w> <w n="42.10">et</w> <w n="42.11">mange</w>.</l>
						<l n="43" num="11.3"><space unit="char" quantity="8"></space><w n="43.1">A</w> <w n="43.2">deux</w> <w n="43.3">pas</w> <w n="43.4">de</w> <w n="43.5">notre</w> <w n="43.6">maison</w>,</l>
						<l n="44" num="11.4"><space unit="char" quantity="8"></space><w n="44.1">La</w> <w n="44.2">face</w> <w n="44.3">de</w> <w n="44.4">l</w>’<w n="44.5">Europe</w> <w n="44.6">change</w>.</l>
					</lg>
					<lg n="12">
						<l n="45" num="12.1"><w n="45.1">Du</w> <w n="45.2">fond</w> <w n="45.3">de</w> <w n="45.4">l</w>’<w n="45.5">avenir</w>, <w n="45.6">au</w> <w n="45.7">bruit</w> <w n="45.8">sourd</w> <w n="45.9">des</w> <w n="45.10">canons</w>,</l>
						<l n="46" num="12.2"><w n="46.1">Voici</w> <w n="46.2">venir</w> <w n="46.3">des</w> <w n="46.4">temps</w> <w n="46.5">qui</w> <w n="46.6">ne</w> <w n="46.7">sont</w> <w n="46.8">plus</w> <w n="46.9">les</w> <w n="46.10">nôtres</w>,</l>
						<l n="47" num="12.3"><space unit="char" quantity="8"></space><w n="47.1">Notre</w> <w n="47.2">époque</w> <w n="47.3">sombre</w>, <w n="47.4">avec</w> <w n="47.5">d</w>’<w n="47.6">autres</w>,</l>
						<l n="48" num="12.4"><space unit="char" quantity="8"></space><w n="48.1">Dans</w> <w n="48.2">l</w>’<w n="48.3">Histoire</w> <w n="48.4">pleine</w> <w n="48.5">de</w> <w n="48.6">noms</w>.</l>
					</lg>
					<lg n="13">
						<l n="49" num="13.1"><w n="49.1">Mais</w> <w n="49.2">le</w> <w n="49.3">jardin</w> <w n="49.4">en</w> <w n="49.5">fleurs</w> <w n="49.6">est</w> <w n="49.7">plus</w> <w n="49.8">fort</w> <w n="49.9">que</w> <w n="49.10">la</w> <w n="49.11">guerre</w>.</l>
						<l n="50" num="13.2"><w n="50.1">Tandis</w> <w n="50.2">que</w> <w n="50.3">tout</w> <w n="50.4">s</w>’<w n="50.5">en</w> <w n="50.6">va</w>, <w n="50.7">pourquoi</w> <w n="50.8">fait</w>-<w n="50.9">il</w> <w n="50.10">si</w> <w n="50.11">beau</w> ?</l>
						<l n="51" num="13.3"><space unit="char" quantity="8"></space><w n="51.1">Ce</w> <w n="51.2">merle</w> <w n="51.3">ne</w> <w n="51.4">peut</w>-<w n="51.5">il</w> <w n="51.6">se</w> <w n="51.7">taire</w></l>
						<l n="52" num="13.4"><space unit="char" quantity="8"></space><w n="52.1">Pendant</w> <w n="52.2">qu</w>’<w n="52.3">on</w> <w n="52.4">nous</w> <w n="52.5">couche</w> <w n="52.6">au</w> <w n="52.7">tombeau</w> ?</l>
					</lg>
					<lg n="14">
						<l n="53" num="14.1"><w n="53.1">Nous</w> <w n="53.2">mourons</w> ! <w n="53.3">Nous</w> <w n="53.4">mourons</w> ! <w n="53.5">Mais</w> <w n="53.6">le</w> <w n="53.7">printemps</w> <w n="53.8">embaume</w>.</l>
						<l n="54" num="14.2"><w n="54.1">On</w> <w n="54.2">tue</w> <w n="54.3">au</w> <w n="54.4">loin</w>, <w n="54.5">mais</w> <w n="54.6">les</w> <w n="54.7">oiseaux</w> <w n="54.8">sont</w> <w n="54.9">triomphants</w>.</l>
						<l n="55" num="14.3"><space unit="char" quantity="8"></space><w n="55.1">Nous</w> <w n="55.2">sommes</w> <w n="55.3">ruine</w> <w n="55.4">et</w> <w n="55.5">fantôme</w>,</l>
						<l n="56" num="14.4"><space unit="char" quantity="8"></space><w n="56.1">Et</w> <w n="56.2">nous</w> <w n="56.3">nous</w> <w n="56.4">sentons</w> <w n="56.5">des</w> <w n="56.6">enfants</w>.</l>
					</lg>
				</div></body></text></TEI>