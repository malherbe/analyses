<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">SOUFFLES DE TEMPÊTE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3271 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_6</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruesoufledetempete.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poèmes de guerre extraits de Souffles de tempêtes (1918)</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poetesses.blog4ever.com</publisher>
						<idno type="URL">https://poetesses.blog4ever.com/nouveaux-poemes-de-guerre-dans-souffles-de-tempete-1918</idno>
					</publicationStmt>
					<sourceDesc>
						<p>Textes complémentaires à ceux du site précédent</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title>SOUFFLES DE TEMPÊTE</title>
					<author>Lucie Delarue-Mardrus</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>E. Fasquelle</publisher>
						<date when="1918">1918</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1918">1918</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes manquants ( "Mes Bonnes routes" -> fin du recueil") ont été ajoutés à partir des poèmes du site : https://poetesses.blog4ever.com.</p>
				<p>Les textes des deux sources numériques comportent de nombreuses erreurs de numérisation ; une relecture complète s’impose.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les corrections métriques ont été faites à partir d’une version numérisée du texte effectuée par la Bibliothèque de Caen le mer.</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-01-31" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-01-31" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">II</head><head type="main_part">ADMIRATIONS</head><div type="poem" key="DLR571">
					<head type="main">YVONNE ASTRUC</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Une</w> <w n="1.2">main</w> <w n="1.3">traînant</w> <w n="1.4">l</w>’<w n="1.5">archet</w> <w n="1.6">long</w>,</l>
						<l n="2" num="1.2"><w n="2.1">L</w>’<w n="2.2">autre</w> <w n="2.3">en</w> <w n="2.4">transe</w> <w n="2.5">qui</w> <w n="2.6">vibre</w> <w n="2.7">et</w> <w n="2.8">bouge</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Être</w>, <w n="3.2">esprit</w>, <w n="3.3">âme</w> <w n="3.4">du</w> <w n="3.5">bois</w> <w n="3.6">rouge</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Martyre</w> <w n="4.2">dans</w> <w n="4.3">l</w>’<w n="4.4">état</w> <w n="4.5">second</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">O</w> <w n="5.2">figure</w> <w n="5.3">de</w> <w n="5.4">cathédrale</w>,</l>
						<l n="6" num="2.2"><w n="6.1">Courte</w> <w n="6.2">sur</w> <w n="6.3">des</w> <w n="6.4">pieds</w> <w n="6.5">écartés</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Au</w> <w n="7.2">bord</w> <w n="7.3">des</w> <w n="7.4">violons</w> <w n="7.5">hantés</w></l>
						<l n="8" num="2.4"><w n="8.1">Fais</w> <w n="8.2">flotter</w> <w n="8.3">une</w> <w n="8.4">tête</w> <w n="8.5">pâle</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Tes</w> <w n="9.2">yeux</w> <w n="9.3">fermés</w> <w n="9.4">de</w> <w n="9.5">séraphin</w></l>
						<l n="10" num="3.2"><w n="10.1">Passionné</w> <w n="10.2">de</w> <w n="10.3">la</w> <w n="10.4">musique</w></l>
						<l n="11" num="3.3"><w n="11.1">Font</w> <w n="11.2">physique</w> <w n="11.3">et</w> <w n="11.4">métaphysique</w></l>
						<l n="12" num="3.4"><w n="12.1">Notre</w> <w n="12.2">tourment</w> <w n="12.3">à</w> <w n="12.4">nous</w>, <w n="12.5">sans</w> <w n="12.6">fin</w>,</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Notre</w> <w n="13.2">tourment</w> <w n="13.3">devant</w> <w n="13.4">l</w>’<w n="13.5">orage</w></l>
						<l n="14" num="4.2"><w n="14.1">Bois</w> <w n="14.2">verni</w>, <w n="14.3">cordes</w> <w n="14.4">et</w> <w n="14.5">crins</w> <w n="14.6">clairs</w></l>
						<l n="15" num="4.3"><w n="15.1">Qui</w>, <w n="15.2">d</w>’<w n="15.3">après</w> <w n="15.4">la</w> <w n="15.5">sublime</w> <w n="15.6">page</w>,</l>
						<l n="16" num="4.4"><w n="16.1">Se</w> <w n="16.2">déchire</w> <w n="16.3">au</w> <w n="16.4">bout</w> <w n="16.5">de</w> <w n="16.6">tes</w> <w n="16.7">nerfs</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Autour</w> <w n="17.2">de</w> <w n="17.3">toi</w> <w n="17.4">sont</w> <w n="17.5">les</w> <w n="17.6">fantômes</w></l>
						<l n="18" num="5.2"><w n="18.1">De</w> <w n="18.2">ceux</w> <w n="18.3">dont</w> <w n="18.4">tu</w> <w n="18.5">te</w> <w n="18.6">fais</w> <w n="18.7">la</w> <w n="18.8">voix</w></l>
						<l n="19" num="5.3"><w n="19.1">De</w> <w n="19.2">par</w> <w n="19.3">ces</w> <w n="19.4">cordes</w> <w n="19.5">et</w> <w n="19.6">ce</w> <w n="19.7">bois</w></l>
						<l n="20" num="5.4"><w n="20.1">Qui</w> <w n="20.2">jettent</w> <w n="20.3">nos</w> <w n="20.4">fronts</w> <w n="20.5">dans</w> <w n="20.6">nos</w> <w n="20.7">paumes</w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Or</w>, <w n="21.2">salut</w> <w n="21.3">au</w> <w n="21.4">magistral</w> <w n="21.5">jeu</w></l>
						<l n="22" num="6.2"><w n="22.1">D</w>’<w n="22.2">où</w> <w n="22.3">montent</w> <w n="22.4">cri</w> <w n="22.5">de</w> <w n="22.6">joie</w> <w n="22.7">et</w> <w n="22.8">plainte</w>,</l>
						<l n="23" num="6.3"><w n="23.1">Salut</w> <w n="23.2">au</w> <w n="23.3">visage</w> <w n="23.4">de</w> <w n="23.5">sainte</w></l>
						<l n="24" num="6.4"><w n="24.1">Qui</w> <w n="24.2">souffre</w> <w n="24.3">et</w> <w n="24.4">pâme</w> <w n="24.5">pour</w> <w n="24.6">son</w> <w n="24.7">dieu</w>.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">Puis</w>, <w n="25.2">la</w> <w n="25.3">sainte</w> <w n="25.4">fougueuse</w> <w n="25.5">et</w> <w n="25.6">triste</w></l>
						<l n="26" num="7.2"><w n="26.1">Ayant</w> <w n="26.2">donné</w> <w n="26.3">tout</w> <w n="26.4">son</w> <w n="26.5">tourment</w>,</l>
						<l n="27" num="7.3"><w n="27.1">Que</w> <w n="27.2">soit</w> <w n="27.3">notre</w> <w n="27.4">violoniste</w></l>
						<l n="28" num="7.4"><w n="28.1">Une</w> <w n="28.2">femme</w>, <w n="28.3">tout</w> <w n="28.4">simplement</w>.</l>
					</lg>
				</div></body></text></TEI>