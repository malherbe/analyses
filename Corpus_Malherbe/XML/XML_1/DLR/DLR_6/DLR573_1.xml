<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">SOUFFLES DE TEMPÊTE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3271 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_6</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruesoufledetempete.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poèmes de guerre extraits de Souffles de tempêtes (1918)</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poetesses.blog4ever.com</publisher>
						<idno type="URL">https://poetesses.blog4ever.com/nouveaux-poemes-de-guerre-dans-souffles-de-tempete-1918</idno>
					</publicationStmt>
					<sourceDesc>
						<p>Textes complémentaires à ceux du site précédent</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title>SOUFFLES DE TEMPÊTE</title>
					<author>Lucie Delarue-Mardrus</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>E. Fasquelle</publisher>
						<date when="1918">1918</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1918">1918</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes manquants ( "Mes Bonnes routes" -> fin du recueil") ont été ajoutés à partir des poèmes du site : https://poetesses.blog4ever.com.</p>
				<p>Les textes des deux sources numériques comportent de nombreuses erreurs de numérisation ; une relecture complète s’impose.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les corrections métriques ont été faites à partir d’une version numérisée du texte effectuée par la Bibliothèque de Caen le mer.</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-01-31" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-01-31" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">II</head><head type="main_part">ADMIRATIONS</head><div type="poem" key="DLR573">
					<head type="main">EN L’HONNEUR DE J. S. BACH</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Jean</w> <w n="1.2">Sébastien</w> <w n="1.3">Bach</w>, <w n="1.4">père</w> <w n="1.5">de</w> <w n="1.6">la</w> <w n="1.7">musique</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Inépuisable</w> <w n="2.2">source</w> <w n="2.3">aux</w> <w n="2.4">murmures</w> <w n="2.5">sans</w> <w n="2.6">fin</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Ici</w> <w n="3.2">je</w> <w n="3.3">te</w> <w n="3.4">salue</w>, <w n="3.5">immortel</w> <w n="3.6">séraphin</w></l>
						<l n="4" num="1.4"><w n="4.1">Qui</w> <w n="4.2">ne</w> <w n="4.3">laisses</w> <w n="4.4">en</w> <w n="4.5">nous</w> <w n="4.6">rien</w> <w n="4.7">vivre</w> <w n="4.8">de</w> <w n="4.9">physique</w></l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Touchant</w> <w n="5.2">le</w> <w n="5.3">bleu</w> <w n="5.4">du</w> <w n="5.5">ciel</w>, <w n="5.6">toute</w> <w n="5.7">sculptée</w> <w n="5.8">à</w> <w n="5.9">jour</w>,</l>
						<l n="6" num="2.2"><w n="6.1">Ton</w> <w n="6.2">église</w> <w n="6.3">de</w> <w n="6.4">sons</w> <w n="6.5">s</w>’<w n="6.6">élève</w>, <w n="6.7">magistrale</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Et</w>, <w n="7.2">quand</w> <w n="7.3">nous</w> <w n="7.4">pénétrons</w> <w n="7.5">dans</w> <w n="7.6">cette</w> <w n="7.7">cathédrale</w>,</l>
						<l n="8" num="2.4"><w n="8.1">Nous</w> <w n="8.2">croyons</w> <w n="8.3">en</w> <w n="8.4">un</w> <w n="8.5">Dieu</w> <w n="8.6">de</w> <w n="8.7">justice</w> <w n="8.8">et</w> <w n="8.9">d</w>’<w n="8.10">amour</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><hi rend="ital"><w n="9.1">Credo</w></hi> !… <w n="9.2">chante</w> <w n="9.3">ton</w> <w n="9.4">œuvre</w> <w n="9.5">à</w> <w n="9.6">nos</w> <w n="9.7">siècles</w> <w n="9.8">athées</w>,</l>
						<l n="10" num="3.2"><hi rend="ital"><w n="10.1">Credo</w></hi> !… <w n="10.2">je</w> <w n="10.3">satisfais</w> <w n="10.4">poésie</w> <w n="10.5">et</w> <w n="10.6">raison</w> !</l>
						<l n="11" num="3.3"><hi rend="ital"><w n="11.1">Credo</w></hi> !… <w n="11.2">ma</w> <w n="11.3">moindre</w> <w n="11.4">page</w> <w n="11.5">élargit</w> <w n="11.6">l</w>’<w n="11.7">horizon</w>,</l>
						<l n="12" num="3.4"><hi rend="ital"><w n="12.1">Credo</w></hi> !… <w n="12.2">tout</w> <w n="12.3">l</w>’<w n="12.4">infini</w> <w n="12.5">s</w>’<w n="12.6">ouvre</w> <w n="12.7">sur</w> <w n="12.8">mes</w> <w n="12.9">portées</w> !</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">« <w n="13.1">Au</w> <w n="13.2">travers</w> <w n="13.3">de</w> <w n="13.4">l</w>’<w n="13.5">orchestre</w>, <w n="13.6">heureux</w>, <w n="13.7">souvent</w> <w n="13.8">joyeux</w>,</l>
						<l n="14" num="4.2"><w n="14.1">Sur</w> <w n="14.2">mon</w> <w n="14.3">clavecin</w> <w n="14.4">grêle</w> <w n="14.5">et</w> <w n="14.6">sur</w> <w n="14.7">mes</w> <w n="14.8">grandes</w> <w n="14.9">orgues</w>,</l>
						<l n="15" num="4.3"><w n="15.1">Je</w> <w n="15.2">chante</w>, <w n="15.3">et</w>, <w n="15.4">doucement</w>, <w n="15.5">je</w> <w n="15.6">fais</w> <w n="15.7">pleurer</w> <w n="15.8">vos</w> <w n="15.9">yeux</w>,</l>
						<l n="16" num="4.4"><w n="16.1">Pour</w> <w n="16.2">calmer</w> <w n="16.3">vos</w> <w n="16.4">chagrins</w>, <w n="16.5">vos</w> <w n="16.6">orgueils</w> <w n="16.7">et</w> <w n="16.8">vos</w> <w n="16.9">morgues</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">« <w n="17.1">Je</w> <w n="17.2">fais</w> <w n="17.3">lever</w> <w n="17.4">vos</w> <w n="17.5">fronts</w> <w n="17.6">vers</w> <w n="17.7">d</w>’<w n="17.8">autres</w> <w n="17.9">absolus</w>.</l>
						<l n="18" num="5.2"><w n="18.1">Loin</w> <w n="18.2">du</w> <w n="18.3">monde</w> <w n="18.4">discord</w> <w n="18.5">mon</w> <w n="18.6">souffle</w> <w n="18.7">vous</w> <w n="18.8">emporte</w>,</l>
						<l n="19" num="5.3"><w n="19.1">Venez</w> <w n="19.2">communier</w>, <w n="19.3">avec</w> <w n="19.4">des</w> <w n="19.5">cœurs</w> <w n="19.6">d</w>’<w n="19.7">élus</w>,</l>
						<l n="20" num="5.4"><w n="20.1">A</w> <w n="20.2">ma</w> <w n="20.3">perfection</w> <w n="20.4">sereine</w>, <w n="20.5">chaste</w> <w n="20.6">et</w> <w n="20.7">forte</w> !</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1">« <w n="21.1">Les</w> <w n="21.2">miracles</w> <w n="21.3">des</w> <w n="21.4">saints</w>, <w n="21.5">chaque</w> <w n="21.6">jour</w> <w n="21.7">je</w> <w n="21.8">les</w> <w n="21.9">fais</w> !</l>
						<l n="22" num="6.2"><w n="22.1">Les</w> <w n="22.2">âmes</w>, <w n="22.3">à</w> <w n="22.4">ma</w> <w n="22.5">voix</w>, <w n="22.6">toutes</w> <w n="22.7">deviennent</w> <w n="22.8">belles</w>.</l>
						<l n="23" num="6.3"><w n="23.1">Les</w> <w n="23.2">épaules</w>, <w n="23.3">soudain</w>, <w n="23.4">ne</w> <w n="23.5">sentent</w> <w n="23.6">plus</w> <w n="23.7">le</w> <w n="23.8">faix</w></l>
						<l n="24" num="6.4"><w n="24.1">De</w> <w n="24.2">vivre</w>, <w n="24.3">mais</w> <w n="24.4">le</w> <w n="24.5">poids</w> <w n="24.6">formidable</w> <w n="24.7">des</w> <w n="24.8">ailes</w>.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1">« <w n="25.1">Venez</w> <w n="25.2">tous</w> ! <w n="25.3">Le</w> <w n="25.4">chemin</w> <w n="25.5">de</w> <w n="25.6">la</w> <w n="25.7">vie</w> <w n="25.8">est</w> <w n="25.9">peu</w> <w n="25.10">sûr</w>,</l>
						<l n="26" num="7.2"><w n="26.1">Prenez</w>-<w n="26.2">moi</w> <w n="26.3">par</w> <w n="26.4">la</w> <w n="26.5">main</w> <w n="26.6">pour</w> <w n="26.7">passer</w> <w n="26.8">le</w> <w n="26.9">portique</w></l>
						<l n="27" num="7.3"><w n="27.1">Qui</w> <w n="27.2">mène</w> <w n="27.3">par</w> <w n="27.4">le</w> <w n="27.5">juste</w> <w n="27.6">et</w> <w n="27.7">l</w>’<w n="27.8">abstrait</w> <w n="27.9">et</w> <w n="27.10">le</w> <w n="27.11">pur</w></l>
						<l n="28" num="7.4"><w n="28.1">Vers</w> <w n="28.2">la</w> <w n="28.3">divine</w> <w n="28.4">horreur</w> <w n="28.5">de</w> <w n="28.6">la</w> <w n="28.7">mathématique</w> !…</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1"><w n="29.1">Et</w> <w n="29.2">nous</w>, <w n="29.3">nous</w> <w n="29.4">répondons</w> : « <w n="29.5">O</w> <w n="29.6">Bach</w> ! <w n="29.7">nous</w> <w n="29.8">voulons</w> <w n="29.9">bien</w> !</l>
						<l n="30" num="8.2"><w n="30.1">Veuille</w> <w n="30.2">nous</w> <w n="30.3">prosterner</w> <w n="30.4">dans</w> <w n="30.5">une</w> <w n="30.6">extase</w> <w n="30.7">austère</w></l>
						<l n="31" num="8.3"><w n="31.1">Fais</w> <w n="31.2">taire</w> <w n="31.3">autour</w> <w n="31.4">de</w> <w n="31.5">nous</w> <w n="31.6">tout</w> <w n="31.7">ce</w> <w n="31.8">qui</w> <w n="31.9">doit</w> <w n="31.10">se</w> <w n="31.11">taire</w>,</l>
						<l n="32" num="8.4"><w n="32.1">Sois</w> <w n="32.2">notre</w> <w n="32.3">conseiller</w>, <w n="32.4">sois</w> <w n="32.5">notre</w> <w n="32.6">ange</w> <w n="32.7">gardien</w> !</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1">« <w n="33.1">Le</w> <w n="33.2">monde</w> <w n="33.3">où</w> <w n="33.4">nous</w> <w n="33.5">vivons</w> <w n="33.6">se</w> <w n="33.7">meurt</w> <w n="33.8">d</w>’<w n="33.9">être</w> <w n="33.10">si</w> <w n="33.11">triste</w>,</l>
						<l n="34" num="9.2"><w n="34.1">Accorde</w> <w n="34.2">nos</w> <w n="34.3">esprits</w> <w n="34.4">à</w> <w n="34.5">ton</w> <w n="34.6">sublime</w> <hi rend="ital"><w n="34.7">la</w></hi> !</l>
						<l n="35" num="9.3"><w n="35.1">Viens</w> <w n="35.2">nous</w> <w n="35.3">persuader</w>, <w n="35.4">ô</w> <w n="35.5">Bach</w> ! <w n="35.6">de</w> <w n="35.7">l</w>’<w n="35.8">au</w>-<w n="35.9">delà</w></l>
						<l n="36" num="9.4"><w n="36.1">Auquel</w> <w n="36.2">nous</w> <w n="36.3">voulons</w> <w n="36.4">croire</w> <w n="36.5">et</w> <w n="36.6">qui</w>, <w n="36.7">peut</w>-<w n="36.8">être</w>, <w n="36.9">existe</w>… »</l>
					</lg>
				</div></body></text></TEI>