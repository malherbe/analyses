<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">SOUFFLES DE TEMPÊTE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3271 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_6</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruesoufledetempete.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poèmes de guerre extraits de Souffles de tempêtes (1918)</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poetesses.blog4ever.com</publisher>
						<idno type="URL">https://poetesses.blog4ever.com/nouveaux-poemes-de-guerre-dans-souffles-de-tempete-1918</idno>
					</publicationStmt>
					<sourceDesc>
						<p>Textes complémentaires à ceux du site précédent</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title>SOUFFLES DE TEMPÊTE</title>
					<author>Lucie Delarue-Mardrus</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>E. Fasquelle</publisher>
						<date when="1918">1918</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1918">1918</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes manquants ( "Mes Bonnes routes" -> fin du recueil") ont été ajoutés à partir des poèmes du site : https://poetesses.blog4ever.com.</p>
				<p>Les textes des deux sources numériques comportent de nombreuses erreurs de numérisation ; une relecture complète s’impose.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les corrections métriques ont été faites à partir d’une version numérisée du texte effectuée par la Bibliothèque de Caen le mer.</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-01-31" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-01-31" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">VI</head><head type="main_part">PROPHÉTIQUES</head><div type="poem" key="DLR614">
					<head type="main">RÊVERIE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Riches</w> <w n="1.2">de</w> <w n="1.3">doux</w> <w n="1.4">trésors</w>, <w n="1.5">l</w>’<w n="1.6">espérance</w> <w n="1.7">et</w> <w n="1.8">l</w>’<w n="1.9">amour</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Les</w> <w n="2.2">foules</w> <w n="2.3">vont</w> <w n="2.4">bêlant</w> <w n="2.5">en</w> <w n="2.6">troupeaux</w> <w n="2.7">nostalgiques</w>.</l>
						<l n="3" num="1.3"><w n="3.1">Quelque</w> <w n="3.2">soit</w> <w n="3.3">leur</w> <w n="3.4">destin</w> <w n="3.5">les</w> <w n="3.6">humains</w> <w n="3.7">sont</w> <w n="3.8">tragiques</w></l>
						<l n="4" num="1.4"><space unit="char" quantity="8"></space><w n="4.1">Ils</w> <w n="4.2">savent</w> <w n="4.3">qu</w>’<w n="4.4">ils</w> <w n="4.5">mourront</w> <w n="4.6">un</w> <w n="4.7">jour</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">La</w> <w n="5.2">plus</w> <w n="5.3">insigne</w> <w n="5.4">brute</w> <w n="5.5">est</w> <w n="5.6">encore</w> <w n="5.7">divine</w></l>
						<l n="6" num="2.2"><w n="6.1">Près</w> <w n="6.2">des</w> <w n="6.3">férocités</w> <w n="6.4">qui</w> <w n="6.5">mènent</w> <w n="6.6">l</w>’<w n="6.7">animal</w>.</l>
						<l n="7" num="2.3"><w n="7.1">Le</w> <w n="7.2">malaise</w> <w n="7.3">de</w> <w n="7.4">vivre</w> <w n="7.5">est</w> <w n="7.6">là</w>. <w n="7.7">Rien</w> <w n="7.8">n</w>’<w n="7.9">est</w> <w n="7.10">banal</w>.</l>
						<l n="8" num="2.4"><space unit="char" quantity="8"></space><w n="8.1">Une</w> <w n="8.2">âme</w> <w n="8.3">dans</w> <w n="8.4">tous</w> <w n="8.5">se</w> <w n="8.6">devine</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Ils</w> <w n="9.2">travaillent</w>. <w n="9.3">Parfois</w> <w n="9.4">ils</w> <w n="9.5">ont</w> <w n="9.6">de</w> <w n="9.7">la</w> <w n="9.8">gaieté</w> !</l>
						<l n="10" num="3.2"><w n="10.1">Poésie</w>, <w n="10.2">art</w>, <w n="10.3">musique</w>… <w n="10.4">Ils</w> <w n="10.5">chantent</w> ! <w n="10.6">Quel</w> <w n="10.7">courage</w> !</l>
						<l n="11" num="3.3"><w n="11.1">Ils</w> <w n="11.2">ont</w> <w n="11.3">l</w>’<w n="11.4">ambition</w> <w n="11.5">et</w> <w n="11.6">la</w> <w n="11.7">méchanceté</w></l>
						<l n="12" num="3.4"><space unit="char" quantity="8"></space><w n="12.1">Parmi</w> <w n="12.2">les</w> <w n="12.3">malheurs</w> <w n="12.4">qui</w> <w n="12.5">font</w> <w n="12.6">rage</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Ils</w> <w n="13.2">grouillent</w> <w n="13.3">en</w> <w n="13.4">enfer</w>, <w n="13.5">et</w> <w n="13.6">beaucoup</w> <w n="13.7">croient</w> <w n="13.8">en</w> <w n="13.9">Dieu</w>.</l>
						<l n="14" num="4.2"><w n="14.1">L</w>’<w n="14.2">espèce</w>, <w n="14.3">obstinément</w>, <w n="14.4">répare</w> <w n="14.5">ses</w> <w n="14.6">guenilles</w> :</l>
						<l n="15" num="4.3"><w n="15.1">Sachant</w> <w n="15.2">ce</w> <w n="15.3">qu</w>’<w n="15.4">est</w> <w n="15.5">la</w> <w n="15.6">vie</w> <w n="15.7">ils</w> <w n="15.8">refont</w> <w n="15.9">des</w> <w n="15.10">familles</w>,</l>
						<l n="16" num="4.4"><space unit="char" quantity="8"></space><w n="16.1">Et</w> <w n="16.2">les</w> <w n="16.3">suicidés</w> <w n="16.4">sont</w> <w n="16.5">peu</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Comme</w> <w n="17.2">s</w>’<w n="17.3">il</w> <w n="17.4">n</w>’<w n="17.5">était</w> <w n="17.6">pas</w> <w n="17.7">de</w> <w n="17.8">tremblement</w> <w n="17.9">de</w> <w n="17.10">terre</w></l>
						<l n="18" num="5.2"><w n="18.1">Et</w> <w n="18.2">comme</w> <w n="18.3">si</w> <w n="18.4">la</w> <w n="18.5">foudre</w> <w n="18.6">avait</w> <w n="18.7">perdu</w> <w n="18.8">ses</w> <w n="18.9">traits</w>,</l>
						<l n="19" num="5.3"><w n="19.1">Parmi</w> <w n="19.2">peste</w> <w n="19.3">et</w> <w n="19.4">famine</w> <w n="19.5">ils</w> <w n="19.6">inventent</w> <w n="19.7">la</w> <w n="19.8">guerre</w>,</l>
						<l n="20" num="5.4"><space unit="char" quantity="8"></space><w n="20.1">Et</w> <w n="20.2">s</w>’<w n="20.3">en</w> <w n="20.4">vont</w> <w n="20.5">répétant</w> : <hi rend="ital"><w n="20.6">progrès</w></hi>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">C</w>’<w n="21.2">est</w> <w n="21.3">ainsi</w> ! <w n="21.4">Naturelle</w> <w n="21.5">ou</w> <w n="21.6">qu</w>’<w n="21.7">ils</w> <w n="21.8">l</w>’<w n="21.9">aient</w> <w n="21.10">inventée</w></l>
						<l n="22" num="6.2"><w n="22.1">Tant</w> <w n="22.2">de</w> <w n="22.3">douleur</w> <w n="22.4">les</w> <w n="22.5">tord</w> <w n="22.6">jusqu</w>’<w n="22.7">au</w> <w n="22.8">jour</w> <w n="22.9">du</w> <w n="22.10">trépas</w></l>
						<l n="23" num="6.3"><w n="23.1">Qu</w>« <w n="23.2">on</w> <w n="23.3">se</w> <w n="23.4">demande</w> <w n="23.5">si</w>, <w n="23.6">la</w> <w n="23.7">leur</w> <w n="23.8">ayant</w> <w n="23.9">ôtée</w>,</l>
						<l n="24" num="6.4"><space unit="char" quantity="8"></space><w n="24.1">Ils</w> <w n="24.2">ne</w> <w n="24.3">la</w> <w n="24.4">réclameront</w> <w n="24.5">pas</w>.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">O</w> <w n="25.2">bêtise</w> <w n="25.3">et</w> <w n="25.4">génie</w> ! <w n="25.5">Humains</w>, <w n="25.6">mes</w> <w n="25.7">pauvres</w> <w n="25.8">frères</w></l>
						<l n="26" num="7.2"><w n="26.1">Que</w> <w n="26.2">j</w>’<w n="26.3">aime</w> <w n="26.4">et</w> <w n="26.5">que</w> <w n="26.6">je</w> <w n="26.7">hais</w> <w n="26.8">si</w> <w n="26.9">passionnément</w>,</l>
						<l n="27" num="7.3"><w n="27.1">Laissez</w>-<w n="27.2">moi</w> <w n="27.3">suivre</w> <w n="27.4">au</w> <w n="27.5">loin</w> <w n="27.6">vos</w> <w n="27.7">cohortes</w> <w n="27.8">amères</w></l>
						<l n="28" num="7.4"><space unit="char" quantity="8"></space><w n="28.1">D</w>’<w n="28.2">un</w> <w n="28.3">éternel</w> <w n="28.4">étonnement</w>.</l>
					</lg>
					<closer>
						<dateline>
							<placeName>Paris</placeName>,
							<date when="1914">20 juin 1914</date>
						</dateline>
					</closer>
				</div></body></text></TEI>