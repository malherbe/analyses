<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">SOUFFLES DE TEMPÊTE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3271 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_6</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruesoufledetempete.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poèmes de guerre extraits de Souffles de tempêtes (1918)</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poetesses.blog4ever.com</publisher>
						<idno type="URL">https://poetesses.blog4ever.com/nouveaux-poemes-de-guerre-dans-souffles-de-tempete-1918</idno>
					</publicationStmt>
					<sourceDesc>
						<p>Textes complémentaires à ceux du site précédent</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title>SOUFFLES DE TEMPÊTE</title>
					<author>Lucie Delarue-Mardrus</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>E. Fasquelle</publisher>
						<date when="1918">1918</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1918">1918</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes manquants ( "Mes Bonnes routes" -> fin du recueil") ont été ajoutés à partir des poèmes du site : https://poetesses.blog4ever.com.</p>
				<p>Les textes des deux sources numériques comportent de nombreuses erreurs de numérisation ; une relecture complète s’impose.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les corrections métriques ont été faites à partir d’une version numérisée du texte effectuée par la Bibliothèque de Caen le mer.</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-01-31" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-01-31" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">II</head><head type="main_part">ADMIRATIONS</head><div type="poem" key="DLR568">
					<head type="main">RÉDEMPTION DU SOIR</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Contour</w> <w n="1.2">presque</w> <w n="1.3">immatériel</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Debout</w> <w n="2.2">dans</w> <w n="2.3">l</w>’<w n="2.4">île</w>, <w n="2.5">Notre</w> <w n="2.6">Dame</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Sur</w> <w n="3.2">le</w> <w n="3.3">rouge</w> <w n="3.4">couchant</w> <w n="3.5">du</w> <w n="3.6">ciel</w></l>
						<l n="4" num="1.4"><w n="4.1">S</w>’<w n="4.2">élève</w> <w n="4.3">en</w> <w n="4.4">priant</w> <w n="4.5">comme</w> <w n="4.6">une</w> <w n="4.7">âme</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Et</w> <w n="5.2">son</w> <w n="5.3">long</w> <w n="5.4">reflet</w> <w n="5.5">dérangé</w>,</l>
						<l n="6" num="2.2"><w n="6.1">Seconde</w> <w n="6.2">inverse</w> <w n="6.3">cathédrale</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Dans</w> <w n="7.2">les</w> <w n="7.3">remous</w> <w n="7.4">du</w> <w n="7.5">fleuve</w> <w n="7.6">pâle</w></l>
						<l n="8" num="2.4"><w n="8.1">Ne</w> <w n="8.2">cesse</w> <w n="8.3">jamais</w> <w n="8.4">de</w> <w n="8.5">bouger</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">C</w>’<w n="9.2">est</w> <w n="9.3">l</w>’<w n="9.4">heure</w> <w n="9.5">des</w> <w n="9.6">grandes</w> <w n="9.7">féeries</w>,</l>
						<l n="10" num="3.2"><w n="10.1">Des</w> <w n="10.2">premières</w> <w n="10.3">ombres</w> <w n="10.4">errant</w></l>
						<l n="11" num="3.3"><w n="11.1">Et</w> <w n="11.2">des</w> <w n="11.3">lumières</w> <w n="11.4">se</w> <w n="11.5">mirant</w>,</l>
						<l n="12" num="3.4"><w n="12.1">Rouges</w> <w n="12.2">et</w> <w n="12.3">vertes</w> <w n="12.4">verreries</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Comme</w> <w n="13.2">nul</w> <w n="13.3">ne</w> <w n="13.4">regarde</w> <w n="13.5">rien</w></l>
						<l n="14" num="4.2"><w n="14.1">Sinon</w> <w n="14.2">moi</w>, <w n="14.3">rêveuse</w> <w n="14.4">qui</w> <w n="14.5">passe</w>,</l>
						<l n="15" num="4.3"><w n="15.1">Tout</w> <w n="15.2">ce</w> <w n="15.3">spectacle</w> <w n="15.4">m</w>’<w n="15.5">appartient</w></l>
						<l n="16" num="4.4"><w n="16.1">Avec</w> <w n="16.2">son</w> <w n="16.3">drame</w>, <w n="16.4">avec</w> <w n="16.5">sa</w> <w n="16.6">grâce</w>,</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Et</w> <w n="17.2">les</w> <w n="17.3">yeux</w> <w n="17.4">tournés</w> <w n="17.5">vers</w> <w n="17.6">cela</w>,</l>
						<l n="18" num="5.2"><w n="18.1">Je</w> <w n="18.2">dis</w> <w n="18.3">à</w> <w n="18.4">Notre</w>-<w n="18.5">Dame</w> <w n="18.6">noire</w>,</l>
						<l n="19" num="5.3"><w n="19.1">Aux</w> <w n="19.2">feux</w> <w n="19.3">mouvants</w>, <w n="19.4">à</w> <w n="19.5">l</w>’<w n="19.6">eau</w> <w n="19.7">d</w>’<w n="19.8">ivoire</w> :</l>
						<l n="20" num="5.4">« <w n="20.1">O</w> <w n="20.2">beauté</w> <w n="20.3">du</w> <w n="20.4">soir</w>, <w n="20.5">je</w> <w n="20.6">suis</w> <w n="20.7">là</w> ! »</l>
					</lg>
				</div></body></text></TEI>