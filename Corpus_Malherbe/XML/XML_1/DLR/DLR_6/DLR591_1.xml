<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">SOUFFLES DE TEMPÊTE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3271 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_6</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruesoufledetempete.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poèmes de guerre extraits de Souffles de tempêtes (1918)</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poetesses.blog4ever.com</publisher>
						<idno type="URL">https://poetesses.blog4ever.com/nouveaux-poemes-de-guerre-dans-souffles-de-tempete-1918</idno>
					</publicationStmt>
					<sourceDesc>
						<p>Textes complémentaires à ceux du site précédent</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title>SOUFFLES DE TEMPÊTE</title>
					<author>Lucie Delarue-Mardrus</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>E. Fasquelle</publisher>
						<date when="1918">1918</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1918">1918</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes manquants ( "Mes Bonnes routes" -> fin du recueil") ont été ajoutés à partir des poèmes du site : https://poetesses.blog4ever.com.</p>
				<p>Les textes des deux sources numériques comportent de nombreuses erreurs de numérisation ; une relecture complète s’impose.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les corrections métriques ont été faites à partir d’une version numérisée du texte effectuée par la Bibliothèque de Caen le mer.</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-01-31" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-01-31" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">IV</head><head type="main_part">CHEVAUX DE LA MER</head><div type="poem" key="DLR591">
					<head type="main">ORAISON</head>
					<lg n="1">
						<l n="1" num="1.1"><space unit="char" quantity="8"></space><w n="1.1">Diurne</w> <w n="1.2">mer</w>, <w n="1.3">ma</w> <w n="1.4">grande</w> <w n="1.5">pâle</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Toi</w> <w n="2.2">dont</w> <w n="2.3">je</w> <w n="2.4">fus</w> <w n="2.5">toujours</w> <w n="2.6">l</w>’<w n="2.7">enfant</w> <w n="2.8">passionné</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Mer</w>, <w n="3.2">si</w> <w n="3.3">de</w> <w n="3.4">ta</w> <w n="3.5">rumeur</w> <w n="3.6">qui</w> <w n="3.7">murmure</w> <w n="3.8">ou</w> <w n="3.9">qui</w> <w n="3.10">râle</w></l>
						<l n="4" num="1.4"><space unit="char" quantity="8"></space><w n="4.1">Je</w> <w n="4.2">sens</w> <w n="4.3">que</w> <w n="4.4">mon</w> <w n="4.5">esprit</w> <w n="4.6">est</w> <w n="4.7">né</w>,</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><space unit="char" quantity="8"></space><w n="5.1">Mer</w> <w n="5.2">nocturne</w>, <w n="5.3">ma</w> <w n="5.4">grande</w> <w n="5.5">noire</w>,</l>
						<l n="6" num="2.2"><w n="6.1">Toi</w> <w n="6.2">dont</w> <w n="6.3">je</w> <w n="6.4">suis</w> <w n="6.5">la</w> <w n="6.6">fille</w> <w n="6.7">éperdue</w> <w n="6.8">à</w> <w n="6.9">jamais</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Seule</w> <w n="7.2">force</w> <w n="7.3">vivante</w> <w n="7.4">à</w> <w n="7.5">qui</w> <w n="7.6">je</w> <w n="7.7">me</w> <w n="7.8">soumets</w>,</l>
						<l n="8" num="2.4"><space unit="char" quantity="8"></space><w n="8.1">Mer</w>, <w n="8.2">ô</w> <w n="8.3">mer</w>, <w n="8.4">toute</w> <w n="8.5">mon</w> <w n="8.6">histoire</w>,</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><space unit="char" quantity="8"></space><w n="9.1">Veuille</w> <w n="9.2">vivre</w> <w n="9.3">toujours</w> <w n="9.4">en</w> <w n="9.5">moi</w>,</l>
						<l n="10" num="3.2"><w n="10.1">Informe</w> <w n="10.2">et</w> <w n="10.3">si</w> <w n="10.4">précise</w>, <w n="10.5">exacte</w> <w n="10.6">et</w> <w n="10.7">si</w> <w n="10.8">fantasque</w>,</l>
						<l n="11" num="3.3"><w n="11.1">Veuille</w> <w n="11.2">que</w> <w n="11.3">ton</w> <w n="11.4">grand</w> <w n="11.5">vent</w> <w n="11.6">délivre</w> <w n="11.7">de</w> <w n="11.8">son</w> <w n="11.9">masque</w></l>
						<l n="12" num="3.4"><space unit="char" quantity="8"></space><w n="12.1">Ma</w> <w n="12.2">face</w> <w n="12.3">qui</w> <w n="12.4">pleure</w> <w n="12.5">d</w>’<w n="12.6">émoi</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><space unit="char" quantity="8"></space><w n="13.1">Mer</w>, <w n="13.2">que</w> <w n="13.3">parfois</w> <w n="13.4">la</w> <w n="13.5">lune</w> <w n="13.6">argente</w>,</l>
						<l n="14" num="4.2"><w n="14.1">Veuille</w> <w n="14.2">ne</w> <w n="14.3">point</w> <w n="14.4">cesser</w> <w n="14.5">d</w>’<w n="14.6">inspirer</w> <w n="14.7">mon</w> <w n="14.8">tourment</w>.</l>
						<l n="15" num="4.3"><w n="15.1">Désaltère</w> <w n="15.2">et</w> <w n="15.3">nourris</w> <w n="15.4">de</w> <w n="15.5">ta</w> <w n="15.6">splendeur</w> <w n="15.7">changeante</w></l>
						<l n="16" num="4.4"><space unit="char" quantity="8"></space><w n="16.1">Ta</w> <w n="16.2">visiteuse</w> <w n="16.3">au</w> <w n="16.4">cœur</w> <w n="16.5">d</w>’<w n="16.6">amant</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><space unit="char" quantity="8"></space><w n="17.1">Debout</w> <w n="17.2">parmi</w> <w n="17.3">l</w>’<w n="17.4">herbe</w> <w n="17.5">salée</w>,</l>
						<l n="18" num="5.2"><w n="18.1">J</w>’<w n="18.2">ouvre</w> <w n="18.3">vers</w> <w n="18.4">toi</w> <w n="18.5">mes</w> <w n="18.6">bras</w> <w n="18.7">somme</w> <w n="18.8">on</w> <w n="18.9">fait</w> <w n="18.10">pour</w> <w n="18.11">quelqu</w>’<w n="18.12">un</w>.</l>
						<l n="19" num="5.3"><w n="19.1">Si</w> <w n="19.2">loi</w>, <w n="19.3">et</w> <w n="19.4">si</w> <w n="19.5">longtemps</w> <w n="19.6">que</w> <w n="19.7">je</w> <w n="19.8">m</w>’<w n="19.9">en</w> <w n="19.10">sois</w> <w n="19.11">allée</w>,</l>
						<l n="20" num="5.4"><space unit="char" quantity="8"></space><w n="20.1">J</w>’<w n="20.2">ai</w> <w n="20.3">gardé</w> <w n="20.4">sur</w> <w n="20.5">moi</w> <w n="20.6">ton</w> <w n="20.7">embrun</w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><space unit="char" quantity="8"></space><w n="21.1">Beaucoup</w> <w n="21.2">plus</w> <w n="21.3">que</w> <w n="21.4">tout</w> <w n="21.5">ce</w> <w n="21.6">que</w> <w n="21.7">j</w>’<w n="21.8">aime</w></l>
						<l n="22" num="6.2"><w n="22.1">Je</w> <w n="22.2">t</w>’<w n="22.3">aime</w> ! <w n="22.4">Car</w> <w n="22.5">je</w> <w n="22.6">suis</w> <w n="22.7">de</w> <w n="22.8">ta</w> <w n="22.9">race</w>, <w n="22.10">la</w> <w n="22.11">mer</w> !</l>
						<l n="23" num="6.3"><w n="23.1">Oui</w>, <w n="23.2">comme</w> <w n="23.3">les</w> <w n="23.4">varechs</w> <w n="23.5">et</w> <w n="23.6">les</w> <w n="23.7">algues</w> <w n="23.8">que</w> <w n="23.9">sème</w></l>
						<l n="24" num="6.4"><space unit="char" quantity="8"></space><w n="24.1">Ton</w> <w n="24.2">reflux</w> <w n="24.3">sur</w> <w n="24.4">le</w> <w n="24.5">sable</w> <w n="24.6">amer</w>.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><space unit="char" quantity="8"></space><w n="25.1">Ivre</w> <w n="25.2">de</w> <w n="25.3">toi</w>, <w n="25.4">vers</w> <w n="25.5">toi</w> <w n="25.6">je</w> <w n="25.7">crie</w>.</l>
						<l n="26" num="7.2"><w n="26.1">La</w> <w n="26.2">mer</w> ! <w n="26.3">La</w> <w n="26.4">mer</w> ! <w n="26.5">A</w> <w n="26.6">moi</w> ! <w n="26.7">Je</w> <w n="26.8">te</w> <w n="26.9">veux</w>, <w n="26.10">élément</w>,</l>
						<l n="27" num="7.3"><w n="27.1">Simplicité</w>, <w n="27.2">mystère</w>, <w n="27.3">ampleur</w>, <w n="27.4">rythme</w>, <w n="27.5">furie</w>,</l>
						<l n="28" num="7.4"><space unit="char" quantity="8"></space><w n="28.1">Éternel</w> <w n="28.2">renouvellement</w> !</l>
					</lg>
				</div></body></text></TEI>