<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">SOUFFLES DE TEMPÊTE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3271 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_6</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruesoufledetempete.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poèmes de guerre extraits de Souffles de tempêtes (1918)</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poetesses.blog4ever.com</publisher>
						<idno type="URL">https://poetesses.blog4ever.com/nouveaux-poemes-de-guerre-dans-souffles-de-tempete-1918</idno>
					</publicationStmt>
					<sourceDesc>
						<p>Textes complémentaires à ceux du site précédent</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title>SOUFFLES DE TEMPÊTE</title>
					<author>Lucie Delarue-Mardrus</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>E. Fasquelle</publisher>
						<date when="1918">1918</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1918">1918</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes manquants ( "Mes Bonnes routes" -> fin du recueil") ont été ajoutés à partir des poèmes du site : https://poetesses.blog4ever.com.</p>
				<p>Les textes des deux sources numériques comportent de nombreuses erreurs de numérisation ; une relecture complète s’impose.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les corrections métriques ont été faites à partir d’une version numérisée du texte effectuée par la Bibliothèque de Caen le mer.</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-01-31" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-01-31" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">II</head><head type="main_part">ADMIRATIONS</head><div type="poem" key="DLR565">
					<head type="main">PAUVRE MORCEAU DE BOIS…</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Pauvre</w> <w n="1.2">morceau</w> <w n="1.3">de</w> <w n="1.4">bois</w> <w n="1.5">qui</w> <w n="1.6">n</w>’<w n="1.7">était</w> <w n="1.8">qu</w>’<w n="1.9">une</w> <w n="1.10">bûche</w></l>
						<l n="2" num="1.2"><space unit="char" quantity="8"></space><w n="2.1">Qu</w>’<w n="2.2">on</w> <w n="2.3">avait</w> <w n="2.4">mise</w> <w n="2.5">dans</w> <w n="2.6">un</w> <w n="2.7">coin</w></l>
						<l n="3" num="1.3"><w n="3.1">Au</w> <w n="3.2">fond</w> <w n="3.3">du</w> <w n="3.4">cellier</w> <w n="3.5">noir</w> <w n="3.6">où</w> <w n="3.7">mainte</w> <w n="3.8">chose</w> <w n="3.9">juche</w></l>
						<l n="4" num="1.4"><space unit="char" quantity="8"></space><w n="4.1">Parmi</w> <w n="4.2">des</w> <w n="4.3">pommes</w> <w n="4.4">et</w> <w n="4.5">du</w> <w n="4.6">foin</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">A</w> <w n="5.2">toi</w>, <w n="5.3">ce</w> <w n="5.4">soir</w>, <w n="5.5">l</w>’<w n="5.6">honneur</w> <w n="5.7">de</w> <w n="5.8">l</w>’<w n="5.9">ample</w> <w n="5.10">cheminée</w>,</l>
						<l n="6" num="2.2"><space unit="char" quantity="8"></space><w n="6.1">A</w> <w n="6.2">toi</w> <w n="6.3">les</w> <w n="6.4">grâces</w> <w n="6.5">du</w> <w n="6.6">salon</w> !</l>
						<l n="7" num="2.3"><w n="7.1">Tu</w> <w n="7.2">vas</w> <w n="7.3">brûler</w> <w n="7.4">afin</w> <w n="7.5">que</w> <w n="7.6">le</w> <w n="7.7">temps</w> <w n="7.8">soit</w> <w n="7.9">moins</w> <w n="7.10">long</w>,</l>
						<l n="8" num="2.4"><space unit="char" quantity="8"></space><w n="8.1">Plein</w> <w n="8.2">des</w> <w n="8.3">tristesses</w> <w n="8.4">de</w> <w n="8.5">l</w>’<w n="8.6">année</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Tu</w> <w n="9.2">vivais</w> <w n="9.3">autrefois</w> <w n="9.4">à</w> <w n="9.5">l</w>’<w n="9.6">arbre</w>, <w n="9.7">en</w> <w n="9.8">quelque</w> <w n="9.9">pré</w>,</l>
						<l n="10" num="3.2"><space unit="char" quantity="8"></space><w n="10.1">Le</w> <w n="10.2">printemps</w> <w n="10.3">aux</w> <w n="10.4">bourgeons</w> <w n="10.5">sans</w> <w n="10.6">nombre</w>,</l>
						<l n="11" num="3.3"><w n="11.1">L</w>’<w n="11.2">épais</w> <w n="11.3">été</w>, <w n="11.4">l</w>’<w n="11.5">automne</w> <w n="11.6">élégant</w> <w n="11.7">et</w> <w n="11.8">doré</w>,</l>
						<l n="12" num="3.4"><space unit="char" quantity="8"></space><w n="12.1">L</w>’<w n="12.2">hiver</w> <w n="12.3">tout</w> <w n="12.4">blanc</w> <w n="12.5">sous</w> <w n="12.6">un</w> <w n="12.7">ciel</w> <w n="12.8">sombre</w>,</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Toutes</w> <w n="13.2">les</w> <w n="13.3">quatre</w> <w n="13.4">au</w> <w n="13.5">vent</w> <w n="13.6">tu</w> <w n="13.7">berçais</w> <w n="13.8">les</w> <w n="13.9">saisons</w>,</l>
						<l n="14" num="4.2"><space unit="char" quantity="8"></space><w n="14.1">Deux</w> <w n="14.2">vertes</w>, <w n="14.3">la</w> <w n="14.4">jaune</w> <w n="14.5">et</w> <w n="14.6">la</w> <w n="14.7">blanche</w>.</l>
						<l n="15" num="4.3"><w n="15.1">Tu</w> <w n="15.2">savais</w> <w n="15.3">les</w> <w n="15.4">secrets</w> <w n="15.5">innocents</w> <w n="15.6">d</w>’<w n="15.7">une</w> <w n="15.8">branche</w>,</l>
						<l n="16" num="4.4"><space unit="char" quantity="8"></space><w n="16.1">Qui</w> <w n="16.2">valent</w> <w n="16.3">bien</w> <w n="16.4">ceux</w> <w n="16.5">des</w> <w n="16.6">maisons</w>…</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Ce</w> <w n="17.2">soir</w> <w n="17.3">tu</w> <w n="17.4">vas</w> <w n="17.5">brûler</w>, <w n="17.6">ô</w> <w n="17.7">vieille</w> <w n="17.8">chose</w> <w n="17.9">morte</w> !</l>
						<l n="18" num="5.2"><space unit="char" quantity="8"></space><w n="18.1">Les</w> <w n="18.2">secrets</w> <w n="18.3">ne</w> <w n="18.4">sont</w> <w n="18.5">pas</w> <w n="18.6">finis</w>.</l>
						<l n="19" num="5.3"><w n="19.1">Tu</w> <w n="19.2">savais</w> <w n="19.3">qu</w>’<w n="19.4">une</w> <w n="19.5">branche</w>, <w n="19.6">une</w> <w n="19.7">fois</w> <w n="19.8">sèche</w>, <w n="19.9">porte</w></l>
						<l n="20" num="5.4"><space unit="char" quantity="8"></space><w n="20.1">Autre</w> <w n="20.2">chose</w> <w n="20.3">encor</w> <w n="20.4">que</w> <w n="20.5">des</w> <w n="20.6">nids</w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Il</w> <w n="21.2">va</w> <w n="21.3">naître</w> <w n="21.4">de</w> <w n="21.5">toi</w> <w n="21.6">qui</w> <w n="21.7">n</w>’<w n="21.8">es</w> <w n="21.9">plus</w> <w n="21.10">rien</w>, <w n="21.11">des</w> <w n="21.12">flammes</w> !</l>
						<l n="22" num="6.2"><space unit="char" quantity="8"></space><w n="22.1">Beaucoup</w> <w n="22.2">plus</w> <w n="22.3">belles</w> <w n="22.4">que</w> <w n="22.5">des</w> <w n="22.6">fleurs</w>,</l>
						<l n="23" num="6.3"><w n="23.1">Les</w> <w n="23.2">flammes</w>, <w n="23.3">purs</w> <w n="23.4">esprits</w>, <w n="23.5">elfes</w>, <w n="23.6">démons</w>, <w n="23.7">dieux</w>, <w n="23.8">âmes</w>,</l>
						<l n="24" num="6.4"><space unit="char" quantity="8"></space><w n="24.1">Miracles</w> <w n="24.2">de</w> <w n="24.3">toutes</w> <w n="24.4">couleurs</w> !</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">Quand</w> <w n="25.2">tu</w> <w n="25.3">ne</w> <w n="25.4">seras</w> <w n="25.5">plus</w> <w n="25.6">qu</w>’<w n="25.7">un</w> <w n="25.8">vieux</w> <w n="25.9">reste</w> <w n="25.10">de</w> <w n="25.11">souche</w></l>
						<l n="26" num="7.2"><space unit="char" quantity="8"></space><w n="26.1">Où</w> <w n="26.2">rougeoie</w> <w n="26.3">encore</w> <w n="26.4">un</w> <w n="26.5">tison</w>,</l>
						<l n="27" num="7.3"><w n="27.1">Tu</w> <w n="27.2">deviendras</w> <w n="27.3">pareille</w> <w n="27.4">au</w> <w n="27.5">sous</w>-<w n="27.6">bois</w> <w n="27.7">où</w> <w n="27.8">se</w> <w n="27.9">couche</w></l>
						<l n="28" num="7.4"><space unit="char" quantity="8"></space><w n="28.1">Un</w> <w n="28.2">soleil</w> <w n="28.3">rouge</w> <w n="28.4">à</w> <w n="28.5">l</w>’<w n="28.6">horizon</w>.</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1"><w n="29.1">Lorsque</w> <w n="29.2">la</w> <w n="29.3">flamme</w> <w n="29.4">aura</w> <w n="29.5">terminer</w> <w n="29.6">ses</w> <w n="29.7">désastres</w>,</l>
						<l n="30" num="8.2"><space unit="char" quantity="8"></space><w n="30.1">Tu</w> <w n="30.2">seras</w> <w n="30.3">quelque</w> <w n="30.4">chose</w> <w n="30.5">encor</w>.</l>
						<l n="31" num="8.3"><w n="31.1">Le</w> <w n="31.2">tison</w>, <w n="31.3">en</w> <w n="31.4">mourant</w>, <w n="31.5">fait</w> <w n="31.6">de</w> <w n="31.7">tout</w> <w n="31.8">petits</w> <w n="31.9">astres</w>,</l>
						<l n="32" num="8.4"><space unit="char" quantity="8"></space><w n="32.1">Étincelles</w> <w n="32.2">d</w>’<w n="32.3">argent</w> <w n="32.4">et</w> <w n="32.5">d</w>’<w n="32.6">or</w>.</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1"><w n="33.1">Ainsi</w> <w n="33.2">tu</w> <w n="33.3">contenais</w> <w n="33.4">ces</w> <w n="33.5">elfes</w> <w n="33.6">et</w> <w n="33.7">ces</w> <w n="33.8">fées</w>,</l>
						<l n="34" num="9.2"><space unit="char" quantity="8"></space><w n="34.1">Ces</w> <w n="34.2">étoiles</w>, <w n="34.3">ce</w> <w n="34.4">pourpre</w> <w n="34.5">soir</w>,</l>
						<l n="35" num="9.3"><w n="35.1">Tout</w> <w n="35.2">ce</w> <w n="35.3">qui</w> <w n="35.4">va</w> <w n="35.5">sortir</w> <w n="35.6">de</w> <w n="35.7">tes</w> <w n="35.8">fibres</w> <w n="35.9">chauffées</w>,</l>
						<l n="36" num="9.4"><space unit="char" quantity="8"></space><w n="36.1">Pauvre</w> <w n="36.2">bûche</w> <w n="36.3">du</w> <w n="36.4">cellier</w> <w n="36.5">noir</w> !</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1"><w n="37.1">Le</w> <w n="37.2">feu</w> <w n="37.3">qui</w> <w n="37.4">va</w> <w n="37.5">monter</w>, <w n="37.6">le</w> <w n="37.7">feu</w> <w n="37.8">qui</w> <w n="37.9">va</w> <w n="37.10">descendre</w></l>
						<l n="38" num="10.2"><space unit="char" quantity="8"></space><w n="38.1">Sera</w> <w n="38.2">l</w>’<w n="38.3">ouvrage</w> <w n="38.4">de</w> <w n="38.5">ton</w> <w n="38.6">corps</w>,</l>
						<l n="39" num="10.3"><w n="39.1">Et</w> <w n="39.2">tu</w> <w n="39.3">te</w> <w n="39.4">survivras</w>, <w n="39.5">après</w> <w n="39.6">toutes</w> <w n="39.7">tes</w> <w n="39.8">morts</w>,</l>
						<l n="40" num="10.4"><space unit="char" quantity="8"></space><w n="40.1">Dans</w> <w n="40.2">la</w> <w n="40.3">noblesse</w> <w n="40.4">de</w> <w n="40.5">la</w> <w n="40.6">cendre</w>.</l>
					</lg>
				</div></body></text></TEI>