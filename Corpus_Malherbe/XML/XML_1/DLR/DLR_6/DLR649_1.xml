<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">SOUFFLES DE TEMPÊTE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3271 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_6</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruesoufledetempete.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poèmes de guerre extraits de Souffles de tempêtes (1918)</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poetesses.blog4ever.com</publisher>
						<idno type="URL">https://poetesses.blog4ever.com/nouveaux-poemes-de-guerre-dans-souffles-de-tempete-1918</idno>
					</publicationStmt>
					<sourceDesc>
						<p>Textes complémentaires à ceux du site précédent</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title>SOUFFLES DE TEMPÊTE</title>
					<author>Lucie Delarue-Mardrus</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>E. Fasquelle</publisher>
						<date when="1918">1918</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1918">1918</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes manquants ( "Mes Bonnes routes" -> fin du recueil") ont été ajoutés à partir des poèmes du site : https://poetesses.blog4ever.com.</p>
				<p>Les textes des deux sources numériques comportent de nombreuses erreurs de numérisation ; une relecture complète s’impose.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les corrections métriques ont été faites à partir d’une version numérisée du texte effectuée par la Bibliothèque de Caen le mer.</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-01-31" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-01-31" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">VII</head><head type="main_part">LA GUERRE</head><div type="poem" key="DLR649">
					<head type="main">AUX POMMES</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Belles</w> <w n="1.2">pommes</w> <w n="1.3">d</w>’<w n="1.4">octobre</w> <w n="1.5">aux</w> <w n="1.6">branches</w> <w n="1.7">des</w> <w n="1.8">pommiers</w>,</l>
						<l n="2" num="1.2"><space unit="char" quantity="8"></space><w n="2.1">Qui</w> <w n="2.2">tomberez</w> <w n="2.3">dans</w> <w n="2.4">les</w> <w n="2.5">paniers</w>,</l>
					</lg>
					<lg n="2">
						<l n="3" num="2.1"><w n="3.1">Brillant</w> <w n="3.2">enfantement</w> <w n="3.3">des</w> <w n="3.4">automnes</w> <w n="3.5">profondes</w>,</l>
						<l n="4" num="2.2"><space unit="char" quantity="8"></space><w n="4.1">Quand</w> <w n="4.2">les</w> <w n="4.3">feuilles</w> <w n="4.4">deviennent</w> <w n="4.5">blondes</w>,</l>
					</lg>
					<lg n="3">
						<l n="5" num="3.1"><w n="5.1">Vous</w> <w n="5.2">avez</w> <w n="5.3">souvenir</w> <w n="5.4">d</w>’<w n="5.5">avoir</w> <w n="5.6">été</w> <w n="5.7">des</w> <w n="5.8">fleurs</w>,</l>
						<l n="6" num="3.2"><space unit="char" quantity="8"></space><w n="6.1">O</w> <w n="6.2">mes</w> <w n="6.3">pommes</w> <w n="6.4">de</w> <w n="6.5">trois</w> <w n="6.6">couleurs</w>,</l>
					</lg>
					<lg n="4">
						<l n="7" num="4.1"><w n="7.1">Comme</w> <w n="7.2">je</w> <w n="7.3">me</w> <w n="7.4">souviens</w>, <w n="7.5">malgré</w> <w n="7.6">l</w>’<w n="7.7">expérience</w>,</l>
						<l n="8" num="4.2"><space unit="char" quantity="8"></space><w n="8.1">De</w> <w n="8.2">ma</w> <w n="8.3">fraîche</w> <w n="8.4">et</w> <w n="8.5">naïve</w> <w n="8.6">enfance</w>.</l>
					</lg>
					<lg n="5">
						<l n="9" num="5.1"><w n="9.1">Au</w> <w n="9.2">bout</w> <w n="9.3">de</w> <w n="9.4">tant</w> <w n="9.5">de</w> <w n="9.6">jours</w>, <w n="9.7">au</w> <w n="9.8">bout</w> <w n="9.9">de</w> <w n="9.10">tant</w> <w n="9.11">de</w> <w n="9.12">nuits</w>,</l>
						<l n="10" num="5.2"><space unit="char" quantity="8"></space><w n="10.1">Voici</w> <w n="10.2">que</w> <w n="10.3">vous</w> <w n="10.4">êtes</w> <w n="10.5">des</w> <w n="10.6">fruits</w>.</l>
					</lg>
					<lg n="6">
						<l n="11" num="6.1"><w n="11.1">Oh</w>! <w n="11.2">parmi</w> <w n="11.3">vos</w> <w n="11.4">pommiers</w> <w n="11.5">épais</w> <w n="11.6">comme</w> <w n="11.7">des</w> <w n="11.8">tentes</w></l>
						<l n="12" num="6.2"><space unit="char" quantity="8"></space><w n="12.1">Combien</w> <w n="12.2">vous</w> <w n="12.3">êtes</w> <w n="12.4">importantes</w> !</l>
					</lg>
					<lg n="7">
						<l n="13" num="7.1"><w n="13.1">Mères</w> <w n="13.2">du</w> <w n="13.3">cidre</w> <w n="13.4">neuf</w> <w n="13.5">et</w> <w n="13.6">du</w> <w n="13.7">calvados</w> <w n="13.8">vieux</w>,</l>
						<l n="14" num="7.2"><space unit="char" quantity="8"></space><w n="14.1">Boisson</w> <w n="14.2">de</w> <w n="14.3">ma</w> <w n="14.4">race</w> <w n="14.5">aux</w> <w n="14.6">yeux</w> <w n="14.7">bleus</w>,</l>
					</lg>
					<lg n="8">
						<l n="15" num="8.1"><w n="15.1">Votre</w> <w n="15.2">suc</w> <w n="15.3">précieux</w> <w n="15.4">est</w> <w n="15.5">en</w> <w n="15.6">vous</w> <w n="15.7">qui</w> <w n="15.8">se</w> <w n="15.9">cache</w></l>
						<l n="16" num="8.2"><space unit="char" quantity="8"></space><w n="16.1">Comme</w> <w n="16.2">le</w> <w n="16.3">lait</w> <w n="16.4">gonfle</w> <w n="16.5">la</w> <w n="16.6">vache</w>.</l>
					</lg>
					<lg n="9">
						<l n="17" num="9.1"><w n="17.1">Pommes</w> <w n="17.2">qui</w> <w n="17.3">fermentez</w> <w n="17.4">dans</w> <w n="17.5">l</w>’<w n="17.6">âme</w> <w n="17.7">de</w> <w n="17.8">nos</w> <w n="17.9">gens</w></l>
						<l n="18" num="9.2"><space unit="char" quantity="8"></space><w n="18.1">Fermés</w>, <w n="18.2">moqueurs</w>, <w n="18.3">intelligents</w>,</l>
					</lg>
					<lg n="10">
						<l n="19" num="10.1"><w n="19.1">Pommes</w> <w n="19.2">de</w> <w n="19.3">cette</w> <w n="19.4">année</w>, <w n="19.5">à</w> <w n="19.6">quand</w> <w n="19.7">l</w>’<w n="19.8">ivresse</w> <w n="19.9">claire</w></l>
						<l n="20" num="10.2"><space unit="char" quantity="8"></space><w n="20.1">De</w> <w n="20.2">nos</w> <w n="20.3">gas</w> <w n="20.4">partis</w> <w n="20.5">pour</w> <w n="20.6">la</w> <w n="20.7">guerre</w>,</l>
					</lg>
					<lg n="11">
						<l n="21" num="11.1"><w n="21.1">De</w> <w n="21.2">nos</w> <w n="21.3">gas</w>… <w n="21.4">ou</w> <w n="21.5">du</w> <w n="21.6">moins</w> <w n="21.7">de</w> <w n="21.8">ceux</w> <w n="21.9">qui</w> <w n="21.10">reviendront</w></l>
						<l n="22" num="11.2"><space unit="char" quantity="8"></space><w n="22.1">S</w>’<w n="22.2">asseoir</w> <w n="22.3">autour</w> <w n="22.4">du</w> <w n="22.5">tonneau</w> <w n="22.6">rond</w> ?</l>
					</lg>
				</div></body></text></TEI>