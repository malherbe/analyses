<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">SOUFFLES DE TEMPÊTE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3271 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_6</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruesoufledetempete.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poèmes de guerre extraits de Souffles de tempêtes (1918)</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poetesses.blog4ever.com</publisher>
						<idno type="URL">https://poetesses.blog4ever.com/nouveaux-poemes-de-guerre-dans-souffles-de-tempete-1918</idno>
					</publicationStmt>
					<sourceDesc>
						<p>Textes complémentaires à ceux du site précédent</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title>SOUFFLES DE TEMPÊTE</title>
					<author>Lucie Delarue-Mardrus</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>E. Fasquelle</publisher>
						<date when="1918">1918</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1918">1918</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes manquants ( "Mes Bonnes routes" -> fin du recueil") ont été ajoutés à partir des poèmes du site : https://poetesses.blog4ever.com.</p>
				<p>Les textes des deux sources numériques comportent de nombreuses erreurs de numérisation ; une relecture complète s’impose.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les corrections métriques ont été faites à partir d’une version numérisée du texte effectuée par la Bibliothèque de Caen le mer.</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-01-31" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-01-31" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">VII</head><head type="main_part">LA GUERRE</head><div type="poem" key="DLR633">
					<head type="main">CONSCIENCE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Si</w> <w n="1.2">je</w> <w n="1.3">cours</w> <w n="1.4">ici</w> <w n="1.5">les</w> <w n="1.6">chemins</w></l>
						<l n="2" num="1.2"><w n="2.1">Parmi</w> <w n="2.2">l</w>’<w n="2.3">ivresse</w> <w n="2.4">journalière</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Si</w> <w n="3.2">je</w> <w n="3.3">revois</w> <w n="3.4">la</w> <w n="3.5">vieille</w> <w n="3.6">ornière</w></l>
						<l n="4" num="1.4"><w n="4.1">Où</w> <w n="4.2">passèrent</w> <w n="4.3">mes</w> <w n="4.4">pas</w> <w n="4.5">gamins</w>,</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Si</w> <w n="5.2">mon</w> <w n="5.3">front</w> <w n="5.4">tristement</w> <w n="5.5">se</w> <w n="5.6">plie</w></l>
						<l n="6" num="2.2"><w n="6.1">Ou</w> <w n="6.2">se</w> <w n="6.3">redresse</w> <w n="6.4">de</w> <w n="6.5">plaisir</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Que</w> <w n="7.2">jamais</w> <w n="7.3">mon</w> <w n="7.4">âme</w> <w n="7.5">n</w>’<w n="7.6">oublie</w></l>
						<l n="8" num="2.4"><w n="8.1">A</w> <w n="8.2">qui</w> <w n="8.3">je</w> <w n="8.4">dois</w> <w n="8.5">ce</w> <w n="8.6">long</w> <w n="8.7">loisir</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">C</w>’<w n="9.2">est</w> <w n="9.3">vous</w>, <w n="9.4">gens</w> <w n="9.5">des</w> <w n="9.6">grandes</w> <w n="9.7">batailles</w>,</l>
						<l n="10" num="3.2"><w n="10.1">Gens</w> <w n="10.2">de</w> <w n="10.3">la</w> <w n="10.4">Marne</w> <w n="10.5">et</w> <w n="10.6">de</w> <w n="10.7">Verdun</w>,</l>
						<l n="11" num="3.3"><w n="11.1">Grands</w> <w n="11.2">et</w> <w n="11.3">petits</w>, <w n="11.4">de</w> <w n="11.5">toutes</w> <w n="11.6">tailles</w>,</l>
						<l n="12" num="3.4"><w n="12.1">Milliers</w> <w n="12.2">qui</w> <w n="12.3">ne</w> <w n="12.4">formez</w> <w n="12.5">plus</w> <w n="12.6">qu</w>’<w n="12.7">un</w>,</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">C</w>’<w n="13.2">est</w> <w n="13.3">vous</w> <w n="13.4">qui</w> <w n="13.5">nous</w> <w n="13.6">gardez</w> <w n="13.7">nos</w> <w n="13.8">villes</w></l>
						<l n="14" num="4.2"><w n="14.1">Et</w> <w n="14.2">nos</w> <w n="14.3">campagnes</w> <w n="14.4">à</w> <w n="14.5">tous</w> <w n="14.6">vents</w>,</l>
						<l n="15" num="4.3"><w n="15.1">C</w>’<w n="15.2">est</w> <w n="15.3">vous</w> <w n="15.4">les</w> <w n="15.5">morts</w>, <w n="15.6">vous</w> <w n="15.7">les</w> <w n="15.8">vivants</w></l>
						<l n="16" num="4.4"><w n="16.1">Qui</w> <w n="16.2">nous</w> <w n="16.3">faites</w> <w n="16.4">nos</w> <w n="16.5">jours</w> <w n="16.6">tranquilles</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Nous</w> <w n="17.2">devrions</w>, <w n="17.3">dans</w> <w n="17.4">nos</w> <w n="17.5">maisons</w>,</l>
						<l n="18" num="5.2"><w n="18.1">Toutes</w> <w n="18.2">les</w> <w n="18.3">fois</w> <w n="18.4">qu</w>’<w n="18.5">art</w> <w n="18.6">ou</w> <w n="18.7">musique</w></l>
						<l n="19" num="5.3"><w n="19.1">Bercent</w> <w n="19.2">notre</w> <w n="19.3">vie</w> <w n="19.4">extatique</w>,</l>
						<l n="20" num="5.4"><w n="20.1">Nous</w> <w n="20.2">tourner</w> <w n="20.3">vers</w> <w n="20.4">les</w> <w n="20.5">horizons</w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Beaux</w> <w n="21.2">rêves</w>, <w n="21.3">belles</w> <w n="21.4">promenades</w>,</l>
						<l n="22" num="6.2"><w n="22.1">Nous</w> <w n="22.2">devrions</w>, <w n="22.3">là</w>-<w n="22.4">bas</w>, <w n="22.5">ici</w>,</l>
						<l n="23" num="6.3"><w n="23.1">Penser</w> <w n="23.2">à</w> <w n="23.3">nos</w> <w n="23.4">grands</w> <w n="23.5">camarades</w>,</l>
						<l n="24" num="6.4"><w n="24.1">Et</w> <w n="24.2">sans</w> <w n="24.3">cesse</w> <w n="24.4">dire</w> : « <w n="24.5">Merci</w> ! »</l>
					</lg>
				</div></body></text></TEI>