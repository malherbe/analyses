<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRABANT</title>
				<title type="medium">Une édition électronique</title>
				<author key="CRM">
					<name>
						<forename>Maurice</forename>
						<surname>CARÊME</surname>
					</name>
					<date from="1899" to="1978">1899-1978</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3326 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CRM_1</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Brabant</title>
						<author>Maurice Carême</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LES ÉDITIONS OUVRIÈRES</publisher>
							<date when="1976">1976</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1967">1967</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-02-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-02-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CRM59" rhyme="none">
				<head type="main">QUAND IL PLEUVAIT</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Quand</w> <w n="1.2">il</w> <w n="1.3">pleuvait</w>, <w n="1.4">en</w> <w n="1.5">ce</w> <w n="1.6">temps</w>-<w n="1.7">là</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Je</w> <w n="2.2">ne</w> <w n="2.3">craignais</w> <w n="2.4">que</w> <w n="2.5">le</w> <w n="2.6">tonnerre</w>.</l>
					<l n="3" num="1.3">« <w n="3.1">Va</w> <w n="3.2">tout</w> <w n="3.3">fermer</w> », <w n="3.4">disait</w> <w n="3.5">mon</w> <w n="3.6">père</w>.</l>
				</lg>
				<lg n="2">
					<l n="4" num="2.1"><w n="4.1">C</w>’<w n="4.2">est</w> <w n="4.3">la</w> <w n="4.4">lucarne</w> <w n="4.5">du</w> <w n="4.6">grenier</w></l>
					<l n="5" num="2.2"><w n="5.1">Que</w> <w n="5.2">je</w> <w n="5.3">rabattais</w> <w n="5.4">la</w> <w n="5.5">dernière</w>.</l>
					<l n="6" num="2.3"><w n="6.1">L</w>’<w n="6.2">éclair</w> <w n="6.3">venait</w> <w n="6.4">s</w>’<w n="6.5">y</w> <w n="6.6">fracasser</w></l>
					<l n="7" num="2.4"><w n="7.1">En</w> <w n="7.2">grosses</w> <w n="7.3">gouttes</w> <w n="7.4">de</w> <w n="7.5">lumière</w>.</l>
				</lg>
				<lg n="3">
					<l n="8" num="3.1"><w n="8.1">Alors</w>, <w n="8.2">je</w> <w n="8.3">restais</w> <w n="8.4">là</w>, <w n="8.5">dans</w> <w n="8.6">l</w>’<w n="8.7">ombre</w>,</l>
					<l n="9" num="3.2"><w n="9.1">A</w> <w n="9.2">écouter</w> <w n="9.3">courir</w> <w n="9.4">la</w> <w n="9.5">pluie</w></l>
					<l n="10" num="3.3"><w n="10.1">Sur</w> <w n="10.2">le</w> <w n="10.3">toit</w> <w n="10.4">couvrant</w> <w n="10.5">les</w> <w n="10.6">colombes</w>.</l>
				</lg>
				<lg n="4">
					<l n="11" num="4.1"><w n="11.1">Elles</w> <w n="11.2">demeuraient</w> <w n="11.3">près</w> <w n="11.4">de</w> <w n="11.5">moi</w>,</l>
					<l n="12" num="4.2"><w n="12.1">Recroquevillées</w> <w n="12.2">dans</w> <w n="12.3">un</w> <w n="12.4">coin</w>,</l>
					<l n="13" num="4.3"><w n="13.1">Mettant</w>, <w n="13.2">un</w> <w n="13.3">peu</w> <w n="13.4">comme</w> <w n="13.5">une</w> <w n="13.6">main</w>,</l>
					<l n="14" num="4.4"><w n="14.1">Leur</w> <w n="14.2">aile</w> <w n="14.3">sur</w> <w n="14.4">leur</w> <w n="14.5">corps</w> <w n="14.6">étroit</w>.</l>
				</lg>
				<lg n="5">
					<l n="15" num="5.1"><w n="15.1">Et</w> <w n="15.2">je</w> <w n="15.3">pensais</w> <w n="15.4">à</w> <w n="15.5">la</w> <w n="15.6">colombe</w></l>
					<l n="16" num="5.2"><w n="16.1">Descendue</w> <w n="16.2">dans</w> <w n="16.3">un</w> <w n="16.4">éclair</w> <w n="16.5">blanc</w> ;</l>
					<l n="17" num="5.3"><w n="17.1">Et</w> <w n="17.2">je</w> <w n="17.3">me</w> <w n="17.4">surprenais</w>, <w n="17.5">tremblant</w>,</l>
				</lg>
				<lg n="6">
					<l n="18" num="6.1"><w n="18.1">A</w> <w n="18.2">tracer</w> <w n="18.3">vite</w> <w n="18.4">sur</w> <w n="18.5">mes</w> <w n="18.6">tempes</w>,</l>
					<l n="19" num="6.2"><w n="19.1">Coup</w> <w n="19.2">sur</w> <w n="19.3">coup</w>, <w n="19.4">trois</w> <w n="19.5">signes</w> <w n="19.6">de</w> <w n="19.7">croix</w></l>
					<l n="20" num="6.3"><w n="20.1">Lorsque</w> <w n="20.2">l</w>’<w n="20.3">éclair</w>, <w n="20.4">levant</w> <w n="20.5">sa</w> <w n="20.6">lampe</w>,</l>
					<l n="21" num="6.4"><w n="21.1">Se</w> <w n="21.2">dressait</w> <w n="21.3">soudain</w> <w n="21.4">devant</w> <w n="21.5">moi</w>.</l>
				</lg>
			</div></body></text></TEI>