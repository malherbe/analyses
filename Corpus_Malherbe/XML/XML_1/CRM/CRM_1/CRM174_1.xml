<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRABANT</title>
				<title type="medium">Une édition électronique</title>
				<author key="CRM">
					<name>
						<forename>Maurice</forename>
						<surname>CARÊME</surname>
					</name>
					<date from="1899" to="1978">1899-1978</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3326 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CRM_1</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Brabant</title>
						<author>Maurice Carême</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LES ÉDITIONS OUVRIÈRES</publisher>
							<date when="1976">1976</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1967">1967</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-02-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-02-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CRM174" rhyme="none">
				<head type="main">DE HAUTS NUAGES BLANCS…</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">De</w> <w n="1.2">hauts</w> <w n="1.3">nuages</w> <w n="1.4">blancs</w> <w n="1.5">passaient</w> <w n="1.6">nonchalamment</w>.</l>
					<l n="2" num="1.2"><w n="2.1">L</w>’<w n="2.2">horizon</w> <w n="2.3">ressemblait</w> <w n="2.4">à</w> <w n="2.5">un</w> <w n="2.6">château</w> <w n="2.7">superbe</w></l>
					<l n="3" num="1.3"><w n="3.1">Que</w> <w n="3.2">crénelaient</w> <w n="3.3">partout</w> <w n="3.4">de</w> <w n="3.5">tours</w> <w n="3.6">dorées</w>, <w n="3.7">les</w> <w n="3.8">gerbes</w>.</l>
				</lg>
				<lg n="2">
					<l n="4" num="2.1"><w n="4.1">Dans</w> <w n="4.2">les</w> <w n="4.3">trous</w> <w n="4.4">bleus</w> <w n="4.5">du</w> <w n="4.6">ciel</w>, <w n="4.7">un</w> <w n="4.8">soleil</w> <w n="4.9">implacable</w></l>
					<l n="5" num="2.2"><w n="5.1">Comme</w> <w n="5.2">un</w> <w n="5.3">couteau</w> <w n="5.4">ouvrant</w> <w n="5.5">sa</w> <w n="5.6">lame</w> <w n="5.7">sur</w> <w n="5.8">la</w> <w n="5.9">table</w></l>
					<l n="6" num="2.3"><w n="6.1">Brûlait</w> <w n="6.2">les</w> <w n="6.3">graminées</w>. <w n="6.4">Mais</w> <w n="6.5">dans</w> <w n="6.6">l</w>’<w n="6.7">ombre</w> <w n="6.8">jaunâtre</w></l>
				</lg>
				<lg n="3">
					<l n="7" num="3.1"><w n="7.1">Que</w> <w n="7.2">les</w> <w n="7.3">nuages</w> <w n="7.4">blancs</w> <w n="7.5">traînaient</w> <w n="7.6">sur</w> <w n="7.7">le</w> <w n="7.8">village</w>,</l>
					<l n="8" num="3.2"><w n="8.1">Une</w> <w n="8.2">telle</w> <w n="8.3">fraîcheur</w> <w n="8.4">surprenait</w> <w n="8.5">tout</w> <w n="8.6">à</w> <w n="8.7">coup</w></l>
					<l n="9" num="3.3"><w n="9.1">Qu</w>’<w n="9.2">on</w> <w n="9.3">entendait</w>, <w n="9.4">au</w> <w n="9.5">bois</w>, <w n="9.6">se</w> <w n="9.7">plaindre</w> <w n="9.8">les</w> <w n="9.9">coucous</w>.</l>
				</lg>
			</div></body></text></TEI>