<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRABANT</title>
				<title type="medium">Une édition électronique</title>
				<author key="CRM">
					<name>
						<forename>Maurice</forename>
						<surname>CARÊME</surname>
					</name>
					<date from="1899" to="1978">1899-1978</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3326 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CRM_1</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Brabant</title>
						<author>Maurice Carême</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LES ÉDITIONS OUVRIÈRES</publisher>
							<date when="1976">1976</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1967">1967</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-02-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-02-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CRM66">
				<head type="main">BROUILLARD</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Doucement</w>, <w n="1.2">le</w> <w n="1.3">brouillard</w> <w n="1.4">escamote</w> <w n="1.5">la</w> <w n="1.6">Dyle</w></l>
					<l n="2" num="1.2"><w n="2.1">Et</w> <w n="2.2">met</w> <w n="2.3">dans</w> <w n="2.4">son</w> <w n="2.5">mouchoir</w> <w n="2.6">deux</w> <w n="2.7">rangs</w> <w n="2.8">de</w> <w n="2.9">peupliers</w>.</l>
					<l n="3" num="1.3"><w n="3.1">Un</w> <w n="3.2">jeu</w> <w n="3.3">de</w> <w n="3.4">passe</w>-<w n="3.5">passe</w>, <w n="3.6">un</w> <w n="3.7">tour</w> <w n="3.8">de</w> <w n="3.9">main</w> <w n="3.10">habile</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Il</w> <w n="4.2">ne</w> <w n="4.3">demeure</w> <w n="4.4">plus</w> <w n="4.5">que</w> <w n="4.6">trois</w> <w n="4.7">bœufs</w> <w n="4.8">dans</w> <w n="4.9">le</w> <w n="4.10">pré</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Mais</w> <w n="5.2">trois</w> <w n="5.3">bœufs</w>, <w n="5.4">c</w>’<w n="5.5">est</w> <w n="5.6">pesant</w> ! <w n="5.7">Comment</w> <w n="5.8">les</w> <w n="5.9">enlever</w> ?</l>
					<l n="6" num="2.2"><w n="6.1">Le</w> <w n="6.2">brouillard</w> <w n="6.3">a</w> <w n="6.4">rongé</w> <w n="6.5">les</w> <w n="6.6">fils</w> <w n="6.7">de</w> <w n="6.8">la</w> <w n="6.9">clôture</w>,</l>
					<l n="7" num="2.3"><w n="7.1">Et</w> <w n="7.2">soudain</w> <w n="7.3">il</w> <w n="7.4">y</w> <w n="7.5">a</w> <w n="7.6">tant</w> <w n="7.7">de</w> <w n="7.8">prés</w> <w n="7.9">jumelés</w></l>
					<l n="8" num="2.4"><w n="8.1">Que</w> <w n="8.2">voici</w> <w n="8.3">les</w> <w n="8.4">trois</w> <w n="8.5">bœufs</w> <w n="8.6">dans</w> <w n="8.7">une</w> <w n="8.8">autre</w> <w n="8.9">pâture</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Quelle</w> <w n="9.2">meule</w> <w n="9.3">de</w> <w n="9.4">foin</w> <w n="9.5">mal</w> <w n="9.6">endormie</w> <w n="9.7">prétend</w></l>
					<l n="10" num="3.2"><w n="10.1">Ne</w> <w n="10.2">pas</w> <w n="10.3">laisser</w> <w n="10.4">noyer</w> <w n="10.5">sa</w> <w n="10.6">chevelure</w> <w n="10.7">blonde</w> ?</l>
					<l n="11" num="3.3"><w n="11.1">Brusquement</w>, <w n="11.2">une</w> <w n="11.3">vague</w> <w n="11.4">plus</w> <w n="11.5">haute</w> <w n="11.6">l</w>’<w n="11.7">inonde</w>.</l>
				</lg>
				<lg n="4">
					<l n="12" num="4.1"><w n="12.1">Et</w>, <w n="12.2">comme</w> <w n="12.3">le</w> <w n="12.4">falot</w> <w n="12.5">d</w>’<w n="12.6">une</w> <w n="12.7">barque</w> <w n="12.8">d</w>’<w n="12.9">argent</w>,</l>
					<l n="13" num="4.2"><w n="13.1">Une</w> <w n="13.2">lune</w> <w n="13.3">étonnée</w> <w n="13.4">commence</w> <w n="13.5">à</w> <w n="13.6">balancer</w></l>
					<l n="14" num="4.3"><w n="14.1">Sur</w> <w n="14.2">une</w> <w n="14.3">mer</w> <w n="14.4">laiteuse</w> <w n="14.5">où</w> <w n="14.6">flotte</w> <w n="14.7">un</w> <w n="14.8">colombier</w>.</l>
				</lg>
			</div></body></text></TEI>