<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRABANT</title>
				<title type="medium">Une édition électronique</title>
				<author key="CRM">
					<name>
						<forename>Maurice</forename>
						<surname>CARÊME</surname>
					</name>
					<date from="1899" to="1978">1899-1978</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3326 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CRM_1</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Brabant</title>
						<author>Maurice Carême</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LES ÉDITIONS OUVRIÈRES</publisher>
							<date when="1976">1976</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1967">1967</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-02-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-02-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CRM100">
				<head type="main">DIEU EN BRABANT</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Mon</w> <w n="1.2">Dieu</w>, <w n="1.3">qui</w> <w n="1.4">es</w> <w n="1.5">aussi</w> <w n="1.6">caché</w> <w n="1.7">dans</w> <w n="1.8">le</w> <w n="1.9">brin</w> <w n="1.10">d</w>’<w n="1.11">herbe</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Dans</w> <w n="2.2">la</w> <w n="2.3">nèpe</w> <w n="2.4">des</w> <w n="2.5">eaux</w> <w n="2.6">et</w> <w n="2.7">dans</w> <w n="2.8">le</w> <w n="2.9">caillou</w> <w n="2.10">bleu</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Souffre</w> <w n="3.2">que</w> <w n="3.3">je</w> <w n="3.4">te</w> <w n="3.5">loue</w>, <w n="3.6">moi</w>, <w n="3.7">le</w> <w n="3.8">fou</w>, <w n="3.9">le</w> <w n="3.10">cendreux</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Qui</w> <w n="4.2">ai</w> <w n="4.3">si</w> <w n="4.4">souvent</w> <w n="4.5">cru</w> <w n="4.6">n</w>’<w n="4.7">en</w> <w n="4.8">faire</w> <w n="4.9">qu</w>’<w n="4.10">à</w> <w n="4.11">ma</w> <w n="4.12">tête</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">N</w>’<w n="5.2">ai</w>-<w n="5.3">je</w> <w n="5.4">pas</w> <w n="5.5">deviné</w> <w n="5.6">déjà</w> <w n="5.7">que</w> <w n="5.8">tu</w> <w n="5.9">t</w>’<w n="5.10">arranges</w></l>
					<l n="6" num="2.2"><w n="6.1">Pour</w> <w n="6.2">que</w> <w n="6.3">l</w>’<w n="6.4">ombre</w> <w n="6.5">fleurie</w> <w n="6.6">au</w> <w n="6.7">pied</w> <w n="6.8">de</w> <w n="6.9">l</w>’<w n="6.10">églantier</w></l>
					<l n="7" num="2.3"><w n="7.1">Prenne</w> <w n="7.2">secrètement</w>, <w n="7.3">sur</w> <w n="7.4">la</w> <w n="7.5">boue</w> <w n="7.6">du</w> <w n="7.7">sentier</w></l>
					<l n="8" num="2.4"><w n="8.1">Où</w> <w n="8.2">s</w>’<w n="8.3">enfoncent</w> <w n="8.4">mes</w> <w n="8.5">pas</w>, <w n="8.6">la</w> <w n="8.7">forme</w> <w n="8.8">ailée</w> <w n="8.9">d</w>’<w n="8.10">un</w> <w n="8.11">ange</w> :</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Ainsi</w> <w n="9.2">partout</w>, <w n="9.3">sous</w> <w n="9.4">le</w> <w n="9.5">ciel</w> <w n="9.6">tendre</w> <w n="9.7">et</w> <w n="9.8">familier</w></l>
					<l n="10" num="3.2"><w n="10.1">De</w> <w n="10.2">mon</w> <w n="10.3">Brabant</w> <w n="10.4">lourd</w> <w n="10.5">de</w> <w n="10.6">limon</w>, <w n="10.7">je</w> <w n="10.8">te</w> <w n="10.9">retrouve</w>,</l>
					<l n="11" num="3.3"><w n="11.1">Toi</w> <w n="11.2">ou</w> <w n="11.3">tes</w> <w n="11.4">saints</w>, <w n="11.5">assis</w> <w n="11.6">dans</w> <w n="11.7">le</w> <w n="11.8">parfum</w> <w n="11.9">des</w> <w n="11.10">flouves</w></l>
					<l n="12" num="3.4"><w n="12.1">Ou</w> <w n="12.2">caressant</w> <w n="12.3">un</w> <w n="12.4">geai</w> <w n="12.5">surpris</w> <w n="12.6">dans</w> <w n="12.7">le</w> <w n="12.8">hallier</w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Ah</w> ! <w n="13.2">ne</w> <w n="13.3">m</w>’<w n="13.4">en</w> <w n="13.5">veuille</w> <w n="13.6">pas</w> <w n="13.7">si</w> <w n="13.8">je</w> <w n="13.9">n</w>’<w n="13.10">ai</w> <w n="13.11">su</w> <w n="13.12">chanter</w></l>
					<l n="14" num="4.2"><w n="14.1">Ni</w> <w n="14.2">le</w> <w n="14.3">tonnant</w> <w n="14.4">fracas</w> <w n="14.5">de</w> <w n="14.6">tes</w> <w n="14.7">soleils</w> <w n="14.8">de</w> <w n="14.9">juin</w>,</l>
					<l n="15" num="4.3"><w n="15.1">Ni</w> <w n="15.2">tes</w> <w n="15.3">lunes</w> <w n="15.4">noyées</w> <w n="15.5">dans</w> <w n="15.6">la</w> <w n="15.7">moiteur</w> <w n="15.8">des</w> <w n="15.9">foins</w>,</l>
					<l n="16" num="4.4"><w n="16.1">Ni</w> <w n="16.2">tes</w> <w n="16.3">mers</w> <w n="16.4">endormant</w> <w n="16.5">leurs</w> <w n="16.6">îles</w> <w n="16.7">éplorées</w> !</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">Je</w> <w n="17.2">n</w>’<w n="17.3">aurai</w> <w n="17.4">fait</w> <w n="17.5">danser</w>, <w n="17.6">mon</w> <w n="17.7">Dieu</w> <w n="17.8">qui</w> <w n="17.9">m</w>’<w n="17.10">as</w> <w n="17.11">aimé</w></l>
					<l n="18" num="5.2"><w n="18.1">De</w> <w n="18.2">toute</w> <w n="18.3">éternité</w>, <w n="18.4">que</w> <w n="18.5">le</w> <w n="18.6">grain</w> <w n="18.7">de</w> <w n="18.8">poussière</w></l>
					<l n="19" num="5.3"><w n="19.1">Qui</w> <w n="19.2">vole</w> <w n="19.3">étourdiment</w> <w n="19.4">dans</w> <w n="19.5">l</w>’<w n="19.6">ombre</w> <w n="19.7">du</w> <w n="19.8">grenier</w>,</l>
					<l n="20" num="5.4"><w n="20.1">Que</w> <w n="20.2">les</w> <w n="20.3">reflets</w> <w n="20.4">Stagnants</w> <w n="20.5">de</w> <w n="20.6">l</w>’<w n="20.7">eau</w> <w n="20.8">sur</w> <w n="20.9">l</w>’<w n="20.10">éphémère</w>.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">Je</w> <w n="21.2">n</w>’<w n="21.3">aurai</w> <w n="21.4">mis</w> <w n="21.5">que</w> <w n="21.6">le</w> <w n="21.7">point</w> <w n="21.8">d</w>’<w n="21.9">orgue</w> <w n="21.10">d</w>’<w n="21.11">une</w> <w n="21.12">abeille</w></l>
					<l n="22" num="6.2"><w n="22.1">Au</w> <w n="22.2">dernier</w> <w n="22.3">des</w> <w n="22.4">versets</w> <w n="22.5">de</w> <w n="22.6">ton</w> <w n="22.7">brûlant</w> <w n="22.8">psautier</w></l>
					<l n="23" num="6.3"><w n="23.1">Et</w> <w n="23.2">fait</w> <w n="23.3">tomber</w>, <w n="23.4">dans</w> <w n="23.5">l</w>’<w n="23.6">osier</w> <w n="23.7">pur</w> <w n="23.8">de</w> <w n="23.9">ta</w> <w n="23.10">corbeille</w>,</l>
					<l n="24" num="6.4"><w n="24.1">Qu</w>’<w n="24.2">un</w> <w n="24.3">peu</w> <w n="24.4">de</w> <w n="24.5">pluie</w> <w n="24.6">rougie</w> <w n="24.7">sur</w> <w n="24.8">un</w> <w n="24.9">gland</w> <w n="24.10">éclaté</w>.</l>
				</lg>
			</div></body></text></TEI>