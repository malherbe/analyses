<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRABANT</title>
				<title type="medium">Une édition électronique</title>
				<author key="CRM">
					<name>
						<forename>Maurice</forename>
						<surname>CARÊME</surname>
					</name>
					<date from="1899" to="1978">1899-1978</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3326 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CRM_1</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Brabant</title>
						<author>Maurice Carême</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LES ÉDITIONS OUVRIÈRES</publisher>
							<date when="1976">1976</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1967">1967</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-02-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-02-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CRM25">
				<head type="main">PAYSAGE</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Les</w> <w n="1.2">semeurs</w> <w n="1.3">s</w>’<w n="1.4">avançaient</w> <w n="1.5">jusqu</w>’<w n="1.6">à</w> <w n="1.7">l</w>’<w n="1.8">orée</w> <w n="1.9">du</w> <w n="1.10">monde</w>.</l>
					<l n="2" num="1.2"><w n="2.1">Le</w> <w n="2.2">Brabant</w> <w n="2.3">dérivait</w> <w n="2.4">au</w> <w n="2.5">milieu</w> <w n="2.6">des</w> <w n="2.7">colombes</w>.</l>
				</lg>
				<lg n="2">
					<l n="3" num="2.1"><w n="3.1">Tout</w> <w n="3.2">était</w> <w n="3.3">large</w> <w n="3.4">et</w> <w n="3.5">haut</w>. <w n="3.6">Une</w> <w n="3.7">allée</w> <w n="3.8">de</w> <w n="3.9">sorbiers</w>,</l>
					<l n="4" num="2.2"><w n="4.1">Dans</w> <w n="4.2">le</w> <w n="4.3">soleil</w>, <w n="4.4">buvait</w> <w n="4.5">du</w> <w n="4.6">ciel</w> <w n="4.7">à</w> <w n="4.8">plein</w> <w n="4.9">gosier</w>.</l>
				</lg>
				<lg n="3">
					<l n="5" num="3.1"><w n="5.1">Le</w> <w n="5.2">jeune</w> <w n="5.3">blé</w> <w n="5.4">était</w> <w n="5.5">d</w>’<w n="5.6">un</w> <w n="5.7">vert</w> <w n="5.8">si</w> <w n="5.9">merveilleux</w></l>
					<l n="6" num="3.2"><w n="6.1">Qu</w>’<w n="6.2">il</w> <w n="6.3">aurait</w> <w n="6.4">pu</w> <w n="6.5">servir</w> <w n="6.6">de</w> <w n="6.7">tapis</w> <w n="6.8">pour</w> <w n="6.9">les</w> <w n="6.10">dieux</w>.</l>
				</lg>
				<lg n="4">
					<l n="7" num="4.1"><w n="7.1">Plus</w> <w n="7.2">remuant</w> <w n="7.3">et</w> <w n="7.4">plus</w> <w n="7.5">furtif</w> <w n="7.6">que</w> <w n="7.7">ses</w> <w n="7.8">furets</w>,</l>
					<l n="8" num="4.2"><w n="8.1">Un</w> <w n="8.2">sentier</w> <w n="8.3">trottinait</w> <w n="8.4">le</w> <w n="8.5">long</w> <w n="8.6">de</w> <w n="8.7">la</w> <w n="8.8">forêt</w>,</l>
				</lg>
				<lg n="5">
					<l n="9" num="5.1"><w n="9.1">Et</w> <w n="9.2">l</w>’<w n="9.3">on</w> <w n="9.4">voyait</w> <w n="9.5">partout</w>, <w n="9.6">tranquilles</w>, <w n="9.7">sur</w> <w n="9.8">leurs</w> <w n="9.9">branches</w>,</l>
					<l n="10" num="5.2"><w n="10.1">Des</w> <w n="10.2">vergers</w> <w n="10.3">étaler</w> <w n="10.4">sans</w> <w n="10.5">fin</w> <w n="10.6">des</w> <w n="10.7">bâches</w> <w n="10.8">blanches</w>.</l>
				</lg>
				<lg n="6">
					<l n="11" num="6.1"><w n="11.1">Déjà</w> <w n="11.2">ma</w> <w n="11.3">voix</w> <w n="11.4">avait</w> <w n="11.5">dressé</w> <w n="11.6">comme</w> <w n="11.7">une</w> <w n="11.8">échelle</w></l>
					<l n="12" num="6.2"><w n="12.1">Où</w> <w n="12.2">se</w> <w n="12.3">précipitaient</w> <w n="12.4">à</w> <w n="12.5">l</w>’<w n="12.6">envi</w> <w n="12.7">des</w> <w n="12.8">voyelles</w>,</l>
				</lg>
				<lg n="7">
					<l n="13" num="7.1"><w n="13.1">Et</w> <w n="13.2">je</w> <w n="13.3">ne</w> <w n="13.4">savais</w> <w n="13.5">plus</w> <w n="13.6">si</w> <w n="13.7">j</w>’<w n="13.8">étais</w> <w n="13.9">une</w> <w n="13.10">odeur</w>,</l>
					<l n="14" num="7.2"><w n="14.1">Un</w> <w n="14.2">scarabée</w>, <w n="14.3">une</w> <w n="14.4">étincelle</w> <w n="14.5">ou</w> <w n="14.6">une</w> <w n="14.7">fleur</w>.</l>
				</lg>
			</div></body></text></TEI>