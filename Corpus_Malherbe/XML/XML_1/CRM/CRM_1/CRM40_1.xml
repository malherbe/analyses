<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRABANT</title>
				<title type="medium">Une édition électronique</title>
				<author key="CRM">
					<name>
						<forename>Maurice</forename>
						<surname>CARÊME</surname>
					</name>
					<date from="1899" to="1978">1899-1978</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3326 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CRM_1</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Brabant</title>
						<author>Maurice Carême</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LES ÉDITIONS OUVRIÈRES</publisher>
							<date when="1976">1976</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1967">1967</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-02-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-02-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CRM40">
				<head type="main">AUTOMNE EN VILLE</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Les</w> <w n="1.2">quelques</w> <w n="1.3">arbres</w> <w n="1.4">de</w> <w n="1.5">la</w> <w n="1.6">ville</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Avec</w> <w n="2.2">un</w> <w n="2.3">ensemble</w> <w n="2.4">émouvant</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Font</w> <w n="3.2">pleuvoir</w> <w n="3.3">leurs</w> <w n="3.4">feuilles</w> <w n="3.5">tranquilles</w></l>
					<l n="4" num="1.4"><space unit="char" quantity="8"></space><w n="4.1">Sur</w> <w n="4.2">les</w> <w n="4.3">enfants</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Et</w> <w n="5.2">Ton</w> <w n="5.3">dirait</w> <w n="5.4">que</w> <w n="5.5">dans</w> <w n="5.6">les</w> <w n="5.7">rues</w></l>
					<l n="6" num="2.2"><w n="6.1">Et</w> <w n="6.2">dans</w> <w n="6.3">les</w> <w n="6.4">cours</w> <w n="6.5">où</w> <w n="6.6">l</w>’<w n="6.7">ombre</w> <w n="6.8">dort</w>,</l>
					<l n="7" num="2.3"><space unit="char" quantity="4"></space><w n="7.1">Une</w> <w n="7.2">main</w> <w n="7.3">inconnue</w></l>
					<l n="8" num="2.4"><w n="8.1">Fait</w> <w n="8.2">doucement</w> <w n="8.3">pleuvoir</w> <w n="8.4">de</w> <w n="8.5">l</w>’<w n="8.6">or</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Les</w> <w n="9.2">tramways</w> <w n="9.3">vont</w>, <w n="9.4">les</w> <w n="9.5">autos</w> <w n="9.6">filent</w>,</l>
					<l n="10" num="3.2"><w n="10.1">Les</w> <w n="10.2">gens</w> <w n="10.3">se</w> <w n="10.4">pressent</w> <w n="10.5">sans</w> <w n="10.6">rien</w> <w n="10.7">voir</w>.</l>
					<l n="11" num="3.3"><w n="11.1">Un</w> <w n="11.2">avion</w> <w n="11.3">fait</w> <w n="11.4">sur</w> <w n="11.5">la</w> <w n="11.6">ville</w></l>
					<l n="12" num="3.4"><w n="12.1">Une</w> <w n="12.2">ombre</w> <w n="12.3">de</w> <w n="12.4">grand</w> <w n="12.5">oiseau</w> <w n="12.6">noir</w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Un</w> <w n="13.2">large</w> <w n="13.3">soleil</w> <w n="13.4">de</w> <w n="13.5">nickel</w></l>
					<l n="14" num="4.2"><w n="14.1">Brille</w>, <w n="14.2">glacé</w>, <w n="14.3">aux</w> <w n="14.4">devantures</w>.</l>
					<l n="15" num="4.3"><w n="15.1">Plus</w> <w n="15.2">un</w> <w n="15.3">ange</w> <w n="15.4">ne</w> <w n="15.5">s</w>’<w n="15.6">aventure</w></l>
					<l n="16" num="4.4"><w n="16.1">Sur</w> <w n="16.2">les</w> <w n="16.3">hauts</w> <w n="16.4">trapèzes</w> <w n="16.5">du</w> <w n="16.6">ciel</w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">Et</w> <w n="17.2">polie</w> <w n="17.3">ainsi</w> <w n="17.4">qu</w>’<w n="17.5">un</w> <w n="17.6">ivoire</w></l>
					<l n="18" num="5.2"><w n="18.1">Derrière</w> <w n="18.2">un</w> <w n="18.3">petit</w> <w n="18.4">rideau</w> <w n="18.5">blanc</w>,</l>
					<l n="19" num="5.3"><w n="19.1">Une</w> <w n="19.2">vieille</w> <w n="19.3">sourit</w> <w n="19.4">de</w> <w n="19.5">voir</w></l>
					<l n="20" num="5.4"><w n="20.1">S</w>’<w n="20.2">affairer</w> <w n="20.3">sans</w> <w n="20.4">fin</w> <w n="20.5">les</w> <w n="20.6">passants</w>.</l>
				</lg>
			</div></body></text></TEI>