<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRABANT</title>
				<title type="medium">Une édition électronique</title>
				<author key="CRM">
					<name>
						<forename>Maurice</forename>
						<surname>CARÊME</surname>
					</name>
					<date from="1899" to="1978">1899-1978</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3326 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CRM_1</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Brabant</title>
						<author>Maurice Carême</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LES ÉDITIONS OUVRIÈRES</publisher>
							<date when="1976">1976</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1967">1967</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-02-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-02-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CRM43">
				<head type="main">ET C’EST TOUJOURS LE VENT…</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Et</w> <w n="1.2">c</w>’<w n="1.3">est</w> <w n="1.4">toujours</w> <w n="1.5">le</w> <w n="1.6">vent</w> <w n="1.7">jouant</w> <w n="1.8">dans</w> <w n="1.9">tes</w> <w n="1.10">cheveux</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Les</w> <w n="2.2">fermes</w> <w n="2.3">frais</w> <w n="2.4">blanchies</w> <w n="2.5">que</w> <w n="2.6">les</w> <w n="2.7">grands</w> <w n="2.8">prés</w> <w n="2.9">écrèment</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Les</w> <w n="3.2">traînées</w> <w n="3.3">bleues</w> <w n="3.4">des</w> <w n="3.5">feux</w> <w n="3.6">se</w> <w n="3.7">roulant</w> <w n="3.8">dans</w> <w n="3.9">la</w> <w n="3.10">plaine</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Les</w> <w n="4.2">horizons</w> <w n="4.3">troués</w> <w n="4.4">d</w>’<w n="4.5">oiseaux</w> <w n="4.6">aventureux</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Et</w> <w n="5.2">le</w> <w n="5.3">Brabant</w> <w n="5.4">devant</w> <w n="5.5">et</w> <w n="5.6">le</w> <w n="5.7">Brabant</w> <w n="5.8">derrière</w>,</l>
					<l n="6" num="2.2"><w n="6.1">Et</w> <w n="6.2">à</w> <w n="6.3">gauche</w> <w n="6.4">et</w> <w n="6.5">à</w> <w n="6.6">droite</w> <w n="6.7">et</w> <w n="6.8">au</w> <w n="6.9">fond</w> <w n="6.10">des</w> <w n="6.11">ruisseaux</w>,</l>
					<l n="7" num="2.3"><w n="7.1">Le</w> <w n="7.2">Brabant</w> <w n="7.3">te</w> <w n="7.4">lançant</w> <w n="7.5">comme</w> <w n="7.6">un</w> <w n="7.7">dard</w> <w n="7.8">de</w> <w n="7.9">lumière</w></l>
					<l n="8" num="2.4"><w n="8.1">Dans</w> <w n="8.2">la</w> <w n="8.3">cible</w> <w n="8.4">vibrante</w> <w n="8.5">et</w> <w n="8.6">fleurie</w> <w n="8.7">des</w> <w n="8.8">coteaux</w>.</l>
				</lg>
			</div></body></text></TEI>