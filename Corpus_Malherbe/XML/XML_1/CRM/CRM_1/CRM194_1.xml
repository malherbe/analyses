<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRABANT</title>
				<title type="medium">Une édition électronique</title>
				<author key="CRM">
					<name>
						<forename>Maurice</forename>
						<surname>CARÊME</surname>
					</name>
					<date from="1899" to="1978">1899-1978</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3326 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CRM_1</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Brabant</title>
						<author>Maurice Carême</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LES ÉDITIONS OUVRIÈRES</publisher>
							<date when="1976">1976</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1967">1967</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-02-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-02-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CRM194">
				<head type="main">IL SUFFIT DE S’ASSEOIR…</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Il</w> <w n="1.2">suffit</w> <w n="1.3">de</w> <w n="1.4">s</w>’<w n="1.5">asseoir</w>, <w n="1.6">de</w> <w n="1.7">ne</w> <w n="1.8">penser</w> <w n="1.9">à</w> <w n="1.10">rien</w></l>
					<l n="2" num="1.2"><w n="2.1">Et</w> <w n="2.2">d</w>’<w n="2.3">attendre</w> <w n="2.4">que</w> <w n="2.5">passe</w> <w n="2.6">un</w> <w n="2.7">nuage</w> <w n="2.8">taquin</w></l>
					<l n="3" num="1.3"><w n="3.1">Ou</w> <w n="3.2">pas</w> <w n="3.3">même</w> <w n="3.4">un</w> <w n="3.5">nuage</w>, <w n="3.6">un</w> <w n="3.7">chien</w>, <w n="3.8">un</w> <w n="3.9">petit</w> <w n="3.10">chien</w></l>
					<l n="4" num="1.4"><w n="4.1">Ou</w> <w n="4.2">pas</w> <w n="4.3">même</w> <w n="4.4">un</w> <w n="4.5">seul</w> <w n="4.6">chien</w>, <w n="4.7">une</w> <w n="4.8">abeille</w>, <w n="4.9">un</w> <w n="4.10">taupin</w>,</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Et</w> <w n="5.2">voilà</w> <w n="5.3">tout</w> <w n="5.4">le</w> <w n="5.5">pré</w> <w n="5.6">qui</w> <w n="5.7">se</w> <w n="5.8">met</w> <w n="5.9">à</w> <w n="5.10">crier</w></l>
					<l n="6" num="2.2"><w n="6.1">Si</w> <w n="6.2">fort</w> <w n="6.3">dans</w> <w n="6.4">votre</w> <w n="6.5">cœur</w> <w n="6.6">où</w> <w n="6.7">tout</w> <w n="6.8">faisait</w> <w n="6.9">silence</w></l>
					<l n="7" num="2.3"><w n="7.1">Que</w> <w n="7.2">vous</w> <w n="7.3">n</w>’<w n="7.4">entendez</w> <w n="7.5">plus</w> <w n="7.6">que</w> <w n="7.7">l</w>’<w n="7.8">abeille</w> <w n="7.9">qui</w> <w n="7.10">danse</w></l>
					<l n="8" num="2.4"><w n="8.1">Ou</w> <w n="8.2">le</w> <w n="8.3">taupin</w> <w n="8.4">qui</w> <w n="8.5">fuit</w> <w n="8.6">dans</w> <w n="8.7">le</w> <w n="8.8">gazon</w> <w n="8.9">mouillé</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Du</w> <w n="9.2">cœur</w> <w n="9.3">surpris</w>, <w n="9.4">le</w> <w n="9.5">cri</w> <w n="9.6">monte</w> <w n="9.7">très</w> <w n="9.8">vite</w> <w n="9.9">aux</w> <w n="9.10">lèvres</w> ;</l>
					<l n="10" num="3.2"><w n="10.1">Des</w> <w n="10.2">mots</w> <w n="10.3">sortent</w> <w n="10.4">on</w> <w n="10.5">ne</w> <w n="10.6">sait</w> <w n="10.7">d</w>’<w n="10.8">où</w> <w n="10.9">comme</w> <w n="10.10">des</w> <w n="10.11">lièvres</w></l>
					<l n="11" num="3.3"><w n="11.1">Qui</w> <w n="11.2">courent</w> <w n="11.3">çà</w> <w n="11.4">et</w> <w n="11.5">là</w>, <w n="11.6">montrent</w> <w n="11.7">naïvement</w></l>
				</lg>
				<lg n="4">
					<l n="12" num="4.1"><w n="12.1">Leurs</w> <w n="12.2">oreilles</w> <w n="12.3">pointues</w> <w n="12.4">et</w> <w n="12.5">leur</w> <w n="12.6">petit</w> <w n="12.7">cul</w> <w n="12.8">blanc</w>,</l>
					<l n="13" num="4.2"><w n="13.1">Et</w> <w n="13.2">sur</w> <w n="13.3">votre</w> <w n="13.4">papier</w> <w n="13.5">font</w> <w n="13.6">des</w> <w n="13.7">ombres</w> <w n="13.8">qui</w> <w n="13.9">glissent</w></l>
					<l n="14" num="4.3"><w n="14.1">Avec</w> <w n="14.2">le</w> <w n="14.3">bruit</w> <w n="14.4">du</w> <w n="14.5">vent</w> <w n="14.6">chantant</w> <w n="14.7">dans</w> <w n="14.8">les</w> <w n="14.9">mélisses</w>.</l>
				</lg>
			</div></body></text></TEI>