<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRABANT</title>
				<title type="medium">Une édition électronique</title>
				<author key="CRM">
					<name>
						<forename>Maurice</forename>
						<surname>CARÊME</surname>
					</name>
					<date from="1899" to="1978">1899-1978</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3326 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CRM_1</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Brabant</title>
						<author>Maurice Carême</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LES ÉDITIONS OUVRIÈRES</publisher>
							<date when="1976">1976</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1967">1967</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-02-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-02-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CRM32">
				<head type="main">AVEC MON PÈRE</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">C</w>’<w n="1.2">est</w> <w n="1.3">un</w> <w n="1.4">pic</w>, <w n="1.5">disais</w>-<w n="1.6">je</w>, <w n="1.7">tout</w> <w n="1.8">fier</w>.</l>
					<l n="2" num="1.2"><w n="2.1">Non</w>, <w n="2.2">c</w>’<w n="2.3">est</w> <w n="2.4">un</w> <w n="2.5">geai</w>, <w n="2.6">disait</w> <w n="2.7">mon</w> <w n="2.8">père</w>.</l>
				</lg>
				<lg n="2">
					<l n="3" num="2.1"><w n="3.1">Et</w> <w n="3.2">ceci</w>, <w n="3.3">est</w>-<w n="3.4">ce</w> <w n="3.5">l</w>’<w n="3.6">aspérule</w> ?</l>
					<l n="4" num="2.2"><w n="4.1">Non</w>, <w n="4.2">disait</w>-<w n="4.3">il</w>, <w n="4.4">la</w> <w n="4.5">campanule</w>.</l>
				</lg>
				<lg n="3">
					<l n="5" num="3.1"><w n="5.1">Cette</w> <w n="5.2">fois</w>, <w n="5.3">je</w> <w n="5.4">suis</w> <w n="5.5">sûr</w> <w n="5.6">de</w> <w n="5.7">moi</w>,</l>
					<l n="6" num="3.2"><w n="6.1">J</w>’<w n="6.2">ai</w> <w n="6.3">bien</w> <w n="6.4">reconnu</w> <w n="6.5">le</w> <w n="6.6">putois</w>.</l>
				</lg>
				<lg n="4">
					<l n="7" num="4.1"><w n="7.1">Non</w>, <w n="7.2">disait</w>-<w n="7.3">il</w>, <w n="7.4">c</w>’<w n="7.5">est</w> <w n="7.6">le</w> <w n="7.7">furet</w></l>
					<l n="8" num="4.2"><w n="8.1">Qui</w> <w n="8.2">vient</w> <w n="8.3">de</w> <w n="8.4">sortir</w> <w n="8.5">des</w> <w n="8.6">genêts</w>.</l>
				</lg>
				<lg n="5">
					<l n="9" num="5.1"><w n="9.1">Ainsi</w>, <w n="9.2">mon</w> <w n="9.3">père</w> <w n="9.4">m</w>’<w n="9.5">apprenait</w></l>
					<l n="10" num="5.2"><w n="10.1">Le</w> <w n="10.2">peu</w> <w n="10.3">de</w> <w n="10.4">choses</w> <w n="10.5">qu</w>’<w n="10.6">il</w> <w n="10.7">savait</w>.</l>
				</lg>
				<lg n="6">
					<l n="11" num="6.1"><w n="11.1">Mais</w> <w n="11.2">c</w>’<w n="11.3">est</w> <w n="11.4">avec</w> <w n="11.5">ce</w> <w n="11.6">peu</w> <w n="11.7">de</w> <w n="11.8">choses</w></l>
					<l n="12" num="6.2"><w n="12.1">Que</w> <w n="12.2">j</w>’<w n="12.3">eus</w> <w n="12.4">une</w> <w n="12.5">vie</w> <w n="12.6">bleue</w> <w n="12.7">et</w> <w n="12.8">rose</w></l>
				</lg>
				<lg n="7">
					<l n="13" num="7.1"><w n="13.1">Et</w> <w n="13.2">que</w>, <w n="13.3">confiante</w>, <w n="13.4">la</w> <w n="13.5">fable</w></l>
					<l n="14" num="7.2"><w n="14.1">Est</w> <w n="14.2">venue</w> <w n="14.3">s</w>’<w n="14.4">asseoir</w> <w n="14.5">à</w> <w n="14.6">ma</w> <w n="14.7">table</w>.</l>
				</lg>
				<lg n="8">
					<l n="15" num="8.1"><w n="15.1">Mon</w> <w n="15.2">père</w> <w n="15.3">m</w>’<w n="15.4">a</w> <w n="15.5">mis</w> <w n="15.6">dans</w> <w n="15.7">la</w> <w n="15.8">tête</w></l>
					<l n="16" num="8.2"><w n="16.1">Une</w> <w n="16.2">étoile</w> <w n="16.3">et</w> <w n="16.4">une</w> <w n="16.5">alouette</w>,</l>
				</lg>
				<lg n="9">
					<l n="17" num="9.1"><w n="17.1">Mais</w> <w n="17.2">c</w>’<w n="17.3">est</w> <w n="17.4">une</w> <w n="17.5">étoile</w> <w n="17.6">qui</w> <w n="17.7">chante</w>,</l>
					<l n="18" num="9.2"><w n="18.1">Une</w> <w n="18.2">alouette</w> <w n="18.3">hallucinante</w>,</l>
				</lg>
				<lg n="10">
					<l n="19" num="10.1"><w n="19.1">Et</w> <w n="19.2">c</w>’<w n="19.3">est</w> <w n="19.4">pourquoi</w> <w n="19.5">vous</w> <w n="19.6">me</w> <w n="19.7">voyez</w></l>
					<l n="20" num="10.2"><w n="20.1">Marcher</w> <w n="20.2">sans</w> <w n="20.3">fin</w> <w n="20.4">dans</w> <w n="20.5">la</w> <w n="20.6">clarté</w>.</l>
				</lg>
			</div></body></text></TEI>