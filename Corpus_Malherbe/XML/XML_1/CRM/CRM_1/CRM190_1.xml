<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRABANT</title>
				<title type="medium">Une édition électronique</title>
				<author key="CRM">
					<name>
						<forename>Maurice</forename>
						<surname>CARÊME</surname>
					</name>
					<date from="1899" to="1978">1899-1978</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3326 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CRM_1</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Brabant</title>
						<author>Maurice Carême</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LES ÉDITIONS OUVRIÈRES</publisher>
							<date when="1976">1976</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1967">1967</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-02-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-02-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CRM190">
				<head type="main">SUR LES ROUTES DE MAI</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Personne</w> <w n="1.2">ne</w> <w n="1.3">sait</w> <w n="1.4">plus</w>, <w n="1.5">tant</w> <w n="1.6">il</w> <w n="1.7">y</w> <w n="1.8">a</w> <w n="1.9">d</w>’<w n="1.10">archanges</w>,</l>
					<l n="2" num="1.2"><w n="2.1">S</w>’<w n="2.2">ils</w> <w n="2.3">marchent</w> <w n="2.4">sur</w> <w n="2.5">la</w> <w n="2.6">terre</w> <w n="2.7">ou</w> <w n="2.8">descendent</w> <w n="2.9">du</w> <w n="2.10">ciel</w>.</l>
					<l n="3" num="1.3"><w n="3.1">Il</w> <w n="3.2">fait</w> <w n="3.3">si</w> <w n="3.4">pleinement</w>, <w n="3.5">si</w> <w n="3.6">ardemment</w> <w n="3.7">dimanche</w></l>
					<l n="4" num="1.4"><w n="4.1">Que</w> <w n="4.2">déjà</w> <w n="4.3">la</w> <w n="4.4">campagne</w> <w n="4.5">entre</w> <w n="4.6">dans</w> <w n="4.7">l</w>’<w n="4.8">éternel</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Sur</w> <w n="5.2">les</w> <w n="5.3">routes</w> <w n="5.4">de</w> <w n="5.5">mai</w> <w n="5.6">où</w> <w n="5.7">les</w> <w n="5.8">fleurs</w> <w n="5.9">se</w> <w n="5.10">font</w> <w n="5.11">signe</w>,</l>
					<l n="6" num="2.2"><w n="6.1">De</w> <w n="6.2">blanches</w> <w n="6.3">processions</w> <w n="6.4">font</w> <w n="6.5">glisser</w> <w n="6.6">le</w> <w n="6.7">Brabant</w></l>
					<l n="7" num="2.3"><w n="7.1">Avec</w> <w n="7.2">une</w> <w n="7.3">lenteur</w> <w n="7.4">héraldique</w> <w n="7.5">de</w> <w n="7.6">cygnes</w></l>
					<l n="8" num="2.4"><w n="8.1">Qui</w> <w n="8.2">regagnent</w> <w n="8.3">le</w> <w n="8.4">soir</w> <w n="8.5">les</w> <w n="8.6">joncs</w> <w n="8.7">de</w> <w n="8.8">leur</w> <w n="8.9">étang</w>.</l>
				</lg>
			</div></body></text></TEI>