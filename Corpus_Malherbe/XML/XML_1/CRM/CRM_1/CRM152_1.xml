<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRABANT</title>
				<title type="medium">Une édition électronique</title>
				<author key="CRM">
					<name>
						<forename>Maurice</forename>
						<surname>CARÊME</surname>
					</name>
					<date from="1899" to="1978">1899-1978</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3326 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CRM_1</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Brabant</title>
						<author>Maurice Carême</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LES ÉDITIONS OUVRIÈRES</publisher>
							<date when="1976">1976</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1967">1967</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-02-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-02-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CRM152">
				<head type="main">J’AI DE HAUTS CHÂTEAUX</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">J</w>’<w n="1.2">ai</w> <w n="1.3">de</w> <w n="1.4">hauts</w> <w n="1.5">châteaux</w> <w n="1.6">de</w> <w n="1.7">fougères</w></l>
					<l n="2" num="1.2"><w n="2.1">Dans</w> <w n="2.2">tous</w> <w n="2.3">les</w> <w n="2.4">coins</w> <w n="2.5">de</w> <w n="2.6">mon</w> <w n="2.7">Brabant</w></l>
					<l n="3" num="1.3"><w n="3.1">Et</w> <w n="3.2">des</w> <w n="3.3">chandeliers</w> <w n="3.4">de</w> <w n="3.5">lumière</w></l>
					<l n="4" num="1.4"><w n="4.1">Plus</w> <w n="4.2">hauts</w> <w n="4.3">que</w> <w n="4.4">les</w> <w n="4.5">peupliers</w> <w n="4.6">blancs</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Toutes</w> <w n="5.2">les</w> <w n="5.3">routes</w> <w n="5.4">m</w>’<w n="5.5">appartiennent</w></l>
					<l n="6" num="2.2"><w n="6.1">Et</w> <w n="6.2">les</w> <w n="6.3">prairies</w> <w n="6.4">et</w> <w n="6.5">leurs</w> <w n="6.6">bestiaux</w></l>
					<l n="7" num="2.3"><w n="7.1">Et</w>, <w n="7.2">du</w> <w n="7.3">plus</w> <w n="7.4">loin</w> <w n="7.5">qu</w>’<w n="7.6">il</w> <w n="7.7">m</w>’<w n="7.8">en</w> <w n="7.9">souvienne</w>,</l>
					<l n="8" num="2.4"><w n="8.1">Je</w> <w n="8.2">suis</w> <w n="8.3">déferlant</w> <w n="8.4">de</w> <w n="8.5">ruisseaux</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Mes</w> <w n="9.2">vergers</w> <w n="9.3">sont</w> <w n="9.4">pleins</w> <w n="9.5">de</w> <w n="9.6">myrtilles</w>,</l>
					<l n="10" num="3.2"><w n="10.1">De</w> <w n="10.2">mûres</w> <w n="10.3">et</w> <w n="10.4">de</w> <w n="10.5">framboisiers</w>.</l>
					<l n="11" num="3.3"><w n="11.1">Je</w> <w n="11.2">ne</w> <w n="11.3">cueille</w> <w n="11.4">que</w> <w n="11.5">ce</w> <w n="11.6">qui</w> <w n="11.7">brille</w></l>
					<l n="12" num="3.4"><w n="12.1">A</w> <w n="12.2">hauteur</w> <w n="12.3">de</w> <w n="12.4">mon</w> <w n="12.5">cœur</w> <w n="12.6">ailé</w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Venez</w> <w n="13.2">admirer</w> <w n="13.3">mes</w> <w n="13.4">volières</w> :</l>
					<l n="14" num="4.2"><w n="14.1">Toutes</w> <w n="14.2">mes</w> <w n="14.3">grives</w> <w n="14.4">y</w> <w n="14.5">sont</w> <w n="14.6">libres</w>.</l>
					<l n="15" num="4.3"><w n="15.1">Leurs</w> <w n="15.2">chants</w> <w n="15.3">sourds</w> <w n="15.4">y</w> <w n="15.5">font</w> <w n="15.6">équilibre</w></l>
					<l n="16" num="4.4"><w n="16.1">Aux</w> <w n="16.2">chants</w> <w n="16.3">profonds</w> <w n="16.4">des</w> <w n="16.5">sapinières</w>,</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">Et</w> <w n="17.2">mes</w> <w n="17.3">étangs</w> <w n="17.4">ont</w> <w n="17.5">plus</w> <w n="17.6">de</w> <w n="17.7">carpes</w></l>
					<l n="18" num="5.2"><w n="18.1">Que</w> <w n="18.2">n</w>’<w n="18.3">en</w> <w n="18.4">connaît</w> <w n="18.5">Fontainebleau</w>.</l>
					<l n="19" num="5.3"><w n="19.1">L</w>’<w n="19.2">or</w> <w n="19.3">magique</w> <w n="19.4">de</w> <w n="19.5">leur</w> <w n="19.6">anneau</w></l>
					<l n="20" num="5.4"><w n="20.1">Change</w> <w n="20.2">les</w> <w n="20.3">eaux</w> <w n="20.4">mortes</w> <w n="20.5">en</w> <w n="20.6">harpe</w>.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">Mes</w> <w n="21.2">pigeons</w> <w n="21.3">ont</w> <w n="21.4">des</w> <w n="21.5">colombiers</w></l>
					<l n="22" num="6.2"><w n="22.1">Comme</w> <w n="22.2">l</w>’<w n="22.3">on</w> <w n="22.4">n</w>’<w n="22.5">en</w> <w n="22.6">voit</w> <w n="22.7">nulle</w> <w n="22.8">part</w>.</l>
					<l n="23" num="6.3"><w n="23.1">La</w> <w n="23.2">pluie</w> <w n="23.3">leur</w> <w n="23.4">ouvre</w> <w n="23.5">un</w> <w n="23.6">abreuvoir</w></l>
					<l n="24" num="6.4"><w n="24.1">Aux</w> <w n="24.2">barreaux</w> <w n="24.3">de</w> <w n="24.4">cristal</w> <w n="24.5">filé</w>.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">Et</w> <w n="25.2">n</w>’<w n="25.3">entrez</w> <w n="25.4">pas</w> <w n="25.5">dans</w> <w n="25.6">mes</w> <w n="25.7">forêts</w> :</l>
					<l n="26" num="7.2"><w n="26.1">Tous</w> <w n="26.2">ses</w> <w n="26.3">halliers</w> <w n="26.4">sont</w> <w n="26.5">enchantés</w>.</l>
					<l n="27" num="7.3"><w n="27.1">Si</w> <w n="27.2">vous</w> <w n="27.3">connaissiez</w> <w n="27.4">leur</w> <w n="27.5">secret</w>,</l>
					<l n="28" num="7.4"><w n="28.1">Jamais</w> <w n="28.2">plus</w> <w n="28.3">vous</w> <w n="28.4">n</w>’<w n="28.5">en</w> <w n="28.6">sortiriez</w>.</l>
				</lg>
			</div></body></text></TEI>