<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRABANT</title>
				<title type="medium">Une édition électronique</title>
				<author key="CRM">
					<name>
						<forename>Maurice</forename>
						<surname>CARÊME</surname>
					</name>
					<date from="1899" to="1978">1899-1978</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3326 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CRM_1</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Brabant</title>
						<author>Maurice Carême</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LES ÉDITIONS OUVRIÈRES</publisher>
							<date when="1976">1976</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1967">1967</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-02-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-02-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CRM109">
				<head type="main">OÙ T’ALLAIS EN ERRANT…</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Où</w> <w n="1.2">j</w>’<w n="1.3">allais</w> <w n="1.4">en</w> <w n="1.5">errant</w>, <w n="1.6">me</w> <w n="1.7">revoici</w> <w n="1.8">en</w> <w n="1.9">maître</w>.</l>
					<l n="2" num="1.2"><w n="2.1">Je</w> <w n="2.2">redonne</w> <w n="2.3">aux</w> <w n="2.4">objets</w> <w n="2.5">un</w> <w n="2.6">si</w> <w n="2.7">juste</w> <w n="2.8">équilibre</w></l>
					<l n="3" num="1.3"><w n="3.1">Qu</w>’<w n="3.2">une</w> <w n="3.3">lueur</w> <w n="3.4">tremblant</w> <w n="3.5">dans</w> <w n="3.6">une</w> <w n="3.7">humble</w> <w n="3.8">fenêtre</w></l>
					<l n="4" num="1.4"><w n="4.1">Se</w> <w n="4.2">sent</w> <w n="4.3">sur</w> <w n="4.4">mes</w> <w n="4.5">coteaux</w> <w n="4.6">merveilleusement</w> <w n="4.7">libre</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Oh</w> ! <w n="5.2">vous</w> <w n="5.3">pouvez</w> <w n="5.4">courir</w>, <w n="5.5">mes</w> <w n="5.6">gerbes</w> <w n="5.7">alignées</w></l>
					<l n="6" num="2.2"><w n="6.1">Comme</w> <w n="6.2">les</w> <w n="6.3">cordes</w> <w n="6.4">lumineuses</w> <w n="6.5">d</w>’<w n="6.6">une</w> <w n="6.7">harpe</w>,</l>
					<l n="7" num="2.3"><w n="7.1">Vous</w> <w n="7.2">n</w>’<w n="7.3">atteindrez</w> <w n="7.4">jamais</w> <w n="7.5">cette</w> <w n="7.6">ligne</w> <w n="7.7">pourprée</w></l>
					<l n="8" num="2.4"><w n="8.1">Où</w> <w n="8.2">mes</w> <w n="8.3">jeunes</w> <w n="8.4">soleils</w> <w n="8.5">montent</w> <w n="8.6">parmi</w> <w n="8.7">les</w> <w n="8.8">arbres</w></l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">La</w> <w n="9.2">rose</w> <w n="9.3">que</w> <w n="9.4">je</w> <w n="9.5">touche</w> <w n="9.6">est</w> <w n="9.7">rose</w> <w n="9.8">pour</w> <w n="9.9">toujours</w>.</l>
					<l n="10" num="3.2"><w n="10.1">Le</w> <w n="10.2">fruit</w> <w n="10.3">que</w> <w n="10.4">j</w>’<w n="10.5">ai</w> <w n="10.6">cueilli</w> <w n="10.7">encore</w> <w n="10.8">acide</w> <w n="10.9">et</w> <w n="10.10">vert</w></l>
					<l n="11" num="3.3"><w n="11.1">Mûrira</w> <w n="11.2">dans</w> <w n="11.3">ma</w> <w n="11.4">main</w> <w n="11.5">si</w> <w n="11.6">je</w> <w n="11.7">l</w>’<w n="11.8">entrouvre</w> <w n="11.9">au</w> <w n="11.10">jour</w>.</l>
					<l n="12" num="3.4"><w n="12.1">Ma</w> <w n="12.2">ronde</w> <w n="12.3">des</w> <w n="12.4">saisons</w> <w n="12.5">ne</w> <w n="12.6">connaît</w> <w n="12.7">pas</w> <w n="12.8">l</w>’<w n="12.9">hiver</w>.</l>
				</lg>
			</div></body></text></TEI>