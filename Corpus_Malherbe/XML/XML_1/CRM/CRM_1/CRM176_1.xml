<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRABANT</title>
				<title type="medium">Une édition électronique</title>
				<author key="CRM">
					<name>
						<forename>Maurice</forename>
						<surname>CARÊME</surname>
					</name>
					<date from="1899" to="1978">1899-1978</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3326 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CRM_1</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Brabant</title>
						<author>Maurice Carême</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LES ÉDITIONS OUVRIÈRES</publisher>
							<date when="1976">1976</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1967">1967</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-02-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-02-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CRM176">
				<head type="main">LES POULES</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Je</w> <w n="1.2">suis</w> <w n="1.3">debout</w> <w n="1.4">dans</w> <w n="1.5">l</w>’<w n="1.6">ombre</w> <w n="1.7">en</w> <w n="1.8">face</w> <w n="1.9">d</w>’<w n="1.10">une</w> <w n="1.11">grille</w></l>
					<l n="2" num="1.2"><w n="2.1">Où</w> <w n="2.2">passent</w> <w n="2.3">au</w> <w n="2.4">soleil</w> <w n="2.5">de</w> <w n="2.6">jeunes</w> <w n="2.7">poules</w> <w n="2.8">blanches</w>.</l>
					<l n="3" num="1.3"><w n="3.1">Leur</w> <w n="3.2">crête</w> <w n="3.3">fait</w> <w n="3.4">penser</w> <w n="3.5">à</w> <w n="3.6">du</w> <w n="3.7">sang</w> <w n="3.8">frais</w> <w n="3.9">qui</w> <w n="3.10">brille</w>.</l>
					<l n="4" num="1.4"><w n="4.1">Leur</w> <w n="4.2">petite</w> <w n="4.3">ombre</w> <w n="4.4">est</w> <w n="4.5">bleue</w> <w n="4.6">comme</w> <w n="4.7">l</w>’<w n="4.8">ombre</w> <w n="4.9">des</w> <w n="4.10">branches</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">C</w>’<w n="5.2">est</w> <w n="5.3">drôle</w>. <w n="5.4">Elles</w> <w n="5.5">ne</w> <w n="5.6">semblent</w> <w n="5.7">même</w> <w n="5.8">pas</w> <w n="5.9">me</w> <w n="5.10">voir</w>.</l>
					<l n="6" num="2.2"><w n="6.1">Où</w> <w n="6.2">courent</w>-<w n="6.3">elles</w> <w n="6.4">donc</w> <w n="6.5">ainsi</w> <w n="6.6">toutes</w> <w n="6.7">ensemble</w> ?</l>
					<l n="7" num="2.3"><w n="7.1">On</w> <w n="7.2">dirait</w> <w n="7.3">que</w> <w n="7.4">le</w> <w n="7.5">vent</w> <w n="7.6">à</w> <w n="7.7">cheval</w> <w n="7.8">sur</w> <w n="7.9">le</w> <w n="7.10">tremble</w></l>
					<l n="8" num="2.4"><w n="8.1">Les</w> <w n="8.2">chasse</w> <w n="8.3">vers</w> <w n="8.4">le</w> <w n="8.5">bord</w> <w n="8.6">humide</w> <w n="8.7">du</w> <w n="8.8">lavoir</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Mais</w> <w n="9.2">non</w> ! <w n="9.3">pour</w> <w n="9.4">un</w> <w n="9.5">fétu</w> <w n="9.6">de</w> <w n="9.7">paille</w>, <w n="9.8">elles</w> <w n="9.9">s</w>’<w n="9.10">arrêtent</w>.</l>
					<l n="10" num="3.2"><w n="10.1">La</w> <w n="10.2">première</w> <w n="10.3">gratte</w> <w n="10.4">le</w> <w n="10.5">sol</w>, <w n="10.6">hoche</w> <w n="10.7">la</w> <w n="10.8">tête</w>,</l>
					<l n="11" num="3.3"><w n="11.1">Et</w>, <w n="11.2">sans</w> <w n="11.3">raison</w>, <w n="11.4">toutes</w> <w n="11.5">reviennent</w> <w n="11.6">vers</w> <w n="11.7">la</w> <w n="11.8">grille</w></l>
					<l n="12" num="3.4"><w n="12.1">Où</w> <w n="12.2">cent</w> <w n="12.3">regards</w> <w n="12.4">pointus</w> <w n="12.5">tout</w> <w n="12.6">à</w> <w n="12.7">coup</w> <w n="12.8">me</w> <w n="12.9">fusillent</w>.</l>
				</lg>
			</div></body></text></TEI>