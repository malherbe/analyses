<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRABANT</title>
				<title type="medium">Une édition électronique</title>
				<author key="CRM">
					<name>
						<forename>Maurice</forename>
						<surname>CARÊME</surname>
					</name>
					<date from="1899" to="1978">1899-1978</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3326 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CRM_1</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Brabant</title>
						<author>Maurice Carême</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LES ÉDITIONS OUVRIÈRES</publisher>
							<date when="1976">1976</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1967">1967</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-02-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-02-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CRM161">
				<head type="main">TU PEUX AVANCER SUR LA ROUTE…</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Tu</w> <w n="1.2">peux</w> <w n="1.3">avancer</w> <w n="1.4">sur</w> <w n="1.5">la</w> <w n="1.6">route</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Tu</w> <w n="2.2">peux</w> <w n="2.3">aller</w> <w n="2.4">où</w> <w n="2.5">tu</w> <w n="2.6">voudras</w>.</l>
					<l n="3" num="1.3"><w n="3.1">Tu</w> <w n="3.2">trouveras</w> <w n="3.3">une</w> <w n="3.4">autre</w> <w n="3.5">route</w></l>
					<l n="4" num="1.4"><w n="4.1">Avec</w> <w n="4.2">des</w> <w n="4.3">hommes</w> <w n="4.4">comme</w> <w n="4.5">toi</w>,</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Des</w> <w n="5.2">hommes</w> <w n="5.3">qu</w>’<w n="5.4">un</w> <w n="5.5">rire</w> <w n="5.6">de</w> <w n="5.7">femme</w></l>
					<l n="6" num="2.2"><w n="6.1">Fait</w> <w n="6.2">rois</w> <w n="6.3">dans</w> <w n="6.4">un</w> <w n="6.5">château</w> <w n="6.6">de</w> <w n="6.7">vent</w>,</l>
					<l n="7" num="2.3"><w n="7.1">Des</w> <w n="7.2">hommes</w> <w n="7.3">bercés</w> <w n="7.4">par</w> <w n="7.5">leur</w> <w n="7.6">âme</w></l>
					<l n="8" num="2.4"><w n="8.1">Comme</w> <w n="8.2">un</w> <w n="8.3">berceau</w> <w n="8.4">sur</w> <w n="8.5">un</w> <w n="8.6">chaland</w></l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Les</w> <w n="9.2">fermes</w> <w n="9.3">larguent</w> <w n="9.4">leurs</w> <w n="9.5">coqs</w> <w n="9.6">rouges</w>,</l>
					<l n="10" num="3.2"><w n="10.1">Les</w> <w n="10.2">prés</w> <w n="10.3">chantent</w>, <w n="10.4">des</w> <w n="10.5">portes</w> <w n="10.6">bougent</w>,</l>
					<l n="11" num="3.3"><w n="11.1">L</w>’<w n="11.2">église</w> <w n="11.3">étoilée</w> <w n="11.4">de</w> <w n="11.5">soleil</w></l>
				</lg>
				<lg n="4">
					<l n="12" num="4.1"><w n="12.1">Est</w> <w n="12.2">comme</w> <w n="12.3">un</w> <w n="12.4">arbre</w> <w n="12.5">de</w> <w n="12.6">Noël</w>.</l>
					<l n="13" num="4.2"><w n="13.1">Tu</w> <w n="13.2">peux</w> <w n="13.3">avancer</w> <w n="13.4">sur</w> <w n="13.5">la</w> <w n="13.6">route</w>.</l>
					<l n="14" num="4.3"><w n="14.1">Les</w> <w n="14.2">villages</w> <w n="14.3">sont</w> <w n="14.4">pleins</w> <w n="14.5">de</w> <w n="14.6">ciel</w>.</l>
				</lg>
			</div></body></text></TEI>