<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRABANT</title>
				<title type="medium">Une édition électronique</title>
				<author key="CRM">
					<name>
						<forename>Maurice</forename>
						<surname>CARÊME</surname>
					</name>
					<date from="1899" to="1978">1899-1978</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3326 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CRM_1</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Brabant</title>
						<author>Maurice Carême</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LES ÉDITIONS OUVRIÈRES</publisher>
							<date when="1976">1976</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1967">1967</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-02-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-02-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CRM3" rhyme="none">
				<head type="main">JE M’EN ALLAIS AINSI…</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Je</w> <w n="1.2">m</w>’<w n="1.3">en</w> <w n="1.4">allais</w> <w n="1.5">ainsi</w> <w n="1.6">le</w> <w n="1.7">long</w> <w n="1.8">de</w> <w n="1.9">ces</w> <w n="1.10">mélèzes</w>.</l>
					<l n="2" num="1.2"><w n="2.1">Le</w> <w n="2.2">soleil</w> <w n="2.3">soulevait</w> <w n="2.4">en</w> <w n="2.5">riant</w> <w n="2.6">les</w> <w n="2.7">oiseaux</w>.</l>
					<l n="3" num="1.3"><w n="3.1">La</w> <w n="3.2">colline</w> <w n="3.3">sentait</w> <w n="3.4">la</w> <w n="3.5">framboise</w> <w n="3.6">et</w> <w n="3.7">la</w> <w n="3.8">fraise</w>.</l>
					<l n="4" num="1.4"><w n="4.1">Les</w> <w n="4.2">ornières</w> <w n="4.3">couraient</w> <w n="4.4">au</w>-<w n="4.5">devant</w> <w n="4.6">des</w> <w n="4.7">chevaux</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Je</w> <w n="5.2">n</w>’<w n="5.3">avais</w> <w n="5.4">dans</w> <w n="5.5">la</w> <w n="5.6">poche</w> <w n="5.7">qu</w>’<w n="5.8">un</w> <w n="5.9">as</w> <w n="5.10">de</w> <w n="5.11">carreau</w>,</l>
					<l n="6" num="2.2"><w n="6.1">Une</w> <w n="6.2">bille</w> <w n="6.3">ébréchée</w> <w n="6.4">et</w> <w n="6.5">un</w> <w n="6.6">petit</w> <w n="6.7">caillou</w>,</l>
					<l n="7" num="2.3"><w n="7.1">Mais</w> <w n="7.2">je</w> <w n="7.3">chantais</w> <w n="7.4">plus</w> <w n="7.5">haut</w> <w n="7.6">que</w> <w n="7.7">le</w> <w n="7.8">vent</w> <w n="7.9">des</w> <w n="7.10">mélèzes</w> ;</l>
					<l n="8" num="2.4"><w n="8.1">Mon</w> <w n="8.2">cœur</w>, <w n="8.3">dans</w> <w n="8.4">le</w> <w n="8.5">soleil</w>, <w n="8.6">tremblait</w> <w n="8.7">comme</w> <w n="8.8">un</w> <w n="8.9">oiseau</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Je</w> <w n="9.2">sautais</w> <w n="9.3">les</w> <w n="9.4">fossés</w>, <w n="9.5">je</w> <w n="9.6">mordais</w> <w n="9.7">dans</w> <w n="9.8">les</w> <w n="9.9">herbes</w>,</l>
					<l n="10" num="3.2"><w n="10.1">Je</w> <w n="10.2">suivais</w> <w n="10.3">les</w> <w n="10.4">criquets</w> <w n="10.5">et</w> <w n="10.6">j</w>’<w n="10.7">embrassais</w> <w n="10.8">les</w> <w n="10.9">gerbes</w>,</l>
					<l n="11" num="3.3"><w n="11.1">Ne</w> <w n="11.2">sachant</w> <w n="11.3">pas</w> <w n="11.4">comment</w> <w n="11.5">éteindre</w> <w n="11.6">cette</w> <w n="11.7">joie</w></l>
				</lg>
				<lg n="4">
					<l n="12" num="4.1"><w n="12.1">Qui</w> <w n="12.2">me</w> <w n="12.3">multipliait</w> <w n="12.4">si</w> <w n="12.5">singulièrement</w></l>
					<l n="13" num="4.2"><w n="13.1">Que</w> <w n="13.2">j</w>’<w n="13.3">avais</w> <w n="13.4">l</w>’<w n="13.5">impression</w> <w n="13.6">aiguë</w> <w n="13.7">d</w>’<w n="13.8">être</w> <w n="13.9">à</w> <w n="13.10">la</w> <w n="13.11">fois</w></l>
					<l n="14" num="4.3"><w n="14.1">La</w> <w n="14.2">colline</w>, <w n="14.3">l</w>’<w n="14.4">oiseau</w>, <w n="14.5">le</w> <w n="14.6">mélèze</w> <w n="14.7">et</w> <w n="14.8">le</w> <w n="14.9">vent</w>.</l>
				</lg>
			</div></body></text></TEI>