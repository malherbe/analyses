<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRABANT</title>
				<title type="medium">Une édition électronique</title>
				<author key="CRM">
					<name>
						<forename>Maurice</forename>
						<surname>CARÊME</surname>
					</name>
					<date from="1899" to="1978">1899-1978</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3326 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CRM_1</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Brabant</title>
						<author>Maurice Carême</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LES ÉDITIONS OUVRIÈRES</publisher>
							<date when="1976">1976</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1967">1967</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-02-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-02-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CRM141">
				<head type="main">LES POMMES DE PIN</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Je</w> <w n="1.2">m</w>’<w n="1.3">en</w> <w n="1.4">allais</w> <w n="1.5">avec</w> <w n="1.6">ma</w> <w n="1.7">mère</w></l>
					<l n="2" num="1.2"><w n="2.1">Ramasser</w> <w n="2.2">des</w> <w n="2.3">pommes</w> <w n="2.4">de</w> <w n="2.5">pin</w>.</l>
					<l n="3" num="1.3"><w n="3.1">Le</w> <w n="3.2">soleil</w> <w n="3.3">tissait</w> <w n="3.4">dans</w> <w n="3.5">le</w> <w n="3.6">thym</w></l>
					<l n="4" num="1.4"><w n="4.1">D</w>’<w n="4.2">immenses</w> <w n="4.3">tapis</w> <w n="4.4">de</w> <w n="4.5">lumière</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Mon</w> <w n="5.2">cœur</w> <w n="5.3">était</w> <w n="5.4">château</w> <w n="5.5">en</w> <w n="5.6">liesse</w>.</l>
					<l n="6" num="2.2"><w n="6.1">Le</w> <w n="6.2">ciel</w> <w n="6.3">volait</w> <w n="6.4">sur</w> <w n="6.5">le</w> <w n="6.6">chemin</w>.</l>
					<l n="7" num="2.3"><w n="7.1">Mes</w> <w n="7.2">mains</w> <w n="7.3">devenaient</w> <w n="7.4">plus</w> <w n="7.5">parfaites</w></l>
					<l n="8" num="2.4"><w n="8.1">Que</w> <w n="8.2">la</w> <w n="8.3">rosée</w> <w n="8.4">dans</w> <w n="8.5">le</w> <w n="8.6">matin</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Il</w> <w n="9.2">y</w> <w n="9.3">avait</w> <w n="9.4">tant</w> <w n="9.5">de</w> <w n="9.6">myrtilles</w></l>
					<l n="10" num="3.2"><w n="10.1">Au</w> <w n="10.2">milieu</w> <w n="10.3">des</w> <w n="10.4">pommes</w> <w n="10.5">de</w> <w n="10.6">pin</w></l>
					<l n="11" num="3.3"><w n="11.1">Que</w> <w n="11.2">mon</w> <w n="11.3">sac</w> <w n="11.4">demeurait</w> <w n="11.5">en</w> <w n="11.6">vrille</w></l>
					<l n="12" num="3.4"><w n="12.1">Longtemps</w> <w n="12.2">à</w> <w n="12.3">l</w>’<w n="12.4">orée</w> <w n="12.5">du</w> <w n="12.6">chemin</w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">A</w> <w n="13.2">midi</w>, <w n="13.3">nous</w> <w n="13.4">mordions</w> <w n="13.5">ensemble</w></l>
					<l n="14" num="4.2"><w n="14.1">Dans</w> <w n="14.2">le</w> <w n="14.3">même</w> <w n="14.4">quignon</w> <w n="14.5">de</w> <w n="14.6">pain</w>.</l>
					<l n="15" num="4.3"><w n="15.1">La</w> <w n="15.2">source</w> <w n="15.3">mettait</w>, <w n="15.4">sous</w> <w n="15.5">un</w> <w n="15.6">tremble</w>,</l>
					<l n="16" num="4.4"><w n="16.1">Le</w> <w n="16.2">ciel</w> <w n="16.3">à</w> <w n="16.4">portée</w> <w n="16.5">de</w> <w n="16.6">nos</w> <w n="16.7">mains</w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">Et</w> <w n="17.2">lorsque</w>, <w n="17.3">dans</w> <w n="17.4">la</w> <w n="17.5">nuit</w> <w n="17.6">tombante</w>,</l>
					<l n="18" num="5.2"><w n="18.1">Nous</w> <w n="18.2">revenions</w>, <w n="18.3">silencieux</w>,</l>
					<l n="19" num="5.3"><w n="19.1">Parfois</w> <w n="19.2">une</w> <w n="19.3">étoile</w> <w n="19.4">filante</w></l>
					<l n="20" num="5.4"><w n="20.1">Venait</w> <w n="20.2">se</w> <w n="20.3">prendre</w> <w n="20.4">à</w> <w n="20.5">nos</w> <w n="20.6">cheveux</w>.</l>
				</lg>
			</div></body></text></TEI>