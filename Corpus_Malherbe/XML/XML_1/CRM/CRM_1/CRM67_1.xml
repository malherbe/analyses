<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRABANT</title>
				<title type="medium">Une édition électronique</title>
				<author key="CRM">
					<name>
						<forename>Maurice</forename>
						<surname>CARÊME</surname>
					</name>
					<date from="1899" to="1978">1899-1978</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3326 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CRM_1</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Brabant</title>
						<author>Maurice Carême</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LES ÉDITIONS OUVRIÈRES</publisher>
							<date when="1976">1976</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1967">1967</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-02-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-02-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CRM67">
				<head type="main">CONNAIS-TU LES FORETS DE DION</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Connais</w>-<w n="1.2">tu</w> <w n="1.3">les</w> <w n="1.4">forêts</w> <w n="1.5">de</w> <w n="1.6">Dion</w></l>
					<l n="2" num="1.2"><w n="2.1">Où</w> <w n="2.2">chantent</w> <w n="2.3">des</w> <w n="2.4">filles</w> <w n="2.5">lointaines</w></l>
					<l n="3" num="1.3"><w n="3.1">Et</w> <w n="3.2">cette</w> <w n="3.3">pluie</w> <w n="3.4">dorée</w> <w n="3.5">de</w> <w n="3.6">faines</w></l>
					<l n="4" num="1.4"><w n="4.1">Sur</w> <w n="4.2">le</w> <w n="4.3">dos</w> <w n="4.4">rude</w> <w n="4.5">des</w> <w n="4.6">buissons</w> ?</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Et</w> <w n="5.2">puis</w>, <w n="5.3">cet</w> <w n="5.4">angélus</w> <w n="5.5">tintant</w></l>
					<l n="6" num="2.2"><w n="6.1">Avec</w> <w n="6.2">tant</w> <w n="6.3">de</w> <w n="6.4">douceur</w> <w n="6.5">tranquille</w></l>
					<l n="7" num="2.3"><w n="7.1">Qu</w>’<w n="7.2">il</w> <w n="7.3">semble</w> <w n="7.4">poser</w> <w n="7.5">sur</w> <w n="7.6">les</w> <w n="7.7">champs</w></l>
					<l n="8" num="2.4"><w n="8.1">Des</w> <w n="8.2">bouquets</w> <w n="8.3">de</w> <w n="8.4">bluets</w> <w n="8.5">graciles</w> ?</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Oui</w>, <w n="9.2">connais</w>-<w n="9.3">tu</w> <w n="9.4">les</w> <w n="9.5">peupliers</w></l>
					<l n="10" num="3.2"><w n="10.1">Qui</w>, <w n="10.2">rangés</w> <w n="10.3">près</w> <w n="10.4">du</w> <w n="10.5">ruisseau</w> <w n="10.6">sage</w>,</l>
					<l n="11" num="3.3"><w n="11.1">Aiment</w> <w n="11.2">ensemble</w> <w n="11.3">fuseler</w></l>
					<l n="12" num="3.4"><w n="12.1">La</w> <w n="12.2">laine</w> <w n="12.3">blanche</w> <w n="12.4">des</w> <w n="12.5">nuages</w></l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Et</w> <w n="13.2">surtout</w> <w n="13.3">cet</w> <w n="13.4">orchestrion</w></l>
					<l n="14" num="4.2"><w n="14.1">Qui</w>, <w n="14.2">dans</w> <w n="14.3">les</w> <w n="14.4">soirées</w> <w n="14.5">du</w> <w n="14.6">dimanche</w>,</l>
					<l n="15" num="4.3"><w n="15.1">Fait</w> <w n="15.2">briller</w> <w n="15.3">les</w> <w n="15.4">astres</w> <w n="15.5">d</w>’<w n="15.6">Orion</w></l>
					<l n="16" num="4.4"><w n="16.1">Sur</w> <w n="16.2">la</w> <w n="16.3">valse</w> <w n="16.4">des</w> <w n="16.5">robes</w> <w n="16.6">blanches</w></l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">Et</w>, <w n="17.2">là</w>-<w n="17.3">bas</w>, <w n="17.4">cette</w> <w n="17.5">lune</w> <w n="17.6">verte</w></l>
					<l n="18" num="5.2"><w n="18.1">Et</w> <w n="18.2">rose</w> <w n="18.3">qui</w>, <w n="18.4">sur</w> <w n="18.5">le</w> <w n="18.6">château</w>,</l>
					<l n="19" num="5.3"><w n="19.1">Fait</w> <w n="19.2">se</w> <w n="19.3">plaindre</w> <w n="19.4">les</w> <w n="19.5">girouettes</w></l>
					<l n="20" num="5.4"><w n="20.1">Tendrement</w> <w n="20.2">comme</w> <w n="20.3">les</w> <w n="20.4">oiseaux</w> ?</l>
				</lg>
			</div></body></text></TEI>