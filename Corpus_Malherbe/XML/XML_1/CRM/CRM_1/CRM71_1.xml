<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRABANT</title>
				<title type="medium">Une édition électronique</title>
				<author key="CRM">
					<name>
						<forename>Maurice</forename>
						<surname>CARÊME</surname>
					</name>
					<date from="1899" to="1978">1899-1978</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3326 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CRM_1</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Brabant</title>
						<author>Maurice Carême</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LES ÉDITIONS OUVRIÈRES</publisher>
							<date when="1976">1976</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1967">1967</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-02-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-02-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CRM71">
				<head type="main">BRUXELLES</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Pourquoi</w> <w n="1.2">ne</w> <w n="1.3">laisserais</w>-<w n="1.4">tu</w> <w n="1.5">pas</w> <w n="1.6">monter</w> <w n="1.7">de</w> <w n="1.8">toi</w></l>
					<l n="2" num="1.2"><space unit="char" quantity="20"></space><w n="2.1">La</w> <w n="2.2">ville</w></l>
					<l n="3" num="1.3"><w n="3.1">Avec</w> <w n="3.2">ses</w> <w n="3.3">tours</w>, <w n="3.4">ses</w> <w n="3.5">volières</w> <w n="3.6">d</w>’<w n="3.7">oiseaux</w>, <w n="3.8">ses</w> <w n="3.9">toits</w></l>
					<l n="4" num="1.4"><space unit="char" quantity="20"></space><w n="4.1">De</w> <w n="4.2">tuiles</w> ?</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">C</w>’<w n="5.2">est</w> <w n="5.3">tout</w> <w n="5.4">le</w> <w n="5.5">long</w> <w n="5.6">des</w> <w n="5.7">cœurs</w> <w n="5.8">qu</w>’<w n="5.9">ici</w> <w n="5.10">les</w> <w n="5.11">douces</w> <w n="5.12">biches</w></l>
					<l n="6" num="2.2"><space unit="char" quantity="20"></space><w n="6.1">Repassent</w></l>
					<l n="7" num="2.3"><w n="7.1">Préférant</w> <w n="7.2">quelquefois</w> <w n="7.3">à</w> <w n="7.4">l</w>’<w n="7.5">ombre</w> <w n="7.6">des</w> <w n="7.7">rues</w> <w n="7.8">riches</w>,</l>
					<l n="8" num="2.4"><space unit="char" quantity="20"></space><w n="8.1">L</w>’<w n="8.2">impasse</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Des</w> <w n="9.2">arbres</w> <w n="9.3">égarés</w> <w n="9.4">regardent</w> <w n="9.5">par</w>-<w n="9.6">dessus</w></l>
					<l n="10" num="3.2"><space unit="char" quantity="20"></space><w n="10.1">Les</w> <w n="10.2">murs</w></l>
					<l n="11" num="3.3"><w n="11.1">Courir</w> <w n="11.2">les</w> <w n="11.3">écoliers</w> <w n="11.4">dont</w> <w n="11.5">les</w> <w n="11.6">cris</w> <w n="11.7">ingénus</w></l>
					<l n="12" num="3.4"><space unit="char" quantity="20"></space><w n="12.1">Rassurent</w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">C</w>’<w n="13.2">est</w> <w n="13.3">là</w> <w n="13.4">que</w> <w n="13.5">l</w>’<w n="13.6">amour</w> <w n="13.7">tient</w> <w n="13.8">si</w> <w n="13.9">candidement</w> <w n="13.10">sa</w></l>
					<l n="14" num="4.2"><space unit="char" quantity="20"></space><w n="14.1">Balance</w></l>
					<l n="15" num="4.3"><w n="15.1">Qu</w>’<w n="15.2">on</w> <w n="15.3">ne</w> <w n="15.4">devine</w> <w n="15.5">plus</w>, <w n="15.6">dans</w> <w n="15.7">le</w> <w n="15.8">va</w>-<w n="15.9">et</w>-<w n="15.10">vient</w>, <w n="15.11">sa</w></l>
					<l n="16" num="4.4"><space unit="char" quantity="20"></space><w n="16.1">Présence</w></l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">Que</w> <w n="17.2">dans</w> <w n="17.3">l</w>’<w n="17.4">apparition</w> <w n="17.5">soudaine</w> <w n="17.6">et</w> <w n="17.7">secrète</w> <w n="17.8">à</w></l>
					<l n="18" num="5.2"><space unit="char" quantity="20"></space><w n="18.1">Nos</w> <w n="18.2">yeux</w>,</l>
					<l n="19" num="5.3"><w n="19.1">Loin</w> <w n="19.2">de</w> <w n="19.3">toute</w> <w n="19.4">verdure</w> <w n="19.5">drue</w>, <w n="19.6">d</w>’<w n="19.7">une</w> <w n="19.8">bête</w> <w n="19.9">à</w></l>
					<l n="20" num="5.4"><space unit="char" quantity="20"></space><w n="20.1">Bon</w> <w n="20.2">Dieu</w>.</l>
				</lg>
			</div></body></text></TEI>