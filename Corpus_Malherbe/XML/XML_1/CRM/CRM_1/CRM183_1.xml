<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRABANT</title>
				<title type="medium">Une édition électronique</title>
				<author key="CRM">
					<name>
						<forename>Maurice</forename>
						<surname>CARÊME</surname>
					</name>
					<date from="1899" to="1978">1899-1978</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3326 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CRM_1</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Brabant</title>
						<author>Maurice Carême</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LES ÉDITIONS OUVRIÈRES</publisher>
							<date when="1976">1976</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1967">1967</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-02-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-02-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CRM183">
				<head type="main">LE CARABE DORE</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Tu</w> <w n="1.2">ne</w> <w n="1.3">finiras</w> <w n="1.4">pas</w> <w n="1.5">de</w> <w n="1.6">mordorer</w> <w n="1.7">tes</w> <w n="1.8">blés</w>.</l>
					<l n="2" num="1.2"><w n="2.1">Ton</w> <w n="2.2">soleil</w> <w n="2.3">ne</w> <w n="2.4">fut</w> <w n="2.5">pas</w> <w n="2.6">plus</w> <w n="2.7">vaste</w> <w n="2.8">que</w> <w n="2.9">ton</w> <w n="2.10">cœur</w>.</l>
					<l n="3" num="1.3"><w n="3.1">Il</w> <w n="3.2">ne</w> <w n="3.3">t</w>’<w n="3.4">aura</w> <w n="3.5">manqué</w>, <w n="3.6">après</w> <w n="3.7">le</w> <w n="3.8">temps</w> <w n="3.9">des</w> <w n="3.10">fleurs</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Qu</w>’<w n="4.2">un</w> <w n="4.3">peu</w> <w n="4.4">plus</w> <w n="4.5">de</w> <w n="4.6">candeur</w> <w n="4.7">dans</w> <w n="4.8">la</w> <w n="4.9">simplicité</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Chaque</w> <w n="5.2">couchant</w> <w n="5.3">te</w> <w n="5.4">voit</w>, <w n="5.5">patiemment</w> <w n="5.6">incliné</w>,</l>
					<l n="6" num="2.2"><w n="6.1">Faucher</w> <w n="6.2">cette</w> <w n="6.3">moisson</w> <w n="6.4">encore</w> <w n="6.5">à</w> <w n="6.6">moitié</w> <w n="6.7">verte</w></l>
					<l n="7" num="2.3"><w n="7.1">Et</w> <w n="7.2">tourner</w> <w n="7.3">ton</w> <w n="7.4">front</w> <w n="7.5">las</w> <w n="7.6">vers</w> <w n="7.7">les</w> <w n="7.8">granges</w> <w n="7.9">ouvertes</w></l>
					<l n="8" num="2.4"><w n="8.1">Où</w> <w n="8.2">s</w>’<w n="8.3">engouffrent</w> <w n="8.4">déjà</w> <w n="8.5">les</w> <w n="8.6">premiers</w> <w n="8.7">vents</w> <w n="8.8">glacés</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Abandonne</w> <w n="9.2">ta</w> <w n="9.3">faux</w>, <w n="9.4">referme</w> <w n="9.5">ton</w> <w n="9.6">portail</w>.</l>
					<l n="10" num="3.2"><w n="10.1">Assieds</w>-<w n="10.2">toi</w> <w n="10.3">sur</w> <w n="10.4">ton</w> <w n="10.5">seuil</w> <w n="10.6">et</w> <w n="10.7">tire</w> <w n="10.8">de</w> <w n="10.9">ta</w> <w n="10.10">poche</w></l>
					<l n="11" num="3.3"><w n="11.1">Cette</w> <w n="11.2">boîte</w> <w n="11.3">où</w>, <w n="11.4">comme</w> <w n="11.5">un</w> <w n="11.6">bijou</w> <w n="11.7">d</w>’<w n="11.8">or</w> <w n="11.9">et</w> <w n="11.10">d</w>’<w n="11.11">émail</w>,</l>
					<l n="12" num="3.4"><w n="12.1">Luit</w> <w n="12.2">un</w> <w n="12.3">insecte</w> <w n="12.4">pris</w> <w n="12.5">dans</w> <w n="12.6">les</w> <w n="12.7">aristoloches</w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Ah</w> ! <w n="13.2">pourquoi</w> <w n="13.3">souris</w>-<w n="13.4">tu</w> <w n="13.5">ainsi</w> <w n="13.6">amèrement</w> ?</l>
					<l n="14" num="4.2"><w n="14.1">Le</w> <w n="14.2">brouillard</w> <w n="14.3">va</w> <w n="14.4">cacher</w> <w n="14.5">l</w>’<w n="14.6">autre</w> <w n="14.7">mort</w> <w n="14.8">du</w> <w n="14.9">verger</w>.</l>
					<l n="15" num="4.3"><w n="15.1">Ne</w> <w n="15.2">vas</w>-<w n="15.3">tu</w> <w n="15.4">pas</w> <w n="15.5">laisser</w> <w n="15.6">dans</w> <w n="15.7">la</w> <w n="15.8">main</w> <w n="15.9">d</w>’<w n="15.10">un</w> <w n="15.11">enfant</w></l>
					<l n="16" num="4.4"><w n="16.1">Ce</w> <w n="16.2">trésor</w> <w n="16.3">inutile</w> : <w n="16.4">un</w> <w n="16.5">carabe</w> <w n="16.6">doré</w>.</l>
				</lg>
			</div></body></text></TEI>