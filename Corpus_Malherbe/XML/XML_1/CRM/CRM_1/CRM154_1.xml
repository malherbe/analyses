<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRABANT</title>
				<title type="medium">Une édition électronique</title>
				<author key="CRM">
					<name>
						<forename>Maurice</forename>
						<surname>CARÊME</surname>
					</name>
					<date from="1899" to="1978">1899-1978</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3326 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CRM_1</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Brabant</title>
						<author>Maurice Carême</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LES ÉDITIONS OUVRIÈRES</publisher>
							<date when="1976">1976</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1967">1967</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-02-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-02-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CRM154">
				<head type="main">QUAND J’ÉTAIS ENFANT</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Que</w> <w n="1.2">n</w>’<w n="1.3">avais</w>-<w n="1.4">je</w> <w n="1.5">pas</w></l>
					<l n="2" num="1.2"><w n="2.1">Quand</w> <w n="2.2">j</w>’<w n="2.3">étais</w> <w n="2.4">enfant</w> !</l>
					<l n="3" num="1.3"><w n="3.1">Des</w> <w n="3.2">patins</w> <w n="3.3">de</w> <w n="3.4">roi</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Des</w> <w n="4.2">dents</w> <w n="4.3">d</w>’<w n="4.4">éléphant</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Je</w> <w n="5.2">ne</w> <w n="5.3">pouvais</w> <w n="5.4">pas</w></l>
					<l n="6" num="2.2"><w n="6.1">Compter</w> <w n="6.2">les</w> <w n="6.3">palais</w></l>
					<l n="7" num="2.3"><w n="7.1">Fleuris</w> <w n="7.2">de</w> <w n="7.3">genêts</w></l>
					<l n="8" num="2.4"><w n="8.1">Que</w> <w n="8.2">j</w>’<w n="8.3">avais</w> <w n="8.4">au</w> <w n="8.5">bois</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Deux</w> <w n="9.2">cosses</w> <w n="9.3">de</w> <w n="9.4">pois</w> :</l>
					<l n="10" num="3.2"><w n="10.1">Roule</w> <w n="10.2">mon</w> <w n="10.3">carrosse</w> !</l>
					<l n="11" num="3.3"><w n="11.1">Un</w> <w n="11.2">vieux</w> <w n="11.3">papier</w> <w n="11.4">rose</w> :</l>
					<l n="12" num="3.4"><w n="12.1">Vogue</w> <w n="12.2">mon</w> <w n="12.3">trois</w>-<w n="12.4">mâts</w> !</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Les</w> <w n="13.2">deux</w> <w n="13.3">nattes</w> <w n="13.4">blondes</w></l>
					<l n="14" num="4.2"><w n="14.1">D</w>’<w n="14.2">Andrée</w> <w n="14.3">soleillaient</w></l>
					<l n="15" num="4.3"><w n="15.1">Et</w> <w n="15.2">son</w> <w n="15.3">rire</w> <w n="15.4">était</w></l>
					<l n="16" num="4.4"><w n="16.1">Le</w> <w n="16.2">rire</w> <w n="16.3">du</w> <w n="16.4">monde</w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">Il</w> <w n="17.2">m</w>’<w n="17.3">en</w> <w n="17.4">faut</w> <w n="17.5">bien</w> <w n="17.6">moins</w></l>
					<l n="18" num="5.2"><w n="18.1">Pour</w> <w n="18.2">être</w>, <w n="18.3">à</w> <w n="18.4">cette</w> <w n="18.5">heure</w>,</l>
					<l n="19" num="5.3"><w n="19.1">Plus</w> <w n="19.2">soûl</w> <w n="19.3">de</w> <w n="19.4">bonheur</w></l>
					<l n="20" num="5.4"><w n="20.1">Qu</w>’<w n="20.2">un</w> <w n="20.3">loir</w> <w n="20.4">sur</w> <w n="20.5">un</w> <w n="20.6">pin</w>.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">Suffit</w> <w n="21.2">de</w> <w n="21.3">tenir</w></l>
					<l n="22" num="6.2"><w n="22.1">Sur</w> <w n="22.2">mes</w> <w n="22.3">genoux</w> <w n="22.4">d</w>’<w n="22.5">homme</w></l>
					<l n="23" num="6.3"><w n="23.1">Un</w> <w n="23.2">seul</w> <w n="23.3">souvenir</w></l>
					<l n="24" num="6.4"><w n="24.1">Rond</w> <w n="24.2">comme</w> <w n="24.3">une</w> <w n="24.4">pomme</w></l>
				</lg>
			</div></body></text></TEI>