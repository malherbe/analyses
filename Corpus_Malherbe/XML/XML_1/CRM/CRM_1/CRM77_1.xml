<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRABANT</title>
				<title type="medium">Une édition électronique</title>
				<author key="CRM">
					<name>
						<forename>Maurice</forename>
						<surname>CARÊME</surname>
					</name>
					<date from="1899" to="1978">1899-1978</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3326 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CRM_1</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Brabant</title>
						<author>Maurice Carême</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LES ÉDITIONS OUVRIÈRES</publisher>
							<date when="1976">1976</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1967">1967</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-02-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-02-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CRM77">
				<head type="main">LE CHEMIN</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Le</w> <w n="1.2">chemin</w> <w n="1.3">remontait</w>, <w n="1.4">abrupt</w>, <w n="1.5">vers</w> <w n="1.6">le</w> <w n="1.7">village</w></l>
					<l n="2" num="1.2"><w n="2.1">Entre</w> <w n="2.2">de</w> <w n="2.3">vastes</w> <w n="2.4">prés</w> <w n="2.5">piquetés</w> <w n="2.6">de</w> <w n="2.7">bœufs</w> <w n="2.8">blancs</w>.</l>
					<l n="3" num="1.3"><w n="3.1">Il</w> <w n="3.2">ne</w> <w n="3.3">suivait</w> <w n="3.4">jamais</w> <w n="3.5">le</w> <w n="3.6">vent</w> <w n="3.7">ni</w> <w n="3.8">les</w> <w n="3.9">nuages</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Il</w> <w n="4.2">s</w>’<w n="4.3">en</w> <w n="4.4">allait</w> <w n="4.5">tout</w> <w n="4.6">seul</w>, <w n="4.7">hérissé</w> <w n="4.8">d</w>’<w n="4.9">origans</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Et</w> <w n="5.2">rien</w>, <w n="5.3">non</w> <w n="5.4">rien</w> <w n="5.5">ne</w> <w n="5.6">l</w>’<w n="5.7">arrêtait</w>. <w n="5.8">Une</w> <w n="5.9">églantine</w></l>
					<l n="6" num="2.2"><w n="6.1">Piquée</w> <w n="6.2">entre</w> <w n="6.3">les</w> <w n="6.4">dents</w>, <w n="6.5">il</w> <w n="6.6">sautait</w> <w n="6.7">le</w> <w n="6.8">coteau</w></l>
					<l n="7" num="2.3"><w n="7.1">Avec</w> <w n="7.2">les</w> <w n="7.3">chars</w> <w n="7.4">pesants</w>, <w n="7.5">excitait</w> <w n="7.6">les</w> <w n="7.7">chevaux</w>,</l>
					<l n="8" num="2.4"><w n="8.1">Puis</w> <w n="8.2">il</w> <w n="8.3">redescendait</w> <w n="8.4">en</w> <w n="8.5">chassant</w> <w n="8.6">les</w> <w n="8.7">gélines</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Ainsi</w>, <w n="9.2">cent</w> <w n="9.3">fois</w> <w n="9.4">le</w> <w n="9.5">jour</w>, <w n="9.6">sans</w> <w n="9.7">jamais</w> <w n="9.8">se</w> <w n="9.9">lasser</w>,</l>
					<l n="10" num="3.2"><w n="10.1">Glissant</w> <w n="10.2">comme</w> <w n="10.3">un</w> <w n="10.4">orvet</w> <w n="10.5">parmi</w> <w n="10.6">les</w> <w n="10.7">longs</w> <w n="10.8">fossés</w></l>
					<l n="11" num="3.3"><w n="11.1">Ou</w> <w n="11.2">dominant</w> <w n="11.3">de</w> <w n="11.4">haut</w> <w n="11.5">un</w> <w n="11.6">grand</w> <w n="11.7">carré</w> <w n="11.8">de</w> <w n="11.9">trèfle</w>,</l>
				</lg>
				<lg n="4">
					<l n="12" num="4.1"><w n="12.1">Il</w> <w n="12.2">allait</w> <w n="12.3">et</w> <w n="12.4">venait</w>, <w n="12.5">lançant</w> <w n="12.6">une</w> <w n="12.7">alouette</w></l>
					<l n="13" num="4.2"><w n="13.1">Au</w> <w n="13.2">ciel</w> <w n="13.3">comme</w> <w n="13.4">un</w> <w n="13.5">caillou</w>, <w n="13.6">mâchonnant</w> <w n="13.7">une</w> <w n="13.8">nèfle</w>,</l>
					<l n="14" num="4.3"><w n="14.1">Riant</w> <w n="14.2">de</w> <w n="14.3">tant</w> <w n="14.4">courir</w> <w n="14.5">sans</w> <w n="14.6">user</w> <w n="14.7">ses</w> <w n="14.8">souliers</w>.</l>
				</lg>
			</div></body></text></TEI>