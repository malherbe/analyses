<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRABANT</title>
				<title type="medium">Une édition électronique</title>
				<author key="CRM">
					<name>
						<forename>Maurice</forename>
						<surname>CARÊME</surname>
					</name>
					<date from="1899" to="1978">1899-1978</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3326 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CRM_1</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Brabant</title>
						<author>Maurice Carême</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LES ÉDITIONS OUVRIÈRES</publisher>
							<date when="1976">1976</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1967">1967</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-02-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-02-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CRM10">
				<head type="main">MON FRÈRE CHARLES D’ORLÉANS</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Mon</w> <w n="1.2">frère</w> <w n="1.3">Charles</w> <w n="1.4">d</w>’<w n="1.5">Orléans</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Je</w> <w n="2.2">suis</w> <w n="2.3">pauvre</w> <w n="2.4">et</w> <w n="2.5">tu</w> <w n="2.6">étais</w> <w n="2.7">riche</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Mais</w> <w n="3.2">je</w> <w n="3.3">n</w>’<w n="3.4">ai</w> <w n="3.5">pas</w> <w n="3.6">laissé</w> <w n="3.7">en</w> <w n="3.8">friche</w></l>
					<l n="4" num="1.4"><w n="4.1">Mon</w> <w n="4.2">maigre</w> <w n="4.3">carré</w> <w n="4.4">de</w> <w n="4.5">froment</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Petits</w> <w n="5.2">métiers</w>, <w n="5.3">petits</w> <w n="5.4">paniers</w>,</l>
					<l n="6" num="2.2"><w n="6.1">Mes</w> <w n="6.2">grands</w>-<w n="6.3">parents</w> <w n="6.4">étaient</w> <w n="6.5">forains</w></l>
					<l n="7" num="2.3"><w n="7.1">Et</w> <w n="7.2">ils</w> <w n="7.3">n</w>’<w n="7.4">avaient</w> <w n="7.5">sur</w> <w n="7.6">leur</w> <w n="7.7">quartier</w></l>
					<l n="8" num="2.4"><w n="8.1">Qu</w>’<w n="8.2">un</w> <w n="8.3">cerceau</w> <w n="8.4">et</w> <w n="8.5">un</w> <w n="8.6">arlequin</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">On</w> <w n="9.2">dit</w> <w n="9.3">qu</w>’<w n="9.4">en</w> <w n="9.5">ton</w> <w n="9.6">château</w> <w n="9.7">de</w> <w n="9.8">Blois</w>,</l>
					<l n="10" num="3.2"><w n="10.1">Les</w> <w n="10.2">fontaines</w> <w n="10.3">montaient</w> <w n="10.4">si</w> <w n="10.5">claires</w></l>
					<l n="11" num="3.3"><w n="11.1">Que</w> <w n="11.2">l</w>’<w n="11.3">on</w> <w n="11.4">y</w> <w n="11.5">voyait</w> <w n="11.6">au</w> <w n="11.7">travers</w></l>
					<l n="12" num="3.4"><w n="12.1">S</w>’<w n="12.2">abreuver</w> <w n="12.3">les</w> <w n="12.4">chevreuils</w> <w n="12.5">du</w> <w n="12.6">roi</w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Mais</w> <w n="13.2">ma</w> <w n="13.3">mère</w> <w n="13.4">était</w> <w n="13.5">une</w> <w n="13.6">reine</w></l>
					<l n="14" num="4.2"><w n="14.1">Couronnée</w> <w n="14.2">de</w> <w n="14.3">vrais</w> <w n="14.4">liserons</w></l>
					<l n="15" num="4.3"><w n="15.1">Dans</w> <w n="15.2">la</w> <w n="15.3">plus</w> <w n="15.4">petite</w> <w n="15.5">maison</w></l>
					<l n="16" num="4.4"><w n="16.1">De</w> <w n="16.2">l</w>’<w n="16.3">étroite</w> <w n="16.4">rue</w> <w n="16.5">des</w> <w n="16.6">Fontaines</w>.</l>
				</lg>
			</div></body></text></TEI>