<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES SONNETS DU DOCTEUR</title>
				<title type="medium">Édition électronique</title>
				<author key="CMS">
					<name>
						<forename>Georges</forename>
						<surname>CAMUSET</surname>
					</name>
					<date from="1840" to="1885">1840-1885</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>522 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CMS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Les Sonnets du docteur</title>
						<author>Georges Camuset</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/BIUSante_80646</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Sonnets du docteur</title>
								<author>Georges Camuset</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>Chez la plupart des libraires</publisher>
									<date when=" 1884">1884</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Sonnets du docteur</title>
						<author>Georges Camuset</author>
						<idno type="URL">https ://www.google.fr/books/edition/Les_sonnets_du_docteur/bsv60_A0jdUC</idno>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>Chez la plupart des libraires</publisher>
							<date when=" 1884">1884</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1884">1884</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poémes absents de l’édition numérique initiale ("Les Préservatifs" et "Du Signe certain de la mort") ont été ajoutés à partir du texte disponible sur Google books.</p>
				<p>Les notes qui suivent les titres dans la table des matières ont été mis en fin de poème.</p>
				<p>La lettre manuscrite de Charles Monselet n’est pas incluse dans la présente édition</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="CMS11">
				<head type="main">MASSAGE</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">DANS</w> <w n="1.2">les</w> <w n="1.3">nuits</w> <w n="1.4">sans</w> <w n="1.5">sommeil</w> <w n="1.6">l</w>’<w n="1.7">amour</w> <w n="1.8">vous</w> <w n="1.9">a</w> <w n="1.10">blêmie</w></l>
					<l n="2" num="1.2"><w n="2.1">Et</w> <w n="2.2">vos</w> <w n="2.3">chairs</w> <w n="2.4">ont</w> <w n="2.5">perdu</w> <w n="2.6">leur</w> <w n="2.7">tonus</w>, <w n="2.8">ô</w> <w n="2.9">ma</w> <w n="2.10">sœur</w> !</l>
					<l n="3" num="1.3"><w n="3.1">Maintenant</w> <w n="3.2">il</w> <w n="3.3">vous</w> <w n="3.4">faut</w> <w n="3.5">confier</w> <w n="3.6">au</w> <w n="3.7">masseur</w></l>
					<l n="4" num="1.4"><w n="4.1">Les</w> <w n="4.2">trésors</w> <w n="4.3">alanguis</w> <w n="4.4">de</w> <w n="4.5">votre</w> <w n="4.6">anatomie</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Ointes</w> <w n="5.2">d</w>’<w n="5.3">une</w> <w n="5.4">huile</w> <w n="5.5">ambrée</w>, <w n="5.6">effort</w> <w n="5.7">de</w> <w n="5.8">la</w> <w n="5.9">chimie</w>.</l>
					<l n="6" num="2.2"><w n="6.1">Ses</w> <w n="6.2">mains</w>, <w n="6.3">en</w> <w n="6.4">qui</w> <w n="6.5">la</w> <w n="6.6">force</w> <w n="6.7">épouse</w> <w n="6.8">la</w> <w n="6.9">douceur</w>.</l>
					<l n="7" num="2.3"><w n="7.1">Pressent</w> <w n="7.2">le</w> <w n="7.3">grand</w>-<w n="7.4">dorsal</w>, <w n="7.5">malaxent</w> <w n="7.6">l</w>’<w n="7.7">extenseur</w>.</l>
					<l n="8" num="2.4"><w n="8.1">Pour</w> <w n="8.2">des</w> <w n="8.3">combats</w> <w n="8.4">nouveaux</w> <w n="8.5">vous</w> <w n="8.6">voilà</w> <w n="8.7">raffermie</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Jadis</w> <w n="9.2">votre</w> <w n="9.3">docteur</w>, <w n="9.4">plein</w> <w n="9.5">de</w> <w n="9.6">calme</w> <w n="9.7">aujourd</w>’<w n="9.8">hui</w>.</l>
					<l n="10" num="3.2"><w n="10.1">Massait</w> <w n="10.2">fougueusement</w> <w n="10.3">sur</w> <w n="10.4">des</w> <w n="10.5">lits</w> <w n="10.6">de</w> <w n="10.7">pervenches</w> .</l>
					<l n="11" num="3.3"><w n="11.1">Il</w> <w n="11.2">opère</w> <w n="11.3">à</w> <w n="11.4">présent</w> <w n="11.5">pour</w> <w n="11.6">le</w> <w n="11.7">compte</w> <w n="11.8">d</w>’<w n="11.9">autrui</w>.</l>
				</lg>
				<lg n="4">
					<l n="12" num="4.1"><w n="12.1">Tel</w>, <w n="12.2">plongeant</w> <w n="12.3">ses</w> <w n="12.4">bras</w> <w n="12.5">nus</w> <w n="12.6">au</w> <w n="12.7">sein</w> <w n="12.8">des</w> <w n="12.9">pâtes</w> <w n="12.10">blanches</w>.</l>
					<l n="13" num="4.2"><w n="13.1">Le</w> <w n="13.2">gindre</w> <w n="13.3">enfariné</w>, <w n="13.4">dévêtu</w> <w n="13.5">jusqu</w>’<w n="13.6">aux</w> <w n="13.7">hanches</w>.</l>
					<l n="14" num="4.3"><w n="14.1">Pétrit</w> <w n="14.2">des</w> <w n="14.3">petits</w> <w n="14.4">pains</w> — <w n="14.5">qui</w> <w n="14.6">ne</w> <w n="14.7">sont</w> <w n="14.8">pas</w> <w n="14.9">pour</w> <w n="14.10">lui</w>.</l>
				</lg>
			</div></body></text></TEI>