<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES SONNETS DU DOCTEUR</title>
				<title type="medium">Édition électronique</title>
				<author key="CMS">
					<name>
						<forename>Georges</forename>
						<surname>CAMUSET</surname>
					</name>
					<date from="1840" to="1885">1840-1885</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>522 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CMS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Les Sonnets du docteur</title>
						<author>Georges Camuset</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/BIUSante_80646</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Sonnets du docteur</title>
								<author>Georges Camuset</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>Chez la plupart des libraires</publisher>
									<date when=" 1884">1884</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Sonnets du docteur</title>
						<author>Georges Camuset</author>
						<idno type="URL">https ://www.google.fr/books/edition/Les_sonnets_du_docteur/bsv60_A0jdUC</idno>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>Chez la plupart des libraires</publisher>
							<date when=" 1884">1884</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1884">1884</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poémes absents de l’édition numérique initiale ("Les Préservatifs" et "Du Signe certain de la mort") ont été ajoutés à partir du texte disponible sur Google books.</p>
				<p>Les notes qui suivent les titres dans la table des matières ont été mis en fin de poème.</p>
				<p>La lettre manuscrite de Charles Monselet n’est pas incluse dans la présente édition</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="CMS34">
				<head type="main">LANGUE FUMÉE</head>
				<opener>
					<salute>A M…, par colis postal.</salute>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">PAS</w> <w n="1.2">d</w>’<w n="1.3">aube</w>. <w n="1.4">Le</w> <w n="1.5">soleil</w> <w n="1.6">surgit</w>. <w n="1.7">Il</w> <w n="1.8">illumine</w></l>
					<l n="2" num="1.2"><w n="2.1">Les</w> <w n="2.2">grèves</w> <w n="2.3">de</w> <w n="2.4">Mannâr</w> <w n="2.5">et</w> <w n="2.6">les</w> <w n="2.7">flots</w> <w n="2.8">attiédis</w>.</l>
					<l n="3" num="1.3"><w n="3.1">Noirs</w> <w n="3.2">démons</w> <w n="3.3">oubliés</w> <w n="3.4">dans</w> <w n="3.5">ces</w> <w n="3.6">verts</w> <w n="3.7">paradis</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Les</w> <w n="4.2">Cinghalais</w>, <w n="4.3">gagnant</w> <w n="4.4">quelque</w> <w n="4.5">roche</w> <w n="4.6">marine</w>,</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">S</w>’<w n="5.2">élancent</w> <w n="5.3">dans</w> <w n="5.4">l</w>’<w n="5.5">abîme</w> <w n="5.6">où</w> <w n="5.7">dort</w> <w n="5.8">là</w> <w n="5.9">pintadine</w>.</l>
					<l n="6" num="2.2"><w n="6.1">Sous</w> <w n="6.2">le</w> <w n="6.3">faix</w> <w n="6.4">du</w> <w n="6.5">butin</w> <w n="6.6">ils</w> <w n="6.7">nagent</w>, <w n="6.8">alourdis</w> ;</l>
					<l n="7" num="2.3"><w n="7.1">Puis</w>, <w n="7.2">dans</w> <w n="7.3">la</w> <w n="7.4">chair</w> <w n="7.5">nacrée</w> <w n="7.6">ouvrant</w> <w n="7.7">leurs</w> <w n="7.8">doigts</w> <w n="7.9">hardis</w>.</l>
					<l n="8" num="2.4"><w n="8.1">De</w> <w n="8.2">son</w> <w n="8.3">écrin</w> <w n="8.4">vivant</w> <w n="8.5">tirent</w> <w n="8.6">la</w> <w n="8.7">perle</w> <w n="8.8">fine</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Je</w> <w n="9.2">suis</w>, <w n="9.3">par</w> <w n="9.4">le</w> <w n="9.5">courage</w>, <w n="9.6">à</w> <w n="9.7">ces</w> <w n="9.8">pêcheurs</w> <w n="9.9">pareil</w>.</l>
					<l n="10" num="3.2"><w n="10.1">Des</w> <w n="10.2">hauteurs</w> <w n="10.3">de</w> <w n="10.4">Paris</w> <w n="10.5">plongeant</w> <w n="10.6">dans</w> <w n="10.7">la</w> <w n="10.8">province</w>.</l>
					<l n="11" num="3.3"><w n="11.1">Où</w> <w n="11.2">je</w> <w n="11.3">ne</w> <w n="11.4">puis</w> <w n="11.5">revoir</w>, <w n="11.6">hélas</w> ! <w n="11.7">qu</w>’<w n="11.8">en</w> <w n="11.9">mon</w> <w n="11.10">sommeil</w>,</l>
				</lg>
				<lg n="4">
					<l n="12" num="4.1"><w n="12.1">Monselet</w> <w n="12.2">pourléchant</w> <w n="12.3">sa</w> <w n="12.4">double</w> <w n="12.5">badigoince</w>.</l>
					<l n="13" num="4.2"><w n="13.1">J</w>’<w n="13.2">arrache</w> <w n="13.3">au</w> <w n="13.4">goufre</w> <w n="13.5">amer</w> <w n="13.6">un</w> <w n="13.7">trésor</w> <w n="13.8">sans</w> <w n="13.9">rival</w> ;</l>
					<l n="14" num="4.3"><w n="14.1">Cette</w> <w n="14.2">langue</w> <w n="14.3">signée</w> <w n="14.4">Aubelle</w>-<w n="14.5">Méneval</w>.</l>
				</lg>
				<closer>
					<dateline>
						(<placeName>DIJON</placeName>)
					</dateline>
					<note type="footnote" id="">La moutarde et la charcuterie, ces deux soeurs jumelles, florissent parallèlement dans la capitale bourguignonne. Spectacle enchanteur ! Ici Monsieur Poupon remplit sans trève ses petits pots de faïence pale ; là Madame Aubelle détaille sans relâche ses jambons d’un rose incomparable.</note>
				</closer>
			</div></body></text></TEI>