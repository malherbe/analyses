<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES SONNETS DU DOCTEUR</title>
				<title type="medium">Édition électronique</title>
				<author key="CMS">
					<name>
						<forename>Georges</forename>
						<surname>CAMUSET</surname>
					</name>
					<date from="1840" to="1885">1840-1885</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>522 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CMS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Les Sonnets du docteur</title>
						<author>Georges Camuset</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/BIUSante_80646</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Sonnets du docteur</title>
								<author>Georges Camuset</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>Chez la plupart des libraires</publisher>
									<date when=" 1884">1884</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Sonnets du docteur</title>
						<author>Georges Camuset</author>
						<idno type="URL">https ://www.google.fr/books/edition/Les_sonnets_du_docteur/bsv60_A0jdUC</idno>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>Chez la plupart des libraires</publisher>
							<date when=" 1884">1884</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1884">1884</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poémes absents de l’édition numérique initiale ("Les Préservatifs" et "Du Signe certain de la mort") ont été ajoutés à partir du texte disponible sur Google books.</p>
				<p>Les notes qui suivent les titres dans la table des matières ont été mis en fin de poème.</p>
				<p>La lettre manuscrite de Charles Monselet n’est pas incluse dans la présente édition</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="CMS17">
				<head type="main">MALADIES SECRÈTES</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">MARQUIS</w> <w n="1.2">de</w> <w n="1.3">Rambuteau</w>, <w n="1.4">j</w>’<w n="1.5">aime</w> <w n="1.6">ces</w> <w n="1.7">labyrinthes</w></l>
					<l n="2" num="1.2"><w n="2.1">Dont</w> <w n="2.2">ta</w> <w n="2.3">main</w> <w n="2.4">paternelle</w> <w n="2.5">a</w> <w n="2.6">semé</w> <w n="2.7">nos</w> <w n="2.8">trottoirs</w>.</l>
					<l n="3" num="1.3"><w n="3.1">Leur</w> <w n="3.2">front</w> <w n="3.3">lumineux</w> <w n="3.4">porte</w> <w n="3.5">au</w> <w n="3.6">sein</w> <w n="3.7">des</w> <w n="3.8">brouillards</w> <w n="3.9">noirs</w></l>
					<l n="4" num="1.4"><w n="4.1">Le</w> <w n="4.2">nom</w> <w n="4.3">des</w> <w n="4.4">Bodegas</w> <w n="4.5">et</w> <w n="4.6">des</w> <w n="4.7">Eucalypsinthes</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Leurs</w> <w n="5.2">murs</w> <w n="5.3">sont</w> <w n="5.4">diaprés</w> <w n="5.5">du</w> <w n="5.6">faîte</w> <w n="5.7">jusqu</w>’<w n="5.8">aux</w> <w n="5.9">plinthes</w></l>
					<l n="6" num="2.2"><w n="6.1">D</w>’<w n="6.2">avis</w> <w n="6.3">offerts</w> <w n="6.4">gratis</w> <w n="6.5">à</w> <w n="6.6">d</w>’<w n="6.7">amers</w> <w n="6.8">désespoirs</w> ;</l>
					<l n="7" num="2.3"><w n="7.1">Et</w> <w n="7.2">c</w>’<w n="7.3">est</w> <w n="7.4">pourquoi</w> <w n="7.5">j</w>’<w n="7.6">entends</w>, <w n="7.7">le</w> <w n="7.8">long</w> <w n="7.9">des</w> <w n="7.10">réservoirs</w>,</l>
					<l n="8" num="2.4"><w n="8.1">Dans</w> <w n="8.2">le</w> <w n="8.3">gazouillement</w> <w n="8.4">des</w> <w n="8.5">eaux</w>, <w n="8.6">monter</w> <w n="8.7">des</w> <w n="8.8">plaintes</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">O</w> <w n="9.2">l</w>’<w n="9.3">anxieux</w> <w n="9.4">regard</w> <w n="9.5">du</w> <w n="9.6">malade</w> <w n="9.7">éperdu</w></l>
					<l n="10" num="3.2"><w n="10.1">Quand</w> <w n="10.2">il</w> <w n="10.3">franchit</w> <w n="10.4">ton</w> <w n="10.5">seuil</w>, <w n="10.6">temple</w> <w n="10.7">du</w> <w n="10.8">copahu</w> !</l>
					<l n="11" num="3.3"><w n="11.1">Moi</w>, <w n="11.2">j</w>’<w n="11.3">en</w> <w n="11.4">sors</w> <w n="11.5">souriant</w>, <w n="11.6">car</w> <w n="11.7">j</w>’<w n="11.8">eus</w> <w n="11.9">des</w> <w n="11.10">mœurs</w> <w n="11.11">austères</w>.</l>
				</lg>
				<lg n="4">
					<l n="12" num="4.1"><w n="12.1">Mes</w> <w n="12.2">organes</w> <w n="12.3">sont</w> <w n="12.4">purs</w> <w n="12.5">comme</w> <w n="12.6">ceux</w> <w n="12.7">des</w> <w n="12.8">agneaux</w>.</l>
					<l n="13" num="4.2"><w n="13.1">L</w>’<w n="13.2">âge</w> <w n="13.3">les</w> <w n="13.4">rend</w> <w n="13.5">peut</w>-<w n="13.6">être</w> <w n="13.7">un</w> <w n="13.8">peu</w> <w n="13.9">moins</w> <w n="13.10">génitaux</w>.</l>
					<l n="14" num="4.3"><w n="14.1">Mais</w> <w n="14.2">ils</w> <w n="14.3">sont</w> <w n="14.4">demeurés</w> <w n="14.5">largement</w> <w n="14.6">urinaires</w>.</l>
				</lg>
				<closer>
					<note type="footnote" id="">Les <hi rend="ital">Bodegas</hi> sont des établissements de mastroquets exotiques où les vins de Cette se vendent en espagnol (<hi rend="ital">aqui se habla</hi>), — Et l’Eucalypsinthe ! Rêve d’un liquoriste marseillais qui prétendait avec l’essence d’eucalyptus imiter l’absinthe !</note>
				</closer>
			</div></body></text></TEI>