<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES SONNETS DU DOCTEUR</title>
				<title type="medium">Édition électronique</title>
				<author key="CMS">
					<name>
						<forename>Georges</forename>
						<surname>CAMUSET</surname>
					</name>
					<date from="1840" to="1885">1840-1885</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>522 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CMS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Les Sonnets du docteur</title>
						<author>Georges Camuset</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/BIUSante_80646</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Sonnets du docteur</title>
								<author>Georges Camuset</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>Chez la plupart des libraires</publisher>
									<date when=" 1884">1884</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Sonnets du docteur</title>
						<author>Georges Camuset</author>
						<idno type="URL">https ://www.google.fr/books/edition/Les_sonnets_du_docteur/bsv60_A0jdUC</idno>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>Chez la plupart des libraires</publisher>
							<date when=" 1884">1884</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1884">1884</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poémes absents de l’édition numérique initiale ("Les Préservatifs" et "Du Signe certain de la mort") ont été ajoutés à partir du texte disponible sur Google books.</p>
				<p>Les notes qui suivent les titres dans la table des matières ont été mis en fin de poème.</p>
				<p>La lettre manuscrite de Charles Monselet n’est pas incluse dans la présente édition</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="CMS30">
				<head type="main">HOMARD NATURE</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">LE</w> <w n="1.2">homard</w> <w n="1.3">est</w> <w n="1.4">enfin</w> <w n="1.5">sorti</w> <w n="1.6">du</w> <w n="1.7">court</w>-<w n="1.8">bouillon</w>.</l>
					<l n="2" num="1.2"><w n="2.1">Au</w> <w n="2.2">sein</w> <w n="2.3">de</w> <w n="2.4">la</w> <w n="2.5">mixture</w> <w n="2.6">épicée</w> <w n="2.7">et</w> <w n="2.8">brûlante</w></l>
					<l n="3" num="1.3"><w n="3.1">Il</w> <w n="3.2">vient</w> <w n="3.3">de</w> <w n="3.4">revêtir</w> <w n="3.5">son</w> <w n="3.6">harnais</w> <w n="3.7">vermillon</w></l>
					<l n="4" num="1.4"><w n="4.1">Qu</w>’<w n="4.2">il</w> <w n="4.3">étale</w>, <w n="4.4">couché</w> <w n="4.5">dans</w> <w n="4.6">l</w>’<w n="4.7">herbe</w> <w n="4.8">verdoyante</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Piquant</w> <w n="5.2">comme</w> <w n="5.3">un</w> <w n="5.4">cactus</w>, <w n="5.5">dur</w> <w n="5.6">comme</w> <w n="5.7">un</w> <w n="5.8">mirmillon</w>.</l>
					<l n="6" num="2.2"><w n="6.1">Il</w> <w n="6.2">oppose</w> <w n="6.3">au</w> <w n="6.4">couteau</w> <w n="6.5">son</w> <w n="6.6">armure</w> <w n="6.7">savante</w> ;</l>
					<l n="7" num="2.3"><w n="7.1">Vain</w> <w n="7.2">refuge</w>, <w n="7.3">où</w> <w n="7.4">ma</w> <w n="7.5">main</w> <w n="7.6">ferme</w> <w n="7.7">et</w> <w n="7.8">persévérante</w></l>
					<l n="8" num="2.4"><w n="8.1">Creuse</w>, <w n="8.2">d</w>’<w n="8.3">un</w> <w n="8.4">bout</w> <w n="8.5">à</w> <w n="8.6">l</w>’<w n="8.7">autre</w>, <w n="8.8">un</w> <w n="8.9">énorme</w> <w n="8.10">sillon</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">O</w> <w n="9.2">chair</w> <w n="9.3">incarnadine</w> <w n="9.4">et</w> <w n="9.5">pâle</w> <w n="9.6">de</w> <w n="9.7">la</w> <w n="9.8">queue</w> !</l>
					<l n="10" num="3.2"><w n="10.1">Pinces</w>, <w n="10.2">qui</w> <w n="10.3">me</w> <w n="10.4">faisiez</w> <w n="10.5">naguère</w> <w n="10.6">une</w> <w n="10.7">peur</w> <w n="10.8">bleue</w> !</l>
					<l n="11" num="3.3"><w n="11.1">Anfractuosités</w> <w n="11.2">que</w> <w n="11.3">j</w>’<w n="11.4">adore</w> <w n="11.5">fouiller</w> !</l>
				</lg>
				<lg n="4">
					<l n="12" num="4.1"><w n="12.1">Votre</w> <w n="12.2">alléchant</w> <w n="12.3">fumet</w> <w n="12.4">trouble</w> <w n="12.5">les</w> <w n="12.6">plus</w> <w n="12.7">bégueules</w>,</l>
					<l n="13" num="4.2"><w n="13.1">Et</w> <w n="13.2">mon</w> <w n="13.3">cœur</w> <w n="13.4">bat</w> <w n="13.5">plus</w> <w n="13.6">fort</w> <w n="13.7">lorsque</w> <w n="13.8">le</w> <w n="13.9">sommelier</w></l>
					<l n="14" num="4.3"><w n="14.1">Met</w> <w n="14.2">le</w> <w n="14.3">Sauternes</w> <w n="14.4">d</w>’<w n="14.5">or</w> <w n="14.6">près</w> <w n="14.7">du</w> <w n="14.8">Homard</w> <w n="14.9">de</w> <w n="14.10">gueules</w>.</l>
				</lg>
				<closer>
					<note type="footnote" id=""><hi rend="ital">Mirmillon</hi>, gladiateur casqué et bardé de fer que l’on opposait au<hi rend="ital">rétiaire</hi> dans les Jeux du Cirque.</note>
				</closer>
			</div></body></text></TEI>