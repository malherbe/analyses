<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES SONNETS DU DOCTEUR</title>
				<title type="medium">Édition électronique</title>
				<author key="CMS">
					<name>
						<forename>Georges</forename>
						<surname>CAMUSET</surname>
					</name>
					<date from="1840" to="1885">1840-1885</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>522 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CMS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Les Sonnets du docteur</title>
						<author>Georges Camuset</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/BIUSante_80646</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Sonnets du docteur</title>
								<author>Georges Camuset</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>Chez la plupart des libraires</publisher>
									<date when=" 1884">1884</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Sonnets du docteur</title>
						<author>Georges Camuset</author>
						<idno type="URL">https ://www.google.fr/books/edition/Les_sonnets_du_docteur/bsv60_A0jdUC</idno>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>Chez la plupart des libraires</publisher>
							<date when=" 1884">1884</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1884">1884</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poémes absents de l’édition numérique initiale ("Les Préservatifs" et "Du Signe certain de la mort") ont été ajoutés à partir du texte disponible sur Google books.</p>
				<p>Les notes qui suivent les titres dans la table des matières ont été mis en fin de poème.</p>
				<p>La lettre manuscrite de Charles Monselet n’est pas incluse dans la présente édition</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="CMS13">
				<head type="main">DERMATOLOGIE</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">SOUS</w> <w n="1.2">les</w> <w n="1.3">rideaux</w> <w n="1.4">discrets</w> <w n="1.5">au</w> <w n="1.6">fond</w> <w n="1.7">du</w> <w n="1.8">vieil</w> <w n="1.9">hospice</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Les</w> <w n="2.2">sylphes</w> <w n="2.3">du</w> <w n="2.4">Midi</w>, <w n="2.5">chantés</w> <w n="2.6">par</w> <w n="2.7">Fracastor</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Donnent</w> <w n="3.2">à</w> <w n="3.3">leurs</w> <w n="3.4">amants</w> <w n="3.5">qui</w> <w n="3.6">sommeillent</w> <w n="3.7">encor</w></l>
					<l n="4" num="1.4"><w n="4.1">Des</w> <w n="4.2">baisers</w> <w n="4.3">dont</w> <w n="4.4">la</w> <w n="4.5">trace</w> <w n="4.6">est</w> <w n="4.7">une</w> <w n="4.8">cicatrice</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">La</w> <w n="5.2">rougissante</w> <w n="5.3">Acné</w>, <w n="5.4">l</w>’<w n="5.5">agaçante</w> <w n="5.6">Eczéma</w>,</l>
					<l n="6" num="2.2"><w n="6.1">Chéloïs</w> <w n="6.2">au</w> <w n="6.3">front</w> <w n="6.4">pur</w>. <w n="6.5">Syphilis</w> <w n="6.6">au</w> <w n="6.7">cœur</w> <w n="6.8">tendre</w>,</l>
					<l n="7" num="2.3"><w n="7.1">Purpura</w>, <w n="7.2">Sycosis</w>, <w n="7.3">Éphélis</w>, <w n="7.4">Ecthyma</w></l>
					<l n="8" num="2.4"><w n="8.1">Sur</w> <w n="8.2">la</w> <w n="8.3">peau</w> <w n="8.4">des</w> <w n="8.5">mortels</w> <w n="8.6">préférés</w> <w n="8.7">vont</w> <w n="8.8">s</w>’<w n="8.9">étendre</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Le</w> <w n="9.2">jour</w> <w n="9.3">luit</w>. <w n="9.4">Une</w> <w n="9.5">horde</w> <w n="9.6">envahit</w> <w n="9.7">les</w> <w n="9.8">dortoirs</w>.</l>
					<l n="10" num="3.2"><w n="10.1">Portant</w> <w n="10.2">tabliers</w> <w n="10.3">blancs</w> <w n="10.4">avec</w> <w n="10.5">paletots</w> <w n="10.6">noirs</w>.</l>
					<l n="11" num="3.3"><w n="11.1">Ce</w> <w n="11.2">sont</w> <w n="11.3">les</w> <w n="11.4">ennemis</w> <w n="11.5">des</w> <w n="11.6">virus</w> <w n="11.7">et</w> <w n="11.8">des</w> <w n="11.9">lymphes</w>.</l>
				</lg>
				<lg n="4">
					<l n="12" num="4.1"><w n="12.1">Ils</w> <w n="12.2">vont</w>, <w n="12.3">et</w> <w n="12.4">devant</w> <w n="12.5">eux</w> <w n="12.6">marche</w> <w n="12.7">le</w> <w n="12.8">professeur</w>.</l>
					<l n="13" num="4.2"><w n="13.1">Comme</w> <w n="13.2">un</w> <w n="13.3">faune</w> <w n="13.4">jaloux</w> <w n="13.5">qui</w> <w n="13.6">s</w>’<w n="13.7">avance</w>, <w n="13.8">grondeur</w>.</l>
					<l n="14" num="4.3"><w n="14.1">Pour</w> <w n="14.2">troubler</w> <w n="14.3">vos</w> <w n="14.4">ébats</w> <w n="14.5">amoureux</w>, <w n="14.6">belles</w> <w n="14.7">nymphes</w>.</l>
				</lg>
				<closer>
					<note type="footnote" id="">Le <hi rend="ital">Midi</hi>, hôpital spécial.</note>
					<note type="footnote" id=""><hi rend="ital">Fracastor</hi>, syphiliographe italien du XVI<hi rend="sup">e</hi> siècle. — Et poète ! </note>
				</closer>
			</div></body></text></TEI>