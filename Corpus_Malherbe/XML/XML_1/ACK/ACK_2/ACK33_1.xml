<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES PHILOSOPHIQUES</title>
				<title type="medium">Une édition électronique</title>
				<author key="ACK">
					<name>
						<forename>Louise-Victorine</forename>
						<surname>ACKERMANN</surname>
					</name>
					<date from="1813" to="1890">1813-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1634 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">ACK_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies Philosophiques</title>
						<author>Louise-Victorine Ackermann</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/louiseakermanpoesiesphilosophiques.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres de L. Ackermann</title>
						<author>Louise-Victorine Ackermann</author>
						<imprint>
							<publisher>Alphonse Lemerre, éditeur</publisher>
							<date when="1885">1885</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique
					après application du programme de calcul de la longueur métrique des vers).
				</p>
				<normalization>
					<p>Les balises de pagination ont été supprimées.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="ACK33">
				<head type="number">XI</head>
				<head type="main">La Guerre</head>
				<head type="sub_1">À la mémoire de mon neveu</head>
				<head type="sub_1">Le lieutenant Victor Fabrègue</head>
				<head type="sub_1">Tué à Gravelotte.</head>
				<div type="section" n="1">
					<head type="number">I</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Du</w> <w n="1.2">fer</w>, <w n="1.3">du</w> <w n="1.4">feu</w>, <w n="1.5">du</w> <w n="1.6">sang</w> ! <w n="1.7">C</w>’<w n="1.8">est</w> <w n="1.9">elle</w> ! <w n="1.10">c</w>’<w n="1.11">est</w> <w n="1.12">la</w> <w n="1.13">Guerre</w></l>
						<l n="2" num="1.2"><w n="2.1">Debout</w>, <w n="2.2">le</w> <w n="2.3">bras</w> <w n="2.4">levé</w>, <w n="2.5">superbe</w> <w n="2.6">en</w> <w n="2.7">sa</w> <w n="2.8">colère</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Animant</w> <w n="3.2">le</w> <w n="3.3">combat</w> <w n="3.4">d</w>’<w n="3.5">un</w> <w n="3.6">geste</w> <w n="3.7">souverain</w>.</l>
						<l n="4" num="1.4"><w n="4.1">Aux</w> <w n="4.2">éclats</w> <w n="4.3">de</w> <w n="4.4">sa</w> <w n="4.5">voix</w> <w n="4.6">s</w>’<w n="4.7">ébranlent</w> <w n="4.8">les</w> <w n="4.9">armées</w> ;</l>
						<l n="5" num="1.5"><w n="5.1">Autour</w> <w n="5.2">d</w>’<w n="5.3">elle</w> <w n="5.4">traçant</w> <w n="5.5">des</w> <w n="5.6">lignes</w> <w n="5.7">enflammées</w>,</l>
						<l n="6" num="1.6"><w n="6.1">Les</w> <w n="6.2">canons</w> <w n="6.3">ont</w> <w n="6.4">ouvert</w> <w n="6.5">leurs</w> <w n="6.6">entrailles</w> <w n="6.7">d</w>’<w n="6.8">airain</w>.</l>
					</lg>
					<lg n="2">
						<l n="7" num="2.1"><w n="7.1">Partout</w> <w n="7.2">chars</w>, <w n="7.3">cavaliers</w>, <w n="7.4">chevaux</w>, <w n="7.5">masse</w> <w n="7.6">mouvante</w> !</l>
						<l n="8" num="2.2"><w n="8.1">En</w> <w n="8.2">ce</w> <w n="8.3">flux</w> <w n="8.4">et</w> <w n="8.5">reflux</w>, <w n="8.6">sur</w> <w n="8.7">cette</w> <w n="8.8">mer</w> <w n="8.9">vivante</w>,</l>
						<l n="9" num="2.3"><w n="9.1">A</w> <w n="9.2">son</w> <w n="9.3">appel</w> <w n="9.4">ardent</w> <w n="9.5">l</w>’<w n="9.6">épouvante</w> <w n="9.7">s</w>’<w n="9.8">abat</w>.</l>
						<l n="10" num="2.4"><w n="10.1">Sous</w> <w n="10.2">sa</w> <w n="10.3">main</w> <w n="10.4">qui</w> <w n="10.5">frémit</w>, <w n="10.6">en</w> <w n="10.7">ses</w> <w n="10.8">desseins</w> <w n="10.9">féroces</w>,</l>
						<l n="11" num="2.5"><w n="11.1">Pour</w> <w n="11.2">aider</w> <w n="11.3">et</w> <w n="11.4">fournir</w> <w n="11.5">aux</w> <w n="11.6">massacres</w> <w n="11.7">atroces</w></l>
						<l n="12" num="2.6"><w n="12.1">Toute</w> <w n="12.2">matière</w> <w n="12.3">est</w> <w n="12.4">arme</w>, <w n="12.5">et</w> <w n="12.6">tout</w> <w n="12.7">homme</w> <w n="12.8">soldat</w>.</l>
					</lg>
					<lg n="3">
						<l n="13" num="3.1"><w n="13.1">Puis</w>, <w n="13.2">quand</w> <w n="13.3">elle</w> <w n="13.4">a</w> <w n="13.5">repu</w> <w n="13.6">ses</w> <w n="13.7">yeux</w> <w n="13.8">et</w> <w n="13.9">ses</w> <w n="13.10">oreilles</w></l>
						<l n="14" num="3.2"><w n="14.1">De</w> <w n="14.2">spectacles</w> <w n="14.3">navrants</w>, <w n="14.4">de</w> <w n="14.5">rumeurs</w> <w n="14.6">sans</w> <w n="14.7">pareilles</w>,</l>
						<l n="15" num="3.3"><w n="15.1">Quand</w> <w n="15.2">un</w> <w n="15.3">peuple</w> <w n="15.4">agonise</w> <w n="15.5">en</w> <w n="15.6">son</w> <w n="15.7">tombeau</w> <w n="15.8">couché</w>,</l>
						<l n="16" num="3.4"><w n="16.1">Pâle</w> <w n="16.2">sous</w> <w n="16.3">ses</w> <w n="16.4">lauriers</w>, <w n="16.5">l</w>’<w n="16.6">âme</w> <w n="16.7">d</w>’<w n="16.8">orgueil</w> <w n="16.9">remplie</w>,</l>
						<l n="17" num="3.5"><w n="17.1">Devant</w> <w n="17.2">l</w>’<w n="17.3">œuvre</w> <w n="17.4">achevée</w> <w n="17.5">et</w> <w n="17.6">la</w> <w n="17.7">tâche</w> <w n="17.8">accomplie</w>,</l>
						<l n="18" num="3.6"><w n="18.1">Triomphante</w> <w n="18.2">elle</w> <w n="18.3">crie</w> <w n="18.4">à</w> <w n="18.5">la</w> <w n="18.6">Mort</w> : « <w n="18.7">Bien</w> <w n="18.8">fauché</w> ! » !</l>
					</lg>
					<lg n="4">
						<l n="19" num="4.1"><w n="19.1">Oui</w>, <w n="19.2">bien</w> <w n="19.3">fauché</w> ! <w n="19.4">Vraiment</w> <w n="19.5">la</w> <w n="19.6">récolte</w> <w n="19.7">est</w> <w n="19.8">superbe</w> ;</l>
						<l n="20" num="4.2"><w n="20.1">Pas</w> <w n="20.2">un</w> <w n="20.3">sillon</w> <w n="20.4">qui</w> <w n="20.5">n</w>’<w n="20.6">ait</w> <w n="20.7">des</w> <w n="20.8">cadavres</w> <w n="20.9">pour</w> <w n="20.10">gerbe</w> !</l>
						<l n="21" num="4.3"><w n="21.1">Les</w> <w n="21.2">plus</w> <w n="21.3">beaux</w>, <w n="21.4">les</w> <w n="21.5">plus</w> <w n="21.6">forts</w> <w n="21.7">sont</w> <w n="21.8">les</w> <w n="21.9">premiers</w> <w n="21.10">frappés</w>.</l>
						<l n="22" num="4.4"><w n="22.1">Sur</w> <w n="22.2">son</w> <w n="22.3">sein</w> <w n="22.4">dévasté</w> <w n="22.5">qui</w> <w n="22.6">saigne</w> <w n="22.7">et</w> <w n="22.8">qui</w> <w n="22.9">frissonne</w></l>
						<l n="23" num="4.5"><w n="23.1">L</w>’<w n="23.2">Humanité</w>, <w n="23.3">semblable</w> <w n="23.4">au</w> <w n="23.5">champ</w> <w n="23.6">que</w> <w n="23.7">l</w>’<w n="23.8">on</w> <w n="23.9">moissonne</w>,</l>
						<l n="24" num="4.6"><w n="24.1">Contemple</w> <w n="24.2">avec</w> <w n="24.3">douleur</w> <w n="24.4">tous</w> <w n="24.5">ces</w> <w n="24.6">épis</w> <w n="24.7">coupés</w>.</l>
					</lg>
					<lg n="5">
						<l n="25" num="5.1"><w n="25.1">Hélas</w> ! <w n="25.2">au</w> <w n="25.3">gré</w> <w n="25.4">du</w> <w n="25.5">vent</w> <w n="25.6">et</w> <w n="25.7">sous</w> <w n="25.8">sa</w> <w n="25.9">douce</w> <w n="25.10">haleine</w></l>
						<l n="26" num="5.2"><w n="26.1">Ils</w> <w n="26.2">ondulaient</w> <w n="26.3">au</w> <w n="26.4">loin</w>, <w n="26.5">des</w> <w n="26.6">coteaux</w> <w n="26.7">à</w> <w n="26.8">la</w> <w n="26.9">plaine</w>,</l>
						<l n="27" num="5.3"><w n="27.1">Sur</w> <w n="27.2">la</w> <w n="27.3">tige</w> <w n="27.4">encor</w> <w n="27.5">verte</w> <w n="27.6">attendant</w> <w n="27.7">leur</w> <w n="27.8">saison</w>.</l>
						<l n="28" num="5.4"><w n="28.1">Le</w> <w n="28.2">soleil</w> <w n="28.3">leur</w> <w n="28.4">versait</w> <w n="28.5">ses</w> <w n="28.6">rayons</w> <w n="28.7">magnifiques</w> ;</l>
						<l n="29" num="5.5"><w n="29.1">Riches</w> <w n="29.2">de</w> <w n="29.3">leur</w> <w n="29.4">trésor</w>, <w n="29.5">sous</w> <w n="29.6">les</w> <w n="29.7">cieux</w> <w n="29.8">pacifiques</w>,</l>
						<l n="30" num="5.6"><w n="30.1">Ils</w> <w n="30.2">auraient</w> <w n="30.3">pu</w> <w n="30.4">mûrir</w> <w n="30.5">pour</w> <w n="30.6">une</w> <w n="30.7">autre</w> <w n="30.8">moisson</w>.</l>
					</lg>
				</div>
				<div type="section" n="2">
					<head type="number">II</head>
					<lg n="1">
						<l n="31" num="1.1"><w n="31.1">Si</w> <w n="31.2">vivre</w> <w n="31.3">c</w>’<w n="31.4">est</w> <w n="31.5">lutter</w>, <w n="31.6">à</w> <w n="31.7">l</w>’<w n="31.8">humaine</w> <w n="31.9">énergie</w></l>
						<l n="32" num="1.2"><w n="32.1">Pourquoi</w> <w n="32.2">n</w>’<w n="32.3">ouvrir</w> <w n="32.4">jamais</w> <w n="32.5">qu</w>’<w n="32.6">une</w> <w n="32.7">arène</w> <w n="32.8">rougie</w> ?</l>
						<l n="33" num="1.3"><w n="33.1">Pour</w> <w n="33.2">un</w> <w n="33.3">prix</w> <w n="33.4">moins</w> <w n="33.5">sanglant</w> <w n="33.6">que</w> <w n="33.7">les</w> <w n="33.8">morts</w> <w n="33.9">que</w> <w n="33.10">voilà</w></l>
						<l n="34" num="1.4"><w n="34.1">L</w>’<w n="34.2">homme</w> <w n="34.3">ne</w> <w n="34.4">pourrait</w>-<w n="34.5">il</w> <w n="34.6">concourir</w> <w n="34.7">et</w> <w n="34.8">combattre</w> ?</l>
						<l n="35" num="1.5"><w n="35.1">Manque</w>-<w n="35.2">t</w>-<w n="35.3">il</w> <w n="35.4">d</w>’<w n="35.5">ennemis</w> <w n="35.6">qu</w>’<w n="35.7">il</w> <w n="35.8">serait</w> <w n="35.9">beau</w> <w n="35.10">d</w>’<w n="35.11">abattre</w> ?</l>
						<l n="36" num="1.6"><w n="36.1">Le</w> <w n="36.2">malheureux</w> ! <w n="36.3">il</w> <w n="36.4">cherche</w>, <w n="36.5">et</w> <w n="36.6">la</w> <w n="36.7">Misère</w> <w n="36.8">est</w> <w n="36.9">là</w> !</l>
					</lg>
					<lg n="2">
						<l n="37" num="2.1"><w n="37.1">Qu</w>’<w n="37.2">il</w> <w n="37.3">lui</w> <w n="37.4">crie</w> : « <w n="37.5">A</w> <w n="37.6">nous</w> <w n="37.7">deux</w> ! » <w n="37.8">et</w> <w n="37.9">que</w> <w n="37.10">sa</w> <w n="37.11">main</w> <w n="37.12">virile</w></l>
						<l n="38" num="2.2"><w n="38.1">S</w>’<w n="38.2">acharne</w> <w n="38.3">sans</w> <w n="38.4">merci</w> <w n="38.5">contre</w> <w n="38.6">ce</w> <w n="38.7">flanc</w> <w n="38.8">stérile</w></l>
						<l n="39" num="2.3"><w n="39.1">Qu</w>’<w n="39.2">il</w> <w n="39.3">s</w>’<w n="39.4">agit</w> <w n="39.5">avant</w> <w n="39.6">tout</w> <w n="39.7">d</w>’<w n="39.8">atteindre</w> <w n="39.9">et</w> <w n="39.10">de</w> <w n="39.11">percer</w>.</l>
						<l n="40" num="2.4"><w n="40.1">A</w> <w n="40.2">leur</w> <w n="40.3">tour</w>, <w n="40.4">le</w> <w n="40.5">front</w> <w n="40.6">haut</w>, <w n="40.7">l</w>’<w n="40.8">Ignorance</w> <w n="40.9">et</w> <w n="40.10">le</w> <w n="40.11">Vice</w>,</l>
						<l n="41" num="2.5"><w n="41.1">L</w>’<w n="41.2">un</w> <w n="41.3">sur</w> <w n="41.4">l</w>’<w n="41.5">autre</w> <w n="41.6">appuyé</w>, <w n="41.7">l</w>’<w n="41.8">attendent</w> <w n="41.9">dans</w> <w n="41.10">la</w> <w n="41.11">lice</w> :</l>
						<l n="42" num="2.6"><w n="42.1">Qu</w>’<w n="42.2">il</w> <w n="42.3">y</w> <w n="42.4">descende</w> <w n="42.5">donc</w>, <w n="42.6">et</w> <w n="42.7">pour</w> <w n="42.8">les</w> <w n="42.9">terrasser</w>.</l>
					</lg>
					<lg n="3">
						<l n="43" num="3.1"><w n="43.1">A</w> <w n="43.2">la</w> <w n="43.3">lutte</w> <w n="43.4">entraînez</w> <w n="43.5">les</w> <w n="43.6">nations</w> <w n="43.7">entières</w>.</l>
						<l n="44" num="3.2"><w n="44.1">Délivrance</w> <w n="44.2">partout</w> ! <w n="44.3">effaçant</w> <w n="44.4">les</w> <w n="44.5">frontières</w>,</l>
						<l n="45" num="3.3"><w n="45.1">Unissez</w> <w n="45.2">vos</w> <w n="45.3">élans</w> <w n="45.4">et</w> <w n="45.5">tendez</w>-<w n="45.6">vous</w> <w n="45.7">la</w> <w n="45.8">main</w>.</l>
						<l n="46" num="3.4"><w n="46.1">Dans</w> <w n="46.2">les</w> <w n="46.3">rangs</w> <w n="46.4">ennemis</w> <w n="46.5">et</w> <w n="46.6">vers</w> <w n="46.7">un</w> <w n="46.8">but</w> <w n="46.9">unique</w>,</l>
						<l n="47" num="3.5"><w n="47.1">Pour</w> <w n="47.2">faire</w> <w n="47.3">avec</w> <w n="47.4">succès</w> <w n="47.5">sa</w> <w n="47.6">trouée</w> <w n="47.7">héroïque</w>,</l>
						<l n="48" num="3.6"><w n="48.1">Certes</w> <w n="48.2">ce</w> <w n="48.3">n</w>’<w n="48.4">est</w> <w n="48.5">pas</w> <w n="48.6">trop</w> <w n="48.7">de</w> <w n="48.8">tout</w> <w n="48.9">l</w>’<w n="48.10">effort</w> <w n="48.11">humain</w>.</l>
					</lg>
					<lg n="4">
						<l n="49" num="4.1"><w n="49.1">L</w>’<w n="49.2">heure</w> <w n="49.3">semblait</w> <w n="49.4">propice</w>, <w n="49.5">et</w> <w n="49.6">le</w> <w n="49.7">penseur</w> <w n="49.8">candide</w></l>
						<l n="50" num="4.2"><w n="50.1">Croyait</w>, <w n="50.2">dans</w> <w n="50.3">le</w> <w n="50.4">lointain</w> <w n="50.5">d</w>’<w n="50.6">une</w> <w n="50.7">aurore</w> <w n="50.8">splendide</w>,</l>
						<l n="51" num="4.3"><w n="51.1">Voir</w> <w n="51.2">de</w> <w n="51.3">la</w> <w n="51.4">Paix</w> <w n="51.5">déjà</w> <w n="51.6">poindre</w> <w n="51.7">le</w> <w n="51.8">front</w> <w n="51.9">tremblant</w>.</l>
						<l n="52" num="4.4"><w n="52.1">On</w> <w n="52.2">respirait</w>. <w n="52.3">Soudain</w>, <w n="52.4">la</w> <w n="52.5">trompette</w> <w n="52.6">à</w> <w n="52.7">la</w> <w n="52.8">bouche</w>,</l>
						<l n="53" num="4.5"><w n="53.1">Guerre</w>, <w n="53.2">tu</w> <w n="53.3">reparais</w>, <w n="53.4">plus</w> <w n="53.5">âpre</w>, <w n="53.6">plus</w> <w n="53.7">farouche</w>,</l>
						<l n="54" num="4.6"><w n="54.1">Écrasant</w> <w n="54.2">le</w> <w n="54.3">progrès</w> <w n="54.4">sous</w> <w n="54.5">ton</w> <w n="54.6">talon</w> <w n="54.7">sanglant</w>.</l>
					</lg>
					<lg n="5">
						<l n="55" num="5.1"><w n="55.1">C</w>’<w n="55.2">est</w> <w n="55.3">à</w> <w n="55.4">qui</w> <w n="55.5">le</w> <w n="55.6">premier</w>, <w n="55.7">aveuglé</w> <w n="55.8">de</w> <w n="55.9">furie</w>,</l>
						<l n="56" num="5.2"><w n="56.1">Se</w> <w n="56.2">précipitera</w> <w n="56.3">vers</w> <w n="56.4">l</w>’<w n="56.5">immense</w> <w n="56.6">tuerie</w>.</l>
						<l n="57" num="5.3"><w n="57.1">A</w> <w n="57.2">mort</w> ! <w n="57.3">point</w> <w n="57.4">de</w> <w n="57.5">quartier</w> ! <w n="57.6">L</w>’<w n="57.7">emporter</w> <w n="57.8">ou</w> <w n="57.9">périr</w> !</l>
						<l n="58" num="5.4"><w n="58.1">Cet</w> <w n="58.2">inconnu</w> <w n="58.3">qui</w> <w n="58.4">vient</w> <w n="58.5">des</w> <w n="58.6">champs</w> <w n="58.7">ou</w> <w n="58.8">de</w> <w n="58.9">la</w> <w n="58.10">forge</w></l>
						<l n="59" num="5.5"><w n="59.1">Est</w> <w n="59.2">un</w> <w n="59.3">frère</w> ; <w n="59.4">il</w> <w n="59.5">fallait</w> <w n="59.6">l</w>’<w n="59.7">embrasser</w>, — <w n="59.8">on</w> <w n="59.9">l</w>’<w n="59.10">égorge</w>.</l>
						<l n="60" num="5.6"><w n="60.1">Quoi</w> ! <w n="60.2">lever</w> <w n="60.3">pour</w> <w n="60.4">frapper</w> <w n="60.5">des</w> <w n="60.6">bras</w> <w n="60.7">faits</w> <w n="60.8">pour</w> <w n="60.9">s</w>’<w n="60.10">ouvrir</w> !</l>
					</lg>
					<lg n="6">
						<l n="61" num="6.1"><w n="61.1">Les</w> <w n="61.2">hameaux</w>, <w n="61.3">les</w> <w n="61.4">cités</w> <w n="61.5">s</w>’<w n="61.6">écroulent</w> <w n="61.7">dans</w> <w n="61.8">les</w> <w n="61.9">flammes</w>.</l>
						<l n="62" num="6.2"><w n="62.1">Les</w> <w n="62.2">pierres</w> <w n="62.3">ont</w> <w n="62.4">souffert</w> ; <w n="62.5">mais</w> <w n="62.6">que</w> <w n="62.7">dire</w> <w n="62.8">des</w> <w n="62.9">âmes</w> ?</l>
						<l n="63" num="6.3"><w n="63.1">Près</w> <w n="63.2">des</w> <w n="63.3">pères</w> <w n="63.4">les</w> <w n="63.5">fils</w> <w n="63.6">gisent</w> <w n="63.7">inanimés</w>.</l>
						<l n="64" num="6.4"><w n="64.1">Le</w> <w n="64.2">Deuil</w> <w n="64.3">sombre</w> <w n="64.4">est</w> <w n="64.5">assis</w> <w n="64.6">devant</w> <w n="64.7">les</w> <w n="64.8">foyers</w> <w n="64.9">vides</w>,</l>
						<l n="65" num="6.5"><w n="65.1">Car</w> <w n="65.2">ces</w> <w n="65.3">monceaux</w> <w n="65.4">de</w> <w n="65.5">morts</w>, <w n="65.6">inertes</w> <w n="65.7">et</w> <w n="65.8">livides</w>,</l>
						<l n="66" num="6.6"><w n="66.1">Étaient</w> <w n="66.2">des</w> <w n="66.3">cœurs</w> <w n="66.4">aimants</w> <w n="66.5">et</w> <w n="66.6">des</w> <w n="66.7">êtres</w> <w n="66.8">aimés</w>.</l>
					</lg>
					<lg n="7">
						<l n="67" num="7.1"><w n="67.1">Affaiblis</w> <w n="67.2">et</w> <w n="67.3">ployant</w> <w n="67.4">sous</w> <w n="67.5">la</w> <w n="67.6">tâche</w> <w n="67.7">infinie</w>,</l>
						<l n="68" num="7.2"><w n="68.1">Recommence</w>, <w n="68.2">Travail</w> ! <w n="68.3">rallume</w>-<w n="68.4">toi</w>, <w n="68.5">Génie</w> !</l>
						<l n="69" num="7.3"><w n="69.1">Le</w> <w n="69.2">fruit</w> <w n="69.3">de</w> <w n="69.4">vos</w> <w n="69.5">labeurs</w> <w n="69.6">est</w> <w n="69.7">broyé</w>, <w n="69.8">dispersé</w>.</l>
						<l n="70" num="7.4"><w n="70.1">Mais</w> <w n="70.2">quoi</w> ! <w n="70.3">tous</w> <w n="70.4">ces</w> <w n="70.5">trésors</w> <w n="70.6">ne</w> <w n="70.7">formaient</w> <w n="70.8">qu</w>’<w n="70.9">un</w> <w n="70.10">domaine</w> ;</l>
						<l n="71" num="7.5"><w n="71.1">C</w>’<w n="71.2">était</w> <w n="71.3">le</w> <w n="71.4">bien</w> <w n="71.5">commun</w> <w n="71.6">de</w> <w n="71.7">la</w> <w n="71.8">famille</w> <w n="71.9">humaine</w>,</l>
						<l n="72" num="7.6"><w n="72.1">Se</w> <w n="72.2">ruiner</w> <w n="72.3">soi</w>-<w n="72.4">même</w>, <w n="72.5">ah</w> ! <w n="72.6">c</w>’<w n="72.7">est</w> <w n="72.8">être</w> <w n="72.9">insensé</w> !</l>
					</lg>
					<lg n="8">
						<l n="73" num="8.1"><w n="73.1">Guerre</w>, <w n="73.2">au</w> <w n="73.3">seul</w> <w n="73.4">souvenir</w> <w n="73.5">des</w> <w n="73.6">maux</w> <w n="73.7">que</w> <w n="73.8">tu</w> <w n="73.9">déchaînes</w>,</l>
						<l n="74" num="8.2"><w n="74.1">Fermente</w> <w n="74.2">au</w> <w n="74.3">fond</w> <w n="74.4">des</w> <w n="74.5">cœurs</w> <w n="74.6">le</w> <w n="74.7">vieux</w> <w n="74.8">levain</w> <w n="74.9">des</w> <w n="74.10">haines</w> ;</l>
						<l n="75" num="8.3"><w n="75.1">Dans</w> <w n="75.2">le</w> <w n="75.3">limon</w> <w n="75.4">laissé</w> <w n="75.5">par</w> <w n="75.6">tes</w> <w n="75.7">flots</w> <w n="75.8">ravageurs</w></l>
						<l n="76" num="8.4"><w n="76.1">Des</w> <w n="76.2">germes</w> <w n="76.3">sont</w> <w n="76.4">semés</w> <w n="76.5">de</w> <w n="76.6">rancune</w> <w n="76.7">et</w> <w n="76.8">de</w> <w n="76.9">rage</w>,</l>
						<l n="77" num="8.5"><w n="77.1">Et</w> <w n="77.2">le</w> <w n="77.3">vaincu</w> <w n="77.4">n</w>’<w n="77.5">a</w> <w n="77.6">plus</w>, <w n="77.7">dévorant</w> <w n="77.8">son</w> <w n="77.9">outrage</w>,</l>
						<l n="78" num="8.6"><w n="78.1">Qu</w>’<w n="78.2">un</w> <w n="78.3">désir</w>, <w n="78.4">qu</w>’<w n="78.5">un</w> <w n="78.6">espoir</w> : <w n="78.7">enfanter</w> <w n="78.8">des</w> <w n="78.9">vengeurs</w>.</l>
					</lg>
					<lg n="9">
						<l n="79" num="9.1"><w n="79.1">Ainsi</w> <w n="79.2">le</w> <w n="79.3">genre</w> <w n="79.4">humain</w>, <w n="79.5">à</w> <w n="79.6">force</w> <w n="79.7">de</w> <w n="79.8">revanches</w>,</l>
						<l n="80" num="9.2"><w n="80.1">Arbre</w> <w n="80.2">découronné</w>, <w n="80.3">verra</w> <w n="80.4">mourir</w> <w n="80.5">ses</w> <w n="80.6">branches</w>,</l>
						<l n="81" num="9.3"><w n="81.1">Adieu</w>, <w n="81.2">printemps</w> <w n="81.3">futurs</w> ! <w n="81.4">Adieu</w>, <w n="81.5">soleils</w> <w n="81.6">nouveaux</w> !</l>
						<l n="82" num="9.4"><w n="82.1">En</w> <w n="82.2">ce</w> <w n="82.3">tronc</w> <w n="82.4">mutilé</w> <w n="82.5">la</w> <w n="82.6">sève</w> <w n="82.7">est</w> <w n="82.8">impossible</w>.</l>
						<l n="83" num="9.5"><w n="83.1">Plus</w> <w n="83.2">d</w>’<w n="83.3">ombre</w>, <w n="83.4">plus</w> <w n="83.5">de</w> <w n="83.6">fleurs</w> ! <w n="83.7">et</w> <w n="83.8">ta</w> <w n="83.9">hache</w> <w n="83.10">inflexible</w>,</l>
						<l n="84" num="9.6"><w n="84.1">Pour</w> <w n="84.2">mieux</w> <w n="84.3">frapper</w> <w n="84.4">les</w> <w n="84.5">fruits</w>, <w n="84.6">a</w> <w n="84.7">tranché</w> <w n="84.8">les</w> <w n="84.9">rameaux</w>.</l>
					</lg>
				</div>
				<div type="section" n="3">
					<head type="number">III</head>
					<lg n="1">
						<l n="85" num="1.1"><w n="85.1">Non</w>, <w n="85.2">ce</w> <w n="85.3">n</w>’<w n="85.4">est</w> <w n="85.5">point</w> <w n="85.6">à</w> <w n="85.7">nous</w>, <w n="85.8">penseur</w> <w n="85.9">et</w> <w n="85.10">chantre</w> <w n="85.11">austère</w>,</l>
						<l n="86" num="1.2"><w n="86.1">De</w> <w n="86.2">nier</w> <w n="86.3">les</w> <w n="86.4">grandeurs</w> <w n="86.5">de</w> <w n="86.6">la</w> <w n="86.7">mort</w> <w n="86.8">volontaire</w> ;</l>
						<l n="87" num="1.3"><w n="87.1">D</w>’<w n="87.2">un</w> <w n="87.3">élan</w> <w n="87.4">généreux</w> <w n="87.5">il</w> <w n="87.6">est</w> <w n="87.7">beau</w> <w n="87.8">d</w>’<w n="87.9">y</w> <w n="87.10">courir</w>.</l>
						<l n="88" num="1.4"><w n="88.1">Philosophes</w>, <w n="88.2">savants</w>, <w n="88.3">explorateurs</w>, <w n="88.4">apôtres</w>,</l>
						<l n="89" num="1.5"><w n="89.1">Soldats</w> <w n="89.2">de</w> <w n="89.3">l</w>’<w n="89.4">Idéal</w>, <w n="89.5">ces</w> <w n="89.6">héros</w> <w n="89.7">sont</w> <w n="89.8">les</w> <w n="89.9">nôtres</w> :</l>
						<l n="90" num="1.6"><w n="90.1">Guerre</w> ! <w n="90.2">ils</w> <w n="90.3">sauront</w> <w n="90.4">sans</w> <w n="90.5">toi</w> <w n="90.6">trouver</w> <w n="90.7">pour</w> <w n="90.8">qui</w> <w n="90.9">mourir</w>.</l>
					</lg>
					<lg n="2">
						<l n="91" num="2.1"><w n="91.1">Mais</w> <w n="91.2">à</w> <w n="91.3">ce</w> <w n="91.4">fier</w> <w n="91.5">brutal</w> <w n="91.6">qui</w> <w n="91.7">frappe</w> <w n="91.8">et</w> <w n="91.9">qui</w> <w n="91.10">mutile</w>,</l>
						<l n="92" num="2.2"><w n="92.1">Aux</w> <w n="92.2">exploits</w> <w n="92.3">destructeurs</w>, <w n="92.4">au</w> <w n="92.5">trépas</w> <w n="92.6">inutile</w>,</l>
						<l n="93" num="2.3"><w n="93.1">Ferme</w> <w n="93.2">dans</w> <w n="93.3">mon</w> <w n="93.4">horreur</w>, <w n="93.5">toujours</w> <w n="93.6">je</w> <w n="93.7">dirai</w> : « <w n="93.8">Non</w> ! »</l>
						<l n="94" num="2.4"><w n="94.1">O</w> <w n="94.2">vous</w> <w n="94.3">que</w> <w n="94.4">l</w>’<w n="94.5">Art</w> <w n="94.6">enivre</w> <w n="94.7">ou</w> <w n="94.8">quelque</w> <w n="94.9">noble</w> <w n="94.10">envie</w>,</l>
						<l n="95" num="2.5"><w n="95.1">Qui</w>, <w n="95.2">débordant</w> <w n="95.3">d</w>’<w n="95.4">amour</w>, <w n="95.5">fleurissez</w> <w n="95.6">pour</w> <w n="95.7">la</w> <w n="95.8">vie</w>,</l>
						<l n="96" num="2.6"><w n="96.1">On</w> <w n="96.2">ose</w> <w n="96.3">vous</w> <w n="96.4">jeter</w> <w n="96.5">en</w> <w n="96.6">pâture</w> <w n="96.7">au</w> <w n="96.8">canon</w> !</l>
					</lg>
					<lg n="3">
						<l n="97" num="3.1"><w n="97.1">Liberté</w>, <w n="97.2">Droit</w>, <w n="97.3">Justice</w>, <w n="97.4">affaire</w> <w n="97.5">de</w> <w n="97.6">mitraille</w> !</l>
						<l n="98" num="3.2"><w n="98.1">Pour</w> <w n="98.2">un</w> <w n="98.3">lambeau</w> <w n="98.4">d</w>’<w n="98.5">État</w>, <w n="98.6">pour</w> <w n="98.7">un</w> <w n="98.8">pan</w> <w n="98.9">de</w> <w n="98.10">muraille</w>,</l>
						<l n="99" num="3.3"><w n="99.1">Sans</w> <w n="99.2">pitié</w>, <w n="99.3">sans</w> <w n="99.4">remords</w>, <w n="99.5">un</w> <w n="99.6">peuple</w> <w n="99.7">est</w> <w n="99.8">massacré</w>.</l>
						<l n="100" num="3.4">— <w n="100.1">Mais</w> <w n="100.2">il</w> <w n="100.3">est</w> <w n="100.4">innocent</w> ! — <w n="100.5">Qu</w>’<w n="100.6">importe</w> ? <w n="100.7">On</w> <w n="100.8">l</w>’<w n="100.9">extermine</w>.</l>
						<l n="101" num="3.5"><w n="101.1">Pourtant</w> <w n="101.2">la</w> <w n="101.3">vie</w> <w n="101.4">humaine</w> <w n="101.5">est</w> <w n="101.6">de</w> <w n="101.7">source</w> <w n="101.8">divine</w> :</l>
						<l n="102" num="3.6"><w n="102.1">N</w>’<w n="102.2">y</w> <w n="102.3">touchez</w> <w n="102.4">pas</w>, <w n="102.5">arrière</w> ! <w n="102.6">Un</w> <w n="102.7">homme</w>, <w n="102.8">c</w>’<w n="102.9">est</w> <w n="102.10">sacré</w> !</l>
					</lg>
					<lg n="4">
						<l n="103" num="4.1"><w n="103.1">Sous</w> <w n="103.2">des</w> <w n="103.3">vapeurs</w> <w n="103.4">de</w> <w n="103.5">poudre</w> <w n="103.6">et</w> <w n="103.7">de</w> <w n="103.8">sang</w>, <w n="103.9">quand</w> <w n="103.10">les</w> <w n="103.11">astres</w></l>
						<l n="104" num="4.2"><w n="104.1">Pâlissent</w> <w n="104.2">indignés</w> <w n="104.3">parmi</w> <w n="104.4">tant</w> <w n="104.5">de</w> <w n="104.6">désastres</w>,</l>
						<l n="105" num="4.3"><w n="105.1">Moi</w>-<w n="105.2">même</w> <w n="105.3">à</w> <w n="105.4">la</w> <w n="105.5">fureur</w> <w n="105.6">me</w> <w n="105.7">laissant</w> <w n="105.8">emporter</w>,</l>
						<l n="106" num="4.4"><w n="106.1">Je</w> <w n="106.2">ne</w> <w n="106.3">distingue</w> <w n="106.4">plus</w> <w n="106.5">les</w> <w n="106.6">bourreaux</w> <w n="106.7">des</w> <w n="106.8">victimes</w> ;</l>
						<l n="107" num="4.5"><w n="107.1">Mon</w> <w n="107.2">âme</w> <w n="107.3">se</w> <w n="107.4">soulève</w>, <w n="107.5">et</w> <w n="107.6">devant</w> <w n="107.7">de</w> <w n="107.8">tels</w> <w n="107.9">crimes</w></l>
						<l n="108" num="4.6"><w n="108.1">Je</w> <w n="108.2">voudrais</w> <w n="108.3">être</w> <w n="108.4">foudre</w> <w n="108.5">et</w> <w n="108.6">pouvoir</w> <w n="108.7">éclater</w>.</l>
					</lg>
					<lg n="5">
						<l n="109" num="5.1"><w n="109.1">Du</w> <w n="109.2">moins</w> <w n="109.3">te</w> <w n="109.4">poursuivant</w> <w n="109.5">jusqu</w>’<w n="109.6">en</w> <w n="109.7">pleine</w> <w n="109.8">victoire</w>,</l>
						<l n="110" num="5.2"><w n="110.1">A</w> <w n="110.2">travers</w> <w n="110.3">tes</w> <w n="110.4">lauriers</w>, <w n="110.5">dans</w> <w n="110.6">les</w> <w n="110.7">bras</w> <w n="110.8">de</w> <w n="110.9">l</w>’<w n="110.10">Histoire</w></l>
						<l n="111" num="5.3"><w n="111.1">Qui</w>, <w n="111.2">séduite</w>, <w n="111.3">pourrait</w> <w n="111.4">t</w>’<w n="111.5">absoudre</w> <w n="111.6">et</w> <w n="111.7">te</w> <w n="111.8">sacrer</w>,</l>
						<l n="112" num="5.4"><w n="112.1">O</w> <w n="112.2">Guerre</w>, <w n="112.3">Guerre</w> <w n="112.4">impie</w>, <w n="112.5">assassin</w> <w n="112.6">qu</w>’<w n="112.7">on</w> <w n="112.8">encense</w>,</l>
						<l n="113" num="5.5"><w n="113.1">Je</w> <w n="113.2">resterai</w>, <w n="113.3">navrée</w> <w n="113.4">et</w> <w n="113.5">dans</w> <w n="113.6">mon</w> <w n="113.7">impuissance</w>,</l>
						<l n="114" num="5.6"><w n="114.1">Bouche</w> <w n="114.2">pour</w> <w n="114.3">te</w> <w n="114.4">maudire</w>, <w n="114.5">et</w> <w n="114.6">cœur</w> <w n="114.7">pour</w> <w n="114.8">t</w>’<w n="114.9">exécrer</w> !</l>
					</lg>
					<closer>
						<dateline>
							<placeName>Paris</placeName>,
							<date when="1871">8 février 1871</date>
						</dateline>
					</closer>
				</div>
			</div></body></text></TEI>