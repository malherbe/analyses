<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">PREMIÈRES POÉSIES</title>
				<title type="medium">Une édition électronique</title>
				<author key="ACK">
					<name>
						<forename>Louise-Victorine</forename>
						<surname>ACKERMANN</surname>
					</name>
					<date from="1813" to="1890">1813-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>588 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">ACK_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies Diverses</title>
						<author>Louise-Victorine Ackermann</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">http://www.poesies.net/louiseackermaNpoesiesdiverses.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres de L. Ackermann</title>
						<author>Louise-Victorine Ackermann</author>
						<imprint>
							<publisher>Alphonse Lemerre, éditeur</publisher>
							<date when="1885">1885</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1862">1862</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>Les balises de pagination ont été supprimées.</p>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique
					après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="ACK7">
				<head type="number">VII</head>
				<head type="main">La Belle au Bois dormant</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Une</w> <w n="1.2">princesse</w>, <w n="1.3">au</w> <w n="1.4">fond</w> <w n="1.5">des</w> <w n="1.6">bois</w>,</l>
					<l n="2" num="1.2"><w n="2.1">A</w> <w n="2.2">dormi</w> <w n="2.3">cent</w> <w n="2.4">ans</w> <w n="2.5">autrefois</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Oui</w>, <w n="3.2">cent</w> <w n="3.3">beaux</w> <w n="3.4">ans</w>, <w n="3.5">tout</w> <w n="3.6">d</w>’<w n="3.7">une</w> <w n="3.8">traite</w>.</l>
					<l n="4" num="1.4"><w n="4.1">L</w>’<w n="4.2">enfant</w>, <w n="4.3">dans</w> <w n="4.4">sa</w> <w n="4.5">fraîche</w> <w n="4.6">retraite</w>,</l>
					<l n="5" num="1.5"><w n="5.1">Laissait</w> <w n="5.2">courir</w> <w n="5.3">le</w> <w n="5.4">temps</w> <w n="5.5">léger</w>.</l>
					<l n="6" num="1.6"><w n="6.1">Tout</w> <w n="6.2">sommeillait</w> <w n="6.3">à</w> <w n="6.4">l</w>’<w n="6.5">entour</w> <w n="6.6">d</w>’<w n="6.7">elle</w> :</l>
					<l n="7" num="1.7"><w n="7.1">La</w> <w n="7.2">brise</w> <w n="7.3">n</w>’<w n="7.4">eût</w> <w n="7.5">pas</w> <w n="7.6">de</w> <w n="7.7">son</w> <w n="7.8">aile</w></l>
					<l n="8" num="1.8"><w n="8.1">Fait</w> <w n="8.2">la</w> <w n="8.3">moindre</w> <w n="8.4">feuille</w> <w n="8.5">bouger</w> ;</l>
					<l n="9" num="1.9"><w n="9.1">Le</w> <w n="9.2">flot</w> <w n="9.3">dormait</w> <w n="9.4">sur</w> <w n="9.5">le</w> <w n="9.6">rivage</w> ;</l>
					<l n="10" num="1.10"><w n="10.1">L</w>’<w n="10.2">oiseau</w>, <w n="10.3">perdu</w> <w n="10.4">dans</w> <w n="10.5">le</w> <w n="10.6">feuillage</w>,</l>
					<l n="11" num="1.11"><w n="11.1">Était</w> <w n="11.2">sans</w> <w n="11.3">voix</w> <w n="11.4">et</w> <w n="11.5">sans</w> <w n="11.6">ébats</w> ;</l>
					<l n="12" num="1.12"><w n="12.1">Sur</w> <w n="12.2">sa</w> <w n="12.3">tige</w> <w n="12.4">fragile</w> <w n="12.5">et</w> <w n="12.6">verte</w></l>
					<l n="13" num="1.13"><w n="13.1">La</w> <w n="13.2">rose</w> <w n="13.3">restait</w> <w n="13.4">entr</w>’<w n="13.5">ouverte</w> :</l>
					<l n="14" num="1.14"><w n="14.1">Cent</w> <w n="14.2">printemps</w> <w n="14.3">ne</w> <w n="14.4">l</w>’<w n="14.5">effeuillaient</w> <w n="14.6">pas</w> !</l>
					<l n="15" num="1.15"><w n="15.1">Le</w> <w n="15.2">charme</w> <w n="15.3">eût</w> <w n="15.4">duré</w>, <w n="15.5">je</w> <w n="15.6">m</w>’<w n="15.7">assure</w>,</l>
					<l n="16" num="1.16"><w n="16.1">À</w> <w n="16.2">jamais</w>, <w n="16.3">sans</w> <w n="16.4">le</w> <w n="16.5">fils</w> <w n="16.6">du</w> <w n="16.7">roi</w>.</l>
					<l n="17" num="1.17"><w n="17.1">Il</w> <w n="17.2">pénétra</w> <w n="17.3">dans</w> <w n="17.4">cet</w> <w n="17.5">endroit</w>,</l>
					<l n="18" num="1.18"><w n="18.1">Et</w> <w n="18.2">découvrit</w> <w n="18.3">par</w> <w n="18.4">aventure</w></l>
					<l n="19" num="1.19"><w n="19.1">Le</w> <w n="19.2">trésor</w> <w n="19.3">que</w> <w n="19.4">Dieu</w> <w n="19.5">lui</w> <w n="19.6">gardait</w>.</l>
					<l n="20" num="1.20"><w n="20.1">Un</w> <w n="20.2">baiser</w>, <w n="20.3">bien</w> <w n="20.4">vite</w>, <w n="20.5">il</w> <w n="20.6">dépose</w></l>
					<l n="21" num="1.21"><w n="21.1">Sur</w> <w n="21.2">la</w> <w n="21.3">bouche</w> <w n="21.4">qui</w>, <w n="21.5">demi</w>-<w n="21.6">close</w>,</l>
					<l n="22" num="1.22"><w n="22.1">Depuis</w> <w n="22.2">un</w> <w n="22.3">siècle</w> <w n="22.4">l</w>’<w n="22.5">attendait</w>.</l>
					<l n="23" num="1.23"><w n="23.1">La</w> <w n="23.2">dame</w>, <w n="23.3">confuse</w> <w n="23.4">et</w> <w n="23.5">vermeille</w>,</l>
					<l n="24" num="1.24"><w n="24.1">À</w> <w n="24.2">cet</w> <w n="24.3">inconnu</w> <w n="24.4">qui</w> <w n="24.5">l</w>’<w n="24.6">éveille</w></l>
					<l n="25" num="1.25"><w n="25.1">Sourit</w> <w n="25.2">dans</w> <w n="25.3">son</w> <w n="25.4">étonnement</w>.</l>
					<l n="26" num="1.26"><w n="26.1">Ô</w> <w n="26.2">surprise</w> <w n="26.3">toujours</w> <w n="26.4">la</w> <w n="26.5">même</w> !</l>
					<l n="27" num="1.27"><w n="27.1">Sourire</w> <w n="27.2">ému</w> ! <w n="27.3">Baiser</w> <w n="27.4">charmant</w> !</l>
					<l n="28" num="1.28"><w n="28.1">L</w>’<w n="28.2">amour</w> <w n="28.3">est</w> <w n="28.4">l</w>’<w n="28.5">éveilleur</w> <w n="28.6">suprême</w>,</l>
					<l n="29" num="1.29"><w n="29.1">L</w>’<w n="29.2">âme</w>, <w n="29.3">la</w> <w n="29.4">Belle</w> <w n="29.5">au</w> <w n="29.6">bois</w> <w n="29.7">dormant</w>.</l>
				</lg>
			</div></body></text></TEI>