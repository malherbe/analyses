<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">PREMIÈRES POÉSIES</title>
				<title type="medium">Une édition électronique</title>
				<author key="ACK">
					<name>
						<forename>Louise-Victorine</forename>
						<surname>ACKERMANN</surname>
					</name>
					<date from="1813" to="1890">1813-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>588 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">ACK_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies Diverses</title>
						<author>Louise-Victorine Ackermann</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">http://www.poesies.net/louiseackermaNpoesiesdiverses.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres de L. Ackermann</title>
						<author>Louise-Victorine Ackermann</author>
						<imprint>
							<publisher>Alphonse Lemerre, éditeur</publisher>
							<date when="1885">1885</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1862">1862</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>Les balises de pagination ont été supprimées.</p>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique
					après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="ACK13">
				<head type="number">XIII</head>
				<head type="main">Deux vers D’Alcée</head>
				<opener>
					<epigraph>
						<cit>
							<quote>
								Ίόπλοχ´ άγνά µειλιχόµειδε Σαπφοί, <lb></lb>
								θέλω τι Fείπñν, άλλά µε xωλύει αίδώς.
							</quote>
							<bibl>
								<name>Alcée</name>.
							</bibl>
						</cit>
					</epigraph>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Quel</w> <w n="1.2">était</w> <w n="1.3">ton</w> <w n="1.4">désir</w> <w n="1.5">et</w> <w n="1.6">ta</w> <w n="1.7">crainte</w> <w n="1.8">secrète</w> ?</l>
					<l n="2" num="1.2"><w n="2.1">Quoi</w> ! <w n="2.2">le</w> <w n="2.3">vœu</w> <w n="2.4">de</w> <w n="2.5">ton</w> <w n="2.6">cœur</w>, <w n="2.7">ta</w> <w n="2.8">Muse</w> <w n="2.9">trop</w> <w n="2.10">discrète</w></l>
					<l n="3" num="1.3"><space unit="char" quantity="8"></space><w n="3.1">Rougit</w>-<w n="3.2">elle</w> <w n="3.3">de</w> <w n="3.4">l</w>’<w n="3.5">exprimer</w> ?</l>
					<l n="4" num="1.4"><w n="4.1">Alcée</w>, <w n="4.2">on</w> <w n="4.3">reconnaît</w> <w n="4.4">l</w>’<w n="4.5">amour</w> <w n="4.6">à</w> <w n="4.7">ce</w> <w n="4.8">langage</w>.</l>
					<l n="5" num="1.5"><w n="5.1">Sapho</w> <w n="5.2">feint</w> <w n="5.3">vainement</w> <w n="5.4">que</w> <w n="5.5">ton</w> <w n="5.6">discours</w> <w n="5.7">l</w>’<w n="5.8">outrage</w>,</l>
					<l n="6" num="1.6"><space unit="char" quantity="8"></space><w n="6.1">Sapho</w> <w n="6.2">sait</w> <w n="6.3">que</w> <w n="6.4">tu</w> <w n="6.5">vas</w> <w n="6.6">l</w>’<w n="6.7">aimer</w>.</l>
				</lg>
				<lg n="2">
					<l n="7" num="2.1"><w n="7.1">Tu</w> <w n="7.2">l</w>’<w n="7.3">entendais</w> <w n="7.4">chanter</w>, <w n="7.5">tu</w> <w n="7.6">la</w> <w n="7.7">voyais</w> <w n="7.8">sourire</w>,</l>
					<l n="8" num="2.2"><w n="8.1">La</w> <w n="8.2">fille</w> <w n="8.3">de</w> <w n="8.4">Lesbos</w>, <w n="8.5">Sapho</w> <w n="8.6">qui</w> <w n="8.7">sur</w> <w n="8.8">sa</w> <w n="8.9">lyre</w></l>
					<l n="9" num="2.3"><space unit="char" quantity="8"></space><w n="9.1">Répandit</w> <w n="9.2">sa</w> <w n="9.3">grâce</w> <w n="9.4">et</w> <w n="9.5">ses</w> <w n="9.6">feux</w>.</l>
					<l n="10" num="2.4"><w n="10.1">Sa</w> <w n="10.2">voix</w> <w n="10.3">te</w> <w n="10.4">trouble</w>, <w n="10.5">Alcée</w>, <w n="10.6">et</w> <w n="10.7">son</w> <w n="10.8">regard</w> <w n="10.9">t</w>’<w n="10.10">enflamme</w></l>
					<l n="11" num="2.5"><w n="11.1">Tandis</w> <w n="11.2">que</w> <w n="11.3">ses</w> <w n="11.4">accents</w> <w n="11.5">pénétraient</w> <w n="11.6">dans</w> <w n="11.7">ton</w> <w n="11.8">âme</w>,</l>
					<l n="12" num="2.6"><space unit="char" quantity="8"></space><w n="12.1">Sa</w> <w n="12.2">beauté</w> <w n="12.3">ravissait</w> <w n="12.4">tes</w> <w n="12.5">yeux</w>.</l>
				</lg>
				<lg n="3">
					<l n="13" num="3.1"><w n="13.1">Que</w> <w n="13.2">devint</w> <w n="13.3">ton</w> <w n="13.4">amour</w> ? <w n="13.5">L</w>’<w n="13.6">heure</w> <w n="13.7">qui</w> <w n="13.8">le</w> <w n="13.9">vit</w> <w n="13.10">naître</w></l>
					<l n="14" num="3.2"><w n="14.1">L</w>’<w n="14.2">a</w>-<w n="14.3">t</w>-<w n="14.4">elle</w> <w n="14.5">vu</w> <w n="14.6">mourir</w> ? <w n="14.7">Vénus</w> <w n="14.8">ailleurs</w> <w n="14.9">peut</w>-<w n="14.10">être</w></l>
					<l n="15" num="3.3"><space unit="char" quantity="8"></space><w n="15.1">Emporta</w> <w n="15.2">tes</w> <w n="15.3">vœux</w> <w n="15.4">fugitifs</w>.</l>
					<l n="16" num="3.4"><w n="16.1">Mais</w> <w n="16.2">le</w> <w n="16.3">parfum</w> <w n="16.4">du</w> <w n="16.5">cœur</w> <w n="16.6">jamais</w> <w n="16.7">ne</w> <w n="16.8">s</w>’<w n="16.9">évapore</w> ;</l>
					<l n="17" num="3.5"><w n="17.1">Même</w> <w n="17.2">après</w> <w n="17.3">deux</w> <w n="17.4">mille</w> <w n="17.5">ans</w> <w n="17.6">je</w> <w n="17.7">le</w> <w n="17.8">respire</w> <w n="17.9">encore</w></l>
					<l n="18" num="3.6"><space unit="char" quantity="8"></space><w n="18.1">Dans</w> <w n="18.2">deux</w> <w n="18.3">vers</w> <w n="18.4">émus</w> <w n="18.5">et</w> <w n="18.6">craintifs</w>.</l>
				</lg>
			</div></body></text></TEI>