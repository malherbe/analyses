<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE BOURDEAU DES NEUF PUCELLES</title>
				<title type="medium">Édition électronique</title>
				<author key="FRT">
					<name>
						<forename>Charles-Théophile</forename>
						<surname>FERET</surname>
					</name>
					<date from="1858" to="1928">1858-1928</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1111 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">FRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le Bourdeau des neuf pucelles</title>
						<author>Charles-Théophile Feret</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">https://www.gutenberg.org/ebooks/66781</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le Bourdeau des neuf pucelles</title>
								<author>Charles-Théophile Feret</author>
								<imprint>
									<pubPlace>CAUDÉRAN-BORDEAUX</pubPlace>
									<publisher>ÉDITIONS DES CAHIERS LITTÉRAIRES</publisher>
									<date when="1923">1923</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1912">1912</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et les notes de l’éditeur n’ont pas été reprises.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>
					Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique
					après application du programme de calcul de la longueur métrique des vers).
				</p>
				<p>Les notes de bas de page (numérotation conforme à l’édition imprimée) ont été reportées en fin de poème.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension (…).</p>
					<p>Les tirets simples ont été remplacés par des tirets demi-cadratin.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-12-09" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.4.xsd)</change>
				<change when="2021-12-10" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">TERPSICHORE</head><head type="sub_part">Muse de la Danse</head><div type="poem" key="FRT14">
					<head type="main">A Vincent Muselli</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Ton</w> <w n="1.2">nom</w>, <w n="1.3">s</w>’<w n="1.4">il</w> <w n="1.5">ne</w> <w n="1.6">m</w>’<w n="1.7">abuse</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Ami</w>, <w n="2.2">t</w>’<w n="2.3">a</w> <w n="2.4">dédié</w></l>
						<l n="3" num="1.3"><space unit="char" quantity="8"></space><w n="3.1">Aux</w> <w n="3.2">Muses</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Pour</w> <w n="4.2">leurs</w> <w n="4.3">beaux</w> <w n="4.4">bras</w> <w n="4.5">lier</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">De</w> <w n="5.2">flûte</w> <w n="5.3">traversière</w></l>
						<l n="6" num="2.2"><w n="6.1">Fais</w>, <w n="6.2">le</w> <w n="6.3">front</w> <w n="6.4">ciselé</w></l>
						<l n="7" num="2.3"><space unit="char" quantity="8"></space><w n="7.1">De</w> <w n="7.2">lierre</w>,</l>
						<l n="8" num="2.4"><w n="8.1">Les</w> <w n="8.2">Camènes</w> <w n="8.3">baller</w>,</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">A</w> <w n="9.2">bonds</w> <w n="9.3">et</w> <w n="9.4">à</w> <w n="9.5">volées</w>,</l>
						<l n="10" num="3.2"><w n="10.1">Et</w> <w n="10.2">tant</w> <w n="10.3">que</w> <w n="10.4">par</w> <w n="10.5">le</w> <w n="10.6">chaud</w></l>
						<l n="11" num="3.3"><space unit="char" quantity="8"></space><w n="11.1">Foulées</w>,</l>
						<l n="12" num="3.4"><w n="12.1">Le</w> <w n="12.2">souffle</w> <w n="12.3">ne</w> <w n="12.4">leur</w> <w n="12.5">fault</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Dès</w> <w n="13.2">qu</w>’<w n="13.3">aux</w> <w n="13.4">gorges</w> <w n="13.5">Neuvaines</w></l>
						<l n="14" num="4.2"><w n="14.1">Des</w> <w n="14.2">perles</w> <w n="14.3">sur</w> <w n="14.4">le</w> <w n="14.5">bleu</w></l>
						<l n="15" num="4.3"><space unit="char" quantity="8"></space><w n="15.1">Des</w> <w n="15.2">veines</w></l>
						<l n="16" num="4.4"><w n="16.1">Ruissellent</w>, <w n="16.2">romps</w> <w n="16.3">le</w> <w n="16.4">jeu</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Et</w> <w n="17.2">quand</w> <w n="17.3">tu</w> <w n="17.4">les</w> <w n="17.5">dénoues</w>,</l>
						<l n="18" num="5.2"><w n="18.1">Vois</w> <w n="18.2">les</w> <w n="18.3">Nymphes</w> <w n="18.4">baigner</w></l>
						<l n="19" num="5.3"><space unit="char" quantity="8"></space><w n="19.1">Leurs</w> <w n="19.2">joues</w>,</l>
						<l n="20" num="5.4"><w n="20.1">Les</w> <w n="20.2">Grâces</w> <w n="20.3">les</w> <w n="20.4">peigner</w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Prends</w> <w n="21.2">la</w> <w n="21.3">houppe</w>, <w n="21.4">le</w> <w n="21.5">peigne</w>,</l>
						<l n="22" num="6.2"><w n="22.1">Les</w> <w n="22.2">fards</w> ; <w n="22.3">qu</w>’<w n="22.4">heureux</w> <w n="22.5">témoin</w></l>
						<l n="23" num="6.3"><space unit="char" quantity="8"></space><w n="23.1">Ne</w> <w n="23.2">craigne</w></l>
						<l n="24" num="6.4"><w n="24.1">De</w> <w n="24.2">leur</w> <w n="24.3">donner</w> <w n="24.4">des</w> <w n="24.5">soins</w>.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">D</w>’<w n="25.2">une</w> <w n="25.3">main</w> <w n="25.4">délicate</w></l>
						<l n="26" num="7.2"><w n="26.1">Tire</w>, <w n="26.2">au</w> <w n="26.3">jais</w> <w n="26.4">du</w> <w n="26.5">chignon</w>,</l>
						<l n="27" num="7.3"><space unit="char" quantity="8"></space><w n="27.1">L</w>’<w n="27.2">agate</w>,</l>
						<l n="28" num="7.4"><w n="28.1">Et</w> <w n="28.2">te</w> <w n="28.3">fais</w> <w n="28.4">leur</w> <w n="28.5">mignon</w>.</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1"><w n="29.1">Si</w> <w n="29.2">le</w> <w n="29.3">col</w> <w n="29.4">sur</w> <w n="29.5">la</w> <w n="29.6">nuque</w></l>
						<l n="30" num="8.2"><w n="30.1">Baille</w>, <w n="30.2">regarde</w> <w n="30.3">aval</w> ;</l>
						<l n="31" num="8.3"><space unit="char" quantity="8"></space><w n="31.1">Reluque</w></l>
						<l n="32" num="8.4"><w n="32.1">Le</w> <w n="32.2">dos</w> <w n="32.3">moite</w> <w n="32.4">du</w> <w n="32.5">bal</w>.</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1"><w n="33.1">Comme</w> <w n="33.2">chez</w> <w n="33.3">les</w> <w n="33.4">modistes</w></l>
						<l n="34" num="9.2"><w n="34.1">Qui</w> <w n="34.2">n</w>’<w n="34.3">ont</w> <w n="34.4">rien</w> <w n="34.5">à</w> <w n="34.6">cacher</w>,</l>
						<l n="35" num="9.3"><space unit="char" quantity="8"></space><w n="35.1">Assiste</w></l>
						<l n="36" num="9.4"><w n="36.1">A</w> <w n="36.2">leur</w> <w n="36.3">petit</w> <w n="36.4">coucher</w>.</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1"><w n="37.1">A</w> <w n="37.2">qui</w> <w n="37.3">fait</w> <w n="37.4">la</w> <w n="37.5">mauvaise</w>,</l>
						<l n="38" num="10.2"><w n="38.1">Et</w> <w n="38.2">la</w> <w n="38.3">main</w> <w n="38.4">sur</w> <w n="38.5">tes</w> <w n="38.6">yeux</w></l>
						<l n="39" num="10.3"><space unit="char" quantity="8"></space><w n="39.1">Te</w> <w n="39.2">lèse</w></l>
						<l n="40" num="10.4"><w n="40.1">D</w>’<w n="40.2">un</w> <w n="40.3">buisson</w> <w n="40.4">radieux</w>,</l>
					</lg>
					<lg n="11">
						<l n="41" num="11.1"><w n="41.1">Baise</w> <w n="41.2">la</w> <w n="41.3">paume</w>, <w n="41.4">opprime</w></l>
						<l n="42" num="11.2"><w n="42.1">Les</w> <w n="42.2">globes</w> <w n="42.3">à</w> <w n="42.4">tâtons</w> ;</l>
						<l n="43" num="11.3"><space unit="char" quantity="8"></space><w n="43.1">Qu</w>’<w n="43.2">aux</w> <w n="43.3">cîmes</w></l>
						<l n="44" num="11.4"><w n="44.1">Grossissent</w> <w n="44.2">les</w> <w n="44.3">boutons</w>.</l>
					</lg>
					<lg n="12">
						<l n="45" num="12.1"><w n="45.1">De</w> <w n="45.2">ta</w> <w n="45.3">langue</w> <w n="45.4">la</w> <w n="45.5">perce</w>,</l>
						<l n="46" num="12.2"><w n="46.1">Et</w> <w n="46.2">lui</w> <w n="46.3">dis</w> : « <w n="46.4">Puella</w> ! »</l>
						<l n="47" num="12.3"><space unit="char" quantity="8"></space>— <w n="47.1">Properce</w></l>
						<l n="48" num="12.4"><w n="48.1">Eût</w> <w n="48.2">aimé</w> <w n="48.3">ce</w> <w n="48.4">nom</w>-<w n="48.5">là</w> —</l>
					</lg>
					<lg n="13">
						<l n="49" num="13.1">« <w n="49.1">Moi</w> <w n="49.2">qui</w> <w n="49.3">les</w> <w n="49.4">Muses</w> <w n="49.5">lie</w>,</l>
						<l n="50" num="13.2"><w n="50.1">Les</w> <w n="50.2">délierai</w>. — <w n="50.3">Ces</w> <w n="50.4">lins</w>,</l>
						<l n="51" num="13.3"><space unit="char" quantity="8"></space><w n="51.1">Thalie</w>,</l>
						<l n="52" num="13.4"><w n="52.1">Font</w> <w n="52.2">mes</w> <w n="52.3">yeux</w> <w n="52.4">orphelins</w>.</l>
					</lg>
					<lg n="14">
						<l n="53" num="14.1">« <w n="53.1">Erato</w>, <w n="53.2">qui</w> <w n="53.3">les</w> <w n="53.4">noces</w></l>
						<l n="54" num="14.2"><w n="54.1">Présides</w>, <w n="54.2">par</w> <w n="54.3">Eros</w> !</l>
						<l n="55" num="14.3"><space unit="char" quantity="8"></space><w n="55.1">Ces</w> <w n="55.2">bosses</w></l>
						<l n="56" num="14.4"><w n="56.1">Ne</w> <w n="56.2">me</w> <w n="56.3">font</w> <w n="56.4">de</w> <w n="56.5">Paros</w>.</l>
					</lg>
					<lg n="15">
						<l n="57" num="15.1">« <w n="57.1">Dans</w> <w n="57.2">mes</w> <w n="57.3">bras</w>, <w n="57.4">Calliope</w>,</l>
						<l n="58" num="15.2"><w n="58.1">En</w> <w n="58.2">belle</w> <w n="58.3">chair</w>, <w n="58.4">et</w> <w n="58.5">non</w></l>
						<l n="59" num="15.3"><space unit="char" quantity="8"></space><w n="59.1">Par</w> <w n="59.2">trope</w>,</l>
						<l n="60" num="15.4"><w n="60.1">Fais</w> <w n="60.2">quinaud</w> <w n="60.3">Apollon</w>.</l>
					</lg>
					<lg n="16">
						<l n="61" num="16.1">« <w n="61.1">On</w> <w n="61.2">sait</w> <w n="61.3">pourquoi</w> <w n="61.4">la</w> <w n="61.5">serpe</w></l>
						<l n="62" num="16.2"><w n="62.1">De</w> <w n="62.2">Bacchus</w>, <w n="62.3">bon</w> <w n="62.4">voisin</w>,</l>
						<l n="63" num="16.3"><space unit="char" quantity="8"></space><w n="63.1">Euterpe</w>,</l>
						<l n="64" num="16.4"><w n="64.1">Te</w> <w n="64.2">coupe</w> <w n="64.3">du</w> <w n="64.4">raisin</w> ;</l>
					</lg>
					<lg n="17">
						<l n="65" num="17.1">« <w n="65.1">Et</w> <w n="65.2">qu</w>’<w n="65.3">à</w> <w n="65.4">Mars</w> <w n="65.5">sous</w> <w n="65.6">la</w> <w n="65.7">tente</w>,</l>
						<l n="66" num="17.2"><w n="66.1">Vivandière</w> <w n="66.2">Clio</w>,</l>
						<l n="67" num="17.3"><space unit="char" quantity="8"></space><w n="67.1">Contente</w>,</l>
						<l n="68" num="17.4"><w n="68.1">Tu</w> <w n="68.2">trousses</w> <w n="68.3">ton</w> <w n="68.4">bliaud</w>.</l>
					</lg>
					<lg n="18">
						<l n="69" num="18.1">« <w n="69.1">Mais</w> <w n="69.2">si</w> <w n="69.3">les</w> <w n="69.4">cœurs</w> <w n="69.5">bondissent</w>,</l>
						<l n="70" num="18.2"><w n="70.1">Quand</w> <w n="70.2">du</w> <w n="70.3">pouce</w> <w n="70.4">et</w> <w n="70.5">du</w> <w n="70.6">doigt</w></l>
						<l n="71" num="18.3"><space unit="char" quantity="8"></space><w n="71.1">Indice</w></l>
						<l n="72" num="18.4"><w n="72.1">Je</w> <w n="72.2">touche</w> <w n="72.3">au</w> <w n="72.4">luth</w> <w n="72.5">françois</w>,</l>
					</lg>
					<lg n="19">
						<l n="73" num="19.1">« <w n="73.1">N</w>’<w n="73.2">osez</w> <w n="73.3">à</w> <w n="73.4">nos</w> <w n="73.5">mains</w> <w n="73.6">pures</w></l>
						<l n="74" num="19.2"><w n="74.1">Fermer</w> <w n="74.2">vos</w> <w n="74.3">peplos</w> <w n="74.4">d</w>’<w n="74.5">or</w>,</l>
						<l n="75" num="19.3"><space unit="char" quantity="8"></space><w n="75.1">Ceintures</w> !</l>
						<l n="76" num="19.4"><w n="76.1">Et</w> <w n="76.2">nous</w> <w n="76.3">dirons</w> <w n="76.4">encor</w></l>
					</lg>
					<lg n="20">
						<l n="77" num="20.1">« <w n="77.1">Que</w> <w n="77.2">d</w>’<w n="77.3">argent</w>, <w n="77.4">de</w> <w n="77.5">prière</w>,</l>
						<l n="78" num="20.2"><w n="78.1">Nul</w> <w n="78.2">n</w>’<w n="78.3">a</w> <w n="78.4">soumis</w> <w n="78.5">vos</w> <w n="78.6">cœurs</w></l>
						<l n="79" num="20.3"><space unit="char" quantity="8"></space><w n="79.1">De</w> <w n="79.2">pierre</w>,</l>
						<l n="80" num="20.4"><w n="80.1">Pucelles</w>, <w n="80.2">Chastes</w> <w n="80.3">Sœurs</w>,</l>
					</lg>
					<lg n="21">
						<l n="81" num="21.1">« <w n="81.1">Quoiqu</w>’<w n="81.2">à</w> <w n="81.3">votre</w> <w n="81.4">huis</w>, <w n="81.5">où</w> <w n="81.6">saigne</w></l>
						<l n="82" num="21.2"><w n="82.1">Un</w> <w n="82.2">gros</w> <w n="82.3">numéro</w> <subst type="phonemization" hand="RR" reason="analysis"><del>9</del><add rend="hidden"><w n="82.4">neuf</w></add></subst></l>
						<l n="83" num="21.3"><space unit="char" quantity="8"></space><w n="83.1">Des</w> <w n="83.2">duègnes</w></l>
						<l n="84" num="21.4"><w n="84.1">Aussi</w> <w n="84.2">vastes</w> <w n="84.3">qu</w>’<w n="84.4">un</w> <w n="84.5">bœuf</w></l>
					</lg>
					<lg n="22">
						<l n="85" num="22.1">« <w n="85.1">Racolent</w> <w n="85.2">pour</w> <w n="85.3">les</w> <w n="85.4">Muses</w>,</l>
						<l n="86" num="22.2"><w n="86.1">Que</w> <w n="86.2">Claude</w> <w n="86.3">Le</w> <w n="86.4">Petit</w></l>
						<l n="87" num="22.3"><space unit="char" quantity="8"></space><w n="87.1">Accuse</w></l>
						<l n="88" num="22.4"><w n="88.1">Du</w> <w n="88.2">chancre</w> <w n="88.3">qui</w> <w n="88.4">le</w> <w n="88.5">cuit</w>.</l>
					</lg>
					<lg n="23">
						<l n="89" num="23.1">« <w n="89.1">Mais</w> <w n="89.2">tant</w> <w n="89.3">pis</w> <w n="89.4">pour</w> <w n="89.5">qui</w> <w n="89.6">cherche</w></l>
						<l n="90" num="23.2"><w n="90.1">Pégase</w>, <w n="90.2">et</w> <w n="90.3">en</w> <w n="90.4">vilain</w></l>
						<l n="91" num="23.3"><space unit="char" quantity="8"></space><w n="91.1">Du</w> <w n="91.2">Perche</w>,</l>
						<l n="92" num="23.4"><w n="92.1">Ne</w> <w n="92.2">trouve</w> <w n="92.3">qu</w>’<w n="92.4">un</w> <w n="92.5">poulain</w>. »</l>
					</lg>
				</div></body></text></TEI>