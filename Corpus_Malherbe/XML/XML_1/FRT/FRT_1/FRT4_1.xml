<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE BOURDEAU DES NEUF PUCELLES</title>
				<title type="medium">Édition électronique</title>
				<author key="FRT">
					<name>
						<forename>Charles-Théophile</forename>
						<surname>FERET</surname>
					</name>
					<date from="1858" to="1928">1858-1928</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1111 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">FRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le Bourdeau des neuf pucelles</title>
						<author>Charles-Théophile Feret</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">https://www.gutenberg.org/ebooks/66781</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le Bourdeau des neuf pucelles</title>
								<author>Charles-Théophile Feret</author>
								<imprint>
									<pubPlace>CAUDÉRAN-BORDEAUX</pubPlace>
									<publisher>ÉDITIONS DES CAHIERS LITTÉRAIRES</publisher>
									<date when="1923">1923</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1912">1912</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et les notes de l’éditeur n’ont pas été reprises.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>
					Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique
					après application du programme de calcul de la longueur métrique des vers).
				</p>
				<p>Les notes de bas de page (numérotation conforme à l’édition imprimée) ont été reportées en fin de poème.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension (…).</p>
					<p>Les tirets simples ont été remplacés par des tirets demi-cadratin.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-12-09" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.4.xsd)</change>
				<change when="2021-12-10" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ERATO</head><div type="poem" key="FRT4">
					<head type="main">La Belle Vieille</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">C</w>’<w n="1.2">est</w> <w n="1.3">d</w>’<w n="1.4">avoir</w> <w n="1.5">tant</w> <w n="1.6">aimé</w> <w n="1.7">l</w>’<w n="1.8">enfance</w> <w n="1.9">de</w> <w n="1.10">ses</w> <w n="1.11">seins</w></l>
						<l n="2" num="1.2"><space unit="char" quantity="8"></space><w n="2.1">Qu</w>’<w n="2.2">en</w> <w n="2.3">son</w> <w n="2.4">déclin</w> <w n="2.5">je</w> <w n="2.6">l</w>’<w n="2.7">aime</w> <w n="2.8">encore</w> ;</l>
						<l n="3" num="1.3"><w n="3.1">Et</w> <w n="3.2">d</w>’<w n="3.3">avoir</w> <w n="3.4">vu</w>, <w n="3.5">des</w> <w n="3.6">bas</w> <w n="3.7">de</w> <w n="3.8">la</w> <w n="3.9">fillette</w>, <w n="3.10">éclore</w></l>
						<l n="4" num="1.4"><space unit="char" quantity="8"></space><w n="4.1">Deux</w> <w n="4.2">globes</w> <w n="4.3">d</w>’<w n="4.4">un</w> <w n="4.5">noble</w> <w n="4.6">dessin</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">J</w>’<w n="5.2">avais</w> <w n="5.3">cet</w> <w n="5.4">âge</w>, <w n="5.5">où</w> <w n="5.6">l</w>’<w n="5.7">on</w> <w n="5.8">n</w>’<w n="5.9">est</w> <w n="5.10">plus</w> <w n="5.11">le</w> <w n="5.12">jeune</w> <w n="5.13">coq</w></l>
						<l n="6" num="2.2"><space unit="char" quantity="8"></space><w n="6.1">Qui</w> <w n="6.2">plonge</w> <w n="6.3">et</w> <w n="6.4">retire</w> <w n="6.5">sa</w> <w n="6.6">lame</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Où</w> <w n="7.2">les</w> <w n="7.3">arômes</w> <w n="7.4">bus</w> <w n="7.5">ramènent</w> <w n="7.6">à</w> <w n="7.7">la</w> <w n="7.8">femme</w>,</l>
						<l n="8" num="2.4"><space unit="char" quantity="8"></space><w n="8.1">Où</w> <w n="8.2">l</w>’<w n="8.3">amour</w> <w n="8.4">prolonge</w> <w n="8.5">le</w> <w n="8.6">choc</w> ;</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Où</w>, <w n="9.2">las</w> <w n="9.3">des</w> <w n="9.4">fards</w>, <w n="9.5">de</w> <w n="9.6">lèvre</w> <w n="9.7">peinte</w>, <w n="9.8">et</w> <w n="9.9">de</w> <w n="9.10">faux</w> <w n="9.11">blond</w>,</l>
						<l n="10" num="3.2"><space unit="char" quantity="8"></space><w n="10.1">Las</w> <w n="10.2">des</w> <w n="10.3">rapides</w> <w n="10.4">ariettes</w>,</l>
						<l n="11" num="3.3"><w n="11.1">L</w>’<w n="11.2">on</w> <w n="11.3">rêve</w> <w n="11.4">du</w> <w n="11.5">menton</w> <w n="11.6">pudique</w> <w n="11.7">où</w> <w n="11.8">Juliette</w></l>
						<l n="12" num="3.4"><space unit="char" quantity="8"></space><w n="12.1">Presse</w> <w n="12.2">son</w> <w n="12.3">tendre</w> <w n="12.4">violon</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Et</w> <w n="13.2">sous</w> <w n="13.3">mes</w> <w n="13.4">yeux</w> <w n="13.5">l</w>’<w n="13.6">adolescence</w> <w n="13.7">pétrissait</w></l>
						<l n="14" num="4.2"><space unit="char" quantity="8"></space><w n="14.1">Ce</w> <w n="14.2">très</w> <w n="14.3">féminin</w> <w n="14.4">bosselage</w>,</l>
						<l n="15" num="4.3"><w n="15.1">Fanfreluchait</w> <w n="15.2">de</w> <w n="15.3">mousse</w> <w n="15.4">un</w> <w n="15.5">joli</w> <w n="15.6">coquillage</w>,</l>
						<l n="16" num="4.4"><space unit="char" quantity="8"></space><w n="16.1">De</w> <w n="16.2">myrrhe</w> <w n="16.3">exaltait</w> <w n="16.4">le</w> <w n="16.5">gousset</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Je</w> <w n="17.2">reniflais</w> <w n="17.3">aux</w> <w n="17.4">courtes</w> <w n="17.5">manches</w> <w n="17.6">de</w> <w n="17.7">l</w>’<w n="17.8">été</w></l>
						<l n="18" num="5.2"><space unit="char" quantity="8"></space><w n="18.1">Le</w> <w n="18.2">fil</w> <w n="18.3">emmêlé</w> <w n="18.4">des</w> <w n="18.5">aisselles</w> ;</l>
						<l n="19" num="5.3"><w n="19.1">Et</w> <w n="19.2">j</w>’<w n="19.3">épiais</w> <w n="19.4">la</w> <w n="19.5">jupe</w> <w n="19.6">aux</w> <w n="19.7">hautes</w> <w n="19.8">balancelles</w></l>
						<l n="20" num="5.4"><space unit="char" quantity="8"></space><w n="20.1">D</w>’<w n="20.2">où</w> <w n="20.3">béait</w> <w n="20.4">sa</w> <w n="20.5">féminité</w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Mon</w> <w n="21.2">rêve</w> <w n="21.3">demandait</w> <w n="21.4">aux</w> <w n="21.5">nattes</w> <w n="21.6">d</w>’<w n="21.7">un</w> <w n="21.8">noir</w> <w n="21.9">bleu</w></l>
						<l n="22" num="6.2"><space unit="char" quantity="8"></space><w n="22.1">Quelque</w> <w n="22.2">image</w> <w n="22.3">du</w> <w n="22.4">tabernacle</w>,</l>
						<l n="23" num="6.3"><w n="23.1">Où</w> <w n="23.2">frise</w> <w n="23.3">un</w> <w n="23.4">crin</w> <w n="23.5">d</w>’<w n="23.6">agneau</w>, <w n="23.7">dont</w> <w n="23.8">l</w>’<w n="23.9">attouchement</w> <w n="23.10">racle</w></l>
						<l n="24" num="6.4"><space unit="char" quantity="8"></space><w n="24.1">L</w>’<w n="24.2">éréthisme</w> <w n="24.3">des</w> <w n="24.4">chairs</w> <w n="24.5">en</w> <w n="24.6">feu</w>.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">Par</w> <w n="25.2">baisers</w> <w n="25.3">décochés</w> <w n="25.4">sur</w> <w n="25.5">ses</w> <w n="25.6">dents</w> <w n="25.7">closes</w>, <w n="25.8">j</w>’<w n="25.9">eus</w></l>
						<l n="26" num="7.2"><space unit="char" quantity="8"></space><w n="26.1">Les</w> <w n="26.2">siens</w> <w n="26.3">qui</w> <w n="26.4">ne</w> <w n="26.5">savaient</w> <w n="26.6">répondre</w>.</l>
						<l n="27" num="7.3"><w n="27.1">Mais</w> <w n="27.2">l</w>’<w n="27.3">imparfait</w> <w n="27.4">contact</w> <w n="27.5">dont</w> <w n="27.6">je</w> <w n="27.7">me</w> <w n="27.8">sentais</w> <w n="27.9">fondre</w></l>
						<l n="28" num="7.4"><space unit="char" quantity="8"></space><w n="28.1">Prélibait</w> <w n="28.2">son</w> <w n="28.3">baume</w> <w n="28.4">et</w> <w n="28.5">son</w> <w n="28.6">jus</w>.</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1"><w n="29.1">Ma</w> <w n="29.2">jeunesse</w> <w n="29.3">barbare</w> <w n="29.4">oubliait</w> <w n="29.5">son</w> <w n="29.6">destin</w></l>
						<l n="30" num="8.2"><space unit="char" quantity="8"></space><w n="30.1">De</w> <w n="30.2">servir</w> <w n="30.3">Mercure</w> <w n="30.4">ou</w> <w n="30.5">Minerve</w>,</l>
						<l n="31" num="8.3"><w n="31.1">Tantale</w> <w n="31.2">du</w> <w n="31.3">poison</w> <w n="31.4">âcre</w> <w n="31.5">et</w> <w n="31.6">doux</w>, <w n="31.7">dont</w> <w n="31.8">s</w>’<w n="31.9">énerve</w></l>
						<l n="32" num="8.4"><space unit="char" quantity="8"></space><w n="32.1">La</w> <w n="32.2">soif</w>, <w n="32.3">au</w> <w n="32.4">flot</w> <w n="32.5">proche</w> <w n="32.6">et</w> <w n="32.7">lointain</w>.</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1"><w n="33.1">O</w> <w n="33.2">bucher</w> <w n="33.3">de</w> <w n="33.4">la</w> <w n="33.5">Longue</w> <w n="33.6">Attente</w> ! <w n="33.7">O</w> <w n="33.8">noir</w> <w n="33.9">ruisseau</w></l>
						<l n="34" num="9.2"><space unit="char" quantity="8"></space><w n="34.1">Des</w> <w n="34.2">désirs</w> <w n="34.3">qui</w> <w n="34.4">coulent</w> <w n="34.5">en</w> <w n="34.6">lave</w> !</l>
						<l n="35" num="9.3"><w n="35.1">Bonds</w> <w n="35.2">cruels</w> <w n="35.3">du</w> <w n="35.4">marteau</w> <w n="35.5">sur</w> <w n="35.6">le</w> <w n="35.7">cœur</w> <w n="35.8">de</w> <w n="35.9">l</w>’<w n="35.10">esclave</w> !</l>
						<l n="36" num="9.4"><space unit="char" quantity="8"></space><w n="36.1">Et</w> <w n="36.2">grésillement</w> <w n="36.3">sous</w> <w n="36.4">le</w> <w n="36.5">sceau</w> !</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1"><w n="37.1">Ce</w> <w n="37.2">long</w> <w n="37.3">souci</w> <w n="37.4">qui</w> <w n="37.5">des</w> <w n="37.6">chairs</w> <w n="37.7">d</w>’<w n="37.8">ambre</w> <w n="37.9">m</w>’<w n="37.10">a</w> <w n="37.11">fait</w> <w n="37.12">serf</w></l>
						<l n="38" num="10.2"><space unit="char" quantity="8"></space><w n="38.1">Aux</w> <w n="38.2">brunes</w> <w n="38.3">chaudes</w> <w n="38.4">me</w> <w n="38.5">consacre</w>,</l>
						<l n="39" num="10.3"><w n="39.1">Aux</w> <w n="39.2">yeux</w> <w n="39.3">d</w>’<w n="39.4">or</w> <w n="39.5">que</w> <w n="39.6">traverse</w> <w n="39.7">un</w> <w n="39.8">reflet</w> <w n="39.9">de</w> <w n="39.10">massacre</w>,</l>
						<l n="40" num="10.4"><space unit="char" quantity="8"></space><w n="40.1">Quand</w> <w n="40.2">le</w> <w n="40.3">spasme</w> <w n="40.4">tire</w> <w n="40.5">le</w> <w n="40.6">nerf</w>.</l>
					</lg>
					<lg n="11">
						<l n="41" num="11.1"><w n="41.1">Enfin</w> <w n="41.2">elle</w> <w n="41.3">mûrit</w> : <w n="41.4">je</w> <w n="41.5">conquis</w> <w n="41.6">des</w> <w n="41.7">chemins</w>,</l>
						<l n="42" num="11.2"><space unit="char" quantity="8"></space><w n="42.1">Dont</w> <w n="42.2">mes</w> <w n="42.3">doigts</w> <w n="42.4">étaient</w> <w n="42.5">les</w> <w n="42.6">couleuvres</w>.</l>
						<l n="43" num="11.3"><w n="43.1">Mais</w> <w n="43.2">la</w> <w n="43.3">chambre</w> <w n="43.4">secrète</w> <w n="43.5">étant</w> <w n="43.6">close</w> <w n="43.7">au</w> <w n="43.8">grand</w> <w n="43.9">œuvre</w>,</l>
						<l n="44" num="11.4"><space unit="char" quantity="8"></space><w n="44.1">La</w> <w n="44.2">clef</w> <w n="44.3">en</w> <w n="44.4">brûlait</w> <w n="44.5">dans</w> <w n="44.6">mes</w> <w n="44.7">mains</w>.</l>
					</lg>
					<lg n="12">
						<l n="45" num="12.1">— <w n="45.1">Non</w> ! <w n="45.2">dit</w> <w n="45.3">la</w> <w n="45.4">bouche</w>, <w n="45.5">mais</w> <w n="45.6">dans</w> <w n="45.7">les</w> <w n="45.8">yeux</w> <w n="45.9">confesseurs</w></l>
						<l n="46" num="12.2"><space unit="char" quantity="8"></space><w n="46.1">La</w> <w n="46.2">chair</w> <w n="46.3">défaille</w> <w n="46.4">et</w> <w n="46.5">s</w>’<w n="46.6">humilie</w>,</l>
						<l n="47" num="12.3"><w n="47.1">Le</w> <w n="47.2">jeune</w> <w n="47.3">sein</w> <w n="47.4">captif</w> <w n="47.5">se</w> <w n="47.6">débat</w> <w n="47.7">en</w> <w n="47.8">folie</w>,</l>
						<l n="48" num="12.4"><space unit="char" quantity="8"></space><w n="48.1">Chevreuil</w> <w n="48.2">lié</w> <w n="48.3">par</w> <w n="48.4">les</w> <w n="48.5">chasseurs</w>.</l>
					</lg>
					<lg n="13">
						<l n="49" num="13.1"><w n="49.1">C</w>’<w n="49.2">est</w> <w n="49.3">dans</w> <w n="49.4">une</w> <w n="49.5">île</w> <w n="49.6">de</w> <w n="49.7">roseaux</w>, <w n="49.8">de</w> <w n="49.9">prés</w> <w n="49.10">herbus</w>,</l>
						<l n="50" num="13.2"><space unit="char" quantity="8"></space><w n="50.1">Sous</w> <w n="50.2">un</w> <w n="50.3">vieux</w> <w n="50.4">saule</w> <w n="50.5">solitaire</w>,</l>
						<l n="51" num="13.3"><w n="51.1">Qu</w>’<w n="51.2">un</w> <w n="51.3">jour</w> <w n="51.4">elle</w> <w n="51.5">m</w>’<w n="51.6">ouvrit</w> <w n="51.7">le</w> <w n="51.8">délicat</w> <w n="51.9">mystère</w>,</l>
						<l n="52" num="13.4"><space unit="char" quantity="8"></space><w n="52.1">Versa</w> <w n="52.2">la</w> <w n="52.3">tête</w>, <w n="52.4">et</w> <w n="52.5">je</w> <w n="52.6">la</w> <w n="52.7">bus</w>.</l>
					</lg>
					<lg n="14">
						<l n="53" num="14.1"><w n="53.1">Cette</w> <w n="53.2">heure</w>-<w n="53.3">là</w>, <w n="53.4">depuis</w>, <w n="53.5">ne</w> <w n="53.6">meurt</w> <w n="53.7">plus</w>. <w n="53.8">Ce</w> <w n="53.9">raisin</w>,</l>
						<l n="54" num="14.2"><space unit="char" quantity="8"></space><w n="54.1">J</w>’<w n="54.2">en</w> <w n="54.3">suce</w> <w n="54.4">encor</w> <w n="54.5">la</w> <w n="54.6">grappe</w> <w n="54.7">bleue</w> ;</l>
						<l n="55" num="14.3"><w n="55.1">Ces</w> <w n="55.2">œillets</w> <w n="55.3">vers</w> <w n="55.4">mes</w> <w n="55.5">dents</w> <w n="55.6">se</w> <w n="55.7">haussent</w> <w n="55.8">sur</w> <w n="55.9">leur</w> <w n="55.10">queue</w> ;</l>
						<l n="56" num="14.4"><space unit="char" quantity="8"></space><w n="56.1">Priape</w> <w n="56.2">les</w> <w n="56.3">cueille</w>, <w n="56.4">et</w> <w n="56.5">me</w> <w n="56.6">ceint</w>,</l>
					</lg>
					<lg n="15">
						<l n="57" num="15.1"><w n="57.1">Quand</w> <w n="57.2">au</w> <w n="57.3">giron</w>, <w n="57.4">immaculé</w> <w n="57.5">comme</w> <w n="57.6">jadis</w>,</l>
						<l n="58" num="15.2"><space unit="char" quantity="8"></space><w n="58.1">Dont</w> <w n="58.2">Sarah</w> <w n="58.3">fait</w> <w n="58.4">Agar</w> <w n="58.5">jalouse</w>,</l>
						<l n="59" num="15.3"><w n="59.1">En</w> <w n="59.2">son</w> <w n="59.3">dixième</w> <w n="59.4">lustre</w>, <w n="59.5">à</w> <w n="59.6">longs</w> <w n="59.7">traits</w>, <w n="59.8">je</w> <w n="59.9">l</w>’<w n="59.10">épouse</w></l>
						<l n="60" num="15.4"><space unit="char" quantity="8"></space><w n="60.1">Parmi</w> <w n="60.2">ses</w> <w n="60.3">genoux</w> <w n="60.4">arrondis</w>.</l>
					</lg>
					<lg n="16">
						<l n="61" num="16.1"><w n="61.1">Vos</w> <w n="61.2">belles</w> <w n="61.3">comparez</w> ! <hi rend="ital"><w n="61.4">Conferte</w> <w n="61.5">puellas</w> !</hi><ref target="*" type="noteAnchor">(1)</ref></l>
						<l n="62" num="16.2"><space unit="char" quantity="8"></space><w n="62.1">Tel</w> <w n="62.2">Paris</w> <w n="62.3">morgua</w> <w n="62.4">deux</w> <w n="62.5">déesses</w>,</l>
						<l n="63" num="16.3"><w n="63.1">Quand</w> <w n="63.2">Vénus</w> <w n="63.3">éteignit</w> <w n="63.4">d</w>’<w n="63.5">un</w> <w n="63.6">remûment</w> <w n="63.7">de</w> <w n="63.8">fesses</w></l>
						<l n="64" num="16.4"><space unit="char" quantity="8"></space><w n="64.1">Madame</w> <w n="64.2">Jupin</w> <w n="64.3">et</w> <w n="64.4">Pallas</w> ;</l>
					</lg>
					<lg n="17">
						<l n="65" num="17.1"><w n="65.1">Tel</w> <w n="65.2">Maynard</w>, <w n="65.3">pour</w> <w n="65.4">donner</w> <w n="65.5">à</w> <w n="65.6">la</w> <w n="65.7">mienne</w> <w n="65.8">le</w> <w n="65.9">prix</w>,</l>
						<l n="66" num="17.2"><space unit="char" quantity="8"></space><w n="66.1">Infidèle</w> <w n="66.2">à</w> <w n="66.3">sa</w> <w n="66.4">belle</w> <w n="66.5">Vieille</w>,</l>
						<l n="67" num="17.3"><w n="67.1">De</w> <w n="67.2">sa</w> <w n="67.3">stance</w> <w n="67.4">eût</w> <w n="67.5">tiré</w> <w n="67.6">la</w> <w n="67.7">couronne</w> <w n="67.8">vermeille</w></l>
						<l n="68" num="17.4"><space unit="char" quantity="8"></space><w n="68.1">Dédiée</w> <w n="68.2">à</w> <w n="68.3">des</w> <w n="68.4">cheveux</w> <w n="68.5">gris</w>.</l>
					</lg>
					<lg n="18">
						<l n="69" num="18.1"><w n="69.1">Car</w> <w n="69.2">l</w>’<w n="69.3">âge</w> <w n="69.4">a</w> <w n="69.5">respecté</w> <w n="69.6">les</w> <w n="69.7">siens</w> ; <w n="69.8">de</w> <w n="69.9">nul</w> <w n="69.10">fanon</w></l>
						<l n="70" num="18.2"><space unit="char" quantity="8"></space><w n="70.1">Il</w> <w n="70.2">n</w>’<w n="70.3">injurie</w> <w n="70.4">un</w> <w n="70.5">cou</w> <w n="70.6">d</w>’<w n="70.7">ivoire</w>,</l>
						<l n="71" num="18.3"><w n="71.1">Ni</w> <w n="71.2">ses</w> <w n="71.3">pommes</w> <w n="71.4">d</w>’<w n="71.5">amour</w> <w n="71.6">qu</w>’<w n="71.7">à</w> <w n="71.8">peine</w> <w n="71.9">il</w> <w n="71.10">mue</w> <w n="71.11">en</w> <w n="71.12">poires</w>,</l>
						<l n="72" num="18.4"><space unit="char" quantity="8"></space><w n="72.1">Ni</w> <w n="72.2">ses</w> <w n="72.3">bras</w> <w n="72.4">dignes</w> <w n="72.5">de</w> <w n="72.6">Junon</w>.</l>
					</lg>
					<lg n="19">
						<l n="73" num="19.1"><w n="73.1">Et</w> <w n="73.2">le</w> <w n="73.3">plaisir</w> <w n="73.4">ramène</w> <w n="73.5">en</w> <w n="73.6">ses</w> <w n="73.7">yeux</w> <w n="73.8">d</w>’<w n="73.9">aujourd</w>’<w n="73.10">hui</w></l>
						<l n="74" num="19.2"><space unit="char" quantity="8"></space><w n="74.1">Le</w> <w n="74.2">trouble</w> <w n="74.3">émouvant</w> <w n="74.4">de</w> <w n="74.5">la</w> <w n="74.6">gosse</w></l>
						<l n="75" num="19.3"><w n="75.1">Dont</w> <w n="75.2">la</w> <w n="75.3">chair</w> <w n="75.4">est</w> <w n="75.5">choyée</w> <w n="75.6">avant</w> <w n="75.7">l</w>’<w n="75.8">âge</w> <w n="75.9">des</w> <w n="75.10">noces</w>,</l>
						<l n="76" num="19.4"><space unit="char" quantity="8"></space><w n="76.1">Qui</w> <w n="76.2">mord</w> <w n="76.3">et</w> <w n="76.4">repousse</w> <w n="76.5">le</w> <w n="76.6">fruit</w>.</l>
					</lg>
					<lg n="20">
						<l n="77" num="20.1"><w n="77.1">Noces</w> <w n="77.2">tardives</w> ! <w n="77.3">qui</w> <w n="77.4">pendant</w> <w n="77.5">les</w> <w n="77.6">plus</w> <w n="77.7">beaux</w> <w n="77.8">jours</w></l>
						<l n="78" num="20.2"><space unit="char" quantity="8"></space><w n="78.1">Laissent</w> <w n="78.2">la</w> <w n="78.3">jeune</w> <w n="78.4">chair</w> <w n="78.5">en</w> <w n="78.6">friche</w> !</l>
						<l n="79" num="20.3"><w n="79.1">Aimer</w>, <w n="79.2">c</w>’<w n="79.3">est</w> <w n="79.4">vivre</w>, <w n="79.5">et</w> <w n="79.6">dans</w> <w n="79.7">la</w> <w n="79.8">saison</w> <w n="79.9">la</w> <w n="79.10">plus</w> <w n="79.11">riche</w></l>
						<l n="80" num="20.4"><space unit="char" quantity="8"></space><w n="80.1">L</w>’<w n="80.2">état</w> <w n="80.3">de</w> <w n="80.4">grâce</w>, <w n="80.5">c</w>’<w n="80.6">est</w> <w n="80.7">l</w>’<w n="80.8">amour</w>.</l>
					</lg>
					<closer>
						<note id="(1)" type="footnote"><hi rend="ital">Ovide.</hi></note>
					</closer>
				</div></body></text></TEI>