<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE BOURDEAU DES NEUF PUCELLES</title>
				<title type="medium">Édition électronique</title>
				<author key="FRT">
					<name>
						<forename>Charles-Théophile</forename>
						<surname>FERET</surname>
					</name>
					<date from="1858" to="1928">1858-1928</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1111 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">FRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le Bourdeau des neuf pucelles</title>
						<author>Charles-Théophile Feret</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">https://www.gutenberg.org/ebooks/66781</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le Bourdeau des neuf pucelles</title>
								<author>Charles-Théophile Feret</author>
								<imprint>
									<pubPlace>CAUDÉRAN-BORDEAUX</pubPlace>
									<publisher>ÉDITIONS DES CAHIERS LITTÉRAIRES</publisher>
									<date when="1923">1923</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1912">1912</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et les notes de l’éditeur n’ont pas été reprises.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>
					Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique
					après application du programme de calcul de la longueur métrique des vers).
				</p>
				<p>Les notes de bas de page (numérotation conforme à l’édition imprimée) ont été reportées en fin de poème.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension (…).</p>
					<p>Les tirets simples ont été remplacés par des tirets demi-cadratin.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-12-09" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.4.xsd)</change>
				<change when="2021-12-10" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">Vers pour <lb></lb>LES SERVANTES</head><div type="poem" key="FRT24">
					<head type="main">Servante d’Auberge</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">En</w> <w n="1.2">bonhomme</w> <w n="1.3">de</w> <w n="1.4">rat</w> <w n="1.5">qui</w> <w n="1.6">joue</w> <w n="1.7">au</w> <w n="1.8">hobereau</w>,</l>
						<l n="2" num="1.2"><w n="2.1">S</w>’<w n="2.2">il</w> <w n="2.3">faut</w> <w n="2.4">me</w> <w n="2.5">retrancher</w> <w n="2.6">un</w> <w n="2.7">jour</w> <w n="2.8">dans</w> <w n="2.9">un</w> <w n="2.10">fromage</w>,</l>
						<l n="3" num="1.3">— <w n="3.1">Mon</w> <w n="3.2">large</w> <w n="3.3">nez</w> <w n="3.4">ne</w> <w n="3.5">craint</w> <w n="3.6">de</w> <w n="3.7">tels</w> <w n="3.8">parfums</w> <w n="3.9">dommage</w> —</l>
						<l n="4" num="1.4"><w n="4.1">Que</w> <w n="4.2">ce</w> <w n="4.3">soit</w> <w n="4.4">par</w> <w n="4.5">fortune</w> <w n="4.6">en</w> <w n="4.7">un</w> <w n="4.8">gras</w> <w n="4.9">Livarot</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Non</w> <w n="5.2">pas</w> <w n="5.3">que</w> <w n="5.4">de</w> <w n="5.5">ton</w> <w n="5.6">nimbe</w> <w n="5.7">et</w> <w n="5.8">de</w> <w n="5.9">ton</w> <w n="5.10">faux</w> <w n="5.11">douro</w></l>
						<l n="6" num="2.2"><w n="6.1">Je</w> <w n="6.2">cherche</w>, <w n="6.3">ô</w> <w n="6.4">Gloire</w> <w n="6.5">ronde</w> <w n="6.6">et</w> <w n="6.7">rouge</w>, <w n="6.8">quelque</w> <w n="6.9">image</w>.</l>
						<l n="7" num="2.3"><w n="7.1">Bon</w> <w n="7.2">pour</w> <w n="7.3">les</w> <w n="7.4">Muses</w> <w n="7.5">de</w> <w n="7.6">frontispice</w> <w n="7.7">et</w> <w n="7.8">les</w> <w n="7.9">Mages</w>.</l>
						<l n="8" num="2.4"><w n="8.1">Je</w> <w n="8.2">préfère</w> <w n="8.3">à</w> <w n="8.4">de</w> <w n="8.5">secs</w> <w n="8.6">lauriers</w> <w n="8.7">un</w> <w n="8.8">bon</w> <w n="8.9">porreau</w>,</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Du</w> <w n="9.2">cidre</w> <w n="9.3">blond</w> <w n="9.4">pour</w> <w n="9.5">boire</w> <w n="9.6">en</w> <w n="9.7">ma</w> <w n="9.8">couleur</w>… <w n="9.9">passée</w> !</l>
						<l n="10" num="3.2"><w n="10.1">Et</w> <w n="10.2">l</w>’<w n="10.3">épais</w> <w n="10.4">Livarot</w> <w n="10.5">que</w> <w n="10.6">me</w> <w n="10.7">sert</w>, <w n="10.8">haut</w> <w n="10.9">troussée</w>,</l>
						<l n="11" num="3.3"><w n="11.1">Chauffe</w>-<w n="11.2">plat</w>, <w n="11.3">chauffe</w>-<w n="11.4">lit</w>, <w n="11.5">la</w> <w n="11.6">rougeaude</w> <w n="11.7">Lison</w>.</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1"><w n="12.1">A</w> <w n="12.2">ma</w> <w n="12.3">barbe</w> <w n="12.4">qui</w> <w n="12.5">poisse</w>, <w n="12.6">à</w> <w n="12.7">ma</w> <w n="12.8">main</w> <w n="12.9">fourvoyée</w>,</l>
						<l n="13" num="4.2"><w n="13.1">Très</w> <w n="13.2">précieusement</w> <w n="13.3">fouettent</w> <w n="13.4">à</w> <w n="13.5">l</w>’<w n="13.6">unisson</w></l>
						<l n="14" num="4.3"><w n="14.1">Le</w> <w n="14.2">fromage</w> <w n="14.3">onctueux</w> <w n="14.4">et</w> <w n="14.5">la</w> <w n="14.6">femme</w> <w n="14.7">mouillée</w>.</l>
					</lg>
				</div></body></text></TEI>