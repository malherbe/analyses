<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Marie</title>
				<title type="medium">Une édition électronique</title>
				<author key="BRI">
					<name>
						<forename>Auguste</forename>
						<surname>BRIZEUX</surname>
					</name>
					<date from="1803" to="1858">1803-1858</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2412 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BRI_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Marie</title>
						<author>Auguste Brizeux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>FRANTEXT</publisher>
						<idno type="FRANTEXT">M547</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Marie</title>
								<author>Auguste Brizeux</author>
								<imprint>
									<publisher>Garnier,Paris</publisher>
									<date when="1910">1910</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Auguste Brizeux</author>
						<imprint>
							<publisher>Alphonse Lemerre,Éditeur</publisher>
							<date when="1884">1879-1884</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1831">1831</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les balises de pagination ont été supprimées.</p>
				<p>Les astérisques placées devant les noms propres ont été supprimés</p>
				<p>Les vers imcomplets (avec renvoi de la fin de vers) ont été restitués.</p>
				<p>Les citations et dédicaces ont été restituées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BRI10">
				<head type="main">HYMNE</head>
				<head type="sub_1">DÉDIÉ À M. INGRES</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Pieux</w> <w n="1.2">servants</w> <w n="1.3">de</w> <w n="1.4">l</w>’<w n="1.5">art</w>, <w n="1.6">conservez</w> <w n="1.7">la</w> <w n="1.8">beauté</w> !</l>
					<l n="2" num="1.2"><w n="2.1">De</w> <w n="2.2">ce</w> <w n="2.3">moule</w> <w n="2.4">où</w> <w n="2.5">le</w> <w n="2.6">monde</w> <w n="2.7">en</w> <w n="2.8">naissant</w> <w n="2.9">fut</w> <w n="2.10">jeté</w></l>
					<l n="3" num="1.3"><w n="3.1">Des</w> <w n="3.2">types</w> <w n="3.3">merveilleux</w> <w n="3.4">sortirent</w> ; <w n="3.5">le</w> <w n="3.6">poète</w></l>
					<l n="4" num="1.4"><w n="4.1">Comme</w> <w n="4.2">dans</w> <w n="4.3">un</w> <w n="4.4">cristal</w> <w n="4.5">dans</w> <w n="4.6">ses</w> <w n="4.7">chants</w> <w n="4.8">les</w> <w n="4.9">reflète</w>.</l>
					<l n="5" num="1.5"><w n="5.1">Par</w> <w n="5.2">le</w> <w n="5.3">grand</w> <w n="5.4">ouvrier</w> <w n="5.5">tel</w> <w n="5.6">fut</w> <w n="5.7">l</w>’<w n="5.8">ordre</w> <w n="5.9">prescrit</w> :</l>
					<l n="6" num="1.6"><w n="6.1">Il</w> <w n="6.2">mit</w> <w n="6.3">les</w> <w n="6.4">éléments</w> <w n="6.5">sous</w> <w n="6.6">la</w> <w n="6.7">loi</w> <w n="6.8">d</w>’<w n="6.9">un</w> <w n="6.10">esprit</w>,</l>
					<l n="7" num="1.7"><w n="7.1">Pour</w> <w n="7.2">que</w> <w n="7.3">chaque</w> <w n="7.4">rouage</w>, <w n="7.5">en</w> <w n="7.6">l</w>’<w n="7.7">immense</w> <w n="7.8">machine</w>,</l>
					<l n="8" num="1.8"><w n="8.1">Remplit</w>, <w n="8.2">sans</w> <w n="8.3">dévier</w>, <w n="8.4">sa</w> <w n="8.5">fonction</w> <w n="8.6">divine</w> ;</l>
					<l n="9" num="1.9"><w n="9.1">Et</w> <w n="9.2">les</w> <w n="9.3">artistes</w> <w n="9.4">saints</w>, <w n="9.5">créateurs</w> <w n="9.6">après</w> <w n="9.7">Dieu</w>,</l>
					<l n="10" num="1.10"><w n="10.1">Animés</w> <w n="10.2">de</w> <w n="10.3">son</w> <w n="10.4">souffle</w>, <w n="10.5">éclairés</w> <w n="10.6">de</w> <w n="10.7">son</w> <w n="10.8">feu</w>,</l>
					<l n="11" num="1.11"><w n="11.1">Durent</w> <w n="11.2">par</w> <w n="11.3">les</w> <w n="11.4">couleurs</w>, <w n="11.5">et</w> <w n="11.6">le</w> <w n="11.7">marbre</w>, <w n="11.8">et</w> <w n="11.9">la</w> <w n="11.10">lyre</w>,</l>
					<l n="12" num="1.12"><w n="12.1">Rendre</w> <w n="12.2">de</w> <w n="12.3">l</w>’<w n="12.4">univers</w> <w n="12.5">ce</w> <w n="12.6">qu</w>’<w n="12.7">ils</w> <w n="12.8">y</w> <w n="12.9">savent</w> <w n="12.10">lire</w>.</l>
					<l n="13" num="1.13"><w n="13.1">Il</w> <w n="13.2">est</w> <w n="13.3">doux</w> <w n="13.4">par</w> <w n="13.5">le</w> <w n="13.6">beau</w> <w n="13.7">d</w>’<w n="13.8">être</w> <w n="13.9">ainsi</w> <w n="13.10">tourmenté</w>,</l>
					<l n="14" num="1.14"><w n="14.1">Et</w> <w n="14.2">de</w> <w n="14.3">le</w> <w n="14.4">reproduire</w> <w n="14.5">avec</w> <w n="14.6">simplicité</w> ;</l>
					<l n="15" num="1.15"><w n="15.1">Il</w> <w n="15.2">est</w> <w n="15.3">doux</w> <w n="15.4">de</w> <w n="15.5">sentir</w> <w n="15.6">une</w> <w n="15.7">jeune</w> <w n="15.8">figure</w></l>
					<l n="16" num="1.16"><w n="16.1">S</w>’<w n="16.2">élever</w>, <w n="16.3">sous</w> <w n="16.4">nos</w> <w n="16.5">mains</w>, <w n="16.6">harmonieuse</w> <w n="16.7">et</w> <w n="16.8">pure</w>,</l>
					<l n="17" num="1.17"><w n="17.1">Si</w> <w n="17.2">belle</w> <w n="17.3">qu</w>’<w n="17.4">on</w> <w n="17.5">l</w>’<w n="17.6">adore</w> <w n="17.7">et</w> <w n="17.8">qu</w>’<w n="17.9">on</w> <w n="17.10">en</w> <w n="17.11">fait</w> <w n="17.12">le</w> <w n="17.13">tour</w>,</l>
					<l n="18" num="1.18"><w n="18.1">Amoureux</w> <w n="18.2">de</w> <w n="18.3">l</w>’<w n="18.4">ensemble</w> <w n="18.5">et</w> <w n="18.6">de</w> <w n="18.7">chaque</w> <w n="18.8">contour</w> ;</l>
					<l n="19" num="1.19"><w n="19.1">Sous</w> <w n="19.2">la</w> <w n="19.3">forme</w> <w n="19.4">il</w> <w n="19.5">est</w> <w n="19.6">doux</w> <w n="19.7">de</w> <w n="19.8">répandre</w> <w n="19.9">la</w> <w n="19.10">flamme</w>,</l>
					<l n="20" num="1.20"><w n="20.1">En</w> <w n="20.2">s</w>’<w n="20.3">écriant</w> : « <w n="20.4">voici</w> <w n="20.5">la</w> <w n="20.6">fille</w> <w n="20.7">de</w> <w n="20.8">mon</w> <w n="20.9">âme</w> !</l>
					<l n="21" num="1.21"><w n="21.1">Jusqu</w>’<w n="21.2">au</w> <w n="21.3">foyer</w> <w n="21.4">d</w>’<w n="21.5">amour</w> <w n="21.6">pour</w> <w n="21.7">elle</w> <w n="21.8">j</w>’<w n="21.9">ai</w> <w n="21.10">monté</w> :</l>
					<l n="22" num="1.22"><w n="22.1">Admirez</w> <w n="22.2">ce</w> <w n="22.3">reflet</w> <w n="22.4">de</w> <w n="22.5">la</w> <w n="22.6">divinité</w> ! »</l>
					<l n="23" num="1.23"><w n="23.1">Nous</w> <w n="23.2">ne</w> <w n="23.3">redirons</w> <w n="23.4">pas</w> <w n="23.5">ce</w> <w n="23.6">que</w> <w n="23.7">disait</w> <w n="23.8">la</w> <w n="23.9">haine</w>,</l>
					<l n="24" num="1.24"><w n="24.1">Que</w> <w n="24.2">toute</w> <w n="24.3">poésie</w> <w n="24.4">est</w> <w n="24.5">une</w> <w n="24.6">chose</w> <w n="24.7">vaine</w> :</l>
					<l n="25" num="1.25"><w n="25.1">Chanter</w>, <w n="25.2">peindre</w>, <w n="25.3">sculpter</w>, <w n="25.4">c</w>’<w n="25.5">est</w> <w n="25.6">ravir</w> <w n="25.7">au</w> <w n="25.8">tombeau</w></l>
					<l n="26" num="1.26"><w n="26.1">Ce</w> <w n="26.2">que</w> <w n="26.3">la</w> <w n="26.4">main</w> <w n="26.5">divine</w> <w n="26.6">a</w> <w n="26.7">créé</w> <w n="26.8">de</w> <w n="26.9">plus</w> <w n="26.10">beau</w> ;</l>
					<l n="27" num="1.27"><w n="27.1">Chanter</w>, <w n="27.2">c</w>’<w n="27.3">est</w> <w n="27.4">prier</w> <w n="27.5">Dieu</w> ; <w n="27.6">peindre</w>, <w n="27.7">c</w>’<w n="27.8">est</w> <w n="27.9">rendre</w> <w n="27.10">hommage</w></l>
					<l n="28" num="1.28"><w n="28.1">À</w> <w n="28.2">celui</w> <w n="28.3">qui</w> <w n="28.4">forma</w> <w n="28.5">l</w>’<w n="28.6">homme</w> <w n="28.7">à</w> <w n="28.8">sa</w> <w n="28.9">propre</w> <w n="28.10">image</w> ;</l>
					<l n="29" num="1.29"><w n="29.1">Le</w> <w n="29.2">poète</w> <w n="29.3">inspiré</w>, <w n="29.4">le</w> <w n="29.5">peintre</w>, <w n="29.6">le</w> <w n="29.7">sculpteur</w>,</l>
					<l n="30" num="1.30"><w n="30.1">L</w>’<w n="30.2">artiste</w>, <w n="30.3">enfant</w> <w n="30.4">du</w> <w n="30.5">ciel</w>, <w n="30.6">après</w> <w n="30.7">Dieu</w> <w n="30.8">créateur</w>,</l>
					<l n="31" num="1.31"><w n="31.1">Qui</w> <w n="31.2">jeta</w> <w n="31.3">dans</w> <w n="31.4">le</w> <w n="31.5">monde</w> <w n="31.6">une</w> <w n="31.7">œuvre</w> <w n="31.8">harmonieuse</w>,</l>
					<l n="32" num="1.32"><w n="32.1">Peut</w> <w n="32.2">se</w> <w n="32.3">dire</w> : « <w n="32.4">j</w>’<w n="32.5">ai</w> <w n="32.6">fait</w> <w n="32.7">une</w> <w n="32.8">œuvre</w> <w n="32.9">vertueuse</w> ! »</l>
					<l n="33" num="1.33"><w n="33.1">Le</w> <w n="33.2">beau</w>, <w n="33.3">c</w>’<w n="33.4">est</w> <w n="33.5">vers</w> <w n="33.6">le</w> <w n="33.7">bien</w> <w n="33.8">un</w> <w n="33.9">sentier</w> <w n="33.10">radieux</w>,</l>
					<l n="34" num="1.34"><w n="34.1">C</w>’<w n="34.2">est</w> <w n="34.3">le</w> <w n="34.4">vêtement</w> <w n="34.5">d</w>’<w n="34.6">or</w> <w n="34.7">qui</w> <w n="34.8">le</w> <w n="34.9">pare</w> <w n="34.10">à</w> <w n="34.11">nos</w> <w n="34.12">yeux</w>.</l>
				</lg>
			</div></body></text></TEI>