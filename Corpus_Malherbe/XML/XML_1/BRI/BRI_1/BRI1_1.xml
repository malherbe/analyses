<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Marie</title>
				<title type="medium">Une édition électronique</title>
				<author key="BRI">
					<name>
						<forename>Auguste</forename>
						<surname>BRIZEUX</surname>
					</name>
					<date from="1803" to="1858">1803-1858</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2412 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BRI_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Marie</title>
						<author>Auguste Brizeux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>FRANTEXT</publisher>
						<idno type="FRANTEXT">M547</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Marie</title>
								<author>Auguste Brizeux</author>
								<imprint>
									<publisher>Garnier,Paris</publisher>
									<date when="1910">1910</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Auguste Brizeux</author>
						<imprint>
							<publisher>Alphonse Lemerre,Éditeur</publisher>
							<date when="1884">1879-1884</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1831">1831</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les balises de pagination ont été supprimées.</p>
				<p>Les astérisques placées devant les noms propres ont été supprimés</p>
				<p>Les vers imcomplets (avec renvoi de la fin de vers) ont été restitués.</p>
				<p>Les citations et dédicaces ont été restituées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BRI1">
				<head type="main">MARIE (1)</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Rien</w> <w n="1.2">ne</w> <w n="1.3">trouble</w> <w n="1.4">ta</w> <w n="1.5">paix</w>, <w n="1.6">ô</w> <w n="1.7">doux</w> <w n="1.8">Léta</w> ! <w n="1.9">Le</w> <w n="1.10">monde</w></l>
					<l n="2" num="1.2"><w n="2.1">En</w> <w n="2.2">vain</w> <w n="2.3">s</w>’<w n="2.4">agite</w> <w n="2.5">et</w> <w n="2.6">pousse</w> <w n="2.7">une</w> <w n="2.8">plainte</w> <w n="2.9">profonde</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Tu</w> <w n="3.2">n</w>’<w n="3.3">as</w> <w n="3.4">pas</w> <w n="3.5">entendu</w> <w n="3.6">ce</w> <w n="3.7">long</w> <w n="3.8">gémissement</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Et</w> <w n="4.2">ton</w> <w n="4.3">eau</w> <w n="4.4">vers</w> <w n="4.5">la</w> <w n="4.6">mer</w> <w n="4.7">coule</w> <w n="4.8">aussi</w> <w n="4.9">mollement</w> ;</l>
					<l n="5" num="1.5"><w n="5.1">Sur</w> <w n="5.2">l</w>’<w n="5.3">herbe</w> <w n="5.4">de</w> <w n="5.5">tes</w> <w n="5.6">prés</w> <w n="5.7">les</w> <w n="5.8">joyeuses</w> <w n="5.9">cavales</w></l>
					<l n="6" num="1.6"><w n="6.1">Luttent</w> <w n="6.2">chaque</w> <w n="6.3">matin</w>, <w n="6.4">et</w> <w n="6.5">ces</w> <w n="6.6">belles</w> <w n="6.7">rivales</w></l>
					<l n="7" num="1.7"><w n="7.1">Toujours</w> <w n="7.2">d</w>’<w n="7.3">un</w> <w n="7.4">bord</w> <w n="7.5">à</w> <w n="7.6">l</w>’<w n="7.7">autre</w> <w n="7.8">appellent</w> <w n="7.9">leurs</w> <w n="7.10">époux</w>,</l>
					<l n="8" num="1.8"><w n="8.1">Qui</w> <w n="8.2">plongent</w> <w n="8.3">dans</w> <w n="8.4">tes</w> <w n="8.5">flots</w>, <w n="8.6">hennissants</w> <w n="8.7">et</w> <w n="8.8">jaloux</w> :</l>
					<l n="9" num="1.9"><w n="9.1">Il</w> <w n="9.2">m</w>’<w n="9.3">en</w> <w n="9.4">souvient</w> <w n="9.5">ici</w>, <w n="9.6">comme</w> <w n="9.7">en</w> <w n="9.8">cette</w> <w n="9.9">soirée</w></l>
					<l n="10" num="1.10"><w n="10.1">Où</w> <w n="10.2">de</w> <w n="10.3">bœufs</w>, <w n="10.4">de</w> <w n="10.5">chevaux</w> <w n="10.6">notre</w> <w n="10.7">barque</w> <w n="10.8">entourée</w></l>
					<l n="11" num="1.11"><w n="11.1">Sous</w> <w n="11.2">leurs</w> <w n="11.3">pieds</w> <w n="11.4">s</w>’<w n="11.5">abîmait</w>, <w n="11.6">quand</w> <w n="11.7">nous</w>, <w n="11.8">hardis</w> <w n="11.9">marins</w>,</l>
					<l n="12" num="1.12"><w n="12.1">Nous</w> <w n="12.2">gagnâmes</w> <w n="12.3">le</w> <w n="12.4">bord</w>, <w n="12.5">suspendus</w> <w n="12.6">à</w> <w n="12.7">leurs</w> <w n="12.8">crins</w>,</l>
					<l n="13" num="1.13"><w n="13.1">Excitant</w> <w n="13.2">par</w> <w n="13.3">nos</w> <w n="13.4">voix</w> <w n="13.5">et</w> <w n="13.6">suivant</w> <w n="13.7">à</w> <w n="13.8">la</w> <w n="13.9">nage</w></l>
					<l n="14" num="1.14"><w n="14.1">Ce</w> <w n="14.2">troupeau</w> <w n="14.3">qui</w> <w n="14.4">montait</w> <w n="14.5">pêle</w>-<w n="14.6">mêle</w> <w n="14.7">au</w> <w n="14.8">rivage</w>.</l>
					<l n="15" num="1.15"><w n="15.1">J</w>’<w n="15.2">irai</w>, <w n="15.3">j</w>’<w n="15.4">irai</w> <w n="15.5">revoir</w> <w n="15.6">les</w> <w n="15.7">saules</w> <w n="15.8">du</w> <w n="15.9">Létâ</w>,</l>
					<l n="16" num="1.16"><w n="16.1">Et</w> <w n="16.2">toi</w> <w n="16.3">qu</w>’<w n="16.4">en</w> <w n="16.5">ses</w> <w n="16.6">beaux</w> <w n="16.7">jours</w> <w n="16.8">mon</w> <w n="16.9">enfance</w> <w n="16.10">habita</w>,</l>
					<l n="17" num="1.17"><w n="17.1">Paroisse</w> <w n="17.2">bien</w>-<w n="17.3">aimée</w>, <w n="17.4">humble</w> <w n="17.5">coin</w> <w n="17.6">de</w> <w n="17.7">la</w> <w n="17.8">terre</w></l>
					<l n="18" num="1.18"><w n="18.1">Où</w> <w n="18.2">l</w>’<w n="18.3">on</w> <w n="18.4">peut</w> <w n="18.5">vivre</w> <w n="18.6">encore</w> <w n="18.7">et</w> <w n="18.8">mourir</w> <w n="18.9">solitaire</w> !</l>
					<l n="19" num="1.19"><w n="19.1">Aujourd</w>’<w n="19.2">hui</w> <w n="19.3">que</w> <w n="19.4">tout</w> <w n="19.5">cœur</w> <w n="19.6">est</w> <w n="19.7">triste</w> <w n="19.8">et</w> <w n="19.9">que</w> <w n="19.10">chacun</w></l>
					<l n="20" num="1.20"><w n="20.1">Doit</w> <w n="20.2">gémir</w> <w n="20.3">sur</w> <w n="20.4">lui</w>-<w n="20.5">même</w> <w n="20.6">et</w> <w n="20.7">sur</w> <w n="20.8">le</w> <w n="20.9">mal</w> <w n="20.10">commun</w> ;</l>
					<l n="21" num="1.21"><w n="21.1">Que</w> <w n="21.2">le</w> <w n="21.3">monde</w>, <w n="21.4">épuisé</w> <w n="21.5">par</w> <w n="21.6">une</w> <w n="21.7">ardente</w> <w n="21.8">fièvre</w>,</l>
					<l n="22" num="1.22"><w n="22.1">N</w>’<w n="22.2">a</w> <w n="22.3">plus</w> <w n="22.4">un</w> <w n="22.5">souffle</w> <w n="22.6">pur</w> <w n="22.7">pour</w> <w n="22.8">rafraîchir</w> <w n="22.9">sa</w> <w n="22.10">lèvre</w> ;</l>
					<l n="23" num="1.23"><w n="23.1">Qu</w>’<w n="23.2">après</w> <w n="23.3">un</w> <w n="23.4">si</w> <w n="23.5">long</w> <w n="23.6">temps</w> <w n="23.7">de</w> <w n="23.8">périls</w> <w n="23.9">et</w> <w n="23.10">d</w>’<w n="23.11">efforts</w>,</l>
					<l n="24" num="1.24"><w n="24.1">Dans</w> <w n="24.2">l</w>’<w n="24.3">ardeur</w> <w n="24.4">du</w> <w n="24.5">combat</w> <w n="24.6">succombent</w> <w n="24.7">les</w> <w n="24.8">plus</w> <w n="24.9">forts</w> ;</l>
					<l n="25" num="1.25"><w n="25.1">Que</w> <w n="25.2">d</w>’<w n="25.3">autres</w>, <w n="25.4">haletants</w>, <w n="25.5">rendus</w> <w n="25.6">de</w> <w n="25.7">lassitude</w>,</l>
					<l n="26" num="1.26"><w n="26.1">Sont</w> <w n="26.2">près</w> <w n="26.3">de</w> <w n="26.4">défaillir</w>, <w n="26.5">alors</w> <w n="26.6">la</w> <w n="26.7">solitude</w></l>
					<l n="27" num="1.27"><w n="27.1">Vers</w> <w n="27.2">son</w> <w n="27.3">riant</w> <w n="27.4">lointain</w> <w n="27.5">nous</w> <w n="27.6">attire</w>, <w n="27.7">et</w> <w n="27.8">nos</w> <w n="27.9">voix</w></l>
					<l n="28" num="1.28"><w n="28.1">Se</w> <w n="28.2">prennent</w> <w n="28.3">à</w> <w n="28.4">chanter</w> <w n="28.5">l</w>’<w n="28.6">eau</w>, <w n="28.7">les</w> <w n="28.8">fleurs</w> <w n="28.9">et</w> <w n="28.10">les</w> <w n="28.11">bois</w> ;</l>
					<l n="29" num="1.29"><w n="29.1">Alors</w> <w n="29.2">c</w>’<w n="29.3">est</w> <w n="29.4">un</w> <w n="29.5">bonheur</w>, <w n="29.6">quand</w> <w n="29.7">tout</w> <w n="29.8">meurt</w> <w n="29.9">ou</w> <w n="29.10">chancelle</w>,</l>
					<l n="30" num="1.30"><w n="30.1">De</w> <w n="30.2">se</w> <w n="30.3">mêler</w> <w n="30.4">à</w> <w n="30.5">l</w>’<w n="30.6">âme</w> <w n="30.7">immense</w>, <w n="30.8">universelle</w>,</l>
					<l n="31" num="1.31"><w n="31.1">D</w>’<w n="31.2">oublier</w> <w n="31.3">ce</w> <w n="31.4">qui</w> <w n="31.5">fuit</w>, <w n="31.6">les</w> <w n="31.7">peuples</w> <w n="31.8">et</w> <w n="31.9">les</w> <w n="31.10">jours</w>,</l>
					<l n="32" num="1.32"><w n="32.1">Pour</w> <w n="32.2">vivre</w> <w n="32.3">avec</w> <w n="32.4">Dieu</w> <w n="32.5">seul</w>, <w n="32.6">et</w> <w n="32.7">partout</w> <w n="32.8">et</w> <w n="32.9">toujours</w>.</l>
					<l n="33" num="1.33"><w n="33.1">Ainsi</w>, <w n="33.2">lorsque</w> <w n="33.3">la</w> <w n="33.4">flamme</w> <w n="33.5">au</w> <w n="33.6">milieu</w> <w n="33.7">d</w>’<w n="33.8">une</w> <w n="33.9">ville</w></l>
					<l n="34" num="1.34"><w n="34.1">Éclate</w>, <w n="34.2">et</w> <w n="34.3">qu</w>’<w n="34.4">il</w> <w n="34.5">n</w>’<w n="34.6">est</w> <w n="34.7">plus</w> <w n="34.8">contre</w> <w n="34.9">elle</w> <w n="34.10">un</w> <w n="34.11">sûr</w> <w n="34.12">asile</w>,</l>
					<l n="35" num="1.35"><w n="35.1">Hommes</w>, <w n="35.2">femmes</w>, <w n="35.3">chargés</w> <w n="35.4">de</w> <w n="35.5">leurs</w> <w n="35.6">petits</w> <w n="35.7">enfants</w>,</l>
					<l n="36" num="1.36"><w n="36.1">Se</w> <w n="36.2">sauvent</w> <w n="36.3">demi</w>-<w n="36.4">nus</w>, <w n="36.5">et</w>, <w n="36.6">couchés</w> <w n="36.7">dans</w> <w n="36.8">les</w> <w n="36.9">champs</w>,</l>
					<l n="37" num="1.37"><w n="37.1">Ils</w> <w n="37.2">regardent</w> <w n="37.3">de</w> <w n="37.4">loin</w>, <w n="37.5">dans</w> <w n="37.6">un</w> <w n="37.7">morne</w> <w n="37.8">silence</w>,</l>
					<l n="38" num="1.38"><w n="38.1">L</w>’<w n="38.2">incendie</w> <w n="38.3">en</w> <w n="38.4">fureur</w> <w n="38.5">qui</w> <w n="38.6">mugit</w> <w n="38.7">et</w> <w n="38.8">s</w>’<w n="38.9">élance</w> ;</l>
					<l n="39" num="1.39"><w n="39.1">Cependant</w> <w n="39.2">la</w> <w n="39.3">nature</w> <w n="39.4">est</w> <w n="39.5">calme</w>, <w n="39.6">dans</w> <w n="39.7">les</w> <w n="39.8">cieux</w></l>
					<l n="40" num="1.40"><w n="40.1">Chaque</w> <w n="40.2">étoile</w> <w n="40.3">poursuit</w> <w n="40.4">son</w> <w n="40.5">cours</w> <w n="40.6">mystérieux</w>,</l>
					<l n="41" num="1.41"><w n="41.1">Nul</w> <w n="41.2">anneau</w> <w n="41.3">n</w>’<w n="41.4">est</w> <w n="41.5">brisé</w> <w n="41.6">dans</w> <w n="41.7">la</w> <w n="41.8">chaîne</w> <w n="41.9">infinie</w>,</l>
					<l n="42" num="1.42"><w n="42.1">Et</w> <w n="42.2">l</w>’<w n="42.3">univers</w> <w n="42.4">entier</w> <w n="42.5">roule</w> <w n="42.6">avec</w> <w n="42.7">harmonie</w>.</l>
					<l n="43" num="1.43"><w n="43.1">Immuable</w> <w n="43.2">nature</w>, <w n="43.3">apparais</w> <w n="43.4">aujourd</w>’<w n="43.5">hui</w> !</l>
					<l n="44" num="1.44"><w n="44.1">Que</w> <w n="44.2">chacun</w> <w n="44.3">dans</w> <w n="44.4">ton</w> <w n="44.5">sein</w> <w n="44.6">dépose</w> <w n="44.7">son</w> <w n="44.8">ennui</w> !</l>
					<l n="45" num="1.45"><w n="45.1">Tâche</w> <w n="45.2">de</w> <w n="45.3">nous</w> <w n="45.4">séduire</w> <w n="45.5">à</w> <w n="45.6">tes</w> <w n="45.7">beautés</w> <w n="45.8">suprêmes</w>,</l>
					<l n="46" num="1.46"><w n="46.1">Car</w> <w n="46.2">nous</w> <w n="46.3">sommes</w> <w n="46.4">bien</w> <w n="46.5">las</w> <w n="46.6">du</w> <w n="46.7">monde</w> <w n="46.8">et</w> <w n="46.9">de</w> <w n="46.10">nous</w>-<w n="46.11">mêmes</w> :</l>
					<l n="47" num="1.47"><w n="47.1">Si</w> <w n="47.2">tu</w> <w n="47.3">veux</w> <w n="47.4">dévoiler</w> <w n="47.5">ton</w> <w n="47.6">front</w> <w n="47.7">jeune</w> <w n="47.8">et</w> <w n="47.9">divin</w>,</l>
					<l n="48" num="1.48"><w n="48.1">Peut</w>-<w n="48.2">être</w>, <w n="48.3">heureux</w> <w n="48.4">vieillards</w>, <w n="48.5">nous</w> <w n="48.6">sourirons</w> <w n="48.7">enfin</w> !</l>
					<l n="49" num="1.49"><w n="49.1">Celle</w> <w n="49.2">pour</w> <w n="49.3">qui</w> <w n="49.4">j</w>’<w n="49.5">écris</w> <w n="49.6">avec</w> <w n="49.7">amour</w> <w n="49.8">ce</w> <w n="49.9">livre</w></l>
					<l n="50" num="1.50"><w n="50.1">Ne</w> <w n="50.2">le</w> <w n="50.3">lira</w> <w n="50.4">jamais</w> ; <w n="50.5">quand</w> <w n="50.6">le</w> <w n="50.7">soir</w> <w n="50.8">la</w> <w n="50.9">délivre</w></l>
					<l n="51" num="1.51"><w n="51.1">Des</w> <w n="51.2">longs</w> <w n="51.3">travaux</w> <w n="51.4">du</w> <w n="51.5">jour</w>, <w n="51.6">des</w> <w n="51.7">soins</w> <w n="51.8">de</w> <w n="51.9">la</w> <w n="51.10">maison</w>,</l>
					<l n="52" num="1.52"><w n="52.1">C</w>’<w n="52.2">est</w> <w n="52.3">assez</w> <w n="52.4">à</w> <w n="52.5">son</w> <w n="52.6">fils</w> <w n="52.7">de</w> <w n="52.8">dire</w> <w n="52.9">une</w> <w n="52.10">chanson</w> ;</l>
					<l n="53" num="1.53"><w n="53.1">D</w>’<w n="53.2">ailleurs</w>, <w n="53.3">en</w> <w n="53.4">parcourant</w> <w n="53.5">chaque</w> <w n="53.6">feuille</w> <w n="53.7">légère</w>,</l>
					<l n="54" num="1.54"><w n="54.1">Ses</w> <w n="54.2">yeux</w> <w n="54.3">n</w>’<w n="54.4">y</w> <w n="54.5">trouveraient</w> <w n="54.6">qu</w>’<w n="54.7">une</w> <w n="54.8">langue</w> <w n="54.9">étrangère</w>,</l>
					<l n="55" num="1.55"><w n="55.1">Elle</w> <w n="55.2">qui</w> <w n="55.3">n</w>’<w n="55.4">a</w> <w n="55.5">rien</w> <w n="55.6">vu</w> <w n="55.7">que</w> <w n="55.8">ses</w> <w n="55.9">champs</w>, <w n="55.10">ses</w> <w n="55.11">taillis</w>,</l>
					<l n="56" num="1.56"><w n="56.1">Et</w> <w n="56.2">parle</w> <w n="56.3">seulement</w> <w n="56.4">la</w> <w n="56.5">langue</w> <w n="56.6">du</w> <w n="56.7">pays</w>.</l>
					<l n="57" num="1.57"><w n="57.1">Pourtant</w> <w n="57.2">je</w> <w n="57.3">veux</w> <w n="57.4">poursuivre</w> ; <w n="57.5">et</w> <w n="57.6">quelque</w> <w n="57.7">ami</w> <w n="57.8">peut</w>-<w n="57.9">être</w>,</l>
					<l n="58" num="1.58"><w n="58.1">Resté</w> <w n="58.2">dans</w> <w n="58.3">nos</w> <w n="58.4">forêts</w> <w n="58.5">et</w> <w n="58.6">venant</w> <w n="58.7">à</w> <w n="58.8">connaître</w></l>
					<l n="59" num="1.59"><w n="59.1">Ce</w> <w n="59.2">livre</w> <w n="59.3">où</w> <w n="59.4">son</w> <w n="59.5">beau</w> <w n="59.6">temps</w> <w n="59.7">tout</w> <w n="59.8">joyeux</w> <w n="59.9">renaîtra</w>,</l>
					<l n="60" num="1.60"><w n="60.1">Dans</w> <w n="60.2">une</w> <w n="60.3">fête</w>, <w n="60.4">un</w> <w n="60.5">jour</w>, <w n="60.6">en</w> <w n="60.7">dansant</w> <w n="60.8">lui</w> <w n="60.9">dira</w></l>
					<l n="61" num="1.61"><w n="61.1">Cette</w> <w n="61.2">histoire</w> <w n="61.3">qu</w>’<w n="61.4">ici</w> <w n="61.5">j</w>’<w n="61.6">ai</w> <w n="61.7">commencé</w> <w n="61.8">d</w>’<w n="61.9">écrire</w>,</l>
					<l n="62" num="1.62"><w n="62.1">Et</w> <w n="62.2">qu</w>’<w n="62.3">en</w> <w n="62.4">son</w> <w n="62.5">ignorance</w> <w n="62.6">elle</w> <w n="62.7">ne</w> <w n="62.8">doit</w> <w n="62.9">pas</w> <w n="62.10">lire</w> ;</l>
					<l n="63" num="1.63"><w n="63.1">Un</w> <w n="63.2">sourire</w> <w n="63.3">incrédule</w>, <w n="63.4">un</w> <w n="63.5">regard</w> <w n="63.6">curieux</w>,</l>
					<l n="64" num="1.64"><w n="64.1">À</w> <w n="64.2">ce</w> <w n="64.3">récit</w> <w n="64.4">naïf</w>, <w n="64.5">passeront</w> <w n="64.6">dans</w> <w n="64.7">ses</w> <w n="64.8">yeux</w> ;</l>
					<l n="65" num="1.65"><w n="65.1">Puis</w>, <w n="65.2">de</w> <w n="65.3">nouveau</w> <w n="65.4">mêlée</w> <w n="65.5">à</w> <w n="65.6">la</w> <w n="65.7">foule</w> <w n="65.8">qui</w> <w n="65.9">gronde</w>,</l>
					<l n="66" num="1.66"><w n="66.1">Tout</w> <w n="66.2">entière</w> <w n="66.3">au</w> <w n="66.4">plaisir</w> <w n="66.5">elle</w> <w n="66.6">suivra</w> <w n="66.7">la</w> <w n="66.8">ronde</w>.</l>
				</lg>
			</div></body></text></TEI>