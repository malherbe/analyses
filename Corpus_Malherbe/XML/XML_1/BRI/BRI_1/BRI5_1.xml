<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Marie</title>
				<title type="medium">Une édition électronique</title>
				<author key="BRI">
					<name>
						<forename>Auguste</forename>
						<surname>BRIZEUX</surname>
					</name>
					<date from="1803" to="1858">1803-1858</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2412 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BRI_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Marie</title>
						<author>Auguste Brizeux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>FRANTEXT</publisher>
						<idno type="FRANTEXT">M547</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Marie</title>
								<author>Auguste Brizeux</author>
								<imprint>
									<publisher>Garnier,Paris</publisher>
									<date when="1910">1910</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Auguste Brizeux</author>
						<imprint>
							<publisher>Alphonse Lemerre,Éditeur</publisher>
							<date when="1884">1879-1884</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1831">1831</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les balises de pagination ont été supprimées.</p>
				<p>Les astérisques placées devant les noms propres ont été supprimés</p>
				<p>Les vers imcomplets (avec renvoi de la fin de vers) ont été restitués.</p>
				<p>Les citations et dédicaces ont été restituées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BRI5">
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Quand</w> <w n="1.2">on</w> <w n="1.3">est</w> <w n="1.4">plein</w> <w n="1.5">de</w> <w n="1.6">jours</w>, <w n="1.7">gaîment</w> <w n="1.8">on</w> <w n="1.9">les</w> <w n="1.10">prodigue</w> ;</l>
					<l n="2" num="1.2"><w n="2.1">Leur</w> <w n="2.2">flot</w> <w n="2.3">bruyant</w> <w n="2.4">s</w>’<w n="2.5">épanche</w> <w n="2.6">au</w> <w n="2.7">hasard</w> <w n="2.8">et</w> <w n="2.9">sans</w> <w n="2.10">digue</w> ;</l>
					<l n="3" num="1.3"><w n="3.1">C</w>’<w n="3.2">est</w> <w n="3.3">une</w> <w n="3.4">source</w> <w n="3.5">vive</w> <w n="3.6">et</w> <w n="3.7">faite</w> <w n="3.8">pour</w> <w n="3.9">courir</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Et</w> <w n="4.2">qu</w>’<w n="4.3">aucune</w> <w n="4.4">chaleur</w> <w n="4.5">ne</w> <w n="4.6">doit</w> <w n="4.7">jamais</w> <w n="4.8">tarir</w> ;</l>
					<l n="5" num="1.5"><w n="5.1">Pourtant</w> <w n="5.2">la</w> <w n="5.3">chaleur</w> <w n="5.4">vient</w>, <w n="5.5">et</w> <w n="5.6">l</w>’<w n="5.7">eau</w> <w n="5.8">coule</w> <w n="5.9">plus</w> <w n="5.10">rare</w> ;</l>
					<l n="6" num="1.6"><w n="6.1">La</w> <w n="6.2">source</w> <w n="6.3">baisse</w> ; <w n="6.4">alors</w> <w n="6.5">le</w> <w n="6.6">prodigue</w> <w n="6.7">est</w> <w n="6.8">avare</w> :</l>
					<l n="7" num="1.7"><w n="7.1">Incliné</w> <w n="7.2">vers</w> <w n="7.3">ses</w> <w n="7.4">jours</w> <w n="7.5">comme</w> <w n="7.6">vers</w> <w n="7.7">un</w> <w n="7.8">miroir</w>,</l>
					<l n="8" num="1.8"><w n="8.1">Dans</w> <w n="8.2">leur</w> <w n="8.3">onde</w> <w n="8.4">limpide</w> <w n="8.5">il</w> <w n="8.6">cherche</w> <w n="8.7">à</w> <w n="8.8">se</w> <w n="8.9">revoir</w> ;</l>
					<l n="9" num="1.9"><w n="9.1">Mais</w>, <w n="9.2">en</w> <w n="9.3">tombant</w>, <w n="9.4">déjà</w> <w n="9.5">les</w> <w n="9.6">feuilles</w> <w n="9.7">l</w>’<w n="9.8">ont</w> <w n="9.9">voilée</w>,</l>
					<l n="10" num="1.10"><w n="10.1">Et</w> <w n="10.2">l</w>’<w n="10.3">œil</w> <w n="10.4">n</w>’<w n="10.5">y</w> <w n="10.6">peut</w> <w n="10.7">saisir</w> <w n="10.8">qu</w>’<w n="10.9">une</w> <w n="10.10">image</w> <w n="10.11">troublée</w>.</l>
				</lg>
			</div></body></text></TEI>