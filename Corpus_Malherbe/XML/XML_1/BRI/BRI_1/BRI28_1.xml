<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Marie</title>
				<title type="medium">Une édition électronique</title>
				<author key="BRI">
					<name>
						<forename>Auguste</forename>
						<surname>BRIZEUX</surname>
					</name>
					<date from="1803" to="1858">1803-1858</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2412 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BRI_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Marie</title>
						<author>Auguste Brizeux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>FRANTEXT</publisher>
						<idno type="FRANTEXT">M547</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Marie</title>
								<author>Auguste Brizeux</author>
								<imprint>
									<publisher>Garnier,Paris</publisher>
									<date when="1910">1910</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Auguste Brizeux</author>
						<imprint>
							<publisher>Alphonse Lemerre,Éditeur</publisher>
							<date when="1884">1879-1884</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1831">1831</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les balises de pagination ont été supprimées.</p>
				<p>Les astérisques placées devant les noms propres ont été supprimés</p>
				<p>Les vers imcomplets (avec renvoi de la fin de vers) ont été restitués.</p>
				<p>Les citations et dédicaces ont été restituées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BRI28">
				<head type="main">MARIE (7)</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Jamais</w> <w n="1.2">je</w> <w n="1.3">n</w>’<w n="1.4">oublierai</w> <w n="1.5">cette</w> <w n="1.6">immense</w> <w n="1.7">bruyère</w></l>
					<l n="2" num="1.2"><w n="2.1">Où</w> <w n="2.2">cheminant</w> <w n="2.3">tous</w> <w n="2.4">deux</w> <w n="2.5">je</w> <w n="2.6">disais</w> <w n="2.7">à</w> <w n="2.8">mon</w> <w n="2.9">frère</w> :</l>
					<l n="3" num="1.3">« <w n="3.1">Entends</w>-<w n="3.2">tu</w> <w n="3.3">ces</w> <w n="3.4">regrets</w>, <w n="3.5">et</w> <w n="3.6">combien</w> <w n="3.7">il</w> <w n="3.8">est</w> <w n="3.9">doux</w></l>
					<l n="4" num="1.4"><w n="4.1">D</w>’<w n="4.2">avoir</w> <w n="4.3">aimé</w>, <w n="4.4">bien</w> <w n="4.5">jeune</w>, <w n="4.6">une</w> <w n="4.7">enfant</w> <w n="4.8">comme</w> <w n="4.9">vous</w> ;</l>
					<l n="5" num="1.5"><w n="5.1">Sur</w> <w n="5.2">les</w> <w n="5.3">monts</w>, <w n="5.4">dans</w> <w n="5.5">les</w> <w n="5.6">prés</w>, <w n="5.7">quand</w> <w n="5.8">tout</w> <w n="5.9">fleurit</w>, <w n="5.10">embaume</w>,</l>
					<l n="6" num="1.6"><w n="6.1">Ou</w> <w n="6.2">dans</w> <w n="6.3">l</w>’<w n="6.4">église</w> <w n="6.5">obscure</w>, <w n="6.6">en</w> <w n="6.7">récitant</w> <w n="6.8">le</w> <w n="6.9">psaume</w>,</l>
					<l n="7" num="1.7"><w n="7.1">En</w> <w n="7.2">face</w> <w n="7.3">sur</w> <w n="7.4">son</w> <w n="7.5">banc</w> <w n="7.6">de</w> <w n="7.7">se</w> <w n="7.8">voir</w> <w n="7.9">chaque</w> <w n="7.10">jour</w>,</l>
					<l n="8" num="1.8"><w n="8.1">Le</w> <w n="8.2">cœur</w> <w n="8.3">plein</w> <w n="8.4">à</w> <w n="8.5">la</w> <w n="8.6">fois</w> <w n="8.7">de</w> <w n="8.8">piété</w>, <w n="8.9">d</w>’<w n="8.10">amour</w> ;</l>
					<l n="9" num="1.9"><w n="9.1">Les</w> <w n="9.2">signes</w>, <w n="9.3">les</w> <w n="9.4">regards</w> <w n="9.5">tout</w> <w n="9.6">chargés</w> <w n="9.7">de</w> <w n="9.8">mollesse</w> ;</l>
					<l n="10" num="1.10"><w n="10.1">Mille</w> <w n="10.2">pensers</w> <w n="10.3">troublants</w> <w n="10.4">qu</w>’<w n="10.5">il</w> <w n="10.6">faut</w> <w n="10.7">dire</w> <w n="10.8">à</w> <w n="10.9">confesse</w> ;</l>
					<l n="11" num="1.11"><w n="11.1">Les</w> <w n="11.2">projets</w> <w n="11.3">d</w>’<w n="11.4">être</w> <w n="11.5">sage</w>, <w n="11.6">et</w>, <w n="11.7">dès</w> <w n="11.8">le</w> <w n="11.9">lendemain</w>,</l>
					<l n="12" num="1.12"><w n="12.1">Un</w> <w n="12.2">baiser</w> <w n="12.3">qu</w>’<w n="12.4">on</w> <w n="12.5">se</w> <w n="12.6">prend</w> <w n="12.7">ou</w> <w n="12.8">qu</w>’<w n="12.9">on</w> <w n="12.10">donne</w> <w n="12.11">en</w> <w n="12.12">chemin</w> ?</l>
					<l n="13" num="1.13"><w n="13.1">Le</w> <w n="13.2">sens</w>-<w n="13.3">tu</w> <w n="13.4">bien</w>, <w n="13.5">mon</w> <w n="13.6">frère</w> ? <w n="13.7">Et</w> <w n="13.8">lorsqu</w>’<w n="13.9">en</w> <w n="13.10">harmonie</w></l>
					<l n="14" num="1.14"><w n="14.1">Deux</w> <w n="14.2">fois</w> <w n="14.3">par</w> <w n="14.4">la</w> <w n="14.5">beauté</w> <w n="14.6">l</w>’<w n="14.7">âme</w> <w n="14.8">au</w> <w n="14.9">corps</w> <w n="14.10">est</w> <w n="14.11">unie</w>,</l>
					<l n="15" num="1.15"><w n="15.1">Et</w> <w n="15.2">qu</w>’<w n="15.3">ensemble</w> <w n="15.4">éveillés</w> <w n="15.5">notre</w> <w n="15.6">cœur</w> <w n="15.7">et</w> <w n="15.8">nos</w> <w n="15.9">sens</w></l>
					<l n="16" num="1.16"><w n="16.1">Dans</w> <w n="16.2">un</w> <w n="16.3">divin</w> <w n="16.4">accord</w> <w n="16.5">résonnent</w> <w n="16.6">frémissants</w>,</l>
					<l n="17" num="1.17"><w n="17.1">De</w> <w n="17.2">ces</w> <w n="17.3">jeunes</w> <w n="17.4">amours</w>, <w n="17.5">dans</w> <w n="17.6">le</w> <w n="17.7">cœur</w> <w n="17.8">le</w> <w n="17.9">plus</w> <w n="17.10">grave</w>,</l>
					<l n="18" num="1.18"><w n="18.1">Il</w> <w n="18.2">reste</w> <w n="18.3">un</w> <w n="18.4">souvenir</w> <w n="18.5">qui</w> <w n="18.6">pour</w> <w n="18.7">jamais</w> <w n="18.8">s</w>’<w n="18.9">y</w> <w n="18.10">grave</w>,</l>
					<l n="19" num="1.19"><w n="19.1">Un</w> <w n="19.2">parfum</w> <w n="19.3">enivrant</w> <w n="19.4">qu</w>’<w n="19.5">on</w> <w n="19.6">respire</w> <w n="19.7">toujours</w>,</l>
					<l n="20" num="1.20"><w n="20.1">Et</w> <w n="20.2">les</w> <w n="20.3">autres</w> <w n="20.4">amours</w> <w n="20.5">ne</w> <w n="20.6">sont</w> <w n="20.7">plus</w> <w n="20.8">des</w> <w n="20.9">amours</w>. »</l>
					<l n="21" num="1.21"><w n="21.1">Et</w> <w n="21.2">cependant</w>, <w n="21.3">pourquoi</w> <w n="21.4">ce</w> <w n="21.5">pénible</w> <w n="21.6">voyage</w> ?</l>
					<l n="22" num="1.22"><w n="22.1">Aujourd</w>’<w n="22.2">hui</w>, <w n="22.3">dans</w> <w n="22.4">quel</w> <w n="22.5">but</w> ? <w n="22.6">Et</w> <w n="22.7">lorsque</w> <w n="22.8">son</w> <w n="22.9">image</w></l>
					<l n="23" num="1.23"><w n="23.1">M</w>’<w n="23.2">est</w> <w n="23.3">demeurée</w> <w n="23.4">entière</w> <w n="23.5">et</w> <w n="23.6">charmante</w>, <w n="23.7">pourquoi</w></l>
					<l n="24" num="1.24"><w n="24.1">Ternir</w> <w n="24.2">ce</w> <w n="24.3">pur</w> <w n="24.4">miroir</w> <w n="24.5">que</w> <w n="24.6">je</w> <w n="24.7">porte</w> <w n="24.8">avec</w> <w n="24.9">moi</w> ?</l>
					<l n="25" num="1.25"><w n="25.1">Un</w> <w n="25.2">teint</w> <w n="25.3">brûlé</w> <w n="25.4">du</w> <w n="25.5">hâle</w>, <w n="25.6">une</w> <w n="25.7">tempe</w> <w n="25.8">amaigrie</w>,</l>
					<l n="26" num="1.26"><w n="26.1">Un</w> <w n="26.2">œil</w> <w n="26.3">cave</w>, <w n="26.4">est</w>-<w n="26.5">ce</w> <w n="26.6">là</w> <w n="26.7">mon</w> <w n="26.8">ancienne</w> <w n="26.9">Marie</w> ?</l>
				</lg>
				<lg n="2">
					<l n="27" num="2.1"><w n="27.1">C</w>’<w n="27.2">était</w> <w n="27.3">jour</w> <w n="27.4">de</w> <w n="27.5">dimanche</w> <w n="27.6">et</w> <w n="27.7">la</w> <w n="27.8">fête</w> <w n="27.9">du</w> <w n="27.10">bourg</w> :</l>
					<l n="28" num="2.2"><w n="28.1">On</w> <w n="28.2">chantait</w> <w n="28.3">dans</w> <w n="28.4">l</w>’<w n="28.5">église</w> ; <w n="28.6">et</w> <w n="28.7">dehors</w>, <w n="28.8">alentour</w>,</l>
					<l n="29" num="2.3"><w n="29.1">Sous</w> <w n="29.2">le</w> <w n="29.3">porche</w>, <w n="29.4">la</w> <w n="29.5">croix</w>, <w n="29.6">les</w> <w n="29.7">ifs</w> <w n="29.8">du</w> <w n="29.9">cimetière</w>,</l>
					<l n="30" num="2.4"><w n="30.1">Mille</w> <w n="30.2">gens</w> <w n="30.3">à</w> <w n="30.4">genoux</w> <w n="30.5">récitaient</w> <w n="30.6">leur</w> <w n="30.7">prière</w> ;</l>
					<l n="31" num="2.5"><w n="31.1">Parfois</w> <w n="31.2">un</w> <w n="31.3">grand</w> <w n="31.4">silence</w>, <w n="31.5">et</w> <w n="31.6">tout</w> <w n="31.7">à</w> <w n="31.8">coup</w> <w n="31.9">les</w> <w n="31.10">voix</w></l>
					<l n="32" num="2.6"><w n="32.1">Éclataient</w>, <w n="32.2">et</w> <w n="32.3">couraient</w> <w n="32.4">se</w> <w n="32.5">perdre</w> <w n="32.6">dans</w> <w n="32.7">le</w> <w n="32.8">bois</w> ;</l>
					<l n="33" num="2.7"><w n="33.1">La</w> <w n="33.2">messe</w> <w n="33.3">terminée</w>, <w n="33.4">à</w> <w n="33.5">grand</w> <w n="33.6">bruit</w> <w n="33.7">cette</w> <w n="33.8">foule</w></l>
					<l n="34" num="2.8"><w n="34.1">Sur</w> <w n="34.2">la</w> <w n="34.3">place</w> <w n="34.4">du</w> <w n="34.5">lieu</w> <w n="34.6">comme</w> <w n="34.7">une</w> <w n="34.8">mer</w> <w n="34.9">s</w>’<w n="34.10">écoule</w> ;</l>
					<l n="35" num="2.9"><w n="35.1">Alors</w> <w n="35.2">appels</w> <w n="35.3">joyeux</w>, <w n="35.4">rires</w> <w n="35.5">et</w> <w n="35.6">gais</w> <w n="35.7">refrains</w> ;</l>
					<l n="36" num="2.10"><w n="36.1">Les</w> <w n="36.2">voix</w> <w n="36.3">des</w> <w n="36.4">bateleurs</w> <w n="36.5">et</w> <w n="36.6">des</w> <w n="36.7">marchands</w> <w n="36.8">forains</w>,</l>
					<l n="37" num="2.11"><w n="37.1">Le</w> <w n="37.2">sonneur</w> <w n="37.3">sur</w> <w n="37.4">le</w> <w n="37.5">mur</w> <w n="37.6">proclamant</w> <w n="37.7">ses</w> <w n="37.8">criées</w> ;</l>
					<l n="38" num="2.12"><w n="38.1">À</w> <w n="38.2">ses</w> <w n="38.3">bons</w> <w n="38.4">mots</w> <w n="38.5">sans</w> <w n="38.6">nombre</w> <w n="38.7">éclats</w>, <w n="38.8">folles</w> <w n="38.9">huées</w> ;</l>
					<l n="39" num="2.13"><w n="39.1">Lui</w>, <w n="39.2">d</w>’<w n="39.3">un</w> <w n="39.4">air</w> <w n="39.5">goguenard</w>, <w n="39.6">pressait</w> <w n="39.7">les</w> <w n="39.8">acheteurs</w>,</l>
					<l n="40" num="2.14"><w n="40.1">Et</w> <w n="40.2">pour</w> <w n="40.3">un</w> <w n="40.4">blé</w> <w n="40.5">si</w> <w n="40.6">beau</w> <w n="40.7">gourmandait</w> <w n="40.8">leurs</w> <w n="40.9">lenteurs</w>.</l>
					<l n="41" num="2.15"><w n="41.1">Dans</w> <w n="41.2">l</w>’<w n="41.3">auberge</w> <w n="41.4">voisine</w> <w n="41.5">enfin</w> <w n="41.6">l</w>’<w n="41.7">aigre</w> <w n="41.8">bombarde</w></l>
					<l n="42" num="2.16"><w n="42.1">Qui</w> <w n="42.2">sonne</w>, <w n="42.3">les</w> <w n="42.4">binioux</w> <w n="42.5">à</w> <w n="42.6">la</w> <w n="42.7">voix</w> <w n="42.8">nasillarde</w>,</l>
					<l n="43" num="2.17"><w n="43.1">Les</w> <w n="43.2">danseurs</w> <w n="43.3">deux</w> <w n="43.4">à</w> <w n="43.5">deux</w> <w n="43.6">passant</w> <w n="43.7">comme</w> <w n="43.8">l</w>’<w n="43.9">éclair</w>,</l>
					<l n="44" num="2.18"><w n="44.1">Et</w> <w n="44.2">jetant</w> <w n="44.3">en</w> <w n="44.4">cadence</w> <w n="44.5">un</w> <w n="44.6">cri</w> <w n="44.7">qui</w> <w n="44.8">perce</w> <w n="44.9">l</w>’<w n="44.10">air</w>.</l>
				</lg>
				<lg n="3">
					<l n="45" num="3.1"><w n="45.1">Devant</w> <w n="45.2">l</w>’<w n="45.3">un</w> <w n="45.4">des</w> <w n="45.5">marchands</w>, <w n="45.6">bientôt</w> <w n="45.7">trois</w> <w n="45.8">jeunes</w> <w n="45.9">filles</w></l>
					<l n="46" num="3.2"><w n="46.1">Se</w> <w n="46.2">tenant</w> <w n="46.3">par</w> <w n="46.4">la</w> <w n="46.5">main</w>, <w n="46.6">rougissantes</w>, <w n="46.7">gentilles</w>,</l>
					<l n="47" num="3.3"><w n="47.1">Dans</w> <w n="47.2">leurs</w> <w n="47.3">plus</w> <w n="47.4">beaux</w> <w n="47.5">habits</w>, <w n="47.6">s</w>’<w n="47.7">en</w> <w n="47.8">vinrent</w> <w n="47.9">toutes</w> <w n="47.10">trois</w></l>
					<l n="48" num="3.4"><w n="48.1">Acheter</w> <w n="48.2">des</w> <w n="48.3">rubans</w>, <w n="48.4">des</w> <w n="48.5">bagues</w> <w n="48.6">et</w> <w n="48.7">des</w> <w n="48.8">croix</w>.</l>
					<l n="49" num="3.5"><w n="49.1">J</w>’<w n="49.2">approchai</w>. <w n="49.3">Faible</w> <w n="49.4">cœur</w>, <w n="49.5">ô</w> <w n="49.6">cœur</w> <w n="49.7">qui</w> <w n="49.8">bats</w> <w n="49.9">si</w> <w n="49.10">vite</w>,</l>
					<l n="50" num="3.6"><w n="50.1">Que</w> <w n="50.2">la</w> <w n="50.3">peine</w> <w n="50.4">et</w> <w n="50.5">la</w> <w n="50.6">joie</w>, <w n="50.7">et</w> <w n="50.8">tout</w> <w n="50.9">ce</w> <w n="50.10">qui</w> <w n="50.11">t</w>’<w n="50.12">excite</w></l>
					<l n="51" num="3.7"><w n="51.1">Arrive</w> <w n="51.2">désormais</w>, <w n="51.3">puisque</w> <w n="51.4">dans</w> <w n="51.5">ce</w> <w n="51.6">moment</w></l>
					<l n="52" num="3.8"><w n="52.1">Tu</w> <w n="52.2">ne</w> <w n="52.3">t</w>’<w n="52.4">es</w> <w n="52.5">pas</w> <w n="52.6">brisé</w> <w n="52.7">sous</w> <w n="52.8">quelque</w> <w n="52.9">battement</w> !</l>
					<l n="53" num="3.9">— <w n="53.1">Marie</w> ! — <w n="53.2">ah</w> ! <w n="53.3">C</w>’<w n="53.4">était</w> <w n="53.5">elle</w>, <w n="53.6">élégante</w>, <w n="53.7">parée</w> ;</l>
					<l n="54" num="3.10"><w n="54.1">De</w> <w n="54.2">ses</w> <w n="54.3">deux</w> <w n="54.4">sœurs</w> <w n="54.5">enfants</w>, <w n="54.6">sœur</w> <w n="54.7">prudente</w>, <w n="54.8">entourée</w> :</l>
					<l n="55" num="3.11"><w n="55.1">Belle</w> <w n="55.2">comme</w> <w n="55.3">un</w> <w n="55.4">fruit</w> <w n="55.5">mûr</w> <w n="55.6">entre</w> <w n="55.7">deux</w> <w n="55.8">jeunes</w> <w n="55.9">fleurs</w>.</l>
					<l n="56" num="3.12"><w n="56.1">Le</w> <w n="56.2">passé</w>, <w n="56.3">le</w> <w n="56.4">présent</w>, <w n="56.5">le</w> <w n="56.6">sourire</w>, <w n="56.7">les</w> <w n="56.8">pleurs</w>,</l>
					<l n="57" num="3.13"><w n="57.1">Tout</w> <w n="57.2">cela</w> <w n="57.3">devant</w> <w n="57.4">moi</w> ! <w n="57.5">Qu</w>’<w n="57.6">elles</w> <w n="57.7">étaient</w> <w n="57.8">riantes</w>,</l>
					<l n="58" num="3.14"><w n="58.1">Ces</w> <w n="58.2">deux</w> <w n="58.3">sœurs</w> <w n="58.4">de</w> <w n="58.5">Marie</w> <w n="58.6">à</w> <w n="58.7">ses</w> <w n="58.8">côtés</w> <w n="58.9">pendantes</w> !</l>
					<l n="59" num="3.15"><w n="59.1">C</w>’<w n="59.2">était</w> <w n="59.3">Marie</w> <w n="59.4">enfant</w> ; <w n="59.5">je</w> <w n="59.6">voyais</w> <w n="59.7">à</w> <w n="59.8">la</w> <w n="59.9">fois</w></l>
					<l n="60" num="3.16"><w n="60.1">Mes</w> <w n="60.2">amours</w> <w n="60.3">d</w>’<w n="60.4">aujourd</w>’<w n="60.5">hui</w>, <w n="60.6">mes</w> <w n="60.7">amours</w> <w n="60.8">d</w>’<w n="60.9">autrefois</w>,</l>
					<l n="61" num="3.17"><w n="61.1">Mon</w> <w n="61.2">ancienne</w> <w n="61.3">Marie</w> <w n="61.4">encor</w> <w n="61.5">plus</w> <w n="61.6">gracieuse</w> ;</l>
					<l n="62" num="3.18"><w n="62.1">Encor</w> <w n="62.2">son</w> <w n="62.3">joli</w> <w n="62.4">cou</w>, <w n="62.5">sa</w> <w n="62.6">peau</w> <w n="62.7">brune</w> <w n="62.8">et</w> <w n="62.9">soyeuse</w> ;</l>
					<l n="63" num="3.19"><w n="63.1">Légère</w> <w n="63.2">sur</w> <w n="63.3">ses</w> <w n="63.4">pieds</w> ; <w n="63.5">encor</w> <w n="63.6">ses</w> <w n="63.7">yeux</w> <w n="63.8">si</w> <w n="63.9">doux</w></l>
					<l n="64" num="3.20"><w n="64.1">Tandis</w> <w n="64.2">qu</w>’<w n="64.3">elle</w> <w n="64.4">sourit</w> <w n="64.5">regardant</w> <w n="64.6">en</w> <w n="64.7">dessous</w> ;</l>
					<l n="65" num="3.21"><w n="65.1">Et</w> <w n="65.2">puis</w>, <w n="65.3">devant</w> <w n="65.4">ses</w> <w n="65.5">sœurs</w> <w n="65.6">à</w> <w n="65.7">la</w> <w n="65.8">voix</w> <w n="65.9">trop</w> <w n="65.10">légère</w>,</l>
					<l n="66" num="3.22"><w n="66.1">L</w>’<w n="66.2">air</w> <w n="66.3">calme</w> <w n="66.4">d</w>’<w n="66.5">une</w> <w n="66.6">épouse</w> <w n="66.7">et</w> <w n="66.8">d</w>’<w n="66.9">une</w> <w n="66.10">jeune</w> <w n="66.11">mère</w>.</l>
				</lg>
				<lg n="4">
					<l n="67" num="4.1"><w n="67.1">Comme</w> <w n="67.2">elle</w> <w n="67.3">m</w>’<w n="67.4">observait</w> : « <w n="67.5">oh</w> ! <w n="67.6">Lui</w> <w n="67.7">dis</w>-<w n="67.8">je</w> <w n="67.9">en</w> <w n="67.10">breton</w>.</l>
					<l n="68" num="4.2"><w n="68.1">Vous</w> <w n="68.2">ne</w> <w n="68.3">savez</w> <w n="68.4">donc</w> <w n="68.5">plus</w> <w n="68.6">mon</w> <w n="68.7">visage</w> <w n="68.8">et</w> <w n="68.9">mon</w> <w n="68.10">nom</w> ?</l>
					<l n="69" num="4.3"><w n="69.1">Maï</w>, <w n="69.2">regardez</w>-<w n="69.3">moi</w> <w n="69.4">bien</w> ; <w n="69.5">car</w>, <w n="69.6">pour</w> <w n="69.7">moi</w>, <w n="69.8">jeune</w> <w n="69.9">belle</w>,</l>
					<l n="70" num="4.4"><w n="70.1">Vos</w> <w n="70.2">traits</w> <w n="70.3">et</w> <w n="70.4">votre</w> <w n="70.5">nom</w>, <w n="70.6">Maï</w>, <w n="70.7">je</w> <w n="70.8">me</w> <w n="70.9">les</w> <w n="70.10">rappelle</w>.</l>
					<l n="71" num="4.5"><w n="71.1">De</w> <w n="71.2">chez</w> <w n="71.3">vous</w> <w n="71.4">bien</w> <w n="71.5">des</w> <w n="71.6">fois</w> <w n="71.7">je</w> <w n="71.8">faisais</w> <w n="71.9">le</w> <w n="71.10">chemin</w>.</l>
					<l n="72" num="4.6">— <w n="72.1">Mon</w> <w n="72.2">dieu</w>, <w n="72.3">c</w>’<w n="72.4">est</w> <w n="72.5">lui</w> ! » <w n="72.6">dit</w>-<w n="72.7">elle</w> <w n="72.8">en</w> <w n="72.9">me</w> <w n="72.10">prenant</w> <w n="72.11">la</w> <w n="72.12">main</w>.</l>
					<l n="73" num="4.7"><w n="73.1">Et</w> <w n="73.2">nous</w> <w n="73.3">pleurions</w>. <w n="73.4">Bientôt</w> <w n="73.5">j</w>’<w n="73.6">eus</w> <w n="73.7">appris</w> <w n="73.8">son</w> <w n="73.9">histoire</w> :</l>
					<l n="74" num="4.8"><w n="74.1">Un</w> <w n="74.2">mari</w>, <w n="74.3">des</w> <w n="74.4">enfants</w>. <w n="74.5">C</w>’<w n="74.6">était</w> <w n="74.7">tout</w>. <w n="74.8">Comment</w> <w n="74.9">croire</w></l>
					<l n="75" num="4.9"><w n="75.1">À</w> <w n="75.2">ce</w> <w n="75.3">triste</w> <w n="75.4">roman</w> <w n="75.5">qu</w>’<w n="75.6">ensuite</w> <w n="75.7">je</w> <w n="75.8">contai</w> ?</l>
					<l n="76" num="4.10"><w n="76.1">Ma</w> <w n="76.2">mère</w> <w n="76.3">et</w> <w n="76.4">mon</w> <w n="76.5">pays</w>, <w n="76.6">que</w> <w n="76.7">j</w>’<w n="76.8">avais</w> <w n="76.9">tout</w> <w n="76.10">quitté</w> ;</l>
					<l n="77" num="4.11"><w n="77.1">Que</w> <w n="77.2">dans</w> <w n="77.3">Paris</w>, <w n="77.4">si</w> <w n="77.5">loin</w>, <w n="77.6">rêvant</w> <w n="77.7">de</w> <w n="77.8">sa</w> <w n="77.9">chaumière</w>,</l>
					<l n="78" num="4.12"><w n="78.1">Je</w> <w n="78.2">pensais</w> <w n="78.3">à</w> <w n="78.4">Marie</w>, <w n="78.5">elle</w>, <w n="78.6">pauvre</w> <w n="78.7">fermière</w>,</l>
					<l n="79" num="4.13"><w n="79.1">Que</w> <w n="79.2">ce</w> <w n="79.3">jour</w> <w n="79.4">même</w> <w n="79.5">au</w> <w n="79.6">bourg</w> <w n="79.7">j</w>’<w n="79.8">étais</w> <w n="79.9">en</w> <w n="79.10">son</w> <w n="79.11">honneur</w>,</l>
					<l n="80" num="4.14"><w n="80.1">Et</w> <w n="80.2">que</w> <w n="80.3">de</w> <w n="80.4">son</w> <w n="80.5">mari</w> <w n="80.6">j</w>’<w n="80.7">enviais</w> <w n="80.8">le</w> <w n="80.9">bonheur</w> :</l>
					<l n="81" num="4.15"><w n="81.1">Imaginations</w>, <w n="81.2">caprices</w>, <w n="81.3">fou</w> <w n="81.4">délire</w></l>
					<l n="82" num="4.16"><w n="82.1">Qui</w> <w n="82.2">glissait</w> <w n="82.3">sans</w> <w n="82.4">l</w>’<w n="82.5">atteindre</w> <w n="82.6">ou</w> <w n="82.7">la</w> <w n="82.8">faisait</w> <w n="82.9">sourire</w> ! …</l>
					<l n="83" num="4.17"><w n="83.1">Il</w> <w n="83.2">fallut</w> <w n="83.3">se</w> <w n="83.4">quitter</w>. <w n="83.5">Alors</w> <w n="83.6">aux</w> <w n="83.7">deux</w> <w n="83.8">enfants</w></l>
					<l n="84" num="4.18"><w n="84.1">J</w>’<w n="84.2">achetai</w> <w n="84.3">des</w> <w n="84.4">velours</w>, <w n="84.5">des</w> <w n="84.6">croix</w>, <w n="84.7">de</w> <w n="84.8">beaux</w> <w n="84.9">rubans</w>,</l>
					<l n="85" num="4.19"><w n="85.1">Et</w> <w n="85.2">pour</w> <w n="85.3">toutes</w> <w n="85.4">les</w> <w n="85.5">trois</w> <w n="85.6">une</w> <w n="85.7">bague</w> <w n="85.8">de</w> <w n="85.9">cuivre</w>,</l>
					<l n="86" num="4.20"><w n="86.1">Qui</w>, <w n="86.2">bénite</w> <w n="86.3">à</w> <w n="86.4">Kemper</w>, <w n="86.5">de</w> <w n="86.6">tout</w> <w n="86.7">mal</w> <w n="86.8">vous</w> <w n="86.9">délivre</w></l>
					<l n="87" num="4.21"><w n="87.1">Et</w> <w n="87.2">moi</w>-<w n="87.3">même</w> <w n="87.4">à</w> <w n="87.5">leur</w> <w n="87.6">cou</w> <w n="87.7">je</w> <w n="87.8">suspendis</w> <w n="87.9">les</w> <w n="87.10">croix</w>,</l>
					<l n="88" num="4.22"><w n="88.1">Et</w>, <w n="88.2">tremblant</w>, <w n="88.3">je</w> <w n="88.4">passai</w> <w n="88.5">les</w> <w n="88.6">bagues</w> <w n="88.7">à</w> <w n="88.8">leurs</w> <w n="88.9">doigts</w>.</l>
					<l n="89" num="4.23"><w n="89.1">Les</w> <w n="89.2">deux</w> <w n="89.3">petites</w> <w n="89.4">sœurs</w> <w n="89.5">riaient</w> ; <w n="89.6">la</w> <w n="89.7">jeune</w> <w n="89.8">femme</w>,</l>
					<l n="90" num="4.24"><w n="90.1">Tranquille</w> <w n="90.2">et</w> <w n="90.3">sans</w> <w n="90.4">rougir</w>, <w n="90.5">dans</w> <w n="90.6">la</w> <w n="90.7">paix</w> <w n="90.8">de</w> <w n="90.9">son</w> <w n="90.10">âme</w>,</l>
					<l n="91" num="4.25"><w n="91.1">Accepta</w> <w n="91.2">mon</w> <w n="91.3">présent</w>. <w n="91.4">Ce</w> <w n="91.5">modeste</w> <w n="91.6">trésor</w>,</l>
					<l n="92" num="4.26"><w n="92.1">Aux</w> <w n="92.2">yeux</w> <w n="92.3">de</w> <w n="92.4">son</w> <w n="92.5">époux</w> <w n="92.6">elle</w> <w n="92.7">le</w> <w n="92.8">porte</w> <w n="92.9">encor</w> ;</l>
					<l n="93" num="4.27"><w n="93.1">L</w>’<w n="93.2">époux</w> <w n="93.3">est</w> <w n="93.4">sans</w> <w n="93.5">soupçon</w>, <w n="93.6">la</w> <w n="93.7">femme</w> <w n="93.8">sans</w> <w n="93.9">mystère</w>.</l>
					<l n="94" num="4.28"><w n="94.1">L</w>’<w n="94.2">un</w> <w n="94.3">n</w>’<w n="94.4">a</w> <w n="94.5">rien</w> <w n="94.6">à</w> <w n="94.7">savoir</w>, <w n="94.8">l</w>’<w n="94.9">autre</w> <w n="94.10">n</w>’<w n="94.11">a</w> <w n="94.12">rien</w> <w n="94.13">à</w> <w n="94.14">taire</w>.</l>
				</lg>
			</div></body></text></TEI>