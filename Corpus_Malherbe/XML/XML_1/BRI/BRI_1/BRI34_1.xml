<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Marie</title>
				<title type="medium">Une édition électronique</title>
				<author key="BRI">
					<name>
						<forename>Auguste</forename>
						<surname>BRIZEUX</surname>
					</name>
					<date from="1803" to="1858">1803-1858</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2412 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BRI_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Marie</title>
						<author>Auguste Brizeux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>FRANTEXT</publisher>
						<idno type="FRANTEXT">M547</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Marie</title>
								<author>Auguste Brizeux</author>
								<imprint>
									<publisher>Garnier,Paris</publisher>
									<date when="1910">1910</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Auguste Brizeux</author>
						<imprint>
							<publisher>Alphonse Lemerre,Éditeur</publisher>
							<date when="1884">1879-1884</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1831">1831</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les balises de pagination ont été supprimées.</p>
				<p>Les astérisques placées devant les noms propres ont été supprimés</p>
				<p>Les vers imcomplets (avec renvoi de la fin de vers) ont été restitués.</p>
				<p>Les citations et dédicaces ont été restituées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BRI34">
				<head type="main">BONHEUR DOMESTIQUE</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Tous</w> <w n="1.2">les</w> <w n="1.3">jours</w> <w n="1.4">m</w>’<w n="1.5">apportaient</w> <w n="1.6">une</w> <w n="1.7">lettre</w> <w n="1.8">nouvelle</w>.</l>
					<l n="2" num="1.2"><w n="2.1">On</w> <w n="2.2">m</w>’<w n="2.3">écrivait</w> : « <w n="2.4">ami</w>, <w n="2.5">viens</w> ! <w n="2.6">La</w> <w n="2.7">saison</w> <w n="2.8">est</w> <w n="2.9">belle</w> ;</l>
					<l n="3" num="1.3"><w n="3.1">Ma</w> <w n="3.2">femme</w> <w n="3.3">a</w> <w n="3.4">fait</w> <w n="3.5">pour</w> <w n="3.6">toi</w> <w n="3.7">décorer</w> <w n="3.8">sa</w> <w n="3.9">maison</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Et</w> <w n="4.2">mon</w> <w n="4.3">petit</w> <w n="4.4">Arthur</w> <w n="4.5">sait</w> <w n="4.6">bégayer</w> <w n="4.7">ton</w> <w n="4.8">nom</w>. »</l>
					<l n="5" num="1.5"><w n="5.1">Je</w> <w n="5.2">partis</w>, <w n="5.3">et</w> <w n="5.4">deux</w> <w n="5.5">jours</w> <w n="5.6">d</w>’<w n="5.7">une</w> <w n="5.8">route</w> <w n="5.9">poudreuse</w></l>
					<l n="6" num="1.6"><w n="6.1">M</w>’<w n="6.2">amenèrent</w> <w n="6.3">enfin</w> <w n="6.4">à</w> <w n="6.5">la</w> <w n="6.6">maison</w> <w n="6.7">heureuse</w>,</l>
					<l n="7" num="1.7"><w n="7.1">À</w> <w n="7.2">la</w> <w n="7.3">blanche</w> <w n="7.4">maison</w> <w n="7.5">de</w> <w n="7.6">mes</w> <w n="7.7">heureux</w> <w n="7.8">amis</w>.</l>
					<l n="8" num="1.8"><w n="8.1">J</w>’<w n="8.2">entrai</w>, <w n="8.3">l</w>’<w n="8.4">heure</w> <w n="8.5">sonnait</w> ; <w n="8.6">autour</w> <w n="8.7">d</w>’<w n="8.8">un</w> <w n="8.9">couvert</w> <w n="8.10">mis</w>,</l>
					<l n="9" num="1.9"><w n="9.1">Dès</w> <w n="9.2">le</w> <w n="9.3">seuil</w> <w n="9.4">j</w>’<w n="9.5">aperçus</w>, <w n="9.6">en</w> <w n="9.7">rond</w> <w n="9.8">sous</w> <w n="9.9">la</w> <w n="9.10">charmille</w>,</l>
					<l n="10" num="1.10"><w n="10.1">Pour</w> <w n="10.2">le</w> <w n="10.3">repas</w> <w n="10.4">du</w> <w n="10.5">soir</w> <w n="10.6">la</w> <w n="10.7">riante</w> <w n="10.8">famille</w>.</l>
					<l n="11" num="1.11">« <w n="11.1">C</w>’<w n="11.2">est</w> <w n="11.3">lui</w> ! <w n="11.4">C</w>’<w n="11.5">est</w> <w n="11.6">lui</w> ! » <w n="11.7">soudain</w>, <w n="11.8">et</w> <w n="11.9">sièges</w> <w n="11.10">et</w> <w n="11.11">repas</w>,</l>
					<l n="12" num="1.12"><w n="12.1">On</w> <w n="12.2">quitte</w> <w n="12.3">tout</w>, <w n="12.4">on</w> <w n="12.5">court</w>, <w n="12.6">on</w> <w n="12.7">me</w> <w n="12.8">presse</w> <w n="12.9">en</w> <w n="12.10">ses</w> <w n="12.11">bras</w> ;</l>
					<l n="13" num="1.13"><w n="13.1">Et</w> <w n="13.2">puis</w> <w n="13.3">les</w> <w n="13.4">questions</w>, <w n="13.5">les</w> <w n="13.6">pleurs</w> <w n="13.7">mêlés</w> <w n="13.8">de</w> <w n="13.9">rire</w> ;</l>
					<l n="14" num="1.14"><w n="14.1">Et</w> <w n="14.2">ces</w> <w n="14.3">mots</w> <w n="14.4">que</w> <w n="14.5">toujours</w> <w n="14.6">on</w> <w n="14.7">se</w> <w n="14.8">reprend</w> <w n="14.9">à</w> <w n="14.10">dire</w> :</l>
					<l n="15" num="1.15">« <w n="15.1">C</w>’<w n="15.2">est</w> <w n="15.3">donc</w> <w n="15.4">lui</w> ! <w n="15.5">Le</w> <w n="15.6">voilà</w> ! <w n="15.7">Le</w> <w n="15.8">voilà</w> <w n="15.9">près</w> <w n="15.10">de</w> <w n="15.11">nous</w> ! »</l>
					<l n="16" num="1.16"><w n="16.1">Moi</w>, <w n="16.2">je</w> <w n="16.3">serrais</w> <w n="16.4">les</w> <w n="16.5">mains</w> <w n="16.6">à</w> <w n="16.7">ces</w> <w n="16.8">tendres</w> <w n="16.9">époux</w>,</l>
					<l n="17" num="1.17"><w n="17.1">Et</w> <w n="17.2">j</w>’<w n="17.3">appelais</w> <w n="17.4">Arthur</w>, <w n="17.5">qui</w>, <w n="17.6">le</w> <w n="17.7">doigt</w> <w n="17.8">dans</w> <w n="17.9">sa</w> <w n="17.10">bouche</w>,</l>
					<l n="18" num="1.18"><w n="18.1">De</w> <w n="18.2">loin</w> <w n="18.3">me</w> <w n="18.4">regardait</w> <w n="18.5">d</w>’<w n="18.6">un</w> <w n="18.7">œil</w> <w n="18.8">noir</w> <w n="18.9">et</w> <w n="18.10">farouche</w>.</l>
					<l n="19" num="1.19"><w n="19.1">Enfin</w> <w n="19.2">on</w> <w n="19.3">se</w> <w n="19.4">rassied</w>. <w n="19.5">Rougissante</w> <w n="19.6">à</w> <w n="19.7">demi</w>,</l>
					<l n="20" num="1.20"><w n="20.1">La</w> <w n="20.2">jeune</w> <w n="20.3">femme</w> <w n="20.4">alors</w> : « <w n="20.5">vraiment</w> <w n="20.6">de</w> <w n="20.7">ton</w> <w n="20.8">ami</w></l>
					<l n="21" num="1.21"><w n="21.1">Tant</w> <w n="21.2">de</w> <w n="21.3">fois</w> <w n="21.4">tu</w> <w n="21.5">parlas</w> <w n="21.6">que</w>, <w n="21.7">moi</w>, <w n="21.8">sans</w> <w n="21.9">le</w> <w n="21.10">connaître</w>,</l>
					<l n="22" num="1.22"><w n="22.1">Je</w> <w n="22.2">le</w> <w n="22.3">jugeais</w> <w n="22.4">ainsi</w>, <w n="22.5">mais</w> <w n="22.6">moins</w> <w n="22.7">pâle</w> <w n="22.8">peut</w>-<w n="22.9">être</w>.</l>
					<l n="23" num="1.23">— <w n="23.1">Et</w> <w n="23.2">toi</w>, <w n="23.3">de</w> <w n="23.4">mon</w> <w n="23.5">Emma</w> <w n="23.6">que</w> <w n="23.7">dis</w>-<w n="23.8">tu</w> ? <w n="23.9">Sans</w> <w n="23.10">façon</w> !</l>
					<l n="24" num="1.24"><w n="24.1">Le</w> <w n="24.2">paresseux</w> <w n="24.3">pourtant</w> <w n="24.4">de</w> <w n="24.5">demeurer</w> <w n="24.6">garçon</w> !</l>
					<l n="25" num="1.25">— <w n="25.1">Non</w>, <w n="25.2">non</w> ! <w n="25.3">Laissez</w>-<w n="25.4">moi</w> <w n="25.5">faire</w> ; <w n="25.6">en</w> <w n="25.7">ce</w> <w n="25.8">bourg</w> <w n="25.9">j</w>’<w n="25.10">en</w> <w n="25.11">sais</w> <w n="25.12">une</w>,</l>
					<l n="26" num="1.26"><w n="26.1">Comme</w> <w n="26.2">il</w> <w n="26.3">les</w> <w n="26.4">sait</w> <w n="26.5">aimer</w>, <w n="26.6">douce</w>, <w n="26.7">élégante</w> <w n="26.8">et</w> <w n="26.9">brune</w>,</l>
					<l n="27" num="1.27"><w n="27.1">Presque</w> <w n="27.2">une</w> <w n="27.3">autre</w> <w n="27.4">Marie</w>. — <w n="27.5">ah</w> ! <w n="27.6">Poète</w>, <w n="27.7">tes</w> <w n="27.8">vers</w></l>
					<l n="28" num="1.28"><w n="28.1">Nous</w> <w n="28.2">ont</w> <w n="28.3">souvent</w> <w n="28.4">distraits</w> <w n="28.5">de</w> <w n="28.6">l</w>’<w n="28.7">ennui</w> <w n="28.8">des</w> <w n="28.9">hivers</w> :</l>
					<l n="29" num="1.29"><w n="29.1">Oh</w> ! <w n="29.2">La</w> <w n="29.3">jolie</w> <w n="29.4">enfant</w> ! <w n="29.5">Mais</w> <w n="29.6">les</w> <w n="29.7">fraîches</w> <w n="29.8">couronnes</w></l>
					<l n="30" num="1.30"><w n="30.1">Que</w> <w n="30.2">tu</w> <w n="30.3">cueilles</w> <w n="30.4">pour</w> <w n="30.5">elle</w> <w n="30.6">et</w> <w n="30.7">dont</w> <w n="30.8">tu</w> <w n="30.9">l</w>’<w n="30.10">environnes</w> !</l>
				</lg>
				<lg n="2">
					<l n="31" num="2.1"><w n="31.1">Dans</w> <w n="31.2">le</w> <w n="31.3">calme</w>, <w n="31.4">la</w> <w n="31.5">paix</w>, <w n="31.6">les</w> <w n="31.7">bienveillants</w> <w n="31.8">discours</w>,</l>
					<l n="32" num="2.2"><w n="32.1">Huit</w> <w n="32.2">jours</w> <w n="32.3">chez</w> <w n="32.4">ces</w> <w n="32.5">amis</w> <w n="32.6">ont</w> <w n="32.7">passé</w>, <w n="32.8">mais</w> <w n="32.9">si</w> <w n="32.10">courts</w>,</l>
					<l n="33" num="2.3"><w n="33.1">Si</w> <w n="33.2">légers</w>, <w n="33.3">que</w> <w n="33.4">mon</w> <w n="33.5">âme</w> <w n="33.6">alors</w> <w n="33.7">rassérénée</w></l>
					<l n="34" num="2.4"><w n="34.1">Comme</w> <w n="34.2">ailleurs</w> <w n="34.3">un</w> <w n="34.4">instant</w> <w n="34.5">eût</w> <w n="34.6">vu</w> <w n="34.7">fuir</w> <w n="34.8">une</w> <w n="34.9">année</w>.</l>
					<l n="35" num="2.5"><w n="35.1">Là</w> <w n="35.2">nul</w> <w n="35.3">vide</w> <w n="35.4">rongeur</w>, <w n="35.5">mais</w> <w n="35.6">les</w> <w n="35.7">soins</w> <w n="35.8">du</w> <w n="35.9">foyer</w>,</l>
					<l n="36" num="2.6"><w n="36.1">L</w>’<w n="36.2">ordre</w>, <w n="36.3">pour</w> <w n="36.4">chaque</w> <w n="36.5">jour</w> <w n="36.6">un</w> <w n="36.7">travail</w> <w n="36.8">régulier</w>,</l>
					<l n="37" num="2.7"><w n="37.1">Une</w> <w n="37.2">table</w> <w n="37.3">modeste</w> <w n="37.4">et</w> <w n="37.5">pourtant</w> <w n="37.6">bien</w> <w n="37.7">remplie</w>,</l>
					<l n="38" num="2.8"><w n="38.1">Cette</w> <w n="38.2">gaîté</w> <w n="38.3">du</w> <w n="38.4">cœur</w> <w n="38.5">qui</w> <w n="38.6">se</w> <w n="38.7">livre</w> <w n="38.8">et</w> <w n="38.9">s</w>’<w n="38.10">oublie</w></l>
					<l n="39" num="2.9"><w n="39.1">Autour</w> <w n="39.2">de</w> <w n="39.3">soi</w> <w n="39.4">l</w>’<w n="39.5">aisance</w>, <w n="39.6">un</w> <w n="39.7">parfum</w> <w n="39.8">de</w> <w n="39.9">santé</w>,</l>
					<l n="40" num="2.10"><w n="40.1">Et</w> <w n="40.2">toujours</w> <w n="40.3">et</w> <w n="40.4">partout</w> <w n="40.5">la</w> <w n="40.6">belle</w> <w n="40.7">propreté</w> ;</l>
					<l n="41" num="2.11"><w n="41.1">Le</w> <w n="41.2">soir</w>, <w n="41.3">le</w> <w n="41.4">long</w> <w n="41.5">des</w> <w n="41.6">blés</w> <w n="41.7">cheminer</w> <w n="41.8">dans</w> <w n="41.9">la</w> <w n="41.10">plaine</w>,</l>
					<l n="42" num="2.12"><w n="42.1">Ou</w> <w n="42.2">dans</w> <w n="42.3">la</w> <w n="42.4">carriole</w> <w n="42.5">une</w> <w n="42.6">course</w> <w n="42.7">lointaine</w> ;</l>
					<l n="43" num="2.13"><w n="43.1">Enfin</w>, <w n="43.2">la</w> <w n="43.3">nuit</w> <w n="43.4">tombée</w>, <w n="43.5">un</w> <w n="43.6">pur</w> <w n="43.7">et</w> <w n="43.8">long</w> <w n="43.9">sommeil</w>,</l>
					<l n="44" num="2.14"><w n="44.1">Et</w> <w n="44.2">les</w> <w n="44.3">joyeux</w> <w n="44.4">bonjours</w> <w n="44.5">à</w> <w n="44.6">l</w>’<w n="44.7">heure</w> <w n="44.8">du</w> <w n="44.9">réveil</w>.</l>
				</lg>
				<lg n="3">
					<l n="45" num="3.1"><w n="45.1">Ami</w>, <w n="45.2">comme</w> <w n="45.3">un</w> <w n="45.4">tissu</w> <w n="45.5">jadis</w> <w n="45.6">imprégné</w> <w n="45.7">d</w>’<w n="45.8">ambre</w>,</l>
					<l n="46" num="3.2"><w n="46.1">Ici</w>, <w n="46.2">ton</w> <w n="46.3">souvenir</w>, <w n="46.4">sous</w> <w n="46.5">les</w> <w n="46.6">bois</w>, <w n="46.7">dans</w> <w n="46.8">ma</w> <w n="46.9">chambre</w>,</l>
					<l n="47" num="3.3"><w n="47.1">Partout</w>, <w n="47.2">à</w> <w n="47.3">moi</w> <w n="47.4">s</w>’<w n="47.5">attache</w>, <w n="47.6">et</w> <w n="47.7">tes</w> <w n="47.8">félicités</w>,</l>
					<l n="48" num="3.4"><w n="48.1">Mirage</w> <w n="48.2">gracieux</w>, <w n="48.3">flottent</w> <w n="48.4">à</w> <w n="48.5">mes</w> <w n="48.6">côtés</w> ;</l>
					<l n="49" num="3.5"><w n="49.1">Et</w> <w n="49.2">voilà</w> <w n="49.3">que</w>, <w n="49.4">cédant</w> <w n="49.5">à</w> <w n="49.6">cette</w> <w n="49.7">fantaisie</w>,</l>
					<l n="50" num="3.6"><w n="50.1">J</w>’<w n="50.2">évoque</w> <w n="50.3">dans</w> <w n="50.4">mon</w> <w n="50.5">cœur</w> <w n="50.6">la</w> <w n="50.7">chaste</w> <w n="50.8">poésie</w></l>
					<l n="51" num="3.7"><w n="51.1">Qui</w> <w n="51.2">dans</w> <w n="51.3">un</w> <w n="51.4">vers</w> <w n="51.5">limpide</w> <w n="51.6">a</w> <w n="51.7">soudain</w> <w n="51.8">reflété</w></l>
					<l n="52" num="3.8"><w n="52.1">Ta</w> <w n="52.2">jeune</w> <w n="52.3">et</w> <w n="52.4">douce</w> <w n="52.5">Emma</w>, <w n="52.6">sa</w> <w n="52.7">candeur</w>, <w n="52.8">sa</w> <w n="52.9">gaîté</w>,</l>
					<l n="53" num="3.9"><w n="53.1">Entre</w> <w n="53.2">sa</w> <w n="53.3">mère</w> <w n="53.4">et</w> <w n="53.5">toi</w> <w n="53.6">ton</w> <w n="53.7">enfant</w> <w n="53.8">qui</w> <w n="53.9">se</w> <w n="53.10">penche</w>,</l>
					<l n="54" num="3.10"><w n="54.1">Et</w> <w n="54.2">ta</w> <w n="54.3">charmille</w> <w n="54.4">en</w> <w n="54.5">fleur</w> <w n="54.6">près</w> <w n="54.7">de</w> <w n="54.8">ta</w> <w n="54.9">maison</w> <w n="54.10">blanche</w>.</l>
				</lg>
			</div></body></text></TEI>