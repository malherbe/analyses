<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Marie</title>
				<title type="medium">Une édition électronique</title>
				<author key="BRI">
					<name>
						<forename>Auguste</forename>
						<surname>BRIZEUX</surname>
					</name>
					<date from="1803" to="1858">1803-1858</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2412 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BRI_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Marie</title>
						<author>Auguste Brizeux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>FRANTEXT</publisher>
						<idno type="FRANTEXT">M547</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Marie</title>
								<author>Auguste Brizeux</author>
								<imprint>
									<publisher>Garnier,Paris</publisher>
									<date when="1910">1910</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Auguste Brizeux</author>
						<imprint>
							<publisher>Alphonse Lemerre,Éditeur</publisher>
							<date when="1884">1879-1884</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1831">1831</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les balises de pagination ont été supprimées.</p>
				<p>Les astérisques placées devant les noms propres ont été supprimés</p>
				<p>Les vers imcomplets (avec renvoi de la fin de vers) ont été restitués.</p>
				<p>Les citations et dédicaces ont été restituées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BRI44">
				<head type="main">LE PAYSAGISTE</head>
				<opener>
					<salute>À Eugène Guieysse</salute>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">D</w>’<w n="1.2">étranges</w> <w n="1.3">bruits</w> <w n="1.4">couraient</w> <w n="1.5">dans</w> <w n="1.6">toute</w> <w n="1.7">la</w> <w n="1.8">commune</w>.</l>
					<l n="2" num="1.2"><w n="2.1">Voici</w> : <w n="2.2">depuis</w> <w n="2.3">deux</w> <w n="2.4">jours</w> <w n="2.5">un</w> <w n="2.6">homme</w> <w n="2.7">en</w> <w n="2.8">veste</w> <w n="2.9">brune</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Un</w> <w n="3.2">monsieur</w> <w n="3.3">inconnu</w>, <w n="3.4">son</w> <w n="3.5">cahier</w> <w n="3.6">à</w> <w n="3.7">la</w> <w n="3.8">main</w>,</l>
					<l n="4" num="1.4"><w n="4.1">S</w>’<w n="4.2">en</w> <w n="4.3">allait</w> <w n="4.4">griffonnant</w> <w n="4.5">de</w> <w n="4.6">chemin</w> <w n="4.7">en</w> <w n="4.8">chemin</w> ;</l>
					<l n="5" num="1.5"><w n="5.1">Au</w> <w n="5.2">bourg</w> <w n="5.3">on</w> <w n="5.4">l</w>’<w n="5.5">avait</w> <w n="5.6">vu</w>, <w n="5.7">d</w>’<w n="5.8">un</w> <w n="5.9">coin</w> <w n="5.10">du</w> <w n="5.11">cimetière</w>,</l>
					<l n="6" num="1.6"><w n="6.1">Dessiner</w> <w n="6.2">le</w> <w n="6.3">clocher</w> <w n="6.4">et</w> <w n="6.5">les</w> <w n="6.6">deux</w> <w n="6.7">croix</w> <w n="6.8">de</w> <w n="6.9">pierre</w>,</l>
					<l n="7" num="1.7"><w n="7.1">Si</w> <w n="7.2">bien</w> <w n="7.3">que</w> <w n="7.4">le</w> <w n="7.5">clocher</w>, <w n="7.6">quoique</w> <w n="7.7">rapetissé</w>,</l>
					<l n="8" num="1.8"><w n="8.1">Sur</w> <w n="8.2">son</w> <w n="8.3">papier</w> <w n="8.4">maudit</w> <w n="8.5">semblait</w> <w n="8.6">avoir</w> <w n="8.7">passé</w> :</l>
					<l n="9" num="1.9"><w n="9.1">Aussi</w>, <w n="9.2">garçon</w> <w n="9.3">prudent</w>, <w n="9.4">Mélèn</w>, <w n="9.5">à</w> <w n="9.6">son</w> <w n="9.7">approche</w>,</l>
					<l n="10" num="1.10"><w n="10.1">Se</w> <w n="10.2">cacha</w> <w n="10.3">tout</w> <w n="10.4">entier</w> <w n="10.5">sous</w> <w n="10.6">une</w> <w n="10.7">grande</w> <w n="10.8">roche</w> ;</l>
					<l n="11" num="1.11"><w n="11.1">Puis</w>, <w n="11.2">comme</w> <w n="11.3">un</w> <w n="11.4">écureuil</w> <w n="11.5">sautillant</w> <w n="11.6">dans</w> <w n="11.7">les</w> <w n="11.8">bois</w>,</l>
					<l n="12" num="1.12"><w n="12.1">Il</w> <w n="12.2">monta</w> <w n="12.3">sur</w> <w n="12.4">un</w> <w n="12.5">chêne</w> <w n="12.6">en</w> <w n="12.7">criant</w> : « <w n="12.8">je</w> <w n="12.9">vous</w> <w n="12.10">vois</w> ! »</l>
					<l n="13" num="1.13"><w n="13.1">Çà</w> ! <w n="13.2">Que</w> <w n="13.3">voulait</w> <w n="13.4">cet</w> <w n="13.5">homme</w> <w n="13.6">avec</w> <w n="13.7">tous</w> <w n="13.8">ces</w> <w n="13.9">mystères</w> ?</l>
					<l n="14" num="1.14"><w n="14.1">Ce</w> <w n="14.2">savant</w> <w n="14.3">venait</w>-<w n="14.4">il</w> <w n="14.5">pour</w> <w n="14.6">mesurer</w> <w n="14.7">les</w> <w n="14.8">terres</w> ?</l>
					<l n="15" num="1.15"><w n="15.1">Ou</w> <w n="15.2">ne</w> <w n="15.3">voulait</w>-<w n="15.4">il</w> <w n="15.5">pas</w> <w n="15.6">emporter</w>, <w n="15.7">ce</w> <w n="15.8">sorcier</w>,</l>
					<l n="16" num="1.16"><w n="16.1">Les</w> <w n="16.2">champs</w> <w n="16.3">et</w> <w n="16.4">les</w> <w n="16.5">maisons</w> <w n="16.6">couchés</w> <w n="16.7">sur</w> <w n="16.8">son</w> <w n="16.9">papier</w> ?</l>
				</lg>
				<lg n="2">
					<l n="17" num="2.1"><w n="17.1">Mon</w> <w n="17.2">ami</w>, <w n="17.3">c</w>’<w n="17.4">était</w> <w n="17.5">vous</w> ! <w n="17.6">Tendre</w> <w n="17.7">et</w> <w n="17.8">pieux</w> <w n="17.9">artiste</w>,</l>
					<l n="18" num="2.2"><w n="18.1">Vous</w> <w n="18.2">dessiniez</w> <w n="18.3">ces</w> <w n="18.4">lieux</w> <w n="18.5">où</w> <w n="18.6">par</w> <w n="18.7">l</w>’<w n="18.8">âme</w> <w n="18.9">j</w>’<w n="18.10">existe</w>.</l>
					<l n="19" num="2.3"><w n="19.1">Ils</w> <w n="19.2">vivaient</w> <w n="19.3">là</w> <w n="19.4">deux</w> <w n="19.5">fois</w> <w n="19.6">par</w> <w n="19.7">votre</w> <w n="19.8">art</w> <w n="19.9">créateur</w>,</l>
					<l n="20" num="2.4"><w n="20.1">Et</w> <w n="20.2">le</w> <w n="20.3">peintre</w> <w n="20.4">achevait</w> <w n="20.5">l</w>’<w n="20.6">ouvrage</w> <w n="20.7">du</w> <w n="20.8">chanteur</w>.</l>
					<l n="21" num="2.5"><w n="21.1">Eh</w> <w n="21.2">quoi</w> ! <w n="21.3">Vous</w> <w n="21.4">avez</w> <w n="21.5">pu</w> <w n="21.6">pour</w> <w n="21.7">moi</w> <w n="21.8">quitter</w> <w n="21.9">les</w> <w n="21.10">vôtres</w>,</l>
					<l n="22" num="2.6"><w n="22.1">Vous</w>, <w n="22.2">père</w>, <w n="22.3">vous</w>, <w n="22.4">époux</w>, <w n="22.5">tel</w> <w n="22.6">qu</w>’<w n="22.7">il</w> <w n="22.8">n</w>’<w n="22.9">en</w> <w n="22.10">est</w> <w n="22.11">point</w> <w n="22.12">d</w>’<w n="22.13">autres</w> ?</l>
					<l n="23" num="2.7"><w n="23.1">Dans</w> <w n="23.2">mes</w> <w n="23.3">chers</w> <w n="23.4">souvenirs</w> <w n="23.5">vous</w> <w n="23.6">mettant</w> <w n="23.7">de</w> <w n="23.8">moitié</w>,</l>
					<l n="24" num="2.8"><w n="24.1">Seul</w> <w n="24.2">vous</w> <w n="24.3">avez</w> <w n="24.4">deux</w> <w n="24.5">jours</w> <w n="24.6">vécu</w> <w n="24.7">pour</w> <w n="24.8">l</w>’<w n="24.9">amitié</w> ?</l>
					<l n="25" num="2.9"><w n="25.1">Ainsi</w> <w n="25.2">vos</w> <w n="25.3">yeux</w> <w n="25.4">ont</w> <w n="25.5">vu</w> <w n="25.6">la</w> <w n="25.7">terre</w> <w n="25.8">de</w> <w n="25.9">Marie</w>,</l>
					<l n="26" num="2.10"><w n="26.1">Vos</w> <w n="26.2">pas</w> <w n="26.3">du</w> <w n="26.4">double</w> <w n="26.5">fleuve</w> <w n="26.6">ont</w> <w n="26.7">foulé</w> <w n="26.8">la</w> <w n="26.9">prairie</w> ;</l>
					<l n="27" num="2.11"><w n="27.1">Et</w> <w n="27.2">leur</w> <w n="27.3">taillis</w> <w n="27.4">bordé</w> <w n="27.5">de</w> <w n="27.6">buis</w> <w n="27.7">vert</w> <w n="27.8">et</w> <w n="27.9">de</w> <w n="27.10">houx</w>,</l>
					<l n="28" num="2.12"><w n="28.1">Berceau</w> <w n="28.2">de</w> <w n="28.3">poésie</w>, <w n="28.4">a</w> <w n="28.5">murmuré</w> <w n="28.6">sur</w> <w n="28.7">vous</w> !</l>
				</lg>
				<lg n="3">
					<l n="29" num="3.1"><w n="29.1">Cher</w> <w n="29.2">Eugène</w>, <w n="29.3">merci</w> ! <w n="29.4">Votre</w> <w n="29.5">pèlerinage</w></l>
					<l n="30" num="3.2"><w n="30.1">De</w> <w n="30.2">tout</w> <w n="30.3">ce</w> <w n="30.4">que</w> <w n="30.5">j</w>’<w n="30.6">aimais</w> <w n="30.7">m</w>’<w n="30.8">a</w> <w n="30.9">rapporté</w> <w n="30.10">l</w>’<w n="30.11">image</w> :</l>
					<l n="31" num="3.3"><w n="31.1">La</w> <w n="31.2">maison</w> <w n="31.3">du</w> <w n="31.4">curé</w>, <w n="31.5">l</w>’<w n="31.6">église</w>, <w n="31.7">le</w> <w n="31.8">manoir</w>,</l>
					<l n="32" num="3.4"><w n="32.1">Ce</w> <w n="32.2">que</w> <w n="32.3">voyait</w> <w n="32.4">mon</w> <w n="32.5">cœur</w>, <w n="32.6">mes</w> <w n="32.7">yeux</w> <w n="32.8">le</w> <w n="32.9">peuvent</w> <w n="32.10">voir</w>,</l>
					<l n="33" num="3.5"><w n="33.1">Et</w> <w n="33.2">d</w>’<w n="33.3">ici</w> <w n="33.4">je</w> <w n="33.5">rends</w> <w n="33.6">grâce</w> <w n="33.7">à</w> <w n="33.8">vos</w> <w n="33.9">crayons</w> <w n="33.10">noirâtres</w>,</l>
					<l n="34" num="3.6"><w n="34.1">La</w> <w n="34.2">terreur</w>, <w n="34.3">dites</w>-<w n="34.4">vous</w>, <w n="34.5">des</w> <w n="34.6">enfants</w> <w n="34.7">et</w> <w n="34.8">des</w> <w n="34.9">pâtres</w>.</l>
					<l n="35" num="3.7"><w n="35.1">Pour</w> <w n="35.2">vous</w>, <w n="35.3">dans</w> <w n="35.4">leurs</w> <w n="35.5">vallons</w> <w n="35.6">rentrez</w> <w n="35.7">sans</w> <w n="35.8">nulle</w> <w n="35.9">peur</w> :</l>
					<l n="36" num="3.8"><w n="36.1">Mes</w> <w n="36.2">lettres</w> <w n="36.3">ouvriront</w> <w n="36.4">la</w> <w n="36.5">route</w> <w n="36.6">au</w> <w n="36.7">voyageur</w>,</l>
					<l n="37" num="3.9"><w n="37.1">Et</w> <w n="37.2">vous</w> <w n="37.3">n</w>’<w n="37.4">entendrez</w> <w n="37.5">plus</w>, <w n="37.6">en</w> <w n="37.7">longeant</w> <w n="37.8">son</w> <w n="37.9">village</w>,</l>
					<l n="38" num="3.10"><w n="38.1">Sur</w> <w n="38.2">un</w> <w n="38.3">chêne</w> <w n="38.4">crier</w> <w n="38.5">Mélèn</w>, <w n="38.6">l</w>’<w n="38.7">enfant</w> <w n="38.8">sauvage</w>.</l>
				</lg>
			</div></body></text></TEI>