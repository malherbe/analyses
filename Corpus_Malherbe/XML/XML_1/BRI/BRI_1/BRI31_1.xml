<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Marie</title>
				<title type="medium">Une édition électronique</title>
				<author key="BRI">
					<name>
						<forename>Auguste</forename>
						<surname>BRIZEUX</surname>
					</name>
					<date from="1803" to="1858">1803-1858</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2412 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BRI_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Marie</title>
						<author>Auguste Brizeux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>FRANTEXT</publisher>
						<idno type="FRANTEXT">M547</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Marie</title>
								<author>Auguste Brizeux</author>
								<imprint>
									<publisher>Garnier,Paris</publisher>
									<date when="1910">1910</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Auguste Brizeux</author>
						<imprint>
							<publisher>Alphonse Lemerre,Éditeur</publisher>
							<date when="1884">1879-1884</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1831">1831</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les balises de pagination ont été supprimées.</p>
				<p>Les astérisques placées devant les noms propres ont été supprimés</p>
				<p>Les vers imcomplets (avec renvoi de la fin de vers) ont été restitués.</p>
				<p>Les citations et dédicaces ont été restituées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BRI31">
				<head type="main">MARIE (8)</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Ô</w> <w n="1.2">maison</w> <w n="1.3">du</w> <w n="1.4">Moustoir</w> ! <w n="1.5">Combien</w> <w n="1.6">de</w> <w n="1.7">fois</w> <w n="1.8">la</w> <w n="1.9">nuit</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Ou</w> <w n="2.2">quand</w> <w n="2.3">j</w>’<w n="2.4">erre</w> <w n="2.5">le</w> <w n="2.6">jour</w> <w n="2.7">dans</w> <w n="2.8">la</w> <w n="2.9">foule</w> <w n="2.10">et</w> <w n="2.11">le</w> <w n="2.12">bruit</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Tu</w> <w n="3.2">m</w>’<w n="3.3">apparais</w> ! — <w n="3.4">je</w> <w n="3.5">vois</w> <w n="3.6">les</w> <w n="3.7">toits</w> <w n="3.8">de</w> <w n="3.9">ton</w> <w n="3.10">village</w></l>
					<l n="4" num="1.4"><w n="4.1">Baignés</w> <w n="4.2">à</w> <w n="4.3">l</w>’<w n="4.4">horizon</w> <w n="4.5">dans</w> <w n="4.6">des</w> <w n="4.7">mers</w> <w n="4.8">de</w> <w n="4.9">feuillage</w>,</l>
					<l n="5" num="1.5"><w n="5.1">Une</w> <w n="5.2">grêle</w> <w n="5.3">fumée</w> <w n="5.4">au</w>-<w n="5.5">dessus</w>, <w n="5.6">dans</w> <w n="5.7">un</w> <w n="5.8">champ</w></l>
					<l n="6" num="1.6"><w n="6.1">Une</w> <w n="6.2">femme</w> <w n="6.3">de</w> <w n="6.4">loin</w> <w n="6.5">appelant</w> <w n="6.6">son</w> <w n="6.7">enfant</w>,</l>
					<l n="7" num="1.7"><w n="7.1">Ou</w> <w n="7.2">bien</w> <w n="7.3">un</w> <w n="7.4">jeune</w> <w n="7.5">pâtre</w> <w n="7.6">assis</w> <w n="7.7">près</w> <w n="7.8">de</w> <w n="7.9">sa</w> <w n="7.10">vache</w>,</l>
					<l n="8" num="1.8"><w n="8.1">Qui</w>, <w n="8.2">tandis</w> <w n="8.3">qu</w>’<w n="8.4">indolente</w> <w n="8.5">elle</w> <w n="8.6">paît</w> <w n="8.7">à</w> <w n="8.8">l</w>’<w n="8.9">attache</w>,</l>
					<l n="9" num="1.9"><w n="9.1">Entonne</w> <w n="9.2">un</w> <w n="9.3">air</w> <w n="9.4">breton</w> <w n="9.5">si</w> <w n="9.6">plaintif</w> <w n="9.7">et</w> <w n="9.8">si</w> <w n="9.9">doux</w></l>
					<l n="10" num="1.10"><w n="10.1">Qu</w>’<w n="10.2">en</w> <w n="10.3">le</w> <w n="10.4">chantant</w> <w n="10.5">ma</w> <w n="10.6">voix</w> <w n="10.7">vous</w> <w n="10.8">ferait</w> <w n="10.9">pleurer</w> <w n="10.10">tous</w>.</l>
					<l n="11" num="1.11"><w n="11.1">Oh</w> ! <w n="11.2">Les</w> <w n="11.3">bruits</w>, <w n="11.4">les</w> <w n="11.5">odeurs</w>, <w n="11.6">les</w> <w n="11.7">murs</w> <w n="11.8">gris</w> <w n="11.9">des</w> <w n="11.10">chaumières</w>,</l>
					<l n="12" num="1.12"><w n="12.1">Le</w> <w n="12.2">petit</w> <w n="12.3">sentier</w> <w n="12.4">blanc</w> <w n="12.5">et</w> <w n="12.6">bordé</w> <w n="12.7">de</w> <w n="12.8">bruyères</w>,</l>
					<l n="13" num="1.13"><w n="13.1">Tout</w> <w n="13.2">renaît</w> <w n="13.3">comme</w> <w n="13.4">au</w> <w n="13.5">temps</w> <w n="13.6">où</w>, <w n="13.7">pieds</w> <w n="13.8">nus</w>, <w n="13.9">sur</w> <w n="13.10">le</w> <w n="13.11">soir</w>,</l>
					<l n="14" num="1.14"><w n="14.1">J</w>’<w n="14.2">escaladais</w> <w n="14.3">la</w> <w n="14.4">porte</w> <w n="14.5">et</w> <w n="14.6">courais</w> <w n="14.7">au</w> <w n="14.8">Moustoir</w> ;</l>
					<l n="15" num="1.15"><w n="15.1">Et</w> <w n="15.2">dans</w> <w n="15.3">ces</w> <w n="15.4">souvenirs</w> <w n="15.5">où</w> <w n="15.6">je</w> <w n="15.7">me</w> <w n="15.8">sens</w> <w n="15.9">revivre</w>,</l>
					<l n="16" num="1.16"><w n="16.1">Mon</w> <w n="16.2">pauvre</w> <w n="16.3">cœur</w> <w n="16.4">troublé</w> <w n="16.5">se</w> <w n="16.6">délecte</w> <w n="16.7">et</w> <w n="16.8">s</w>’<w n="16.9">enivre</w> !</l>
					<l n="17" num="1.17"><w n="17.1">Aussi</w>, <w n="17.2">sans</w> <w n="17.3">me</w> <w n="17.4">lasser</w>, <w n="17.5">tous</w> <w n="17.6">les</w> <w n="17.7">jours</w> <w n="17.8">je</w> <w n="17.9">revois</w></l>
					<l n="18" num="1.18"><w n="18.1">Le</w> <w n="18.2">haut</w> <w n="18.3">des</w> <w n="18.4">toits</w> <w n="18.5">de</w> <w n="18.6">chaume</w> <w n="18.7">et</w> <w n="18.8">le</w> <w n="18.9">bouquet</w> <w n="18.10">de</w> <w n="18.11">bois</w>,</l>
					<l n="19" num="1.19"><w n="19.1">Au</w> <w n="19.2">vieux</w> <w n="19.3">puits</w> <w n="19.4">la</w> <w n="19.5">servante</w> <w n="19.6">allant</w> <w n="19.7">emplir</w> <w n="19.8">ses</w> <w n="19.9">cruches</w></l>
					<l n="20" num="1.20"><w n="20.1">Et</w> <w n="20.2">le</w> <w n="20.3">courtil</w> <w n="20.4">en</w> <w n="20.5">fleur</w> <w n="20.6">où</w> <w n="20.7">bourdonnent</w> <w n="20.8">les</w> <w n="20.9">ruches</w>,</l>
					<l n="21" num="1.21"><w n="21.1">Et</w> <w n="21.2">l</w>’<w n="21.3">aire</w>, <w n="21.4">et</w> <w n="21.5">le</w> <w n="21.6">lavoir</w>, <w n="21.7">et</w> <w n="21.8">la</w> <w n="21.9">grange</w> ; <w n="21.10">en</w> <w n="21.11">un</w> <w n="21.12">coin</w>,</l>
					<l n="22" num="1.22"><w n="22.1">Les</w> <w n="22.2">pommes</w> <w n="22.3">par</w> <w n="22.4">monceaux</w> ; <w n="22.5">et</w> <w n="22.6">les</w> <w n="22.7">meules</w> <w n="22.8">de</w> <w n="22.9">foin</w> ;</l>
					<l n="23" num="1.23"><w n="23.1">Les</w> <w n="23.2">grands</w> <w n="23.3">bœufs</w> <w n="23.4">étendus</w> <w n="23.5">aux</w> <w n="23.6">portes</w> <w n="23.7">de</w> <w n="23.8">la</w> <w n="23.9">crèche</w>,</l>
					<l n="24" num="1.24"><w n="24.1">Et</w> <w n="24.2">devant</w> <w n="24.3">la</w> <w n="24.4">maison</w> <w n="24.5">un</w> <w n="24.6">lit</w> <w n="24.7">de</w> <w n="24.8">paille</w> <w n="24.9">fraîche</w>.</l>
					<l n="25" num="1.25"><w n="25.1">Et</w> <w n="25.2">j</w>’<w n="25.3">entre</w>, <w n="25.4">et</w> <w n="25.5">c</w>’<w n="25.6">est</w> <w n="25.7">d</w>’<w n="25.8">abord</w> <w n="25.9">un</w> <w n="25.10">silence</w> <w n="25.11">profond</w>,</l>
					<l n="26" num="1.26"><w n="26.1">Une</w> <w n="26.2">nuit</w> <w n="26.3">calme</w> <w n="26.4">et</w> <w n="26.5">noire</w> ; <w n="26.6">aux</w> <w n="26.7">poutres</w> <w n="26.8">du</w> <w n="26.9">plafond</w></l>
					<l n="27" num="1.27"><w n="27.1">Un</w> <w n="27.2">rayon</w> <w n="27.3">de</w> <w n="27.4">soleil</w>, <w n="27.5">seul</w>, <w n="27.6">darde</w> <w n="27.7">sa</w> <w n="27.8">lumière</w>,</l>
					<l n="28" num="1.28"><w n="28.1">Et</w> <w n="28.2">tout</w> <w n="28.3">autour</w> <w n="28.4">de</w> <w n="28.5">lui</w> <w n="28.6">fait</w> <w n="28.7">danser</w> <w n="28.8">la</w> <w n="28.9">poussière</w>.</l>
					<l n="29" num="1.29"><w n="29.1">Chaque</w> <w n="29.2">objet</w> <w n="29.3">cependant</w> <w n="29.4">s</w>’<w n="29.5">éclaircit</w> : <w n="29.6">à</w> <w n="29.7">deux</w> <w n="29.8">pas</w>,</l>
					<l n="30" num="1.30"><w n="30.1">Je</w> <w n="30.2">vois</w> <w n="30.3">le</w> <w n="30.4">lit</w> <w n="30.5">de</w> <w n="30.6">chêne</w> <w n="30.7">et</w> <w n="30.8">son</w> <w n="30.9">coffre</w> ; <w n="30.10">et</w> <w n="30.11">plus</w> <w n="30.12">bas</w></l>
					<l n="31" num="1.31">(<w n="31.1">vers</w> <w n="31.2">la</w> <w n="31.3">porte</w>, <w n="31.4">en</w> <w n="31.5">tournant</w>), <w n="31.6">sur</w> <w n="31.7">le</w> <w n="31.8">bahut</w> <w n="31.9">énorme</w></l>
					<l n="32" num="1.32"><w n="32.1">Pêle</w>-<w n="32.2">mêle</w> <w n="32.3">bassins</w>, <w n="32.4">vases</w> <w n="32.5">de</w> <w n="32.6">toute</w> <w n="32.7">forme</w>,</l>
					<l n="33" num="1.33"><w n="33.1">Pain</w> <w n="33.2">de</w> <w n="33.3">seigle</w>, <w n="33.4">laitage</w>, <w n="33.5">écuelles</w> <w n="33.6">de</w> <w n="33.7">noyer</w> ;</l>
					<l n="34" num="1.34"><w n="34.1">Enfin</w>, <w n="34.2">plus</w> <w n="34.3">bas</w> <w n="34.4">encor</w>, <w n="34.5">sur</w> <w n="34.6">le</w> <w n="34.7">bord</w> <w n="34.8">du</w> <w n="34.9">foyer</w>,</l>
					<l n="35" num="1.35"><w n="35.1">Assise</w> <w n="35.2">à</w> <w n="35.3">son</w> <w n="35.4">rouet</w> <w n="35.5">près</w> <w n="35.6">du</w> <w n="35.7">grillon</w> <w n="35.8">qui</w> <w n="35.9">crie</w>,</l>
					<l n="36" num="1.36"><w n="36.1">Et</w> <w n="36.2">dans</w> <w n="36.3">l</w>’<w n="36.4">ombre</w> <w n="36.5">filant</w>, <w n="36.6">je</w> <w n="36.7">reconnais</w> <w n="36.8">Marie</w> ;</l>
					<l n="37" num="1.37"><w n="37.1">Et</w> <w n="37.2">sous</w> <w n="37.3">sa</w> <w n="37.4">jupe</w> <w n="37.5">blanche</w>, <w n="37.6">arrangeant</w> <w n="37.7">ses</w> <w n="37.8">genoux</w>,</l>
					<l n="38" num="1.38"><w n="38.1">Avec</w> <w n="38.2">son</w> <w n="38.3">doux</w> <w n="38.4">parler</w> <w n="38.5">elle</w> <w n="38.6">me</w> <w n="38.7">dit</w> : « <w n="38.8">c</w>’<w n="38.9">est</w> <w n="38.10">vous</w> ! »</l>
				</lg>
			</div></body></text></TEI>