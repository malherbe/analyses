<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Marie</title>
				<title type="medium">Une édition électronique</title>
				<author key="BRI">
					<name>
						<forename>Auguste</forename>
						<surname>BRIZEUX</surname>
					</name>
					<date from="1803" to="1858">1803-1858</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2412 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BRI_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Marie</title>
						<author>Auguste Brizeux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>FRANTEXT</publisher>
						<idno type="FRANTEXT">M547</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Marie</title>
								<author>Auguste Brizeux</author>
								<imprint>
									<publisher>Garnier,Paris</publisher>
									<date when="1910">1910</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Auguste Brizeux</author>
						<imprint>
							<publisher>Alphonse Lemerre,Éditeur</publisher>
							<date when="1884">1879-1884</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1831">1831</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les balises de pagination ont été supprimées.</p>
				<p>Les astérisques placées devant les noms propres ont été supprimés</p>
				<p>Les vers imcomplets (avec renvoi de la fin de vers) ont été restitués.</p>
				<p>Les citations et dédicaces ont été restituées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BRI32">
				<head type="main">LE DOUTE</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Souvent</w>, <w n="1.2">le</w> <w n="1.3">front</w> <w n="1.4">baissé</w>, <w n="1.5">l</w>’<w n="1.6">œil</w> <w n="1.7">hagard</w>, <w n="1.8">sur</w> <w n="1.9">ma</w> <w n="1.10">route</w></l>
					<l n="2" num="1.2"><w n="2.1">Errant</w> <w n="2.2">à</w> <w n="2.3">mes</w> <w n="2.4">côtés</w>, <w n="2.5">j</w>’<w n="2.6">ai</w> <w n="2.7">rencontré</w> <w n="2.8">le</w> <w n="2.9">doute</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Être</w> <w n="3.2">capricieux</w>, <w n="3.3">craintif</w>, <w n="3.4">qui</w> <w n="3.5">chaque</w> <w n="3.6">fois</w></l>
					<l n="4" num="1.4"><w n="4.1">Changeait</w> <w n="4.2">de</w> <w n="4.3">vêtements</w>, <w n="4.4">de</w> <w n="4.5">visage</w> <w n="4.6">et</w> <w n="4.7">de</w> <w n="4.8">voix</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Un</w> <w n="5.2">jour</w>, <w n="5.3">vieillard</w> <w n="5.4">cynique</w>, <w n="5.5">au</w> <w n="5.6">front</w> <w n="5.7">chauve</w>, <w n="5.8">à</w> <w n="5.9">l</w>’<w n="5.10">œil</w> <w n="5.11">cave</w>,</l>
					<l n="6" num="2.2"><w n="6.1">Le</w> <w n="6.2">désespoir</w> <w n="6.3">empreint</w> <w n="6.4">sur</w> <w n="6.5">son</w> <w n="6.6">teint</w> <w n="6.7">blême</w> <w n="6.8">et</w> <w n="6.9">hâve</w>,</l>
					<l n="7" num="2.3"><w n="7.1">Chancelant</w> <w n="7.2">et</w> <w n="7.3">boiteux</w>, <w n="7.4">d</w>’<w n="7.5">un</w> <w n="7.6">regard</w> <w n="7.7">suppliant</w>,</l>
					<l n="8" num="2.4"><w n="8.1">Il</w> <w n="8.2">se</w> <w n="8.3">traînait</w> <w n="8.4">vers</w> <w n="8.5">moi</w>, <w n="8.6">tel</w> <w n="8.7">qu</w>’<w n="8.8">un</w> <w n="8.9">vil</w> <w n="8.10">mendiant</w></l>
					<l n="9" num="2.5"><w n="9.1">Qui</w> <w n="9.2">de</w> <w n="9.3">loin</w> <w n="9.4">vous</w> <w n="9.5">poursuit</w> <w n="9.6">du</w> <w n="9.7">cri</w> <w n="9.8">de</w> <w n="9.9">ses</w> <w n="9.10">misères</w></l>
					<l n="10" num="2.6"><w n="10.1">Et</w> <w n="10.2">sous</w> <w n="10.3">ses</w> <w n="10.4">haillons</w> <w n="10.5">noirs</w> <w n="10.6">met</w> <w n="10.7">à</w> <w n="10.8">nu</w> <w n="10.9">ses</w> <w n="10.10">ulcères</w>.</l>
					<l n="11" num="2.7"><w n="11.1">Ainsi</w> <w n="11.2">l</w>’<w n="11.3">affreux</w> <w n="11.4">vieillard</w>, <w n="11.5">sans</w> <w n="11.6">honte</w>, <w n="11.7">sans</w> <w n="11.8">remords</w>,</l>
					<l n="12" num="2.8"><w n="12.1">M</w>’<w n="12.2">étalait</w> <w n="12.3">chaque</w> <w n="12.4">plaie</w> <w n="12.5">et</w> <w n="12.6">de</w> <w n="12.7">l</w>’<w n="12.8">âme</w> <w n="12.9">et</w> <w n="12.10">du</w> <w n="12.11">corps</w> :</l>
					<l n="13" num="2.9"><w n="13.1">Sa</w> <w n="13.2">naissance</w> <w n="13.3">sans</w> <w n="13.4">but</w>, <w n="13.5">sa</w> <w n="13.6">fin</w> <w n="13.7">sans</w> <w n="13.8">espérance</w>,</l>
					<l n="14" num="2.10"><w n="14.1">Comme</w> <w n="14.2">il</w> <w n="14.3">avait</w> <w n="14.4">grandi</w> <w n="14.5">pauvre</w> <w n="14.6">et</w> <w n="14.7">dans</w> <w n="14.8">la</w> <w n="14.9">souffrance</w>,</l>
					<l n="15" num="2.11"><w n="15.1">Sa</w> <w n="15.2">jeunesse</w> <w n="15.3">écoulée</w>, <w n="15.4">et</w> <w n="15.5">puis</w>, <w n="15.6">pour</w> <w n="15.7">quelques</w> <w n="15.8">fleurs</w>,</l>
					<l n="16" num="2.12"><w n="16.1">Des</w> <w n="16.2">épines</w> <w n="16.3">sans</w> <w n="16.4">nombre</w> <w n="16.5">et</w> <w n="16.6">d</w>’<w n="16.7">amères</w> <w n="16.8">douleurs</w> ;</l>
					<l n="17" num="2.13"><w n="17.1">Ces</w> <w n="17.2">éternels</w> <w n="17.3">combats</w> <w n="17.4">d</w>’<w n="17.5">une</w> <w n="17.6">nature</w> <w n="17.7">double</w>,</l>
					<l n="18" num="2.14"><w n="18.1">La</w> <w n="18.2">raison</w> <w n="18.3">qui</w> <w n="18.4">commande</w> <w n="18.5">et</w> <w n="18.6">l</w>’<w n="18.7">âme</w> <w n="18.8">qui</w> <w n="18.9">se</w> <w n="18.10">trouble</w> ;</l>
					<l n="19" num="2.15"><w n="19.1">Et</w> <w n="19.2">le</w> <w n="19.3">bien</w> <w n="19.4">et</w> <w n="19.5">le</w> <w n="19.6">mal</w>, <w n="19.7">vieux</w> <w n="19.8">mots</w> <w n="19.9">qu</w>’<w n="19.10">on</w> <w n="19.11">n</w>’<w n="19.12">entend</w> <w n="19.13">pas</w>,</l>
					<l n="20" num="2.16"><w n="20.1">Pareils</w> <w n="20.2">à</w> <w n="20.3">deux</w> <w n="20.4">geôliers</w> <w n="20.5">attachés</w> <w n="20.6">à</w> <w n="20.7">nos</w> <w n="20.8">pas</w>.</l>
					<l n="21" num="2.17"><w n="21.1">Et</w> <w n="21.2">si</w> <w n="21.3">je</w> <w n="21.4">reculais</w> <w n="21.5">devant</w> <w n="21.6">un</w> <w n="21.7">tel</w> <w n="21.8">délire</w>,</l>
					<l n="22" num="2.18"><w n="22.1">Il</w> <w n="22.2">fuyait</w> <w n="22.3">en</w> <w n="22.4">jetant</w> <w n="22.5">un</w> <w n="22.6">grand</w> <w n="22.7">éclat</w> <w n="22.8">de</w> <w n="22.9">rire</w> ;</l>
					<l n="23" num="2.19"><w n="23.1">Et</w> <w n="23.2">moi</w>, <w n="23.3">tel</w> <w n="23.4">qu</w>’<w n="23.5">un</w> <w n="23.6">aveugle</w> <w n="23.7">aux</w> <w n="23.8">murs</w> <w n="23.9">tendant</w> <w n="23.10">la</w> <w n="23.11">main</w>,</l>
					<l n="24" num="2.20"><w n="24.1">À</w> <w n="24.2">tâtons</w>, <w n="24.3">dans</w> <w n="24.4">la</w> <w n="24.5">nuit</w>, <w n="24.6">je</w> <w n="24.7">cherchais</w> <w n="24.8">mon</w> <w n="24.9">chemin</w>.</l>
				</lg>
				<lg n="3">
					<l n="25" num="3.1"><w n="25.1">Une</w> <w n="25.2">autre</w> <w n="25.3">fois</w>, <w n="25.4">paré</w> <w n="25.5">comme</w> <w n="25.6">pour</w> <w n="25.7">un</w> <w n="25.8">dimanche</w>,</l>
					<l n="26" num="3.2"><w n="26.1">C</w>’<w n="26.2">était</w> <w n="26.3">un</w> <w n="26.4">beau</w> <w n="26.5">vieillard</w> <w n="26.6">à</w> <w n="26.7">chevelure</w> <w n="26.8">blanche</w>,</l>
					<l n="27" num="3.3"><w n="27.1">Ferme</w> <w n="27.2">encor</w> <w n="27.3">dans</w> <w n="27.4">sa</w> <w n="27.5">marche</w> <w n="27.6">et</w> <w n="27.7">vert</w>, <w n="27.8">et</w> <w n="27.9">cependant</w></l>
					<l n="28" num="3.4"><w n="28.1">S</w>’<w n="28.2">avançant</w> <w n="28.3">pas</w> <w n="28.4">à</w> <w n="28.5">pas</w> <w n="28.6">d</w>’<w n="28.7">un</w> <w n="28.8">pied</w> <w n="28.9">grave</w> <w n="28.10">et</w> <w n="28.11">prudent</w>.</l>
					<l n="29" num="3.5"><w n="29.1">Il</w> <w n="29.2">disait</w> <w n="29.3">revenir</w> <w n="29.4">de</w> <w n="29.5">quelque</w> <w n="29.6">long</w> <w n="29.7">voyage</w>,</l>
					<l n="30" num="3.6"><w n="30.1">De</w> <w n="30.2">pays</w> <w n="30.3">où</w> <w n="30.4">souvent</w> <w n="30.5">il</w> <w n="30.6">avait</w> <w n="30.7">fait</w> <w n="30.8">naufrage</w> ;</l>
					<l n="31" num="3.7"><w n="31.1">Il</w> <w n="31.2">avait</w> <w n="31.3">vu</w> <w n="31.4">les</w> <w n="31.5">cours</w>, <w n="31.6">les</w> <w n="31.7">villes</w>, <w n="31.8">les</w> <w n="31.9">déserts</w>,</l>
					<l n="32" num="3.8"><w n="32.1">Les</w> <w n="32.2">peuples</w> <w n="32.3">différents</w> <w n="32.4">sous</w> <w n="32.5">leurs</w> <w n="32.6">soleils</w> <w n="32.7">divers</w> ;</l>
					<l n="33" num="3.9"><w n="33.1">Hasards</w> <w n="33.2">bons</w> <w n="33.3">et</w> <w n="33.4">mauvais</w>, <w n="33.5">éprouvant</w> <w n="33.6">toute</w> <w n="33.7">chose</w>,</l>
					<l n="34" num="3.10"><w n="34.1">Il</w> <w n="34.2">arrivait</w> <w n="34.3">enfin</w>, <w n="34.4">non</w> <w n="34.5">désolé</w>, <w n="34.6">morose</w>,</l>
					<l n="35" num="3.11"><w n="35.1">Mais</w> <w n="35.2">mélangeant</w> <w n="35.3">le</w> <w n="35.4">bien</w> <w n="35.5">et</w> <w n="35.6">le</w> <w n="35.7">mal</w> <w n="35.8">par</w> <w n="35.9">moitié</w>,</l>
					<l n="36" num="3.12"><w n="36.1">Et</w> <w n="36.2">plein</w> <w n="36.3">pour</w> <w n="36.4">nous</w>, <w n="36.5">mortels</w>, <w n="36.6">d</w>’<w n="36.7">une</w> <w n="36.8">tendre</w> <w n="36.9">pitié</w>,</l>
					<l n="37" num="3.13"><w n="37.1">Plaignant</w> <w n="37.2">notre</w> <w n="37.3">faiblesse</w>, <w n="37.4">appelant</w> <w n="37.5">l</w>’<w n="37.6">indulgence</w></l>
					<l n="38" num="3.14"><w n="38.1">Sur</w> <w n="38.2">ces</w> <w n="38.3">fautes</w> <w n="38.4">d</w>’<w n="38.5">un</w> <w n="38.6">jour</w>, <w n="38.7">et</w> <w n="38.8">jamais</w> <w n="38.9">la</w> <w n="38.10">vengeance</w>.</l>
					<l n="39" num="3.15"><w n="39.1">Son</w> <w n="39.2">accent</w> <w n="39.3">était</w> <w n="39.4">doux</w>, <w n="39.5">mais</w> <w n="39.6">dans</w> <w n="39.7">ses</w> <w n="39.8">actions</w></l>
					<l n="40" num="3.16"><w n="40.1">Perçait</w> <w n="40.2">le</w> <w n="40.3">feu</w> <w n="40.4">d</w>’<w n="40.5">un</w> <w n="40.6">cœur</w> <w n="40.7">riche</w> <w n="40.8">d</w>’<w n="40.9">émotions</w> ;</l>
					<l n="41" num="3.17"><w n="41.1">Cherchant</w> <w n="41.2">la</w> <w n="41.3">vérité</w>, <w n="41.4">l</w>’<w n="41.5">aimant</w>, <w n="41.6">railleur</w> <w n="41.7">honnête</w>,</l>
					<l n="42" num="3.18"><w n="42.1">À</w> <w n="42.2">toute</w> <w n="42.3">foi</w> <w n="42.4">trop</w> <w n="42.5">vive</w> <w n="42.6">il</w> <w n="42.7">secouait</w> <w n="42.8">la</w> <w n="42.9">tête</w> ;</l>
					<l n="43" num="3.19"><w n="43.1">Souvent</w> <w n="43.2">des</w> <w n="43.3">pleurs</w> <w n="43.4">brillaient</w> <w n="43.5">à</w> <w n="43.6">travers</w> <w n="43.7">son</w> <w n="43.8">souris</w>,</l>
					<l n="44" num="3.20"><w n="44.1">Et</w> <w n="44.2">tout</w> <w n="44.3">en</w> <w n="44.4">vous</w> <w n="44.5">grondant</w> <w n="44.6">il</w> <w n="44.7">vous</w> <w n="44.8">nommait</w> <w n="44.9">son</w> <w n="44.10">fils</w>.</l>
				</lg>
			</div></body></text></TEI>