<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Marie</title>
				<title type="medium">Une édition électronique</title>
				<author key="BRI">
					<name>
						<forename>Auguste</forename>
						<surname>BRIZEUX</surname>
					</name>
					<date from="1803" to="1858">1803-1858</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2412 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BRI_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Marie</title>
						<author>Auguste Brizeux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>FRANTEXT</publisher>
						<idno type="FRANTEXT">M547</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Marie</title>
								<author>Auguste Brizeux</author>
								<imprint>
									<publisher>Garnier,Paris</publisher>
									<date when="1910">1910</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Auguste Brizeux</author>
						<imprint>
							<publisher>Alphonse Lemerre,Éditeur</publisher>
							<date when="1884">1879-1884</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1831">1831</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les balises de pagination ont été supprimées.</p>
				<p>Les astérisques placées devant les noms propres ont été supprimés</p>
				<p>Les vers imcomplets (avec renvoi de la fin de vers) ont été restitués.</p>
				<p>Les citations et dédicaces ont été restituées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BRI33">
				<head type="main">L’ÉLÉGIE DE LE BRAZ</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Si</w> <w n="1.2">vous</w> <w n="1.3">laissez</w> <w n="1.4">encor</w> <w n="1.5">les</w> <w n="1.6">beaux</w> <w n="1.7">genêts</w> <w n="1.8">fleuris</w></l>
					<l n="2" num="1.2"><w n="2.1">Et</w> <w n="2.2">les</w> <w n="2.3">champs</w> <w n="2.4">de</w> <w n="2.5">blé</w> <w n="2.6">noir</w> <w n="2.7">pour</w> <w n="2.8">aller</w> <w n="2.9">à</w> <w n="2.10">Paris</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Quand</w> <w n="3.2">vous</w> <w n="3.3">aurez</w> <w n="3.4">tout</w> <w n="3.5">vu</w> <w n="3.6">dans</w> <w n="3.7">cette</w> <w n="3.8">grande</w> <w n="3.9">ville</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Combien</w> <w n="4.2">elle</w> <w n="4.3">est</w> <w n="4.4">superbe</w> <w n="4.5">et</w> <w n="4.6">combien</w> <w n="4.7">elle</w> <w n="4.8">est</w> <w n="4.9">vile</w>,</l>
					<l n="5" num="1.5"><w n="5.1">Regrettant</w> <w n="5.2">le</w> <w n="5.3">pays</w>, <w n="5.4">informez</w>-<w n="5.5">vous</w> <w n="5.6">alors</w></l>
					<l n="6" num="1.6"><w n="6.1">Où</w> <w n="6.2">du</w> <w n="6.3">pauvre</w> <w n="6.4">Le</w> <w n="6.5">Brâz</w> <w n="6.6">on</w> <w n="6.7">a</w> <w n="6.8">jeté</w> <w n="6.9">le</w> <w n="6.10">corps</w>.</l>
					<l n="7" num="1.7">(<w n="7.1">son</w> <w n="7.2">nom</w> <w n="7.3">serait</w> <w n="7.4">Ar</w>-<w n="7.5">Brâz</w>, <w n="7.6">mais</w> <w n="7.7">nous</w>, <w n="7.8">lâches</w> <w n="7.9">et</w> <w n="7.10">traîtres</w>,</l>
					<l n="8" num="1.8"><w n="8.1">Nous</w> <w n="8.2">avons</w> <w n="8.3">oublié</w> <w n="8.4">les</w> <w n="8.5">noms</w> <w n="8.6">de</w> <w n="8.7">nos</w> <w n="8.8">ancêtres</w>.)</l>
					<l n="9" num="1.9"><w n="9.1">Et</w> <w n="9.2">puis</w> <w n="9.3">devant</w> <w n="9.4">ce</w> <w n="9.5">corps</w> <w n="9.6">brûlé</w> <w n="9.7">par</w> <w n="9.8">le</w> <w n="9.9">charbon</w></l>
					<l n="10" num="1.10"><w n="10.1">Songez</w> <w n="10.2">comme</w> <w n="10.3">il</w> <w n="10.4">mourut</w>, <w n="10.5">lui</w> <w n="10.6">simple</w>, <w n="10.7">honnête</w> <w n="10.8">et</w> <w n="10.9">bon</w>.</l>
					<l n="11" num="1.11"><w n="11.1">C</w>’<w n="11.2">est</w> <w n="11.3">qu</w>’<w n="11.4">il</w> <w n="11.5">avait</w> <w n="11.6">aussi</w> <w n="11.7">quitté</w> <w n="11.8">son</w> <w n="11.9">coin</w> <w n="11.10">de</w> <w n="11.11">terre</w>,</l>
					<l n="12" num="1.12"><w n="12.1">Sur</w> <w n="12.2">le</w> <w n="12.3">bord</w> <w n="12.4">du</w> <w n="12.5">chemin</w> <w n="12.6">sa</w> <w n="12.7">maison</w> <w n="12.8">solitaire</w>,</l>
					<l n="13" num="1.13"><w n="13.1">Le</w> <w n="13.2">pré</w> <w n="13.3">de</w> <w n="13.4">Ker</w>-<w n="13.5">Végan</w>, <w n="13.6">Ar</w>-<w n="13.7">Ros</w>, <w n="13.8">sombres</w> <w n="13.9">coteaux</w> :</l>
					<l n="14" num="1.14"><w n="14.1">Là</w>, <w n="14.2">rencontrant</w> <w n="14.3">la</w> <w n="14.4">mer</w>, <w n="14.5">le</w> <w n="14.6">Scorf</w> <w n="14.7">brise</w> <w n="14.8">ses</w> <w n="14.9">flots</w> ;</l>
					<l n="15" num="1.15"><w n="15.1">Dans</w> <w n="15.2">le</w> <w n="15.3">fond</w>, <w n="15.4">le</w> <w n="15.5">moulin</w> <w n="15.6">fait</w> <w n="15.7">mugir</w> <w n="15.8">son</w> <w n="15.9">écluse</w>,</l>
					<l n="16" num="1.16"><w n="16.1">Et</w> <w n="16.2">dès</w> <w n="16.3">que</w> <w n="16.4">le</w> <w n="16.5">meunier</w> <w n="16.6">enfle</w> <w n="16.7">sa</w> <w n="16.8">cornemuse</w>,</l>
					<l n="17" num="1.17"><w n="17.1">Au</w> <w n="17.2">tomber</w> <w n="17.3">de</w> <w n="17.4">la</w> <w n="17.5">nuit</w>, <w n="17.6">les</w> <w n="17.7">esprits</w> <w n="17.8">des</w> <w n="17.9">talus</w>,</l>
					<l n="18" num="1.18"><w n="18.1">Les</w> <w n="18.2">noirs</w> <w n="18.3">corriganed</w> <w n="18.4">dansent</w> <w n="18.5">sur</w> <w n="18.6">le</w> <w n="18.7">palus</w>.</l>
				</lg>
				<lg n="2">
					<l n="19" num="2.1">— <w n="19.1">Je</w> <w n="19.2">dirai</w> : <w n="19.3">si</w> <w n="19.4">la</w> <w n="19.5">mort</w>, <w n="19.6">dans</w> <w n="19.7">la</w> <w n="19.8">ville</w> <w n="19.9">muette</w></l>
					<l n="20" num="2.2"><w n="20.1">Et</w> <w n="20.2">les</w> <w n="20.3">tristes</w> <w n="20.4">faubourgs</w>, <w n="20.5">passe</w> <w n="20.6">sur</w> <w n="20.7">sa</w> <w n="20.8">charrette</w>,</l>
					<l n="21" num="2.3"><w n="21.1">Prenez</w> <w n="21.2">entre</w> <w n="21.3">vos</w> <w n="21.4">mains</w> <w n="21.5">un</w> <w n="21.6">des</w> <w n="21.7">pans</w> <w n="21.8">du</w> <w n="21.9">linceul</w>,</l>
					<l n="22" num="2.4"><w n="22.1">Car</w> <w n="22.2">le</w> <w n="22.3">malheur</w> <w n="22.4">de</w> <w n="22.5">tous</w> <w n="22.6">est</w> <w n="22.7">le</w> <w n="22.8">malheur</w> <w n="22.9">d</w>’<w n="22.10">un</w> <w n="22.11">seul</w> ;</l>
					<l n="23" num="2.5"><w n="23.1">Mais</w>, <w n="23.2">ô</w> <w n="23.3">bardes</w> <w n="23.4">pieux</w> ! <w n="23.5">Vous</w> <w n="23.6">qui</w> <w n="23.7">parmi</w> <w n="23.8">la</w> <w n="23.9">mousse</w></l>
					<l n="24" num="2.6"><w n="24.1">Retrouverez</w> <w n="24.2">un</w> <w n="24.3">jour</w> <w n="24.4">la</w> <w n="24.5">harpe</w> <w n="24.6">antique</w> <w n="24.7">et</w> <w n="24.8">douce</w>,</l>
					<l n="25" num="2.7"><w n="25.1">Et</w> <w n="25.2">dont</w> <w n="25.3">le</w> <w n="25.4">lai</w> <w n="25.5">savant</w> <w n="25.6">répétera</w> <w n="25.7">dans</w> <w n="25.8">l</w>’<w n="25.9">air</w></l>
					<l n="26" num="2.8"><w n="26.1">Les</w> <w n="26.2">soupirs</w> <w n="26.3">de</w> <w n="26.4">la</w> <w n="26.5">lande</w> <w n="26.6">et</w> <w n="26.7">les</w> <w n="26.8">cris</w> <w n="26.9">de</w> <w n="26.10">la</w> <w n="26.11">mer</w>,</l>
					<l n="27" num="2.9"><w n="27.1">Quand</w> <w n="27.2">avec</w> <w n="27.3">ses</w> <w n="27.4">faubourgs</w> <w n="27.5">la</w> <w n="27.6">ville</w> <w n="27.7">est</w> <w n="27.8">ivre</w> <w n="27.9">et</w> <w n="27.10">folle</w>,</l>
					<l n="28" num="2.10"><w n="28.1">Criez</w> <w n="28.2">qu</w>’<w n="28.3">un</w> <w n="28.4">malheureux</w> <w n="28.5">en</w> <w n="28.6">secret</w> <w n="28.7">se</w> <w n="28.8">désole</w> ;</l>
					<l n="29" num="2.11"><w n="29.1">Si</w> <w n="29.2">vos</w> <w n="29.3">cœurs</w> <w n="29.4">sont</w> <w n="29.5">souffrants</w>, <w n="29.6">vous</w>-<w n="29.7">mêmes</w> <w n="29.8">plaignez</w>-<w n="29.9">vous</w>,</l>
					<l n="30" num="2.12"><w n="30.1">Car</w> <w n="30.2">le</w> <w n="30.3">malheur</w> <w n="30.4">d</w>’<w n="30.5">un</w> <w n="30.6">seul</w> <w n="30.7">est</w> <w n="30.8">le</w> <w n="30.9">malheur</w> <w n="30.10">de</w> <w n="30.11">tous</w>.</l>
					<l n="31" num="2.13"><w n="31.1">Chantres</w> <w n="31.2">de</w> <w n="31.3">mon</w> <w n="31.4">pays</w>, <w n="31.5">plaignez</w> <w n="31.6">celui</w> <w n="31.7">qui</w> <w n="31.8">souffre</w> !</l>
					<l n="32" num="2.14"><w n="32.1">Paris</w> <w n="32.2">roula</w> <w n="32.3">Le</w> <w n="32.4">Brâz</w> <w n="32.5">bien</w> <w n="32.6">longtemps</w> <w n="32.7">dans</w> <w n="32.8">son</w> <w n="32.9">gouffre</w> ;</l>
					<l n="33" num="2.15"><w n="33.1">Un</w> <w n="33.2">ami</w> <w n="33.3">le</w> <w n="33.4">suivait</w> <w n="33.5">durant</w> <w n="33.6">ces</w> <w n="33.7">jours</w> <w n="33.8">hideux</w> :</l>
					<l n="34" num="2.16"><w n="34.1">Tous</w> <w n="34.2">deux</w>, <w n="34.3">pour</w> <w n="34.4">en</w> <w n="34.5">finir</w>, <w n="34.6">s</w>’<w n="34.7">étouffèrent</w> <w n="34.8">tous</w> <w n="34.9">deux</w>. —</l>
				</lg>
				<lg n="3">
					<l n="35" num="3.1"><w n="35.1">Non</w>, <w n="35.2">ce</w> <w n="35.3">n</w>’<w n="35.4">est</w> <w n="35.5">pas</w> <w n="35.6">ainsi</w> <w n="35.7">que</w> <w n="35.8">l</w>’<w n="35.9">on</w> <w n="35.10">meurt</w> <w n="35.11">en</w> <w n="35.12">Bretagne</w> !</l>
					<l n="36" num="3.2"><w n="36.1">La</w> <w n="36.2">vie</w> <w n="36.3">a</w> <w n="36.4">tout</w> <w n="36.5">son</w> <w n="36.6">cours</w> ; <w n="36.7">ou</w>, <w n="36.8">si</w> <w n="36.9">le</w> <w n="36.10">froid</w> <w n="36.11">vous</w> <w n="36.12">gagne</w>,</l>
					<l n="37" num="3.3"><w n="37.1">Comme</w> <w n="37.2">une</w> <w n="37.3">jeune</w> <w n="37.4">plante</w> <w n="37.5">encor</w> <w n="37.6">loin</w> <w n="37.7">de</w> <w n="37.8">juillet</w>,</l>
					<l n="38" num="3.4"><w n="38.1">Celle</w> <w n="38.2">qui</w> <w n="38.3">vous</w> <w n="38.4">nourrit</w> <w n="38.5">autrefois</w> <w n="38.6">de</w> <w n="38.7">son</w> <w n="38.8">lait</w></l>
					<l n="39" num="3.5"><w n="39.1">S</w>’<w n="39.2">assied</w> <w n="39.3">à</w> <w n="39.4">votre</w> <w n="39.5">lit</w> ; <w n="39.6">pleurant</w> <w n="39.7">sur</w> <w n="39.8">son</w> <w n="39.9">ouvrage</w>,</l>
					<l n="40" num="3.6"><w n="40.1">De</w> <w n="40.2">la</w> <w n="40.3">voix</w> <w n="40.4">cependant</w> <w n="40.5">elle</w> <w n="40.6">vous</w> <w n="40.7">encourage</w> ;</l>
					<l n="41" num="3.7"><w n="41.1">Et</w> <w n="41.2">lorsqu</w>’<w n="41.3">enfin</w> <w n="41.4">le</w> <w n="41.5">corps</w> <w n="41.6">reste</w> <w n="41.7">seul</w> <w n="41.8">sur</w> <w n="41.9">le</w> <w n="41.10">lit</w>,</l>
					<l n="42" num="3.8"><w n="42.1">De</w> <w n="42.2">ses</w> <w n="42.3">tremblantes</w> <w n="42.4">mains</w> <w n="42.5">elle</w> <w n="42.6">l</w>’<w n="42.7">ensevelit</w> ;</l>
					<l n="43" num="3.9"><w n="43.1">La</w> <w n="43.2">foule</w>, <w n="43.3">vers</w> <w n="43.4">le</w> <w n="43.5">soir</w>, <w n="43.6">l</w>’<w n="43.7">emporte</w> <w n="43.8">et</w> <w n="43.9">l</w>’<w n="43.10">accompagne</w></l>
					<l n="44" num="3.10"><w n="44.1">Jusques</w> <w n="44.2">au</w> <w n="44.3">cimetière</w> <w n="44.4">ouvert</w> <w n="44.5">dans</w> <w n="44.6">la</w> <w n="44.7">campagne</w>. —</l>
					<l n="45" num="3.11"><w n="45.1">Si</w> <w n="45.2">Le</w> <w n="45.3">Brâz</w> <w n="45.4">eût</w> <w n="45.5">aimé</w> <w n="45.6">le</w> <w n="45.7">pré</w> <w n="45.8">de</w> <w n="45.9">Ker</w>-<w n="45.10">Végan</w>,</l>
					<l n="46" num="3.12"><w n="46.1">Les</w> <w n="46.2">taillis</w> <w n="46.3">d</w>’<w n="46.4">alentour</w>, <w n="46.5">le</w> <w n="46.6">Scorf</w> <w n="46.7">et</w> <w n="46.8">son</w> <w n="46.9">étang</w>,</l>
					<l n="47" num="3.13"><w n="47.1">Il</w> <w n="47.2">chanterait</w> <w n="47.3">encor</w> <w n="47.4">sur</w> <w n="47.5">le</w> <w n="47.6">Ros</w> ; <w n="47.7">ou</w> <w n="47.8">sa</w> <w n="47.9">mère</w>,</l>
					<l n="48" num="3.14"><w n="48.1">Mourant</w>, <w n="48.2">l</w>’<w n="48.3">aurait</w> <w n="48.4">soigné</w> <w n="48.5">comme</w>, <w n="48.6">depuis</w>, <w n="48.7">son</w> <w n="48.8">frère</w>.</l>
					<l n="49" num="3.15"><w n="49.1">Son</w> <w n="49.2">corps</w> <w n="49.3">reposerait</w> <w n="49.4">dans</w> <w n="49.5">le</w> <w n="49.6">bourg</w> <w n="49.7">de</w> <w n="49.8">Kéven</w>,</l>
					<l n="50" num="3.16"><w n="50.1">Près</w> <w n="50.2">du</w> <w n="50.3">mur</w> <w n="50.4">de</w> <w n="50.5">l</w>’<w n="50.6">église</w> <w n="50.7">et</w> <w n="50.8">sous</w> <w n="50.9">un</w> <w n="50.10">tertre</w> <w n="50.11">fin</w> ;</l>
					<l n="51" num="3.17"><w n="51.1">Ses</w> <w n="51.2">parents</w> <w n="51.3">y</w> <w n="51.4">viendraient</w> <w n="51.5">prier</w> <w n="51.6">avant</w> <w n="51.7">la</w> <w n="51.8">messe</w>,</l>
					<l n="52" num="3.18"><w n="52.1">Tous</w> <w n="52.2">les</w> <w n="52.3">petits</w> <w n="52.4">enfants</w> <w n="52.5">y</w> <w n="52.6">lutteraient</w> <w n="52.7">sans</w> <w n="52.8">cesse</w>.</l>
					<l n="53" num="3.19"><w n="53.1">Étendu</w> <w n="53.2">dans</w> <w n="53.3">sa</w> <w n="53.4">fosse</w>, <w n="53.5">il</w> <w n="53.6">entendrait</w> <w n="53.7">leur</w> <w n="53.8">bruit</w>,</l>
					<l n="54" num="3.20"><w n="54.1">Et</w> <w n="54.2">les</w> <w n="54.3">corriganed</w> <w n="54.4">y</w> <w n="54.5">danseraient</w> <w n="54.6">la</w> <w n="54.7">nuit</w>.</l>
				</lg>
				<lg n="4">
					<l n="55" num="4.1"><w n="55.1">Oh</w> ! <w n="55.2">Ne</w> <w n="55.3">quittez</w> <w n="55.4">jamais</w> <w n="55.5">le</w> <w n="55.6">seuil</w> <w n="55.7">de</w> <w n="55.8">votre</w> <w n="55.9">porte</w> !</l>
					<l n="56" num="4.2"><w n="56.1">Mourez</w> <w n="56.2">dans</w> <w n="56.3">la</w> <w n="56.4">maison</w> <w n="56.5">où</w> <w n="56.6">votre</w> <w n="56.7">mère</w> <w n="56.8">est</w> <w n="56.9">morte</w> !</l>
					<l n="57" num="4.3"><w n="57.1">Voilà</w> <w n="57.2">ce</w> <w n="57.3">qu</w>’<w n="57.4">à</w> <w n="57.5">Paris</w> <w n="57.6">avait</w> <w n="57.7">déjà</w> <w n="57.8">chanté</w></l>
					<l n="58" num="4.4"><w n="58.1">Un</w> <w n="58.2">poète</w> <w n="58.3">inconnu</w> <w n="58.4">qu</w>’<w n="58.5">on</w> <w n="58.6">n</w>’<w n="58.7">a</w> <w n="58.8">pas</w> <w n="58.9">écouté</w>.</l>
				</lg>
			</div></body></text></TEI>