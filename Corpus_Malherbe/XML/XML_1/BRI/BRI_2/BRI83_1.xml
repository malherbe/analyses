<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FLEUR D’OR</title>
				<title type="medium">Une édition électronique</title>
				<author key="BRI">
					<name>
						<forename>Auguste</forename>
						<surname>BRIZEUX</surname>
					</name>
					<date from="1803" to="1858">1803-1858</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2992 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BRI_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA FLEUR D’OR</title>
						<author>Auguste Brizeux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">https://fr.wikisource.org/wiki/La_Fleur_d%E2%80%99Or</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Œuvres de Auguste Brizeux — LA FLEUR D’OR</title>
								<author>Auguste Brizeux</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Alphonse Lemerre, éditeur</publisher>
									<date when="1981">1981</date>
								</imprint>
								<biblScope unit="tome">3</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1874">1874</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les deux préfaces ne sont pas reprises.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-02-05" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-02-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LIVRE QUATRIÈME</head><head type="sub_part">A FLORENCE</head><div type="poem" key="BRI83">
					<head type="main">L’Aleatico</head>
					<opener>
						<salute>À Ferdinand Rosellini</salute>
					</opener>
					<lg n="1">
						<l n="1" num="1.1"><space unit="char" quantity="12"></space><w n="1.1">LA</w> <w n="1.2">poésie</w> <w n="1.3">émane</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Émane</w> <w n="2.2">mollement</w> <w n="2.3">du</w> <w n="2.4">vase</w> <w n="2.5">de</w> <w n="2.6">mon</w> <w n="2.7">cœur</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Depuis</w> <w n="3.2">que</w> <w n="3.3">j</w>’<w n="3.4">y</w> <w n="3.5">versai</w> <w n="3.6">cette</w> <w n="3.7">heureuse</w> <w n="3.8">liqueur</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Douce</w> <w n="4.2">comme</w> <w n="4.3">le</w> <w n="4.4">ciel</w> <w n="4.5">de</w> <w n="4.6">la</w> <w n="4.7">blonde</w> <w n="4.8">Toscane</w>.</l>
						<l n="5" num="1.5"><w n="5.1">Eh</w> <w n="5.2">quoi</w> ! <w n="5.3">le</w> <w n="5.4">bon</w> <w n="5.5">Pétrarque</w> <w n="5.6">oublia</w> <w n="5.7">la</w> <w n="5.8">boisson</w></l>
						<l n="6" num="1.6"><w n="6.1">Où</w> <w n="6.2">le</w> <w n="6.3">barde</w> <w n="6.4">étranger</w> <w n="6.5">enivre</w> <w n="6.6">sa</w> <w n="6.7">chanson</w> !</l>
						<l n="7" num="1.7"><w n="7.1">Ah</w> ! <w n="7.2">ce</w> <w n="7.3">vin</w> <w n="7.4">réjouit</w> <w n="7.5">l</w>’<w n="7.6">esprit</w> <w n="7.7">sans</w> <w n="7.8">qu</w>’<w n="7.9">il</w> <w n="7.10">l</w>’<w n="7.11">offusque</w> !</l>
						<l n="8" num="1.8"><w n="8.1">Je</w> <w n="8.2">l</w>’<w n="8.3">appelle</w> <w n="8.4">un</w> <w n="8.5">nectar</w>, <w n="8.6">un</w> <w n="8.7">élixir</w> <w n="8.8">divin</w> :</l>
						<l n="9" num="1.9"><w n="9.1">Si</w> <w n="9.2">j</w>’<w n="9.3">étais</w> <w n="9.4">le</w> <w n="9.5">Grand</w>-<w n="9.6">Duc</w>, <w n="9.7">je</w> <w n="9.8">boirais</w> <w n="9.9">de</w> <w n="9.10">ce</w> <w n="9.11">vin</w></l>
						<l n="10" num="1.10"><space unit="char" quantity="12"></space><w n="10.1">Dans</w> <w n="10.2">un</w> <w n="10.3">beau</w> <w n="10.4">vase</w> <w n="10.5">étrusque</w>.</l>
					</lg>
					<lg n="2">
						<l n="11" num="2.1"><w n="11.1">Tu</w> <w n="11.2">vois</w> <w n="11.3">dans</w> <w n="11.4">ce</w> <w n="11.5">palais</w> <w n="11.6">ce</w> <w n="11.7">grand</w> <w n="11.8">arc</w> <w n="11.9">et</w> <w n="11.10">son</w> <w n="11.11">dard</w> :</l>
						<l n="12" num="2.2"><w n="12.1">Eh</w> <w n="12.2">bien</w>, <w n="12.3">Toscan</w> <w n="12.4">subtil</w>, <w n="12.5">je</w> <w n="12.6">l</w>’<w n="12.7">appelle</w> <w n="12.8">un</w> <w n="12.9">symbole</w>.</l>
						<l n="13" num="2.3">— <w n="13.1">Oui</w>, <w n="13.2">Barde</w>, <w n="13.3">saluons</w> <w n="13.4">ce</w> <w n="13.5">symbole</w> <w n="13.6">de</w> <w n="13.7">l</w>’<w n="13.8">art</w></l>
						<l n="14" num="2.4"><w n="14.1">Qui</w> <w n="14.2">nous</w> <w n="14.3">sert</w> <w n="14.4">à</w> <w n="14.5">lancer</w> <w n="14.6">la</w> <w n="14.7">divine</w> <w n="14.8">parole</w> :</l>
						<l n="15" num="2.5"><w n="15.1">Homère</w> <w n="15.2">l</w>’<w n="15.3">inventeur</w> <w n="15.4">au</w> <w n="15.5">poète</w> <w n="15.6">romain</w></l>
						<l n="16" num="2.6"><w n="16.1">Le</w> <w n="16.2">transmit</w> ; <w n="16.3">depuis</w> <w n="16.4">Dante</w> <w n="16.5">il</w> <w n="16.6">va</w> <w n="16.7">de</w> <w n="16.8">main</w> <w n="16.9">en</w> <w n="16.10">main</w>.</l>
						<l n="17" num="2.7"><w n="17.1">Dis</w> : <w n="17.2">ai</w>-<w n="17.3">je</w> <w n="17.4">pénétré</w> <w n="17.5">l</w>’<w n="17.6">ingénieux</w> <w n="17.7">emblème</w> ?</l>
						<l n="18" num="2.8">— <w n="18.1">Bien</w>, <w n="18.2">Toscan</w>. <w n="18.3">Cependant</w> <w n="18.4">l</w>’<w n="18.5">arc</w> <w n="18.6">a</w>-<w n="18.7">t</w>-<w n="18.8">il</w> <w n="18.9">voyagé</w> ?</l>
						<l n="19" num="2.9"><w n="19.1">Ou</w>, <w n="19.2">d</w>’<w n="19.3">Homère</w> <w n="19.4">à</w> <w n="19.5">Milton</w> (<w n="19.6">grand</w> <w n="19.7">et</w> <w n="19.8">nouveau</w> <w n="19.9">problème</w> !),</l>
						<l n="20" num="2.10"><w n="20.1">Tous</w> <w n="20.2">ont</w>-<w n="20.3">ils</w> <w n="20.4">changé</w> <w n="20.5">d</w>’<w n="20.6">arc</w> <w n="20.7">quand</w> <w n="20.8">le</w> <w n="20.9">but</w> <w n="20.10">a</w> <w n="20.11">changé</w> ?</l>
					</lg>
					<lg n="3">
						<l n="21" num="3.1"><space unit="char" quantity="12"></space><w n="21.1">Qu</w>’<w n="21.2">elle</w> <w n="21.3">est</w> <w n="21.4">prompte</w> <w n="21.5">et</w> <w n="21.6">subtile</w>,</l>
						<l n="22" num="3.2"><w n="22.1">La</w> <w n="22.2">flamme</w> <w n="22.3">de</w> <w n="22.4">l</w>’<w n="22.5">esprit</w> <w n="22.6">chez</w> <w n="22.7">vous</w>, <w n="22.8">peuple</w> <w n="22.9">toscan</w></l>
						<l n="23" num="3.3"><w n="23.1">Elle</w> <w n="23.2">éclate</w> <w n="23.3">soudain</w> <w n="23.4">comme</w> <w n="23.5">un</w> <w n="23.6">feu</w> <w n="23.7">de</w> <w n="23.8">volcan</w>,</l>
						<l n="24" num="3.4"><w n="24.1">Ou</w> <w n="24.2">jusqu</w>’<w n="24.3">au</w> <w n="24.4">fond</w> <w n="24.5">du</w> <w n="24.6">cœur</w> <w n="24.7">pénètre</w> <w n="24.8">comme</w> <w n="24.9">l</w>’<w n="24.10">huile</w>.</l>
						<l n="25" num="3.5"><w n="25.1">Instruisez</w> <w n="25.2">un</w> <w n="25.3">barbare</w> <w n="25.4">égaré</w> <w n="25.5">dans</w> <w n="25.6">vos</w> <w n="25.7">murs</w> !</l>
						<l n="26" num="3.6"><w n="26.1">Versez</w>-<w n="26.2">moi</w> <w n="26.3">de</w> <w n="26.4">ce</w> <w n="26.5">vin</w> <w n="26.6">fait</w> <w n="26.7">des</w> <w n="26.8">fruits</w> <w n="26.9">les</w> <w n="26.10">plus</w> <w n="26.11">mûrs</w> !</l>
						<l n="27" num="3.7"><w n="27.1">Il</w> <w n="27.2">vous</w> <w n="27.3">donne</w> <w n="27.4">la</w> <w n="27.5">force</w>, <w n="27.6">il</w> <w n="27.7">vous</w> <w n="27.8">donne</w> <w n="27.9">la</w> <w n="27.10">grâce</w>,</l>
						<l n="28" num="3.8"><w n="28.1">Des</w> <w n="28.2">Celtes</w> <w n="28.3">à</w> <w n="28.4">Florence</w> <w n="28.5">un</w> <w n="28.6">vestige</w> <w n="28.7">est</w> <w n="28.8">resté</w> :</l>
						<l n="29" num="3.9"><w n="29.1">Par</w> <w n="29.2">leur</w> <w n="29.3">grand</w> <w n="29.4">souvenir</w> <w n="29.5">à</w> <w n="29.6">ce</w> <w n="29.7">vin</w> <w n="29.8">exalté</w>,</l>
						<l n="30" num="3.10"><space unit="char" quantity="12"></space><w n="30.1">Je</w> <w n="30.2">veux</w> <w n="30.3">chanter</w> <w n="30.4">ma</w> <w n="30.5">race</w>.</l>
					</lg>
					<lg n="4">
						<l n="31" num="4.1"><w n="31.1">Le</w> <w n="31.2">char</w> <w n="31.3">celte</w>, <w n="31.4">le</w> <w n="31.5">char</w> <w n="31.6">tout</w> <w n="31.7">en</w> <w n="31.8">bois</w> <w n="31.9">de</w> <w n="31.10">bouleau</w>,</l>
						<l n="32" num="4.2"><w n="32.1">Je</w> <w n="32.2">l</w>’<w n="32.3">ai</w> <w n="32.4">vu</w> ! <w n="32.5">Le</w> <w n="32.6">timon</w>, <w n="32.7">le</w> <w n="32.8">cercle</w> <w n="32.9">de</w> <w n="32.10">la</w> <w n="32.11">roue</w></l>
						<l n="33" num="4.3"><w n="33.1">Avec</w> <w n="33.2">les</w> <w n="33.3">membres</w> <w n="33.4">durs</w> <w n="33.5">et</w> <w n="33.6">tors</w> <w n="33.7">d</w>’<w n="33.8">un</w> <w n="33.9">arbrisseau</w></l>
						<l n="34" num="4.4"><w n="34.1">Furent</w> <w n="34.2">construits</w> <w n="34.3">sans</w> <w n="34.4">bronze</w> <w n="34.5">ou</w> <w n="34.6">fer</w> ; <w n="34.7">rien</w> <w n="34.8">qui</w> <w n="34.9">les</w> <w n="34.10">noue</w>.</l>
						<l n="35" num="4.5"><w n="35.1">À</w> <w n="35.2">Florence</w>, <w n="35.3">au</w> <w n="35.4">milieu</w> <w n="35.5">des</w> <w n="35.6">arts</w> <w n="35.7">dans</w> <w n="35.8">leur</w> <w n="35.9">splendeur</w>,</l>
						<l n="36" num="4.6"><w n="36.1">Pour</w> <w n="36.2">un</w> <w n="36.3">enfant</w> <w n="36.4">de</w> <w n="36.5">l</w>’<w n="36.6">Ouest</w> <w n="36.7">ce</w> <w n="36.8">char</w> <w n="36.9">a</w> <w n="36.10">sa</w> <w n="36.11">grandeur</w>.</l>
						<l n="37" num="4.7"><w n="37.1">Où</w> <w n="37.2">sont</w> <w n="37.3">les</w> <w n="37.4">deux</w> <w n="37.5">coursiers</w>, <w n="37.6">les</w> <w n="37.7">coursiers</w> <w n="37.8">blancs</w> <w n="37.9">du</w> <w n="37.10">Celte</w> ?</l>
						<l n="38" num="4.8"><w n="38.1">Leurs</w> <w n="38.2">attaches</w> <w n="38.3">de</w> <w n="38.4">cuir</w> <w n="38.5">pendent</w> <w n="38.6">le</w> <w n="38.7">long</w> <w n="38.8">du</w> <w n="38.9">char</w> :</l>
						<l n="39" num="4.9"><w n="39.1">Lui</w>-<w n="39.2">même</w> <w n="39.3">où</w> <w n="39.4">donc</w> <w n="39.5">est</w>-<w n="39.6">il</w>, <w n="39.7">le</w> <w n="39.8">guerrier</w> <w n="39.9">jeune</w> <w n="39.10">et</w> <w n="39.11">svelte</w> ?</l>
						<l n="40" num="4.10"><w n="40.1">Qu</w>’<w n="40.2">il</w> <w n="40.3">vienne</w> <w n="40.4">l</w>’<w n="40.5">arc</w> <w n="40.6">en</w> <w n="40.7">main</w> <w n="40.8">et</w> <w n="40.9">lance</w> <w n="40.10">au</w> <w n="40.11">loin</w> <w n="40.12">son</w> <w n="40.13">dard</w> !</l>
					</lg>
					<lg n="5">
						<l n="41" num="5.1"><space unit="char" quantity="12"></space><w n="41.1">La</w> <w n="41.2">poésie</w> <w n="41.3">émane</w>.</l>
						<l n="42" num="5.2"><w n="42.1">Émane</w> <w n="42.2">mollement</w> <w n="42.3">du</w> <w n="42.4">vase</w> <w n="42.5">de</w> <w n="42.6">mon</w> <w n="42.7">cœur</w>,</l>
						<l n="43" num="5.3"><w n="43.1">Depuis</w> <w n="43.2">que</w> <w n="43.3">j</w>’<w n="43.4">y</w> <w n="43.5">versai</w> <w n="43.6">cette</w> <w n="43.7">heureuse</w> <w n="43.8">liqueur</w>.</l>
						<l n="44" num="5.4"><w n="44.1">Douce</w> <w n="44.2">comme</w> <w n="44.3">le</w> <w n="44.4">ciel</w> <w n="44.5">de</w> <w n="44.6">la</w> <w n="44.7">blonde</w> <w n="44.8">Toscane</w>.</l>
						<l n="45" num="5.5"><w n="45.1">Eh</w> <w n="45.2">quoi</w> ! <w n="45.3">le</w> <w n="45.4">bon</w> <w n="45.5">Pétrarque</w> <w n="45.6">oublia</w> <w n="45.7">la</w> <w n="45.8">boisson</w></l>
						<l n="46" num="5.6"><w n="46.1">Où</w> <w n="46.2">le</w> <w n="46.3">barde</w> <w n="46.4">étranger</w> <w n="46.5">enivre</w> <w n="46.6">sa</w> <w n="46.7">chanson</w> !</l>
						<l n="47" num="5.7"><w n="47.1">Ah</w> ! <w n="47.2">ce</w> <w n="47.3">vin</w> <w n="47.4">réjouit</w> <w n="47.5">l</w>’<w n="47.6">esprit</w> <w n="47.7">sans</w> <w n="47.8">qu</w>’<w n="47.9">il</w> <w n="47.10">l</w>’<w n="47.11">offusque</w> !</l>
						<l n="48" num="5.8"><w n="48.1">Je</w> <w n="48.2">l</w>’<w n="48.3">appelle</w> <w n="48.4">un</w> <w n="48.5">nectar</w>, <w n="48.6">un</w> <w n="48.7">élixir</w> <w n="48.8">divin</w> :</l>
						<l n="49" num="5.9"><w n="49.1">Si</w> <w n="49.2">j</w>’<w n="49.3">étais</w> <w n="49.4">le</w> <w n="49.5">Grand</w>-<w n="49.6">Duc</w>, <w n="49.7">je</w> <w n="49.8">boirais</w> <w n="49.9">de</w> <w n="49.10">ce</w> <w n="49.11">vin</w></l>
						<l n="50" num="5.10"><space unit="char" quantity="12"></space><w n="50.1">Dans</w> <w n="50.2">un</w> <w n="50.3">beau</w> <w n="50.4">vase</w> <w n="50.5">étrusque</w>.</l>
					</lg>
				</div></body></text></TEI>