<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FLEUR D’OR</title>
				<title type="medium">Une édition électronique</title>
				<author key="BRI">
					<name>
						<forename>Auguste</forename>
						<surname>BRIZEUX</surname>
					</name>
					<date from="1803" to="1858">1803-1858</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2992 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BRI_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA FLEUR D’OR</title>
						<author>Auguste Brizeux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">https://fr.wikisource.org/wiki/La_Fleur_d%E2%80%99Or</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Œuvres de Auguste Brizeux — LA FLEUR D’OR</title>
								<author>Auguste Brizeux</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Alphonse Lemerre, éditeur</publisher>
									<date when="1981">1981</date>
								</imprint>
								<biblScope unit="tome">3</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1874">1874</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les deux préfaces ne sont pas reprises.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-02-05" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-02-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LIVRE CINQUIÈME</head><head type="sub_part">A ROME</head><div type="poem" key="BRI92">
					<head type="main">Aux environs d’Albano</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">BOIS</w> <w n="1.2">où</w> <w n="1.3">se</w> <w n="1.4">sont</w> <w n="1.5">perdus</w> <w n="1.6">Merlin</w> <w n="1.7">et</w> <w n="1.8">Viviane</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Aviez</w>-<w n="2.2">vous</w> <w n="2.3">plus</w> <w n="2.4">d</w>’<w n="2.5">arôme</w> <w n="2.6">aux</w> <w n="2.7">coupes</w> <w n="2.8">de</w> <w n="2.9">vos</w> <w n="2.10">fleurs</w> ?</l>
						<l n="3" num="1.3"><w n="3.1">Plus</w> <w n="3.2">d</w>’<w n="3.3">émail</w> <w n="3.4">à</w> <w n="3.5">vos</w> <w n="3.6">pieds</w> ? <w n="3.7">Sur</w> <w n="3.8">la</w> <w n="3.9">verte</w> <w n="3.10">liane</w></l>
						<l n="4" num="1.4"><w n="4.1">Les</w> <w n="4.2">oiseaux</w> <w n="4.3">brillaient</w>-<w n="4.4">ils</w> <w n="4.5">de</w> <w n="4.6">plus</w> <w n="4.7">riches</w> <w n="4.8">couleurs</w> ?</l>
						<l n="5" num="1.5"><space unit="char" quantity="8"></space><w n="5.1">O</w> <w n="5.2">délices</w> <w n="5.3">du</w> <w n="5.4">lac</w> <w n="5.5">d</w>’<w n="5.6">Albane</w> !</l>
					</lg>
					<lg n="2">
						<l n="6" num="2.1"><w n="6.1">Calme</w> <w n="6.2">et</w> <w n="6.3">parfum</w> ! <w n="6.4">Et</w> <w n="6.5">puis</w> <w n="6.6">des</w> <w n="6.7">couples</w> <w n="6.8">gracieux</w></l>
						<l n="7" num="2.2"><w n="7.1">Qui</w> <w n="7.2">la</w> <w n="7.3">main</w> <w n="7.4">dans</w> <w n="7.5">la</w> <w n="7.6">main</w>, <w n="7.7">lentement</w>, <w n="7.8">sous</w> <w n="7.9">les</w> <w n="7.10">chênes</w>,</l>
						<l n="8" num="2.3"><w n="8.1">Vont</w> <w n="8.2">former</w> <w n="8.3">quelque</w> <w n="8.4">danse</w> <w n="8.5">aux</w> <w n="8.6">collines</w> <w n="8.7">prochaines</w>.</l>
						<l n="9" num="2.4"><w n="9.1">Je</w> <w n="9.2">passe</w> <w n="9.3">et</w> <w n="9.4">poliment</w> <w n="9.5">on</w> <w n="9.6">me</w> <w n="9.7">sourit</w> <w n="9.8">des</w> <w n="9.9">yeux</w>.</l>
						<l n="10" num="2.5"><w n="10.1">Ainsi</w>, <w n="10.2">vers</w> <w n="10.3">Comanâ</w>, <w n="10.4">cheminant</w> <w n="10.5">un</w> <w n="10.6">dimanche</w>,</l>
						<l n="11" num="2.6"><w n="11.1">De</w> <w n="11.2">chaque</w> <w n="11.3">feutre</w> <w n="11.4">noir</w>, <w n="11.5">de</w> <w n="11.6">chaque</w> <w n="11.7">coiffe</w> <w n="11.8">blanche</w>,</l>
						<l n="12" num="2.7"><w n="12.1">M</w>-<w n="12.2">arrivait</w> <w n="12.3">un</w> <w n="12.4">bonjour</w>, <w n="12.5">mais</w> <w n="12.6">grave</w> <w n="12.7">et</w> <w n="12.8">sérieux</w>.</l>
					</lg>
					<lg n="3">
						<l n="13" num="3.1"><w n="13.1">Ébloui</w> <w n="13.2">des</w> <w n="13.3">splendeurs</w> <w n="13.4">de</w> <w n="13.5">cette</w> <w n="13.6">terre</w> <w n="13.7">épique</w>,</l>
						<l n="14" num="3.2"><w n="14.1">À</w> <w n="14.2">midi</w> <w n="14.3">je</w> <w n="14.4">m</w>’<w n="14.5">assieds</w> <w n="14.6">sous</w> <w n="14.7">la</w> <w n="14.8">chaleur</w> <w n="14.9">du</w> <w n="14.10">jour</w> ;</l>
						<l n="15" num="3.3"><w n="15.1">Je</w> <w n="15.2">tourne</w> <w n="15.3">encor</w> <w n="15.4">les</w> <w n="15.5">yeux</w> <w n="15.6">vers</w> <w n="15.7">la</w> <w n="15.8">Sabine</w> <w n="15.9">antique</w>,</l>
						<l n="16" num="3.4"><w n="16.1">Puis</w>, <w n="16.2">vaincu</w>, <w n="16.3">je</w> <w n="16.4">les</w> <w n="16.5">ferme</w> <w n="16.6">au</w> <w n="16.7">bruit</w> <w n="16.8">d</w>’<w n="16.9">un</w> <w n="16.10">chant</w> <w n="16.11">rustique</w> :</l>
						<l n="17" num="3.5"><w n="17.1">Un</w> <w n="17.2">jour</w> <w n="17.3">Milton</w> <w n="17.4">dormant</w> <w n="17.5">eut</w> <w n="17.6">un</w> <w n="17.7">baiser</w> <w n="17.8">d</w>’<w n="17.9">amour</w>.</l>
						<l n="18" num="3.6"><w n="18.1">Pour</w> <w n="18.2">les</w> <w n="18.3">dames</w> <w n="18.4">allant</w> <w n="18.5">vers</w> <w n="18.6">leur</w> <w n="18.7">villa</w> <w n="18.8">de</w> <w n="18.9">marbre</w></l>
						<l n="19" num="3.7"><w n="19.1">Sans</w> <w n="19.2">gloire</w> <w n="19.3">et</w> <w n="19.4">sans</w> <w n="19.5">lauriers</w> <w n="19.6">mon</w> <w n="19.7">front</w> <w n="19.8">ne</w> <w n="19.9">brille</w> <w n="19.10">pas</w> ;</l>
						<l n="20" num="3.8"><w n="20.1">Mais</w>, <w n="20.2">filles</w> <w n="20.3">d</w>’<w n="20.4">Albano</w>, <w n="20.5">sous</w> <w n="20.6">l</w>’<w n="20.7">ombre</w> <w n="20.8">de</w> <w n="20.9">cet</w> <w n="20.10">arbre</w></l>
						<l n="21" num="3.9"><space unit="char" quantity="8"></space><w n="21.1">Arrêtez</w>, <w n="21.2">arrêtez</w> <w n="21.3">vos</w> <w n="21.4">pas</w> !</l>
						<l n="22" num="3.10"><w n="22.1">Les</w> <w n="22.2">lèvres</w> <w n="22.3">qui</w> <w n="22.4">chantaient</w> <w n="22.5">ma</w> <w n="22.6">jeune</w> <w n="22.7">paysanne</w></l>
						<l n="23" num="3.11"><space unit="char" quantity="4"></space><w n="23.1">S</w>’<w n="23.2">ouvrent</w> <w n="23.3">encore</w> <w n="23.4">et</w> <w n="23.5">mon</w> <w n="23.6">cœur</w> <w n="23.7">en</w> <w n="23.8">émane</w>.</l>
					</lg>
				</div></body></text></TEI>