<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FLEUR D’OR</title>
				<title type="medium">Une édition électronique</title>
				<author key="BRI">
					<name>
						<forename>Auguste</forename>
						<surname>BRIZEUX</surname>
					</name>
					<date from="1803" to="1858">1803-1858</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2992 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BRI_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA FLEUR D’OR</title>
						<author>Auguste Brizeux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">https://fr.wikisource.org/wiki/La_Fleur_d%E2%80%99Or</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Œuvres de Auguste Brizeux — LA FLEUR D’OR</title>
								<author>Auguste Brizeux</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Alphonse Lemerre, éditeur</publisher>
									<date when="1981">1981</date>
								</imprint>
								<biblScope unit="tome">3</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1874">1874</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les deux préfaces ne sont pas reprises.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-02-05" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-02-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LIVRE CINQUIÈME</head><head type="sub_part">A ROME</head><div type="poem" key="BRI94">
					<head type="main">Pour <lb></lb>l’Académie de France à Rome</head>
					<head type="sub_1">INSCRIPTION</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">ICI</w> <w n="1.2">la</w> <w n="1.3">ruche</w> <w n="1.4">d</w>’<w n="1.5">or</w> <w n="1.6">des</w> <w n="1.7">abeilles</w> <w n="1.8">de</w> <w n="1.9">l</w>’<w n="1.10">Art</w>.</l>
						<l n="2" num="1.2"><w n="2.1">Chaque</w> <w n="2.2">automne</w>, <w n="2.3">fuyant</w> <w n="2.4">la</w> <w n="2.5">Seine</w> <w n="2.6">et</w> <w n="2.7">son</w> <w n="2.8">brouillard</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Fervents</w> <w n="3.2">nous</w> <w n="3.3">arrivons</w> <w n="3.4">sous</w> <w n="3.5">l</w>’<w n="3.6">abri</w> <w n="3.7">des</w> <w n="3.8">grands</w> <w n="3.9">arbres</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Près</w> <w n="4.2">du</w> <w n="4.3">buis</w> <w n="4.4">odorant</w> <w n="4.5">qui</w> <w n="4.6">pousse</w> <w n="4.7">entre</w> <w n="4.8">les</w> <w n="4.9">marbres</w>,</l>
						<l n="5" num="1.5"><space unit="char" quantity="8"></space><w n="5.1">Dans</w> <w n="5.2">la</w> <w n="5.3">chaleur</w> <w n="5.4">et</w> <w n="5.5">la</w> <w n="5.6">clarté</w>.</l>
						<l n="6" num="1.6"><w n="6.1">Nous</w> <w n="6.2">venons</w> <w n="6.3">recueillir</w> <w n="6.4">le</w> <w n="6.5">miel</w> <w n="6.6">de</w> <w n="6.7">la</w> <w n="6.8">Beauté</w> :</l>
						<l n="7" num="1.7"><w n="7.1">Car</w> <w n="7.2">la</w> <w n="7.3">divine</w> <w n="7.4">fleur</w> <w n="7.5">qu</w>’<w n="7.6">Homère</w> <w n="7.7">fit</w> <w n="7.8">éclore</w>,</l>
						<l n="8" num="1.8"><w n="8.1">La</w> <w n="8.2">fleur</w> <w n="8.3">de</w> <w n="8.4">poésie</w> <w n="8.5">a</w> <w n="8.6">son</w> <w n="8.7">arôme</w> <w n="8.8">encore</w>.</l>
						<l n="9" num="1.9"><w n="9.1">Et</w> <w n="9.2">de</w> <w n="9.3">la</w> <w n="9.4">ruche</w> <w n="9.5">d</w>’<w n="9.6">or</w> <w n="9.7">quand</w> <w n="9.8">te</w> <w n="9.9">vient</w> <w n="9.10">la</w> <w n="9.11">moisson</w>,</l>
						<l n="10" num="1.10"><w n="10.1">Tu</w> <w n="10.2">savoures</w>, <w n="10.3">Paris</w>, <w n="10.4">sous</w> <w n="10.5">ton</w> <w n="10.6">pâle</w> <w n="10.7">horizon</w>,</l>
						<l n="11" num="1.11"><w n="11.1">Oubliant</w>, <w n="11.2">ce</w> <w n="11.3">jour</w>-<w n="11.4">là</w>, <w n="11.5">tes</w> <w n="11.6">bruits</w> <w n="11.7">et</w> <w n="11.8">tes</w> <w n="11.9">secousses</w>,</l>
						<l n="12" num="1.12"><w n="12.1">Le</w> <w n="12.2">miel</w> <w n="12.3">savant</w> <w n="12.4">de</w> <w n="12.5">l</w>’<w n="12.6">Art</w> <w n="12.7">qui</w> <w n="12.8">fait</w> <w n="12.9">les</w> <w n="12.10">mœurs</w> <w n="12.11">plus</w> <w n="12.12">douces</w>.</l>
					</lg>
				</div></body></text></TEI>