<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FLEUR D’OR</title>
				<title type="medium">Une édition électronique</title>
				<author key="BRI">
					<name>
						<forename>Auguste</forename>
						<surname>BRIZEUX</surname>
					</name>
					<date from="1803" to="1858">1803-1858</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2992 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BRI_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA FLEUR D’OR</title>
						<author>Auguste Brizeux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">https://fr.wikisource.org/wiki/La_Fleur_d%E2%80%99Or</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Œuvres de Auguste Brizeux — LA FLEUR D’OR</title>
								<author>Auguste Brizeux</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Alphonse Lemerre, éditeur</publisher>
									<date when="1981">1981</date>
								</imprint>
								<biblScope unit="tome">3</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1874">1874</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les deux préfaces ne sont pas reprises.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-02-05" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-02-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LIVRE PREMIER</head><head type="sub_part">EN BRETAGNE</head><div type="poem" key="BRI57">
					<head type="main">Les deux Routes</head>
					<div n="1" type="section">
						<head type="number">I</head>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">DEUX</w> <w n="1.2">routes</w> <w n="1.3">vers</w> <w n="1.4">le</w> <w n="1.5">Bien</w> <w n="1.6">mènent</w> <w n="1.7">d</w>’<w n="1.8">un</w> <w n="1.9">pas</w> <w n="1.10">égal</w>,</l>
							<l n="2" num="1.2"><w n="2.1">L</w>’<w n="2.2">amour</w> <w n="2.3">du</w> <w n="2.4">Bien</w> <w n="2.5">lui</w>-<w n="2.6">même</w> <w n="2.7">et</w> <w n="2.8">la</w> <w n="2.9">haine</w> <w n="2.10">du</w> <w n="2.11">Mal</w>,</l>
							<l n="3" num="1.3"><w n="3.1">Et</w> <w n="3.2">chaque</w> <w n="3.3">homme</w>, <w n="3.4">selon</w> <w n="3.5">que</w> <w n="3.6">son</w> <w n="3.7">penchant</w> <w n="3.8">l</w>’<w n="3.9">entraîne</w>,</l>
							<l n="4" num="1.4"><w n="4.1">Suit</w> <w n="4.2">vers</w> <w n="4.3">le</w> <w n="4.4">but</w> <w n="4.5">commun</w> <w n="4.6">ou</w> <w n="4.7">l</w>’<w n="4.8">amour</w> <w n="4.9">ou</w> <w n="4.10">la</w> <w n="4.11">haine</w> ;</l>
							<l n="5" num="1.5"><w n="5.1">La</w> <w n="5.2">haine</w> <w n="5.3">est</w> <w n="5.4">d</w>’<w n="5.5">un</w> <w n="5.6">cœur</w> <w n="5.7">fier</w> <w n="5.8">et</w> <w n="5.9">d</w>’<w n="5.10">un</w> <w n="5.11">sens</w> <w n="5.12">affermi</w></l>
							<l n="6" num="1.6"><w n="6.1">Que</w> <w n="6.2">le</w> <w n="6.3">péril</w> <w n="6.4">excite</w> <w n="6.5">et</w> <w n="6.6">pousse</w> <w n="6.7">à</w> <w n="6.8">l</w>’<w n="6.9">ennemi</w> ;</l>
							<l n="7" num="1.7"><w n="7.1">L</w>’<w n="7.2">amour</w>, <w n="7.3">d</w>’<w n="7.4">un</w> <w n="7.5">cœur</w> <w n="7.6">pensif</w>, <w n="7.7">intelligent</w> <w n="7.8">et</w> <w n="7.9">tendre</w></l>
							<l n="8" num="1.8"><w n="8.1">Qui</w>, <w n="8.2">plaignant</w> <w n="8.3">les</w> <w n="8.4">pervers</w>, <w n="8.5">voudrait</w> <w n="8.6">s</w>’<w n="8.7">en</w> <w n="8.8">faire</w> <w n="8.9">entendre</w></l>
							<l n="9" num="1.9"><w n="9.1">Amour</w>, <w n="9.2">haine</w>, <w n="9.3">lequel</w> <w n="9.4">de</w> <w n="9.5">ce</w> <w n="9.6">double</w> <w n="9.7">sentier</w></l>
							<l n="10" num="1.10"><w n="10.1">Choisir</w> ? <w n="10.2">tous</w> <w n="10.3">deux</w> <w n="10.4">sont</w> <w n="10.5">sûrs</w> : <w n="10.6">j</w>’<w n="10.7">ai</w> <w n="10.8">suivi</w> <w n="10.9">le</w> <w n="10.10">premier</w>.</l>
						</lg>
					</div>
					<div n="2" type="section">
						<head type="number">II</head>
						<lg n="1">
							<l n="11" num="1.1"><w n="11.1">Si</w> <w n="11.2">le</w> <w n="11.3">Mal</w> <w n="11.4">devant</w> <w n="11.5">moi</w> <w n="11.6">passe</w> <w n="11.7">comme</w> <w n="11.8">invisible</w>,</l>
							<l n="12" num="1.2"><w n="12.1">Je</w> <w n="12.2">ne</w> <w n="12.3">suis</w> <w n="12.4">point</w> <w n="12.5">aveugle</w> <w n="12.6">et</w> <w n="12.7">surtout</w> <w n="12.8">insensible</w> ;</l>
							<l n="13" num="1.3"><w n="13.1">Plus</w> <w n="13.2">d</w>’<w n="13.3">une</w> <w n="13.4">fois</w> <w n="13.5">mon</w> <w n="13.6">œil</w> <w n="13.7">s</w>’<w n="13.8">ouvrit</w> <w n="13.9">épouvanté</w>,</l>
							<l n="14" num="1.4"><w n="14.1">Et</w> <w n="14.2">mon</w> <w n="14.3">cœur</w> <w n="14.4">sait</w> <w n="14.5">des</w> <w n="14.6">coups</w> <w n="14.7">qui</w> <w n="14.8">l</w>’<w n="14.9">ont</w> <w n="14.10">ensanglanté</w>.</l>
							<l n="15" num="1.5"><w n="15.1">Mais</w> <w n="15.2">pourquoi</w> <w n="15.3">ramener</w> <w n="15.4">la</w> <w n="15.5">chose</w> <w n="15.6">inexplicable</w> ?</l>
							<l n="16" num="1.6"><w n="16.1">L</w>’<w n="16.2">homme</w> <w n="16.3">doit</w> <w n="16.4">mépriser</w> <w n="16.5">le</w> <w n="16.6">fardeau</w> <w n="16.7">qui</w> <w n="16.8">l</w>’<w n="16.9">accable</w>.</l>
							<l n="17" num="1.7"><w n="17.1">Chaque</w> <w n="17.2">jour</w> <w n="17.3">dans</w> <w n="17.4">la</w> <w n="17.5">route</w> <w n="17.6">il</w> <w n="17.7">marche</w> <w n="17.8">en</w> <w n="17.9">s</w>’<w n="17.10">allégeant</w>,</l>
							<l n="18" num="1.8"><w n="18.1">Jusqu</w>’<w n="18.2">à</w> <w n="18.3">l</w>’<w n="18.4">heure</w> <w n="18.5">où</w>, <w n="18.6">plus</w> <w n="18.7">tendre</w> <w n="18.8">et</w> <w n="18.9">plus</w> <w n="18.10">intelligent</w>,</l>
							<l n="19" num="1.9"><w n="19.1">Meilleur</w>, <w n="19.2">il</w> <w n="19.3">rentrera</w> <w n="19.4">dans</w> <w n="19.5">ce</w> <w n="19.6">monde</w> <w n="19.7">harmonique</w></l>
							<l n="20" num="1.10"><w n="20.1">Que</w> <w n="20.2">chante</w> <w n="20.3">incessamment</w> <w n="20.4">mon</w> <w n="20.5">âme</w> <w n="20.6">synthétique</w>.</l>
						</lg>
					</div>
					<div n="3" type="section">
						<head type="number">III</head>
						<lg n="1">
							<l n="21" num="1.1"><w n="21.1">Il</w> <w n="21.2">vit</w> <w n="21.3">pourtant</w>, <w n="21.4">il</w> <w n="21.5">vit</w>, <w n="21.6">celui</w> <w n="21.7">qui</w> <w n="21.8">doit</w> <w n="21.9">mourir</w>,</l>
							<l n="22" num="1.2"><w n="22.1">Plus</w> <w n="22.2">fort</w>, <w n="22.3">on</w> <w n="22.4">le</w> <w n="22.5">dirait</w>, <w n="22.6">plus</w> <w n="22.7">il</w> <w n="22.8">nous</w> <w n="22.9">voit</w> <w n="22.10">souffrir</w>,</l>
							<l n="23" num="1.3"><w n="23.1">Et</w> <w n="23.2">bien</w> <w n="23.3">des</w> <w n="23.4">malheureux</w>, <w n="23.5">sans</w> <w n="23.6">puissance</w> <w n="23.7">en</w> <w n="23.8">eux</w>-<w n="23.9">mêmes</w>,</l>
							<l n="24" num="1.4"><w n="24.1">Sous</w> <w n="24.2">ses</w> <w n="24.3">hideuses</w> <w n="24.4">mains</w> <w n="24.5">se</w> <w n="24.6">renversent</w> <w n="24.7">tout</w> <w n="24.8">blêmes</w>.</l>
							<l n="25" num="1.5"><w n="25.1">C</w>’<w n="25.2">est</w> <w n="25.3">de</w> <w n="25.4">lutter</w> <w n="25.5">aussi</w> ! <w n="25.6">Comme</w> <w n="25.7">les</w> <w n="25.8">premiers</w> <w n="25.9">saints</w></l>
							<l n="26" num="1.6"><w n="26.1">Qui</w> <w n="26.2">soumettaient</w> <w n="26.3">le</w> <w n="26.4">diable</w> <w n="26.5">à</w> <w n="26.6">leurs</w> <w n="26.7">pieux</w> <w n="26.8">desseins</w>,</l>
							<l n="27" num="1.7"><w n="27.1">Et</w> <w n="27.2">le</w> <w n="27.3">menaient</w> <w n="27.4">en</w> <w n="27.5">laisse</w>, <w n="27.6">un</w> <w n="27.7">signe</w> <w n="27.8">sur</w> <w n="27.9">la</w> <w n="27.10">tête</w>,</l>
							<l n="28" num="1.8"><w n="28.1">C</w>’<w n="28.2">est</w>, <w n="28.3">en</w> <w n="28.4">invoquant</w> <w n="28.5">Dieu</w>, <w n="28.6">de</w> <w n="28.7">combattre</w> <w n="28.8">la</w> <w n="28.9">Bête</w>,</l>
							<l n="29" num="1.9"><w n="29.1">En</w> <w n="29.2">lui</w> <w n="29.3">criant</w> : « <w n="29.4">Obstacle</w>, <w n="29.5">oh</w> ! <w n="29.6">tu</w> <w n="29.7">t</w>’<w n="29.8">abaisseras</w> !</l>
							<l n="30" num="1.10"><w n="30.1">Pour</w> <w n="30.2">produire</w> <w n="30.3">le</w> <w n="30.4">Bien</w>, <w n="30.5">Mal</w>, <w n="30.6">tu</w> <w n="30.7">m</w>’<w n="30.8">obéiras</w> ! »</l>
						</lg>
					</div>
				</div></body></text></TEI>