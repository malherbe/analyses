<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FLEUR D’OR</title>
				<title type="medium">Une édition électronique</title>
				<author key="BRI">
					<name>
						<forename>Auguste</forename>
						<surname>BRIZEUX</surname>
					</name>
					<date from="1803" to="1858">1803-1858</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2992 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BRI_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA FLEUR D’OR</title>
						<author>Auguste Brizeux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">https://fr.wikisource.org/wiki/La_Fleur_d%E2%80%99Or</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Œuvres de Auguste Brizeux — LA FLEUR D’OR</title>
								<author>Auguste Brizeux</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Alphonse Lemerre, éditeur</publisher>
									<date when="1981">1981</date>
								</imprint>
								<biblScope unit="tome">3</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1874">1874</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les deux préfaces ne sont pas reprises.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-02-05" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-02-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LIVRE NEUVIÈME</head><head type="sub_part">EN BRETAGNE</head><div type="poem" key="BRI150">
					<head type="main">L’Idéal</head>
					<opener>
						<salute>À mon frère Edmond, en Amérique</salute>
					</opener>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">TOUS</w> <w n="1.2">le</w> <w n="1.3">voyaient</w> <w n="1.4">en</w> <w n="1.5">rêve</w> <w n="1.6">aux</w> <w n="1.7">terres</w> <w n="1.8">atlantiques</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Et</w>, <w n="2.2">malgré</w> <w n="2.3">les</w> <w n="2.4">boas</w> <w n="2.5">et</w> <w n="2.6">les</w> <w n="2.7">serpents</w> <w n="2.8">ailés</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Chercheurs</w> <w n="3.2">d</w>’<w n="3.3">El</w>-<w n="3.4">Dorado</w>, <w n="3.5">les</w> <w n="3.6">voilà</w> <w n="3.7">tous</w> <w n="3.8">allés</w></l>
						<l n="4" num="1.4"><space unit="char" quantity="8"></space><w n="4.1">Au</w> <w n="4.2">pays</w> <w n="4.3">lointain</w> <w n="4.4">des</w> <w n="4.5">Caciques</w>.</l>
						<l n="5" num="1.5"><w n="5.1">Là</w>, <w n="5.2">sur</w> <w n="5.3">un</w> <w n="5.4">lit</w> <w n="5.5">d</w>’<w n="5.6">onyx</w> <w n="5.7">et</w> <w n="5.8">de</w> <w n="5.9">saphirs</w>, <w n="5.10">il</w> <w n="5.11">dort</w>,</l>
						<l n="6" num="1.6"><w n="6.1">Le</w> <w n="6.2">vieillard</w> <w n="6.3">idéal</w> <w n="6.4">couvert</w> <w n="6.5">de</w> <w n="6.6">poudre</w> <w n="6.7">d</w>’<w n="6.8">or</w> !</l>
						<l n="7" num="1.7"><space unit="char" quantity="8"></space><w n="7.1">Au</w> <w n="7.2">pays</w> <w n="7.3">lointain</w> <w n="7.4">des</w> <w n="7.5">Caciques</w></l>
						<l n="8" num="1.8"><w n="8.1">Heureux</w>, <w n="8.2">nouveaux</w> <w n="8.3">Jasons</w>, <w n="8.4">ceux</w>-<w n="8.5">là</w> <w n="8.6">qui</w> <w n="8.7">sont</w> <w n="8.8">allés</w> !</l>
						<l n="9" num="1.9"><w n="9.1">Qu</w>’<w n="9.2">importent</w> <w n="9.3">les</w> <w n="9.4">boas</w> <w n="9.5">et</w> <w n="9.6">les</w> <w n="9.7">serpents</w> <w n="9.8">ailés</w>,</l>
						<l n="10" num="1.10"><w n="10.1">Si</w> <w n="10.2">l</w>’<w n="10.3">on</w> <w n="10.4">suit</w> <w n="10.5">son</w> <w n="10.6">beau</w> <w n="10.7">rêve</w> <w n="10.8">aux</w> <w n="10.9">terres</w> <w n="10.10">atlantiques</w> ! —</l>
					</lg>
					<lg n="2">
						<l n="11" num="2.1"><w n="11.1">Fantôme</w> <w n="11.2">du</w> <w n="11.3">bonheur</w>, <w n="11.4">son</w> <w n="11.5">ombre</w>, <w n="11.6">son</w> <w n="11.7">reflet</w>,</l>
						<l n="12" num="2.2"><space unit="char" quantity="8"></space><w n="12.1">Que</w> <w n="12.2">vous</w> <w n="12.3">attirez</w> <w n="12.4">l</w>’<w n="12.5">âme</w> <w n="12.6">humaine</w> !</l>
						<l n="13" num="2.3"><w n="13.1">Ah</w> ! <w n="13.2">s</w>’<w n="13.3">il</w> <w n="13.4">est</w> <w n="13.5">un</w> <w n="13.6">bonheur</w> <w n="13.7">pur</w>, <w n="13.8">durable</w>, <w n="13.9">complet</w>,</l>
						<l n="14" num="2.4"><w n="14.1">Anges</w>, <w n="14.2">emportez</w>-<w n="14.3">nous</w> <w n="14.4">vers</w> <w n="14.5">son</w> <w n="14.6">riche</w> <w n="14.7">domaine</w> !</l>
						<l n="15" num="2.5"><w n="15.1">Dieu</w> <w n="15.2">sur</w> <w n="15.3">tout</w> <w n="15.4">l</w>’<w n="15.5">univers</w> <w n="15.6">refléta</w> <w n="15.7">sa</w> <w n="15.8">beauté</w>.</l>
						<l n="16" num="2.6"><w n="16.1">Notre</w> <w n="16.2">âme</w> <w n="16.3">par</w> <w n="16.4">instinct</w> <w n="16.5">cherche</w> <w n="16.6">la</w> <w n="16.7">belle</w> <w n="16.8">image</w>,</l>
						<l n="17" num="2.7"><w n="17.1">Et</w>, <w n="17.2">croyant</w> <w n="17.3">la</w> <w n="17.4">saisir</w>, <w n="17.5">frémit</w> <w n="17.6">de</w> <w n="17.7">volupté</w> ;</l>
						<l n="18" num="2.8"><w n="18.1">O</w> <w n="18.2">mers</w>, <w n="18.3">cieux</w> <w n="18.4">étoilés</w>, <w n="18.5">vallons</w> <w n="18.6">pleins</w> <w n="18.7">de</w> <w n="18.8">ramage</w>,</l>
						<l n="19" num="2.9"><w n="19.1">Où</w> <w n="19.2">l</w>’<w n="19.3">homme</w> <w n="19.4">bien</w> <w n="19.5">souvent</w> <w n="19.6">poursuit</w> <w n="19.7">son</w> <w n="19.8">idéal</w>,</l>
						<l n="20" num="2.10"><w n="20.1">Jusqu</w>’<w n="20.2">au</w> <w n="20.3">divin</w> <w n="20.4">auteur</w> <w n="20.5">transmettez</w> <w n="20.6">cet</w> <w n="20.7">hommage</w> !</l>
						<l n="21" num="2.11"><w n="21.1">Heureux</w> <w n="21.2">les</w> <w n="21.3">cœurs</w> <w n="21.4">saisis</w> <w n="21.5">d</w>’<w n="21.6">un</w> <w n="21.7">amour</w> <w n="21.8">virginal</w>,</l>
						<l n="22" num="2.12"><w n="22.1">L</w>’<w n="22.2">un</w> <w n="22.3">dans</w> <w n="22.4">l</w>’<w n="22.5">autre</w> <w n="22.6">absorbés</w> <w n="22.7">comme</w> <w n="22.8">en</w> <w n="22.9">leur</w> <w n="22.10">bien</w> <w n="22.11">suprême</w> :</l>
						<l n="23" num="2.13">« <w n="23.1">Enfin</w>, <w n="23.2">murmurent</w>-<w n="23.3">ils</w>, <w n="23.4">j</w>’<w n="23.5">ai</w> <w n="23.6">l</w>’<w n="23.7">être</w> <w n="23.8">sans</w> <w n="23.9">égal</w> ! »</l>
						<l n="24" num="2.14"><w n="24.1">C</w>’<w n="24.2">est</w> <w n="24.3">que</w> <w n="24.4">l</w>’<w n="24.5">objet</w> <w n="24.6">aimé</w> <w n="24.7">nous</w> <w n="24.8">semble</w> <w n="24.9">Dieu</w> <w n="24.10">lui</w>-<w n="24.11">même</w>. —</l>
					</lg>
					<lg n="3">
						<l n="25" num="3.1"><w n="25.1">Fantôme</w> <w n="25.2">de</w> <w n="25.3">l</w>’<w n="25.4">amour</w>, <w n="25.5">son</w> <w n="25.6">ombre</w>, <w n="25.7">son</w> <w n="25.8">reflet</w>,</l>
						<l n="26" num="3.2"><space unit="char" quantity="8"></space><w n="26.1">Que</w> <w n="26.2">vous</w> <w n="26.3">entraînez</w> <w n="26.4">l</w>’<w n="26.5">âme</w> <w n="26.6">humaine</w> !</l>
						<l n="27" num="3.3"><w n="27.1">Anges</w>, <w n="27.2">emportez</w>-<w n="27.3">nous</w> <w n="27.4">vers</w> <w n="27.5">le</w> <w n="27.6">brûlant</w> <w n="27.7">domaine</w></l>
						<l n="28" num="3.4"><w n="28.1">Où</w> <w n="28.2">rayonne</w> <w n="28.3">l</w>’<w n="28.4">amour</w> <w n="28.5">pur</w>, <w n="28.6">durable</w>, <w n="28.7">complet</w> !</l>
					</lg>
					<lg n="4">
						<l n="29" num="4.1"><space unit="char" quantity="8"></space><w n="29.1">Des</w> <w n="29.2">âmes</w> <w n="29.3">ont</w> <w n="29.4">trouve</w> <w n="29.5">des</w> <w n="29.6">ailes</w></l>
						<l n="30" num="4.2"><w n="30.1">Pour</w> <w n="30.2">voler</w> <w n="30.3">avant</w> <w n="30.4">l</w>’<w n="30.5">heure</w> <w n="30.6">aux</w> <w n="30.7">choses</w> <w n="30.8">éternelles</w>.</l>
						<l n="31" num="4.3"><w n="31.1">Elles</w> <w n="31.2">ont</w> <w n="31.3">vu</w>, — <w n="31.4">l</w>’<w n="31.5">Amour</w>, <w n="31.6">dissipant</w> <w n="31.7">tout</w> <w n="31.8">brouillard</w>,</l>
						<l n="32" num="4.4"><w n="32.1">Fervent</w>, <w n="32.2">leur</w> <w n="32.3">déroulait</w> <w n="32.4">ses</w> <w n="32.5">plaines</w> <w n="32.6">infinies</w>, —</l>
						<l n="33" num="4.5"><w n="33.1">Enfin</w> <w n="33.2">elles</w> <w n="33.3">ont</w> <w n="33.4">vu</w> <w n="33.5">le</w> <w n="33.6">mystique</w> <w n="33.7">vieillard</w> !</l>
						<l n="34" num="4.6"><w n="34.1">O</w> <w n="34.2">saint</w> <w n="34.3">El</w>-<w n="34.4">Dorado</w>, <w n="34.5">roi</w> <w n="34.6">des</w> <w n="34.7">sphères</w> <w n="34.8">bénies</w>,</l>
						<l n="35" num="4.7"><w n="35.1">Après</w> <w n="35.2">ta</w> <w n="35.3">grande</w> <w n="35.4">voix</w> <w n="35.5">que</w> <w n="35.6">sont</w> <w n="35.7">nos</w> <w n="35.8">harmonies</w> ?</l>
						<l n="36" num="4.8"><w n="36.1">Nos</w> <w n="36.2">rubis</w> <w n="36.3">sont</w> <w n="36.4">les</w> <w n="36.5">feux</w> <w n="36.6">de</w> <w n="36.7">ton</w> <w n="36.8">ardent</w> <w n="36.9">regard</w>.</l>
						<l n="37" num="4.9"><w n="37.1">Pour</w> <w n="37.2">voler</w> <w n="37.3">avant</w> <w n="37.4">l</w>’<w n="37.5">heure</w> <w n="37.6">aux</w> <w n="37.7">choses</w> <w n="37.8">éternelles</w>,</l>
						<l n="38" num="4.10"><space unit="char" quantity="8"></space><w n="38.1">Des</w> <w n="38.2">âmes</w> <w n="38.3">ont</w> <w n="38.4">trouvé</w> <w n="38.5">des</w> <w n="38.6">ailes</w>.</l>
					</lg>
				</div></body></text></TEI>