<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FLEUR D’OR</title>
				<title type="medium">Une édition électronique</title>
				<author key="BRI">
					<name>
						<forename>Auguste</forename>
						<surname>BRIZEUX</surname>
					</name>
					<date from="1803" to="1858">1803-1858</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2992 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BRI_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA FLEUR D’OR</title>
						<author>Auguste Brizeux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">https://fr.wikisource.org/wiki/La_Fleur_d%E2%80%99Or</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Œuvres de Auguste Brizeux — LA FLEUR D’OR</title>
								<author>Auguste Brizeux</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Alphonse Lemerre, éditeur</publisher>
									<date when="1981">1981</date>
								</imprint>
								<biblScope unit="tome">3</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1874">1874</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les deux préfaces ne sont pas reprises.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-02-05" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-02-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LIVRE SEPTIÈME</head><head type="sub_part">A VENISE</head><div type="poem" key="BRI120">
					<head type="main">La Courtoisie</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">LA</w> <w n="1.2">belle</w> <w n="1.3">Courtoisie</w> <w n="1.4">est</w> <w n="1.5">née</w> <w n="1.6">Italienne</w> :</l>
						<l n="2" num="1.2"><w n="2.1">Par</w> <w n="2.2">la</w> <w n="2.3">ville</w> <w n="2.4">elle</w> <w n="2.5">va</w> <w n="2.6">saluant</w> <w n="2.7">de</w> <w n="2.8">la</w> <w n="2.9">main</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Et</w> <w n="3.2">nul</w> <w n="3.3">ne</w> <w n="3.4">la</w> <w n="3.5">peut</w> <w n="3.6">dire</w> <w n="3.7">ou</w> <w n="3.8">noble</w>, <w n="3.9">ou</w> <w n="3.10">plébéienne</w> ;</l>
						<l n="4" num="1.4"><w n="4.1">Hors</w> <w n="4.2">des</w> <w n="4.3">murs</w>, <w n="4.4">on</w> <w n="4.5">la</w> <w n="4.6">voit</w> <w n="4.7">sur</w> <w n="4.8">le</w> <w n="4.9">bord</w> <w n="4.10">du</w> <w n="4.11">chemin</w>,</l>
						<l n="5" num="1.5"><w n="5.1">De</w> <w n="5.2">loin</w> <w n="5.3">reconnaissable</w> <w n="5.4">aux</w> <w n="5.5">lignes</w> <w n="5.6">de</w> <w n="5.7">sa</w> <w n="5.8">tête</w>,</l>
						<l n="6" num="1.6"><w n="6.1">Élégance</w> <w n="6.2">lombarde</w> <w n="6.3">ou</w> <w n="6.4">grand</w> <w n="6.5">type</w> <w n="6.6">romain</w> ;</l>
						<l n="7" num="1.7"><w n="7.1">Si</w> <w n="7.2">près</w> <w n="7.3">d</w>’<w n="7.4">elle</w>, <w n="7.5">égaré</w>, <w n="7.6">le</w> <w n="7.7">voyageur</w> <w n="7.8">s</w>’<w n="7.9">arrête</w>,</l>
						<l n="8" num="1.8"><w n="8.1">Avec</w> <w n="8.2">discrétion</w> <w n="8.3">elle</w> <w n="8.4">s</w>’<w n="8.5">approche</w> <w n="8.6">aussi</w>,</l>
						<l n="9" num="1.9"><w n="9.1">Et</w> <w n="9.2">dans</w> <w n="9.3">les</w> <w n="9.4">mots</w> <w n="9.5">choisis</w> <w n="9.6">que</w> <w n="9.7">son</w> <w n="9.8">front</w> <w n="9.9">pur</w> <w n="9.10">reflète</w></l>
						<l n="10" num="1.10"><w n="10.1">Désigne</w> <w n="10.2">le</w> <w n="10.3">sentier</w> <w n="10.4">sans</w> <w n="10.5">attendre</w> <w n="10.6">merci</w>.</l>
					</lg>
				</div></body></text></TEI>