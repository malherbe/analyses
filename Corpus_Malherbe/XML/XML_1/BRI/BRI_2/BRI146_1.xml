<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FLEUR D’OR</title>
				<title type="medium">Une édition électronique</title>
				<author key="BRI">
					<name>
						<forename>Auguste</forename>
						<surname>BRIZEUX</surname>
					</name>
					<date from="1803" to="1858">1803-1858</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2992 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BRI_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA FLEUR D’OR</title>
						<author>Auguste Brizeux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">https://fr.wikisource.org/wiki/La_Fleur_d%E2%80%99Or</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Œuvres de Auguste Brizeux — LA FLEUR D’OR</title>
								<author>Auguste Brizeux</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Alphonse Lemerre, éditeur</publisher>
									<date when="1981">1981</date>
								</imprint>
								<biblScope unit="tome">3</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1874">1874</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les deux préfaces ne sont pas reprises.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-02-05" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-02-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LIVRE NEUVIÈME</head><head type="sub_part">EN BRETAGNE</head><div type="poem" key="BRI146">
					<head type="main">La Chanson de l’Ermite</head>
					<opener>
						<placeName>En Cornouailles.</placeName>
					</opener>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">LA</w> <w n="1.2">chaumière</w> <w n="1.3">où</w> <w n="1.4">seul</w> <w n="1.5">j</w>’<w n="1.6">habite</w></l>
						<l n="2" num="1.2"><space unit="char" quantity="8"></space><w n="2.1">Est</w> <w n="2.2">petite</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Mais</w> <w n="3.2">elle</w> <w n="3.3">est</w> <w n="3.4">près</w> <w n="3.5">d</w>’<w n="3.6">un</w> <w n="3.7">étang</w></l>
						<l n="4" num="1.4"><w n="4.1">Et</w> <w n="4.2">d</w>’<w n="4.3">un</w> <w n="4.4">bois</w> <w n="4.5">jeune</w> <w n="4.6">et</w> <w n="4.7">flottant</w></l>
						<l n="5" num="1.5"><space unit="char" quantity="8"></space><w n="5.1">Qui</w> <w n="5.2">l</w>’<w n="5.3">abrite</w>.</l>
					</lg>
					<lg n="2">
						<l n="6" num="2.1"><w n="6.1">Dés</w> <w n="6.2">le</w> <w n="6.3">matin</w> <w n="6.4">sous</w> <w n="6.5">mon</w> <w n="6.6">chaume</w></l>
						<l n="7" num="2.2"><space unit="char" quantity="8"></space><w n="7.1">Tout</w> <w n="7.2">embaume</w>.</l>
						<l n="8" num="2.3"><w n="8.1">Mes</w> <w n="8.2">deux</w> <w n="8.3">volets</w> <w n="8.4">sont</w> <w n="8.5">ouverts</w> :</l>
						<l n="9" num="2.4"><w n="9.1">Du</w> <w n="9.2">chanvre</w> <w n="9.3">et</w> <w n="9.4">des</w> <w n="9.5">genêts</w> <w n="9.6">verts</w></l>
						<l n="10" num="2.5"><space unit="char" quantity="8"></space><w n="10.1">Quel</w> <w n="10.2">arôme</w> !</l>
					</lg>
					<lg n="3">
						<l n="11" num="3.1"><w n="11.1">Lorsque</w> <w n="11.2">la</w> <w n="11.3">chaleur</w> <w n="11.4">arrive</w>,</l>
						<l n="12" num="3.2"><space unit="char" quantity="8"></space><w n="12.1">Quand</w> <w n="12.2">le</w> <w n="12.3">givre</w></l>
						<l n="13" num="3.3"><w n="13.1">Se</w> <w n="13.2">cache</w> <w n="13.3">au</w> <w n="13.4">fond</w> <w n="13.5">du</w> <w n="13.6">blé</w> <w n="13.7">noir</w>,</l>
						<l n="14" num="3.4"><w n="14.1">Je</w> <w n="14.2">puise</w> <w n="14.3">à</w> <w n="14.4">mon</w> <w n="14.5">réservoir</w>.</l>
						<l n="15" num="3.5"><space unit="char" quantity="8"></space><w n="15.1">Une</w> <w n="15.2">eau</w> <w n="15.3">vive</w>.</l>
					</lg>
					<lg n="4">
						<l n="16" num="4.1"><w n="16.1">Enfin</w> <w n="16.2">la</w> <w n="16.3">fraîcheur</w> <w n="16.4">retombe</w>,</l>
						<l n="17" num="4.2"><space unit="char" quantity="8"></space><w n="17.1">La</w> <w n="17.2">colombe</w></l>
						<l n="18" num="4.3"><w n="18.1">Roucoule</w> <w n="18.2">sur</w> <w n="18.3">ma</w> <w n="18.4">maison</w> ;</l>
						<l n="19" num="4.4"><w n="19.1">Moi</w>, <w n="19.2">j</w>’<w n="19.3">entonne</w> <w n="19.4">une</w> <w n="19.5">oraison</w> :</l>
						<l n="20" num="4.5"><space unit="char" quantity="8"></space><w n="20.1">Le</w> <w n="20.2">jour</w> <w n="20.3">tombe</w>.</l>
					</lg>
					<lg n="5">
						<l n="21" num="5.1"><w n="21.1">Ainsi</w> <w n="21.2">je</w> <w n="21.3">vis</w> <w n="21.4">en</w> <w n="21.5">ermite</w>.</l>
						<l n="22" num="5.2"><space unit="char" quantity="8"></space><w n="22.1">Dans</w> <w n="22.2">mon</w> <w n="22.3">gîte</w>,</l>
						<l n="23" num="5.3"><w n="23.1">D</w>’<w n="23.2">eau</w>, <w n="23.3">de</w> <w n="23.4">parfum</w>, <w n="23.5">de</w> <w n="23.6">chanson</w> ;</l>
						<l n="24" num="5.4"><w n="24.1">Et</w> <w n="24.2">la</w> <w n="24.3">nuit</w> <w n="24.4">je</w> <w n="24.5">dis</w> <w n="24.6">ton</w> <w n="24.7">nom</w>,</l>
						<l n="25" num="5.5"><space unit="char" quantity="8"></space><w n="25.1">Marguerite</w> !</l>
					</lg>
					<lg n="6">
						<l n="26" num="6.1"><w n="26.1">Marguerite</w>, <w n="26.2">ô</w> <w n="26.3">pèlerine</w></l>
						<l n="27" num="6.2"><space unit="char" quantity="8"></space><w n="27.1">Blanche</w> <w n="27.2">et</w> <w n="27.3">fine</w>.</l>
						<l n="28" num="6.3"><w n="28.1">En</w> <w n="28.2">regagnant</w> <w n="28.3">ton</w> <w n="28.4">manoir</w>.</l>
						<l n="29" num="6.4"><w n="29.1">Dans</w> <w n="29.2">mon</w> <w n="29.3">clos</w> <w n="29.4">viens</w> <w n="29.5">donc</w> <w n="29.6">t</w>’<w n="29.7">asseoir</w></l>
						<l n="30" num="6.5"><space unit="char" quantity="8"></space><w n="30.1">En</w> <w n="30.2">voisine</w>.</l>
					</lg>
				</div></body></text></TEI>