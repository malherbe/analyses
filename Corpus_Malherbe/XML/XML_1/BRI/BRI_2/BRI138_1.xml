<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FLEUR D’OR</title>
				<title type="medium">Une édition électronique</title>
				<author key="BRI">
					<name>
						<forename>Auguste</forename>
						<surname>BRIZEUX</surname>
					</name>
					<date from="1803" to="1858">1803-1858</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2992 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BRI_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA FLEUR D’OR</title>
						<author>Auguste Brizeux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">https://fr.wikisource.org/wiki/La_Fleur_d%E2%80%99Or</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Œuvres de Auguste Brizeux — LA FLEUR D’OR</title>
								<author>Auguste Brizeux</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Alphonse Lemerre, éditeur</publisher>
									<date when="1981">1981</date>
								</imprint>
								<biblScope unit="tome">3</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1874">1874</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les deux préfaces ne sont pas reprises.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-02-05" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-02-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LIVRE NEUVIÈME</head><head type="sub_part">EN BRETAGNE</head><div type="poem" key="BRI138">
					<head type="main">Les Courants</head>
					<opener>
						<salute>À Pélage Lenir</salute>
					</opener>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">SUR</w> <w n="1.2">un</w> <w n="1.3">tertre</w>, <w n="1.4">aux</w> <w n="1.5">confins</w> <w n="1.6">du</w> <w n="1.7">pays</w> <w n="1.8">de</w> <w n="1.9">Bretagne</w>,</l>
						<l n="2" num="1.2"><space unit="char" quantity="8"></space><w n="2.1">S</w>’<w n="2.2">était</w> <w n="2.3">assis</w> <w n="2.4">un</w> <w n="2.5">voyageur</w>,</l>
						<l n="3" num="1.3"><space unit="char" quantity="8"></space><w n="3.1">Observant</w> <w n="3.2">là</w>, <w n="3.3">pauvre</w> <w n="3.4">songeur</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Une</w> <w n="4.2">eau</w> <w n="4.3">qui</w> <w n="4.4">jaillissait</w> <w n="4.5">de</w> <w n="4.6">cette</w> <w n="4.7">humble</w> <w n="4.8">montagne</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Car</w> <w n="5.2">au</w> <w n="5.3">sortir</w> <w n="5.4">du</w> <w n="5.5">sol</w> <w n="5.6">et</w> <w n="5.7">par</w> <w n="5.8">un</w> <w n="5.9">accident</w></l>
						<l n="6" num="2.2"><space unit="char" quantity="8"></space><w n="6.1">L</w>’<w n="6.2">eau</w> <w n="6.3">vive</w> <w n="6.4">s</w>’<w n="6.5">était</w> <w n="6.6">divisée</w> ;</l>
						<l n="7" num="2.3"><space unit="char" quantity="8"></space><w n="7.1">Puis</w>, <w n="7.2">selon</w> <w n="7.3">la</w> <w n="7.4">pente</w> <w n="7.5">opposée</w>,</l>
						<l n="8" num="2.4"><w n="8.1">Courait</w> <w n="8.2">moitié</w> <w n="8.3">vers</w> <w n="8.4">l</w>’<w n="8.5">Est</w>, <w n="8.6">moitié</w> <w n="8.7">vers</w> <w n="8.8">l</w>’<w n="8.9">Occident</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Tout</w> <w n="9.2">chez</w> <w n="9.3">le</w> <w n="9.4">voyageur</w> <w n="9.5">se</w> <w n="9.6">change</w> <w n="9.7">en</w> <w n="9.8">rêverie</w> :</l>
						<l n="10" num="3.2"><space unit="char" quantity="8"></space><w n="10.1">Celui</w>-<w n="10.2">ci</w> <w n="10.3">dans</w> <w n="10.4">ce</w> <w n="10.5">double</w> <w n="10.6">cours</w></l>
						<l n="11" num="3.3"><space unit="char" quantity="8"></space><w n="11.1">Trouvant</w> <w n="11.2">l</w>’<w n="11.3">image</w> <w n="11.4">de</w> <w n="11.5">ses</w> <w n="11.6">jours</w>,</l>
						<l n="12" num="3.4"><w n="12.1">Son</w> <w n="12.2">cœur</w> <w n="12.3">suivait</w> <w n="12.4">les</w> <w n="12.5">flots</w> <w n="12.6">qui</w> <w n="12.7">vont</w> <w n="12.8">vers</w> <w n="12.9">sa</w> <w n="12.10">patrie</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Mais</w> <w n="13.2">sur</w> <w n="13.3">l</w>’<w n="13.4">autre</w> <w n="13.5">penchant</w>, <w n="13.6">il</w> <w n="13.7">opposait</w> <w n="13.8">sa</w> <w n="13.9">main</w></l>
						<l n="14" num="4.2"><space unit="char" quantity="8"></space><w n="14.1">Aux</w> <w n="14.2">flux</w> <w n="14.3">de</w> <w n="14.4">cette</w> <w n="14.5">onde</w> <w n="14.6">égarée</w>,</l>
						<l n="15" num="4.3"><space unit="char" quantity="8"></space><w n="15.1">Et</w> <w n="15.2">sur</w> <w n="15.3">la</w> <w n="15.4">pente</w> <w n="15.5">préférée</w></l>
						<l n="16" num="4.4"><w n="16.1">Il</w> <w n="16.2">la</w> <w n="16.3">forçait</w> <w n="16.4">de</w> <w n="16.5">prendre</w> <w n="16.6">avec</w> <w n="16.7">lui</w> <w n="16.8">son</w> <w n="16.9">chemin</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Ainsi</w> <w n="17.2">vers</w> <w n="17.3">son</w> <w n="17.4">pays</w>, <w n="17.5">seul</w> <w n="17.6">but</w> <w n="17.7">qui</w> <w n="17.8">le</w> <w n="17.9">réclame</w>,</l>
						<l n="18" num="5.2"><space unit="char" quantity="8"></space><w n="18.1">Ainsi</w> <w n="18.2">vers</w> <w n="18.3">ses</w> <w n="18.4">chères</w> <w n="18.5">amours</w></l>
						<l n="19" num="5.3"><space unit="char" quantity="8"></space><w n="19.1">Allaient</w> <w n="19.2">et</w> <w n="19.3">sans</w> <w n="19.4">plus</w> <w n="19.5">de</w> <w n="19.6">détours</w></l>
						<l n="20" num="5.4"><w n="20.1">Tous</w> <w n="20.2">les</w> <w n="20.3">courants</w> <w n="20.4">de</w> <w n="20.5">l</w>’<w n="20.6">onde</w> <w n="20.7">et</w> <w n="20.8">tous</w> <w n="20.9">ceux</w> <w n="20.10">de</w> <w n="20.11">son</w> <w n="20.12">âme</w>.</l>
					</lg>
				</div></body></text></TEI>