<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="BUS">
					<name>
						<forename>Alfred</forename>
						<surname>BUSQUET</surname>
					</name>
					<date from="1819" to="1883">1819-1883</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2356 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BUS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÉSIES</title>
						<author>Alfred Busquet</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URI">https ://archive.org/details/poesiespremieres00alfr/mode/2up</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title type="main">POÉSIES</title>
								<author>Alfred Busquet</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE HACHETTE ET Cie</publisher>
									<date when="1884">1884</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1884">1884</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface (AU LECTEUR) n’a pas été reprise.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension(…).</p>
					<p>Les tirets simples précédés et/ou suivis d’une espace ont été remplacés par des tirets demi-cadratin</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-04-17" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-04-17" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">SUR LES CHEMINS</head><head type="sub_part">IMPRESSIONS DE VOYAGE</head><head type="sub_part">PORTUGAL-ESPAGNE</head><div type="poem" key="BUS2">
					<head type="number">I</head>
					<head type="main">LES DAUPHINS</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Les</w> <w n="1.2">dauphins</w>, <w n="1.3">poussés</w> <w n="1.4">par</w> <w n="1.5">l</w>’<w n="1.6">orage</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Dans</w> <w n="2.2">les</w> <w n="2.3">blancs</w> <w n="2.4">remous</w> <w n="2.5">du</w> <w n="2.6">vapeur</w></l>
						<l n="3" num="1.3"><w n="3.1">Se</w> <w n="3.2">font</w> <w n="3.3">la</w> <w n="3.4">poursuite</w> <w n="3.5">avec</w> <w n="3.6">rage</w>.</l>
						<l n="4" num="1.4"><w n="4.1">Gais</w> <w n="4.2">compagnons</w>, <w n="4.3">vifs</w> <w n="4.4">et</w> <w n="4.5">sans</w> <w n="4.6">peur</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Ils</w> <w n="5.2">bondissent</w>, <w n="5.3">oiseaux</w> <w n="5.4">sans</w> <w n="5.5">ailes</w>,</l>
						<l n="6" num="2.2"><w n="6.1">Bateleurs</w> <w n="6.2">des</w> <w n="6.3">flots</w> <w n="6.4">et</w> <w n="6.5">du</w> <w n="6.6">vent</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Ils</w> <w n="7.2">lancent</w> <w n="7.3">l</w>’<w n="7.4">eau</w> <w n="7.5">par</w> <w n="7.6">leur</w> <w n="7.7">évent</w> ;</l>
						<l n="8" num="2.4"><w n="8.1">Leur</w> <w n="8.2">nageoire</w> <w n="8.3">a</w> <w n="8.4">mille</w> <w n="8.5">étincelles</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Dans</w> <w n="9.2">le</w> <w n="9.3">seul</w> <w n="9.4">intérêt</w> <w n="9.5">de</w> <w n="9.6">l</w>’<w n="9.7">art</w></l>
						<l n="10" num="3.2"><w n="10.1">Et</w> <w n="10.2">sans</w> <w n="10.3">espoir</w> <w n="10.4">de</w> <w n="10.5">récompense</w>,</l>
						<l n="11" num="3.3"><w n="11.1">Ils</w> <w n="11.2">pratiquent</w> <w n="11.3">le</w> <w n="11.4">grand</w> <w n="11.5">écart</w>,</l>
						<l n="12" num="3.4"><w n="12.1">N</w>’<w n="12.2">ayant</w> <w n="12.3">encor</w> <w n="12.4">rien</w> <w n="12.5">dans</w> <w n="12.6">leur</w> <w n="12.7">panse</w> :</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Les</w> <w n="13.2">passagers</w>, <w n="13.3">peuple</w> <w n="13.4">imbécile</w>,</l>
						<l n="14" num="4.2"><w n="14.1">Sifflent</w> <w n="14.2">les</w> <w n="14.3">pauvres</w> <w n="14.4">baladins</w> ;</l>
						<l n="15" num="4.3"><w n="15.1">Ils</w> <w n="15.2">n</w>’<w n="15.3">obtiennent</w> <w n="15.4">que</w> <w n="15.5">des</w> <w n="15.6">dédains</w> ;</l>
						<l n="16" num="4.4"><w n="16.1">Les</w> <w n="16.2">nourrir</w> <w n="16.3">serait</w> <w n="16.4">si</w> <w n="16.5">facile</w> !</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Amis</w> <w n="17.2">des</w> <w n="17.3">poètes</w> <w n="17.4">errans</w>.</l>
						<l n="18" num="5.2"><w n="18.1">Tous</w> <w n="18.2">vos</w> <w n="18.3">jeux</w> <w n="18.4">et</w> <w n="18.5">vos</w> <w n="18.6">drôleries</w></l>
						<l n="19" num="5.3"><w n="19.1">Ne</w> <w n="19.2">provoquent</w> <w n="19.3">que</w> <w n="19.4">railleries</w> :</l>
						<l n="20" num="5.4"><w n="20.1">Disparaissez</w> <w n="20.2">dans</w> <w n="20.3">les</w> <w n="20.4">courans</w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Et</w> <w n="21.2">quand</w> <w n="21.3">des</w> <w n="21.4">forces</w> <w n="21.5">invisibles</w></l>
						<l n="22" num="6.2"><w n="22.1">Se</w> <w n="22.2">promèneront</w> <w n="22.3">sur</w> <w n="22.4">les</w> <w n="22.5">flots</w>.</l>
						<l n="23" num="6.3"><w n="23.1">Parmi</w> <w n="23.2">les</w> <w n="23.3">cris</w> <w n="23.4">et</w> <w n="23.5">les</w> <w n="23.6">sanglots</w></l>
						<l n="24" num="6.4"><w n="24.1">Vous</w> <w n="24.2">passerez</w> <w n="24.3">fiers</w> <w n="24.4">et</w> <w n="24.5">terribles</w> !</l>
					</lg>
					<closer>
						<dateline>
							<placeName>En mer</placeName>,
							<date when="1859">1859</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>