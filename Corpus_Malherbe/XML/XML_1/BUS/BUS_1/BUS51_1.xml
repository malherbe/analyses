<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="BUS">
					<name>
						<forename>Alfred</forename>
						<surname>BUSQUET</surname>
					</name>
					<date from="1819" to="1883">1819-1883</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2356 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BUS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÉSIES</title>
						<author>Alfred Busquet</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URI">https ://archive.org/details/poesiespremieres00alfr/mode/2up</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title type="main">POÉSIES</title>
								<author>Alfred Busquet</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE HACHETTE ET Cie</publisher>
									<date when="1884">1884</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1884">1884</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface (AU LECTEUR) n’a pas été reprise.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension(…).</p>
					<p>Les tirets simples précédés et/ou suivis d’une espace ont été remplacés par des tirets demi-cadratin</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-04-17" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-04-17" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CHOSES D’AUTREFOIS</head><div type="poem" key="BUS51">
					<head type="main">A AIMÉ MILLET,</head>
					<head type="sub_1">statuaire</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Triste</w> <w n="1.2">Ariane</w>, <w n="1.3">abandonnée</w></l>
						<l n="2" num="1.2"><w n="2.1">Par</w> <w n="2.2">le</w> <w n="2.3">plus</w> <w n="2.4">traître</w> <w n="2.5">des</w> <w n="2.6">amans</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Je</w> <w n="3.2">ne</w> <w n="3.3">plains</w> <w n="3.4">pas</w> <w n="3.5">ta</w> <w n="3.6">destinée</w></l>
						<l n="4" num="1.4"><w n="4.1">Et</w> <w n="4.2">je</w> <w n="4.3">souris</w> <w n="4.4">à</w> <w n="4.5">tes</w> <w n="4.6">tourmens</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Guidant</w> <w n="5.2">sa</w> <w n="5.3">troupe</w> <w n="5.4">époumonnée</w>,</l>
						<l n="6" num="2.2"><w n="6.1">Lycæus</w>, <w n="6.2">au</w> <w n="6.3">regard</w> <w n="6.4">charmant</w>,</l>
						<l n="7" num="2.3"><w n="7.1">S</w>’<w n="7.2">avance</w> : <w n="7.3">heureuse</w> <w n="7.4">infortunée</w>.</l>
						<l n="8" num="2.4"><w n="8.1">Les</w> <w n="8.2">destins</w> <w n="8.3">pour</w> <w n="8.4">toi</w> <w n="8.5">sont</w> <w n="8.6">démens</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Io</w> <w n="9.2">Bacchè</w> ! <w n="9.3">Divin</w> <w n="9.4">échange</w> !</l>
						<l n="10" num="3.2"><w n="10.1">Un</w> <w n="10.2">mortel</w> <w n="10.3">a</w> <w n="10.4">fait</w> <w n="10.5">place</w> <w n="10.6">au</w> <w n="10.7">Dieu</w>.</l>
						<l n="11" num="3.3"><w n="11.1">Amour</w>, <w n="11.2">amour</w> ! <w n="11.3">voilà</w> <w n="11.4">ton</w> <w n="11.5">jeu</w>.</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1"><w n="12.1">Et</w> <w n="12.2">par</w> <w n="12.3">une</w> <w n="12.4">aventure</w> <w n="12.5">étrange</w>.</l>
						<l n="13" num="4.2"><w n="13.1">Divin</w> <w n="13.2">secours</w> <w n="13.3">d</w>’<w n="13.4">un</w> <w n="13.5">immortel</w>.</l>
						<l n="14" num="4.3"><w n="14.1">Ton</w> <w n="14.2">rocher</w> <w n="14.3">devient</w> <w n="14.4">un</w> <w n="14.5">autel</w>.</l>
					</lg>
				</div></body></text></TEI>