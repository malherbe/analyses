<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="BUS">
					<name>
						<forename>Alfred</forename>
						<surname>BUSQUET</surname>
					</name>
					<date from="1819" to="1883">1819-1883</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2356 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BUS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÉSIES</title>
						<author>Alfred Busquet</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URI">https ://archive.org/details/poesiespremieres00alfr/mode/2up</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title type="main">POÉSIES</title>
								<author>Alfred Busquet</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE HACHETTE ET Cie</publisher>
									<date when="1884">1884</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1884">1884</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface (AU LECTEUR) n’a pas été reprise.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension(…).</p>
					<p>Les tirets simples précédés et/ou suivis d’une espace ont été remplacés par des tirets demi-cadratin</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-04-17" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-04-17" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">SUR LES CHEMINS</head><head type="sub_part">IMPRESSIONS DE VOYAGE</head><head type="sub_part">PORTUGAL-ESPAGNE</head><div type="poem" key="BUS18">
					<head type="number">XVII</head>
					<head type="main">LA VEILLÉE DU GRAND SABBAT</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">L</w>’ <w n="1.2">Angélus</w> <w n="1.3">à</w> <w n="1.4">la</w> <w n="1.5">Cartoga</w></l>
						<l n="2" num="1.2"><w n="2.1">Vient</w> <w n="2.2">de</w> <w n="2.3">sonner</w> : <w n="2.4">allons</w>, <w n="2.5">ma</w> <w n="2.6">fille</w>.</l>
						<l n="3" num="1.3"><w n="3.1">Laisse</w> <w n="3.2">ta</w> <w n="3.3">caille</w> <w n="3.4">qui</w> <w n="3.5">babille</w>.</l>
						<l n="4" num="1.4"><w n="4.1">Voici</w> <w n="4.2">la</w> <w n="4.3">nuit</w> <w n="4.4">qui</w> <w n="4.5">vient</w>. <w n="4.6">Déjà</w>,</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Dom</w> <w n="5.2">Bazile</w>, <w n="5.3">frais</w> <w n="5.4">et</w> <w n="5.5">prospère</w>.</l>
						<l n="6" num="2.2"><w n="6.1">Se</w> <w n="6.2">dirige</w> <w n="6.3">vers</w> <w n="6.4">la</w> <w n="6.5">prison</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Afin</w> <w n="7.2">de</w> <w n="7.3">confesser</w> <w n="7.4">ton</w> <w n="7.5">père</w></l>
						<l n="8" num="2.4"><w n="8.1">Et</w> <w n="8.2">lui</w> <w n="8.3">faire</w> <w n="8.4">entendre</w> <w n="8.5">raison</w> ;</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Carmen</w>, <w n="9.2">qui</w> <w n="9.3">veut</w> <w n="9.4">être</w> <w n="9.5">novice</w>,</l>
						<l n="10" num="3.2"><w n="10.1">Se</w> <w n="10.2">hâte</w> <w n="10.3">vers</w> <w n="10.4">la</w> <w n="10.5">Mosquitta</w>,</l>
						<l n="11" num="3.3"><w n="11.1">Espérant</w> <w n="11.2">trouver</w> <w n="11.3">à</w> <w n="11.4">l</w>’<w n="11.5">office</w></l>
						<l n="12" num="3.4"><w n="12.1">Le</w> <w n="12.2">beau</w> <hi rend="ital"><w n="12.3">mayo</w></hi> <w n="12.4">qui</w> <w n="12.5">la</w> <w n="12.6">quitta</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Nous</w>, <w n="13.2">qu</w>’<w n="13.3">on</w> <w n="13.4">proscrit</w> <w n="13.5">dans</w> <w n="13.6">ce</w> <w n="13.7">royaume</w>.</l>
						<l n="14" num="4.2"><w n="14.1">Profitons</w> <w n="14.2">de</w> <w n="14.3">l</w>’<w n="14.4">heure</w> <w n="14.5">et</w> <w n="14.6">du</w> <w n="14.7">lieu</w>.</l>
						<l n="15" num="4.3"><w n="15.1">Fidèles</w> <w n="15.2">au</w> <w n="15.3">Deutéronome</w>,</l>
						<l n="16" num="4.4"><w n="16.1">Aux</w> <w n="16.2">règles</w> <w n="16.3">que</w> <w n="16.4">nous</w> <w n="16.5">traça</w> <w n="16.6">Dieu</w> ;</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Quand</w> <w n="17.2">les</w> <w n="17.3">étoiles</w> <w n="17.4">coutumières</w></l>
						<l n="18" num="5.2"><w n="18.1">Perceront</w> <w n="18.2">la</w> <w n="18.3">nuit</w> <w n="18.4">qui</w> <w n="18.5">s</w>’<w n="18.6">abat</w>.</l>
						<l n="19" num="5.3"><w n="19.1">Nous</w> <w n="19.2">allumerons</w> <w n="19.3">les</w> <w n="19.4">lumières</w></l>
						<l n="20" num="5.4"><w n="20.1">De</w> <w n="20.2">la</w> <w n="20.3">lampe</w> <w n="20.4">du</w> <w n="20.5">grand</w> <w n="20.6">Sabbat</w>,</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Et</w> <w n="21.2">lorsque</w> <w n="21.3">les</w> <w n="21.4">trois</w> <w n="21.5">sœurs</w> <w n="21.6">nocturnes</w></l>
						<l n="22" num="6.2"><w n="22.1">Disparaîtront</w> <w n="22.2">à</w> <w n="22.3">l</w>’<w n="22.4">horizon</w>.</l>
						<l n="23" num="6.3"><w n="23.1">Nous</w> <w n="23.2">irons</w> <w n="23.3">souffler</w>, <w n="23.4">taciturnes</w>.</l>
						<l n="24" num="6.4"><w n="24.1">Les</w> <w n="24.2">trois</w> <w n="24.3">astres</w> <w n="24.4">de</w> <w n="24.5">la</w> <w n="24.6">maison</w>.</l>
					</lg>
					<closer>
						<placeName>Cordoba</placeName>.
					</closer>
				</div></body></text></TEI>