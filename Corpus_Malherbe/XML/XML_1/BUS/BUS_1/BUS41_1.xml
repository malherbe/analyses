<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="BUS">
					<name>
						<forename>Alfred</forename>
						<surname>BUSQUET</surname>
					</name>
					<date from="1819" to="1883">1819-1883</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2356 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BUS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÉSIES</title>
						<author>Alfred Busquet</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URI">https ://archive.org/details/poesiespremieres00alfr/mode/2up</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title type="main">POÉSIES</title>
								<author>Alfred Busquet</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE HACHETTE ET Cie</publisher>
									<date when="1884">1884</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1884">1884</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface (AU LECTEUR) n’a pas été reprise.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension(…).</p>
					<p>Les tirets simples précédés et/ou suivis d’une espace ont été remplacés par des tirets demi-cadratin</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-04-17" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-04-17" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">POÉSIES DOMESTIQUES</head><div type="poem" key="BUS41">
					<head type="main">LE ROUET</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Quatre</w>-<w n="1.2">vingts</w> <w n="1.3">ans</w> <w n="1.4">avaient</w> <w n="1.5">sonné</w> <w n="1.6">pour</w> <w n="1.7">notre</w> <w n="1.8">tante</w>.</l>
						<l n="2" num="1.2"><w n="2.1">Riche</w> <w n="2.2">de</w> <w n="2.3">ses</w> <w n="2.4">vertus</w> <w n="2.5">et</w> <w n="2.6">de</w> <w n="2.7">son</w> <w n="2.8">sort</w> <w n="2.9">contente</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Elle</w> <w n="3.2">jetait</w> <w n="3.3">encore</w>, <w n="3.4">âme</w> <w n="3.5">de</w> <w n="3.6">la</w> <w n="3.7">maison</w>.</l>
						<l n="4" num="1.4"><w n="4.1">Son</w> <w n="4.2">pain</w> <w n="4.3">dur</w> <w n="4.4">aux</w> <w n="4.5">oiseaux</w>, <w n="4.6">au</w> <w n="4.7">logis</w> <w n="4.8">sa</w> <w n="4.9">chanson</w>.</l>
						<l n="5" num="1.5"><w n="5.1">Elle</w> <w n="5.2">avait</w> <w n="5.3">recueilli</w> <w n="5.4">dans</w> <w n="5.5">sa</w> <w n="5.6">robe</w> <w n="5.7">de</w> <w n="5.8">bure</w></l>
						<l n="6" num="1.6"><w n="6.1">Treize</w> <w n="6.2">petits</w>-<w n="6.3">neveux</w>, <w n="6.4">son</w> <w n="6.5">orgueil</w>, <w n="6.6">sa</w> <w n="6.7">parure</w>,</l>
						<l n="7" num="1.7"><w n="7.1">Et</w> <w n="7.2">fille</w>, <w n="7.3">elle</w> <w n="7.4">gardait</w> <w n="7.5">pour</w> <w n="7.6">les</w> <w n="7.7">hymens</w> <w n="7.8">du</w> <w n="7.9">ciel</w></l>
						<l n="8" num="1.8"><w n="8.1">La</w> <w n="8.2">fleur</w> <w n="8.3">de</w> <w n="8.4">son</w> <w n="8.5">sourire</w> <w n="8.6">aussi</w> <w n="8.7">doux</w> <w n="8.8">que</w> <w n="8.9">le</w> <w n="8.10">miel</w>.</l>
						<l n="9" num="1.9"><w n="9.1">Cinquante</w> <w n="9.2">écus</w> <w n="9.3">de</w> <w n="9.4">rente</w> <w n="9.5">et</w> <w n="9.6">son</w> <w n="9.7">toit</w> <w n="9.8">pour</w> <w n="9.9">patrie</w>.</l>
						<l n="10" num="1.10"><w n="10.1">Pour</w> <w n="10.2">blason</w>, <w n="10.3">trois</w> <w n="10.4">cents</w> <w n="10.5">ans</w> <w n="10.6">de</w> <w n="10.7">paysannerie</w> ;</l>
						<l n="11" num="1.11"><w n="11.1">Noble</w> <w n="11.2">blason</w> <w n="11.3">signé</w> <w n="11.4">sur</w> <w n="11.5">les</w> <w n="11.6">vitraux</w> <w n="11.7">pieux</w></l>
						<l n="12" num="1.12"><w n="12.1">De</w> <w n="12.2">l</w>’<w n="12.3">église</w>, <w n="12.4">et</w> <w n="12.5">donnés</w> <w n="12.6">jadis</w> <w n="12.7">par</w> <w n="12.8">les</w> <w n="12.9">aïeux</w>.</l>
						<l n="13" num="1.13"><w n="13.1">Un</w> <w n="13.2">jardin</w> <w n="13.3">exigu</w>, <w n="13.4">la</w> <w n="13.5">santé</w>, <w n="13.6">la</w> <w n="13.7">prière</w></l>
						<l n="14" num="1.14"><w n="14.1">Et</w> <w n="14.2">son</w> <w n="14.3">rouet</w>, <w n="14.4">rendaient</w> <w n="14.5">sa</w> <w n="14.6">vieillesse</w> <w n="14.7">prospère</w>.</l>
						<l n="15" num="1.15"><w n="15.1">Comme</w> <w n="15.2">elle</w> <w n="15.3">était</w> <w n="15.4">joyeuse</w> <w n="15.5">alors</w> <w n="15.6">que</w> <w n="15.7">le</w> <w n="15.8">printemps</w></l>
						<l n="16" num="1.16"><w n="16.1">Fleurissait</w> <w n="16.2">la</w> <w n="16.3">prairie</w> <w n="16.4">et</w> <w n="16.5">ses</w> <w n="16.6">quatre</w>-<w n="16.7">vingts</w> <w n="16.8">ans</w>.</l>
						<l n="17" num="1.17"><w n="17.1">Et</w> <w n="17.2">qu</w>’<w n="17.3">une</w> <w n="17.4">bonne</w> <w n="17.5">odeur</w> <w n="17.6">de</w> <w n="17.7">foin</w> <w n="17.8">et</w> <w n="17.9">d</w>’<w n="17.10">algue</w> <w n="17.11">verte</w></l>
						<l n="18" num="1.18"><w n="18.1">Lui</w> <w n="18.2">venait</w> <w n="18.3">de</w> <w n="18.4">la</w> <w n="18.5">mer</w> <w n="18.6">par</w> <w n="18.7">la</w> <w n="18.8">fenêtre</w> <w n="18.9">ouverte</w> !</l>
						<l n="19" num="1.19"><w n="19.1">Alors</w> <w n="19.2">elle</w> <w n="19.3">appelait</w> <w n="19.4">tous</w> <w n="19.5">ses</w> <w n="19.6">petits</w>-<w n="19.7">neveux</w>,</l>
						<l n="20" num="1.20"><w n="20.1">Démons</w> <w n="20.2">charmans</w>, <w n="20.3">toujours</w> <w n="20.4">se</w> <w n="20.5">disputant</w> <w n="20.6">entre</w> <w n="20.7">eux</w>,</l>
						<l n="21" num="1.21"><w n="21.1">Et</w> <w n="21.2">de</w> <w n="21.3">son</w> <w n="21.4">superflu</w>, <w n="21.5">prodigue</w> <w n="21.6">outre</w> <w n="21.7">mesure</w>.</l>
						<l n="22" num="1.22"><w n="22.1">Les</w> <w n="22.2">bourrait</w> <w n="22.3">de</w> <w n="22.4">gâteaux</w>, <w n="22.5">de</w> <w n="22.6">noix</w>, <w n="22.7">de</w> <w n="22.8">confitures</w>.</l>
						<l n="23" num="1.23"><w n="23.1">Puis</w>, <w n="23.2">les</w> <w n="23.3">ayant</w> <w n="23.4">rangés</w> <w n="23.5">en</w> <w n="23.6">cercle</w>, <w n="23.7">leur</w> <w n="23.8">chantait</w>.</l>
						<l n="24" num="1.24"><w n="24.1">En</w> <w n="24.2">tremblotant</w> <w n="24.3">un</w> <w n="24.4">peu</w>, <w n="24.5">la</w> <w n="24.6">chanson</w> <w n="24.7">du</w> <w n="24.8">Rouet</w> :</l>
					</lg>
					<lg n="2">
						<l n="25" num="2.1"><space unit="char" quantity="8"></space><w n="25.1">Tourne</w>, <w n="25.2">rouet</w>, <w n="25.3">tourne</w>, <w n="25.4">quenouille</w> !</l>
						<l n="26" num="2.2"><space unit="char" quantity="8"></space><w n="26.1">Tourne</w>, <w n="26.2">bobine</w>, <w n="26.3">allègrement</w>,</l>
						<l n="27" num="2.3"><space unit="char" quantity="8"></space><w n="27.1">Et</w> <w n="27.2">sous</w> <w n="27.3">mon</w> <w n="27.4">vieux</w> <w n="27.5">doigt</w> <w n="27.6">qui</w> <w n="27.7">te</w> <w n="27.8">mouille</w>.</l>
						<l n="28" num="2.4"><space unit="char" quantity="8"></space><w n="28.1">Lin</w>, <w n="28.2">fais</w>-<w n="28.3">nous</w> <w n="28.4">un</w> <w n="28.5">chaud</w> <w n="28.6">vêtement</w>.</l>
						<l n="29" num="2.5"><space unit="char" quantity="8"></space><w n="29.1">Tandis</w> <w n="29.2">que</w> <w n="29.3">l</w>’<w n="29.4">horizon</w> <w n="29.5">flamboie</w>.</l>
						<l n="30" num="2.6"><space unit="char" quantity="8"></space><w n="30.1">Que</w> <w n="30.2">monte</w> <w n="30.3">le</w> <w n="30.4">soleil</w> <w n="30.5">d</w>’<w n="30.6">été</w>.</l>
						<l n="31" num="2.7"><space unit="char" quantity="8"></space><w n="31.1">Avec</w> <w n="31.2">du</w> <w n="31.3">pain</w> <w n="31.4">et</w> <w n="31.5">la</w> <w n="31.6">santé</w>.</l>
						<l n="32" num="2.8"><space unit="char" quantity="8"></space><w n="32.1">Je</w> <w n="32.2">file</w>, <w n="32.3">et</w> <w n="32.4">mon</w> <w n="32.5">cœur</w> <w n="32.6">est</w> <w n="32.7">en</w> <w n="32.8">joie</w> !</l>
					</lg>
					<lg n="3">
						<l n="33" num="3.1"><space unit="char" quantity="8"></space><w n="33.1">Tourne</w>, <w n="33.2">rouet</w>, <w n="33.3">tourne</w> <w n="33.4">toujours</w>.</l>
						<l n="34" num="3.2"><space unit="char" quantity="8"></space><w n="34.1">Avec</w> <w n="34.2">le</w> <w n="34.3">lin</w> <w n="34.4">file</w> <w n="34.5">mes</w> <w n="34.6">jours</w> !</l>
					</lg>
					<lg n="4">
						<l n="35" num="4.1"><space unit="char" quantity="8"></space><w n="35.1">Roulant</w> <w n="35.2">ses</w> <w n="35.3">eaux</w> <w n="35.4">et</w> <w n="35.5">la</w> <w n="35.6">lumière</w>.</l>
						<l n="36" num="4.2"><space unit="char" quantity="8"></space><w n="36.1">Le</w> <w n="36.2">fleuve</w> <w n="36.3">a</w> <w n="36.4">des</w> <w n="36.5">bruits</w> <w n="36.6">si</w> <w n="36.7">charmans</w> !</l>
						<l n="37" num="4.3"><space unit="char" quantity="8"></space><w n="37.1">Il</w> <w n="37.2">chante</w> <w n="37.3">aux</w> <w n="37.4">pieds</w> <w n="37.5">de</w> <w n="37.6">la</w> <w n="37.7">chaumière</w></l>
						<l n="38" num="4.4"><space unit="char" quantity="8"></space><w n="38.1">Où</w> <w n="38.2">coulent</w> <w n="38.3">mes</w> <w n="38.4">quatre</w>-<w n="38.5">vingts</w> <w n="38.6">ans</w>.</l>
						<l n="39" num="4.5"><space unit="char" quantity="8"></space><w n="39.1">De</w> <w n="39.2">peur</w> <w n="39.3">que</w> <w n="39.4">la</w> <w n="39.5">fauvette</w> <w n="39.6">étouffe</w>,</l>
						<l n="40" num="4.6"><space unit="char" quantity="8"></space><w n="40.1">Pour</w> <w n="40.2">que</w> <w n="40.3">le</w> <w n="40.4">pinson</w> <w n="40.5">soit</w> <w n="40.6">au</w> <w n="40.7">frais</w>,</l>
						<l n="41" num="4.7"><space unit="char" quantity="8"></space><w n="41.1">L</w>’<w n="41.2">églantier</w> <w n="41.3">fleurit</w> <w n="41.4">tout</w> <w n="41.5">exprès</w>.</l>
						<l n="42" num="4.8"><space unit="char" quantity="8"></space><w n="42.1">Et</w> <w n="42.2">le</w> <w n="42.3">sureau</w> <w n="42.4">blanchit</w> <w n="42.5">sa</w> <w n="42.6">touffe</w>.</l>
					</lg>
					<lg n="5">
						<l n="43" num="5.1"><space unit="char" quantity="8"></space><w n="43.1">Tourne</w>, <w n="43.2">rouet</w>, <w n="43.3">tourne</w> <w n="43.4">toujours</w>.</l>
						<l n="44" num="5.2"><space unit="char" quantity="8"></space><w n="44.1">Avec</w> <w n="44.2">le</w> <w n="44.3">lin</w> <w n="44.4">file</w> <w n="44.5">mes</w> <w n="44.6">jours</w> !</l>
					</lg>
					<lg n="6">
						<l n="45" num="6.1"><space unit="char" quantity="8"></space><w n="45.1">Le</w> <w n="45.2">ramier</w> <w n="45.3">gémit</w> <w n="45.4">dans</w> <w n="45.5">les</w> <w n="45.6">chênes</w></l>
						<l n="46" num="6.2"><space unit="char" quantity="8"></space><w n="46.1">Et</w> <w n="46.2">répète</w> <w n="46.3">aux</w> <w n="46.4">échos</w> <w n="46.5">son</w> <w n="46.6">deuil</w>.</l>
						<l n="47" num="6.3"><space unit="char" quantity="8"></space><w n="47.1">Les</w> <w n="47.2">corneilles</w> <w n="47.3">mangent</w> <w n="47.4">les</w> <w n="47.5">faînes</w>.</l>
						<l n="48" num="6.4"><space unit="char" quantity="8"></space><w n="48.1">Le</w> <w n="48.2">linot</w> <w n="48.3">tient</w> <w n="48.4">tête</w> <w n="48.5">au</w> <w n="48.6">bouvreuil</w> ;</l>
						<l n="49" num="6.5"><space unit="char" quantity="8"></space><w n="49.1">Le</w> <w n="49.2">râle</w> <w n="49.3">court</w> <w n="49.4">dans</w> <w n="49.5">la</w> <w n="49.6">prairie</w>,</l>
						<l n="50" num="6.6"><space unit="char" quantity="8"></space><w n="50.1">La</w> <w n="50.2">bécassine</w> <w n="50.3">nous</w> <w n="50.4">revient</w>.</l>
						<l n="51" num="6.7"><space unit="char" quantity="8"></space><w n="51.1">L</w>’<w n="51.2">hirondelle</w> <w n="51.3">qui</w> <w n="51.4">se</w> <w n="51.5">souvient</w></l>
						<l n="52" num="6.8"><space unit="char" quantity="8"></space><w n="52.1">A</w> <w n="52.2">ma</w> <w n="52.3">fenêtre</w> <w n="52.4">pour</w> <w n="52.5">patrie</w>.</l>
					</lg>
					<lg n="7">
						<l n="53" num="7.1"><space unit="char" quantity="8"></space><w n="53.1">Tourne</w>, <w n="53.2">rouet</w>, <w n="53.3">tourne</w> <w n="53.4">toujours</w>.</l>
						<l n="54" num="7.2"><space unit="char" quantity="8"></space><w n="54.1">Avec</w> <w n="54.2">le</w> <w n="54.3">lin</w> <w n="54.4">file</w> <w n="54.5">mes</w> <w n="54.6">jours</w> !</l>
					</lg>
					<lg n="8">
						<l n="55" num="8.1"><space unit="char" quantity="8"></space><w n="55.1">Je</w> <w n="55.2">n</w>’<w n="55.3">ai</w> <w n="55.4">pas</w> <w n="55.5">une</w> <w n="55.6">grande</w> <w n="55.7">peine</w></l>
						<l n="56" num="8.2"><space unit="char" quantity="8"></space><w n="56.1">A</w> <w n="56.2">voir</w> <w n="56.3">mes</w> <w n="56.4">ans</w> <w n="56.5">tôt</w> <w n="56.6">écoulés</w> ;</l>
						<l n="57" num="8.3"><space unit="char" quantity="8"></space><w n="57.1">Lorsque</w> <w n="57.2">de</w> <w n="57.3">pain</w> <w n="57.4">la</w> <w n="57.5">huche</w> <w n="57.6">est</w> <w n="57.7">pleine</w>.</l>
						<l n="58" num="8.4"><space unit="char" quantity="8"></space><w n="58.1">Tous</w> <w n="58.2">mes</w> <w n="58.3">ennuis</w> <w n="58.4">sont</w> <w n="58.5">refoulés</w>.</l>
						<l n="59" num="8.5"><space unit="char" quantity="8"></space><w n="59.1">Les</w> <w n="59.2">fleurs</w>, <w n="59.3">les</w> <w n="59.4">oiseaux</w>, <w n="59.5">la</w> <w n="59.6">famille</w>.</l>
						<l n="60" num="8.6"><space unit="char" quantity="8"></space><w n="60.1">Suffisent</w> <w n="60.2">à</w> <w n="60.3">dorer</w> <w n="60.4">mon</w> <w n="60.5">sort</w> :</l>
						<l n="61" num="8.7"><space unit="char" quantity="8"></space><w n="61.1">Je</w> <w n="61.2">vends</w> <w n="61.3">mes</w> <w n="61.4">toiles</w>, <w n="61.5">pauvre</w> <w n="61.6">fille</w>.</l>
						<l n="62" num="8.8"><space unit="char" quantity="8"></space><w n="62.1">N</w>’<w n="62.2">en</w> <w n="62.3">gardant</w> <w n="62.4">qu</w>’<w n="62.5">une</w> <w n="62.6">pour</w> <w n="62.7">la</w> <w n="62.8">mort</w>.</l>
					</lg>
					<lg n="9">
						<l n="63" num="9.1"><space unit="char" quantity="8"></space><w n="63.1">Tourne</w>, <w n="63.2">rouet</w>, <w n="63.3">tourne</w> <w n="63.4">toujours</w>.</l>
						<l n="64" num="9.2"><space unit="char" quantity="8"></space><w n="64.1">Avec</w> <w n="64.2">le</w> <w n="64.3">lin</w> <w n="64.4">file</w> <w n="64.5">mes</w> <w n="64.6">jours</w> !</l>
					</lg>
				</div></body></text></TEI>