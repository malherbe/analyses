<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="BUS">
					<name>
						<forename>Alfred</forename>
						<surname>BUSQUET</surname>
					</name>
					<date from="1819" to="1883">1819-1883</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2356 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BUS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÉSIES</title>
						<author>Alfred Busquet</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URI">https ://archive.org/details/poesiespremieres00alfr/mode/2up</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title type="main">POÉSIES</title>
								<author>Alfred Busquet</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE HACHETTE ET Cie</publisher>
									<date when="1884">1884</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1884">1884</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface (AU LECTEUR) n’a pas été reprise.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension(…).</p>
					<p>Les tirets simples précédés et/ou suivis d’une espace ont été remplacés par des tirets demi-cadratin</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-04-17" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-04-17" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">RELIQUIÆ</head><div type="poem" key="BUS67">
					<head type="main">A UNE JEUNE AVEUGLE-NÉE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Pauvre</w> <w n="1.2">enfant</w> <w n="1.3">si</w> <w n="1.4">blonde</w> <w n="1.5">et</w> <w n="1.6">charmante</w></l>
						<l n="2" num="1.2"><w n="2.1">Que</w> <w n="2.2">je</w> <w n="2.3">trouvai</w> <w n="2.4">sur</w> <w n="2.5">mon</w> <w n="2.6">chemin</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Soleil</w> <w n="3.2">éteint</w>, <w n="3.3">lumière</w> <w n="3.4">absente</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Toi</w> <w n="4.2">dont</w> <w n="4.3">la</w> <w n="4.4">paupière</w> <w n="4.5">tremblante</w></l>
						<l n="5" num="1.5"><w n="5.1">Cherche</w> <w n="5.2">mon</w> <w n="5.3">regard</w>, <w n="5.4">mais</w> <w n="5.5">en</w> <w n="5.6">vain</w>.</l>
					</lg>
					<lg n="2">
						<l n="6" num="2.1"><w n="6.1">Tu</w> <w n="6.2">ne</w> <w n="6.3">verras</w> <w n="6.4">pas</w> <w n="6.5">la</w> <w n="6.6">nature</w>.</l>
						<l n="7" num="2.2"><w n="7.1">Les</w> <w n="7.2">roses</w> <w n="7.3">et</w> <w n="7.4">le</w> <w n="7.5">blond</w> <w n="7.6">soleil</w>,</l>
						<l n="8" num="2.3"><w n="8.1">L</w>’<w n="8.2">Océan</w> <w n="8.3">jaloux</w> <w n="8.4">qui</w> <w n="8.5">murmure</w>.</l>
						<l n="9" num="2.4"><w n="9.1">Les</w> <w n="9.2">bois</w> <w n="9.3">touffus</w> <w n="9.4">et</w> <w n="9.5">la</w> <w n="9.6">verdure</w>.</l>
						<l n="10" num="2.5"><w n="10.1">Les</w> <w n="10.2">astres</w>, <w n="10.3">ces</w> <w n="10.4">diamans</w> <w n="10.5">du</w> <w n="10.6">ciel</w>.</l>
					</lg>
					<lg n="3">
						<l n="11" num="3.1"><w n="11.1">Ni</w> <w n="11.2">le</w> <w n="11.3">ruisseau</w> <w n="11.4">dans</w> <w n="11.5">la</w> <w n="11.6">vallée</w>.</l>
						<l n="12" num="3.2"><w n="12.1">Ni</w> <w n="12.2">la</w> <w n="12.3">neige</w> <w n="12.4">aux</w> <w n="12.5">monts</w> <w n="12.6">sourcilleux</w>.</l>
						<l n="13" num="3.3"><w n="13.1">Ni</w> <w n="13.2">sur</w> <w n="13.3">la</w> <w n="13.4">grève</w> <w n="13.5">désolée</w></l>
						<l n="14" num="3.4"><w n="14.1">La</w> <w n="14.2">mouette</w>, <w n="14.3">cette</w> <w n="14.4">plainte</w> <w n="14.5">ailée</w>.</l>
						<l n="15" num="3.5"><w n="15.1">N</w>’<w n="15.2">ont</w> <w n="15.3">pu</w> <w n="15.4">jamais</w> <w n="15.5">frapper</w> <w n="15.6">tes</w> <w n="15.7">yeux</w> !</l>
					</lg>
					<lg n="4">
						<l n="16" num="4.1"><w n="16.1">Autour</w> <w n="16.2">de</w> <w n="16.3">toi</w> <w n="16.4">sans</w> <w n="16.5">cesse</w> <w n="16.6">l</w>’<w n="16.7">ombre</w>,</l>
						<l n="17" num="4.2"><w n="17.1">Sans</w> <w n="17.2">cesse</w> <w n="17.3">autour</w> <w n="17.4">de</w> <w n="17.5">toi</w> <w n="17.6">la</w> <w n="17.7">nuit</w> ;</l>
						<l n="18" num="4.3"><w n="18.1">Jamais</w>, <w n="18.2">dissipant</w> <w n="18.3">la</w> <w n="18.4">pénombre</w>,</l>
						<l n="19" num="4.4"><w n="19.1">Dans</w> <w n="19.2">ton</w> <w n="19.3">cachot</w> <w n="19.4">si</w> <w n="19.5">triste</w> <w n="19.6">et</w> <w n="19.7">sombre</w></l>
						<l n="20" num="4.5"><w n="20.1">Un</w> <w n="20.2">chaud</w> <w n="20.3">rayon</w> <w n="20.4">d</w>’<w n="20.5">été</w> <w n="20.6">n</w>’<w n="20.7">a</w> <w n="20.8">lui</w> !</l>
					</lg>
					<lg n="5">
						<l n="21" num="5.1"><w n="21.1">Au</w> <w n="21.2">banquet</w> <w n="21.3">de</w> <w n="21.4">la</w> <w n="21.5">vie</w> <w n="21.6">humaine</w></l>
						<l n="22" num="5.2"><w n="22.1">Pourquoi</w> <w n="22.2">Dieu</w> <w n="22.3">t</w>’<w n="22.4">a</w>-<w n="22.5">t</w>-<w n="22.6">il</w> <w n="22.7">invité</w> ?</l>
						<l n="23" num="5.3"><w n="23.1">Toi</w> <w n="23.2">qui</w> <w n="23.3">devais</w> <w n="23.4">tramer</w> <w n="23.5">la</w> <w n="23.6">chaîne</w></l>
						<l n="24" num="5.4"><w n="24.1">Et</w> <w n="24.2">ne</w> <w n="24.3">connaître</w> <w n="24.4">que</w> <w n="24.5">la</w> <w n="24.6">peine</w></l>
						<l n="25" num="5.5"><w n="25.1">Jusqu</w>’<w n="25.2">au</w> <w n="25.3">tombeau</w> <w n="25.4">tant</w> <w n="25.5">souhaité</w> !</l>
					</lg>
					<ab type="dot">. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .</ab>
					<lg n="6">
						<l n="26" num="6.1"><w n="26.1">Après</w> <w n="26.2">tout</w>, <w n="26.3">frêle</w> <w n="26.4">enfant</w> <w n="26.5">si</w> <w n="26.6">blonde</w>.</l>
						<l n="27" num="6.2"><w n="27.1">Pourquoi</w> <w n="27.2">tant</w> <w n="27.3">pleurer</w> <w n="27.4">sur</w> <w n="27.5">ton</w> <w n="27.6">sort</w> ?</l>
						<l n="28" num="6.3"><w n="28.1">Sommes</w>-<w n="28.2">nous</w> <w n="28.3">pas</w> <w n="28.4">en</w> <w n="28.5">ce</w> <w n="28.6">bas</w> <w n="28.7">monde</w></l>
						<l n="29" num="6.4"><w n="29.1">Des</w> <w n="29.2">hochets</w>, <w n="29.3">dans</w> <w n="29.4">la</w> <w n="29.5">main</w> <w n="29.6">profonde</w></l>
						<l n="30" num="6.5"><w n="30.1">De</w> <w n="30.2">Dieu</w>, <w n="30.3">jusques</w> <w n="30.4">à</w> <w n="30.5">notre</w> <w n="30.6">mort</w> ?</l>
					</lg>
					<lg n="7">
						<l n="31" num="7.1"><w n="31.1">Tu</w> <w n="31.2">pris</w> <w n="31.3">ta</w> <w n="31.4">place</w> <w n="31.5">dans</w> <w n="31.6">la</w> <w n="31.7">vie</w></l>
						<l n="32" num="7.2"><w n="32.1">Sans</w> <w n="32.2">te</w> <w n="32.3">préoccuper</w> <w n="32.4">de</w> <w n="32.5">rien</w>,</l>
						<l n="33" num="7.3"><w n="33.1">Prêtant</w> <w n="33.2">une</w> <w n="33.3">oreille</w> <w n="33.4">ravie</w></l>
						<l n="34" num="7.4"><w n="34.1">A</w> <w n="34.2">la</w> <w n="34.3">céleste</w> <w n="34.4">mélodie</w></l>
						<l n="35" num="7.5"><w n="35.1">Sans</w> <w n="35.2">t</w>’<w n="35.3">occuper</w> <w n="35.4">du</w> <w n="35.5">musicien</w> !</l>
					</lg>
					<lg n="8">
						<l n="36" num="8.1"><w n="36.1">Tu</w> <w n="36.2">sais</w> <w n="36.3">l</w>’<w n="36.4">effet</w> <w n="36.5">et</w> <w n="36.6">non</w> <w n="36.7">les</w> <w n="36.8">causes</w> ;</l>
						<l n="37" num="8.2"><w n="37.1">Le</w> <w n="37.2">bien</w>, <w n="37.3">tu</w> <w n="37.4">le</w> <w n="37.5">sais</w>, <w n="37.6">non</w> <w n="37.7">le</w> <w n="37.8">mal</w> ;</l>
						<l n="38" num="8.3"><w n="38.1">De</w> <w n="38.2">nos</w> <w n="38.3">tristes</w> <w n="38.4">métamorphoses</w></l>
						<l n="39" num="8.4"><w n="39.1">Tu</w> <w n="39.2">n</w>’<w n="39.3">as</w> <w n="39.4">jamais</w> <w n="39.5">vu</w> <w n="39.6">que</w> <w n="39.7">les</w> <w n="39.8">roses</w>,</l>
						<l n="40" num="8.5"><w n="40.1">Jamais</w> <w n="40.2">le</w> <w n="40.3">vrai</w>, <w n="40.4">mais</w> <w n="40.5">Fidéal</w> !</l>
					</lg>
					<lg n="9">
						<l n="41" num="9.1"><w n="41.1">Ton</w> <w n="41.2">illusion</w> <w n="41.3">la</w> <w n="41.4">plus</w> <w n="41.5">chère</w>.</l>
						<l n="42" num="9.2"><w n="42.1">Ton</w> <w n="42.2">rêve</w> <w n="42.3">aimé</w> <w n="42.4">de</w> <w n="42.5">tous</w> <w n="42.6">les</w> <w n="42.7">jours</w>.</l>
						<l n="43" num="9.3"><w n="43.1">Tu</w> <w n="43.2">peux</w> <w n="43.3">les</w> <w n="43.4">suivre</w> <w n="43.5">sur</w> <w n="43.6">la</w> <w n="43.7">terre</w>.</l>
						<l n="44" num="9.4"><w n="44.1">Heureuse</w> <w n="44.2">enfant</w>, <w n="44.3">et</w> <w n="44.4">puis</w> <w n="44.5">ta</w> <w n="44.6">mère</w></l>
						<l n="45" num="9.5"><w n="45.1">Aura</w> <w n="45.2">pour</w> <w n="45.3">toi</w> <w n="45.4">vingt</w> <w n="45.5">ans</w> <w n="45.6">toujours</w> !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1842">Décembre 1842</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>