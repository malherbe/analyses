<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="BUS">
					<name>
						<forename>Alfred</forename>
						<surname>BUSQUET</surname>
					</name>
					<date from="1819" to="1883">1819-1883</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2356 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BUS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÉSIES</title>
						<author>Alfred Busquet</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URI">https ://archive.org/details/poesiespremieres00alfr/mode/2up</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title type="main">POÉSIES</title>
								<author>Alfred Busquet</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE HACHETTE ET Cie</publisher>
									<date when="1884">1884</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1884">1884</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface (AU LECTEUR) n’a pas été reprise.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension(…).</p>
					<p>Les tirets simples précédés et/ou suivis d’une espace ont été remplacés par des tirets demi-cadratin</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-04-17" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-04-17" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">TRISTESSES ET JOIES</head><div type="poem" key="BUS60">
					<head type="main">AVEU SINCÈRE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Vous</w> <w n="1.2">avez</w> <w n="1.3">une</w> <w n="1.4">main</w> <w n="1.5">de</w> <w n="1.6">reine</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Un</w> <w n="2.2">pied</w> <w n="2.3">délicat</w> <w n="2.4">et</w> <w n="2.5">mignon</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Des</w> <w n="3.2">dents</w> <w n="3.3">de</w> <w n="3.4">lait</w> <w n="3.5">qu</w>’<w n="3.6">on</w> <w n="3.7">voit</w> <w n="3.8">à</w> <w n="3.9">peine</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Une</w> <w n="4.2">chevelure</w> <w n="4.3">d</w>’<w n="4.4">ébène</w>.</l>
						<l n="5" num="1.5"><w n="5.1">Des</w> <w n="5.2">yeux</w> <w n="5.3">qui</w> <w n="5.4">n</w>’<w n="5.5">ont</w> <w n="5.6">jamais</w> <w n="5.7">dit</w> : <w n="5.8">Non</w> !</l>
					</lg>
					<lg n="2">
						<l n="6" num="2.1"><w n="6.1">Vous</w> <w n="6.2">avez</w> <w n="6.3">un</w> <w n="6.4">beau</w> <w n="6.5">corps</w> <w n="6.6">qui</w> <w n="6.7">ploie</w></l>
						<l n="7" num="2.2"><w n="7.1">Avec</w> <w n="7.2">un</w> <w n="7.3">balancement</w> <w n="7.4">doux</w>.</l>
						<l n="8" num="2.3"><w n="8.1">Un</w> <w n="8.2">sein</w> <w n="8.3">où</w> <w n="8.4">le</w> <w n="8.5">désir</w> <w n="8.6">se</w> <w n="8.7">noie</w>.</l>
						<l n="9" num="2.4"><w n="9.1">Une</w> <w n="9.2">peau</w> <w n="9.3">de</w> <w n="9.4">neige</w> <w n="9.5">et</w> <w n="9.6">de</w> <w n="9.7">soie</w>,</l>
						<l n="10" num="2.5"><w n="10.1">A</w> <w n="10.2">rendre</w> <w n="10.3">les</w> <w n="10.4">anges</w> <w n="10.5">jaloux</w>.</l>
					</lg>
					<lg n="3">
						<l n="11" num="3.1"><w n="11.1">Vous</w> <w n="11.2">avez</w> <w n="11.3">tout</w> !… <w n="11.4">mais</w> <w n="11.5">je</w> <w n="11.6">préfère</w></l>
						<l n="12" num="3.2">— <w n="12.1">De</w> <w n="12.2">moi</w> <w n="12.3">ne</w> <w n="12.4">prenez</w> <w n="12.5">pas</w> <w n="12.6">pitié</w>, —</l>
						<l n="13" num="3.3"><w n="13.1">La</w> <w n="13.2">marguerite</w> <w n="13.3">printanière</w></l>
						<l n="14" num="3.4"><w n="14.1">A</w> <w n="14.2">la</w> <w n="14.3">rose</w> <w n="14.4">la</w> <w n="14.5">plus</w> <w n="14.6">altière</w>,</l>
						<l n="15" num="3.5"><w n="15.1">A</w> <w n="15.2">votre</w> <w n="15.3">amour</w>, <w n="15.4">votre</w> <w n="15.5">amitié</w> !</l>
					</lg>
				</div></body></text></TEI>