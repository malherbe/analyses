<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="BUS">
					<name>
						<forename>Alfred</forename>
						<surname>BUSQUET</surname>
					</name>
					<date from="1819" to="1883">1819-1883</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2356 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BUS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÉSIES</title>
						<author>Alfred Busquet</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URI">https ://archive.org/details/poesiespremieres00alfr/mode/2up</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title type="main">POÉSIES</title>
								<author>Alfred Busquet</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE HACHETTE ET Cie</publisher>
									<date when="1884">1884</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1884">1884</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface (AU LECTEUR) n’a pas été reprise.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension(…).</p>
					<p>Les tirets simples précédés et/ou suivis d’une espace ont été remplacés par des tirets demi-cadratin</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-04-17" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-04-17" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">SUR LES CHEMINS</head><head type="sub_part">IMPRESSIONS DE VOYAGE</head><head type="sub_part">PORTUGAL-ESPAGNE</head><div type="poem" key="BUS4">
					<head type="number">III</head>
					<head type="main">APPARITION</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Les</w> <w n="1.2">yeux</w> <w n="1.3">modestement</w> <w n="1.4">baissés</w> <w n="1.5">comme</w> <w n="1.6">Mignon</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Une</w> <w n="2.2">grenade</w> <w n="2.3">en</w> <w n="2.4">fleur</w> <w n="2.5">dans</w> <w n="2.6">son</w> <w n="2.7">rouge</w> <w n="2.8">chignon</w>.</l>
						<l n="3" num="1.3"><w n="3.1">Le</w> <w n="3.2">pied</w> <w n="3.3">cambré</w>, <w n="3.4">la</w> <w n="3.5">taille</w> <w n="3.6">onduleuse</w> <w n="3.7">et</w> <w n="3.8">mouvante</w>,</l>
						<l n="4" num="1.4"><w n="4.1">La</w> <w n="4.2">mantille</w> <w n="4.3">serrée</w> <w n="4.4">autour</w> <w n="4.5">du</w> <w n="4.6">corps</w>, <w n="4.7">savante</w></l>
						<l n="5" num="1.5"><w n="5.1">A</w> <w n="5.2">marcher</w>, <w n="5.3">à</w> <w n="5.4">frôler</w>, <w n="5.5">vive</w> <w n="5.6">comme</w> <w n="5.7">un</w> <w n="5.8">oiseau</w>,</l>
						<l n="6" num="1.6"><w n="6.1">Le</w> <w n="6.2">passant</w> <w n="6.3">immobile</w> <w n="6.4">et</w> <w n="6.5">pris</w> <w n="6.6">dans</w> <w n="6.7">son</w> <w n="6.8">réseau</w>,</l>
						<l n="7" num="1.7"><w n="7.1">La</w> <hi rend="ital"><w n="7.2">novia</w></hi> <w n="7.3">suivait</w> <w n="7.4">le</w> <w n="7.5">chemin</w> <w n="7.6">de</w> <w n="7.7">l</w>’<w n="7.8">église</w> :</l>
					</lg>
					<lg n="2">
						<l n="8" num="2.1"><w n="8.1">A</w> <w n="8.2">dix</w> <w n="8.3">pas</w> <w n="8.4">en</w> <w n="8.5">arrière</w>, <w n="8.6">escortant</w> <w n="8.7">sa</w> <w n="8.8">promise</w>,</l>
						<l n="9" num="2.2"><w n="9.1">Respectueux</w> <w n="9.2">et</w> <w n="9.3">fier</w> <w n="9.4">comme</w> <w n="9.5">un</w> <w n="9.6">campéador</w>.</l>
						<l n="10" num="2.3"><w n="10.1">Jarret</w> <w n="10.2">tendu</w>, <w n="10.3">couvant</w> <w n="10.4">du</w> <w n="10.5">regard</w> <w n="10.6">son</w> <w n="10.7">trésor</w>,</l>
						<l n="11" num="2.4"><w n="11.1">Le</w> <w n="11.2">manteau</w> <w n="11.3">rejeté</w> <w n="11.4">sur</w> <w n="11.5">l</w>’<w n="11.6">épaule</w>, <w n="11.7">en</w> <w n="11.8">bravache</w>,</l>
						<l n="12" num="2.5"><w n="12.1">Soupçonneux</w>, <w n="12.2">inquiet</w>, <w n="12.3">mine</w> <w n="12.4">d</w>’<w n="12.5">aigle</w> <w n="12.6">à</w> <w n="12.7">moustache</w>,</l>
					</lg>
					<lg n="3">
						<l part="I" n="13" num="3.1"><w n="13.1">Le</w> <hi rend="ital"><w n="13.2">novio</w></hi> <w n="13.3">suivait</w> <w n="13.4">sa</w> <hi rend="ital"><w n="13.5">novia</w></hi> : </l>
						<l part="F" n="13" num="3.1"><w n="13.6">J</w>’<w n="13.7">allai</w></l>
						<l n="14" num="3.2"><w n="14.1">Me</w> <w n="14.2">cacher</w> <w n="14.3">dans</w> <w n="14.4">un</w> <w n="14.5">coin</w>, <w n="14.6">et</w> <w n="14.7">là</w>, <w n="14.8">je</w> <w n="14.9">contemplai</w>.</l>
						<l n="15" num="3.3"><w n="15.1">Pour</w> <w n="15.2">la</w> <w n="15.3">première</w> <w n="15.4">fois</w>, — <w n="15.5">souvenir</w> <w n="15.6">qui</w> <w n="15.7">m</w>’<w n="15.8">affole</w> !</l>
						<l n="16" num="3.4"><w n="16.1">Comme</w> <w n="16.2">les</w> <w n="16.3">amans</w> <w n="16.4">font</w> <w n="16.5">l</w>’<w n="16.6">amour</w>, <w n="16.7">à</w> <w n="16.8">l</w>’<w n="16.9">espagnole</w> :</l>
					</lg>
					<lg n="4">
						<l n="17" num="4.1"><w n="17.1">La</w> <hi rend="ital"><w n="17.2">novia</w></hi> <w n="17.3">s</w>’<w n="17.4">assit</w> <w n="17.5">d</w>’<w n="17.6">abord</w> <w n="17.7">sur</w> <w n="17.8">le</w> <w n="17.9">talon</w></l>
						<l n="18" num="4.2"><w n="18.1">Dans</w> <w n="18.2">la</w> <w n="18.3">nef</w>, <w n="18.4">et</w> <w n="18.5">sa</w> <w n="18.6">robe</w> <w n="18.7">à</w> <w n="18.8">l</w>’<w n="18.9">entour</w> <w n="18.10">fait</w> <w n="18.11">ballon</w> ;</l>
						<l n="19" num="4.3"><w n="19.1">Sur</w> <w n="19.2">le</w> <w n="19.3">flanc</w> <w n="19.4">inclinée</w> <w n="19.5">elle</w> <w n="19.6">joue</w>, <w n="19.7">elle</w> <w n="19.8">avance</w></l>
						<l n="20" num="4.4"><w n="20.1">Son</w> <w n="20.2">beau</w> <w n="20.3">corps</w>, — <w n="20.4">tel</w> <w n="20.5">un</w> <w n="20.6">lys</w> <w n="20.7">que</w> <w n="20.8">le</w> <w n="20.9">zéphyr</w> <w n="20.10">balance</w>.</l>
						<l n="21" num="4.5"><w n="21.1">Et</w> <w n="21.2">jette</w> <w n="21.3">à</w> <w n="21.4">son</w> <w n="21.5">promis</w> <w n="21.6">un</w> <w n="21.7">long</w> <w n="21.8">regard</w> <w n="21.9">brûlant</w></l>
						<l n="22" num="4.6"><w n="22.1">Qu</w>’<w n="22.2">on</w> <w n="22.3">lui</w> <w n="22.4">renvoie</w> <w n="22.5">avec</w> <w n="22.6">un</w> <w n="22.7">soin</w> <w n="22.8">tout</w> <w n="22.9">vigilant</w>.</l>
					</lg>
					<lg n="5">
						<l n="23" num="5.1"><w n="23.1">Ils</w> <w n="23.2">n</w>’<w n="23.3">en</w> <w n="23.4">sont</w> <w n="23.5">pas</w> <w n="23.6">à</w> <w n="23.7">l</w>’<hi rend="ital"><w n="23.8">Introït</w></hi>… <w n="23.9">et</w> <w n="23.10">leur</w> <w n="23.11">cœur</w> <w n="23.12">vole</w></l>
						<l n="24" num="5.2"><w n="24.1">De</w> <w n="24.2">l</w>’<w n="24.3">un</w> <w n="24.4">à</w> <w n="24.5">l</w>’<w n="24.6">autre</w>, <w n="24.7">sans</w> <w n="24.8">musique</w> <w n="24.9">ni</w> <w n="24.10">parole</w> :</l>
						<l n="25" num="5.3"><w n="25.1">Je</w> <w n="25.2">vois</w> <w n="25.3">sous</w> <w n="25.4">les</w> <w n="25.5">froufrous</w> <w n="25.6">pressés</w> <w n="25.7">de</w> <w n="25.8">l</w>’<w n="25.9">éventail</w></l>
						<l n="26" num="5.4"><w n="26.1">Un</w> <w n="26.2">doigt</w> <w n="26.3">blanc</w> <w n="26.4">appuyer</w> <w n="26.5">aux</w> <w n="26.6">lèvres</w> <w n="26.7">de</w> <w n="26.8">corail</w></l>
						<l part="I" n="27" num="5.5"><w n="27.1">Et</w> <w n="27.2">s</w>’<w n="27.3">envoler</w> : </l>
						<l part="F" n="27" num="5.5"><w n="27.4">Amour</w>, <w n="27.5">dominateur</w> <w n="27.6">du</w> <w n="27.7">monde</w>.</l>
						<l n="28" num="5.6"><w n="28.1">Ton</w> <w n="28.2">règne</w> <w n="28.3">ne</w> <w n="28.4">doit</w> <w n="28.5">pas</w> <w n="28.6">dans</w> <w n="28.7">une</w> <w n="28.8">nuit</w> <w n="28.9">profonde</w></l>
						<l n="29" num="5.7"><w n="29.1">S</w>’<w n="29.2">éteindre</w> <w n="29.3">de</w> <w n="29.4">longtemps</w>. <w n="29.5">Les</w> <w n="29.6">autres</w> <w n="29.7">dieux</w> <w n="29.8">sont</w> <w n="29.9">morts</w> ;</l>
						<l n="30" num="5.8"><w n="30.1">Toi</w>, <w n="30.2">tu</w> <w n="30.3">vivras</w> <w n="30.4">toujours</w>, <w n="30.5">j</w>’<w n="30.6">atteste</w> <w n="30.7">les</w> <w n="30.8">transports</w></l>
						<l n="31" num="5.9"><w n="31.1">De</w> <w n="31.2">ces</w> <w n="31.3">amans</w> <w n="31.4">épris</w> <w n="31.5">d</w>’<w n="31.6">une</w> <w n="31.7">ardeur</w> <w n="31.8">tout</w> <w n="31.9">antique</w>.</l>
						<l n="32" num="5.10"><w n="32.1">Sous</w> <w n="32.2">tes</w> <w n="32.3">yeux</w> <w n="32.4">bienveillans</w>. <w n="32.5">Église</w> <w n="32.6">catholique</w>.</l>
						<l n="33" num="5.11"><w n="33.1">Qui</w> <w n="33.2">sais</w>, <w n="33.3">dans</w> <w n="33.4">ces</w> <w n="33.5">pays</w> <w n="33.6">favorisés</w> <w n="33.7">du</w> <w n="33.8">ciel</w>,</l>
						<l n="34" num="5.12"><w n="34.1">Que</w> <w n="34.2">succomber</w> <w n="34.3">est</w> <w n="34.4">doux</w> <w n="34.5">et</w> <w n="34.6">péché</w> <w n="34.7">véniel</w>.</l>
					</lg>
					<closer>
						<dateline>
							<placeName>Vigo</placeName>,
							<date when="1859">1859</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>