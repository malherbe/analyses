<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="BUS">
					<name>
						<forename>Alfred</forename>
						<surname>BUSQUET</surname>
					</name>
					<date from="1819" to="1883">1819-1883</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2356 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BUS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÉSIES</title>
						<author>Alfred Busquet</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URI">https ://archive.org/details/poesiespremieres00alfr/mode/2up</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title type="main">POÉSIES</title>
								<author>Alfred Busquet</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE HACHETTE ET Cie</publisher>
									<date when="1884">1884</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1884">1884</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface (AU LECTEUR) n’a pas été reprise.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension(…).</p>
					<p>Les tirets simples précédés et/ou suivis d’une espace ont été remplacés par des tirets demi-cadratin</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-04-17" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-04-17" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CHOSES D’AUTREFOIS</head><div type="poem" key="BUS53">
					<head type="main">LES DIEUX ONT PRONONCÉ</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Les</w> <w n="1.2">dieux</w> <w n="1.3">ont</w> <w n="1.4">prononcé</w> : <w n="1.5">les</w> <w n="1.6">Gaulois</w> <w n="1.7">ont</w> <w n="1.8">vécu</w>,</l>
						<l n="2" num="1.2"><w n="2.1">L</w>’<w n="2.2">aigle</w> <w n="2.3">du</w> <w n="2.4">Capitole</w>, <w n="2.5">un</w> <w n="2.6">seul</w> <w n="2.7">instant</w> <w n="2.8">vaincu</w>,</l>
						<l n="3" num="1.3"><space unit="char" quantity="8"></space><w n="3.1">A</w> <w n="3.2">repris</w> <w n="3.3">sa</w> <w n="3.4">course</w> <w n="3.5">première</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Et</w> <w n="4.2">Vercingétorix</w>, <w n="4.3">qui</w> <w n="4.4">n</w>’<w n="4.5">a</w> <w n="4.6">plus</w> <w n="4.7">qu</w>’<w n="4.8">à</w> <w n="4.9">mourir</w></l>
						<l n="5" num="1.5"><w n="5.1">La</w> <w n="5.2">main</w> <w n="5.3">sur</w> <w n="5.4">son</w> <w n="5.5">épée</w>, <w n="5.6">attend</w> <w n="5.7">de</w> <w n="5.8">l</w>’<w n="5.9">avenir</w></l>
						<l n="6" num="1.6"><space unit="char" quantity="8"></space><w n="6.1">Des</w> <w n="6.2">vengeurs</w> <w n="6.3">nés</w> <w n="6.4">de</w> <w n="6.5">sa</w> <w n="6.6">poussière</w> !</l>
					</lg>
					<lg n="2">
						<l n="7" num="2.1"><w n="7.1">Avec</w> <w n="7.2">les</w> <w n="7.3">yeux</w> <w n="7.4">de</w> <w n="7.5">l</w>’<w n="7.6">âme</w>, <w n="7.7">il</w> <w n="7.8">les</w> <w n="7.9">voit</w>, <w n="7.10">ces</w> <w n="7.11">grands</w> <w n="7.12">cœurs</w>.</l>
						<l n="8" num="2.2"><w n="8.1">C</w>’<w n="8.2">est</w> <w n="8.3">Clovis</w> <w n="8.4">le</w> <w n="8.5">premier</w>, <w n="8.6">avec</w> <w n="8.7">ses</w> <w n="8.8">Franks</w> <w n="8.9">vainqueurs</w>,</l>
						<l n="9" num="2.3"><space unit="char" quantity="8"></space><w n="9.1">C</w>’<w n="9.2">est</w> <w n="9.3">Karl</w> <w n="9.4">fatal</w> <w n="9.5">aux</w> <w n="9.6">Abdérames</w>,</l>
						<l n="10" num="2.4"><w n="10.1">C</w>’<w n="10.2">est</w> <w n="10.3">Charlemagne</w> <w n="10.4">et</w> <w n="10.5">puis</w> <w n="10.6">une</w> <w n="10.7">fille</w> <w n="10.8">des</w> <w n="10.9">champs</w></l>
						<l n="11" num="2.5"><w n="11.1">Avec</w> <w n="11.2">son</w> <w n="11.3">air</w> <w n="11.4">de</w> <w n="11.5">vierge</w> <w n="11.6">et</w> <w n="11.7">ses</w> <w n="11.8">grands</w> <w n="11.9">yeux</w> <w n="11.10">touchans</w></l>
						<l n="12" num="2.6"><space unit="char" quantity="8"></space><w n="12.1">Dont</w> <w n="12.2">des</w> <w n="12.3">tigres</w> <w n="12.4">ont</w> <w n="12.5">bu</w> <w n="12.6">les</w> <w n="12.7">larmes</w> ;</l>
					</lg>
					<lg n="3">
						<l n="13" num="3.1"><w n="13.1">C</w>’<w n="13.2">est</w> <w n="13.3">Duguesclin</w>, <w n="13.4">Villars</w>, <w n="13.5">Napoléon</w>, <w n="13.6">c</w>’<w n="13.7">est</w> <w n="13.8">vous</w></l>
						<l n="14" num="3.2"><w n="14.1">Que</w> <w n="14.2">pour</w> <w n="14.3">ces</w> <w n="14.4">fiers</w> <w n="14.5">desseins</w> <w n="14.6">Dieu</w> <w n="14.7">choisit</w> <w n="14.8">entre</w> <w n="14.9">tous</w></l>
						<l n="15" num="3.3"><space unit="char" quantity="8"></space><w n="15.1">Afin</w> <w n="15.2">que</w> <w n="15.3">la</w> <w n="15.4">France</w> <w n="15.5">fût</w> <w n="15.6">Reine</w>,</l>
						<l n="16" num="3.4"><w n="16.1">Et</w>, <w n="16.2">déposant</w> <w n="16.3">un</w> <w n="16.4">jour</w> <w n="16.5">le</w> <w n="16.6">glaive</w> <w n="16.7">détesté</w>.</l>
						<l n="17" num="3.5"><w n="17.1">Donnât</w> <w n="17.2">la</w> <w n="17.3">paix</w> <w n="17.4">au</w> <w n="17.5">monde</w> <w n="17.6">avec</w> <w n="17.7">la</w> <w n="17.8">Liberté</w>,</l>
						<l n="18" num="3.6"><space unit="char" quantity="8"></space><w n="18.1">Dans</w> <w n="18.2">sa</w> <w n="18.3">force</w> <w n="18.4">auguste</w> <w n="18.5">et</w> <w n="18.6">sereine</w> !</l>
					</lg>
				</div></body></text></TEI>