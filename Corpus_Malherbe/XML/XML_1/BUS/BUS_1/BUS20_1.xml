<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="BUS">
					<name>
						<forename>Alfred</forename>
						<surname>BUSQUET</surname>
					</name>
					<date from="1819" to="1883">1819-1883</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2356 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BUS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÉSIES</title>
						<author>Alfred Busquet</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URI">https ://archive.org/details/poesiespremieres00alfr/mode/2up</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title type="main">POÉSIES</title>
								<author>Alfred Busquet</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE HACHETTE ET Cie</publisher>
									<date when="1884">1884</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1884">1884</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface (AU LECTEUR) n’a pas été reprise.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension(…).</p>
					<p>Les tirets simples précédés et/ou suivis d’une espace ont été remplacés par des tirets demi-cadratin</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-04-17" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-04-17" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">SUR LES CHEMINS</head><head type="sub_part">IMPRESSIONS DE VOYAGE</head><head type="sub_part">PORTUGAL-ESPAGNE</head><head type="main_subpart">XVIII</head><head type="main_subpart">A FIGARO</head><div type="poem" key="BUS20">
						<head type="number">II</head>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">Vrai</w> ! <w n="1.2">je</w> <w n="1.3">n</w>’<w n="1.4">y</w> <w n="1.5">comptais</w> <w n="1.6">plus</w>. <w n="1.7">Pourtant</w> <w n="1.8">j</w>’<w n="1.9">étais</w> <w n="1.10">certain</w></l>
							<l n="2" num="1.2"><w n="2.1">Qu</w>’<w n="2.2">il</w> <w n="2.3">était</w> <w n="2.4">demeuré</w> <w n="2.5">quelqu</w>’<w n="2.6">un</w> <w n="2.7">de</w> <w n="2.8">ta</w> <w n="2.9">famille</w>…</l>
							<l n="3" num="1.3"><w n="3.1">Un</w> <w n="3.2">homme</w> <w n="3.3">aux</w> <w n="3.4">yeux</w> <w n="3.5">béats</w>… (<w n="3.6">c</w>’<w n="3.7">était</w> <w n="3.8">un</w> <w n="3.9">sacristain</w>)</l>
							<l n="4" num="1.4"><w n="4.1">S</w>’<w n="4.2">approche</w> <w n="4.3">et</w> <w n="4.4">puis</w> <w n="4.5">me</w> <w n="4.6">dit</w> : <w n="4.7">N</w>’<w n="4.8">est</w>-<w n="4.9">elle</w> <w n="4.10">pas</w> <w n="4.11">gentille</w>,</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1"><w n="5.1">Seigneur</w> ? <w n="5.2">Quel</w> <w n="5.3">pied</w> ! <w n="5.4">quel</w> <w n="5.5">feu</w> ! <w n="5.6">Quelle</w> <w n="5.7">démarche</w> <w n="5.8">agile</w> !</l>
							<l n="6" num="2.2"><w n="6.1">Quel</w> <w n="6.2">corsage</w> <w n="6.3">opulent</w> <w n="6.4">et</w> <w n="6.5">quel</w> <w n="6.6">regard</w> <w n="6.7">hautain</w> !</l>
							<l n="7" num="2.3"><w n="7.1">Mais</w> <w n="7.2">jamais</w> <w n="7.3">l</w>’<w n="7.4">étranger</w> <w n="7.5">n</w>’<w n="7.6">essuya</w> <w n="7.7">son</w> <w n="7.8">dédain</w>…</l>
							<l n="8" num="2.4">— <w n="8.1">Vous</w> <w n="8.2">la</w> <w n="8.3">connaissez</w> <w n="8.4">donc</w> ? — <w n="8.5">Garamba</w>, <w n="8.6">c</w>’<w n="8.7">est</w> <w n="8.8">ma</w> <w n="8.9">fille</w> !</l>
						</lg>
						<lg n="3">
							<l n="9" num="3.1"><w n="9.1">Son</w> <w n="9.2">logis</w> <w n="9.3">est</w> <w n="9.4">tout</w> <w n="9.5">près</w>… <hi rend="ital"> <w n="9.6">Calle</w> <w n="9.7">de</w> <choice reason="analysis" type="false_rhyme" hand="RR"><sic>Gallegas</sic><corr source="none"><w n="9.8">Gallegos</w></corr></choice></hi>,</l>
							<l n="10" num="3.2"><w n="10.1">Ça</w> <w n="10.2">loge</w> ! <w n="10.3">ça</w> <w n="10.4">n</w>’<w n="10.5">est</w> <w n="10.6">point</w> <w n="10.7">ainsi</w> <w n="10.8">que</w> <w n="10.9">ces</w> <w n="10.10">Margots</w>,</l>
							<l n="11" num="3.3"><w n="11.1">Ces</w> <w n="11.2">Gitanes</w> <w n="11.3">que</w> <w n="11.4">Dieu</w> <w n="11.5">confonde</w> — <w n="11.6">et</w> <w n="11.7">qu</w>’<w n="11.8">il</w> <w n="11.9">m</w>’<w n="11.10">assiste</w>.</l>
						</lg>
						<lg n="4">
							<l n="12" num="4.1"><w n="12.1">Puis</w> <w n="12.2">il</w> <w n="12.3">s</w>’<w n="12.4">est</w> <w n="12.5">éloigné</w>, <w n="12.6">ce</w> <w n="12.7">papa</w> <w n="12.8">moraliste</w>.</l>
							<l n="13" num="4.2"><w n="13.1">Et</w> <w n="13.2">moi</w>, <w n="13.3">je</w> <w n="13.4">regardais</w> <w n="13.5">passer</w> <w n="13.6">en</w> <w n="13.7">tourbillons</w></l>
							<l n="14" num="4.3"><w n="14.1">Les</w> <w n="14.2">pieds</w> <w n="14.3">de</w> <w n="14.4">la</w> <w n="14.5">danseuse</w>, <w n="14.6">ailes</w> <w n="14.7">de</w> <w n="14.8">papillons</w> !</l>
						</lg>
					</div></body></text></TEI>