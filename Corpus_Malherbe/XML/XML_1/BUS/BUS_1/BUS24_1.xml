<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="BUS">
					<name>
						<forename>Alfred</forename>
						<surname>BUSQUET</surname>
					</name>
					<date from="1819" to="1883">1819-1883</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2356 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BUS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÉSIES</title>
						<author>Alfred Busquet</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URI">https ://archive.org/details/poesiespremieres00alfr/mode/2up</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title type="main">POÉSIES</title>
								<author>Alfred Busquet</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE HACHETTE ET Cie</publisher>
									<date when="1884">1884</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1884">1884</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface (AU LECTEUR) n’a pas été reprise.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension(…).</p>
					<p>Les tirets simples précédés et/ou suivis d’une espace ont été remplacés par des tirets demi-cadratin</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-04-17" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-04-17" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BUS24">
				<head type="main">DUO D’AMOUR</head>
				<head type="sub_2">Traduit de la très-excellente histoire du Marchand de Venise <lb></lb>par William Shakespeare</head>
				<lg n="1">
					<head type="main">LORENZO</head>
					<l n="1" num="1.1"><w n="1.1">La</w> <w n="1.2">lune</w> <w n="1.3">resplendit</w> ! <w n="1.4">Par</w> <w n="1.5">une</w> <w n="1.6">nuit</w> <w n="1.7">pareille</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Tandis</w> <w n="2.2">que</w> <w n="2.3">le</w> <w n="2.4">zéphyr</w> <w n="2.5">baisait</w> <w n="2.6">tout</w> <w n="2.7">doucement</w></l>
					<l n="3" num="1.3"><w n="3.1">Les</w> <w n="3.2">arbres</w> <w n="3.3">sans</w> <w n="3.4">qu</w>’<w n="3.5">un</w> <w n="3.6">bruit</w> <w n="3.7">vînt</w> <w n="3.8">offenser</w> <w n="3.9">l</w>’<w n="3.10">oreille</w>,</l>
					<l n="4" num="1.4"><w n="4.1">La</w> <w n="4.2">lune</w> <w n="4.3">resplendit</w> — <w n="4.4">par</w> <w n="4.5">une</w> <w n="4.6">nuit</w> <w n="4.7">pareille</w>,</l>
					<l n="5" num="1.5"><w n="5.1">Troylus</w> <w n="5.2">est</w> <w n="5.3">monté</w> <w n="5.4">sur</w> <w n="5.5">le</w> <w n="5.6">rempart</w> <w n="5.7">fumant</w>.</l>
				</lg>
				<lg n="2">
					<l n="6" num="2.1"><w n="6.1">La</w> <w n="6.2">lune</w> <w n="6.3">resplendit</w> ! <w n="6.4">Par</w> <w n="6.5">une</w> <w n="6.6">nuit</w> <w n="6.7">pareille</w>.</l>
					<l n="7" num="2.2"><w n="7.1">Il</w> <w n="7.2">mit</w> <w n="7.3">dans</w> <w n="7.4">un</w> <w n="7.5">baiser</w> <w n="7.6">son</w> <w n="7.7">pauvre</w> <w n="7.8">cœur</w> <w n="7.9">aimant</w>.</l>
					<l n="8" num="2.3"><w n="8.1">Et</w> <w n="8.2">vers</w> <w n="8.3">sa</w> <w n="8.4">Cressida</w> <w n="8.5">qui</w> <w n="8.6">reposait</w> <w n="8.7">vermeille</w>,</l>
					<l n="9" num="2.4"><w n="9.1">La</w> <w n="9.2">lune</w> <w n="9.3">resplendit</w> — <w n="9.4">par</w> <w n="9.5">une</w> <w n="9.6">nuit</w> <w n="9.7">pareille</w>.</l>
					<l n="10" num="2.5"><w n="10.1">Vers</w> <w n="10.2">les</w> <w n="10.3">tentes</w> <w n="10.4">des</w> <w n="10.5">Grecs</w> <w n="10.6">l</w>’<w n="10.7">envoya</w> <w n="10.8">mollement</w>.</l>
				</lg>
				<lg n="3">
					<head type="main">JESSICA</head>
					<l n="11" num="3.1"><w n="11.1">La</w> <w n="11.2">lune</w> <w n="11.3">resplendit</w> ! <w n="11.4">Par</w> <w n="11.5">une</w> <w n="11.6">nuit</w> <w n="11.7">pareille</w>.</l>
					<l n="12" num="3.2"><w n="12.1">Au</w> <w n="12.2">rendez</w>-<w n="12.3">vous</w> <w n="12.4">d</w>’<w n="12.5">amour</w> <w n="12.6">Thisbé</w>, <w n="12.7">vers</w> <w n="12.8">son</w> <w n="12.9">amant</w>,</l>
					<l n="13" num="3.3"><w n="13.1">Gourait</w>… <w n="13.2">son</w> <w n="13.3">pied</w> <w n="13.4">rasait</w> <w n="13.5">les</w> <w n="13.6">fleurs</w> <w n="13.7">comme</w> <w n="13.8">une</w> <w n="13.9">abeille</w>.</l>
					<l n="14" num="3.4"><w n="14.1">La</w> <w n="14.2">lune</w> <w n="14.3">resplendit</w> — <w n="14.4">par</w> <w n="14.5">une</w> <w n="14.6">nuit</w> <w n="14.7">pareille</w>,</l>
					<l n="15" num="3.5"><w n="15.1">Elle</w> <w n="15.2">vit</w> <w n="15.3">le</w> <w n="15.4">lion</w> — <w n="15.5">et</w> <w n="15.6">s</w>’<w n="15.7">enfuit</w> <w n="15.8">follement</w> !</l>
				</lg>
				<lg n="4">
					<head type="main">LORENZO</head>
					<l n="16" num="4.1"><w n="16.1">La</w> <w n="16.2">lune</w> <w n="16.3">resplendit</w> ! <w n="16.4">Par</w> <w n="16.5">une</w> <w n="16.6">nuit</w> <w n="16.7">pareille</w>,</l>
					<l n="17" num="4.2"><w n="17.1">Didon</w> <w n="17.2">était</w> <w n="17.3">debout</w> <w n="17.4">au</w> <w n="17.5">bord</w> <w n="17.6">du</w> <w n="17.7">flot</w> <w n="17.8">dormant</w>.</l>
					<l n="18" num="4.3"><w n="18.1">Elle</w> <w n="18.2">agitait</w> <w n="18.3">dans</w> <w n="18.4">l</w>’<w n="18.5">air</w> <w n="18.6">le</w> <w n="18.7">saule</w> <w n="18.8">qui</w> <w n="18.9">conseille</w>.</l>
					<l n="19" num="4.4"><w n="19.1">La</w> <w n="19.2">lune</w> <w n="19.3">resplendit</w> — <w n="19.4">par</w> <w n="19.5">une</w> <w n="19.6">nuit</w> <w n="19.7">pareille</w>.</l>
					<l n="20" num="4.5"><w n="20.1">Et</w> <w n="20.2">suppliait</w> <w n="20.3">l</w>’<w n="20.4">ingrat</w> <w n="20.5">qui</w> <w n="20.6">fuit</w> <w n="20.7">traîtreusement</w>.</l>
				</lg>
				<lg n="5">
					<head type="main">JESSICA</head>
					<l n="21" num="5.1"><w n="21.1">La</w> <w n="21.2">lune</w> <w n="21.3">resplendit</w> ! <w n="21.4">Par</w> <w n="21.5">une</w> <w n="21.6">nuit</w> <w n="21.7">pareille</w>,</l>
					<l n="22" num="5.2"><w n="22.1">Médée</w> <w n="22.2">allait</w> <w n="22.3">cueillir</w> <w n="22.4">l</w>’<w n="22.5">eringium</w> <w n="22.6">charmant</w>,</l>
					<l n="23" num="5.3"><w n="23.1">Par</w> <w n="23.2">les</w> <w n="23.3">monts</w> <w n="23.4">ténébreux</w>, <w n="23.5">elle</w> <w n="23.6">emplit</w> <w n="23.7">sa</w> <w n="23.8">corbeille</w>,</l>
					<l n="24" num="5.4"><w n="24.1">La</w> <w n="24.2">lune</w> <w n="24.3">resplendit</w> — <w n="24.4">par</w> <w n="24.5">une</w> <w n="24.6">nuit</w> <w n="24.7">pareille</w>,</l>
					<l n="25" num="5.5"><w n="25.1">Œson</w> <w n="25.2">meurt</w> <w n="25.3">et</w> <w n="25.4">renaît</w> <w n="25.5">de</w> <w n="25.6">son</w> <w n="25.7">égorgement</w>.</l>
				</lg>
				<lg n="6">
					<head type="main">LORENZO</head>
					<l n="26" num="6.1"><w n="26.1">La</w> <w n="26.2">lune</w> <w n="26.3">resplendit</w> ! <w n="26.4">Par</w> <w n="26.5">une</w> <w n="26.6">nuit</w> <w n="26.7">pareille</w>,</l>
					<l n="27" num="6.2"><w n="27.1">Jessica</w>, <w n="27.2">ma</w> <w n="27.3">beauté</w>, <w n="27.4">quitta</w> <w n="27.5">furtivement</w></l>
					<l n="28" num="6.3"><w n="28.1">Son</w> <w n="28.2">vieux</w> <w n="28.3">père</w>, <w n="28.4">et</w> <w n="28.5">tandis</w> <w n="28.6">que</w> <w n="28.7">Venise</w> <w n="28.8">s</w>’<w n="28.9">éveille</w>,</l>
					<l n="29" num="6.4"><w n="29.1">La</w> <w n="29.2">lune</w> <w n="29.3">resplendit</w> — <w n="29.4">par</w> <w n="29.5">une</w> <w n="29.6">nuit</w> <w n="29.7">pareille</w>,</l>
					<l n="30" num="6.5"><w n="30.1">Elle</w> <w n="30.2">courait</w> <w n="30.3">sans</w> <w n="30.4">peur</w> <w n="30.5">au</w> <w n="30.6">bras</w> <w n="30.7">de</w> <w n="30.8">son</w> <w n="30.9">galant</w>.</l>
				</lg>
				<lg n="7">
					<head type="main">JESSICA</head>
					<l n="31" num="7.1"><w n="31.1">La</w> <w n="31.2">lune</w> <w n="31.3">resplendit</w> ! <w n="31.4">Par</w> <w n="31.5">une</w> <w n="31.6">nuit</w> <w n="31.7">pareille</w>.</l>
					<l n="32" num="7.2"><w n="32.1">Le</w> <w n="32.2">jeune</w> <w n="32.3">Lorenzo</w> <w n="32.4">jurait</w> — <w n="32.5">toujours</w> <w n="32.6">il</w> <w n="32.7">ment</w>,</l>
					<l n="33" num="7.3"><w n="33.1">De</w> <w n="33.2">n</w>’<w n="33.3">aimer</w> <w n="33.4">qu</w>’<w n="33.5">une</w> <w n="33.6">fois</w> <w n="33.7">sa</w> <w n="33.8">beauté</w> <w n="33.9">nonpareille</w>.</l>
					<l n="34" num="7.4"><w n="34.1">La</w> <w n="34.2">lune</w> <w n="34.3">resplendit</w> — <w n="34.4">par</w> <w n="34.5">une</w> <w n="34.6">nuit</w> <w n="34.7">pareille</w>.</l>
					<l n="35" num="7.5"><w n="35.1">Le</w> <w n="35.2">jeune</w> <w n="35.3">Lorenzo</w> <w n="35.4">mentait</w> <w n="35.5">affreusement</w>.</l>
				</lg>
				<lg n="8">
					<head type="main">LORENZO</head>
					<l n="36" num="8.1"><w n="36.1">La</w> <w n="36.2">lune</w> <w n="36.3">resplendit</w> ! <w n="36.4">Par</w> <w n="36.5">une</w> <w n="36.6">nuit</w> <w n="36.7">pareille</w>,</l>
					<l n="37" num="8.2"><w n="37.1">Jessica</w> <w n="37.2">mon</w> <w n="37.3">amour</w>, <w n="37.4">Jessica</w> <w n="37.5">mon</w> <w n="37.6">tourment</w>,</l>
					<l n="38" num="8.3"><w n="38.1">Affligeait</w> <w n="38.2">Lorenzo</w> <w n="38.3">dont</w> <w n="38.4">le</w> <w n="38.5">cœur</w> <w n="38.6">s</w>’<w n="38.7">émerveille</w>.</l>
					<l n="39" num="8.4"><w n="39.1">La</w> <w n="39.2">lune</w> <w n="39.3">resplendit</w> — <w n="39.4">par</w> <w n="39.5">une</w> <w n="39.6">nuit</w> <w n="39.7">pareille</w>,</l>
					<l n="40" num="8.5"><w n="40.1">De</w> <w n="40.2">sentir</w> <w n="40.3">sa</w> <w n="40.4">blessure</w> <w n="40.5">et</w> <w n="40.6">de</w> <w n="40.7">la</w> <w n="40.8">chérir</w> <w n="40.9">tant</w>.</l>
				</lg>
				<lg n="9">
					<head type="main">LORENZO. — JESSICA</head>
					<l n="41" num="9.1"><w n="41.1">La</w> <w n="41.2">lune</w> <w n="41.3">resplendit</w> ! <w n="41.4">Par</w> <w n="41.5">une</w> <w n="41.6">nuit</w> <w n="41.7">pareille</w>.</l>
					<l n="42" num="9.2"><w n="42.1">Prolongeons</w> <w n="42.2">notre</w> <w n="42.3">extase</w> <w n="42.4">et</w> <w n="42.5">notre</w> <w n="42.6">aveuglement</w>.</l>
					<l n="43" num="9.3"><w n="43.1">Ne</w> <w n="43.2">blessons</w> <w n="43.3">pas</w> <w n="43.4">l</w>’<w n="43.5">amour</w> <w n="43.6">qui</w> <w n="43.7">déjà</w> <w n="43.8">nous</w> <w n="43.9">surveille</w>.</l>
					<l n="44" num="9.4"><w n="44.1">La</w> <w n="44.2">lune</w> <w n="44.3">resplendit</w> — <w n="44.4">par</w> <w n="44.5">une</w> <w n="44.6">nuit</w> <w n="44.7">pareille</w>.</l>
					<l n="45" num="9.5"><w n="45.1">Aimons</w>-<w n="45.2">nous</w>, <w n="45.3">aimons</w>-<w n="45.4">nous</w> <w n="45.5">jusqu</w>’<w n="45.6">au</w> <w n="45.7">dernier</w> <w n="45.8">moment</w>.</l>
				</lg>
				<closer>
					<placeName>Londres</placeName>.
				</closer>
			</div></body></text></TEI>