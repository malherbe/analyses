<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="BUS">
					<name>
						<forename>Alfred</forename>
						<surname>BUSQUET</surname>
					</name>
					<date from="1819" to="1883">1819-1883</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2356 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BUS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÉSIES</title>
						<author>Alfred Busquet</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URI">https ://archive.org/details/poesiespremieres00alfr/mode/2up</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title type="main">POÉSIES</title>
								<author>Alfred Busquet</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE HACHETTE ET Cie</publisher>
									<date when="1884">1884</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1884">1884</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface (AU LECTEUR) n’a pas été reprise.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension(…).</p>
					<p>Les tirets simples précédés et/ou suivis d’une espace ont été remplacés par des tirets demi-cadratin</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-04-17" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-04-17" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">SUR LES CHEMINS</head><head type="sub_part">IMPRESSIONS DE VOYAGE</head><head type="sub_part">PORTUGAL-ESPAGNE</head><div type="poem" key="BUS5">
					<head type="number">IV</head>
					<head type="main">LE DIABLE SOMMELIER</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Les</w> <w n="1.2">compaignons</w> <w n="1.3">de</w> <w n="1.4">Sanche</w>, <w n="1.5">roi</w> <w n="1.6">d</w>’<w n="1.7">Espaigne</w>,</l>
						<l n="2" num="1.2"><w n="2.1">S</w>’<w n="2.2">en</w> <w n="2.3">revenaient</w> <w n="2.4">du</w> <w n="2.5">pays</w> <w n="2.6">sarrazin</w></l>
						<l n="3" num="1.3"><w n="3.1">En</w> <w n="3.2">grand</w> <w n="3.3">meschief</w>… <w n="3.4">Sanche</w> <w n="3.5">les</w> <w n="3.6">accompaigne</w></l>
						<l n="4" num="1.4"><w n="4.1">Teste</w> <w n="4.2">baissée</w> <w n="4.3">et</w> <w n="4.4">la</w> <w n="4.5">douleur</w> <w n="4.6">au</w> <w n="4.7">sein</w>.</l>
						<l n="5" num="1.5"><w n="5.1">Il</w> <w n="5.2">n</w>’<w n="5.3">est</w> <w n="5.4">pour</w> <w n="5.5">lui</w> <w n="5.6">chanson</w> <w n="5.7">ne</w> <w n="5.8">mélodie</w></l>
						<l n="6" num="1.6"><w n="6.1">Qui</w> <w n="6.2">lui</w> <w n="6.3">pourrait</w> <w n="6.4">apaiser</w> <w n="6.5">son</w> <w n="6.6">remords</w>,</l>
						<l n="7" num="1.7"><w n="7.1">La</w> <w n="7.2">soif</w> <w n="7.3">le</w> <w n="7.4">brûle</w> <w n="7.5">ainsi</w> <w n="7.6">qu</w>’<w n="7.7">un</w> <w n="7.8">incendie</w>,</l>
						<l n="8" num="1.8">« <w n="8.1">Las</w> ! <w n="8.2">dit</w> <w n="8.3">le</w> <w n="8.4">Roi</w>, <w n="8.5">je</w> <w n="8.6">voudrais</w> <w n="8.7">être</w> <w n="8.8">mort</w> !</l>
						<l n="9" num="1.9">« <w n="9.1">C</w>’<w n="9.2">est</w> <w n="9.3">grand</w>’<w n="9.4">pitié</w> <w n="9.5">de</w> <w n="9.6">voir</w> <w n="9.7">mes</w> <w n="9.8">gentilshommes</w></l>
						<l n="10" num="1.10">« <w n="10.1">Se</w> <w n="10.2">débander</w> <w n="10.3">ainsi</w> <w n="10.4">par</w> <w n="10.5">les</w> <w n="10.6">chemins</w> !</l>
						<l n="11" num="1.11">« <w n="11.1">Soudan</w> <w n="11.2">maudit</w>, <w n="11.3">malheureux</w> <w n="11.4">que</w> <w n="11.5">nous</w> <w n="11.6">sommes</w> ! »</l>
						<l n="12" num="1.12"><w n="12.1">Et</w> <w n="12.2">ce</w> <w n="12.3">disant</w>, <w n="12.4">il</w> <w n="12.5">se</w> <w n="12.6">tordait</w> <w n="12.7">les</w> <w n="12.8">mains</w>.</l>
						<l n="13" num="1.13"><w n="13.1">Il</w> <w n="13.2">ajouta</w> : <w n="13.3">Par</w> <w n="13.4">le</w> <w n="13.5">mal</w> <w n="13.6">que</w> <w n="13.7">j</w>’<w n="13.8">endure</w></l>
						<l n="14" num="1.14"><w n="14.1">J</w>’<w n="14.2">ai</w> <w n="14.3">bien</w> <w n="14.4">gaigné</w>, <w n="14.5">je</w> <w n="14.6">crois</w>, <w n="14.7">le</w> <w n="14.8">Paradis</w>,</l>
						<l n="15" num="1.15"><w n="15.1">Mais</w> <w n="15.2">je</w> <w n="15.3">le</w> <w n="15.4">cède</w> <w n="15.5">et</w> <w n="15.6">je</w> <w n="15.7">ne</w> <w n="15.8">m</w>’<w n="15.9">en</w> <w n="15.10">dédis</w>,</l>
						<l n="16" num="1.16"><w n="16.1">A</w> <w n="16.2">qui</w> <w n="16.3">me</w> <w n="16.4">baille</w> <w n="16.5">une</w> <w n="16.6">goutte</w> <w n="16.7">d</w>’<w n="16.8">eau</w> <w n="16.9">pure</w>.</l>
						<l n="17" num="1.17">— <w n="17.1">Livre</w> <w n="17.2">le</w> <w n="17.3">gant</w> <w n="17.4">et</w> <w n="17.5">le</w> <w n="17.6">baston</w> <w n="17.7">aussi</w>,</l>
						<l n="18" num="1.18"><w n="18.1">Dit</w> <w n="18.2">un</w> <w n="18.3">baron</w> <w n="18.4">tout</w> <w n="18.5">vestu</w> <w n="18.6">d</w>’<w n="18.7">écarlate</w>.</l>
						<l n="19" num="1.19">— <w n="19.1">Je</w> <w n="19.2">te</w> <w n="19.3">les</w> <w n="19.4">livre</w>, <w n="19.5">a</w> <w n="19.6">dit</w> <w n="19.7">Sanche</w>, <w n="19.8">et</w> <w n="19.9">voici</w></l>
						<l n="20" num="1.20"><w n="20.1">Que</w> <w n="20.2">sous</w> <w n="20.3">ses</w> <w n="20.4">pieds</w> <w n="20.5">une</w> <w n="20.6">fontaine</w> <w n="20.7">éclate</w> :</l>
						<l n="21" num="1.21"><w n="21.1">Petits</w> <w n="21.2">poissons</w> <w n="21.3">y</w> <w n="21.4">reluisaient</w> <w n="21.5">dans</w> <w n="21.6">l</w>’<w n="21.7">eau</w>,</l>
						<l n="22" num="1.22"><w n="22.1">Rouges</w> <w n="22.2">et</w> <w n="22.3">vifs</w>, <w n="22.4">ainsi</w> <w n="22.5">que</w> <w n="22.6">salamandres</w>.</l>
						<l n="23" num="1.23">— <w n="23.1">Mes</w> <w n="23.2">compaignons</w>, <w n="23.3">gardons</w>-<w n="23.4">nous</w> <w n="23.5">du</w> <w n="23.6">panneau</w> ;</l>
						<l n="24" num="1.24"><w n="24.1">Dans</w> <w n="24.2">ce</w> <w n="24.3">ruissel</w>, <w n="24.4">j</w>’<w n="24.5">aperçois</w> <w n="24.6">des</w> <w n="24.7">filandres</w>.</l>
						<l n="25" num="1.25"><w n="25.1">N</w>’<w n="25.2">en</w> <w n="25.3">buvez</w> <w n="25.4">pas</w>. <w n="25.5">Or</w> <w n="25.6">çà</w>, <w n="25.7">monsieur</w> <w n="25.8">l</w>’<w n="25.9">abbé</w>,</l>
						<l n="26" num="1.26"><w n="26.1">Bénissez</w>-<w n="26.2">nous</w> <w n="26.3">cette</w> <w n="26.4">eau</w> <w n="26.5">contre</w> <w n="26.6">nature</w>.</l>
						<l n="27" num="1.27"><w n="27.1">L</w>’<w n="27.2">abbé</w> <w n="27.3">bénit</w>… <w n="27.4">mais</w> <w n="27.5">voici</w> <w n="27.6">l</w>’<w n="27.7">aventure</w>,</l>
						<l n="28" num="1.28"><w n="28.1">Le</w> <w n="28.2">Ganelon</w> <w n="28.3">soudain</w> <w n="28.4">s</w>’<w n="28.5">est</w> <w n="28.6">dérobé</w>.</l>
						<l n="29" num="1.29"><w n="29.1">Car</w> <w n="29.2">c</w>’<w n="29.3">était</w> <w n="29.4">lui</w>… <w n="29.5">Satanas</w> <w n="29.6">en</w> <w n="29.7">personne</w>,</l>
						<l n="30" num="1.30"><w n="30.1">Qui</w> <w n="30.2">s</w>’<w n="30.3">était</w> <w n="30.4">plu</w> <w n="30.5">dans</w> <w n="30.6">ce</w> <w n="30.7">tour</w> <w n="30.8">familier</w>.</l>
						<l n="31" num="1.31"><w n="31.1">Don</w> <w n="31.2">Sanche</w> <w n="31.3">en</w> <w n="31.4">rit</w>, <w n="31.5">et</w> <w n="31.6">tous</w>, <w n="31.7">comme</w> <w n="31.8">à</w> <w n="31.9">la</w> <w n="31.10">tonne</w>.</l>
						<l n="32" num="1.32"><w n="32.1">Ont</w> <w n="32.2">bu</w> <w n="32.3">de</w> <w n="32.4">l</w>’<w n="32.5">eau</w> <w n="32.6">du</w> <w n="32.7">Diable</w> <w n="32.8">sommelier</w>.</l>
					</lg>
					<closer>
						<dateline>
							<placeName>Santiago</placeName>,
							<date when="1859">1859</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>