<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="BUS">
					<name>
						<forename>Alfred</forename>
						<surname>BUSQUET</surname>
					</name>
					<date from="1819" to="1883">1819-1883</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2356 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BUS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÉSIES</title>
						<author>Alfred Busquet</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URI">https ://archive.org/details/poesiespremieres00alfr/mode/2up</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title type="main">POÉSIES</title>
								<author>Alfred Busquet</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE HACHETTE ET Cie</publisher>
									<date when="1884">1884</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1884">1884</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface (AU LECTEUR) n’a pas été reprise.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension(…).</p>
					<p>Les tirets simples précédés et/ou suivis d’une espace ont été remplacés par des tirets demi-cadratin</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-04-17" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-04-17" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CHOSES D’AUTREFOIS</head><div type="poem" key="BUS50">
					<head type="main">LE TIMBRE D’OR</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Moi</w>, <w n="1.2">je</w> <w n="1.3">dirai</w> <w n="1.4">sa</w> <w n="1.5">voix</w> <w n="1.6">douce</w> <w n="1.7">et</w> <w n="1.8">si</w> <w n="1.9">pénétrante</w> !…</l>
						<l n="2" num="1.2"><w n="2.1">Timbre</w> <w n="2.2">d</w>’<w n="2.3">or</w> <w n="2.4">de</w> <w n="2.5">Hugo</w>, <w n="2.6">de</w> <w n="2.7">Gérard</w> <w n="2.8">de</w> <w n="2.9">Nerval</w>,</l>
						<l n="3" num="1.3"><w n="3.1">De</w> <w n="3.2">Rogier</w>, <w n="3.3">de</w> <w n="3.4">Stadler</w>, <w n="3.5">de</w> <w n="3.6">Houssaye</w> <w n="3.7">et</w> <w n="3.8">Dorval</w>,</l>
						<l n="4" num="1.4"><w n="4.1">De</w> <w n="4.2">ce</w> <w n="4.3">groupe</w> <w n="4.4">d</w>’<w n="4.5">amis</w> <w n="4.6">que</w> <w n="4.7">la</w> <w n="4.8">Muse</w> <w n="4.9">apparente</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">O</w> <w n="5.2">douce</w> <w n="5.3">voix</w> !… <w n="5.4">soupir</w> <w n="5.5">de</w> <w n="5.6">flûte</w> <w n="5.7">au</w> <w n="5.8">fond</w> <w n="5.9">du</w> <w n="5.10">val</w> !</l>
						<l n="6" num="2.2"><w n="6.1">Écho</w> <w n="6.2">de</w> <w n="6.3">la</w> <w n="6.4">syrinx</w>, <w n="6.5">mélodie</w> <w n="6.6">enivrante</w>.</l>
						<l n="7" num="2.3"><w n="7.1">Pour</w> <w n="7.2">quel</w> <w n="7.3">divin</w> <w n="7.4">concert</w> <w n="7.5">votre</w> <w n="7.6">chœur</w> <w n="7.7">sans</w> <w n="7.8">rival</w></l>
						<l n="8" num="2.4"><w n="8.1">A</w>-<w n="8.2">t</w>-<w n="8.3">il</w> <w n="8.4">reçu</w> <w n="8.5">le</w> <w n="8.6">la</w> <w n="8.7">de</w> <w n="8.8">quelque</w> <w n="8.9">fée</w> <w n="8.10">errante</w> ?</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Toujours</w> <w n="9.2">jusqu</w>’<w n="9.3">au</w> <w n="9.4">tombeau</w> <w n="9.5">mon</w> <w n="9.6">âme</w> <w n="9.7">l</w>’<w n="9.8">entendra</w>.</l>
						<l n="10" num="3.2">— <w n="10.1">Tel</w> <w n="10.2">un</w> <w n="10.3">Brahmine</w> <w n="10.4">écoute</w> <w n="10.5">et</w> <w n="10.6">croit</w> <w n="10.7">entendre</w> <w n="10.8">Indra</w>,</l>
						<l n="11" num="3.3"><w n="11.1">La</w> <w n="11.2">chère</w> <w n="11.3">voix</w>, <w n="11.4">aux</w> <w n="11.5">sons</w> <w n="11.6">caressants</w> <w n="11.7">et</w> <w n="11.8">pareille</w></l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1"><w n="12.1">Aux</w> <w n="12.2">plaintives</w> <w n="12.3">rumeurs</w> <w n="12.4">des</w> <w n="12.5">bois</w>, <w n="12.6">quand</w> <w n="12.7">vient</w> <w n="12.8">le</w> <w n="12.9">jour</w>,</l>
						<l n="13" num="4.2"><w n="13.1">A</w> <w n="13.2">la</w> <w n="13.3">Guzla</w> <w n="13.4">qui</w> <w n="13.5">pleure</w> <w n="13.6">et</w> <w n="13.7">chante</w> <w n="13.8">un</w> <w n="13.9">chant</w> <w n="13.10">d</w>’<w n="13.11">amour</w></l>
						<l n="14" num="4.3"><w n="14.1">Lorsque</w> <w n="14.2">la</w> <w n="14.3">tribu</w> <w n="14.4">dort</w> <w n="14.5">et</w> <w n="14.6">que</w> <w n="14.7">l</w>’<w n="14.8">amante</w> <w n="14.9">veille</w>.</l>
					</lg>
					<p>Extrait du <hi rend="ital">Tombeau de Théophile Gautier</hi>.</p>
				</div></body></text></TEI>