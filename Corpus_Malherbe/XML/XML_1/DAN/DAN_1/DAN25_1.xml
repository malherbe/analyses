<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Coffret de Crusoé</title>
				<title type="medium">Édition électronique</title>
				<author key="DAN">
					<name>
						<forename>Louis</forename>
						<surname>DANTIN</surname>
					</name>
					<date from="1865" to="1945">1865-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2468 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2016</date>
				<idno type="local">DAN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Coffret de Crusoé</title>
						<author>Louis Dantin</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/louisdantinlecofretdecrusoee.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Le Coffret de Crusoé</title>
						<author>Louis Dantin</author>
						<imprint>
							<publisher>ÉDITIONS ALBERT LÉVESQUE</publisher>
							<date when="1932">1932</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1932">1932</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">VI</head><head type="main_part">CHANSON INTIME</head><div type="poem" key="DAN25">
						<head type="main">Sympathie astrale</head>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">Comme</w> <w n="1.2">des</w> <w n="1.3">astres</w> <w n="1.4">seuls</w> <w n="1.5">dans</w> <w n="1.6">les</w> <w n="1.7">éthers</w> <w n="1.8">sans</w> <w n="1.9">fin</w>,</l>
							<l n="2" num="1.2"><w n="2.1">Suivant</w> <w n="2.2">l</w>’<w n="2.3">orbe</w> <w n="2.4">cruel</w> <w n="2.5">dont</w> <w n="2.6">la</w> <w n="2.7">loi</w> <w n="2.8">les</w> <w n="2.9">captive</w>,</l>
							<l n="3" num="1.3"><w n="3.1">Rose</w>, <w n="3.2">nos</w> <w n="3.3">coeurs</w> <w n="3.4">erraient</w> <w n="3.5">par</w> <w n="3.6">la</w> <w n="3.7">route</w> <w n="3.8">pensive</w></l>
							<l n="4" num="1.4"><w n="4.1">Où</w> <w n="4.2">vont</w> <w n="4.3">les</w> <w n="4.4">coeurs</w> <w n="4.5">amis</w> <w n="4.6">qui</w> <w n="4.7">se</w> <w n="4.8">cherchent</w> <w n="4.9">en</w> <w n="4.10">vain</w>.</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1"><w n="5.1">Comme</w> <w n="5.2">des</w> <w n="5.3">astres</w> <w n="5.4">seuls</w> <w n="5.5">que</w> <w n="5.6">leur</w> <w n="5.7">flamme</w> <w n="5.8">consume</w>,</l>
							<l n="6" num="2.2"><w n="6.1">Et</w> <w n="6.2">qui</w>, <w n="6.3">dans</w> <w n="6.4">l</w>’<w n="6.5">infini</w>, <w n="6.6">mélancoliquement</w></l>
							<l n="7" num="2.3"><w n="7.1">Dispersent</w> <w n="7.2">les</w> <w n="7.3">rayons</w> <w n="7.4">où</w> <w n="7.5">saigne</w> <w n="7.6">leur</w> <w n="7.7">tourment</w></l>
							<l n="8" num="2.4"><w n="8.1">Sans</w> <w n="8.2">que</w> <w n="8.3">pour</w> <w n="8.4">leur</w> <w n="8.5">sourire</w> <w n="8.6">aucun</w> <w n="8.7">reflet</w> <w n="8.8">s</w>’<w n="8.9">allume</w> ;</l>
						</lg>
						<lg n="3">
							<l n="9" num="3.1"><w n="9.1">Rose</w>, <w n="9.2">nos</w> <w n="9.3">coeurs</w> <w n="9.4">erraient</w>, <w n="9.5">de</w> <w n="9.6">rêves</w> <w n="9.7">lacérés</w>,</l>
							<l n="10" num="3.2"><w n="10.1">Et</w> <w n="10.2">parmi</w> <w n="10.3">la</w> <w n="10.4">gaieté</w> <w n="10.5">de</w> <w n="10.6">la</w> <w n="10.7">tourbe</w> <w n="10.8">qui</w> <w n="10.9">passe</w>,</l>
							<l n="11" num="3.3"><w n="11.1">Nous</w> <w n="11.2">mourions</w> <w n="11.3">de</w> <w n="11.4">marcher</w> <w n="11.5">isolés</w> <w n="11.6">dans</w> <w n="11.7">l</w>’<w n="11.8">espace</w></l>
							<l n="12" num="3.4"><w n="12.1">Et</w> <w n="12.2">du</w> <w n="12.3">regret</w> <w n="12.4">latent</w> <w n="12.5">de</w> <w n="12.6">nous</w> <w n="12.7">être</w> <w n="12.8">ignorés</w>.</l>
						</lg>
						<lg n="4">
							<l n="13" num="4.1"><w n="13.1">Mais</w> <w n="13.2">un</w> <w n="13.3">jour</w>, <w n="13.4">j</w>’<w n="13.5">ai</w> <w n="13.6">senti</w> <w n="13.7">frémir</w> <w n="13.8">au</w> <w n="13.9">loin</w> <w n="13.10">ta</w> <w n="13.11">plainte</w> ;</l>
							<l n="14" num="4.2"><w n="14.1">Ta</w> <w n="14.2">lueur</w> <w n="14.3">a</w> <w n="14.4">percé</w>, <w n="14.5">rapide</w>, <w n="14.6">mon</w> <w n="14.7">exil</w>,</l>
							<l n="15" num="4.3"><w n="15.1">Et</w> <w n="15.2">dans</w> <w n="15.3">mon</w> <w n="15.4">être</w>, <w n="15.5">ému</w> <w n="15.6">d</w>’<w n="15.7">un</w> <w n="15.8">effluve</w> <w n="15.9">subtil</w>,</l>
							<l n="16" num="4.4"><w n="16.1">Chaque</w> <w n="16.2">atome</w> <w n="16.3">a</w> <w n="16.4">vibré</w> <w n="16.5">sous</w> <w n="16.6">l</w>’<w n="16.7">attraction</w> <w n="16.8">sainte</w>.</l>
						</lg>
						<lg n="5">
							<l n="17" num="5.1"><w n="17.1">Nous</w> <w n="17.2">nous</w> <w n="17.3">sommes</w> <w n="17.4">aimés</w> <w n="17.5">sans</w> <w n="17.6">nous</w> <w n="17.7">connaître</w> <w n="17.8">encor</w>,</l>
							<l n="18" num="5.2"><w n="18.1">Et</w> <w n="18.2">sans</w> <w n="18.3">que</w> <w n="18.4">le</w> <w n="18.5">baiser</w> <w n="18.6">eût</w> <w n="18.7">fiancé</w> <w n="18.8">nos</w> <w n="18.9">lèvres</w></l>
							<l n="19" num="5.3"><w n="19.1">Nos</w> <w n="19.2">âmes</w> <w n="19.3">se</w> <w n="19.4">donnaient</w> <w n="19.5">en</w> <w n="19.6">d</w>’<w n="19.7">électriques</w> <w n="19.8">fièvres</w></l>
							<l n="20" num="5.4"><w n="20.1">Et</w> <w n="20.2">nos</w> <w n="20.3">jeunes</w> <w n="20.4">désirs</w> <w n="20.5">chantaient</w> <w n="20.6">des</w> <w n="20.7">hymnes</w> <w n="20.8">d</w>’<w n="20.9">or</w>.</l>
						</lg>
						<lg n="6">
							<l n="21" num="6.1"><w n="21.1">Trop</w> <w n="21.2">loin</w> <w n="21.3">pour</w> <w n="21.4">que</w> <w n="21.5">nos</w> <w n="21.6">yeux</w> <w n="21.7">fondissent</w> <w n="21.8">leurs</w> <w n="21.9">prunelles</w>,</l>
							<l n="22" num="6.2"><w n="22.1">Nous</w> <w n="22.2">mirions</w> <w n="22.3">nos</w> <w n="22.4">pensers</w> <w n="22.5">comme</w> <w n="22.6">des</w> <w n="22.7">cristaux</w> <w n="22.8">purs</w>,</l>
							<l n="23" num="6.3"><w n="23.1">Extasiés</w> <w n="23.2">de</w> <w n="23.3">voir</w> <w n="23.4">dans</w> <w n="23.5">nos</w> <w n="23.6">rêves</w> <w n="23.7">obscurs</w></l>
							<l n="24" num="6.4"><w n="24.1">S</w>’<w n="24.2">allumer</w> <w n="24.3">le</w> <w n="24.4">flambeau</w> <w n="24.5">des</w> <w n="24.6">amours</w> <w n="24.7">éternelles</w>.</l>
						</lg>
						<ab type="dot">… ... … ... … ... … ... … ... … ... … ... … ... … ... …</ab>
						<lg n="7">
							<l n="25" num="7.1"><w n="25.1">Mais</w> <w n="25.2">déjà</w>, <w n="25.3">sans</w> <w n="25.4">pitié</w>, <w n="25.5">dans</w> <w n="25.6">notre</w> <w n="25.7">âpre</w> <w n="25.8">chemin</w></l>
							<l n="26" num="7.2"><w n="26.1">L</w>’<w n="26.2">inflexible</w> <w n="26.3">Destin</w> <w n="26.4">nous</w> <w n="26.5">entraînait</w> <w n="26.6">plus</w> <w n="26.7">vite</w> :</l>
							<l n="27" num="7.3"><w n="27.1">Et</w> <w n="27.2">nous</w> <w n="27.3">suivions</w> <w n="27.4">chacun</w> <w n="27.5">notre</w> <w n="27.6">fatal</w> <w n="27.7">orbite</w>,</l>
							<l n="28" num="7.4"><w n="28.1">Comme</w> <w n="28.2">des</w> <w n="28.3">astres</w> <w n="28.4">seuls</w> <w n="28.5">dans</w> <w n="28.6">les</w> <w n="28.7">éthers</w> <w n="28.8">sans</w> <w n="28.9">fin</w>…</l>
						</lg>
					</div></body></text></TEI>