<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Coffret de Crusoé</title>
				<title type="medium">Édition électronique</title>
				<author key="DAN">
					<name>
						<forename>Louis</forename>
						<surname>DANTIN</surname>
					</name>
					<date from="1865" to="1945">1865-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2468 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2016</date>
				<idno type="local">DAN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Coffret de Crusoé</title>
						<author>Louis Dantin</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/louisdantinlecofretdecrusoee.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Le Coffret de Crusoé</title>
						<author>Louis Dantin</author>
						<imprint>
							<publisher>ÉDITIONS ALBERT LÉVESQUE</publisher>
							<date when="1932">1932</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1932">1932</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">I</head><head type="main_part">CHANSON GRAVE</head><div type="poem" key="DAN7">
						<head type="main">Mosaïque ancienne</head>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">La</w> <w n="1.2">noble</w> <w n="1.3">Théléa</w>, <w n="1.4">la</w> <w n="1.5">fille</w> <w n="1.6">de</w> <w n="1.7">Byzance</w>,</l>
							<l n="2" num="1.2"><w n="2.1">Porte</w> <w n="2.2">au</w> <w n="2.3">front</w> <w n="2.4">la</w> <w n="2.5">splendeur</w> <w n="2.6">chaste</w> <w n="2.7">de</w> <w n="2.8">ses</w> <w n="2.9">vingt</w> <w n="2.10">ans</w>,</l>
							<l n="3" num="1.3"><w n="3.1">Et</w> <w n="3.2">ses</w> <w n="3.3">doigts</w> <w n="3.4">délicats</w> <w n="3.5">lustrés</w> <w n="3.6">de</w> <w n="3.7">diamants</w></l>
							<l n="4" num="1.4"><w n="4.1">Tiennent</w> <w n="4.2">pour</w> <w n="4.3">sceptre</w> <w n="4.4">l</w>’<w n="4.5">or</w>, <w n="4.6">la</w> <w n="4.7">grâce</w> <w n="4.8">et</w> <w n="4.9">la</w> <w n="4.10">puissance</w>.</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1"><w n="5.1">Or</w> <w n="5.2">le</w> <w n="5.3">seigneur</w> <w n="5.4">Quintus</w>, <w n="5.5">que</w> <w n="5.6">toute</w> <w n="5.7">gloire</w> <w n="5.8">encense</w>,</l>
							<l n="6" num="2.2"><w n="6.1">A</w> <w n="6.2">suspendu</w> <w n="6.3">son</w> <w n="6.4">âme</w> <w n="6.5">à</w> <w n="6.6">ses</w> <w n="6.7">cheveux</w> <w n="6.8">flottants</w>,</l>
							<l n="7" num="2.3"><w n="7.1">Et</w> <w n="7.2">l</w>’<w n="7.3">on</w> <w n="7.4">verra</w>, <w n="7.5">ce</w> <w n="7.6">soir</w>, <w n="7.7">enivrés</w> <w n="7.8">de</w> <w n="7.9">printemps</w>,</l>
							<l n="8" num="2.4"><w n="8.1">Les</w> <w n="8.2">jeunes</w> <w n="8.3">fiancés</w> <w n="8.4">sceller</w> <w n="8.5">leur</w> <w n="8.6">alliance</w>.</l>
						</lg>
						<lg n="3">
							<l n="9" num="3.1"><w n="9.1">Déjà</w> <w n="9.2">le</w> <w n="9.3">seuil</w> <w n="9.4">résonne</w> <w n="9.5">au</w> <w n="9.6">pas</w> <w n="9.7">des</w> <w n="9.8">coursiers</w> <w n="9.9">roux</w></l>
							<l n="10" num="3.2"><w n="10.1">Et</w> <w n="10.2">la</w> <w n="10.3">cithare</w> <w n="10.4">berce</w> <w n="10.5">à</w> <w n="10.6">ses</w> <w n="10.7">préludes</w> <w n="10.8">doux</w></l>
							<l n="11" num="3.3"><w n="11.1">Les</w> <w n="11.2">choeurs</w> <w n="11.3">enguirlandés</w> <w n="11.4">de</w> <w n="11.5">jacinthe</w> <w n="11.6">et</w> <w n="11.7">de</w> <w n="11.8">rose</w> ;</l>
						</lg>
						<lg n="4">
							<l n="12" num="4.1"><w n="12.1">Mais</w> <w n="12.2">la</w> <w n="12.3">vierge</w> <w n="12.4">s</w>’<w n="12.5">enfuit</w> <w n="12.6">en</w> <w n="12.7">sa</w> <w n="12.8">cellule</w> <w n="12.9">close</w>,</l>
							<l n="13" num="4.2"><w n="13.1">Car</w> <w n="13.2">elle</w> <w n="13.3">a</w> <w n="13.4">dans</w> <w n="13.5">son</w> <w n="13.6">coeur</w> <w n="13.7">choisi</w> <w n="13.8">pour</w> <w n="13.9">seul</w> <w n="13.10">époux</w></l>
							<l n="14" num="4.3"><w n="14.1">Le</w> <w n="14.2">Christ</w>, <w n="14.3">qui</w> <w n="14.4">lui</w> <w n="14.5">sourit</w> <w n="14.6">dans</w> <w n="14.7">une</w> <w n="14.8">apothéose</w>.</l>
						</lg>
					</div></body></text></TEI>