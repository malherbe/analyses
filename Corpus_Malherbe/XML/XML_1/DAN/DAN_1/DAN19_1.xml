<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Coffret de Crusoé</title>
				<title type="medium">Édition électronique</title>
				<author key="DAN">
					<name>
						<forename>Louis</forename>
						<surname>DANTIN</surname>
					</name>
					<date from="1865" to="1945">1865-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2468 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2016</date>
				<idno type="local">DAN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Coffret de Crusoé</title>
						<author>Louis Dantin</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/louisdantinlecofretdecrusoee.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Le Coffret de Crusoé</title>
						<author>Louis Dantin</author>
						<imprint>
							<publisher>ÉDITIONS ALBERT LÉVESQUE</publisher>
							<date when="1932">1932</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1932">1932</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">IV</head><head type="main_part">CHANSON FOLÂTRE</head><div type="poem" key="DAN19">
						<head type="main">Le billet doux du carabin</head>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">Jusqu</w>’<w n="1.2">à</w> <w n="1.3">ce</w> <w n="1.4">soir</w>, <w n="1.5">blonde</w> <w n="1.6">Lucie</w>,</l>
							<l n="2" num="1.2"><w n="2.1">Je</w> <w n="2.2">croyais</w> <w n="2.3">m</w>’<w n="2.4">être</w> <w n="2.5">sans</w> <w n="2.6">retour</w>,</l>
							<l n="3" num="1.3"><w n="3.1">Par</w> <w n="3.2">miracle</w> <w n="3.3">d</w>’<w n="3.4">antisepsie</w>,</l>
							<l n="4" num="1.4"><w n="4.1">Immunisé</w> <w n="4.2">du</w> <w n="4.3">mal</w> <w n="4.4">d</w>’<w n="4.5">amour</w>.</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1"><w n="5.1">Je</w> <w n="5.2">croyais</w>, <w n="5.3">dans</w> <w n="5.4">mon</w> <w n="5.5">coeur</w> <w n="5.6">frigide</w></l>
							<l n="6" num="2.2"><w n="6.1">Ainsi</w> <w n="6.2">qu</w>’<w n="6.3">un</w> <w n="6.4">marbre</w> <w n="6.5">d</w>’<w n="6.6">hôtel</w>-<w n="6.7">dieu</w></l>
							<l n="7" num="2.3"><w n="7.1">N</w>’<w n="7.2">offrir</w> <w n="7.3">au</w> <w n="7.4">bacille</w> <w n="7.5">morbide</w></l>
							<l n="8" num="2.4"><w n="8.1">Qu</w>’<w n="8.2">un</w> <w n="8.3">antipathique</w> <w n="8.4">milieu</w>.</l>
						</lg>
						<lg n="3">
							<l n="9" num="3.1"><w n="9.1">J</w>’<w n="9.2">en</w> <w n="9.3">étais</w> <w n="9.4">sûr</w>, <w n="9.5">nulle</w> <w n="9.6">cellule</w></l>
							<l n="10" num="3.2"><w n="10.1">En</w> <w n="10.2">moi</w> <w n="10.3">qui</w> <w n="10.4">ne</w> <w n="10.5">fût</w> <w n="10.6">à</w> <w n="10.7">l</w>’<w n="10.8">abri</w></l>
							<l n="11" num="3.3"><w n="11.1">Du</w> <w n="11.2">doux</w> <w n="11.3">symptôme</w> <w n="11.4">qui</w> <w n="11.5">pullule</w></l>
							<l n="12" num="3.4"><w n="12.1">Dans</w> <w n="12.2">un</w> <w n="12.3">plasma</w> <w n="12.4">moins</w> <w n="12.5">aguerri</w>.</l>
						</lg>
						<lg n="4">
							<l n="13" num="4.1"><w n="13.1">Grâce</w> <w n="13.2">à</w> <w n="13.3">la</w> <w n="13.4">vertu</w> <w n="13.5">souveraine</w></l>
							<l n="14" num="4.2"><w n="14.1">Des</w> <w n="14.2">prompts</w> <w n="14.3">sérums</w> <w n="14.4">que</w> <w n="14.5">nous</w> <w n="14.6">créons</w>,</l>
							<l n="15" num="4.3"><w n="15.1">J</w>’<w n="15.2">avais</w> <w n="15.3">mis</w> <w n="15.4">hors</w> <w n="15.5">de</w> <w n="15.6">mon</w> <w n="15.7">domaine</w></l>
							<l n="16" num="4.4"><w n="16.1">Les</w> <w n="16.2">redoutables</w> <w n="16.3">vibrions</w>.</l>
						</lg>
						<lg n="5">
							<l n="17" num="5.1"><w n="17.1">Hélas</w> ! <w n="17.2">illusion</w> <w n="17.3">risible</w> !</l>
							<l n="18" num="5.2"><w n="18.1">Sous</w> <w n="18.2">ton</w> <w n="18.3">oeil</w> <w n="18.4">où</w> <w n="18.5">l</w>’<w n="18.6">ardeur</w> <w n="18.7">se</w> <w n="18.8">peint</w></l>
						</lg>
						<lg n="6">
							<l n="19" num="6.1"><w n="19.1">Je</w> <w n="19.2">me</w> <w n="19.3">revois</w> <w n="19.4">plus</w> <w n="19.5">susceptible</w></l>
							<l n="20" num="6.2"><w n="20.1">Qu</w>’<w n="20.2">un</w> <w n="20.3">cochon</w>-<w n="20.4">d</w>’<w n="20.5">inde</w> <w n="20.6">ou</w> <w n="20.7">qu</w>’<w n="20.8">un</w> <w n="20.9">lapin</w>.</l>
						</lg>
						<lg n="7">
							<l n="21" num="7.1"><w n="21.1">Devant</w> <w n="21.2">toi</w>, <w n="21.3">chère</w> <w n="21.4">créature</w>,</l>
							<l n="22" num="7.2"><w n="22.1">Mon</w> <w n="22.2">sang</w>, <w n="22.3">que</w> <w n="22.4">sa</w> <w n="22.5">flamme</w> <w n="22.6">a</w> <w n="22.7">trahi</w>,</l>
							<l n="23" num="7.3"><w n="23.1">N</w>’<w n="23.2">est</w> <w n="23.3">plus</w> <w n="23.4">qu</w>’<w n="23.5">un</w> <w n="23.6">bouillon</w> <w n="23.7">de</w> <w n="23.8">culture</w></l>
							<l n="24" num="7.4"><w n="24.1">Par</w> <w n="24.2">mille</w> <w n="24.3">fièvres</w> <w n="24.4">envahi</w>.</l>
						</lg>
						<lg n="8">
							<l n="25" num="8.1"><w n="25.1">Et</w> <w n="25.2">ma</w> <w n="25.3">lèvre</w>, <w n="25.4">au</w> <w n="25.5">repli</w> <w n="25.6">sonore</w></l>
							<l n="26" num="8.2"><w n="26.1">De</w> <w n="26.2">ton</w> <w n="26.3">baiser</w> <w n="26.4">contagieux</w>,</l>
							<l n="27" num="8.3"><w n="27.1">Sent</w> <w n="27.2">un</w> <w n="27.3">fourmillement</w> <w n="27.4">éclore</w></l>
							<l n="28" num="8.4"><w n="28.1">De</w> <w n="28.2">microbes</w> <w n="28.3">délicieux</w>.</l>
						</lg>
					</div></body></text></TEI>