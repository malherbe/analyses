<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Coffret de Crusoé</title>
				<title type="medium">Édition électronique</title>
				<author key="DAN">
					<name>
						<forename>Louis</forename>
						<surname>DANTIN</surname>
					</name>
					<date from="1865" to="1945">1865-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2468 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2016</date>
				<idno type="local">DAN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Coffret de Crusoé</title>
						<author>Louis Dantin</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/louisdantinlecofretdecrusoee.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Le Coffret de Crusoé</title>
						<author>Louis Dantin</author>
						<imprint>
							<publisher>ÉDITIONS ALBERT LÉVESQUE</publisher>
							<date when="1932">1932</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1932">1932</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">VI</head><head type="main_part">CHANSON INTIME</head><div type="poem" key="DAN23">
						<head type="main">Âme-Univers</head>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">En</w> <w n="1.2">mon</w> <w n="1.3">âme</w>, <w n="1.4">comme</w> <w n="1.5">en</w> <w n="1.6">des</w> <w n="1.7">jours</w>,</l>
							<l n="2" num="1.2"><w n="2.1">Flottent</w>, <w n="2.2">lumières</w> <w n="2.3">nuancées</w>,</l>
							<l n="3" num="1.3"><w n="3.1">Des</w> <w n="3.2">rayons</w> <w n="3.3">qui</w> <w n="3.4">sont</w> <w n="3.5">des</w> <w n="3.6">amours</w>,</l>
							<l n="4" num="1.4"><w n="4.1">Des</w> <w n="4.2">reflets</w> <w n="4.3">qui</w> <w n="4.4">sont</w> <w n="4.5">des</w> <w n="4.6">pensées</w>.</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1"><w n="5.1">En</w> <w n="5.2">mon</w> <w n="5.3">âme</w>, <w n="5.4">comme</w> <w n="5.5">en</w> <w n="5.6">des</w> <w n="5.7">nuits</w>,</l>
							<l n="6" num="2.2"><w n="6.1">Errent</w>, <w n="6.2">au</w> <w n="6.3">caprice</w> <w n="6.4">des</w> <w n="6.5">songes</w>,</l>
							<l n="7" num="2.3"><w n="7.1">Des</w> <w n="7.2">spectres</w> <w n="7.3">qui</w> <w n="7.4">sont</w> <w n="7.5">des</w> <w n="7.6">ennuis</w>,</l>
							<l n="8" num="2.4"><w n="8.1">Des</w> <w n="8.2">sylphes</w> <w n="8.3">qui</w> <w n="8.4">sont</w> <w n="8.5">des</w> <w n="8.6">mensonges</w>.</l>
						</lg>
						<lg n="3">
							<l n="9" num="3.1"><w n="9.1">En</w> <w n="9.2">mon</w> <w n="9.3">âme</w>, <w n="9.4">comme</w> <w n="9.5">en</w> <w n="9.6">des</w> <w n="9.7">cieux</w>,</l>
							<l n="10" num="3.2"><w n="10.1">Évoluent</w> <w n="10.2">en</w> <w n="10.3">orbes</w> <w n="10.4">de</w> <w n="10.5">flammes</w></l>
							<l n="11" num="3.3"><w n="11.1">Des</w> <w n="11.2">étoiles</w> <w n="11.3">qui</w> <w n="11.4">sont</w> <w n="11.5">des</w> <w n="11.6">yeux</w></l>
							<l n="12" num="3.4"><w n="12.1">Et</w> <w n="12.2">des</w> <w n="12.3">lunes</w> <w n="12.4">qui</w> <w n="12.5">sont</w> <w n="12.6">des</w> <w n="12.7">âmes</w>.</l>
						</lg>
						<lg n="4">
							<l n="13" num="4.1"><w n="13.1">En</w> <w n="13.2">mon</w> <w n="13.3">âme</w>, <w n="13.4">pareille</w> <w n="13.5">au</w> <w n="13.6">temps</w>,</l>
							<l n="14" num="4.2"><w n="14.1">Se</w> <w n="14.2">succèdent</w>, <w n="14.3">flot</w> <w n="14.4">monotone</w>,</l>
							<l n="15" num="4.3"><w n="15.1">Des</w> <w n="15.2">fièvres</w> <w n="15.3">qui</w> <w n="15.4">sont</w> <w n="15.5">le</w> <w n="15.6">printemps</w></l>
							<l n="16" num="4.4"><w n="16.1">Et</w> <w n="16.2">des</w> <w n="16.3">frissons</w> <w n="16.4">qui</w> <w n="16.5">sont</w> <w n="16.6">l</w>’<w n="16.7">automne</w>.</l>
						</lg>
						<lg n="5">
							<l n="17" num="5.1"><w n="17.1">En</w> <w n="17.2">mon</w> <w n="17.3">âme</w>, <w n="17.4">comme</w> <w n="17.5">aux</w> <w n="17.6">jardins</w>,</l>
							<l n="18" num="5.2"><w n="18.1">Se</w> <w n="18.2">frôlent</w>, <w n="18.3">en</w> <w n="18.4">l</w>’<w n="18.5">émail</w> <w n="18.6">des</w> <w n="18.7">vases</w>,</l>
							<l n="19" num="5.3"><w n="19.1">Des</w> <w n="19.2">glaïeuls</w> <w n="19.3">qui</w> <w n="19.4">sont</w> <w n="19.5">des</w> <w n="19.6">dédains</w></l>
							<l n="20" num="5.4"><w n="20.1">Et</w> <w n="20.2">des</w> <w n="20.3">lis</w> <w n="20.4">qui</w> <w n="20.5">sont</w> <w n="20.6">des</w> <w n="20.7">extases</w>.</l>
						</lg>
						<lg n="6">
							<l n="21" num="6.1"><w n="21.1">En</w> <w n="21.2">mon</w> <w n="21.3">âme</w>, <w n="21.4">comme</w> <w n="21.5">aux</w> <w n="21.6">prés</w> <w n="21.7">verts</w>,</l>
							<l n="22" num="6.2"><w n="22.1">Papillonnent</w>, <w n="22.2">buvant</w> <w n="22.3">les</w> <w n="22.4">sèves</w>,</l>
							<l n="23" num="6.3"><w n="23.1">Des</w> <w n="23.2">linottes</w> <w n="23.3">qui</w> <w n="23.4">sont</w> <w n="23.5">des</w> <w n="23.6">vers</w></l>
							<l n="24" num="6.4"><w n="24.1">Et</w> <w n="24.2">des</w> <w n="24.3">merles</w> <w n="24.4">qui</w> <w n="24.5">sont</w> <w n="24.6">des</w> <w n="24.7">rêves</w>.</l>
						</lg>
						<lg n="7">
							<l n="25" num="7.1"><w n="25.1">En</w> <w n="25.2">mon</w> <w n="25.3">âme</w>, <w n="25.4">comme</w> <w n="25.5">en</w> <w n="25.6">les</w> <w n="25.7">bois</w>,</l>
							<l n="26" num="7.2"><w n="26.1">Glissent</w>, <w n="26.2">aux</w> <w n="26.3">surprises</w> <w n="26.4">des</w> <w n="26.5">routes</w>,</l>
							<l n="27" num="7.3"><w n="27.1">Sous</w> <w n="27.2">des</w> <w n="27.3">chênes</w> <w n="27.4">qui</w> <w n="27.5">sont</w> <w n="27.6">mes</w> <w n="27.7">fois</w></l>
							<l n="28" num="7.4"><w n="28.1">Des</w> <w n="28.2">reptiles</w> <w n="28.3">qui</w> <w n="28.4">sont</w> <w n="28.5">des</w> <w n="28.6">doutes</w>.</l>
						</lg>
						<lg n="8">
							<l n="29" num="8.1"><w n="29.1">En</w> <w n="29.2">mon</w> <w n="29.3">âme</w>, <w n="29.4">comme</w> <w n="29.5">aux</w> <w n="29.6">lavoirs</w>,</l>
							<l n="30" num="8.2"><w n="30.1">Se</w> <w n="30.2">déchirent</w>, <w n="30.3">en</w> <w n="30.4">troupes</w> <w n="30.5">blêmes</w>,</l>
							<l n="31" num="8.3"><w n="31.1">Des</w> <w n="31.2">brebis</w> <w n="31.3">qui</w> <w n="31.4">sont</w> <w n="31.5">des</w> <w n="31.6">espoirs</w></l>
							<l n="32" num="8.4"><w n="32.1">Et</w> <w n="32.2">des</w> <w n="32.3">loups</w> <w n="32.4">qui</w> <w n="32.5">sont</w> <w n="32.6">des</w> <w n="32.7">blasphèmes</w>.</l>
						</lg>
						<lg n="9">
							<l n="33" num="9.1"><w n="33.1">En</w> <w n="33.2">mon</w> <w n="33.3">âme</w>, <w n="33.4">comme</w> <w n="33.5">zéphirs</w></l>
							<l n="34" num="9.2"><w n="34.1">Ou</w> <w n="34.2">ruisseaux</w> <w n="34.3">à</w> <w n="34.4">l</w>’<w n="34.5">ombre</w> <w n="34.6">des</w> <w n="34.7">charmes</w>,</l>
							<l n="35" num="9.3"><w n="35.1">Coulent</w> <w n="35.2">des</w> <w n="35.3">brises</w> <w n="35.4">de</w> <w n="35.5">soupirs</w></l>
							<l n="36" num="9.4"><w n="36.1">Et</w> <w n="36.2">des</w> <w n="36.3">sources</w> <w n="36.4">qui</w> <w n="36.5">sont</w> <w n="36.6">des</w> <w n="36.7">larmes</w>.</l>
						</lg>
						<lg n="10">
							<l n="37" num="10.1"><w n="37.1">En</w> <w n="37.2">mon</w> <w n="37.3">âme</w>, <w n="37.4">comme</w> <w n="37.5">en</w> <w n="37.6">les</w> <w n="37.7">flots</w>,</l>
							<l n="38" num="10.2"><w n="38.1">Se</w> <w n="38.2">soulèvent</w>, <w n="38.3">mouvantes</w> <w n="38.4">plaines</w>,</l>
							<l n="39" num="10.3"><w n="39.1">Des</w> <w n="39.2">vagues</w> <w n="39.3">qui</w> <w n="39.4">sont</w> <w n="39.5">des</w> <w n="39.6">sanglots</w>,</l>
							<l n="40" num="10.4"><w n="40.1">Des</w> <w n="40.2">tempêtes</w> <w n="40.3">qui</w> <w n="40.4">sont</w> <w n="40.5">des</w> <w n="40.6">haînes</w>.</l>
						</lg>
						<lg n="11">
							<l n="41" num="11.1"><w n="41.1">En</w> <w n="41.2">mon</w> <w n="41.3">âme</w>, <w n="41.4">comme</w> <w n="41.5">aux</w> <w n="41.6">enfers</w>,</l>
							<l n="42" num="11.2"><w n="42.1">Vont</w>, <w n="42.2">rivés</w>, <w n="42.3">aux</w> <w n="42.4">squelettes</w> <w n="42.5">caves</w>,</l>
							<l n="43" num="11.3"><w n="43.1">Des</w> <w n="43.2">souillures</w> <w n="43.3">qui</w> <w n="43.4">sont</w> <w n="43.5">des</w> <w n="43.6">fers</w></l>
							<l n="44" num="11.4"><w n="44.1">Et</w> <w n="44.2">des</w> <w n="44.3">remords</w> <w n="44.4">qui</w> <w n="44.5">sont</w> <w n="44.6">des</w> <w n="44.7">laves</w>.</l>
						</lg>
					</div></body></text></TEI>