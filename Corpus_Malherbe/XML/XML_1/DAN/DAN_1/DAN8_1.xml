<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Coffret de Crusoé</title>
				<title type="medium">Édition électronique</title>
				<author key="DAN">
					<name>
						<forename>Louis</forename>
						<surname>DANTIN</surname>
					</name>
					<date from="1865" to="1945">1865-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2468 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2016</date>
				<idno type="local">DAN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Coffret de Crusoé</title>
						<author>Louis Dantin</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/louisdantinlecofretdecrusoee.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Le Coffret de Crusoé</title>
						<author>Louis Dantin</author>
						<imprint>
							<publisher>ÉDITIONS ALBERT LÉVESQUE</publisher>
							<date when="1932">1932</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1932">1932</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">I</head><head type="main_part">CHANSON GRAVE</head><div type="poem" key="DAN8">
						<head type="main">Évocation</head>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">Lorsque</w> <w n="1.2">le</w> <w n="1.3">soir</w> <w n="1.4">s</w>’<w n="1.5">abat</w> <w n="1.6">sur</w> <w n="1.7">ton</w> <w n="1.8">sourcil</w> <w n="1.9">géant</w></l>
							<l n="2" num="1.2"><w n="2.1">Et</w> <w n="2.2">que</w>, <w n="2.3">plus</w> <w n="2.4">fantastique</w>, <w n="2.5">au</w> <w n="2.6">bord</w> <w n="2.7">du</w> <w n="2.8">flot</w> <w n="2.9">béant</w></l>
							<l n="3" num="1.3"><space unit="char" quantity="8"></space><w n="3.1">Québec</w>, <w n="3.2">ta</w> <w n="3.3">grande</w> <w n="3.4">ombre</w> <w n="3.5">se</w> <w n="3.6">penche</w>,</l>
							<l n="4" num="1.4"><space unit="char" quantity="2"></space><w n="4.1">Comme</w> <w n="4.2">portée</w> <w n="4.3">au</w> <w n="4.4">vol</w> <w n="4.5">de</w> <w n="4.6">vents</w> <w n="4.7">magiciens</w>,</l>
							<l n="5" num="1.5"><w n="5.1">Vers</w> <w n="5.2">toi</w> <w n="5.3">furtivement</w> <w n="5.4">l</w>’<w n="5.5">âme</w> <w n="5.6">des</w> <w n="5.7">jours</w> <w n="5.8">anciens</w></l>
							<l n="6" num="1.6"><space unit="char" quantity="8"></space><w n="6.1">Accourt</w>, <w n="6.2">mystérieuse</w> <w n="6.3">et</w> <w n="6.4">blanche</w>.</l>
						</lg>
						<lg n="2">
							<l n="7" num="2.1"><w n="7.1">Le</w> <w n="7.2">jour</w> <w n="7.3">est</w> <w n="7.4">aux</w> <w n="7.5">vivants</w>, <w n="7.6">à</w> <w n="7.7">ces</w> <w n="7.8">fils</w> <w n="7.9">nés</w> <w n="7.10">d</w>’<w n="7.11">hier</w></l>
							<l n="8" num="2.2"><w n="8.1">Et</w> <w n="8.2">que</w> <w n="8.3">demain</w> <w n="8.4">appelle</w>, <w n="8.5">et</w> <w n="8.6">qui</w> <w n="8.7">de</w> <w n="8.8">leur</w> <w n="8.9">pas</w> <w n="8.10">fier</w></l>
							<l n="9" num="2.3"><space unit="char" quantity="8"></space><w n="9.1">Foulent</w> <w n="9.2">tes</w> <w n="9.3">places</w> <w n="9.4">et</w> <w n="9.5">tes</w> <w n="9.6">rues</w> ;</l>
							<l n="10" num="2.4"><w n="10.1">Mais</w> <w n="10.2">le</w> <w n="10.3">passé</w> <w n="10.4">frissonne</w> <w n="10.5">et</w> <w n="10.6">flotte</w> <w n="10.7">dans</w> <w n="10.8">la</w> <w n="10.9">nuit</w> ;</l>
							<l n="11" num="2.5"><w n="11.1">Et</w> <w n="11.2">tu</w> <w n="11.3">t</w>’<w n="11.4">émeus</w> <w n="11.5">à</w> <w n="11.6">voir</w>, <w n="11.7">dans</w> <w n="11.8">ses</w> <w n="11.9">ombres</w>, <w n="11.10">sans</w> <w n="11.11">bruit</w></l>
							<l n="12" num="2.6"><space unit="char" quantity="8"></space><w n="12.1">Glisser</w> <w n="12.2">les</w> <w n="12.3">gloires</w> <w n="12.4">disparues</w>.</l>
						</lg>
						<lg n="3">
							<l n="13" num="3.1"><w n="13.1">Ils</w> <w n="13.2">sont</w> <w n="13.3">là</w> <w n="13.4">tous</w>, <w n="13.5">tous</w> <w n="13.6">les</w> <w n="13.7">héros</w>, <w n="13.8">tous</w> <w n="13.9">les</w> <w n="13.10">vainqueurs</w>,</l>
							<l n="14" num="3.2"><w n="14.1">Tous</w> <w n="14.2">les</w> <w n="14.3">vaincus</w>, <w n="14.4">tous</w> <w n="14.5">les</w> <w n="14.6">martyrs</w>, <w n="14.7">tous</w> <w n="14.8">les</w> <w n="14.9">grands</w> <w n="14.10">coeurs</w>,</l>
							<l n="15" num="3.3"><space unit="char" quantity="8"></space><w n="15.1">Marins</w>, <w n="15.2">femmes</w>, <w n="15.3">soldats</w> <w n="15.4">ou</w> <w n="15.5">prêtres</w> ;</l>
							<l n="16" num="3.4"><w n="16.1">Et</w> <w n="16.2">dans</w> <w n="16.3">tes</w> <w n="16.4">murs</w> <w n="16.5">ayant</w> <w n="16.6">leur</w> <w n="16.7">cendre</w> <w n="16.8">pour</w> <w n="16.9">ciment</w></l>
							<l n="17" num="3.5"><w n="17.1">Ils</w> <w n="17.2">refont</w> <w n="17.3">chaque</w> <w n="17.4">nuit</w> <w n="17.5">mélancoliquement</w></l>
							<l n="18" num="3.6"><space unit="char" quantity="8"></space><w n="18.1">La</w> <w n="18.2">procession</w> <w n="18.3">des</w> <w n="18.4">ancêtres</w>.</l>
						</lg>
						<lg n="4">
							<l n="19" num="4.1"><w n="19.1">Car</w> <w n="19.2">il</w> <w n="19.3">faut</w> <w n="19.4">que</w> <w n="19.5">leur</w> <w n="19.6">nom</w> <w n="19.7">aille</w> <w n="19.8">aux</w> <w n="19.9">siècles</w> <w n="19.10">lointains</w> ;</l>
							<l n="20" num="4.2"><w n="20.1">Car</w> <w n="20.2">il</w> <w n="20.3">faut</w> <w n="20.4">que</w> <w n="20.5">leur</w> <w n="20.6">race</w> <w n="20.7">achève</w> <w n="20.8">les</w> <w n="20.9">destins</w></l>
							<l n="21" num="4.3"><space unit="char" quantity="8"></space><w n="21.1">Dont</w> <w n="21.2">ils</w> <w n="21.3">la</w> <w n="21.4">laissèrent</w> <w n="21.5">gardienne</w> :</l>
							<l n="22" num="4.4"><w n="22.1">Ô</w> <w n="22.2">Québec</w> ! <w n="22.3">si</w> <w n="22.4">leur</w> <w n="22.5">vision</w> <w n="22.6">hante</w> <w n="22.7">tes</w> <w n="22.8">soirs</w>,</l>
							<l n="23" num="4.5"><w n="23.1">C</w>’<w n="23.2">est</w> <w n="23.3">pour</w> <w n="23.4">hausser</w> <w n="23.5">ton</w> <w n="23.6">âme</w> <w n="23.7">et</w> <w n="23.8">grandir</w> <w n="23.9">tes</w> <w n="23.10">espoirs</w>,</l>
							<l n="24" num="4.6"><space unit="char" quantity="8"></space><w n="24.1">C</w>’<w n="24.2">est</w> <w n="24.3">pour</w> <w n="24.4">que</w> <w n="24.5">Québec</w> <w n="24.6">se</w> <w n="24.7">souvienne</w>.</l>
						</lg>
					</div></body></text></TEI>