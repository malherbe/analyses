<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Petits airs</title>
				<title type="medium">Édition électronique</title>
				<author key="CRC">
					<name>
						<forename>Francis</forename>
						<surname>Carco</surname>
					</name>
					<date from="1886" to="1958">1886-1958</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>280 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CRC_2</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Petits airs</title>
						<author>Francis Carco</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/petitsairspome00carc/page/24/mode/2up</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies complètes</title>
								<author>Francis Carco</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>RONALD DAVIS ET CIE</publisher>
									<date when="1920">1920</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1920">1920</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-04" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-04" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CRC22" rhyme="none">
		<head type="main"><hi rend="ital">QUELLE VOIX ?</hi></head>
			<opener>
				<salute><hi rend="smallcap">À PIERRE MAC-ORLAN</hi>.</salute>
			</opener>
		<lg n="1">
			<l n="1" num="1.1"><hi rend="ital"><w n="1.1">Quelle</w> <w n="1.2">voix</w> <w n="1.3">dans</w> <w n="1.4">l</w>’<w n="1.5">aube</w> <w n="1.6">nouvelle</w></hi></l>
			<l n="2" num="1.2"><space unit="char" quantity="6"></space><hi rend="ital"><w n="2.1">S</w>’<w n="2.2">unit</w> <w n="2.3">à</w> <w n="2.4">la</w> <w n="2.5">pluie</w>,</hi></l>
			<l n="3" num="1.3"><hi rend="ital"><w n="3.1">Pour</w> <w n="3.2">chanter</w> <w n="3.3">que</w> <w n="3.4">tout</w> <w n="3.5">ce</w> <w n="3.6">qui</w> <w n="3.7">brille</w></hi></l>
			<l n="4" num="1.4"><space unit="char" quantity="6"></space><hi rend="ital"><w n="4.1">Ne</w> <w n="4.2">brillera</w> <w n="4.3">plus</w> ?</hi></l>
		</lg>
		<lg n="2">
			<l n="5" num="2.1"><hi rend="ital"><w n="5.1">Où</w> <w n="5.2">s</w>’<w n="5.3">en</w> <w n="5.4">va</w> <w n="5.5">Bébé</w>-<w n="5.6">la</w>-<w n="5.7">Bohème</w></hi></l>
			<l n="6" num="2.2"><space unit="char" quantity="6"></space><hi rend="ital"><w n="6.1">Sous</w> <w n="6.2">ses</w> <w n="6.3">cheveux</w> <w n="6.4">gras</w> ?</hi></l>
			<l n="7" num="2.3"><hi rend="ital"><w n="7.1">L</w>’<w n="7.2">autre</w> <w n="7.3">chante</w> <w n="7.4">encore</w> <w n="7.5">et</w> <w n="7.6">quand</w> <w n="7.7">même</w></hi></l>
			<l n="8" num="2.4"><space unit="char" quantity="6"></space><hi rend="ital"><w n="8.1">Qu</w>’<w n="8.2">il</w> <w n="8.3">n</w> <w n="8.4">entende</w> <w n="8.5">pas</w>.</hi></l>
		</lg>
	</div></body></text></TEI>