<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Poésies complètes</title>
				<title type="medium">Édition électronique</title>
				<author key="CRC">
					<name>
						<forename>Francis</forename>
						<surname>Carco</surname>
					</name>
					<date from="1886" to="1958">1886-1958</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>227 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CRC_1</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Poésies complètes</title>
						<author>Francis Carco</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k3366885p.texteImage</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies complètes</title>
								<author>Francis Carco</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher></publisher>
									<date when="1955">1955</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1955">1955</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CRC2">
			<head type="main">CHANSON</head>
			<lg n="1">
				<l n="1" num="1.1"><w n="1.1">La</w> <w n="1.2">fée</w> <w n="1.3">un</w> <w n="1.4">soir</w> <w n="1.5">m</w>’<w n="1.6">a</w> <w n="1.7">dit</w></l>
				<l n="2" num="1.2"><w n="2.1">Qu</w>’<w n="2.2">il</w> <w n="2.3">était</w> <w n="2.4">une</w> <w n="2.5">étoile</w></l>
				<l n="3" num="1.3"><w n="3.1">Sans</w> <w n="3.2">voilette</w> <w n="3.3">ni</w> <w n="3.4">voile</w></l>
				<l n="4" num="1.4"><w n="4.1">Au</w> <w n="4.2">cœur</w> <w n="4.3">fort</w> <w n="4.4">inédit</w></l>
				<l n="5" num="1.5"><w n="5.1">Et</w> <w n="5.2">que</w> <w n="5.3">cette</w> <w n="5.4">ingénue</w>,</l>
				<l n="6" num="1.6"><w n="6.1">Idéalement</w> <w n="6.2">nue</w>,</l>
				<l n="7" num="1.7"><w n="7.1">Fêterait</w> <w n="7.2">ma</w> <w n="7.3">venue</w></l>
				<l n="8" num="1.8"><w n="8.1">À</w> <w n="8.2">son</w> <w n="8.3">bleu</w> <w n="8.4">paradis</w>.</l>
			</lg>
			<lg n="2">
				<l n="9" num="2.1"><w n="9.1">Elle</w> <w n="9.2">m</w>’<w n="9.3">a</w> <w n="9.4">dit</w> <w n="9.5">encor</w> :</l>
				<l n="10" num="2.2">« <w n="10.1">Pour</w> <w n="10.2">arriver</w> <w n="10.3">chez</w> <w n="10.4">elle</w></l>
				<l n="11" num="2.3"><w n="11.1">Je</w> <w n="11.2">te</w> <w n="11.3">ferai</w> <w n="11.4">des</w> <w n="11.5">ailes</w></l>
				<l n="12" num="2.4"><w n="12.1">Et</w> <w n="12.2">des</w> <w n="12.3">couronnes</w> <w n="12.4">d</w>’<w n="12.5">or</w>.</l>
				<l n="13" num="2.5"><w n="13.1">Loin</w> <w n="13.2">de</w> <w n="13.3">la</w> <w n="13.4">poésie</w></l>
				<l n="14" num="2.6"><w n="14.1">De</w> <w n="14.2">ta</w> <w n="14.3">liqueur</w> <w n="14.4">choisie</w>,</l>
				<l n="15" num="2.7"><w n="15.1">Tu</w> <w n="15.2">boiras</w> <w n="15.3">l</w>’<w n="15.4">ambroisie</w></l>
				<l n="16" num="2.8"><w n="16.1">Dans</w> <w n="16.2">ton</w> <w n="16.3">verre</w>, <w n="16.4">à</w> <w n="16.5">plein</w> <w n="16.6">bord</w>.</l>
			</lg>
			<lg n="3">
				<l n="17" num="3.1"><w n="17.1">Le</w> <w n="17.2">front</w> <w n="17.3">auréolé</w></l>
				<l n="18" num="3.2"><w n="18.1">D</w>’<w n="18.2">impérissable</w> <w n="18.3">gloire</w>,</l>
				<l n="19" num="3.3"><w n="19.1">Tu</w> <w n="19.2">pourras</w> <w n="19.3">rire</w> <w n="19.4">et</w> <w n="19.5">boire</w>,</l>
				<l n="20" num="3.4"><w n="20.1">Beau</w> <w n="20.2">poète</w> <w n="20.3">envolé</w>.</l>
				<l n="21" num="3.5"><w n="21.1">Et</w> <w n="21.2">l</w>’<w n="21.3">Éternel</w> <w n="21.4">lui</w>-<w n="21.5">même</w>,</l>
				<l n="22" num="3.6"><w n="22.1">À</w> <w n="22.2">ton</w> <w n="22.3">clair</w> <w n="22.4">diadème</w>,</l>
				<l n="23" num="3.7"><w n="23.1">Joindra</w> <w n="23.2">l</w>’<w n="23.3">éclat</w> <w n="23.4">suprême</w></l>
				<l n="24" num="3.8"><w n="24.1">D</w>’<w n="24.2">un</w> <w n="24.3">amour</w> <w n="24.4">étoilé</w>. »</l>
			</lg>
			<lg n="4">
				<l n="25" num="4.1"><w n="25.1">J</w>’<w n="25.2">avais</w> <w n="25.3">déjà</w> <w n="25.4">quitté</w></l>
				<l n="26" num="4.2"><w n="26.1">Mon</w> <w n="26.2">étroite</w> <w n="26.3">mansarde</w></l>
				<l n="27" num="4.3"><w n="27.1">Quand</w> <w n="27.2">la</w> <w n="27.3">lune</w> <w n="27.4">blafarde</w></l>
				<l n="28" num="4.4"><w n="28.1">S</w>’<w n="28.2">en</w> <w n="28.3">vint</w> <w n="28.4">me</w> <w n="28.5">visiter</w>.</l>
				<l n="29" num="4.5"><w n="29.1">À</w> <w n="29.2">la</w> <w n="29.3">lueur</w> <w n="29.4">bénie</w></l>
				<l n="30" num="4.6"><w n="30.1">De</w> <w n="30.2">cette</w> <w n="30.3">vieille</w> <w n="30.4">amie</w></l>
				<l n="31" num="4.7"><w n="31.1">J</w>’<w n="31.2">ai</w> <w n="31.3">compris</w> <w n="31.4">l</w>’<w n="31.5">ironie</w></l>
				<l n="32" num="4.8"><w n="32.1">De</w> <w n="32.2">la</w> <w n="32.3">réalité</w>.</l>
			</lg>
		</div></body></text></TEI>