<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Au vent crispé du matin</title>
				<title type="medium">Édition électronique</title>
				<author key="CRC">
					<name>
						<forename>Francis</forename>
						<surname>Carco</surname>
					</name>
					<date from="1886" to="1958">1886-1958</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>407 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CRC_3</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Au vent crispé du matin</title>
						<author>Francis Carco</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/auventcrispdum00carc/mode/2up</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Au vent crispé du matin</title>
								<author>Francis Carco</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Nouvelle Édition Nouvelle</publisher>
									<date when="1913">1913</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1913">1913</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-04" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-04" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LA BOHÊME ET MON CŒUR</head><div type="poem" key="CRC49" rhyme="none">
			<head type="main"><hi rend="ital">Sagesse</hi></head>
			<lg n="1">
				<l n="1" num="1.1"><w n="1.1">Mieux</w> <w n="1.2">qu</w>’<w n="1.3">un</w> <w n="1.4">jardin</w> <w n="1.5">paisible</w> <w n="1.6">aux</w> <w n="1.7">arbres</w> <w n="1.8">vermoulus</w>,</l>
				<l n="2" num="1.2"><w n="2.1">Je</w> <w n="2.2">fume</w> <w n="2.3">les</w> <w n="2.4">ronciers</w> <w n="2.5">épais</w> <w n="2.6">de</w> <w n="2.7">ma</w> <w n="2.8">tristesse</w></l>
				<l n="3" num="1.3"><w n="3.1">Avec</w> <w n="3.2">de</w> <w n="3.3">bons</w> <w n="3.4">chagrins</w> <w n="3.5">amers</w> <w n="3.6">et</w>, <w n="3.7">pour</w> <w n="3.8">le</w> <w n="3.9">reste</w>,</l>
				<l n="4" num="1.4"><space unit="char" quantity="12"></space><w n="4.1">J</w>’<w n="4.2">attends</w> <w n="4.3">que</w> <w n="4.4">le</w> <w n="4.5">temps</w> <w n="4.6">l</w>’<w n="4.7">use</w>.</l>
			</lg>
			<lg n="2">
				<l n="5" num="2.1"><w n="5.1">Les</w> <w n="5.2">jours</w> <w n="5.3">sont</w> <w n="5.4">comme</w> <w n="5.5">un</w> <w n="5.6">vol</w> <w n="5.7">de</w> <w n="5.8">mouette</w> <w n="5.9">à</w> <w n="5.10">l</w>’<w n="5.11">horizon</w>,</l>
				<l n="6" num="2.2"><w n="6.1">Le</w> <w n="6.2">soleil</w> <w n="6.3">de</w> <w n="6.4">midi</w> <w n="6.5">fait</w> <w n="6.6">tourner</w> <w n="6.7">les</w> <w n="6.8">tulipes</w>,</l>
				<l n="7" num="2.3"><w n="7.1">Tout</w> <w n="7.2">passe</w> : <w n="7.3">œillets</w>, <w n="7.4">lilas</w>, <w n="7.5">roses</w> <w n="7.6">et</w> <w n="7.7">clématites</w>.</l>
				<l n="8" num="2.4"><space unit="char" quantity="12"></space><w n="8.1">Les</w> <w n="8.2">tilleuls</w> <w n="8.3">et</w> <w n="8.4">la</w> <w n="8.5">viorne</w>.</l>
			</lg>
			<lg n="3">
				<l n="9" num="3.1"><w n="9.1">Pourtant</w>, <w n="9.2">pleureur</w> <w n="9.3">joyeux</w>, <w n="9.4">sache</w> <w n="9.5">te</w> <w n="9.6">réserver</w></l>
				<l n="10" num="3.2"><w n="10.1">Pour</w> <w n="10.2">le</w> <w n="10.3">jour</w> <w n="10.4">où</w> <w n="10.5">mourra</w>, <w n="10.6">sans</w> <w n="10.7">clameur</w> <w n="10.8">et</w> <w n="10.9">sans</w> <w n="10.10">geste</w>.</l>
				<l n="11" num="3.3"><w n="11.1">Ingénument</w>, <w n="11.2">le</w> <w n="11.3">beau</w> <w n="11.4">roncier</w> <w n="11.5">de</w> <w n="11.6">ta</w> <w n="11.7">tristesse</w> :</l>
				<l n="12" num="3.4"><space unit="char" quantity="12"></space><w n="12.1">Et</w> <w n="12.2">ménage</w> <w n="12.3">le</w> <w n="12.4">Rêve</w> !</l>
			</lg>
		</div></body></text></TEI>