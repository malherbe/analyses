<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Au vent crispé du matin</title>
				<title type="medium">Édition électronique</title>
				<author key="CRC">
					<name>
						<forename>Francis</forename>
						<surname>Carco</surname>
					</name>
					<date from="1886" to="1958">1886-1958</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>407 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CRC_3</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Au vent crispé du matin</title>
						<author>Francis Carco</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/auventcrispdum00carc/mode/2up</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Au vent crispé du matin</title>
								<author>Francis Carco</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Nouvelle Édition Nouvelle</publisher>
									<date when="1913">1913</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1913">1913</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-04" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-04" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LA BOHÊME ET MON CŒUR</head><div type="poem" key="CRC37">
			<head type="main"><hi rend="ital">Province</hi></head>
			<lg n="1">
				<l n="1" num="1.1"><w n="1.1">L</w>’<w n="1.2">ombre</w> <w n="1.3">du</w> <w n="1.4">clocher</w> <w n="1.5">noir</w> <w n="1.6">entre</w> <w n="1.7">dans</w> <w n="1.8">la</w> <w n="1.9">boutique</w></l>
				<l n="2" num="1.2"><w n="2.1">Un</w> <w n="2.2">lilas</w>, <w n="2.3">débordant</w> <w n="2.4">les</w> <w n="2.5">grilles</w> <w n="2.6">d</w>’<w n="2.7">un</w> <w n="2.8">jardin</w>,</l>
				<l n="3" num="1.3"><w n="3.1">Se</w> <w n="3.2">balance</w> <w n="3.3">et</w> <w n="3.4">je</w> <w n="3.5">vois</w> <w n="3.6">luire</w> <w n="3.7">et</w> <w n="3.8">trembler</w> <w n="3.9">soudain</w></l>
				<l n="4" num="1.4"><w n="4.1">Des</w> <w n="4.2">fouillis</w> <w n="4.3">bleus</w>, <w n="4.4">la</w> <w n="4.5">route</w> <w n="4.6">et</w> <w n="4.7">l</w>’<w n="4.8">auberge</w> <w n="4.9">rustique</w>.</l>
			</lg>
			<lg n="2">
				<l n="5" num="2.1"><w n="5.1">Des</w> <w n="5.2">pigeons</w>, <w n="5.3">mollement</w> <w n="5.4">arrivés</w> <w n="5.5">sur</w> <w n="5.6">le</w> <w n="5.7">vent</w>,</l>
				<l n="6" num="2.2"><w n="6.1">Tournent</w> <w n="6.2">dans</w> <w n="6.3">l</w>’<w n="6.4">azur</w> <w n="6.5">pâle</w> <w n="6.6">en</w> <w n="6.7">éployant</w> <w n="6.8">leurs</w> <w n="6.9">ailes</w>…</l>
				<l n="7" num="2.3"><w n="7.1">Province</w> ! <w n="7.2">Ah</w> ! <w n="7.3">ce</w> <w n="7.4">bonheur</w>, <w n="7.5">que</w> <w n="7.6">j</w>’<w n="7.7">ai</w> <w n="7.8">connu</w> <w n="7.9">loin</w> <w n="7.10">d</w>’<w n="7.11">elle</w>,</l>
				<l n="8" num="2.4"><w n="8.1">Comme</w> <w n="8.2">il</w> <w n="8.3">pèse</w> <w n="8.4">en</w> <w n="8.5">mon</w> <w n="8.6">cœur</w> <w n="8.7">scrupuleux</w> <w n="8.8">et</w> <w n="8.9">fervent</w> !</l>
			</lg>
			<lg n="3">
				<l n="9" num="3.1"><w n="9.1">Et</w> <w n="9.2">pourtant</w>, <w n="9.3">il</w> <w n="9.4">faut</w> <w n="9.5">s</w>’<w n="9.6">habituer</w> <w n="9.7">à</w> <w n="9.8">vivre</w>,</l>
				<l n="10" num="3.2"><w n="10.1">Même</w> <w n="10.2">seul</w>, <w n="10.3">même</w> <w n="10.4">triste</w>, <w n="10.5">indifférent</w> <w n="10.6">et</w> <w n="10.7">las</w>.</l>
				<l n="11" num="3.3"><w n="11.1">Car</w>, <w n="11.2">ô</w> <w n="11.3">ma</w> <w n="11.4">Vision</w> <w n="11.5">troublante</w>, <w n="11.6">n</w>’<w n="11.7">es</w>-<w n="11.8">tu</w> <w n="11.9">pas</w></l>
				<l n="12" num="3.4"><w n="12.1">Un</w> <w n="12.2">mirage</w> <w n="12.3">incessant</w> <w n="12.4">trop</w> <w n="12.5">difficile</w> <w n="12.6">à</w> <w n="12.7">suivre</w> ?</l>
			</lg>
		</div></body></text></TEI>