<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Au vent crispé du matin</title>
				<title type="medium">Édition électronique</title>
				<author key="CRC">
					<name>
						<forename>Francis</forename>
						<surname>Carco</surname>
					</name>
					<date from="1886" to="1958">1886-1958</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>407 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CRC_3</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Au vent crispé du matin</title>
						<author>Francis Carco</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/auventcrispdum00carc/mode/2up</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Au vent crispé du matin</title>
								<author>Francis Carco</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Nouvelle Édition Nouvelle</publisher>
									<date when="1913">1913</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1913">1913</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-04" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-04" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CHANSONS AIGRES-DOUCES</head><div type="poem" key="CRC56">
			<head type="main"><hi rend="ital">Figaro</hi></head>
			<lg n="1">
				<l n="1" num="1.1"><w n="1.1">Figaro</w> <w n="1.2">joue</w> <w n="1.3">de</w> <w n="1.4">la</w> <w n="1.5">guitare</w>.</l>
				<l n="2" num="1.2"><w n="2.1">Ma</w> <w n="2.2">bien</w>-<w n="2.3">aimée</w>, <w n="2.4">comme</w> <w n="2.5">il</w> <w n="2.6">joue</w> <w n="2.7">faux</w>…</l>
				<l n="3" num="1.3"><w n="3.1">La</w> <w n="3.2">pluie</w> <w n="3.3">d</w>’<w n="3.4">été</w> <w n="3.5">mouille</w> <w n="3.6">les</w> <w n="3.7">coteaux</w></l>
				<l n="4" num="1.4"><w n="4.1">Gris</w>, <w n="4.2">verts</w> <w n="4.3">et</w> <w n="4.4">bleuissants</w> <w n="4.5">du</w> <w n="4.6">soir</w>…</l>
				<l n="5" num="1.5"><w n="5.1">Oh</w> ! <w n="5.2">la</w> <w n="5.3">guitare</w> <w n="5.4">et</w> <w n="5.5">ce</w> <w n="5.6">bruit</w> <w n="5.7">d</w>’<w n="5.8">eau</w> !</l>
			</lg>
			<lg n="2">
				<l n="6" num="2.1"><w n="6.1">Entends</w>-<w n="6.2">tu</w> ? … <w n="6.3">Maintenant</w> <w n="6.4">qu</w>’<w n="6.5">il</w> <w n="6.6">chante</w>,</l>
				<l n="7" num="2.2"><w n="7.1">Comme</w> <w n="7.2">tu</w> <w n="7.3">es</w> <w n="7.4">troublée</w>, <w n="7.5">tout</w> <w n="7.6">à</w> <w n="7.7">coup</w> !</l>
				<l n="8" num="2.3"><w n="8.1">Or</w>, <w n="8.2">ce</w> <w n="8.3">Figaro</w> — <w n="8.4">coiffeur</w> <w n="8.5">dans</w> <w n="8.6">un</w> <w n="8.7">trou</w></l>
				<l n="9" num="2.4"><w n="9.1">De</w> <w n="9.2">province</w> <w n="9.3">déjà</w> <w n="9.4">pourrissante</w> —</l>
				<l n="10" num="2.5"><w n="10.1">N</w>’<w n="10.2">est</w> <w n="10.3">qu</w>’<w n="10.4">un</w> <w n="10.5">vieillard</w> <w n="10.6">à</w> <w n="10.7">moitié</w> <w n="10.8">fou</w>…</l>
			</lg>
			<lg n="3">
				<l n="11" num="3.1"><w n="11.1">Mais</w> <w n="11.2">tu</w> <w n="11.3">trembles</w> <w n="11.4">sous</w> <w n="11.5">ma</w> <w n="11.6">caresse</w>…</l>
				<l n="12" num="3.2"><w n="12.1">Tu</w> <w n="12.2">te</w> <w n="12.3">serres</w>, <w n="12.4">nue</w>, <w n="12.5">contre</w> <w n="12.6">moi</w>.</l>
				<l n="13" num="3.3"><w n="13.1">Nue</w> <w n="13.2">et</w> <w n="13.3">frissonnante</w> <w n="13.4">tandis</w> <w n="13.5">que</w> <w n="13.6">ta</w> <w n="13.7">voix</w>,</l>
				<l n="14" num="3.4"><w n="14.1">Rauque</w> <w n="14.2">un</w> <w n="14.3">peu</w>, <w n="14.4">répond</w> <w n="14.5">à</w> <w n="14.6">l</w>’<w n="14.7">amoureuse</w> <w n="14.8">averse</w></l>
				<l n="15" num="3.5"><w n="15.1">Qui</w> <w n="15.2">s</w>’<w n="15.3">abat</w> <w n="15.4">et</w> <w n="15.5">gémit</w> <w n="15.6">sur</w> <w n="15.7">le</w> <w n="15.8">toit</w>…</l>
			</lg>
		</div></body></text></TEI>