<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÈMES DIVERS</title>
				<title type="medium">Édition électronique</title>
				<author key="COP">
					<name>
						<forename>François</forename>
						<surname>COPPÉE</surname>
					</name>
					<date from="1842" to="1908">1842-1908</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>626 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2019</date>
				<idno type="local">COP_8</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poèmes Divers II</title>
						<author>François Coppée</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/francoiscopeepoemesdivers2.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition papier d’origine.</p>
					</sourceDesc>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title type="main">Œuvres complètes</title>
								<author>François Coppée</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Librairie ALEXANDRE HOUSSIAUX</publisher>
									<date when="1885">1885</date>
								</imprint>
								<biblScope unit="tome">Poésie, tome 1</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1869">1869</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="COP201">
				<head type="main">LE JONGLEUR</head>
				<opener>
					<salute>A Catulle Mendès.</salute>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Las</w> <w n="1.2">des</w> <w n="1.3">pédants</w> <w n="1.4">de</w> <w n="1.5">Salamanque</w></l>
					<l n="2" num="1.2"><w n="2.1">Et</w> <w n="2.2">de</w> <w n="2.3">l</w>’<w n="2.4">école</w> <w n="2.5">aux</w> <w n="2.6">noirs</w> <w n="2.7">gradins</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Je</w> <w n="3.2">vais</w> <w n="3.3">me</w> <w n="3.4">faire</w> <w n="3.5">saltimbanque</w></l>
					<l n="4" num="1.4"><w n="4.1">Et</w> <w n="4.2">vivre</w> <w n="4.3">avec</w> <w n="4.4">les</w> <w n="4.5">baladins</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Que</w> <w n="5.2">je</w> <w n="5.3">dorme</w> <w n="5.4">entre</w> <w n="5.5">quatre</w> <w n="5.6">toiles</w>,</l>
					<l n="6" num="2.2"><w n="6.1">La</w> <w n="6.2">nuque</w> <w n="6.3">sur</w> <w n="6.4">un</w> <w n="6.5">vieux</w> <w n="6.6">tambour</w>,</l>
					<l n="7" num="2.3"><w n="7.1">Mais</w> <w n="7.2">que</w> <w n="7.3">la</w> <w n="7.4">fraîcheur</w> <w n="7.5">des</w> <w n="7.6">étoiles</w></l>
					<l n="8" num="2.4"><w n="8.1">Baigne</w> <w n="8.2">mon</w> <w n="8.3">front</w> <w n="8.4">brûlé</w> <w n="8.5">d</w>’<w n="8.6">amour</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Je</w> <w n="9.2">consens</w> <w n="9.3">à</w> <w n="9.4">risquer</w> <w n="9.5">ma</w> <w n="9.6">tête</w></l>
					<l n="10" num="3.2"><w n="10.1">En</w> <w n="10.2">jonglant</w> <w n="10.3">avec</w> <w n="10.4">des</w> <w n="10.5">couteaux</w>,</l>
					<l n="11" num="3.3"><w n="11.1">Si</w> <w n="11.2">le</w> <w n="11.3">vin</w>, <w n="11.4">ce</w> <w n="11.5">but</w> <w n="11.6">de</w> <w n="11.7">la</w> <w n="11.8">quête</w>,</l>
					<l n="12" num="3.4"><w n="12.1">Coule</w> <w n="12.2">à</w> <w n="12.3">gros</w> <w n="12.4">sous</w> <w n="12.5">sur</w> <w n="12.6">mes</w> <w n="12.7">tréteaux</w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Que</w> <w n="13.2">la</w> <w n="13.3">bise</w> <w n="13.4">des</w> <w n="13.5">nuits</w> <w n="13.6">flagelle</w></l>
					<l n="14" num="4.2"><w n="14.1">La</w> <w n="14.2">tente</w> <w n="14.3">où</w> <w n="14.4">j</w>’<w n="14.5">irai</w> <w n="14.6">bivaquant</w>,</l>
					<l n="15" num="4.3"><w n="15.1">Mais</w> <w n="15.2">que</w> <w n="15.3">le</w> <w n="15.4">maillot</w> <w n="15.5">où</w> <w n="15.6">je</w> <w n="15.7">gèle</w></l>
					<l n="16" num="4.4"><w n="16.1">Soit</w> <w n="16.2">fait</w> <w n="16.3">de</w> <w n="16.4">pourpre</w> <w n="16.5">et</w> <w n="16.6">de</w> <w n="16.7">clinquant</w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">Que</w> <w n="17.2">j</w>’<w n="17.3">aille</w> <w n="17.4">errant</w> <w n="17.5">de</w> <w n="17.6">ville</w> <w n="17.7">en</w> <w n="17.8">ville</w></l>
					<l n="18" num="5.2"><w n="18.1">Chassé</w> <w n="18.2">par</w> <w n="18.3">le</w> <w n="18.4">corrégidor</w>,</l>
					<l n="19" num="5.3"><w n="19.1">Mais</w> <w n="19.2">que</w> <w n="19.3">la</w> <w n="19.4">populace</w> <w n="19.5">vile</w></l>
					<l n="20" num="5.4"><w n="20.1">M</w>’<w n="20.2">admire</w> <w n="20.3">ceint</w> <w n="20.4">d</w>’<w n="20.5">un</w> <w n="20.6">bandeau</w> <w n="20.7">d</w>’<w n="20.8">or</w>.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">Qu</w>’<w n="21.2">importe</w> <w n="21.3">que</w> <w n="21.4">sous</w> <w n="21.5">la</w> <w n="21.6">dentelle</w>,</l>
					<l n="22" num="6.2"><w n="22.1">Devant</w> <w n="22.2">mon</w> <w n="22.3">cynisme</w> <w n="22.4">doré</w>,</l>
					<l n="23" num="6.3"><w n="23.1">Les</w> <w n="23.2">dévotes</w> <w n="23.3">de</w> <w n="23.4">Compostelle</w></l>
					<l n="24" num="6.4"><w n="24.1">Se</w> <w n="24.2">signent</w> <w n="24.3">d</w>’<w n="24.4">un</w> <w n="24.5">air</w> <w n="24.6">timoré</w>,</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">Si</w> <w n="25.2">la</w> <w n="25.3">gitane</w> <w n="25.4">de</w> <w n="25.5">Cordoue</w>,</l>
					<l n="26" num="7.2"><w n="26.1">Qui</w> <w n="26.2">sait</w> <w n="26.3">se</w> <w n="26.4">mettre</w> <w n="26.5">sans</w> <w n="26.6">miroir</w></l>
					<l n="27" num="7.3"><w n="27.1">Des</w> <w n="27.2">accroche</w>-<w n="27.3">cœurs</w> <w n="27.4">sur</w> <w n="27.5">la</w> <w n="27.6">joue</w></l>
					<l n="28" num="7.4"><w n="28.1">Et</w> <w n="28.2">du</w> <w n="28.3">gros</w> <w n="28.4">fard</w> <w n="28.5">sous</w> <w n="28.6">son</w> <w n="28.7">œil</w> <w n="28.8">noir</w>,</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1">Trompant</w> <w n="29.2">un</w> <w n="29.3">hercule</w> <w n="29.4">de</w> <w n="29.5">foire</w></l>
					<l n="30" num="8.2"><w n="30.1">Stupide</w> <w n="30.2">et</w> <w n="30.3">fort</w> <w n="30.4">comme</w> <w n="30.5">un</w> <w n="30.6">cheval</w>,</l>
					<l n="31" num="8.3"><w n="31.1">M</w>’<w n="31.2">accorde</w> <w n="31.3">un</w> <w n="31.4">soir</w> <w n="31.5">d</w>’<w n="31.6">été</w> <w n="31.7">la</w> <w n="31.8">gloire</w></l>
					<l n="32" num="8.4"><w n="32.1">D</w>’<w n="32.2">avoir</w> <w n="32.3">un</w> <w n="32.4">géant</w> <w n="32.5">pour</w> <w n="32.6">rival</w> !</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1"><w n="33.1">Croule</w> <w n="33.2">donc</w>, <w n="33.3">ô</w> <w n="33.4">mon</w> <w n="33.5">passé</w>, <w n="33.6">croule</w>,</l>
					<l n="34" num="9.2"><w n="34.1">Espoir</w> <w n="34.2">des</w> <w n="34.3">avenirs</w> <w n="34.4">mesquins</w>,</l>
					<l n="35" num="9.3"><w n="35.1">Et</w> <w n="35.2">que</w> <w n="35.3">je</w> <w n="35.4">tienne</w> <w n="35.5">enfin</w> <w n="35.6">la</w> <w n="35.7">foule</w></l>
					<l n="36" num="9.4"><w n="36.1">Béante</w> <w n="36.2">sous</w> <w n="36.3">mes</w> <w n="36.4">brodequins</w> !</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1"><w n="37.1">Et</w> <w n="37.2">que</w>, <w n="37.3">l</w>’<w n="37.4">œil</w> <w n="37.5">fou</w> <w n="37.6">de</w> <w n="37.7">l</w>’<w n="37.8">auréole</w></l>
					<l n="38" num="10.2"><w n="38.1">Qu</w>’<w n="38.2">allume</w> <w n="38.3">ce</w> <w n="38.4">serpent</w> <w n="38.5">vermeil</w>,</l>
					<l n="39" num="10.3"><w n="39.1">Elle</w> <w n="39.2">prenne</w> <w n="39.3">un</w> <w n="39.4">jour</w> <w n="39.5">pour</w> <w n="39.6">idole</w></l>
					<l n="40" num="10.4"><w n="40.1">Le</w> <w n="40.2">fier</w> <w n="40.3">jongleur</w>, <w n="40.4">aux</w> <w n="40.5">dieux</w> <w n="40.6">pareil</w> !</l>
				</lg>
			</div></body></text></TEI>