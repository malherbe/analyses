<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Sonnets intimes et Poèmes inédits</title>
				<title type="medium">Édition électronique</title>
				<author key="COP">
					<name>
						<forename>François</forename>
						<surname>COPPÉE</surname>
					</name>
					<date from="1842" to="1908">1842-1908</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1824 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">COP_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Sonnets intimes et Poèmes inédits</title>
						<author>François Coppée</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/francoiscoppesonetsintimes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Poésies complètes de François Coppée</title>
						<author>François Coppée</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre</publisher>
							<date when="1925">1925</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1925">1925</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			<change when="2017-11-30" who="RR">Les vers incomplets du refrain du poème "RONDE D’ENFANTS AUX TUILERIES" ("Les lilas passeront, etc.") ont été remplacés par la strophe du refrain complet.</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">DEUXIÈME PARTIE</head><div type="poem" key="COP150">
					<head type="main">ÉCRIT SUR L’EXEMPLAIRE DES <hi rend="ital">JACOBITES</hi></head>
					<head type="sub_1">DONNÉ À MADEMOISELLE WEBER</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Vous</w> <w n="1.2">êtes</w> <w n="1.3">arrivée</w> <w n="1.4">au</w> <w n="1.5">sommet</w> <w n="1.6">d</w>’<w n="1.7">un</w> <w n="1.8">coup</w> <w n="1.9">d</w>’<w n="1.10">aile</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Brune</w> <w n="2.2">enfant</w> <w n="2.3">au</w> <w n="2.4">regard</w> <w n="2.5">de</w> <w n="2.6">sauvage</w> <w n="2.7">hirondelle</w> ;</l>
						<l n="3" num="1.3"><w n="3.1">Recueillez</w> <w n="3.2">les</w> <w n="3.3">lauriers</w> <w n="3.4">et</w> <w n="3.5">les</w> <w n="3.6">bravos</w> <w n="3.7">offerts</w>.</l>
						<l n="4" num="1.4"><w n="4.1">Un</w> <w n="4.2">poète</w> <w n="4.3">vous</w> <w n="4.4">a</w> <w n="4.5">devinée</w> <w n="4.6">et</w> <w n="4.7">choisie</w> ;</l>
						<l n="5" num="1.5"><w n="5.1">Servez</w> <w n="5.2">l</w>’<w n="5.3">art</w> <w n="5.4">noble</w> <w n="5.5">et</w> <w n="5.6">pur</w>, <w n="5.7">servez</w> <w n="5.8">la</w> <w n="5.9">poésie</w>,</l>
						<l n="6" num="1.6"><w n="6.1">Des</w> <w n="6.2">vers</w>, <w n="6.3">encor</w>, <w n="6.4">toujours</w>, <w n="6.5">dites</w>-<w n="6.6">nous</w> <w n="6.7">de</w> <w n="6.8">beaux</w> <w n="6.9">vers</w> !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1885">1885.</date>
						</dateline>
					</closer>
				</div></body></text></TEI>