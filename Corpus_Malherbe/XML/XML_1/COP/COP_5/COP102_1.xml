<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Sonnets intimes et Poèmes inédits</title>
				<title type="medium">Édition électronique</title>
				<author key="COP">
					<name>
						<forename>François</forename>
						<surname>COPPÉE</surname>
					</name>
					<date from="1842" to="1908">1842-1908</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1824 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">COP_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Sonnets intimes et Poèmes inédits</title>
						<author>François Coppée</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/francoiscoppesonetsintimes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Poésies complètes de François Coppée</title>
						<author>François Coppée</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre</publisher>
							<date when="1925">1925</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1925">1925</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			<change when="2017-11-30" who="RR">Les vers incomplets du refrain du poème "RONDE D’ENFANTS AUX TUILERIES" ("Les lilas passeront, etc.") ont été remplacés par la strophe du refrain complet.</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PREMIÈRE PARTIE</head><div type="poem" key="COP102">
					<head type="main">LE SIÈGE DE PARIS</head>
					<div type="section" n="1">
						<head type="number">I</head>
						<head type="main">APPROVISIONNEMENTS</head>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">Les</w> <w n="1.2">troupeaux</w> <w n="1.3">poussiéreux</w> <w n="1.4">et</w> <w n="1.5">gris</w></l>
							<l n="2" num="1.2"><w n="2.1">Qui</w> <w n="2.2">promettent</w> <w n="2.3">maigre</w> <w n="2.4">ripaille</w></l>
							<l n="3" num="1.3"><w n="3.1">Ruminent</w>, <w n="3.2">couchés</w> <w n="3.3">sur</w> <w n="3.4">la</w> <w n="3.5">paille</w>,</l>
							<l n="4" num="1.4"><w n="4.1">Dans</w> <w n="4.2">tous</w> <w n="4.3">les</w> <w n="4.4">jardins</w> <w n="4.5">de</w> <w n="4.6">Paris</w>.</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1"><w n="5.1">Mais</w> <w n="5.2">le</w> <w n="5.3">passant</w> <w n="5.4">mélancolique</w></l>
							<l n="6" num="2.2"><w n="6.1">Ne</w> <w n="6.2">trouve</w> <w n="6.3">dans</w> <w n="6.4">tout</w> <w n="6.5">ce</w> <w n="6.6">bétail</w></l>
							<l n="7" num="2.3"><w n="7.1">Ni</w> <w n="7.2">d</w>’<w n="7.3">ensemble</w> <w n="7.4">ni</w> <w n="7.5">de</w> <w n="7.6">détail</w></l>
							<l n="8" num="2.4"><w n="8.1">Empreint</w> <w n="8.2">d</w>’<w n="8.3">un</w> <w n="8.4">charme</w> <w n="8.5">bucolique</w> ;</l>
						</lg>
						<lg n="3">
							<l n="9" num="3.1"><w n="9.1">Ces</w> <w n="9.2">grands</w> <w n="9.3">bœufs</w> <w n="9.4">aux</w> <w n="9.5">gens</w> <w n="9.6">peu</w> <w n="9.7">frugaux</w></l>
							<l n="10" num="3.2"><w n="10.1">Font</w> <w n="10.2">rêver</w> <w n="10.3">des</w> <w n="10.4">repas</w> <w n="10.5">d</w>’<w n="10.6">Homère</w>,</l>
							<l n="11" num="3.3"><w n="11.1">Et</w> <w n="11.2">cet</w> <w n="11.3">agneau</w> <w n="11.4">tétant</w> <w n="11.5">sa</w> <w n="11.6">mère</w></l>
							<l n="12" num="3.4"><w n="12.1">N</w>’<w n="12.2">est</w> <w n="12.3">qu</w>’<w n="12.4">un</w> <w n="12.5">avenir</w> <w n="12.6">de</w> <w n="12.7">gigots</w>.</l>
						</lg>
						<lg n="4">
							<l n="13" num="4.1"><w n="13.1">Ils</w> <w n="13.2">ont</w> <w n="13.3">faim</w> <w n="13.4">et</w> <w n="13.5">froid</w>, <w n="13.6">ils</w> <w n="13.7">sont</w> <w n="13.8">mornes</w>.</l>
							<l n="14" num="4.2"><w n="14.1">L</w>’<w n="14.2">un</w> <w n="14.3">contre</w> <w n="14.4">l</w>’<w n="14.5">autre</w> <w n="14.6">acoquinés</w>,</l>
							<l n="15" num="4.3"><w n="15.1">Ils</w> <w n="15.2">ont</w> <w n="15.3">des</w> <w n="15.4">airs</w> <w n="15.5">de</w> <w n="15.6">condamnés</w></l>
							<l n="16" num="4.4"><w n="16.1">Et</w> <w n="16.2">baissent</w> <w n="16.3">tristement</w> <w n="16.4">leurs</w> <w n="16.5">cornes</w>.</l>
						</lg>
						<lg n="5">
							<l n="17" num="5.1"><w n="17.1">Le</w> <w n="17.2">pourceau</w> <w n="17.3">dormant</w> <w n="17.4">au</w> <w n="17.5">soleil</w></l>
							<l n="18" num="5.2"><w n="18.1">Frémit</w> <w n="18.2">au</w> <w n="18.3">contact</w> <w n="18.4">d</w>’<w n="18.5">une</w> <w n="18.6">mouche</w></l>
							<l n="19" num="5.3"><w n="19.1">Dont</w> <w n="19.2">l</w>’<w n="19.3">ardent</w> <w n="19.4">aiguillon</w> <w n="19.5">le</w> <w n="19.6">touche</w></l>
							<l n="20" num="5.4"><w n="20.1">Et</w> <w n="20.2">le</w> <w n="20.3">fait</w> <w n="20.4">geindre</w> <w n="20.5">en</w> <w n="20.6">son</w> <w n="20.7">sommeil</w>.</l>
						</lg>
						<lg n="6">
							<l n="21" num="6.1"><w n="21.1">Et</w> <w n="21.2">dans</w> <w n="21.3">leurs</w> <w n="21.4">clôtures</w> <w n="21.5">de</w> <w n="21.6">planches</w></l>
							<l n="22" num="6.2"><w n="22.1">Ils</w> <w n="22.2">semblent</w>, <w n="22.3">pauvres</w> <w n="22.4">animaux</w>,</l>
							<l n="23" num="6.3"><w n="23.1">Savoir</w> <w n="23.2">qu</w>’<w n="23.3">au</w> <w n="23.4">bout</w> <w n="23.5">de</w> <w n="23.6">tous</w> <w n="23.7">ces</w> <w n="23.8">maux</w></l>
							<l n="24" num="6.4"><w n="24.1">Ils</w> <w n="24.2">seront</w> <w n="24.3">mangés</w> <w n="24.4">par</w> <w n="24.5">éclanches</w>.</l>
						</lg>
						<lg n="7">
							<l n="25" num="7.1">— <w n="25.1">Mais</w> <w n="25.2">n</w>’<w n="25.3">ayons</w> <w n="25.4">pas</w> <w n="25.5">naïvement</w></l>
							<l n="26" num="7.2"><w n="26.1">De</w> <w n="26.2">pitié</w> <w n="26.3">pour</w> <w n="26.4">cette</w> <w n="26.5">hécatombe</w> ;</l>
							<l n="27" num="7.3"><w n="27.1">Car</w> <w n="27.2">j</w>’<w n="27.3">entends</w>, <w n="27.4">dans</w> <w n="27.5">le</w> <w n="27.6">soir</w> <w n="27.7">qui</w> <w n="27.8">tombe</w>,</l>
							<l n="28" num="7.4"><w n="28.1">Les</w> <w n="28.2">durs</w> <w n="28.3">clairons</w> <w n="28.4">d</w>’<w n="28.5">un</w> <w n="28.6">régiment</w>,</l>
						</lg>
						<lg n="8">
							<l n="29" num="8.1"><w n="29.1">Et</w>, <w n="29.2">songeant</w> <w n="29.3">au</w> <w n="29.4">temps</w> <w n="29.5">où</w> <w n="29.6">nous</w> <w n="29.7">sommes</w>,</l>
							<l n="30" num="8.2"><w n="30.1">Sombre</w>, <w n="30.2">j</w>’<w n="30.3">ai</w> <w n="30.4">murmuré</w> <w n="30.5">bien</w> <w n="30.6">bas</w> :</l>
							<l n="31" num="8.3">« <w n="31.1">O</w> <w n="31.2">troupeaux</w>, <w n="31.3">ne</w> <w n="31.4">vous</w> <w n="31.5">plaignez</w> <w n="31.6">pas</w></l>
							<l n="32" num="8.4"><w n="32.1">De</w> <w n="32.2">la</w> <w n="32.3">férocité</w> <w n="32.4">des</w> <w n="32.5">hommes</w> ! »</l>
						</lg>
					</div>
					<div type="section" n="2">
						<head type="number">II</head>
						<head type="main">VOITURES D’AMBULANCE</head>
						<lg n="1">
							<l n="33" num="1.1"><w n="33.1">L</w>’<w n="33.2">été</w>, <w n="33.3">sous</w> <w n="33.4">la</w> <w n="33.5">claire</w> <w n="33.6">nuit</w> <w n="33.7">bleue</w>,</l>
							<l n="34" num="1.2"><w n="34.1">Galopant</w> <w n="34.2">le</w> <w n="34.3">long</w> <w n="34.4">des</w> <w n="34.5">moissons</w>,</l>
							<l n="35" num="1.3"><w n="35.1">Les</w> <w n="35.2">omnibus</w> <w n="35.3">de</w> <w n="35.4">la</w> <w n="35.5">banlieue</w></l>
							<l n="36" num="1.4"><w n="36.1">Rentraient</w>, <w n="36.2">le</w> <w n="36.3">soir</w>, <w n="36.4">pleins</w> <w n="36.5">de</w> <w n="36.6">chansons</w>.</l>
						</lg>
						<lg n="2">
							<l n="37" num="2.1"><w n="37.1">Les</w> <w n="37.2">grisettes</w> <w n="37.3">sur</w> <w n="37.4">ces</w> <w n="37.5">voitures</w></l>
							<l n="38" num="2.2"><w n="38.1">Grimpaient</w> <w n="38.2">avec</w> <w n="38.3">les</w> <w n="38.4">calicots</w>.</l>
							<l n="39" num="2.3"><w n="39.1">On</w> <w n="39.2">avait</w> <w n="39.3">mangé</w> <w n="39.4">des</w> <w n="39.5">fritures</w></l>
							<l n="40" num="2.4"><w n="40.1">Et</w> <w n="40.2">cueilli</w> <w n="40.3">des</w> <w n="40.4">coquelicots</w>.</l>
						</lg>
						<lg n="3">
							<l n="41" num="3.1"><w n="41.1">Les</w> <w n="41.2">moustaches</w> <w n="41.3">frôlaient</w> <w n="41.4">les</w> <w n="41.5">joues</w>,</l>
							<l n="42" num="3.2"><w n="42.1">Car</w> <w n="42.2">dans</w> <w n="42.3">l</w>’<w n="42.4">ombre</w> <w n="42.5">on</w> <w n="42.6">peut</w> <w n="42.7">tout</w> <w n="42.8">oser</w>,</l>
							<l n="43" num="3.3"><w n="43.1">Le</w> <w n="43.2">bruit</w> <w n="43.3">des</w> <w n="43.4">grelots</w> <w n="43.5">et</w> <w n="43.6">des</w> <w n="43.7">roues</w></l>
							<l n="44" num="3.4"><w n="44.1">Étouffant</w> <w n="44.2">le</w> <w n="44.3">bruit</w> <w n="44.4">d</w>’<w n="44.5">un</w> <w n="44.6">baiser</w>.</l>
						</lg>
						<lg n="4">
							<l n="45" num="4.1"><w n="45.1">Et</w> <w n="45.2">l</w>’<w n="45.3">on</w> <w n="45.4">revenait</w>, <w n="45.5">sous</w> <w n="45.6">les</w> <w n="45.7">branches</w>,</l>
							<l n="46" num="4.2"><w n="46.1">De</w> <w n="46.2">Boulogne</w> <w n="46.3">ou</w> <w n="46.4">de</w> <w n="46.5">Charenton</w>,</l>
							<l n="47" num="4.3"><w n="47.1">Les</w> <w n="47.2">bras</w> <w n="47.3">noirs</w> <w n="47.4">sur</w> <w n="47.5">les</w> <w n="47.6">tailles</w> <w n="47.7">blanches</w>,</l>
							<l n="48" num="4.4"><w n="48.1">Tout</w> <w n="48.2">en</w> <w n="48.3">jouant</w> <w n="48.4">du</w> <w n="48.5">mirliton</w>.</l>
						</lg>
						<lg n="5">
							<l n="49" num="5.1">— <w n="49.1">Or</w> <w n="49.2">j</w>’<w n="49.3">ai</w> <w n="49.4">revu</w> <w n="49.5">ces</w> <w n="49.6">voiturées</w>,</l>
							<l n="50" num="5.2"><w n="50.1">Mais</w> <w n="50.2">non</w> <w n="50.3">plus</w> <w n="50.4">telles</w> <w n="50.5">que</w> <w n="50.6">jadis</w>,</l>
							<l n="51" num="5.3"><w n="51.1">Par</w> <w n="51.2">les</w> <w n="51.3">amusantes</w> <w n="51.4">soirées</w></l>
							<l n="52" num="5.4"><w n="52.1">Des</w> <w n="52.2">dimanches</w> <w n="52.3">et</w> <w n="52.4">des</w> <w n="52.5">lundis</w>.</l>
						</lg>
						<lg n="6">
							<l n="53" num="6.1"><w n="53.1">Le</w> <w n="53.2">drapeau</w> <w n="53.3">blanc</w> <w n="53.4">de</w> <w n="53.5">l</w>’<w n="53.6">ambulance</w></l>
							<l n="54" num="6.2"><w n="54.1">Pendait</w>, <w n="54.2">morne</w>, <w n="54.3">auprès</w> <w n="54.4">du</w> <w n="54.5">cocher</w>.</l>
							<l n="55" num="6.3"><w n="55.1">C</w>’<w n="55.2">est</w> <w n="55.3">au</w> <w n="55.4">petit</w> <w n="55.5">pas</w>, <w n="55.6">en</w> <w n="55.7">silence</w>,</l>
							<l n="56" num="6.4"><w n="56.1">Que</w> <w n="56.2">leurs</w> <w n="56.3">chevaux</w> <w n="56.4">devaient</w> <w n="56.5">marcher</w>.</l>
						</lg>
						<lg n="7">
							<l n="57" num="7.1"><w n="57.1">Elles</w> <w n="57.2">glissaient</w> <w n="57.3">comme</w> <w n="57.4">des</w> <w n="57.5">ombres</w>,</l>
							<l n="58" num="7.2"><w n="58.1">Et</w> <w n="58.2">les</w> <w n="58.3">passants</w>, <w n="58.4">d</w>’<w n="58.5">horreur</w> <w n="58.6">saisis</w>,</l>
							<l n="59" num="7.3"><w n="59.1">Voyaient</w> <w n="59.2">par</w> <w n="59.3">les</w> <w n="59.4">portières</w> <w n="59.5">sombres</w></l>
							<l n="60" num="7.4"><w n="60.1">Passer</w> <w n="60.2">des</w> <w n="60.3">canons</w> <w n="60.4">de</w> <w n="60.5">fusils</w>.</l>
						</lg>
						<lg n="8">
							<l n="61" num="8.1"><w n="61.1">Ceux</w> <w n="61.2">de</w> <w n="61.3">la</w> <w n="61.4">bataille</w> <w n="61.5">dernière</w></l>
							<l n="62" num="8.2"><w n="62.1">Revenaient</w> <w n="62.2">là</w>, <w n="62.3">tristes</w> <w n="62.4">et</w> <w n="62.5">lents</w>,</l>
							<l n="63" num="8.3"><w n="63.1">Et</w> <w n="63.2">l</w>’<w n="63.3">on</w> <w n="63.4">souffrait</w> <w n="63.5">à</w> <w n="63.6">chaque</w> <w n="63.7">ornière</w></l>
							<l n="64" num="8.4"><w n="64.1">Qui</w> <w n="64.2">secouait</w> <w n="64.3">leurs</w> <w n="64.4">fronts</w> <w n="64.5">ballants</w>.</l>
						</lg>
						<lg n="9">
							<l n="65" num="9.1"><w n="65.1">Ils</w> <w n="65.2">ont</w> <w n="65.3">fait</w> <w n="65.4">à</w> <w n="65.5">peine</w> <w n="65.6">deux</w> <w n="65.7">lieues</w>,</l>
							<l n="66" num="9.2"><w n="66.1">Ces</w> <w n="66.2">ironiques</w> <w n="66.3">omnibus</w></l>
							<l n="67" num="9.3"><w n="67.1">Pleins</w> <w n="67.2">de</w> <w n="67.3">blessés</w> <w n="67.4">aux</w> <w n="67.5">vestes</w> <w n="67.6">bleues</w></l>
							<l n="68" num="9.4"><w n="68.1">Qu</w>’<w n="68.2">ensanglanta</w> <w n="68.3">l</w>’<w n="68.4">éclat</w> <w n="68.5">d</w>’<w n="68.6">obus</w>.</l>
						</lg>
						<lg n="10">
							<l n="69" num="10.1"><w n="69.1">Ce</w> <w n="69.2">convoi</w> <w n="69.3">de</w> <w n="69.4">coucous</w> <w n="69.5">qui</w> <w n="69.6">passe</w></l>
							<l n="70" num="10.2"><w n="70.1">Semble</w> <w n="70.2">nous</w> <w n="70.3">faire</w> <w n="70.4">réfléchir</w></l>
							<l n="71" num="10.3"><w n="71.1">A</w> <w n="71.2">l</w>’<w n="71.3">étroitesse</w> <w n="71.4">de</w> <w n="71.5">l</w>’<w n="71.6">espace</w></l>
							<l n="72" num="10.4"><w n="72.1">Qui</w> <w n="72.2">nous</w> <w n="72.3">reste</w> <w n="72.4">encor</w> <w n="72.5">pour</w> <w n="72.6">mourir</w> ;</l>
						</lg>
						<lg n="11">
							<l n="73" num="11.1"><w n="73.1">Et</w>, <w n="73.2">malgré</w> <w n="73.3">mes</w> <w n="73.4">pleurs</w> <w n="73.5">de</w> <w n="73.6">souffrance</w>,</l>
							<l n="74" num="11.2"><w n="74.1">J</w>’<w n="74.2">ai</w> <w n="74.3">pu</w> <w n="74.4">lire</w> <w n="74.5">sur</w> <w n="74.6">leurs</w> <w n="74.7">panneaux</w></l>
							<l n="75" num="11.3"><w n="75.1">Les</w> <w n="75.2">noms</w> <w n="75.3">des</w> <w n="75.4">frontières</w> <w n="75.5">de</w> <w n="75.6">France</w> :</l>
							<l n="76" num="11.4"><w n="76.1">Courbevoie</w>, <w n="76.2">Asnières</w>, <w n="76.3">Puteaux</w>.</l>
						</lg>
					</div>
					<div type="section" n="3">
						<head type="number">III</head>
						<head type="main">EN FACTION</head>
						<lg n="1">
							<l n="77" num="1.1"><space quantity="4" unit="char"></space><w n="77.1">L</w>’<w n="77.2">œil</w> <w n="77.3">ouvert</w> <w n="77.4">sur</w> <w n="77.5">l</w>’<w n="77.6">horizon</w>,</l>
							<l n="78" num="1.2"><space quantity="4" unit="char"></space><w n="78.1">On</w> <w n="78.2">m</w>’<w n="78.3">a</w> <w n="78.4">mis</w> <w n="78.5">en</w> <w n="78.6">sentinelle</w>.</l>
							<l n="79" num="1.3"><space quantity="4" unit="char"></space>— <w n="79.1">Comme</w> <w n="79.2">l</w>’<w n="79.3">arrière</w>-<w n="79.4">saison</w></l>
							<l n="80" num="1.4"><space quantity="4" unit="char"></space><w n="80.1">Est</w> <w n="80.2">morose</w> <w n="80.3">et</w> <w n="80.4">solennelle</w> !</l>
						</lg>
						<lg n="2">
							<l n="81" num="2.1"><space quantity="4" unit="char"></space><w n="81.1">Un</w> <w n="81.2">long</w> <w n="81.3">convoi</w> <w n="81.4">de</w> <w n="81.5">blessés</w>,</l>
							<l n="82" num="2.2"><space quantity="4" unit="char"></space><w n="82.1">Funèbre</w>, <w n="82.2">franchit</w> <w n="82.3">nos</w> <w n="82.4">portes</w>.</l>
							<l n="83" num="2.3"><space quantity="4" unit="char"></space>— <w n="83.1">Combien</w> <w n="83.2">sous</w> <w n="83.3">ces</w> <w n="83.4">vents</w> <w n="83.5">glacés</w></l>
							<l n="84" num="2.4"><space quantity="4" unit="char"></space><w n="84.1">S</w>’<w n="84.2">envolent</w> <w n="84.3">de</w> <w n="84.4">feuilles</w> <w n="84.5">mortes</w> ?</l>
						</lg>
						<lg n="3">
							<l n="85" num="3.1"><space quantity="4" unit="char"></space><w n="85.1">On</w> <w n="85.2">a</w> <w n="85.3">vaincu</w> <w n="85.4">cependant</w>,</l>
							<l n="86" num="3.2"><space quantity="4" unit="char"></space><w n="86.1">Mais</w> <w n="86.2">nos</w> <w n="86.3">pertes</w> <w n="86.4">sont</w> <w n="86.5">trop</w> <w n="86.6">sûres</w>.</l>
							<l n="87" num="3.3"><space quantity="4" unit="char"></space>— <w n="87.1">Pourquoi</w> <w n="87.2">ce</w> <w n="87.3">soir</w> <w n="87.4">l</w>’<w n="87.5">occident</w></l>
							<l n="88" num="3.4"><space quantity="4" unit="char"></space><w n="88.1">Saigne</w>-<w n="88.2">t</w>-<w n="88.3">il</w> <w n="88.4">par</w> <w n="88.5">vingt</w> <w n="88.6">blessures</w> ?</l>
						</lg>
						<lg n="4">
							<l n="89" num="4.1"><space quantity="4" unit="char"></space><w n="89.1">Dans</w> <w n="89.2">tes</w> <w n="89.3">vieux</w> <w n="89.4">murs</w>, <w n="89.5">ô</w> <w n="89.6">Paris</w>,</l>
							<l n="90" num="4.2"><space quantity="4" unit="char"></space><w n="90.1">Nous</w> <w n="90.2">tiendrons</w>, <w n="90.3">forts</w> <w n="90.4">et</w> <w n="90.5">fidèles</w>.</l>
							<l n="91" num="4.3"><space quantity="4" unit="char"></space>— <w n="91.1">Qu</w>’<w n="91.2">il</w> <w n="91.3">fait</w> <w n="91.4">mal</w>, <w n="91.5">dans</w> <w n="91.6">ce</w> <w n="91.7">ciel</w> <w n="91.8">gris</w>,</l>
							<l n="92" num="4.4"><space quantity="4" unit="char"></space><w n="92.1">Le</w> <w n="92.2">départ</w> <w n="92.3">des</w> <w n="92.4">hirondelles</w>.</l>
						</lg>
					</div>
					<div type="section" n="4">
						<head type="number">IV</head>
						<head type="main">TABLEAU DE BIVOUAC</head>
						<lg n="1">
							<l n="93" num="1.1"><w n="93.1">Furieux</w> <w n="93.2">de</w> <w n="93.3">la</w> <w n="93.4">double</w> <w n="93.5">étape</w>,</l>
							<l n="94" num="1.2"><w n="94.1">Les</w> <w n="94.2">soldats</w> <w n="94.3">n</w>’<w n="94.4">ont</w> <w n="94.5">pas</w> <w n="94.6">le</w> <w n="94.7">cœur</w> <w n="94.8">gai</w> ;</l>
							<l n="95" num="1.3"><w n="95.1">On</w> <w n="95.2">a</w> <w n="95.3">donné</w> <w n="95.4">plus</w> <w n="95.5">d</w>’<w n="95.6">une</w> <w n="95.7">tape</w></l>
							<l n="96" num="1.4"><w n="96.1">Au</w> <w n="96.2">petit</w> <w n="96.3">tambour</w> <w n="96.4">fatigué</w>.</l>
						</lg>
						<lg n="2">
							<l n="97" num="2.1"><w n="97.1">Mais</w>, <w n="97.2">quand</w> <w n="97.3">il</w> <w n="97.4">a</w> <w n="97.5">taillé</w> <w n="97.6">la</w> <w n="97.7">soupe</w>,</l>
							<l n="98" num="2.2"><w n="98.1">Coupé</w> <w n="98.2">le</w> <w n="98.3">bois</w> <w n="98.4">et</w> <w n="98.5">fait</w> <w n="98.6">du</w> <w n="98.7">feu</w>,</l>
							<l n="99" num="2.3"><w n="99.1">On</w> <w n="99.2">laisse</w> <w n="99.3">enfin</w> <w n="99.4">l</w>’<w n="99.5">enfant</w> <w n="99.6">de</w> <w n="99.7">troupe</w></l>
							<l n="100" num="2.4"><w n="100.1">Se</w> <w n="100.2">coucher</w> <w n="100.3">et</w> <w n="100.4">dormir</w> <w n="100.5">un</w> <w n="100.6">peu</w>.</l>
						</lg>
						<lg n="3">
							<l n="101" num="3.1"><w n="101.1">Les</w> <w n="101.2">vétérans</w> <w n="101.3">sont</w> <w n="101.4">là</w>, <w n="101.5">farouches</w>,</l>
							<l n="102" num="3.2"><w n="102.1">A</w> <w n="102.2">ce</w> <w n="102.3">bivouac</w> <w n="102.4">qu</w>’<w n="102.5">on</w> <w n="102.6">voit</w> <w n="102.7">briller</w> ;</l>
							<l n="103" num="3.3"><w n="103.1">Et</w> <w n="103.2">d</w>’<w n="103.3">un</w> <w n="103.4">sac</w> <w n="103.5">rempli</w> <w n="103.6">de</w> <w n="103.7">cartouches</w></l>
							<l n="104" num="3.4"><w n="104.1">L</w>’<w n="104.2">enfant</w> <w n="104.3">s</w>’<w n="104.4">est</w> <w n="104.5">fait</w> <w n="104.6">un</w> <w n="104.7">oreiller</w>.</l>
						</lg>
						<lg n="4">
							<l n="105" num="4.1"><w n="105.1">Le</w> <w n="105.2">sac</w> <w n="105.3">est</w> <w n="105.4">si</w> <w n="105.5">gonflé</w> <w n="105.6">qu</w>’<w n="105.7">il</w> <w n="105.8">crève</w>,</l>
							<l n="106" num="4.2"><w n="106.1">La</w> <w n="106.2">poudre</w> <w n="106.3">à</w> <w n="106.4">terre</w> <w n="106.5">se</w> <w n="106.6">répand</w> ;</l>
							<l n="107" num="4.3"><w n="107.1">Mais</w> <w n="107.2">l</w>’<w n="107.3">orphelin</w> <w n="107.4">sommeille</w> <w n="107.5">et</w> <w n="107.6">rêve</w></l>
							<l n="108" num="4.4"><w n="108.1">A</w> <w n="108.2">trois</w> <w n="108.3">pas</w> <w n="108.4">du</w> <w n="108.5">foyer</w> <w n="108.6">flambant</w>.</l>
						</lg>
						<lg n="5">
							<l n="109" num="5.1"><w n="109.1">Rien</w> <w n="109.2">qu</w>’<w n="109.3">une</w> <w n="109.4">étincelle</w>, <w n="109.5">une</w> <w n="109.6">seule</w> !</l>
							<l n="110" num="5.2"><w n="110.1">Tout</w> <w n="110.2">est</w> <w n="110.3">dit</w> — <w n="110.4">Cela</w> <w n="110.5">fait</w> <w n="110.6">frémir</w> !</l>
							<l n="111" num="5.3"><w n="111.1">Comme</w> <w n="111.2">une</w> <w n="111.3">vigilante</w> <w n="111.4">aïeule</w>,</l>
							<l n="112" num="5.4"><w n="112.1">La</w> <w n="112.2">mort</w> <w n="112.3">le</w> <w n="112.4">regarde</w> <w n="112.5">dormir</w>.</l>
						</lg>
						<lg n="6">
							<l n="113" num="6.1">— <w n="113.1">Mais</w> <w n="113.2">non</w> ! <w n="113.3">Lorsque</w> <w n="113.4">par</w> <w n="113.5">les</w> <w n="113.6">espaces</w></l>
							<l n="114" num="6.2"><w n="114.1">L</w>’<w n="114.2">hirondelle</w> <w n="114.3">fuyant</w> <w n="114.4">l</w>’<w n="114.5">hiver</w></l>
							<l n="115" num="6.3"><w n="115.1">Repose</w> <w n="115.2">un</w> <w n="115.3">peu</w> <w n="115.4">ses</w> <w n="115.5">ailes</w> <w n="115.6">lasses</w></l>
							<l n="116" num="6.4"><w n="116.1">Sur</w> <w n="116.2">l</w>’<w n="116.3">onde</w> <w n="116.4">en</w> <w n="116.5">courroux</w> <w n="116.6">de</w> <w n="116.7">la</w> <w n="116.8">mer</w>,</l>
						</lg>
						<lg n="7">
							<l n="117" num="7.1"><w n="117.1">La</w> <w n="117.2">lame</w> <w n="117.3">énorme</w>, <w n="117.4">dont</w> <w n="117.5">la</w> <w n="117.6">chute</w></l>
							<l n="118" num="7.2"><w n="118.1">Pourrait</w> <w n="118.2">écraser</w> <w n="118.3">un</w> <w n="118.4">vaisseau</w>,</l>
							<l n="119" num="7.3"><w n="119.1">Offre</w> <w n="119.2">un</w> <w n="119.3">repos</w> <w n="119.4">d</w>’<w n="119.5">une</w> <w n="119.6">minute</w></l>
							<l n="120" num="7.4"><w n="120.1">A</w> <w n="120.2">la</w> <w n="120.3">fatigue</w> <w n="120.4">de</w> <w n="120.5">l</w>’<w n="120.6">oiseau</w>…</l>
						</lg>
					</div>
				</div></body></text></TEI>