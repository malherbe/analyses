<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Sonnets intimes et Poèmes inédits</title>
				<title type="medium">Édition électronique</title>
				<author key="COP">
					<name>
						<forename>François</forename>
						<surname>COPPÉE</surname>
					</name>
					<date from="1842" to="1908">1842-1908</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1824 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">COP_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Sonnets intimes et Poèmes inédits</title>
						<author>François Coppée</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/francoiscoppesonetsintimes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Poésies complètes de François Coppée</title>
						<author>François Coppée</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre</publisher>
							<date when="1925">1925</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1925">1925</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			<change when="2017-11-30" who="RR">Les vers incomplets du refrain du poème "RONDE D’ENFANTS AUX TUILERIES" ("Les lilas passeront, etc.") ont été remplacés par la strophe du refrain complet.</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">DEUXIÈME PARTIE</head><div type="poem" key="COP151">
					<head type="main">A LA MÉMOIRE DE JOSÉPHIN SOULARY</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">L</w>’<w n="1.2">art</w> <w n="1.3">ne</w> <w n="1.4">peut</w> <w n="1.5">garder</w> <w n="1.6">son</w> <w n="1.7">trésor</w>.</l>
						<l n="2" num="1.2"><w n="2.1">La</w> <w n="2.2">statue</w> <w n="2.3">en</w> <w n="2.4">marbre</w> ! <w n="2.5">On</w> <w n="2.6">la</w> <w n="2.7">casse</w>.</l>
						<l n="3" num="1.3"><w n="3.1">Le</w> <w n="3.2">tableau</w> ! <w n="3.3">La</w> <w n="3.4">couleur</w> <w n="3.5">en</w> <w n="3.6">passe</w></l>
						<l n="4" num="1.4"><w n="4.1">Et</w> <w n="4.2">prend</w> <w n="4.3">des</w> <w n="4.4">tons</w> <w n="4.5">de</w> <w n="4.6">vieux</w> <w n="4.7">décor</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Vénus</w> <w n="5.2">est</w> <w n="5.3">sur</w> <w n="5.4">le</w> <w n="5.5">socle</w> <w n="5.6">encor</w>,</l>
						<l n="6" num="2.2"><w n="6.1">Mais</w> <w n="6.2">sans</w> <w n="6.3">bras</w>, <w n="6.4">un</w> <w n="6.5">chancre</w> <w n="6.6">à</w> <w n="6.7">la</w> <w n="6.8">face</w>.</l>
						<l n="7" num="2.3"><w n="7.1">Joconde</w> <w n="7.2">verdit</w> <w n="7.3">et</w> <w n="7.4">s</w>’<w n="7.5">efface</w>,</l>
						<l n="8" num="2.4"><w n="8.1">Au</w> <w n="8.2">Louvre</w>, <w n="8.3">dans</w> <w n="8.4">son</w> <w n="8.5">cadre</w> <w n="8.6">d</w>’<w n="8.7">or</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">N</w>’<w n="9.2">est</w>-<w n="9.3">il</w> <w n="9.4">point</w> <w n="9.5">d</w>’<w n="9.6">ouvrage</w> <w n="9.7">qui</w> <w n="9.8">dure</w> ?</l>
						<l n="10" num="3.2"><w n="10.1">Si</w>, <w n="10.2">ce</w> <w n="10.3">camée</w> <w n="10.4">en</w> <w n="10.5">pierre</w> <w n="10.6">dure</w>,</l>
						<l n="11" num="3.3"><w n="11.1">Cette</w> <w n="11.2">médaille</w> <w n="11.3">en</w> <w n="11.4">argent</w> <w n="11.5">fin</w>.</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1"><w n="12.1">Quand</w> <w n="12.2">moisira</w> <w n="12.3">tel</w> <w n="12.4">long</w> <w n="12.5">poème</w>,</l>
						<l n="13" num="4.2"><w n="13.1">Sois</w> <w n="13.2">tranquille</w>, <w n="13.3">on</w> <w n="13.4">lira</w> <w n="13.5">quand</w> <w n="13.6">même</w></l>
						<l n="14" num="4.3"><w n="14.1">Tes</w> <w n="14.2">sonnets</w>, <w n="14.3">maître</w> <w n="14.4">Joséphin</w>.</l>
					</lg>
				</div></body></text></TEI>