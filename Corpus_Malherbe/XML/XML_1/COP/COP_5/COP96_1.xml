<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Sonnets intimes et Poèmes inédits</title>
				<title type="medium">Édition électronique</title>
				<author key="COP">
					<name>
						<forename>François</forename>
						<surname>COPPÉE</surname>
					</name>
					<date from="1842" to="1908">1842-1908</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1824 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">COP_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Sonnets intimes et Poèmes inédits</title>
						<author>François Coppée</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/francoiscoppesonetsintimes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Poésies complètes de François Coppée</title>
						<author>François Coppée</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre</publisher>
							<date when="1925">1925</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1925">1925</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			<change when="2017-11-30" who="RR">Les vers incomplets du refrain du poème "RONDE D’ENFANTS AUX TUILERIES" ("Les lilas passeront, etc.") ont été remplacés par la strophe du refrain complet.</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PREMIÈRE PARTIE</head><div type="poem" key="COP96">
					<head type="main">LE PASSANT</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Sous</w> <w n="1.2">le</w> <w n="1.3">bandeau</w> <w n="1.4">trop</w> <w n="1.5">lourd</w> <w n="1.6">pour</w> <w n="1.7">son</w> <w n="1.8">front</w> <w n="1.9">de</w> <w n="1.10">seize</w> <w n="1.11">ans</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Assise</w> <w n="2.2">sur</w> <w n="2.3">un</w> <w n="2.4">trône</w> <w n="2.5">aux</w> <w n="2.6">longs</w> <w n="2.7">rideaux</w> <w n="2.8">pesants</w></l>
						<l n="3" num="1.3"><w n="3.1">Où</w> <w n="3.2">l</w>’<w n="3.3">orgueil</w> <w n="3.4">brodé</w> <w n="3.5">d</w>’<w n="3.6">or</w> <w n="3.7">des</w> <w n="3.8">blasons</w> <w n="3.9">s</w>’<w n="3.10">écartèle</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Couverte</w> <w n="4.2">de</w> <w n="4.3">lampas</w> <w n="4.4">et</w> <w n="4.5">d</w>’<w n="4.6">antique</w> <w n="4.7">dentelle</w>,</l>
						<l n="5" num="1.5"><w n="5.1">Blanche</w> <w n="5.2">aux</w> <w n="5.3">longs</w> <w n="5.4">cheveux</w> <w n="5.5">noirs</w>, <w n="5.6">ayant</w> <w n="5.7">dans</w> <w n="5.8">ses</w> <w n="5.9">yeux</w> <w n="5.10">noirs</w></l>
						<l n="6" num="1.6"><w n="6.1">L</w>’<w n="6.2">éclat</w> <w n="6.3">resplendissant</w> <w n="6.4">de</w> <w n="6.5">l</w>’<w n="6.6">étoile</w> <w n="6.7">des</w> <w n="6.8">soirs</w>,</l>
						<l n="7" num="1.7"><w n="7.1">Et</w> <w n="7.2">triste</w> <w n="7.3">doucement</w>, <w n="7.4">se</w> <w n="7.5">tient</w> <w n="7.6">la</w> <w n="7.7">jeune</w> <w n="7.8">reine</w></l>
						<l n="8" num="1.8"><w n="8.1">Par</w> <w n="8.2">la</w> <w n="8.3">naissance</w> <w n="8.4">et</w> <w n="8.5">par</w> <w n="8.6">la</w> <w n="8.7">beauté</w> <w n="8.8">souveraine</w>.</l>
						<l n="9" num="1.9"><w n="9.1">La</w> <w n="9.2">fenêtre</w> <w n="9.3">est</w> <w n="9.4">ouverte</w>, <w n="9.5">et</w>, <w n="9.6">splendide</w> <w n="9.7">décor</w>,</l>
						<l n="10" num="1.10"><w n="10.1">Elle</w> <w n="10.2">voit</w> <w n="10.3">des</w> <w n="10.4">forêts</w> <w n="10.5">où</w> <w n="10.6">résonne</w> <w n="10.7">le</w> <w n="10.8">cor</w>,</l>
						<l n="11" num="1.11"><w n="11.1">Des</w> <w n="11.2">donjons</w> <w n="11.3">sur</w> <w n="11.4">des</w> <w n="11.5">rocs</w> <w n="11.6">plus</w> <w n="11.7">hauts</w> <w n="11.8">que</w> <w n="11.9">les</w> <w n="11.10">orages</w>,</l>
						<l n="12" num="1.12"><w n="12.1">Des</w> <w n="12.2">vals</w> <w n="12.3">et</w> <w n="12.4">des</w> <w n="12.5">coteaux</w> <w n="12.6">aux</w> <w n="12.7">riches</w> <w n="12.8">pâturages</w>,</l>
						<l n="13" num="1.13"><w n="13.1">Tout</w> <w n="13.2">un</w> <w n="13.3">royaume</w> <w n="13.4">libre</w> <w n="13.5">et</w> <w n="13.6">fort</w> <w n="13.7">par</w> <w n="13.8">le</w> <w n="13.9">travail</w>.</l>
						<l n="14" num="1.14"><w n="14.1">Dans</w> <w n="14.2">le</w> <w n="14.3">cadre</w> <w n="14.4">borné</w> <w n="14.5">que</w> <w n="14.6">forme</w> <w n="14.7">le</w> <w n="14.8">vitrail</w></l>
						<l n="15" num="1.15"><w n="15.1">Et</w> <w n="15.2">qu</w>’<w n="15.3">entoure</w> <w n="15.4">un</w> <w n="15.5">frisson</w> <w n="15.6">de</w> <w n="15.7">fraîches</w> <w n="15.8">giroflées</w>,</l>
						<l n="16" num="1.16"><w n="16.1">Elle</w> <w n="16.2">voit</w> <w n="16.3">des</w> <w n="16.4">vaisseaux</w> <w n="16.5">aux</w> <w n="16.6">voilures</w> <w n="16.7">gonflées</w></l>
						<l n="17" num="1.17"><w n="17.1">Qui</w> <w n="17.2">remontent</w> <w n="17.3">le</w> <w n="17.4">fleuve</w> <w n="17.5">et</w> <w n="17.6">de</w> <w n="17.7">lourds</w> <w n="17.8">galions</w></l>
						<l n="18" num="1.18"><w n="18.1">Dont</w> <w n="18.2">le</w> <w n="18.3">ventre</w> <w n="18.4">bombé</w> <w n="18.5">crève</w> <w n="18.6">de</w> <w n="18.7">millions</w>.</l>
						<l n="19" num="1.19"><w n="19.1">Elle</w> <w n="19.2">n</w>’<w n="19.3">y</w> <w n="19.4">pense</w> <w n="19.5">pas</w>, <w n="19.6">elle</w> <w n="19.7">rêve</w>, <w n="19.8">elle</w> <w n="19.9">écoute</w></l>
						<l n="20" num="1.20"><w n="20.1">Le</w> <w n="20.2">zéphyr</w> — <w n="20.3">Elle</w> <w n="20.4">voit</w> <w n="20.5">défiler</w> <w n="20.6">sur</w> <w n="20.7">la</w> <w n="20.8">route</w></l>
						<l n="21" num="1.21"><w n="21.1">Les</w> <w n="21.2">bataillons</w> <w n="21.3">touffus</w> <w n="21.4">de</w> <w n="21.5">ses</w> <w n="21.6">pertuisaniers</w></l>
						<l n="22" num="1.22"><w n="22.1">Chamarrés</w> <w n="22.2">d</w>’<w n="22.3">or</w> <w n="22.4">de</w> <w n="22.5">pied</w> <w n="22.6">en</w> <w n="22.7">cap</w> <w n="22.8">par</w> <w n="22.9">ses</w> <w n="22.10">deniers</w>.</l>
						<l n="23" num="1.23"><w n="23.1">Elle</w> <w n="23.2">rêve</w>, <w n="23.3">et</w> <w n="23.4">sa</w> <w n="23.5">tête</w> <w n="23.6">adorable</w> <w n="23.7">s</w>’<w n="23.8">incline</w>.</l>
						<l n="24" num="1.24"><w n="24.1">Et</w> <w n="24.2">là</w>-<w n="24.3">bas</w>, <w n="24.4">descendant</w> <w n="24.5">de</w> <w n="24.6">la</w> <w n="24.7">verte</w> <w n="24.8">colline</w>,</l>
						<l n="25" num="1.25"><w n="25.1">Précédé</w> <w n="25.2">par</w> <w n="25.3">un</w> <w n="25.4">bruit</w> <w n="25.5">de</w> <w n="25.6">lointaines</w> <w n="25.7">chansons</w>,</l>
						<l n="26" num="1.26"><w n="26.1">Pensif</w> <w n="26.2">et</w> <w n="26.3">s</w>’<w n="26.4">arrêtant</w> <w n="26.5">pour</w> <w n="26.6">cueillir</w> <w n="26.7">aux</w> <w n="26.8">buissons</w></l>
						<l n="27" num="1.27"><w n="27.1">Des</w> <w n="27.2">lianes</w> <w n="27.3">dont</w> <w n="27.4">il</w> <w n="27.5">adorne</w> <w n="27.6">sa</w> <w n="27.7">guitare</w>,</l>
						<l n="28" num="1.28"><w n="28.1">Un</w> <w n="28.2">pâle</w> <w n="28.3">et</w> <w n="28.4">maigre</w> <w n="28.5">enfant</w> <w n="28.6">à</w> <w n="28.7">l</w>’<w n="28.8">allure</w> <w n="28.9">bizarre</w></l>
						<l n="29" num="1.29"><w n="29.1">S</w>’<w n="29.2">approche</w> <w n="29.3">et</w> <w n="29.4">voit</w> <w n="29.5">la</w> <w n="29.6">reine</w> <w n="29.7">assise</w> <w n="29.8">en</w> <w n="29.9">son</w> <w n="29.10">château</w>.</l>
						<l n="30" num="1.30"><w n="30.1">Celle</w>-<w n="30.2">ci</w> <w n="30.3">l</w>’<w n="30.4">aperçoit</w> <w n="30.5">qui</w> <w n="30.6">descend</w> <w n="30.7">du</w> <w n="30.8">coteau</w>.</l>
						<l n="31" num="1.31"><w n="31.1">Étonnée</w>, <w n="31.2">elle</w> <w n="31.3">tend</w> <w n="31.4">son</w> <w n="31.5">svelte</w> <w n="31.6">cou</w> <w n="31.7">de</w> <w n="31.8">cygne</w></l>
						<l n="32" num="1.32"><w n="32.1">Et</w> <w n="32.2">de</w> <w n="32.3">sa</w> <w n="32.4">main</w> <w n="32.5">exquise</w> <w n="32.6">elle</w> <w n="32.7">lui</w> <w n="32.8">fait</w> <w n="32.9">un</w> <w n="32.10">signe</w>.</l>
						<l n="33" num="1.33"><w n="33.1">Il</w> <w n="33.2">monte</w>, <w n="33.3">tout</w> <w n="33.4">tremblant</w> <w n="33.5">déjà</w> <w n="33.6">d</w>’<w n="33.7">un</w> <w n="33.8">vague</w> <w n="33.9">émoi</w>,</l>
						<l n="34" num="1.34"><w n="34.1">Et</w> <w n="34.2">la</w> <w n="34.3">reine</w> <w n="34.4">lui</w> <w n="34.5">dit</w> : « <w n="34.6">Chante</w> <w n="34.7">et</w> <w n="34.8">divertis</w>-<w n="34.9">moi</w>. »</l>
						<l n="35" num="1.35"><w n="35.1">Et</w> <w n="35.2">le</w> <w n="35.3">petit</w> <w n="35.4">chanteur</w>, <w n="35.5">tout</w> <w n="35.6">fier</w> <w n="35.7">au</w> <w n="35.8">fond</w> <w n="35.9">de</w> <w n="35.10">l</w>’<w n="35.11">âme</w>,</l>
						<l n="36" num="1.36"><w n="36.1">Prélude</w> ; <w n="36.2">mais</w> <w n="36.3">soudain</w>, <w n="36.4">en</w> <w n="36.5">voyant</w> <w n="36.6">cette</w> <w n="36.7">femme</w></l>
						<l n="37" num="1.37"><w n="37.1">Si</w> <w n="37.2">belle</w> <w n="37.3">lui</w> <w n="37.4">sourire</w> <w n="37.5">et</w> <w n="37.6">le</w> <w n="37.7">considérer</w>,</l>
						<l n="38" num="1.38"><w n="38.1">Il</w> <w n="38.2">jette</w> <w n="38.3">au</w> <w n="38.4">loin</w> <w n="38.5">son</w> <w n="38.6">luth</w> <w n="38.7">et</w> <w n="38.8">se</w> <w n="38.9">met</w> <w n="38.10">à</w> <w n="38.11">pleurer</w>.</l>
					</lg>
				</div></body></text></TEI>