<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Sonnets intimes et Poèmes inédits</title>
				<title type="medium">Édition électronique</title>
				<author key="COP">
					<name>
						<forename>François</forename>
						<surname>COPPÉE</surname>
					</name>
					<date from="1842" to="1908">1842-1908</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1824 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">COP_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Sonnets intimes et Poèmes inédits</title>
						<author>François Coppée</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/francoiscoppesonetsintimes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Poésies complètes de François Coppée</title>
						<author>François Coppée</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre</publisher>
							<date when="1925">1925</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1925">1925</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			<change when="2017-11-30" who="RR">Les vers incomplets du refrain du poème "RONDE D’ENFANTS AUX TUILERIES" ("Les lilas passeront, etc.") ont été remplacés par la strophe du refrain complet.</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">DEUXIÈME PARTIE</head><div type="poem" key="COP137">
					<head type="main">ÉCRIT SUR L’ALBUM DES CHATS <lb></lb>D’HENRIETTE RONNER</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Je</w> <w n="1.2">regarde</w>, <w n="1.3">en</w> <w n="1.4">ce</w> <w n="1.5">bel</w> <w n="1.6">album</w> <w n="1.7">paru</w> <w n="1.8">d</w>’<w n="1.9">hier</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Ces</w> <w n="2.2">chats</w> <w n="2.3">pris</w> <w n="2.4">sur</w> <w n="2.5">le</w> <w n="2.6">vif</w> <w n="2.7">avec</w> <w n="2.8">un</w> <w n="2.9">talent</w> <w n="2.10">rare</w>.</l>
						<l n="3" num="1.3"><w n="3.1">Jamais</w> <w n="3.2">il</w> <w n="3.3">ne</w> <w n="3.4">fut</w> <w n="3.5">mieux</w> <w n="3.6">compris</w>, <w n="3.7">je</w> <w n="3.8">le</w> <w n="3.9">déclare</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Le</w> <w n="4.2">tigre</w> <w n="4.3">familier</w>, <w n="4.4">caressant</w> <w n="4.5">quoique</w> <w n="4.6">fier</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Vos</w> <w n="5.2">félins</w> <w n="5.3">sont</w> <w n="5.4">exquis</w>, <w n="5.5">Henriette</w> <w n="5.6">Ronner</w>.</l>
						<l n="6" num="2.2"><w n="6.1">Je</w> <w n="6.2">les</w> <w n="6.3">admire</w> <w n="6.4">et</w>, <w n="6.5">non</w> <w n="6.6">sans</w> <w n="6.7">orgueil</w>, <w n="6.8">les</w> <w n="6.9">compare</w></l>
						<l n="7" num="2.3"><w n="7.1">Au</w> <w n="7.2">charmant</w> <w n="7.3">angora</w> <w n="7.4">dont</w> <w n="7.5">mon</w> <w n="7.6">logis</w> <w n="7.7">se</w> <w n="7.8">pare</w></l>
						<l n="8" num="2.4"><w n="8.1">Et</w> <w n="8.2">qui</w> <w n="8.3">vient</w> <w n="8.4">de</w> <w n="8.5">vêtir</w> <w n="8.6">sa</w> <w n="8.7">fourrure</w> <w n="8.8">d</w>’<w n="8.9">hiver</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Comme</w> <w n="9.2">vous</w>, <w n="9.3">pour</w> <w n="9.4">les</w> <w n="9.5">chats</w> <w n="9.6">j</w>’<w n="9.7">ai</w> <w n="9.8">tant</w> <w n="9.9">de</w> <w n="9.10">sympathies</w> !</l>
						<l n="10" num="3.2"><w n="10.1">Chez</w> <w n="10.2">moi</w>, <w n="10.3">j</w>’<w n="10.4">ai</w> <w n="10.5">vu</w> <w n="10.6">régner</w> <w n="10.7">de</w> <w n="10.8">longues</w> <w n="10.9">dynasties</w></l>
						<l n="11" num="3.3"><w n="11.1">De</w> <w n="11.2">ces</w> <w n="11.3">rois</w> <w n="11.4">fainéants</w> <w n="11.5">au</w> <w n="11.6">pelage</w> <w n="11.7">soyeux</w> ;</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1"><w n="12.1">Et</w>, <w n="12.2">dans</w> <w n="12.3">mon</w> <w n="12.4">calme</w> <w n="12.5">coin</w> <w n="12.6">de</w> <w n="12.7">vieux</w> <w n="12.8">célibataire</w>,</l>
						<l n="13" num="4.2"><w n="13.1">Toujours</w> <w n="13.2">les</w> <w n="13.3">chats</w> <w n="13.4">prudents</w>, <w n="13.5">les</w> <w n="13.6">chats</w> <w n="13.7">silencieux</w></l>
						<l n="14" num="4.3"><w n="14.1">Promènent</w> <w n="14.2">leur</w> <w n="14.3">beauté</w>, <w n="14.4">leur</w> <w n="14.5">grâce</w> <w n="14.6">et</w> <w n="14.7">leur</w> <w n="14.8">mystère</w>.</l>
					</lg>
				</div></body></text></TEI>