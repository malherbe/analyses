<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Sonnets intimes et Poèmes inédits</title>
				<title type="medium">Édition électronique</title>
				<author key="COP">
					<name>
						<forename>François</forename>
						<surname>COPPÉE</surname>
					</name>
					<date from="1842" to="1908">1842-1908</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1824 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">COP_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Sonnets intimes et Poèmes inédits</title>
						<author>François Coppée</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/francoiscoppesonetsintimes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Poésies complètes de François Coppée</title>
						<author>François Coppée</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre</publisher>
							<date when="1925">1925</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1925">1925</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			<change when="2017-11-30" who="RR">Les vers incomplets du refrain du poème "RONDE D’ENFANTS AUX TUILERIES" ("Les lilas passeront, etc.") ont été remplacés par la strophe du refrain complet.</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">DEUXIÈME PARTIE</head><div type="poem" key="COP123">
					<head type="main">PORTRAIT <lb></lb>DE Mlle SABINE CAROLUS DURAN <lb></lb>PAR SON PÈRE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Près</w> <w n="1.2">du</w> <w n="1.3">grand</w> <w n="1.4">lévrier</w> <w n="1.5">d</w>’<w n="1.6">Écosse</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Toute</w> <w n="2.2">en</w> <w n="2.3">velours</w> <w n="2.4">et</w> <w n="2.5">satin</w> <w n="2.6">gris</w>,</l>
						<l n="3" num="1.3"><w n="3.1">L</w>’<w n="3.2">enfant</w>, <w n="3.3">qu</w>’<w n="3.4">on</w> <w n="3.5">devine</w> <w n="3.6">précoce</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Ouvre</w> <w n="4.2">sur</w> <w n="4.3">vous</w> <w n="4.4">ses</w> <w n="4.5">yeux</w> <w n="4.6">surpris</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Elle</w> <w n="5.2">est</w> <w n="5.3">si</w> <w n="5.4">jeune</w> ! <w n="5.5">tout</w> <w n="5.6">lui</w> <w n="5.7">semble</w></l>
						<l n="6" num="2.2"><w n="6.1">Comme</w> <w n="6.2">elle</w> <w n="6.3">charmant</w>, <w n="6.4">pur</w> <w n="6.5">et</w> <w n="6.6">beau</w> ;</l>
						<l n="7" num="2.3"><w n="7.1">Une</w> <w n="7.2">rose</w> — <w n="7.3">qui</w> <w n="7.4">lui</w> <w n="7.5">ressemble</w> —</l>
						<l n="8" num="2.4"><w n="8.1">S</w>’<w n="8.2">épanouit</w> <w n="8.3">à</w> <w n="8.4">son</w> <w n="8.5">chapeau</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Et</w> <w n="9.2">ses</w> <w n="9.3">mignonnes</w> <w n="9.4">jambes</w> <w n="9.5">nues</w></l>
						<l n="10" num="3.2"><w n="10.1">Hors</w> <w n="10.2">de</w> <w n="10.3">leurs</w> <w n="10.4">petits</w> <w n="10.5">bas</w> <w n="10.6">soyeux</w></l>
						<l n="11" num="3.3"><w n="11.1">Sont</w>, <w n="11.2">dans</w> <w n="11.3">leurs</w> <w n="11.4">grâces</w> <w n="11.5">ingénues</w>,</l>
						<l n="12" num="3.4"><w n="12.1">Aussi</w> <w n="12.2">naïves</w> <w n="12.3">que</w> <w n="12.4">ses</w> <w n="12.5">yeux</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Comme</w> <w n="13.2">son</w> <w n="13.3">existence</w> <w n="13.4">enfuie</w></l>
						<l n="14" num="4.2"><w n="14.1">N</w>’<w n="14.2">a</w> <w n="14.3">pas</w> <w n="14.4">un</w> <w n="14.5">souvenir</w> <w n="14.6">troublant</w>,</l>
						<l n="15" num="4.3"><w n="15.1">Elle</w> <w n="15.2">est</w> <w n="15.3">confiante</w> <w n="15.4">et</w> <w n="15.5">s</w>’<w n="15.6">appuie</w></l>
						<l n="16" num="4.4"><w n="16.1">Au</w> <w n="16.2">collier</w> <w n="16.3">du</w> <w n="16.4">chien</w> <w n="16.5">noir</w> <w n="16.6">et</w> <w n="16.7">blanc</w> ;</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Et</w> <w n="17.2">devant</w> <w n="17.3">l</w>’<w n="17.4">air</w> <w n="17.5">bon</w> <w n="17.6">et</w> <w n="17.7">prospère</w></l>
						<l n="18" num="5.2"><w n="18.1">Du</w> <w n="18.2">petit</w> <w n="18.3">modèle</w> <w n="18.4">enchanté</w></l>
						<l n="19" num="5.3"><w n="19.1">On</w> <w n="19.2">sent</w> <w n="19.3">combien</w> <w n="19.4">l</w>’<w n="19.5">artiste</w>-<w n="19.6">père</w></l>
						<l n="20" num="5.4"><w n="20.1">Adore</w> <w n="20.2">son</w> <w n="20.3">enfant</w> <w n="20.4">gâté</w>.</l>
					</lg>
					<closer>
						<dateline>
							<date when="1875">1875.</date>
						</dateline>
					</closer>
				</div></body></text></TEI>