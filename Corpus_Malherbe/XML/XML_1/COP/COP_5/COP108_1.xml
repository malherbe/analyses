<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Sonnets intimes et Poèmes inédits</title>
				<title type="medium">Édition électronique</title>
				<author key="COP">
					<name>
						<forename>François</forename>
						<surname>COPPÉE</surname>
					</name>
					<date from="1842" to="1908">1842-1908</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1824 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">COP_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Sonnets intimes et Poèmes inédits</title>
						<author>François Coppée</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/francoiscoppesonetsintimes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Poésies complètes de François Coppée</title>
						<author>François Coppée</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre</publisher>
							<date when="1925">1925</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1925">1925</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			<change when="2017-11-30" who="RR">Les vers incomplets du refrain du poème "RONDE D’ENFANTS AUX TUILERIES" ("Les lilas passeront, etc.") ont été remplacés par la strophe du refrain complet.</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PREMIÈRE PARTIE</head><div type="poem" key="COP108">
					<head type="main">AU THÉÂTRE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">On</w> <w n="1.2">jouait</w> <w n="1.3">un</w> <hi rend="ital"><w n="1.4">opéra</w>-<w n="1.5">boufe</w></hi>.</l>
						<l n="2" num="1.2"><w n="2.1">C</w>’<w n="2.2">est</w> <w n="2.3">le</w> <w n="2.4">nom</w> <w n="2.5">qu</w>’<w n="2.6">on</w> <w n="2.7">donne</w> <w n="2.8">aujourd</w>’<w n="2.9">hui</w></l>
						<l n="3" num="1.3"><w n="3.1">Aux</w> <w n="3.2">farces</w> <w n="3.3">impures</w> <w n="3.4">dont</w> <w n="3.5">pouffe</w></l>
						<l n="4" num="1.4"><w n="4.1">Notre</w> <w n="4.2">siècle</w> <w n="4.3">si</w> <w n="4.4">fier</w> <w n="4.5">de</w> <w n="4.6">lui</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">On</w> <w n="5.2">riait</w> <w n="5.3">très</w> <w n="5.4">fort</w>. <w n="5.5">La</w> <w n="5.6">machine</w></l>
						<l n="6" num="2.2"><w n="6.1">Était</w> <w n="6.2">bête</w>, <w n="6.3">et</w> <w n="6.4">sale</w> <w n="6.5">souvent</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Et</w> <w n="7.2">se</w> <w n="7.3">passait</w> <w n="7.4">dans</w> <w n="7.5">cette</w> <w n="7.6">Chine</w></l>
						<l n="8" num="2.4"><w n="8.1">De</w> <w n="8.2">théâtre</w> <w n="8.3">et</w> <w n="8.4">de</w> <w n="8.5">paravent</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Poussahs</w>, <w n="9.2">pagodes</w> <w n="9.3">et</w> <w n="9.4">lanternes</w>,</l>
						<l n="10" num="3.2"><w n="10.1">Vous</w> <w n="10.2">voyez</w> <w n="10.3">la</w> <w n="10.4">chose</w> <w n="10.5">d</w>’<w n="10.6">ici</w>.</l>
						<l n="11" num="3.3"><w n="11.1">Et</w> <w n="11.2">les</w> <w n="11.3">Athéniens</w> <w n="11.4">modernes</w></l>
						<l n="12" num="3.4"><w n="12.1">Bissaient</w> <w n="12.2">les</w> <w n="12.3">plus</w> <w n="12.4">honteux</w> <w n="12.5">lazzi</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Deux</w> <w n="13.2">mandarins</w> — <w n="13.3">on</w> <w n="13.4">pâmait</w> <w n="13.5">d</w>’<w n="13.6">aise</w></l>
						<l n="14" num="4.2"><w n="14.1">A</w> <w n="14.2">ce</w> <w n="14.3">comique</w> <w n="14.4">et</w> <w n="14.5">fin</w> <w n="14.6">détail</w> —</l>
						<l n="15" num="4.3"><w n="15.1">Étaient</w> <w n="15.2">l</w>’<w n="15.3">un</w> <w n="15.4">maigre</w> <w n="15.5">et</w> <w n="15.6">l</w>’<w n="15.7">autre</w> <w n="15.8">obèse</w></l>
						<l n="16" num="4.4"><w n="16.1">Et</w> <w n="16.2">coquetaient</w> <w n="16.3">de</w> <w n="16.4">l</w>’<w n="16.5">éventail</w> ;</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Et</w> <w n="17.2">la</w> <w n="17.3">convoitise</w> <w n="17.4">sournoise</w></l>
						<l n="18" num="5.2"><w n="18.1">Des</w> <w n="18.2">messieurs</w> <w n="18.3">chauves</w> <w n="18.4">et</w> <w n="18.5">pesants</w></l>
						<l n="19" num="5.3"><w n="19.1">Lorgnaient</w> <w n="19.2">une</w> <w n="19.3">jeune</w> <w n="19.4">Chinoise</w></l>
						<l n="20" num="5.4"><w n="20.1">Âgée</w> <w n="20.2">à</w> <w n="20.3">peine</w> <w n="20.4">de</w> <w n="20.5">seize</w> <w n="20.6">ans</w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Adorable</w>, <w n="21.2">l</w>’<w n="21.3">air</w> <w n="21.4">un</w> <w n="21.5">peu</w> <w n="21.6">bête</w>,</l>
						<l n="22" num="6.2"><w n="22.1">Toute</w> <w n="22.2">de</w> <w n="22.3">gaze</w> <w n="22.4">et</w> <w n="22.5">de</w> <w n="22.6">paillon</w>,</l>
						<l n="23" num="6.3"><w n="23.1">Deux</w> <w n="23.2">épingles</w> <w n="23.3">d</w>’<w n="23.4">or</w> <w n="23.5">sur</w> <w n="23.6">la</w> <w n="23.7">tête</w>,</l>
						<l n="24" num="6.4"><w n="24.1">Elle</w> <w n="24.2">semblait</w> <w n="24.3">un</w> <w n="24.4">papillon</w>.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">Elle</w> <w n="25.2">n</w>’<w n="25.3">était</w> <w n="25.4">pas</w> <w n="25.5">même</w> <w n="25.6">émue</w></l>
						<l n="26" num="7.2"><w n="26.1">Et</w>, <w n="26.2">toute</w> <w n="26.3">rose</w> <w n="26.4">sous</w> <w n="26.5">son</w> <w n="26.6">fard</w>,</l>
						<l n="27" num="7.3"><w n="27.1">Forçait</w> <w n="27.2">sa</w> <w n="27.3">frêle</w> <w n="27.4">voix</w> <w n="27.5">en</w> <w n="27.6">mue</w></l>
						<l n="28" num="7.4"><w n="28.1">Qu</w>’<w n="28.2">étouffait</w> <w n="28.3">l</w>’<w n="28.4">orchestre</w> <w n="28.5">bavard</w>.</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1"><w n="29.1">C</w>’<w n="29.2">était</w> <w n="29.3">bien</w> <w n="29.4">la</w> <w n="29.5">grâce</w> <w n="29.6">éphémère</w>,</l>
						<l n="30" num="8.2"><w n="30.1">L</w>’<w n="30.2">enfance</w>, <w n="30.3">la</w> <w n="30.4">gaîté</w>, <w n="30.5">l</w>’<w n="30.6">essor</w>,</l>
						<l n="31" num="8.3"><w n="31.1">Et</w> <w n="31.2">l</w>’<w n="31.3">on</w> <w n="31.4">devinait</w> <w n="31.5">que</w> <w n="31.6">sa</w> <w n="31.7">mère</w></l>
						<l n="32" num="8.4"><w n="32.1">Ne</w> <w n="32.2">l</w>’<w n="32.3">avait</w> <w n="32.4">pas</w> <w n="32.5">vendue</w> <w n="32.6">encor</w>.</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1"><w n="33.1">Je</w> <w n="33.2">me</w> <w n="33.3">sentais</w> <w n="33.4">rougir</w> <w n="33.5">de</w> <w n="33.6">honte</w></l>
						<l n="34" num="9.2"><w n="34.1">Quand</w> <w n="34.2">elle</w> <w n="34.3">disait</w> <w n="34.4">certains</w> <w n="34.5">mots</w>,</l>
						<l n="35" num="9.3"><w n="35.1">Comme</w> <w n="35.2">la</w> <w n="35.3">princesse</w> <w n="35.4">du</w> <w n="35.5">conte</w></l>
						<l n="36" num="9.4"><w n="36.1">Qui</w> <w n="36.2">crachait</w> <w n="36.3">serpents</w> <w n="36.4">et</w> <w n="36.5">crapauds</w>.</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1"><w n="37.1">Je</w> <w n="37.2">songeais</w> <w n="37.3">à</w> <w n="37.4">la</w> <w n="37.5">demoiselle</w></l>
						<l n="38" num="10.2"><w n="38.1">Qu</w>’<w n="38.2">on</w> <w n="38.3">invite</w> <w n="38.4">en</w> <w n="38.5">saluant</w> <w n="38.6">bas</w>,</l>
						<l n="39" num="10.3"><w n="39.1">Et</w>, <w n="39.2">baissant</w> <w n="39.3">ses</w> <w n="39.4">yeux</w> <w n="39.5">de</w> <w n="39.6">gazelle</w>,</l>
						<l n="40" num="10.4"><w n="40.1">Qui</w> <w n="40.2">répond</w> : « <w n="40.3">Je</w> <w n="40.4">ne</w> <w n="40.5">valse</w> <w n="40.6">pas</w> ; »</l>
					</lg>
					<lg n="11">
						<l n="41" num="11.1"><w n="41.1">A</w> <w n="41.2">l</w>’<w n="41.3">héritière</w> <w n="41.4">très</w> <w n="41.5">titrée</w></l>
						<l n="42" num="11.2"><w n="42.1">De</w> <w n="42.2">l</w>’<w n="42.3">altier</w> <w n="42.4">faubourg</w> <w n="42.5">Saint</w>-<w n="42.6">Germain</w></l>
						<l n="43" num="11.3"><w n="43.1">Que</w> <w n="43.2">suit</w> <w n="43.3">un</w> <w n="43.4">laquais</w> <w n="43.5">en</w> <w n="43.6">livrée</w></l>
						<l n="44" num="11.4"><w n="44.1">Portant</w> <w n="44.2">le</w> <w n="44.3">missel</w> <w n="44.4">à</w> <w n="44.5">la</w> <w n="44.6">main</w> ;</l>
					</lg>
					<lg n="12">
						<l n="45" num="12.1"><w n="45.1">Et</w> <w n="45.2">même</w> <w n="45.3">à</w> <w n="45.4">la</w> <w n="45.5">libre</w> <w n="45.6">grisette</w></l>
						<l n="46" num="12.2"><w n="46.1">Que</w> <w n="46.2">font</w> <w n="46.3">danser</w> <w n="46.4">les</w> <w n="46.5">calicots</w></l>
						<l n="47" num="12.3"><w n="47.1">Dans</w> <w n="47.2">des</w> <w n="47.3">bals</w> <w n="47.4">ayant</w> <w n="47.5">pour</w> <w n="47.6">musette</w></l>
						<l n="48" num="12.4"><w n="48.1">Des</w> <w n="48.2">mirlitons</w> <w n="48.3">peu</w> <w n="48.4">musicaux</w>.</l>
					</lg>
					<lg n="13">
						<l n="49" num="13.1"><w n="49.1">Et</w> <w n="49.2">je</w> <w n="49.3">me</w> <w n="49.4">disais</w> : « <w n="49.5">Ouvrière</w>,</l>
						<l n="50" num="13.2"><w n="50.1">Fille</w> <w n="50.2">de</w> <w n="50.3">noble</w> <w n="50.4">ou</w> <w n="50.5">de</w> <w n="50.6">bourgeois</w>,</l>
						<l n="51" num="13.3"><w n="51.1">A</w> <w n="51.2">cette</w> <w n="51.3">heure</w> <w n="51.4">fait</w> <w n="51.5">sa</w> <w n="51.6">prière</w></l>
						<l n="52" num="13.4"><w n="52.1">Ou</w> <w n="52.2">rêve</w> <w n="52.3">à</w> <w n="52.4">l</w>’<w n="52.5">amour</w> <w n="52.6">de</w> <w n="52.7">son</w> <w n="52.8">choix</w> ;</l>
					</lg>
					<lg n="14">
						<l n="53" num="14.1">« <w n="53.1">Et</w>, <w n="53.2">pendant</w> <w n="53.3">ce</w> <w n="53.4">temps</w>-<w n="53.5">là</w>, <w n="53.6">le</w> <w n="53.7">père</w>,</l>
						<l n="54" num="14.2"><w n="54.1">Le</w> <w n="54.2">frère</w>, <w n="54.3">même</w> <w n="54.4">un</w> <w n="54.5">fiancé</w>,</l>
						<l n="55" num="14.3"><w n="55.1">Sont</w> <w n="55.2">peut</w>-<w n="55.3">être</w> <w n="55.4">dans</w> <w n="55.5">ce</w> <w n="55.6">repaire</w>,</l>
						<l n="56" num="14.4"><w n="56.1">Devant</w> <w n="56.2">ce</w> <w n="56.3">spectacle</w> <w n="56.4">insensé</w>,</l>
					</lg>
					<lg n="15">
						<l n="57" num="15.1">« <w n="57.1">Et</w>, <w n="57.2">dans</w> <w n="57.3">le</w> <w n="57.4">vertige</w> <w n="57.5">où</w> <w n="57.6">les</w> <w n="57.7">plonge</w></l>
						<l n="58" num="15.2"><w n="58.1">Cet</w> <w n="58.2">art</w> <w n="58.3">érotique</w> <w n="58.4">et</w> <w n="58.5">scabreux</w>,</l>
						<l n="59" num="15.3"><w n="59.1">Sans</w> <w n="59.2">doute</w> <w n="59.3">qu</w>’<w n="59.4">aucun</w> <w n="59.5">d</w>’<w n="59.6">eux</w> <w n="59.7">ne</w> <w n="59.8">songe</w></l>
						<l n="60" num="15.4"><w n="60.1">A</w> <w n="60.2">cette</w> <w n="60.3">enfant</w> <w n="60.4">qu</w>’<w n="60.5">on</w> <w n="60.6">perd</w> <w n="60.7">pour</w> <w n="60.8">eux</w>.</l>
					</lg>
					<lg n="16">
						<l n="61" num="16.1">« <w n="61.1">Siècle</w> <w n="61.2">de</w> <w n="61.3">toi</w>-<w n="61.4">même</w> <w n="61.5">idolâtre</w>,</l>
						<l n="62" num="16.2"><w n="62.1">Époque</w> <w n="62.2">aux</w> <w n="62.3">grands</w> <w n="62.4">mots</w> <w n="62.5">puérils</w>,</l>
						<l n="63" num="16.3"><w n="63.1">Les</w> <w n="63.2">spectacles</w> <w n="63.3">de</w> <w n="63.4">ton</w> <w n="63.5">théâtre</w></l>
						<l n="64" num="16.4"><w n="64.1">Sont</w> <w n="64.2">moins</w> <w n="64.3">sanglants</w>, <w n="64.4">mais</w> <w n="64.5">sont</w> <w n="64.6">plus</w> <w n="64.7">vils</w>.</l>
					</lg>
					<lg n="17">
						<l n="65" num="17.1">« <w n="65.1">Cette</w> <w n="65.2">innocente</w>, <w n="65.3">encore</w> <w n="65.4">dupe</w>,</l>
						<l n="66" num="17.2"><w n="66.1">Qui</w> <w n="66.2">ne</w> <w n="66.3">sait</w> <w n="66.4">pas</w> <w n="66.5">dans</w> <w n="66.6">quel</w> <w n="66.7">dessein</w></l>
						<l n="67" num="17.3"><w n="67.1">On</w> <w n="67.2">fait</w> <w n="67.3">aussi</w> <w n="67.4">courte</w> <w n="67.5">sa</w> <w n="67.6">jupe</w></l>
						<l n="68" num="17.4"><w n="68.1">Et</w> <w n="68.2">l</w>’<w n="68.3">on</w> <w n="68.4">découvre</w> <w n="68.5">autant</w> <w n="68.6">son</w> <w n="68.7">sein</w>,</l>
					</lg>
					<lg n="18">
						<l n="69" num="18.1">« <w n="69.1">Cette</w> <w n="69.2">victime</w>, <w n="69.3">c</w>’<w n="69.4">est</w> <w n="69.5">la</w> <w n="69.6">tienne</w>,</l>
						<l n="70" num="18.2"><w n="70.1">Multitude</w> <w n="70.2">aux</w> <w n="70.3">instincts</w> <w n="70.4">fangeux</w> !</l>
						<l n="71" num="18.3"><w n="71.1">C</w>’<w n="71.2">est</w> <w n="71.3">toujours</w> <w n="71.4">la</w> <w n="71.5">jeune</w> <w n="71.6">chrétienne</w></l>
						<l n="72" num="18.4"><w n="72.1">Toute</w> <w n="72.2">nue</w> <w n="72.3">au</w> <w n="72.4">milieu</w> <w n="72.5">des</w> <w n="72.6">jeux</w> ;</l>
					</lg>
					<lg n="19">
						<l n="73" num="19.1">« <w n="73.1">Ce</w> <w n="73.2">sont</w> <w n="73.3">toujours</w> <w n="73.4">tes</w> <w n="73.5">mille</w> <w n="73.6">têtes</w></l>
						<l n="74" num="19.2"><w n="74.1">Fixant</w> <w n="74.2">leurs</w> <w n="74.3">yeux</w> <w n="74.4">de</w> <w n="74.5">basilic</w></l>
						<l n="75" num="19.3"><w n="75.1">Sur</w> <w n="75.2">la</w> <w n="75.3">femme</w> <w n="75.4">livrée</w> <w n="75.5">aux</w> <w n="75.6">bêtes</w>,</l>
						<l n="76" num="19.4"><w n="76.1">Sur</w> <w n="76.2">l</w>’<w n="76.3">enfant</w> <w n="76.4">jetée</w> <w n="76.5">au</w> <w n="76.6">public</w> ! »</l>
					</lg>
					<lg n="20">
						<l n="77" num="20.1">— <w n="77.1">Je</w> <w n="77.2">m</w>’<w n="77.3">indignais</w>, <w n="77.4">et</w>, <w n="77.5">sur</w> <w n="77.6">la</w> <w n="77.7">scène</w>,</l>
						<l n="78" num="20.2"><w n="78.1">Celle</w> <w n="78.2">qui</w> <w n="78.3">n</w>’<w n="78.4">avait</w> <w n="78.5">pas</w> <w n="78.6">seize</w> <w n="78.7">ans</w></l>
						<l n="79" num="20.3"><w n="79.1">Chantait</w> <w n="79.2">un</w> <w n="79.3">couplet</w> <w n="79.4">trop</w> <w n="79.5">obscène</w></l>
						<l n="80" num="20.4"><w n="80.1">Pour</w> <w n="80.2">qu</w>’<w n="80.3">elle</w> <w n="80.4">en</w> <w n="80.5">pût</w> <w n="80.6">savoir</w> <w n="80.7">le</w> <w n="80.8">sens</w>,</l>
					</lg>
					<lg n="21">
						<l n="81" num="21.1"><w n="81.1">Et</w>, <w n="81.2">l</w>’<w n="81.3">horreur</w> <w n="81.4">crispant</w> <w n="81.5">ma</w> <w n="81.6">narine</w>,</l>
						<l n="82" num="21.2"><w n="82.1">Loin</w> <w n="82.2">du</w> <w n="82.3">mauvais</w> <w n="82.4">lieu</w> <w n="82.5">je</w> <w n="82.6">m</w>’<w n="82.7">enfuis</w>,</l>
						<l n="83" num="21.3"><w n="83.1">Respirant</w> <w n="83.2">à</w> <w n="83.3">pleine</w> <w n="83.4">poitrine</w></l>
						<l n="84" num="21.4"><w n="84.1">L</w>’<w n="84.2">air</w> <w n="84.3">salubre</w> <w n="84.4">et</w> <w n="84.5">glacé</w> <w n="84.6">des</w> <w n="84.7">nuits</w>.</l>
					</lg>
				</div></body></text></TEI>