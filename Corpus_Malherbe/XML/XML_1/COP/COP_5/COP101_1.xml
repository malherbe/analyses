<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Sonnets intimes et Poèmes inédits</title>
				<title type="medium">Édition électronique</title>
				<author key="COP">
					<name>
						<forename>François</forename>
						<surname>COPPÉE</surname>
					</name>
					<date from="1842" to="1908">1842-1908</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1824 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">COP_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Sonnets intimes et Poèmes inédits</title>
						<author>François Coppée</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/francoiscoppesonetsintimes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Poésies complètes de François Coppée</title>
						<author>François Coppée</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre</publisher>
							<date when="1925">1925</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1925">1925</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			<change when="2017-11-30" who="RR">Les vers incomplets du refrain du poème "RONDE D’ENFANTS AUX TUILERIES" ("Les lilas passeront, etc.") ont été remplacés par la strophe du refrain complet.</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PREMIÈRE PARTIE</head><div type="poem" key="COP101">
					<head type="main">HYMNE À LA PAIX</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">La</w> <w n="1.2">paix</w> <w n="1.3">sereine</w> <w n="1.4">et</w> <w n="1.5">radieuse</w></l>
						<l n="2" num="1.2"><w n="2.1">Fait</w> <w n="2.2">resplendir</w> <w n="2.3">l</w>’<w n="2.4">or</w> <w n="2.5">des</w> <w n="2.6">moissons</w> ;</l>
						<l n="3" num="1.3"><w n="3.1">La</w> <w n="3.2">nature</w> <w n="3.3">est</w> <w n="3.4">blonde</w> <w n="3.5">et</w> <w n="3.6">joyeuse</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Le</w> <w n="4.2">ciel</w> <w n="4.3">est</w> <w n="4.4">plein</w> <w n="4.5">de</w> <w n="4.6">grands</w> <w n="4.7">frissons</w>.</l>
						<l n="5" num="1.5"><w n="5.1">Hosanna</w> <w n="5.2">dans</w> <w n="5.3">la</w> <w n="5.4">fange</w> <w n="5.5">noire</w></l>
						<l n="6" num="1.6"><w n="6.1">Et</w> <w n="6.2">dans</w> <w n="6.3">le</w> <w n="6.4">pré</w> <w n="6.5">blanc</w> <w n="6.6">de</w> <w n="6.7">troupeaux</w> ;</l>
						<l n="7" num="1.7"><w n="7.1">Salut</w>, <w n="7.2">ô</w> <w n="7.3">reine</w> ! <w n="7.4">ô</w> <w n="7.5">mère</w> ! <w n="7.6">ô</w> <w n="7.7">gloire</w></l>
						<l n="8" num="1.8"><w n="8.1">Du</w> <w n="8.2">fort</w> <w n="8.3">travail</w>, <w n="8.4">du</w> <w n="8.5">doux</w> <w n="8.6">repos</w> !</l>
						<l n="9" num="1.9"><w n="9.1">Viens</w>, <w n="9.2">nous</w> <w n="9.3">t</w>’<w n="9.4">offrons</w> <w n="9.5">l</w>’<w n="9.6">encens</w> <w n="9.7">des</w> <w n="9.8">meules</w> ;</l>
						<l n="10" num="1.10"><w n="10.1">Reste</w> <w n="10.2">avec</w> <w n="10.3">nous</w> <w n="10.4">dans</w> <w n="10.5">l</w>’<w n="10.6">avenir</w> ;</l>
						<l n="11" num="1.11"><w n="11.1">Les</w> <w n="11.2">bras</w> <w n="11.3">tremblants</w> <w n="11.4">de</w> <w n="11.5">nos</w> <w n="11.6">aïeules</w></l>
						<l n="12" num="1.12"><w n="12.1">Sont</w> <w n="12.2">tous</w> <w n="12.3">levés</w> <w n="12.4">pour</w> <w n="12.5">te</w> <w n="12.6">bénir</w>.</l>
						<l n="13" num="1.13"><w n="13.1">Le</w> <w n="13.2">front</w> <w n="13.3">tourné</w> <w n="13.4">vers</w> <w n="13.5">ton</w> <w n="13.6">aurore</w>,</l>
						<l n="14" num="1.14"><w n="14.1">Heureuse</w> <w n="14.2">paix</w>, <w n="14.3">nous</w> <w n="14.4">t</w>’<w n="14.5">implorons</w> ;</l>
						<l n="15" num="1.15"><w n="15.1">Et</w> <w n="15.2">nous</w> <w n="15.3">rythmons</w> <w n="15.4">l</w>’<w n="15.5">hymne</w> <w n="15.6">sonore</w></l>
						<l n="16" num="1.16"><w n="16.1">Sur</w> <w n="16.2">les</w> <w n="16.3">marteaux</w> <w n="16.4">des</w> <w n="16.5">forgerons</w>.</l>
						<l n="17" num="1.17"><w n="17.1">Reste</w> <w n="17.2">toujours</w>, <w n="17.3">reste</w> <w n="17.4">où</w> <w n="17.5">nous</w> <w n="17.6">sommes</w> !</l>
						<l n="18" num="1.18"><w n="18.1">Et</w> <w n="18.2">tes</w> <w n="18.3">bienfaits</w> <w n="18.4">seront</w> <w n="18.5">bénis</w></l>
						<l n="19" num="1.19"><w n="19.1">Par</w> <w n="19.2">la</w> <w n="19.3">nature</w> <w n="19.4">et</w> <w n="19.5">par</w> <w n="19.6">les</w> <w n="19.7">hommes</w>,</l>
						<l n="20" num="1.20"><w n="20.1">Par</w> <w n="20.2">les</w> <w n="20.3">cités</w> <w n="20.4">et</w> <w n="20.5">par</w> <w n="20.6">les</w> <w n="20.7">nids</w>.</l>
						<l n="21" num="1.21"><w n="21.1">Tous</w> <w n="21.2">les</w> <w n="21.3">labeurs</w> <w n="21.4">sauront</w> <w n="21.5">te</w> <w n="21.6">dire</w></l>
						<l n="22" num="1.22"><w n="22.1">Leurs</w> <w n="22.2">grands</w> <w n="22.3">efforts</w> <w n="22.4">jamais</w> <w n="22.5">troublés</w> :</l>
						<l n="23" num="1.23"><w n="23.1">Le</w> <w n="23.2">saint</w> <w n="23.3">poète</w> <w n="23.4">avec</w> <w n="23.5">la</w> <w n="23.6">lyre</w>,</l>
						<l n="24" num="1.24"><w n="24.1">Le</w> <w n="24.2">vent</w> <w n="24.3">du</w> <w n="24.4">soir</w> <w n="24.5">avec</w> <w n="24.6">les</w> <w n="24.7">blés</w>.</l>
						<l n="25" num="1.25"><w n="25.1">Ainsi</w> <w n="25.2">qu</w>’<w n="25.3">un</w> <w n="25.4">aigle</w> <w n="25.5">ivre</w> <w n="25.6">d</w>’<w n="25.7">espace</w></l>
						<l n="26" num="1.26"><w n="26.1">Monte</w> <w n="26.2">toujours</w> <w n="26.3">vers</w> <w n="26.4">le</w> <w n="26.5">soleil</w>,</l>
						<l n="27" num="1.27"><w n="27.1">Le</w> <w n="27.2">monde</w> <w n="27.3">entier</w> <w n="27.4">qui</w> <w n="27.5">te</w> <w n="27.6">rend</w> <w n="27.7">grâce</w></l>
						<l n="28" num="1.28"><w n="28.1">Accourt</w> <w n="28.2">joyeux</w> <w n="28.3">à</w> <w n="28.4">ton</w> <w n="28.5">réveil</w> ;</l>
						<l n="29" num="1.29"><w n="29.1">Car</w> <w n="29.2">le</w> <w n="29.3">laurier</w> <w n="29.4">croît</w> <w n="29.5">sur</w> <w n="29.6">les</w> <w n="29.7">tombes</w>,</l>
						<l n="30" num="1.30"><w n="30.1">Et</w> <w n="30.2">ces</w> <w n="30.3">temps</w>-<w n="30.4">là</w> <w n="30.5">sont</w> <w n="30.6">les</w> <w n="30.7">meilleurs</w></l>
						<l n="31" num="1.31"><w n="31.1">Où</w> <w n="31.2">dans</w> <w n="31.3">l</w>’<w n="31.4">azur</w> <w n="31.5">plein</w> <w n="31.6">de</w> <w n="31.7">colombes</w></l>
						<l n="32" num="1.32"><w n="32.1">Monte</w> <w n="32.2">le</w> <w n="32.3">chant</w> <w n="32.4">des</w> <w n="32.5">travailleurs</w>.</l>
					</lg>
				</div></body></text></TEI>