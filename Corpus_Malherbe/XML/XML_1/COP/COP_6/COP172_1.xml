<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">DES VERS FRANÇAIS</title>
				<title type="medium">Édition électronique</title>
				<author key="COP">
					<name>
						<forename>François</forename>
						<surname>COPPÉE</surname>
					</name>
					<date from="1842" to="1908">1842-1908</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2263 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">COP_6</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">DES VERS FRANÇAIS</title>
						<author>François Coppée</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/francoiscoppeedesversfrancais.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Poésies complètes de François Coppée</title>
						<author>François Coppée</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre</publisher>
							<date when="1925">1925</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1906">1906</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="COP172">
				<head type="main">A Joris-Karl Huysmans</head>
				<head type="sub_1">Pour sa biographie de Don Bosco</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Lisez</w>. <w n="1.2">Ces</w> <w n="1.3">faits</w> <w n="1.4">récents</w> <w n="1.5">n</w>’<w n="1.6">ont</w> <w n="1.7">rien</w> <w n="1.8">d</w>’<w n="1.9">une</w> <w n="1.10">légende</w>.</l>
					<l n="2" num="1.2"><w n="2.1">Des</w> <w n="2.2">enfants</w> <w n="2.3">du</w> <w n="2.4">ruisseau</w> — <w n="2.5">pour</w> <w n="2.6">demain</w> <w n="2.7">des</w> <w n="2.8">pervers</w></l>
					<l n="3" num="1.3"><w n="3.1">Virent</w> <w n="3.2">un</w> <w n="3.3">saint</w> <w n="3.4">venir</w> <w n="3.5">vers</w> <w n="3.6">eux</w>, <w n="3.7">les</w> <w n="3.8">bras</w> <w n="3.9">ouverts</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Et</w> <w n="4.2">furent</w> <w n="4.3">bons</w> <w n="4.4">et</w> <w n="4.5">purs</w>, <w n="4.6">comme</w> <w n="4.7">Dieu</w> <w n="4.8">le</w> <w n="4.9">commande</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">L</w>’<w n="5.2">homme</w> <w n="5.3">est</w> <w n="5.4">mort</w> ; <w n="5.5">mais</w> <w n="5.6">toujours</w> <w n="5.7">plus</w> <w n="5.8">féconde</w> <w n="5.9">et</w> <w n="5.10">plus</w> <w n="5.11">grande</w></l>
					<l n="6" num="2.2"><w n="6.1">Et</w> <w n="6.2">vivant</w> <w n="6.3">des</w> <w n="6.4">seuls</w> <w n="6.5">dons</w> <w n="6.6">par</w> <w n="6.7">les</w> <w n="6.8">chrétiens</w> <w n="6.9">offerts</w>,</l>
					<l n="7" num="2.3"><w n="7.1">Son</w> <w n="7.2">œuvre</w> <w n="7.3">a</w> <w n="7.4">rayonné</w> <w n="7.5">sur</w> <w n="7.6">le</w> <w n="7.7">vaste</w> <w n="7.8">univers</w>.</l>
					<l n="8" num="2.4"><w n="8.1">Lisez</w>. <w n="8.2">Est</w>-<w n="8.3">ce</w> <w n="8.4">un</w> <w n="8.5">miracle</w> <w n="8.6">ou</w> <w n="8.7">non</w> ? <w n="8.8">Je</w> <w n="8.9">le</w> <w n="8.10">demande</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Jadis</w>, <w n="9.2">du</w> <w n="9.3">tablier</w> <w n="9.4">de</w> <w n="9.5">sainte</w> <w n="9.6">Élisabeth</w>,</l>
					<l n="10" num="3.2"><w n="10.1">C</w>’<w n="10.2">était</w> <w n="10.3">une</w> <w n="10.4">moisson</w> <w n="10.5">de</w> <w n="10.6">roses</w> <w n="10.7">qui</w> <w n="10.8">tombait</w>.</l>
					<l n="11" num="3.3"><w n="11.1">Aujourd</w>’<w n="11.2">hui</w> <w n="11.3">Don</w> <w n="11.4">Bosco</w>, <w n="11.5">qui</w>, <w n="11.6">d</w>’<w n="11.7">abord</w>, <w n="11.8">dans</w> <w n="11.9">les</w> <w n="11.10">fanges</w>,</l>
				</lg>
				<lg n="4">
					<l n="12" num="4.1"><w n="12.1">Ramassa</w> <w n="12.2">les</w> <w n="12.3">petits</w> <w n="12.4">vagabonds</w> <w n="12.5">de</w> <w n="12.6">Turin</w>,</l>
					<l n="13" num="4.2"><w n="13.1">Voit</w> <w n="13.2">s</w>’<w n="13.3">envoler</w>, <w n="13.4">devant</w> <w n="13.5">le</w> <w n="13.6">Juge</w> <w n="13.7">Souverain</w>,</l>
					<l n="14" num="4.3"><w n="14.1">De</w> <w n="14.2">sa</w> <w n="14.3">vieille</w> <w n="14.4">soutane</w>, <w n="14.5">une</w> <w n="14.6">légion</w> <w n="14.7">d</w>’<w n="14.8">anges</w>.</l>
				</lg>
			</div></body></text></TEI>