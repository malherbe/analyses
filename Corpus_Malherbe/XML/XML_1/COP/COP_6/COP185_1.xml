<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">DES VERS FRANÇAIS</title>
				<title type="medium">Édition électronique</title>
				<author key="COP">
					<name>
						<forename>François</forename>
						<surname>COPPÉE</surname>
					</name>
					<date from="1842" to="1908">1842-1908</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2263 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">COP_6</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">DES VERS FRANÇAIS</title>
						<author>François Coppée</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/francoiscoppeedesversfrancais.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Poésies complètes de François Coppée</title>
						<author>François Coppée</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre</publisher>
							<date when="1925">1925</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1906">1906</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="COP185">
				<head type="main">Chauvinisme</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">J</w>’<w n="1.2">en</w> <w n="1.3">conviens</w>. <w n="1.4">Je</w> <w n="1.5">suis</w> <w n="1.6">en</w> <w n="1.7">retard</w></l>
					<l n="2" num="1.2"><w n="2.1">Avec</w> <w n="2.2">le</w> <w n="2.3">rêve</w> <w n="2.4">humanitaire</w>.</l>
					<l n="3" num="1.3"><w n="3.1">J</w>’<w n="3.2">aime</w>, <w n="3.3">ô</w> <w n="3.4">France</w>, <w n="3.5">ta</w> <w n="3.6">vieille</w> <w n="3.7">terre</w></l>
					<l n="4" num="1.4"><w n="4.1">En</w> <w n="4.2">chauvin</w>, <w n="4.3">en</w> <w n="4.4">patriotard</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Des</w> <w n="5.2">orateurs</w> <w n="5.3">pleins</w> <w n="5.4">de</w> <w n="5.5">faconde</w>,</l>
					<l n="6" num="2.2"><w n="6.1">Apôtres</w> <w n="6.2">de</w> <w n="6.3">la</w> <w n="6.4">crosse</w> <w n="6.5">en</w> <w n="6.6">l</w>’<w n="6.7">air</w>,</l>
					<l n="7" num="2.3"><w n="7.1">Me</w> <w n="7.2">hurlent</w> <w n="7.3">le</w> <w n="7.4">mot</w> <w n="7.5">de</w> <w n="7.6">Schiller</w> :</l>
					<l n="8" num="2.4">« <w n="8.1">Nous</w> <w n="8.2">sommes</w> <w n="8.3">citoyens</w> <w n="8.4">du</w> <w n="8.5">monde</w> !</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Hélas</w> ! <w n="9.2">je</w> <w n="9.3">l</w>’<w n="9.4">entendis</w> <w n="9.5">déjà</w></l>
					<l n="10" num="3.2"><w n="10.1">S</w>’<w n="10.2">élever</w>, <w n="10.3">cette</w> <w n="10.4">clameur</w> <w n="10.5">vaine</w>.</l>
					<l n="11" num="3.3"><w n="11.1">Mais</w> <w n="11.2">alors</w>, <w n="11.3">sous</w> <w n="11.4">un</w> <w n="11.5">flot</w> <w n="11.6">de</w> <w n="11.7">haine</w>,</l>
					<l n="12" num="3.4"><w n="12.1">L</w>’<w n="12.2">invasion</w> <w n="12.3">nous</w> <w n="12.4">submergea</w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Voilà</w> <w n="13.2">pourtant</w> <w n="13.3">qu</w>’<w n="13.4">on</w> <w n="13.5">recommence</w></l>
					<l n="14" num="4.2"><w n="14.1">A</w> <w n="14.2">faire</w> <w n="14.3">aux</w> <w n="14.4">vainqueurs</w> <w n="14.5">les</w> <w n="14.6">yeux</w> <w n="14.7">doux</w>.</l>
					<l n="15" num="4.3">« <w n="15.1">Peuples</w> <w n="15.2">frères</w>, <w n="15.3">embrassons</w>-<w n="15.4">nous</w> ! »</l>
					<l n="16" num="4.4"><w n="16.1">Quelle</w> <w n="16.2">pitoyable</w> <w n="16.3">démence</w> !</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">Ceux</w> <w n="17.2">de</w> <w n="17.3">mon</w> <w n="17.4">âge</w> <w n="17.5">ont</w> <w n="17.6">trop</w> <w n="17.7">vécu</w>.</l>
					<l n="18" num="5.2"><w n="18.1">Ce</w> <w n="18.2">fier</w> <w n="18.3">pays</w> <w n="18.4">s</w>’<w n="18.5">abaisse</w> <w n="18.6">et</w> <w n="18.7">rampe</w> ;</l>
					<l n="19" num="5.3"><w n="19.1">Et</w>, <w n="19.2">de</w> <w n="19.3">colère</w>, <w n="19.4">sur</w> <w n="19.5">ta</w> <w n="19.6">hampe</w>,</l>
					<l n="20" num="5.4"><w n="20.1">Tu</w> <w n="20.2">frémis</w>, <w n="20.3">ô</w> <w n="20.4">drapeau</w> <w n="20.5">vaincu</w> !</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">Démentis</w> <w n="21.2">par</w> <w n="21.3">tant</w> <w n="21.4">de</w> <w n="21.5">tueries</w>,</l>
					<l n="22" num="6.2"><w n="22.1">Des</w> <w n="22.2">rhéteurs</w> <w n="22.3">par</w> <w n="22.4">la</w> <w n="22.5">plèbe</w> <w n="22.6">élus</w></l>
					<l n="23" num="6.3"><w n="23.1">Nous</w> <w n="23.2">déclarent</w> <w n="23.3">qu</w>’<w n="23.4">il</w> <w n="23.5">ne</w> <w n="23.6">faut</w> <w n="23.7">plus</w></l>
					<l n="24" num="6.4"><w n="24.1">De</w> <w n="24.2">frontières</w> <w n="24.3">ni</w> <w n="24.4">de</w> <w n="24.5">patries</w>.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">Chimère</w> ! <w n="25.2">Songe</w> <w n="25.3">creux</w> ! <w n="25.4">Roman</w> !…</l>
					<l n="26" num="7.2">« <w n="26.1">Qui</w> <w n="26.2">donc</w> <w n="26.3">a</w> <w n="26.4">la</w> <w n="26.5">meilleure</w> <w n="26.6">place</w></l>
					<l n="27" num="7.3"><w n="27.1">Dans</w> <w n="27.2">ton</w> <w n="27.3">cœur</w>, — <w n="27.4">dis</w>, <w n="27.5">l</w>’<w n="27.6">enfant</w> <w n="27.7">qui</w> <w n="27.8">passe</w>, —</l>
					<l n="28" num="7.4"><w n="28.1">Les</w> <w n="28.2">voisines</w> <w n="28.3">ou</w> <w n="28.4">ta</w> <w n="28.5">maman</w> ? »</l>
				</lg>
			</div></body></text></TEI>