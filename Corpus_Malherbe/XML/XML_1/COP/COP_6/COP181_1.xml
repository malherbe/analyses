<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">DES VERS FRANÇAIS</title>
				<title type="medium">Édition électronique</title>
				<author key="COP">
					<name>
						<forename>François</forename>
						<surname>COPPÉE</surname>
					</name>
					<date from="1842" to="1908">1842-1908</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2263 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">COP_6</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">DES VERS FRANÇAIS</title>
						<author>François Coppée</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/francoiscoppeedesversfrancais.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Poésies complètes de François Coppée</title>
						<author>François Coppée</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre</publisher>
							<date when="1925">1925</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1906">1906</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="COP181">
				<head type="main">Les Disciples d’Emmaüs</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Très</w> <w n="1.2">tristement</w>, <w n="1.3">les</w> <w n="1.4">deux</w> <w n="1.5">disciples</w>, <w n="1.6">dans</w> <w n="1.7">la</w> <w n="1.8">plaine</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Allaient</w> <w n="2.2">vers</w> <w n="2.3">Emmaüs</w>, <w n="2.4">et</w> <w n="2.5">leur</w> <w n="2.6">âme</w> <w n="2.7">était</w> <w n="2.8">pleine</w></l>
					<l n="3" num="1.3"><w n="3.1">D</w>’<w n="3.2">horreur</w>. <w n="3.3">Ils</w> <w n="3.4">avaient</w> <w n="3.5">vu</w> <w n="3.6">Jésus</w> <w n="3.7">mourir</w> <w n="3.8">en</w> <w n="3.9">croix</w>.</l>
					<l n="4" num="1.4"><w n="4.1">Tout</w> <w n="4.2">en</w> <w n="4.3">marchant</w>, <w n="4.4">ils</w> <w n="4.5">se</w> <w n="4.6">parlaient</w> <w n="4.7">à</w> <w n="4.8">demi</w>-<w n="4.9">voix</w></l>
					<l n="5" num="1.5"><w n="5.1">Du</w> <w n="5.2">crime</w> <w n="5.3">monstrueux</w> <w n="5.4">commis</w> <w n="5.5">sur</w> <w n="5.6">le</w> <w n="5.7">Calvaire</w>.</l>
					<l n="6" num="1.6"><w n="6.1">La</w> <w n="6.2">nuit</w> <w n="6.3">envahissait</w> <w n="6.4">le</w> <w n="6.5">ciel</w> <w n="6.6">calme</w> <w n="6.7">et</w> <w n="6.8">sévère</w>.</l>
					<l n="7" num="1.7"><w n="7.1">Pas</w> <w n="7.2">d</w>’<w n="7.3">étoiles</w> <w n="7.4">encor</w>, <w n="7.5">mais</w> <w n="7.6">le</w> <w n="7.7">dernier</w> <w n="7.8">tison</w></l>
					<l n="8" num="1.8"><w n="8.1">Du</w> <w n="8.2">couchant</w> <w n="8.3">s</w>’<w n="8.4">éteignait</w> <w n="8.5">au</w> <w n="8.6">sanglant</w> <w n="8.7">horizon</w>.</l>
					<l n="9" num="1.9"><w n="9.1">Parfois</w> <w n="9.2">le</w> <w n="9.3">vent</w> <w n="9.4">du</w> <w n="9.5">soir</w>, <w n="9.6">dans</w> <w n="9.7">le</w> <w n="9.8">feuillage</w> <w n="9.9">pâle</w></l>
					<l n="10" num="1.10"><w n="10.1">Des</w> <w n="10.2">oliviers</w>, <w n="10.3">soufflait</w> <w n="10.4">avec</w> <w n="10.5">un</w> <w n="10.6">faible</w> <w n="10.7">râle</w>.</l>
					<l n="11" num="1.11"><w n="11.1">L</w>’<w n="11.2">ombre</w>, <w n="11.3">de</w> <w n="11.4">toutes</w> <w n="11.5">parts</w>, <w n="11.6">sur</w> <w n="11.7">les</w> <w n="11.8">champs</w> <w n="11.9">accourait</w>.</l>
				</lg>
				<lg n="2">
					<l n="12" num="2.1">« <w n="12.1">Il</w> <w n="12.2">avait</w> <w n="12.3">pourtant</w> <w n="12.4">dit</w> <w n="12.5">qu</w>’<w n="12.6">il</w> <w n="12.7">ressusciterait</w>,</l>
					<l n="13" num="2.2"><w n="13.1">Murmura</w> <w n="13.2">l</w>’<w n="13.3">un</w> <w n="13.4">des</w> <w n="13.5">deux</w> <w n="13.6">hommes</w>, <w n="13.7">hochant</w> <w n="13.8">la</w> <w n="13.9">tête</w>,</l>
					<l n="14" num="2.3"><w n="14.1">Et</w> <w n="14.2">le</w> <w n="14.3">Nazaréen</w> <w n="14.4">était</w> <w n="14.5">un</w> <w n="14.6">grand</w> <w n="14.7">prophète</w>.</l>
					<l n="15" num="2.4"><w n="15.1">Mais</w> <w n="15.2">nous</w> <w n="15.3">avons</w> <w n="15.4">bien</w> <w n="15.5">vu</w> <w n="15.6">mettre</w> <w n="15.7">au</w> <w n="15.8">tombeau</w> <w n="15.9">son</w> <w n="15.10">corps</w>,</l>
					<l n="16" num="2.5"><w n="16.1">Cléophas</w>, <w n="16.2">et</w> <w n="16.3">trois</w> <w n="16.4">jours</w> <w n="16.5">sont</w> <w n="16.6">passés</w> <w n="16.7">depuis</w> <w n="16.8">lors</w>. »</l>
				</lg>
				<lg n="3">
					<l n="17" num="3.1"><w n="17.1">Et</w> <w n="17.2">l</w>’<w n="17.3">autre</w> <w n="17.4">dit</w>, <w n="17.5">tordant</w> <w n="17.6">ses</w> <w n="17.7">deux</w> <w n="17.8">mains</w> <w n="17.9">désolées</w> :</l>
				</lg>
				<lg n="4">
					<l n="18" num="4.1">« <w n="18.1">Cependant</w>, <w n="18.2">cette</w> <w n="18.3">nuit</w>, <w n="18.4">les</w> <w n="18.5">femmes</w> <w n="18.6">sont</w> <w n="18.7">allées</w></l>
					<l n="19" num="4.2"><w n="19.1">Au</w> <w n="19.2">sépulcre</w>. <w n="19.3">Il</w> <w n="19.4">était</w> <w n="19.5">vide</w>, <w n="19.6">et</w>, <w n="19.7">placé</w> <w n="19.8">devant</w>,</l>
					<l n="20" num="4.3"><w n="20.1">Un</w> <w n="20.2">ange</w> <w n="20.3">leur</w> <w n="20.4">a</w> <w n="20.5">dit</w> <w n="20.6">que</w> <w n="20.7">le</w> <w n="20.8">Christ</w> <w n="20.9">est</w> <w n="20.10">vivant</w>. »</l>
					<l part="I" n="21" num="4.4"><w n="21.1">Mais</w> <w n="21.2">le</w> <w n="21.3">premier</w> <w n="21.4">reprit</w> : </l>
					<l part="F" n="21" num="4.4">« <w n="21.5">C</w>’<w n="21.6">est</w> <w n="21.7">vrai</w>. <w n="21.8">Plusieurs</w> <w n="21.9">des</w> <w n="21.10">nôtres</w>,</l>
					<l n="22" num="4.5"><w n="22.1">Ceux</w> <w n="22.2">qu</w>’<w n="22.3">il</w> <w n="22.4">aimait</w> <w n="22.5">et</w> <w n="22.6">qu</w>’<w n="22.7">il</w> <w n="22.8">appelait</w> <w n="22.9">ses</w> <w n="22.10">apôtres</w>,</l>
					<l n="23" num="4.6"><w n="23.1">Ont</w> <w n="23.2">vu</w> <w n="23.3">le</w> <w n="23.4">tombeau</w> <w n="23.5">vide</w>, <w n="23.6">après</w> <w n="23.7">le</w> <w n="23.8">jour</w> <w n="23.9">levé</w> ;</l>
					<l n="24" num="4.7"><w n="24.1">Mais</w> <w n="24.2">ils</w> <w n="24.3">cherchaient</w> <w n="24.4">Jésus</w> <w n="24.5">et</w> <w n="24.6">ne</w> <w n="24.7">l</w>’<w n="24.8">ont</w> <w n="24.9">point</w> <w n="24.10">trouvé</w>. »</l>
				</lg>
				<lg n="5">
					<l n="25" num="5.1"><w n="25.1">Et</w> <w n="25.2">les</w> <w n="25.3">deux</w> <w n="25.4">pèlerins</w> <w n="25.5">mainte</w> <w n="25.6">fois</w> <w n="25.7">se</w> <w n="25.8">redirent</w></l>
					<l n="26" num="5.2"><w n="26.1">Leur</w> <w n="26.2">angoisse</w> <w n="26.3">et</w> <w n="26.4">leur</w> <w n="26.5">deuil</w>. <w n="26.6">Tout</w> <w n="26.7">à</w> <w n="26.8">coup</w>, <w n="26.9">ils</w> <w n="26.10">sentirent</w></l>
					<l n="27" num="5.3"><w n="27.1">Qu</w>’<w n="27.2">un</w> <w n="27.3">autre</w> <w n="27.4">voyageur</w> <w n="27.5">marchait</w> <w n="27.6">à</w> <w n="27.7">côté</w> <w n="27.8">d</w>’<w n="27.9">eux</w>.</l>
				</lg>
				<lg n="6">
					<l n="28" num="6.1">« <w n="28.1">Tristes</w> <w n="28.2">passants</w>, <w n="28.3">de</w> <w n="28.4">quoi</w> <w n="28.5">parliez</w>-<w n="28.6">vous</w> <w n="28.7">donc</w> <w n="28.8">tous</w> <w n="28.9">deux</w> ?»</l>
					<l part="I" n="29" num="6.2"><w n="29.1">Demanda</w>-<w n="29.2">t</w>-<w n="29.3">il</w>. </l>
					<l part="F" n="29" num="6.2"><w n="29.4">C</w>’<w n="29.5">était</w> <w n="29.6">Jésus</w>, <w n="29.7">c</w>’<w n="29.8">était</w> <w n="29.9">leur</w> <w n="29.10">Maître</w> ;</l>
					<l n="30" num="6.3"><w n="30.1">Mais</w> <w n="30.2">il</w> <w n="30.3">ne</w> <w n="30.4">voulait</w> <w n="30.5">pas</w> <w n="30.6">qu</w>’<w n="30.7">ils</w> <w n="30.8">pussent</w> <w n="30.9">reconnaître</w></l>
					<l n="31" num="6.4"><w n="31.1">Encor</w> <w n="31.2">le</w> <w n="31.3">Dieu</w> <w n="31.4">surgi</w> <w n="31.5">pour</w> <w n="31.6">les</w> <w n="31.7">interroger</w>.</l>
				</lg>
				<lg n="7">
					<l n="32" num="7.1">« <w n="32.1">Êtes</w>-<w n="32.2">vous</w> <w n="32.3">au</w> <w n="32.4">pays</w> <w n="32.5">tellement</w> <w n="32.6">étranger</w>,</l>
					<l n="33" num="7.2"><w n="33.1">Dit</w> <w n="33.2">Cléophas</w>, <w n="33.3">que</w> <w n="33.4">vous</w> <w n="33.5">ne</w> <w n="33.6">sachiez</w> <w n="33.7">pas</w> <w n="33.8">ces</w> <w n="33.9">choses</w> ? »</l>
				</lg>
				<lg n="8">
					<l n="34" num="8.1"><w n="34.1">Puis</w>, <w n="34.2">une</w> <w n="34.3">fois</w> <w n="34.4">de</w> <w n="34.5">plus</w>, <w n="34.6">il</w> <w n="34.7">répéta</w> <w n="34.8">les</w> <w n="34.9">causes</w></l>
					<l n="35" num="8.2"><w n="35.1">De</w> <w n="35.2">leur</w> <w n="35.3">douleur</w> : <w n="35.4">le</w> <w n="35.5">Juste</w>, <w n="35.6">après</w> <w n="35.7">d</w>’<w n="35.8">abjects</w> <w n="35.9">affronts</w>,</l>
					<l n="36" num="8.3"><w n="36.1">Cloué</w> <w n="36.2">sur</w> <w n="36.3">une</w> <w n="36.4">croix</w> <w n="36.5">entre</w> <w n="36.6">les</w> <w n="36.7">deux</w> <w n="36.8">larrons</w> ;</l>
					<l n="37" num="8.4"><w n="37.1">Ses</w> <w n="37.2">vertus</w>, <w n="37.3">ses</w> <w n="37.4">discours</w>, <w n="37.5">ses</w> <w n="37.6">gestes</w>, <w n="37.7">ses</w> <w n="37.8">miracles</w> ;</l>
					<l n="38" num="8.5"><w n="38.1">Et</w> <w n="38.2">qu</w>’<w n="38.3">il</w> <w n="38.4">semblait</w> <w n="38.5">le</w> <w n="38.6">Christ</w> <w n="38.7">promis</w> <w n="38.8">par</w> <w n="38.9">les</w> <w n="38.10">oracles</w> ;</l>
					<l n="39" num="8.6"><w n="39.1">Qu</w>’<w n="39.2">il</w> <w n="39.3">devait</w>, <w n="39.4">ce</w> <w n="39.5">jour</w> <w n="39.6">même</w>, — <w n="39.7">il</w> <w n="39.8">l</w>’<w n="39.9">avait</w> <w n="39.10">annoncé</w>, —</l>
					<l n="40" num="8.7"><w n="40.1">Reparaître</w> <w n="40.2">et</w> <w n="40.3">qu</w>’<w n="40.4">hélas</w> ! <w n="40.5">le</w> <w n="40.6">jour</w> <w n="40.7">était</w> <w n="40.8">passé</w>.</l>
				</lg>
				<lg n="9">
					<l part="I" n="41" num="9.1"><w n="41.1">Et</w> <w n="41.2">l</w>’<w n="41.3">inconnu</w> <w n="41.4">leur</w> <w n="41.5">dit</w> : </l>
					<l part="F" n="41" num="9.1">« <w n="41.6">O</w> <w n="41.7">cœurs</w> <w n="41.8">trop</w> <w n="41.9">lents</w> <w n="41.10">à</w> <w n="41.11">croire</w>,</l>
					<l n="42" num="9.2"><w n="42.1">Le</w> <w n="42.2">Christ</w> <w n="42.3">devait</w> <w n="42.4">souffrir</w> <w n="42.5">pour</w> <w n="42.6">entrer</w> <w n="42.7">dans</w> <w n="42.8">la</w> <w n="42.9">gloire</w>. »</l>
					<l n="43" num="9.3"><w n="43.1">Puis</w> <w n="43.2">il</w> <w n="43.3">leur</w> <w n="43.4">expliqua</w> <w n="43.5">que</w> <w n="43.6">Jésus</w>, <w n="43.7">ses</w> <w n="43.8">desseins</w></l>
					<l n="44" num="9.4"><w n="44.1">Et</w> <w n="44.2">ses</w> <w n="44.3">actes</w> <w n="44.4">étaient</w> <w n="44.5">prédits</w>, <w n="44.6">aux</w> <w n="44.7">Livres</w> <w n="44.8">Saints</w>,</l>
					<l n="45" num="9.5"><w n="45.1">Et</w> <w n="45.2">que</w>, <w n="45.3">depuis</w> <w n="45.4">la</w> <w n="45.5">plus</w> <w n="45.6">antique</w> <w n="45.7">prophétie</w>,</l>
					<l n="46" num="9.6"><w n="46.1">Tout</w> <w n="46.2">prouvait</w> <w n="46.3">que</w> <w n="46.4">ce</w> <w n="46.5">Juste</w> <w n="46.6">était</w> <w n="46.7">bien</w> <w n="46.8">le</w> <w n="46.9">Messie</w>.</l>
				</lg>
				<lg n="10">
					<l n="47" num="10.1"><w n="47.1">Dans</w> <w n="47.2">le</w> <w n="47.3">bourg</w>, <w n="47.4">au</w> <w n="47.5">dernier</w> <w n="47.6">crépuscule</w> <w n="47.7">du</w> <w n="47.8">soir</w>,</l>
					<l n="48" num="10.2"><w n="48.1">Ils</w> <w n="48.2">entrèrent</w> <w n="48.3">tous</w> <w n="48.4">trois</w> <w n="48.5">et</w>, <w n="48.6">sur</w> <w n="48.7">le</w> <w n="48.8">chemin</w> <w n="48.9">noir</w>,</l>
					<l n="49" num="10.3"><w n="49.1">Jésus</w> <w n="49.2">semblait</w> <w n="49.3">vouloir</w> <w n="49.4">poursuivre</w> <w n="49.5">son</w> <w n="49.6">voyage</w>.</l>
					<l n="50" num="10.4"><w n="50.1">Mais</w> <w n="50.2">les</w> <w n="50.3">deux</w> <w n="50.4">pèlerins</w>, <w n="50.5">émus</w> <w n="50.6">par</w> <w n="50.7">son</w> <w n="50.8">langage</w>,</l>
					<l n="51" num="10.5"><w n="51.1">Sentaient</w> <w n="51.2">leur</w> <w n="51.3">cœur</w> <w n="51.4">brûler</w> <w n="51.5">d</w>’<w n="51.6">un</w> <w n="51.7">feu</w> <w n="51.8">puissant</w> <w n="51.9">et</w> <w n="51.10">doux</w>.</l>
				</lg>
				<lg n="11">
					<l n="52" num="11.1">« <w n="52.1">Demeurez</w>, <w n="52.2">dirent</w>-<w n="52.3">ils</w>, <w n="52.4">et</w> <w n="52.5">soupez</w> <w n="52.6">avec</w> <w n="52.7">nous</w>. »</l>
				</lg>
				<lg n="12">
					<l n="53" num="12.1"><w n="53.1">Mais</w> <w n="53.2">quand</w> <w n="53.3">ils</w> <w n="53.4">l</w>’<w n="53.5">eurent</w> <w n="53.6">vu</w>, <w n="53.7">bien</w> <w n="53.8">qu</w>’<w n="53.9">il</w> <w n="53.10">ne</w> <w n="53.11">fût</w> <w n="53.12">que</w> <w n="53.13">l</w>’<w n="53.14">hôte</w>,</l>
					<l n="54" num="12.2"><w n="54.1">Choisir</w>, <w n="54.2">pour</w> <w n="54.3">le</w> <w n="54.4">repas</w>, <w n="54.5">la</w> <w n="54.6">place</w> <w n="54.7">la</w> <w n="54.8">plus</w> <w n="54.9">haute</w>,</l>
					<l n="55" num="12.3"><w n="55.1">Et</w>, <w n="55.2">comme</w> <w n="55.3">il</w> <w n="55.4">le</w> <w n="55.5">faisait</w> <w n="55.6">souvent</w>, — <w n="55.7">quel</w> <w n="55.8">souvenir</w> ! —</l>
					<l n="56" num="12.4"><w n="56.1">Prendre</w> <w n="56.2">en</w> <w n="56.3">ses</w> <w n="56.4">doigts</w> <w n="56.5">le</w> <w n="56.6">pain</w>, <w n="56.7">le</w> <w n="56.8">rompre</w> <w n="56.9">et</w> <w n="56.10">le</w> <w n="56.11">bénir</w>,</l>
					<l n="57" num="12.5"><w n="57.1">Leur</w> <w n="57.2">esprit</w> <w n="57.3">fut</w> <w n="57.4">soudain</w> <w n="57.5">inondé</w> <w n="57.6">de</w> <w n="57.7">lumière</w>.’</l>
					<l n="58" num="12.6"><w n="58.1">Tendant</w> <w n="58.2">vers</w> <w n="58.3">le</w> <w n="58.4">Seigneur</w> <w n="58.5">leurs</w> <w n="58.6">deux</w> <w n="58.7">mains</w> <w n="58.8">en</w> <w n="58.9">prière</w>,</l>
					<l n="59" num="12.7"><w n="59.1">Sûrs</w> <w n="59.2">de</w> <w n="59.3">le</w> <w n="59.4">reconnaître</w>, <w n="59.5">heureux</w> <w n="59.6">éperdument</w>,</l>
					<l part="I" n="60" num="12.8"><w n="60.1">Ils</w> <w n="60.2">l</w>’<w n="60.3">adoraient</w>… </l>
				</lg>
				<lg n="13">
					<l part="F" n="60"><w n="60.4">Jésus</w> <w n="60.5">disparut</w> <w n="60.6">brusquement</w>.</l>
				</lg>
				<lg n="14">
					<l n="61" num="14.1"><w n="61.1">Il</w> <w n="61.2">étaient</w> <w n="61.3">pour</w> <w n="61.4">toujours</w> <w n="61.5">délivrés</w> <w n="61.6">de</w> <w n="61.7">leur</w> <w n="61.8">doute</w> ;</l>
					<l n="62" num="14.2"><w n="62.1">Et</w>, <w n="62.2">vers</w> <w n="62.3">Jérusalem</w> <w n="62.4">ayant</w> <w n="62.5">refait</w> <w n="62.6">la</w> <w n="62.7">route</w></l>
					<l n="63" num="14.3"><w n="63.1">Dans</w> <w n="63.2">la</w> <w n="63.3">nuit</w>, <w n="63.4">ils</w> <w n="63.5">allaient</w> <w n="63.6">à</w> <w n="63.7">travers</w> <w n="63.8">la</w> <w n="63.9">cité</w>,</l>
					<l part="I" n="64" num="14.4"><w n="64.1">Disant</w> <w n="64.2">à</w> <w n="64.3">leurs</w> <w n="64.4">amis</w> : </l>
					<l part="F" n="64" num="14.4">« <w n="64.5">Il</w> <w n="64.6">est</w> <w n="64.7">ressuscité</w> ! »</l>
				</lg>
				<ab type="star">※※※</ab>
				<lg n="15">
					<l n="65" num="15.1"><w n="65.1">Vingt</w> <w n="65.2">siècles</w> <w n="65.3">de</w> <w n="65.4">bonté</w> <w n="65.5">sont</w> <w n="65.6">nés</w> <w n="65.7">de</w> <w n="65.8">ces</w> <w n="65.9">mystères</w>.</l>
					<l n="66" num="15.2"><w n="66.1">Je</w> <w n="66.2">crois</w> <w n="66.3">en</w> <w n="66.4">toi</w>, <w n="66.5">Jésus</w> !… <w n="66.6">Hélas</w> ! <w n="66.7">d</w>’<w n="66.8">affreux</w> <w n="66.9">sectaires</w></l>
					<l n="67" num="15.3"><w n="67.1">Veulent</w> <w n="67.2">faire</w> <w n="67.3">oublier</w> <w n="67.4">ton</w> <w n="67.5">nom</w> <w n="67.6">à</w> <w n="67.7">nos</w> <w n="67.8">enfants</w></l>
					<l n="68" num="15.4"><w n="68.1">Et</w>, <w n="68.2">pour</w> <w n="68.3">de</w> <w n="68.4">bien</w> <w n="68.5">longs</w> <w n="68.6">jours</w>, <w n="68.7">ils</w> <w n="68.8">semblent</w> <w n="68.9">triomphants</w>.</l>
					<l n="69" num="15.5"><w n="69.1">Qu</w>’<w n="69.2">importe</w> ? <w n="69.3">Pleins</w> <w n="69.4">de</w> <w n="69.5">haine</w> <w n="69.6">et</w> <w n="69.7">d</w>’<w n="69.8">orgueil</w> <w n="69.9">imbécile</w>,</l>
					<l n="70" num="15.6"><w n="70.1">Quand</w> <w n="70.2">ils</w> <w n="70.3">auraient</w> <w n="70.4">brûlé</w> <w n="70.5">le</w> <w n="70.6">dernier</w> <w n="70.7">évangile</w>,</l>
					<l n="71" num="15.7"><w n="71.1">Quand</w> <w n="71.2">ils</w> <w n="71.3">auraient</w> <w n="71.4">brisé</w> <w n="71.5">le</w> <w n="71.6">dernier</w> <w n="71.7">crucifix</w>,</l>
					<l n="72" num="15.8"><w n="72.1">Et</w> <w n="72.2">quand</w> <w n="72.3">aux</w> <w n="72.4">fils</w> <w n="72.5">de</w> <w n="72.6">nos</w> <w n="72.7">arrière</w>-<w n="72.8">petits</w>-<w n="72.9">fils</w></l>
					<l n="73" num="15.9"><w n="73.1">Ils</w> <w n="73.2">auraient</w> <w n="73.3">travaillé</w> <w n="73.4">l</w>’<w n="73.5">âme</w> <w n="73.6">de</w> <w n="73.7">telle</w> <w n="73.8">sorte</w></l>
					<l n="74" num="15.10"><w n="74.1">Qu</w>’<w n="74.2">on</w> <w n="74.3">croirait</w> <w n="74.4">que</w> <w n="74.5">la</w> <w n="74.6">foi</w> <w n="74.7">dans</w> <w n="74.8">le</w> <w n="74.9">Christ</w> <w n="74.10">est</w> <w n="74.11">bien</w> <w n="74.12">morte</w></l>
					<l n="75" num="15.11"><w n="75.1">Et</w> <w n="75.2">que</w>, <w n="75.3">dans</w> <w n="75.4">le</w> <w n="75.5">sépulcre</w>, <w n="75.6">au</w> <w n="75.7">fond</w> <w n="75.8">d</w>’<w n="75.9">un</w> <w n="75.10">souterrain</w>,</l>
					<l n="76" num="15.12"><w n="76.1">Elle</w> <w n="76.2">est</w> <w n="76.3">scellée</w> <w n="76.4">avec</w> <w n="76.5">le</w> <w n="76.6">sceau</w> <w n="76.7">du</w> <w n="76.8">Sanhédrin</w>,</l>
					<l n="77" num="15.13"><w n="77.1">Comme</w> <w n="77.2">le</w> <w n="77.3">fut</w> <w n="77.4">jadis</w> <w n="77.5">ton</w> <w n="77.6">corps</w>, <w n="77.7">ô</w> <w n="77.8">divin</w> <w n="77.9">Maître</w>,</l>
					<l n="78" num="15.14"><w n="78.1">Alors</w> — <w n="78.2">oh</w> ! <w n="78.3">n</w>’<w n="78.4">est</w>-<w n="78.5">ce</w> <w n="78.6">pas</w> ? — <w n="78.7">il</w> <w n="78.8">suffirait</w> <w n="78.9">qu</w>’<w n="78.10">un</w> <w n="78.11">prêtre</w>,</l>
					<l n="79" num="15.15"><w n="79.1">Errant</w>, <w n="79.2">au</w> <w n="79.3">crépuscule</w>, <w n="79.4">en</w> <w n="79.5">de</w> <w n="79.6">mornes</w> <w n="79.7">sentiers</w>,</l>
					<l n="80" num="15.16"><w n="80.1">Trouvât</w> <w n="80.2">sur</w> <w n="80.3">son</w> <w n="80.4">chemin</w> <w n="80.5">deux</w> <w n="80.6">chrétiens</w>, <w n="80.7">les</w> <w n="80.8">derniers</w>,</l>
					<l n="81" num="15.17"><w n="81.1">Et</w> <w n="81.2">rompît</w> <w n="81.3">avec</w> <w n="81.4">eux</w>, <w n="81.5">Jésus</w>, <w n="81.6">le</w> <w n="81.7">pain</w> <w n="81.8">Pnystique</w>.</l>
					<l n="82" num="15.18"><w n="82.1">Oh</w> ! <w n="82.2">n</w>’<w n="82.3">est</w>-<w n="82.4">ce</w> <w n="82.5">pas</w> <w n="82.6">qu</w>’<w n="82.7">alors</w>, <w n="82.8">forts</w> <w n="82.9">de</w> <w n="82.10">ce</w> <w n="82.11">viatique</w>,</l>
					<l n="83" num="15.19"><w n="83.1">Comme</w> <w n="83.2">ceux</w> <w n="83.3">d</w>’<w n="83.4">Emmaüs</w>, <w n="83.5">dès</w> <w n="83.6">le</w> <w n="83.7">soleil</w> <w n="83.8">levant</w>,</l>
					<l n="84" num="15.20"><w n="84.1">Ils</w> <w n="84.2">iraient</w> <w n="84.3">proclamer</w> <w n="84.4">que</w> <w n="84.5">le</w> <w n="84.6">Christ</w> <w n="84.7">est</w> <w n="84.8">vivant</w> ?</l>
					<l n="85" num="15.21"><w n="85.1">N</w>’<w n="85.2">est</w>-<w n="85.3">ce</w> <w n="85.4">pas</w> <w n="85.5">que</w>, <w n="85.6">semant</w> <w n="85.7">ta</w> <w n="85.8">parole</w> <w n="85.9">féconde</w>,</l>
					<l n="86" num="15.22"><w n="86.1">Ils</w> <w n="86.2">feraient</w> <w n="86.3">de</w> <w n="86.4">nouveau</w> <w n="86.5">la</w> <w n="86.6">conquête</w> <w n="86.7">du</w> <w n="86.8">monde</w>,</l>
					<l n="87" num="15.23"><w n="87.1">Et</w> <w n="87.2">que</w> <w n="87.3">tous</w>, <w n="87.4">revenant</w> <w n="87.5">au</w> <w n="87.6">Dieu</w> <w n="87.7">de</w> <w n="87.8">vérité</w>,</l>
					<l part="I" n="88" num="15.24"><w n="88.1">De</w> <w n="88.2">nouveau</w> <w n="88.3">s</w>’<w n="88.4">écrieraient</w> : </l>
					<l part="F" n="88" num="15.24">— <w n="88.5">Il</w> <w n="88.6">est</w> <w n="88.7">ressuscité</w> ! »</l>
				</lg>
			</div></body></text></TEI>