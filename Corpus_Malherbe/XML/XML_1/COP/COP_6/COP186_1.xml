<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">DES VERS FRANÇAIS</title>
				<title type="medium">Édition électronique</title>
				<author key="COP">
					<name>
						<forename>François</forename>
						<surname>COPPÉE</surname>
					</name>
					<date from="1842" to="1908">1842-1908</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2263 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">COP_6</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">DES VERS FRANÇAIS</title>
						<author>François Coppée</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/francoiscoppeedesversfrancais.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Poésies complètes de François Coppée</title>
						<author>François Coppée</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre</publisher>
							<date when="1925">1925</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1906">1906</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="COP186">
				<head type="main">Veillée de Noël</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Dehors</w>, <w n="1.2">c</w>’<w n="1.3">est</w> <w n="1.4">un</w> <w n="1.5">confus</w> <w n="1.6">murmure</w>, <w n="1.7">un</w> <w n="1.8">vague</w> <w n="1.9">bruit</w>.</l>
					<l n="2" num="1.2"><w n="2.1">On</w> <w n="2.2">dirait</w> <w n="2.3">un</w> <w n="2.4">léger</w> <w n="2.5">bourdonnement</w> <w n="2.6">d</w>’<w n="2.7">abeille</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Et</w>, <w n="3.2">dans</w> <w n="3.3">la</w> <w n="3.4">chambre</w> <w n="3.5">tiède</w> <w n="3.6">et</w> <w n="3.7">bien</w> <w n="3.8">close</w> <w n="3.9">où</w> <w n="3.10">je</w> <w n="3.11">veille</w>,</l>
					<l n="4" num="1.4"><w n="4.1">J</w>’<w n="4.2">écoute</w> <w n="4.3">au</w> <w n="4.4">loin</w> <w n="4.5">sonner</w> <w n="4.6">la</w> <w n="4.7">messe</w> <w n="4.8">de</w> <w n="4.9">minuit</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Je</w> <w n="5.2">songe</w>, <w n="5.3">en</w> <w n="5.4">tisonnant</w> <w n="5.5">la</w> <w n="5.6">braise</w> <w n="5.7">qui</w> <w n="5.8">s</w>’<w n="5.9">écroule</w>,</l>
					<l n="6" num="2.2"><w n="6.1">Que</w> <w n="6.2">pour</w> <w n="6.3">moi</w>, <w n="6.4">vieux</w> <w n="6.5">pécheur</w>, <w n="6.6">a</w> <w n="6.7">retenti</w> <w n="6.8">la</w> <w n="6.9">voix</w></l>
					<l n="7" num="2.3"><w n="7.1">Des</w> <w n="7.2">cloches</w> <w n="7.3">de</w> <w n="7.4">Noël</w> <w n="7.5">vainement</w> <w n="7.6">tant</w> <w n="7.7">de</w> <w n="7.8">fois</w> ;</l>
					<l n="8" num="2.4"><w n="8.1">Et</w> <w n="8.2">voilà</w> <w n="8.3">les</w> <w n="8.4">regrets</w> <w n="8.5">qui</w> <w n="8.6">m</w>’<w n="8.7">assaillent</w> <w n="8.8">en</w> <w n="8.9">foule</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Car</w> <w n="9.2">je</w> <w n="9.3">serai</w> <w n="9.4">jugé</w> <w n="9.5">bientôt</w>, — <w n="9.6">qui</w> <w n="9.7">sait</w> ? — <w n="9.8">demain</w> ;</l>
					<l n="10" num="3.2"><w n="10.1">Et</w> <w n="10.2">cependant</w>, <w n="10.3">pareil</w> <w n="10.4">à</w> <w n="10.5">la</w> <w n="10.6">mer</w> <w n="10.7">sur</w> <w n="10.8">les</w> <w n="10.9">côtes</w>,</l>
					<l n="11" num="3.3"><w n="11.1">Monte</w> <w n="11.2">toujours</w> <w n="11.3">en</w> <w n="11.4">moi</w> <w n="11.5">le</w> <w n="11.6">flot</w> <w n="11.7">lourd</w> <w n="11.8">de</w> <w n="11.9">mes</w> <w n="11.10">fautes</w>,</l>
					<l n="12" num="3.4"><w n="12.1">Et</w> <w n="12.2">j</w>’<w n="12.3">en</w> <w n="12.4">fais</w> <w n="12.5">tristement</w> <w n="12.6">le</w> <w n="12.7">sévère</w> <w n="12.8">examen</w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">C</w>’<w n="13.2">est</w> <w n="13.3">donc</w> <w n="13.4">vrai</w>. <w n="13.5">J</w>’<w n="13.6">ai</w> <w n="13.7">vécu</w> <w n="13.8">si</w> <w n="13.9">longtemps</w> <w n="13.10">près</w> <w n="13.11">des</w> <w n="13.12">fanges</w></l>
					<l n="14" num="4.2"><w n="14.1">Où</w> <w n="14.2">mes</w> <w n="14.3">pieds</w> <w n="14.4">imprudents</w> <w n="14.5">souvent</w> <w n="14.6">se</w> <w n="14.7">sont</w> <w n="14.8">plongés</w>,</l>
					<l n="15" num="4.3"><w n="15.1">Et</w> <w n="15.2">je</w> <w n="15.3">n</w>’<w n="15.4">ai</w> <w n="15.5">pas</w> <w n="15.6">suivi</w> <w n="15.7">l</w>’<w n="15.8">étoile</w> <w n="15.9">des</w> <w n="15.10">bergers</w>,</l>
					<l n="16" num="4.4"><w n="16.1">Et</w> <w n="16.2">je</w> <w n="16.3">suis</w> <w n="16.4">resté</w> <w n="16.5">sourd</w> <w n="16.6">au</w> <w n="16.7">chœur</w> <w n="16.8">d</w>’<w n="16.9">appel</w> <w n="16.10">des</w> <w n="16.11">anges</w> !</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">O</w> <w n="17.2">nuit</w> <w n="17.3">de</w> <w n="17.4">Bethléem</w>, <w n="17.5">en</w> <w n="17.6">ton</w> <w n="17.7">suave</w> <w n="17.8">azur</w>,</l>
					<l n="18" num="5.2"><w n="18.1">A</w> <w n="18.2">présent</w> <w n="18.3">je</w> <w n="18.4">vois</w> <w n="18.5">l</w>’<w n="18.6">astre</w> <w n="18.7">et</w> <w n="18.8">j</w>’<w n="18.9">entends</w> <w n="18.10">le</w> <w n="18.11">cantique</w>.</l>
					<l n="19" num="5.3"><w n="19.1">Pourtant</w>, <w n="19.2">bien</w> <w n="19.3">qu</w>’<w n="19.4">éclairé</w> <w n="19.5">par</w> <w n="19.6">ta</w> <w n="19.7">splendeur</w> <w n="19.8">mystique</w>,</l>
					<l n="20" num="5.4"><w n="20.1">Suis</w>-<w n="20.2">je</w> <w n="20.3">vraiment</w> <w n="20.4">meilleur</w> ? <w n="20.5">Suis</w>-<w n="20.6">je</w> <w n="20.7">un</w> <w n="20.8">peu</w> <w n="20.9">moins</w> <w n="20.10">impur</w> ?</l>
				</lg>
				<ab type="star">※※※</ab>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">Mais</w> <w n="21.2">j</w>’<w n="21.3">ai</w> <w n="21.4">tort</w>. <w n="21.5">Reprenons</w> <w n="21.6">courage</w> <w n="21.7">et</w> <w n="21.8">confiance</w>.</l>
					<l n="22" num="6.2"><w n="22.1">L</w>’<w n="22.2">Enfant</w>-<w n="22.3">Dieu</w> <w n="22.4">ne</w> <w n="22.5">veut</w> <w n="22.6">pas</w> <w n="22.7">qu</w>’<w n="22.8">on</w> <w n="22.9">tremble</w> <w n="22.10">devant</w> <w n="22.11">lui</w>.</l>
					<l n="23" num="6.3"><w n="23.1">Je</w> <w n="23.2">prétends</w> <w n="23.3">l</w>’<w n="23.4">adorer</w> <w n="23.5">et</w> <w n="23.6">le</w> <w n="23.7">voir</w> <w n="23.8">aujourd</w>’<w n="23.9">hui</w></l>
					<l n="24" num="6.4"><w n="24.1">Avec</w> <w n="24.2">les</w> <w n="24.3">yeux</w>, <w n="24.4">avec</w> <w n="24.5">l</w>’<w n="24.6">âme</w> <w n="24.7">de</w> <w n="24.8">mon</w> <w n="24.9">enfance</w>.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">Car</w> <w n="25.2">mes</w> <w n="25.3">soirs</w> <w n="25.4">de</w> <w n="25.5">Noël</w> <w n="25.6">les</w> <w n="25.7">meilleurs</w>, <w n="25.8">je</w> <w n="25.9">les</w> <w n="25.10">eus</w></l>
					<l n="26" num="7.2"><w n="26.1">Alors</w> <w n="26.2">qu</w>’<w n="26.3">innocemment</w> — <w n="26.4">Bonne</w> <w n="26.5">Vierge</w>, <w n="26.6">pardonne</w> ! —</l>
					<l n="27" num="7.3"><w n="27.1">Je</w> <w n="27.2">confondais</w> <w n="27.3">un</w> <w n="27.4">peu</w> <w n="27.5">ma</w> <w n="27.6">mère</w> <w n="27.7">et</w> <w n="27.8">la</w> <w n="27.9">Madone</w>,</l>
					<l n="28" num="7.4"><w n="28.1">Et</w> <w n="28.2">quand</w> <w n="28.3">j</w>’<w n="28.4">étais</w> <w n="28.5">pour</w> <w n="28.6">elle</w> <w n="28.7">un</w> <w n="28.8">peu</w> <w n="28.9">l</w>’<w n="28.10">Enfant</w>-<w n="28.11">Jésus</w>.</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1">Elle</w> <w n="29.2">m</w>’<w n="29.3">avait</w> <w n="29.4">montré</w>, <w n="29.5">dans</w> <w n="29.6">un</w> <w n="29.7">livre</w> <w n="29.8">d</w>’<w n="29.9">images</w>,</l>
					<l n="30" num="8.2"><w n="30.1">Saint</w> <w n="30.2">Joseph</w> <w n="30.3">s</w>’<w n="30.4">appuyant</w>, <w n="30.5">las</w>, <w n="30.6">sur</w> <w n="30.7">son</w> <w n="30.8">grand</w> <w n="30.9">bâton</w>,</l>
					<l n="31" num="8.3"><w n="31.1">Les</w> <w n="31.2">rustiques</w> <w n="31.3">pasteurs</w> <w n="31.4">sous</w> <w n="31.5">leurs</w> <w n="31.6">peaux</w> <w n="31.7">de</w> <w n="31.8">mouton</w>,</l>
					<l n="32" num="8.4"><w n="32.1">Et</w>, <w n="32.2">coiffés</w> <w n="32.3">de</w> <w n="32.4">turbans</w> <w n="32.5">somptueux</w>, <w n="32.6">les</w> <w n="32.7">Rois</w> <w n="32.8">Mages</w>.</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1"><w n="33.1">Comme</w> <w n="33.2">il</w> <w n="33.3">s</w>’<w n="33.4">était</w> <w n="33.5">gravé</w> <w n="33.6">dans</w> <w n="33.7">mon</w> <w n="33.8">cerveau</w> <w n="33.9">tout</w> <w n="33.10">neuf</w>,</l>
					<l n="34" num="9.2"><w n="34.1">Cet</w> <w n="34.2">enfant</w> <w n="34.3">radieux</w> <w n="34.4">dans</w> <w n="34.5">cette</w> <w n="34.6">étable</w> <w n="34.7">sombre</w></l>
					<l n="35" num="9.3"><w n="35.1">Où</w>, <w n="35.2">sur</w> <w n="35.3">le</w> <w n="35.4">mur</w> <w n="35.5">croulant</w>, <w n="35.6">se</w> <w n="35.7">dresse</w> <w n="35.8">et</w> <w n="35.9">grandit</w> <w n="35.10">l</w>’<w n="35.11">ombre</w></l>
					<l n="36" num="9.4"><w n="36.1">Des</w> <w n="36.2">oreilles</w> <w n="36.3">de</w> <w n="36.4">l</w>’<w n="36.5">âne</w> <w n="36.6">et</w> <w n="36.7">des</w> <w n="36.8">cornes</w> <w n="36.9">du</w> <w n="36.10">bœuf</w> !</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1"><w n="37.1">Je</w> <w n="37.2">retrouve</w> <w n="37.3">aujourd</w>’<w n="37.4">hui</w> <w n="37.5">l</w>’<w n="37.6">impression</w> <w n="37.7">première</w>.</l>
					<l n="38" num="10.2"><w n="38.1">A</w> <w n="38.2">genoux</w>, <w n="38.3">cils</w> <w n="38.4">baissés</w>, <w n="38.5">devant</w> <w n="38.6">le</w> <w n="38.7">cher</w> <w n="38.8">petit</w>,</l>
					<l n="39" num="10.3"><w n="39.1">La</w> <w n="39.2">Vierge</w> <w n="39.3">est</w> <w n="39.4">là</w>, <w n="39.5">priant</w> <w n="39.6">son</w> <w n="39.7">Fils</w> <w n="39.8">qui</w> <w n="39.9">resplendit</w></l>
					<l n="40" num="10.4"><w n="40.1">D</w>’<w n="40.2">une</w> <w n="40.3">mystérieuse</w> <w n="40.4">et</w> <w n="40.5">céleste</w> <w n="40.6">lumière</w>.</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1"><w n="41.1">Je</w> <w n="41.2">le</w> <w n="41.3">vois</w> <w n="41.4">comme</w> <w n="41.5">alors</w>, <w n="41.6">le</w> <w n="41.7">divin</w> <w n="41.8">nouveau</w>-<w n="41.9">né</w> :</l>
					<l n="42" num="11.2"><w n="42.1">Dans</w> <w n="42.2">un</w> <w n="42.3">geste</w> <w n="42.4">charmant</w> <w n="42.5">qui</w> <w n="42.6">bénit</w> <w n="42.7">et</w> <w n="42.8">qui</w> <w n="42.9">joue</w>,</l>
					<l n="43" num="11.3"><w n="43.1">De</w> <w n="43.2">sa</w> <w n="43.3">petite</w> <w n="43.4">main</w> <w n="43.5">il</w> <w n="43.6">caresse</w> <w n="43.7">la</w> <w n="43.8">joue</w></l>
					<l n="44" num="11.4"><w n="44.1">Du</w> <w n="44.2">pâtre</w> <w n="44.3">en</w> <w n="44.4">cheveux</w> <w n="44.5">gris</w> <w n="44.6">devant</w> <w n="44.7">lui</w> <w n="44.8">prosterné</w> ;</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1"><w n="45.1">Ou</w> <w n="45.2">bien</w>, <w n="45.3">si</w> <w n="45.4">gracieux</w>, <w n="45.5">nu</w> <w n="45.6">malgré</w> <w n="45.7">la</w> <w n="45.8">nuit</w> <w n="45.9">fraîche</w>,</l>
					<l n="46" num="12.2"><w n="46.1">Il</w> <w n="46.2">se</w> <w n="46.3">roule</w> <w n="46.4">en</w> <w n="46.5">tenant</w> <w n="46.6">à</w> <w n="46.7">plein</w> <w n="46.8">poing</w> <w n="46.9">son</w> <w n="46.10">orteil</w>,</l>
					<l n="47" num="12.3"><w n="47.1">Et</w> <w n="47.2">son</w> <w n="47.3">corps</w> <w n="47.4">potelé</w> <w n="47.5">brille</w> <w n="47.6">comme</w> <w n="47.7">un</w> <w n="47.8">soleil</w></l>
					<l n="48" num="12.4"><w n="48.1">Et</w> <w n="48.2">transforme</w> <w n="48.3">en</w> <w n="48.4">rayons</w> <w n="48.5">les</w> <w n="48.6">pailles</w> <w n="48.7">de</w> <w n="48.8">la</w> <w n="48.9">crèche</w>.</l>
				</lg>
				<ab type="star">※※※</ab>
				<lg n="13">
					<l n="49" num="13.1"><w n="49.1">C</w>’<w n="49.2">est</w> <w n="49.3">ainsi</w> <w n="49.4">que</w> <w n="49.5">la</w> <w n="49.6">Foi</w>, <w n="49.7">comme</w> <w n="49.8">éclôt</w> <w n="49.9">une</w> <w n="49.10">fleur</w>,</l>
					<l n="50" num="13.2"><w n="50.1">Naquit</w> <w n="50.2">en</w> <w n="50.3">moi</w>, <w n="50.4">candide</w>, <w n="50.5">ingénue</w>, <w n="50.6">instinctive</w>,</l>
					<l n="51" num="13.3"><w n="51.1">Quand</w> <w n="51.2">je</w> <w n="51.3">balbutiais</w> <w n="51.4">la</w> <w n="51.5">prière</w> <w n="51.6">naïve</w></l>
					<l n="52" num="13.4"><w n="52.1">Des</w> <w n="52.2">tout</w> <w n="52.3">petits</w> : « <w n="52.4">Mon</w> <w n="52.5">Dieu</w>, <w n="52.6">je</w> <w n="52.7">vous</w> <w n="52.8">donne</w> <w n="52.9">mon</w> <w n="52.10">cœur</w> ! »</l>
				</lg>
				<lg n="14">
					<l n="53" num="14.1"><w n="53.1">Et</w> <w n="53.2">quand</w> <w n="53.3">dans</w> <w n="53.4">ma</w> <w n="53.5">couchette</w>, <w n="53.6">enfant</w> <w n="53.7">faible</w> <w n="53.8">et</w> <w n="53.9">malade</w>,</l>
					<l n="54" num="14.2"><w n="54.1">Ma</w> <w n="54.2">mère</w> <w n="54.3">me</w> <w n="54.4">voyait</w> <w n="54.5">tendre</w>, <w n="54.6">avec</w> <w n="54.7">un</w> <w n="54.8">soupir</w>,</l>
					<l n="55" num="14.3"><w n="55.1">Mes</w> <w n="55.2">deux</w> <w n="55.3">mains</w> <w n="55.4">vers</w> <w n="55.5">Jésus</w>, <w n="55.6">avant</w> <w n="55.7">de</w> <w n="55.8">m</w>’<w n="55.9">endormir</w>,</l>
					<l n="56" num="14.4"><w n="56.1">Pour</w> <w n="56.2">l</w>’<w n="56.3">embrasser</w> <w n="56.4">ainsi</w> <w n="56.5">qu</w>’<w n="56.6">un</w> <w n="56.7">petit</w> <w n="56.8">camarade</w>.</l>
				</lg>
				<ab type="star">※※※</ab>
				<lg n="15">
					<l n="57" num="15.1"><w n="57.1">Un</w> <w n="57.2">demi</w>-<w n="57.3">siècle</w> <w n="57.4">et</w> <w n="57.5">plus</w> <w n="57.6">a</w> <w n="57.7">passé</w> <w n="57.8">depuis</w> <w n="57.9">lors</w>,</l>
					<l n="58" num="15.2"><w n="58.1">Le</w> <w n="58.2">vent</w> <w n="58.3">des</w> <w n="58.4">passions</w>, <w n="58.5">partout</w> <w n="58.6">où</w> <w n="58.7">l</w>’<w n="58.8">homme</w> <w n="58.9">pèche</w>,</l>
					<l n="59" num="15.3"><w n="59.1">M</w>’<w n="59.2">emporta</w>, <w n="59.3">me</w> <w n="59.4">roula</w> <w n="59.5">comme</w> <w n="59.6">une</w> <w n="59.7">feuille</w> <w n="59.8">sèche</w>,</l>
					<l n="60" num="15.4"><w n="60.1">Et</w> <w n="60.2">je</w> <w n="60.3">me</w> <w n="60.4">suis</w> <w n="60.5">cent</w> <w n="60.6">fois</w> <w n="60.7">souillé</w> <w n="60.8">l</w>’<w n="60.9">âme</w> <w n="60.10">et</w> <w n="60.11">le</w> <w n="60.12">corps</w>.</l>
				</lg>
				<lg n="16">
					<l n="61" num="16.1"><w n="61.1">Mais</w>, <w n="61.2">enfin</w>, <w n="61.3">j</w>’<w n="61.4">ai</w> <w n="61.5">rougi</w> <w n="61.6">de</w> <w n="61.7">ce</w> <w n="61.8">honteux</w> <w n="61.9">délire</w>.</l>
					<l n="62" num="16.2"><w n="62.1">J</w>’<w n="62.2">ai</w> <w n="62.3">rouvert</w> <w n="62.4">le</w> <w n="62.5">vieux</w> <w n="62.6">livre</w> <w n="62.7">où</w>, <w n="62.8">montrant</w> <w n="62.9">chaque</w> <w n="62.10">mot</w>,</l>
					<l n="63" num="16.3"><w n="63.1">Patiemment</w>, <w n="63.2">avec</w> <w n="63.3">son</w> <w n="63.4">aiguille</w> <w n="63.5">à</w> <w n="63.6">tricot</w>,</l>
					<l n="64" num="16.4"><w n="64.1">Ma</w> <w n="64.2">mère</w>, <w n="64.3">quand</w> <w n="64.4">j</w>’<w n="64.5">étais</w> <w n="64.6">enfant</w>, <w n="64.7">m</w>’<w n="64.8">apprit</w> <w n="64.9">à</w> <w n="64.10">lire</w>.</l>
				</lg>
				<lg n="17">
					<l n="65" num="17.1"><w n="65.1">Je</w> <w n="65.2">revins</w> <w n="65.3">humblement</w> <w n="65.4">au</w> <w n="65.5">Dieu</w> <w n="65.6">qui</w> <w n="65.7">fut</w> <w n="65.8">le</w> <w n="65.9">sien</w>.</l>
					<l n="66" num="17.2"><w n="66.1">Je</w> <w n="66.2">retrouvai</w> <w n="66.3">le</w> <w n="66.4">pur</w> <w n="66.5">trésor</w> <w n="66.6">de</w> <w n="66.7">ma</w> <w n="66.8">croyance</w>,</l>
					<l n="67" num="17.3"><w n="67.1">Et</w> <w n="67.2">maintenant</w>, <w n="67.3">malgré</w> <w n="67.4">plus</w> <w n="67.5">d</w>’<w n="67.6">une</w> <w n="67.7">défaillance</w>,</l>
					<l n="68" num="17.4"><w n="68.1">Je</w> <w n="68.2">tâche</w> <w n="68.3">de</w> <w n="68.4">finir</w> <w n="68.5">mon</w> <w n="68.6">voyage</w> <w n="68.7">en</w> <w n="68.8">chrétien</w>.</l>
				</lg>
				<lg n="18">
					<l n="69" num="18.1"><w n="69.1">Hélas</w> ! <w n="69.2">c</w>’<w n="69.3">est</w> <w n="69.4">un</w> <w n="69.5">chemin</w> <w n="69.6">où</w> <w n="69.7">je</w> <w n="69.8">trébuche</w> <w n="69.9">et</w> <w n="69.10">glisse</w>,</l>
					<l n="70" num="18.2"><w n="70.1">Ployant</w> <w n="70.2">sous</w> <w n="70.3">le</w> <w n="70.4">fardeau</w> <w n="70.5">si</w> <w n="70.6">lourd</w> <w n="70.7">de</w> <w n="70.8">mon</w> <w n="70.9">passé</w>,</l>
					<l n="71" num="18.3"><w n="71.1">Un</w> <w n="71.2">sentier</w> <w n="71.3">dans</w> <w n="71.4">les</w> <w n="71.5">monts</w>, <w n="71.6">par</w> <w n="71.7">la</w> <w n="71.8">neige</w> <w n="71.9">effacé</w>,</l>
					<l n="72" num="18.4"><w n="72.1">Où</w> <w n="72.2">j</w>’<w n="72.3">ai</w> <w n="72.4">souvent</w> <w n="72.5">failli</w> <w n="72.6">choir</w> <w n="72.7">dans</w> <w n="72.8">le</w> <w n="72.9">précipice</w>.</l>
				</lg>
				<lg n="19">
					<l n="73" num="19.1"><w n="73.1">Mais</w>, <w n="73.2">ce</w> <w n="73.3">soir</w>, <w n="73.4">écoutant</w> <w n="73.5">les</w> <w n="73.6">cloches</w> <w n="73.7">bourdonner</w></l>
					<l n="74" num="19.2"><w n="74.1">Derrière</w> <w n="74.2">les</w> <w n="74.3">épais</w> <w n="74.4">rideaux</w> <w n="74.5">de</w> <w n="74.6">ma</w> <w n="74.7">fenêtre</w>,</l>
					<l n="75" num="19.3"><w n="75.1">Je</w> <w n="75.2">songe</w> <w n="75.3">à</w> <w n="75.4">la</w> <w n="75.5">bonté</w> <w n="75.6">du</w> <w n="75.7">Dieu</w> <w n="75.8">qui</w> <w n="75.9">vient</w> <w n="75.10">de</w> <w n="75.11">naître</w></l>
					<l n="76" num="19.4"><w n="76.1">Et</w> <w n="76.2">j</w>’<w n="76.3">ai</w> <w n="76.4">le</w> <w n="76.5">ferme</w> <w n="76.6">espoir</w> <w n="76.7">qu</w>’<w n="76.8">il</w> <w n="76.9">veut</w> <w n="76.10">me</w> <w n="76.11">pardonner</w>.</l>
				</lg>
				<lg n="20">
					<l n="77" num="20.1"><w n="77.1">Debout</w> <w n="77.2">dans</w> <w n="77.3">le</w> <w n="77.4">giron</w> <w n="77.5">de</w> <w n="77.6">la</w> <w n="77.7">Vierge</w> <w n="77.8">Marie</w>,</l>
					<l n="78" num="20.2"><w n="78.1">Il</w> <w n="78.2">m</w>’<w n="78.3">accueille</w> <w n="78.4">et</w> <w n="78.5">m</w>’<w n="78.6">absout</w> <w n="78.7">d</w>’<w n="78.8">un</w> <w n="78.9">geste</w>, <w n="78.10">en</w> <w n="78.11">souriant</w>,</l>
					<l n="79" num="20.3"><w n="79.1">Et</w>, <w n="79.2">comme</w> <w n="79.3">les</w> <w n="79.4">bergers</w> <w n="79.5">et</w> <w n="79.6">les</w> <w n="79.7">rois</w> <w n="79.8">d</w>’<w n="79.9">Orient</w>,</l>
					<l n="80" num="20.4"><w n="80.1">Plein</w> <w n="80.2">d</w>’<w n="80.3">amour</w>, <w n="80.4">devant</w> <w n="80.5">lui</w> <w n="80.6">je</w> <w n="80.7">m</w>’<w n="80.8">agenouille</w> <w n="80.9">et</w> <w n="80.10">prie</w>.</l>
				</lg>
				<lg n="21">
					<l n="81" num="21.1"><w n="81.1">Mon</w> <w n="81.2">cœur</w>, <w n="81.3">ce</w> <w n="81.4">soir</w>, <w n="81.5">au</w> <w n="81.6">cœur</w> <w n="81.7">d</w>’<w n="81.8">un</w> <w n="81.9">enfant</w> <w n="81.10">est</w> <w n="81.11">pareil</w>.</l>
					<l n="82" num="21.2"><w n="82.1">Je</w> <w n="82.2">suis</w> <w n="82.3">sûr</w> <w n="82.4">que</w> <w n="82.5">sur</w> <w n="82.6">moi</w> <w n="82.7">le</w> <w n="82.8">pardon</w> <w n="82.9">va</w> <w n="82.10">descendre</w>,</l>
					<l n="83" num="21.3"><w n="83.1">Comme</w> <w n="83.2">jadis</w>, <w n="83.3">mettant</w> <w n="83.4">mes</w> <w n="83.5">souliers</w> <w n="83.6">dans</w> <w n="83.7">la</w> <w n="83.8">cendre</w>,</l>
					<l n="84" num="21.4"><w n="84.1">J</w>’<w n="84.2">étais</w> <w n="84.3">sûr</w> <w n="84.4">d</w>’<w n="84.5">y</w> <w n="84.6">trouver</w> <w n="84.7">des</w> <w n="84.8">jouets</w>, <w n="84.9">au</w> <w n="84.10">réveil</w>.</l>
				</lg>
				<lg n="22">
					<l n="85" num="22.1"><w n="85.1">O</w> <w n="85.2">douceur</w> ! <w n="85.3">Le</w> <w n="85.4">petit</w> <w n="85.5">Jésus</w> <w n="85.6">a</w> <w n="85.7">la</w> <w n="85.8">puissance</w></l>
					<l n="86" num="22.2"><w n="86.1">De</w> <w n="86.2">faire</w> <w n="86.3">refleurir</w>, <w n="86.4">avec</w> <w n="86.5">un</w> <w n="86.6">seul</w> <w n="86.7">regard</w>,</l>
					<l n="87" num="22.3"><w n="87.1">L</w>’<w n="87.2">enfantine</w> <w n="87.3">candeur</w> <w n="87.4">dans</w> <w n="87.5">l</w>’<w n="87.6">âme</w> <w n="87.7">d</w>’<w n="87.8">un</w> <w n="87.9">vieillard</w>,</l>
					<l n="88" num="22.4"><w n="88.1">Et</w>, <w n="88.2">dans</w> <w n="88.3">un</w> <w n="88.4">vieux</w> <w n="88.5">coupable</w>, <w n="88.6">une</w> <w n="88.7">jeune</w> <w n="88.8">innocence</w> !</l>
				</lg>
				<lg n="23">
					<l n="89" num="23.1"><w n="89.1">De</w> <w n="89.2">puissants</w> <w n="89.3">malfaiteurs</w>, <w n="89.4">en</w> <w n="89.5">ce</w> <w n="89.6">temps</w> <w n="89.7">trop</w> <w n="89.8">vanté</w>,</l>
					<l n="90" num="23.2"><w n="90.1">S</w>’<w n="90.2">acharnent</w>, <w n="90.3">furieux</w>, <w n="90.4">contre</w> <w n="90.5">l</w>’<w n="90.6">œuvre</w> <w n="90.7">féconde</w></l>
					<l n="91" num="23.3"><w n="91.1">De</w> <w n="91.2">celui</w> <w n="91.3">qui</w> — <w n="91.4">voilà</w> <w n="91.5">vingt</w> <w n="91.6">siècles</w> — <w n="91.7">dans</w> <w n="91.8">ce</w> <w n="91.9">monde</w></l>
					<l n="92" num="23.4"><w n="92.1">Fonda</w> <w n="92.2">la</w> <w n="92.3">plus</w> <w n="92.4">sublime</w> <w n="92.5">école</w> <w n="92.6">de</w> <w n="92.7">bonté</w>.</l>
				</lg>
				<lg n="24">
					<l n="93" num="24.1"><w n="93.1">En</w> <w n="93.2">plus</w> <w n="93.3">d</w>’<w n="93.4">un</w> <w n="93.5">lieu</w>, <w n="93.6">déjà</w>, — <w n="93.7">spectacle</w> <w n="93.8">lamentable</w> ! —</l>
					<l n="94" num="24.2"><w n="94.1">L</w>’<w n="94.2">herbe</w> <w n="94.3">de</w> <w n="94.4">l</w>’<w n="94.5">abandon</w> <w n="94.6">pousse</w> <w n="94.7">au</w> <w n="94.8">pied</w> <w n="94.9">de</w> <w n="94.10">la</w> <w n="94.11">Croix</w>.</l>
					<l n="95" num="24.3"><w n="95.1">Ils</w> <w n="95.2">veulent</w>, <w n="95.3">à</w> <w n="95.4">présent</w>, <w n="95.5">par</w> <w n="95.6">leurs</w> <w n="95.7">iniques</w> <w n="95.8">lois</w>,</l>
					<l n="96" num="24.4"><w n="96.1">Éloigner</w> <w n="96.2">nos</w> <w n="96.3">enfants</w> <w n="96.4">du</w> <w n="96.5">Dieu</w> <w n="96.6">né</w> <w n="96.7">dans</w> <w n="96.8">l</w>’<w n="96.9">étable</w>.</l>
				</lg>
				<lg n="25">
					<l n="97" num="25.1"><w n="97.1">Pousseront</w>-<w n="97.2">ils</w> <w n="97.3">plus</w> <w n="97.4">loin</w> <w n="97.5">leur</w> <w n="97.6">labeur</w> <w n="97.7">criminel</w> ?</l>
					<l n="98" num="25.2"><w n="98.1">Fermeront</w>-<w n="98.2">ils</w> <w n="98.3">bientôt</w> <w n="98.4">l</w>’<w n="98.5">église</w> — <w n="98.6">après</w> <w n="98.7">l</w>’<w n="98.8">école</w> ?</l>
					<l n="99" num="25.3"><w n="99.1">L</w>’<w n="99.2">an</w> <w n="99.3">prochain</w>, — <w n="99.4">que</w> <w n="99.5">sait</w>-<w n="99.6">on</w> ?… <w n="99.7">la</w> <w n="99.8">rage</w> <w n="99.9">les</w> <w n="99.10">affole</w>… —</l>
					<l n="100" num="25.4"><w n="100.1">Entendrons</w>-<w n="100.2">nous</w> <w n="100.3">encor</w> <w n="100.4">les</w> <w n="100.5">cloches</w> <w n="100.6">de</w> <w n="100.7">Noël</w> ?</l>
				</lg>
				<lg n="26">
					<l n="101" num="26.1"><w n="101.1">Mais</w> <w n="101.2">la</w> <w n="101.3">haine</w> <w n="101.4">est</w> <w n="101.5">stérile</w> <w n="101.6">et</w> <w n="101.7">son</w> <w n="101.8">œuvre</w> <w n="101.9">éphémère</w>.</l>
					<l n="102" num="26.2"><w n="102.1">Ils</w> <w n="102.2">n</w>’<w n="102.3">auront</w> <w n="102.4">rien</w> <w n="102.5">fait</w>, <w n="102.6">rien</w>, <w n="102.7">tant</w> <w n="102.8">qu</w>’<w n="102.9">un</w> <w n="102.10">pauvre</w> <w n="102.11">petit</w>,</l>
					<l n="103" num="26.3"><w n="103.1">Devant</w> <w n="103.2">un</w> <w n="103.3">Christ</w> <w n="103.4">orné</w> <w n="103.5">d</w>’<w n="103.6">un</w> <w n="103.7">brin</w> <w n="103.8">de</w> <w n="103.9">buis</w> <w n="103.10">bénit</w>,</l>
					<l n="104" num="26.4"><w n="104.1">Répétera</w>, <w n="104.2">naïf</w>, <w n="104.3">les</w> <w n="104.4">mots</w> <w n="104.5">dits</w> <w n="104.6">par</w> <w n="104.7">sa</w> <w n="104.8">mère</w>.</l>
				</lg>
				<lg n="27">
					<l n="105" num="27.1"><w n="105.1">Jetez</w> <w n="105.2">la</w> <w n="105.3">Croix</w> <w n="105.4">à</w> <w n="105.5">terre</w> <w n="105.6">et</w> <w n="105.7">l</w>’<w n="105.8">Évangile</w> <w n="105.9">au</w> <w n="105.10">feu</w>,</l>
					<l n="106" num="27.2"><w n="106.1">Persécuteurs</w> ! <w n="106.2">Un</w> <w n="106.3">peu</w> <w n="106.4">de</w> <w n="106.5">vérité</w> <w n="106.6">chrétienne</w></l>
					<l n="107" num="27.3"><w n="107.1">Suffira</w> <w n="107.2">tôt</w> <w n="107.3">ou</w> <w n="107.4">tard</w> <w n="107.5">pour</w> <w n="107.6">qu</w>’<w n="107.7">une</w> <w n="107.8">âme</w> <w n="107.9">revienne</w></l>
					<l n="108" num="27.4"><w n="108.1">A</w> <w n="108.2">la</w> <w n="108.3">foi</w> <w n="108.4">confiante</w>, <w n="108.5">à</w> <w n="108.6">la</w> <w n="108.7">paix</w> <w n="108.8">avec</w> <w n="108.9">Dieu</w>.</l>
				</lg>
				<lg n="28">
					<l n="109" num="28.1"><w n="109.1">Faire</w> <w n="109.2">une</w> <w n="109.3">France</w> <w n="109.4">athée</w>, <w n="109.5">oui</w>, <w n="109.6">c</w>’<w n="109.7">est</w> <w n="109.8">votre</w> <w n="109.9">démence</w> !</l>
					<l n="110" num="28.2"><w n="110.1">Mais</w> <w n="110.2">notre</w> <w n="110.3">sol</w>, <w n="110.4">depuis</w> <w n="110.5">plus</w> <w n="110.6">de</w> <w n="110.7">treize</w> <w n="110.8">cents</w> <w n="110.9">ans</w>,</l>
					<l n="111" num="28.3"><w n="111.1">Avec</w> <w n="111.2">nos</w> <w n="111.3">morts</w>, <w n="111.4">au</w> <w n="111.5">fond</w> <w n="111.6">des</w> <w n="111.7">guérets</w> <w n="111.8">bienfaisants</w>,</l>
					<l n="112" num="28.4"><w n="112.1">Conserve</w> <w n="112.2">une</w> <w n="112.3">immortelle</w> <w n="112.4">et</w> <w n="112.5">pieuse</w> <w n="112.6">semence</w>.</l>
				</lg>
				<lg n="29">
					<l n="113" num="29.1"><w n="113.1">Sachez</w>-<w n="113.2">le</w>. <w n="113.3">Quand</w> <w n="113.4">seraient</w> <w n="113.5">jetés</w> <w n="113.6">bas</w> <w n="113.7">et</w> <w n="113.8">couchés</w></l>
					<l n="114" num="29.2"><w n="114.1">Sur</w> <w n="114.2">la</w> <w n="114.3">terre</w>, <w n="114.4">en</w> <w n="114.5">débris</w>, <w n="114.6">les</w> <w n="114.7">murs</w> <w n="114.8">de</w> <w n="114.9">nos</w> <w n="114.10">églises</w>,</l>
					<l n="115" num="29.3"><w n="115.1">Un</w> <w n="115.2">jour</w> <w n="115.3">nous</w> <w n="115.4">reverrions</w>, <w n="115.5">dardant</w> <w n="115.6">leurs</w> <w n="115.7">flèches</w> <w n="115.8">grises</w>,</l>
					<l n="116" num="29.4"><w n="116.1">Surgir</w> <w n="116.2">une</w> <w n="116.3">moisson</w> <w n="116.4">nouvelle</w> <w n="116.5">de</w> <w n="116.6">clochers</w> ;</l>
				</lg>
				<lg n="30">
					<l n="117" num="30.1"><w n="117.1">Et</w>, <w n="117.2">dans</w> <w n="117.3">un</w> <w n="117.4">très</w> <w n="117.5">joyeux</w> <w n="117.6">branle</w>, <w n="117.7">à</w> <w n="117.8">toute</w> <w n="117.9">volée</w>,</l>
					<l n="118" num="30.2"><w n="118.1">Pour</w> <w n="118.2">célébrer</w> <w n="118.3">l</w>’<w n="118.4">instant</w> <w n="118.5">à</w> <w n="118.6">jamais</w> <w n="118.7">solennel</w></l>
					<l n="119" num="30.3"><w n="119.1">Où</w> <w n="119.2">naquit</w> <w n="119.3">l</w>’<w n="119.4">Homme</w>-<w n="119.5">Dieu</w>, <w n="119.6">le</w> <w n="119.7">Sauveur</w> <w n="119.8">éternel</w>,</l>
					<l n="120" num="30.4"><w n="120.1">Les</w> <w n="120.2">cloches</w> <w n="120.3">sonneraient</w> <w n="120.4">dans</w> <w n="120.5">la</w> <w n="120.6">nuit</w> <w n="120.7">étoilée</w>.</l>
				</lg>
			</div></body></text></TEI>