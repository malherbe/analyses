<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">DES VERS FRANÇAIS</title>
				<title type="medium">Édition électronique</title>
				<author key="COP">
					<name>
						<forename>François</forename>
						<surname>COPPÉE</surname>
					</name>
					<date from="1842" to="1908">1842-1908</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2263 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">COP_6</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">DES VERS FRANÇAIS</title>
						<author>François Coppée</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/francoiscoppeedesversfrancais.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Poésies complètes de François Coppée</title>
						<author>François Coppée</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre</publisher>
							<date when="1925">1925</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1906">1906</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="COP180">
				<head type="main">Sur le Passage d’un Régiment</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Hier</w>, <w n="1.2">songeant</w> <w n="1.3">à</w> <w n="1.4">la</w> <w n="1.5">pauvre</w> <w n="1.6">France</w></l>
					<l n="2" num="1.2"><w n="2.1">Dont</w> <w n="2.2">s</w>’<w n="2.3">achève</w> <w n="2.4">l</w>’<w n="2.5">effondrement</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Et</w> <w n="3.2">navré</w> <w n="3.3">de</w> <w n="3.4">désespérance</w>,</l>
					<l n="4" num="1.4"><w n="4.1">J</w>’<w n="4.2">ai</w> <w n="4.3">vu</w> <w n="4.4">passer</w> <w n="4.5">un</w> <w n="4.6">régiment</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Sans</w> <w n="5.2">doute</w> <w n="5.3">il</w> <w n="5.4">ne</w> <w n="5.5">rappelait</w> <w n="5.6">guère</w></l>
					<l n="6" num="2.2"><w n="6.1">Ceux</w> <w n="6.2">que</w> <w n="6.3">mon</w> <w n="6.4">enfance</w> <w n="6.5">escorta</w>,</l>
					<l n="7" num="2.3"><w n="7.1">Si</w> <w n="7.2">beaux</w> <w n="7.3">sous</w> <w n="7.4">leurs</w> <w n="7.5">haillons</w> <w n="7.6">de</w> <w n="7.7">guerre</w></l>
					<l n="8" num="2.4"><w n="8.1">Encor</w> <w n="8.2">poudreux</w> <w n="8.3">de</w> <w n="8.4">Magenta</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Elles</w> <w n="9.2">sont</w> <w n="9.3">en</w> <w n="9.4">Prusse</w>, <w n="9.5">captives</w>,</l>
					<l n="10" num="3.2"><w n="10.1">Nos</w> <w n="10.2">aigles</w> <w n="10.3">de</w> <w n="10.4">cinquante</w>-<w n="10.5">neuf</w> ;</l>
					<l n="11" num="3.3"><w n="11.1">Et</w>, <w n="11.2">malgré</w> <w n="11.3">ses</w> <w n="11.4">couleurs</w> <w n="11.5">si</w> <w n="11.6">vives</w>,</l>
					<l n="12" num="3.4"><w n="12.1">C</w>’<w n="12.2">est</w> <w n="12.3">toujours</w> <w n="12.4">triste</w>, <w n="12.5">un</w> <w n="12.6">drapeau</w> <w n="12.7">neuf</w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Les</w> <w n="13.2">noms</w> <w n="13.3">glorieux</w> <w n="13.4">qu</w>’<w n="13.5">on</w> <w n="13.6">y</w> <w n="13.7">brode</w></l>
					<l n="14" num="4.2"><w n="14.1">En</w> <w n="14.2">lettrés</w> <w n="14.3">d</w>’<w n="14.4">or</w> <w n="14.5">sont</w> <w n="14.6">par</w> <w n="14.7">trop</w> <w n="14.8">vieux</w>,</l>
					<l n="15" num="4.3"><w n="15.1">Et</w> <w n="15.2">pour</w> <w n="15.3">le</w> <w n="15.4">remettre</w> <w n="15.5">à</w> <w n="15.6">la</w> <w n="15.7">mode</w>,</l>
					<l n="16" num="4.4"><w n="16.1">De</w> <w n="16.2">la</w> <w n="16.3">mitraille</w> <w n="16.4">vaudrait</w> <w n="16.5">mieux</w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">C</w>’<w n="17.2">est</w> <w n="17.3">de</w> <w n="17.4">l</w>’<w n="17.5">opulente</w> <w n="17.6">soierie</w> ;</l>
					<l n="18" num="5.2"><w n="18.1">Mais</w> <w n="18.2">il</w> <w n="18.3">n</w>’<w n="18.4">est</w> <w n="18.5">sublime</w> <w n="18.6">et</w> <w n="18.7">sacré</w>,</l>
					<l n="19" num="5.3"><w n="19.1">Ce</w> <w n="19.2">symbole</w> <w n="19.3">de</w> <w n="19.4">la</w> <w n="19.5">patrie</w>,</l>
					<l n="20" num="5.4"><w n="20.1">Que</w> <w n="20.2">sanglant</w> <w n="20.3">et</w> <w n="20.4">que</w> <w n="20.5">déchiré</w>.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">Cependant</w> <w n="21.2">je</w> <w n="21.3">fus</w> <w n="21.4">ému</w>, <w n="21.5">certe</w>,</l>
					<l n="22" num="6.2"><w n="22.1">Devant</w> <w n="22.2">nos</w> <w n="22.3">soldats</w> <w n="22.4">de</w> <w n="22.5">vingt</w> <w n="22.6">ans</w>.</l>
					<l n="23" num="6.3"><w n="23.1">Leur</w> <w n="23.2">air</w> <w n="23.3">crâne</w>, <w n="23.4">leur</w> <w n="23.5">pas</w> <w n="23.6">alerte</w></l>
					<l n="24" num="6.4"><w n="24.1">Font</w> <w n="24.2">penser</w> <w n="24.3">à</w> <w n="24.4">de</w> <w n="24.5">meilleurs</w> <w n="24.6">temps</w>.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">Ils</w> <w n="25.2">avaient</w> <w n="25.3">cette</w> <w n="25.4">silhouette</w>,</l>
					<l n="26" num="7.2"><w n="26.1">Ceux</w> <w n="26.2">qui</w> <w n="26.3">triomphaient</w>, <w n="26.4">à</w> <w n="26.5">Valmy</w>,</l>
					<l n="27" num="7.3"><w n="27.1">Le</w> <w n="27.2">chapeau</w> <w n="27.3">sur</w> <w n="27.4">la</w> <w n="27.5">baïonnette</w>,</l>
					<l n="28" num="7.4"><w n="28.1">En</w> <w n="28.2">voyant</w> <w n="28.3">s</w>’<w n="28.4">enfuir</w> <w n="28.5">l</w>’<w n="28.6">ennemi</w> ;</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1">Et</w> <w n="29.2">ces</w> <w n="29.3">gamins</w> <w n="29.4">hier</w> <w n="29.5">à</w> <w n="29.6">l</w>’<w n="29.7">école</w>,</l>
					<l n="30" num="8.2"><w n="30.1">Ces</w> <w n="30.2">officiers</w> <w n="30.3">d</w>’<w n="30.4">âge</w> <w n="30.5">plus</w> <w n="30.6">mûr</w>,</l>
					<l n="31" num="8.3"><w n="31.1">Passeraient</w> <w n="31.2">sur</w> <w n="31.3">le</w> <w n="31.4">pont</w> <w n="31.5">d</w>’<w n="31.6">Arcole</w></l>
					<l n="32" num="8.4"><w n="32.1">Comme</w> <w n="32.2">leurs</w> <w n="32.3">anciens</w>, <w n="32.4">j</w>’<w n="32.5">en</w> <w n="32.6">suis</w> <w n="32.7">sûr</w>.</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1"><w n="33.1">De</w> <w n="33.2">vrais</w> <w n="33.3">Français</w> ! <w n="33.4">Qu</w>’<w n="33.5">ils</w> <w n="33.6">sont</w> <w n="33.7">ingambes</w> !</l>
					<l n="34" num="9.2"><w n="34.1">Ils</w> <w n="34.2">gagneraient</w>, <w n="34.3">un</w> <w n="34.4">contre</w> <w n="34.5">dix</w>,</l>
					<l n="35" num="9.3"><w n="35.1">Des</w> <w n="35.2">batailles</w> <w n="35.3">avec</w> <w n="35.4">leurs</w> <w n="35.5">jambes</w>,</l>
					<l n="36" num="9.4"><w n="36.1">Comme</w> <w n="36.2">a</w> <w n="36.3">dit</w> <w n="36.4">l</w>’<w n="36.5">homme</w> <w n="36.6">d</w>’<w n="36.7">Austerlitz</w>.</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1"><w n="37.1">L</w>’<w n="37.2">arme</w> <w n="37.3">sur</w> <w n="37.4">l</w>’<w n="37.5">épaule</w>, <w n="37.6">par</w> <w n="37.7">quatre</w>,</l>
					<l n="38" num="10.2"><w n="38.1">Légers</w> <w n="38.2">malgré</w> <w n="38.3">le</w> <w n="38.4">poids</w> <w n="38.5">du</w> <w n="38.6">sac</w>,</l>
					<l n="39" num="10.3"><w n="39.1">Ils</w> <w n="39.2">allaient</w>, <w n="39.3">et</w> <w n="39.4">je</w> <w n="39.5">sentais</w> <w n="39.6">battre</w></l>
					<l n="40" num="10.4"><w n="40.1">Mon</w> <w n="40.2">vieux</w> <w n="40.3">cœur</w> <w n="40.4">d</w>’<w n="40.5">un</w> <w n="40.6">joyeux</w> <w n="40.7">tic</w> <w n="40.8">tac</w>.</l>
				</lg>
				<ab type="star">※※※</ab>
				<lg n="11">
					<l n="41" num="11.1"><w n="41.1">Mais</w>, <w n="41.2">brusquement</w>, <w n="41.3">par</w> <w n="41.4">la</w> <w n="41.5">pensée</w></l>
					<l n="42" num="11.2"><w n="42.1">De</w> <w n="42.2">notre</w> <w n="42.3">honte</w> <w n="42.4">d</w>’<w n="42.5">aujourd</w>’<w n="42.6">hui</w>,</l>
					<l n="43" num="11.3"><w n="43.1">Ma</w> <w n="43.2">mémoire</w> <w n="43.3">fut</w> <w n="43.4">traversée</w>,</l>
					<l n="44" num="11.4"><w n="44.1">Et</w> <w n="44.2">les</w> <w n="44.3">beaux</w> <w n="44.4">souvenirs</w> <w n="44.5">ont</w> <w n="44.6">fui</w>.</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1"><w n="45.1">Car</w>, <w n="45.2">ce</w> <w n="45.3">n</w>’<w n="45.4">est</w> <w n="45.5">douteux</w> <w n="45.6">pour</w> <w n="45.7">personne</w>,</l>
					<l n="46" num="12.2"><w n="46.1">L</w>’<w n="46.2">âme</w> <w n="46.3">de</w> <w n="46.4">ces</w> <w n="46.5">pauvres</w> <w n="46.6">soldats</w>,</l>
					<l n="47" num="12.3"><w n="47.1">On</w> <w n="47.2">la</w> <w n="47.3">corrompt</w>, <w n="47.4">on</w> <w n="47.5">l</w>’<w n="47.6">empoisonne</w>,</l>
					<l n="48" num="12.4"><w n="48.1">Sous</w> <w n="48.2">ce</w> <w n="48.3">régime</w> <w n="48.4">de</w> <w n="48.5">Judas</w>.</l>
				</lg>
				<lg n="13">
					<l n="49" num="13.1"><w n="49.1">Ce</w> <w n="49.2">petit</w> <w n="49.3">pioupiou</w> <w n="49.4">dont</w> <w n="49.5">à</w> <w n="49.6">peine</w></l>
					<l n="50" num="13.2"><w n="50.1">La</w> <w n="50.2">barbe</w> <w n="50.3">pousse</w> <w n="50.4">maintenant</w>,</l>
					<l n="51" num="13.3"><w n="51.1">Observez</w> <w n="51.2">le</w> <w n="51.3">regard</w> <w n="51.4">de</w> <w n="51.5">haine</w></l>
					<l n="52" num="13.4"><w n="52.1">Qu</w>’<w n="52.2">il</w> <w n="52.3">jette</w> <w n="52.4">sur</w> <w n="52.5">son</w> <w n="52.6">lieutenant</w>.</l>
				</lg>
				<lg n="14">
					<l n="53" num="14.1"><w n="53.1">Il</w> <w n="53.2">doit</w> <w n="53.3">avoir</w>, <w n="53.4">sous</w> <w n="53.5">sa</w> <w n="53.6">capote</w>,</l>
					<l n="54" num="14.2"><w n="54.1">Quelque</w> <w n="54.2">écrit</w> <w n="54.3">de</w> <w n="54.4">l</w>’<w n="54.5">infâme</w> <w n="54.6">Hervé</w>,</l>
					<l n="55" num="14.3"><w n="55.1">Et</w> <w n="55.2">dans</w> <w n="55.3">sa</w> <w n="55.4">cervelle</w> <w n="55.5">idiote</w>,</l>
					<l n="56" num="14.4">— <w n="56.1">Que</w> <w n="56.2">sait</w>-<w n="56.3">on</w> ? — <w n="56.4">un</w> <w n="56.5">crime</w> <w n="56.6">est</w> <w n="56.7">couvé</w>.</l>
				</lg>
				<lg n="15">
					<l n="57" num="15.1"><w n="57.1">Il</w> <w n="57.2">connaît</w> <w n="57.3">le</w> <w n="57.4">couplet</w> <w n="57.5">horrible</w></l>
					<l n="58" num="15.2"><w n="58.1">Qui</w> <w n="58.2">lui</w> <w n="58.3">conseille</w> <w n="58.4">en</w> <w n="58.5">termes</w> <w n="58.6">brefs</w></l>
					<l n="59" num="15.3"><w n="59.1">De</w> <w n="59.2">prendre</w>, <w n="59.3">à</w> <w n="59.4">la</w> <w n="59.5">guerre</w>, <w n="59.6">pour</w> <w n="59.7">cible</w>,</l>
					<l n="60" num="15.4"><w n="60.1">Non</w> <w n="60.2">les</w> <w n="60.3">ennemis</w>, <w n="60.4">mais</w> <w n="60.5">ses</w> <w n="60.6">chefs</w> ;</l>
				</lg>
				<lg n="16">
					<l n="61" num="16.1"><w n="61.1">Et</w>, <w n="61.2">devant</w> <w n="61.3">son</w> <w n="61.4">œil</w> <w n="61.5">faux</w> <w n="61.6">et</w> <w n="61.7">terne</w>,</l>
					<l n="62" num="16.2"><w n="62.1">J</w>’<w n="62.2">ai</w> <w n="62.3">bien</w> <w n="62.4">peur</w> <w n="62.5">que</w> <w n="62.6">ce</w> <w n="62.7">fantassin</w></l>
					<l n="63" num="16.3"><w n="63.1">Ne</w> <w n="63.2">garde</w>, <w n="63.3">au</w> <w n="63.4">fond</w> <w n="63.5">de</w> <w n="63.6">sa</w> <w n="63.7">giberne</w>,</l>
					<l n="64" num="16.4"><w n="64.1">Une</w> <w n="64.2">cartouche</w> <w n="64.3">d</w>’<w n="64.4">assassin</w>.</l>
				</lg>
				<lg n="17">
					<l n="65" num="17.1"><w n="65.1">Quant</w> <w n="65.2">aux</w> <w n="65.3">chefs</w>… <w n="65.4">Oh</w> ! <w n="65.5">l</w>’<w n="65.6">ignominie</w> !</l>
					<l n="66" num="17.2"><w n="66.1">Oh</w> ! <w n="66.2">j</w>’<w n="66.3">entends</w> <w n="66.4">la</w> <w n="66.5">France</w> <w n="66.6">pleurer</w> !…</l>
					<l n="67" num="17.3"><w n="67.1">Sous</w> <w n="67.2">cette</w> <w n="67.3">basse</w> <w n="67.4">tyrannie</w></l>
					<l n="68" num="17.4"><w n="68.1">On</w> <w n="68.2">voudrait</w> <w n="68.3">les</w> <w n="68.4">déshonorer</w>.</l>
				</lg>
				<lg n="18">
					<l n="69" num="18.1"><w n="69.1">Peut</w>-<w n="69.2">être</w>, — <w n="69.3">ô</w> <w n="69.4">doute</w> <w n="69.5">affreux</w> ! — <w n="69.6">peut</w>-<w n="69.7">être</w></l>
					<l n="70" num="18.2"><w n="70.1">Ce</w> <w n="70.2">capitaine</w> <w n="70.3">au</w> <w n="70.4">mâle</w> <w n="70.5">aspect</w>,</l>
					<l n="71" num="18.3"><w n="71.1">Est</w> <w n="71.2">un</w> <w n="71.3">délateur</w>, <w n="71.4">est</w> <w n="71.5">un</w> <w n="71.6">traître</w>,</l>
					<l n="72" num="18.4"><w n="72.1">Est</w> <w n="72.2">l</w>’<w n="72.3">espion</w> <w n="72.4">le</w> <w n="72.5">plus</w> <w n="72.6">abject</w>.</l>
				</lg>
				<lg n="19">
					<l n="73" num="19.1"><w n="73.1">Sachant</w> <w n="73.2">que</w>, <w n="73.3">sous</w> <w n="73.4">la</w> <w n="73.5">République</w>,</l>
					<l n="74" num="19.2"><w n="74.1">On</w> <w n="74.2">n</w>’<w n="74.3">a</w> <w n="74.4">rien</w> <w n="74.5">que</w> <w n="74.6">par</w> <w n="74.7">ruse</w> <w n="74.8">et</w> <w n="74.9">dol</w>,</l>
					<l n="75" num="19.3"><w n="75.1">Il</w> <w n="75.2">tient</w> <w n="75.3">son</w> <w n="75.4">bijou</w> <w n="75.5">maçonnique</w></l>
					<l n="76" num="19.4"><w n="76.1">Bien</w> <w n="76.2">caché</w> <w n="76.3">sous</w> <w n="76.4">son</w> <w n="76.5">hausse</w>-<w n="76.6">col</w>.</l>
				</lg>
				<lg n="20">
					<l n="77" num="20.1"><w n="77.1">Mais</w> <w n="77.2">pour</w> <w n="77.3">obtenir</w> <w n="77.4">tous</w> <w n="77.5">ses</w> <w n="77.6">grades</w></l>
					<l n="78" num="20.2"><w n="78.1">Et</w> <w n="78.2">ses</w> <w n="78.3">galons</w> <w n="78.4">d</w>’<w n="78.5">or</w> <w n="78.6">sur</w> <w n="78.7">drap</w> <w n="78.8">fin</w>,</l>
					<l n="79" num="20.3"><w n="79.1">Il</w> <w n="79.2">a</w> <w n="79.3">trahi</w> <w n="79.4">ses</w> <w n="79.5">camarades</w></l>
					<l n="80" num="20.4"><w n="80.1">Qu</w>’<w n="80.2">il</w> <w n="80.3">réduit</w> <w n="80.4">à</w> <w n="80.5">mourir</w> <w n="80.6">de</w> <w n="80.7">faim</w> ;</l>
				</lg>
				<lg n="21">
					<l n="81" num="21.1"><w n="81.1">Et</w> <w n="81.2">sa</w> <w n="81.3">croix</w> <w n="81.4">d</w>’<w n="81.5">honneur</w> <w n="81.6">fut</w> <w n="81.7">conquise</w>,</l>
					<l n="82" num="21.2"><w n="82.1">Quand</w> <w n="82.2">il</w> <w n="82.3">dénonçait</w> <w n="82.4">en</w> <w n="82.5">haut</w> <w n="82.6">lieu</w></l>
					<l n="83" num="21.3"><w n="83.1">Son</w> <w n="83.2">colonel</w> <w n="83.3">à</w> <w n="83.4">tête</w> <w n="83.5">grise</w></l>
					<l n="84" num="21.4"><w n="84.1">Qui</w> <w n="84.2">se</w> <w n="84.3">permet</w> <w n="84.4">de</w> <w n="84.5">prier</w> <w n="84.6">Dieu</w>.</l>
				</lg>
				<ab type="star">※※※</ab>
				<lg n="22">
					<l n="85" num="22.1"><w n="85.1">Oh</w> ! <w n="85.2">l</w>’<w n="85.3">odeur</w> <w n="85.4">de</w> <w n="85.5">mort</w> <w n="85.6">qu</w>’<w n="85.7">on</w> <w n="85.8">renifle</w></l>
					<l n="86" num="22.2"><w n="86.1">Et</w> <w n="86.2">qui</w> <w n="86.3">rôde</w> <w n="86.4">autour</w> <w n="86.5">du</w> <w n="86.6">drapeau</w>,</l>
					<l n="87" num="22.3"><w n="87.1">Sous</w> <w n="87.2">cet</w> <w n="87.3">André</w>, <w n="87.4">l</w>’<w n="87.5">homme</w> <w n="87.6">à</w> <w n="87.7">la</w> <w n="87.8">gifle</w>,</l>
					<l n="88" num="22.4"><w n="88.1">Sous</w> <w n="88.2">ce</w> <w n="88.3">Loubet</w>, <w n="88.4">l</w>’<w n="88.5">homme</w> <w n="88.6">au</w> <w n="88.7">chapeau</w> !</l>
				</lg>
				<lg n="23">
					<l n="89" num="23.1"><w n="89.1">O</w> <w n="89.2">malheureuse</w> <w n="89.3">et</w> <w n="89.4">chère</w> <w n="89.5">armée</w>,</l>
					<l n="90" num="23.2"><w n="90.1">Qui</w> <w n="90.2">devais</w> <w n="90.3">nous</w> <w n="90.4">reconquérir</w></l>
					<l n="91" num="23.3"><w n="91.1">La</w> <w n="91.2">vieille</w> <w n="91.3">frontière</w> <w n="91.4">entamée</w>,</l>
					<l n="92" num="23.4"><w n="92.1">C</w>’<w n="92.2">est</w> <w n="92.3">donc</w> <w n="92.4">vrai</w> <w n="92.5">que</w> <w n="92.6">tu</w> <w n="92.7">vas</w> <w n="92.8">périr</w> !</l>
				</lg>
				<lg n="24">
					<l n="93" num="24.1"><w n="93.1">Récemment</w>, <w n="93.2">ils</w> <w n="93.3">ont</w> <w n="93.4">mis</w> <w n="93.5">tes</w> <w n="93.6">armes</w></l>
					<l n="94" num="24.2"><w n="94.1">Au</w> <w n="94.2">service</w> <w n="94.3">de</w> <w n="94.4">leurs</w> <w n="94.5">mouchards</w>,</l>
					<l n="95" num="24.3"><w n="95.1">Et</w> <w n="95.2">contre</w> <w n="95.3">des</w> <w n="95.4">vierges</w> <w n="95.5">en</w> <w n="95.6">larmes</w>,</l>
					<l n="96" num="24.4"><w n="96.1">Contre</w> <w n="96.2">de</w> <w n="96.3">purs</w> <w n="96.4">et</w> <w n="96.5">doux</w> <w n="96.6">vieillards</w>.</l>
				</lg>
				<lg n="25">
					<l n="97" num="25.1"><w n="97.1">Mais</w> <w n="97.2">alors</w> <w n="97.3">ton</w> <w n="97.4">frisson</w> <w n="97.5">de</w> <w n="97.6">honte</w>,</l>
					<l n="98" num="25.2"><w n="98.1">L</w>’<w n="98.2">affreux</w> <w n="98.3">Combes</w> <w n="98.4">l</w>’<w n="98.5">a</w> <w n="98.6">remarqué</w>,</l>
					<l n="99" num="25.3"><w n="99.1">Et</w> <w n="99.2">tu</w> <w n="99.3">vas</w> <w n="99.4">périr</w> ! <w n="99.5">Il</w> <w n="99.6">y</w> <w n="99.7">compte</w>,</l>
					<l n="100" num="25.4"><w n="100.1">Dans</w> <w n="100.2">sa</w> <w n="100.3">rage</w> <w n="100.4">de</w> <w n="100.5">défroqué</w>.</l>
				</lg>
				<lg n="26">
					<l n="101" num="26.1"><w n="101.1">Le</w> <w n="101.2">voudras</w>-<w n="101.3">tu</w> ?… <w n="101.4">Moi</w>, <w n="101.5">dans</w> <w n="101.6">la</w> <w n="101.7">rue</w>,</l>
					<l n="102" num="26.2"><w n="102.1">Lorsque</w> <w n="102.2">passa</w> <w n="102.3">ce</w> <w n="102.4">régiment</w>,</l>
					<l n="103" num="26.3"><w n="103.1">J</w>’<w n="103.2">ai</w> <w n="103.3">salué</w> <w n="103.4">comme</w> <w n="103.5">on</w> <w n="103.6">salue</w></l>
					<l n="104" num="26.4"><w n="104.1">Un</w> <w n="104.2">cortège</w> <w n="104.3">d</w>’<w n="104.4">enterrement</w>.</l>
				</lg>
			</div></body></text></TEI>