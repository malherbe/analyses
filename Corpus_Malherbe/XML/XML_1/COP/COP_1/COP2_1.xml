<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">PROMENADES ET INTÉRIEURS</title>
				<title type="medium">Édition électronique</title>
				<author key="COP">
					<name>
						<forename>François</forename>
						<surname>COPPÉE</surname>
					</name>
					<date from="1842" to="1908">1842-1908</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1057 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">COP_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">PROMENADES ET INTÉRIEURS</title>
						<author>François Coppée</author>
					</titleStmt>
					<publicationStmt>
						<publisher>ebooksgratuits.com</publisher>
						<idno type="URL">http://www.ebooksgratuits.com/</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Promenades et intérieurs</title>
						<author>François Coppée</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie Lemerre</publisher>
							<date when="1920">1920</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1872">1872</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La date de création est celle donnée par l’Académie Française pour le recueil "Les Humbles"</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">II</head><div type="poem" key="COP2">
					<head type="main">Mon père</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Tenez</w>, <w n="1.2">lecteur</w> ! – <w n="1.3">souvent</w>, <w n="1.4">tout</w> <w n="1.5">seul</w>, <w n="1.6">je</w> <w n="1.7">me</w> <w n="1.8">promène</w></l>
						<l n="2" num="1.2"><w n="2.1">Au</w> <w n="2.2">lieu</w> <w n="2.3">qui</w> <w n="2.4">fut</w> <w n="2.5">jadis</w> <w n="2.6">la</w> <w n="2.7">barrière</w> <w n="2.8">du</w> <w n="2.9">Maine</w>.</l>
						<l n="3" num="1.3"><w n="3.1">C</w>’<w n="3.2">est</w> <w n="3.3">laid</w>, <w n="3.4">surtout</w> <w n="3.5">depuis</w> <w n="3.6">le</w> <w n="3.7">siège</w> <w n="3.8">de</w> <w n="3.9">Paris</w>.</l>
						<l n="4" num="1.4"><w n="4.1">On</w> <w n="4.2">a</w> <w n="4.3">planté</w> <w n="4.4">d</w>’<w n="4.5">affreux</w> <w n="4.6">arbustes</w> <w n="4.7">rabougris</w></l>
						<l n="5" num="1.5"><w n="5.1">Sur</w> <w n="5.2">ces</w> <w n="5.3">longs</w> <w n="5.4">boulevards</w> <w n="5.5">où</w> <w n="5.6">naguère</w> <w n="5.7">des</w> <w n="5.8">ormes</w></l>
						<l n="6" num="1.6"><w n="6.1">De</w> <w n="6.2">deux</w> <w n="6.3">cents</w> <w n="6.4">ans</w> <w n="6.5">croisaient</w> <w n="6.6">leurs</w> <w n="6.7">ramures</w> <w n="6.8">énormes</w>.</l>
						<l n="7" num="1.7"><w n="7.1">Le</w> <w n="7.2">mur</w> <w n="7.3">d</w>’<w n="7.4">octroi</w> <w n="7.5">n</w>’<w n="7.6">est</w> <w n="7.7">plus</w> ; <w n="7.8">le</w> <w n="7.9">quartier</w> <w n="7.10">se</w> <w n="7.11">bâtit</w>.</l>
						<l n="8" num="1.8"><w n="8.1">Mais</w> <w n="8.2">c</w>’<w n="8.3">est</w> <w n="8.4">là</w> <w n="8.5">que</w> <w n="8.6">jadis</w>, <w n="8.7">quand</w> <w n="8.8">j</w>’<w n="8.9">étais</w> <w n="8.10">tout</w> <w n="8.11">petit</w>,</l>
						<l n="9" num="1.9"><w n="9.1">Mon</w> <w n="9.2">père</w> <w n="9.3">me</w> <w n="9.4">menait</w>, <w n="9.5">enfant</w> <w n="9.6">faible</w> <w n="9.7">et</w> <w n="9.8">malade</w>,</l>
						<l n="10" num="1.10"><w n="10.1">Par</w> <w n="10.2">les</w> <w n="10.3">couchants</w> <w n="10.4">d</w>’<w n="10.5">été</w> <w n="10.6">faire</w> <w n="10.7">une</w> <w n="10.8">promenade</w>.</l>
						<l n="11" num="1.11"><w n="11.1">C</w>’<w n="11.2">est</w> <w n="11.3">sur</w> <w n="11.4">ces</w> <w n="11.5">boulevards</w> <w n="11.6">déserts</w>, <w n="11.7">c</w>’<w n="11.8">est</w> <w n="11.9">dans</w> <w n="11.10">ce</w> <w n="11.11">lieu</w></l>
						<l n="12" num="1.12"><w n="12.1">Que</w> <w n="12.2">cet</w> <w n="12.3">homme</w> <w n="12.4">de</w> <w n="12.5">bien</w>, <w n="12.6">pur</w>, <w n="12.7">simple</w> <w n="12.8">et</w> <w n="12.9">craignant</w> <w n="12.10">Dieu</w>,</l>
						<l n="13" num="1.13"><w n="13.1">Qui</w> <w n="13.2">fut</w> <w n="13.3">bon</w> <w n="13.4">comme</w> <w n="13.5">un</w> <w n="13.6">saint</w>, <w n="13.7">naïf</w> <w n="13.8">comme</w> <w n="13.9">un</w> <w n="13.10">poète</w>,</l>
						<l n="14" num="1.14"><w n="14.1">Et</w> <w n="14.2">qui</w>, <w n="14.3">bien</w> <w n="14.4">que</w> <w n="14.5">très</w> <w n="14.6">pauvre</w>, <w n="14.7">eut</w> <w n="14.8">toujours</w> <w n="14.9">l</w>’<w n="14.10">âme</w> <w n="14.11">en</w> <w n="14.12">fête</w>,</l>
						<l n="15" num="1.15"><w n="15.1">Au</w> <w n="15.2">fond</w> <w n="15.3">d</w>’<w n="15.4">un</w> <w n="15.5">bureau</w> <w n="15.6">sombre</w> <w n="15.7">après</w> <w n="15.8">avoir</w> <w n="15.9">passé</w></l>
						<l n="16" num="1.16"><w n="16.1">Tout</w> <w n="16.2">le</w> <w n="16.3">jour</w>, <w n="16.4">se</w> <w n="16.5">croyant</w> <w n="16.6">assez</w> <w n="16.7">récompensé</w></l>
						<l n="17" num="1.17"><w n="17.1">Par</w> <w n="17.2">la</w> <w n="17.3">douce</w> <w n="17.4">chaleur</w> <w n="17.5">qu</w>’<w n="17.6">au</w> <w n="17.7">cœur</w> <w n="17.8">nous</w> <w n="17.9">communique</w></l>
						<l n="18" num="1.18"><w n="18.1">La</w> <w n="18.2">main</w> <w n="18.3">d</w>’<w n="18.4">un</w> <w n="18.5">dernier</w>-<w n="18.6">né</w>, <w n="18.7">la</w> <w n="18.8">main</w> <w n="18.9">d</w>’<w n="18.10">un</w> <w n="18.11">fils</w> <w n="18.12">unique</w>,</l>
						<l n="19" num="1.19"><w n="19.1">C</w>’<w n="19.2">est</w> <w n="19.3">là</w> <w n="19.4">qu</w>’<w n="19.5">il</w> <w n="19.6">me</w> <w n="19.7">menait</w>. <w n="19.8">Tous</w> <w n="19.9">deux</w> <w n="19.10">nous</w> <w n="19.11">allions</w> <w n="19.12">voir</w></l>
						<l n="20" num="1.20"><w n="20.1">Les</w> <w n="20.2">longs</w> <w n="20.3">troupeaux</w> <w n="20.4">de</w> <w n="20.5">bœufs</w> <w n="20.6">marchant</w> <w n="20.7">vers</w> <w n="20.8">l</w>’<w n="20.9">abattoir</w>,</l>
						<l n="21" num="1.21"><w n="21.1">Et</w> <w n="21.2">quand</w> <w n="21.3">mes</w> <w n="21.4">petits</w> <w n="21.5">pieds</w> <w n="21.6">étaient</w> <w n="21.7">assez</w> <w n="21.8">solides</w>,</l>
						<l n="22" num="1.22"><w n="22.1">Nous</w> <w n="22.2">poussions</w> <w n="22.3">quelquefois</w> <w n="22.4">jusques</w> <w n="22.5">aux</w> <w n="22.6">Invalides</w>,</l>
						<l n="23" num="1.23"><w n="23.1">Où</w>, <w n="23.2">mêlés</w> <w n="23.3">aux</w> <w n="23.4">badauds</w> <w n="23.5">descendus</w> <w n="23.6">des</w> <w n="23.7">faubourgs</w>,</l>
						<l n="24" num="1.24"><w n="24.1">Nous</w> <w n="24.2">suivions</w> <w n="24.3">la</w> <w n="24.4">retraite</w> <w n="24.5">et</w> <w n="24.6">les</w> <w n="24.7">petits</w> <w n="24.8">tambours</w>.</l>
						<l n="25" num="1.25"><w n="25.1">Et</w> <w n="25.2">puis</w> <w n="25.3">enfin</w>, <w n="25.4">à</w> <w n="25.5">l</w>’<w n="25.6">heure</w> <w n="25.7">où</w> <w n="25.8">la</w> <w n="25.9">lune</w> <w n="25.10">se</w> <w n="25.11">lève</w>,</l>
						<l n="26" num="1.26"><w n="26.1">Nous</w> <w n="26.2">prenions</w> <w n="26.3">pour</w> <w n="26.4">rentrer</w> <w n="26.5">la</w> <w n="26.6">route</w> <w n="26.7">la</w> <w n="26.8">plus</w> <w n="26.9">brève</w> ;</l>
						<l n="27" num="1.27"><w n="27.1">On</w> <w n="27.2">montait</w> <w n="27.3">au</w> <w n="27.4">cinquième</w> <w n="27.5">étage</w>, <w n="27.6">lentement</w> ;</l>
						<l n="28" num="1.28"><w n="28.1">Et</w> <w n="28.2">j</w>’<w n="28.3">embrassais</w> <w n="28.4">alors</w> <w n="28.5">mes</w> <w n="28.6">trois</w> <w n="28.7">sœurs</w> <w n="28.8">et</w> <w n="28.9">maman</w>,</l>
						<l n="29" num="1.29"><w n="29.1">Assises</w> <w n="29.2">et</w> <w n="29.3">cousant</w> <w n="29.4">auprès</w> <w n="29.5">d</w>’<w n="29.6">une</w> <w n="29.7">bougie</w>.</l>
						<l n="30" num="1.30">– <w n="30.1">Eh</w> <w n="30.2">bien</w>, <w n="30.3">quand</w> <w n="30.4">m</w>’<w n="30.5">abandonne</w> <w n="30.6">un</w> <w n="30.7">instant</w> <w n="30.8">l</w>’<w n="30.9">énergie</w>,</l>
						<l n="31" num="1.31"><w n="31.1">Quand</w> <w n="31.2">m</w>’<w n="31.3">accable</w> <w n="31.4">par</w> <w n="31.5">trop</w> <w n="31.6">le</w> <w n="31.7">spleen</w> <w n="31.8">décourageant</w>,</l>
						<l n="32" num="1.32"><w n="32.1">Je</w> <w n="32.2">retourne</w>, <w n="32.3">tout</w> <w n="32.4">seul</w>, <w n="32.5">à</w> <w n="32.6">l</w>’<w n="32.7">heure</w> <w n="32.8">du</w> <w n="32.9">couchant</w>,</l>
						<l n="33" num="1.33"><w n="33.1">Dans</w> <w n="33.2">ce</w> <w n="33.3">quartier</w> <w n="33.4">paisible</w> <w n="33.5">où</w> <w n="33.6">me</w> <w n="33.7">menait</w> <w n="33.8">mon</w> <w n="33.9">père</w> ;</l>
						<l n="34" num="1.34"><w n="34.1">Et</w> <w n="34.2">du</w> <w n="34.3">cher</w> <w n="34.4">souvenir</w> <w n="34.5">toujours</w> <w n="34.6">le</w> <w n="34.7">charme</w> <w n="34.8">opère</w>.</l>
						<l n="35" num="1.35"><w n="35.1">Je</w> <w n="35.2">songe</w> <w n="35.3">à</w> <w n="35.4">ce</w> <w n="35.5">qu</w>’<w n="35.6">il</w> <w n="35.7">fit</w>, <w n="35.8">cet</w> <w n="35.9">homme</w> <w n="35.10">de</w> <w n="35.11">devoir</w>,</l>
						<l n="36" num="1.36"><w n="36.1">Ce</w> <w n="36.2">pauvre</w> <w n="36.3">fier</w> <w n="36.4">et</w> <w n="36.5">pur</w>, <w n="36.6">à</w> <w n="36.7">ce</w> <w n="36.8">qu</w>’<w n="36.9">il</w> <w n="36.10">dut</w> <w n="36.11">avoir</w></l>
						<l n="37" num="1.37"><w n="37.1">De</w> <w n="37.2">résignation</w> <w n="37.3">patiente</w> <w n="37.4">et</w> <w n="37.5">chrétienne</w></l>
						<l n="38" num="1.38"><w n="38.1">Pour</w> <w n="38.2">gagner</w> <w n="38.3">notre</w> <w n="38.4">pain</w>, <w n="38.5">tâche</w> <w n="38.6">quotidienne</w>,</l>
						<l n="39" num="1.39"><w n="39.1">Et</w> <w n="39.2">se</w> <w n="39.3">priver</w> <w n="39.4">de</w> <w n="39.5">tout</w>, <w n="39.6">sans</w> <w n="39.7">se</w> <w n="39.8">plaindre</w> <w n="39.9">jamais</w>.</l>
						<l n="40" num="1.40">– <w n="40.1">Au</w> <w n="40.2">chagrin</w> <w n="40.3">qui</w> <w n="40.4">me</w> <w n="40.5">frappe</w> <w n="40.6">alors</w> <w n="40.7">je</w> <w n="40.8">me</w> <w n="40.9">soumets</w>,</l>
						<l n="41" num="1.41"><w n="41.1">Et</w> <w n="41.2">je</w> <w n="41.3">sens</w> <w n="41.4">remonter</w> <w n="41.5">à</w> <w n="41.6">mes</w> <w n="41.7">lèvres</w> <w n="41.8">surprises</w></l>
						<l n="42" num="1.42"><w n="42.1">Les</w> <w n="42.2">prières</w> <w n="42.3">qu</w>’<w n="42.4">il</w> <w n="42.5">m</w>’<w n="42.6">a</w> <w n="42.7">dans</w> <w n="42.8">mon</w> <w n="42.9">enfance</w> <w n="42.10">apprises</w>.</l>
					</lg>
				</div></body></text></TEI>