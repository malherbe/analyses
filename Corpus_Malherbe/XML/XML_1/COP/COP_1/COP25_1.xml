<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">PROMENADES ET INTÉRIEURS</title>
				<title type="medium">Édition électronique</title>
				<author key="COP">
					<name>
						<forename>François</forename>
						<surname>COPPÉE</surname>
					</name>
					<date from="1842" to="1908">1842-1908</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1057 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">COP_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">PROMENADES ET INTÉRIEURS</title>
						<author>François Coppée</author>
					</titleStmt>
					<publicationStmt>
						<publisher>ebooksgratuits.com</publisher>
						<idno type="URL">http://www.ebooksgratuits.com/</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Promenades et intérieurs</title>
						<author>François Coppée</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie Lemerre</publisher>
							<date when="1920">1920</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1872">1872</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La date de création est celle donnée par l’Académie Française pour le recueil "Les Humbles"</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">III</head><div type="poem" key="COP25">
					<head type="main">Croquis de banlieue</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">L</w>’<w n="1.2">homme</w>, <w n="1.3">en</w> <w n="1.4">manches</w> <w n="1.5">de</w> <w n="1.6">veste</w>, <w n="1.7">et</w> <w n="1.8">sous</w> <w n="1.9">son</w> <w n="1.10">chapeau</w> <w n="1.11">noir</w>,</l>
						<l n="2" num="1.2"><w n="2.1">À</w> <w n="2.2">cause</w> <w n="2.3">du</w> <w n="2.4">soleil</w>, <w n="2.5">ayant</w> <w n="2.6">mis</w> <w n="2.7">son</w> <w n="2.8">mouchoir</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Tire</w> <w n="3.2">gaillardement</w> <w n="3.3">la</w> <w n="3.4">petite</w> <w n="3.5">voiture</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Pour</w> <w n="4.2">faire</w> <w n="4.3">prendre</w> <w n="4.4">l</w>’<w n="4.5">air</w> <w n="4.6">à</w> <w n="4.7">sa</w> <w n="4.8">progéniture</w>,</l>
						<l n="5" num="1.5"><w n="5.1">Deux</w> <w n="5.2">bébés</w>, <w n="5.3">l</w>’<w n="5.4">un</w> <w n="5.5">qui</w> <w n="5.6">dort</w>, <w n="5.7">l</w>’<w n="5.8">autre</w> <w n="5.9">suçant</w> <w n="5.10">son</w> <w n="5.11">doigt</w>.</l>
						<l n="6" num="1.6"><w n="6.1">La</w> <w n="6.2">femme</w> <w n="6.3">suit</w> <w n="6.4">et</w> <w n="6.5">pousse</w>, <w n="6.6">ainsi</w> <w n="6.7">qu</w>’<w n="6.8">elle</w> <w n="6.9">le</w> <w n="6.10">doit</w>,</l>
						<l n="7" num="1.7"><w n="7.1">Très</w> <w n="7.2">lasse</w>, <w n="7.3">et</w> <w n="7.4">sous</w> <w n="7.5">son</w> <w n="7.6">bras</w> <w n="7.7">portant</w> <w n="7.8">la</w> <w n="7.9">redingote</w> ;</l>
						<l n="8" num="1.8"><w n="8.1">Et</w> <w n="8.2">l</w>’<w n="8.3">on</w> <w n="8.4">s</w>’<w n="8.5">en</w> <w n="8.6">va</w> <w n="8.7">dîner</w> <w n="8.8">dans</w> <w n="8.9">une</w> <w n="8.10">humble</w> <w n="8.11">gargote</w></l>
						<l n="9" num="1.9"><w n="9.1">Où</w> <w n="9.2">sur</w> <w n="9.3">le</w> <w n="9.4">mur</w> <w n="9.5">est</w> <w n="9.6">peint</w> – <w n="9.7">vous</w> <w n="9.8">savez</w> ? <w n="9.9">à</w> <w n="9.10">Clamart</w> ! –</l>
						<l n="10" num="1.10"><w n="10.1">Un</w> <w n="10.2">lapin</w> <w n="10.3">mort</w>, <w n="10.4">avec</w> <w n="10.5">trois</w> <w n="10.6">billes</w> <w n="10.7">de</w> <w n="10.8">billard</w>.</l>
					</lg>
				</div></body></text></TEI>