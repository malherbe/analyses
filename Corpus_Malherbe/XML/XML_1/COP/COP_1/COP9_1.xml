<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">PROMENADES ET INTÉRIEURS</title>
				<title type="medium">Édition électronique</title>
				<author key="COP">
					<name>
						<forename>François</forename>
						<surname>COPPÉE</surname>
					</name>
					<date from="1842" to="1908">1842-1908</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1057 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">COP_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">PROMENADES ET INTÉRIEURS</title>
						<author>François Coppée</author>
					</titleStmt>
					<publicationStmt>
						<publisher>ebooksgratuits.com</publisher>
						<idno type="URL">http://www.ebooksgratuits.com/</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Promenades et intérieurs</title>
						<author>François Coppée</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie Lemerre</publisher>
							<date when="1920">1920</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1872">1872</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La date de création est celle donnée par l’Académie Française pour le recueil "Les Humbles"</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">II</head><div type="poem" key="COP9">
					<head type="main">La cueillette des cerises</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Espiègle</w> ! <w n="1.2">j</w>’<w n="1.3">ai</w> <w n="1.4">bien</w> <w n="1.5">vu</w> <w n="1.6">tout</w> <w n="1.7">ce</w> <w n="1.8">que</w> <w n="1.9">vous</w> <w n="1.10">faisiez</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Ce</w> <w n="2.2">matin</w>, <w n="2.3">dans</w> <w n="2.4">le</w> <w n="2.5">champ</w> <w n="2.6">planté</w> <w n="2.7">de</w> <w n="2.8">cerisiers</w></l>
						<l n="3" num="1.3"><w n="3.1">Où</w> <w n="3.2">seule</w> <w n="3.3">vous</w> <w n="3.4">étiez</w>, <w n="3.5">nu</w>-<w n="3.6">tête</w>, <w n="3.7">en</w> <w n="3.8">robe</w> <w n="3.9">blanche</w>.</l>
						<l n="4" num="1.4"><w n="4.1">Caché</w> <w n="4.2">par</w> <w n="4.3">le</w> <w n="4.4">taillis</w>, <w n="4.5">j</w>’<w n="4.6">observais</w>. <w n="4.7">Une</w> <w n="4.8">branche</w>,</l>
						<l n="5" num="1.5"><w n="5.1">Lourde</w> <w n="5.2">sous</w> <w n="5.3">les</w> <w n="5.4">fruits</w> <w n="5.5">mûrs</w>, <w n="5.6">vous</w> <w n="5.7">barrait</w> <w n="5.8">le</w> <w n="5.9">chemin</w></l>
						<l n="6" num="1.6"><w n="6.1">Et</w> <w n="6.2">se</w> <w n="6.3">trouvait</w> <w n="6.4">à</w> <w n="6.5">la</w> <w n="6.6">hauteur</w> <w n="6.7">de</w> <w n="6.8">votre</w> <w n="6.9">main</w>.</l>
						<l n="7" num="1.7"><w n="7.1">Or</w>, <w n="7.2">vous</w> <w n="7.3">avez</w> <w n="7.4">cueilli</w> <w n="7.5">des</w> <w n="7.6">cerises</w> <w n="7.7">vermeilles</w>,</l>
						<l n="8" num="1.8"><w n="8.1">Coquette</w> ! <w n="8.2">et</w> <w n="8.3">les</w> <w n="8.4">avez</w> <w n="8.5">mises</w> <w n="8.6">à</w> <w n="8.7">vos</w> <w n="8.8">oreilles</w>,</l>
						<l n="9" num="1.9"><w n="9.1">Tandis</w> <w n="9.2">qu</w>’<w n="9.3">un</w> <w n="9.4">vent</w> <w n="9.5">léger</w> <w n="9.6">dans</w> <w n="9.7">vos</w> <w n="9.8">boucles</w> <w n="9.9">jouait</w>.</l>
						<l n="10" num="1.10"><w n="10.1">Alors</w>, <w n="10.2">vous</w> <w n="10.3">asseyant</w> <w n="10.4">pour</w> <w n="10.5">cueillir</w> <w n="10.6">un</w> <w n="10.7">bleuet</w></l>
						<l n="11" num="1.11"><w n="11.1">Dans</w> <w n="11.2">l</w>’<w n="11.3">herbe</w>, <w n="11.4">et</w> <w n="11.5">puis</w> <w n="11.6">un</w> <w n="11.7">autre</w>, <w n="11.8">et</w> <w n="11.9">puis</w> <w n="11.10">un</w> <w n="11.11">autre</w> <w n="11.12">encore</w>,</l>
						<l n="12" num="1.12"><w n="12.1">Vous</w> <w n="12.2">les</w> <w n="12.3">avez</w> <w n="12.4">piqués</w> <w n="12.5">dans</w> <w n="12.6">vos</w> <w n="12.7">cheveux</w> <w n="12.8">d</w>’<w n="12.9">aurore</w> ;</l>
						<l n="13" num="1.13"><w n="13.1">Et</w>, <w n="13.2">les</w> <w n="13.3">bras</w> <w n="13.4">recourbés</w> <w n="13.5">sur</w> <w n="13.6">votre</w> <w n="13.7">front</w> <w n="13.8">fleuri</w>,</l>
						<l n="14" num="1.14"><w n="14.1">Assise</w> <w n="14.2">dans</w> <w n="14.3">le</w> <w n="14.4">vert</w> <w n="14.5">gazon</w>, <w n="14.6">vous</w> <w n="14.7">avez</w> <w n="14.8">ri</w> ;</l>
						<l n="15" num="1.15"><w n="15.1">Et</w> <w n="15.2">vos</w> <w n="15.3">joyeuses</w> <w n="15.4">dents</w> <w n="15.5">jetaient</w> <w n="15.6">une</w> <w n="15.7">étincelle</w>.</l>
						<l n="16" num="1.16"><w n="16.1">Mais</w> <w n="16.2">pendant</w> <w n="16.3">ce</w> <w n="16.4">temps</w>-<w n="16.5">là</w>, <w n="16.6">ma</w> <w n="16.7">belle</w> <w n="16.8">demoiselle</w>,</l>
						<l n="17" num="1.17"><w n="17.1">Un</w> <w n="17.2">seul</w> <w n="17.3">témoin</w>, <w n="17.4">qui</w> <w n="17.5">vous</w> <w n="17.6">gardera</w> <w n="17.7">le</w> <w n="17.8">secret</w>,</l>
						<l n="18" num="1.18"><w n="18.1">Tout</w> <w n="18.2">heureux</w> <w n="18.3">de</w> <w n="18.4">vous</w> <w n="18.5">voir</w> <w n="18.6">heureuse</w>, <w n="18.7">comparait</w>,</l>
						<l n="19" num="1.19"><w n="19.1">Sur</w> <w n="19.2">votre</w> <w n="19.3">frais</w> <w n="19.4">visage</w> <w n="19.5">animé</w> <w n="19.6">par</w> <w n="19.7">les</w> <w n="19.8">brises</w>,</l>
						<l n="20" num="1.20"><w n="20.1">Vos</w> <w n="20.2">regards</w> <w n="20.3">aux</w> <w n="20.4">bleuets</w>, <w n="20.5">vos</w> <w n="20.6">lèvres</w> <w n="20.7">aux</w> <w n="20.8">cerises</w>.</l>
					</lg>
				</div></body></text></TEI>