<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE RELIQUAIRE</title>
				<title type="medium">Édition électronique</title>
				<author key="COP">
					<name>
						<forename>François</forename>
						<surname>COPPÉE</surname>
					</name>
					<date from="1842" to="1908">1842-1908</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>421 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2019</date>
				<idno type="local">COP_7</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE RELIQUAIRE</title>
						<author>François Coppée</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/francoiscoppeelereliquaire.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition papier d’origine.</p>
					</sourceDesc>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title type="main">Œuvres complètes</title>
								<author>François Coppée</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Librairie ALEXANDRE HOUSSIAUX</publisher>
									<date when="1885">1885</date>
								</imprint>
								<biblScope unit="tome">Poésie, tome 1</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1866">1866</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-05-25" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2019-05-25" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="COP190">
				<head type="main">ADAGIO</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">La</w> <w n="1.2">rue</w> <w n="1.3">était</w> <w n="1.4">déserte</w> <w n="1.5">et</w> <w n="1.6">donnait</w> <w n="1.7">sur</w> <w n="1.8">les</w> <w n="1.9">champs</w>.</l>
					<l n="2" num="1.2"><w n="2.1">Quand</w> <w n="2.2">j</w>’<w n="2.3">allais</w> <w n="2.4">voir</w>, <w n="2.5">l</w>’<w n="2.6">été</w>, <w n="2.7">les</w> <w n="2.8">beaux</w> <w n="2.9">soleils</w> <w n="2.10">couchants</w></l>
					<l n="3" num="1.3"><w n="3.1">Avec</w> <w n="3.2">le</w> <w n="3.3">rêve</w> <w n="3.4">aimé</w> <w n="3.5">qui</w> <w n="3.6">partout</w> <w n="3.7">m</w>’<w n="3.8">accompagne</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Je</w> <w n="4.2">la</w> <w n="4.3">suivais</w> <w n="4.4">toujours</w> <w n="4.5">pour</w> <w n="4.6">gagner</w> <w n="4.7">la</w> <w n="4.8">campagne</w>,</l>
					<l n="5" num="1.5"><w n="5.1">Et</w> <w n="5.2">j</w>’<w n="5.3">avais</w> <w n="5.4">remarqué</w> <w n="5.5">que</w>, <w n="5.6">dans</w> <w n="5.7">une</w> <w n="5.8">maison</w></l>
					<l n="6" num="1.6"><w n="6.1">Qui</w> <w n="6.2">fait</w> <w n="6.3">l</w>’<w n="6.4">angle</w> <w n="6.5">et</w> <w n="6.6">qui</w> <w n="6.7">tient</w>, <w n="6.8">ainsi</w> <w n="6.9">qu</w>’<w n="6.10">une</w> <w n="6.11">prison</w>,</l>
					<l n="7" num="1.7"><w n="7.1">Fermée</w> <w n="7.2">au</w> <w n="7.3">vent</w> <w n="7.4">du</w> <w n="7.5">soir</w> <w n="7.6">son</w> <w n="7.7">étroite</w> <w n="7.8">persienne</w>,</l>
					<l n="8" num="1.8"><w n="8.1">Toujours</w> <w n="8.2">à</w> <w n="8.3">la</w> <w n="8.4">même</w> <w n="8.5">heure</w>, <w n="8.6">une</w> <w n="8.7">musicienne</w></l>
					<l n="9" num="1.9"><w n="9.1">Mystérieuse</w>, <w n="9.2">et</w> <w n="9.3">qui</w> <w n="9.4">sans</w> <w n="9.5">doute</w> <w n="9.6">habitait</w> <w n="9.7">là</w>,</l>
					<l n="10" num="1.10"><w n="10.1">Jouait</w> <w n="10.2">l</w>’<w n="10.3">adagio</w> <w n="10.4">de</w> <w n="10.5">la</w> <w n="10.6">sonate</w> <w n="10.7">en</w> <hi rend="ital"><w n="10.8">la</w></hi>.</l>
					<l n="11" num="1.11"><w n="11.1">Le</w> <w n="11.2">ciel</w> <w n="11.3">se</w> <w n="11.4">nuançait</w> <w n="11.5">de</w> <w n="11.6">vert</w> <w n="11.7">tendre</w> <w n="11.8">et</w> <w n="11.9">de</w> <w n="11.10">rose</w>.</l>
					<l n="12" num="1.12"><w n="12.1">La</w> <w n="12.2">rue</w> <w n="12.3">était</w> <w n="12.4">déserte</w> ; <w n="12.5">et</w> <w n="12.6">le</w> <w n="12.7">flâneur</w> <w n="12.8">morose</w></l>
					<l n="13" num="1.13"><w n="13.1">Et</w> <w n="13.2">triste</w>, <w n="13.3">comme</w> <w n="13.4">sont</w> <w n="13.5">souvent</w> <w n="13.6">les</w> <w n="13.7">amoureux</w>,</l>
					<l n="14" num="1.14"><w n="14.1">Qui</w> <w n="14.2">passaient</w>, <w n="14.3">l</w>’<w n="14.4">œil</w> <w n="14.5">fixé</w> <w n="14.6">sur</w> <w n="14.7">les</w> <w n="14.8">gazons</w> <w n="14.9">poudreux</w>,</l>
					<l n="15" num="1.15"><w n="15.1">Toujours</w> <w n="15.2">à</w> <w n="15.3">la</w> <w n="15.4">même</w> <w n="15.5">heure</w>, <w n="15.6">avait</w> <w n="15.7">pris</w> <w n="15.8">l</w>’<w n="15.9">habitude</w></l>
					<l n="16" num="1.16"><w n="16.1">D</w>’<w n="16.2">entendre</w> <w n="16.3">ce</w> <w n="16.4">vieil</w> <w n="16.5">air</w> <w n="16.6">dans</w> <w n="16.7">cette</w> <w n="16.8">solitude</w>.</l>
					<l n="17" num="1.17"><w n="17.1">Le</w> <w n="17.2">piano</w> <w n="17.3">chantait</w> <w n="17.4">sourd</w>, <w n="17.5">doux</w>, <w n="17.6">attendrissant</w>,</l>
					<l n="18" num="1.18"><w n="18.1">Rempli</w> <w n="18.2">du</w> <w n="18.3">souvenir</w> <w n="18.4">douloureux</w> <w n="18.5">de</w> <w n="18.6">l</w>’<w n="18.7">absent</w>,</l>
					<l n="19" num="1.19"><w n="19.1">Et</w> <w n="19.2">reprochant</w> <w n="19.3">tout</w> <w n="19.4">bas</w> <w n="19.5">les</w> <w n="19.6">anciennes</w> <w n="19.7">extases</w>.</l>
					<l n="20" num="1.20"><w n="20.1">Et</w> <w n="20.2">moi</w>, <w n="20.3">je</w> <w n="20.4">devinais</w> <w n="20.5">des</w> <w n="20.6">fleurs</w> <w n="20.7">dans</w> <w n="20.8">de</w> <w n="20.9">grands</w> <w n="20.10">vases</w>,</l>
					<l n="21" num="1.21"><w n="21.1">Des</w> <w n="21.2">parfums</w>, <w n="21.3">un</w> <w n="21.4">profond</w> <w n="21.5">et</w> <w n="21.6">funèbre</w> <w n="21.7">miroir</w>,</l>
					<l n="22" num="1.22"><w n="22.1">Un</w> <w n="22.2">portrait</w> <w n="22.3">d</w>’<w n="22.4">homme</w> <w n="22.5">à</w> <w n="22.6">l</w>’<w n="22.7">œil</w> <w n="22.8">fier</w>, <w n="22.9">magnétique</w> <w n="22.10">et</w> <w n="22.11">noir</w>,</l>
					<l n="23" num="1.23"><w n="23.1">Des</w> <w n="23.2">plis</w> <w n="23.3">majestueux</w> <w n="23.4">dans</w> <w n="23.5">les</w> <w n="23.6">tentures</w> <w n="23.7">sombres</w>,</l>
					<l n="24" num="1.24"><w n="24.1">Une</w> <w n="24.2">lampe</w> <w n="24.3">d</w>’<w n="24.4">argent</w>, <w n="24.5">discrète</w>, <w n="24.6">sous</w> <w n="24.7">les</w> <w n="24.8">ombres</w>,</l>
					<l n="25" num="1.25"><w n="25.1">Le</w> <w n="25.2">vieux</w> <w n="25.3">clavier</w> <w n="25.4">s</w>’<w n="25.5">offrant</w> <w n="25.6">dans</w> <w n="25.7">sa</w> <w n="25.8">froide</w> <w n="25.9">pâleur</w>,</l>
					<l n="26" num="1.26"><w n="26.1">Et</w>, <w n="26.2">dans</w> <w n="26.3">cette</w> <w n="26.4">atmosphère</w> <w n="26.5">émue</w>, <w n="26.6">une</w> <w n="26.7">douleur</w></l>
					<l n="27" num="1.27"><w n="27.1">Épanouie</w> <w n="27.2">au</w> <w n="27.3">charme</w> <w n="27.4">ineffable</w> <w n="27.5">et</w> <w n="27.6">physique</w></l>
					<l n="28" num="1.28"><w n="28.1">Du</w> <w n="28.2">silence</w>, <w n="28.3">de</w> <w n="28.4">la</w> <w n="28.5">fraîcheur</w>, <w n="28.6">de</w> <w n="28.7">la</w> <w n="28.8">musique</w>.</l>
					<l n="29" num="1.29"><w n="29.1">Le</w> <w n="29.2">piano</w> <w n="29.3">chantait</w> <w n="29.4">toujours</w> <w n="29.5">plus</w> <w n="29.6">bas</w>, <w n="29.7">plus</w> <w n="29.8">bas</w>.</l>
					<l n="30" num="1.30"><w n="30.1">Puis</w>, <w n="30.2">un</w> <w n="30.3">certain</w> <w n="30.4">soir</w> <w n="30.5">d</w>’<w n="30.6">août</w>, <w n="30.7">je</w> <w n="30.8">ne</w> <w n="30.9">l</w>’<w n="30.10">entendis</w> <w n="30.11">pas</w>.</l>
				</lg>
				<lg n="2">
					<l n="31" num="2.1"><w n="31.1">Depuis</w>, <w n="31.2">je</w> <w n="31.3">mène</w> <w n="31.4">ailleurs</w> <w n="31.5">mes</w> <w n="31.6">promenades</w> <w n="31.7">lentes</w>.</l>
					<l n="32" num="2.2"><w n="32.1">Moi</w> <w n="32.2">qui</w> <w n="32.3">hais</w> <w n="32.4">et</w> <w n="32.5">qui</w> <w n="32.6">fuis</w> <w n="32.7">les</w> <w n="32.8">foules</w> <w n="32.9">turbulentes</w>,</l>
					<l n="33" num="2.3"><w n="33.1">Je</w> <w n="33.2">regrette</w> <w n="33.3">parfois</w> <w n="33.4">ce</w> <w n="33.5">vieux</w> <w n="33.6">coin</w> <w n="33.7">négligé</w>.</l>
					<l n="34" num="2.4"><w n="34.1">Mais</w> <w n="34.2">la</w> <w n="34.3">vieille</w> <w n="34.4">ruelle</w> <w n="34.5">a</w>, <w n="34.6">dit</w>-<w n="34.7">on</w>, <w n="34.8">bien</w> <w n="34.9">changé</w> :</l>
					<l n="35" num="2.5"><w n="35.1">Les</w> <w n="35.2">enfants</w> <w n="35.3">d</w>’<w n="35.4">alentour</w> <w n="35.5">y</w> <w n="35.6">vont</w> <w n="35.7">jouer</w> <w n="35.8">aux</w> <w n="35.9">billes</w>,</l>
					<l n="36" num="2.6"><w n="36.1">Et</w> <w n="36.2">d</w>’<w n="36.3">autres</w> <w n="36.4">pianos</w> <w n="36.5">l</w>’<w n="36.6">emplissent</w> <w n="36.7">de</w> <w n="36.8">quadrilles</w>.</l>
				</lg>
			</div></body></text></TEI>