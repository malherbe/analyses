<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE RELIQUAIRE</title>
				<title type="medium">Édition électronique</title>
				<author key="COP">
					<name>
						<forename>François</forename>
						<surname>COPPÉE</surname>
					</name>
					<date from="1842" to="1908">1842-1908</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>421 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2019</date>
				<idno type="local">COP_7</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE RELIQUAIRE</title>
						<author>François Coppée</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/francoiscoppeelereliquaire.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition papier d’origine.</p>
					</sourceDesc>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title type="main">Œuvres complètes</title>
								<author>François Coppée</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Librairie ALEXANDRE HOUSSIAUX</publisher>
									<date when="1885">1885</date>
								</imprint>
								<biblScope unit="tome">Poésie, tome 1</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1866">1866</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-05-25" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2019-05-25" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="COP199">
				<head type="main">UNE SAINTE</head>
				<opener>
					<salute>A ma mère</salute>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">C</w>’<w n="1.2">EST</w> <w n="1.3">une</w> <w n="1.4">vieille</w> <w n="1.5">fille</w> <w n="1.6">en</w> <w n="1.7">cheveux</w> <w n="1.8">blancs</w> ; <w n="1.9">elle</w> <w n="1.10">est</w></l>
					<l n="2" num="1.2"><w n="2.1">Pâle</w> <w n="2.2">et</w> <w n="2.3">maigre</w> ; <w n="2.4">un</w> <w n="2.5">antique</w> <w n="2.6">et</w> <w n="2.7">grossier</w> <w n="2.8">chapelet</w></l>
					<l n="3" num="1.3"><w n="3.1">S</w>’<w n="3.2">égrène</w>, <w n="3.3">machinal</w>, <w n="3.4">sous</w> <w n="3.5">ses</w> <w n="3.6">doigts</w> <w n="3.7">à</w> <w n="3.8">mitaines</w>.</l>
					<l n="4" num="1.4"><w n="4.1">Sans</w> <w n="4.2">cesse</w> <w n="4.3">remuant</w> <w n="4.4">ses</w> <w n="4.5">lèvres</w> <w n="4.6">puritaines</w></l>
					<l n="5" num="1.5"><w n="5.1">D</w>’<w n="5.2">où</w> <w n="5.3">tombent</w> <w n="5.4">les</w> <hi rend="ital"><w n="5.5">Pater</w> <w n="5.6">noster</w></hi> <w n="5.7">et</w> <w n="5.8">les</w> <hi rend="ital"><w n="5.9">Ave</w></hi>,</l>
					<l n="6" num="1.6"><w n="6.1">Et</w> <w n="6.2">laissant</w> <w n="6.3">son</w> <w n="6.4">tricot</w> <w n="6.5">de</w> <w n="6.6">laine</w> <w n="6.7">inachevé</w>,</l>
					<l n="7" num="1.7"><w n="7.1">Droite</w>, <w n="7.2">elle</w> <w n="7.3">prie</w>, <w n="7.4">assise</w> <w n="7.5">au</w> <w n="7.6">coin</w> <w n="7.7">d</w>’<w n="7.8">un</w> <w n="7.9">feu</w> <w n="7.10">de</w> <w n="7.11">veuve</w>,</l>
					<l n="8" num="1.8"><w n="8.1">Dans</w> <w n="8.2">sa</w> <w n="8.3">robe</w> <w n="8.4">de</w> <w n="8.5">deuil</w> <w n="8.6">rigide</w> <w n="8.7">et</w> <w n="8.8">toujours</w> <w n="8.9">neuve</w>.</l>
					<l n="9" num="1.9"><w n="9.1">Le</w> <w n="9.2">logis</w> <w n="9.3">est</w> <w n="9.4">glacé</w> <w n="9.5">comme</w> <w n="9.6">elle</w>. <w n="9.7">Le</w> <w n="9.8">cordeau</w></l>
					<l n="10" num="1.10"><w n="10.1">Semble</w> <w n="10.2">avoir</w> <w n="10.3">aligné</w> <w n="10.4">les</w> <w n="10.5">plis</w> <w n="10.6">droits</w> <w n="10.7">du</w> <w n="10.8">rideau</w>,</l>
					<l n="11" num="1.11"><w n="11.1">Que</w> <w n="11.2">blêmit</w> <w n="11.3">le</w> <w n="11.4">reflet</w> <w n="11.5">pâle</w> <w n="11.6">d</w>’<w n="11.7">un</w> <w n="11.8">jour</w> <w n="11.9">d</w>’<w n="11.10">automne</w> ;</l>
					<l n="12" num="1.12"><w n="12.1">Et</w>, <w n="12.2">s</w>’<w n="12.3">il</w> <w n="12.4">vient</w> <w n="12.5">un</w> <w n="12.6">rayon</w> <w n="12.7">de</w> <w n="12.8">soleil</w>, <w n="12.9">il</w> <w n="12.10">détonne</w></l>
					<l n="13" num="1.13"><w n="13.1">Et</w> <w n="13.2">sur</w> <w n="13.3">le</w> <w n="13.4">sol</w> <w n="13.5">découpe</w> <w n="13.6">un</w> <w n="13.7">grand</w> <w n="13.8">carré</w> <w n="13.9">brutal</w>.</l>
					<l n="14" num="1.14"><w n="14.1">Le</w> <w n="14.2">lit</w> <w n="14.3">est</w> <w n="14.4">étriqué</w> <w n="14.5">comme</w> <w n="14.6">un</w> <w n="14.7">lit</w> <w n="14.8">d</w>’<w n="14.9">hôpital</w>.</l>
					<l n="15" num="1.15"><w n="15.1">L</w>’<w n="15.2">heure</w> <w n="15.3">marche</w> <w n="15.4">sans</w> <w n="15.5">bruit</w> <w n="15.6">sous</w> <w n="15.7">son</w> <w n="15.8">globe</w> <w n="15.9">de</w> <w n="15.10">verre</w>.</l>
					<l n="16" num="1.16"><w n="16.1">Tout</w> <w n="16.2">est</w> <w n="16.3">froid</w>, <w n="16.4">triste</w>, <w n="16.5">gris</w>, <w n="16.6">monotone</w> <w n="16.7">et</w> <w n="16.8">sévère</w> ;</l>
					<l n="17" num="1.17"><w n="17.1">Et</w> <w n="17.2">près</w> <w n="17.3">du</w> <w n="17.4">crucifix</w> <w n="17.5">penché</w> <w n="17.6">comme</w> <w n="17.7">un</w> <w n="17.8">fruit</w> <w n="17.9">mûr</w>,</l>
					<l n="18" num="1.18"><w n="18.1">Deux</w> <w n="18.2">béquilles</w> <w n="18.3">d</w>’<w n="18.4">enfant</w>, <w n="18.5">en</w> <w n="18.6">croix</w>, <w n="18.7">pendent</w> <w n="18.8">au</w> <w n="18.9">mur</w>.</l>
				</lg>
				<lg n="2">
					<l n="19" num="2.1"><w n="19.1">C</w>’<w n="19.2">est</w> <w n="19.3">une</w> <w n="19.4">histoire</w> <w n="19.5">simple</w> <w n="19.6">et</w> <w n="19.7">très</w> <w n="19.8">mélancolique</w></l>
					<l n="20" num="2.2"><w n="20.1">Que</w> <w n="20.2">raconte</w> <w n="20.3">l</w>’<w n="20.4">étrange</w> <w n="20.5">et</w> <w n="20.6">lugubre</w> <w n="20.7">relique</w> :</l>
					<l n="21" num="2.3"><w n="21.1">Les</w> <w n="21.2">baisers</w> <w n="21.3">sur</w> <w n="21.4">les</w> <w n="21.5">mains</w> <w n="21.6">froides</w> <w n="21.7">des</w> <w n="21.8">vieux</w> <w n="21.9">parents</w> ;</l>
					<l n="22" num="2.4"><w n="22.1">La</w> <w n="22.2">bénédiction</w> <w n="22.3">tremblante</w> <w n="22.4">des</w> <w n="22.5">mourants</w> ;</l>
					<l n="23" num="2.5"><w n="23.1">Et</w> <w n="23.2">puis</w> <w n="23.3">deux</w> <w n="23.4">orphelins</w> <w n="23.5">tout</w> <w n="23.6">seuls</w>, <w n="23.7">le</w> <w n="23.8">petit</w> <w n="23.9">frère</w></l>
					<l n="24" num="2.6"><w n="24.1">Infirme</w>, <w n="24.2">étiolé</w>, <w n="24.3">qui</w> <w n="24.4">souffre</w> <w n="24.5">et</w> <w n="24.6">qui</w> <w n="24.7">se</w> <w n="24.8">serre</w>,</l>
					<l n="25" num="2.7"><w n="25.1">Frileux</w>, <w n="25.2">contre</w> <w n="25.3">le</w> <w n="25.4">sein</w> <w n="25.5">d</w>’<w n="25.6">un</w> <w n="25.7">ange</w> <w n="25.8">aux</w> <w n="25.9">cheveux</w> <w n="25.10">blonds</w> ;</l>
					<l n="26" num="2.8"><w n="26.1">La</w> <w n="26.2">grande</w> <w n="26.3">sœur</w>, <w n="26.4">si</w> <w n="26.5">pâle</w> <w n="26.6">avec</w> <w n="26.7">ses</w> <w n="26.8">voiles</w> <w n="26.9">longs</w>,</l>
					<l n="27" num="2.9"><w n="27.1">Qui</w>, <w n="27.2">la</w> <w n="27.3">veille</w>, <w n="27.4">devant</w> <w n="27.5">le</w> <w n="27.6">linceul</w> <w n="27.7">et</w> <w n="27.8">le</w> <w n="27.9">cierge</w>,</l>
					<l n="28" num="2.10"><w n="28.1">Jurait</w> <w n="28.2">aux</w> <w n="28.3">parents</w> <w n="28.4">morts</w>, <w n="28.5">à</w> <w n="28.6">Jésus</w>, <w n="28.7">à</w> <w n="28.8">la</w> <w n="28.9">Vierge</w>,</l>
					<l n="29" num="2.11"><w n="29.1">D</w>’<w n="29.2">être</w> <w n="29.3">une</w> <w n="29.4">mère</w> <w n="29.5">au</w> <w n="29.6">pauvre</w> <w n="29.7">enfant</w>, <w n="29.8">frêle</w> <w n="29.9">roseau</w> ;</l>
					<l n="30" num="2.12"><w n="30.1">Ce</w> <w n="30.2">sont</w> <w n="30.3">les</w> <w n="30.4">petits</w> <w n="30.5">bras</w> <w n="30.6">tendus</w> <w n="30.7">hors</w> <w n="30.8">du</w> <w n="30.9">berceau</w>,</l>
					<l n="31" num="2.13"><w n="31.1">La</w> <w n="31.2">douleur</w> <w n="31.3">apaisée</w> <w n="31.4">un</w> <w n="31.5">instant</w> <w n="31.6">par</w> <w n="31.7">un</w> <w n="31.8">conte</w>,</l>
					<l n="32" num="2.14"><w n="32.1">L</w>’<w n="32.2">insomnie</w> <w n="32.3">et</w> <w n="32.4">la</w> <w n="32.5">voix</w> <w n="32.6">de</w> <w n="32.7">l</w>’<w n="32.8">horloge</w> <w n="32.9">qui</w> <w n="32.10">compte</w></l>
					<l n="33" num="2.15"><w n="33.1">L</w>’<w n="33.2">heure</w> <w n="33.3">très</w> <w n="33.4">lentement</w>, <w n="33.5">les</w> <w n="33.6">réveils</w> <w n="33.7">pleins</w> <w n="33.8">d</w>’<w n="33.9">effrois</w>,</l>
					<l n="34" num="2.16"><w n="34.1">Les</w> <w n="34.2">soins</w> <w n="34.3">donnés</w>, <w n="34.4">les</w> <w n="34.5">pieds</w> <w n="34.6">nus</w> <w n="34.7">sur</w> <w n="34.8">les</w> <w n="34.9">carreaux</w> <w n="34.10">froids</w>,</l>
					<l n="35" num="2.17"><w n="35.1">Les</w> <w n="35.2">baisers</w> <w n="35.3">appuyés</w> <w n="35.4">sur</w> <w n="35.5">la</w> <w n="35.6">trace</w> <w n="35.7">des</w> <w n="35.8">larmes</w>,</l>
					<l n="36" num="2.18"><w n="36.1">Et</w> <w n="36.2">la</w> <w n="36.3">tisane</w> <w n="36.4">offerte</w>, <w n="36.5">et</w> <w n="36.6">les</w> <w n="36.7">folles</w> <w n="36.8">alarmes</w>,</l>
					<l n="37" num="2.19"><w n="37.1">Et</w> <w n="37.2">le</w> <w n="37.3">petit</w> <w n="37.4">malade</w> <w n="37.5">à</w> <w n="37.6">l</w>’<w n="37.7">aurore</w> <w n="37.8">n</w>’<w n="37.9">offrant</w></l>
					<l n="38" num="2.20"><w n="38.1">Qu</w>’<w n="38.2">un</w> <w n="38.3">front</w> <w n="38.4">plus</w> <w n="38.5">pâle</w> <w n="38.6">et</w> <w n="38.7">qu</w>’<w n="38.8">un</w> <w n="38.9">sourire</w> <w n="38.10">plus</w> <w n="38.11">navrant</w>.</l>
				</lg>
				<lg n="3">
					<l n="39" num="3.1"><w n="39.1">Ce</w> <w n="39.2">dévoûment</w> <w n="39.3">obscur</w> <w n="39.4">a</w> <w n="39.5">duré</w> <w n="39.6">dix</w> <w n="39.7">années</w> :</l>
					<l n="40" num="3.2"><w n="40.1">Beauté</w>, <w n="40.2">jeunesse</w>, <w n="40.3">fleurs</w> <w n="40.4">loin</w> <w n="40.5">du</w> <w n="40.6">soleil</w> <w n="40.7">fanées</w>,</l>
					<l n="41" num="3.3"><w n="41.1">Tout</w> <w n="41.2">fut</w> <w n="41.3">sacrifié</w> <w n="41.4">sans</w> <w n="41.5">plainte</w> <w n="41.6">et</w> <w n="41.7">sans</w> <w n="41.8">regret</w> ;</l>
					<l n="42" num="3.4"><w n="42.1">Et</w> <w n="42.2">quand</w>, <w n="42.3">par</w> <w n="42.4">les</w> <w n="42.5">beaux</w> <w n="42.6">soirs</w>, <w n="42.7">un</w> <w n="42.8">instant</w> <w n="42.9">elle</w> <w n="42.10">ouvrait</w></l>
					<l n="43" num="3.5"><w n="43.1">A</w> <w n="43.2">la</w> <w n="43.3">brise</w> <w n="43.4">de</w> <w n="43.5">mai</w> <w n="43.6">charmante</w> <w n="43.7">et</w> <w n="43.8">parfumée</w></l>
					<l n="44" num="3.6"><w n="44.1">La</w> <w n="44.2">fenêtre</w> <w n="44.3">toujours</w> <w n="44.4">par</w> <w n="44.5">prudence</w> <w n="44.6">fermée</w></l>
					<l n="45" num="3.7"><w n="45.1">Et</w> <w n="45.2">laissait</w> <w n="45.3">ses</w> <w n="45.4">regards</w> <w n="45.5">errer</w> <w n="45.6">à</w> <w n="45.7">l</w>’<w n="45.8">horizon</w>,</l>
					<l n="46" num="3.8"><w n="46.1">Une</w> <w n="46.2">toux</w> <w n="46.3">de</w> <w n="46.4">l</w>’<w n="46.5">enfant</w> <w n="46.6">refermait</w> <w n="46.7">sa</w> <w n="46.8">prison</w>.</l>
				</lg>
				<lg n="4">
					<l part="I" n="47" num="4.1"><w n="47.1">Elle</w> <w n="47.2">est</w> <w n="47.3">libre</w> <w n="47.4">aujourd</w>’<w n="47.5">hui</w>. </l>
					<l part="F" n="47" num="4.1"><w n="47.6">C</w>’<w n="47.7">est</w> <w n="47.8">une</w> <w n="47.9">pauvre</w> <w n="47.10">vieille</w>,</l>
					<l n="48" num="4.2"><w n="48.1">Toujours</w> <w n="48.2">en</w> <w n="48.3">deuil</w>, <w n="48.4">dévote</w>, <w n="48.5">ascétique</w>, <w n="48.6">pareille</w></l>
					<l n="49" num="4.3"><w n="49.1">Aux</w> <w n="49.2">béguines</w> <w n="49.3">qu</w>’<w n="49.4">on</w> <w n="49.5">voit</w> <w n="49.6">errer</w> <w n="49.7">dans</w> <w n="49.8">le</w> <w n="49.9">couvent</w>.</l>
					<l n="50" num="4.4"><w n="50.1">Libre</w> ! <w n="50.2">Pauvre</w> <w n="50.3">âme</w> <w n="50.4">simple</w> <w n="50.5">et</w> <w n="50.6">douce</w> ! <w n="50.7">Bien</w> <w n="50.8">souvent</w></l>
					<l n="51" num="4.5"><w n="51.1">Elle</w> <w n="51.2">songe</w>, <w n="51.3">très</w> <w n="51.4">triste</w>, <w n="51.5">à</w> <w n="51.6">son</w> <w n="51.7">cher</w> <w n="51.8">esclavage</w>,</l>
					<l n="52" num="4.6"><w n="52.1">Et</w>, <w n="52.2">tout</w> <w n="52.3">bas</w>, <w n="52.4">d</w>’<w n="52.5">une</w> <w n="52.6">voix</w> <w n="52.7">sourde</w>, <w n="52.8">presque</w> <w n="52.9">sauvage</w>,</l>
					<l n="53" num="4.7"><w n="53.1">Elle</w> <w n="53.2">dit</w> : « <w n="53.3">Il</w> <w n="53.4">est</w> <w n="53.5">mort</w> ! » <w n="53.6">Puis</w> <w n="53.7">elle</w> <w n="53.8">s</w>’<w n="53.9">attendrit</w>,</l>
					<l n="54" num="4.8"><w n="54.1">Et</w> <w n="54.2">reprend</w>. <w n="54.3">Il</w> <w n="54.4">avait</w> <w n="54.5">déjà</w> <w n="54.6">beaucoup</w> <w n="54.7">d</w>’<w n="54.8">esprit</w>.</l>
					<l n="55" num="4.9"><w n="55.1">Quand</w> <w n="55.2">il</w> <w n="55.3">était</w> <w n="55.4">méchant</w>, <w n="55.5">il</w> <w n="55.6">m</w>’<w n="55.7">appelait</w> <w n="55.8">Madame</w>.</l>
					<l n="56" num="4.10"><w n="56.1">Il</w> <w n="56.2">est</w> <w n="56.3">mort</w> ! <w n="56.4">Le</w> <w n="56.5">bon</w> <w n="56.6">Dieu</w> <w n="56.7">l</w>’<w n="56.8">a</w> <w n="56.9">pris</w>. <w n="56.10">Sa</w> <w n="56.11">petite</w> <w n="56.12">âme</w></l>
					<l n="57" num="4.11"><w n="57.1">A</w> <w n="57.2">des</w> <w n="57.3">ailes</w>. <w n="57.4">Il</w> <w n="57.5">est</w> <w n="57.6">un</w> <w n="57.7">ange</w> <w n="57.8">au</w> <w n="57.9">paradis</w>.</l>
					<l n="58" num="4.12"><w n="58.1">Sans</w> <w n="58.2">quoi</w>, <w n="58.3">serait</w>-<w n="58.4">il</w> <w n="58.5">mort</w> ? <w n="58.6">Quelquefois</w> <w n="58.7">je</w> <w n="58.8">me</w> <w n="58.9">dis</w></l>
					<l n="59" num="4.13"><w n="59.1">Que</w> <w n="59.2">Dieu</w> <w n="59.3">prend</w> <w n="59.4">les</w> <w n="59.5">enfants</w> <w n="59.6">pour</w> <w n="59.7">en</w> <w n="59.8">faire</w> <w n="59.9">des</w> <w n="59.10">anges</w>.</l>
					<l n="60" num="4.14"><w n="60.1">Puis</w> <w n="60.2">il</w> <w n="60.3">avait</w> <w n="60.4">des</w> <w n="60.5">mots</w> <w n="60.6">et</w> <w n="60.7">des</w> <w n="60.8">regards</w> <w n="60.9">étranges</w> :</l>
					<l n="61" num="4.15"><w n="61.1">Peut</w>-<w n="61.2">être</w> <w n="61.3">qu</w>’<w n="61.4">il</w> <w n="61.5">était</w> <w n="61.6">ange</w> <w n="61.7">avant</w> <w n="61.8">d</w>’<w n="61.9">être</w> <w n="61.10">né</w> ?</l>
					<l n="62" num="4.16"><w n="62.1">Tes</w> <w n="62.2">pleurs</w> <w n="62.3">de</w> <w n="62.4">chaque</w> <w n="62.5">jour</w>, <w n="62.6">ô</w> <w n="62.7">pauvre</w> <w n="62.8">condamné</w>,</l>
					<l n="63" num="4.17"><w n="63.1">Valent</w> <w n="63.2">bien</w> <w n="63.3">tous</w> <w n="63.4">les</w> <w n="63.5">longs</w> <hi rend="ital"><w n="63.6">Oremus</w></hi> <w n="63.7">qu</w>’<w n="63.8">on</w> <w n="63.9">prodigue</w>.</l>
					<l n="64" num="4.18"><w n="64.1">Puis</w> <w n="64.2">un</w> <w n="64.3">signe</w> <w n="64.4">de</w> <w n="64.5">croix</w> <w n="64.6">était</w> <w n="64.7">une</w> <w n="64.8">fatigue</w></l>
					<l n="65" num="4.19"><w n="65.1">Pour</w> <w n="65.2">son</w> <w n="65.3">bras</w>. <w n="65.4">Il</w> <w n="65.5">savait</w> <w n="65.6">sourire</w>, <w n="65.7">et</w> <w n="65.8">non</w> <w n="65.9">prier</w>.</l>
					<l n="66" num="4.20"><w n="66.1">Il</w> <w n="66.2">est</w> <w n="66.3">mort</w> ! <w n="66.4">Une</w> <w n="66.5">nuit</w>, <w n="66.6">je</w> <w n="66.7">l</w>’<w n="66.8">entendis</w> <w n="66.9">crier</w>.</l>
					<l n="67" num="4.21"><w n="67.1">J</w>’<w n="67.2">accourus</w>, <w n="67.3">je</w> <w n="67.4">penchai</w> <w n="67.5">la</w> <w n="67.6">tête</w> <w n="67.7">vers</w> <w n="67.8">sa</w> <w n="67.9">couche</w>,</l>
					<l n="68" num="4.22"><w n="68.1">Et</w> <w n="68.2">sa</w> <w n="68.3">dernière</w> <w n="68.4">haleine</w> <w n="68.5">a</w> <w n="68.6">passé</w> <w n="68.7">sur</w> <w n="68.8">ma</w> <w n="68.9">bouche</w>,</l>
					<l n="69" num="4.23"><w n="69.1">Et</w> <w n="69.2">depuis</w> <w n="69.3">ce</w> <w n="69.4">temps</w>-<w n="69.5">là</w> <w n="69.6">je</w> <w n="69.7">n</w>’<w n="69.8">ai</w> <w n="69.9">plus</w> <w n="69.10">de</w> <w n="69.11">gaîté</w>.</l>
					<l n="70" num="4.24"><w n="70.1">Le</w> <w n="70.2">lendemain</w>, <w n="70.3">des</w> <w n="70.4">gens</w> <w n="70.5">sombres</w> <w n="70.6">l</w>’<w n="70.7">ont</w> <w n="70.8">emporté</w>.</l>
					<l n="71" num="4.25"><w n="71.1">Pauvre</w> <w n="71.2">martyr</w> ! <w n="71.3">Sa</w> <w n="71.4">bière</w> <w n="71.5">était</w> <w n="71.6">toute</w> <w n="71.7">petite</w> !</l>
					<l n="72" num="4.26"><w n="72.1">J</w>’<w n="72.2">ai</w> <w n="72.3">laissé</w> <w n="72.4">sur</w> <w n="72.5">son</w> <w n="72.6">cœur</w> <w n="72.7">sa</w> <w n="72.8">médaille</w> <w n="72.9">bénite</w>.</l>
					<l n="73" num="4.27"><w n="73.1">Cela</w> <w n="73.2">fera</w> <w n="73.3">plaisir</w> <w n="73.4">au</w> <w n="73.5">bon</w> <w n="73.6">Dieu</w>, <w n="73.7">n</w>’<w n="73.8">est</w>-<w n="73.9">ce</w> <w n="73.10">pas</w> ?</l>
					<l n="74" num="4.28"><w n="74.1">Il</w> <w n="74.2">est</w> <w n="74.3">au</w> <w n="74.4">Ciel</w>. <w n="74.5">Hélas</w> ! <w n="74.6">est</w>-<w n="74.7">il</w> <w n="74.8">heureux</w> <w n="74.9">là</w>-<w n="74.10">bas</w> ?</l>
					<l n="75" num="4.29"><w n="75.1">Les</w> <w n="75.2">anges</w>, <w n="75.3">on</w> <w n="75.4">se</w> <w n="75.5">fait</w> <w n="75.6">parfois</w> <w n="75.7">de</w> <w n="75.8">ces</w> <w n="75.9">chimères</w>,</l>
					<l n="76" num="4.30"><w n="76.1">Ont</w>-<w n="76.2">ils</w> <w n="76.3">soin</w> <w n="76.4">des</w> <w n="76.5">enfants</w> <w n="76.6">aussi</w> <w n="76.7">bien</w> <w n="76.8">que</w> <w n="76.9">les</w> <w n="76.10">mères</w> ?</l>
					<l n="77" num="4.31"><w n="77.1">Je</w> <w n="77.2">doute</w>. <w n="77.3">Pardonnez</w>, <w n="77.4">Seigneur</w>, <w n="77.5">à</w> <w n="77.6">mon</w> <w n="77.7">regret</w> ! »</l>
					<l n="78" num="4.32"><w n="78.1">Et</w> <w n="78.2">baissant</w> <w n="78.3">ses</w> <w n="78.4">grands</w> <w n="78.5">yeux</w>, <w n="78.6">où</w> <w n="78.7">l</w>’<w n="78.8">âme</w> <w n="78.9">transparaît</w>,</l>
					<l n="79" num="4.33"><w n="79.1">Elle</w> <w n="79.2">active</w> <w n="79.3">le</w> <w n="79.4">cours</w> <w n="79.5">rythmique</w> <w n="79.6">et</w> <w n="79.7">monotone</w></l>
					<l n="80" num="4.34"><w n="80.1">De</w> <w n="80.2">son</w> <w n="80.3">lent</w> <w n="80.4">chapelet</w>. <w n="80.5">Et</w> <w n="80.6">le</w> <w n="80.7">soleil</w> <w n="80.8">d</w>’<w n="80.9">automne</w>,</l>
					<l n="81" num="4.35"><w n="81.1">Qui</w> <w n="81.2">dore</w> <w n="81.3">les</w> <w n="81.4">carreaux</w> <w n="81.5">de</w> <w n="81.6">ses</w> <w n="81.7">rayons</w> <w n="81.8">tremblants</w>,</l>
					<l n="82" num="4.36"><w n="82.1">Met</w> <w n="82.2">de</w> <w n="82.3">vagues</w> <w n="82.4">lueurs</w> <w n="82.5">parmi</w> <w n="82.6">ses</w> <w n="82.7">cheveux</w> <w n="82.8">blancs</w>.</l>
				</lg>
			</div></body></text></TEI>