<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE RELIQUAIRE</title>
				<title type="medium">Édition électronique</title>
				<author key="COP">
					<name>
						<forename>François</forename>
						<surname>COPPÉE</surname>
					</name>
					<date from="1842" to="1908">1842-1908</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>421 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2019</date>
				<idno type="local">COP_7</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE RELIQUAIRE</title>
						<author>François Coppée</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/francoiscoppeelereliquaire.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition papier d’origine.</p>
					</sourceDesc>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title type="main">Œuvres complètes</title>
								<author>François Coppée</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Librairie ALEXANDRE HOUSSIAUX</publisher>
									<date when="1885">1885</date>
								</imprint>
								<biblScope unit="tome">Poésie, tome 1</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1866">1866</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-05-25" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2019-05-25" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="COP192">
				<head type="main"><hi rend="ital">ET NUNC ET SEMPER</hi></head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Sous</w> <w n="1.2">l</w>’<w n="1.3">éclat</w> <w n="1.4">blanc</w> <w n="1.5">du</w> <w n="1.6">jour</w>, <w n="1.7">sous</w> <w n="1.8">la</w> <w n="1.9">fraîcheur</w> <w n="1.10">des</w> <w n="1.11">cèdres</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Sous</w> <w n="2.2">la</w> <w n="2.3">nuit</w> <w n="2.4">où</w> <w n="2.5">poudroie</w> <w n="2.6">un</w> <w n="2.7">peuple</w> <w n="2.8">de</w> <w n="2.9">soleils</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Longtemps</w> <w n="3.2">j</w>’<w n="3.3">ai</w> <w n="3.4">promené</w> <w n="3.5">mes</w> <w n="3.6">souvenirs</w>, <w n="3.7">pareils</w></l>
					<l n="4" num="1.4"><w n="4.1">Aux</w> <w n="4.2">tragiques</w> <w n="4.3">douleurs</w> <w n="4.4">des</w> <w n="4.5">Saphos</w> <w n="4.6">et</w> <w n="4.7">des</w> <w n="4.8">Phèdres</w> ;</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Mais</w> <w n="5.2">l</w>’<w n="5.3">azur</w> <w n="5.4">clair</w>, <w n="5.5">les</w> <w n="5.6">bois</w> <w n="5.7">profonds</w>, <w n="5.8">les</w> <w n="5.9">blondes</w> <w n="5.10">nuits</w></l>
					<l n="6" num="2.2"><w n="6.1">En</w> <w n="6.2">moi</w> <w n="6.3">n</w>’<w n="6.4">ont</w> <w n="6.5">point</w> <w n="6.6">versé</w> <w n="6.7">leurs</w> <w n="6.8">influences</w> <w n="6.9">calmes</w> ;</l>
					<l n="7" num="2.3"><w n="7.1">Sous</w> <w n="7.2">les</w> <w n="7.3">astres</w>, <w n="7.4">sous</w> <w n="7.5">les</w> <w n="7.6">rayons</w> <w n="7.7">et</w> <w n="7.8">sous</w> <w n="7.9">les</w> <w n="7.10">palmes</w>,</l>
					<l n="8" num="2.4"><w n="8.1">Sans</w> <w n="8.2">espoir</w> <w n="8.3">je</w> <w n="8.4">promène</w> <w n="8.5">encore</w> <w n="8.6">mes</w> <w n="8.7">ennuis</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Que</w> <w n="9.2">la</w> <w n="9.3">forêt</w> <w n="9.4">frémisse</w> <w n="9.5">ainsi</w> <w n="9.6">qu</w>’<w n="9.7">un</w> <w n="9.8">chœur</w> <w n="9.9">de</w> <w n="9.10">harpes</w>,</l>
					<l n="10" num="3.2"><w n="10.1">Ou</w> <w n="10.2">que</w> <w n="10.3">le</w> <w n="10.4">soir</w> <w n="10.5">s</w>’<w n="10.6">embaume</w> <w n="10.7">aux</w> <w n="10.8">calices</w> <w n="10.9">ouverts</w>,</l>
					<l n="11" num="3.3"><w n="11.1">Le</w> <w n="11.2">son</w> <w n="11.3">ou</w> <w n="11.4">le</w> <w n="11.5">parfum</w> <w n="11.6">des</w> <w n="11.7">maux</w> <w n="11.8">jadis</w> <w n="11.9">soufferts</w></l>
					<l n="12" num="3.4"><w n="12.1">Descend</w> <w n="12.2">sur</w> <w n="12.3">ma</w> <w n="12.4">pensée</w> <w n="12.5">en</w> <w n="12.6">funèbres</w> <w n="12.7">écharpes</w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Âmes</w> <w n="13.2">tristes</w> <w n="13.3">des</w> <w n="13.4">fleurs</w>, <w n="13.5">chastes</w> <w n="13.6">frissons</w> <w n="13.7">des</w> <w n="13.8">bois</w>,</l>
					<l n="14" num="4.2"><w n="14.1">Me</w> <w n="14.2">haïssez</w>-<w n="14.3">vous</w> <w n="14.4">donc</w>, <w n="14.5">puisqu</w>’<w n="14.6">il</w> <w n="14.7">faut</w> <w n="14.8">que</w> <w n="14.9">je</w> <w n="14.10">sente</w></l>
					<l n="15" num="4.3"><w n="15.1">Dans</w> <w n="15.2">vos</w> <w n="15.3">aromes</w> <w n="15.4">chers</w> <w n="15.5">les</w> <w n="15.6">baisers</w> <w n="15.7">de</w> <w n="15.8">l</w>’<w n="15.9">absente</w></l>
					<l n="16" num="4.4"><w n="16.1">Et</w> <w n="16.2">que</w> <w n="16.3">j</w>’<w n="16.4">entende</w> <w n="16.5">en</w> <w n="16.6">vos</w> <w n="16.7">échos</w> <w n="16.8">vibrer</w> <w n="16.9">sa</w> <w n="16.10">voix</w> ?</l>
				</lg>
			</div></body></text></TEI>