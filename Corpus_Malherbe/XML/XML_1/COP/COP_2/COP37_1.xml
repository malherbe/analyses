<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÈMES MODERNES</title>
				<title type="medium">Édition électronique</title>
				<author key="COP">
					<name>
						<forename>François</forename>
						<surname>COPPÉE</surname>
					</name>
					<date from="1842" to="1908">1842-1908</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1442 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">COP_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÈMES MODERNES</title>
						<author>François Coppée</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/francoiscopeepoesiesmodernes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Œuvres complètes de François Coppée</title>
						<author>François Coppée</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie L. Hébert</publisher>
							<date when="1885">1885</date>
						</imprint>
						<biblScope unit="tome">Poésie, tome 1</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1869">1867-1869</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="COP37">
					<head type="main">L’Attente</head>
					<opener>
						<salute>À Auguste Vacquerie</salute>
					</opener>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Au</w> <w n="1.2">bout</w> <w n="1.3">du</w> <w n="1.4">vieux</w> <w n="1.5">canal</w> <w n="1.6">plein</w> <w n="1.7">de</w> <w n="1.8">mâts</w>, <w n="1.9">juste</w> <w n="1.10">en</w> <w n="1.11">face</w></l>
						<l n="2" num="1.2"><w n="2.1">De</w> <w n="2.2">l</w>’<w n="2.3">Océan</w> <w n="2.4">et</w> <w n="2.5">dans</w> <w n="2.6">la</w> <w n="2.7">dernière</w> <w n="2.8">maison</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Assise</w> <w n="3.2">à</w> <w n="3.3">sa</w> <w n="3.4">fenêtre</w>, <w n="3.5">et</w> <w n="3.6">quelque</w> <w n="3.7">temps</w> <w n="3.8">qu</w>’<w n="3.9">il</w> <w n="3.10">fasse</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Elle</w> <w n="4.2">se</w> <w n="4.3">tient</w>, <w n="4.4">les</w> <w n="4.5">yeux</w> <w n="4.6">fixés</w> <w n="4.7">sur</w> <w n="4.8">l</w>’<w n="4.9">horizon</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Bien</w> <w n="5.2">qu</w>’<w n="5.3">elle</w> <w n="5.4">ait</w> <w n="5.5">la</w> <w n="5.6">pâleur</w> <w n="5.7">des</w> <w n="5.8">éternels</w> <w n="5.9">veuvages</w>,</l>
						<l n="6" num="2.2"><w n="6.1">Sa</w> <w n="6.2">robe</w> <w n="6.3">est</w> <w n="6.4">claire</w> ; <w n="6.5">et</w> <w n="6.6">bien</w> <w n="6.7">que</w> <w n="6.8">les</w> <w n="6.9">soucis</w> <w n="6.10">pesants</w></l>
						<l n="7" num="2.3"><w n="7.1">Aient</w> <w n="7.2">sur</w> <w n="7.3">ses</w> <w n="7.4">traits</w> <w n="7.5">flétris</w> <w n="7.6">exercé</w> <w n="7.7">leurs</w> <w n="7.8">ravages</w>,</l>
						<l n="8" num="2.4"><w n="8.1">Ses</w> <w n="8.2">vêtements</w> <w n="8.3">sont</w> <w n="8.4">ceux</w> <w n="8.5">des</w> <w n="8.6">filles</w> <w n="8.7">de</w> <w n="8.8">seize</w> <w n="8.9">ans</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Car</w> <w n="9.2">depuis</w> <w n="9.3">bien</w> <w n="9.4">des</w> <w n="9.5">jours</w>, <w n="9.6">patiente</w> <w n="9.7">vigie</w>,</l>
						<l n="10" num="3.2"><w n="10.1">Dès</w> <w n="10.2">l</w>’<w n="10.3">instant</w> <w n="10.4">où</w> <w n="10.5">la</w> <w n="10.6">mer</w> <w n="10.7">bleuit</w> <w n="10.8">dans</w> <w n="10.9">le</w> <w n="10.10">matin</w></l>
						<l n="11" num="3.3"><w n="11.1">Jusqu</w>’<w n="11.2">à</w> <w n="11.3">ce</w> <w n="11.4">qu</w>’<w n="11.5">elle</w> <w n="11.6">soit</w> <w n="11.7">par</w> <w n="11.8">le</w> <w n="11.9">couchant</w> <w n="11.10">rougie</w>,</l>
						<l n="12" num="3.4"><w n="12.1">Elle</w> <w n="12.2">est</w> <w n="12.3">assise</w> <w n="12.4">là</w>, <w n="12.5">regardant</w> <w n="12.6">au</w> <w n="12.7">lointain</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Chaque</w> <w n="13.2">aurore</w> <w n="13.3">elle</w> <w n="13.4">voit</w> <w n="13.5">une</w> <w n="13.6">tardive</w> <w n="13.7">étoile</w></l>
						<l n="14" num="4.2"><w n="14.1">S</w>’<w n="14.2">éteindre</w>, <w n="14.3">et</w> <w n="14.4">chaque</w> <w n="14.5">soir</w> <w n="14.6">le</w> <w n="14.7">soleil</w> <w n="14.8">s</w>’<w n="14.9">enfoncer</w></l>
						<l n="15" num="4.3"><w n="15.1">A</w> <w n="15.2">cette</w> <w n="15.3">place</w> <w n="15.4">où</w> <w n="15.5">doit</w> <w n="15.6">reparaître</w> <w n="15.7">la</w> <w n="15.8">voile</w></l>
						<l n="16" num="4.4"><w n="16.1">Qu</w>’<w n="16.2">elle</w> <w n="16.3">vit</w> <w n="16.4">là</w>, <w n="16.5">jadis</w>, <w n="16.6">pâlir</w> <w n="16.7">et</w> <w n="16.8">s</w>’<w n="16.9">effacer</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Son</w> <w n="17.2">cœur</w> <w n="17.3">de</w> <w n="17.4">fiancée</w>, <w n="17.5">immuable</w> <w n="17.6">et</w> <w n="17.7">fidèle</w>,</l>
						<l n="18" num="5.2"><w n="18.1">Attend</w> <w n="18.2">toujours</w>, <w n="18.3">certain</w> <w n="18.4">de</w> <w n="18.5">l</w>’<w n="18.6">espoir</w> <w n="18.7">partagé</w>,</l>
						<l n="19" num="5.3"><w n="19.1">Loyal</w> ; <w n="19.2">et</w> <w n="19.3">rien</w> <w n="19.4">en</w> <w n="19.5">elle</w>, <w n="19.6">aussi</w> <w n="19.7">bien</w> <w n="19.8">qu</w>’<w n="19.9">autour</w> <w n="19.10">d</w>’<w n="19.11">elle</w>,</l>
						<l n="20" num="5.4"><w n="20.1">Depuis</w> <w n="20.2">dix</w> <w n="20.3">ans</w> <w n="20.4">qu</w>’<w n="20.5">il</w> <w n="20.6">est</w> <w n="20.7">parti</w>, <w n="20.8">rien</w> <w n="20.9">n</w>’<w n="20.10">a</w> <w n="20.11">changé</w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Les</w> <w n="21.2">quelques</w> <w n="21.3">doux</w> <w n="21.4">vieillards</w> <w n="21.5">qui</w> <w n="21.6">lui</w> <w n="21.7">rendent</w> <w n="21.8">visite</w>,</l>
						<l n="22" num="6.2"><w n="22.1">En</w> <w n="22.2">la</w> <w n="22.3">voyant</w> <w n="22.4">avec</w> <w n="22.5">ses</w> <w n="22.6">bandeaux</w> <w n="22.7">réguliers</w>,</l>
						<l n="23" num="6.3"><w n="23.1">Son</w> <w n="23.2">ruban</w> <w n="23.3">mince</w> <w n="23.4">où</w> <w n="23.5">pend</w> <w n="23.6">sa</w> <w n="23.7">médaille</w> <w n="23.8">bénite</w>,</l>
						<l n="24" num="6.4"><w n="24.1">Son</w> <w n="24.2">corsage</w> <w n="24.3">à</w> <w n="24.4">la</w> <w n="24.5">vierge</w> <w n="24.6">et</w> <w n="24.7">ses</w> <w n="24.8">petits</w> <w n="24.9">souliers</w>,</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">La</w> <w n="25.2">croiraient</w> <w n="25.3">une</w> <w n="25.4">enfant</w> <w n="25.5">ingénue</w> <w n="25.6">et</w> <w n="25.7">qui</w> <w n="25.8">boude</w>,</l>
						<l n="26" num="7.2"><w n="26.1">Si</w> <w n="26.2">parfois</w> <w n="26.3">ses</w> <w n="26.4">doigts</w> <w n="26.5">purs</w>, <w n="26.6">ivoirins</w> <w n="26.7">et</w> <w n="26.8">tremblants</w>,</l>
						<l n="27" num="7.3"><w n="27.1">Alors</w> <w n="27.2">que</w> <w n="27.3">sur</w> <w n="27.4">sa</w> <w n="27.5">main</w> <w n="27.6">fiévreuse</w> <w n="27.7">elle</w> <w n="27.8">s</w>’<w n="27.9">accoude</w>,</l>
						<l n="28" num="7.4"><w n="28.1">Ne</w> <w n="28.2">livraient</w> <w n="28.3">le</w> <w n="28.4">secret</w> <w n="28.5">des</w> <w n="28.6">premiers</w> <w n="28.7">cheveux</w> <w n="28.8">blancs</w>.</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1"><w n="29.1">Partout</w> <w n="29.2">le</w> <w n="29.3">souvenir</w> <w n="29.4">de</w> <w n="29.5">l</w>’<w n="29.6">absent</w> <w n="29.7">se</w> <w n="29.8">rencontre</w></l>
						<l n="30" num="8.2"><w n="30.1">En</w> <w n="30.2">mille</w> <w n="30.3">objets</w> <w n="30.4">fanés</w> <w n="30.5">et</w> <w n="30.6">déjà</w> <w n="30.7">presque</w> <w n="30.8">anciens</w></l>
						<l n="31" num="8.3"><w n="31.1">Cette</w> <w n="31.2">lunette</w> <w n="31.3">en</w> <w n="31.4">cuivre</w> <w n="31.5">est</w> <w n="31.6">à</w> <w n="31.7">lui</w>, <w n="31.8">cette</w> <w n="31.9">montre</w></l>
						<l n="32" num="8.4"><w n="32.1">Est</w> <w n="32.2">la</w> <w n="32.3">sienne</w>, <w n="32.4">et</w> <w n="32.5">ces</w> <w n="32.6">vieux</w> <w n="32.7">instruments</w> <w n="32.8">sont</w> <w n="32.9">les</w> <w n="32.10">siens</w>.</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1"><w n="33.1">Il</w> <w n="33.2">a</w> <w n="33.3">laissé</w>, <w n="33.4">de</w> <w n="33.5">peur</w> <w n="33.6">d</w>’<w n="33.7">encombrer</w> <w n="33.8">sa</w> <w n="33.9">cabine</w>,</l>
						<l n="34" num="9.2"><w n="34.1">Ces</w> <w n="34.2">gros</w> <w n="34.3">livres</w> <w n="34.4">poudreux</w> <w n="34.5">dans</w> <w n="34.6">leur</w> <w n="34.7">oubli</w> <w n="34.8">profond</w>,</l>
						<l n="35" num="9.3"><w n="35.1">Et</w> <w n="35.2">c</w>’<w n="35.3">est</w> <w n="35.4">lui</w> <w n="35.5">qui</w> <w n="35.6">tua</w> <w n="35.7">d</w>’<w n="35.8">un</w> <w n="35.9">coup</w> <w n="35.10">de</w> <w n="35.11">carabine</w></l>
						<l n="36" num="9.4"><w n="36.1">Le</w> <w n="36.2">monstrueux</w> <w n="36.3">lézard</w> <w n="36.4">qui</w> <w n="36.5">s</w>’<w n="36.6">étale</w> <w n="36.7">au</w> <w n="36.8">plafond</w>.</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1"><w n="37.1">Ces</w> <w n="37.2">mille</w> <w n="37.3">riens</w>, <w n="37.4">décor</w> <w n="37.5">naïf</w> <w n="37.6">de</w> <w n="37.7">la</w> <w n="37.8">muraille</w>,</l>
						<l n="38" num="10.2"><w n="38.1">Naguère</w>, <w n="38.2">il</w> <w n="38.3">les</w> <w n="38.4">a</w> <w n="38.5">tous</w> <w n="38.6">apportés</w> <w n="38.7">de</w> <w n="38.8">très</w> <w n="38.9">loin</w>.</l>
						<l n="39" num="10.3"><w n="39.1">Seule</w>, <w n="39.2">comme</w> <w n="39.3">un</w> <w n="39.4">témoin</w> <w n="39.5">inclément</w> <w n="39.6">et</w> <w n="39.7">qui</w> <w n="39.8">raille</w>,</l>
						<l n="40" num="10.4"><w n="40.1">Une</w> <w n="40.2">carte</w> <w n="40.3">navale</w> <w n="40.4">est</w> <w n="40.5">pendue</w> <w n="40.6">en</w> <w n="40.7">un</w> <w n="40.8">coin</w> ;</l>
					</lg>
					<lg n="11">
						<l n="41" num="11.1"><w n="41.1">Sur</w> <w n="41.2">le</w> <w n="41.3">tableau</w> <w n="41.4">jaunâtre</w>, <w n="41.5">entre</w> <w n="41.6">ses</w> <w n="41.7">noires</w> <w n="41.8">tringles</w>,</l>
						<l n="42" num="11.2"><w n="42.1">Les</w> <w n="42.2">vents</w> <w n="42.3">et</w> <w n="42.4">les</w> <w n="42.5">courants</w> <w n="42.6">se</w> <w n="42.7">croisent</w> <w n="42.8">à</w> <w n="42.9">l</w>’<w n="42.10">envi</w> :</l>
						<l n="43" num="11.3"><w n="43.1">Et</w> <w n="43.2">la</w> <w n="43.3">succession</w> <w n="43.4">des</w> <w n="43.5">petites</w> <w n="43.6">épingles</w></l>
						<l n="44" num="11.4"><w n="44.1">N</w>’<w n="44.2">a</w> <w n="44.3">pas</w> <w n="44.4">marqué</w> <w n="44.5">longtemps</w> <w n="44.6">le</w> <w n="44.7">voyage</w> <w n="44.8">suivi</w>.</l>
					</lg>
					<lg n="12">
						<l n="45" num="12.1"><w n="45.1">Elle</w> <w n="45.2">conduit</w> <w n="45.3">jusqu</w>’<w n="45.4">à</w> <w n="45.5">la</w> <w n="45.6">ligne</w> <w n="45.7">tropicale</w></l>
						<l n="46" num="12.2"><w n="46.1">Le</w> <w n="46.2">navire</w> <w n="46.3">vainqueur</w> <w n="46.4">du</w> <w n="46.5">flux</w> <w n="46.6">et</w> <w n="46.7">du</w> <w n="46.8">reflux</w>,</l>
						<l n="47" num="12.3"><w n="47.1">Puis</w> <w n="47.2">cesse</w> <w n="47.3">brusquement</w> <w n="47.4">à</w> <w n="47.5">la</w> <w n="47.6">dernière</w> <w n="47.7">escale</w>,</l>
						<l n="48" num="12.4"><w n="48.1">Celle</w> <w n="48.2">d</w>’<w n="48.3">où</w> <w n="48.4">le</w> <w n="48.5">marin</w>, <w n="48.6">hélas</w> ! <w n="48.7">n</w>’<w n="48.8">écrivit</w> <w n="48.9">plus</w>.</l>
					</lg>
					<lg n="13">
						<l n="49" num="13.1"><w n="49.1">Et</w> <w n="49.2">ce</w> <w n="49.3">point</w> <w n="49.4">justement</w> <w n="49.5">où</w> <w n="49.6">sa</w> <w n="49.7">trace</w> <w n="49.8">s</w>’<w n="49.9">arrête</w></l>
						<l n="50" num="13.2"><w n="50.1">Est</w> <w n="50.2">celui</w> <w n="50.3">qu</w>’<w n="50.4">un</w> <w n="50.5">burin</w> <w n="50.6">savant</w> <w n="50.7">fit</w> <w n="50.8">le</w> <w n="50.9">plus</w> <w n="50.10">noir</w> :</l>
						<l n="51" num="13.3"><w n="51.1">C</w>’<w n="51.2">est</w> <w n="51.3">l</w>’<w n="51.4">obscur</w> <w n="51.5">rendez</w>-<w n="51.6">vous</w> <w n="51.7">des</w> <w n="51.8">flots</w> <w n="51.9">où</w> <w n="51.10">la</w> <w n="51.11">tempête</w></l>
						<l n="52" num="13.4"><w n="52.1">Creuse</w> <w n="52.2">un</w> <w n="52.3">inexorable</w> <w n="52.4">et</w> <w n="52.5">profond</w> <w n="52.6">entonnoir</w>.</l>
					</lg>
					<lg n="14">
						<l n="53" num="14.1"><w n="53.1">Mais</w> <w n="53.2">elle</w> <w n="53.3">ne</w> <w n="53.4">voit</w> <w n="53.5">pas</w> <w n="53.6">le</w> <w n="53.7">tableau</w> <w n="53.8">redoutable</w></l>
						<l n="54" num="14.2"><w n="54.1">Et</w> <w n="54.2">feuillette</w>, <w n="54.3">l</w>’<w n="54.4">esprit</w> <w n="54.5">ailleurs</w>, <w n="54.6">du</w> <w n="54.7">bout</w> <w n="54.8">des</w> <w n="54.9">doigts</w>,</l>
						<l n="55" num="14.3"><w n="55.1">Les</w> <w n="55.2">planches</w> <w n="55.3">d</w>’<w n="55.4">un</w> <w n="55.5">herbier</w> <w n="55.6">éparses</w> <w n="55.7">sur</w> <w n="55.8">la</w> <w n="55.9">table</w>,</l>
						<l n="56" num="14.4"><w n="56.1">Fleurs</w> <w n="56.2">pâles</w> <w n="56.3">qu</w>’<w n="56.4">il</w> <w n="56.5">cueillit</w> <w n="56.6">aux</w> <w n="56.7">Indes</w> <w n="56.8">autrefois</w>.</l>
					</lg>
					<lg n="15">
						<l n="57" num="15.1"><w n="57.1">Jusqu</w>’<w n="57.2">au</w> <w n="57.3">soir</w> <w n="57.4">sa</w> <w n="57.5">pensée</w> <w n="57.6">extatique</w> <w n="57.7">et</w> <w n="57.8">sereine</w></l>
						<l n="58" num="15.2"><w n="58.1">Songe</w> <w n="58.2">au</w> <w n="58.3">chemin</w> <w n="58.4">qu</w>’<w n="58.5">il</w> <w n="58.6">fait</w> <w n="58.7">en</w> <w n="58.8">mer</w> <w n="58.9">pour</w> <w n="58.10">revenir</w>,</l>
						<l n="59" num="15.3"><w n="59.1">Ou</w> <w n="59.2">parfois</w>, <w n="59.3">évoquant</w> <w n="59.4">des</w> <w n="59.5">jours</w> <w n="59.6">meilleurs</w>, <w n="59.7">égrène</w></l>
						<l n="60" num="15.4"><w n="60.1">Le</w> <w n="60.2">chapelet</w> <w n="60.3">mystique</w> <w n="60.4">et</w> <w n="60.5">doux</w> <w n="60.6">du</w> <w n="60.7">souvenir</w> ;</l>
					</lg>
					<lg n="16">
						<l n="61" num="16.1"><w n="61.1">Et</w>, <w n="61.2">quand</w> <w n="61.3">sur</w> <w n="61.4">l</w>’<w n="61.5">Océan</w> <w n="61.6">la</w> <w n="61.7">nuit</w> <w n="61.8">met</w> <w n="61.9">son</w> <w n="61.10">mystère</w>,</l>
						<l n="62" num="16.2"><w n="62.1">Calme</w> <w n="62.2">et</w> <w n="62.3">fermant</w> <w n="62.4">les</w> <w n="62.5">yeux</w>, <w n="62.6">elle</w> <w n="62.7">rêve</w> <w n="62.8">du</w> <w n="62.9">chant</w></l>
						<l n="63" num="16.3"><w n="63.1">Des</w> <w n="63.2">matelots</w> <w n="63.3">joyeux</w> <w n="63.4">d</w>’<w n="63.5">apercevoir</w> <w n="63.6">la</w> <w n="63.7">terre</w>,</l>
						<l n="64" num="16.4"><w n="64.1">Et</w> <w n="64.2">d</w>’<w n="64.3">un</w> <w n="64.4">navire</w> <w n="64.5">d</w>’<w n="64.6">or</w> <w n="64.7">dans</w> <w n="64.8">le</w> <w n="64.9">soleil</w> <w n="64.10">couchant</w>.</l>
					</lg>
				</div></body></text></TEI>