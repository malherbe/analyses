<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE CAHIER ROUGE</title>
				<title type="medium">Édition électronique</title>
				<author key="COP">
					<name>
						<forename>François</forename>
						<surname>COPPÉE</surname>
					</name>
					<date from="1842" to="1908">1842-1908</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1198 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">COP_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE CAHIER ROUGE</title>
						<author>François Coppée</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/francoiscopeelecahierrouge.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Œuvres complètes de François Coppée</title>
						<author>François Coppée</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie L. Hébert</publisher>
							<date when="1885">1885</date>
						</imprint>
						<biblScope unit="tome">Poésie, tome 2</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1874">1874</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="COP70">
				<head type="main">Presque une fable</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Un</w> <w n="1.2">liseron</w>, <w n="1.3">madame</w>, <w n="1.4">aimait</w> <w n="1.5">une</w> <w n="1.6">fauvette</w>.</l>
					<l n="2" num="1.2">— <w n="2.1">Vous</w> <w n="2.2">pardonnerez</w> <w n="2.3">bien</w> <w n="2.4">cette</w> <w n="2.5">idée</w> <w n="2.6">au</w> <w n="2.7">poète</w></l>
					<l n="3" num="1.3"><w n="3.1">Qu</w>’<w n="3.2">une</w> <w n="3.3">plante</w> <w n="3.4">puisse</w> <w n="3.5">être</w> <w n="3.6">éprise</w> <w n="3.7">d</w>’<w n="3.8">un</w> <w n="3.9">oiseau</w>. —</l>
					<l n="4" num="1.4"><w n="4.1">Un</w> <w n="4.2">liseron</w> <w n="4.3">des</w> <w n="4.4">bois</w>, <w n="4.5">éclos</w> <w n="4.6">près</w> <w n="4.7">d</w>’<w n="4.8">un</w> <w n="4.9">ruisseau</w>,</l>
					<l n="5" num="1.5"><w n="5.1">Au</w> <w n="5.2">fond</w> <w n="5.3">du</w> <w n="5.4">parc</w>, <w n="5.5">au</w> <w n="5.6">bout</w> <w n="5.7">du</w> <w n="5.8">vieux</w> <w n="5.9">mur</w> <w n="5.10">plein</w> <w n="5.11">de</w> <w n="5.12">brèches</w>,</l>
					<l n="6" num="1.6"><w n="6.1">Et</w> <w n="6.2">qui</w>, <w n="6.3">triste</w>, <w n="6.4">rampait</w> <w n="6.5">parmi</w> <w n="6.6">les</w> <w n="6.7">feuilles</w> <w n="6.8">sèches</w>,</l>
					<l n="7" num="1.7"><w n="7.1">Écoutant</w> <w n="7.2">cette</w> <w n="7.3">voix</w> <w n="7.4">d</w>’<w n="7.5">oiseau</w> <w n="7.6">dans</w> <w n="7.7">un</w> <w n="7.8">tilleul</w>,</l>
					<l n="8" num="1.8"><w n="8.1">Était</w> <w n="8.2">au</w> <w n="8.3">désespoir</w> <w n="8.4">de</w> <w n="8.5">fleurir</w> <w n="8.6">pour</w> <w n="8.7">lui</w> <w n="8.8">seul</w>.</l>
					<l n="9" num="1.9"><w n="9.1">Il</w> <w n="9.2">voulut</w> <w n="9.3">essayer</w>, <w n="9.4">s</w>’<w n="9.5">il</w> <w n="9.6">en</w> <w n="9.7">avait</w> <w n="9.8">la</w> <w n="9.9">force</w>,</l>
					<l n="10" num="1.10"><w n="10.1">D</w>’<w n="10.2">enlacer</w> <w n="10.3">ce</w> <w n="10.4">grand</w> <w n="10.5">arbre</w> <w n="10.6">à</w> <w n="10.7">la</w> <w n="10.8">rugueuse</w> <w n="10.9">écorce</w></l>
					<l n="11" num="1.11"><w n="11.1">Et</w> <w n="11.2">de</w> <w n="11.3">grimper</w> <w n="11.4">là</w>-<w n="11.5">haut</w>, <w n="11.6">là</w>-<w n="11.7">haut</w>, <w n="11.8">près</w> <w n="11.9">de</w> <w n="11.10">ce</w> <w n="11.11">nid</w>.</l>
					<l n="12" num="1.12"><w n="12.1">Il</w> <w n="12.2">croyait</w>, <w n="12.3">l</w>’<w n="12.4">innocent</w>, <w n="12.5">que</w> <w n="12.6">quelque</w> <w n="12.7">chose</w> <w n="12.8">unit</w></l>
					<l n="13" num="1.13"><w n="13.1">Ce</w> <w n="13.2">qui</w> <w n="13.3">pousse</w> <w n="13.4">et</w> <w n="13.5">fleurit</w> <w n="13.6">à</w> <w n="13.7">ce</w> <w n="13.8">qui</w> <w n="13.9">vole</w> <w n="13.10">et</w> <w n="13.11">chante</w>.</l>
					<l n="14" num="1.14">— <w n="14.1">Moi</w>, <w n="14.2">son</w> <w n="14.3">ambition</w> <w n="14.4">me</w> <w n="14.5">semble</w> <w n="14.6">assez</w> <w n="14.7">touchante</w>,</l>
					<l n="15" num="1.15"><w n="15.1">Madame</w>. <w n="15.2">Vous</w> <w n="15.3">savez</w> <w n="15.4">que</w> <w n="15.5">les</w> <w n="15.6">amants</w> <w n="15.7">sont</w> <w n="15.8">fous</w></l>
					<l n="16" num="1.16"><w n="16.1">Et</w> <w n="16.2">ce</w> <w n="16.3">qu</w>’<w n="16.4">ils</w> <w n="16.5">tenteraient</w> <w n="16.6">pour</w> <w n="16.7">être</w> <w n="16.8">auprès</w> <w n="16.9">de</w> <w n="16.10">vous</w>. —</l>
					<l n="17" num="1.17"><w n="17.1">Comme</w> <w n="17.2">le</w> <w n="17.3">chasseur</w> <w n="17.4">grec</w>, <w n="17.5">pour</w> <w n="17.6">surprendre</w> <w n="17.7">Diane</w>,</l>
					<l n="18" num="1.18"><w n="18.1">Suivait</w> <w n="18.2">le</w> <w n="18.3">son</w> <w n="18.4">lointain</w> <w n="18.5">du</w> <w n="18.6">cor</w>, <w n="18.7">l</w>’<w n="18.8">humble</w> <w n="18.9">liane</w>,</l>
					<l n="19" num="1.19"><w n="19.1">De</w> <w n="19.2">ses</w> <w n="19.3">clochetons</w> <w n="19.4">bleus</w> <w n="19.5">semant</w> <w n="19.6">le</w> <w n="19.7">chapelet</w>,</l>
					<l n="20" num="1.20"><w n="20.1">Monta</w> <w n="20.2">donc</w> <w n="20.3">vers</w> <w n="20.4">l</w>’<w n="20.5">oiseau</w> <w n="20.6">que</w> <w n="20.7">son</w> <w n="20.8">chant</w> <w n="20.9">décelait</w>.</l>
					<l n="21" num="1.21"><w n="21.1">Atteindre</w> <w n="21.2">la</w> <w n="21.3">fauvette</w> <w n="21.4">et</w> <w n="21.5">la</w> <w n="21.6">charmer</w>, <w n="21.7">quel</w> <w n="21.8">rêve</w> !</l>
					<l n="22" num="1.22"><w n="22.1">Hélas</w> ! <w n="22.2">c</w>’<w n="22.3">était</w> <w n="22.4">trop</w> <w n="22.5">beau</w> ; <w n="22.6">car</w> <w n="22.7">la</w> <w n="22.8">goutte</w> <w n="22.9">de</w> <w n="22.10">sève</w></l>
					<l n="23" num="1.23"><w n="23.1">Que</w> <w n="23.2">la</w> <w n="23.3">terre</w> <w n="23.4">donnait</w> <w n="23.5">à</w> <w n="23.6">ce</w> <w n="23.7">frêle</w> <w n="23.8">sarment</w></l>
					<l n="24" num="1.24"><w n="24.1">S</w>’<w n="24.2">épuisait</w>. <w n="24.3">Il</w> <w n="24.4">montait</w>, <w n="24.5">toujours</w> <w n="24.6">plus</w> <w n="24.7">lentement</w> ;</l>
					<l n="25" num="1.25"><w n="25.1">Chaque</w> <w n="25.2">matin</w>, <w n="25.3">sa</w> <w n="25.4">fleur</w> <w n="25.5">devenait</w> <w n="25.6">plus</w> <w n="25.7">débile</w> ;</l>
					<l n="26" num="1.26"><w n="26.1">Puis</w>, <w n="26.2">bien</w> <w n="26.3">que</w> <w n="26.4">liseron</w>, <w n="26.5">il</w> <w n="26.6">était</w> <w n="26.7">malhabile</w>,</l>
					<l n="27" num="1.27"><w n="27.1">Lui</w>, <w n="27.2">né</w> <w n="27.3">dans</w> <w n="27.4">l</w>’<w n="27.5">herbe</w> <w n="27.6">courte</w> <w n="27.7">où</w> <w n="27.8">vivent</w> <w n="27.9">les</w> <w n="27.10">fourmis</w>,</l>
					<l n="28" num="1.28"><w n="28.1">A</w> <w n="28.2">gravir</w> <w n="28.3">ces</w> <w n="28.4">sommets</w> <w n="28.5">aux</w> <w n="28.6">écureuils</w> <w n="28.7">permis</w>.</l>
					<l n="29" num="1.29"><w n="29.1">Là</w>, <w n="29.2">le</w> <w n="29.3">vent</w> <w n="29.4">est</w> <w n="29.5">trop</w> <w n="29.6">rude</w> <w n="29.7">et</w> <w n="29.8">l</w>’<w n="29.9">ombre</w> <w n="29.10">est</w> <w n="29.11">trop</w> <w n="29.12">épaisse</w>.</l>
					<l n="30" num="1.30">— <w n="30.1">Mais</w> <w n="30.2">tous</w> <w n="30.3">les</w> <w n="30.4">amoureux</w> <w n="30.5">sont</w> <w n="30.6">de</w> <w n="30.7">la</w> <w n="30.8">même</w> <w n="30.9">espèce</w>,</l>
					<l n="31" num="1.31"><w n="31.1">Madame</w>, — <w n="31.2">et</w> <w n="31.3">vers</w> <w n="31.4">le</w> <w n="31.5">nid</w> <w n="31.6">d</w>’<w n="31.7">où</w> <w n="31.8">venait</w> <w n="31.9">cette</w> <w n="31.10">voix</w></l>
					<l n="32" num="1.32"><w n="32.1">Montait</w>, <w n="32.2">montait</w> <w n="32.3">toujours</w> <w n="32.4">le</w> <w n="32.5">liseron</w> <w n="32.6">des</w> <w n="32.7">bois</w>.</l>
					<l n="33" num="1.33"><w n="33.1">Enfin</w>, <w n="33.2">comme</w> <w n="33.3">il</w> <w n="33.4">touchait</w> <w n="33.5">au</w> <w n="33.6">but</w> <w n="33.7">de</w> <w n="33.8">son</w> <w n="33.9">voyage</w>,</l>
					<l n="34" num="1.34"><w n="34.1">Il</w> <w n="34.2">ne</w> <w n="34.3">put</w> <w n="34.4">supporter</w> <w n="34.5">la</w> <w n="34.6">fraîcheur</w> <w n="34.7">du</w> <w n="34.8">feuillage</w></l>
					<l n="35" num="1.35"><w n="35.1">Et</w> <w n="35.2">mourut</w>, <w n="35.3">en</w> <w n="35.4">donnant</w>, <w n="35.5">le</w> <w n="35.6">jour</w> <w n="35.7">de</w> <w n="35.8">son</w> <w n="35.9">trépas</w>,</l>
					<l n="36" num="1.36"><w n="36.1">Une</w> <w n="36.2">dernière</w> <w n="36.3">fleur</w> <w n="36.4">que</w> <w n="36.5">l</w>’<w n="36.6">oiseau</w> <w n="36.7">ne</w> <w n="36.8">vit</w> <w n="36.9">pas</w>.</l>
					<l n="37" num="1.37">— <w n="37.1">Comment</w> ? <w n="37.2">vous</w> <w n="37.3">soupirez</w> <w n="37.4">et</w> <w n="37.5">vous</w> <w n="37.6">baissez</w> <w n="37.7">la</w> <w n="37.8">tête</w>,</l>
					<l n="38" num="1.38"><w n="38.1">Madame</w>…<w n="38.2">Un</w> <w n="38.3">liseron</w> <w n="38.4">adore</w> <w n="38.5">une</w> <w n="38.6">fauvette</w>.</l>
				</lg>
			</div></body></text></TEI>