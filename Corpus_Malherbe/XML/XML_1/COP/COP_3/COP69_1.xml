<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE CAHIER ROUGE</title>
				<title type="medium">Édition électronique</title>
				<author key="COP">
					<name>
						<forename>François</forename>
						<surname>COPPÉE</surname>
					</name>
					<date from="1842" to="1908">1842-1908</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1198 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">COP_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE CAHIER ROUGE</title>
						<author>François Coppée</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/francoiscopeelecahierrouge.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Œuvres complètes de François Coppée</title>
						<author>François Coppée</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie L. Hébert</publisher>
							<date when="1885">1885</date>
						</imprint>
						<biblScope unit="tome">Poésie, tome 2</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1874">1874</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="COP69">
				<head type="main">Blessure rouverte</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">O</w> <w n="1.2">mon</w> <w n="1.3">cœur</w>, <w n="1.4">es</w>-<w n="1.5">tu</w> <w n="1.6">donc</w> <w n="1.7">si</w> <w n="1.8">débile</w> <w n="1.9">et</w> <w n="1.10">si</w> <w n="1.11">lâche</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Et</w> <w n="2.2">serais</w>-<w n="2.3">tu</w> <w n="2.4">pareil</w> <w n="2.5">au</w> <w n="2.6">forçat</w> <w n="2.7">qu</w>’<w n="2.8">on</w> <w n="2.9">relâche</w></l>
					<l n="3" num="1.3"><w n="3.1">Et</w> <w n="3.2">qui</w> <w n="3.3">boite</w> <w n="3.4">toujours</w> <w n="3.5">de</w> <w n="3.6">son</w> <w n="3.7">boulet</w> <w n="3.8">traîné</w> ?</l>
					<l n="4" num="1.4"><w n="4.1">Tais</w>-<w n="4.2">toi</w>, <w n="4.3">car</w> <w n="4.4">tu</w> <w n="4.5">sais</w> <w n="4.6">bien</w> <w n="4.7">qu</w>’<w n="4.8">elle</w> <w n="4.9">t</w>’<w n="4.10">a</w> <w n="4.11">condamné</w>.</l>
					<l n="5" num="1.5"><w n="5.1">Je</w> <w n="5.2">ne</w> <w n="5.3">veux</w> <w n="5.4">plus</w> <w n="5.5">souffrir</w> <w n="5.6">et</w> <w n="5.7">je</w> <w n="5.8">t</w>’<w n="5.9">en</w> <w n="5.10">donne</w> <w n="5.11">l</w>’<w n="5.12">ordre</w>.</l>
					<l n="6" num="1.6"><w n="6.1">Si</w> <w n="6.2">je</w> <w n="6.3">te</w> <w n="6.4">sens</w> <w n="6.5">encor</w> <w n="6.6">te</w> <w n="6.7">gonfler</w> <w n="6.8">et</w> <w n="6.9">te</w> <w n="6.10">tordre</w>,</l>
					<l n="7" num="1.7"><w n="7.1">Je</w> <w n="7.2">veux</w>, <w n="7.3">dans</w> <w n="7.4">un</w> <w n="7.5">sanglot</w> <w n="7.6">contenu</w>, <w n="7.7">te</w> <w n="7.8">broyer</w> ;</l>
					<l n="8" num="1.8"><w n="8.1">Et</w> <w n="8.2">l</w>’<w n="8.3">on</w> <w n="8.4">n</w>’<w n="8.5">en</w> <w n="8.6">saura</w> <w n="8.7">rien</w>, <w n="8.8">et</w>, <w n="8.9">pour</w> <w n="8.10">ne</w> <w n="8.11">pas</w> <w n="8.12">crier</w>,</l>
					<l n="9" num="1.9"><w n="9.1">On</w> <w n="9.2">me</w> <w n="9.3">verra</w>, <w n="9.4">pendant</w> <w n="9.5">l</w>’<w n="9.6">effroyable</w> <w n="9.7">minute</w>,</l>
					<l n="10" num="1.10"><w n="10.1">Serrer</w> <w n="10.2">les</w> <w n="10.3">dents</w> <w n="10.4">ainsi</w> <w n="10.5">qu</w>’<w n="10.6">un</w> <w n="10.7">soldat</w> <w n="10.8">qu</w>’<w n="10.9">on</w> <w n="10.10">ampute</w>.</l>
				</lg>
			</div></body></text></TEI>