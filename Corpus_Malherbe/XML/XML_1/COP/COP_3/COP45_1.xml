<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE CAHIER ROUGE</title>
				<title type="medium">Édition électronique</title>
				<author key="COP">
					<name>
						<forename>François</forename>
						<surname>COPPÉE</surname>
					</name>
					<date from="1842" to="1908">1842-1908</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1198 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">COP_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE CAHIER ROUGE</title>
						<author>François Coppée</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/francoiscopeelecahierrouge.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Œuvres complètes de François Coppée</title>
						<author>François Coppée</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie L. Hébert</publisher>
							<date when="1885">1885</date>
						</imprint>
						<biblScope unit="tome">Poésie, tome 2</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1874">1874</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="COP45">
				<head type="main">Fantaisie nostalgique</head>
				<opener>
					<salute>A Sully-Prudhomme</salute>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">D</w>’<w n="1.2">être</w> <w n="1.3">ou</w> <w n="1.4">de</w> <w n="1.5">n</w>’<w n="1.6">être</w> <w n="1.7">pas</w> <w n="1.8">je</w> <w n="1.9">n</w>’<w n="1.10">ai</w> <w n="1.11">point</w> <w n="1.12">eu</w> <w n="1.13">le</w> <w n="1.14">choix</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Mais</w>, <w n="2.2">dans</w> <w n="2.3">ce</w> <w n="2.4">siècle</w> <w n="2.5">vide</w>, <w n="2.6">ennuyeux</w> <w n="2.7">et</w> <w n="2.8">bourgeois</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Je</w> <w n="3.2">suis</w> <w n="3.3">comme</w> <w n="3.4">un</w> <w n="3.5">enfant</w> <w n="3.6">volé</w> <w n="3.7">par</w> <w n="3.8">des</w> <w n="3.9">tziganes</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Qui</w> <w n="4.2">chassa</w> <w n="4.3">les</w> <w n="4.4">oiseaux</w> <w n="4.5">avec</w> <w n="4.6">des</w> <w n="4.7">sarbacanes</w>,</l>
					<l n="5" num="1.5"><w n="5.1">Et</w> <w n="5.2">devint</w> <w n="5.3">saltimbanque</w> <w n="5.4">et</w> <w n="5.5">joueur</w> <w n="5.6">de</w> <w n="5.7">guzla</w>.</l>
					<l n="6" num="1.6"><w n="6.1">Longtemps</w> <w n="6.2">il</w> <w n="6.3">n</w>’<w n="6.4">a</w> <w n="6.5">mangé</w> <w n="6.6">que</w> <w n="6.7">le</w> <w n="6.8">pain</w> <w n="6.9">qu</w>’<w n="6.10">il</w> <w n="6.11">vola</w>,</l>
					<l n="7" num="1.7"><w n="7.1">Et</w>, <w n="7.2">comme</w> <w n="7.3">un</w> <w n="7.4">loup</w>, <w n="7.5">il</w> <w n="7.6">n</w>’<w n="7.7">eut</w> <w n="7.8">que</w> <w n="7.9">les</w> <w n="7.10">bois</w> <w n="7.11">pour</w> <w n="7.12">repaire</w>.</l>
					<l n="8" num="1.8"><w n="8.1">Puis</w>, <w n="8.2">un</w> <w n="8.3">beau</w> <w n="8.4">jour</w>, <w n="8.5">il</w> <w n="8.6">est</w> <w n="8.7">retrouvé</w> <w n="8.8">par</w> <w n="8.9">son</w> <w n="8.10">père</w>,</l>
					<l n="9" num="1.9"><w n="9.1">Un</w> <w n="9.2">magnat</w>, <w n="9.3">tout</w> <w n="9.4">couvert</w> <w n="9.5">de</w> <w n="9.6">fourrure</w> <w n="9.7">et</w> <w n="9.8">d</w>’<w n="9.9">acier</w>,</l>
					<l n="10" num="1.10"><w n="10.1">Portant</w> <w n="10.2">l</w>’<w n="10.3">aigrette</w> <w n="10.4">blanche</w> <w n="10.5">à</w> <w n="10.6">son</w> <w n="10.7">bonnet</w> <w n="10.8">princier</w>.</l>
					<l n="11" num="1.11"><w n="11.1">Le</w> <w n="11.2">vieil</w> <w n="11.3">homme</w> <w n="11.4">l</w>’<w n="11.5">emporte</w> <w n="11.6">en</w> <w n="11.7">sanglotant</w> <w n="11.8">de</w> <w n="11.9">joie</w>.</l>
					<l n="12" num="1.12"><w n="12.1">On</w> <w n="12.2">habille</w> <w n="12.3">l</w>’<w n="12.4">enfant</w> <w n="12.5">de</w> <w n="12.6">velours</w> <w n="12.7">et</w> <w n="12.8">de</w> <w n="12.9">soie</w> ;</l>
					<l n="13" num="1.13"><w n="13.1">Il</w> <w n="13.2">couche</w> <w n="13.3">sur</w> <w n="13.4">la</w> <w n="13.5">plume</w> <w n="13.6">et</w> <w n="13.7">mange</w> <w n="13.8">dans</w> <w n="13.9">de</w> <w n="13.10">l</w>’<w n="13.11">or</w>.</l>
					<l n="14" num="1.14"><w n="14.1">Quand</w> <w n="14.2">il</w> <w n="14.3">rentre</w> <w n="14.4">au</w> <w n="14.5">château</w>, <w n="14.6">le</w> <w n="14.7">nain</w> <w n="14.8">sonne</w> <w n="14.9">du</w> <w n="14.10">cor</w>,</l>
					<l n="15" num="1.15"><w n="15.1">Et</w>, <w n="15.2">monté</w> <w n="15.3">comme</w> <w n="15.4">lui</w> <w n="15.5">sur</w> <w n="15.6">un</w> <w n="15.7">genet</w> <w n="15.8">d</w>’<w n="15.9">Espagne</w>,</l>
					<l n="16" num="1.16"><w n="16.1">Un</w> <w n="16.2">antique</w> <w n="16.3">écuyer</w> <w n="16.4">balafré</w> <w n="16.5">l</w>’<w n="16.6">accompagne</w>.</l>
					<l n="17" num="1.17"><w n="17.1">Un</w> <w n="17.2">clerc</w>, <w n="17.3">très</w> <w n="17.4">patient</w>, <w n="17.5">lui</w> <w n="17.6">donne</w> <w n="17.7">des</w> <w n="17.8">leçons</w>.</l>
					<l n="18" num="1.18"><w n="18.1">Son</w> <w n="18.2">père</w>, <w n="18.3">en</w> <w n="18.4">son</w> <w n="18.5">fauteuil</w> <w n="18.6">tout</w> <w n="18.7">chargé</w> <w n="18.8">d</w>’<w n="18.9">écussons</w>,</l>
					<l n="19" num="1.19"><w n="19.1">L</w>’<w n="19.2">attire</w> <w n="19.3">quelquefois</w> <w n="19.4">tendrement</w>, <w n="19.5">puis</w> <w n="19.6">se</w> <w n="19.7">penche</w></l>
					<l n="20" num="1.20"><w n="20.1">Et</w> <w n="20.2">longtemps</w> <w n="20.3">le</w> <w n="20.4">caresse</w> <w n="20.5">avec</w> <w n="20.6">sa</w> <w n="20.7">barbe</w> <w n="20.8">blanche</w>.</l>
					<l n="21" num="1.21"><w n="21.1">Des</w> <w n="21.2">femmes</w>, <w n="21.3">dont</w> <w n="21.4">les</w> <w n="21.5">yeux</w> <w n="21.6">sont</w> <w n="21.7">doux</w> <w n="21.8">comme</w> <w n="21.9">les</w> <w n="21.10">mains</w>,</l>
					<l n="22" num="1.22"><w n="22.1">Baisent</w> <w n="22.2">son</w> <w n="22.3">front</w> <w n="22.4">hâlé</w> <w n="22.5">par</w> <w n="22.6">le</w> <w n="22.7">vent</w> <w n="22.8">des</w> <w n="22.9">chemins</w></l>
					<l n="23" num="1.23"><w n="23.1">Et</w> <w n="23.2">détachent</w> <w n="23.3">pour</w> <w n="23.4">lui</w> <w n="23.5">le</w> <w n="23.6">bijou</w> <w n="23.7">qui</w> <w n="23.8">l</w>’<w n="23.9">occupe</w>,</l>
					<l n="24" num="1.24"><w n="24.1">Ne</w> <w n="24.2">sachant</w> <w n="24.3">pas</w> <w n="24.4">qu</w>’<w n="24.5">il</w> <w n="24.6">sent</w> <w n="24.7">leurs</w> <w n="24.8">genoux</w> <w n="24.9">sous</w> <w n="24.10">la</w> <w n="24.11">jupe</w></l>
					<l n="25" num="1.25"><w n="25.1">Et</w> <w n="25.2">qu</w>’<w n="25.3">au</w> <w n="25.4">pays</w> <w n="25.5">bohème</w> <w n="25.6">où</w> <w n="25.7">l</w>’<w n="25.8">enfant</w> <w n="25.9">voyagea</w>,</l>
					<l n="26" num="1.26"><w n="26.1">Avant</w> <w n="26.2">d</w>’<w n="26.3">avoir</w> <w n="26.4">quinze</w> <w n="26.5">ans</w> <w n="26.6">on</w> <w n="26.7">est</w> <w n="26.8">homme</w> <w n="26.9">déjà</w>.</l>
					<l n="27" num="1.27"><w n="27.1">Mais</w> <w n="27.2">ni</w> <w n="27.3">les</w> <w n="27.4">beaux</w> <w n="27.5">habits</w>, <w n="27.6">ni</w> <w n="27.7">les</w> <w n="27.8">tables</w> <w n="27.9">chargées</w></l>
					<l n="28" num="1.28"><w n="28.1">De</w> <w n="28.2">gâteaux</w> <w n="28.3">délicats</w>, <w n="28.4">de</w> <w n="28.5">fruits</w> <w n="28.6">et</w> <w n="28.7">de</w> <w n="28.8">dragées</w>,</l>
					<l n="29" num="1.29"><w n="29.1">Ni</w> <w n="29.2">le</w> <w n="29.3">vieil</w> <w n="29.4">écuyer</w> <w n="29.5">qui</w> <w n="29.6">lui</w> <w n="29.7">dit</w> <w n="29.8">ses</w> <w n="29.9">combats</w>,</l>
					<l n="30" num="1.30"><w n="30.1">Ni</w> <w n="30.2">les</w> <w n="30.3">propos</w> <w n="30.4">du</w> <w n="30.5">clerc</w> <w n="30.6">qui</w> <w n="30.7">le</w> <w n="30.8">flatte</w> <w n="30.9">tout</w> <w n="30.10">bas</w>,</l>
					<l n="31" num="1.31"><w n="31.1">Ni</w> <w n="31.2">les</w> <w n="31.3">doux</w> <w n="31.4">oreillers</w> <w n="31.5">de</w> <w n="31.6">la</w> <w n="31.7">profonde</w> <w n="31.8">alcôve</w>,</l>
					<l n="32" num="1.32"><w n="32.1">Ni</w> <w n="32.2">le</w> <w n="32.3">palefroi</w> <w n="32.4">blanc</w> <w n="32.5">harnaché</w> <w n="32.6">de</w> <w n="32.7">cuir</w> <w n="32.8">fauve</w>,</l>
					<l n="33" num="1.33"><w n="33.1">Ni</w> <w n="33.2">les</w> <w n="33.3">jeux</w> <w n="33.4">féminins</w> <w n="33.5">qui</w> <w n="33.6">font</w> <w n="33.7">bouillir</w> <w n="33.8">son</w> <w n="33.9">sang</w>,</l>
					<l n="34" num="1.34"><w n="34.1">Ni</w> <w n="34.2">son</w> <w n="34.3">père</w> <w n="34.4">qui</w> <w n="34.5">rit</w> <w n="34.6">et</w> <w n="34.7">pleure</w> <w n="34.8">en</w> <w n="34.9">l</w>’<w n="34.10">embrassant</w>,</l>
					<l n="35" num="1.35"><w n="35.1">Rien</w> <w n="35.2">ne</w> <w n="35.3">peut</w> <w n="35.4">empêcher</w> <w n="35.5">que</w> <w n="35.6">son</w> <w n="35.7">cœur</w> <w n="35.8">ne</w> <w n="35.9">se</w> <w n="35.10">serre</w></l>
					<l n="36" num="1.36"><w n="36.1">Alors</w> <w n="36.2">qu</w>’<w n="36.3">il</w> <w n="36.4">se</w> <w n="36.5">souvient</w> <w n="36.6">de</w> <w n="36.7">sa</w> <w n="36.8">libre</w> <w n="36.9">misère</w>.</l>
					<l n="37" num="1.37"><w n="37.1">Ah</w> ! <w n="37.2">qu</w>’<w n="37.3">il</w> <w n="37.4">aimerait</w> <w n="37.5">mieux</w> <w n="37.6">le</w> <w n="37.7">fruit</w> <w n="37.8">à</w> <w n="37.9">peine</w> <w n="37.10">mûr</w></l>
					<l n="38" num="1.38"><w n="38.1">Qu</w>’<w n="38.2">on</w> <w n="38.3">dérobe</w> <w n="38.4">et</w> <w n="38.5">qu</w>’<w n="38.6">on</w> <w n="38.7">mange</w>, <w n="38.8">à</w> <w n="38.9">cheval</w> <w n="38.10">sur</w> <w n="38.11">un</w> <w n="38.12">mur</w>,</l>
					<l n="39" num="1.39"><w n="39.1">Le</w> <w n="39.2">revers</w> <w n="39.3">du</w> <w n="39.4">fossé</w> <w n="39.5">pour</w> <w n="39.6">dormir</w>, <w n="39.7">et</w> <w n="39.8">la</w> <w n="39.9">source</w></l>
					<l n="40" num="1.40"><w n="40.1">Pour</w> <w n="40.2">laver</w> <w n="40.3">ses</w> <w n="40.4">pieds</w> <w n="40.5">nus</w> <w n="40.6">fatigués</w> <w n="40.7">d</w>’<w n="40.8">une</w> <w n="40.9">course</w>,</l>
					<l n="41" num="1.41"><w n="41.1">Mais</w> <w n="41.2">du</w> <w n="41.3">moins</w> <w n="41.4">le</w> <w n="41.5">plein</w> <w n="41.6">ciel</w> <w n="41.7">et</w> <w n="41.8">le</w> <w n="41.9">vaste</w> <w n="41.10">horizon</w> !</l>
					<l n="42" num="1.42">— <w n="42.1">Parfois</w>, <w n="42.2">sur</w> <w n="42.3">le</w> <w n="42.4">rempart</w> <w n="42.5">de</w> <w n="42.6">sa</w> <w n="42.7">noble</w> <w n="42.8">prison</w>,</l>
					<l n="43" num="1.43"><w n="43.1">On</w> <w n="43.2">le</w> <w n="43.3">voit</w>, <w n="43.4">poursuivant</w> <w n="43.5">sa</w> <w n="43.6">chimère</w> <w n="43.7">innocente</w>,</l>
					<l n="44" num="1.44"><w n="44.1">Caresser</w> <w n="44.2">de</w> <w n="44.3">ses</w> <w n="44.4">doigts</w> <w n="44.5">une</w> <w n="44.6">guitare</w> <w n="44.7">absente</w></l>
					<l n="45" num="1.45"><w n="45.1">Et</w>, <w n="45.2">les</w> <w n="45.3">regards</w> <w n="45.4">au</w> <w n="45.5">ciel</w>, <w n="45.6">le</w> <w n="45.7">seul</w> <w n="45.8">pays</w> <w n="45.9">natal</w>,</l>
					<l n="46" num="1.46"><w n="46.1">Se</w> <w n="46.2">chanter</w> <w n="46.3">à</w> <w n="46.4">voix</w> <w n="46.5">basse</w> <w n="46.6">un</w> <w n="46.7">air</w> <w n="46.8">oriental</w>.</l>
				</lg>
			</div></body></text></TEI>