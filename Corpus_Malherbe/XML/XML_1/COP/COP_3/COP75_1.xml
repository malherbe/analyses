<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE CAHIER ROUGE</title>
				<title type="medium">Édition électronique</title>
				<author key="COP">
					<name>
						<forename>François</forename>
						<surname>COPPÉE</surname>
					</name>
					<date from="1842" to="1908">1842-1908</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1198 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">COP_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE CAHIER ROUGE</title>
						<author>François Coppée</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/francoiscopeelecahierrouge.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Œuvres complètes de François Coppée</title>
						<author>François Coppée</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie L. Hébert</publisher>
							<date when="1885">1885</date>
						</imprint>
						<biblScope unit="tome">Poésie, tome 2</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1874">1874</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="COP75">
				<head type="main">Prologue</head>
				<head type="sub_1">d’une série de causeries en vers</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Bonjour</w>, <w n="1.2">lecteurs</w>. <w n="1.3">On</w> <w n="1.4">me</w> <w n="1.5">propose</w></l>
					<l n="2" num="1.2"><w n="2.1">Et</w> <w n="2.2">j</w>’<w n="2.3">accepte</w> — <w n="2.4">oh</w> ! <w n="2.5">les</w> <w n="2.6">étourdis</w> ! —</l>
					<l n="3" num="1.3"><w n="3.1">De</w> <w n="3.2">vous</w> <w n="3.3">parler</w> <w n="3.4">tous</w> <w n="3.5">les</w> <w n="3.6">lundis</w></l>
					<l n="4" num="1.4"><w n="4.1">Et</w> <w n="4.2">même</w> <w n="4.3">pas</w> <w n="4.4">toujours</w> <w n="4.5">en</w> <w n="4.6">prose</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">La</w> <w n="5.2">causerie</w> <w n="5.3">est</w> <w n="5.4">cependant</w></l>
					<l n="6" num="2.2"><w n="6.1">Chose</w> <w n="6.2">insaisissable</w> <w n="6.3">et</w> <w n="6.4">légère</w></l>
					<l n="7" num="2.3"><w n="7.1">Ainsi</w> <w n="7.2">que</w> <w n="7.3">l</w>’<w n="7.4">ombre</w> <w n="7.5">passagère</w></l>
					<l n="8" num="2.4"><w n="8.1">D</w>’<w n="8.2">un</w> <w n="8.3">nuage</w> <w n="8.4">sur</w> <w n="8.5">un</w> <w n="8.6">étang</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Causer</w> <w n="9.2">en</w> <w n="9.3">vers</w>, <w n="9.4">c</w>’<w n="9.5">est</w> <w n="9.6">l</w>’<w n="9.7">art</w> <w n="9.8">suprême</w> ;</l>
					<l n="10" num="3.2"><w n="10.1">Et</w>, <w n="10.2">pour</w> <w n="10.3">m</w>’<w n="10.4">apprendre</w> <w n="10.5">mon</w> <w n="10.6">état</w>,</l>
					<l n="11" num="3.3"><w n="11.1">Il</w> <w n="11.2">faudrait</w> <w n="11.3">qu</w>’<w n="11.4">on</w> <w n="11.5">ressuscitât</w></l>
					<l n="12" num="3.4"><w n="12.1">Le</w> <w n="12.2">pauvre</w> <w n="12.3">grand</w> <w n="12.4">Musset</w> <w n="12.5">lui</w>-<w n="12.6">même</w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Je</w> <w n="13.2">crains</w> <w n="13.3">fort</w> <w n="13.4">de</w> <w n="13.5">n</w>’<w n="13.6">être</w> <w n="13.7">pas</w> <w n="13.8">bon</w></l>
					<l n="14" num="4.2"><w n="14.1">A</w> <w n="14.2">vous</w> <w n="14.3">inventer</w> <w n="14.4">ces</w> <w n="14.5">chimères</w></l>
					<l n="15" num="4.3"><w n="15.1">Radieuses</w>, <w n="15.2">mais</w> <w n="15.3">éphémères</w></l>
					<l n="16" num="4.4"><w n="16.1">Comme</w> <w n="16.2">les</w> <w n="16.3">bulles</w> <w n="16.4">de</w> <w n="16.5">savon</w> ;</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">A</w> <w n="17.2">vous</w> <w n="17.3">rimer</w> <w n="17.4">des</w> <w n="17.5">amusettes</w></l>
					<l n="18" num="5.2"><w n="18.1">Sur</w> <w n="18.2">des</w> <w n="18.3">sujets</w> <w n="18.4">de</w> <w n="18.5">presque</w> <w n="18.6">rien</w>,</l>
					<l n="19" num="5.3"><w n="19.1">Avec</w> <w n="19.2">l</w>’<w n="19.3">art</w> <w n="19.4">du</w> <w n="19.5">galérien</w></l>
					<l n="20" num="5.4"><w n="20.1">Qui</w> <w n="20.2">sculpte</w> <w n="20.3">au</w> <w n="20.4">couteau</w> <w n="20.5">des</w> <w n="20.6">noisettes</w>.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1">— <w n="21.1">Mais</w>, <w n="21.2">bah</w> ! <w n="21.3">j</w>’<w n="21.4">ai</w> <w n="21.5">l</w>’<w n="21.6">horreur</w> <w n="21.7">du</w> <w n="21.8">banal</w></l>
					<l n="22" num="6.2"><w n="22.1">Et</w> <w n="22.2">le</w> <w n="22.3">difficile</w> <w n="22.4">me</w> <w n="22.5">tente</w>.</l>
					<l n="23" num="6.3"><w n="23.1">J</w>’<w n="23.2">éprouve</w> <w n="23.3">une</w> <w n="23.4">envie</w> <w n="23.5">irritante</w></l>
					<l n="24" num="6.4"><w n="24.1">D</w>’<w n="24.2">écrire</w> <w n="24.3">en</w> <w n="24.4">vers</w> <w n="24.5">dans</w> <w n="24.6">un</w> <w n="24.7">journal</w>.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">Et</w> <w n="25.2">d</w>’<w n="25.3">ailleurs</w> <w n="25.4">mon</w> <w n="25.5">rêve</w> <w n="25.6">impossible</w>,</l>
					<l n="26" num="7.2"><w n="26.1">Je</w> <w n="26.2">l</w>’<w n="26.3">ai</w> <w n="26.4">souvent</w> <w n="26.5">réalisé</w> ;</l>
					<l n="27" num="7.3"><w n="27.1">Sans</w> <w n="27.2">que</w> <w n="27.3">mon</w> <w n="27.4">regard</w> <w n="27.5">ait</w> <w n="27.6">visé</w>,</l>
					<l n="28" num="7.4"><w n="28.1">J</w>’<w n="28.2">ai</w> <w n="28.3">quelquefois</w> <w n="28.4">touché</w> <w n="28.5">la</w> <w n="28.6">cible</w>.</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1">J</w>’<w n="29.2">irai</w> <w n="29.3">chercher</w>, <w n="29.4">je</w> <w n="29.5">ne</w> <w n="29.6">sais</w> <w n="29.7">où</w>,</l>
					<l n="30" num="8.2"><w n="30.1">Des</w> <w n="30.2">conversations</w> <w n="30.3">frivoles</w> ;</l>
					<l n="31" num="8.3"><w n="31.1">Je</w> <w n="31.2">vous</w> <w n="31.3">dirai</w> <w n="31.4">des</w> <w n="31.5">choses</w> <w n="31.6">folles</w>,</l>
					<l n="32" num="8.4"><w n="32.1">Car</w> <w n="32.2">je</w> <w n="32.3">suis</w> <w n="32.4">moi</w>-<w n="32.5">même</w> <w n="32.6">un</w> <w n="32.7">peu</w> <w n="32.8">fou</w>.</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1"><w n="33.1">Ayant</w> <w n="33.2">le</w> <w n="33.3">ciel</w> <w n="33.4">bleu</w> <w n="33.5">pour</w> <w n="33.6">auberge</w>,</l>
					<l n="34" num="9.2"><w n="34.1">Je</w> <w n="34.2">vis</w> <w n="34.3">comme</w> <w n="34.4">un</w> <w n="34.5">petit</w> <w n="34.6">oiseau</w>,</l>
					<l n="35" num="9.3"><w n="35.1">Et</w> <w n="35.2">Mab</w> <w n="35.3">m</w>’<w n="35.4">a</w> <w n="35.5">prêté</w> <w n="35.6">son</w> <w n="35.7">fuseau</w></l>
					<l n="36" num="9.4"><w n="36.1">A</w> <w n="36.2">filer</w> <w n="36.3">le</w> <w n="36.4">fil</w> <w n="36.5">de</w> <w n="36.6">la</w> <w n="36.7">vierge</w>.</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1"><w n="37.1">Je</w> <w n="37.2">fais</w> <w n="37.3">de</w> <w n="37.4">la</w> <w n="37.5">dépense</w>, <w n="37.6">et</w> <w n="37.7">c</w>’<w n="37.8">est</w></l>
					<l n="38" num="10.2"><w n="38.1">Royalement</w> <w n="38.2">que</w> <w n="38.3">je</w> <w n="38.4">la</w> <w n="38.5">paie</w>,</l>
					<l n="39" num="10.3"><w n="39.1">Car</w> <w n="39.2">le</w> <w n="39.3">poète</w> <w n="39.4">a</w> <w n="39.5">pour</w> <w n="39.6">monnaie</w></l>
					<l n="40" num="10.4"><w n="40.1">Des</w> <w n="40.2">étoiles</w> <w n="40.3">dans</w> <w n="40.4">son</w> <w n="40.5">gousset</w>.</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1"><w n="41.1">L</w>’<w n="41.2">aile</w> <w n="41.3">et</w> <w n="41.4">le</w> <w n="41.5">parfum</w> <w n="41.6">étant</w> <w n="41.7">choses</w></l>
					<l n="42" num="11.2"><w n="42.1">Qu</w>’<w n="42.2">il</w> <w n="42.3">faut</w> <w n="42.4">que</w> <w n="42.5">nous</w> <w n="42.6">réunissions</w>,</l>
					<l n="43" num="11.3"><w n="43.1">J</w>’<w n="43.2">ai</w> <w n="43.3">découvert</w> <w n="43.4">des</w> <w n="43.5">papillons</w></l>
					<l n="44" num="11.4"><w n="44.1">Qui</w> <w n="44.2">sentaient</w> <w n="44.3">bon</w> <w n="44.4">comme</w> <w n="44.5">des</w> <w n="44.6">roses</w>.</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1"><w n="45.1">Les</w> <w n="45.2">plus</w> <w n="45.3">beaux</w> <w n="45.4">décors</w> <w n="45.5">d</w>’<w n="45.6">opéra</w></l>
					<l n="46" num="12.2"><w n="46.1">Me</w> <w n="46.2">semblent</w> <w n="46.3">mesquins</w> <w n="46.4">et</w> <w n="46.5">timides</w> ;</l>
					<l n="47" num="12.3"><w n="47.1">Quand</w> <w n="47.2">j</w>’<w n="47.3">irai</w> <w n="47.4">voir</w> <w n="47.5">les</w> <w n="47.6">Pyramides</w>,</l>
					<l n="48" num="12.4"><w n="48.1">Je</w> <w n="48.2">veux</w> <w n="48.3">qu</w>’<w n="48.4">il</w> <w n="48.5">neige</w>. <w n="48.6">Il</w> <w n="48.7">neigera</w>.</l>
				</lg>
				<lg n="13">
					<l n="49" num="13.1"><w n="49.1">Parfois</w> <w n="49.2">la</w> <w n="49.3">lune</w> <w n="49.4">me</w> <w n="49.5">fait</w> <w n="49.6">signe</w> ;</l>
					<l n="50" num="13.2"><w n="50.1">Mais</w> <w n="50.2">aller</w> <w n="50.3">là</w>-<w n="50.4">haut</w>, <w n="50.5">c</w>’<w n="50.6">est</w> <w n="50.7">trop</w> <w n="50.8">long</w>.</l>
					<l n="51" num="13.3"><w n="51.1">Si</w> <w n="51.2">je</w> <w n="51.3">jouais</w> <w n="51.4">du</w> <w n="51.5">violon</w></l>
					<l n="52" num="13.4"><w n="52.1">Je</w> <w n="52.2">noterais</w> <w n="52.3">le</w> <w n="52.4">chant</w> <w n="52.5">du</w> <w n="52.6">cygne</w>.</l>
				</lg>
				<lg n="14">
					<l n="53" num="14.1">— <w n="53.1">Je</w> <w n="53.2">vous</w> <w n="53.3">dirai</w> <w n="53.4">sur</w> <w n="53.5">mon</w> <w n="53.6">chemin</w></l>
					<l n="54" num="14.2"><w n="54.1">Ce</w> <w n="54.2">qui</w> <w n="54.3">m</w>’<w n="54.4">intéresse</w> <w n="54.5">ou</w> <w n="54.6">me</w> <w n="54.7">charme</w>,</l>
					<l n="55" num="14.3"><w n="55.1">Et</w> <w n="55.2">même</w> <w n="55.3">d</w>’<w n="55.4">où</w> <w n="55.5">vient</w> <w n="55.6">cette</w> <w n="55.7">larme</w></l>
					<l n="56" num="14.4"><w n="56.1">Qui</w> <w n="56.2">tombe</w> <w n="56.3">parfois</w> <w n="56.4">sur</w> <w n="56.5">ma</w> <w n="56.6">main</w>.</l>
				</lg>
				<lg n="15">
					<l n="57" num="15.1"><w n="57.1">De</w> <w n="57.2">cet</w> <w n="57.3">entretien</w> <w n="57.4">de</w> <w n="57.5">poète</w></l>
					<l n="58" num="15.2"><w n="58.1">Vous</w> <w n="58.2">ne</w> <w n="58.3">serez</w> <w n="58.4">jamais</w> <w n="58.5">plus</w> <w n="58.6">las</w></l>
					<l n="59" num="15.3"><w n="59.1">Que</w> <w n="59.2">n</w>’<w n="59.3">est</w> <w n="59.4">un</w> <w n="59.5">rameau</w> <w n="59.6">de</w> <w n="59.7">lilas</w></l>
					<l n="60" num="15.4"><w n="60.1">De</w> <w n="60.2">la</w> <w n="60.3">halte</w> <w n="60.4">d</w>’<w n="60.5">une</w> <w n="60.6">fauvette</w> ;</l>
				</lg>
				<lg n="16">
					<l n="61" num="16.1"><w n="61.1">Et</w> <w n="61.2">quand</w> <w n="61.3">vous</w> <w n="61.4">y</w> <w n="61.5">lirez</w> <w n="61.6">l</w>’<w n="61.7">aveu</w></l>
					<l n="62" num="16.2"><w n="62.1">D</w>’<w n="62.2">une</w> <w n="62.3">bonne</w> <w n="62.4">pensée</w> <w n="62.5">intime</w>,</l>
					<l n="63" num="16.3"><w n="63.1">Vous</w> <w n="63.2">me</w> <w n="63.3">donnerez</w> <w n="63.4">votre</w> <w n="63.5">estime</w></l>
					<l n="64" num="16.4"><w n="64.1">Et</w> <w n="64.2">m</w>’<w n="64.3">aimerez</w> <w n="64.4">peut</w>-<w n="64.5">être</w> <w n="64.6">un</w> <w n="64.7">peu</w>.</l>
				</lg>
				<lg n="17">
					<l n="65" num="17.1">— <w n="65.1">Mais</w> <w n="65.2">voici</w> <w n="65.3">ma</w> <w n="65.4">préface</w> <w n="65.5">faite</w>.</l>
					<l n="66" num="17.2"><w n="66.1">Au</w> <w n="66.2">revoir</w>, <w n="66.3">car</w> <w n="66.4">j</w>’<w n="66.5">ai</w> <w n="66.6">mérité</w></l>
					<l n="67" num="17.3"><w n="67.1">De</w> <w n="67.2">finir</w> <w n="67.3">ma</w> <w n="67.4">tasse</w> <w n="67.5">de</w> <w n="67.6">thé</w>,</l>
					<l n="68" num="17.4"><w n="68.1">En</w> <w n="68.2">fumant</w> <w n="68.3">une</w> <w n="68.4">cigarette</w>.</l>
				</lg>
			</div></body></text></TEI>