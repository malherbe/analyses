<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE CAHIER ROUGE</title>
				<title type="medium">Édition électronique</title>
				<author key="COP">
					<name>
						<forename>François</forename>
						<surname>COPPÉE</surname>
					</name>
					<date from="1842" to="1908">1842-1908</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1198 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">COP_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE CAHIER ROUGE</title>
						<author>François Coppée</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/francoiscopeelecahierrouge.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Œuvres complètes de François Coppée</title>
						<author>François Coppée</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie L. Hébert</publisher>
							<date when="1885">1885</date>
						</imprint>
						<biblScope unit="tome">Poésie, tome 2</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1874">1874</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="COP65">
				<head type="main">Gaieté du cimetière</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Avis</w> <w n="1.2">aux</w> <w n="1.3">amateurs</w> <w n="1.4">de</w> <w n="1.5">la</w> <w n="1.6">gaîté</w> <w n="1.7">française</w>.</l>
					<l n="2" num="1.2"><w n="2.1">Le</w> <w n="2.2">printemps</w> <w n="2.3">fait</w> <w n="2.4">neiger</w>, <w n="2.5">dans</w> <w n="2.6">le</w> <w n="2.7">Père</w>-<w n="2.8">Lachaise</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Les</w> <w n="3.2">fleurs</w> <w n="3.3">des</w> <w n="3.4">marronniers</w> <w n="3.5">sur</w> <w n="3.6">les</w> <w n="3.7">arbres</w> <w n="3.8">muets</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Et</w> <w n="4.2">la</w> <w n="4.3">fosse</w> <w n="4.4">commune</w> <w n="4.5">est</w> <w n="4.6">pleine</w> <w n="4.7">de</w> <w n="4.8">bleuets</w>.</l>
					<l n="5" num="1.5"><w n="5.1">Le</w> <w n="5.2">liseron</w> <w n="5.3">grimpeur</w> <w n="5.4">fleurit</w> <w n="5.5">les</w> <w n="5.6">croix</w> <w n="5.7">célèbres</w> ;</l>
					<l n="6" num="1.6"><w n="6.1">Les</w> <w n="6.2">oiseaux</w> <w n="6.3">font</w> <w n="6.4">l</w>’<w n="6.5">amour</w> <w n="6.6">près</w> <w n="6.7">des</w> <w n="6.8">bustes</w> <w n="6.9">funèbres</w> ;</l>
					<l n="7" num="1.7"><w n="7.1">Et</w> <w n="7.2">l</w>’<w n="7.3">on</w> <w n="7.4">voit</w> <w n="7.5">un</w> <w n="7.6">joyeux</w> <w n="7.7">commissaire</w> <w n="7.8">des</w> <w n="7.9">morts</w>,</l>
					<l n="8" num="1.8"><w n="8.1">Tricorne</w> <w n="8.2">en</w> <w n="8.3">tête</w> <w n="8.4">et</w> <w n="8.5">canne</w> <w n="8.6">à</w> <w n="8.7">la</w> <w n="8.8">main</w>, <w n="8.9">sans</w> <w n="8.10">remords</w>,</l>
					<l n="9" num="1.9"><w n="9.1">Cueillir</w> <w n="9.2">de</w> <w n="9.3">ses</w> <w n="9.4">doigts</w> <w n="9.5">noirs</w>, <w n="9.6">gantés</w> <w n="9.7">de</w> <w n="9.8">filoselle</w>,</l>
					<l n="10" num="1.10"><w n="10.1">Des</w> <w n="10.2">bouquets</w> <w n="10.3">pour</w> <w n="10.4">sa</w> <w n="10.5">dame</w> <w n="10.6">et</w> <w n="10.7">pour</w> <w n="10.8">sa</w> <w n="10.9">demoiselle</w>.</l>
				</lg>
			</div></body></text></TEI>