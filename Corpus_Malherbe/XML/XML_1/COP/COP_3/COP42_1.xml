<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE CAHIER ROUGE</title>
				<title type="medium">Édition électronique</title>
				<author key="COP">
					<name>
						<forename>François</forename>
						<surname>COPPÉE</surname>
					</name>
					<date from="1842" to="1908">1842-1908</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1198 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">COP_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE CAHIER ROUGE</title>
						<author>François Coppée</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/francoiscopeelecahierrouge.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Œuvres complètes de François Coppée</title>
						<author>François Coppée</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie L. Hébert</publisher>
							<date when="1885">1885</date>
						</imprint>
						<biblScope unit="tome">Poésie, tome 2</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1874">1874</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="COP42">
				<head type="main">Le Vieux Soulier</head>
				<opener>
					<salute>A Jocelyn Bargoin</salute>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">EN</w> <w n="1.2">mai</w>, <w n="1.3">par</w> <w n="1.4">une</w> <w n="1.5">pure</w> <w n="1.6">et</w> <w n="1.7">chaude</w> <w n="1.8">après</w>-<w n="1.9">midi</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Je</w> <w n="2.2">cheminais</w> <w n="2.3">au</w> <w n="2.4">bord</w> <w n="2.5">du</w> <w n="2.6">doux</w> <w n="2.7">fleuve</w> <w n="2.8">attiédi</w></l>
					<l n="3" num="1.3"><w n="3.1">Où</w> <w n="3.2">se</w> <w n="3.3">réfléchissait</w> <w n="3.4">la</w> <w n="3.5">fuite</w> <w n="3.6">d</w>’<w n="3.7">un</w> <w n="3.8">nuage</w>.</l>
					<l n="4" num="1.4"><w n="4.1">Je</w> <w n="4.2">suivais</w> <w n="4.3">lentement</w> <w n="4.4">le</w> <w n="4.5">chemin</w> <w n="4.6">de</w> <w n="4.7">halage</w></l>
					<l n="5" num="1.5"><w n="5.1">Tout</w> <w n="5.2">en</w> <w n="5.3">fleurs</w>, <w n="5.4">qui</w> <w n="5.5">descend</w> <w n="5.6">en</w> <w n="5.7">pente</w> <w n="5.8">vers</w> <w n="5.9">les</w> <w n="5.10">eaux</w>.</l>
					<l n="6" num="1.6"><w n="6.1">Des</w> <w n="6.2">peupliers</w> <w n="6.3">à</w> <w n="6.4">droite</w>, <w n="6.5">à</w> <w n="6.6">gauche</w> <w n="6.7">des</w> <w n="6.8">roseaux</w> ;</l>
					<l n="7" num="1.7"><w n="7.1">Devant</w> <w n="7.2">moi</w>, <w n="7.3">les</w> <w n="7.4">détours</w> <w n="7.5">de</w> <w n="7.6">la</w> <w n="7.7">rivière</w> <w n="7.8">en</w> <w n="7.9">marche</w></l>
					<l n="8" num="1.8"><w n="8.1">Et</w>, <w n="8.2">fermant</w> <w n="8.3">l</w>’<w n="8.4">horizon</w>, <w n="8.5">un</w> <w n="8.6">pont</w> <w n="8.7">d</w>’<w n="8.8">une</w> <w n="8.9">seule</w> <w n="8.10">arche</w>.</l>
					<l n="9" num="1.9"><w n="9.1">Le</w> <w n="9.2">courant</w> <w n="9.3">murmurait</w>, <w n="9.4">en</w> <w n="9.5">inclinant</w> <w n="9.6">les</w> <w n="9.7">joncs</w>,</l>
					<l n="10" num="1.10"><w n="10.1">Et</w> <w n="10.2">les</w> <w n="10.3">poissons</w>, <w n="10.4">avec</w> <w n="10.5">leurs</w> <w n="10.6">sauts</w> <w n="10.7">et</w> <w n="10.8">leurs</w> <w n="10.9">plongeons</w>,</l>
					<l n="11" num="1.11"><w n="11.1">Sans</w> <w n="11.2">cesse</w> <w n="11.3">le</w> <w n="11.4">ridaient</w> <w n="11.5">de</w> <w n="11.6">grands</w> <w n="11.7">cercles</w> <w n="11.8">de</w> <w n="11.9">moire</w>.</l>
					<l n="12" num="1.12"><w n="12.1">Le</w> <w n="12.2">loriot</w> <w n="12.3">et</w> <w n="12.4">la</w> <w n="12.5">fauvette</w> <w n="12.6">à</w> <w n="12.7">tête</w> <w n="12.8">noire</w></l>
					<l n="13" num="1.13"><w n="13.1">Se</w> <w n="13.2">répondaient</w> <w n="13.3">parmi</w> <w n="13.4">les</w> <w n="13.5">arbres</w> <w n="13.6">en</w> <w n="13.7">rideau</w> ;</l>
					<l n="14" num="1.14"><w n="14.1">Et</w> <w n="14.2">ces</w> <w n="14.3">chansons</w> <w n="14.4">des</w> <w n="14.5">nids</w> <w n="14.6">joyeux</w> <w n="14.7">et</w> <w n="14.8">ce</w> <w n="14.9">bruit</w> <w n="14.10">d</w>’<w n="14.11">eau</w></l>
					<l n="15" num="1.15"><w n="15.1">Accompagnaient</w> <w n="15.2">ma</w> <w n="15.3">douce</w> <w n="15.4">et</w> <w n="15.5">lente</w> <w n="15.6">flânerie</w>.</l>
				</lg>
				<lg n="2">
					<l n="16" num="2.1"><w n="16.1">Soudain</w>, <w n="16.2">dans</w> <w n="16.3">le</w> <w n="16.4">gazon</w> <w n="16.5">de</w> <w n="16.6">la</w> <w n="16.7">berge</w> <w n="16.8">fleurie</w>,</l>
					<l n="17" num="2.2"><w n="17.1">Parmi</w> <w n="17.2">les</w> <w n="17.3">boutons</w> <w n="17.4">d</w>’<w n="17.5">or</w> <w n="17.6">qui</w> <w n="17.7">criblaient</w> <w n="17.8">le</w> <w n="17.9">chemin</w>,</l>
					<l n="18" num="2.3"><w n="18.1">J</w>’<w n="18.2">aperçus</w> <w n="18.3">à</w> <w n="18.4">mes</w> <w n="18.5">pieds</w>, — <w n="18.6">premier</w> <w n="18.7">vestige</w> <w n="18.8">humain</w></l>
					<l n="19" num="2.4"><w n="19.1">Que</w> <w n="19.2">j</w>’<w n="19.3">eusse</w> <w n="19.4">rencontré</w> <w n="19.5">dans</w> <w n="19.6">ce</w> <w n="19.7">lieu</w> <w n="19.8">solitaire</w>, —</l>
					<l n="20" num="2.5"><w n="20.1">Sous</w> <w n="20.2">l</w>’<w n="20.3">herbe</w> <w n="20.4">et</w> <w n="20.5">se</w> <w n="20.6">mêlant</w> <w n="20.7">déjà</w> <w n="20.8">presque</w> <w n="20.9">à</w> <w n="20.10">la</w> <w n="20.11">terre</w>,</l>
					<l n="21" num="2.6"><w n="21.1">Un</w> <w n="21.2">soulier</w> <w n="21.3">laissé</w> <w n="21.4">là</w> <w n="21.5">par</w> <w n="21.6">quelque</w> <w n="21.7">mendiant</w>.</l>
				</lg>
				<lg n="3">
					<l n="22" num="3.1"><w n="22.1">C</w>’<w n="22.2">était</w> <w n="22.3">un</w> <w n="22.4">vieux</w> <w n="22.5">soulier</w>, <w n="22.6">sale</w>, <w n="22.7">ignoble</w>, <w n="22.8">effrayant</w>,</l>
					<l n="23" num="3.2"><w n="23.1">Éculé</w> <w n="23.2">du</w> <w n="23.3">talon</w>, <w n="23.4">bâillant</w> <w n="23.5">de</w> <w n="23.6">la</w> <w n="23.7">semelle</w>,</l>
					<l n="24" num="3.3"><w n="24.1">Laid</w> <w n="24.2">comme</w> <w n="24.3">la</w> <w n="24.4">misère</w> <w n="24.5">et</w> <w n="24.6">sinistre</w> <w n="24.7">comme</w> <w n="24.8">elle</w>,</l>
					<l n="25" num="3.4"><w n="25.1">Qui</w> <w n="25.2">jadis</w> <w n="25.3">fut</w> <w n="25.4">sans</w> <w n="25.5">doute</w> <w n="25.6">usé</w> <w n="25.7">par</w> <w n="25.8">un</w> <w n="25.9">soldat</w>,</l>
					<l n="26" num="3.5"><w n="26.1">Puis</w>, <w n="26.2">chez</w> <w n="26.3">le</w> <w n="26.4">savetier</w>, <w n="26.5">bien</w> <w n="26.6">qu</w>’<w n="26.7">en</w> <w n="26.8">piteux</w> <w n="26.9">état</w>,</l>
					<l n="27" num="3.6"><w n="27.1">Fut</w> <w n="27.2">à</w> <w n="27.3">quelque</w> <w n="27.4">rôdeur</w> <w n="27.5">vendu</w> <w n="27.6">dans</w> <w n="27.7">une</w> <w n="27.8">échoppe</w> ;</l>
					<l n="28" num="3.7"><w n="28.1">Un</w> <w n="28.2">de</w> <w n="28.3">ces</w> <w n="28.4">vieux</w> <w n="28.5">souliers</w> <w n="28.6">qui</w> <w n="28.7">font</w> <w n="28.8">le</w> <w n="28.9">tour</w> <w n="28.10">d</w>’<w n="28.11">Europe</w></l>
					<l n="29" num="3.8"><w n="29.1">Et</w> <w n="29.2">qu</w>’<w n="29.3">un</w> <w n="29.4">jour</w>, <w n="29.5">tout</w> <w n="29.6">meurtri</w>, <w n="29.7">sanglant</w>, <w n="29.8">estropié</w>,</l>
					<l n="30" num="3.9"><w n="30.1">Le</w> <w n="30.2">pied</w> <w n="30.3">ne</w> <w n="30.4">quitte</w> <w n="30.5">pas</w>, <w n="30.6">mais</w> <w n="30.7">qui</w> <w n="30.8">quittent</w> <w n="30.9">le</w> <w n="30.10">pied</w>.</l>
				</lg>
				<lg n="4">
					<l n="31" num="4.1"><w n="31.1">Quel</w> <w n="31.2">poème</w> <w n="31.3">navrant</w> <w n="31.4">dans</w> <w n="31.5">cette</w> <w n="31.6">morne</w> <w n="31.7">épave</w> !</l>
					<l n="32" num="4.2"><w n="32.1">Le</w> <w n="32.2">boulet</w> <w n="32.3">du</w> <w n="32.4">forçat</w> <w n="32.5">ou</w> <w n="32.6">le</w> <w n="32.7">fer</w> <w n="32.8">de</w> <w n="32.9">l</w>’<w n="32.10">esclave</w></l>
					<l n="33" num="4.3"><w n="33.1">Sont</w>-<w n="33.2">ils</w> <w n="33.3">plus</w> <w n="33.4">lourds</w> <w n="33.5">que</w> <w n="33.6">toi</w>, <w n="33.7">soulier</w> <w n="33.8">du</w> <w n="33.9">vagabond</w> ?</l>
					<l n="34" num="4.4"><w n="34.1">Pourquoi</w> <w n="34.2">t</w>’<w n="34.3">a</w>-<w n="34.4">t</w>-<w n="34.5">on</w> <w n="34.6">laissé</w> <w n="34.7">sous</w> <w n="34.8">cette</w> <w n="34.9">arche</w> <w n="34.10">de</w> <w n="34.11">pont</w> ?</l>
					<l n="35" num="4.5"><w n="35.1">L</w>’<w n="35.2">eau</w> <w n="35.3">doit</w> <w n="35.4">être</w> <w n="35.5">profonde</w> <w n="35.6">ici</w> ? <w n="35.7">Cette</w> <w n="35.8">rivière</w></l>
					<l n="36" num="4.6"><w n="36.1">N</w>’<w n="36.2">a</w>-<w n="36.3">t</w>-<w n="36.4">elle</w> <w n="36.5">pas</w> <w n="36.6">été</w> <w n="36.7">mauvaise</w> <w n="36.8">conseillère</w></l>
					<l n="37" num="4.7"><w n="37.1">Au</w> <w n="37.2">voyageur</w> <w n="37.3">si</w> <w n="37.4">las</w> <w n="37.5">et</w> <w n="37.6">de</w> <w n="37.7">si</w> <w n="37.8">loin</w> <w n="37.9">venu</w> ?</l>
					<l n="38" num="4.8"><w n="38.1">Réponds</w> ! <w n="38.2">S</w>’<w n="38.3">en</w> <w n="38.4">alla</w>-<w n="38.5">t</w>-<w n="38.6">il</w>, <w n="38.7">en</w> <w n="38.8">traînant</w> <w n="38.9">son</w> <w n="38.10">pied</w> <w n="38.11">nu</w>,</l>
					<l n="39" num="4.9"><w n="39.1">Mendier</w> <w n="39.2">des</w> <w n="39.3">sabots</w> <w n="39.4">à</w> <w n="39.5">la</w> <w n="39.6">prochaine</w> <w n="39.7">auberge</w> ?</l>
					<l n="40" num="4.10"><w n="40.1">Ou</w> <w n="40.2">bien</w>, <w n="40.3">après</w> <w n="40.4">t</w>’<w n="40.5">avoir</w> <w n="40.6">perdu</w> <w n="40.7">sur</w> <w n="40.8">cette</w> <w n="40.9">berge</w>,</l>
					<l n="41" num="4.11"><w n="41.1">Ce</w> <w n="41.2">pauvre</w>, <w n="41.3">abandonné</w> <w n="41.4">même</w> <w n="41.5">par</w> <w n="41.6">ses</w> <w n="41.7">haillons</w>,</l>
					<l n="42" num="4.12"><w n="42.1">Est</w>-<w n="42.2">il</w> <w n="42.3">allé</w> <w n="42.4">savoir</w> <w n="42.5">au</w> <w n="42.6">sein</w> <w n="42.7">des</w> <w n="42.8">tourbillons</w></l>
					<l n="43" num="4.13"><w n="43.1">Si</w> <w n="43.2">l</w>’<w n="43.3">on</w> <w n="43.4">n</w>’<w n="43.5">a</w> <w n="43.6">plus</w> <w n="43.7">besoin</w>, <w n="43.8">quand</w> <w n="43.9">on</w> <w n="43.10">dort</w> <w n="43.11">dans</w> <w n="43.12">le</w> <w n="43.13">fleuve</w>,</l>
					<l n="44" num="4.14"><w n="44.1">De</w> <w n="44.2">costume</w> <w n="44.3">décent</w> <w n="44.4">et</w> <w n="44.5">de</w> <w n="44.6">chaussure</w> <w n="44.7">neuve</w> ?</l>
				</lg>
				<lg n="5">
					<l n="45" num="5.1"><w n="45.1">En</w> <w n="45.2">vain</w> <w n="45.3">je</w> <w n="45.4">me</w> <w n="45.5">défends</w> <w n="45.6">du</w> <w n="45.7">dégoût</w> <w n="45.8">singulier</w></l>
					<l n="46" num="5.2"><w n="46.1">Que</w> <w n="46.2">j</w>’<w n="46.3">éprouve</w> <w n="46.4">à</w> <w n="46.5">l</w>’<w n="46.6">aspect</w> <w n="46.7">de</w> <w n="46.8">ce</w> <w n="46.9">mauvais</w> <w n="46.10">soulier</w>,</l>
					<l n="47" num="5.3"><w n="47.1">Trouvé</w> <w n="47.2">sur</w> <w n="47.3">mon</w> <w n="47.4">chemin</w>, <w n="47.5">tout</w> <w n="47.6">seul</w>, <w n="47.7">dans</w> <w n="47.8">la</w> <w n="47.9">campagne</w>.</l>
					<l n="48" num="5.4"><w n="48.1">Il</w> <w n="48.2">est</w> <w n="48.3">infâme</w>, <w n="48.4">il</w> <w n="48.5">a</w> <w n="48.6">l</w>’<w n="48.7">air</w> <w n="48.8">de</w> <w n="48.9">venir</w> <w n="48.10">du</w> <w n="48.11">bagne</w> ;</l>
					<l n="49" num="5.5"><w n="49.1">Il</w> <w n="49.2">est</w> <w n="49.3">rouge</w>, <w n="49.4">l</w>’<w n="49.5">averse</w> <w n="49.6">ayant</w> <w n="49.7">lavé</w> <w n="49.8">le</w> <w n="49.9">cuir</w> ;</l>
					<l n="50" num="5.6"><w n="50.1">Et</w> <w n="50.2">je</w> <w n="50.3">rêve</w> <w n="50.4">de</w> <w n="50.5">meurtre</w>, <w n="50.6">et</w> <w n="50.7">j</w>’<w n="50.8">entends</w> <w n="50.9">quelqu</w>’<w n="50.10">un</w> <w n="50.11">fuir</w></l>
					<l n="51" num="5.7"><w n="51.1">Loin</w> <w n="51.2">d</w>’<w n="51.3">un</w> <w n="51.4">homme</w> <w n="51.5">râlant</w> <w n="51.6">dans</w> <w n="51.7">une</w> <w n="51.8">rue</w> <w n="51.9">obscure</w></l>
					<l n="52" num="5.8"><w n="52.1">Et</w> <w n="52.2">dont</w> <w n="52.3">les</w> <w n="52.4">clous</w> <w n="52.5">sanglants</w> <w n="52.6">ont</w> <w n="52.7">broyé</w> <w n="52.8">la</w> <w n="52.9">figure</w> !</l>
				</lg>
				<lg n="6">
					<l n="53" num="6.1"><w n="53.1">Abominable</w> <w n="53.2">objet</w> <w n="53.3">sous</w> <w n="53.4">mes</w> <w n="53.5">pas</w> <w n="53.6">rencontré</w>,</l>
					<l n="54" num="6.2"><w n="54.1">Rebut</w> <w n="54.2">du</w> <w n="54.3">scélérat</w> <w n="54.4">ou</w> <w n="54.5">du</w> <w n="54.6">désespéré</w>,</l>
					<l n="55" num="6.3"><w n="55.1">Tu</w> <w n="55.2">donnes</w> <w n="55.3">le</w> <w n="55.4">frisson</w>. <w n="55.5">Tout</w> <w n="55.6">en</w> <w n="55.7">toi</w> <w n="55.8">me</w> <w n="55.9">rappelle</w>,</l>
					<l n="56" num="6.4"><w n="56.1">Devant</w> <w n="56.2">les</w> <w n="56.3">fleurs</w>, <w n="56.4">devant</w> <w n="56.5">la</w> <w n="56.6">nature</w> <w n="56.7">si</w> <w n="56.8">belle</w>,</l>
					<l n="57" num="6.5"><w n="57.1">Devant</w> <w n="57.2">les</w> <w n="57.3">cieux</w> <w n="57.4">où</w> <w n="57.5">court</w> <w n="57.6">le</w> <w n="57.7">doux</w> <w n="57.8">vent</w> <w n="57.9">aromal</w>,</l>
					<l n="58" num="6.6"><w n="58.1">Devant</w> <w n="58.2">le</w> <w n="58.3">bon</w> <w n="58.4">soleil</w>, <w n="58.5">l</w>’<w n="58.6">éternité</w> <w n="58.7">du</w> <w n="58.8">mal</w>.</l>
					<l n="59" num="6.7"><w n="59.1">Tu</w> <w n="59.2">me</w> <w n="59.3">dis</w> <w n="59.4">devant</w> <w n="59.5">eux</w>, <w n="59.6">triste</w> <w n="59.7">témoin</w> <w n="59.8">sincère</w>,</l>
					<l n="60" num="6.8"><w n="60.1">Que</w> <w n="60.2">le</w> <w n="60.3">monde</w> <w n="60.4">est</w> <w n="60.5">rempli</w> <w n="60.6">de</w> <w n="60.7">vice</w> <w n="60.8">et</w> <w n="60.9">de</w> <w n="60.10">misère</w></l>
					<l n="61" num="6.9"><w n="61.1">Et</w> <w n="61.2">que</w> <w n="61.3">ceux</w> <w n="61.4">dont</w> <w n="61.5">les</w> <w n="61.6">pieds</w> <w n="61.7">saignent</w> <w n="61.8">sur</w> <w n="61.9">les</w> <w n="61.10">chemins</w>,</l>
					<l n="62" num="6.10"><w n="62.1">O</w> <w n="62.2">malheur</w> ! <w n="62.3">sont</w> <w n="62.4">bien</w> <w n="62.5">près</w> <w n="62.6">d</w>’<w n="62.7">ensanglanter</w> <w n="62.8">leurs</w> <w n="62.9">mains</w>.</l>
					<l n="63" num="6.11">— <w n="63.1">Sois</w> <w n="63.2">maudit</w>, <w n="63.3">instrument</w> <w n="63.4">de</w> <w n="63.5">crime</w> <w n="63.6">ou</w> <w n="63.7">de</w> <w n="63.8">torture</w> !</l>
					<l n="64" num="6.12"><w n="64.1">Mais</w> <w n="64.2">qu</w>’<w n="64.3">est</w>-<w n="64.4">ce</w> <w n="64.5">que</w> <w n="64.6">cela</w> <w n="64.7">peut</w> <w n="64.8">faire</w> <w n="64.9">à</w> <w n="64.10">la</w> <w n="64.11">nature</w> ?</l>
					<l n="65" num="6.13"><w n="65.1">Voyez</w>, <w n="65.2">il</w> <w n="65.3">disparaît</w> <w n="65.4">sous</w> <w n="65.5">l</w>’<w n="65.6">herbe</w> <w n="65.7">des</w> <w n="65.8">sillons</w> ;</l>
					<l n="66" num="6.14"><w n="66.1">Hideux</w>, <w n="66.2">il</w> <w n="66.3">ne</w> <w n="66.4">fait</w> <w n="66.5">pas</w> <w n="66.6">horreur</w> <w n="66.7">aux</w> <w n="66.8">papillons</w> ;</l>
					<l n="67" num="6.15"><w n="67.1">La</w> <w n="67.2">terre</w> <w n="67.3">le</w> <w n="67.4">reprend</w>, <w n="67.5">il</w> <w n="67.6">verdit</w> <w n="67.7">sous</w> <w n="67.8">la</w> <w n="67.9">mousse</w>,</l>
					<l n="68" num="6.16"><w n="68.1">Et</w> <w n="68.2">dans</w> <w n="68.3">le</w> <w n="68.4">vieux</w> <w n="68.5">soulier</w> <w n="68.6">une</w> <w n="68.7">fleur</w> <w n="68.8">des</w> <w n="68.9">champs</w> <w n="68.10">pousse</w>.</l>
				</lg>
			</div></body></text></TEI>