<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE CAHIER ROUGE</title>
				<title type="medium">Édition électronique</title>
				<author key="COP">
					<name>
						<forename>François</forename>
						<surname>COPPÉE</surname>
					</name>
					<date from="1842" to="1908">1842-1908</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1198 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">COP_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE CAHIER ROUGE</title>
						<author>François Coppée</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/francoiscopeelecahierrouge.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Œuvres complètes de François Coppée</title>
						<author>François Coppée</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie L. Hébert</publisher>
							<date when="1885">1885</date>
						</imprint>
						<biblScope unit="tome">Poésie, tome 2</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1874">1874</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="COP64">
				<head type="main">Sur la terrasse</head>
				<head type="sub_1">du Château de R…</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Devant</w> <w n="1.2">le</w> <w n="1.3">pur</w>, <w n="1.4">devant</w> <w n="1.5">le</w> <w n="1.6">vaste</w> <w n="1.7">ciel</w> <w n="1.8">du</w> <w n="1.9">soir</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Où</w> <w n="2.2">scintillaient</w> <w n="2.3">déjà</w> <w n="2.4">quelques</w> <w n="2.5">étoiles</w> <w n="2.6">pâles</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Sur</w> <w n="3.2">la</w> <w n="3.3">terrasse</w>, <w n="3.4">avec</w> <w n="3.5">des</w> <w n="3.6">fichus</w> <w n="3.7">et</w> <w n="3.8">des</w> <w n="3.9">châles</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Toute</w> <w n="4.2">la</w> <w n="4.3">compagnie</w> <w n="4.4">avait</w> <w n="4.5">voulu</w> <w n="4.6">s</w>’<w n="4.7">asseoir</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Devant</w> <w n="5.2">nous</w> <w n="5.3">l</w>’<w n="5.4">étendue</w> <w n="5.5">immense</w>, <w n="5.6">froide</w> <w n="5.7">et</w> <w n="5.8">grise</w>,</l>
					<l n="6" num="2.2"><w n="6.1">D</w>’<w n="6.2">une</w> <w n="6.3">plaine</w>, <w n="6.4">la</w> <w n="6.5">nuit</w>, <w n="6.6">à</w> <w n="6.7">la</w> <w n="6.8">fin</w> <w n="6.9">de</w> <w n="6.10">l</w>’<w n="6.11">été</w>.</l>
					<l n="7" num="2.3"><w n="7.1">Puis</w> <w n="7.2">un</w> <w n="7.3">silence</w>, <w n="7.4">un</w> <w n="7.5">calme</w>, <w n="7.6">une</w> <w n="7.7">sérénité</w> ;</l>
					<l n="8" num="2.4"><w n="8.1">Pas</w> <w n="8.2">un</w> <w n="8.3">chant</w> <w n="8.4">de</w> <w n="8.5">grillon</w>, <w n="8.6">pas</w> <w n="8.7">un</w> <w n="8.8">souffle</w> <w n="8.9">de</w> <w n="8.10">brise</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Nos</w> <w n="9.2">cigares</w> <w n="9.3">étaient</w> <w n="9.4">les</w> <w n="9.5">seuls</w> <w n="9.6">points</w> <w n="9.7">lumineux</w> ;</l>
					<l n="10" num="3.2"><w n="10.1">Les</w> <w n="10.2">femmes</w> <w n="10.3">avaient</w> <w n="10.4">froid</w> <w n="10.5">sous</w> <w n="10.6">leurs</w> <w n="10.7">manteaux</w> <w n="10.8">de</w> <w n="10.9">laine</w>.</l>
					<l n="11" num="3.3"><w n="11.1">Tous</w> <w n="11.2">se</w> <w n="11.3">taisaient</w>, <w n="11.4">sentant</w> <w n="11.5">que</w> <w n="11.6">la</w> <w n="11.7">parole</w> <w n="11.8">humaine</w></l>
					<l n="12" num="3.4"><w n="12.1">Romprait</w> <w n="12.2">le</w> <w n="12.3">charme</w> <w n="12.4">pur</w> <w n="12.5">qui</w> <w n="12.6">pénétrait</w> <w n="12.7">en</w> <w n="12.8">eux</w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Près</w> <w n="13.2">de</w> <w n="13.3">moi</w>, <w n="13.4">s</w>’<w n="13.5">éloignant</w> <w n="13.6">du</w> <w n="13.7">groupe</w> <w n="13.8">noir</w> <w n="13.9">des</w> <w n="13.10">femmes</w>,</l>
					<l n="14" num="4.2"><w n="14.1">La</w> <w n="14.2">jeune</w> <w n="14.3">fille</w> <w n="14.4">était</w> <w n="14.5">assise</w> <w n="14.6">de</w> <w n="14.7">profil</w>,</l>
					<l n="15" num="4.3"><w n="15.1">Et</w>, <w n="15.2">brillant</w> <w n="15.3">du</w> <w n="15.4">regret</w> <w n="15.5">des</w> <w n="15.6">anges</w> .<w n="15.7">en</w> <w n="15.8">exil</w>,</l>
					<l n="16" num="4.4"><w n="16.1">Son</w> <w n="16.2">regard</w> <w n="16.3">se</w> <w n="16.4">levait</w> <w n="16.5">vers</w> <w n="16.6">le</w> <w n="16.7">pays</w> <w n="16.8">des</w> <w n="16.9">âmes</w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">Ses</w> <w n="17.2">mains</w> <w n="17.3">blanches</w>, <w n="17.4">ses</w> <w n="17.5">mains</w> <w n="17.6">d</w>’<w n="17.7">enfant</w> <w n="17.8">sur</w> <w n="17.9">ses</w> <w n="17.10">genoux</w></l>
					<l n="18" num="5.2"><w n="18.1">Se</w> <w n="18.2">joignaient</w> <w n="18.3">faiblement</w>, <w n="18.4">presque</w> <w n="18.5">avec</w> <w n="18.6">lassitude</w>,</l>
					<l n="19" num="5.3"><w n="19.1">Et</w> <w n="19.2">ses</w> <w n="19.3">yeux</w> <w n="19.4">exprimaient</w>, <w n="19.5">comme</w> <w n="19.6">son</w> <w n="19.7">attitude</w>,</l>
					<l n="20" num="5.4"><w n="20.1">Tout</w> <w n="20.2">ce</w> <w n="20.3">que</w> <w n="20.4">la</w> <w n="20.5">souffrance</w> <w n="20.6">a</w> <w n="20.7">de</w> <w n="20.8">cher</w> <w n="20.9">et</w> <w n="20.10">de</w> <w n="20.11">doux</w>.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">Elle</w> <w n="21.2">semblait</w> <w n="21.3">frileuse</w> <w n="21.4">en</w> <w n="21.5">son</w> <w n="21.6">lourd</w> <w n="21.7">plaid</w> <w n="21.8">d</w>’<w n="21.9">Écosse</w>,</l>
					<l n="22" num="6.2"><w n="22.1">Et</w> <w n="22.2">pourtant</w> <w n="22.3">souriait</w>, <w n="22.4">heureuse</w> <w n="22.5">vaguement</w> ;</l>
					<l n="23" num="6.3"><w n="23.1">Mais</w> <w n="23.2">ce</w> <w n="23.3">sourire</w> <w n="23.4">était</w> <w n="23.5">si</w> <w n="23.6">faible</w> <w n="23.7">en</w> <w n="23.8">ce</w> <w n="23.9">moment</w></l>
					<l n="24" num="6.4"><w n="24.1">Qu</w>’<w n="24.2">il</w> <w n="24.3">avait</w> <w n="24.4">plutôt</w> <w n="24.5">l</w>’<w n="24.6">air</w> <w n="24.7">d</w>’<w n="24.8">une</w> <w n="24.9">ride</w> <w n="24.10">précoce</w>.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">Pourquoi</w> <w n="25.2">donc</w> <w n="25.3">ai</w>-<w n="25.4">je</w> <w n="25.5">alors</w> <w n="25.6">rêvé</w> <w n="25.7">de</w> <w n="25.8">la</w> <w n="25.9">saison</w></l>
					<l n="26" num="7.2"><w n="26.1">Qui</w> <w n="26.2">dépouille</w> <w n="26.3">les</w> <w n="26.4">bois</w> <w n="26.5">sous</w> <w n="26.6">la</w> <w n="26.7">bise</w> <w n="26.8">plus</w> <w n="26.9">aigre</w>,</l>
					<l n="27" num="7.3"><w n="27.1">Et</w> <w n="27.2">pourquoi</w> <w n="27.3">ce</w> <w n="27.4">sillon</w> <w n="27.5">dans</w> <w n="27.6">la</w> <w n="27.7">joue</w> <w n="27.8">un</w> <w n="27.9">peu</w> <w n="27.10">maigre</w></l>
					<l n="28" num="7.4"><w n="28.1">M</w>’<w n="28.2">a</w>-<w n="28.3">t</w>-<w n="28.4">il</w> <w n="28.5">inquiété</w> <w n="28.6">bien</w> <w n="28.7">plus</w> <w n="28.8">que</w> <w n="28.9">de</w> <w n="28.10">raison</w> ?</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1">Je</w> <w n="29.2">connais</w> <w n="29.3">cette</w> <w n="29.4">enfant</w> ; <w n="29.5">elle</w> <w n="29.6">n</w>’<w n="29.7">est</w> <w n="29.8">que</w> <w n="29.9">débile</w>.</l>
					<l n="30" num="8.2"><w n="30.1">Depuis</w> <w n="30.2">le</w> <w n="30.3">bel</w> <w n="30.4">été</w> <w n="30.5">passé</w> <w n="30.6">dans</w> <w n="30.7">ce</w> <w n="30.8">château</w>,</l>
					<l n="31" num="8.3"><w n="31.1">Elle</w> <w n="31.2">va</w> <w n="31.3">mieux</w>. <w n="31.4">C</w>’<w n="31.5">est</w> <w n="31.6">moi</w> <w n="31.7">qui</w> <w n="31.8">lui</w> <w n="31.9">mets</w> <w n="31.10">son</w> <w n="31.11">manteau</w>,</l>
					<l n="32" num="8.4">— <w n="32.1">Lorsque</w> <w n="32.2">le</w> <w n="32.3">vent</w> <w n="32.4">fraîchit</w>, — <w n="32.5">d</w>’<w n="32.6">une</w> <w n="32.7">main</w> <w n="32.8">malhabile</w>.</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1"><w n="33.1">J</w>’<w n="33.2">ai</w> <w n="33.3">ma</w> <w n="33.4">place</w> <w n="33.5">auprès</w> <w n="33.6">d</w>’<w n="33.7">elle</w>, <w n="33.8">à</w> <w n="33.9">l</w>’<w n="33.10">heure</w> <w n="33.11">des</w> <w n="33.12">repas</w> ;</l>
					<l n="34" num="9.2"><w n="34.1">Je</w> <w n="34.2">la</w> <w n="34.3">gronde</w> <w n="34.4">parfois</w> <w n="34.5">d</w>’<w n="34.6">être</w> <w n="34.7">à</w> <w n="34.8">mes</w> <w n="34.9">soins</w> <w n="34.10">rebelle</w>,</l>
					<l n="35" num="9.3"><w n="35.1">Et</w>, <w n="35.2">tout</w> <w n="35.3">en</w> <w n="35.4">plaisantant</w>, <w n="35.5">c</w>’<w n="35.6">est</w> <w n="35.7">moi</w> <w n="35.8">qui</w> <w n="35.9">lui</w> <w n="35.10">rappelle</w></l>
					<l n="36" num="9.4"><w n="36.1">Le</w> <w n="36.2">cordial</w> <w n="36.3">amer</w> <w n="36.4">qu</w>’<w n="36.5">elle</w> <w n="36.6">ne</w> <w n="36.7">prendrait</w> <w n="36.8">pas</w>.</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1"><w n="37.1">Elle</w> <w n="37.2">ne</w> <w n="37.3">peut</w> <w n="37.4">nous</w> <w n="37.5">être</w> <w n="37.6">aussi</w> <w n="37.7">vite</w> <w n="37.8">ravie</w> !…</l>
					<l n="38" num="10.2">— <w n="38.1">Non</w>, <w n="38.2">mais</w> <w n="38.3">devant</w> <w n="38.4">ce</w> <w n="38.5">ciel</w> <w n="38.6">calme</w> <w n="38.7">et</w> <w n="38.8">mystérieux</w>,</l>
					<l n="39" num="10.3"><w n="39.1">Avec</w> <w n="39.2">ce</w> <w n="39.3">doux</w> <w n="39.4">reflet</w> <w n="39.5">d</w>’<w n="39.6">étoile</w> <w n="39.7">dans</w> <w n="39.8">les</w> <w n="39.9">yeux</w>,</l>
					<l n="40" num="10.4"><w n="40.1">Cette</w> <w n="40.2">enfant</w> <w n="40.3">m</w>’<w n="40.4">a</w> <w n="40.5">paru</w> <w n="40.6">trop</w> <w n="40.7">faible</w> <w n="40.8">pour</w> <w n="40.9">la</w> <w n="40.10">vie</w> ;</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1"><w n="41.1">Et</w>, <w n="41.2">sans</w> <w n="41.3">avoir</w> <w n="41.4">pitié</w>, <w n="41.5">je</w> <w n="41.6">n</w>’<w n="41.7">ai</w> <w n="41.8">pas</w> <w n="41.9">pu</w> <w n="41.10">prévoir</w></l>
					<l n="42" num="11.2"><w n="42.1">Tout</w> <w n="42.2">ce</w> <w n="42.3">qui</w> <w n="42.4">doit</w> <w n="42.5">changer</w> <w n="42.6">en</w> <w n="42.7">ride</w> <w n="42.8">ce</w> <w n="42.9">sourire</w></l>
					<l n="43" num="11.3"><w n="43.1">Et</w> <w n="43.2">flétrir</w> <w n="43.3">dans</w> <w n="43.4">les</w> <w n="43.5">pleurs</w> <w n="43.6">ce</w> <w n="43.7">regard</w> <w n="43.8">où</w> <w n="43.9">se</w> <w n="43.10">mire</w></l>
					<l n="44" num="11.4"><w n="44.1">Le</w> <w n="44.2">charme</w> <w n="44.3">triste</w> <w n="44.4">et</w> <w n="44.5">pur</w> <w n="44.6">de</w> <w n="44.7">l</w>’<w n="44.8">automne</w> <w n="44.9">et</w> <w n="44.10">du</w> <w n="44.11">soir</w>.</l>
				</lg>
			</div></body></text></TEI>