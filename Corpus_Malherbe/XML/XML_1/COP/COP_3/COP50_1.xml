<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE CAHIER ROUGE</title>
				<title type="medium">Édition électronique</title>
				<author key="COP">
					<name>
						<forename>François</forename>
						<surname>COPPÉE</surname>
					</name>
					<date from="1842" to="1908">1842-1908</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1198 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">COP_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE CAHIER ROUGE</title>
						<author>François Coppée</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/francoiscopeelecahierrouge.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Œuvres complètes de François Coppée</title>
						<author>François Coppée</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie L. Hébert</publisher>
							<date when="1885">1885</date>
						</imprint>
						<biblScope unit="tome">Poésie, tome 2</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1874">1874</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="COP50">
				<head type="main">En sortant d’un bal</head>
				<opener>
					<salute>A julien Travers</salute>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">On</w> <w n="1.2">n</w>’<w n="1.3">a</w> <w n="1.4">pu</w> <w n="1.5">l</w>’<w n="1.6">emmener</w> <w n="1.7">qu</w>’<w n="1.8">à</w> <w n="1.9">la</w> <w n="1.10">dernière</w> <w n="1.11">danse</w>.</l>
					<l n="2" num="1.2"><w n="2.1">C</w>’<w n="2.2">était</w> <w n="2.3">son</w> <w n="2.4">premier</w> <w n="2.5">bal</w>, <w n="2.6">songez</w> ! <w n="2.7">et</w> <w n="2.8">la</w> <w n="2.9">prudence</w></l>
					<l n="3" num="1.3"><w n="3.1">De</w> <w n="3.2">sa</w> <w n="3.3">mère</w> <w n="3.4">a</w> <w n="3.5">cédé</w> <w n="3.6">jusqu</w>’<w n="3.7">au</w> <w n="3.8">bout</w> <w n="3.9">au</w> <w n="3.10">désir</w></l>
					<l n="4" num="1.4"><w n="4.1">De</w> <w n="4.2">la</w> <w n="4.3">voir</w>, <w n="4.4">embellie</w> <w n="4.5">encor</w> <w n="4.6">par</w> <w n="4.7">le</w> <w n="4.8">plaisir</w>,</l>
					<l n="5" num="1.5"><w n="5.1">Résister</w> <w n="5.2">du</w> <w n="5.3">regard</w> <w n="5.4">au</w> <w n="5.5">doigt</w> <w n="5.6">qui</w> <w n="5.7">lui</w> <w n="5.8">fait</w> <w n="5.9">signe</w>,</l>
					<l n="6" num="1.6"><w n="6.1">Ou</w> <w n="6.2">venir</w> <w n="6.3">effleurer</w>, <w n="6.4">d</w>’<w n="6.5">un</w> <w n="6.6">air</w> <w n="6.7">qui</w> <w n="6.8">se</w> <w n="6.9">résigne</w>,</l>
					<l n="7" num="1.7"><w n="7.1">L</w>’<w n="7.2">oreille</w> <w n="7.3">maternelle</w> <w n="7.4">où</w> <w n="7.5">sa</w> <w n="7.6">claire</w> <w n="7.7">voix</w> <w n="7.8">d</w>’<w n="7.9">or</w></l>
					<l n="8" num="1.8"><w n="8.1">Murmure</w> <w n="8.2">ces</w> <w n="8.3">deux</w> <w n="8.4">mots</w> <w n="8.5">suppliants</w> : « <w n="8.6">Pas</w> <w n="8.7">encor</w>. »</l>
					<l n="9" num="1.9"><w n="9.1">C</w>’<w n="9.2">est</w> <w n="9.3">la</w> <w n="9.4">première</w> <w n="9.5">fois</w> <w n="9.6">qu</w>’<w n="9.7">elle</w> <w n="9.8">entre</w> <w n="9.9">dans</w> <w n="9.10">ces</w> <w n="9.11">fêtes</w>.</l>
					<l n="10" num="1.10"><w n="10.1">Elle</w> <w n="10.2">est</w> <w n="10.3">en</w> <w n="10.4">blanc</w> ; <w n="10.5">elle</w> <w n="10.6">a</w>, <w n="10.7">dans</w> <w n="10.8">les</w> <w n="10.9">tresses</w> <w n="10.10">défaites</w></l>
					<l n="11" num="1.11"><w n="11.1">De</w> <w n="11.2">ses</w> <w n="11.3">cheveux</w>, <w n="11.4">un</w> <w n="11.5">brin</w> <w n="11.6">délicat</w> <w n="11.7">de</w> <w n="11.8">lilas</w> ;</l>
					<l n="12" num="1.12"><w n="12.1">Elle</w> <w n="12.2">accueille</w> <w n="12.3">d</w>’<w n="12.4">abord</w> <w n="12.5">d</w>’<w n="12.6">un</w> <w n="12.7">sourire</w> <w n="12.8">un</w> <w n="12.9">peu</w> <w n="12.10">las</w></l>
					<l n="13" num="1.13"><w n="13.1">Le</w> <w n="13.2">danseur</w> <w n="13.3">qui</w> <w n="13.4">lui</w> <w n="13.5">tend</w> <w n="13.6">la</w> <w n="13.7">main</w> <w n="13.8">et</w> <w n="13.9">qui</w> <w n="13.10">l</w>’<w n="13.11">invite</w>,</l>
					<l n="14" num="1.14"><w n="14.1">Et</w> <w n="14.2">rougit</w> <w n="14.3">vaguement</w>, <w n="14.4">et</w> <w n="14.5">se</w> <w n="14.6">lève</w> <w n="14.7">bien</w> <w n="14.8">vite</w>,</l>
					<l n="15" num="1.15"><w n="15.1">Quand</w>, <w n="15.2">parmi</w> <w n="15.3">la</w> <w n="15.4">clarté</w> <w n="15.5">joyeuse</w> <w n="15.6">des</w> <w n="15.7">salons</w>,</l>
					<l n="16" num="1.16"><w n="16.1">Ont</w> <w n="16.2">préludé</w> <w n="16.3">la</w> <w n="16.4">flûte</w> <w n="16.5">et</w> <w n="16.6">les</w> <w n="16.7">deux</w> <w n="16.8">violons</w>.</l>
					<l n="17" num="1.17"><w n="17.1">Et</w> <w n="17.2">ce</w> <w n="17.3">bal</w> <w n="17.4">lui</w> <w n="17.5">paraît</w> <w n="17.6">étincelant</w>, <w n="17.7">immense</w>.</l>
					<l n="18" num="1.18"><w n="18.1">C</w>’<w n="18.2">est</w> <w n="18.3">le</w> <w n="18.4">premier</w> ! <w n="18.5">Avant</w> <w n="18.6">que</w> <w n="18.7">la</w> <w n="18.8">valse</w> <w n="18.9">commence</w>,</l>
					<l n="19" num="1.19"><w n="19.1">Elle</w> <w n="19.2">a</w> <w n="19.3">peur</w> <w n="19.4">tout</w> <w n="19.5">à</w> <w n="19.6">coup</w>, <w n="19.7">et</w> <w n="19.8">regarde</w>, <w n="19.9">en</w> <w n="19.10">tremblant</w>,</l>
					<l n="20" num="1.20"><w n="20.1">Au</w> <w n="20.2">bras</w> <w n="20.3">de</w> <w n="20.4">son</w> <w n="20.5">danseur</w> <w n="20.6">s</w>’<w n="20.7">appuyer</w> <w n="20.8">son</w> <w n="20.9">gant</w> <w n="20.10">blanc</w>.</l>
					<l n="21" num="1.21"><w n="21.1">La</w> <w n="21.2">voilà</w> <w n="21.3">donc</w> <w n="21.4">parmi</w> <w n="21.5">les</w> <w n="21.6">grandes</w> <w n="21.7">demoiselles</w>,</l>
					<l n="22" num="1.22"><w n="22.1">Oiselet</w> <w n="22.2">tout</w> <w n="22.3">surpris</w> <w n="22.4">de</w> <w n="22.5">l</w>’<w n="22.6">émoi</w> <w n="22.7">de</w> <w n="22.8">ses</w> <w n="22.9">ailes</w>.</l>
					<l n="23" num="1.23"><w n="23.1">Un</w> <w n="23.2">jeune</w> <w n="23.3">homme</w> <w n="23.4">lui</w> <w n="23.5">parle</w> <w n="23.6">et</w> <w n="23.7">marche</w> <w n="23.8">à</w> <w n="23.9">son</w> <w n="23.10">côté</w>.</l>
					<l n="24" num="1.24"><w n="24.1">Elle</w> <w n="24.2">jette</w> <w n="24.3">autour</w> <w n="24.4">d</w>’<w n="24.5">elle</w> <w n="24.6">un</w> <w n="24.7">regard</w> <w n="24.8">enchanté</w></l>
					<l n="25" num="1.25"><w n="25.1">Et</w> <w n="25.2">qui</w> <w n="25.3">de</w> <w n="25.4">toutes</w> <w n="25.5">parts</w> <w n="25.6">reflète</w> <w n="25.7">des</w> <w n="25.8">féeries</w>,</l>
					<l n="26" num="1.26"><w n="26.1">Et</w> <w n="26.2">devant</w> <w n="26.3">les</w> <w n="26.4">seins</w> <w n="26.5">nus</w> <w n="26.6">couverts</w> <w n="26.7">de</w> <w n="26.8">pierreries</w>,</l>
					<l n="27" num="1.27"><w n="27.1">Les</w> <w n="27.2">souples</w> <w n="27.3">éventails</w> <w n="27.4">aux</w> <w n="27.5">joyeuses</w> <w n="27.6">couleurs</w></l>
					<l n="28" num="1.28"><w n="28.1">Semblent</w> <w n="28.2">des</w> <w n="28.3">papillons</w> <w n="28.4">palpitant</w> <w n="28.5">sur</w> <w n="28.6">des</w> <w n="28.7">fleurs</w>.</l>
				</lg>
				<lg n="2">
					<l n="29" num="2.1"><w n="29.1">Pourtant</w> <w n="29.2">elle</w> <w n="29.3">est</w> <w n="29.4">partie</w>, <w n="29.5">à</w> <w n="29.6">la</w> <w n="29.7">fin</w>. <w n="29.8">Mais</w> <w n="29.9">mon</w> <w n="29.10">rêve</w></l>
					<l n="30" num="2.2"><w n="30.1">Reste</w> <w n="30.2">encor</w> <w n="30.3">sous</w> <w n="30.4">le</w> <w n="30.5">charme</w> <w n="30.6">et</w>, <w n="30.7">la</w> <w n="30.8">suivant</w>, <w n="30.9">achève</w></l>
					<l n="31" num="2.3"><w n="31.1">Cette</w> <w n="31.2">première</w> <w n="31.3">nuit</w> <w n="31.4">du</w> <w n="31.5">plaisir</w> <w n="31.6">révélé</w>.</l>
					<l n="32" num="2.4"><w n="32.1">Dans</w> <w n="32.2">le</w> <w n="32.3">calme</w> <w n="32.4">du</w> <w n="32.5">frais</w> <w n="32.6">boudoir</w> <w n="32.7">inviolé</w>,</l>
					<l n="33" num="2.5"><w n="33.1">Assise</w>, — <w n="33.2">car</w> <w n="33.3">la</w> <w n="33.4">danse</w> <w n="33.5">est</w> <w n="33.6">un</w> <w n="33.7">peu</w> <w n="33.8">fatigante</w>, —</l>
					<l n="34" num="2.6"><w n="34.1">Elle</w> <w n="34.2">ôte</w> <w n="34.3">son</w> <w n="34.4">collier</w> <w n="34.5">de</w> <w n="34.6">perles</w>, <w n="34.7">se</w> <w n="34.8">dégante</w></l>
					<l n="35" num="2.7"><w n="35.1">Et</w> <w n="35.2">tressaille</w> <w n="35.3">soudain</w> <w n="35.4">de</w> <w n="35.5">frissons</w> <w n="35.6">ingénus</w></l>
					<l n="36" num="2.8"><w n="36.1">En</w> <w n="36.2">voyant</w> <w n="36.3">au</w> <w n="36.4">miroir</w> <w n="36.5">son</w> <w n="36.6">col</w> <w n="36.7">et</w> <w n="36.8">ses</w> <w n="36.9">bras</w> <w n="36.10">nus</w> ;</l>
					<l n="37" num="2.9"><w n="37.1">Puis</w> <w n="37.2">le</w> <w n="37.3">petit</w> <w n="37.4">bouquet</w> <w n="37.5">qui</w> <w n="37.6">meurt</w> <w n="37.7">à</w> <w n="37.8">son</w> <w n="37.9">corsage</w></l>
					<l n="38" num="2.10"><w n="38.1">Dans</w> <w n="38.2">son</w> <w n="38.3">dernier</w> <w n="38.4">parfum</w> <w n="38.5">lui</w> <w n="38.6">rappelle</w> <w n="38.7">un</w> <w n="38.8">passage</w></l>
					<l n="39" num="2.11"><w n="39.1">De</w> <w n="39.2">la</w> <w n="39.3">valse</w> <w n="39.4">où</w> <w n="39.5">ce</w> <w n="39.6">blond</w> <w n="39.7">cavalier</w> <w n="39.8">l</w>’<w n="39.9">entraînait</w> ;</l>
					<l n="40" num="2.12"><w n="40.1">Elle</w> <w n="40.2">cherche</w> <w n="40.3">un</w> <w n="40.4">instant</w> <w n="40.5">sur</w> <w n="40.6">son</w> <w n="40.7">mignon</w> <w n="40.8">carnet</w></l>
					<l n="41" num="2.13"><w n="41.1">Un</w> <w n="41.2">nom</w> <w n="41.3">que</w> <w n="41.4">nul</w> <w n="41.5">encor</w> <w n="41.6">n</w>’<w n="41.7">a</w> <w n="41.8">le</w> <w n="41.9">droit</w> <w n="41.10">de</w> <w n="41.11">connaître</w>,</l>
					<l n="42" num="2.14"><w n="42.1">Tandis</w> <w n="42.2">qu</w>’<w n="42.3">entre</w> <w n="42.4">les</w> <w n="42.5">deux</w> <w n="42.6">rideaux</w> <w n="42.7">de</w> <w n="42.8">la</w> <w n="42.9">fenêtre</w></l>
					<l n="43" num="2.15"><w n="43.1">L</w>’<w n="43.2">aube</w> <w n="43.3">surprend</w> <w n="43.4">déjà</w> <w n="43.5">la</w> <w n="43.6">lampe</w> <w n="43.7">qui</w> <w n="43.8">pâlit</w>…</l>
				</lg>
				<lg n="3">
					<l n="44" num="3.1"><w n="44.1">Mais</w> <w n="44.2">la</w> <w n="44.3">fatigue</w> <w n="44.4">enfin</w> <w n="44.5">l</w>’<w n="44.6">appelle</w> <w n="44.7">vers</w> <w n="44.8">son</w> <w n="44.9">lit</w> ;</l>
					<l n="45" num="3.2"><w n="45.1">Et</w>, <w n="45.2">dans</w> <w n="45.3">l</w>’<w n="45.4">alcôve</w> <w n="45.5">obscure</w> <w n="45.6">où</w> <w n="45.7">la</w> <w n="45.8">vierge</w> <w n="45.9">se</w> <w n="45.10">couche</w>,</l>
					<l n="46" num="3.3"><w n="46.1">Un</w> <w n="46.2">doux</w> <w n="46.3">ange</w> <w n="46.4">gardien</w> <w n="46.5">veille</w>, <w n="46.6">un</w> <w n="46.7">doigt</w> <w n="46.8">sur</w> <w n="46.9">la</w> <w n="46.10">bouche</w>.</l>
					<l n="47" num="3.4"><w n="47.1">Mon</w> <w n="47.2">rêve</w>, <w n="47.3">éloigne</w>-<w n="47.4">toi</w> ! <w n="47.5">Le</w> <w n="47.6">respect</w> <w n="47.7">nous</w> <w n="47.8">bannit</w>.</l>
					<l n="48" num="3.5"><w n="48.1">C</w>’<w n="48.2">est</w> <w n="48.3">violer</w> <w n="48.4">un</w> <w n="48.5">temple</w> <w n="48.6">et</w> <w n="48.7">c</w>’<w n="48.8">est</w> <w n="48.9">troubler</w> <w n="48.10">un</w> <w n="48.11">nid</w></l>
					<l n="49" num="3.6"><w n="49.1">Que</w> <w n="49.2">de</w> <w n="49.3">parler</w> <w n="49.4">encor</w> <w n="49.5">de</w> <w n="49.6">ces</w> <w n="49.7">choses</w> <w n="49.8">divines</w>,</l>
					<l n="50" num="3.7"><w n="50.1">Alors</w> <w n="50.2">qu</w>’<w n="50.3">il</w> <w n="50.4">ne</w> <w n="50.5">faut</w> <w n="50.6">pas</w> <w n="50.7">même</w> <w n="50.8">que</w> <w n="50.9">tu</w> <w n="50.10">devines</w>.</l>
				</lg>
			</div></body></text></TEI>