<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES HUMBLES</title>
				<title type="medium">Édition électronique</title>
				<author key="COP">
					<name>
						<forename>François</forename>
						<surname>COPPÉE</surname>
					</name>
					<date from="1842" to="1908">1842-1908</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1160 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">COP_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LES HUMBLES</title>
						<author>François Coppée</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/francoiscopeeleshumbles.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Œuvres complètes de François Coppée</title>
						<author>François Coppée</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie L. Hébert</publisher>
							<date when="1885">1885</date>
						</imprint>
						<biblScope unit="tome">Poésie, tome 1</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1872">1872</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="COP83">
				<head type="main">Le Petit Épicier</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">C</w>’<w n="1.2">était</w> <w n="1.3">un</w> <w n="1.4">tout</w> <w n="1.5">petit</w> <w n="1.6">épicier</w> <w n="1.7">de</w> <w n="1.8">Montrouge</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Et</w> <w n="2.2">sa</w> <w n="2.3">boutique</w> <w n="2.4">sombre</w>, <w n="2.5">aux</w> <w n="2.6">volets</w> <w n="2.7">peints</w> <w n="2.8">en</w> <w n="2.9">rouge</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Exhalait</w> <w n="3.2">une</w> <w n="3.3">odeur</w> <w n="3.4">fade</w> <w n="3.5">sur</w> <w n="3.6">le</w> <w n="3.7">trottoir</w>.</l>
					<l n="4" num="1.4"><w n="4.1">On</w> <w n="4.2">le</w> <w n="4.3">voyait</w> <w n="4.4">debout</w> <w n="4.5">derrière</w> <w n="4.6">son</w> <w n="4.7">comptoir</w>,</l>
					<l n="5" num="1.5"><w n="5.1">En</w> <w n="5.2">tablier</w>, <w n="5.3">cassant</w> <w n="5.4">du</w> <w n="5.5">sucre</w> <w n="5.6">avec</w> <w n="5.7">méthode</w>.</l>
					<l n="6" num="1.6"><w n="6.1">Tous</w> <w n="6.2">les</w> <w n="6.3">huit</w> <w n="6.4">jours</w>, <w n="6.5">sa</w> <w n="6.6">vie</w> <w n="6.7">avait</w> <w n="6.8">pour</w> <w n="6.9">épisode</w></l>
					<l n="7" num="1.7"><w n="7.1">Le</w> <w n="7.2">bruit</w> <w n="7.3">d</w>’<w n="7.4">un</w> <w n="7.5">camion</w> <w n="7.6">apportant</w> <w n="7.7">des</w> <w n="7.8">tonneaux</w></l>
					<l n="8" num="1.8"><w n="8.1">De</w> <w n="8.2">harengs</w> <w n="8.3">saurs</w> <w n="8.4">ou</w> <w n="8.5">bien</w> <w n="8.6">des</w> <w n="8.7">caisses</w> <w n="8.8">de</w> <w n="8.9">pruneaux</w> ;</l>
					<l n="9" num="1.9"><w n="9.1">Et</w>, <w n="9.2">le</w> <w n="9.3">reste</w> <w n="9.4">du</w> <w n="9.5">temps</w>, <w n="9.6">c</w>’<w n="9.7">était</w> <w n="9.8">dans</w> <w n="9.9">sa</w> <w n="9.10">boutique</w>,</l>
					<l n="10" num="1.10"><w n="10.1">Un</w> <w n="10.2">calme</w> <w n="10.3">rarement</w> <w n="10.4">troublé</w> <w n="10.5">par</w> <w n="10.6">la</w> <w n="10.7">pratique</w>,</l>
					<l n="11" num="1.11"><w n="11.1">Servante</w> <w n="11.2">de</w> <w n="11.3">rentier</w> <w n="11.4">ou</w> <w n="11.5">femme</w> <w n="11.6">d</w>’<w n="11.7">artisan</w>,</l>
					<l n="12" num="1.12"><w n="12.1">Logeant</w> <w n="12.2">dans</w> <w n="12.3">ce</w> <w n="12.4">faubourg</w> <w n="12.5">à</w> <w n="12.6">demi</w> <w n="12.7">paysan</w>.</l>
					<l n="13" num="1.13"><w n="13.1">Ce</w> <w n="13.2">petit</w> <w n="13.3">homme</w> <w n="13.4">roux</w>, <w n="13.5">aux</w> <w n="13.6">pâleurs</w> <w n="13.7">maladives</w>,</l>
					<l n="14" num="1.14"><w n="14.1">Était</w> <w n="14.2">triste</w>, <w n="14.3">faisant</w> <w n="14.4">des</w> <w n="14.5">affaires</w> <w n="14.6">chétives</w></l>
					<l n="15" num="1.15"><w n="15.1">Et</w>, <w n="15.2">comme</w> <w n="15.3">on</w> <w n="15.4">dit</w>, <w n="15.5">ayant</w> <w n="15.6">grand</w>’<w n="15.7">peine</w> <w n="15.8">à</w> <w n="15.9">vivoter</w>.</l>
					<l n="16" num="1.16"><w n="16.1">Son</w> <w n="16.2">histoire</w> <w n="16.3">pouvait</w> <w n="16.4">vite</w> <w n="16.5">se</w> <w n="16.6">raconter</w>.</l>
					<l n="17" num="1.17"><w n="17.1">Il</w> <w n="17.2">était</w> <w n="17.3">de</w> <w n="17.4">Soissons</w>, <w n="17.5">et</w> <w n="17.6">son</w> <w n="17.7">humble</w> <w n="17.8">famille</w>,</l>
					<l n="18" num="1.18"><w n="18.1">Le</w> <w n="18.2">voyant</w> <w n="18.3">à</w> <w n="18.4">quinze</w> <w n="18.5">ans</w> <w n="18.6">faible</w> <w n="18.7">comme</w> <w n="18.8">une</w> <w n="18.9">fille</w>,</l>
					<l n="19" num="1.19"><w n="19.1">Voulut</w> <w n="19.2">lui</w> <w n="19.3">faire</w> <w n="19.4">apprendre</w> <w n="19.5">un</w> <w n="19.6">commerce</w> <w n="19.7">à</w> <w n="19.8">Paris</w>.</l>
					<l n="20" num="1.20"><w n="20.1">Un</w> <w n="20.2">cousin</w>, <w n="20.3">épicier</w> <w n="20.4">lui</w>-<w n="20.5">même</w>, <w n="20.6">l</w>’<w n="20.7">avait</w> <w n="20.8">pris</w>,</l>
					<l n="21" num="1.21"><w n="21.1">Lui</w> <w n="21.2">donnant</w> <w n="21.3">le</w> <w n="21.4">logis</w> <w n="21.5">avec</w> <w n="21.6">la</w> <w n="21.7">nourriture</w> ;</l>
					<l n="22" num="1.22"><w n="22.1">Et</w>, <w n="22.2">malgré</w> <w n="22.3">la</w> <w n="22.4">cousine</w>, <w n="22.5">épouse</w> <w n="22.6">avare</w> <w n="22.7">et</w> <w n="22.8">dure</w>,</l>
					<l n="23" num="1.23"><w n="23.1">Aux</w> <w n="23.2">mystères</w> <w n="23.3">de</w> <w n="23.4">l</w>’<w n="23.5">art</w> <w n="23.6">il</w> <w n="23.7">put</w> <w n="23.8">l</w>’<w n="23.9">initier</w>.</l>
					<l n="24" num="1.24"><w n="24.1">Il</w> <w n="24.2">avait</w> <w n="24.3">ce</w> <w n="24.4">qu</w>’<w n="24.5">il</w> <w n="24.6">faut</w> <w n="24.7">pour</w> <w n="24.8">un</w> <w n="24.9">bon</w> <w n="24.10">épicier</w> :</l>
					<l n="25" num="1.25"><w n="25.1">Il</w> <w n="25.2">était</w> <w n="25.3">ponctuel</w>, <w n="25.4">sobre</w>, <w n="25.5">chaste</w>, <w n="25.6">économe</w>.</l>
					<l n="26" num="1.26"><w n="26.1">Son</w> <w n="26.2">patron</w> <w n="26.3">l</w>’<w n="26.4">estimait</w>, <w n="26.5">et</w>, <w n="26.6">quand</w> <w n="26.7">ce</w> <w n="26.8">fut</w> <w n="26.9">un</w> <w n="26.10">homme</w>,</l>
					<l n="27" num="1.27"><w n="27.1">Voulant</w> <w n="27.2">récompenser</w> <w n="27.3">ses</w> <w n="27.4">mérites</w> <w n="27.5">profonds</w>,</l>
					<l n="28" num="1.28"><w n="28.1">Il</w> <w n="28.2">lui</w> <w n="28.3">fit</w> <w n="28.4">prendre</w> <w n="28.5">femme</w> <w n="28.6">et</w> <w n="28.7">lui</w> <w n="28.8">vendit</w> <w n="28.9">son</w> <w n="28.10">fonds</w>.</l>
				</lg>
				<lg n="2">
					<l n="29" num="2.1">— <w n="29.1">Quand</w> <w n="29.2">on</w> <w n="29.3">trouve</w> <w n="29.4">un</w> <w n="29.5">garçon</w> <w n="29.6">pareil</w>, <w n="29.7">il</w> <w n="29.8">faut</w> <w n="29.9">qu</w>’<w n="29.10">on</w> <w n="29.11">l</w>’<w n="29.12">aide</w></l>
					<l part="I" n="30" num="2.2"><w n="30.1">Disait</w>-<w n="30.2">il</w>. </l>
					<l part="F" n="30" num="2.2"><w n="30.3">La</w> <w n="30.4">future</w> <w n="30.5">était</w> <w n="30.6">aisée</w> <w n="30.7">et</w> <w n="30.8">laide</w>,</l>
					<l n="31" num="2.3"><w n="31.1">Mais</w> <w n="31.2">ce</w> <w n="31.3">naïf</w> <w n="31.4">resta</w> <w n="31.5">devant</w> <w n="31.6">elle</w> <w n="31.7">tremblant</w> ;</l>
					<l n="32" num="2.4"><w n="32.1">Et</w> <w n="32.2">quand</w> <w n="32.3">il</w> <w n="32.4">l</w>’<w n="32.5">amena</w>, <w n="32.6">blonde</w> <w n="32.7">en</w> <w n="32.8">costume</w> <w n="32.9">blanc</w>,</l>
					<l n="33" num="2.5"><w n="33.1">La</w> <w n="33.2">boutique</w> <w n="33.3">aux</w> <w n="33.4">murs</w> <w n="33.5">noirs</w> <w n="33.6">lui</w> <w n="33.7">parut</w> <w n="33.8">toute</w> <w n="33.9">neuve</w>.</l>
					<l n="34" num="2.6"><w n="34.1">Or</w> <w n="34.2">sa</w> <w n="34.3">mère</w>, <w n="34.4">depuis</w> <w n="34.5">quelques</w> <w n="34.6">mois</w>, <w n="34.7">était</w> <w n="34.8">veuve</w>.</l>
					<l n="35" num="2.7"><w n="35.1">Vite</w> <w n="35.2">il</w> <w n="35.3">l</w>’<w n="35.4">alla</w> <w n="35.5">chercher</w> <w n="35.6">et</w> <w n="35.7">lui</w> <w n="35.8">dit</w>, <w n="35.9">triomphant</w> :</l>
				</lg>
				<lg n="3">
					<l n="36" num="3.1">— <w n="36.1">Viens</w> <w n="36.2">donc</w>, <w n="36.3">tu</w> <w n="36.4">berceras</w> <w n="36.5">notre</w> <w n="36.6">premier</w> <w n="36.7">enfant</w>.</l>
				</lg>
				<lg n="4">
					<l n="37" num="4.1"><w n="37.1">C</w>’<w n="37.2">était</w> <w n="37.3">déjà</w> <w n="37.4">son</w> <w n="37.5">rêve</w>, <w n="37.6">à</w> <w n="37.7">cet</w> <w n="37.8">homme</w>, <w n="37.9">être</w> <w n="37.10">père</w> !</l>
					<l n="38" num="4.2"><w n="38.1">Mais</w> <w n="38.2">il</w> <w n="38.3">ne</w> <w n="38.4">devait</w> <w n="38.5">pas</w> <w n="38.6">durer</w>, <w n="38.7">le</w> <w n="38.8">temps</w> <w n="38.9">prospère</w> :</l>
					<l n="39" num="4.3"><w n="39.1">Sa</w> <w n="39.2">femme</w> <w n="39.3">n</w>’<w n="39.4">aimait</w> <w n="39.5">pas</w> <w n="39.6">le</w> <w n="39.7">commerce</w> ; <w n="39.8">elle</w> <w n="39.9">était</w></l>
					<l n="40" num="4.4"><w n="40.1">Hargneuse</w>, <w n="40.2">lymphatique</w> <w n="40.3">et</w> <w n="40.4">froide</w> ; <w n="40.5">elle</w> <w n="40.6">restait</w></l>
					<l n="41" num="4.5"><w n="41.1">A</w> <w n="41.2">l</w>’<w n="41.3">écart</w> <w n="41.4">et</w> <w n="41.5">passait</w> <w n="41.6">des</w> <w n="41.7">heures</w> <w n="41.8">dans</w> <w n="41.9">sa</w> <w n="41.10">chambre</w>.</l>
					<l n="42" num="4.6"><w n="42.1">De</w> <w n="42.2">sa</w> <w n="42.3">boutique</w> <w n="42.4">ouverte</w> <w n="42.5">au</w> <w n="42.6">vent</w> <w n="42.7">froid</w> <w n="42.8">de</w> <w n="42.9">décembre</w>,</l>
					<l n="43" num="4.7"><w n="43.1">Lui</w> <w n="43.2">ne</w> <w n="43.3">pouvait</w> <w n="43.4">bouger</w>, <w n="43.5">mais</w> <w n="43.6">ne</w> <w n="43.7">se</w> <w n="43.8">plaignait</w> <w n="43.9">pas</w> ;</l>
					<l n="44" num="4.8"><w n="44.1">Car</w> <w n="44.2">sa</w> <w n="44.3">mère</w>, <w n="44.4">en</w> <w n="44.5">bonnet</w> <w n="44.6">et</w> <w n="44.7">tricotant</w> <w n="44.8">des</w> <w n="44.9">bas</w>,</l>
					<l n="45" num="4.9"><w n="45.1">Était</w> <w n="45.2">là</w>, <w n="45.3">toute</w> <w n="45.4">fière</w> <w n="45.5">et</w> <w n="45.6">de</w> <w n="45.7">son</w> <w n="45.8">fils</w> <w n="45.9">et</w> <w n="45.10">d</w>’<w n="45.11">elle</w>,</l>
					<l n="46" num="4.10"><w n="46.1">Tandis</w> <w n="46.2">qu</w>’<w n="46.3">il</w> <w n="46.4">débitait</w> <w n="46.5">le</w> <w n="46.6">beurre</w> <w n="46.7">et</w> <w n="46.8">la</w> <w n="46.9">chandelle</w>.</l>
					<l n="47" num="4.11"><w n="47.1">Donc</w> <w n="47.2">il</w> <w n="47.3">était</w> <w n="47.4">encor</w> <w n="47.5">satisfait</w> <w n="47.6">comme</w> <w n="47.7">ça</w>.</l>
					<l n="48" num="4.12"><w n="48.1">Mais</w>, <w n="48.2">dans</w> <w n="48.3">un</w> <w n="48.4">mauvais</w> <w n="48.5">jour</w>, <w n="48.6">sa</w> <w n="48.7">femme</w> <w n="48.8">s</w>’<w n="48.9">offensa</w></l>
					<l n="49" num="4.13"><w n="49.1">De</w> <w n="49.2">ce</w> <w n="49.3">qu</w>’<w n="49.4">il</w> <w n="49.5">ne</w> <w n="49.6">rut</w> <w n="49.7">pas</w> <w n="49.8">seul</w> <w n="49.9">comme</w> <w n="49.10">elle</w>, <w n="49.11">et</w> <w n="49.12">l</w>’<w n="49.13">épouse</w>,</l>
					<l n="50" num="4.14">— <w n="50.1">Vieille</w> <w n="50.2">histoire</w>, — <w n="50.3">devint</w> <w n="50.4">de</w> <w n="50.5">la</w> <w n="50.6">mère</w> <w n="50.7">jalouse</w>.</l>
					<l part="I" n="51" num="4.15"><w n="51.1">Celle</w>-<w n="51.2">ci</w> <w n="51.3">comprit</w> <w n="51.4">tout</w> : </l>
					<l part="F" n="51" num="4.15">— <w n="51.5">Mon</w> <w n="51.6">enfant</w>, <w n="51.7">j</w>’<w n="51.8">avais</w> <w n="51.9">cru</w>,</l>
					<l n="52" num="4.16"><w n="52.1">Lui</w> <w n="52.2">dit</w>-<w n="52.3">elle</w>, <w n="52.4">pouvoir</w> <w n="52.5">bien</w> <w n="52.6">vivre</w> <w n="52.7">avec</w> <w n="52.8">ma</w> <w n="52.9">bru</w>.</l>
					<l n="53" num="4.17"><w n="53.1">Mais</w>, <w n="53.2">à</w> <w n="53.3">la</w> <w n="53.4">fin</w>, <w n="53.5">il</w> <w n="53.6">faut</w> <w n="53.7">que</w> <w n="53.8">je</w> <w n="53.9">le</w> <w n="53.10">reconnaisse</w>,</l>
					<l n="54" num="4.18"><w n="54.1">Je</w> <w n="54.2">la</w> <w n="54.3">gêne</w> <w n="54.4">et</w> <w n="54.5">ne</w> <w n="54.6">puis</w> <w n="54.7">plaire</w> <w n="54.8">à</w> <w n="54.9">cette</w> <w n="54.10">jeunesse</w>.</l>
					<l n="55" num="4.19"><w n="55.1">Je</w> <w n="55.2">retourne</w> <w n="55.3">à</w> <w n="55.4">Soissons</w>, <w n="55.5">vois</w>-<w n="55.6">tu</w>, <w n="55.7">cela</w> <w n="55.8">vaut</w> <w n="55.9">mieux</w>.</l>
				</lg>
				<lg n="5">
					<l n="56" num="5.1"><w n="56.1">Elle</w> <w n="56.2">dit</w>, <w n="56.3">de</w> <w n="56.4">l</w>’<w n="56.5">air</w> <w n="56.6">doux</w> <w n="56.7">et</w> <w n="56.8">résigné</w> <w n="56.9">des</w> <w n="56.10">vieux</w>,</l>
					<l n="57" num="5.2"><w n="57.1">Et</w> <w n="57.2">partit</w>, <w n="57.3">sans</w> <w n="57.4">pleurer</w>, <w n="57.5">mais</w> <w n="57.6">affreusement</w> <w n="57.7">triste</w>.</l>
					<l n="58" num="5.3"><w n="58.1">Hélas</w> ! <w n="58.2">il</w> <w n="58.3">n</w>’<w n="58.4">avait</w> <w n="58.5">pas</w> <w n="58.6">ce</w> <w n="58.7">qui</w> <w n="58.8">fait</w> <w n="58.9">qu</w>’<w n="58.10">on</w> <w n="58.11">résiste</w>.</l>
					<l n="59" num="5.4"><w n="59.1">Il</w> <w n="59.2">consentit</w>, <w n="59.3">devint</w> <w n="59.4">plus</w> <w n="59.5">morose</w> <w n="59.6">qu</w>’<w n="59.7">avant</w></l>
					<l n="60" num="5.5"><w n="60.1">Et</w> <w n="60.2">pria</w>, <w n="60.3">tous</w> <w n="60.4">les</w> <w n="60.5">soirs</w>, <w n="60.6">pour</w> <w n="60.7">avoir</w> <w n="60.8">un</w> <w n="60.9">enfant</w>.</l>
					<l n="61" num="5.6"><w n="61.1">Car</w> <w n="61.2">c</w>’<w n="61.3">était</w> <w n="61.4">là</w> <w n="61.5">son</w> <w n="61.6">but</w>, <w n="61.7">décidément</w>. <w n="61.8">Ce</w> <w n="61.9">rêve</w>,</l>
					<l n="62" num="5.7"><w n="62.1">Cet</w> <w n="62.2">instinct</w>, <w n="62.3">ce</w> <w n="62.4">besoin</w> <w n="62.5">le</w> <w n="62.6">poursuivait</w> <w n="62.7">sans</w> <w n="62.8">trêve</w>,</l>
					<l n="63" num="5.8"><w n="63.1">Il</w> <w n="63.2">n</w>’<w n="63.3">avait</w> <w n="63.4">qu</w>’<w n="63.5">un</w> <w n="63.6">désir</w>, <w n="63.7">il</w> <w n="63.8">n</w>’<w n="63.9">avait</w> <w n="63.10">qu</w>’<w n="63.11">un</w> <w n="63.12">espoir</w> :</l>
					<l n="64" num="5.9"><w n="64.1">Être</w> <w n="64.2">père</w> ! <w n="64.3">c</w>’<w n="64.4">était</w> <w n="64.5">son</w> <w n="64.6">idéal</w>. — <w n="64.7">Le</w> <w n="64.8">soir</w>,</l>
					<l n="65" num="5.10"><w n="65.1">Quand</w> <w n="65.2">un</w> <w n="65.3">noir</w> <w n="65.4">ouvrier</w>, <w n="65.5">portant</w> <w n="65.6">un</w> <w n="65.7">enfant</w> <w n="65.8">rose</w>,</l>
					<l n="66" num="5.11"><w n="66.1">Entrait</w> <w n="66.2">dans</w> <w n="66.3">sa</w> <w n="66.4">boutique</w> <w n="66.5">acheter</w> <w n="66.6">quelque</w> <w n="66.7">chose</w>,</l>
					<l n="67" num="5.12"><w n="67.1">Soudain</w> <w n="67.2">il</w> <w n="67.3">se</w> <w n="67.4">sentait</w> <w n="67.5">plein</w> <w n="67.6">d</w>’<w n="67.7">attendrissement</w>.</l>
				</lg>
				<lg n="6">
					<l n="68" num="6.1"><w n="68.1">Mais</w> <w n="68.2">les</w> <w n="68.3">ans</w> <w n="68.4">ont</w> <w n="68.5">passé</w>, <w n="68.6">lentement</w>, <w n="68.7">lentement</w>.</l>
					<l n="69" num="6.2"><w n="69.1">Il</w> <w n="69.2">comprend</w> <w n="69.3">aujourd</w>’<w n="69.4">hui</w> <w n="69.5">que</w> <w n="69.6">ce</w> <w n="69.7">n</w>’<w n="69.8">est</w> <w n="69.9">pas</w> <w n="69.10">possible</w> ;</l>
					<l n="70" num="6.3"><w n="70.1">Il</w> <w n="70.2">partage</w> <w n="70.3">le</w> <w n="70.4">lit</w> <w n="70.5">d</w>’<w n="70.6">une</w> <w n="70.7">femme</w> <w n="70.8">insensible</w>,</l>
					<l n="71" num="6.4"><w n="71.1">Et</w> <w n="71.2">tous</w> <w n="71.3">les</w> <w n="71.4">deux</w> <w n="71.5">ils</w> <w n="71.6">ont</w> <w n="71.7">froid</w> <w n="71.8">au</w> <w n="71.9">cœur</w>, <w n="71.10">froid</w> <w n="71.11">aux</w> <w n="71.12">pieds</w>.</l>
					<l n="72" num="6.5">— <w n="72.1">Ah</w> ! <w n="72.2">les</w> <w n="72.3">rêves</w> <w n="72.4">aussi</w> <w n="72.5">durement</w> <w n="72.6">expiés</w></l>
					<l n="73" num="6.6"><w n="73.1">Allument</w> <w n="73.2">à</w> <w n="73.3">la</w> <w n="73.4">longue</w> <w n="73.5">un</w> <w n="73.6">désespoir</w> <w n="73.7">qui</w> <w n="73.8">couve</w> !</l>
					<l n="74" num="6.7"><w n="74.1">Cet</w> <w n="74.2">homme</w> <w n="74.3">est</w> <w n="74.4">fatigué</w> <w n="74.5">de</w> <w n="74.6">l</w>’<w n="74.7">existence</w>. <w n="74.8">Il</w> <w n="74.9">trouve</w>,</l>
					<l n="75" num="6.8">— <w n="75.1">Où</w> <w n="75.2">de</w> <w n="75.3">pareils</w> <w n="75.4">dégoûts</w> <w n="75.5">vont</w>-<w n="75.6">ils</w> <w n="75.7">donc</w> <w n="75.8">se</w> <w n="75.9">nicher</w> —</l>
					<l n="76" num="6.9"><w n="76.1">La</w> <w n="76.2">colle</w> <w n="76.3">et</w> <w n="76.4">le</w> <w n="76.5">fromage</w> <w n="76.6">ignobles</w> <w n="76.7">à</w> <w n="76.8">toucher</w>.</l>
					<l n="77" num="6.10"><w n="77.1">Il</w> <w n="77.2">hait</w> <w n="77.3">le</w> <w n="77.4">vent</w> <w n="77.5">coulis</w> <w n="77.6">qui</w> <w n="77.7">souffle</w> <w n="77.8">de</w> <w n="77.9">la</w> <w n="77.10">rue</w>,</l>
					<l n="78" num="6.11"><w n="78.1">Il</w> <w n="78.2">ne</w> <w n="78.3">peut</w> <w n="78.4">plus</w> <w n="78.5">sentir</w> <w n="78.6">l</w>’<w n="78.7">odeur</w> <w n="78.8">de</w> <w n="78.9">la</w> <w n="78.10">morue</w>,</l>
					<l n="79" num="6.12"><w n="79.1">Et</w> <w n="79.2">ses</w> <w n="79.3">doigts</w> <w n="79.4">crevassés</w>, <w n="79.5">maudissant</w> <w n="79.6">leur</w> <w n="79.7">destin</w>,</l>
					<l n="80" num="6.13"><w n="80.1">Ont</w> <w n="80.2">trop</w> <w n="80.3">froid</w> <w n="80.4">au</w> <w n="80.5">contact</w> <w n="80.6">des</w> <w n="80.7">entonnoirs</w> <w n="80.8">d</w>’<w n="80.9">étain</w> !</l>
				</lg>
				<lg n="7">
					<l n="81" num="7.1"><w n="81.1">Pourtant</w> <w n="81.2">il</w> <w n="81.3">brille</w> <w n="81.4">encore</w> <w n="81.5">un</w> <w n="81.6">rayon</w> <w n="81.7">dans</w> <w n="81.8">cette</w> <w n="81.9">ombre</w>.</l>
					<l n="82" num="7.2"><w n="82.1">Derrière</w> <w n="82.2">son</w> <w n="82.3">comptoir</w>, <w n="82.4">seul</w>, <w n="82.5">debout</w>, <w n="82.6">le</w> <w n="82.7">cœur</w> <w n="82.8">sombre</w>,</l>
					<l n="83" num="7.3"><w n="83.1">Quand</w> <w n="83.2">il</w> <w n="83.3">casse</w> <w n="83.4">du</w> <w n="83.5">sucre</w> <w n="83.6">avec</w> <w n="83.7">férocité</w>,</l>
					<l n="84" num="7.4"><w n="84.1">Parfois</w> <w n="84.2">entre</w> <w n="84.3">un</w> <w n="84.4">enfant</w>, <w n="84.5">un</w> <w n="84.6">doux</w> <w n="84.7">blondin</w>, <w n="84.8">tenté</w></l>
					<l n="85" num="7.5"><w n="85.1">Par</w> <w n="85.2">les</w> <w n="85.3">trésors</w> <w n="85.4">poudreux</w> <w n="85.5">du</w> <w n="85.6">petit</w> <w n="85.7">étalage</w>.</l>
					<l n="86" num="7.6"><w n="86.1">Dans</w> <w n="86.2">la</w> <w n="86.3">naïveté</w> <w n="86.4">du</w> <w n="86.5">désir</w> <w n="86.6">et</w> <w n="86.7">de</w> <w n="86.8">l</w>’<w n="86.9">âge</w>,</l>
					<l n="87" num="7.7"><w n="87.1">Il</w> <w n="87.2">montre</w> <w n="87.3">d</w>’<w n="87.4">une</w> <w n="87.5">main</w> <w n="87.6">le</w> <w n="87.7">bonbon</w> <w n="87.8">alléchant</w></l>
					<l n="88" num="7.8"><w n="88.1">Et</w> <w n="88.2">de</w> <w n="88.3">l</w>’<w n="88.4">autre</w> <w n="88.5">il</w> <w n="88.6">présente</w> <w n="88.7">un</w> <w n="88.8">sou</w> <w n="88.9">noir</w> <w n="88.10">au</w> <w n="88.11">marchand</w>.</l>
					<l n="89" num="7.9"><w n="89.1">L</w>’<w n="89.2">homme</w> <w n="89.3">alors</w> <w n="89.4">est</w> <w n="89.5">heureux</w> <w n="89.6">plus</w> <w n="89.7">qu</w>’<w n="89.8">on</w> <w n="89.9">ne</w> <w n="89.10">peut</w> <w n="89.11">le</w> <w n="89.12">dire</w></l>
					<l n="90" num="7.10"><w n="90.1">Et</w>, <w n="90.2">tout</w> <w n="90.3">en</w> <w n="90.4">souriant</w>, — <w n="90.5">s</w>’<w n="90.6">ils</w> <w n="90.7">voyaient</w> <w n="90.8">ce</w> <w n="90.9">sourire</w>,</l>
					<l n="91" num="7.11"><w n="91.1">Les</w> <w n="91.2">autres</w> <w n="91.3">épiciers</w> <w n="91.4">le</w> <w n="91.5">prendraient</w> <w n="91.6">pour</w> <w n="91.7">un</w> <w n="91.8">fou</w>, —</l>
					<l n="92" num="7.12"><w n="92.1">Il</w> <w n="92.2">donne</w> <w n="92.3">le</w> <w n="92.4">bonbon</w> <w n="92.5">et</w> <w n="92.6">refuse</w> <w n="92.7">le</w> <w n="92.8">sou</w>.</l>
				</lg>
				<lg n="8">
					<l n="93" num="8.1"><w n="93.1">Mais</w> <w n="93.2">aussi</w>, <w n="93.3">ces</w> <w n="93.4">jours</w>-<w n="93.5">là</w>, <w n="93.6">sa</w> <w n="93.7">tristesse</w> <w n="93.8">est</w> <w n="93.9">plus</w> <w n="93.10">douce</w> ;</l>
					<l n="94" num="8.2"><w n="94.1">S</w>’<w n="94.2">il</w> <w n="94.3">lui</w> <w n="94.4">vient</w> <w n="94.5">un</w> <w n="94.6">dégoût</w> <w n="94.7">coupable</w>, <w n="94.8">il</w> <w n="94.9">le</w> <w n="94.10">repousse</w> ;</l>
					<l n="95" num="8.3"><w n="95.1">Il</w> <w n="95.2">rêve</w>, <w n="95.3">il</w> <w n="95.4">croit</w> <w n="95.5">revoir</w> <w n="95.6">sa</w> <w n="95.7">mère</w> <w n="95.8">qui</w> <w n="95.9">partit</w>,</l>
					<l n="96" num="8.4"><w n="96.1">Soissons</w>, <w n="96.2">et</w> <w n="96.3">le</w> <w n="96.4">bon</w> <w n="96.5">temps</w>, <w n="96.6">quand</w> <w n="96.7">il</w> <w n="96.8">était</w> <w n="96.9">petit</w>.</l>
					<l n="97" num="8.5"><w n="97.1">Le</w> <w n="97.2">pauvre</w> <w n="97.3">être</w> <w n="97.4">pardonne</w>, <w n="97.5">il</w> <w n="97.6">s</w>’<w n="97.7">apaise</w>, <w n="97.8">il</w> <w n="97.9">oublie</w>,</l>
					<l n="98" num="8.6"><w n="98.1">Et</w>, <w n="98.2">lent</w>, <w n="98.3">casse</w> <w n="98.4">son</w> <w n="98.5">sucre</w> <w n="98.6">avec</w> <w n="98.7">mélancolie</w>.</l>
				</lg>
			</div></body></text></TEI>