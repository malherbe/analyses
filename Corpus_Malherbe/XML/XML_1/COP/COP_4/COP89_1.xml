<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES HUMBLES</title>
				<title type="medium">Édition électronique</title>
				<author key="COP">
					<name>
						<forename>François</forename>
						<surname>COPPÉE</surname>
					</name>
					<date from="1842" to="1908">1842-1908</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1160 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">COP_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LES HUMBLES</title>
						<author>François Coppée</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/francoiscopeeleshumbles.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Œuvres complètes de François Coppée</title>
						<author>François Coppée</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie L. Hébert</publisher>
							<date when="1885">1885</date>
						</imprint>
						<biblScope unit="tome">Poésie, tome 1</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1872">1872</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="COP89">
				<head type="main">Simple Ambition</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Être</w> <w n="1.2">un</w> <w n="1.3">modeste</w> <w n="1.4">croque</w>-<w n="1.5">notes</w></l>
					<l n="2" num="1.2"><w n="2.1">Donnant</w> <w n="2.2">des</w> <w n="2.3">leçons</w> <w n="2.4">de</w> <w n="2.5">hasard</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Qui</w> <w n="3.2">court</w> <w n="3.3">Paris</w> <w n="3.4">en</w> <w n="3.5">grosses</w> <w n="3.6">bottes</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Mais</w> <w n="4.2">qui</w> <w n="4.3">comprend</w> <w n="4.4">Gluck</w> <w n="4.5">et</w> <w n="4.6">Mozart</w> ;</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Avoir</w> <w n="5.2">quelque</w> <w n="5.3">part</w> <w n="5.4">un</w> <w n="5.5">vieux</w> <w n="5.6">maitre</w> ;</l>
					<l n="6" num="2.2"><w n="6.1">Aimer</w> <w n="6.2">sa</w> <w n="6.3">fille</w> ; <w n="6.4">et</w>, <w n="6.5">chaque</w> <w n="6.6">soir</w>,</l>
					<l n="7" num="2.3"><w n="7.1">Brosser</w> <w n="7.2">son</w> <w n="7.3">vieil</w> <w n="7.4">habit</w> <w n="7.5">et</w> <w n="7.6">mettre</w></l>
					<l n="8" num="2.4"><w n="8.1">Du</w> <w n="8.2">linge</w> <w n="8.3">pour</w> <w n="8.4">aller</w> <w n="8.5">les</w> <w n="8.6">voir</w> ;</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Ils</w> <w n="9.2">logent</w> <w n="9.3">loin</w> ! <w n="9.4">Faire</w> <w n="9.5">une</w> <w n="9.6">lieue</w></l>
					<l n="10" num="3.2"><w n="10.1">En</w> <w n="10.2">chantonnant</w> <w n="10.3">quelques</w> <w n="10.4">vieux</w> <w n="10.5">airs</w>,</l>
					<l n="11" num="3.3"><w n="11.1">L</w>’<w n="11.2">été</w> <w n="11.3">sous</w> <w n="11.4">la</w> <w n="11.5">douce</w> <w n="11.6">nuit</w> <w n="11.7">bleue</w></l>
					<l n="12" num="3.4"><w n="12.1">Et</w> <w n="12.2">par</w> <w n="12.3">les</w> <w n="12.4">bons</w> <w n="12.5">quartiers</w> <w n="12.6">déserts</w> ;</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Aimer</w> <w n="13.2">d</w>’<w n="13.3">un</w> <w n="13.4">amour</w> <w n="13.5">très</w>-<w n="13.6">honnête</w> ;</l>
					<l n="14" num="4.2"><w n="14.1">Avoir</w> <w n="14.2">peur</w>, <w n="14.3">en</w> <w n="14.4">portant</w> <w n="14.5">la</w> <w n="14.6">main</w></l>
					<l n="15" num="4.3"><w n="15.1">A</w> <w n="15.2">certain</w> <w n="15.3">cordon</w> <w n="15.4">de</w> <w n="15.5">sonnette</w></l>
					<l n="16" num="4.4"><w n="16.1">Dont</w> <w n="16.2">on</w> <w n="16.3">sait</w> <w n="16.4">pourtant</w> <w n="16.5">le</w> <w n="16.6">chemin</w>…</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1">— <w n="17.1">Ah</w> ! <w n="17.2">monsieur</w> <w n="17.3">Paul</w> ! — <w n="17.4">Mademoiselle</w> !</l>
					<l n="18" num="5.2">— <w n="18.1">Mon</w> <w n="18.2">père</w> <w n="18.3">vous</w> <w n="18.4">attend</w>. <w n="18.5">Voyez</w>.</l>
					<l n="19" num="5.3"><w n="19.1">Voici</w> <w n="19.2">votre</w> <w n="19.3">violoncelle</w>,</l>
					<l n="20" num="5.4"><w n="20.1">Son</w> <w n="20.2">violon</w> <w n="20.3">et</w> <w n="20.4">les</w> <w n="20.5">cahiers</w>.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">Demander</w> <w n="21.2">comment</w> <w n="21.3">va</w> <w n="21.4">le</w> <w n="21.5">maître</w>,</l>
					<l n="22" num="6.2"><w n="22.1">Qui</w> <w n="22.2">survient</w>, <w n="22.3">simple</w> <w n="22.4">et</w> <w n="22.5">cordial</w> ;</l>
					<l n="23" num="6.3"><w n="23.1">Oh</w> ! <w n="23.2">le</w> <w n="23.3">bon</w> <w n="23.4">moment</w> ! — <w n="23.5">La</w> <w n="23.6">fenêtre</w></l>
					<l n="24" num="6.4"><w n="24.1">S</w>’<w n="24.2">ouvre</w> <w n="24.3">sur</w> <w n="24.4">le</w> <w n="24.5">ciel</w> <w n="24.6">nuptial</w> ;</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">Les</w> <w n="25.2">brises</w> <w n="25.3">déjà</w> <w n="25.4">rafraichies</w></l>
					<l n="26" num="7.2"><w n="26.1">Entrent</w> <w n="26.2">avec</w> <w n="26.3">des</w> <w n="26.4">papillons</w></l>
					<l n="27" num="7.3"><w n="27.1">Bien</w> <w n="27.2">vite</w> <w n="27.3">brûlés</w> <w n="27.4">aux</w> <w n="27.5">bougies</w></l>
					<l n="28" num="7.4"><w n="28.1">Qui</w> <w n="28.2">jettent</w> <w n="28.3">de</w> <w n="28.4">faibles</w> <w n="28.5">rayons</w>.</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1">Le</w> <w n="29.2">concert</w> <w n="29.3">commence</w>. <w n="29.4">Elle</w> <w n="29.5">écoute</w>,</l>
					<l n="30" num="8.2"><w n="30.1">Blonde</w>, <w n="30.2">accoudée</w> <w n="30.3">et</w> <w n="30.4">tout</w> <w n="30.5">en</w> <w n="30.6">blanc</w>,</l>
					<l n="31" num="8.3"><w n="31.1">Et</w> <w n="31.2">son</w> <w n="31.3">cœur</w> <w n="31.4">frissonne</w> <w n="31.5">sans</w> <w n="31.6">doute</w></l>
					<l n="32" num="8.4"><w n="32.1">Avec</w> <w n="32.2">l</w>’<w n="32.3">allegretto</w> <w n="32.4">tremblant</w>.</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1"><w n="33.1">Puis</w>, <w n="33.2">c</w>’<w n="33.3">est</w> <w n="33.4">le</w> <w n="33.5">menuet</w>, <w n="33.6">l</w>’<w n="33.7">andante</w>,</l>
					<l n="34" num="9.2"><w n="34.1">Tout</w> <w n="34.2">le</w> <w n="34.3">beau</w> <w n="34.4">poëme</w> <w n="34.5">du</w> <w n="34.6">bruit</w>,</l>
					<l n="35" num="9.3"><w n="35.1">Toute</w> <w n="35.2">la</w> <w n="35.3">symphonie</w> <w n="35.4">ardente</w>.</l>
					<l n="36" num="9.4"><w n="36.1">Et</w> <w n="36.2">le</w> <w n="36.3">temps</w> <w n="36.4">passe</w>. <w n="36.5">Il</w> <w n="36.6">est</w> <w n="36.7">minuit</w>.</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1">— <w n="37.1">Sauvez</w>-<w n="37.2">vous</w>. <w n="37.3">C</w>’<w n="37.4">est</w> <w n="37.5">une</w> <w n="37.6">heure</w> <w n="37.7">indue</w></l>
					<l n="38" num="10.2"><w n="38.1">Pour</w> <w n="38.2">vous</w> <w n="38.3">qui</w> <w n="38.4">logez</w> <w n="38.5">tout</w> <w n="38.6">là</w>-<w n="38.7">bas</w> ;</l>
					<l n="39" num="10.3"><w n="39.1">Et</w> <w n="39.2">cette</w> <w n="39.3">banlieue</w> <w n="39.4">est</w> <w n="39.5">perdue</w>.</l>
					<l n="40" num="10.4"><w n="40.1">Vous</w> <w n="40.2">viendrez</w> <w n="40.3">demain</w>, <w n="40.4">n</w>’<w n="40.5">est</w>-<w n="40.6">ce</w> <w n="40.7">pas</w> ?</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1"><w n="41.1">Mais</w> <w n="41.2">avant</w> <w n="41.3">de</w> <w n="41.4">partir</w>, <w n="41.5">encore</w></l>
					<l n="42" num="11.2"><w n="42.1">Un</w> <w n="42.2">peu</w> <w n="42.3">de</w> <w n="42.4">musique</w> ; <w n="42.5">pas</w> <w n="42.6">trop</w> —</l>
					<l n="43" num="11.3"><w n="43.1">Pendant</w> <w n="43.2">que</w> <w n="43.3">Julie</w> <w n="43.4">élabore</w></l>
					<l n="44" num="11.4"><w n="44.1">Trois</w> <w n="44.2">humbles</w> <w n="44.3">verres</w> <w n="44.4">de</w> <w n="44.5">sirop</w>.</l>
				</lg>
			</div></body></text></TEI>