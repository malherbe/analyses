<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES HUMBLES</title>
				<title type="medium">Édition électronique</title>
				<author key="COP">
					<name>
						<forename>François</forename>
						<surname>COPPÉE</surname>
					</name>
					<date from="1842" to="1908">1842-1908</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1160 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">COP_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LES HUMBLES</title>
						<author>François Coppée</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/francoiscopeeleshumbles.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Œuvres complètes de François Coppée</title>
						<author>François Coppée</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie L. Hébert</publisher>
							<date when="1885">1885</date>
						</imprint>
						<biblScope unit="tome">Poésie, tome 1</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1872">1872</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="COP88">
				<head type="main">Une Femme seule</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Dans</w> <w n="1.2">le</w> <w n="1.3">salon</w> <w n="1.4">bourgeois</w> <w n="1.5">où</w> <w n="1.6">je</w> <w n="1.7">l</w>’<w n="1.8">ai</w> <w n="1.9">rencontrée</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Ses</w> <w n="2.2">yeux</w> <w n="2.3">doux</w> <w n="2.4">et</w> <w n="2.5">craintifs</w>, <w n="2.6">son</w> <w n="2.7">front</w> <w n="2.8">d</w>’<w n="2.9">ange</w> <w n="2.10">proscrit</w>,</l>
					<l n="3" num="1.3"><w n="3.1">M</w>’<w n="3.2">attirèrent</w> <w n="3.3">d</w>’<w n="3.4">abord</w> <w n="3.5">vers</w> <w n="3.6">elle</w>, <w n="3.7">et</w> <w n="3.8">l</w>’<w n="3.9">on</w> <w n="3.10">m</w>’<w n="3.11">apprit</w></l>
					<l n="4" num="1.4"><w n="4.1">Que</w> <w n="4.2">d</w>’<w n="4.3">un</w> <w n="4.4">mari</w> <w n="4.5">brutal</w> <w n="4.6">elle</w> <w n="4.7">était</w> <w n="4.8">séparée</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Elle</w> <w n="5.2">venait</w> <w n="5.3">encor</w> <w n="5.4">chez</w> <w n="5.5">ces</w> <w n="5.6">anciens</w> <w n="5.7">amis</w>,</l>
					<l n="6" num="2.2"><w n="6.1">Dont</w> <w n="6.2">la</w> <w n="6.3">maison</w> <w n="6.4">avait</w> <w n="6.5">vu</w> <w n="6.6">grandir</w> <w n="6.7">son</w> <w n="6.8">enfance</w></l>
					<l n="7" num="2.3"><w n="7.1">Et</w> <w n="7.2">qui</w>, <w n="7.3">malgré</w> <w n="7.4">le</w> <w n="7.5">bruit</w> <w n="7.6">dont</w> <w n="7.7">le</w> <w n="7.8">monde</w> <w n="7.9">s</w>’<w n="7.10">offense</w>,</l>
					<l n="8" num="2.4"><w n="8.1">Au</w> <w n="8.2">préjugé</w> <w n="8.3">cruel</w> <w n="8.4">ne</w> <w n="8.5">s</w>’<w n="8.6">étaient</w> <w n="8.7">point</w> <w n="8.8">soumis</w>,</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Mais</w> <w n="9.2">elle</w> <w n="9.3">savait</w> <w n="9.4">bien</w>, <w n="9.5">résignée</w> <w n="9.6">et</w> <w n="9.7">très</w>-<w n="9.8">douce</w>,</l>
					<l n="10" num="3.2"><w n="10.1">Qu</w>’<w n="10.2">on</w> <w n="10.3">ne</w> <w n="10.4">la</w> <w n="10.5">recevait</w> <w n="10.6">qu</w>’<w n="10.7">en</w> <w n="10.8">petit</w> <w n="10.9">comité</w>,</l>
					<l n="11" num="3.3"><w n="11.1">Et</w> <w n="11.2">s</w>’<w n="11.3">attendait</w> <w n="11.4">toujours</w>, <w n="11.5">dans</w> <w n="11.6">sa</w> <w n="11.7">tranquillité</w>,</l>
					<l n="12" num="3.4"><w n="12.1">Au</w> <w n="12.2">mot</w> <w n="12.3">qui</w> <w n="12.4">congédie</w>, <w n="12.5">à</w> <w n="12.6">l</w>’<w n="12.7">accueil</w> <w n="12.8">qui</w> <w n="12.9">repousse</w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Donc</w>, <w n="13.2">les</w> <w n="13.3">soirs</w> <w n="13.4">sans</w> <w n="13.5">dîner</w> <w n="13.6">ni</w> <w n="13.7">bal</w> <w n="13.8">au</w> <w n="13.9">piano</w>,</l>
					<l n="14" num="4.2"><w n="14.1">Elle</w> <w n="14.2">venait</w> <w n="14.3">broder</w> <w n="14.4">près</w> <w n="14.5">de</w> <w n="14.6">l</w>’<w n="14.7">âtre</w>, <w n="14.8">en</w> <w n="14.9">famille</w>,</l>
					<l n="15" num="4.3"><w n="15.1">Et</w> <w n="15.2">c</w>’<w n="15.3">est</w> <w n="15.4">là</w> <w n="15.5">que</w>, <w n="15.6">devant</w> <w n="15.7">son</w> <w n="15.8">air</w> <w n="15.9">de</w> <w n="15.10">jeune</w> <w n="15.11">fille</w>,</l>
					<l n="16" num="4.4"><w n="16.1">Je</w> <w n="16.2">m</w>’<w n="16.3">étonnai</w> <w n="16.4">de</w> <w n="16.5">voir</w> <w n="16.6">à</w> <w n="16.7">son</w> <w n="16.8">doigt</w> <w n="16.9">un</w> <w n="16.10">anneau</w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">Stoïque</w>, <w n="17.2">elle</w> <w n="17.3">acceptait</w> <w n="17.4">son</w> <w n="17.5">étrange</w> <w n="17.6">veuvage</w>,</l>
					<l n="18" num="5.2"><w n="18.1">Sans</w> <w n="18.2">arrière</w>-<w n="18.3">pensée</w> <w n="18.4">et</w> <w n="18.5">très</w> <w n="18.6">naïvement</w> ;</l>
					<l n="19" num="5.3"><w n="19.1">Pour</w> <w n="19.2">prouver</w> <w n="19.3">qu</w>’<w n="19.4">elle</w> <w n="19.5">était</w> <w n="19.6">fidèle</w> <w n="19.7">à</w> <w n="19.8">son</w> <w n="19.9">serment</w>,</l>
					<l n="20" num="5.4"><w n="20.1">Sa</w> <w n="20.2">main</w> <w n="20.3">avait</w> <w n="20.4">gardé</w> <w n="20.5">le</w> <w n="20.6">signe</w> <w n="20.7">d</w>’<w n="20.8">esclavage</w>.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">Elle</w> <w n="21.2">était</w> <w n="21.3">pâle</w> <w n="21.4">et</w> <w n="21.5">brune</w>, <w n="21.6">elle</w> <w n="21.7">avait</w> <w n="21.8">vingt</w>-<w n="21.9">cinq</w> <w n="21.10">ans</w>.</l>
					<l n="22" num="6.2"><w n="22.1">Le</w> <w n="22.2">sang</w> <w n="22.3">veinait</w> <w n="22.4">de</w> <w n="22.5">bleu</w> <w n="22.6">ses</w> <w n="22.7">mains</w> <w n="22.8">longues</w> <w n="22.9">et</w> <w n="22.10">fières</w>,</l>
					<l n="23" num="6.3"><w n="23.1">Et</w>, <w n="23.2">nerveux</w>, <w n="23.3">les</w> <w n="23.4">longs</w> <w n="23.5">cils</w> <w n="23.6">de</w> <w n="23.7">ses</w> <w n="23.8">chastes</w> <w n="23.9">paupières</w></l>
					<l n="24" num="6.4"><w n="24.1">Voilaient</w> <w n="24.2">ses</w> <w n="24.3">regards</w> <w n="24.4">bruns</w> <w n="24.5">de</w> <w n="24.6">battements</w> <w n="24.7">fréquents</w>.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">Ni</w> <w n="25.2">bijou</w>, <w n="25.3">ni</w> <w n="25.4">ruban</w>. <w n="25.5">Nulle</w> <w n="25.6">marque</w> <w n="25.7">de</w> <w n="25.8">joie</w>.</l>
					<l n="26" num="7.2"><w n="26.1">Jamais</w> <w n="26.2">la</w> <w n="26.3">moindre</w> <w n="26.4">fleur</w> <w n="26.5">dans</w> <w n="26.6">le</w> <w n="26.7">bandeau</w> <w n="26.8">châtain</w> ;</l>
					<l n="27" num="7.3"><w n="27.1">Et</w> <w n="27.2">le</w> <w n="27.3">petit</w> <w n="27.4">col</w> <w n="27.5">blanc</w>, <w n="27.6">étroit</w> <w n="27.7">et</w> <w n="27.8">puritain</w>,</l>
					<l n="28" num="7.4"><w n="28.1">Tranchait</w> <w n="28.2">seul</w> <w n="28.3">sur</w> <w n="28.4">le</w> <w n="28.5">deuil</w> <w n="28.6">de</w> <w n="28.7">la</w> <w n="28.8">robe</w> <w n="28.9">de</w> <w n="28.10">soie</w></l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1">Brodant</w> <w n="29.2">très</w>-<w n="29.3">lentement</w> <w n="29.4">et</w> <w n="29.5">d</w>’<w n="29.6">un</w> <w n="29.7">geste</w> <w n="29.8">assoupli</w></l>
					<l n="30" num="8.2"><w n="30.1">Et</w> <w n="30.2">ne</w> <w n="30.3">se</w> <w n="30.4">doutant</w> <w n="30.5">pas</w> <w n="30.6">que</w> <w n="30.7">l</w>’<w n="30.8">ombre</w> <w n="30.9">transfigure</w>,</l>
					<l n="31" num="8.3"><w n="31.1">Sa</w> <w n="31.2">place</w> <w n="31.3">dans</w> <w n="31.4">la</w> <w n="31.5">chambre</w> <w n="31.6">était</w> <w n="31.7">la</w> <w n="31.8">plus</w> <w n="31.9">obscure</w> ;</l>
					<l n="32" num="8.4"><w n="32.1">Elle</w> <w n="32.2">parlait</w> <w n="32.3">à</w> <w n="32.4">peine</w> <w n="32.5">et</w> <w n="32.6">désirait</w> <w n="32.7">l</w>’<w n="32.8">oubli</w>.</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1"><w n="33.1">Mais</w>, <w n="33.2">à</w> <w n="33.3">la</w> <w n="33.4">question</w> <w n="33.5">banale</w> <w n="33.6">qu</w>’<w n="33.7">on</w> <w n="33.8">adresse</w></l>
					<l n="34" num="9.2"><w n="34.1">Quand</w> <w n="34.2">elle</w> <w n="34.3">répondait</w> <w n="34.4">quelques</w> <w n="34.5">mots</w> <w n="34.6">en</w> <w n="34.7">passant</w>,</l>
					<l n="35" num="9.3"><w n="35.1">Cela</w> <w n="35.2">faisait</w> <w n="35.3">du</w> <w n="35.4">mal</w> <w n="35.5">d</w>’<w n="35.6">entendre</w> <w n="35.7">cet</w> <w n="35.8">accent</w></l>
					<l n="36" num="9.4"><w n="36.1">Brisé</w> <w n="36.2">par</w> <w n="36.3">la</w> <w n="36.4">douleur</w> <w n="36.5">et</w> <w n="36.6">fait</w> <w n="36.7">pour</w> <w n="36.8">la</w> <w n="36.9">tendresse</w>,</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1"><w n="37.1">Cette</w> <w n="37.2">voix</w> <w n="37.3">lente</w> <w n="37.4">et</w> <w n="37.5">pure</w>, <w n="37.6">et</w> <w n="37.7">lasse</w> <w n="37.8">de</w> <w n="37.9">prier</w>,</l>
					<l n="38" num="10.2"><w n="38.1">Qu</w>’<w n="38.2">interrompait</w> <w n="38.3">jadis</w> <w n="38.4">la</w> <w n="38.5">forte</w> <w n="38.6">voix</w> <w n="38.7">d</w>’<w n="38.8">un</w> <w n="38.9">maître</w></l>
					<l n="39" num="10.3"><w n="39.1">Et</w> <w n="39.2">qu</w>’<w n="39.3">une</w> <w n="39.4">insulte</w>, <w n="39.5">hélas</w> ! <w n="39.6">un</w> <w n="39.7">bras</w> <w n="39.8">levé</w> <w n="39.9">peut</w>-<w n="39.10">être</w>,</l>
					<l n="40" num="10.4"><w n="40.1">De</w> <w n="40.2">honte</w> <w n="40.3">et</w> <w n="40.4">de</w> <w n="40.5">terreur</w> <w n="40.6">un</w> <w n="40.7">jour</w> <w n="40.8">firent</w> <w n="40.9">crier</w>.</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1"><w n="41.1">Quand</w> <w n="41.2">un</w> <w n="41.3">petit</w> <w n="41.4">enfant</w> <w n="41.5">présentait</w> <w n="41.6">à</w> <w n="41.7">la</w> <w n="41.8">ronde</w></l>
					<l n="42" num="11.2"><w n="42.1">Son</w> <w n="42.2">front</w> <w n="42.3">à</w> <w n="42.4">nos</w> <w n="42.5">baisers</w>, <w n="42.6">oh</w> ! <w n="42.7">comme</w> <w n="42.8">lentement</w>,</l>
					<l n="43" num="11.3"><w n="43.1">Mélancoliquement</w> <w n="43.2">et</w> <w n="43.3">douloureusement</w>,</l>
					<l n="44" num="11.4"><w n="44.1">Ses</w> <w n="44.2">lèvres</w> <w n="44.3">s</w>’<w n="44.4">appuyaient</w> <w n="44.5">sur</w> <w n="44.6">cette</w> <w n="44.7">tête</w> <w n="44.8">blonde</w> !</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1"><w n="45.1">Mais</w> <w n="45.2">aussitôt</w> <w n="45.3">après</w> <w n="45.4">ce</w> <w n="45.5">trop</w> <w n="45.6">cruel</w> <w n="45.7">plaisir</w>,</l>
					<l n="46" num="12.2"><w n="46.1">Comme</w> <w n="46.2">elle</w> <w n="46.3">reprenait</w> <w n="46.4">son</w> <w n="46.5">travail</w> <w n="46.6">au</w> <w n="46.7">plus</w> <w n="46.8">vite</w> !</l>
					<l n="47" num="12.3"><w n="47.1">Et</w> <w n="47.2">sur</w> <w n="47.3">ses</w> <w n="47.4">traits</w> <w n="47.5">alors</w> <w n="47.6">quelle</w> <w n="47.7">rougeur</w> <w n="47.8">subite</w>,</l>
					<l n="48" num="12.4"><w n="48.1">En</w> <w n="48.2">songeant</w> <w n="48.3">au</w> <w n="48.4">regret</w> <w n="48.5">qu</w>’<w n="48.6">on</w> <w n="48.7">avait</w> <w n="48.8">pu</w> <w n="48.9">saisir</w> !</l>
				</lg>
				<lg n="13">
					<l n="49" num="13.1"><w n="49.1">Car</w> <w n="49.2">je</w> <w n="49.3">m</w>’<w n="49.4">apercevais</w>, <w n="49.5">quoiqu</w>’<w n="49.6">on</w> <w n="49.7">fût</w> <w n="49.8">bon</w> <w n="49.9">pour</w> <w n="49.10">elle</w>,</l>
					<l n="50" num="13.2"><w n="50.1">Qu</w>’<w n="50.2">on</w> <w n="50.3">la</w> <w n="50.4">plaignît</w> <w n="50.5">d</w>’<w n="50.6">avoir</w> <w n="50.7">fait</w> <w n="50.8">un</w> <w n="50.9">si</w> <w n="50.10">mauvais</w> <w n="50.11">choix</w>,</l>
					<l n="51" num="13.3"><w n="51.1">Que</w> <w n="51.2">ce</w> <w n="51.3">monde</w> <w n="51.4">aux</w> <w n="51.5">instincts</w> <w n="51.6">timorés</w> <w n="51.7">et</w> <w n="51.8">bourgeois</w></l>
					<l n="52" num="13.4"><w n="52.1">Conservait</w> <w n="52.2">une</w> <w n="52.3">crainte</w>, <w n="52.4">après</w> <w n="52.5">tout</w> <w n="52.6">naturelle</w>.</l>
				</lg>
				<lg n="14">
					<l n="53" num="14.1"><w n="53.1">J</w>’ <w n="53.2">avais</w> <w n="53.3">bien</w> <w n="53.4">remarqué</w> <w n="53.5">que</w> <w n="53.6">son</w> <w n="53.7">humble</w> <w n="53.8">regard</w></l>
					<l n="54" num="14.2"><w n="54.1">Tremblait</w> <w n="54.2">d</w>’<w n="54.3">être</w> <w n="54.4">heurté</w> <w n="54.5">par</w> <w n="54.6">un</w> <w n="54.7">regard</w> <w n="54.8">qui</w> <w n="54.9">brille</w>,</l>
					<l n="55" num="14.3"><w n="55.1">Qu</w>’<w n="55.2">elle</w> <w n="55.3">n</w>’<w n="55.4">allait</w> <w n="55.5">jamais</w> <w n="55.6">près</w> <w n="55.7">d</w>’<w n="55.8">une</w> <w n="55.9">jeune</w> <w n="55.10">fille</w></l>
					<l n="56" num="14.4"><w n="56.1">Et</w> <w n="56.2">ne</w> <w n="56.3">levait</w> <w n="56.4">les</w> <w n="56.5">yeux</w> <w n="56.6">que</w> <w n="56.7">devant</w> <w n="56.8">un</w> <w n="56.9">vieillard</w>.</l>
				</lg>
				<lg n="15">
					<l n="57" num="15.1">— <w n="57.1">Jeune</w> <w n="57.2">homme</w> <w n="57.3">qui</w> <w n="57.4">pourrais</w> <w n="57.5">aimer</w> <w n="57.6">la</w> <w n="57.7">pauvre</w> <w n="57.8">femme</w></l>
					<l n="58" num="15.2"><w n="58.1">Et</w> <w n="58.2">qui</w> <w n="58.3">la</w> <w n="58.4">trouveras</w> <w n="58.5">quelque</w> <w n="58.6">jour</w> <w n="58.7">sur</w> <w n="58.8">tes</w> <w n="58.9">pas</w>,</l>
					<l n="59" num="15.3"><w n="59.1">Ne</w> <w n="59.2">la</w> <w n="59.3">regarde</w> <w n="59.4">pas</w> <w n="59.5">et</w> <w n="59.6">ne</w> <w n="59.7">lui</w> <w n="59.8">parle</w> <w n="59.9">pas</w>.</l>
					<l n="60" num="15.4"><w n="60.1">Ne</w> <w n="60.2">te</w> <w n="60.3">fais</w> <w n="60.4">pas</w> <w n="60.5">aimer</w>, <w n="60.6">car</w> <w n="60.7">ce</w> <w n="60.8">serait</w> <w n="60.9">infâme</w> !</l>
				</lg>
				<lg n="16">
					<l n="61" num="16.1"><w n="61.1">Va</w>, <w n="61.2">je</w> <w n="61.3">connais</w> <w n="61.4">l</w>’<w n="61.5">adresse</w> <w n="61.6">et</w> <w n="61.7">les</w> <w n="61.8">subtilités</w></l>
					<l n="62" num="16.2"><w n="62.1">Du</w> <w n="62.2">sophisme</w>, <w n="62.3">aussi</w> <w n="62.4">bien</w> <w n="62.5">que</w> <w n="62.6">tu</w> <w n="62.7">peux</w> <w n="62.8">les</w> <w n="62.9">connaître</w>.</l>
					<l n="63" num="16.3"><w n="63.1">Je</w> <w n="63.2">sais</w> <w n="63.3">que</w> <w n="63.4">son</w> <w n="63.5">œil</w> <w n="63.6">brûle</w> <w n="63.7">et</w> <w n="63.8">que</w> <w n="63.9">sa</w> <w n="63.10">voix</w> <w n="63.11">pénètre</w>,</l>
					<l n="64" num="16.4"><w n="64.1">Et</w> <w n="64.2">quel</w> <w n="64.3">sang</w> <w n="64.4">bondira</w> <w n="64.5">dans</w> <w n="64.6">vos</w> <w n="64.7">cœurs</w> <w n="64.8">révoltés</w>.</l>
				</lg>
				<lg n="17">
					<l n="65" num="17.1"><w n="65.1">Je</w> <w n="65.2">sais</w> <w n="65.3">qu</w>’<w n="65.4">elle</w> <w n="65.5">succombe</w> <w n="65.6">et</w> <w n="65.7">qu</w>’<w n="65.8">elle</w> <w n="65.9">est</w> <w n="65.10">sans</w> <w n="65.11">défense</w>,</l>
					<l n="66" num="17.2"><w n="66.1">Qu</w>’<w n="66.2">elle</w> <w n="66.3">meurtrit</w> <w n="66.4">son</w> <w n="66.5">sein</w> <w n="66.6">devant</w> <w n="66.7">le</w> <w n="66.8">crucifix</w>,</l>
					<l n="67" num="17.3"><w n="67.1">Qu</w>’<w n="67.2">elle</w> <w n="67.3">t</w>’<w n="67.4">adorerait</w> <w n="67.5">comme</w> <w n="67.6">un</w> <w n="67.7">dieu</w>, <w n="67.8">comme</w> <w n="67.9">un</w> <w n="67.10">fils</w> ;</l>
					<l n="68" num="17.4"><w n="68.1">Je</w> <w n="68.2">sais</w> <w n="68.3">que</w> <w n="68.4">ta</w> <w n="68.5">victoire</w> <w n="68.6">est</w> <w n="68.7">certaine</w> <w n="68.8">d</w>’<w n="68.9">avance</w>.</l>
				</lg>
				<lg n="18">
					<l n="69" num="18.1"><w n="69.1">Oui</w>, <w n="69.2">pour</w> <w n="69.3">toi</w> <w n="69.4">je</w> <w n="69.5">suis</w> <w n="69.6">sûr</w> <w n="69.7">qu</w>’<w n="69.8">elle</w> <w n="69.9">sacrifierait</w></l>
					<l n="70" num="18.2"><w n="70.1">Son</w> <w n="70.2">unique</w> <w n="70.3">trésor</w>, <w n="70.4">l</w>’<w n="70.5">honneur</w> <w n="70.6">pur</w> <w n="70.7">et</w> <w n="70.8">fidèle</w>,</l>
					<l n="71" num="18.3"><w n="71.1">Et</w> <w n="71.2">que</w> <w n="71.3">tu</w> <w n="71.4">voudrais</w> <w n="71.5">vivre</w> <w n="71.6">et</w> <w n="71.7">mourir</w> <w n="71.8">auprès</w> <w n="71.9">d</w>’<w n="71.10">elle</w>.</l>
					<l n="72" num="18.4">— <w n="72.1">C</w>’<w n="72.2">est</w> <w n="72.3">bien</w>. <w n="72.4">Mais</w> <w n="72.5">je</w> <w n="72.6">suis</w> <w n="72.7">sûr</w> <w n="72.8">aussi</w> <w n="72.9">qu</w>’<w n="72.10">elle</w> <w n="72.11">en</w> <w n="72.12">mourrait</w>.</l>
				</lg>
			</div></body></text></TEI>