<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Poésies de Th. Gautier qui ne figureront pas dans ses œuvres</title>
				<title type="sub">Poèmes absents des précédentes éditions</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>504 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">GAU_9</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies de Th. Gautier qui ne figureront pas dans ses œuvres</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">https://fr.wikisource.org/wiki/Po%C3%A9sies_de_Th._Gautier_qui_ne_figureront_pas_dans_ses_%C5%93uvres</idno>
						<p>Exporté de Wikisource le 26 juillet 2022</p>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies de Th. Gautier qui ne figureront pas dans ses œuvres</title>
								<author>Théophile Gautier</author>
								<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k72036w</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Impr. particulière</publisher>
									<date when="1873">1873</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1873">1873</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>

			<samplingDecl>
				<p>La présente édition électronique est issue d’une exportation à partir de Wikisource.org (Exporté de Wikisource le 26 juillet 2022).</p>
				<p>La partie "Autobiographie" n’est pas incluse dans la présente édition électronique.</p>
				<p>Cette édition ne contient que les poèmes de Th. Gautier non inclus dans les précédentes éditions.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU368">
				<head type="main">LA MORT, L’APPARITION ET LES OBSÈQUES <lb></lb>DU CAPITAINE MORPION</head>
				<div type="section" n="1">
					<head type="number">I</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Cent</w> <w n="1.2">mille</w> <w n="1.3">poux</w> <w n="1.4">de</w> <w n="1.5">forte</w> <w n="1.6">taille</w></l>
						<l n="2" num="1.2"><w n="2.1">Sur</w> <w n="2.2">la</w> <w n="2.3">motte</w> <w n="2.4">ont</w> <w n="2.5">livré</w> <w n="2.6">bataille</w></l>
						<l n="3" num="1.3"><w n="3.1">À</w> <w n="3.2">nombre</w> <w n="3.3">égal</w> <w n="3.4">de</w> <w n="3.5">morpions</w></l>
						<l n="4" num="1.4"><w n="4.1">Portant</w> <w n="4.2">écus</w> <w n="4.3">et</w> <w n="4.4">morions</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Transpercé</w>, <w n="5.2">malgré</w> <w n="5.3">sa</w> <w n="5.4">cuirasse</w></l>
						<l n="6" num="2.2"><w n="6.1">Faite</w> <w n="6.2">d</w>’<w n="6.3">une</w> <w n="6.4">écaille</w> <w n="6.5">de</w> <w n="6.6">crasse</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Le</w> <w n="7.2">capitaine</w> <w n="7.3">Morpion</w></l>
						<l n="8" num="2.4"><w n="8.1">Est</w> <w n="8.2">tombé</w> <w n="8.3">mort</w> <w n="8.4">au</w> <w n="8.5">bord</w> <w n="8.6">du</w> <w n="8.7">con</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">En</w> <w n="9.2">vain</w> <w n="9.3">la</w> <w n="9.4">foule</w> <w n="9.5">désolée</w>,</l>
						<l n="10" num="3.2"><w n="10.1">Pour</w> <w n="10.2">lui</w> <w n="10.3">dresser</w> <w n="10.4">un</w> <w n="10.5">mausolée</w>,</l>
						<l n="11" num="3.3"><w n="11.1">Pendant</w> <w n="11.2">huit</w> <w n="11.3">jours</w> <w n="11.4">chercha</w> <w n="11.5">son</w> <w n="11.6">corps</w>…</l>
						<l n="12" num="3.4"><w n="12.1">L</w>’<w n="12.2">abîme</w> <w n="12.3">ne</w> <w n="12.4">rend</w> <w n="12.5">pas</w> <w n="12.6">les</w> <w n="12.7">morts</w> !</l>
					</lg>
					<head type="number">II</head>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Un</w> <w n="13.2">soir</w>, <w n="13.3">au</w> <w n="13.4">bord</w> <w n="13.5">de</w> <w n="13.6">la</w> <w n="13.7">ravine</w>,</l>
						<l n="14" num="4.2"><w n="14.1">Ruisselant</w> <w n="14.2">de</w> <w n="14.3">foutre</w> <w n="14.4">et</w> <w n="14.5">d</w>’<w n="14.6">urine</w>,</l>
						<l n="15" num="4.3"><w n="15.1">On</w> <w n="15.2">vit</w> <w n="15.3">un</w> <w n="15.4">fantôme</w> <w n="15.5">tout</w> <w n="15.6">nu</w>,</l>
						<l n="16" num="4.4"><w n="16.1">À</w> <w n="16.2">cheval</w> <w n="16.3">sur</w> <w n="16.4">un</w> <w n="16.5">poil</w> <w n="16.6">de</w> <w n="16.7">cu</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">C</w>’<w n="17.2">était</w> <w n="17.3">l</w>’<w n="17.4">ombre</w> <w n="17.5">du</w> <w n="17.6">capitaine</w>,</l>
						<l n="18" num="5.2"><w n="18.1">Dont</w> <w n="18.2">la</w> <w n="18.3">carcasse</w> <w n="18.4">de</w> <w n="18.5">vers</w> <w n="18.6">pleine</w>,</l>
						<l n="19" num="5.3"><w n="19.1">Par</w> <w n="19.2">défaut</w> <w n="19.3">d</w>’<w n="19.4">inhumation</w></l>
						<l n="20" num="5.4"><w n="20.1">Sentait</w> <w n="20.2">le</w> <w n="20.3">marolle</w> <w n="20.4">et</w> <w n="20.5">l</w>’<w n="20.6">arpion</w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Devant</w> <w n="21.2">cette</w> <w n="21.3">ombre</w> <w n="21.4">qui</w> <w n="21.5">murmure</w></l>
						<l n="22" num="6.2"><w n="22.1">Triste</w>, <w n="22.2">faute</w> <w n="22.3">de</w> <w n="22.4">sépulture</w>,</l>
						<l n="23" num="6.3"><w n="23.1">Tous</w> <w n="23.2">les</w> <w n="23.3">morpions</w> <w n="23.4">font</w> <w n="23.5">serment</w></l>
						<l n="24" num="6.4"><w n="24.1">De</w> <w n="24.2">lui</w> <w n="24.3">dresser</w> <w n="24.4">un</w> <w n="24.5">monument</w>.</l>
					</lg>
					<head type="number">III</head>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">On</w> <w n="25.2">l</w>’<w n="25.3">a</w> <w n="25.4">recouvert</w> <w n="25.5">d</w>’<w n="25.6">une</w> <w n="25.7">toile</w></l>
						<l n="26" num="7.2"><w n="26.1">Où</w> <w n="26.2">de</w> <w n="26.3">l</w>’<w n="26.4">honneur</w> <w n="26.5">brille</w> <w n="26.6">l</w>’<w n="26.7">étoile</w>,</l>
						<l n="27" num="7.3"><w n="27.1">Comme</w> <w n="27.2">au</w> <w n="27.3">convoi</w> <w n="27.4">d</w>’<w n="27.5">un</w> <w n="27.6">général</w></l>
						<l n="28" num="7.4"><w n="28.1">Ou</w> <w n="28.2">d</w>’<w n="28.3">un</w> <w n="28.4">garde</w> <w n="28.5">national</w>.</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1"><w n="29.1">Son</w> <w n="29.2">cheval</w> <w n="29.3">à</w> <w n="29.4">pied</w> <w n="29.5">l</w>’<w n="29.6">accompagne</w></l>
						<l n="30" num="8.2"><w n="30.1">Quatre</w> <w n="30.2">morpions</w> <w n="30.3">grands</w> <w n="30.4">d</w>’<w n="30.5">Espagne</w>,</l>
						<l n="31" num="8.3"><w n="31.1">La</w> <w n="31.2">larme</w> <w n="31.3">à</w> <w n="31.4">l</w>’<w n="31.5">œil</w>, <w n="31.6">l</w>’<w n="31.7">écharpe</w> <w n="31.8">au</w> <w n="31.9">bras</w>,</l>
						<l n="32" num="8.4"><w n="32.1">Tiennent</w> <w n="32.2">les</w> <w n="32.3">quatre</w> <w n="32.4">coins</w> <w n="32.5">du</w> <w n="32.6">drap</w>.</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1"><w n="33.1">On</w> <w n="33.2">lui</w> <w n="33.3">bâtit</w> <w n="33.4">un</w> <w n="33.5">cénotaphe</w></l>
						<l n="34" num="9.2"><w n="34.1">Où</w> <w n="34.2">l</w>’<w n="34.3">on</w> <w n="34.4">grava</w> <w n="34.5">cette</w> <w n="34.6">épitaphe</w> :</l>
						<l n="35" num="9.3">« <w n="35.1">Ci</w>-<w n="35.2">gît</w> <w n="35.3">un</w> <w n="35.4">morpion</w> <w n="35.5">de</w> <w n="35.6">cœur</w>,</l>
						<l n="36" num="9.4"><w n="36.1">Mort</w> <w n="36.2">vaillamment</w> <w n="36.3">au</w> <w n="36.4">champ</w> <w n="36.5">d</w>’<w n="36.6">honneur</w>. »</l>
					</lg>
				</div>
				<p>
					Cette poésie héroïque se chante sur la musique 
					d’une marche funèbre, composée par M. Reyer pour 
					le convoi du maréchal Gérard.
				</p>
			</div></body></text></TEI>