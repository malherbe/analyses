<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Poésies de Th. Gautier qui ne figureront pas dans ses œuvres</title>
				<title type="sub">Poèmes absents des précédentes éditions</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>504 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">GAU_9</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies de Th. Gautier qui ne figureront pas dans ses œuvres</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">https://fr.wikisource.org/wiki/Po%C3%A9sies_de_Th._Gautier_qui_ne_figureront_pas_dans_ses_%C5%93uvres</idno>
						<p>Exporté de Wikisource le 26 juillet 2022</p>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies de Th. Gautier qui ne figureront pas dans ses œuvres</title>
								<author>Théophile Gautier</author>
								<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k72036w</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Impr. particulière</publisher>
									<date when="1873">1873</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1873">1873</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>

			<samplingDecl>
				<p>La présente édition électronique est issue d’une exportation à partir de Wikisource.org (Exporté de Wikisource le 26 juillet 2022).</p>
				<p>La partie "Autobiographie" n’est pas incluse dans la présente édition électronique.</p>
				<p>Cette édition ne contient que les poèmes de Th. Gautier non inclus dans les précédentes éditions.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU375">
				<head type="main">XV DÉCEMBRE MDCCCXL</head>
				<opener>
					<dateline>
						<date when="1869">29 avril 1869</date>.
					</dateline>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Quand</w> <w n="1.2">sous</w> <w n="1.3">l</w>’<w n="1.4">arc</w> <w n="1.5">triomphal</w> <w n="1.6">où</w> <w n="1.7">s</w>’<w n="1.8">inscrivent</w> <w n="1.9">nos</w> <w n="1.10">gloires</w></l>
					<l n="2" num="1.2"><w n="2.1">Passait</w> <w n="2.2">le</w> <w n="2.3">sombre</w> <w n="2.4">char</w> <w n="2.5">couronné</w> <w n="2.6">de</w> <w n="2.7">victoires</w></l>
					<l n="3" num="1.3"><space unit="char" quantity="12"></space><w n="3.1">Aux</w> <w n="3.2">longues</w> <w n="3.3">ailes</w> <w n="3.4">d</w>’<w n="3.5">or</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Et</w> <w n="4.2">qu</w>’<w n="4.3">enfin</w> <w n="4.4">Sainte</w>-<w n="4.5">Hélène</w>, <w n="4.6">après</w> <w n="4.7">tant</w> <w n="4.8">de</w> <w n="4.9">souffrance</w>,</l>
					<l n="5" num="1.5"><w n="5.1">Délivrait</w> <w n="5.2">la</w> <w n="5.3">grande</w> <w n="5.4">ombre</w> <w n="5.5">et</w> <w n="5.6">rendait</w> <w n="5.7">à</w> <w n="5.8">la</w> <w n="5.9">France</w></l>
					<l n="6" num="1.6"><space unit="char" quantity="12"></space><w n="6.1">Son</w> <w n="6.2">funèbre</w> <w n="6.3">trésor</w>,</l>
				</lg>
				<lg n="2">
					<l n="7" num="2.1"><w n="7.1">Un</w> <w n="7.2">rêveur</w>, <w n="7.3">un</w> <w n="7.4">captif</w>, <w n="7.5">derrière</w> <w n="7.6">ses</w> <w n="7.7">murailles</w>,</l>
					<l n="8" num="2.2"><w n="8.1">Triste</w> <w n="8.2">de</w> <w n="8.3">ne</w> <w n="8.4">pouvoir</w>, <w n="8.5">aux</w> <w n="8.6">saintes</w> <w n="8.7">funérailles</w>,</l>
					<l n="9" num="2.3"><space unit="char" quantity="12"></space><w n="9.1">Assister</w>, <w n="9.2">l</w>’<w n="9.3">œil</w> <w n="9.4">en</w> <w n="9.5">pleurs</w>,</l>
					<l n="10" num="2.4"><w n="10.1">Dans</w> <w n="10.2">l</w>’<w n="10.3">étroite</w> <w n="10.4">prison</w>, <w n="10.5">sans</w> <w n="10.6">échos</w> <w n="10.7">et</w> <w n="10.8">muette</w>,</l>
					<l n="11" num="2.5"><w n="11.1">Mêlant</w> <w n="11.2">sa</w> <w n="11.3">note</w> <w n="11.4">émue</w> <w n="11.5">à</w> <w n="11.6">l</w>’<w n="11.7">ode</w> <w n="11.8">du</w> <w n="11.9">poète</w>,</l>
					<l n="12" num="2.6"><space unit="char" quantity="12"></space><w n="12.1">Épanchait</w> <w n="12.2">ses</w> <w n="12.3">douleurs</w> :</l>
				</lg>
				<lg n="3">
					<l n="13" num="3.1">« <w n="13.1">Sire</w>, <w n="13.2">vous</w> <w n="13.3">revenez</w> <w n="13.4">dans</w> <w n="13.5">votre</w> <w n="13.6">capitale</w>,</l>
					<l n="14" num="3.2"><w n="14.1">Et</w> <w n="14.2">moi</w>, <w n="14.3">qu</w>’<w n="14.4">en</w> <w n="14.5">un</w> <w n="14.6">cachot</w> <w n="14.7">tient</w> <w n="14.8">une</w> <w n="14.9">loi</w> <w n="14.10">fatale</w>,</l>
					<l n="15" num="3.3"><space unit="char" quantity="12"></space><w n="15.1">Exilé</w> <w n="15.2">de</w> <w n="15.3">Paris</w>,</l>
					<l n="16" num="3.4"><w n="16.1">J</w>’<w n="16.2">apercevrai</w> <w n="16.3">de</w> <w n="16.4">loin</w>, <w n="16.5">comme</w> <w n="16.6">sur</w> <w n="16.7">une</w> <w n="16.8">cime</w>,</l>
					<l n="17" num="3.5"><w n="17.1">Le</w> <w n="17.2">soleil</w> <w n="17.3">descendant</w> <w n="17.4">sur</w> <w n="17.5">le</w> <w n="17.6">cercueil</w> <w n="17.7">sublime</w></l>
					<l n="18" num="3.6"><space unit="char" quantity="12"></space><w n="18.1">Dans</w> <w n="18.2">la</w> <w n="18.3">foule</w> <w n="18.4">aux</w> <w n="18.5">longs</w> <w n="18.6">cris</w>.</l>
				</lg>
				<lg n="4">
					<l n="19" num="4.1"><w n="19.1">Oh</w> ! <w n="19.2">non</w> ! <w n="19.3">n</w>’<w n="19.4">en</w> <w n="19.5">veuillez</w> <w n="19.6">pas</w>, <w n="19.7">sire</w>, <w n="19.8">à</w> <w n="19.9">votre</w> <w n="19.10">famille</w></l>
					<l n="20" num="4.2"><w n="20.1">De</w> <w n="20.2">n</w>’<w n="20.3">avoir</w> <w n="20.4">pas</w> <w n="20.5">formé</w>, <w n="20.6">sous</w> <w n="20.7">le</w> <w n="20.8">rayon</w> <w n="20.9">qui</w> <w n="20.10">brille</w>,</l>
					<l n="21" num="4.3"><space unit="char" quantity="12"></space><w n="21.1">Un</w> <w n="21.2">groupe</w> <w n="21.3">filial</w>,</l>
					<l n="22" num="4.4"><w n="22.1">Pour</w> <w n="22.2">recevoir</w>, <w n="22.3">au</w> <w n="22.4">seuil</w> <w n="22.5">de</w> <w n="22.6">son</w> <w n="22.7">apothéose</w>,</l>
					<l n="23" num="4.5"><w n="23.1">Comme</w> <w n="23.2">Hercule</w> <w n="23.3">ayant</w> <w n="23.4">fait</w> <w n="23.5">sa</w> <w n="23.6">tâche</w> <w n="23.7">grandiose</w>,</l>
					<l n="24" num="4.6"><space unit="char" quantity="12"></space><w n="24.1">L</w>’<w n="24.2">ancêtre</w> <w n="24.3">impérial</w> !</l>
				</lg>
				<lg n="5">
					<l n="25" num="5.1"><w n="25.1">Vos</w> <w n="25.2">malheurs</w> <w n="25.3">sont</w> <w n="25.4">finis</w> ; <w n="25.5">toujours</w> <w n="25.6">durent</w> <w n="25.7">les</w> <w n="25.8">nôtres</w>.</l>
					<l n="26" num="5.2"><w n="26.1">Vous</w> <w n="26.2">êtes</w> <w n="26.3">mort</w> <w n="26.4">là</w>-<w n="26.5">bas</w>, <w n="26.6">enchaîné</w>, <w n="26.7">loin</w> <w n="26.8">des</w> <w n="26.9">vôtres</w>,</l>
					<l n="27" num="5.3"><space unit="char" quantity="12"></space><w n="27.1">Titan</w> <w n="27.2">sur</w> <w n="27.3">un</w> <w n="27.4">écueil</w> ;</l>
					<l n="28" num="5.4"><w n="28.1">Pas</w> <w n="28.2">de</w> <w n="28.3">fils</w> <w n="28.4">pour</w> <w n="28.5">fermer</w> <w n="28.6">vos</w> <w n="28.7">yeux</w> <w n="28.8">que</w> <w n="28.9">l</w>’<w n="28.10">ombre</w> <w n="28.11">inonde</w> ;</l>
					<l n="29" num="5.5"><w n="29.1">Même</w> <w n="29.2">ici</w>, <w n="29.3">nul</w> <w n="29.4">parent</w>, — <w n="29.5">oh</w> ! <w n="29.6">misère</w> <w n="29.7">profonde</w> ! —</l>
					<l n="30" num="5.6"><space unit="char" quantity="12"></space><w n="30.1">Conduisant</w> <w n="30.2">votre</w> <w n="30.3">deuil</w> !</l>
				</lg>
				<lg n="6">
					<l n="31" num="6.1"><w n="31.1">Montholon</w>, <w n="31.2">le</w> <w n="31.3">plus</w> <w n="31.4">cher</w> <w n="31.5">comme</w> <w n="31.6">le</w> <w n="31.7">plus</w> <w n="31.8">fidèle</w>,</l>
					<l n="32" num="6.2"><w n="32.1">Jusqu</w>’<w n="32.2">au</w> <w n="32.3">bout</w>, <w n="32.4">du</w> <w n="32.5">vautour</w> <w n="32.6">affrontant</w> <w n="32.7">les</w> <w n="32.8">coups</w> <w n="32.9">d</w>’<w n="32.10">aile</w>,</l>
					<l n="33" num="6.3"><space unit="char" quantity="12"></space><w n="33.1">Vous</w> <w n="33.2">a</w> <w n="33.3">gardé</w> <w n="33.4">sa</w> <w n="33.5">foi</w> ;</l>
					<l n="34" num="6.4"><w n="34.1">Près</w> <w n="34.2">du</w> <w n="34.3">dieu</w> <w n="34.4">foudroyé</w>, <w n="34.5">qu</w>’<w n="34.6">un</w> <w n="34.7">vaste</w> <w n="34.8">ennui</w> <w n="34.9">dévore</w>,</l>
					<l n="35" num="6.5"><w n="35.1">Il</w> <w n="35.2">se</w> <w n="35.3">tenait</w> <w n="35.4">debout</w>, <w n="35.5">et</w> <w n="35.6">même</w> <w n="35.7">il</w> <w n="35.8">est</w> <w n="35.9">encore</w></l>
					<l n="36" num="6.6"><space unit="char" quantity="12"></space><w n="36.1">En</w> <w n="36.2">prison</w> <w n="36.3">avec</w> <w n="36.4">moi</w>.</l>
				</lg>
				<lg n="7">
					<l n="37" num="7.1"><w n="37.1">Un</w> <w n="37.2">navire</w>, <w n="37.3">conduit</w> <w n="37.4">par</w> <w n="37.5">un</w> <w n="37.6">noble</w> <w n="37.7">jeune</w> <w n="37.8">homme</w>,</l>
					<l n="38" num="7.2"><w n="38.1">Sous</w> <w n="38.2">l</w>’<w n="38.3">arbre</w> <w n="38.4">où</w> <w n="38.5">vous</w> <w n="38.6">dormiez</w>, <w n="38.7">Sire</w>, <w n="38.8">votre</w> <w n="38.9">long</w> <w n="38.10">somme</w>,</l>
					<l n="39" num="7.3"><space unit="char" quantity="12"></space><w n="39.1">Captif</w> <w n="39.2">dans</w> <w n="39.3">le</w> <w n="39.4">trépas</w>,</l>
					<l n="40" num="7.4"><w n="40.1">Est</w> <w n="40.2">allé</w> <w n="40.3">vous</w> <w n="40.4">chercher</w> <w n="40.5">avec</w> <w n="40.6">une</w> <w n="40.7">escadrille</w> ;</l>
					<l n="41" num="7.5"><w n="41.1">Mais</w> <w n="41.2">votre</w> <w n="41.3">œil</w> <w n="41.4">sur</w> <w n="41.5">le</w> <w n="41.6">pont</w> <w n="41.7">cherchait</w> <w n="41.8">votre</w> <w n="41.9">famille</w></l>
					<l n="42" num="7.6"><space unit="char" quantity="12"></space><w n="42.1">Qui</w> <w n="42.2">ne</w> <w n="42.3">s</w>’<w n="42.4">y</w> <w n="42.5">trouvait</w> <w n="42.6">pas</w>.</l>
				</lg>
				<lg n="8">
					<l n="43" num="8.1"><w n="43.1">Quand</w> <w n="43.2">la</w> <w n="43.3">nef</w> <w n="43.4">aborda</w>, <w n="43.5">France</w>, <w n="43.6">ton</w> <w n="43.7">sol</w> <w n="43.8">antique</w>,</l>
					<l n="44" num="8.2"><w n="44.1">Votre</w> <w n="44.2">âme</w> <w n="44.3">réveillée</w> <w n="44.4">à</w> <w n="44.5">ce</w> <w n="44.6">choc</w> <w n="44.7">électrique</w>,</l>
					<l n="45" num="8.3"><space unit="char" quantity="12"></space><w n="45.1">Au</w> <w n="45.2">bruit</w> <w n="45.3">des</w> <w n="45.4">voix</w>, <w n="45.5">des</w> <w n="45.6">pas</w>,</l>
					<l n="46" num="8.4"><w n="46.1">De</w> <w n="46.2">sa</w> <w n="46.3">prunelle</w> <w n="46.4">d</w>’<w n="46.5">ombre</w> <w n="46.6">entrevit</w> <w n="46.7">dans</w> <w n="46.8">l</w>’<w n="46.9">aurore</w>,</l>
					<l n="47" num="8.5"><w n="47.1">Palpiter</w> <w n="47.2">vaguement</w> <w n="47.3">un</w> <w n="47.4">drapeau</w> <w n="47.5">tricolore</w></l>
					<l n="48" num="8.6"><space unit="char" quantity="12"></space><w n="48.1">Où</w> <w n="48.2">l</w>’<w n="48.3">aigle</w> <w n="48.4">n</w>’<w n="48.5">était</w> <w n="48.6">pas</w>.</l>
				</lg>
				<lg n="9">
					<l n="49" num="9.1"><w n="49.1">Comme</w> <w n="49.2">autrefois</w> <w n="49.3">le</w> <w n="49.4">peuple</w> <w n="49.5">autour</w> <w n="49.6">de</w> <w n="49.7">vous</w> <w n="49.8">s</w>’<w n="49.9">empresse</w> ;</l>
					<l n="50" num="9.2"><w n="50.1">Cris</w> <w n="50.2">d</w>’<w n="50.3">amour</w> <w n="50.4">furieux</w>, <w n="50.5">délirante</w> <w n="50.6">tendresse</w>,</l>
					<l n="51" num="9.3"><space unit="char" quantity="12"></space><w n="51.1">À</w> <w n="51.2">genoux</w>, <w n="51.3">chapeau</w> <w n="51.4">bas</w> !</l>
					<l n="52" num="9.4"><w n="52.1">Dans</w> <w n="52.2">l</w>’<w n="52.3">acclamation</w>, <w n="52.4">les</w> <w n="52.5">prudents</w> <w n="52.6">et</w> <w n="52.7">les</w> <w n="52.8">sages</w></l>
					<l n="53" num="9.5"><w n="53.1">Murmurent</w>, <w n="53.2">au</w> <w n="53.3">César</w> <w n="53.4">faisant</w> <w n="53.5">sa</w> <w n="53.6">part</w> <w n="53.7">d</w>’<w n="53.8">hommages</w> :</l>
					<l n="54" num="9.6"><space unit="char" quantity="12"></space>« <w n="54.1">Dieu</w> ! <w n="54.2">ne</w> <w n="54.3">l</w>’<w n="54.4">éveillez</w> <w n="54.5">pas</w> ! »</l>
				</lg>
				<lg n="10">
					<l n="55" num="10.1"><w n="55.1">Vous</w> <w n="55.2">les</w> <w n="55.3">avez</w> <w n="55.4">revus</w> — <w n="55.5">peuple</w> <w n="55.6">élu</w> <w n="55.7">de</w> <w n="55.8">votre</w> <w n="55.9">âme</w> —</l>
					<l n="56" num="10.2"><w n="56.1">Ces</w> <w n="56.2">Français</w> <w n="56.3">tant</w> <w n="56.4">aimés</w> <w n="56.5">que</w> <w n="56.6">votre</w> <w n="56.7">nom</w> <w n="56.8">enflamme</w>,</l>
					<l n="57" num="10.3"><space unit="char" quantity="12"></space><w n="57.1">Héros</w> <w n="57.2">des</w> <w n="57.3">grands</w> <w n="57.4">combats</w> ;</l>
					<l n="58" num="10.4"><w n="58.1">Mais</w> <w n="58.2">sur</w> <w n="58.3">ton</w> <w n="58.4">sol</w> <w n="58.5">sacré</w>, <w n="58.6">patrie</w> <w n="58.7">autrefois</w> <w n="58.8">crainte</w>,</l>
					<l n="59" num="10.5"><w n="59.1">Du</w> <w n="59.2">pas</w> <w n="59.3">de</w> <w n="59.4">l</w>’<w n="59.5">étranger</w> <w n="59.6">on</w> <w n="59.7">distingue</w> <w n="59.8">une</w> <w n="59.9">empreinte</w></l>
					<l n="60" num="10.6"><space unit="char" quantity="12"></space><w n="60.1">Qui</w> <w n="60.2">ne</w> <w n="60.3">s</w>’<w n="60.4">efface</w> <w n="60.5">pas</w>.</l>
				</lg>
				<lg n="11">
					<l n="61" num="11.1"><w n="61.1">Voyez</w> <w n="61.2">la</w> <w n="61.3">jeune</w> <w n="61.4">armée</w>, <w n="61.5">où</w> <w n="61.6">les</w> <w n="61.7">fils</w> <w n="61.8">de</w> <w n="61.9">nos</w> <w n="61.10">braves</w>,</l>
					<l n="62" num="11.2"><w n="62.1">Avides</w> <w n="62.2">d</w>’<w n="62.3">action</w>, <w n="62.4">impatients</w> <w n="62.5">d</w>’<w n="62.6">entraves</w>,</l>
					<l n="63" num="11.3"><space unit="char" quantity="12"></space><w n="63.1">Voudraient</w> <w n="63.2">presser</w> <w n="63.3">le</w> <w n="63.4">pas</w> ;</l>
					<l n="64" num="11.4"><w n="64.1">Votre</w> <w n="64.2">nom</w> <w n="64.3">les</w> <w n="64.4">émeut</w>, <w n="64.5">car</w> <w n="64.6">vous</w> <w n="64.7">êtes</w> <w n="64.8">la</w> <w n="64.9">gloire</w> ;</l>
					<l n="65" num="11.5"><w n="65.1">Mais</w> <w n="65.2">on</w> <w n="65.3">leur</w> <w n="65.4">dit</w> : « <w n="65.5">Laissez</w> <w n="65.6">reposer</w> <w n="65.7">la</w> <w n="65.8">Victoire</w> ;</l>
					<l n="66" num="11.6"><space unit="char" quantity="12"></space><w n="66.1">Assez</w>. <w n="66.2">Croisez</w> <w n="66.3">les</w> <w n="66.4">bras</w>. »</l>
				</lg>
				<lg n="12">
					<l n="67" num="12.1"><w n="67.1">Sur</w> <w n="67.2">le</w> <w n="67.3">pays</w>, <w n="67.4">le</w> <w n="67.5">peuple</w>, <w n="67.6">étoffe</w> <w n="67.7">rude</w> <w n="67.8">et</w> <w n="67.9">forte</w>,</l>
					<l n="68" num="12.2"><w n="68.1">S</w>’<w n="68.2">étend</w> <w n="68.3">comme</w> <w n="68.4">un</w> <w n="68.5">manteau</w> <w n="68.6">qui</w> <w n="68.7">vaillamment</w> <w n="68.8">supporte</w></l>
					<l n="69" num="12.3"><space unit="char" quantity="12"></space><w n="69.1">L</w>’<w n="69.2">orage</w> <w n="69.3">et</w> <w n="69.4">les</w> <w n="69.5">frimas</w> ;</l>
					<l n="70" num="12.4"><w n="70.1">Mais</w> <w n="70.2">ces</w> <w n="70.3">grands</w> <w n="70.4">si</w> <w n="70.5">petits</w>, <w n="70.6">chamarrés</w> <w n="70.7">de</w> <w n="70.8">dorures</w>,</l>
					<l n="71" num="12.5"><w n="71.1">Qui</w> <w n="71.2">cachent</w> <w n="71.3">leur</w> <w n="71.4">néant</w> <w n="71.5">sous</w> <w n="71.6">de</w> <w n="71.7">riches</w> <w n="71.8">parures</w>,</l>
					<l n="72" num="12.6"><space unit="char" quantity="12"></space><w n="72.1">Ne</w> <w n="72.2">les</w> <w n="72.3">regrettez</w> <w n="72.4">pas</w>.</l>
				</lg>
				<lg n="13">
					<l n="73" num="13.1"><w n="73.1">Comme</w> <w n="73.2">ils</w> <w n="73.3">ont</w> <w n="73.4">renié</w>, <w n="73.5">troupe</w> <w n="73.6">au</w> <w n="73.7">parjure</w> <w n="73.8">agile</w>,</l>
					<l n="74" num="13.2"><w n="74.1">Votre</w> <w n="74.2">nom</w>, <w n="74.3">votre</w> <w n="74.4">sang</w>, <w n="74.5">vos</w> <w n="74.6">lois</w>, <w n="74.7">votre</w> <w n="74.8">évangile</w>,</l>
					<l n="75" num="13.3"><space unit="char" quantity="12"></space><w n="75.1">Pour</w> <w n="75.2">vous</w> <w n="75.3">suivre</w> <w n="75.4">trop</w> <w n="75.5">las</w> !</l>
					<l n="76" num="13.4"><w n="76.1">Et</w> <w n="76.2">quand</w> <w n="76.3">j</w>’<w n="76.4">ai</w> <w n="76.5">devant</w> <w n="76.6">eux</w> <w n="76.7">parlé</w> <w n="76.8">de</w> <w n="76.9">votre</w> <w n="76.10">cause</w>,</l>
					<l n="77" num="13.5"><w n="77.1">Comme</w> <w n="77.2">ils</w> <w n="77.3">ont</w> <w n="77.4">dit</w>, <w n="77.5">tournés</w> <w n="77.6">déjà</w> <w n="77.7">vers</w> <w n="77.8">autre</w> <w n="77.9">chose</w> :</l>
					<l n="78" num="13.6"><space unit="char" quantity="12"></space>« <w n="78.1">Nous</w> <w n="78.2">ne</w> <w n="78.3">comprenons</w> <w n="78.4">pas</w>. »</l>
				</lg>
				<lg n="14">
					<l n="79" num="14.1"><w n="79.1">Laissez</w>-<w n="79.2">les</w> <w n="79.3">dire</w> <w n="79.4">et</w> <w n="79.5">faire</w>, <w n="79.6">et</w> <w n="79.7">sur</w> <w n="79.8">eux</w> <w n="79.9">soit</w> <w n="79.10">la</w> <w n="79.11">honte</w> !</l>
					<l n="80" num="14.2"><w n="80.1">Qu</w>’<w n="80.2">importe</w> <w n="80.3">pierre</w> <w n="80.4">ou</w> <w n="80.5">sable</w> <w n="80.6">au</w> <w n="80.7">char</w> <w n="80.8">qui</w> <w n="80.9">toujours</w> <w n="80.10">monte</w></l>
					<l n="81" num="14.3"><space unit="char" quantity="12"></space><w n="81.1">Et</w> <w n="81.2">les</w> <w n="81.3">broie</w> <w n="81.4">en</w> <w n="81.5">éclats</w> ?</l>
					<l n="82" num="14.4"><w n="82.1">En</w> <w n="82.2">vain</w> <w n="82.3">vous</w> <w n="82.4">nomment</w>-<w n="82.5">ils</w> « <w n="82.6">fugitif</w> <w n="82.7">météore</w>, »</l>
					<l n="83" num="14.5"><w n="83.1">Votre</w> <w n="83.2">gloire</w> <w n="83.3">est</w> <w n="83.4">à</w> <w n="83.5">nous</w>, <w n="83.6">elle</w> <w n="83.7">rayonne</w> <w n="83.8">encore</w> ;</l>
					<l n="84" num="14.6"><space unit="char" quantity="12"></space><w n="84.1">Ils</w> <w n="84.2">ne</w> <w n="84.3">la</w> <w n="84.4">prendront</w> <w n="84.5">pas</w>.</l>
				</lg>
				<lg n="15">
					<l n="85" num="15.1"><w n="85.1">Sire</w>, <w n="85.2">c</w>’<w n="85.3">est</w> <w n="85.4">un</w> <w n="85.5">grand</w> <w n="85.6">jour</w> <w n="85.7">que</w> <w n="85.8">le</w> <w n="85.9">quinze</w> <w n="85.10">décembre</w> !</l>
					<l n="86" num="15.2"><w n="86.1">Votre</w> <w n="86.2">voix</w>, <w n="86.3">est</w>-<w n="86.4">ce</w> <w n="86.5">un</w> <w n="86.6">rêve</w> ? <w n="86.7">a</w> <w n="86.8">parlé</w> <w n="86.9">dans</w> <w n="86.10">ma</w> <w n="86.11">chambre</w> :</l>
					<l n="87" num="15.3"><space unit="char" quantity="12"></space>« <w n="87.1">Toi</w> <w n="87.2">qui</w> <w n="87.3">souffres</w> <w n="87.4">pour</w> <w n="87.5">moi</w>,</l>
					<l n="88" num="15.4"><w n="88.1">Ami</w>, <w n="88.2">de</w> <w n="88.3">la</w> <w n="88.4">prison</w> <w n="88.5">le</w> <w n="88.6">lent</w> <w n="88.7">et</w> <w n="88.8">dur</w> <w n="88.9">martyre</w>,</l>
					<l n="89" num="15.5"><w n="89.1">Je</w> <w n="89.2">quitte</w> <w n="89.3">mon</w> <w n="89.4">triomphe</w> <w n="89.5">et</w> <w n="89.6">je</w> <w n="89.7">viens</w> <w n="89.8">pour</w> <w n="89.9">te</w> <w n="89.10">dire</w> :</l>
					<l n="90" num="15.6"><space unit="char" quantity="12"></space><w n="90.1">Je</w> <w n="90.2">suis</w> <w n="90.3">content</w> <w n="90.4">de</w> <w n="90.5">toi</w> ! »</l>
				</lg>
				<p>
					Tout le monde connaît la pièce qui a inspiré ces vers ; cependant, peut-être nous saura-t-on gré de la reproduire.<lb></lb>
					<lb></lb>
					Citadelle de Ham, le 15 décembre 1840<lb></lb>
					<lb></lb>
					« Sire, <lb></lb>
					« Vous revenez dans votre capitale, et le peuple en foule salue votre retour ; mais moi, du fond de mon cachot, je ne puis apercevoir qu’un rayon du soleil qui éclaire vos funérailles.
					« N’en veuillez pas à votre famille de ce qu’elle n’est pas là pour vous recevoir. Votre exil et vos malheurs ont cessé avec votre vie ; mais les nôtres durent toujours ! Vous êtes mort sur un rocher, loin de la patrie et des vôtres ; la main d’un fils n’a point fermé vos yeux. Aujourd’hui encore aucun parent ne conduira votre deuil.
					« Montholon, lui que vous aimiez le plus parmi vos dévoués compagnons, vous a rendu les soins d’un fils : il est resté fidèle à votre pensée, à vos dernières volontés ; il m’a rapporté vos dernières paroles ; il est en prison avec moi !
					« Un vaisseau français, conduit par un noble jeune homme, est allé réclamer vos cendres ; mais c’est en vain que vous cherchiez sur le pont quelques-uns des vôtres ; votre famille n’y était pas.
					« En abordant au sol français, un choc électrique s’est fait sentir ; vous vous êtes soulevé dans votre cercueil ; vos yeux un moment se sont rouverts : le drapeau tricolore flottait sur le rivage, mais votre aigle n’y était pas.
					« Le peuple se presse comme autrefois sur votre passage, il vous salue de ses acclamations comme si vous étiez vivant ; mais les grands du jour, tout en vous rendant hommage, diront tout bas : « Dieu ! ne l’éveillez pas ! »
					« Vous avez enfin revu ces Français que vous aimiez tant ; vous êtes revenu dans cette France que vous avez rendue si grande ; mais l’étranger y a laissé des traces que toutes les pompes de votre retour n’effaceront pas !
					« Voyez cette jeune armée ; ce sont les fils de vos braves ; ils vous vénèrent, car vous êtes la gloire ; mais on leur dit : « Croisez vos bras ! »
					« Sire, le peuple, c’est la bonne étoffe qui couvre notre beau pays ; mais ces hommes que vous avez faits si grands, et qui étaient si petits, ah ! Sire, ne les regrettez pas.
					« Ils ont renié votre évangile, votre idée, votre gloire, votre sang ; quand je leur ai parlé de votre cause, il nous ont dit : Nous ne comprenons pas.
					« Laissez-les dire, laissez-les faire ; qu’importent, au char qui monte, les grains de sable qui se jettent sous les roues ! Ils ont beau dire que vous êtes un météore qui ne laisse pas de trace ; ils ont beau nier votre gloire civile ; ils ne vous déshériteront pas !
					« Sire, le 15 décembre est un grand jour pour la France et pour moi. Du milieu de votre somptueux cortège, dédaignant certains hommages, vous avez un instant jeté vos regards sur ma sombre demeure, et vous souvenant des caresses que vous prodiguiez à mon enfance, vous m’avez dit : « <hi rend="ital">Tu souffres pour moi, je suis content de toi.</hi> »
					<lb></lb>
					Louis-Napoléon.
					<lb></lb>
					Ces rimes françaises sur une matière à mettre en vers latins, et la prose qui les suit, sont une réimpression textuelle de la brochure : <hi rend="ital">XV Décembre MDCCCXL</hi> ; imp. du Journal officiel, MDCCCLXIX, A. Wittersheim et Cie, in-4, 16 p., papier vergé, tirée à 44 exemplaires, offerts à Napoléon III, disparus pour la plupart dans l’incendie des Tuileries.
				</p>
			</div></body></text></TEI>