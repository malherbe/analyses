<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ESPAÑA</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1374 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_7</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Second</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://gutenberg.org/files/45886/45886-h/45886-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Second</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1890">1890</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1845">1845</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">ESPAÑA</head><div type="poem" key="GAU248">
					<head type="main">A LA BIDASSOA</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">A</w> <w n="1.2">la</w> <w n="1.3">Bidassoa</w>, <w n="1.4">près</w> <w n="1.5">d</w>’<w n="1.6">entrer</w> <w n="1.7">en</w> <w n="1.8">Espagne</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Je</w> <w n="2.2">descendis</w>, <w n="2.3">voulant</w> <w n="2.4">regarder</w> <w n="2.5">la</w> <w n="2.6">campagne</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Et</w> <w n="3.2">l</w>’<w n="3.3">île</w> <w n="3.4">des</w> <w n="3.5">Faisans</w>, <w n="3.6">et</w> <w n="3.7">l</w>’<w n="3.8">étrange</w> <w n="3.9">horizon</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Pendant</w> <w n="4.2">qu</w>’<w n="4.3">on</w> <w n="4.4">nous</w> <w n="4.5">timbrait</w> <w n="4.6">d</w>’<w n="4.7">un</w> <w n="4.8">nouvel</w> <w n="4.9">écusson</w>.</l>
						<l n="5" num="1.5"><w n="5.1">Et</w> <w n="5.2">je</w> <w n="5.3">vis</w>, <w n="5.4">en</w> <w n="5.5">errant</w> <w n="5.6">à</w> <w n="5.7">travers</w> <w n="5.8">le</w> <w n="5.9">village</w>,</l>
						<l n="6" num="1.6"><w n="6.1">Un</w> <w n="6.2">homme</w> <w n="6.3">qui</w> <w n="6.4">mettait</w> <w n="6.5">des</w> <w n="6.6">balles</w> <w n="6.7">hors</w> <w n="6.8">d</w>’<w n="6.9">usage</w>,</l>
						<l n="7" num="1.7"><w n="7.1">Avec</w> <w n="7.2">un</w> <w n="7.3">gros</w> <w n="7.4">marteau</w>, <w n="7.5">sur</w> <w n="7.6">un</w> <w n="7.7">quartier</w> <w n="7.8">de</w> <w n="7.9">grès</w>,</l>
						<l n="8" num="1.8"><w n="8.1">Pour</w> <w n="8.2">en</w> <w n="8.3">faire</w> <w n="8.4">du</w> <w n="8.5">plomb</w> <w n="8.6">et</w> <w n="8.7">le</w> <w n="8.8">revendre</w> <w n="8.9">après</w>.</l>
						<l n="9" num="1.9"><w n="9.1">Car</w> <w n="9.2">la</w> <w n="9.3">guerre</w> <w n="9.4">a</w> <w n="9.5">versé</w> <w n="9.6">sur</w> <w n="9.7">ces</w> <w n="9.8">terres</w> <w n="9.9">fatales</w></l>
						<l n="10" num="1.10"><w n="10.1">De</w> <w n="10.2">son</w> <w n="10.3">urne</w> <w n="10.4">d</w>’<w n="10.5">airain</w> <w n="10.6">une</w> <w n="10.7">grêle</w> <w n="10.8">de</w> <w n="10.9">balles</w>,</l>
						<l n="11" num="1.11"><w n="11.1">Une</w> <w n="11.2">grêle</w> <w n="11.3">de</w> <w n="11.4">mort</w> <w n="11.5">que</w> <w n="11.6">nul</w> <w n="11.7">soleil</w> <w n="11.8">ne</w> <w n="11.9">fond</w>.</l>
						<l n="12" num="1.12"><w n="12.1">Hélas</w> ! <w n="12.2">ce</w> <w n="12.3">que</w> <w n="12.4">Dieu</w> <w n="12.5">fait</w>, <w n="12.6">les</w> <w n="12.7">hommes</w> <w n="12.8">le</w> <w n="12.9">défont</w> !</l>
						<l n="13" num="1.13"><w n="13.1">Sur</w> <w n="13.2">un</w> <w n="13.3">sol</w> <w n="13.4">qui</w> <w n="13.5">n</w>’<w n="13.6">attend</w> <w n="13.7">qu</w>’<w n="13.8">une</w> <w n="13.9">bonne</w> <w n="13.10">semaille</w></l>
						<l n="14" num="1.14"><w n="14.1">De</w> <w n="14.2">leurs</w> <w n="14.3">sanglantes</w> <w n="14.4">mains</w> <w n="14.5">ils</w> <w n="14.6">sèment</w> <w n="14.7">la</w> <w n="14.8">mitraille</w> !</l>
						<l n="15" num="1.15"><w n="15.1">Aussi</w> <w n="15.2">les</w> <w n="15.3">laboureurs</w> <w n="15.4">vendent</w>, <w n="15.5">au</w> <w n="15.6">lieu</w> <w n="15.7">de</w> <w n="15.8">blé</w>,</l>
						<l n="16" num="1.16"><w n="16.1">Des</w> <w n="16.2">boulets</w> <w n="16.3">recueillis</w> <w n="16.4">dans</w> <w n="16.5">leur</w> <w n="16.6">champ</w> <w n="16.7">constellé</w>.</l>
						<l n="17" num="1.17"><w n="17.1">Mais</w> <w n="17.2">du</w> <w n="17.3">ciel</w> <w n="17.4">épuré</w> <w n="17.5">descend</w> <w n="17.6">la</w> <w n="17.7">Paix</w> <w n="17.8">sereine</w>,</l>
						<l n="18" num="1.18"><w n="18.1">Qui</w> <w n="18.2">répand</w> <w n="18.3">de</w> <w n="18.4">sa</w> <w n="18.5">corne</w> <w n="18.6">une</w> <w n="18.7">meilleure</w> <w n="18.8">graine</w>,</l>
						<l n="19" num="1.19"><w n="19.1">Fait</w> <w n="19.2">taire</w> <w n="19.3">les</w> <w n="19.4">canons</w> <w n="19.5">à</w> <w n="19.6">ses</w> <w n="19.7">pieds</w> <w n="19.8">accroupis</w>,</l>
						<l n="20" num="1.20"><w n="20.1">Et</w> <w n="20.2">presse</w> <w n="20.3">sur</w> <w n="20.4">son</w> <w n="20.5">cœur</w> <w n="20.6">une</w> <w n="20.7">gerbe</w> <w n="20.8">d</w>’<w n="20.9">épis</w>.</l>
					</lg>
					<closer>
						<dateline>
						<placeName>Behobie</placeName>,
							<date when="1840">184.</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>