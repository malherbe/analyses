<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ESPAÑA</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1374 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_7</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Second</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://gutenberg.org/files/45886/45886-h/45886-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Second</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1890">1890</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1845">1845</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">ESPAÑA</head><div type="poem" key="GAU256">
					<head type="main">A MADRID</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Dans</w> <w n="1.2">le</w> <w n="1.3">boudoir</w> <w n="1.4">ambré</w> <w n="1.5">d</w>’<w n="1.6">une</w> <w n="1.7">jeune</w> <w n="1.8">marquise</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Grande</w> <w n="2.2">d</w>’<w n="2.3">Espagne</w>, <w n="2.4">belle</w>, <w n="2.5">et</w> <w n="2.6">d</w>’<w n="2.7">une</w> <w n="2.8">grâce</w> <w n="2.9">exquise</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Au</w> <w n="3.2">milieu</w> <w n="3.3">de</w> <w n="3.4">la</w> <w n="3.5">table</w>, <w n="3.6">à</w> <w n="3.7">la</w> <w n="3.8">place</w> <w n="3.9">des</w> <w n="3.10">fleurs</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Frais</w> <w n="4.2">groupe</w> <w n="4.3">mariant</w> <w n="4.4">et</w> <w n="4.5">parfums</w> <w n="4.6">et</w> <w n="4.7">couleurs</w>,</l>
						<l n="5" num="1.5"><w n="5.1">Grimaçait</w> <w n="5.2">sur</w> <w n="5.3">un</w> <w n="5.4">plat</w> <w n="5.5">une</w> <w n="5.6">tête</w> <w n="5.7">coupée</w>,</l>
						<l n="6" num="1.6"><w n="6.1">Sculptée</w> <w n="6.2">en</w> <w n="6.3">bois</w> <w n="6.4">et</w> <w n="6.5">peinte</w>, <w n="6.6">et</w> <w n="6.7">dans</w> <w n="6.8">le</w> <w n="6.9">sang</w> <w n="6.10">trempée</w>,</l>
						<l n="7" num="1.7"><w n="7.1">Le</w> <w n="7.2">front</w> <w n="7.3">humide</w> <w n="7.4">encor</w> <w n="7.5">des</w> <w n="7.6">suprêmes</w> <w n="7.7">sueurs</w>,</l>
						<l n="8" num="1.8"><w n="8.1">L</w>’<w n="8.2">œil</w> <w n="8.3">vitreux</w> <w n="8.4">et</w> <w n="8.5">blanchi</w> <w n="8.6">de</w> <w n="8.7">ces</w> <w n="8.8">pâles</w> <w n="8.9">lueurs</w></l>
						<l n="9" num="1.9"><w n="9.1">Dont</w> <w n="9.2">la</w> <w n="9.3">lampe</w> <w n="9.4">de</w> <w n="9.5">l</w>’<w n="9.6">âme</w> <w n="9.7">en</w> <w n="9.8">s</w>’<w n="9.9">éteignant</w> <w n="9.10">scintille</w> ;</l>
						<l n="10" num="1.10"><w n="10.1">Chef</w>-<w n="10.2">d</w>’<w n="10.3">œuvre</w> <w n="10.4">affreux</w>, <w n="10.5">signé</w> <w n="10.6">Montañès</w> <w n="10.7">de</w> <w n="10.8">Séville</w>,</l>
						<l n="11" num="1.11"><w n="11.1">D</w>’<w n="11.2">une</w> <w n="11.3">vérité</w> <w n="11.4">telle</w> <w n="11.5">et</w> <w n="11.6">d</w>’<w n="11.7">un</w> <w n="11.8">si</w> <w n="11.9">fin</w> <w n="11.10">travail</w>,</l>
						<l n="12" num="1.12"><w n="12.1">Qu</w>’<w n="12.2">un</w> <w n="12.3">bourreau</w> <w n="12.4">n</w>’<w n="12.5">aurait</w> <w n="12.6">su</w> <w n="12.7">reprendre</w> <w n="12.8">un</w> <w n="12.9">seul</w> <w n="12.10">détail</w>.</l>
					</lg>
					<lg n="2">
						<l n="13" num="2.1"><w n="13.1">La</w> <w n="13.2">marquise</w> <w n="13.3">disait</w> : — <w n="13.4">Voyez</w> <w n="13.5">donc</w> <w n="13.6">quel</w> <w n="13.7">artiste</w> !</l>
						<l n="14" num="2.2"><w n="14.1">Nul</w> <w n="14.2">sculpteur</w> <w n="14.3">n</w>’<w n="14.4">a</w> <w n="14.5">jamais</w> <w n="14.6">fait</w> <w n="14.7">les</w> <w n="14.8">saint</w> <w n="14.9">Jean</w>-<w n="14.10">Baptiste</w></l>
						<l n="15" num="2.3"><w n="15.1">Et</w> <w n="15.2">rendu</w> <w n="15.3">les</w> <w n="15.4">effets</w> <w n="15.5">du</w> <w n="15.6">damas</w> <w n="15.7">sur</w> <w n="15.8">un</w> <w n="15.9">col</w></l>
						<l n="16" num="2.4"><w n="16.1">Comme</w> <w n="16.2">ce</w> <w n="16.3">Sévillan</w>, <w n="16.4">Michel</w>-<w n="16.5">Ange</w> <w n="16.6">espagnol</w> !</l>
						<l n="17" num="2.5"><w n="17.1">Quelle</w> <w n="17.2">imitation</w> <w n="17.3">dans</w> <w n="17.4">ces</w> <w n="17.5">veines</w> <w n="17.6">tranchées</w>,</l>
						<l n="18" num="2.6"><w n="18.1">Où</w> <w n="18.2">le</w> <w n="18.3">sang</w> <w n="18.4">perle</w> <w n="18.5">encore</w> <w n="18.6">en</w> <w n="18.7">gouttes</w> <w n="18.8">mal</w> <w n="18.9">séchées</w> !</l>
						<l n="19" num="2.7"><w n="19.1">Et</w> <w n="19.2">comme</w> <w n="19.3">dans</w> <w n="19.4">la</w> <w n="19.5">bouche</w> <w n="19.6">on</w> <w n="19.7">sent</w> <w n="19.8">le</w> <w n="19.9">dernier</w> <w n="19.10">cri</w></l>
						<l n="20" num="2.8"><w n="20.1">Sons</w> <w n="20.2">le</w> <w n="20.3">fer</w> <w n="20.4">jaillissant</w> <w n="20.5">de</w> <w n="20.6">ce</w> <w n="20.7">gosier</w> <w n="20.8">tari</w> ! —</l>
					</lg>
					<lg n="3">
						<l n="21" num="3.1"><w n="21.1">En</w> <w n="21.2">me</w> <w n="21.3">disant</w> <w n="21.4">cela</w> <w n="21.5">d</w>’<w n="21.6">une</w> <w n="21.7">voix</w> <w n="21.8">claire</w> <w n="21.9">et</w> <w n="21.10">douce</w>,</l>
						<l n="22" num="3.2"><w n="22.1">Sur</w> <w n="22.2">l</w>’<w n="22.3">atroce</w> <w n="22.4">sculpture</w> <w n="22.5">elle</w> <w n="22.6">passait</w> <w n="22.7">son</w> <w n="22.8">pouce</w>,</l>
						<l n="23" num="3.3"><w n="23.1">Coquette</w>, <w n="23.2">souriant</w> <w n="23.3">d</w>’<w n="23.4">un</w> <w n="23.5">sourire</w> <w n="23.6">charmant</w>,</l>
						<l n="24" num="3.4"><w n="24.1">L</w>’<w n="24.2">œil</w> <w n="24.3">humide</w> <w n="24.4">et</w> <w n="24.5">lustré</w> <w n="24.6">comme</w> <w n="24.7">pour</w> <w n="24.8">un</w> <w n="24.9">amant</w>.</l>
					</lg>
					<closer>
						<dateline>
						<placeName>Madrid</placeName>,
							<date not-before="1839" not-after="1849">184.</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>