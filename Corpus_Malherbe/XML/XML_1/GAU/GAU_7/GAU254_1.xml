<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ESPAÑA</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1374 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_7</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Second</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://gutenberg.org/files/45886/45886-h/45886-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Second</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1890">1890</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1845">1845</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">ESPAÑA</head><div type="poem" key="GAU254">
					<head type="main">LES YEUX BLEUS DE LA MONTAGNE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">On</w> <w n="1.2">trouve</w> <w n="1.3">dans</w> <w n="1.4">les</w> <w n="1.5">monts</w> <w n="1.6">des</w> <w n="1.7">lacs</w> <w n="1.8">de</w> <w n="1.9">quelques</w> <w n="1.10">toises</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Purs</w> <w n="2.2">comme</w> <w n="2.3">des</w> <w n="2.4">cristaux</w>, <w n="2.5">bleus</w> <w n="2.6">comme</w> <w n="2.7">des</w> <w n="2.8">turquoises</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Joyaux</w> <w n="3.2">tombés</w> <w n="3.3">du</w> <w n="3.4">doigt</w> <w n="3.5">de</w> <w n="3.6">l</w>’<w n="3.7">ange</w> <w n="3.8">Ithuriel</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Où</w>, <w n="4.2">le</w> <w n="4.3">chamois</w> <w n="4.4">craintif</w>, <w n="4.5">lorsqu</w>’<w n="4.6">il</w> <w n="4.7">vient</w> <w n="4.8">pour</w> <w n="4.9">y</w> <w n="4.10">boire</w>,</l>
						<l n="5" num="1.5"><w n="5.1">S</w>’<w n="5.2">imagine</w>, <w n="5.3">trompé</w> <w n="5.4">par</w> <w n="5.5">l</w>’<w n="5.6">optique</w> <w n="5.7">illusoire</w>,</l>
						<l n="6" num="1.6"><space quantity="12" unit="char"></space><w n="6.1">Laper</w> <w n="6.2">l</w>’<w n="6.3">azur</w> <w n="6.4">du</w> <w n="6.5">ciel</w>.</l>
					</lg>
					<lg n="2">
						<l n="7" num="2.1"><w n="7.1">Ces</w> <w n="7.2">limpides</w> <w n="7.3">bassins</w>, <w n="7.4">quand</w> <w n="7.5">le</w> <w n="7.6">jour</w> <w n="7.7">s</w>’<w n="7.8">y</w> <w n="7.9">reflète</w>,</l>
						<l n="8" num="2.2"><w n="8.1">Ont</w> <w n="8.2">comme</w> <w n="8.3">la</w> <w n="8.4">prunelle</w> <w n="8.5">une</w> <w n="8.6">humide</w> <w n="8.7">paillette</w> ;</l>
						<l n="9" num="2.3"><w n="9.1">Et</w> <w n="9.2">ce</w> <w n="9.3">sont</w> <w n="9.4">les</w> <w n="9.5">yeux</w> <w n="9.6">bleus</w>, <w n="9.7">au</w> <w n="9.8">regard</w> <w n="9.9">calme</w> <w n="9.10">et</w> <w n="9.11">doux</w>,</l>
						<l n="10" num="2.4"><w n="10.1">Par</w> <w n="10.2">lesquels</w> <w n="10.3">la</w> <w n="10.4">montagne</w> <w n="10.5">en</w> <w n="10.6">extase</w> <w n="10.7">contemple</w>,</l>
						<l n="11" num="2.5"><w n="11.1">Forgeant</w> <w n="11.2">quelque</w> <w n="11.3">soleil</w> <w n="11.4">dans</w> <w n="11.5">le</w> <w n="11.6">fond</w> <w n="11.7">de</w> <w n="11.8">son</w> <w n="11.9">temple</w>,</l>
						<l n="12" num="2.6"><space quantity="12" unit="char"></space><w n="12.1">Dieu</w>, <w n="12.2">l</w>’<w n="12.3">ouvrier</w> <w n="12.4">jaloux</w> !</l>
					</lg>
					<closer>
						<dateline>
						<placeName>Guadarrama</placeName>,
							<date not-before="1839" not-after="1849">184.</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>