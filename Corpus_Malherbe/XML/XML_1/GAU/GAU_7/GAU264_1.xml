<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ESPAÑA</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1374 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_7</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Second</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://gutenberg.org/files/45886/45886-h/45886-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Second</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1890">1890</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1845">1845</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">ESPAÑA</head><div type="poem" key="GAU264">
					<head type="main">STANCES</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Maintenant</w>, — <w n="1.2">dans</w> <w n="1.3">la</w> <w n="1.4">plaine</w> <w n="1.5">ou</w> <w n="1.6">bien</w> <w n="1.7">dans</w> <w n="1.8">la</w> <w n="1.9">montagne</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Chêne</w> <w n="2.2">ou</w> <w n="2.3">sapin</w>, <w n="2.4">un</w> <w n="2.5">arbre</w> <w n="2.6">est</w> <w n="2.7">en</w> <w n="2.8">train</w> <w n="2.9">de</w> <w n="2.10">pousser</w>,</l>
						<l n="3" num="1.3"><w n="3.1">En</w> <w n="3.2">France</w>, <w n="3.3">en</w> <w n="3.4">Amérique</w>, <w n="3.5">en</w> <w n="3.6">Turquie</w>, <w n="3.7">en</w> <w n="3.8">Espagne</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Un</w> <w n="4.2">arbre</w> <w n="4.3">sous</w> <w n="4.4">lequel</w> <w n="4.5">un</w> <w n="4.6">jour</w> <w n="4.7">je</w> <w n="4.8">puis</w> <w n="4.9">passer</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Maintenant</w>, — <w n="5.2">sur</w> <w n="5.3">le</w> <w n="5.4">seuil</w> <w n="5.5">d</w>’<w n="5.6">une</w> <w n="5.7">pauvre</w> <w n="5.8">chaumière</w>,</l>
						<l n="6" num="2.2"><w n="6.1">Une</w> <w n="6.2">femme</w>, <w n="6.3">du</w> <w n="6.4">pied</w> <w n="6.5">agitant</w> <w n="6.6">un</w> <w n="6.7">berceau</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Sans</w> <w n="7.2">se</w> <w n="7.3">douter</w> <w n="7.4">qu</w>’<w n="7.5">elle</w> <w n="7.6">est</w> <w n="7.7">la</w> <w n="7.8">parque</w> <w n="7.9">filandière</w>,</l>
						<l n="8" num="2.4"><w n="8.1">Allonge</w> <w n="8.2">entre</w> <w n="8.3">ses</w> <w n="8.4">doigts</w> <w n="8.5">l</w>’<w n="8.6">étoupe</w> <w n="8.7">d</w>’<w n="8.8">un</w> <w n="8.9">fuseau</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Maintenant</w>, — <w n="9.2">loin</w> <w n="9.3">du</w> <w n="9.4">ciel</w> <w n="9.5">à</w> <w n="9.6">la</w> <w n="9.7">splendeur</w> <w n="9.8">divine</w>,</l>
						<l n="10" num="3.2"><w n="10.1">Comme</w> <w n="10.2">une</w> <w n="10.3">taupe</w> <w n="10.4">aveugle</w> <w n="10.5">en</w> <w n="10.6">son</w> <w n="10.7">étroit</w> <w n="10.8">couloir</w>,</l>
						<l n="11" num="3.3"><w n="11.1">Pour</w> <w n="11.2">arracher</w> <w n="11.3">le</w> <w n="11.4">fer</w> <w n="11.5">au</w> <w n="11.6">ventre</w> <w n="11.7">de</w> <w n="11.8">la</w> <w n="11.9">mine</w>,</l>
						<l n="12" num="3.4"><w n="12.1">Sous</w> <w n="12.2">le</w> <w n="12.3">sol</w> <w n="12.4">des</w> <w n="12.5">vivants</w> <w n="12.6">plonge</w> <w n="12.7">un</w> <w n="12.8">travailleur</w> <w n="12.9">noir</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Maintenant</w>, — <w n="13.2">dans</w> <w n="13.3">un</w> <w n="13.4">coin</w> <w n="13.5">du</w> <w n="13.6">monde</w> <w n="13.7">que</w> <w n="13.8">j</w>’<w n="13.9">ignore</w>,</l>
						<l n="14" num="4.2"><w n="14.1">Il</w> <w n="14.2">existe</w> <w n="14.3">une</w> <w n="14.4">place</w> <w n="14.5">où</w> <w n="14.6">le</w> <w n="14.7">gazon</w> <w n="14.8">fleurit</w>,</l>
						<l n="15" num="4.3"><w n="15.1">Où</w> <w n="15.2">le</w> <w n="15.3">soleil</w> <w n="15.4">joyeux</w> <w n="15.5">boit</w> <w n="15.6">les</w> <w n="15.7">pleurs</w> <w n="15.8">de</w> <w n="15.9">l</w>’<w n="15.10">aurore</w>,</l>
						<l n="16" num="4.4"><w n="16.1">Où</w> <w n="16.2">l</w>’<w n="16.3">abeille</w> <w n="16.4">bourdonne</w>, <w n="16.5">où</w> <w n="16.6">l</w>’<w n="16.7">oiseau</w> <w n="16.8">chante</w> <w n="16.9">et</w> <w n="16.10">rit</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Cet</w> <w n="17.2">arbre</w> <w n="17.3">qui</w> <w n="17.4">soutient</w> <w n="17.5">tant</w> <w n="17.6">de</w> <w n="17.7">nids</w> <w n="17.8">sur</w> <w n="17.9">ses</w> <w n="17.10">branches</w>,</l>
						<l n="18" num="5.2"><w n="18.1">Cet</w> <w n="18.2">arbre</w> <w n="18.3">épais</w> <w n="18.4">et</w> <w n="18.5">vert</w>, <w n="18.6">frais</w> <w n="18.7">et</w> <w n="18.8">riant</w> <w n="18.9">à</w> <w n="18.10">l</w>’<w n="18.11">œil</w>,</l>
						<l n="19" num="5.3"><w n="19.1">Dans</w> <w n="19.2">son</w> <w n="19.3">tronc</w> <w n="19.4">renversé</w> <w n="19.5">l</w>’<w n="19.6">on</w> <w n="19.7">taillera</w> <w n="19.8">des</w> <w n="19.9">planches</w>,</l>
						<l n="20" num="5.4"><w n="20.1">Les</w> <w n="20.2">planches</w> <w n="20.3">dont</w> <w n="20.4">un</w> <w n="20.5">jour</w> <w n="20.6">on</w> <w n="20.7">fera</w> <w n="20.8">mon</w> <w n="20.9">cercueil</w> !</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Cette</w> <w n="21.2">étoupe</w> <w n="21.3">qu</w>’<w n="21.4">on</w> <w n="21.5">file</w> <w n="21.6">et</w> <w n="21.7">qui</w>, <w n="21.8">tissée</w> <w n="21.9">en</w> <w n="21.10">toile</w>,</l>
						<l n="22" num="6.2"><w n="22.1">Donne</w> <w n="22.2">une</w> <w n="22.3">aile</w> <w n="22.4">au</w> <w n="22.5">vaisseau</w> <w n="22.6">dans</w> <w n="22.7">le</w> <w n="22.8">port</w> <w n="22.9">engourdi</w>,</l>
						<l n="23" num="6.3"><w n="23.1">A</w> <w n="23.2">l</w>’<w n="23.3">orgie</w> <w n="23.4">une</w> <w n="23.5">nappe</w>, <w n="23.6">à</w> <w n="23.7">la</w> <w n="23.8">pudeur</w> <w n="23.9">un</w> <w n="23.10">voile</w>,</l>
						<l n="24" num="6.4"><w n="24.1">Linceul</w>, <w n="24.2">revêtira</w> <w n="24.3">mon</w> <w n="24.4">cadavre</w> <w n="24.5">verdi</w> !</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">Ce</w> <w n="25.2">fer</w> <w n="25.3">que</w> <w n="25.4">le</w> <w n="25.5">mineur</w> <w n="25.6">cherche</w> <w n="25.7">au</w> <w n="25.8">fond</w> <w n="25.9">de</w> <w n="25.10">la</w> <w n="25.11">terre</w></l>
						<l n="26" num="7.2"><w n="26.1">Aux</w> <w n="26.2">brumeuses</w> <w n="26.3">clartés</w> <w n="26.4">de</w> <w n="26.5">son</w> <w n="26.6">pâle</w> <w n="26.7">fanal</w>,</l>
						<l n="27" num="7.3"><w n="27.1">Hélas</w> ! <w n="27.2">le</w> <w n="27.3">forgeron</w> <w n="27.4">quelque</w> <w n="27.5">jour</w> <w n="27.6">en</w> <w n="27.7">doit</w> <w n="27.8">faire</w></l>
						<l n="28" num="7.4"><w n="28.1">Le</w> <w n="28.2">clou</w> <w n="28.3">qui</w> <w n="28.4">fermera</w> <w n="28.5">le</w> <w n="28.6">couvercle</w> <w n="28.7">fatal</w> !</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1"><w n="29.1">A</w> <w n="29.2">cette</w> <w n="29.3">même</w> <w n="29.4">place</w> <w n="29.5">où</w> <w n="29.6">mille</w> <w n="29.7">fois</w> <w n="29.8">peut</w>-<w n="29.9">être</w></l>
						<l n="30" num="8.2"><w n="30.1">J</w>’<w n="30.2">allai</w> <w n="30.3">m</w>’<w n="30.4">asseoir</w>, <w n="30.5">le</w> <w n="30.6">cœur</w> <w n="30.7">plein</w> <w n="30.8">de</w> <w n="30.9">rêves</w> <w n="30.10">charmants</w>,</l>
						<l n="31" num="8.3"><w n="31.1">S</w>’<w n="31.2">entr</w>’<w n="31.3">ouvrira</w> <w n="31.4">le</w> <w n="31.5">gouffre</w> <w n="31.6">où</w> <w n="31.7">je</w> <w n="31.8">dois</w> <w n="31.9">disparaître</w>,</l>
						<l n="32" num="8.4"><w n="32.1">Pour</w> <w n="32.2">descendre</w> <w n="32.3">au</w> <w n="32.4">séjour</w> <w n="32.5">des</w> <w n="32.6">épouvantements</w> !</l>
					</lg>
					<closer>
						<dateline>
						<placeName>Manche</placeName>,
							<date when="1843">184.</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>