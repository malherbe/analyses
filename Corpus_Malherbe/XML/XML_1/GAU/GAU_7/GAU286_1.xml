<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ESPAÑA</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1374 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_7</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Second</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://gutenberg.org/files/45886/45886-h/45886-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Second</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1890">1890</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1845">1845</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">ESPAÑA</head><div type="poem" key="GAU286">
					<head type="main">LES AFFRES DE LA MORT</head>
					<head type="sub_1">(SUR LES MURS D’UNE CHARTREUSE)</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">O</w> <w n="1.2">toi</w> <w n="1.3">qui</w> <w n="1.4">passes</w> <w n="1.5">par</w> <w n="1.6">ce</w> <w n="1.7">cloître</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Songe</w> <w n="2.2">à</w> <w n="2.3">la</w> <w n="2.4">mort</w> ! — <w n="2.5">Tu</w> <w n="2.6">n</w>’<w n="2.7">es</w> <w n="2.8">pas</w> <w n="2.9">sûr</w></l>
						<l n="3" num="1.3"><w n="3.1">De</w> <w n="3.2">voir</w> <w n="3.3">s</w>’<w n="3.4">allonger</w> <w n="3.5">et</w> <w n="3.6">décroître</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Une</w> <w n="4.2">autre</w> <w n="4.3">fois</w>, <w n="4.4">ton</w> <w n="4.5">ombre</w> <w n="4.6">au</w> <w n="4.7">mur</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Frère</w>, <w n="5.2">peut</w>-<w n="5.3">être</w> <w n="5.4">cette</w> <w n="5.5">dalle</w></l>
						<l n="6" num="2.2"><w n="6.1">Qu</w>’<w n="6.2">aujourd</w>’<w n="6.3">hui</w>, <w n="6.4">sans</w> <w n="6.5">songer</w> <w n="6.6">aux</w> <w n="6.7">morts</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Tu</w> <w n="7.2">soufflettes</w> <w n="7.3">de</w> <w n="7.4">ta</w> <w n="7.5">sandale</w>,</l>
						<l n="8" num="2.4"><w n="8.1">Demain</w> <w n="8.2">pèsera</w> <w n="8.3">sur</w> <w n="8.4">ton</w> <w n="8.5">corps</w> !</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">La</w> <w n="9.2">vie</w> <w n="9.3">est</w> <w n="9.4">un</w> <w n="9.5">plancher</w> <w n="9.6">qui</w> <w n="9.7">couvre</w></l>
						<l n="10" num="3.2"><w n="10.1">L</w>’<w n="10.2">abîme</w> <w n="10.3">de</w> <w n="10.4">l</w>’<w n="10.5">éternité</w> :</l>
						<l n="11" num="3.3"><w n="11.1">Une</w> <w n="11.2">trappe</w> <w n="11.3">soudain</w> <w n="11.4">s</w>’<w n="11.5">entr</w>’<w n="11.6">ouvre</w></l>
						<l n="12" num="3.4"><w n="12.1">Sous</w> <w n="12.2">le</w> <w n="12.3">pécheur</w> <w n="12.4">épouvanté</w> ;</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Le</w> <w n="13.2">pied</w> <w n="13.3">lui</w> <w n="13.4">manque</w>, <w n="13.5">il</w> <w n="13.6">tombe</w>, <w n="13.7">il</w> <w n="13.8">glisse</w> :</l>
						<l n="14" num="4.2"><w n="14.1">Que</w> <w n="14.2">va</w>-<w n="14.3">t</w>-<w n="14.4">il</w> <w n="14.5">trouver</w> ? <w n="14.6">le</w> <w n="14.7">ciel</w> <w n="14.8">bleu</w></l>
						<l n="15" num="4.3"><w n="15.1">Ou</w> <w n="15.2">l</w>’<w n="15.3">enfer</w> <w n="15.4">rouge</w> ? <w n="15.5">le</w> <w n="15.6">supplice</w></l>
						<l n="16" num="4.4"><w n="16.1">Ou</w> <w n="16.2">la</w> <w n="16.3">palme</w> ? <w n="16.4">Satan</w> <w n="16.5">ou</w> <w n="16.6">Dieu</w> ?…</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Souvent</w> <w n="17.2">sur</w> <w n="17.3">cette</w> <w n="17.4">idée</w> <w n="17.5">affreuse</w></l>
						<l n="18" num="5.2"><w n="18.1">Fixe</w> <w n="18.2">ton</w> <w n="18.3">esprit</w> <w n="18.4">éperdu</w> :</l>
						<l n="19" num="5.3"><w n="19.1">Le</w> <w n="19.2">teint</w> <w n="19.3">jaune</w> <w n="19.4">et</w> <w n="19.5">la</w> <w n="19.6">peau</w> <w n="19.7">terreuse</w>,</l>
						<l n="20" num="5.4"><w n="20.1">Vois</w>-<w n="20.2">toi</w> <w n="20.3">sur</w> <w n="20.4">un</w> <w n="20.5">lit</w> <w n="20.6">étendu</w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Vois</w>-<w n="21.2">toi</w> <w n="21.3">brûlé</w>, <w n="21.4">transi</w> <w n="21.5">de</w> <w n="21.6">fièvre</w>,</l>
						<l n="22" num="6.2"><w n="22.1">Tordu</w> <w n="22.2">comme</w> <w n="22.3">un</w> <w n="22.4">bois</w> <w n="22.5">vert</w> <w n="22.6">au</w> <w n="22.7">feu</w>,</l>
						<l n="23" num="6.3"><w n="23.1">Le</w> <w n="23.2">fiel</w> <w n="23.3">crevé</w>, <w n="23.4">l</w>’<w n="23.5">âme</w> <w n="23.6">à</w> <w n="23.7">la</w> <w n="23.8">lèvre</w>,</l>
						<l n="24" num="6.4"><w n="24.1">Sanglotant</w> <w n="24.2">le</w> <w n="24.3">suprême</w> <w n="24.4">adieu</w>,</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">Entre</w> <w n="25.2">deux</w> <w n="25.3">draps</w>, <w n="25.4">dont</w> <w n="25.5">l</w>’<w n="25.6">un</w> <w n="25.7">doit</w> <w n="25.8">être</w></l>
						<l n="26" num="7.2"><w n="26.1">Le</w> <w n="26.2">linceul</w> <w n="26.3">où</w> <w n="26.4">l</w>’<w n="26.5">on</w> <w n="26.6">te</w> <w n="26.7">coudra</w> ;</l>
						<l n="27" num="7.3"><w n="27.1">Triste</w> <w n="27.2">habit</w> <w n="27.3">que</w> <w n="27.4">nul</w> <w n="27.5">ne</w> <w n="27.6">veut</w> <w n="27.7">mettre</w>,</l>
						<l n="28" num="7.4"><w n="28.1">Et</w> <w n="28.2">que</w> <w n="28.3">pourtant</w> <w n="28.4">chacun</w> <w n="28.5">mettra</w>.</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1"><w n="29.1">Représente</w>-<w n="29.2">toi</w> <w n="29.3">bien</w> <w n="29.4">l</w>’<w n="29.5">angoisse</w></l>
						<l n="30" num="8.2"><w n="30.1">De</w> <w n="30.2">ta</w> <w n="30.3">chair</w> <w n="30.4">flairant</w> <w n="30.5">le</w> <w n="30.6">tombeau</w>,</l>
						<l n="31" num="8.3"><w n="31.1">Tes</w> <w n="31.2">pieds</w> <w n="31.3">crispés</w>, <w n="31.4">ta</w> <w n="31.5">main</w> <w n="31.6">qui</w> <w n="31.7">froisse</w></l>
						<l n="32" num="8.4"><w n="32.1">Tes</w> <w n="32.2">couvertures</w> <w n="32.3">en</w> <w n="32.4">lambeau</w>.</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1"><w n="33.1">En</w> <w n="33.2">pensée</w>, <w n="33.3">écoute</w> <w n="33.4">le</w> <w n="33.5">râle</w>,</l>
						<l n="34" num="9.2"><w n="34.1">Bramant</w> <w n="34.2">comme</w> <w n="34.3">un</w> <w n="34.4">cerf</w> <w n="34.5">aux</w> <w n="34.6">abois</w>,</l>
						<l n="35" num="9.3"><w n="35.1">Pousser</w> <w n="35.2">sa</w> <w n="35.3">note</w> <w n="35.4">sépulcrale</w></l>
						<l n="36" num="9.4"><w n="36.1">Par</w> <w n="36.2">ton</w> <w n="36.3">gosier</w> <w n="36.4">rauque</w> <w n="36.5">et</w> <w n="36.6">sans</w> <w n="36.7">voix</w>.</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1"><w n="37.1">Le</w> <w n="37.2">sang</w> <w n="37.3">quitte</w> <w n="37.4">tes</w> <w n="37.5">jambes</w> <w n="37.6">roides</w>,</l>
						<l n="38" num="10.2"><w n="38.1">Les</w> <w n="38.2">ombres</w> <w n="38.3">gagnent</w> <w n="38.4">ton</w> <w n="38.5">cerveau</w>,</l>
						<l n="39" num="10.3"><w n="39.1">Et</w> <w n="39.2">sur</w> <w n="39.3">ton</w> <w n="39.4">front</w> <w n="39.5">les</w> <w n="39.6">perles</w> <w n="39.7">froides</w></l>
						<l n="40" num="10.4"><w n="40.1">Coulent</w> <w n="40.2">comme</w> <w n="40.3">aux</w> <w n="40.4">murs</w> <w n="40.5">d</w>’<w n="40.6">un</w> <w n="40.7">caveau</w>.</l>
					</lg>
					<lg n="11">
						<l n="41" num="11.1"><w n="41.1">Les</w> <w n="41.2">prêtres</w> <w n="41.3">à</w> <w n="41.4">soutane</w> <w n="41.5">noire</w>,</l>
						<l n="42" num="11.2"><w n="42.1">Toujours</w> <w n="42.2">en</w> <w n="42.3">deuil</w> <w n="42.4">de</w> <w n="42.5">nos</w> <w n="42.6">péchés</w>,</l>
						<l n="43" num="11.3"><w n="43.1">Apportent</w> <w n="43.2">l</w>’<w n="43.3">huile</w> <w n="43.4">et</w> <w n="43.5">le</w> <w n="43.6">ciboire</w>,</l>
						<l n="44" num="11.4"><w n="44.1">Autour</w> <w n="44.2">de</w> <w n="44.3">ton</w> <w n="44.4">grabat</w> <w n="44.5">penchés</w>.</l>
					</lg>
					<lg n="12">
						<l n="45" num="12.1"><w n="45.1">Tes</w> <w n="45.2">enfants</w>, <w n="45.3">ta</w> <w n="45.4">femme</w> <w n="45.5">et</w> <w n="45.6">tes</w> <w n="45.7">proches</w></l>
						<l n="46" num="12.2"><w n="46.1">Pleurent</w> <w n="46.2">en</w> <w n="46.3">se</w> <w n="46.4">tordant</w> <w n="46.5">les</w> <w n="46.6">bras</w>,</l>
						<l n="47" num="12.3"><w n="47.1">Et</w> <w n="47.2">déjà</w> <w n="47.3">le</w> <w n="47.4">sonneur</w> <w n="47.5">aux</w> <w n="47.6">cloches</w></l>
						<l n="48" num="12.4"><w n="48.1">Se</w> <w n="48.2">suspend</w> <w n="48.3">pour</w> <w n="48.4">sonner</w> <w n="48.5">ton</w> <w n="48.6">glas</w>.</l>
					</lg>
					<lg n="13">
						<l n="49" num="13.1"><w n="49.1">Le</w> <w n="49.2">fossoyeur</w> <w n="49.3">a</w> <w n="49.4">pris</w> <w n="49.5">sa</w> <w n="49.6">bêche</w></l>
						<l n="50" num="13.2"><w n="50.1">Pour</w> <w n="50.2">te</w> <w n="50.3">creuser</w> <w n="50.4">ton</w> <w n="50.5">dernier</w> <w n="50.6">lit</w>,</l>
						<l n="51" num="13.3"><w n="51.1">Et</w> <w n="51.2">d</w>’<w n="51.3">une</w> <w n="51.4">terre</w> <w n="51.5">brune</w> <w n="51.6">et</w> <w n="51.7">fraîche</w></l>
						<l n="52" num="13.4"><w n="52.1">Bientôt</w> <w n="52.2">ta</w> <w n="52.3">fosse</w> <w n="52.4">se</w> <w n="52.5">remplit</w>.</l>
					</lg>
					<lg n="14">
						<l n="53" num="14.1"><w n="53.1">Ta</w> <w n="53.2">chair</w> <w n="53.3">délicate</w> <w n="53.4">et</w> <w n="53.5">superbe</w></l>
						<l n="54" num="14.2"><w n="54.1">Va</w> <w n="54.2">servir</w> <w n="54.3">de</w> <w n="54.4">pâture</w> <w n="54.5">aux</w> <w n="54.6">vers</w>,</l>
						<l n="55" num="14.3"><w n="55.1">Et</w> <w n="55.2">tu</w> <w n="55.3">feras</w> <w n="55.4">pousser</w> <w n="55.5">de</w> <w n="55.6">l</w>’<w n="55.7">herbe</w></l>
						<l n="56" num="14.4"><w n="56.1">Plus</w> <w n="56.2">drue</w> <w n="56.3">avec</w> <w n="56.4">des</w> <w n="56.5">brins</w> <w n="56.6">plus</w> <w n="56.7">verts</w>.</l>
					</lg>
					<lg n="15">
						<l n="57" num="15.1"><w n="57.1">Donc</w>, <w n="57.2">pour</w> <w n="57.3">n</w>’<w n="57.4">être</w> <w n="57.5">pas</w> <w n="57.6">surpris</w>, <w n="57.7">frère</w>,</l>
						<l n="58" num="15.2"><w n="58.1">Aux</w> <w n="58.2">transes</w> <w n="58.3">du</w> <w n="58.4">dernier</w> <w n="58.5">moment</w>,</l>
						<l n="59" num="15.3"><w n="59.1">Réfléchis</w> ! — <w n="59.2">La</w> <w n="59.3">mort</w> <w n="59.4">est</w> <w n="59.5">amère</w></l>
						<l n="60" num="15.4"><w n="60.1">A</w> <w n="60.2">qui</w> <w n="60.3">vécut</w> <w n="60.4">trop</w> <w n="60.5">doucement</w>.</l>
					</lg>
					<lg n="16">
						<l n="61" num="16.1"><w n="61.1">Sur</w> <w n="61.2">ce</w>, <w n="61.3">frère</w>, <w n="61.4">que</w> <w n="61.5">Dieu</w> <w n="61.6">t</w>’<w n="61.7">accorde</w></l>
						<l n="62" num="16.2"><w n="62.1">De</w> <w n="62.2">trépasser</w> <w n="62.3">en</w> <w n="62.4">bon</w> <w n="62.5">chrétien</w>,</l>
						<l n="63" num="16.3"><w n="63.1">Et</w> <w n="63.2">te</w> <w n="63.3">fasse</w> <w n="63.4">miséricorde</w> ;</l>
						<l n="64" num="16.4"><w n="64.1">Ici</w>-<w n="64.2">bas</w>, <w n="64.3">nul</w> <w n="64.4">ne</w> <w n="64.5">peut</w> <w n="64.6">plus</w> <w n="64.7">rien</w> !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1843">1843</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>