<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ESPAÑA</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1374 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_7</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Second</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://gutenberg.org/files/45886/45886-h/45886-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Second</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1890">1890</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1845">1845</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">ESPAÑA</head><div type="poem" key="GAU275">
					<head type="main">LA LUNE ET LE SOLEIL</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Le</w> <w n="1.2">soleil</w> <w n="1.3">dit</w> <w n="1.4">à</w> <w n="1.5">la</w> <w n="1.6">lune</w> :</l>
						<l n="2" num="1.2">— <w n="2.1">Que</w> <w n="2.2">fais</w>-<w n="2.3">tu</w> <w n="2.4">sur</w> <w n="2.5">l</w>’<w n="2.6">horizon</w> ?</l>
						<l n="3" num="1.3"><w n="3.1">Il</w> <w n="3.2">est</w> <w n="3.3">bien</w> <w n="3.4">tard</w>, <w n="3.5">à</w> <w n="3.6">la</w> <w n="3.7">brune</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Pour</w> <w n="4.2">sortir</w> <w n="4.3">de</w> <w n="4.4">sa</w> <w n="4.5">maison</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">L</w>’<w n="5.2">honnête</w> <w n="5.3">femme</w>, <w n="5.4">à</w> <w n="5.5">cette</w> <w n="5.6">heure</w>,</l>
						<l n="6" num="2.2"><w n="6.1">Défile</w> <w n="6.2">son</w> <w n="6.3">chapelet</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Couche</w> <w n="7.2">son</w> <w n="7.3">enfant</w> <w n="7.4">qui</w> <w n="7.5">pleure</w>,</l>
						<l n="8" num="2.4"><w n="8.1">Et</w> <w n="8.2">met</w> <w n="8.3">la</w> <w n="8.4">barre</w> <w n="8.5">au</w> <w n="8.6">volet</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Le</w> <w n="9.2">follet</w> <w n="9.3">court</w> <w n="9.4">sur</w> <w n="9.5">la</w> <w n="9.6">dune</w> ;</l>
						<l n="10" num="3.2"><w n="10.1">Gitanas</w>, <w n="10.2">chauves</w>-<w n="10.3">souris</w>,</l>
						<l n="11" num="3.3"><w n="11.1">Rôdent</w> <w n="11.2">en</w> <w n="11.3">cherchant</w> <w n="11.4">fortune</w> ;</l>
						<l n="12" num="3.4"><w n="12.1">Noirs</w> <w n="12.2">ou</w> <w n="12.3">blancs</w>, <w n="12.4">tous</w> <w n="12.5">chats</w> <w n="12.6">sont</w> <w n="12.7">gris</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Des</w> <w n="13.2">planètes</w> <w n="13.3">équivoques</w></l>
						<l n="14" num="4.2"><w n="14.1">Et</w> <w n="14.2">des</w> <w n="14.3">astres</w> <w n="14.4">libertins</w>,</l>
						<l n="15" num="4.3"><w n="15.1">Croyant</w> <w n="15.2">que</w> <w n="15.3">tu</w> <w n="15.4">les</w> <w n="15.5">provoques</w>,</l>
						<l n="16" num="4.4"><w n="16.1">Suivront</w> <w n="16.2">tes</w> <w n="16.3">pas</w> <w n="16.4">clandestins</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">La</w> <w n="17.2">nuit</w>, <w n="17.3">dehors</w> <w n="17.4">on</w> <w n="17.5">s</w>’<w n="17.6">enrhume</w>.</l>
						<l n="18" num="5.2"><w n="18.1">Vas</w>-<w n="18.2">tu</w> <w n="18.3">prendre</w> <w n="18.4">encor</w> <w n="18.5">ce</w> <w n="18.6">soir</w></l>
						<l n="19" num="5.3"><w n="19.1">Le</w> <w n="19.2">brouillard</w> <w n="19.3">pour</w> <w n="19.4">lit</w> <w n="19.5">de</w> <w n="19.6">plume</w></l>
						<l n="20" num="5.4"><w n="20.1">Et</w> <w n="20.2">l</w>’<w n="20.3">eau</w> <w n="20.4">du</w> <w n="20.5">lac</w> <w n="20.6">pour</w> <w n="20.7">miroir</w> ?</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Réponds</w>-<w n="21.2">moi</w>. — <w n="21.3">J</w>’<w n="21.4">ai</w> <w n="21.5">cent</w> <w n="21.6">retraites</w></l>
						<l n="22" num="6.2"><w n="22.1">Sur</w> <w n="22.2">la</w> <w n="22.3">terre</w> <w n="22.4">et</w> <w n="22.5">dans</w> <w n="22.6">les</w> <w n="22.7">cieux</w>,</l>
						<l n="23" num="6.3"><w n="23.1">Monsieur</w> <w n="23.2">mon</w> <w n="23.3">frère</w> ; <w n="23.4">et</w> <w n="23.5">vous</w> <w n="23.6">êtes</w></l>
						<l n="24" num="6.4"><w n="24.1">Un</w> <w n="24.2">astre</w> <w n="24.3">bien</w> <w n="24.4">curieux</w> !</l>
					</lg>
					<closer>
						<dateline>
						<placeName>Généralife</placeName>,
							<date when="1844">184.</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>