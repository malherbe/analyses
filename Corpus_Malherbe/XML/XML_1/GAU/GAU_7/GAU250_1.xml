<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ESPAÑA</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1374 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_7</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Second</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://gutenberg.org/files/45886/45886-h/45886-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Second</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1890">1890</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1845">1845</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">ESPAÑA</head><div type="poem" key="GAU250">
					<head type="main">EN ALLANT A LA CHARTREUSE DE MIRAFLORES</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Oui</w>, <w n="1.2">c</w>’<w n="1.3">est</w> <w n="1.4">une</w> <w n="1.5">montée</w> <w n="1.6">âpre</w>, <w n="1.7">longue</w> <w n="1.8">et</w> <w n="1.9">poudreuse</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Un</w> <w n="2.2">revers</w> <w n="2.3">décharné</w>, <w n="2.4">vrai</w> <w n="2.5">site</w> <w n="2.6">de</w> <w n="2.7">Chartreuse</w>.</l>
						<l n="3" num="1.3"><w n="3.1">Les</w> <w n="3.2">pierres</w> <w n="3.3">du</w> <w n="3.4">chemin</w>, <w n="3.5">qui</w> <w n="3.6">croulent</w> <w n="3.7">sous</w> <w n="3.8">les</w> <w n="3.9">pieds</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Trompent</w> <w n="4.2">à</w> <w n="4.3">chaque</w> <w n="4.4">instant</w> <w n="4.5">les</w> <w n="4.6">pas</w> <w n="4.7">mal</w> <w n="4.8">appuyés</w>.</l>
						<l n="5" num="1.5"><w n="5.1">Pas</w> <w n="5.2">un</w> <w n="5.3">brin</w> <w n="5.4">d</w>’<w n="5.5">herbe</w> <w n="5.6">vert</w>, <w n="5.7">pas</w> <w n="5.8">une</w> <w n="5.9">teinte</w> <w n="5.10">fraîche</w> ;</l>
						<l n="6" num="1.6"><w n="6.1">On</w> <w n="6.2">ne</w> <w n="6.3">voit</w> <w n="6.4">que</w> <w n="6.5">des</w> <w n="6.6">murs</w> <w n="6.7">bâtis</w> <w n="6.8">en</w> <w n="6.9">pierre</w> <w n="6.10">sèche</w>,</l>
						<l n="7" num="1.7"><w n="7.1">Des</w> <w n="7.2">groupes</w> <w n="7.3">contrefaits</w> <w n="7.4">d</w>’<w n="7.5">oliviers</w> <w n="7.6">rabougris</w>,</l>
						<l n="8" num="1.8"><w n="8.1">Au</w> <w n="8.2">feuillage</w> <w n="8.3">malsain</w> <w n="8.4">couleur</w> <w n="8.5">de</w> <w n="8.6">vert</w>-<w n="8.7">de</w>-<w n="8.8">gris</w>,</l>
						<l n="9" num="1.9"><w n="9.1">Des</w> <w n="9.2">pentes</w> <w n="9.3">au</w> <w n="9.4">soleil</w>, <w n="9.5">que</w> <w n="9.6">nulle</w> <w n="9.7">fleur</w> <w n="9.8">n</w>’<w n="9.9">égaie</w>,</l>
						<l n="10" num="1.10"><w n="10.1">Des</w> <w n="10.2">roches</w> <w n="10.3">de</w> <w n="10.4">granit</w> <w n="10.5">et</w> <w n="10.6">des</w> <w n="10.7">ravins</w> <w n="10.8">de</w> <w n="10.9">craie</w>,</l>
						<l n="11" num="1.11"><w n="11.1">Et</w> <w n="11.2">l</w>’<w n="11.3">on</w> <w n="11.4">se</w> <w n="11.5">sent</w> <w n="11.6">le</w> <w n="11.7">cœur</w> <w n="11.8">de</w> <w n="11.9">tristesse</w> <w n="11.10">serré</w>…</l>
						<l n="12" num="1.12"><w n="12.1">Mais</w>, <w n="12.2">quand</w> <w n="12.3">on</w> <w n="12.4">est</w> <w n="12.5">en</w> <w n="12.6">haut</w>, <w n="12.7">coup</w> <w n="12.8">d</w>’<w n="12.9">œil</w> <w n="12.10">inespéré</w> !</l>
						<l n="13" num="1.13"><w n="13.1">L</w>’<w n="13.2">on</w> <w n="13.3">aperçoit</w> <w n="13.4">là</w>-<w n="13.5">bas</w>, <w n="13.6">dans</w> <w n="13.7">le</w> <w n="13.8">bleu</w> <w n="13.9">de</w> <w n="13.10">la</w> <w n="13.11">plaine</w>,</l>
						<l n="14" num="1.14"><w n="14.1">L</w>’<w n="14.2">église</w> <w n="14.3">où</w> <w n="14.4">dort</w> <w n="14.5">le</w> <w n="14.6">Cid</w> <w n="14.7">près</w> <w n="14.8">de</w> <w n="14.9">doña</w> <w n="14.10">Chimène</w> !</l>
					</lg>
					<closer>
						<dateline>
						<placeName>Cartuja de Miraflores</placeName>,
							<date not-before="1839" not-after="1849">184.</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>