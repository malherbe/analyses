<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ESPAÑA</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1374 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_7</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Second</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://gutenberg.org/files/45886/45886-h/45886-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Second</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1890">1890</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1845">1845</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">ESPAÑA</head><div type="poem" key="GAU257">
					<head type="main">SÉGUIDILLE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Un</w> <w n="1.2">jupon</w> <w n="1.3">serré</w> <w n="1.4">sur</w> <w n="1.5">les</w> <w n="1.6">hanches</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Un</w> <w n="2.2">peigne</w> <w n="2.3">énorme</w> <w n="2.4">à</w> <w n="2.5">son</w> <w n="2.6">chignon</w></l>
						<l n="3" num="1.3"><w n="3.1">Jambe</w> <w n="3.2">nerveuse</w> <w n="3.3">et</w> <w n="3.4">pied</w> <w n="3.5">mignon</w>.</l>
						<l n="4" num="1.4"><w n="4.1">œil</w> <w n="4.2">de</w> <w n="4.3">feu</w>, <w n="4.4">teint</w> <w n="4.5">pâle</w> <w n="4.6">et</w> <w n="4.7">dents</w> <w n="4.8">blanches</w>.</l>
						<l n="5" num="1.5"><space quantity="12" unit="char"></space><w n="5.1">Alza</w> ! <w n="5.2">olà</w> !</l>
						<l n="6" num="1.6"><space quantity="18" unit="char"></space><w n="6.1">Voilà</w></l>
						<l n="7" num="1.7"><space quantity="8" unit="char"></space><w n="7.1">La</w> <w n="7.2">véritable</w> <w n="7.3">Manola</w>.</l>
					</lg>
					<lg n="2">
						<l n="8" num="2.1"><w n="8.1">Gestes</w> <w n="8.2">hardis</w>, <w n="8.3">libre</w> <w n="8.4">parole</w>,</l>
						<l n="9" num="2.2"><w n="9.1">Sel</w> <w n="9.2">et</w> <w n="9.3">piment</w> <w n="9.4">à</w> <w n="9.5">pleine</w> <w n="9.6">main</w>,</l>
						<l n="10" num="2.3"><w n="10.1">Oubli</w> <w n="10.2">parfait</w> <w n="10.3">du</w> <w n="10.4">lendemain</w>,</l>
						<l n="11" num="2.4"><w n="11.1">Amour</w> <w n="11.2">fantasque</w> <w n="11.3">et</w> <w n="11.4">grâce</w> <w n="11.5">folle</w>,</l>
						<l n="12" num="2.5"><space quantity="12" unit="char"></space><w n="12.1">Alza</w> ! <w n="12.2">olà</w> !</l>
						<l n="13" num="2.6"><space quantity="18" unit="char"></space><w n="13.1">Voilà</w></l>
						<l n="14" num="2.7"><space quantity="8" unit="char"></space><w n="14.1">La</w> <w n="14.2">véritable</w> <w n="14.3">Manola</w>.</l>
					</lg>
					<lg n="3">
						<l n="15" num="3.1"><w n="15.1">Chanter</w>, <w n="15.2">danser</w> <w n="15.3">aux</w> <w n="15.4">castagnettes</w>,</l>
						<l n="16" num="3.2"><w n="16.1">Et</w>, <w n="16.2">dans</w> <w n="16.3">les</w> <w n="16.4">courses</w> <w n="16.5">de</w> <w n="16.6">taureaux</w>,</l>
						<l n="17" num="3.3"><w n="17.1">Juger</w> <w n="17.2">les</w> <w n="17.3">coups</w> <w n="17.4">des</w> <w n="17.5">toreros</w>,</l>
						<l n="18" num="3.4"><w n="18.1">Tout</w> <w n="18.2">en</w> <w n="18.3">fumant</w> <w n="18.4">des</w> <w n="18.5">cigarettes</w> ;</l>
						<l n="19" num="3.5"><space quantity="12" unit="char"></space><w n="19.1">Alza</w> ! <w n="19.2">olà</w> !</l>
						<l n="20" num="3.6"><space quantity="18" unit="char"></space><w n="20.1">Voilà</w></l>
						<l n="21" num="3.7"><space quantity="8" unit="char"></space><w n="21.1">La</w> <w n="21.2">véritable</w> <w n="21.3">Manola</w>.</l>
					</lg>
					<closer>
						<dateline>
							<date when="1843">184.</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>