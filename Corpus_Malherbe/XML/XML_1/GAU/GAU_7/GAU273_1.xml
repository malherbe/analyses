<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ESPAÑA</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1374 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_7</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Second</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://gutenberg.org/files/45886/45886-h/45886-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Second</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1890">1890</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1845">1845</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">ESPAÑA</head><div type="poem" key="GAU273">
					<head type="main">J’AI DANS MON CŒUR</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">J</w>’<w n="1.2">ai</w> <w n="1.3">dans</w> <w n="1.4">mon</w> <w n="1.5">cœur</w>, <w n="1.6">dont</w> <w n="1.7">tout</w> <w n="1.8">voile</w> <w n="1.9">s</w>’<w n="1.10">écarte</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Deux</w> <w n="2.2">bancs</w> <w n="2.3">d</w>’<w n="2.4">ivoire</w>, <w n="2.5">une</w> <w n="2.6">table</w> <w n="2.7">en</w> <w n="2.8">cristal</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Où</w> <w n="3.2">sont</w> <w n="3.3">assis</w>, <w n="3.4">tenant</w> <w n="3.5">chacun</w> <w n="3.6">leur</w> <w n="3.7">carte</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Ton</w> <w n="4.2">faux</w> <w n="4.3">amour</w> <w n="4.4">et</w> <w n="4.5">mon</w> <w n="4.6">amour</w> <w n="4.7">loyal</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">J</w>’<w n="5.2">ai</w> <w n="5.3">dans</w> <w n="5.4">mon</w> <w n="5.5">cœur</w>, <w n="5.6">dans</w> <w n="5.7">mon</w> <w n="5.8">cœur</w> <w n="5.9">diaphane</w></l>
						<l n="6" num="2.2"><w n="6.1">Ton</w> <w n="6.2">nom</w> <w n="6.3">chéri</w> <w n="6.4">qu</w>’<w n="6.5">enferme</w> <w n="6.6">un</w> <w n="6.7">coffret</w> <w n="6.8">d</w>’<w n="6.9">or</w> ;</l>
						<l n="7" num="2.3"><w n="7.1">Prends</w>-<w n="7.2">en</w> <w n="7.3">la</w> <w n="7.4">clef</w>, <w n="7.5">car</w> <w n="7.6">nulle</w> <w n="7.7">main</w> <w n="7.8">profane</w></l>
						<l n="8" num="2.4"><w n="8.1">Ne</w> <w n="8.2">doit</w> <w n="8.3">l</w>’<w n="8.4">ouvrir</w> <w n="8.5">ni</w> <w n="8.6">ne</w> <w n="8.7">l</w>’<w n="8.8">ouvrit</w> <w n="8.9">encor</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Fouille</w> <w n="9.2">mon</w> <w n="9.3">cœur</w>, <w n="9.4">ce</w> <w n="9.5">cœur</w> <w n="9.6">que</w> <w n="9.7">tu</w> <w n="9.8">dédaignes</w></l>
						<l n="10" num="3.2"><w n="10.1">Et</w> <w n="10.2">qui</w> <w n="10.3">pourtant</w> <w n="10.4">n</w>’<w n="10.5">est</w> <w n="10.6">peuplé</w> <w n="10.7">que</w> <w n="10.8">de</w> <w n="10.9">toi</w>,</l>
						<l n="11" num="3.3"><w n="11.1">Et</w> <w n="11.2">tu</w> <w n="11.3">verras</w>, <w n="11.4">mon</w> <w n="11.5">amour</w>, <w n="11.6">que</w> <w n="11.7">tu</w> <w n="11.8">règnes</w></l>
						<l n="12" num="3.4"><w n="12.1">Sur</w> <w n="12.2">un</w> <w n="12.3">pays</w> <w n="12.4">dont</w> <w n="12.5">nul</w> <w n="12.6">homme</w> <w n="12.7">n</w>’<w n="12.8">est</w> <w n="12.9">roi</w> !</l>
					</lg>
					<closer>
						<dateline>
						<placeName>Grenade</placeName>,
							<date not-before="1839" not-after="1849">184.</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>