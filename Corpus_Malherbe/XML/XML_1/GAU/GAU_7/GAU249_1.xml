<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ESPAÑA</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1374 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_7</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Second</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://gutenberg.org/files/45886/45886-h/45886-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Second</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1890">1890</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1845">1845</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">ESPAÑA</head><div type="poem" key="GAU249">
					<head type="main">SAINTE CASILDA</head>
					<head type="form">SONNET</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">A</w> <w n="1.2">Burgos</w>, <w n="1.3">dans</w> <w n="1.4">un</w> <w n="1.5">coin</w> <w n="1.6">de</w> <w n="1.7">l</w>’<w n="1.8">église</w> <w n="1.9">déserte</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Un</w> <w n="2.2">tableau</w> <w n="2.3">me</w> <w n="2.4">surprit</w> <w n="2.5">par</w> <w n="2.6">son</w> <w n="2.7">effet</w> <w n="2.8">puissant</w> :</l>
						<l n="3" num="1.3"><w n="3.1">Un</w> <w n="3.2">ange</w>, <w n="3.3">pâle</w> <w n="3.4">et</w> <w n="3.5">fier</w>, <w n="3.6">d</w>’<w n="3.7">un</w> <w n="3.8">ciel</w> <w n="3.9">fauve</w> <w n="3.10">descend</w>,</l>
						<l n="4" num="1.4"><w n="4.1">A</w> <w n="4.2">sainte</w> <w n="4.3">Casilda</w> <w n="4.4">portant</w> <w n="4.5">la</w> <w n="4.6">palme</w> <w n="4.7">verte</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Pour</w> <w n="5.2">l</w>’<w n="5.3">œuvre</w> <w n="5.4">des</w> <w n="5.5">bourreaux</w> <w n="5.6">la</w> <w n="5.7">vierge</w> <w n="5.8">découverte</w></l>
						<l n="6" num="2.2"><w n="6.1">Montre</w> <w n="6.2">sur</w> <w n="6.3">sa</w> <w n="6.4">poitrine</w>, <w n="6.5">albâtre</w> <w n="6.6">éblouissant</w>,</l>
						<l n="7" num="2.3"><w n="7.1">A</w> <w n="7.2">la</w> <w n="7.3">place</w> <w n="7.4">des</w> <w n="7.5">seins</w>, <w n="7.6">deux</w> <w n="7.7">ronds</w> <w n="7.8">couleur</w> <w n="7.9">de</w> <w n="7.10">sang</w>,</l>
						<l n="8" num="2.4"><w n="8.1">Distillant</w> <w n="8.2">un</w> <w n="8.3">rubis</w> <w n="8.4">par</w> <w n="8.5">chaque</w> <w n="8.6">veine</w> <w n="8.7">ouverte</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Et</w> <w n="9.2">les</w> <w n="9.3">seins</w> <w n="9.4">déjà</w> <w n="9.5">morts</w>, <w n="9.6">beaux</w> <w n="9.7">lis</w> <w n="9.8">coupés</w> <w n="9.9">en</w> <w n="9.10">fleur</w>,</l>
						<l n="10" num="3.2"><w n="10.1">Blancs</w> <w n="10.2">comme</w> <w n="10.3">les</w> <w n="10.4">morceaux</w> <w n="10.5">d</w>’<w n="10.6">une</w> <w n="10.7">Vénus</w> <w n="10.8">de</w> <w n="10.9">marbre</w>,</l>
						<l n="11" num="3.3"><w n="11.1">Dans</w> <w n="11.2">un</w> <w n="11.3">bassin</w> <w n="11.4">d</w>’<w n="11.5">argent</w> <w n="11.6">gisent</w> <w n="11.7">au</w> <w n="11.8">pied</w> <w n="11.9">d</w>’<w n="11.10">un</w> <w n="11.11">arbre</w>.</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1"><w n="12.1">Mais</w> <w n="12.2">la</w> <w n="12.3">sainte</w> <w n="12.4">en</w> <w n="12.5">extase</w>, <w n="12.6">oubliant</w> <w n="12.7">sa</w> <w n="12.8">douleur</w>,</l>
						<l n="13" num="4.2"><w n="13.1">Comme</w> <w n="13.2">aux</w> <w n="13.3">bras</w> <w n="13.4">d</w>’<w n="13.5">un</w> <w n="13.6">amant</w>, <w n="13.7">de</w> <w n="13.8">volupté</w> <w n="13.9">se</w> <w n="13.10">pâme</w>,</l>
						<l n="14" num="4.3"><w n="14.1">Car</w> <w n="14.2">aux</w> <w n="14.3">lèvres</w> <w n="14.4">du</w> <w n="14.5">Christ</w> <w n="14.6">elle</w> <w n="14.7">suspend</w> <w n="14.8">son</w> <w n="14.9">âme</w> !</l>
					</lg>
					<closer>
						<placeName>Burgos</placeName>.
					</closer>
				</div></body></text></TEI>