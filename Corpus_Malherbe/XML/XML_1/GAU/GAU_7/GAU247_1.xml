<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ESPAÑA</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1374 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_7</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Second</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://gutenberg.org/files/45886/45886-h/45886-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Second</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1890">1890</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1845">1845</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">ESPAÑA</head><div type="poem" key="GAU247">
					<head type="main">L’HORLOGE</head>
					<opener>
						<epigraph>
							<cit>
								<quote>Vulnerant omnes, ultima necat.</quote>
							</cit>
						</epigraph>
					</opener>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">La</w> <w n="1.2">voiture</w> <w n="1.3">fit</w> <w n="1.4">halte</w> <w n="1.5">à</w> <w n="1.6">l</w>’<w n="1.7">église</w> <w n="1.8">d</w>’<w n="1.9">Urrugne</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Nom</w> <w n="2.2">rauque</w>, <w n="2.3">dont</w> <w n="2.4">le</w> <w n="2.5">son</w> <w n="2.6">à</w> <w n="2.7">la</w> <w n="2.8">rime</w> <w n="2.9">répugne</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Mais</w> <w n="3.2">qui</w> <w n="3.3">n</w>’<w n="3.4">en</w> <w n="3.5">est</w> <w n="3.6">pas</w> <w n="3.7">moins</w> <w n="3.8">un</w> <w n="3.9">village</w> <w n="3.10">charmant</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Sur</w> <w n="4.2">un</w> <w n="4.3">sol</w> <w n="4.4">montueux</w> <w n="4.5">perché</w> <w n="4.6">bizarrement</w>.</l>
						<l n="5" num="1.5"><w n="5.1">C</w>’<w n="5.2">est</w> <w n="5.3">un</w> <w n="5.4">bâtiment</w> <w n="5.5">pauvre</w>, <w n="5.6">en</w> <w n="5.7">grosses</w> <w n="5.8">pierres</w> <w n="5.9">grises</w>,</l>
						<l n="6" num="1.6"><w n="6.1">Sans</w> <w n="6.2">archanges</w> <w n="6.3">sculptés</w>, <w n="6.4">sans</w> <w n="6.5">nervures</w> <w n="6.6">ni</w> <w n="6.7">frises</w>,</l>
						<l n="7" num="1.7"><w n="7.1">Qui</w> <w n="7.2">n</w>’<w n="7.3">a</w> <w n="7.4">pour</w> <w n="7.5">ornement</w> <w n="7.6">que</w> <w n="7.7">le</w> <w n="7.8">fer</w> <w n="7.9">de</w> <w n="7.10">sa</w> <w n="7.11">croix</w>,</l>
						<l n="8" num="1.8"><w n="8.1">Une</w> <w n="8.2">horloge</w> <w n="8.3">rustique</w> <w n="8.4">et</w> <w n="8.5">son</w> <w n="8.6">cadran</w> <w n="8.7">de</w> <w n="8.8">bois</w>,</l>
						<l n="9" num="1.9"><w n="9.1">Dont</w> <w n="9.2">les</w> <w n="9.3">chiffres</w> <w n="9.4">romains</w>, <w n="9.5">épongés</w> <w n="9.6">par</w> <w n="9.7">la</w> <w n="9.8">pluie</w>,</l>
						<l n="10" num="1.10"><w n="10.1">Ont</w> <w n="10.2">coulé</w> <w n="10.3">sur</w> <w n="10.4">le</w> <w n="10.5">fond</w> <w n="10.6">que</w> <w n="10.7">nul</w> <w n="10.8">pinceau</w> <w n="10.9">n</w>’<w n="10.10">essuie</w>.</l>
						<l n="11" num="1.11"><w n="11.1">Mais</w> <w n="11.2">sur</w> <w n="11.3">l</w>’<w n="11.4">humble</w> <w n="11.5">cadran</w> <w n="11.6">regardé</w> <w n="11.7">par</w> <w n="11.8">hasard</w>,</l>
						<l n="12" num="1.12"><w n="12.1">Comme</w> <w n="12.2">les</w> <w n="12.3">mots</w> <w n="12.4">de</w> <w n="12.5">flamme</w> <w n="12.6">aux</w> <w n="12.7">murs</w> <w n="12.8">de</w> <w n="12.9">Balthazar</w>,</l>
						<l n="13" num="1.13"><w n="13.1">Comme</w> <w n="13.2">l</w>’<w n="13.3">inscription</w> <w n="13.4">de</w> <w n="13.5">la</w> <w n="13.6">porte</w> <w n="13.7">maudite</w>,</l>
						<l n="14" num="1.14"><w n="14.1">En</w> <w n="14.2">caractères</w> <w n="14.3">noirs</w> <w n="14.4">une</w> <w n="14.5">phrase</w> <w n="14.6">est</w> <w n="14.7">écrite</w> ;</l>
						<l n="15" num="1.15"><w n="15.1">Quatre</w> <w n="15.2">mots</w> <w n="15.3">solennels</w>, <w n="15.4">quatre</w> <w n="15.5">mots</w> <w n="15.6">de</w> <w n="15.7">latin</w>,</l>
						<l n="16" num="1.16"><w n="16.1">Où</w> <w n="16.2">tout</w> <w n="16.3">homme</w> <w n="16.4">en</w> <w n="16.5">passant</w> <w n="16.6">peut</w> <w n="16.7">lire</w> <w n="16.8">son</w> <w n="16.9">destin</w> :</l>
						<l n="17" num="1.17">«<w n="17.1">Chaque</w> <w n="17.2">heure</w> <w n="17.3">fait</w> <w n="17.4">sa</w> <w n="17.5">plaie</w> <w n="17.6">et</w> <w n="17.7">la</w> <w n="17.8">dernière</w> <w n="17.9">achève</w> !»</l>
					</lg>
					<lg n="2">
						<l n="18" num="2.1"><w n="18.1">Oui</w>, <w n="18.2">c</w>’<w n="18.3">est</w> <w n="18.4">bien</w> <w n="18.5">vrai</w>, <w n="18.6">la</w> <w n="18.7">vie</w> <w n="18.8">est</w> <w n="18.9">un</w> <w n="18.10">combat</w> <w n="18.11">sans</w> <w n="18.12">trêve</w>,</l>
						<l n="19" num="2.2"><w n="19.1">Un</w> <w n="19.2">combat</w> <w n="19.3">inégal</w> <w n="19.4">contre</w> <w n="19.5">un</w> <w n="19.6">lutteur</w> <w n="19.7">caché</w>,</l>
						<l n="20" num="2.3"><w n="20.1">Qui</w> <w n="20.2">d</w>’<w n="20.3">aucun</w> <w n="20.4">de</w> <w n="20.5">nos</w> <w n="20.6">coups</w> <w n="20.7">ne</w> <w n="20.8">peut</w>-<w n="20.9">être</w> <w n="20.10">touché</w> ;</l>
						<l n="21" num="2.4"><w n="21.1">Et</w> <w n="21.2">dans</w> <w n="21.3">nos</w> <w n="21.4">cœurs</w> <w n="21.5">criblés</w>, <w n="21.6">comme</w> <w n="21.7">dans</w> <w n="21.8">une</w> <w n="21.9">cible</w>,</l>
						<l n="22" num="2.5"><w n="22.1">Tremblent</w> <w n="22.2">les</w> <w n="22.3">traits</w> <w n="22.4">lancés</w> <w n="22.5">par</w> <w n="22.6">l</w>’<w n="22.7">archer</w> <w n="22.8">invisible</w>.</l>
						<l n="23" num="2.6"><w n="23.1">Nous</w> <w n="23.2">sommes</w> <w n="23.3">condamnés</w>, <w n="23.4">nous</w> <w n="23.5">devons</w> <w n="23.6">tous</w> <w n="23.7">périr</w> ;</l>
						<l n="24" num="2.7"><w n="24.1">Naître</w>, <w n="24.2">c</w>’<w n="24.3">est</w> <w n="24.4">seulement</w> <w n="24.5">commencer</w> <w n="24.6">à</w> <w n="24.7">mourir</w>,</l>
						<l n="25" num="2.8"><w n="25.1">Et</w> <w n="25.2">l</w>’<w n="25.3">enfant</w>, <w n="25.4">hier</w> <w n="25.5">encor</w> <w n="25.6">chérubin</w> <w n="25.7">chez</w> <w n="25.8">les</w> <w n="25.9">anges</w>,</l>
						<l n="26" num="2.9"><w n="26.1">Par</w> <w n="26.2">le</w> <w n="26.3">ver</w> <w n="26.4">du</w> <w n="26.5">linceul</w> <w n="26.6">est</w> <w n="26.7">piqué</w> <w n="26.8">sous</w> <w n="26.9">ses</w> <w n="26.10">langes</w>.</l>
						<l n="27" num="2.10"><w n="27.1">Le</w> <w n="27.2">disque</w> <w n="27.3">de</w> <w n="27.4">l</w>’<w n="27.5">horloge</w> <w n="27.6">est</w> <w n="27.7">le</w> <w n="27.8">champ</w> <w n="27.9">du</w> <w n="27.10">combat</w>,</l>
						<l n="28" num="2.11"><w n="28.1">Où</w> <w n="28.2">la</w> <w n="28.3">Mort</w> <w n="28.4">de</w> <w n="28.5">sa</w> <w n="28.6">faux</w> <w n="28.7">par</w> <w n="28.8">milliers</w> <w n="28.9">nous</w> <w n="28.10">abat</w> ;</l>
						<l n="29" num="2.12"><w n="29.1">La</w> <w n="29.2">Mort</w>, <w n="29.3">rude</w> <w n="29.4">jouteur</w> <w n="29.5">qui</w> <w n="29.6">suffit</w> <w n="29.7">pour</w> <w n="29.8">défendre</w></l>
						<l n="30" num="2.13"><w n="30.1">L</w>’<w n="30.2">éternité</w> <w n="30.3">de</w> <w n="30.4">Dieu</w>, <w n="30.5">qu</w>’<w n="30.6">on</w> <w n="30.7">voudrait</w> <w n="30.8">bien</w> <w n="30.9">lui</w> <w n="30.10">prendre</w>.</l>
						<l n="31" num="2.14"><w n="31.1">Sur</w> <w n="31.2">le</w> <w n="31.3">grand</w> <w n="31.4">cheval</w> <w n="31.5">pâle</w>, <w n="31.6">entrevu</w> <w n="31.7">par</w> <w n="31.8">saint</w> <w n="31.9">Jean</w>,</l>
						<l n="32" num="2.15"><w n="32.1">Les</w> <w n="32.2">Heures</w>, <w n="32.3">sans</w> <w n="32.4">repos</w>, <w n="32.5">parcourent</w> <w n="32.6">le</w> <w n="32.7">cadran</w> ;</l>
						<l n="33" num="2.16"><w n="33.1">Comme</w> <w n="33.2">ces</w> <w n="33.3">inconnus</w> <w n="33.4">des</w> <w n="33.5">chants</w> <w n="33.6">du</w> <w n="33.7">moyen</w> <w n="33.8">âge</w>,</l>
						<l n="34" num="2.17"><w n="34.1">Leurs</w> <w n="34.2">casques</w> <w n="34.3">sont</w> <w n="34.4">fermés</w> <w n="34.5">sur</w> <w n="34.6">leur</w> <w n="34.7">sombre</w> <w n="34.8">visage</w>,</l>
						<l n="35" num="2.18"><w n="35.1">Et</w> <w n="35.2">leurs</w> <w n="35.3">armes</w> <w n="35.4">d</w>’<w n="35.5">acier</w> <w n="35.6">deviennent</w> <w n="35.7">tour</w> <w n="35.8">à</w> <w n="35.9">tour</w></l>
						<l n="36" num="2.19"><w n="36.1">Noires</w> <w n="36.2">comme</w> <w n="36.3">la</w> <w n="36.4">nuit</w>, <w n="36.5">blanches</w> <w n="36.6">comme</w> <w n="36.7">le</w> <w n="36.8">jour</w>.</l>
						<l n="37" num="2.20"><w n="37.1">Chaque</w> <w n="37.2">sœur</w> <w n="37.3">à</w> <w n="37.4">l</w>’<w n="37.5">appel</w> <w n="37.6">de</w> <w n="37.7">la</w> <w n="37.8">cloche</w> <w n="37.9">s</w>’<w n="37.10">élance</w>,</l>
						<l n="38" num="2.21"><w n="38.1">Prend</w> <w n="38.2">aussitôt</w> <w n="38.3">l</w>’<w n="38.4">aiguille</w> <w n="38.5">ouvrée</w> <w n="38.6">en</w> <w n="38.7">fer</w> <w n="38.8">de</w> <w n="38.9">lance</w>,</l>
						<l n="39" num="2.22"><w n="39.1">Et</w> <w n="39.2">toutes</w>, <w n="39.3">sans</w> <w n="39.4">pitié</w>, <w n="39.5">nous</w> <w n="39.6">piquent</w> <w n="39.7">en</w> <w n="39.8">passant</w>,</l>
						<l n="40" num="2.23"><w n="40.1">Pour</w> <w n="40.2">nous</w> <w n="40.3">tirer</w> <w n="40.4">du</w> <w n="40.5">cœur</w> <w n="40.6">une</w> <w n="40.7">perle</w> <w n="40.8">de</w> <w n="40.9">sang</w>,</l>
						<l n="41" num="2.24"><w n="41.1">Jusqu</w>’<w n="41.2">au</w> <w n="41.3">jour</w> <w n="41.4">d</w>’<w n="41.5">épouvante</w> <w n="41.6">où</w> <w n="41.7">paraît</w> <w n="41.8">la</w> <w n="41.9">dernière</w></l>
						<l n="42" num="2.25"><w n="42.1">Avec</w> <w n="42.2">le</w> <w n="42.3">sablier</w> <w n="42.4">et</w> <w n="42.5">la</w> <w n="42.6">noire</w> <w n="42.7">bannière</w> ;</l>
						<l n="43" num="2.26"><w n="43.1">Celle</w> <w n="43.2">qu</w>’<w n="43.3">on</w> <w n="43.4">n</w>’<w n="43.5">attend</w> <w n="43.6">pas</w>, <w n="43.7">celle</w> <w n="43.8">qui</w> <w n="43.9">vient</w> <w n="43.10">toujours</w>,</l>
						<l n="44" num="2.27"><w n="44.1">Et</w> <w n="44.2">qui</w> <w n="44.3">se</w> <w n="44.4">met</w> <w n="44.5">en</w> <w n="44.6">marche</w> <w n="44.7">au</w> <w n="44.8">premier</w> <w n="44.9">de</w> <w n="44.10">nos</w> <w n="44.11">jours</w> !</l>
						<l n="45" num="2.28"><w n="45.1">Elle</w> <w n="45.2">va</w> <w n="45.3">droit</w> <w n="45.4">à</w> <w n="45.5">vous</w>, <w n="45.6">et</w>, <w n="45.7">d</w>’<w n="45.8">une</w> <w n="45.9">main</w> <w n="45.10">trop</w> <w n="45.11">sûre</w>,</l>
						<l n="46" num="2.29"><w n="46.1">Vous</w> <w n="46.2">porte</w> <w n="46.3">dans</w> <w n="46.4">le</w> <w n="46.5">flanc</w> <w n="46.6">la</w> <w n="46.7">suprême</w> <w n="46.8">blessure</w>,</l>
						<l n="47" num="2.30"><w n="47.1">Et</w> <w n="47.2">remonte</w> <w n="47.3">à</w> <w n="47.4">cheval</w>, <w n="47.5">après</w> <w n="47.6">avoir</w> <w n="47.7">jeté</w></l>
						<l n="48" num="2.31"><w n="48.1">Le</w> <w n="48.2">cadavre</w> <w n="48.3">au</w> <w n="48.4">néant</w>, <w n="48.5">l</w>’<w n="48.6">âme</w> <w n="48.7">à</w> <w n="48.8">l</w>’<w n="48.9">éternité</w> !</l>
					</lg>
					<closer>
						<dateline>
						<placeName>Urrugne</placeName>,
							<date not-before="1839" not-after="1849">184.</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>