<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ESPAÑA</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1374 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_7</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Second</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://gutenberg.org/files/45886/45886-h/45886-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Second</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1890">1890</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1845">1845</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">ESPAÑA</head><div type="poem" key="GAU282">
					<head type="main">PERSPECTIVE</head>
					<head type="form">SONNET</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Sur</w> <w n="1.2">le</w> <w n="1.3">Guadalquivir</w>, <w n="1.4">en</w> <w n="1.5">sortant</w> <w n="1.6">de</w> <w n="1.7">Séville</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Quand</w> <w n="2.2">l</w>’<w n="2.3">œil</w> <w n="2.4">à</w> <w n="2.5">l</w>’<w n="2.6">horizon</w> <w n="2.7">se</w> <w n="2.8">tourne</w> <w n="2.9">avec</w> <w n="2.10">regret</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Les</w> <w n="3.2">dômes</w>, <w n="3.3">les</w> <w n="3.4">clochers</w> <w n="3.5">font</w> <w n="3.6">comme</w> <w n="3.7">une</w> <w n="3.8">forêt</w> ;</l>
						<l n="4" num="1.4"><w n="4.1">A</w> <w n="4.2">chaque</w> <w n="4.3">tour</w> <w n="4.4">de</w> <w n="4.5">roue</w> <w n="4.6">il</w> <w n="4.7">surgit</w> <w n="4.8">une</w> <w n="4.9">aiguille</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">D</w>’<w n="5.2">abord</w> <w n="5.3">la</w> <w n="5.4">Giralda</w>, <w n="5.5">dont</w> <w n="5.6">l</w>’<w n="5.7">ange</w> <w n="5.8">d</w>’<w n="5.9">or</w> <w n="5.10">scintille</w>,</l>
						<l n="6" num="2.2"><w n="6.1">Rose</w> <w n="6.2">dans</w> <w n="6.3">le</w> <w n="6.4">ciel</w> <w n="6.5">bleu</w>, <w n="6.6">darde</w> <w n="6.7">son</w> <w n="6.8">minaret</w> ;</l>
						<l n="7" num="2.3"><w n="7.1">La</w> <w n="7.2">cathédrale</w> <w n="7.3">énorme</w> <w n="7.4">à</w> <w n="7.5">son</w> <w n="7.6">tour</w> <w n="7.7">apparaît</w></l>
						<l n="8" num="2.4"><w n="8.1">Par</w>-<w n="8.2">dessus</w> <w n="8.3">les</w> <w n="8.4">maisons</w>, <w n="8.5">qui</w> <w n="8.6">vont</w> <w n="8.7">à</w> <w n="8.8">sa</w> <w n="8.9">cheville</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">De</w> <w n="9.2">près</w>, <w n="9.3">l</w>’<w n="9.4">on</w> <w n="9.5">n</w>’<w n="9.6">aperçoit</w> <w n="9.7">que</w> <w n="9.8">des</w> <w n="9.9">fragments</w> <w n="9.10">d</w>’<w n="9.11">arceaux</w></l>
						<l n="10" num="3.2"><w n="10.1">Un</w> <w n="10.2">pignon</w> <w n="10.3">biscornu</w>, <w n="10.4">l</w>’<w n="10.5">angle</w> <w n="10.6">d</w>’<w n="10.7">un</w> <w n="10.8">mur</w> <w n="10.9">maussade</w></l>
						<l n="11" num="3.3"><w n="11.1">Cache</w> <w n="11.2">la</w> <w n="11.3">flèche</w> <w n="11.4">ouvrée</w> <w n="11.5">et</w> <w n="11.6">la</w> <w n="11.7">riche</w> <w n="11.8">façade</w>.</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1"><w n="12.1">Grands</w> <w n="12.2">hommes</w>, <w n="12.3">obstrués</w> <w n="12.4">et</w> <w n="12.5">masqués</w> <w n="12.6">par</w> <w n="12.7">les</w> <w n="12.8">sots</w>,</l>
						<l n="13" num="4.2"><w n="13.1">Comme</w> <w n="13.2">les</w> <w n="13.3">hautes</w> <w n="13.4">tours</w> <w n="13.5">par</w> <w n="13.6">les</w> <w n="13.7">toits</w> <w n="13.8">de</w> <w n="13.9">la</w> <w n="13.10">ville</w>,</l>
						<l n="14" num="4.3"><w n="14.1">De</w> <w n="14.2">loin</w> <w n="14.3">vos</w> <w n="14.4">fronts</w> <w n="14.5">grandis</w> <w n="14.6">montent</w> <w n="14.7">dans</w> <w n="14.8">l</w>’<w n="14.9">air</w> <w n="14.10">tranquille</w> !</l>
					</lg>
					<closer>
						<dateline>
						<placeName>Sur le Guadalquivir</placeName>,
							<date when="1844">184.</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>