<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ESPAÑA</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1374 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_7</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Second</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://gutenberg.org/files/45886/45886-h/45886-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Second</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1890">1890</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1845">1845</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">ESPAÑA</head><div type="poem" key="GAU266">
					<head type="main">LES TROIS GRACES DE GRENADE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">A</w> <w n="1.2">vous</w>, <w n="1.3">Martirio</w>, <w n="1.4">Dolorès</w>, <w n="1.5">Gracia</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Sœurs</w> <w n="2.2">de</w> <w n="2.3">beauté</w>, <w n="2.4">bouquet</w> <w n="2.5">de</w> <w n="2.6">la</w> <hi rend="ital"><w n="2.7">tertulia</w></hi>,</l>
						<l n="3" num="1.3"><w n="3.1">Que</w> <w n="3.2">tout</w> <w n="3.3">fin</w> <w n="3.4">cavalier</w> <w n="3.5">nomme</w> <w n="3.6">à</w> <w n="3.7">la</w> <w n="3.8">promenade</w></l>
						<l n="4" num="1.4"><w n="4.1">Les</w> <w n="4.2">Nymphes</w> <w n="4.3">du</w> <w n="4.4">Jénil</w>, <w n="4.5">les</w> <w n="4.6">perles</w> <w n="4.7">de</w> <w n="4.8">Grenade</w>,</l>
						<l n="5" num="1.5"><w n="5.1">A</w> <w n="5.2">vous</w> <w n="5.3">ces</w> <w n="5.4">vers</w> <w n="5.5">écrits</w> <w n="5.6">en</w> <w n="5.7">langage</w> <w n="5.8">inconnu</w></l>
						<l n="6" num="1.6"><w n="6.1">Par</w> <w n="6.2">l</w>’<w n="6.3">étranger</w> <w n="6.4">de</w> <w n="6.5">France</w> <w n="6.6">à</w> <w n="6.7">l</w>’<w n="6.8">Alhambra</w> <w n="6.9">venu</w>,</l>
						<l n="7" num="1.7"><w n="7.1">Où</w> <w n="7.2">votre</w> <w n="7.3">nom</w>, <w n="7.4">seul</w> <w n="7.5">mot</w> <w n="7.6">que</w> <w n="7.7">vous</w> <w n="7.8">y</w> <w n="7.9">saurez</w> <w n="7.10">lire</w>,</l>
						<l n="8" num="1.8"><w n="8.1">Attirera</w> <w n="8.2">vos</w> <w n="8.3">yeux</w> <w n="8.4">et</w> <w n="8.5">vous</w> <w n="8.6">fera</w> <w n="8.7">sourire</w>,</l>
						<l n="9" num="1.9"><w n="9.1">Si</w>, <w n="9.2">franchissant</w> <w n="9.3">flots</w> <w n="9.4">bleus</w> <w n="9.5">et</w> <w n="9.6">monts</w> <w n="9.7">aux</w> <w n="9.8">blonds</w> <w n="9.9">sommets</w>,</l>
						<l n="10" num="1.10"><w n="10.1">Ce</w> <w n="10.2">livre</w> <w n="10.3">jusqu</w>’<w n="10.4">à</w> <w n="10.5">vous</w> <w n="10.6">peut</w> <w n="10.7">arriver</w> <w n="10.8">jamais</w>.</l>
					</lg>
					<lg n="2">
						<l n="11" num="2.1"><w n="11.1">Douce</w> <w n="11.2">Martirio</w>, <w n="11.3">je</w> <w n="11.4">crois</w> <w n="11.5">te</w> <w n="11.6">voir</w> <w n="11.7">encore</w>,</l>
						<l n="12" num="2.2"><w n="12.1">Fraîche</w> <w n="12.2">à</w> <w n="12.3">faire</w> <w n="12.4">jaunir</w> <w n="12.5">les</w> <w n="12.6">roses</w> <w n="12.7">de</w> <w n="12.8">l</w>’<w n="12.9">aurore</w>,</l>
						<l n="13" num="2.3"><w n="13.1">Dans</w> <w n="13.2">ton</w> <w n="13.3">éclat</w> <w n="13.4">vermeil</w>, <w n="13.5">dans</w> <w n="13.6">ta</w> <w n="13.7">fleur</w> <w n="13.8">de</w> <w n="13.9">beauté</w>,</l>
						<l n="14" num="2.4"><w n="14.1">Comme</w> <w n="14.2">une</w> <w n="14.3">pêche</w> <w n="14.4">intacte</w> <w n="14.5">au</w> <w n="14.6">duvet</w> <w n="14.7">velouté</w>,</l>
						<l n="15" num="2.5"><w n="15.1">Avec</w> <w n="15.2">tes</w> <w n="15.3">yeux</w> <w n="15.4">nacrés</w>, <w n="15.5">ciel</w> <w n="15.6">aux</w> <w n="15.7">astres</w> <w n="15.8">d</w>’<w n="15.9">ébène</w>,</l>
						<l n="16" num="2.6"><w n="16.1">Et</w> <w n="16.2">ta</w> <w n="16.3">bouche</w> <w n="16.4">d</w>’<w n="16.5">œillet</w> <w n="16.6">épanouie</w> <w n="16.7">à</w> <w n="16.8">peine</w>,</l>
						<l n="17" num="2.7"><w n="17.1">Si</w> <w n="17.2">petite</w> <w n="17.3">vraiment</w> <w n="17.4">qu</w>’<w n="17.5">on</w> <w n="17.6">n</w>’<w n="17.7">y</w> <w n="17.8">saurait</w> <w n="17.9">poser</w>,</l>
						<l n="18" num="2.8"><w n="18.1">Même</w> <w n="18.2">quand</w> <w n="18.3">elle</w> <w n="18.4">rit</w>, <w n="18.5">que</w> <w n="18.6">le</w> <w n="18.7">quart</w> <w n="18.8">d</w>’<w n="18.9">un</w> <w n="18.10">baiser</w>.</l>
						<l n="19" num="2.9"><w n="19.1">Je</w> <w n="19.2">te</w> <w n="19.3">vois</w> <w n="19.4">déployant</w> <w n="19.5">ta</w> <w n="19.6">chevelure</w> <w n="19.7">brune</w>,</l>
						<l n="20" num="2.10"><w n="20.1">Et</w> <w n="20.2">nous</w> <w n="20.3">questionnant</w> <w n="20.4">pour</w> <w n="20.5">savoir</w> <w n="20.6">si</w> <w n="20.7">quelqu</w>’<w n="20.8">une</w></l>
						<l n="21" num="2.11"><w n="21.1">Dans</w> <w n="21.2">notre</w> <w n="21.3">France</w> <w n="21.4">avait</w> <w n="21.5">les</w> <w n="21.6">cheveux</w> <w n="21.7">assez</w> <w n="21.8">longs</w>,</l>
						<l n="22" num="2.12"><w n="22.1">Pour</w> <w n="22.2">filer</w> <w n="22.3">d</w>’<w n="22.4">un</w> <w n="22.5">seul</w> <w n="22.6">jet</w> <w n="22.7">de</w> <w n="22.8">la</w> <w n="22.9">nuque</w> <w n="22.10">aux</w> <w n="22.11">talons</w>.</l>
						<l n="23" num="2.13"><w n="23.1">Et</w> <w n="23.2">toi</w> <w n="23.3">qui</w> <w n="23.4">demeurais</w>, <w n="23.5">ainsi</w> <w n="23.6">qu</w>’<w n="23.7">une</w> <w n="23.8">sultane</w>,</l>
						<l n="24" num="2.14"><w n="24.1">Dans</w> <w n="24.2">un</w> <w n="24.3">palais</w> <w n="24.4">moresque</w> <w n="24.5">aux</w> <w n="24.6">murs</w> <w n="24.7">de</w> <w n="24.8">filigrane</w>,</l>
						<l n="25" num="2.15"><w n="25.1">Dolorès</w>, <w n="25.2">belle</w> <w n="25.3">enfant</w> <w n="25.4">à</w> <w n="25.5">l</w>’<w n="25.6">œil</w> <w n="25.7">déjà</w> <w n="25.8">rêveur</w>,</l>
						<l n="26" num="2.16"><w n="26.1">Que</w> <w n="26.2">nous</w> <w n="26.3">reconduisions</w>, — <w n="26.4">ô</w> <w n="26.5">la</w> <w n="26.6">douce</w> <w n="26.7">faveur</w> !</l>
						<l n="27" num="2.17"><w n="27.1">Sans</w> <w n="27.2">duègne</w> <w n="27.3">revêche</w> <w n="27.4">et</w> <w n="27.5">sans</w> <w n="27.6">parents</w> <w n="27.7">moroses</w>,</l>
						<l n="28" num="2.18"><w n="28.1">Prés</w> <w n="28.2">du</w> <w n="28.3">Généralife</w> <w n="28.4">où</w> <w n="28.5">sont</w> <w n="28.6">les</w> <w n="28.7">lauriers</w>-<w n="28.8">roses</w>,</l>
						<l n="29" num="2.19"><w n="29.1">Te</w> <w n="29.2">souvient</w>-<w n="29.3">il</w> <w n="29.4">encor</w> <w n="29.5">de</w> <w n="29.6">ces</w> <w n="29.7">deux</w> <w n="29.8">étrangers</w></l>
						<l n="30" num="2.20"><w n="30.1">Qui</w> <w n="30.2">demandaient</w> <w n="30.3">toujours</w> <w n="30.4">à</w> <w n="30.5">voir</w> <w n="30.6">les</w> <w n="30.7">orangers</w>,</l>
						<l n="31" num="2.21"><w n="31.1">Les</w> <w n="31.2">boleros</w> <w n="31.3">dansés</w> <w n="31.4">au</w> <w n="31.5">son</w> <w n="31.6">des</w> <w n="31.7">séguidilles</w>,</l>
						<l n="32" num="2.22"><w n="32.1">Les</w> <w n="32.2">basquines</w> <w n="32.3">de</w> <w n="32.4">soie</w> <w n="32.5">et</w> <w n="32.6">les</w> <w n="32.7">noires</w> <w n="32.8">mantilles</w> ?</l>
						<l n="33" num="2.23"><w n="33.1">Nous</w> <w n="33.2">parlions</w> <w n="33.3">l</w>’<w n="33.4">espagnol</w> <w n="33.5">comme</w> <w n="33.6">toi</w> <w n="33.7">le</w> <w n="33.8">français</w>,</l>
						<l n="34" num="2.24"><w n="34.1">Nous</w> <w n="34.2">commencions</w> <w n="34.3">les</w> <w n="34.4">mots</w> <w n="34.5">et</w> <w n="34.6">tu</w> <w n="34.7">les</w> <w n="34.8">finissais</w>,</l>
						<l n="35" num="2.25"><w n="35.1">Et</w>, <w n="35.2">malgré</w> <w n="35.3">notre</w> <w n="35.4">accent</w> <w n="35.5">au</w> <w n="35.6">dur</w> <w n="35.7">jota</w> <w n="35.8">rebelle</w>,</l>
						<l n="36" num="2.26"><w n="36.1">Tu</w> <w n="36.2">comprenais</w> <w n="36.3">très</w>-<w n="36.4">bien</w> <w n="36.5">que</w> <w n="36.6">nous</w> <w n="36.7">te</w> <w n="36.8">trouvions</w> <w n="36.9">belle</w>.</l>
					</lg>
					<lg n="3">
						<l n="37" num="3.1"><w n="37.1">Quoiqu</w>’<w n="37.2">il</w> <w n="37.3">fît</w> <w n="37.4">nuit</w>, <w n="37.5">le</w> <w n="37.6">ciel</w> <w n="37.7">brillait</w> <w n="37.8">d</w>’<w n="37.9">un</w> <w n="37.10">éclat</w> <w n="37.11">pur</w>,</l>
						<l n="38" num="3.2"><w n="38.1">Cent</w> <w n="38.2">mille</w> <w n="38.3">astres</w>, <w n="38.4">fleurs</w> <w n="38.5">d</w>’<w n="38.6">or</w>, <w n="38.7">s</w>’<w n="38.8">entr</w>’<w n="38.9">ouvraient</w> <w n="38.10">dans</w> <w n="38.11">l</w>’<w n="38.12">azur</w></l>
						<l n="39" num="3.3"><w n="39.1">Et</w>, <w n="39.2">de</w> <w n="39.3">son</w> <w n="39.4">arc</w> <w n="39.5">d</w>’<w n="39.6">argent</w> <w n="39.7">courbant</w> <w n="39.8">les</w> <w n="39.9">cornes</w> <w n="39.10">blanches</w>,</l>
						<l n="40" num="3.4"><w n="40.1">La</w> <w n="40.2">lune</w> <w n="40.3">décochait</w> <w n="40.4">ses</w> <w n="40.5">flèches</w> <w n="40.6">sous</w> <w n="40.7">les</w> <w n="40.8">branches</w> ;</l>
						<l n="41" num="3.5"><w n="41.1">La</w> <w n="41.2">neige</w> <w n="41.3">virginale</w> <w n="41.4">et</w> <w n="41.5">qui</w> <w n="41.6">ne</w> <w n="41.7">fond</w> <w n="41.8">jamais</w></l>
						<l n="42" num="3.6"><w n="42.1">Scintillait</w> <w n="42.2">vaguement</w> <w n="42.3">sur</w> <w n="42.4">les</w> <w n="42.5">lointains</w> <w n="42.6">sommets</w>,</l>
						<l n="43" num="3.7"><w n="43.1">Et</w> <w n="43.2">du</w> <w n="43.3">ciel</w> <w n="43.4">transparent</w> <w n="43.5">tombait</w> <w n="43.6">un</w> <w n="43.7">jour</w> <w n="43.8">bleuâtre</w></l>
						<l n="44" num="3.8"><w n="44.1">Qui</w>, <w n="44.2">baignant</w> <w n="44.3">ton</w> <w n="44.4">front</w> <w n="44.5">pur</w> <w n="44.6">des</w> <w n="44.7">pâleurs</w> <w n="44.8">de</w> <w n="44.9">l</w>’<w n="44.10">albâtre</w>,</l>
						<l n="45" num="3.9"><w n="45.1">Te</w> <w n="45.2">faisait</w> <w n="45.3">ressembler</w> <w n="45.4">à</w> <w n="45.5">la</w> <w n="45.6">jeune</w> <w n="45.7">péri</w></l>
						<l n="46" num="3.10"><w n="46.1">Revenant</w> <w n="46.2">visiter</w> <w n="46.3">son</w> <w n="46.4">Alhambra</w> <w n="46.5">chéri</w>.</l>
					</lg>
					<lg n="4">
						<l n="47" num="4.1"><w n="47.1">Pour</w> <w n="47.2">toi</w> <w n="47.3">les</w> <w n="47.4">derniers</w> <w n="47.5">vers</w>, <w n="47.6">toi</w> <w n="47.7">que</w> <w n="47.8">j</w>’<w n="47.9">aurais</w> <w n="47.10">aimée</w>,</l>
						<l n="48" num="4.2"><w n="48.1">Gracia</w>, <w n="48.2">tendre</w> <w n="48.3">fleur</w> <w n="48.4">dont</w> <w n="48.5">mon</w> <w n="48.6">âme</w> <w n="48.7">charmée</w>,</l>
						<l n="49" num="4.3"><w n="49.1">Pour</w> <w n="49.2">l</w>’<w n="49.3">avoir</w> <w n="49.4">respirée</w> <w n="49.5">un</w> <w n="49.6">moment</w>, <w n="49.7">gardera</w></l>
						<l n="50" num="4.4"><w n="50.1">Un</w> <w n="50.2">long</w> <w n="50.3">ressouvenir</w> <w n="50.4">qui</w> <w n="50.5">la</w> <w n="50.6">parfumera</w>.</l>
						<l n="51" num="4.5"><w n="51.1">Comment</w> <w n="51.2">peindre</w> <w n="51.3">tes</w> <w n="51.4">yeux</w> <w n="51.5">aux</w> <w n="51.6">paupières</w> <w n="51.7">arquées</w>,</l>
						<l n="52" num="4.6"><w n="52.1">Tes</w> <w n="52.2">tempes</w> <w n="52.3">couleur</w> <w n="52.4">d</w>’<w n="52.5">or</w>, <w n="52.6">de</w> <w n="52.7">cheveux</w> <w n="52.8">noirs</w> <w n="52.9">plaquées</w>.</l>
						<l n="53" num="4.7"><w n="53.1">Ta</w> <w n="53.2">bouche</w> <w n="53.3">de</w> <w n="53.4">grenade</w> <w n="53.5">où</w> <w n="53.6">luit</w> <w n="53.7">le</w> <w n="53.8">feu</w> <w n="53.9">vermeil</w></l>
						<l n="54" num="4.8"><w n="54.1">Que</w> <w n="54.2">dans</w> <w n="54.3">le</w> <w n="54.4">sang</w> <w n="54.5">du</w> <w n="54.6">More</w> <w n="54.7">alluma</w> <w n="54.8">le</w> <w n="54.9">soleil</w> ?</l>
						<l n="55" num="4.9"><w n="55.1">L</w>’<w n="55.2">Orient</w> <w n="55.3">tout</w> <w n="55.4">entier</w> <w n="55.5">dans</w> <w n="55.6">tes</w> <w n="55.7">regards</w> <w n="55.8">rayonne</w>,</l>
						<l n="56" num="4.10"><w n="56.1">Et</w> <w n="56.2">bien</w> <w n="56.3">que</w> <w n="56.4">Gracia</w> <w n="56.5">soit</w> <w n="56.6">le</w> <w n="56.7">nom</w> <w n="56.8">qu</w>’<w n="56.9">on</w> <w n="56.10">te</w> <w n="56.11">donne</w>,</l>
						<l n="57" num="4.11"><w n="57.1">Et</w> <w n="57.2">que</w> <w n="57.3">jamais</w> <w n="57.4">objet</w> <w n="57.5">n</w>’<w n="57.6">ait</w> <w n="57.7">été</w> <w n="57.8">mieux</w> <w n="57.9">nommé</w>,</l>
						<l n="58" num="4.12"><w n="58.1">Tu</w> <w n="58.2">devrais</w> <w n="58.3">t</w>’<w n="58.4">appeler</w> <w n="58.5">Zoraïde</w> <w n="58.6">ou</w> <w n="58.7">Fatmé</w> !</l>
					</lg>
					<closer>
						<dateline>
						<placeName>Grenade</placeName>,
							<date when="1842">184.</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>