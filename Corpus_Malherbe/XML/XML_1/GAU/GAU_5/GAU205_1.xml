<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES DIVERSES, 1833-1838</title>
				<title type="sub_2">Tome premier</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3874 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Premier</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/files/44180/44180-h/44180-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome premier</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1838">1833-1838</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU205">
				<head type="main">CHANT DU GRILLON</head>
				<div type="section" n="1">
					<head type="number">I</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Souffle</w>, <w n="1.2">bise</w> ! <w n="1.3">tombe</w> <w n="1.4">à</w> <w n="1.5">flots</w>, <w n="1.6">pluie</w> !</l>
						<l n="2" num="1.2"><w n="2.1">Dans</w> <w n="2.2">mon</w> <w n="2.3">palais</w> <w n="2.4">tout</w> <w n="2.5">noir</w> <w n="2.6">de</w> <w n="2.7">suie</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Je</w> <w n="3.2">ris</w> <w n="3.3">de</w> <w n="3.4">la</w> <w n="3.5">pluie</w> <w n="3.6">et</w> <w n="3.7">du</w> <w n="3.8">vent</w> ;</l>
						<l n="4" num="1.4"><w n="4.1">En</w> <w n="4.2">attendant</w> <w n="4.3">que</w> <w n="4.4">l</w>’<w n="4.5">hiver</w> <w n="4.6">fuie</w>,</l>
						<l n="5" num="1.5"><w n="5.1">Je</w> <w n="5.2">reste</w> <w n="5.3">au</w> <w n="5.4">coin</w> <w n="5.5">du</w> <w n="5.6">feu</w>, <w n="5.7">rêvant</w>.</l>
					</lg>
					<lg n="2">
						<l n="6" num="2.1"><w n="6.1">C</w>’<w n="6.2">est</w> <w n="6.3">moi</w> <w n="6.4">qui</w> <w n="6.5">suis</w> <w n="6.6">l</w>’<w n="6.7">esprit</w> <w n="6.8">de</w> <w n="6.9">l</w>’<w n="6.10">âtre</w> !</l>
						<l n="7" num="2.2"><w n="7.1">Le</w> <w n="7.2">gaz</w>, <w n="7.3">de</w> <w n="7.4">sa</w> <w n="7.5">langue</w> <w n="7.6">bleuâtre</w>,</l>
						<l n="8" num="2.3"><w n="8.1">Lèche</w> <w n="8.2">plus</w> <w n="8.3">doucement</w> <w n="8.4">le</w> <w n="8.5">bois</w> ;</l>
						<l n="9" num="2.4"><w n="9.1">La</w> <w n="9.2">fumée</w>, <w n="9.3">en</w> <w n="9.4">filet</w> <w n="9.5">d</w>’<w n="9.6">albâtre</w>,</l>
						<l n="10" num="2.5"><w n="10.1">Monte</w> <w n="10.2">et</w> <w n="10.3">se</w> <w n="10.4">contourne</w> <w n="10.5">à</w> <w n="10.6">ma</w> <w n="10.7">voix</w>.</l>
					</lg>
					<lg n="3">
						<l n="11" num="3.1"><w n="11.1">La</w> <w n="11.2">bouilloire</w> <w n="11.3">rit</w> <w n="11.4">et</w> <w n="11.5">babille</w> ;</l>
						<l n="12" num="3.2"><w n="12.1">La</w> <w n="12.2">flamme</w> <w n="12.3">aux</w> <w n="12.4">pieds</w> <w n="12.5">d</w>’<w n="12.6">argent</w> <w n="12.7">sautille</w></l>
						<l n="13" num="3.3"><w n="13.1">En</w> <w n="13.2">accompagnant</w> <w n="13.3">ma</w> <w n="13.4">chanson</w> ;</l>
						<l n="14" num="3.4"><w n="14.1">La</w> <w n="14.2">bûche</w> <w n="14.3">de</w> <w n="14.4">duvet</w> <w n="14.5">s</w>’<w n="14.6">habille</w> ;</l>
						<l n="15" num="3.5"><w n="15.1">La</w> <w n="15.2">sève</w> <w n="15.3">bout</w> <w n="15.4">dans</w> <w n="15.5">le</w> <w n="15.6">tison</w>.</l>
					</lg>
					<lg n="4">
						<l n="16" num="4.1"><w n="16.1">Le</w> <w n="16.2">soufflet</w> <w n="16.3">au</w> <w n="16.4">râle</w> <w n="16.5">asthmatique</w></l>
						<l n="17" num="4.2"><w n="17.1">Me</w> <w n="17.2">fait</w> <w n="17.3">entendre</w> <w n="17.4">sa</w> <w n="17.5">musique</w> ;</l>
						<l n="18" num="4.3"><w n="18.1">Le</w> <w n="18.2">tourne</w>-<w n="18.3">broche</w> <w n="18.4">aux</w> <w n="18.5">dents</w> <w n="18.6">d</w>’<w n="18.7">acier</w></l>
						<l n="19" num="4.4"><w n="19.1">Mêle</w> <w n="19.2">au</w> <w n="19.3">concerto</w> <w n="19.4">domestique</w></l>
						<l n="20" num="4.5"><w n="20.1">Le</w> <w n="20.2">tic</w>-<w n="20.3">tac</w> <w n="20.4">de</w> <w n="20.5">son</w> <w n="20.6">balancier</w>.</l>
					</lg>
					<lg n="5">
						<l n="21" num="5.1"><w n="21.1">Les</w> <w n="21.2">étincelles</w> <w n="21.3">réjouies</w>,</l>
						<l n="22" num="5.2"><w n="22.1">En</w> <w n="22.2">étoiles</w> <w n="22.3">épanouies</w>,</l>
						<l n="23" num="5.3"><w n="23.1">Vont</w> <w n="23.2">et</w> <w n="23.3">viennent</w>, <w n="23.4">croisant</w> <w n="23.5">dans</w> <w n="23.6">l</w>’<w n="23.7">air</w></l>
						<l n="24" num="5.4"><w n="24.1">Les</w> <w n="24.2">salamandres</w> <w n="24.3">éblouies</w>,</l>
						<l n="25" num="5.5"><w n="25.1">Au</w> <w n="25.2">ricanement</w> <w n="25.3">grêle</w> <w n="25.4">et</w> <w n="25.5">clair</w>.</l>
					</lg>
					<lg n="6">
						<l n="26" num="6.1"><w n="26.1">Du</w> <w n="26.2">fond</w> <w n="26.3">de</w> <w n="26.4">ma</w> <w n="26.5">cellule</w> <w n="26.6">noire</w>,</l>
						<l n="27" num="6.2"><w n="27.1">Quand</w> <w n="27.2">Berthe</w> <w n="27.3">vous</w> <w n="27.4">conte</w> <w n="27.5">une</w> <w n="27.6">histoire</w>,</l>
						<l n="28" num="6.3"><hi rend="ital"><w n="28.1">Le</w> <w n="28.2">Chaperon</w></hi> <w n="28.3">ou</w> <hi rend="ital"><w n="28.4">l</w>’<w n="28.5">Oiseau</w> <w n="28.6">bleu</w></hi>,</l>
						<l n="29" num="6.4"><w n="29.1">C</w>’<w n="29.2">est</w> <w n="29.3">moi</w> <w n="29.4">qui</w> <w n="29.5">soutiens</w> <w n="29.6">sa</w> <w n="29.7">mémoire</w>,</l>
						<l n="30" num="6.5"><w n="30.1">C</w>’<w n="30.2">est</w> <w n="30.3">moi</w> <w n="30.4">qui</w> <w n="30.5">fais</w> <w n="30.6">taire</w> <w n="30.7">le</w> <w n="30.8">feu</w>.</l>
					</lg>
					<lg n="7">
						<l n="31" num="7.1"><w n="31.1">J</w>’<w n="31.2">étouffe</w> <w n="31.3">le</w> <w n="31.4">bruit</w> <w n="31.5">monotone</w></l>
						<l n="32" num="7.2"><w n="32.1">Du</w> <w n="32.2">rouet</w> <w n="32.3">qui</w> <w n="32.4">grince</w> <w n="32.5">et</w> <w n="32.6">bourdonne</w> ;</l>
						<l n="33" num="7.3"><w n="33.1">J</w>’<w n="33.2">impose</w> <w n="33.3">silence</w> <w n="33.4">au</w> <w n="33.5">matou</w> ;</l>
						<l n="34" num="7.4"><w n="34.1">Les</w> <w n="34.2">heures</w> <w n="34.3">s</w>’<w n="34.4">en</w> <w n="34.5">vont</w>, <w n="34.6">et</w> <w n="34.7">personne</w></l>
						<l n="35" num="7.5"><w n="35.1">N</w>’<w n="35.2">entend</w> <w n="35.3">le</w> <w n="35.4">timbre</w> <w n="35.5">du</w> <w n="35.6">coucou</w>.</l>
					</lg>
					<lg n="8">
						<l n="36" num="8.1"><w n="36.1">Pendant</w> <w n="36.2">la</w> <w n="36.3">nuit</w> <w n="36.4">et</w> <w n="36.5">la</w> <w n="36.6">journée</w>,</l>
						<l n="37" num="8.2"><w n="37.1">Je</w> <w n="37.2">chante</w> <w n="37.3">sous</w> <w n="37.4">la</w> <w n="37.5">cheminée</w> ;</l>
						<l n="38" num="8.3"><w n="38.1">Dans</w> <w n="38.2">mon</w> <w n="38.3">langage</w> <w n="38.4">de</w> <w n="38.5">grillon</w></l>
						<l n="39" num="8.4"><w n="39.1">J</w>’<w n="39.2">ai</w>, <w n="39.3">des</w> <w n="39.4">rebuts</w> <w n="39.5">de</w> <w n="39.6">son</w> <w n="39.7">aînée</w>,</l>
						<l n="40" num="8.5"><w n="40.1">Souvent</w> <w n="40.2">consolé</w> <w n="40.3">Cendrillon</w>.</l>
					</lg>
					<lg n="9">
						<l n="41" num="9.1"><w n="41.1">Le</w> <w n="41.2">renard</w> <w n="41.3">glapit</w> <w n="41.4">dans</w> <w n="41.5">le</w> <w n="41.6">piège</w> ;</l>
						<l n="42" num="9.2"><w n="42.1">Le</w> <w n="42.2">loup</w>, <w n="42.3">hurlant</w> <w n="42.4">de</w> <w n="42.5">faim</w>, <w n="42.6">assiège</w></l>
						<l n="43" num="9.3"><w n="43.1">La</w> <w n="43.2">ferme</w> <w n="43.3">au</w> <w n="43.4">milieu</w> <w n="43.5">des</w> <w n="43.6">grands</w> <w n="43.7">bois</w> ;</l>
						<l n="44" num="9.4"><w n="44.1">Décembre</w> <w n="44.2">met</w>, <w n="44.3">avec</w> <w n="44.4">sa</w> <w n="44.5">neige</w>,</l>
						<l n="45" num="9.5"><w n="45.1">Des</w> <w n="45.2">chemises</w> <w n="45.3">blanches</w> <w n="45.4">aux</w> <w n="45.5">toits</w>.</l>
					</lg>
					<lg n="10">
						<l n="46" num="10.1"><w n="46.1">Allons</w>, <w n="46.2">fagot</w>, <w n="46.3">pétille</w> <w n="46.4">et</w> <w n="46.5">flambe</w> ;</l>
						<l n="47" num="10.2"><w n="47.1">Courage</w> ! <w n="47.2">farfadet</w> <w n="47.3">ingambe</w>,</l>
						<l n="48" num="10.3"><w n="48.1">Saule</w>, <w n="48.2">bondis</w> <w n="48.3">plus</w> <w n="48.4">haut</w> <w n="48.5">encor</w> ;</l>
						<l n="49" num="10.4"><w n="49.1">Salamandre</w>, <w n="49.2">montre</w> <w n="49.3">ta</w> <w n="49.4">jambe</w>,</l>
						<l n="50" num="10.5"><w n="50.1">Lève</w> <w n="50.2">en</w> <w n="50.3">dansant</w> <w n="50.4">ton</w> <w n="50.5">jupon</w> <w n="50.6">d</w>’<w n="50.7">or</w>.</l>
					</lg>
					<lg n="11">
						<l n="51" num="11.1"><w n="51.1">Quel</w> <w n="51.2">plaisir</w> ? <w n="51.3">prolonger</w> <w n="51.4">sa</w> <w n="51.5">veille</w>,</l>
						<l n="52" num="11.2"><w n="52.1">Regarder</w> <w n="52.2">la</w> <w n="52.3">flamme</w> <w n="52.4">vermeille</w></l>
						<l n="53" num="11.3"><w n="53.1">Prenant</w> <w n="53.2">à</w> <w n="53.3">deux</w> <w n="53.4">bras</w> <w n="53.5">le</w> <w n="53.6">tison</w>,</l>
						<l n="54" num="11.4"><w n="54.1">A</w> <w n="54.2">tous</w> <w n="54.3">les</w> <w n="54.4">bruits</w> <w n="54.5">prêter</w> <w n="54.6">l</w>’<w n="54.7">oreille</w>,</l>
						<l n="55" num="11.5"><w n="55.1">Entendre</w> <w n="55.2">vivre</w> <w n="55.3">la</w> <w n="55.4">maison</w> !</l>
					</lg>
					<lg n="12">
						<l n="56" num="12.1"><w n="56.1">Tapi</w> <w n="56.2">dans</w> <w n="56.3">sa</w> <w n="56.4">niche</w> <w n="56.5">bien</w> <w n="56.6">chaude</w>,</l>
						<l n="57" num="12.2"><w n="57.1">Sentir</w> <w n="57.2">l</w>’<w n="57.3">hiver</w> <w n="57.4">qui</w> <w n="57.5">pleure</w> <w n="57.6">et</w> <w n="57.7">rôde</w>,</l>
						<l n="58" num="12.3"><w n="58.1">Tout</w> <w n="58.2">blême</w> <w n="58.3">et</w> <w n="58.4">le</w> <w n="58.5">nez</w> <w n="58.6">violet</w>,</l>
						<l n="59" num="12.4"><w n="59.1">Tâchant</w> <w n="59.2">de</w> <w n="59.3">s</w>’<w n="59.4">introduire</w> <w n="59.5">en</w> <w n="59.6">fraude</w></l>
						<l n="60" num="12.5"><w n="60.1">Par</w> <w n="60.2">quelque</w> <w n="60.3">fente</w> <w n="60.4">du</w> <w n="60.5">volet</w> !</l>
					</lg>
					<lg n="13">
						<l n="61" num="13.1"><w n="61.1">Souffle</w>, <w n="61.2">bise</w> ! <w n="61.3">tombe</w> <w n="61.4">à</w> <w n="61.5">flots</w>, <w n="61.6">pluie</w> !</l>
						<l n="62" num="13.2"><w n="62.1">Dans</w> <w n="62.2">mon</w> <w n="62.3">palais</w> <w n="62.4">tout</w> <w n="62.5">noir</w> <w n="62.6">de</w> <w n="62.7">suie</w>,</l>
						<l n="63" num="13.3"><w n="63.1">Je</w> <w n="63.2">ris</w> <w n="63.3">de</w> <w n="63.4">la</w> <w n="63.5">pluie</w> <w n="63.6">et</w> <w n="63.7">du</w> <w n="63.8">vent</w> ;</l>
						<l n="64" num="13.4"><w n="64.1">En</w> <w n="64.2">attendant</w> <w n="64.3">que</w> <w n="64.4">l</w>’<w n="64.5">hiver</w> <w n="64.6">fuie</w></l>
						<l n="65" num="13.5"><w n="65.1">Je</w> <w n="65.2">reste</w> <w n="65.3">au</w> <w n="65.4">coin</w> <w n="65.5">du</w> <w n="65.6">feu</w>, <w n="65.7">rêvant</w>.</l>
					</lg>
				</div>
				<div type="section" n="2">
					<head type="number">II</head>
					<lg n="1">
						<l n="66" num="1.1"><space quantity="4" unit="char"></space><w n="66.1">Regardez</w> <w n="66.2">les</w> <w n="66.3">branches</w>,</l>
						<l n="67" num="1.2"><space quantity="4" unit="char"></space><w n="67.1">Comme</w> <w n="67.2">elles</w> <w n="67.3">sont</w> <w n="67.4">blanches</w> !</l>
						<l n="68" num="1.3"><space quantity="4" unit="char"></space><w n="68.1">Il</w> <w n="68.2">neige</w> <w n="68.3">des</w> <w n="68.4">fleurs</w>.</l>
						<l n="69" num="1.4"><space quantity="4" unit="char"></space><w n="69.1">Riant</w> <w n="69.2">dans</w> <w n="69.3">la</w> <w n="69.4">pluie</w>,</l>
						<l n="70" num="1.5"><space quantity="4" unit="char"></space><w n="70.1">Le</w> <w n="70.2">soleil</w> <w n="70.3">essuie</w></l>
						<l n="71" num="1.6"><space quantity="4" unit="char"></space><w n="71.1">Les</w> <w n="71.2">saules</w> <w n="71.3">en</w> <w n="71.4">pleurs</w>,</l>
						<l n="72" num="1.7"><space quantity="4" unit="char"></space><w n="72.1">Et</w> <w n="72.2">le</w> <w n="72.3">ciel</w> <w n="72.4">reflète</w></l>
						<l n="73" num="1.8"><space quantity="4" unit="char"></space><w n="73.1">Dans</w> <w n="73.2">la</w> <w n="73.3">violette</w></l>
						<l n="74" num="1.9"><space quantity="4" unit="char"></space><w n="74.1">Ses</w> <w n="74.2">pures</w> <w n="74.3">couleurs</w>.</l>
					</lg>
					<lg n="2">
						<l n="75" num="2.1"><space quantity="4" unit="char"></space><w n="75.1">La</w> <w n="75.2">nature</w> <w n="75.3">en</w> <w n="75.4">joie</w></l>
						<l n="76" num="2.2"><space quantity="4" unit="char"></space><w n="76.1">Se</w> <w n="76.2">pare</w> <w n="76.3">et</w> <w n="76.4">déploie</w></l>
						<l n="77" num="2.3"><space quantity="4" unit="char"></space><w n="77.1">Son</w> <w n="77.2">manteau</w> <w n="77.3">vermeil</w>.</l>
						<l n="78" num="2.4"><space quantity="4" unit="char"></space><w n="78.1">Le</w> <w n="78.2">paon</w>, <w n="78.3">qui</w> <w n="78.4">se</w> <w n="78.5">joue</w>,</l>
						<l n="79" num="2.5"><space quantity="4" unit="char"></space><w n="79.1">Fait</w> <w n="79.2">tourner</w> <w n="79.3">en</w> <w n="79.4">roue</w></l>
						<l n="80" num="2.6"><space quantity="4" unit="char"></space><w n="80.1">Sa</w> <w n="80.2">queue</w> <w n="80.3">au</w> <w n="80.4">soleil</w>.</l>
						<l n="81" num="2.7"><space quantity="4" unit="char"></space><w n="81.1">Tout</w> <w n="81.2">court</w>, <w n="81.3">tout</w> <w n="81.4">s</w>’<w n="81.5">agite</w>,</l>
						<l n="82" num="2.8"><space quantity="4" unit="char"></space><w n="82.1">Pas</w> <w n="82.2">un</w> <w n="82.3">lièvre</w> <w n="82.4">au</w> <w n="82.5">gîte</w> ;</l>
						<l n="83" num="2.9"><space quantity="4" unit="char"></space><w n="83.1">L</w>’<w n="83.2">ours</w> <w n="83.3">sort</w> <w n="83.4">du</w> <w n="83.5">sommeil</w>.</l>
					</lg>
					<lg n="3">
						<l n="84" num="3.1"><space quantity="4" unit="char"></space><w n="84.1">La</w> <w n="84.2">mouche</w> <w n="84.3">ouvre</w> <w n="84.4">l</w>’<w n="84.5">aile</w>,</l>
						<l n="85" num="3.2"><space quantity="4" unit="char"></space><w n="85.1">Et</w> <w n="85.2">la</w> <w n="85.3">demoiselle</w></l>
						<l n="86" num="3.3"><space quantity="4" unit="char"></space><w n="86.1">Aux</w> <w n="86.2">prunelles</w> <w n="86.3">d</w>’<w n="86.4">or</w>,</l>
						<l n="87" num="3.4"><space quantity="4" unit="char"></space><w n="87.1">Au</w> <w n="87.2">corset</w> <w n="87.3">de</w> <w n="87.4">guêpe</w>,</l>
						<l n="88" num="3.5"><space quantity="4" unit="char"></space><w n="88.1">Dépliant</w> <w n="88.2">son</w> <w n="88.3">crêpe</w>,</l>
						<l n="89" num="3.6"><space quantity="4" unit="char"></space><w n="89.1">A</w> <w n="89.2">repris</w> <w n="89.3">l</w>’<w n="89.4">essor</w>.</l>
						<l n="90" num="3.7"><space quantity="4" unit="char"></space><w n="90.1">L</w>’<w n="90.2">eau</w> <w n="90.3">gaîment</w> <w n="90.4">babille</w>,</l>
						<l n="91" num="3.8"><space quantity="4" unit="char"></space><w n="91.1">Le</w> <w n="91.2">goujon</w> <w n="91.3">frétille</w> :</l>
						<l n="92" num="3.9"><space quantity="4" unit="char"></space><w n="92.1">Un</w> <w n="92.2">printemps</w> <w n="92.3">encor</w> !</l>
					</lg>
					<lg n="4">
						<l n="93" num="4.1"><space quantity="4" unit="char"></space><w n="93.1">Tout</w> <w n="93.2">se</w> <w n="93.3">cherche</w> <w n="93.4">et</w> <w n="93.5">s</w>’<w n="93.6">aime</w> ;</l>
						<l n="94" num="4.2"><space quantity="4" unit="char"></space><w n="94.1">Le</w> <w n="94.2">crapaud</w> <w n="94.3">lui</w>-<w n="94.4">même</w>,</l>
						<l n="95" num="4.3"><space quantity="4" unit="char"></space><w n="95.1">Les</w> <w n="95.2">aspics</w> <w n="95.3">méchants</w>,</l>
						<l n="96" num="4.4"><space quantity="4" unit="char"></space><w n="96.1">Toute</w> <w n="96.2">créature</w>,</l>
						<l n="97" num="4.5"><space quantity="4" unit="char"></space><w n="97.1">Selon</w> <w n="97.2">sa</w> <w n="97.3">nature</w> :</l>
						<l n="98" num="4.6"><space quantity="4" unit="char"></space><w n="98.1">La</w> <w n="98.2">feuille</w> <w n="98.3">a</w> <w n="98.4">des</w> <w n="98.5">chants</w> ;</l>
						<l n="99" num="4.7"><space quantity="4" unit="char"></space><w n="99.1">Les</w> <w n="99.2">herbes</w> <w n="99.3">résonnent</w>,</l>
						<l n="100" num="4.8"><space quantity="4" unit="char"></space><w n="100.1">Les</w> <w n="100.2">buissons</w> <w n="100.3">bourdonnent</w>,</l>
						<l n="101" num="4.9"><space quantity="4" unit="char"></space><w n="101.1">C</w>’<w n="101.2">est</w> <w n="101.3">concert</w> <w n="101.4">aux</w> <w n="101.5">champs</w>.</l>
					</lg>
					<lg n="5">
						<l n="102" num="5.1"><space quantity="4" unit="char"></space><w n="102.1">Moi</w> <w n="102.2">seul</w> <w n="102.3">je</w> <w n="102.4">suis</w> <w n="102.5">triste</w>.</l>
						<l n="103" num="5.2"><space quantity="4" unit="char"></space><w n="103.1">Qui</w> <w n="103.2">sait</w> <w n="103.3">si</w> <w n="103.4">j</w>’<w n="103.5">existe</w>,</l>
						<l n="104" num="5.3"><space quantity="4" unit="char"></space><w n="104.1">Dans</w> <w n="104.2">mon</w> <w n="104.3">palais</w> <w n="104.4">noir</w> ?</l>
						<l n="105" num="5.4"><space quantity="4" unit="char"></space><w n="105.1">Sous</w> <w n="105.2">la</w> <w n="105.3">cheminée</w>,</l>
						<l n="106" num="5.5"><space quantity="4" unit="char"></space><w n="106.1">Ma</w> <w n="106.2">vie</w> <w n="106.3">enchaînée</w></l>
						<l n="107" num="5.6"><space quantity="4" unit="char"></space><w n="107.1">Coule</w> <w n="107.2">sans</w> <w n="107.3">espoir</w>.</l>
						<l n="108" num="5.7"><space quantity="4" unit="char"></space><w n="108.1">Je</w> <w n="108.2">ne</w> <w n="108.3">puis</w>, <w n="108.4">malade</w>,</l>
						<l n="109" num="5.8"><space quantity="4" unit="char"></space><w n="109.1">Chanter</w> <w n="109.2">ma</w> <w n="109.3">ballade</w></l>
						<l n="110" num="5.9"><space quantity="4" unit="char"></space><w n="110.1">Aux</w> <w n="110.2">hôtes</w> <w n="110.3">du</w> <w n="110.4">soir</w>.</l>
					</lg>
					<lg n="6">
						<l n="111" num="6.1"><space quantity="4" unit="char"></space><w n="111.1">Si</w> <w n="111.2">la</w> <w n="111.3">brise</w> <w n="111.4">tiède</w></l>
						<l n="112" num="6.2"><space quantity="4" unit="char"></space><w n="112.1">Au</w> <w n="112.2">vent</w> <w n="112.3">froid</w> <w n="112.4">succède</w>,</l>
						<l n="113" num="6.3"><space quantity="4" unit="char"></space><w n="113.1">Si</w> <w n="113.2">le</w> <w n="113.3">ciel</w> <w n="113.4">est</w> <w n="113.5">clair</w>,</l>
						<l n="114" num="6.4"><space quantity="4" unit="char"></space><w n="114.1">Moi</w>, <w n="114.2">ma</w> <w n="114.3">cheminée</w></l>
						<l n="115" num="6.5"><space quantity="4" unit="char"></space><w n="115.1">N</w>’<w n="115.2">est</w> <w n="115.3">illuminée</w></l>
						<l n="116" num="6.6"><space quantity="4" unit="char"></space><w n="116.1">Que</w> <w n="116.2">d</w>’<w n="116.3">un</w> <w n="116.4">pâle</w> <w n="116.5">éclair</w> ;</l>
						<l n="117" num="6.7"><space quantity="4" unit="char"></space><w n="117.1">Le</w> <w n="117.2">cercle</w> <w n="117.3">folâtre</w></l>
						<l n="118" num="6.8"><space quantity="4" unit="char"></space><w n="118.1">Abandonne</w> <w n="118.2">l</w>’<w n="118.3">âtre</w> :</l>
						<l n="119" num="6.9"><space quantity="4" unit="char"></space><w n="119.1">Pour</w> <w n="119.2">moi</w> <w n="119.3">c</w>’<w n="119.4">est</w> <w n="119.5">l</w>’<w n="119.6">hiver</w>.</l>
					</lg>
					<lg n="7">
						<l n="120" num="7.1"><space quantity="4" unit="char"></space><w n="120.1">Sur</w> <w n="120.2">la</w> <w n="120.3">cendre</w> <w n="120.4">grise</w>,</l>
						<l n="121" num="7.2"><space quantity="4" unit="char"></space><w n="121.1">La</w> <w n="121.2">pincette</w> <w n="121.3">brise</w></l>
						<l n="122" num="7.3"><space quantity="4" unit="char"></space><w n="122.1">Un</w> <w n="122.2">charbon</w> <w n="122.3">sans</w> <w n="122.4">feu</w>.</l>
						<l n="123" num="7.4"><space quantity="4" unit="char"></space><w n="123.1">Adieu</w> <w n="123.2">les</w> <w n="123.3">paillettes</w>,</l>
						<l n="124" num="7.5"><space quantity="4" unit="char"></space><w n="124.1">Les</w> <w n="124.2">blondes</w> <w n="124.3">aigrettes</w> !</l>
						<l n="125" num="7.6"><space quantity="4" unit="char"></space><w n="125.1">Pour</w> <w n="125.2">six</w> <w n="125.3">mois</w> <w n="125.4">adieu</w></l>
						<l n="126" num="7.7"><space quantity="4" unit="char"></space><w n="126.1">La</w> <w n="126.2">maîtresse</w> <w n="126.3">bûche</w>,</l>
						<l n="127" num="7.8"><space quantity="4" unit="char"></space><w n="127.1">Où</w> <w n="127.2">sous</w> <w n="127.3">la</w> <w n="127.4">peluche</w></l>
						<l n="128" num="7.9"><space quantity="4" unit="char"></space><w n="128.1">Sifflait</w> <w n="128.2">le</w> <w n="128.3">gaz</w> <w n="128.4">bleu</w> !</l>
					</lg>
					<lg n="8">
						<l n="129" num="8.1"><space quantity="4" unit="char"></space><w n="129.1">Dans</w> <w n="129.2">ma</w> <w n="129.3">niche</w> <w n="129.4">creuse</w>,</l>
						<l n="130" num="8.2"><space quantity="4" unit="char"></space><w n="130.1">Ma</w> <w n="130.2">patte</w> <w n="130.3">boiteuse</w></l>
						<l n="131" num="8.3"><space quantity="4" unit="char"></space><w n="131.1">Me</w> <w n="131.2">tient</w> <w n="131.3">en</w> <w n="131.4">prison</w>.</l>
						<l n="132" num="8.4"><space quantity="4" unit="char"></space><w n="132.1">Quand</w> <w n="132.2">l</w>’<w n="132.3">insecte</w> <w n="132.4">rôde</w>,</l>
						<l n="133" num="8.5"><space quantity="4" unit="char"></space><w n="133.1">Comme</w> <w n="133.2">une</w> <w n="133.3">émeraude</w>,</l>
						<l n="134" num="8.6"><space quantity="4" unit="char"></space><w n="134.1">Sous</w> <w n="134.2">le</w> <w n="134.3">vert</w> <w n="134.4">gazon</w>,</l>
						<l n="135" num="8.7"><space quantity="4" unit="char"></space><w n="135.1">Moi</w> <w n="135.2">seul</w> <w n="135.3">je</w> <w n="135.4">m</w>’<w n="135.5">ennuie</w> ;</l>
						<l n="136" num="8.8"><space quantity="4" unit="char"></space><w n="136.1">Un</w> <w n="136.2">mur</w>, <w n="136.3">noir</w> <w n="136.4">de</w> <w n="136.5">suie</w>,</l>
						<l n="137" num="8.9"><space quantity="4" unit="char"></space><w n="137.1">Est</w> <w n="137.2">mon</w> <w n="137.3">horizon</w>.</l>
					</lg>
				</div>
			</div></body></text></TEI>