<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES DIVERSES, 1833-1838</title>
				<title type="sub_2">Tome premier</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3874 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Premier</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/files/44180/44180-h/44180-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome premier</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1838">1833-1838</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU217">
				<head type="main">CHINOISERIE</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Ce</w> <w n="1.2">n</w>’<w n="1.3">est</w> <w n="1.4">pas</w> <w n="1.5">vous</w>, <w n="1.6">non</w>, <w n="1.7">madame</w>, <w n="1.8">que</w> <w n="1.9">j</w>’<w n="1.10">aime</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Ni</w> <w n="2.2">vous</w> <w n="2.3">non</w> <w n="2.4">plus</w>, <w n="2.5">Juliette</w>, <w n="2.6">ni</w> <w n="2.7">vous</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Ophélia</w>, <w n="3.2">ni</w> <w n="3.3">Béatrix</w>, <w n="3.4">ni</w> <w n="3.5">même</w></l>
					<l n="4" num="1.4"><w n="4.1">Laure</w> <w n="4.2">la</w> <w n="4.3">blonde</w>, <w n="4.4">avec</w> <w n="4.5">ses</w> <w n="4.6">grands</w> <w n="4.7">yeux</w> <w n="4.8">doux</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Celle</w> <w n="5.2">que</w> <w n="5.3">j</w>’<w n="5.4">aime</w>, <w n="5.5">à</w> <w n="5.6">présent</w>, <w n="5.7">est</w> <w n="5.8">en</w> <w n="5.9">Chine</w> ;</l>
					<l n="6" num="2.2"><w n="6.1">Elle</w> <w n="6.2">demeure</w> <w n="6.3">avec</w> <w n="6.4">ses</w> <w n="6.5">vieux</w> <w n="6.6">parents</w>,</l>
					<l n="7" num="2.3"><w n="7.1">Dans</w> <w n="7.2">une</w> <w n="7.3">tour</w> <w n="7.4">de</w> <w n="7.5">porcelaine</w> <w n="7.6">fine</w>,</l>
					<l n="8" num="2.4"><w n="8.1">Au</w> <w n="8.2">fleuve</w> <w n="8.3">Jaune</w>, <w n="8.4">où</w> <w n="8.5">sont</w> <w n="8.6">les</w> <w n="8.7">cormorans</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Elle</w> <w n="9.2">a</w> <w n="9.3">des</w> <w n="9.4">yeux</w> <w n="9.5">retroussés</w> <w n="9.6">vers</w> <w n="9.7">les</w> <w n="9.8">tempes</w>,</l>
					<l n="10" num="3.2"><w n="10.1">Un</w> <w n="10.2">pied</w> <w n="10.3">petit</w> <w n="10.4">à</w> <w n="10.5">tenir</w> <w n="10.6">dans</w> <w n="10.7">la</w> <w n="10.8">main</w>,</l>
					<l n="11" num="3.3"><w n="11.1">Le</w> <w n="11.2">teint</w> <w n="11.3">plus</w> <w n="11.4">clair</w> <w n="11.5">que</w> <w n="11.6">le</w> <w n="11.7">cuivre</w> <w n="11.8">des</w> <w n="11.9">lampes</w>,</l>
					<l n="12" num="3.4"><w n="12.1">Les</w> <w n="12.2">ongles</w> <w n="12.3">longs</w> <w n="12.4">et</w> <w n="12.5">rougis</w> <w n="12.6">de</w> <w n="12.7">carmin</w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Par</w> <w n="13.2">son</w> <w n="13.3">treillis</w> <w n="13.4">elle</w> <w n="13.5">passe</w> <w n="13.6">sa</w> <w n="13.7">tête</w>,</l>
					<l n="14" num="4.2"><w n="14.1">Que</w> <w n="14.2">l</w>’<w n="14.3">hirondelle</w>, <w n="14.4">en</w> <w n="14.5">volant</w>, <w n="14.6">vient</w> <w n="14.7">toucher</w>,</l>
					<l n="15" num="4.3"><w n="15.1">Et</w>, <w n="15.2">chaque</w> <w n="15.3">soir</w>, <w n="15.4">aussi</w> <w n="15.5">bien</w> <w n="15.6">qu</w>’<w n="15.7">un</w> <w n="15.8">poëte</w>,</l>
					<l n="16" num="4.4"><w n="16.1">Chante</w> <w n="16.2">le</w> <w n="16.3">saule</w> <w n="16.4">et</w> <w n="16.5">la</w> <w n="16.6">fleur</w> <w n="16.7">du</w> <w n="16.8">pêcher</w>.</l>
				</lg>
			</div></body></text></TEI>