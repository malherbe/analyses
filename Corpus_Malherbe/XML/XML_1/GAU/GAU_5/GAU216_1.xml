<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES DIVERSES, 1833-1838</title>
				<title type="sub_2">Tome premier</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3874 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Premier</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/files/44180/44180-h/44180-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome premier</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1838">1833-1838</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU216">
				<head type="main">COMPENSATION</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Il</w> <w n="1.2">naît</w> <w n="1.3">sous</w> <w n="1.4">le</w> <w n="1.5">soleil</w> <w n="1.6">de</w> <w n="1.7">nobles</w> <w n="1.8">créatures</w></l>
					<l n="2" num="1.2"><w n="2.1">Unissant</w> <w n="2.2">ici</w>-<w n="2.3">bas</w> <w n="2.4">tout</w> <w n="2.5">ce</w> <w n="2.6">qu</w>’<w n="2.7">on</w> <w n="2.8">peut</w> <w n="2.9">rêver</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Corps</w> <w n="3.2">de</w> <w n="3.3">fer</w>, <w n="3.4">cœur</w> <w n="3.5">de</w> <w n="3.6">flamme</w>, <w n="3.7">admirables</w> <w n="3.8">natures</w>.</l>
				</lg>
				<lg n="2">
					<l n="4" num="2.1"><w n="4.1">Dieu</w> <w n="4.2">semble</w> <w n="4.3">les</w> <w n="4.4">produire</w> <w n="4.5">afin</w> <w n="4.6">de</w> <w n="4.7">se</w> <w n="4.8">prouver</w> ;</l>
					<l n="5" num="2.2"><w n="5.1">Il</w> <w n="5.2">prend</w>, <w n="5.3">pour</w> <w n="5.4">les</w> <w n="5.5">pétrir</w>, <w n="5.6">une</w> <w n="5.7">argile</w> <w n="5.8">plus</w> <w n="5.9">douce</w>,</l>
					<l n="6" num="2.3"><w n="6.1">Et</w> <w n="6.2">souvent</w> <w n="6.3">passe</w> <w n="6.4">un</w> <w n="6.5">siècle</w> <w n="6.6">à</w> <w n="6.7">les</w> <w n="6.8">parachever</w>.</l>
				</lg>
				<lg n="3">
					<l n="7" num="3.1"><w n="7.1">Il</w> <w n="7.2">met</w>, <w n="7.3">comme</w> <w n="7.4">un</w> <w n="7.5">sculpteur</w>, <w n="7.6">l</w>’<w n="7.7">empreinte</w> <w n="7.8">de</w> <w n="7.9">son</w> <w n="7.10">pouce</w></l>
					<l n="8" num="3.2"><w n="8.1">Sur</w> <w n="8.2">leurs</w> <w n="8.3">fronts</w> <w n="8.4">rayonnant</w> <w n="8.5">de</w> <w n="8.6">la</w> <w n="8.7">gloire</w> <w n="8.8">des</w> <w n="8.9">cieux</w>,</l>
					<l n="9" num="3.3"><w n="9.1">Et</w> <w n="9.2">l</w>’<w n="9.3">ardente</w> <w n="9.4">auréole</w> <w n="9.5">en</w> <w n="9.6">gerbe</w> <w n="9.7">d</w>’<w n="9.8">or</w> <w n="9.9">y</w> <w n="9.10">pousse</w>.</l>
				</lg>
				<lg n="4">
					<l n="10" num="4.1"><w n="10.1">Ces</w> <w n="10.2">hommes</w>-<w n="10.3">là</w> <w n="10.4">s</w>’<w n="10.5">en</w> <w n="10.6">vont</w>, <w n="10.7">calmes</w> <w n="10.8">et</w> <w n="10.9">radieux</w>,</l>
					<l n="11" num="4.2"><w n="11.1">Sans</w> <w n="11.2">quitter</w> <w n="11.3">un</w> <w n="11.4">instant</w> <w n="11.5">leur</w> <w n="11.6">pose</w> <w n="11.7">solennelle</w>,</l>
					<l n="12" num="4.3"><w n="12.1">Avec</w> <w n="12.2">l</w>’<w n="12.3">œil</w> <w n="12.4">immobile</w> <w n="12.5">et</w> <w n="12.6">le</w> <w n="12.7">maintien</w> <w n="12.8">des</w> <w n="12.9">dieux</w>.</l>
				</lg>
				<lg n="5">
					<l n="13" num="5.1"><w n="13.1">Leur</w> <w n="13.2">moindre</w> <w n="13.3">fantaisie</w> <w n="13.4">est</w> <w n="13.5">une</w> <w n="13.6">œuvre</w> <w n="13.7">éternelle</w>,</l>
					<l n="14" num="5.2"><w n="14.1">Tout</w> <w n="14.2">cède</w> <w n="14.3">devant</w> <w n="14.4">eux</w> ; <w n="14.5">les</w> <w n="14.6">sables</w> <w n="14.7">inconstants</w></l>
					<l n="15" num="5.3"><w n="15.1">Gardent</w> <w n="15.2">leurs</w> <w n="15.3">pas</w> <w n="15.4">empreints</w>, <w n="15.5">comme</w> <w n="15.6">un</w> <w n="15.7">airain</w> <w n="15.8">fidèle</w>.</l>
				</lg>
				<lg n="6">
					<l n="16" num="6.1"><w n="16.1">Ne</w> <w n="16.2">leur</w> <w n="16.3">donnez</w> <w n="16.4">qu</w>’<w n="16.5">un</w> <w n="16.6">jour</w> <w n="16.7">ou</w> <w n="16.8">donnez</w>-<w n="16.9">leur</w> <w n="16.10">cent</w> <w n="16.11">ans</w>,</l>
					<l n="17" num="6.2"><w n="17.1">L</w>’<w n="17.2">orage</w> <w n="17.3">ou</w> <w n="17.4">le</w> <w n="17.5">repos</w>, <w n="17.6">la</w> <w n="17.7">palette</w> <w n="17.8">ou</w> <w n="17.9">le</w> <w n="17.10">glaive</w> :</l>
					<l n="18" num="6.3"><w n="18.1">Ils</w> <w n="18.2">mèneront</w> <w n="18.3">à</w> <w n="18.4">bout</w> <w n="18.5">leurs</w> <w n="18.6">destins</w> <w n="18.7">éclatants</w>.</l>
				</lg>
				<lg n="7">
					<l n="19" num="7.1"><w n="19.1">Leur</w> <w n="19.2">existence</w> <w n="19.3">étrange</w> <w n="19.4">est</w> <w n="19.5">le</w> <w n="19.6">réel</w> <w n="19.7">du</w> <w n="19.8">rêve</w> ;</l>
					<l n="20" num="7.2"><w n="20.1">Ils</w> <w n="20.2">exécuteront</w> <w n="20.3">votre</w> <w n="20.4">plan</w> <w n="20.5">idéal</w>,</l>
					<l n="21" num="7.3"><w n="21.1">Comme</w> <w n="21.2">un</w> <w n="21.3">maître</w> <w n="21.4">savant</w> <w n="21.5">le</w> <w n="21.6">croquis</w> <w n="21.7">d</w>’<w n="21.8">un</w> <w n="21.9">élève</w>.</l>
				</lg>
				<lg n="8">
					<l n="22" num="8.1"><w n="22.1">Vos</w> <w n="22.2">désirs</w> <w n="22.3">inconnus</w>, <w n="22.4">sous</w> <w n="22.5">l</w>’<w n="22.6">arceau</w> <w n="22.7">triomphal</w></l>
					<l n="23" num="8.2"><w n="23.1">Dont</w> <w n="23.2">votre</w> <w n="23.3">esprit</w> <w n="23.4">en</w> <w n="23.5">songe</w> <w n="23.6">arrondissait</w> <w n="23.7">la</w> <w n="23.8">voûte</w>,</l>
					<l n="24" num="8.3"><w n="24.1">Passent</w> <w n="24.2">assis</w> <w n="24.3">en</w> <w n="24.4">croupe</w> <w n="24.5">au</w> <w n="24.6">dos</w> <w n="24.7">de</w> <w n="24.8">leur</w> <w n="24.9">cheval</w>.</l>
				</lg>
				<lg n="9">
					<l n="25" num="9.1"><w n="25.1">D</w>’<w n="25.2">un</w> <w n="25.3">pied</w> <w n="25.4">sûr</w>, <w n="25.5">jusqu</w>’<w n="25.6">au</w> <w n="25.7">bout</w> <w n="25.8">ils</w> <w n="25.9">ont</w> <w n="25.10">suivi</w> <w n="25.11">la</w> <w n="25.12">route</w></l>
					<l n="26" num="9.2"><w n="26.1">Où</w>, <w n="26.2">dès</w> <w n="26.3">les</w> <w n="26.4">premiers</w> <w n="26.5">pas</w>, <w n="26.6">vous</w> <w n="26.7">vous</w> <w n="26.8">êtes</w> <w n="26.9">assis</w>,</l>
					<l n="27" num="9.3"><w n="27.1">N</w>’<w n="27.2">osant</w> <w n="27.3">prendre</w> <w n="27.4">une</w> <w n="27.5">branche</w> <w n="27.6">au</w> <w n="27.7">carrefour</w> <w n="27.8">du</w> <w n="27.9">doute</w>.</l>
				</lg>
				<lg n="10">
					<l n="28" num="10.1"><w n="28.1">De</w> <w n="28.2">ceux</w>-<w n="28.3">là</w> <w n="28.4">chaque</w> <w n="28.5">peuple</w> <w n="28.6">en</w> <w n="28.7">compte</w> <w n="28.8">cinq</w> <w n="28.9">ou</w> <w n="28.10">six</w>,</l>
					<l n="29" num="10.2"><w n="29.1">Cinq</w> <w n="29.2">ou</w> <w n="29.3">six</w> <w n="29.4">tout</w> <w n="29.5">au</w> <w n="29.6">plus</w>, <w n="29.7">dans</w> <w n="29.8">les</w> <w n="29.9">siècles</w> <w n="29.10">prospères</w>,</l>
					<l n="30" num="10.3"><w n="30.1">Types</w> <w n="30.2">toujours</w> <w n="30.3">vivants</w> <w n="30.4">dont</w> <w n="30.5">on</w> <w n="30.6">fait</w> <w n="30.7">des</w> <w n="30.8">récits</w>.</l>
				</lg>
				<lg n="11">
					<l n="31" num="11.1"><w n="31.1">Nature</w> <w n="31.2">avare</w>, <w n="31.3">ô</w> <w n="31.4">toi</w>, <w n="31.5">si</w> <w n="31.6">féconde</w> <w n="31.7">en</w> <w n="31.8">vipères</w>,</l>
					<l n="32" num="11.2"><w n="32.1">En</w> <w n="32.2">serpents</w>, <w n="32.3">en</w> <w n="32.4">crapauds</w> <w n="32.5">tout</w> <w n="32.6">gonflés</w> <w n="32.7">de</w> <w n="32.8">venins</w>,</l>
					<l n="33" num="11.3"><w n="33.1">Si</w> <w n="33.2">prompte</w> <w n="33.3">à</w> <w n="33.4">repeupler</w> <w n="33.5">tes</w> <w n="33.6">immondes</w> <w n="33.7">repaires</w>,</l>
				</lg>
				<lg n="12">
					<l n="34" num="12.1"><w n="34.1">Pour</w> <w n="34.2">tant</w> <w n="34.3">d</w>’<w n="34.4">animaux</w> <w n="34.5">vils</w>, <w n="34.6">d</w>’<w n="34.7">idiots</w> <w n="34.8">et</w> <w n="34.9">de</w> <w n="34.10">nains</w>,</l>
					<l n="35" num="12.2"><w n="35.1">Pour</w> <w n="35.2">tant</w> <w n="35.3">d</w>’<w n="35.4">avortements</w> <w n="35.5">et</w> <w n="35.6">d</w>’<w n="35.7">œuvres</w> <w n="35.8">imparfaites</w>,</l>
					<l n="36" num="12.3"><w n="36.1">Tant</w> <w n="36.2">de</w> <w n="36.3">monstres</w> <w n="36.4">impurs</w> <w n="36.5">échappés</w> <w n="36.6">de</w> <w n="36.7">tes</w> <w n="36.8">mains</w>,</l>
				</lg>
				<lg n="13">
					<l n="37" num="13.1"><w n="37.1">Nature</w>, <w n="37.2">tu</w> <w n="37.3">nous</w> <w n="37.4">dois</w> <w n="37.5">encor</w> <w n="37.6">bien</w> <w n="37.7">des</w> <w n="37.8">poëtes</w> !</l>
				</lg>
			</div></body></text></TEI>