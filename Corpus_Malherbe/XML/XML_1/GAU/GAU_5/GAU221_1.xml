<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES DIVERSES, 1833-1838</title>
				<title type="sub_2">Tome premier</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3874 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Premier</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/files/44180/44180-h/44180-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome premier</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1838">1833-1838</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU221">
				<head type="main">ÉLÉGIE</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">J</w>’<w n="1.2">ai</w> <w n="1.3">fait</w> <w n="1.4">une</w> <w n="1.5">remarque</w> <w n="1.6">hier</w> <w n="1.7">en</w> <w n="1.8">te</w> <w n="1.9">quittant</w>.</l>
					<l n="2" num="1.2"><w n="2.1">Sans</w> <w n="2.2">doute</w> <w n="2.3">j</w>’<w n="2.4">ai</w> <w n="2.5">mal</w> <w n="2.6">vu</w> ; <w n="2.7">mais</w> <w n="2.8">quand</w> <w n="2.9">on</w> <w n="2.10">aime</w> <w n="2.11">tant</w></l>
					<l n="3" num="1.3"><w n="3.1">On</w> <w n="3.2">a</w> <w n="3.3">peur</w> ; <w n="3.4">on</w> <w n="3.5">se</w> <w n="3.6">fait</w> <w n="3.7">avec</w> <w n="3.8">la</w> <w n="3.9">moindre</w> <w n="3.10">chose</w></l>
					<l n="4" num="1.4"><w n="4.1">Un</w> <w n="4.2">sujet</w> <w n="4.3">de</w> <w n="4.4">tourments</w>. <w n="4.5">On</w> <w n="4.6">veut</w> <w n="4.7">savoir</w> <w n="4.8">la</w> <w n="4.9">cause</w></l>
					<l n="5" num="1.5"><w n="5.1">De</w> <w n="5.2">chaque</w> <w n="5.3">effet</w>. <w n="5.4">Un</w> <w n="5.5">mot</w>, <w n="5.6">un</w> <w n="5.7">geste</w>, <w n="5.8">une</w> <w n="5.9">ombre</w>, <w n="5.10">un</w> <w n="5.11">rien</w>,</l>
					<l n="6" num="1.6"><w n="6.1">La</w> <w n="6.2">plus</w> <w n="6.3">folle</w> <w n="6.4">chimère</w>, <w n="6.5">un</w> <w n="6.6">souvenir</w> <w n="6.7">ancien</w></l>
					<l n="7" num="1.7"><w n="7.1">Qui</w> <w n="7.2">dormait</w> <w n="7.3">dans</w> <w n="7.4">un</w> <w n="7.5">coin</w> <w n="7.6">du</w> <w n="7.7">cœur</w> <w n="7.8">et</w> <w n="7.9">qui</w> <w n="7.10">s</w>’<w n="7.11">éveille</w>,</l>
					<l n="8" num="1.8"><w n="8.1">Tout</w> <w n="8.2">vous</w> <w n="8.3">effraie</w>. <w n="8.4">On</w> <w n="8.5">dit</w> <w n="8.6">qu</w>’<w n="8.7">infortune</w> <w n="8.8">pareille</w></l>
					<l n="9" num="1.9"><w n="9.1">Ne</w> <w n="9.2">s</w>’<w n="9.3">est</w> <w n="9.4">pas</w> <w n="9.5">encor</w> <w n="9.6">vue</w> <w n="9.7">et</w> <w n="9.8">que</w> <w n="9.9">l</w>’<w n="9.10">on</w> <w n="9.11">en</w> <w n="9.12">mourra</w> ;</l>
					<l n="10" num="1.10"><w n="10.1">L</w>’<w n="10.2">on</w> <w n="10.3">n</w>’<w n="10.4">en</w> <w n="10.5">meurt</w> <w n="10.6">pas</w> ; <w n="10.7">demain</w> <w n="10.8">peut</w>-<w n="10.9">être</w> <w n="10.10">on</w> <w n="10.11">en</w> <w n="10.12">rira</w>.</l>
					<l n="11" num="1.11"><w n="11.1">Vous</w> <w n="11.2">veniez</w> <w n="11.3">pour</w> <w n="11.4">vous</w> <w n="11.5">plaindre</w> : <w n="11.6">un</w> <w n="11.7">baiser</w>, <w n="11.8">un</w> <w n="11.9">sourire</w>,</l>
					<l n="12" num="1.12"><w n="12.1">Et</w> <w n="12.2">vous</w> <w n="12.3">ne</w> <w n="12.4">savez</w> <w n="12.5">plus</w> <w n="12.6">ce</w> <w n="12.7">que</w> <w n="12.8">vous</w> <w n="12.9">veniez</w> <w n="12.10">dire</w>.</l>
					<l n="13" num="1.13"><w n="13.1">Quand</w> <w n="13.2">tu</w> <w n="13.3">liras</w> <w n="13.4">ces</w> <w n="13.5">vers</w>, <w n="13.6">sans</w> <w n="13.7">doute</w> <w n="13.8">tu</w> <w n="13.9">diras</w></l>
					<l n="14" num="1.14"><w n="14.1">Que</w> <w n="14.2">mon</w> <w n="14.3">idée</w> <w n="14.4">est</w> <w n="14.5">folle</w> <w n="14.6">et</w> <w n="14.7">tu</w> <w n="14.8">m</w>’<w n="14.9">embrasseras</w>,</l>
					<l n="15" num="1.15"><w n="15.1">Et</w> <w n="15.2">puis</w>, <w n="15.3">j</w>’<w n="15.4">oublîrai</w> <w n="15.5">tout</w>, <w n="15.6">excepté</w> <w n="15.7">que</w> <w n="15.8">je</w> <w n="15.9">t</w>’<w n="15.10">aime</w></l>
					<l n="16" num="1.16"><w n="16.1">Et</w> <w n="16.2">que</w> <w n="16.3">je</w> <w n="16.4">t</w>’<w n="16.5">aimerai</w> <w n="16.6">toujours</w>. <w n="16.7">Fais</w>-<w n="16.8">en</w> <w n="16.9">de</w> <w n="16.10">même</w>.</l>
					<l n="17" num="1.17"><w n="17.1">Or</w>, <w n="17.2">voici</w> <w n="17.3">ma</w> <w n="17.4">remarque</w> ; <w n="17.5">il</w> <w n="17.6">m</w>’<w n="17.7">a</w> <w n="17.8">semblé</w> <w n="17.9">cela</w>.</l>
					<l n="18" num="1.18"><w n="18.1">Je</w> <w n="18.2">voudrais</w> <w n="18.3">oublier</w> <w n="18.4">toutes</w> <w n="18.5">ces</w> <w n="18.6">choses</w>-<w n="18.7">là</w> ;</l>
					<l n="19" num="1.19"><w n="19.1">Mais</w> <w n="19.2">je</w> <w n="19.3">ne</w> <w n="19.4">puis</w>. <w n="19.5">Hier</w> <w n="19.6">tu</w> <w n="19.7">paraissais</w> <w n="19.8">distraite</w>,</l>
					<l n="20" num="1.20"><w n="20.1">Et</w> <w n="20.2">ce</w> <w n="20.3">n</w>’<w n="20.4">est</w> <w n="20.5">pas</w> <w n="20.6">ainsi</w>, <w n="20.7">certes</w>, <w n="20.8">que</w> <w n="20.9">Juliette</w></l>
					<l n="21" num="1.21"><w n="21.1">Laisse</w> <w n="21.2">aller</w> <w n="21.3">Roméo</w> <w n="21.4">qui</w> <w n="21.5">part</w>. <w n="21.6">En</w> <w n="21.7">ce</w> <w n="21.8">moment</w></l>
					<l n="22" num="1.22"><w n="22.1">Où</w> <w n="22.2">mon</w> <w n="22.3">âme</w> <w n="22.4">pâmée</w> <w n="22.5">à</w> <w n="22.6">chaque</w> <w n="22.7">embrassement</w></l>
					<l n="23" num="1.23"><w n="23.1">S</w>’<w n="23.2">élançait</w> <w n="23.3">sur</w> <w n="23.4">ta</w> <w n="23.5">bouche</w> <w n="23.6">au</w>-<w n="23.7">devant</w> <w n="23.8">de</w> <w n="23.9">ton</w> <w n="23.10">âme</w>,</l>
					<l n="24" num="1.24"><w n="24.1">Où</w> <w n="24.2">ma</w> <w n="24.3">prunelle</w> <w n="24.4">en</w> <w n="24.5">pleurs</w> <w n="24.6">baignait</w> <w n="24.7">ma</w> <w n="24.8">joue</w> <w n="24.9">en</w> <w n="24.10">flamme</w>,</l>
					<l n="25" num="1.25"><w n="25.1">Où</w> <w n="25.2">mon</w> <w n="25.3">cœur</w> <w n="25.4">éperdu</w>, <w n="25.5">sur</w> <w n="25.6">ton</w> <w n="25.7">cœur</w> <w n="25.8">qu</w>’<w n="25.9">il</w> <w n="25.10">cherchait</w>,</l>
					<l n="26" num="1.26"><w n="26.1">Vibrait</w> <w n="26.2">comme</w> <w n="26.3">une</w> <w n="26.4">lyre</w> <w n="26.5">au</w> <w n="26.6">toucher</w> <w n="26.7">de</w> <w n="26.8">l</w>’<w n="26.9">archet</w>,</l>
					<l n="27" num="1.27"><w n="27.1">Où</w> <w n="27.2">mes</w> <w n="27.3">deux</w> <w n="27.4">bras</w> <w n="27.5">noués</w>, <w n="27.6">comme</w> <w n="27.7">ceux</w> <w n="27.8">d</w>’<w n="27.9">un</w> <w n="27.10">avare</w></l>
					<l n="28" num="1.28"><w n="28.1">Qui</w> <w n="28.2">tient</w> <w n="28.3">son</w> <w n="28.4">or</w> <w n="28.5">et</w> <w n="28.6">craint</w> <w n="28.7">qu</w>’<w n="28.8">un</w> <w n="28.9">larron</w> <w n="28.10">s</w>’<w n="28.11">en</w> <w n="28.12">empare</w>,</l>
					<l n="29" num="1.29"><w n="29.1">Te</w> <w n="29.2">tenaient</w> <w n="29.3">enfermée</w> <w n="29.4">et</w> <w n="29.5">t</w>’<w n="29.6">enchaînaient</w> <w n="29.7">à</w> <w n="29.8">moi</w>,</l>
					<l n="30" num="1.30"><w n="30.1">Toi</w>, <w n="30.2">tu</w> <w n="30.3">ne</w> <w n="30.4">disais</w> <w n="30.5">rien</w> ; <w n="30.6">tu</w> <w n="30.7">n</w>’<w n="30.8">écoutais</w> <w n="30.9">pas</w>, <w n="30.10">toi</w> ;</l>
					<l n="31" num="1.31"><w n="31.1">Mes</w> <w n="31.2">baisers</w> <w n="31.3">s</w>’<w n="31.4">éteignaient</w> <w n="31.5">sur</w> <w n="31.6">ta</w> <w n="31.7">lèvre</w> <w n="31.8">glacée</w> ;</l>
					<l n="32" num="1.32"><w n="32.1">Je</w> <w n="32.2">ne</w> <w n="32.3">te</w> <w n="32.4">sentais</w> <w n="32.5">pas</w> <w n="32.6">sentir</w> ; <w n="32.7">ta</w> <w n="32.8">main</w> <w n="32.9">pressée</w></l>
					<l n="33" num="1.33"><w n="33.1">N</w>’<w n="33.2">entendait</w> <w n="33.3">pas</w> <w n="33.4">la</w> <w n="33.5">mienne</w> <w n="33.6">et</w> <w n="33.7">ne</w> <w n="33.8">répondait</w> <w n="33.9">rien</w>.</l>
					<l n="34" num="1.34"><w n="34.1">J</w>’<w n="34.2">étais</w> <w n="34.3">là</w>, <w n="34.4">devant</w> <w n="34.5">toi</w>, <w n="34.6">comme</w> <w n="34.7">un</w> <w n="34.8">musicien</w>,</l>
					<l n="35" num="1.35"><w n="35.1">Tourmentant</w> <w n="35.2">le</w> <w n="35.3">clavier</w> <w n="35.4">d</w>’<w n="35.5">un</w> <w n="35.6">clavecin</w> <w n="35.7">sans</w> <w n="35.8">cordes</w>.</l>
					<l n="36" num="1.36"><w n="36.1">O</w> <w n="36.2">mon</w> <w n="36.3">âme</w> ! <w n="36.4">pourquoi</w> <w n="36.5">faut</w>-<w n="36.6">il</w>, <w n="36.7">quand</w> <w n="36.8">tu</w> <w n="36.9">débordes</w>,</l>
					<l n="37" num="1.37"><w n="37.1">Comme</w> <w n="37.2">un</w> <w n="37.3">lis</w> <w n="37.4">rempli</w> <w n="37.5">d</w>’<w n="37.6">eau</w> <w n="37.7">que</w> <w n="37.8">le</w> <w n="37.9">vent</w> <w n="37.10">fait</w> <w n="37.11">pencher</w>,</l>
					<l n="38" num="1.38"><w n="38.1">Que</w> <w n="38.2">l</w>’<w n="38.3">âme</w> <w n="38.4">où</w> <w n="38.5">tout</w> <w n="38.6">en</w> <w n="38.7">pleurs</w> <w n="38.8">tu</w> <w n="38.9">voudrais</w> <w n="38.10">t</w>’<w n="38.11">épancher</w></l>
					<l n="39" num="1.39"><w n="39.1">Se</w> <w n="39.2">ferme</w> <w n="39.3">et</w> <w n="39.4">te</w> <w n="39.5">repousse</w>, <w n="39.6">et</w> <w n="39.7">te</w> <w n="39.8">laisse</w> <w n="39.9">répandre</w></l>
					<l n="40" num="1.40"><w n="40.1">Tes</w> <w n="40.2">plus</w> <w n="40.3">divins</w> <w n="40.4">parfums</w> <w n="40.5">sans</w> <w n="40.6">en</w> <w n="40.7">vouloir</w> <w n="40.8">rien</w> <w n="40.9">prendre</w> !</l>
					<l n="41" num="1.41"><w n="41.1">J</w>’<w n="41.2">ai</w> <w n="41.3">cherché</w> <w n="41.4">vainement</w> <w n="41.5">pourquoi</w> <w n="41.6">cette</w> <w n="41.7">froideur</w>,</l>
					<l n="42" num="1.42"><w n="42.1">Après</w> <w n="42.2">tant</w> <w n="42.3">de</w> <w n="42.4">baisers</w> <w n="42.5">vivants</w> <w n="42.6">et</w> <w n="42.7">pleins</w> <w n="42.8">d</w>’<w n="42.9">ardeur</w>,</l>
					<l n="43" num="1.43"><w n="43.1">Après</w> <w n="43.2">tant</w> <w n="43.3">de</w> <w n="43.4">serments</w> <w n="43.5">et</w> <w n="43.6">de</w> <w n="43.7">douces</w> <w n="43.8">paroles</w>,</l>
					<l n="44" num="1.44"><w n="44.1">Tant</w> <w n="44.2">de</w> <w n="44.3">soupirs</w> <w n="44.4">d</w>’<w n="44.5">ivresse</w> <w n="44.6">et</w> <w n="44.7">de</w> <w n="44.8">caresses</w> <w n="44.9">folles</w> ;</l>
					<l n="45" num="1.45"><w n="45.1">Je</w> <w n="45.2">n</w>’<w n="45.3">ai</w> <w n="45.4">rien</w> <w n="45.5">pu</w> <w n="45.6">trouver</w> <w n="45.7">autre</w> <w n="45.8">chose</w>, <w n="45.9">sinon</w></l>
					<l n="46" num="1.46"><w n="46.1">Qu</w>’<w n="46.2">on</w> <w n="46.3">était</w> <w n="46.4">fou</w> <w n="46.5">d</w>’<w n="46.6">avoir</w> <w n="46.7">au</w> <w n="46.8">fond</w> <w n="46.9">du</w> <w n="46.10">cœur</w> <w n="46.11">un</w> <w n="46.12">nom</w></l>
					<l n="47" num="1.47"><w n="47.1">Que</w> <w n="47.2">l</w>’<w n="47.3">on</w> <w n="47.4">ne</w> <w n="47.5">dira</w> <w n="47.6">pas</w>, <w n="47.7">et</w> <w n="47.8">que</w> <w n="47.9">c</w>’<w n="47.10">était</w> <w n="47.11">chimère</w></l>
					<l n="48" num="1.48"><w n="48.1">D</w>’<w n="48.2">aimer</w> <w n="48.3">une</w> <w n="48.4">autre</w> <w n="48.5">femme</w> <w n="48.6">au</w> <w n="48.7">monde</w> <w n="48.8">que</w> <w n="48.9">sa</w> <w n="48.10">mère</w>.</l>
					<l n="49" num="1.49"><w n="49.1">Rousseau</w> <w n="49.2">dit</w> <w n="49.3">quelque</w> <w n="49.4">part</w> : — <w n="49.5">Regardez</w> <w n="49.6">votre</w> <w n="49.7">amant</w></l>
					<l n="50" num="1.50"><w n="50.1">Au</w> <w n="50.2">sortir</w> <w n="50.3">de</w> <w n="50.4">vos</w> <w n="50.5">bras</w>. — <w n="50.6">Il</w> <w n="50.7">a</w> <w n="50.8">raison</w> <w n="50.9">vraiment</w>.</l>
					<l n="51" num="1.51"><w n="51.1">Lorsque</w>, <w n="51.2">le</w> <w n="51.3">désir</w> <w n="51.4">mort</w>, <w n="51.5">naît</w> <w n="51.6">la</w> <w n="51.7">mélancolie</w>,</l>
					<l n="52" num="1.52"><w n="52.1">Que</w> <w n="52.2">l</w>’<w n="52.3">amour</w> <w n="52.4">satisfait</w> <w n="52.5">se</w> <w n="52.6">recueille</w> <w n="52.7">et</w> <w n="52.8">s</w>’<w n="52.9">oublie</w>,</l>
					<l n="53" num="1.53"><w n="53.1">Comme</w> <w n="53.2">au</w> <w n="53.3">sein</w> <w n="53.4">de</w> <w n="53.5">sa</w> <w n="53.6">mère</w> <w n="53.7">un</w> <w n="53.8">enfant</w> <w n="53.9">qui</w> <w n="53.10">s</w>’<w n="53.11">endort</w>,</l>
					<l n="54" num="1.54"><w n="54.1">Que</w> <w n="54.2">l</w>’<w n="54.3">ennui</w> <w n="54.4">vient</w> <w n="54.5">d</w>’<w n="54.6">entrer</w> <w n="54.7">et</w> <w n="54.8">que</w> <w n="54.9">le</w> <w n="54.10">plaisir</w> <w n="54.11">sort</w>,</l>
					<l n="55" num="1.55"><w n="55.1">Le</w> <w n="55.2">moment</w> <w n="55.3">est</w> <w n="55.4">venu</w> <w n="55.5">de</w> <w n="55.6">regarder</w> <w n="55.7">en</w> <w n="55.8">face</w></l>
					<l n="56" num="1.56"><w n="56.1">L</w>’<w n="56.2">amant</w> <w n="56.3">qu</w>’<w n="56.4">on</w> <w n="56.5">s</w>’<w n="56.6">est</w> <w n="56.7">choisi</w>. <w n="56.8">Quoi</w> <w n="56.9">qu</w>’<w n="56.10">il</w> <w n="56.11">dise</w> <w n="56.12">ou</w> <w n="56.13">qu</w>’<w n="56.14">il</w> <w n="56.15">fasse</w>,</l>
					<l n="57" num="1.57"><w n="57.1">Vous</w> <w n="57.2">lirez</w> <w n="57.3">sur</w> <w n="57.4">son</w> <w n="57.5">front</w> <w n="57.6">son</w> <w n="57.7">amour</w> <w n="57.8">tel</w> <w n="57.9">qu</w>’<w n="57.10">il</w> <w n="57.11">est</w>.</l>
					<l n="58" num="1.58"><w n="58.1">Le</w> <w n="58.2">mot</w> <w n="58.3">sans</w> <w n="58.4">doute</w> <w n="58.5">est</w> <w n="58.6">beau</w>, <w n="58.7">mais</w> <w n="58.8">ce</w> <w n="58.9">qui</w> <w n="58.10">m</w>’<w n="58.11">en</w> <w n="58.12">déplaît</w>,</l>
					<l n="59" num="1.59"><w n="59.1">C</w>’<w n="59.2">est</w> <w n="59.3">qu</w>’<w n="59.4">il</w> <w n="59.5">s</w>’<w n="59.6">adresse</w> <w n="59.7">à</w> <w n="59.8">l</w>’<w n="59.9">homme</w> <w n="59.10">et</w> <w n="59.11">non</w> <w n="59.12">pas</w> <w n="59.13">à</w> <w n="59.14">la</w> <w n="59.15">femme</w>.</l>
					<l n="60" num="1.60"><w n="60.1">Quand</w> <w n="60.2">le</w> <w n="60.3">corps</w> <w n="60.4">assouvi</w> <w n="60.5">laisse</w> <w n="60.6">en</w> <w n="60.7">paix</w> <w n="60.8">régner</w> <w n="60.9">l</w>’<w n="60.10">âme</w>,</l>
					<l n="61" num="1.61"><w n="61.1">Qu</w>’<w n="61.2">on</w> <w n="61.3">s</w>’<w n="61.4">écoute</w> <w n="61.5">penser</w> <w n="61.6">et</w> <w n="61.7">qu</w>’<w n="61.8">on</w> <w n="61.9">entend</w> <w n="61.10">son</w> <w n="61.11">cœur</w>,</l>
					<l n="62" num="1.62"><w n="62.1">Et</w> <w n="62.2">que</w> <w n="62.3">dans</w> <w n="62.4">la</w> <w n="62.5">maîtresse</w> <w n="62.6">on</w> <w n="62.7">embrasse</w> <w n="62.8">la</w> <w n="62.9">sœur</w>,</l>
					<l n="63" num="1.63"><w n="63.1">La</w> <w n="63.2">première</w> <w n="63.3">lassée</w> <w n="63.4">est</w> <w n="63.5">la</w> <w n="63.6">femme</w>. <w n="63.7">La</w> <w n="63.8">honte</w></l>
					<l n="64" num="1.64"><w n="64.1">D</w>’<w n="64.2">avoir</w> <w n="64.3">été</w> <w n="64.4">vaincue</w> <w n="64.5">au</w> <w n="64.6">fond</w> <w n="64.7">d</w>’<w n="64.8">elle</w> <w n="64.9">surmonte</w></l>
					<l n="65" num="1.65"><w n="65.1">Le</w> <w n="65.2">bonheur</w> <w n="65.3">d</w>’<w n="65.4">être</w> <w n="65.5">aimée</w> ; <w n="65.6">elle</w> <w n="65.7">hait</w> <w n="65.8">son</w> <w n="65.9">amant</w>,</l>
					<l n="66" num="1.66"><w n="66.1">Comme</w> <w n="66.2">on</w> <w n="66.3">hait</w> <w n="66.4">un</w> <w n="66.5">vainqueur</w>, <w n="66.6">et</w>, <w n="66.7">certe</w>, <w n="66.8">en</w> <w n="66.9">ce</w> <w n="66.10">moment</w></l>
					<l n="67" num="1.67"><w n="67.1">Les</w> <w n="67.2">choses</w> <w n="67.3">sont</w> <w n="67.4">ainsi</w> ; <w n="67.5">s</w>’<w n="67.6">il</w> <w n="67.7">est</w> <w n="67.8">quelqu</w>’<w n="67.9">un</w> <w n="67.10">au</w> <w n="67.11">monde</w></l>
					<l n="68" num="1.68"><w n="68.1">Qu</w>’<w n="68.2">elle</w> <w n="68.3">haïsse</w> <w n="68.4">bien</w> <w n="68.5">et</w> <w n="68.6">de</w> <w n="68.7">haine</w> <w n="68.8">profonde</w>,</l>
					<l n="69" num="1.69"><w n="69.1">C</w>’<w n="69.2">est</w> <w n="69.3">lui</w>, <w n="69.4">car</w> <w n="69.5">c</w>’<w n="69.6">est</w> <w n="69.7">son</w> <w n="69.8">maître</w> <w n="69.9">et</w> <w n="69.10">son</w> <w n="69.11">seigneur</w> ; <w n="69.12">il</w> <w n="69.13">peut</w></l>
					<l n="70" num="1.70"><w n="70.1">Divulguer</w> <w n="70.2">tout</w> ; <w n="70.3">il</w> <w n="70.4">peut</w> <w n="70.5">la</w> <w n="70.6">perdre</w> <w n="70.7">s</w>’<w n="70.8">il</w> <w n="70.9">le</w> <w n="70.10">veut</w> ;</l>
					<l n="71" num="1.71"><w n="71.1">Il</w> <w n="71.2">ne</w> <w n="71.3">le</w> <w n="71.4">voudra</w> <w n="71.5">pas</w>, <w n="71.6">mais</w> <w n="71.7">il</w> <w n="71.8">le</w> <w n="71.9">peut</w>. <w n="71.10">La</w> <w n="71.11">crainte</w></l>
					<l n="72" num="1.72"><w n="72.1">A</w> <w n="72.2">remplacé</w> <w n="72.3">l</w>’<w n="72.4">amour</w> ; <w n="72.5">une</w> <w n="72.6">froide</w> <w n="72.7">contrainte</w></l>
					<l n="73" num="1.73"><w n="73.1">Succède</w> <w n="73.2">aux</w> <w n="73.3">beaux</w> <w n="73.4">élans</w> <w n="73.5">de</w> <w n="73.6">folle</w> <w n="73.7">liberté</w>.</l>
					<l n="74" num="1.74"><w n="74.1">Adieu</w> <w n="74.2">l</w>’<w n="74.3">enivrement</w>, <w n="74.4">le</w> <w n="74.5">rire</w> <w n="74.6">et</w> <w n="74.7">la</w> <w n="74.8">gaîté</w>.</l>
					<l n="75" num="1.75"><w n="75.1">La</w> <w n="75.2">femme</w> <w n="75.3">se</w> <w n="75.4">repent</w> <w n="75.5">et</w> <w n="75.6">l</w>’<w n="75.7">homme</w> <w n="75.8">se</w> <w n="75.9">repose</w> :</l>
					<l n="76" num="1.76"><w n="76.1">Il</w> <w n="76.2">a</w> <w n="76.3">touché</w> <w n="76.4">son</w> <w n="76.5">but</w>, <w n="76.6">il</w> <w n="76.7">a</w> <w n="76.8">gagné</w> <w n="76.9">sa</w> <w n="76.10">cause</w> ;</l>
					<l n="77" num="1.77"><w n="77.1">C</w>’<w n="77.2">est</w> <w n="77.3">le</w> <w n="77.4">triomphateur</w>, <w n="77.5">le</w> <w n="77.6">vainqueur</w>, <w n="77.7">le</w> <w n="77.8">César</w>,</l>
					<l n="78" num="1.78"><w n="78.1">Qui</w>, <w n="78.2">la</w> <w n="78.3">couronne</w> <w n="78.4">au</w> <w n="78.5">front</w>, <w n="78.6">au</w>-<w n="78.7">devant</w> <w n="78.8">de</w> <w n="78.9">son</w> <w n="78.10">char</w>,</l>
					<l n="79" num="1.79"><w n="79.1">Malgré</w> <w n="79.2">tout</w> <w n="79.3">son</w> <w n="79.4">amour</w>, <w n="79.5">s</w>’<w n="79.6">il</w> <w n="79.7">peut</w> <w n="79.8">la</w> <w n="79.9">prendre</w> <w n="79.10">vive</w>,</l>
					<l n="80" num="1.80"><w n="80.1">Traînera</w> <w n="80.2">sans</w> <w n="80.3">pitié</w> <w n="80.4">Cléopâtre</w> <w n="80.5">captive</w>.</l>
					<l n="81" num="1.81"><w n="81.1">Aspic</w>, <w n="81.2">dresse</w> <w n="81.3">ton</w> <w n="81.4">col</w> <w n="81.5">tout</w> <w n="81.6">gonflé</w> <w n="81.7">de</w> <w n="81.8">venin</w> :</l>
					<l n="82" num="1.82"><w n="82.1">Sors</w> <w n="82.2">du</w> <w n="82.3">panier</w> <w n="82.4">de</w> <w n="82.5">fleurs</w>, <w n="82.6">siffle</w> <w n="82.7">et</w> <w n="82.8">mords</w> <w n="82.9">ce</w> <w n="82.10">beau</w> <w n="82.11">sein</w>.</l>
					<l n="83" num="1.83"><w n="83.1">César</w> <w n="83.2">attend</w> <w n="83.3">dehors</w> ! <w n="83.4">il</w> <w n="83.5">lui</w> <w n="83.6">faut</w> <w n="83.7">Cléopâtre</w></l>
					<l n="84" num="1.84"><w n="84.1">Pour</w> <w n="84.2">suivre</w> <w n="84.3">le</w> <w n="84.4">triomphe</w> <w n="84.5">et</w> <w n="84.6">paraître</w> <w n="84.7">au</w> <w n="84.8">théâtre</w> ;</l>
					<l n="85" num="1.85"><w n="85.1">Il</w> <w n="85.2">faut</w> <w n="85.3">que</w> <w n="85.4">sur</w> <w n="85.5">leurs</w> <w n="85.6">bancs</w> <w n="85.7">les</w> <w n="85.8">chevaliers</w> <w n="85.9">romains</w></l>
					<l n="86" num="1.86"><w n="86.1">Disent</w> : — <w n="86.2">Heureux</w> <w n="86.3">César</w> ! <w n="86.4">et</w> <w n="86.5">lui</w> <w n="86.6">battent</w> <w n="86.7">des</w> <w n="86.8">mains</w>.</l>
					<l n="87" num="1.87"><w n="87.1">La</w> <w n="87.2">femme</w> <w n="87.3">sait</w> <w n="87.4">cela</w>, <w n="87.5">que</w> <w n="87.6">de</w> <w n="87.7">reine</w> <w n="87.8">et</w> <w n="87.9">maîtresse</w></l>
					<l n="88" num="1.88"><w n="88.1">Elle</w> <w n="88.2">devient</w> <w n="88.3">esclave</w>, <w n="88.4">et</w> <w n="88.5">que</w> <w n="88.6">son</w> <w n="88.7">pouvoir</w> <w n="88.8">cesse</w> ;</l>
					<l n="89" num="1.89"><w n="89.1">Mais</w> <w n="89.2">le</w> <w n="89.3">sceptre</w> <w n="89.4">qu</w>’<w n="89.5">hier</w>, <w n="89.6">dans</w> <w n="89.7">l</w>’<w n="89.8">oubli</w> <w n="89.9">du</w> <w n="89.10">plaisir</w>,</l>
					<l n="90" num="1.90"><w n="90.1">Elle</w> <w n="90.2">a</w> <w n="90.3">laissé</w> <w n="90.4">tomber</w>, <w n="90.5">aujourd</w>’<w n="90.6">hui</w> <w n="90.7">le</w> <w n="90.8">désir</w></l>
					<l n="91" num="1.91"><w n="91.1">Le</w> <w n="91.2">lui</w> <w n="91.3">remet</w> <w n="91.4">en</w> <w n="91.5">main</w> <w n="91.6">et</w> <w n="91.7">la</w> <w n="91.8">fait</w> <w n="91.9">souveraine</w>.</l>
					<l n="92" num="1.92"><w n="92.1">Il</w> <w n="92.2">faut</w> <w n="92.3">que</w> <w n="92.4">son</w> <w n="92.5">amant</w> <w n="92.6">à</w> <w n="92.7">ses</w> <w n="92.8">genoux</w> <w n="92.9">se</w> <w n="92.10">traîne</w></l>
					<l n="93" num="1.93"><w n="93.1">Et</w> <w n="93.2">lui</w> <w n="93.3">baise</w> <w n="93.4">les</w> <w n="93.5">pieds</w> <w n="93.6">et</w> <w n="93.7">demande</w> <w n="93.8">pardon</w>.</l>
					<l n="94" num="1.94"><w n="94.1">Mais</w> <w n="94.2">elle</w> <w n="94.3">maintenant</w>, <w n="94.4">froide</w> <w n="94.5">et</w> <w n="94.6">sans</w> <w n="94.7">abandon</w>,</l>
					<l n="95" num="1.95"><w n="95.1">Avec</w> <w n="95.2">un</w> <w n="95.3">double</w> <w n="95.4">fil</w> <w n="95.5">nouant</w> <w n="95.6">son</w> <w n="95.7">nouveau</w> <w n="95.8">masque</w>,</l>
					<l n="96" num="1.96"><w n="96.1">Ainsi</w> <w n="96.2">qu</w>’<w n="96.3">un</w> <w n="96.4">chevalier</w> <w n="96.5">à</w> <w n="96.6">l</w>’<w n="96.7">abri</w> <w n="96.8">sous</w> <w n="96.9">son</w> <w n="96.10">casque</w>,</l>
					<l n="97" num="1.97"><w n="97.1">Guette</w> <w n="97.2">à</w> <w n="97.3">couvert</w> <w n="97.4">l</w>’<w n="97.5">instant</w> <w n="97.6">où</w>, <w n="97.7">faible</w> <w n="97.8">et</w> <w n="97.9">désarmé</w>,</l>
					<l n="98" num="1.98"><w n="98.1">Se</w> <w n="98.2">livre</w> <w n="98.3">à</w> <w n="98.4">son</w> <w n="98.5">poignard</w> <w n="98.6">l</w>’<w n="98.7">amant</w> <w n="98.8">qu</w>’<w n="98.9">on</w> <w n="98.10">croit</w> <w n="98.11">aimé</w>.</l>
					<l n="99" num="1.99"><w n="99.1">Mon</w> <w n="99.2">ange</w>, <w n="99.3">n</w>’<w n="99.4">est</w>-<w n="99.5">ce</w> <w n="99.6">pas</w> <w n="99.7">qu</w>’<w n="99.8">une</w> <w n="99.9">telle</w> <w n="99.10">pensée</w></l>
					<l n="100" num="1.100"><w n="100.1">N</w>’<w n="100.2">eût</w> <w n="100.3">pas</w> <w n="100.4">dû</w> <w n="100.5">me</w> <w n="100.6">venir</w> <w n="100.7">et</w> <w n="100.8">doit</w> <w n="100.9">être</w> <w n="100.10">chassée</w>,</l>
					<l n="101" num="1.101"><w n="101.1">Et</w> <w n="101.2">que</w> <w n="101.3">je</w> <w n="101.4">suis</w> <w n="101.5">bien</w> <w n="101.6">fou</w> <w n="101.7">de</w> <w n="101.8">douter</w> <w n="101.9">d</w>’<w n="101.10">un</w> <w n="101.11">amour</w></l>
					<l n="102" num="1.102"><w n="102.1">Dont</w> <w n="102.2">personne</w> <w n="102.3">ne</w> <w n="102.4">doute</w>, <w n="102.5">et</w> <w n="102.6">prouvé</w> <w n="102.7">chaque</w> <w n="102.8">jour</w> ?</l>
					<l n="103" num="1.103"><w n="103.1">J</w>’<w n="103.2">ai</w> <w n="103.3">tort</w> ; <w n="103.4">mais</w> <w n="103.5">que</w> <w n="103.6">veux</w>-<w n="103.7">tu</w> ? <w n="103.8">ces</w> <w n="103.9">angoisses</w> <w n="103.10">si</w> <w n="103.11">vives</w>,</l>
					<l n="104" num="1.104"><w n="104.1">Ces</w> <w n="104.2">haines</w>, <w n="104.3">ces</w> <w n="104.4">retours</w> <w n="104.5">et</w> <w n="104.6">ces</w> <w n="104.7">alternatives</w>,</l>
					<l n="105" num="1.105"><w n="105.1">Ces</w> <w n="105.2">désespoirs</w> <w n="105.3">mortels</w> <w n="105.4">suivis</w> <w n="105.5">d</w>’<w n="105.6">espoirs</w> <w n="105.7">charmants</w>,</l>
					<l n="106" num="1.106"><w n="106.1">C</w>’<w n="106.2">est</w> <w n="106.3">l</w>’<w n="106.4">amour</w>, <w n="106.5">c</w>’<w n="106.6">est</w> <w n="106.7">ainsi</w> <w n="106.8">que</w> <w n="106.9">vivent</w> <w n="106.10">les</w> <w n="106.11">amants</w>.</l>
					<l n="107" num="1.107"><w n="107.1">Cette</w> <w n="107.2">existence</w>-<w n="107.3">là</w>, <w n="107.4">c</w>’<w n="107.5">est</w> <w n="107.6">la</w> <w n="107.7">mienne</w>, <w n="107.8">la</w> <w n="107.9">nôtre</w> ;</l>
					<l n="108" num="1.108"><w n="108.1">Telle</w> <w n="108.2">qu</w>’<w n="108.3">elle</w> <w n="108.4">est</w>, <w n="108.5">pourtant</w>, <w n="108.6">je</w> <w n="108.7">n</w>’<w n="108.8">en</w> <w n="108.9">voudrais</w> <w n="108.10">pas</w> <w n="108.11">d</w>’<w n="108.12">autre</w>.</l>
					<l n="109" num="1.109"><w n="109.1">On</w> <w n="109.2">est</w> <w n="109.3">bien</w> <w n="109.4">malheureux</w> ; <w n="109.5">mais</w> <w n="109.6">pour</w> <w n="109.7">un</w> <w n="109.8">tel</w> <w n="109.9">malheur</w></l>
					<l n="110" num="1.110"><w n="110.1">Les</w> <w n="110.2">heureux</w> <w n="110.3">volontiers</w> <w n="110.4">changeraient</w> <w n="110.5">leur</w> <w n="110.6">bonheur</w>.</l>
					<l n="111" num="1.111"><w n="111.1">Aimer</w> ! <w n="111.2">ce</w> <w n="111.3">mot</w>-<w n="111.4">là</w> <w n="111.5">seul</w> <w n="111.6">contient</w> <w n="111.7">toute</w> <w n="111.8">la</w> <w n="111.9">vie</w>.</l>
					<l n="112" num="1.112"><w n="112.1">Près</w> <w n="112.2">de</w> <w n="112.3">l</w>’<w n="112.4">amour</w> <w n="112.5">que</w> <w n="112.6">sont</w> <w n="112.7">les</w> <w n="112.8">choses</w> <w n="112.9">qu</w>’<w n="112.10">on</w> <w n="112.11">envie</w> ?</l>
					<l n="113" num="1.113"><w n="113.1">Trésors</w>, <w n="113.2">sceptres</w>, <w n="113.3">lauriers</w>, <w n="113.4">qu</w>’<w n="113.5">est</w> <w n="113.6">tout</w> <w n="113.7">cela</w>, <w n="113.8">mon</w> <w n="113.9">Dieu</w> !</l>
					<l n="114" num="1.114"><w n="114.1">Comme</w> <w n="114.2">la</w> <w n="114.3">gloire</w> <w n="114.4">est</w> <w n="114.5">creuse</w> <w n="114.6">et</w> <w n="114.7">vous</w> <w n="114.8">contente</w> <w n="114.9">peu</w> !</l>
					<l n="115" num="1.115"><w n="115.1">L</w>’<w n="115.2">amour</w> <w n="115.3">seul</w> <w n="115.4">peut</w> <w n="115.5">combler</w> <w n="115.6">les</w> <w n="115.7">profondeurs</w> <w n="115.8">de</w> <w n="115.9">l</w>’<w n="115.10">âme</w></l>
					<l n="116" num="1.116"><w n="116.1">Et</w> <w n="116.2">toute</w> <w n="116.3">ambition</w> <w n="116.4">meurt</w> <w n="116.5">aux</w> <w n="116.6">bras</w> <w n="116.7">d</w>’<w n="116.8">une</w> <w n="116.9">femme</w> !</l>
				</lg>
			</div></body></text></TEI>