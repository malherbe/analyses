<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES DIVERSES, 1833-1838</title>
				<title type="sub_2">Tome premier</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3874 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Premier</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/files/44180/44180-h/44180-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome premier</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1838">1833-1838</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU202">
				<head type="main">DESTINÉE</head>
				<head type="form">SONNET</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Comme</w> <w n="1.2">la</w> <w n="1.3">vie</w> <w n="1.4">est</w> <w n="1.5">faite</w> ! <w n="1.6">et</w> <w n="1.7">que</w> <w n="1.8">le</w> <w n="1.9">train</w> <w n="1.10">du</w> <w n="1.11">monde</w></l>
					<l n="2" num="1.2"><w n="2.1">Nous</w> <w n="2.2">pousse</w> <w n="2.3">aveuglément</w> <w n="2.4">en</w> <w n="2.5">des</w> <w n="2.6">chemins</w> <w n="2.7">divers</w> !</l>
					<l n="3" num="1.3"><w n="3.1">Pareil</w> <w n="3.2">au</w> <w n="3.3">Juif</w> <w n="3.4">maudit</w>, <w n="3.5">l</w>’<w n="3.6">un</w>, <w n="3.7">par</w> <w n="3.8">tout</w> <w n="3.9">l</w>’<w n="3.10">univers</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Promène</w> <w n="4.2">sans</w> <w n="4.3">repos</w> <w n="4.4">sa</w> <w n="4.5">course</w> <w n="4.6">vagabonde</w> ;</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">L</w>’<w n="5.2">autre</w>, <w n="5.3">vrai</w> <w n="5.4">docteur</w> <w n="5.5">Faust</w>, <w n="5.6">baigné</w> <w n="5.7">d</w>’<w n="5.8">ombre</w> <w n="5.9">profonde</w>,</l>
					<l n="6" num="2.2"><w n="6.1">Auprès</w> <w n="6.2">de</w> <w n="6.3">sa</w> <w n="6.4">croisée</w> <w n="6.5">étroite</w>, <w n="6.6">à</w> <w n="6.7">carreaux</w> <w n="6.8">verts</w>,</l>
					<l n="7" num="2.3"><w n="7.1">Poursuit</w> <w n="7.2">de</w> <w n="7.3">son</w> <w n="7.4">fauteuil</w> <w n="7.5">quelques</w> <w n="7.6">rêves</w> <w n="7.7">amers</w>,</l>
					<l n="8" num="2.4"><w n="8.1">Et</w> <w n="8.2">dans</w> <w n="8.3">l</w>’<w n="8.4">âme</w> <w n="8.5">sans</w> <w n="8.6">fond</w> <w n="8.7">laisse</w> <w n="8.8">filer</w> <w n="8.9">la</w> <w n="8.10">sonde</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Eh</w> <w n="9.2">bien</w> ! <w n="9.3">celui</w> <w n="9.4">qui</w> <w n="9.5">court</w> <w n="9.6">sur</w> <w n="9.7">la</w> <w n="9.8">terre</w> <w n="9.9">était</w> <w n="9.10">né</w></l>
					<l n="10" num="3.2"><w n="10.1">Pour</w> <w n="10.2">vivre</w> <w n="10.3">au</w> <w n="10.4">coin</w> <w n="10.5">du</w> <w n="10.6">feu</w> : <w n="10.7">le</w> <w n="10.8">foyer</w>, <w n="10.9">la</w> <w n="10.10">famille</w>,</l>
					<l n="11" num="3.3"><w n="11.1">C</w>’<w n="11.2">était</w> <w n="11.3">son</w> <w n="11.4">vœu</w> ; <w n="11.5">mais</w> <w n="11.6">Dieu</w> <w n="11.7">ne</w> <w n="11.8">l</w>’<w n="11.9">a</w> <w n="11.10">pas</w> <w n="11.11">couronné</w>.</l>
				</lg>
				<lg n="4">
					<l n="12" num="4.1"><w n="12.1">Et</w> <w n="12.2">l</w>’<w n="12.3">autre</w>, <w n="12.4">qui</w> <w n="12.5">n</w>’<w n="12.6">a</w> <w n="12.7">vu</w> <w n="12.8">du</w> <w n="12.9">ciel</w> <w n="12.10">que</w> <w n="12.11">ce</w> <w n="12.12">qui</w> <w n="12.13">brille</w></l>
					<l n="13" num="4.2"><w n="13.1">Par</w> <w n="13.2">le</w> <w n="13.3">trou</w> <w n="13.4">du</w> <w n="13.5">volet</w>, <w n="13.6">était</w> <w n="13.7">le</w> <w n="13.8">voyageur</w>.</l>
					<l n="14" num="4.3"><w n="14.1">Ils</w> <w n="14.2">ont</w> <w n="14.3">passé</w> <w n="14.4">tous</w> <w n="14.5">deux</w> <w n="14.6">à</w> <w n="14.7">côté</w> <w n="14.8">du</w> <w n="14.9">bonheur</w>.</l>
				</lg>
			</div></body></text></TEI>