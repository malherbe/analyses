<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES DIVERSES, 1833-1838</title>
				<title type="sub_2">Tome premier</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3874 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Premier</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/files/44180/44180-h/44180-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome premier</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1838">1833-1838</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU171">
				<head type="main">LES COLOMBES</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Sur</w> <w n="1.2">le</w> <w n="1.3">coteau</w>, <w n="1.4">là</w>-<w n="1.5">bas</w> <w n="1.6">où</w> <w n="1.7">sont</w> <w n="1.8">les</w> <w n="1.9">tombes</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Un</w> <w n="2.2">beau</w> <w n="2.3">palmier</w>, <w n="2.4">comme</w> <w n="2.5">un</w> <w n="2.6">panache</w> <w n="2.7">vert</w></l>
					<l n="3" num="1.3"><w n="3.1">Dresse</w> <w n="3.2">sa</w> <w n="3.3">tête</w>, <w n="3.4">où</w> <w n="3.5">le</w> <w n="3.6">soir</w> <w n="3.7">les</w> <w n="3.8">colombes</w></l>
					<l n="4" num="1.4"><w n="4.1">Viennent</w> <w n="4.2">nicher</w> <w n="4.3">et</w> <w n="4.4">se</w> <w n="4.5">mettre</w> <w n="4.6">à</w> <w n="4.7">couvert</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Mais</w> <w n="5.2">le</w> <w n="5.3">matin</w> <w n="5.4">elles</w> <w n="5.5">quittent</w> <w n="5.6">les</w> <w n="5.7">branches</w> :</l>
					<l n="6" num="2.2"><w n="6.1">Comme</w> <w n="6.2">un</w> <w n="6.3">collier</w> <w n="6.4">qui</w> <w n="6.5">s</w>’<w n="6.6">égrène</w>, <w n="6.7">on</w> <w n="6.8">les</w> <w n="6.9">voit</w></l>
					<l n="7" num="2.3"><w n="7.1">S</w>’<w n="7.2">éparpiller</w> <w n="7.3">dans</w> <w n="7.4">l</w>’<w n="7.5">air</w> <w n="7.6">bleu</w>, <w n="7.7">toutes</w> <w n="7.8">blanches</w>,</l>
					<l n="8" num="2.4"><w n="8.1">Et</w> <w n="8.2">se</w> <w n="8.3">poser</w> <w n="8.4">plus</w> <w n="8.5">loin</w> <w n="8.6">sur</w> <w n="8.7">quelque</w> <w n="8.8">toit</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Mon</w> <w n="9.2">âme</w> <w n="9.3">est</w> <w n="9.4">l</w>’<w n="9.5">arbre</w> <w n="9.6">où</w> <w n="9.7">tous</w> <w n="9.8">les</w> <w n="9.9">soirs</w>, <w n="9.10">comme</w> <w n="9.11">elles</w>,</l>
					<l n="10" num="3.2"><w n="10.1">De</w> <w n="10.2">blancs</w> <w n="10.3">essaims</w> <w n="10.4">de</w> <w n="10.5">folles</w> <w n="10.6">visions</w></l>
					<l n="11" num="3.3"><w n="11.1">Tombent</w> <w n="11.2">des</w> <w n="11.3">cieux</w>, <w n="11.4">en</w> <w n="11.5">palpitant</w> <w n="11.6">des</w> <w n="11.7">ailes</w>,</l>
					<l n="12" num="3.4"><w n="12.1">Pour</w> <w n="12.2">s</w>’<w n="12.3">envoler</w> <w n="12.4">dès</w> <w n="12.5">les</w> <w n="12.6">premiers</w> <w n="12.7">rayons</w>.</l>
				</lg>
			</div></body></text></TEI>