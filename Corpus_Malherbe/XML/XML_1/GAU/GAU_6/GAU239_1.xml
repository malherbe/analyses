<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES DIVERSES, 1838-1845</title>
				<title type="sub_2">Tome second</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>625 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_6</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Second</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://gutenberg.org/files/45886/45886-h/45886-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Second</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1890">1890</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1845">1838-1845</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU239">
			<head type="main">L’ESCLAVE</head>
			<lg n="1">
				<l n="1" num="1.1"><w n="1.1">Captive</w> <w n="1.2">et</w> <w n="1.3">peut</w>-<w n="1.4">être</w> <w n="1.5">oubliée</w>,</l>
				<l n="2" num="1.2"><w n="2.1">Je</w> <w n="2.2">songe</w> <w n="2.3">à</w> <w n="2.4">mes</w> <w n="2.5">jeunes</w> <w n="2.6">amours</w>,</l>
				<l n="3" num="1.3"><space quantity="4" unit="char"></space><w n="3.1">A</w> <w n="3.2">mes</w> <w n="3.3">beaux</w> <w n="3.4">jours</w>,</l>
				<l n="4" num="1.4"><w n="4.1">Et</w> <w n="4.2">par</w> <w n="4.3">la</w> <w n="4.4">fenêtre</w> <w n="4.5">grillée</w></l>
				<l n="5" num="1.5"><w n="5.1">Je</w> <w n="5.2">regarde</w> <w n="5.3">l</w>’<w n="5.4">oiseau</w> <w n="5.5">joyeux</w>,</l>
				<l n="6" num="1.6"><space quantity="4" unit="char"></space><w n="6.1">Fendant</w> <w n="6.2">les</w> <w n="6.3">cieux</w>.</l>
			</lg>
			<lg n="2">
				<l n="7" num="2.1"><w n="7.1">Douce</w> <w n="7.2">et</w> <w n="7.3">pâle</w> <w n="7.4">consolatrice</w>,</l>
				<l n="8" num="2.2"><w n="8.1">Espérance</w>, <w n="8.2">rayon</w> <w n="8.3">d</w>’<w n="8.4">en</w> <w n="8.5">haut</w>,</l>
				<l n="9" num="2.3"><space quantity="4" unit="char"></space><w n="9.1">Dans</w> <w n="9.2">mon</w> <w n="9.3">cachot</w>,</l>
				<l n="10" num="2.4"><w n="10.1">Fais</w>-<w n="10.2">moi</w>, <w n="10.3">sous</w> <w n="10.4">ta</w> <w n="10.5">clarté</w> <w n="10.6">propice</w>,</l>
				<l n="11" num="2.5"><w n="11.1">A</w> <w n="11.2">ton</w> <w n="11.3">miroir</w> <w n="11.4">faux</w> <w n="11.5">et</w> <w n="11.6">charmant</w></l>
				<l n="12" num="2.6"><space quantity="4" unit="char"></space><w n="12.1">Voir</w> <w n="12.2">mon</w> <w n="12.3">amant</w> !</l>
			</lg>
			<lg n="3">
				<l n="13" num="3.1"><w n="13.1">Auprès</w> <w n="13.2">de</w> <w n="13.3">lui</w>, <w n="13.4">belle</w> <w n="13.5">Espérance</w>,</l>
				<l n="14" num="3.2"><w n="14.1">Porte</w>-<w n="14.2">moi</w> <w n="14.3">sur</w> <w n="14.4">tes</w> <w n="14.5">ailes</w> <w n="14.6">d</w>’<w n="14.7">or</w>,</l>
				<l n="15" num="3.3"><space quantity="4" unit="char"></space><w n="15.1">S</w>’<w n="15.2">il</w> <w n="15.3">m</w>’<w n="15.4">aime</w> <w n="15.5">encor</w>,</l>
				<l n="16" num="3.4"><w n="16.1">Et</w>, <w n="16.2">pour</w> <w n="16.3">endormir</w> <w n="16.4">ma</w> <w n="16.5">souffrance</w>,</l>
				<l n="17" num="3.5"><w n="17.1">Suspends</w> <w n="17.2">mon</w> <w n="17.3">âme</w> <w n="17.4">sur</w> <w n="17.5">son</w> <w n="17.6">cœur</w></l>
				<l n="18" num="3.6"><space quantity="4" unit="char"></space><w n="18.1">Comme</w> <w n="18.2">une</w> <w n="18.3">fleur</w> !</l>
			</lg>
			<closer>
				<dateline>
					<date when="1840">1840</date>.
				</dateline>
			</closer>
		</div></body></text></TEI>