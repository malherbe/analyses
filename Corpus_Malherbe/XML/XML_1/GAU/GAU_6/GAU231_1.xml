<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES DIVERSES, 1838-1845</title>
				<title type="sub_2">Tome second</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>625 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_6</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Second</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://gutenberg.org/files/45886/45886-h/45886-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Second</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1890">1890</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1845">1838-1845</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU231">
			<head type="main">A TROIS PAYSAGISTES</head>
			<head type="sub_1">SALON DE 1839</head>
			<lg n="1">
				<l n="1" num="1.1"><w n="1.1">C</w>’<w n="1.2">est</w> <w n="1.3">un</w> <w n="1.4">bonheur</w> <w n="1.5">pour</w> <w n="1.6">nous</w>, <w n="1.7">hommes</w> <w n="1.8">de</w> <w n="1.9">la</w> <w n="1.10">critique</w>,</l>
				<l n="2" num="1.2"><w n="2.1">Qui</w>, <w n="2.2">le</w> <w n="2.3">collier</w> <w n="2.4">au</w> <w n="2.5">cou</w>, <w n="2.6">comme</w> <w n="2.7">l</w>’<w n="2.8">esclave</w> <w n="2.9">antique</w>,</l>
				<l n="3" num="1.3"><w n="3.1">Sans</w> <w n="3.2">trêve</w> <w n="3.3">et</w> <w n="3.4">sans</w> <w n="3.5">repos</w>, <w n="3.6">dans</w> <w n="3.7">le</w> <w n="3.8">moulin</w> <w n="3.9">banal</w></l>
				<l n="4" num="1.4"><w n="4.1">Tournons</w> <w n="4.2">aveuglément</w> <w n="4.3">la</w> <w n="4.4">meule</w> <w n="4.5">du</w> <w n="4.6">journal</w>,</l>
				<l n="5" num="1.5"><w n="5.1">Et</w> <w n="5.2">qui</w> <w n="5.3">vivons</w> <w n="5.4">perdus</w> <w n="5.5">dans</w> <w n="5.6">un</w> <w n="5.7">désert</w> <w n="5.8">de</w> <w n="5.9">plâtre</w>,</l>
				<l n="6" num="1.6"><w n="6.1">N</w>’<w n="6.2">ayant</w> <w n="6.3">d</w>’<w n="6.4">autre</w> <w n="6.5">soleil</w> <w n="6.6">qu</w>’<w n="6.7">un</w> <w n="6.8">lustre</w> <w n="6.9">de</w> <w n="6.10">théâtre</w> ;</l>
				<l n="7" num="1.7"><w n="7.1">Qu</w>’<w n="7.2">un</w> <w n="7.3">grand</w> <w n="7.4">paysagiste</w>, <w n="7.5">un</w> <w n="7.6">poëte</w> <w n="7.7">inspiré</w>,</l>
				<l n="8" num="1.8"><w n="8.1">Au</w> <w n="8.2">feuillage</w> <w n="8.3">abondant</w>, <w n="8.4">au</w> <w n="8.5">beau</w> <w n="8.6">ciel</w> <w n="8.7">azuré</w>,</l>
				<l n="9" num="1.9"><w n="9.1">Déchire</w> <w n="9.2">d</w>’<w n="9.3">un</w> <w n="9.4">rayon</w> <w n="9.5">la</w> <w n="9.6">nuit</w> <w n="9.7">qui</w> <w n="9.8">nous</w> <w n="9.9">inonde</w></l>
				<l n="10" num="1.10"><w n="10.1">Et</w> <w n="10.2">nous</w> <w n="10.3">fasse</w> <w n="10.4">un</w> <w n="10.5">portrait</w> <w n="10.6">de</w> <w n="10.7">la</w> <w n="10.8">beauté</w> <w n="10.9">du</w> <w n="10.10">monde</w>,</l>
				<l n="11" num="1.11"><w n="11.1">Pour</w> <w n="11.2">nous</w> <w n="11.3">montrer</w> <w n="11.4">qu</w>’<w n="11.5">il</w> <w n="11.6">est</w> <w n="11.7">encor</w> <w n="11.8">loin</w> <w n="11.9">des</w> <w n="11.10">cités</w>,</l>
				<l n="12" num="1.12"><w n="12.1">Malgré</w> <w n="12.2">les</w> <w n="12.3">feuilletons</w>, <w n="12.4">de</w> <w n="12.5">sévères</w> <w n="12.6">beautés</w></l>
				<l n="13" num="1.13"><w n="13.1">Que</w> <w n="13.2">du</w> <w n="13.3">livre</w> <w n="13.4">de</w> <w n="13.5">Dieu</w> <w n="13.6">la</w> <w n="13.7">main</w> <w n="13.8">de</w> <w n="13.9">l</w>’<w n="13.10">homme</w> <w n="13.11">efface</w> ;</l>
				<l n="14" num="1.14"><w n="14.1">De</w> <w n="14.2">l</w>’<w n="14.3">air</w>, <w n="14.4">de</w> <w n="14.5">l</w>’<w n="14.6">eau</w>, <w n="14.7">du</w> <w n="14.8">ciel</w>, <w n="14.9">des</w> <w n="14.10">arbres</w>, <w n="14.11">de</w> <w n="14.12">l</w>’<w n="14.13">espace</w>,</l>
				<l n="15" num="1.15"><w n="15.1">Et</w> <w n="15.2">des</w> <w n="15.3">prés</w> <w n="15.4">de</w> <w n="15.5">velours</w>, <w n="15.6">qu</w>’<w n="15.7">avril</w> <w n="15.8">étoile</w> <w n="15.9">encor</w></l>
				<l n="16" num="1.16"><w n="16.1">De</w> <w n="16.2">paillettes</w> <w n="16.3">d</w>’<w n="16.4">argent</w> <w n="16.5">et</w> <w n="16.6">d</w>’<w n="16.7">étincelles</w> <w n="16.8">d</w>’<w n="16.9">or</w>.</l>
				<l n="17" num="1.17">— <w n="17.1">Enfants</w> <w n="17.2">déshérités</w>, <w n="17.3">hélas</w> ! <w n="17.4">sans</w> <w n="17.5">la</w> <w n="17.6">peinture</w>,</l>
				<l n="18" num="1.18"><w n="18.1">Nous</w> <w n="18.2">pourrions</w> <w n="18.3">oublier</w> <w n="18.4">notre</w> <w n="18.5">mère</w> <w n="18.6">nature</w> ;</l>
				<l n="19" num="1.19"><w n="19.1">Nous</w> <w n="19.2">pourrions</w>, <w n="19.3">assourdis</w> <w n="19.4">du</w> <w n="19.5">vain</w> <w n="19.6">bourdonnement</w></l>
				<l n="20" num="1.20"><w n="20.1">Que</w> <w n="20.2">fait</w> <w n="20.3">la</w> <w n="20.4">presse</w> <w n="20.5">autour</w> <w n="20.6">de</w> <w n="20.7">tout</w> <w n="20.8">événement</w>,</l>
				<l n="21" num="1.21"><w n="21.1">Le</w> <w n="21.2">cœur</w> <w n="21.3">envenimé</w> <w n="21.4">de</w> <w n="21.5">futiles</w> <w n="21.6">querelles</w>,</l>
				<l n="22" num="1.22"><w n="22.1">Perdre</w> <w n="22.2">le</w> <w n="22.3">saint</w> <w n="22.4">amour</w> <w n="22.5">des</w> <w n="22.6">choses</w> <w n="22.7">éternelles</w>,</l>
				<l n="23" num="1.23"><w n="23.1">Et</w> <w n="23.2">ne</w> <w n="23.3">plus</w> <w n="23.4">rien</w> <w n="23.5">comprendre</w> <w n="23.6">à</w> <w n="23.7">l</w>’<w n="23.8">antique</w> <w n="23.9">beauté</w>,</l>
				<l n="24" num="1.24"><w n="24.1">A</w> <w n="24.2">la</w> <w n="24.3">forme</w>, <w n="24.4">manteau</w> <w n="24.5">sur</w> <w n="24.6">le</w> <w n="24.7">monde</w> <w n="24.8">jeté</w>,</l>
				<l n="25" num="1.25"><w n="25.1">Comme</w> <w n="25.2">autour</w> <w n="25.3">d</w>’<w n="25.4">une</w> <w n="25.5">vierge</w> <w n="25.6">une</w> <w n="25.7">souple</w> <w n="25.8">tunique</w>,</l>
				<l n="26" num="1.26"><w n="26.1">Ne</w> <w n="26.2">voilant</w> <w n="26.3">qu</w>’<w n="26.4">à</w> <w n="26.5">demi</w> <w n="26.6">sa</w> <w n="26.7">nudité</w> <w n="26.8">pudique</w> !</l>
			</lg>
			<lg n="2">
				<l n="27" num="2.1"><w n="27.1">Merci</w> <w n="27.2">donc</w>, <w n="27.3">ô</w> <w n="27.4">vous</w> <w n="27.5">tous</w>, <w n="27.6">artistes</w> <w n="27.7">souverains</w> !</l>
				<l n="28" num="2.2"><w n="28.1">Amants</w> <w n="28.2">des</w> <w n="28.3">chênes</w> <w n="28.4">verts</w> <w n="28.5">et</w> <w n="28.6">des</w> <w n="28.7">rouges</w> <w n="28.8">terrains</w>,</l>
				<l n="29" num="2.3"><w n="29.1">Que</w> <w n="29.2">Rome</w> <w n="29.3">voit</w> <w n="29.4">errer</w> <w n="29.5">dans</w> <w n="29.6">sa</w> <w n="29.7">morne</w> <w n="29.8">campagne</w>,</l>
				<l n="30" num="2.4"><w n="30.1">Dessinant</w> <w n="30.2">un</w> <w n="30.3">arbuste</w>, <w n="30.4">un</w> <w n="30.5">profil</w> <w n="30.6">de</w> <w n="30.7">montagne</w>,</l>
				<l n="31" num="2.5"><w n="31.1">Et</w> <w n="31.2">qui</w> <w n="31.3">nous</w> <w n="31.4">rapportez</w> <w n="31.5">la</w> <w n="31.6">vie</w> <w n="31.7">et</w> <w n="31.8">le</w> <w n="31.9">soleil</w></l>
				<l n="32" num="2.6"><w n="32.1">Dans</w> <w n="32.2">vos</w> <w n="32.3">toiles</w> <w n="32.4">qu</w>’<w n="32.5">échauffe</w> <w n="32.6">un</w> <w n="32.7">beau</w> <w n="32.8">reflet</w> <w n="32.9">vermeil</w> !</l>
				<l n="33" num="2.7"><w n="33.1">Sans</w> <w n="33.2">sortir</w>, <w n="33.3">avec</w> <w n="33.4">vous</w> <w n="33.5">nous</w> <w n="33.6">faisons</w> <w n="33.7">des</w> <w n="33.8">voyages</w>,</l>
				<l n="34" num="2.8"><w n="34.1">Nous</w> <w n="34.2">errons</w>, <w n="34.3">à</w> <w n="34.4">Paris</w>, <w n="34.5">dans</w> <w n="34.6">mille</w> <w n="34.7">paysages</w> ;</l>
				<l n="35" num="2.9"><w n="35.1">Nous</w> <w n="35.2">nageons</w> <w n="35.3">dans</w> <w n="35.4">les</w> <w n="35.5">flots</w> <w n="35.6">de</w> <w n="35.7">l</w>’<w n="35.8">immuable</w> <w n="35.9">azur</w>,</l>
				<l n="36" num="2.10"><w n="36.1">Et</w> <w n="36.2">vos</w> <w n="36.3">tableaux</w>, <w n="36.4">faisant</w> <w n="36.5">une</w> <w n="36.6">trouée</w> <w n="36.7">au</w> <w n="36.8">mur</w>,</l>
				<l n="37" num="2.11"><w n="37.1">Sont</w> <w n="37.2">pour</w> <w n="37.3">nous</w> <w n="37.4">comme</w> <w n="37.5">autant</w> <w n="37.6">de</w> <w n="37.7">fenêtres</w> <w n="37.8">ouvertes</w>,</l>
				<l n="38" num="2.12"><w n="38.1">Par</w> <w n="38.2">où</w> <w n="38.3">nous</w> <w n="38.4">regardons</w> <w n="38.5">les</w> <w n="38.6">grandes</w> <w n="38.7">plaines</w> <w n="38.8">vertes</w>,</l>
				<l n="39" num="2.13"><w n="39.1">Les</w> <w n="39.2">moissons</w> <w n="39.3">d</w>’<w n="39.4">or</w>, <w n="39.5">le</w> <w n="39.6">bois</w> <w n="39.7">que</w> <w n="39.8">l</w>’<w n="39.9">automne</w> <w n="39.10">a</w> <w n="39.11">jauni</w>,</l>
				<l n="40" num="2.14"><w n="40.1">Les</w> <w n="40.2">horizons</w> <w n="40.3">sans</w> <w n="40.4">borne</w> <w n="40.5">et</w> <w n="40.6">le</w> <w n="40.7">ciel</w> <w n="40.8">infini</w> !</l>
			</lg>
			<lg n="3">
				<l n="41" num="3.1"><w n="41.1">Ainsi</w> <w n="41.2">nous</w> <w n="41.3">vous</w> <w n="41.4">voyons</w>, <w n="41.5">austères</w> <w n="41.6">solitudes</w>,</l>
				<l n="42" num="3.2"><w n="42.1">Ou</w> <w n="42.2">l</w>’<w n="42.3">âme</w> <w n="42.4">endort</w> <w n="42.5">sa</w> <w n="42.6">peine</w> <w n="42.7">et</w> <w n="42.8">ses</w> <w n="42.9">inquiétudes</w> !</l>
				<l n="43" num="3.3"><hi rend="ital"><w n="43.1">Grottes</w> <w n="43.2">de</w> <w n="43.3">Cervara</w></hi>, <w n="43.4">que</w> <w n="43.5">d</w>’<w n="43.6">un</w> <w n="43.7">pinceau</w> <w n="43.8">certain</w></l>
				<l n="44" num="3.4"><w n="44.1">Creusa</w> <w n="44.2">profondément</w> <w n="44.3">le</w> <w n="44.4">sévère</w> <w n="44.5">Bertin</w>,</l>
				<l n="45" num="3.5"><w n="45.1">Ainsi</w> <w n="45.2">nous</w> <w n="45.3">vous</w> <w n="45.4">voyons</w> <w n="45.5">avec</w> <w n="45.6">vos</w> <w n="45.7">blocs</w> <w n="45.8">rougeâtres</w></l>
				<l n="46" num="3.6"><w n="46.1">Aux</w> <w n="46.2">flancs</w> <w n="46.3">tout</w> <w n="46.4">lézardés</w>, <w n="46.5">où</w> <w n="46.6">les</w> <w n="46.7">chèvres</w> <w n="46.8">des</w> <w n="46.9">pâtres</w></l>
				<l n="47" num="3.7"><w n="47.1">Se</w> <w n="47.2">pendent</w> <w n="47.3">à</w> <w n="47.4">midi</w> <w n="47.5">sous</w> <w n="47.6">le</w> <w n="47.7">soleil</w> <w n="47.8">ardent</w>,</l>
				<l n="48" num="3.8"><w n="48.1">Sans</w> <w n="48.2">trouver</w> <w n="48.3">un</w> <w n="48.4">bourgeon</w> <w n="48.5">à</w> <w n="48.6">ronger</w> <w n="48.7">de</w> <w n="48.8">la</w> <w n="48.9">dent</w>,</l>
				<l n="49" num="3.9"><w n="49.1">Avec</w> <w n="49.2">votre</w> <w n="49.3">chemin</w> <w n="49.4">poudroyant</w> <w n="49.5">de</w> <w n="49.6">lumière</w>,</l>
				<l n="50" num="3.10"><w n="50.1">De</w> <w n="50.2">son</w> <w n="50.3">ruban</w> <w n="50.4">crayeux</w> <w n="50.5">rayant</w> <w n="50.6">le</w> <w n="50.7">sol</w> <w n="50.8">de</w> <w n="50.9">pierre</w>,</l>
				<l n="51" num="3.11"><w n="51.1">Bien</w> <w n="51.2">rarement</w> <w n="51.3">foulé</w> <w n="51.4">par</w> <w n="51.5">le</w> <w n="51.6">talon</w> <w n="51.7">humain</w>,</l>
				<l n="52" num="3.12"><w n="52.1">Et</w> <w n="52.2">se</w> <w n="52.3">perdant</w> <w n="52.4">au</w> <w n="52.5">fond</w> <w n="52.6">parmi</w> <w n="52.7">le</w> <w n="52.8">champ</w> <w n="52.9">romain</w>.</l>
				<l n="53" num="3.13">— <w n="53.1">Les</w> <w n="53.2">grands</w> <w n="53.3">arbres</w> <w n="53.4">fluets</w>, <w n="53.5">au</w> <w n="53.6">feuille</w> <w n="53.7">sobre</w> <w n="53.8">et</w> <w n="53.9">rare</w>.</l>
				<l n="54" num="3.14"><w n="54.1">A</w> <w n="54.2">peine</w> <w n="54.3">noircissant</w> <w n="54.4">leurs</w> <w n="54.5">pieds</w> <w n="54.6">d</w>’<w n="54.7">une</w> <w n="54.8">ombre</w> <w n="54.9">avare</w>,</l>
				<l n="55" num="3.15"><w n="55.1">Montent</w> <w n="55.2">comme</w> <w n="55.3">la</w> <w n="55.4">flèche</w> <w n="55.5">et</w> <w n="55.6">vont</w> <w n="55.7">baigner</w> <w n="55.8">leur</w> <w n="55.9">front</w></l>
				<l n="56" num="3.16"><w n="56.1">Dans</w> <w n="56.2">la</w> <w n="56.3">limpidité</w> <w n="56.4">du</w> <w n="56.5">ciel</w> <w n="56.6">clair</w> <w n="56.7">et</w> <w n="56.8">profond</w> ;</l>
				<l n="57" num="3.17"><w n="57.1">Comme</w> <w n="57.2">s</w>’<w n="57.3">ils</w> <w n="57.4">dédaignaient</w> <w n="57.5">les</w> <w n="57.6">plaisirs</w> <w n="57.7">de</w> <w n="57.8">la</w> <w n="57.9">terre</w>,</l>
				<l n="58" num="3.18"><w n="58.1">Pour</w> <w n="58.2">cacher</w> <w n="58.3">une</w> <w n="58.4">nymphe</w> <w n="58.5">ils</w> <w n="58.6">manquent</w> <w n="58.7">de</w> <w n="58.8">mystère</w>,</l>
				<l n="59" num="3.19"><w n="59.1">Leurs</w> <w n="59.2">branches</w>, <w n="59.3">laissant</w> <w n="59.4">trop</w> <w n="59.5">filtrer</w> <w n="59.6">d</w>’<w n="59.7">air</w> <w n="59.8">et</w> <w n="59.9">de</w> <w n="59.10">jour</w>,</l>
				<l n="60" num="3.20"><w n="60.1">Éloignent</w> <w n="60.2">les</w> <w n="60.3">désirs</w> <w n="60.4">et</w> <w n="60.5">les</w> <w n="60.6">rêves</w> <w n="60.7">d</w>’<w n="60.8">amour</w> ;</l>
				<l n="61" num="3.21"><w n="61.1">Sous</w> <w n="61.2">leur</w> <w n="61.3">grêle</w> <w n="61.4">ramure</w> <w n="61.5">un</w> <w n="61.6">maigre</w> <w n="61.7">anachorète</w></l>
				<l n="62" num="3.22"><w n="62.1">Pourrait</w> <w n="62.2">seul</w> <w n="62.3">s</w>’<w n="62.4">abriter</w> <w n="62.5">et</w> <w n="62.6">choisir</w> <w n="62.7">sa</w> <w n="62.8">retraite</w>.</l>
			</lg>
			<lg n="4">
				<l n="63" num="4.1"><w n="63.1">Nulle</w> <w n="63.2">fleur</w> <w n="63.3">n</w>’<w n="63.4">adoucit</w> <w n="63.5">cette</w> <w n="63.6">sévérité</w> ;</l>
				<l n="64" num="4.2"><w n="64.1">Nul</w> <w n="64.2">ton</w> <w n="64.3">frais</w> <w n="64.4">ne</w> <w n="64.5">se</w> <w n="64.6">mêle</w> <w n="64.7">à</w> <w n="64.8">la</w> <w n="64.9">fauve</w> <w n="64.10">clarté</w> ;</l>
				<l n="65" num="4.3"><w n="65.1">Des</w> <w n="65.2">blessures</w> <w n="65.3">du</w> <w n="65.4">roc</w>, <w n="65.5">ainsi</w> <w n="65.6">que</w> <w n="65.7">des</w> <w n="65.8">vipères</w></l>
				<l n="66" num="4.4"><w n="66.1">Qui</w> <w n="66.2">sortent</w> <w n="66.3">à</w> <w n="66.4">demi</w> <w n="66.5">le</w> <w n="66.6">corps</w> <w n="66.7">de</w> <w n="66.8">leurs</w> <w n="66.9">repaires</w>,</l>
				<l n="67" num="4.5"><w n="67.1">De</w> <w n="67.2">pâles</w> <w n="67.3">filaments</w> <w n="67.4">d</w>’<w n="67.5">un</w> <w n="67.6">aspect</w> <w n="67.7">vénéneux</w></l>
				<l n="68" num="4.6"><w n="68.1">S</w>’<w n="68.2">allongent</w> <w n="68.3">au</w> <w n="68.4">soleil</w> <w n="68.5">en</w> <w n="68.6">enlaçant</w> <w n="68.7">leurs</w> <w n="68.8">nœuds</w> ;</l>
				<l n="69" num="4.7"><w n="69.1">Et</w> <w n="69.2">l</w>’<w n="69.3">oiseau</w> <w n="69.4">pour</w> <w n="69.5">sa</w> <w n="69.6">soif</w> <w n="69.7">n</w>’<w n="69.8">a</w> <w n="69.9">d</w>’<w n="69.10">autre</w> <w n="69.11">eau</w> <w n="69.12">que</w> <w n="69.13">les</w> <w n="69.14">gouttes</w>. —</l>
				<l n="70" num="4.8"><w n="70.1">Pleurs</w> <w n="70.2">amers</w> <w n="70.3">du</w> <w n="70.4">rocher</w>, — <w n="70.5">qui</w> <w n="70.6">suintent</w> <w n="70.7">des</w> <w n="70.8">voûtes</w>.</l>
				<l n="71" num="4.9"><w n="71.1">Cependant</w> <w n="71.2">ce</w> <w n="71.3">désert</w> <w n="71.4">a</w> <w n="71.5">de</w> <w n="71.6">puissants</w> <w n="71.7">attraits</w></l>
				<l n="72" num="4.10"><w n="72.1">Que</w> <w n="72.2">n</w>’<w n="72.3">ont</w> <w n="72.4">point</w> <w n="72.5">nos</w> <w n="72.6">climats</w> <w n="72.7">et</w> <w n="72.8">nos</w> <w n="72.9">sites</w> <w n="72.10">plus</w> <w n="72.11">frais</w>.</l>
				<l n="73" num="4.11"><w n="73.1">Où</w> <w n="73.2">l</w>’<w n="73.3">ombrage</w> <w n="73.4">est</w> <w n="73.5">opaque</w>, <w n="73.6">où</w> <w n="73.7">dans</w> <w n="73.8">des</w> <w n="73.9">vagues</w> <w n="73.10">d</w>’<w n="73.11">herbes</w></l>
				<l n="74" num="4.12"><w n="74.1">Nagent</w> <w n="74.2">à</w> <w n="74.3">plein</w> <w n="74.4">poitrail</w> <w n="74.5">les</w> <w n="74.6">génisses</w> <w n="74.7">superbes</w> :</l>
				<l n="75" num="4.13"><w n="75.1">C</w>’<w n="75.2">est</w> <w n="75.3">que</w> <w n="75.4">l</w>’<w n="75.5">œil</w> <w n="75.6">éternel</w> <w n="75.7">brille</w> <w n="75.8">dans</w> <w n="75.9">ce</w> <w n="75.10">ciel</w> <w n="75.11">bleu</w>,</l>
				<l n="76" num="4.14"><w n="76.1">Et</w> <w n="76.2">que</w> <w n="76.3">l</w>’<w n="76.4">homme</w> <w n="76.5">est</w> <w n="76.6">si</w> <w n="76.7">loin</w> <w n="76.8">qu</w>’<w n="76.9">on</w> <w n="76.10">se</w> <w n="76.11">sent</w> <w n="76.12">près</w> <w n="76.13">de</w> <w n="76.14">Dieu</w>.</l>
			</lg>
			<lg n="5">
				<l n="77" num="5.1"><w n="77.1">O</w> <w n="77.2">mère</w> <w n="77.3">du</w> <w n="77.4">génie</w> ! <w n="77.5">ô</w> <w n="77.6">divine</w> <w n="77.7">nourrice</w> !</l>
				<l n="78" num="5.2"><w n="78.1">Des</w> <w n="78.2">grands</w> <w n="78.3">cœurs</w> <w n="78.4">méconnus</w> <w n="78.5">pâle</w> <w n="78.6">consolatrice</w>,</l>
				<l n="79" num="5.3"><w n="79.1">Solitude</w> ! <w n="79.2">qui</w> <w n="79.3">tends</w> <w n="79.4">tes</w> <w n="79.5">bras</w> <w n="79.6">silencieux</w></l>
				<l n="80" num="5.4"><w n="80.1">Aux</w> <w n="80.2">ennuyés</w> <w n="80.3">du</w> <w n="80.4">monde</w>, <w n="80.5">aux</w> <w n="80.6">aspirants</w> <w n="80.7">des</w> <w n="80.8">cieux</w>,</l>
				<l n="81" num="5.5"><w n="81.1">Quand</w> <w n="81.2">pourrai</w>-<w n="81.3">je</w> <w n="81.4">avec</w> <w n="81.5">toi</w>, <w n="81.6">comme</w> <w n="81.7">le</w> <w n="81.8">vieil</w> <w n="81.9">ermite</w>,</l>
				<l n="82" num="5.6"><w n="82.1">Sur</w> <w n="82.2">le</w> <w n="82.3">livre</w> <w n="82.4">pencher</w> <w n="82.5">ma</w> <w n="82.6">tête</w> <w n="82.7">qui</w> <w n="82.8">médite</w> !</l>
			</lg>
			<lg n="6">
				<l n="83" num="6.1"><w n="83.1">Plus</w> <w n="83.2">loin</w> <w n="83.3">c</w>’<w n="83.4">est</w> <w n="83.5">Aligny</w>, <w n="83.6">qui</w>, <w n="83.7">le</w> <w n="83.8">crayon</w> <w n="83.9">en</w> <w n="83.10">main</w>,</l>
				<l n="84" num="6.2"><w n="84.1">Comme</w> <w n="84.2">Ingres</w> <w n="84.3">le</w> <w n="84.4">ferait</w> <w n="84.5">pour</w> <w n="84.6">un</w> <w n="84.7">profil</w> <w n="84.8">humain</w>,</l>
			</lg>
			<lg n="7">
				<l n="85" num="7.1"><w n="85.1">Recherche</w> <w n="85.2">l</w>’<w n="85.3">idéal</w> <w n="85.4">et</w> <w n="85.5">la</w> <w n="85.6">beauté</w> <w n="85.7">d</w>’<w n="85.8">un</w> <w n="85.9">arbre</w>,</l>
				<l n="86" num="7.2"><w n="86.1">Et</w> <w n="86.2">cisèle</w> <w n="86.3">au</w> <w n="86.4">pinceau</w> <w n="86.5">sa</w> <w n="86.6">peinture</w> <w n="86.7">de</w> <w n="86.8">marbre</w>.</l>
				<l n="87" num="7.3"><w n="87.1">Il</w> <w n="87.2">sait</w>, <w n="87.3">dans</w> <w n="87.4">la</w> <w n="87.5">prison</w> <w n="87.6">d</w>’<w n="87.7">un</w> <w n="87.8">rigide</w> <w n="87.9">contour</w>,</l>
				<l n="88" num="7.4"><w n="88.1">Enfermer</w> <w n="88.2">des</w> <w n="88.3">flots</w> <w n="88.4">d</w>’<w n="88.5">air</w> <w n="88.6">et</w> <w n="88.7">des</w> <w n="88.8">torrents</w> <w n="88.9">de</w> <w n="88.10">jour</w>,</l>
				<l n="89" num="7.5"><w n="89.1">Et</w> <w n="89.2">dans</w> <w n="89.3">tous</w> <w n="89.4">ses</w> <w n="89.5">tableaux</w>, <w n="89.6">fidèle</w> <w n="89.7">au</w> <w n="89.8">nom</w> <w n="89.9">qu</w>’<w n="89.10">il</w> <w n="89.11">signe</w>,</l>
				<l n="90" num="7.6"><w n="90.1">Sculpteur</w> <w n="90.2">athénien</w>, <w n="90.3">il</w> <w n="90.4">caresse</w> <w n="90.5">la</w> <w n="90.6">ligne</w>,</l>
				<l n="91" num="7.7"><w n="91.1">Et</w>, <w n="91.2">comme</w> <w n="91.3">Phidias</w> <w n="91.4">le</w> <w n="91.5">corps</w> <w n="91.6">de</w> <w n="91.7">sa</w> <w n="91.8">Vénus</w>,</l>
				<l n="92" num="7.8"><w n="92.1">Polit</w> <w n="92.2">avec</w> <w n="92.3">amour</w> <w n="92.4">le</w> <w n="92.5">flanc</w> <w n="92.6">des</w> <w n="92.7">rochers</w> <w n="92.8">nus</w>.</l>
			</lg>
			<lg n="8">
				<l n="93" num="8.1"><w n="93.1">Voici</w> <w n="93.2">la</w> <hi rend="ital"><w n="93.3">Madeleine</w></hi>. — <w n="93.4">Une</w> <w n="93.5">dernière</w> <w n="93.6">étoile</w></l>
				<l n="94" num="8.2"><w n="94.1">Luit</w> <w n="94.2">comme</w> <w n="94.3">une</w> <w n="94.4">fleur</w> <w n="94.5">d</w>’<w n="94.6">or</w> <w n="94.7">sur</w> <w n="94.8">la</w> <w n="94.9">céleste</w> <w n="94.10">toile</w> :</l>
				<l n="95" num="8.3"><w n="95.1">La</w> <w n="95.2">grande</w> <w n="95.3">repentie</w>, <w n="95.4">au</w> <w n="95.5">fond</w> <w n="95.6">de</w> <w n="95.7">son</w> <w n="95.8">désert</w>,</l>
				<l n="96" num="8.4"><w n="96.1">En</w> <w n="96.2">extase</w>, <w n="96.3">à</w> <w n="96.4">genoux</w>, <w n="96.5">écoute</w> <w n="96.6">le</w> <w n="96.7">concert</w></l>
				<l n="97" num="8.5"><w n="97.1">Que</w> <w n="97.2">dès</w> <w n="97.3">l</w>’<w n="97.4">aube</w> <w n="97.5">lui</w> <w n="97.6">donne</w> <w n="97.7">un</w> <w n="97.8">orchestre</w> <w n="97.9">angélique</w>,</l>
				<l n="98" num="8.6"><w n="98.1">Avec</w> <w n="98.2">le</w> <w n="98.3">kinnar</w> <w n="98.4">juif</w> <w n="98.5">et</w> <w n="98.6">le</w> <w n="98.7">rebec</w> <w n="98.8">gothique</w>.</l>
				<l n="99" num="8.7"><w n="99.1">Un</w> <w n="99.2">rayon</w> <w n="99.3">curieux</w>, <w n="99.4">perçant</w> <w n="99.5">le</w> <w n="99.6">dôme</w> <w n="99.7">épais</w>,</l>
				<l n="100" num="8.8"><w n="100.1">Où</w> <w n="100.2">les</w> <w n="100.3">petits</w> <w n="100.4">oiseaux</w> <w n="100.5">dorment</w> <w n="100.6">encore</w> <w n="100.7">en</w> <w n="100.8">paix</w>,</l>
				<l n="101" num="8.9"><w n="101.1">Allume</w> <w n="101.2">une</w> <w n="101.3">auréole</w> <w n="101.4">aux</w> <w n="101.5">blonds</w> <w n="101.6">cheveux</w> <w n="101.7">des</w> <w n="101.8">anges</w>,</l>
				<l n="102" num="8.10"><w n="102.1">Illuminés</w> <w n="102.2">soudain</w> <w n="102.3">de</w> <w n="102.4">nuances</w> <w n="102.5">étranges</w>,</l>
				<l n="103" num="8.11"><w n="103.1">Tandis</w> <w n="103.2">que</w> <w n="103.3">leur</w> <w n="103.4">tunique</w> <w n="103.5">et</w> <w n="103.6">le</w> <w n="103.7">bout</w> <w n="103.8">de</w> <w n="103.9">leurs</w> <w n="103.10">pieds</w></l>
				<l n="104" num="8.12"><w n="104.1">Dans</w> <w n="104.2">l</w>’<w n="104.3">ombre</w> <w n="104.4">du</w> <w n="104.5">matin</w> <w n="104.6">sont</w> <w n="104.7">encore</w> <w n="104.8">noyés</w>.</l>
			</lg>
			<lg n="9">
				<l n="105" num="9.1">— <w n="105.1">Fauve</w> <w n="105.2">et</w> <w n="105.3">le</w> <w n="105.4">teint</w> <w n="105.5">hâlé</w> <w n="105.6">comme</w> <w n="105.7">Cérès</w> <w n="105.8">la</w> <w n="105.9">blonde</w>,</l>
				<l n="106" num="9.2"><w n="106.1">La</w> <w n="106.2">campagne</w> <w n="106.3">de</w> <w n="106.4">Rome</w>, <w n="106.5">embrasée</w> <w n="106.6">et</w> <w n="106.7">féconde</w>,</l>
				<l n="107" num="9.3"><w n="107.1">En</w> <w n="107.2">sillons</w> <w n="107.3">rutilants</w> <w n="107.4">jusques</w> <w n="107.5">à</w> <w n="107.6">l</w>’<w n="107.7">horizon</w></l>
				<l n="108" num="9.4"><w n="108.1">Roule</w> <w n="108.2">l</w>’<w n="108.3">océan</w> <w n="108.4">d</w>’<w n="108.5">or</w> <w n="108.6">de</w> <w n="108.7">sa</w> <w n="108.8">riche</w> <hi rend="ital"><w n="108.9">moisson</w></hi>.</l>
				<l n="109" num="9.5"><w n="109.1">Comme</w> <w n="109.2">d</w>’<w n="109.3">un</w> <w n="109.4">encensoir</w> <w n="109.5">la</w> <w n="109.6">vapeur</w> <w n="109.7">embaumée</w>,</l>
				<l n="110" num="9.6"><w n="110.1">Dans</w> <w n="110.2">le</w> <w n="110.3">lointain</w> <w n="110.4">tournoie</w> <w n="110.5">et</w> <w n="110.6">monte</w> <w n="110.7">une</w> <w n="110.8">fumée</w>,</l>
				<l n="111" num="9.7"><w n="111.1">Et</w> <w n="111.2">le</w> <w n="111.3">ciel</w> <w n="111.4">est</w> <w n="111.5">si</w> <w n="111.6">clair</w>, <w n="111.7">si</w> <w n="111.8">cristallin</w>, <w n="111.9">si</w> <w n="111.10">pur</w>,</l>
				<l n="112" num="9.8"><w n="112.1">Que</w> <w n="112.2">l</w>’<w n="112.3">on</w> <w n="112.4">voit</w> <w n="112.5">l</w>’<w n="112.6">infini</w> <w n="112.7">derrière</w> <w n="112.8">son</w> <w n="112.9">azur</w>.</l>
				<l n="113" num="9.9"><w n="113.1">Au</w>-<w n="113.2">devant</w>, <w n="113.3">près</w> <w n="113.4">d</w>’<w n="113.5">un</w> <w n="113.6">mur</w> <w n="113.7">réticulaire</w>, <w n="113.8">en</w> <w n="113.9">briques</w>,</l>
				<l n="114" num="9.10"><w n="114.1">Sont</w> <w n="114.2">quelques</w> <w n="114.3">laboureurs</w> <w n="114.4">dans</w> <w n="114.5">des</w> <w n="114.6">poses</w> <w n="114.7">antiques</w>,</l>
				<l n="115" num="9.11"><w n="115.1">Avec</w> <w n="115.2">leur</w> <w n="115.3">chien</w> <w n="115.4">couché</w>, <w n="115.5">haletant</w> <w n="115.6">de</w> <w n="115.7">chaleur</w>,</l>
				<l n="116" num="9.12"><w n="116.1">Cherchant</w> <w n="116.2">contre</w> <w n="116.3">le</w> <w n="116.4">sol</w> <w n="116.5">un</w> <w n="116.6">reste</w> <w n="116.7">de</w> <w n="116.8">fraîcheur</w> ;</l>
				<l n="117" num="9.13"><w n="117.1">Un</w> <w n="117.2">groupe</w> <w n="117.3">simple</w> <w n="117.4">et</w> <w n="117.5">beau</w> <w n="117.6">dans</w> <w n="117.7">sa</w> <w n="117.8">grâce</w> <w n="117.9">tranquille</w>,</l>
				<l n="118" num="9.14"><w n="118.1">Que</w> <w n="118.2">Poussin</w> <w n="118.3">avoûrait</w> <w n="118.4">et</w> <w n="118.5">qu</w>’<w n="118.6">eût</w> <w n="118.7">aimé</w> <w n="118.8">Virgile</w>.</l>
			</lg>
			<lg n="10">
				<l n="119" num="10.1"><w n="119.1">Mais</w> <w n="119.2">voici</w> <w n="119.3">que</w> <w n="119.4">le</w> <w n="119.5">soir</w> <w n="119.6">du</w> <w n="119.7">haut</w> <w n="119.8">des</w> <w n="119.9">monts</w> <w n="119.10">descend</w> :</l>
				<l n="120" num="10.2"><w n="120.1">L</w>’<w n="120.2">ombre</w> <w n="120.3">devient</w> <w n="120.4">plus</w> <w n="120.5">grise</w> <w n="120.6">et</w> <w n="120.7">va</w> <w n="120.8">s</w>’<w n="120.9">élargissant</w> ;</l>
				<l n="121" num="10.3"><w n="121.1">Le</w> <w n="121.2">ciel</w> <w n="121.3">vert</w> <w n="121.4">a</w> <w n="121.5">des</w> <w n="121.6">tons</w> <w n="121.7">de</w> <w n="121.8">citron</w> <w n="121.9">et</w> <w n="121.10">d</w>’<w n="121.11">orange</w>.</l>
				<l n="122" num="10.4"><w n="122.1">Le</w> <w n="122.2">couchant</w> <w n="122.3">s</w>’<w n="122.4">amincit</w> <w n="122.5">et</w> <w n="122.6">va</w> <w n="122.7">plier</w> <w n="122.8">sa</w> <w n="122.9">frange</w>,</l>
				<l n="123" num="10.5"><w n="123.1">La</w> <w n="123.2">cigale</w> <w n="123.3">se</w> <w n="123.4">tait</w>, <w n="123.5">et</w> <w n="123.6">l</w>’<w n="123.7">on</w> <w n="123.8">n</w>’<w n="123.9">entend</w> <w n="123.10">de</w> <w n="123.11">bruit</w></l>
				<l n="124" num="10.6"><w n="124.1">Que</w> <w n="124.2">le</w> <w n="124.3">soupir</w> <w n="124.4">de</w> <w n="124.5">l</w>’<w n="124.6">eau</w> <w n="124.7">qui</w> <w n="124.8">se</w> <w n="124.9">divise</w> <w n="124.10">et</w> <w n="124.11">fuit</w>.</l>
				<l n="125" num="10.7"><w n="125.1">Sur</w> <w n="125.2">le</w> <w n="125.3">monde</w> <w n="125.4">assoupi</w> <w n="125.5">les</w> <w n="125.6">heures</w> <w n="125.7">taciturnes</w></l>
				<l n="126" num="10.8"><w n="126.1">Tordent</w> <w n="126.2">leurs</w> <w n="126.3">cheveux</w> <w n="126.4">bruns</w> <w n="126.5">mouillés</w> <w n="126.6">des</w> <w n="126.7">pleurs</w> <w n="126.8">nocturnes</w></l>
				<l n="127" num="10.9"><w n="127.1">A</w> <w n="127.2">peine</w> <w n="127.3">reste</w>-<w n="127.4">t</w>-<w n="127.5">il</w> <w n="127.6">assez</w> <w n="127.7">de</w> <w n="127.8">jour</w> <w n="127.9">pour</w> <w n="127.10">voir</w>,</l>
				<l n="128" num="10.10"><w n="128.1">Corot</w>, <w n="128.2">ton</w> <w n="128.3">nom</w> <w n="128.4">modeste</w> <w n="128.5">écrit</w> <w n="128.6">dans</w> <w n="128.7">un</w> <w n="128.8">coin</w> <w n="128.9">noir</w>.</l>
			</lg>
			<lg n="11">
				<l n="129" num="11.1"><w n="129.1">Nous</w> <w n="129.2">voici</w> <w n="129.3">replongés</w> <w n="129.4">dans</w> <w n="129.5">la</w> <w n="129.6">brume</w> <w n="129.7">et</w> <w n="129.8">la</w> <w n="129.9">pluie</w>,</l>
				<l n="130" num="11.2"><w n="130.1">Sur</w> <w n="130.2">un</w> <w n="130.3">pavé</w> <w n="130.4">de</w> <w n="130.5">boue</w> <w n="130.6">et</w> <w n="130.7">sous</w> <w n="130.8">un</w> <w n="130.9">ciel</w> <w n="130.10">de</w> <w n="130.11">suie</w>,</l>
				<l n="131" num="11.3"><w n="131.1">Ne</w> <w n="131.2">voyant</w> <w n="131.3">plus</w>, <w n="131.4">au</w> <w n="131.5">lieu</w> <w n="131.6">de</w> <w n="131.7">ces</w> <w n="131.8">beaux</w> <w n="131.9">horizons</w>,</l>
				<l n="132" num="11.4"><w n="132.1">Que</w> <w n="132.2">des</w> <w n="132.3">angles</w> <w n="132.4">de</w> <w n="132.5">murs</w> <w n="132.6">ou</w> <w n="132.7">des</w> <w n="132.8">toits</w> <w n="132.9">de</w> <w n="132.10">maisons</w> ;</l>
				<l n="133" num="11.5"><w n="133.1">Le</w> <w n="133.2">vent</w> <w n="133.3">pleure</w>, <w n="133.4">la</w> <w n="133.5">nuit</w> <w n="133.6">s</w>’<w n="133.7">étoile</w> <w n="133.8">de</w> <w n="133.9">lanternes</w>,</l>
				<l n="134" num="11.6"><w n="134.1">Les</w> <w n="134.2">ruisseaux</w> <w n="134.3">miroitants</w> <w n="134.4">lancent</w> <w n="134.5">des</w> <w n="134.6">reflets</w> <w n="134.7">ternes</w>,</l>
				<l n="135" num="11.7"><w n="135.1">Partout</w> <w n="135.2">des</w> <w n="135.3">bruits</w> <w n="135.4">de</w> <w n="135.5">char</w>, <w n="135.6">des</w> <w n="135.7">chants</w>, <w n="135.8">des</w> <w n="135.9">voix</w>, <w n="135.10">des</w> <w n="135.11">cris</w>.</l>
				<l n="136" num="11.8"><w n="136.1">Blonde</w> <w n="136.2">Italie</w>, <w n="136.3">adieu</w> ! — <w n="136.4">Nous</w> <w n="136.5">sommes</w> <w n="136.6">à</w> <w n="136.7">Paris</w> !</l>
			</lg>
			<closer>
				<dateline>
					<date when="1839">1839</date>.
				</dateline>
			</closer>
		</div></body></text></TEI>