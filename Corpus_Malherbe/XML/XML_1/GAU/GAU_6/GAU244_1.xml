<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES DIVERSES, 1838-1845</title>
				<title type="sub_2">Tome second</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>625 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_6</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Second</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://gutenberg.org/files/45886/45886-h/45886-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Second</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1890">1890</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1845">1838-1845</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU244">
			<head type="main">AMBITION</head>
			<head type="form">SONNET</head>
			<lg n="1">
				<l n="1" num="1.1"><w n="1.1">Poëte</w>, <w n="1.2">dans</w> <w n="1.3">les</w> <w n="1.4">cœurs</w> <w n="1.5">mettre</w> <w n="1.6">un</w> <w n="1.7">écho</w> <w n="1.8">sonore</w>,</l>
				<l n="2" num="1.2"><w n="2.1">Remuer</w> <w n="2.2">une</w> <w n="2.3">foule</w> <w n="2.4">avec</w> <w n="2.5">ses</w> <w n="2.6">passions</w>,</l>
				<l n="3" num="1.3"><w n="3.1">Écrire</w> <w n="3.2">sur</w> <w n="3.3">l</w>’<w n="3.4">airain</w> <w n="3.5">ses</w> <w n="3.6">moindres</w> <w n="3.7">actions</w>,</l>
				<l n="4" num="1.4"><w n="4.1">Faire</w> <w n="4.2">luire</w> <w n="4.3">son</w> <w n="4.4">nom</w> <w n="4.5">sur</w> <w n="4.6">tous</w> <w n="4.7">ceux</w> <w n="4.8">qu</w>’<w n="4.9">on</w> <w n="4.10">adore</w> ;</l>
			</lg>
			<lg n="2">
				<l n="5" num="2.1"><w n="5.1">Courir</w> <w n="5.2">en</w> <w n="5.3">quatre</w> <w n="5.4">pas</w> <w n="5.5">du</w> <w n="5.6">couchant</w> <w n="5.7">à</w> <w n="5.8">l</w>’<w n="5.9">aurore</w>,</l>
				<l n="6" num="2.2"><w n="6.1">Avoir</w> <w n="6.2">un</w> <w n="6.3">peuple</w> <w n="6.4">fait</w> <w n="6.5">de</w> <w n="6.6">trente</w> <w n="6.7">nations</w>,</l>
				<l n="7" num="2.3"><w n="7.1">Voir</w> <w n="7.2">la</w> <w n="7.3">terre</w> <w n="7.4">manquer</w> <w n="7.5">à</w> <w n="7.6">ses</w> <w n="7.7">ambitions</w>,</l>
				<l n="8" num="2.4"><w n="8.1">Être</w> <w n="8.2">Napoléon</w>, <w n="8.3">être</w> <w n="8.4">plus</w> <w n="8.5">grand</w> <w n="8.6">encore</w> !</l>
			</lg>
			<lg n="3">
				<l n="9" num="3.1"><w n="9.1">Que</w> <w n="9.2">sais</w>-<w n="9.3">je</w> ? <w n="9.4">être</w> <w n="9.5">Shakspeare</w>, <w n="9.6">être</w> <w n="9.7">Dante</w>, <w n="9.8">être</w> <w n="9.9">Dieu</w> !</l>
				<l n="10" num="3.2"><w n="10.1">Quand</w> <w n="10.2">on</w> <w n="10.3">est</w> <w n="10.4">tout</w> <w n="10.5">cela</w>, <w n="10.6">tout</w> <w n="10.7">cela</w>, <w n="10.8">c</w>’<w n="10.9">est</w> <w n="10.10">bien</w> <w n="10.11">peu</w> :</l>
				<l n="11" num="3.3"><w n="11.1">Le</w> <w n="11.2">monde</w> <w n="11.3">est</w> <w n="11.4">plein</w> <w n="11.5">de</w> <w n="11.6">vous</w>, <w n="11.7">le</w> <w n="11.8">vide</w> <w n="11.9">est</w> <w n="11.10">dans</w> <w n="11.11">votre</w> <w n="11.12">âme</w>…</l>
			</lg>
			<lg n="4">
				<l n="12" num="4.1"><w n="12.1">Mais</w> <w n="12.2">qui</w> <w n="12.3">donc</w> <w n="12.4">comblera</w> <w n="12.5">l</w>’<w n="12.6">abîme</w> <w n="12.7">de</w> <w n="12.8">ton</w> <w n="12.9">cœur</w> ?</l>
				<l n="13" num="4.2"><w n="13.1">Que</w> <w n="13.2">veux</w>-<w n="13.3">tu</w> <w n="13.4">qu</w>’<w n="13.5">on</w> <w n="13.6">y</w> <w n="13.7">jette</w>, <w n="13.8">ô</w> <w n="13.9">poëte</w> ! <w n="13.10">ô</w> <w n="13.11">vainqueur</w> ?</l>
				<l n="14" num="4.3">— <w n="14.1">Un</w> <w n="14.2">mot</w> <w n="14.3">d</w>’<w n="14.4">amour</w> <w n="14.5">tombé</w> <w n="14.6">d</w>’<w n="14.7">une</w> <w n="14.8">bouche</w> <w n="14.9">de</w> <w n="14.10">femme</w> !</l>
			</lg>
			<closer>
				<dateline>
					<date when="1844">1844</date>.
				</dateline>
			</closer>
		</div></body></text></TEI>