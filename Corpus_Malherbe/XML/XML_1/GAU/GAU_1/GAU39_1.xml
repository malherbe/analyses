<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ÉMAUX ET CAMÉES</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2510 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2011">2012</date>
				<idno type="local">GAU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Émaux et Camées</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Émaux et Camées</title>
								<author>Théophile Gautier</author>
								<imprint>
									<publisher>G. CHARPENTIER et E. FASQUELLE</publisher>
									<date when="1895">1895</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Émaux et Camées</title>
						<author>Théophile Gautier</author>
						<imprint>
							<publisher>G. CHARPENTIER et E. FASQUELLE</publisher>
							<date when="1872">1872</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1872">1872</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Texte vérifié avec l’édition Charpentier et Cie, 1872</p>
				<p>Les corrections signalées dans la version électronique n’ont pas été intégrées ; erreurs absentes de l’édition de référence</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU39">
				<head type="main">LA FELLAH</head>
				<head type="sub">SUR UNE AQUARELLE DE LA PRINCESSE M…</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Caprice</w> <w n="1.2">d</w>’<w n="1.3">un</w> <w n="1.4">pinceau</w> <w n="1.5">fantasque</w></l>
					<l n="2" num="1.2"><w n="2.1">Et</w> <w n="2.2">d</w>’<w n="2.3">un</w> <w n="2.4">impérial</w> <w n="2.5">loisir</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Votre</w> <w n="3.2">fellah</w>, <w n="3.3">sphinx</w> <w n="3.4">qui</w> <w n="3.5">se</w> <w n="3.6">masque</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Propose</w> <w n="4.2">une</w> <w n="4.3">énigme</w> <w n="4.4">au</w> <w n="4.5">désir</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">C</w>’<w n="5.2">est</w> <w n="5.3">une</w> <w n="5.4">mode</w> <w n="5.5">bien</w> <w n="5.6">austère</w></l>
					<l n="6" num="2.2"><w n="6.1">Que</w> <w n="6.2">ce</w> <w n="6.3">masque</w> <w n="6.4">et</w> <w n="6.5">cet</w> <w n="6.6">habit</w> <w n="6.7">long</w>,</l>
					<l n="7" num="2.3"><w n="7.1">Elle</w> <w n="7.2">intrigue</w> <w n="7.3">par</w> <w n="7.4">son</w> <w n="7.5">mystère</w></l>
					<l n="8" num="2.4"><w n="8.1">Tous</w> <w n="8.2">les</w> <w n="8.3">Œdipes</w> <w n="8.4">du</w> <w n="8.5">salon</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">L</w>’<w n="9.2">antique</w> <w n="9.3">Isis</w> <w n="9.4">légua</w> <w n="9.5">ses</w> <w n="9.6">voiles</w></l>
					<l n="10" num="3.2"><w n="10.1">Aux</w> <w n="10.2">modernes</w> <w n="10.3">filles</w> <w n="10.4">du</w> <w n="10.5">Nil</w> ;</l>
					<l n="11" num="3.3"><w n="11.1">Mais</w>, <w n="11.2">sous</w> <w n="11.3">le</w> <w n="11.4">bandeau</w>, <w n="11.5">deux</w> <w n="11.6">étoiles</w></l>
					<l n="12" num="3.4"><w n="12.1">Brillent</w> <w n="12.2">d</w>’<w n="12.3">un</w> <w n="12.4">feu</w> <w n="12.5">pur</w> <w n="12.6">et</w> <w n="12.7">subtil</w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Ces</w> <w n="13.2">yeux</w> <w n="13.3">qui</w> <w n="13.4">sont</w> <w n="13.5">tout</w> <w n="13.6">un</w> <w n="13.7">poème</w></l>
					<l n="14" num="4.2"><w n="14.1">De</w> <w n="14.2">langueur</w> <w n="14.3">et</w> <w n="14.4">de</w> <w n="14.5">volupté</w></l>
					<l n="15" num="4.3"><w n="15.1">Disent</w>, <w n="15.2">résolvant</w> <w n="15.3">le</w> <w n="15.4">problème</w>,</l>
					<l n="16" num="4.4">«<w n="16.1">Sois</w> <w n="16.2">l</w>’<w n="16.3">amour</w>, <w n="16.4">je</w> <w n="16.5">suis</w> <w n="16.6">la</w> <w n="16.7">beauté</w>.»</l>
				</lg>
			</div></body></text></TEI>