<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ÉMAUX ET CAMÉES</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2510 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2011">2012</date>
				<idno type="local">GAU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Émaux et Camées</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Émaux et Camées</title>
								<author>Théophile Gautier</author>
								<imprint>
									<publisher>G. CHARPENTIER et E. FASQUELLE</publisher>
									<date when="1895">1895</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Émaux et Camées</title>
						<author>Théophile Gautier</author>
						<imprint>
							<publisher>G. CHARPENTIER et E. FASQUELLE</publisher>
							<date when="1872">1872</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1872">1872</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Texte vérifié avec l’édition Charpentier et Cie, 1872</p>
				<p>Les corrections signalées dans la version électronique n’ont pas été intégrées ; erreurs absentes de l’édition de référence</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU41">
				<head type="main">LA NUE</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">A</w> <w n="1.2">l</w>’<w n="1.3">horizon</w> <w n="1.4">monte</w> <w n="1.5">une</w> <w n="1.6">nue</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Sculptant</w> <w n="2.2">sa</w> <w n="2.3">forme</w> <w n="2.4">dans</w> <w n="2.5">l</w>’<w n="2.6">azur</w> :</l>
					<l n="3" num="1.3"><w n="3.1">On</w> <w n="3.2">dirait</w> <w n="3.3">une</w> <w n="3.4">vierge</w> <w n="3.5">nue</w></l>
					<l n="4" num="1.4"><w n="4.1">Émergeant</w> <w n="4.2">d</w>’<w n="4.3">un</w> <w n="4.4">lac</w> <w n="4.5">au</w> <w n="4.6">flot</w> <w n="4.7">pur</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Debout</w> <w n="5.2">dans</w> <w n="5.3">sa</w> <w n="5.4">conque</w> <w n="5.5">nacrée</w>,</l>
					<l n="6" num="2.2"><w n="6.1">Elle</w> <w n="6.2">vogue</w> <w n="6.3">sur</w> <w n="6.4">le</w> <w n="6.5">bleu</w> <w n="6.6">clair</w>.</l>
					<l n="7" num="2.3"><w n="7.1">Comme</w> <w n="7.2">une</w> <w n="7.3">Aphrodite</w> <w n="7.4">éthérée</w>,</l>
					<l n="8" num="2.4"><w n="8.1">Faite</w> <w n="8.2">de</w> <w n="8.3">l</w>’<w n="8.4">écume</w> <w n="8.5">de</w> <w n="8.6">l</w>’<w n="8.7">air</w> ;</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">On</w> <w n="9.2">voit</w> <w n="9.3">onder</w> <w n="9.4">en</w> <w n="9.5">molles</w> <w n="9.6">poses</w></l>
					<l n="10" num="3.2"><w n="10.1">Son</w> <w n="10.2">torse</w> <w n="10.3">au</w> <w n="10.4">contour</w> <w n="10.5">incertain</w>,</l>
					<l n="11" num="3.3"><w n="11.1">Et</w> <w n="11.2">l</w>’<w n="11.3">aurore</w> <w n="11.4">répand</w> <w n="11.5">des</w> <w n="11.6">roses</w></l>
					<l n="12" num="3.4"><w n="12.1">Sur</w> <w n="12.2">son</w> <w n="12.3">épaule</w> <w n="12.4">de</w> <w n="12.5">satin</w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Ses</w> <w n="13.2">blancheurs</w> <w n="13.3">de</w> <w n="13.4">marbre</w> <w n="13.5">et</w> <w n="13.6">de</w> <w n="13.7">neige</w></l>
					<l n="14" num="4.2"><w n="14.1">Se</w> <w n="14.2">fondent</w> <w n="14.3">amoureusement</w></l>
					<l n="15" num="4.3"><w n="15.1">Comme</w>, <w n="15.2">au</w> <w n="15.3">clair</w>-<w n="15.4">obscur</w> <w n="15.5">du</w> <w n="15.6">Corrége</w>,</l>
					<l n="16" num="4.4"><w n="16.1">Le</w> <w n="16.2">corps</w> <w n="16.3">d</w>’<w n="16.4">Antiope</w> <w n="16.5">dormant</w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">Elle</w> <w n="17.2">plane</w> <w n="17.3">dans</w> <w n="17.4">la</w> <w n="17.5">lumière</w></l>
					<l n="18" num="5.2"><w n="18.1">Plus</w> <w n="18.2">haut</w> <w n="18.3">que</w> <w n="18.4">l</w>’<w n="18.5">Alpe</w> <w n="18.6">ou</w> <w n="18.7">l</w>’<w n="18.8">Apennin</w> ;</l>
					<l n="19" num="5.3"><w n="19.1">Reflet</w> <w n="19.2">de</w> <w n="19.3">la</w> <w n="19.4">beauté</w> <w n="19.5">première</w>,</l>
					<l n="20" num="5.4"><w n="20.1">Sœur</w> <w n="20.2">de</w> «<w n="20.3">l</w>’<w n="20.4">éternel</w> <w n="20.5">féminin</w>».</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">A</w> <w n="21.2">son</w> <w n="21.3">corps</w>, <w n="21.4">en</w> <w n="21.5">vain</w> <w n="21.6">retenue</w>,</l>
					<l n="22" num="6.2"><w n="22.1">Sur</w> <w n="22.2">l</w>’<w n="22.3">aile</w> <w n="22.4">de</w> <w n="22.5">la</w> <w n="22.6">passion</w>,</l>
					<l n="23" num="6.3"><w n="23.1">Mon</w> <w n="23.2">âme</w> <w n="23.3">vole</w> <w n="23.4">à</w> <w n="23.5">cette</w> <w n="23.6">nue</w></l>
					<l n="24" num="6.4"><w n="24.1">Et</w> <w n="24.2">l</w>’<w n="24.3">embrasse</w> <w n="24.4">comme</w> <w n="24.5">Ixion</w>.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">La</w> <w n="25.2">raison</w> <w n="25.3">dit</w> : «<w n="25.4">Vague</w> <w n="25.5">fumée</w>,</l>
					<l n="26" num="7.2"><w n="26.1">Où</w> <w n="26.2">l</w>’<w n="26.3">on</w> <w n="26.4">croit</w> <w n="26.5">voir</w> <w n="26.6">ce</w> <w n="26.7">qu</w>’<w n="26.8">on</w> <w n="26.9">rêva</w>,</l>
					<l n="27" num="7.3"><w n="27.1">Ombre</w> <w n="27.2">au</w> <w n="27.3">gré</w> <w n="27.4">du</w> <w n="27.5">vent</w> <w n="27.6">déformée</w>,</l>
					<l n="28" num="7.4"><w n="28.1">Bulle</w> <w n="28.2">qui</w> <w n="28.3">crève</w> <w n="28.4">et</w> <w n="28.5">qui</w> <w n="28.6">s</w>’<w n="28.7">en</w> <w n="28.8">va</w> !»</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1">Le</w> <w n="29.2">sentiment</w> <w n="29.3">répond</w> : «<w n="29.4">Qu</w>’<w n="29.5">importe</w> !</l>
					<l n="30" num="8.2"><w n="30.1">Qu</w>’<w n="30.2">est</w>-<w n="30.3">ce</w> <w n="30.4">après</w> <w n="30.5">tout</w> <w n="30.6">que</w> <w n="30.7">la</w> <w n="30.8">beauté</w> ?</l>
					<l n="31" num="8.3"><w n="31.1">Spectre</w> <w n="31.2">charmant</w> <w n="31.3">qu</w>’<w n="31.4">un</w> <w n="31.5">souffle</w> <w n="31.6">emporte</w></l>
					<l n="32" num="8.4"><w n="32.1">Et</w> <w n="32.2">qui</w> <w n="32.3">n</w>’<w n="32.4">est</w> <w n="32.5">rien</w>, <w n="32.6">ayant</w> <w n="32.7">été</w> !</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1">«<w n="33.1">A</w> <w n="33.2">l</w>’<w n="33.3">Idéal</w> <w n="33.4">ouvre</w> <w n="33.5">ton</w> <w n="33.6">âme</w>,</l>
					<l n="34" num="9.2"><w n="34.1">Mets</w> <w n="34.2">dans</w> <w n="34.3">ton</w> <w n="34.4">cœur</w> <w n="34.5">beaucoup</w> <w n="34.6">de</w> <w n="34.7">ciel</w>,</l>
					<l n="35" num="9.3"><w n="35.1">Aime</w> <w n="35.2">une</w> <w n="35.3">nue</w>, <w n="35.4">aime</w> <w n="35.5">une</w> <w n="35.6">femme</w>,</l>
					<l n="36" num="9.4"><w n="36.1">Mais</w> <w n="36.2">aime</w> !— <w n="36.3">C</w>’<w n="36.4">est</w> <w n="36.5">l</w>’<w n="36.6">essentiel</w> !»</l>
				</lg>
			</div></body></text></TEI>