<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ÉMAUX ET CAMÉES</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2510 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2011">2012</date>
				<idno type="local">GAU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Émaux et Camées</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Émaux et Camées</title>
								<author>Théophile Gautier</author>
								<imprint>
									<publisher>G. CHARPENTIER et E. FASQUELLE</publisher>
									<date when="1895">1895</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Émaux et Camées</title>
						<author>Théophile Gautier</author>
						<imprint>
							<publisher>G. CHARPENTIER et E. FASQUELLE</publisher>
							<date when="1872">1872</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1872">1872</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Texte vérifié avec l’édition Charpentier et Cie, 1872</p>
				<p>Les corrections signalées dans la version électronique n’ont pas été intégrées ; erreurs absentes de l’édition de référence</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU3">
				<head type="main">LE POÈME DE LA FEMME</head>
				<head type="sub">marbre de paros</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Un</w> <w n="1.2">jour</w>, <w n="1.3">au</w> <w n="1.4">doux</w> <w n="1.5">rêveur</w> <w n="1.6">qui</w> <w n="1.7">l</w>’<w n="1.8">aime</w>,</l>
					<l n="2" num="1.2"><w n="2.1">En</w> <w n="2.2">train</w> <w n="2.3">de</w> <w n="2.4">montrer</w> <w n="2.5">ses</w> <w n="2.6">trésors</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Elle</w> <w n="3.2">voulut</w> <w n="3.3">lire</w> <w n="3.4">un</w> <w n="3.5">poème</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Le</w> <w n="4.2">poème</w> <w n="4.3">de</w> <w n="4.4">son</w> <w n="4.5">beau</w> <w n="4.6">corps</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">D</w>’<w n="5.2">abord</w>, <w n="5.3">superbe</w> <w n="5.4">et</w> <w n="5.5">triomphante</w></l>
					<l n="6" num="2.2"><w n="6.1">Elle</w> <w n="6.2">vint</w> <w n="6.3">en</w> <w n="6.4">grand</w> <w n="6.5">apparat</w>,</l>
					<l n="7" num="2.3"><w n="7.1">Traînant</w> <w n="7.2">avec</w> <w n="7.3">des</w> <w n="7.4">airs</w> <w n="7.5">d</w>’<w n="7.6">infante</w></l>
					<l n="8" num="2.4"><w n="8.1">Un</w> <w n="8.2">flot</w> <w n="8.3">de</w> <w n="8.4">velours</w> <w n="8.5">nacarat</w> :</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Telle</w> <w n="9.2">qu</w>’<w n="9.3">au</w> <w n="9.4">rebord</w> <w n="9.5">de</w> <w n="9.6">sa</w> <w n="9.7">loge</w></l>
					<l n="10" num="3.2"><w n="10.1">Elle</w> <w n="10.2">brille</w> <w n="10.3">aux</w> <w n="10.4">Italiens</w>,</l>
					<l n="11" num="3.3"><w n="11.1">Écoutant</w> <w n="11.2">passer</w> <w n="11.3">son</w> <w n="11.4">éloge</w></l>
					<l n="12" num="3.4"><w n="12.1">Dans</w> <w n="12.2">les</w> <w n="12.3">chants</w> <w n="12.4">des</w> <w n="12.5">musiciens</w></l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Ensuite</w>, <w n="13.2">en</w> <w n="13.3">sa</w> <w n="13.4">verve</w> <w n="13.5">d</w>’<w n="13.6">artiste</w>,</l>
					<l n="14" num="4.2"><w n="14.1">Laissant</w> <w n="14.2">tomber</w> <w n="14.3">l</w>’<w n="14.4">épais</w> <w n="14.5">velours</w>,</l>
					<l n="15" num="4.3"><w n="15.1">Dans</w> <w n="15.2">un</w> <w n="15.3">nuage</w> <w n="15.4">de</w> <w n="15.5">batiste</w></l>
					<l n="16" num="4.4"><w n="16.1">Elle</w> <w n="16.2">ébaucha</w> <w n="16.3">ses</w> <w n="16.4">fiers</w> <w n="16.5">contours</w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">Glissant</w> <w n="17.2">de</w> <w n="17.3">l</w>’<w n="17.4">épaule</w> <w n="17.5">à</w> <w n="17.6">la</w> <w n="17.7">hanche</w>,</l>
					<l n="18" num="5.2"><w n="18.1">La</w> <w n="18.2">chemise</w> <w n="18.3">aux</w> <w n="18.4">plis</w> <w n="18.5">nonchalants</w>,</l>
					<l n="19" num="5.3"><w n="19.1">Comme</w> <w n="19.2">une</w> <w n="19.3">tourterelle</w> <w n="19.4">blanche</w></l>
					<l n="20" num="5.4"><w n="20.1">Vint</w> <w n="20.2">s</w>’<w n="20.3">abattre</w> <w n="20.4">sur</w> <w n="20.5">ses</w> <w n="20.6">pieds</w> <w n="20.7">blancs</w>.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">Pour</w> <w n="21.2">Apelle</w> <w n="21.3">ou</w> <w n="21.4">pour</w> <w n="21.5">Cléomène</w>,</l>
					<l n="22" num="6.2"><w n="22.1">Elle</w> <w n="22.2">semblait</w>, <w n="22.3">marbre</w> <w n="22.4">de</w> <w n="22.5">chair</w>,</l>
					<l n="23" num="6.3"><w n="23.1">En</w> <w n="23.2">Vénus</w> <w n="23.3">Anadyomène</w></l>
					<l n="24" num="6.4"><w n="24.1">Poser</w> <w n="24.2">nue</w> <w n="24.3">au</w> <w n="24.4">bord</w> <w n="24.5">de</w> <w n="24.6">la</w> <w n="24.7">mer</w>.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">De</w> <w n="25.2">grosses</w> <w n="25.3">perles</w> <w n="25.4">de</w> <w n="25.5">Venise</w></l>
					<l n="26" num="7.2"><w n="26.1">Roulaient</w> <w n="26.2">au</w> <w n="26.3">lieu</w> <w n="26.4">de</w> <w n="26.5">gouttes</w> <w n="26.6">d</w>’<w n="26.7">eau</w>,</l>
					<l n="27" num="7.3"><w n="27.1">Grains</w> <w n="27.2">laiteux</w> <w n="27.3">qu</w>’<w n="27.4">un</w> <w n="27.5">rayon</w> <w n="27.6">irise</w>,</l>
					<l n="28" num="7.4"><w n="28.1">Sur</w> <w n="28.2">le</w> <w n="28.3">frais</w> <w n="28.4">satin</w> <w n="28.5">de</w> <w n="28.6">sa</w> <w n="28.7">peau</w>.</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1">Oh</w> ! <w n="29.2">quelles</w> <w n="29.3">ravissantes</w> <w n="29.4">choses</w></l>
					<l n="30" num="8.2"><w n="30.1">Dans</w> <w n="30.2">sa</w> <w n="30.3">divine</w> <w n="30.4">nudité</w>,</l>
					<l n="31" num="8.3"><w n="31.1">Avec</w> <w n="31.2">les</w> <w n="31.3">strophes</w> <w n="31.4">de</w> <w n="31.5">ses</w> <w n="31.6">poses</w>,</l>
					<l n="32" num="8.4"><w n="32.1">Chantait</w> <w n="32.2">cet</w> <w n="32.3">hymne</w> <w n="32.4">de</w> <w n="32.5">beauté</w> !</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1"><w n="33.1">Comme</w> <w n="33.2">les</w> <w n="33.3">flots</w> <w n="33.4">baisant</w> <w n="33.5">le</w> <w n="33.6">sable</w></l>
					<l n="34" num="9.2"><w n="34.1">Sous</w> <w n="34.2">la</w> <w n="34.3">lune</w> <w n="34.4">aux</w> <w n="34.5">tremblants</w> <w n="34.6">rayons</w>,</l>
					<l n="35" num="9.3"><w n="35.1">Sa</w> <w n="35.2">grâce</w> <w n="35.3">était</w> <w n="35.4">intarissable</w></l>
					<l n="36" num="9.4"><w n="36.1">En</w> <w n="36.2">molles</w> <w n="36.3">ondulations</w>.</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1"><w n="37.1">Mais</w> <w n="37.2">bientôt</w>, <w n="37.3">lasse</w> <w n="37.4">d</w>’<w n="37.5">art</w> <w n="37.6">antique</w>,</l>
					<l n="38" num="10.2"><w n="38.1">De</w> <w n="38.2">Phidias</w> <w n="38.3">et</w> <w n="38.4">de</w> <w n="38.5">Vénus</w>,</l>
					<l n="39" num="10.3"><w n="39.1">Dans</w> <w n="39.2">une</w> <w n="39.3">autre</w> <w n="39.4">stance</w> <w n="39.5">plastique</w></l>
					<l n="40" num="10.4"><w n="40.1">Elle</w> <w n="40.2">groupe</w> <w n="40.3">ses</w> <w n="40.4">charmes</w> <w n="40.5">nus</w>.</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1"><w n="41.1">Sur</w> <w n="41.2">un</w> <w n="41.3">tapis</w> <w n="41.4">de</w> <w n="41.5">Cachemire</w>,</l>
					<l n="42" num="11.2"><w n="42.1">C</w>’<w n="42.2">est</w> <w n="42.3">la</w> <w n="42.4">sultane</w> <w n="42.5">du</w> <w n="42.6">sérail</w>,</l>
					<l n="43" num="11.3"><w n="43.1">Riant</w> <w n="43.2">au</w> <w n="43.3">miroir</w> <w n="43.4">qui</w> <w n="43.5">l</w>’<w n="43.6">admire</w></l>
					<l n="44" num="11.4"><w n="44.1">Avec</w> <w n="44.2">un</w> <w n="44.3">rire</w> <w n="44.4">de</w> <w n="44.5">corail</w> ;</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1"><w n="45.1">La</w> <w n="45.2">Géorgienne</w> <w n="45.3">indolente</w>,</l>
					<l n="46" num="12.2"><w n="46.1">Avec</w> <w n="46.2">son</w> <w n="46.3">souple</w> <w n="46.4">narguilhé</w>,</l>
					<l n="47" num="12.3"><w n="47.1">Étalant</w> <w n="47.2">sa</w> <w n="47.3">hanche</w> <w n="47.4">opulente</w>,</l>
					<l n="48" num="12.4"><w n="48.1">Un</w> <w n="48.2">pied</w> <w n="48.3">sous</w> <w n="48.4">l</w>’<w n="48.5">autre</w> <w n="48.6">replié</w>,</l>
				</lg>
				<lg n="13">
					<l n="49" num="13.1"><w n="49.1">Et</w> <w n="49.2">comme</w> <w n="49.3">l</w>’<w n="49.4">odalisque</w> <w n="49.5">d</w>’<w n="49.6">Ingres</w>,</l>
					<l n="50" num="13.2"><w n="50.1">De</w> <w n="50.2">ses</w> <w n="50.3">reins</w> <w n="50.4">cambrant</w> <w n="50.5">les</w> <w n="50.6">rondeurs</w>,</l>
					<l n="51" num="13.3"><w n="51.1">En</w> <w n="51.2">dépit</w> <w n="51.3">des</w> <w n="51.4">vertus</w> <w n="51.5">malingres</w>,</l>
					<l n="52" num="13.4"><w n="52.1">En</w> <w n="52.2">dépit</w> <w n="52.3">des</w> <w n="52.4">maigres</w> <w n="52.5">pudeurs</w> !</l>
				</lg>
				<lg n="14">
					<l n="53" num="14.1"><w n="53.1">Paresseuse</w> <w n="53.2">odalisque</w>, <w n="53.3">arrière</w> !</l>
					<l n="54" num="14.2"><w n="54.1">Voici</w> <w n="54.2">le</w> <w n="54.3">tableau</w> <w n="54.4">dans</w> <w n="54.5">son</w> <w n="54.6">jour</w>,</l>
					<l n="55" num="14.3"><w n="55.1">Le</w> <w n="55.2">diamant</w> <w n="55.3">dans</w> <w n="55.4">sa</w> <w n="55.5">lumière</w> ;</l>
					<l n="56" num="14.4"><w n="56.1">Voici</w> <w n="56.2">la</w> <w n="56.3">beauté</w> <w n="56.4">dans</w> <w n="56.5">l</w>’<w n="56.6">amour</w> !</l>
				</lg>
				<lg n="15">
					<l n="57" num="15.1"><w n="57.1">Sa</w> <w n="57.2">tête</w> <w n="57.3">penche</w> <w n="57.4">et</w> <w n="57.5">se</w> <w n="57.6">renverse</w> ;</l>
					<l n="58" num="15.2"><w n="58.1">Haletante</w>, <w n="58.2">dressant</w> <w n="58.3">les</w> <w n="58.4">seins</w>,</l>
					<l n="59" num="15.3"><w n="59.1">Aux</w> <w n="59.2">bras</w> <w n="59.3">du</w> <w n="59.4">rêve</w> <w n="59.5">qui</w> <w n="59.6">la</w> <w n="59.7">berce</w>,</l>
					<l n="60" num="15.4"><w n="60.1">Elle</w> <w n="60.2">tombe</w> <w n="60.3">sur</w> <w n="60.4">ses</w> <w n="60.5">coussins</w>.</l>
				</lg>
				<lg n="16">
					<l n="61" num="16.1"><w n="61.1">Ses</w> <w n="61.2">paupières</w> <w n="61.3">battent</w> <w n="61.4">des</w> <w n="61.5">ailes</w></l>
					<l n="62" num="16.2"><w n="62.1">Sur</w> <w n="62.2">leurs</w> <w n="62.3">globes</w> <w n="62.4">d</w>’<w n="62.5">argent</w> <w n="62.6">bruni</w>,</l>
					<l n="63" num="16.3"><w n="63.1">Et</w> <w n="63.2">l</w>’<w n="63.3">on</w> <w n="63.4">voit</w> <w n="63.5">monter</w> <w n="63.6">ses</w> <w n="63.7">prunelles</w></l>
					<l n="64" num="16.4"><w n="64.1">Dans</w> <w n="64.2">la</w> <w n="64.3">nacre</w> <w n="64.4">de</w> <w n="64.5">l</w>’<w n="64.6">infini</w>.</l>
				</lg>
				<lg n="17">
					<l n="65" num="17.1"><w n="65.1">D</w>’<w n="65.2">un</w> <w n="65.3">linceul</w> <w n="65.4">de</w> <w n="65.5">point</w> <w n="65.6">d</w>’<w n="65.7">Angleterre</w></l>
					<l n="66" num="17.2"><w n="66.1">Que</w> <w n="66.2">l</w>’<w n="66.3">on</w> <w n="66.4">recouvre</w> <w n="66.5">sa</w> <w n="66.6">beauté</w> :</l>
					<l n="67" num="17.3"><w n="67.1">L</w>’<w n="67.2">extase</w> <w n="67.3">l</w>’<w n="67.4">a</w> <w n="67.5">prise</w> <w n="67.6">à</w> <w n="67.7">la</w> <w n="67.8">terre</w> ;</l>
					<l n="68" num="17.4"><w n="68.1">Elle</w> <w n="68.2">est</w> <w n="68.3">morte</w> <w n="68.4">de</w> <w n="68.5">volupté</w> !</l>
				</lg>
				<lg n="18">
					<l n="69" num="18.1"><w n="69.1">Que</w> <w n="69.2">les</w> <w n="69.3">violettes</w> <w n="69.4">de</w> <w n="69.5">Parme</w>,</l>
					<l n="70" num="18.2"><w n="70.1">Au</w> <w n="70.2">lieu</w> <w n="70.3">des</w> <w n="70.4">tristes</w> <w n="70.5">fleurs</w> <w n="70.6">des</w> <w n="70.7">morts</w></l>
					<l n="71" num="18.3"><w n="71.1">Où</w> <w n="71.2">chaque</w> <w n="71.3">perle</w> <w n="71.4">est</w> <w n="71.5">une</w> <w n="71.6">larme</w>,</l>
					<l n="72" num="18.4"><w n="72.1">Pleurent</w> <w n="72.2">en</w> <w n="72.3">bouquets</w> <w n="72.4">sur</w> <w n="72.5">son</w> <w n="72.6">corps</w> !</l>
				</lg>
				<lg n="19">
					<l n="73" num="19.1"><w n="73.1">Et</w> <w n="73.2">que</w> <w n="73.3">mollement</w> <w n="73.4">on</w> <w n="73.5">la</w> <w n="73.6">pose</w></l>
					<l n="74" num="19.2"><w n="74.1">Sur</w> <w n="74.2">son</w> <w n="74.3">lit</w>, <w n="74.4">tombeau</w> <w n="74.5">blanc</w> <w n="74.6">et</w> <w n="74.7">doux</w>,</l>
					<l n="75" num="19.3"><w n="75.1">Où</w> <w n="75.2">le</w> <w n="75.3">poète</w>, <w n="75.4">à</w> <w n="75.5">la</w> <w n="75.6">nuit</w> <w n="75.7">close</w>,</l>
					<l n="76" num="19.4"><w n="76.1">Ira</w> <w n="76.2">prier</w> <w n="76.3">à</w> <w n="76.4">deux</w> <w n="76.5">genoux</w>.</l>
				</lg>
			</div></body></text></TEI>