<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ÉMAUX ET CAMÉES</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2510 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2011">2012</date>
				<idno type="local">GAU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Émaux et Camées</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Émaux et Camées</title>
								<author>Théophile Gautier</author>
								<imprint>
									<publisher>G. CHARPENTIER et E. FASQUELLE</publisher>
									<date when="1895">1895</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Émaux et Camées</title>
						<author>Théophile Gautier</author>
						<imprint>
							<publisher>G. CHARPENTIER et E. FASQUELLE</publisher>
							<date when="1872">1872</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1872">1872</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Texte vérifié avec l’édition Charpentier et Cie, 1872</p>
				<p>Les corrections signalées dans la version électronique n’ont pas été intégrées ; erreurs absentes de l’édition de référence</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU33">
				<head type="main">CE QUE DISENT LES HIRONDELLES</head>
				<head type="sub">CHANSON D’AUTOMNE</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Déjà</w> <w n="1.2">plus</w> <w n="1.3">d</w>’<w n="1.4">une</w> <w n="1.5">feuille</w> <w n="1.6">sèche</w></l>
					<l n="2" num="1.2"><w n="2.1">Parsème</w> <w n="2.2">les</w> <w n="2.3">gazons</w> <w n="2.4">jaunis</w> ;</l>
					<l n="3" num="1.3"><w n="3.1">Soir</w> <w n="3.2">et</w> <w n="3.3">matin</w>, <w n="3.4">la</w> <w n="3.5">brise</w> <w n="3.6">est</w> <w n="3.7">fraîche</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Hélas</w> ! <w n="4.2">les</w> <w n="4.3">beaux</w> <w n="4.4">jours</w> <w n="4.5">sont</w> <w n="4.6">finis</w> !</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">On</w> <w n="5.2">voit</w> <w n="5.3">s</w>’<w n="5.4">ouvrir</w> <w n="5.5">les</w> <w n="5.6">fleurs</w> <w n="5.7">que</w> <w n="5.8">garde</w></l>
					<l n="6" num="2.2"><w n="6.1">Le</w> <w n="6.2">jardin</w>, <w n="6.3">pour</w> <w n="6.4">dernier</w> <w n="6.5">trésor</w> :</l>
					<l n="7" num="2.3"><w n="7.1">Le</w> <w n="7.2">dahlia</w> <w n="7.3">met</w> <w n="7.4">sa</w> <w n="7.5">cocarde</w></l>
					<l n="8" num="2.4"><w n="8.1">Et</w> <w n="8.2">le</w> <w n="8.3">souci</w> <w n="8.4">sa</w> <w n="8.5">toque</w> <w n="8.6">d</w>’<w n="8.7">or</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">La</w> <w n="9.2">pluie</w> <w n="9.3">au</w> <w n="9.4">bassin</w> <w n="9.5">fait</w> <w n="9.6">des</w> <w n="9.7">bulles</w>,</l>
					<l n="10" num="3.2"><w n="10.1">Les</w> <w n="10.2">hirondelles</w> <w n="10.3">sur</w> <w n="10.4">le</w> <w n="10.5">toit</w></l>
					<l n="11" num="3.3"><w n="11.1">Tiennent</w> <w n="11.2">des</w> <w n="11.3">conciliabules</w> :</l>
					<l n="12" num="3.4"><w n="12.1">Voici</w> <w n="12.2">l</w>’<w n="12.3">hiver</w>, <w n="12.4">voici</w> <w n="12.5">le</w> <w n="12.6">froid</w> !</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Elles</w> <w n="13.2">s</w>’<w n="13.3">assemblent</w> <w n="13.4">par</w> <w n="13.5">centaines</w>,</l>
					<l n="14" num="4.2"><w n="14.1">Se</w> <w n="14.2">concertant</w> <w n="14.3">pour</w> <w n="14.4">le</w> <w n="14.5">départ</w>.</l>
					<l n="15" num="4.3"><w n="15.1">L</w>’<w n="15.2">une</w> <w n="15.3">dit</w> : «<w n="15.4">Oh</w> ! <w n="15.5">que</w> <w n="15.6">dans</w> <w n="15.7">Athènes</w></l>
					<l n="16" num="4.4"><w n="16.1">Il</w> <w n="16.2">fait</w> <w n="16.3">bon</w> <w n="16.4">sur</w> <w n="16.5">le</w> <w n="16.6">vieux</w> <w n="16.7">rempart</w> !</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1">«<w n="17.1">Tous</w> <w n="17.2">les</w> <w n="17.3">ans</w> <w n="17.4">j</w>’<w n="17.5">y</w> <w n="17.6">vais</w> <w n="17.7">et</w> <w n="17.8">je</w> <w n="17.9">niche</w></l>
					<l n="18" num="5.2"><w n="18.1">Aux</w> <w n="18.2">métopes</w> <w n="18.3">du</w> <w n="18.4">Parthénon</w>.</l>
					<l n="19" num="5.3"><w n="19.1">Mon</w> <w n="19.2">nid</w> <w n="19.3">bouche</w> <w n="19.4">dans</w> <w n="19.5">la</w> <w n="19.6">corniche</w></l>
					<l n="20" num="5.4"><w n="20.1">Le</w> <w n="20.2">trou</w> <w n="20.3">d</w>’<w n="20.4">un</w> <w n="20.5">boulet</w> <w n="20.6">de</w> <w n="20.7">canon</w>.»</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">L</w>’<w n="21.2">autre</w> : «<w n="21.3">J</w>’<w n="21.4">ai</w> <w n="21.5">ma</w> <w n="21.6">petite</w> <w n="21.7">chambre</w></l>
					<l n="22" num="6.2"><w n="22.1">A</w> <w n="22.2">Smyrne</w>, <w n="22.3">au</w> <w n="22.4">plafond</w> <w n="22.5">d</w>’<w n="22.6">un</w> <w n="22.7">café</w>.</l>
					<l n="23" num="6.3"><w n="23.1">Les</w> <w n="23.2">Hadjis</w> <w n="23.3">comptent</w> <w n="23.4">leurs</w> <w n="23.5">grains</w> <w n="23.6">d</w>’<w n="23.7">ambre</w></l>
					<l n="24" num="6.4"><w n="24.1">Sur</w> <w n="24.2">le</w> <w n="24.3">seuil</w>, <w n="24.4">d</w>’<w n="24.5">un</w> <w n="24.6">rayon</w> <w n="24.7">chauffé</w>.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1">«<w n="25.1">J</w>’<w n="25.2">entre</w> <w n="25.3">et</w> <w n="25.4">je</w> <w n="25.5">sors</w>, <w n="25.6">accoutumée</w></l>
					<l n="26" num="7.2"><w n="26.1">Aux</w> <w n="26.2">blondes</w> <w n="26.3">vapeurs</w> <w n="26.4">des</w> <w n="26.5">chibouchs</w>,</l>
					<l n="27" num="7.3"><w n="27.1">Et</w> <w n="27.2">parmi</w> <w n="27.3">des</w> <w n="27.4">flots</w> <w n="27.5">de</w> <w n="27.6">fumée</w>,</l>
					<l n="28" num="7.4"><w n="28.1">Je</w> <w n="28.2">rase</w> <w n="28.3">turbans</w> <w n="28.4">et</w> <w n="28.5">tarbouchs</w>.»</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1">Celle</w>-<w n="29.2">ci</w> : «<w n="29.3">J</w>’<w n="29.4">habite</w> <w n="29.5">un</w> <w n="29.6">triglyphe</w></l>
					<l n="30" num="8.2"><w n="30.1">Au</w> <w n="30.2">fronton</w> <w n="30.3">d</w>’<w n="30.4">un</w> <w n="30.5">temple</w>, <w n="30.6">à</w> <w n="30.7">Balbeck</w>.</l>
					<l n="31" num="8.3"><w n="31.1">Je</w> <w n="31.2">m</w>’<w n="31.3">y</w> <w n="31.4">suspends</w> <w n="31.5">avec</w> <w n="31.6">ma</w> <w n="31.7">griffe</w></l>
					<l n="32" num="8.4"><w n="32.1">Sur</w> <w n="32.2">mes</w> <w n="32.3">petits</w> <w n="32.4">au</w> <w n="32.5">large</w> <w n="32.6">bec</w>.»</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1"><w n="33.1">Celle</w>-<w n="33.2">là</w> : «<w n="33.3">Voici</w> <w n="33.4">mon</w> <w n="33.5">adresse</w> :</l>
					<l n="34" num="9.2"><w n="34.1">Rhodes</w>, <w n="34.2">palais</w> <w n="34.3">des</w> <w n="34.4">chevaliers</w> ;</l>
					<l n="35" num="9.3"><w n="35.1">Chaque</w> <w n="35.2">hiver</w>, <w n="35.3">ma</w> <w n="35.4">tente</w> <w n="35.5">s</w>’<w n="35.6">y</w> <w n="35.7">dresse</w></l>
					<l n="36" num="9.4"><w n="36.1">Au</w> <w n="36.2">chapiteau</w> <w n="36.3">des</w> <w n="36.4">noirs</w> <w n="36.5">piliers</w>.»</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1"><w n="37.1">La</w> <w n="37.2">cinquième</w> : «<w n="37.3">Je</w> <w n="37.4">ferai</w> <w n="37.5">halte</w>,</l>
					<l n="38" num="10.2"><w n="38.1">Car</w> <w n="38.2">l</w>’<w n="38.3">âge</w> <w n="38.4">m</w>’<w n="38.5">alourdit</w> <w n="38.6">un</w> <w n="38.7">peu</w>,</l>
					<l n="39" num="10.3"><w n="39.1">Aux</w> <w n="39.2">blanches</w> <w n="39.3">terrasses</w> <w n="39.4">de</w> <w n="39.5">Malte</w>,</l>
					<l n="40" num="10.4"><w n="40.1">Entre</w> <w n="40.2">l</w>’<w n="40.3">eau</w> <w n="40.4">bleue</w> <w n="40.5">et</w> <w n="40.6">le</w> <w n="40.7">ciel</w> <w n="40.8">bleu</w>.»</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1"><w n="41.1">La</w> <w n="41.2">sixième</w> : «<w n="41.3">Qu</w>’<w n="41.4">on</w> <w n="41.5">est</w> <w n="41.6">à</w> <w n="41.7">l</w>’<w n="41.8">aise</w></l>
					<l n="42" num="11.2"><w n="42.1">Au</w> <w n="42.2">Caire</w>, <w n="42.3">en</w> <w n="42.4">haut</w> <w n="42.5">des</w> <w n="42.6">minarets</w> !</l>
					<l n="43" num="11.3"><w n="43.1">J</w>’<w n="43.2">empâte</w> <w n="43.3">un</w> <w n="43.4">ornement</w> <w n="43.5">de</w> <w n="43.6">glaise</w>,</l>
					<l n="44" num="11.4"><w n="44.1">Et</w> <w n="44.2">mes</w> <w n="44.3">quartiers</w> <w n="44.4">d</w>’<w n="44.5">hiver</w> <w n="44.6">sont</w> <w n="44.7">prêts</w>.»</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1">«<w n="45.1">A</w> <w n="45.2">la</w> <w n="45.3">seconde</w> <w n="45.4">cataracte</w>,</l>
					<l n="46" num="12.2"><w n="46.1">Fait</w> <w n="46.2">la</w> <w n="46.3">dernière</w>, <w n="46.4">j</w>’<w n="46.5">ai</w> <w n="46.6">mon</w> <w n="46.7">nid</w> ;</l>
					<l n="47" num="12.3"><w n="47.1">J</w>’<w n="47.2">en</w> <w n="47.3">ai</w> <w n="47.4">noté</w> <w n="47.5">la</w> <w n="47.6">place</w> <w n="47.7">exacte</w>,</l>
					<l n="48" num="12.4"><w n="48.1">Dans</w> <w n="48.2">le</w> <w n="48.3">pschent</w> <w n="48.4">d</w>’<w n="48.5">un</w> <w n="48.6">roi</w> <w n="48.7">de</w> <w n="48.8">granit</w>.»</l>
				</lg>
				<lg n="13">
					<l n="49" num="13.1"><w n="49.1">Toutes</w> : «<w n="49.2">Demain</w> <w n="49.3">combien</w> <w n="49.4">de</w> <w n="49.5">lieues</w></l>
					<l n="50" num="13.2"><w n="50.1">Auront</w> <w n="50.2">filé</w> <w n="50.3">sous</w> <w n="50.4">notre</w> <w n="50.5">essaim</w>,</l>
					<l n="51" num="13.3"><w n="51.1">Plaines</w> <w n="51.2">brunes</w>, <w n="51.3">pics</w> <w n="51.4">blancs</w>, <w n="51.5">mers</w> <w n="51.6">bleues</w></l>
					<l n="52" num="13.4"><w n="52.1">Brodant</w> <w n="52.2">d</w>’<w n="52.3">écume</w> <w n="52.4">leur</w> <w n="52.5">bassin</w> !»</l>
				</lg>
				<lg n="14">
					<l n="53" num="14.1"><w n="53.1">Avec</w> <w n="53.2">cris</w> <w n="53.3">et</w> <w n="53.4">battements</w> <w n="53.5">d</w>’<w n="53.6">ailes</w>,</l>
					<l n="54" num="14.2"><w n="54.1">Sur</w> <w n="54.2">la</w> <w n="54.3">moulure</w> <w n="54.4">aux</w> <w n="54.5">bords</w> <w n="54.6">étroits</w>,</l>
					<l n="55" num="14.3"><w n="55.1">Ainsi</w> <w n="55.2">jasent</w> <w n="55.3">les</w> <w n="55.4">hirondelles</w>,</l>
					<l n="56" num="14.4"><w n="56.1">Voyant</w> <w n="56.2">venir</w> <w n="56.3">la</w> <w n="56.4">rouille</w> <w n="56.5">aux</w> <w n="56.6">bois</w>.</l>
				</lg>
				<lg n="15">
					<l n="57" num="15.1"><w n="57.1">Je</w> <w n="57.2">comprends</w> <w n="57.3">tout</w> <w n="57.4">ce</w> <w n="57.5">qu</w>’<w n="57.6">elles</w> <w n="57.7">disent</w>,</l>
					<l n="58" num="15.2"><w n="58.1">Car</w> <w n="58.2">le</w> <w n="58.3">poëte</w> <w n="58.4">est</w> <w n="58.5">un</w> <w n="58.6">oiseau</w> ;</l>
					<l n="59" num="15.3"><w n="59.1">Mais</w>, <w n="59.2">captif</w>, <w n="59.3">ses</w> <w n="59.4">élans</w> <w n="59.5">se</w> <w n="59.6">brisent</w></l>
					<l n="60" num="15.4"><w n="60.1">Contre</w> <w n="60.2">un</w> <w n="60.3">invisible</w> <w n="60.4">réseau</w> !</l>
				</lg>
				<lg n="16">
					<l n="61" num="16.1"><w n="61.1">Des</w> <w n="61.2">ailes</w> ! <w n="61.3">des</w> <w n="61.4">ailes</w> ! <w n="61.5">des</w> <w n="61.6">ailes</w> !</l>
					<l n="62" num="16.2"><w n="62.1">Comme</w> <w n="62.2">dans</w> <w n="62.3">le</w> <w n="62.4">chant</w> <w n="62.5">de</w> <w n="62.6">Ruckert</w>,</l>
					<l n="63" num="16.3"><w n="63.1">Pour</w> <w n="63.2">voler</w>, <w n="63.3">là</w>-<w n="63.4">bas</w> <w n="63.5">avec</w> <w n="63.6">elles</w></l>
					<l n="64" num="16.4"><w n="64.1">Au</w> <w n="64.2">soleil</w> <w n="64.3">d</w>’<w n="64.4">or</w>, <w n="64.5">au</w> <w n="64.6">printemps</w> <w n="64.7">vert</w> !</l>
				</lg>
			</div></body></text></TEI>