<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ÉMAUX ET CAMÉES</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2510 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2011">2012</date>
				<idno type="local">GAU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Émaux et Camées</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Émaux et Camées</title>
								<author>Théophile Gautier</author>
								<imprint>
									<publisher>G. CHARPENTIER et E. FASQUELLE</publisher>
									<date when="1895">1895</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Émaux et Camées</title>
						<author>Théophile Gautier</author>
						<imprint>
							<publisher>G. CHARPENTIER et E. FASQUELLE</publisher>
							<date when="1872">1872</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1872">1872</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Texte vérifié avec l’édition Charpentier et Cie, 1872</p>
				<p>Les corrections signalées dans la version électronique n’ont pas été intégrées ; erreurs absentes de l’édition de référence</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU26">
				<head type="main">BUCHERS ET TOMBEAUX</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Le</w> <w n="1.2">squelette</w> <w n="1.3">était</w> <w n="1.4">invisible</w></l>
					<l n="2" num="1.2"><w n="2.1">Au</w> <w n="2.2">temps</w> <w n="2.3">heureux</w> <w n="2.4">de</w> <w n="2.5">l</w>’<w n="2.6">Art</w> <w n="2.7">païen</w> ;</l>
					<l n="3" num="1.3"><w n="3.1">L</w>’<w n="3.2">homme</w>, <w n="3.3">sous</w> <w n="3.4">la</w> <w n="3.5">forme</w> <w n="3.6">sensible</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Content</w> <w n="4.2">du</w> <w n="4.3">beau</w>, <w n="4.4">ne</w> <w n="4.5">cherchait</w> <w n="4.6">rien</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Pas</w> <w n="5.2">de</w> <w n="5.3">cadavre</w> <w n="5.4">sous</w> <w n="5.5">la</w> <w n="5.6">tombe</w>,</l>
					<l n="6" num="2.2"><w n="6.1">Spectre</w> <w n="6.2">hideux</w> <w n="6.3">de</w> <w n="6.4">l</w>’<w n="6.5">être</w> <w n="6.6">cher</w>,</l>
					<l n="7" num="2.3"><w n="7.1">Comme</w> <w n="7.2">d</w>’<w n="7.3">un</w> <w n="7.4">vêtement</w> <w n="7.5">qui</w> <w n="7.6">tombe</w></l>
					<l n="8" num="2.4"><w n="8.1">Se</w> <w n="8.2">déshabillant</w> <w n="8.3">de</w> <w n="8.4">sa</w> <w n="8.5">chair</w>,</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Et</w>, <w n="9.2">quand</w> <w n="9.3">la</w> <w n="9.4">pierre</w> <w n="9.5">se</w> <w n="9.6">lézarde</w>,</l>
					<l n="10" num="3.2"><w n="10.1">Parmi</w> <w n="10.2">les</w> <w n="10.3">épouvantements</w>,</l>
					<l n="11" num="3.3"><w n="11.1">Montrant</w> <w n="11.2">à</w> <w n="11.3">l</w>’<w n="11.4">œil</w> <w n="11.5">qui</w> <w n="11.6">s</w>’<w n="11.7">y</w> <w n="11.8">hasarde</w></l>
					<l n="12" num="3.4"><w n="12.1">Une</w> <w n="12.2">armature</w> <w n="12.3">d</w>’<w n="12.4">ossements</w> ;</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Mais</w> <w n="13.2">au</w> <w n="13.3">feu</w> <w n="13.4">du</w> <w n="13.5">bûcher</w> <w n="13.6">ravie</w></l>
					<l n="14" num="4.2"><w n="14.1">Une</w> <w n="14.2">pincée</w> <w n="14.3">entre</w> <w n="14.4">les</w> <w n="14.5">doigts</w>,</l>
					<l n="15" num="4.3"><w n="15.1">Résidu</w> <w n="15.2">léger</w> <w n="15.3">de</w> <w n="15.4">la</w> <w n="15.5">vie</w>,</l>
					<l n="16" num="4.4"><w n="16.1">Qu</w>’<w n="16.2">enserrait</w> <w n="16.3">l</w>’<w n="16.4">urne</w> <w n="16.5">aux</w> <w n="16.6">flancs</w> <w n="16.7">étroits</w> ;</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">Ce</w> <w n="17.2">que</w> <w n="17.3">le</w> <w n="17.4">papillon</w> <w n="17.5">de</w> <w n="17.6">l</w>’<w n="17.7">âme</w></l>
					<l n="18" num="5.2"><w n="18.1">Laisse</w> <w n="18.2">de</w> <w n="18.3">poussière</w> <w n="18.4">après</w> <w n="18.5">lui</w>,</l>
					<l n="19" num="5.3"><w n="19.1">Et</w> <w n="19.2">ce</w> <w n="19.3">qui</w> <w n="19.4">reste</w> <w n="19.5">de</w> <w n="19.6">la</w> <w n="19.7">flamme</w></l>
					<l n="20" num="5.4"><w n="20.1">Sur</w> <w n="20.2">le</w> <w n="20.3">trépied</w>, <w n="20.4">quand</w> <w n="20.5">elle</w> <w n="20.6">a</w> <w n="20.7">lui</w> !</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">Entre</w> <w n="21.2">les</w> <w n="21.3">fleurs</w> <w n="21.4">et</w> <w n="21.5">les</w> <w n="21.6">acanthes</w>,</l>
					<l n="22" num="6.2"><w n="22.1">Dans</w> <w n="22.2">le</w> <w n="22.3">marbre</w> <w n="22.4">joyeusement</w>,</l>
					<l n="23" num="6.3"><w n="23.1">Amours</w>, <w n="23.2">ægipans</w> <w n="23.3">et</w> <w n="23.4">bacchantes</w></l>
					<l n="24" num="6.4"><w n="24.1">Dansaient</w> <w n="24.2">autour</w> <w n="24.3">du</w> <w n="24.4">monument</w> ;</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">Tout</w> <w n="25.2">au</w> <w n="25.3">plus</w> <w n="25.4">un</w> <w n="25.5">petit</w> <w n="25.6">génie</w></l>
					<l n="26" num="7.2"><w n="26.1">Du</w> <w n="26.2">pied</w> <w n="26.3">éteignait</w> <w n="26.4">un</w> <w n="26.5">flambeau</w> ;</l>
					<l n="27" num="7.3"><w n="27.1">Et</w> <w n="27.2">l</w>’<w n="27.3">art</w> <w n="27.4">versait</w> <w n="27.5">son</w> <w n="27.6">harmonie</w></l>
					<l n="28" num="7.4"><w n="28.1">Sur</w> <w n="28.2">la</w> <w n="28.3">tristesse</w> <w n="28.4">du</w> <w n="28.5">tombeau</w>.</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1">Les</w> <w n="29.2">tombes</w> <w n="29.3">étaient</w> <w n="29.4">attrayantes</w> :</l>
					<l n="30" num="8.2"><w n="30.1">Comme</w> <w n="30.2">on</w> <w n="30.3">fait</w> <w n="30.4">d</w>’<w n="30.5">un</w> <w n="30.6">enfant</w> <w n="30.7">qui</w> <w n="30.8">dort</w>,</l>
					<l n="31" num="8.3"><w n="31.1">D</w>’<w n="31.2">images</w> <w n="31.3">douces</w> <w n="31.4">et</w> <w n="31.5">riantes</w></l>
					<l n="32" num="8.4"><w n="32.1">La</w> <w n="32.2">vie</w> <w n="32.3">enveloppait</w> <w n="32.4">la</w> <w n="32.5">mort</w> ;</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1"><w n="33.1">La</w> <w n="33.2">mort</w> <w n="33.3">dissimulait</w> <w n="33.4">sa</w> <w n="33.5">face</w></l>
					<l n="34" num="9.2"><w n="34.1">Aux</w> <w n="34.2">trous</w> <w n="34.3">profonds</w>, <w n="34.4">au</w> <w n="34.5">nez</w> <w n="34.6">camard</w>,</l>
					<l n="35" num="9.3"><w n="35.1">Dont</w> <w n="35.2">la</w> <w n="35.3">hideur</w> <w n="35.4">railleuse</w> <w n="35.5">efface</w></l>
					<l n="36" num="9.4"><w n="36.1">Les</w> <w n="36.2">chimères</w> <w n="36.3">du</w> <w n="36.4">cauchemar</w>.</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1"><w n="37.1">Le</w> <w n="37.2">monstre</w>, <w n="37.3">sous</w> <w n="37.4">la</w> <w n="37.5">chair</w> <w n="37.6">splendide</w></l>
					<l n="38" num="10.2"><w n="38.1">Cachait</w> <w n="38.2">son</w> <w n="38.3">fantôme</w> <w n="38.4">inconnu</w>,</l>
					<l n="39" num="10.3"><w n="39.1">Et</w> <w n="39.2">l</w>’<w n="39.3">œil</w> <w n="39.4">de</w> <w n="39.5">la</w> <w n="39.6">vierge</w> <w n="39.7">candide</w></l>
					<l n="40" num="10.4"><w n="40.1">Allait</w> <w n="40.2">au</w> <w n="40.3">bel</w> <w n="40.4">éphèbe</w> <w n="40.5">nu</w>.</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1"><w n="41.1">Seulement</w> <w n="41.2">pour</w> <w n="41.3">pousser</w> <w n="41.4">à</w> <w n="41.5">boire</w>,</l>
					<l n="42" num="11.2"><w n="42.1">Au</w> <w n="42.2">banquet</w> <w n="42.3">de</w> <w n="42.4">Trimalcion</w>,</l>
					<l n="43" num="11.3"><w n="43.1">Une</w> <w n="43.2">larve</w>, <w n="43.3">joujou</w> <w n="43.4">d</w>’<w n="43.5">ivoire</w>,</l>
					<l n="44" num="11.4"><w n="44.1">Faisait</w> <w n="44.2">son</w> <w n="44.3">apparition</w> ;</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1"><w n="45.1">Des</w> <w n="45.2">dieux</w> <w n="45.3">que</w> <w n="45.4">l</w>’<w n="45.5">art</w> <w n="45.6">toujours</w> <w n="45.7">révère</w></l>
					<l n="46" num="12.2"><w n="46.1">Trônaient</w> <w n="46.2">au</w> <w n="46.3">ciel</w> <w n="46.4">marmoréen</w> ;</l>
					<l n="47" num="12.3"><w n="47.1">Mais</w> <w n="47.2">l</w>’<w n="47.3">Olympe</w> <w n="47.4">cède</w> <w n="47.5">au</w> <w n="47.6">Calvaire</w>,</l>
					<l n="48" num="12.4"><w n="48.1">Jupiter</w> <w n="48.2">au</w> <w n="48.3">Nazaréen</w> ;</l>
				</lg>
				<lg n="13">
					<l n="49" num="13.1"><w n="49.1">Une</w> <w n="49.2">voix</w> <w n="49.3">dit</w> : <w n="49.4">Pan</w> <w n="49.5">est</w> <w n="49.6">mort</w> !— <w n="49.7">L</w>’<w n="49.8">ombre</w></l>
					<l n="50" num="13.2"><w n="50.1">S</w>’<w n="50.2">étend</w>.— <w n="50.3">Comme</w> <w n="50.4">sur</w> <w n="50.5">un</w> <w n="50.6">drap</w> <w n="50.7">noir</w>,</l>
					<l n="51" num="13.3"><w n="51.1">Sur</w> <w n="51.2">la</w> <w n="51.3">tristesse</w> <w n="51.4">immense</w> <w n="51.5">et</w> <w n="51.6">sombre</w></l>
					<l n="52" num="13.4"><w n="52.1">Le</w> <w n="52.2">blanc</w> <w n="52.3">squelette</w> <w n="52.4">se</w> <w n="52.5">fait</w> <w n="52.6">voir</w> ;</l>
				</lg>
				<lg n="14">
					<l n="53" num="14.1"><w n="53.1">Il</w> <w n="53.2">signe</w> <w n="53.3">les</w> <w n="53.4">pierres</w> <w n="53.5">funèbres</w></l>
					<l n="54" num="14.2"><w n="54.1">De</w> <w n="54.2">son</w> <w n="54.3">paraphe</w> <w n="54.4">de</w> <w n="54.5">fémurs</w>,</l>
					<l n="55" num="14.3"><w n="55.1">Pend</w> <w n="55.2">son</w> <w n="55.3">chapelet</w> <w n="55.4">de</w> <w n="55.5">vertèbres</w></l>
					<l n="56" num="14.4"><w n="56.1">Dans</w> <w n="56.2">les</w> <w n="56.3">charniers</w>, <w n="56.4">le</w> <w n="56.5">long</w> <w n="56.6">des</w> <w n="56.7">murs</w>,</l>
				</lg>
				<lg n="15">
					<l n="57" num="15.1"><w n="57.1">Des</w> <w n="57.2">cercueils</w> <w n="57.3">lève</w> <w n="57.4">le</w> <w n="57.5">couvercle</w></l>
					<l n="58" num="15.2"><w n="58.1">Avec</w> <w n="58.2">ses</w> <w n="58.3">bras</w> <w n="58.4">aux</w> <w n="58.5">os</w> <w n="58.6">pointus</w> :</l>
					<l n="59" num="15.3"><w n="59.1">Dessine</w> <w n="59.2">ses</w> <w n="59.3">côtes</w> <w n="59.4">en</w> <w n="59.5">cercle</w></l>
					<l n="60" num="15.4"><w n="60.1">Et</w> <w n="60.2">rit</w> <w n="60.3">de</w> <w n="60.4">son</w> <w n="60.5">large</w> <w n="60.6">rictus</w> ;</l>
				</lg>
				<lg n="16">
					<l n="61" num="16.1"><w n="61.1">Il</w> <w n="61.2">pousse</w> <w n="61.3">à</w> <w n="61.4">la</w> <w n="61.5">danse</w> <w n="61.6">macabre</w></l>
					<l n="62" num="16.2"><w n="62.1">L</w>’<w n="62.2">empereur</w>, <w n="62.3">le</w> <w n="62.4">pape</w> <w n="62.5">et</w> <w n="62.6">le</w> <w n="62.7">roi</w>,</l>
					<l n="63" num="16.3"><w n="63.1">Et</w> <w n="63.2">de</w> <w n="63.3">son</w> <w n="63.4">cheval</w> <w n="63.5">qui</w> <w n="63.6">se</w> <w n="63.7">cabre</w></l>
					<l n="64" num="16.4"><w n="64.1">Jette</w> <w n="64.2">bas</w> <w n="64.3">le</w> <w n="64.4">preux</w> <w n="64.5">plein</w> <w n="64.6">d</w>’<w n="64.7">effroi</w> ;</l>
				</lg>
				<lg n="17">
					<l n="65" num="17.1"><w n="65.1">Il</w> <w n="65.2">entre</w> <w n="65.3">chez</w> <w n="65.4">la</w> <w n="65.5">courtisane</w></l>
					<l n="66" num="17.2"><w n="66.1">Et</w> <w n="66.2">fait</w> <w n="66.3">des</w> <w n="66.4">mines</w> <w n="66.5">au</w> <w n="66.6">miroir</w>,</l>
					<l n="67" num="17.3"><w n="67.1">Du</w> <w n="67.2">malade</w> <w n="67.3">il</w> <w n="67.4">boit</w> <w n="67.5">la</w> <w n="67.6">tisane</w>,</l>
					<l n="68" num="17.4"><w n="68.1">De</w> <w n="68.2">l</w>’<w n="68.3">avare</w> <w n="68.4">ouvre</w> <w n="68.5">le</w> <w n="68.6">tiroir</w> ;</l>
				</lg>
				<lg n="18">
					<l n="69" num="18.1"><w n="69.1">Piquant</w> <w n="69.2">l</w>’<w n="69.3">attelage</w> <w n="69.4">qui</w> <w n="69.5">rue</w></l>
					<l n="70" num="18.2"><w n="70.1">Avec</w> <w n="70.2">un</w> <w n="70.3">os</w> <w n="70.4">pour</w> <w n="70.5">aiguillon</w>,</l>
					<l n="71" num="18.3"><w n="71.1">Du</w> <w n="71.2">laboureur</w> <w n="71.3">à</w> <w n="71.4">la</w> <w n="71.5">charrue</w></l>
					<l n="72" num="18.4"><w n="72.1">Termine</w> <w n="72.2">en</w> <w n="72.3">fosse</w> <w n="72.4">le</w> <w n="72.5">sillon</w> ;</l>
				</lg>
				<lg n="19">
					<l n="73" num="19.1"><w n="73.1">Et</w>, <w n="73.2">parmi</w> <w n="73.3">la</w> <w n="73.4">foule</w> <w n="73.5">priée</w>,</l>
					<l n="74" num="19.2"><w n="74.1">Hôte</w> <w n="74.2">inattendu</w>, <w n="74.3">sous</w> <w n="74.4">le</w> <w n="74.5">banc</w>,</l>
					<l n="75" num="19.3"><w n="75.1">Vole</w> <w n="75.2">à</w> <w n="75.3">la</w> <w n="75.4">pâle</w> <w n="75.5">mariée</w></l>
					<l n="76" num="19.4"><w n="76.1">Sa</w> <w n="76.2">jarretière</w> <w n="76.3">de</w> <w n="76.4">ruban</w>.</l>
				</lg>
				<lg n="20">
					<l n="77" num="20.1"><w n="77.1">A</w> <w n="77.2">chaque</w> <w n="77.3">pas</w> <w n="77.4">grossit</w> <w n="77.5">la</w> <w n="77.6">bande</w> ;</l>
					<l n="78" num="20.2"><w n="78.1">Le</w> <w n="78.2">jeune</w> <w n="78.3">au</w> <w n="78.4">vieux</w> <w n="78.5">donne</w> <w n="78.6">la</w> <w n="78.7">main</w> ;</l>
					<l n="79" num="20.3"><w n="79.1">L</w>’<w n="79.2">irrésistible</w> <w n="79.3">sarabande</w></l>
					<l n="80" num="20.4"><w n="80.1">Met</w> <w n="80.2">en</w> <w n="80.3">branle</w> <w n="80.4">le</w> <w n="80.5">genre</w> <w n="80.6">humain</w>.</l>
				</lg>
				<lg n="21">
					<l n="81" num="21.1"><w n="81.1">Le</w> <w n="81.2">spectre</w> <w n="81.3">en</w> <w n="81.4">tête</w> <w n="81.5">se</w> <w n="81.6">déhanche</w>,</l>
					<l n="82" num="21.2"><w n="82.1">Dansant</w> <w n="82.2">et</w> <w n="82.3">jouant</w> <w n="82.4">du</w> <w n="82.5">rebec</w>,</l>
					<l n="83" num="21.3"><w n="83.1">Et</w> <w n="83.2">sur</w> <w n="83.3">fond</w> <w n="83.4">noir</w>, <w n="83.5">en</w> <w n="83.6">couleur</w> <w n="83.7">blanche</w>,</l>
					<l n="84" num="21.4"><w n="84.1">Holbein</w> <w n="84.2">l</w>’<w n="84.3">esquisse</w> <w n="84.4">d</w>’<w n="84.5">un</w> <w n="84.6">trait</w> <w n="84.7">sec</w>.</l>
				</lg>
				<lg n="22">
					<l n="85" num="22.1"><w n="85.1">Quand</w> <w n="85.2">le</w> <w n="85.3">siècle</w> <w n="85.4">devient</w> <w n="85.5">frivole</w></l>
					<l n="86" num="22.2"><w n="86.1">Il</w> <w n="86.2">suit</w> <w n="86.3">la</w> <w n="86.4">mode</w> ; <w n="86.5">en</w> <w n="86.6">tonnelet</w></l>
					<l n="87" num="22.3"><w n="87.1">Retrousse</w> <w n="87.2">son</w> <w n="87.3">linceul</w> <w n="87.4">et</w> <w n="87.5">vole</w></l>
					<l n="88" num="22.4"><w n="88.1">Comme</w> <w n="88.2">un</w> <w n="88.3">Cupidon</w> <w n="88.4">de</w> <w n="88.5">ballet</w></l>
				</lg>
				<lg n="23">
					<l n="89" num="23.1"><w n="89.1">Au</w> <w n="89.2">tombeau</w>-<w n="89.3">sofa</w> <w n="89.4">des</w> <w n="89.5">marquises</w></l>
					<l n="90" num="23.2"><w n="90.1">Qui</w> <w n="90.2">reposent</w>, <w n="90.3">lasses</w> <w n="90.4">d</w>’<w n="90.5">amour</w>,</l>
					<l n="91" num="23.3"><w n="91.1">En</w> <w n="91.2">des</w> <w n="91.3">attitudes</w> <w n="91.4">exquises</w>,</l>
					<l n="92" num="23.4"><w n="92.1">Dans</w> <w n="92.2">les</w> <w n="92.3">chapelles</w> <w n="92.4">Pompadour</w>.</l>
				</lg>
				<lg n="24">
					<l n="93" num="24.1"><w n="93.1">Mais</w> <w n="93.2">voile</w>-<w n="93.3">toi</w>, <w n="93.4">masque</w> <w n="93.5">sans</w> <w n="93.6">joues</w>,</l>
					<l n="94" num="24.2"><w n="94.1">Comédien</w> <w n="94.2">que</w> <w n="94.3">le</w> <w n="94.4">ver</w> <w n="94.5">mord</w>,</l>
					<l n="95" num="24.3"><w n="95.1">Depuis</w> <w n="95.2">assez</w> <w n="95.3">longtemps</w> <w n="95.4">tu</w> <w n="95.5">joues</w></l>
					<l n="96" num="24.4"><w n="96.1">Le</w> <w n="96.2">mélodrame</w> <w n="96.3">de</w> <w n="96.4">la</w> <w n="96.5">Mort</w>.</l>
				</lg>
				<lg n="25">
					<l n="97" num="25.1"><w n="97.1">Reviens</w>, <w n="97.2">reviens</w>, <w n="97.3">bel</w> <w n="97.4">art</w> <w n="97.5">antique</w>,</l>
					<l n="98" num="25.2"><w n="98.1">De</w> <w n="98.2">ton</w> <w n="98.3">paros</w> <w n="98.4">étincelant</w></l>
					<l n="99" num="25.3"><w n="99.1">Couvrir</w> <w n="99.2">ce</w> <w n="99.3">squelette</w> <w n="99.4">gothique</w> ;</l>
					<l n="100" num="25.4"><w n="100.1">Dévore</w>-<w n="100.2">le</w>, <w n="100.3">bûcher</w> <w n="100.4">brûlant</w> !</l>
				</lg>
				<lg n="26">
					<l n="101" num="26.1"><w n="101.1">Si</w> <w n="101.2">nous</w> <w n="101.3">sommes</w> <w n="101.4">une</w> <w n="101.5">statue</w></l>
					<l n="102" num="26.2"><w n="102.1">Sculptée</w> <w n="102.2">à</w> <w n="102.3">l</w>’<w n="102.4">image</w> <w n="102.5">de</w> <w n="102.6">Dieu</w>,</l>
					<l n="103" num="26.3"><w n="103.1">Quand</w> <w n="103.2">cette</w> <w n="103.3">image</w> <w n="103.4">est</w> <w n="103.5">abattue</w>,</l>
					<l n="104" num="26.4"><w n="104.1">Jetons</w>-<w n="104.2">en</w> <w n="104.3">les</w> <w n="104.4">débris</w> <w n="104.5">au</w> <w n="104.6">feu</w>.</l>
				</lg>
				<lg n="27">
					<l n="105" num="27.1"><w n="105.1">Toi</w>, <w n="105.2">forme</w> <w n="105.3">immortelle</w>, <w n="105.4">remonte</w></l>
					<l n="106" num="27.2"><w n="106.1">Dans</w> <w n="106.2">la</w> <w n="106.3">flamme</w> <w n="106.4">aux</w> <w n="106.5">sources</w> <w n="106.6">du</w> <w n="106.7">beau</w>,</l>
					<l n="107" num="27.3"><w n="107.1">Sans</w> <w n="107.2">que</w> <w n="107.3">ton</w> <w n="107.4">argile</w> <w n="107.5">ait</w> <w n="107.6">la</w> <w n="107.7">honte</w></l>
					<l n="108" num="27.4"><w n="108.1">Et</w> <w n="108.2">les</w> <w n="108.3">misères</w> <w n="108.4">du</w> <w n="108.5">tombeau</w> !</l>
				</lg>
			</div></body></text></TEI>