<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ÉMAUX ET CAMÉES</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2510 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2011">2012</date>
				<idno type="local">GAU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Émaux et Camées</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Émaux et Camées</title>
								<author>Théophile Gautier</author>
								<imprint>
									<publisher>G. CHARPENTIER et E. FASQUELLE</publisher>
									<date when="1895">1895</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Émaux et Camées</title>
						<author>Théophile Gautier</author>
						<imprint>
							<publisher>G. CHARPENTIER et E. FASQUELLE</publisher>
							<date when="1872">1872</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1872">1872</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Texte vérifié avec l’édition Charpentier et Cie, 1872</p>
				<p>Les corrections signalées dans la version électronique n’ont pas été intégrées ; erreurs absentes de l’édition de référence</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU7">
				<head type="main">COQUETTERIE POSTHUME</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Quand</w> <w n="1.2">je</w> <w n="1.3">mourrai</w>, <w n="1.4">que</w> <w n="1.5">l</w>’<w n="1.6">on</w> <w n="1.7">me</w> <w n="1.8">mette</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Avant</w> <w n="2.2">de</w> <w n="2.3">clouer</w> <w n="2.4">mon</w> <w n="2.5">cercueil</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Un</w> <w n="3.2">peu</w> <w n="3.3">de</w> <w n="3.4">rouge</w> <w n="3.5">à</w> <w n="3.6">la</w> <w n="3.7">pommette</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Un</w> <w n="4.2">peu</w> <w n="4.3">de</w> <w n="4.4">noir</w> <w n="4.5">au</w> <w n="4.6">bord</w> <w n="4.7">de</w> <w n="4.8">l</w>’<w n="4.9">œil</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Car</w> <w n="5.2">je</w> <w n="5.3">veux</w>, <w n="5.4">dans</w> <w n="5.5">ma</w> <w n="5.6">bière</w> <w n="5.7">close</w>,</l>
					<l n="6" num="2.2"><w n="6.1">Comme</w> <w n="6.2">le</w> <w n="6.3">soir</w> <w n="6.4">de</w> <w n="6.5">son</w> <w n="6.6">aveu</w>,</l>
					<l n="7" num="2.3"><w n="7.1">Rester</w> <w n="7.2">éternellement</w> <w n="7.3">rose</w></l>
					<l n="8" num="2.4"><w n="8.1">Avec</w> <w n="8.2">du</w> <w n="8.3">kh</w>’<w n="8.4">ol</w> <w n="8.5">sous</w> <w n="8.6">mon</w> <w n="8.7">œil</w> <w n="8.8">bleu</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Pas</w> <w n="9.2">de</w> <w n="9.3">suaire</w> <w n="9.4">en</w> <w n="9.5">toile</w> <w n="9.6">fine</w>,</l>
					<l n="10" num="3.2"><w n="10.1">Mais</w> <w n="10.2">drapez</w>-<w n="10.3">moi</w> <w n="10.4">dans</w> <w n="10.5">les</w> <w n="10.6">plis</w> <w n="10.7">blancs</w></l>
					<l n="11" num="3.3"><w n="11.1">De</w> <w n="11.2">ma</w> <w n="11.3">robe</w> <w n="11.4">de</w> <w n="11.5">mousseline</w>,</l>
					<l n="12" num="3.4"><w n="12.1">De</w> <w n="12.2">ma</w> <w n="12.3">robe</w> <w n="12.4">à</w> <w n="12.5">treize</w> <w n="12.6">volants</w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">C</w>’<w n="13.2">est</w> <w n="13.3">ma</w> <w n="13.4">parure</w> <w n="13.5">préférée</w> ;</l>
					<l n="14" num="4.2"><w n="14.1">Je</w> <w n="14.2">la</w> <w n="14.3">portais</w> <w n="14.4">quand</w> <w n="14.5">je</w> <w n="14.6">lui</w> <w n="14.7">plus</w>.</l>
					<l n="15" num="4.3"><w n="15.1">Son</w> <w n="15.2">premier</w> <w n="15.3">regard</w> <w n="15.4">l</w>’<w n="15.5">a</w> <w n="15.6">sacrée</w>,</l>
					<l n="16" num="4.4"><w n="16.1">Et</w> <w n="16.2">depuis</w> <w n="16.3">je</w> <w n="16.4">ne</w> <w n="16.5">la</w> <w n="16.6">mis</w> <w n="16.7">plus</w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">Posez</w>-<w n="17.2">moi</w>, <w n="17.3">sans</w> <w n="17.4">jaune</w> <w n="17.5">immortelle</w>,</l>
					<l n="18" num="5.2"><w n="18.1">Sans</w> <w n="18.2">coussin</w> <w n="18.3">de</w> <w n="18.4">larmes</w> <w n="18.5">brodé</w>,</l>
					<l n="19" num="5.3"><w n="19.1">Sur</w> <w n="19.2">mon</w> <w n="19.3">oreiller</w> <w n="19.4">de</w> <w n="19.5">dentelle</w></l>
					<l n="20" num="5.4"><w n="20.1">De</w> <w n="20.2">ma</w> <w n="20.3">chevelure</w> <w n="20.4">inondé</w>.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">Cet</w> <w n="21.2">oreiller</w>, <w n="21.3">dans</w> <w n="21.4">les</w> <w n="21.5">nuits</w> <w n="21.6">folles</w>,</l>
					<l n="22" num="6.2"><w n="22.1">A</w> <w n="22.2">vu</w> <w n="22.3">dormir</w> <w n="22.4">nos</w> <w n="22.5">fronts</w> <w n="22.6">unis</w>,</l>
					<l n="23" num="6.3"><w n="23.1">Et</w> <w n="23.2">sous</w> <w n="23.3">le</w> <w n="23.4">drap</w> <w n="23.5">noir</w> <w n="23.6">des</w> <w n="23.7">gondoles</w></l>
					<l n="24" num="6.4"><w n="24.1">Compté</w> <w n="24.2">nos</w> <w n="24.3">baisers</w> <w n="24.4">infinis</w>.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">Entre</w> <w n="25.2">mes</w> <w n="25.3">mains</w> <w n="25.4">de</w> <w n="25.5">cire</w> <w n="25.6">pâle</w>,</l>
					<l n="26" num="7.2"><w n="26.1">Que</w> <w n="26.2">la</w> <w n="26.3">prière</w> <w n="26.4">réunit</w>,</l>
					<l n="27" num="7.3"><w n="27.1">Tournez</w> <w n="27.2">ce</w> <w n="27.3">chapelet</w> <w n="27.4">d</w>’<w n="27.5">opale</w>,</l>
					<l n="28" num="7.4"><w n="28.1">Par</w> <w n="28.2">le</w> <w n="28.3">pape</w> <w n="28.4">à</w> <w n="28.5">Rome</w> <w n="28.6">bénit</w> :</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1">Je</w> <w n="29.2">l</w>’<w n="29.3">égrènerai</w> <w n="29.4">dans</w> <w n="29.5">la</w> <w n="29.6">couche</w></l>
					<l n="30" num="8.2"><w n="30.1">D</w>’<w n="30.2">où</w> <w n="30.3">nul</w> <w n="30.4">encor</w> <w n="30.5">ne</w> <w n="30.6">s</w>’<w n="30.7">est</w> <w n="30.8">levé</w> ;</l>
					<l n="31" num="8.3"><w n="31.1">Sa</w> <w n="31.2">bouche</w> <w n="31.3">en</w> <w n="31.4">a</w> <w n="31.5">dit</w> <w n="31.6">sur</w> <w n="31.7">ma</w> <w n="31.8">bouche</w></l>
					<l n="32" num="8.4"><w n="32.1">Chaque</w> <hi rend="ital"><w n="32.2">Pater</w></hi> <w n="32.3">et</w> <w n="32.4">chaque</w> <hi rend="ital"><w n="32.5">Ave</w></hi>.</l>
				</lg>
			</div></body></text></TEI>