<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ÉMAUX ET CAMÉES</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2510 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2011">2012</date>
				<idno type="local">GAU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Émaux et Camées</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Émaux et Camées</title>
								<author>Théophile Gautier</author>
								<imprint>
									<publisher>G. CHARPENTIER et E. FASQUELLE</publisher>
									<date when="1895">1895</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Émaux et Camées</title>
						<author>Théophile Gautier</author>
						<imprint>
							<publisher>G. CHARPENTIER et E. FASQUELLE</publisher>
							<date when="1872">1872</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1872">1872</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Texte vérifié avec l’édition Charpentier et Cie, 1872</p>
				<p>Les corrections signalées dans la version électronique n’ont pas été intégrées ; erreurs absentes de l’édition de référence</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU11">
				<head type="main">CÆRULEI OCULI</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Une</w> <w n="1.2">femme</w> <w n="1.3">mystérieuse</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Dont</w> <w n="2.2">la</w> <w n="2.3">beauté</w> <w n="2.4">trouble</w> <w n="2.5">mes</w> <w n="2.6">sens</w></l>
					<l n="3" num="1.3"><w n="3.1">Se</w> <w n="3.2">tient</w> <w n="3.3">debout</w>, <w n="3.4">silencieuse</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Au</w> <w n="4.2">bord</w> <w n="4.3">des</w> <w n="4.4">flots</w> <w n="4.5">retentissants</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Ses</w> <w n="5.2">yeux</w>, <w n="5.3">où</w> <w n="5.4">le</w> <w n="5.5">ciel</w> <w n="5.6">se</w> <w n="5.7">reflète</w>,</l>
					<l n="6" num="2.2"><w n="6.1">Mêlent</w> <w n="6.2">à</w> <w n="6.3">leur</w> <w n="6.4">azur</w> <w n="6.5">amer</w>,</l>
					<l n="7" num="2.3"><w n="7.1">Qu</w>’<w n="7.2">étoile</w> <w n="7.3">une</w> <w n="7.4">humide</w> <w n="7.5">paillette</w>,</l>
					<l n="8" num="2.4"><w n="8.1">Les</w> <w n="8.2">teintes</w> <w n="8.3">glauques</w> <w n="8.4">de</w> <w n="8.5">la</w> <w n="8.6">mer</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Dans</w> <w n="9.2">les</w> <w n="9.3">langueurs</w> <w n="9.4">de</w> <w n="9.5">leurs</w> <w n="9.6">prunelles</w>,</l>
					<l n="10" num="3.2"><w n="10.1">Une</w> <w n="10.2">grâce</w> <w n="10.3">triste</w> <w n="10.4">sourit</w> ;</l>
					<l n="11" num="3.3"><w n="11.1">Les</w> <w n="11.2">pleurs</w> <w n="11.3">mouillent</w> <w n="11.4">les</w> <w n="11.5">étincelles</w></l>
					<l n="12" num="3.4"><w n="12.1">Et</w> <w n="12.2">la</w> <w n="12.3">lumière</w> <w n="12.4">s</w>’<w n="12.5">attendrit</w> ;</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Et</w> <w n="13.2">leurs</w> <w n="13.3">cils</w> <w n="13.4">comme</w> <w n="13.5">des</w> <w n="13.6">mouettes</w></l>
					<l n="14" num="4.2"><w n="14.1">Qui</w> <w n="14.2">rasent</w> <w n="14.3">le</w> <w n="14.4">flot</w> <w n="14.5">aplani</w>,</l>
					<l n="15" num="4.3"><w n="15.1">Palpitent</w>, <w n="15.2">ailes</w> <w n="15.3">inquiètes</w>,</l>
					<l n="16" num="4.4"><w n="16.1">Sur</w> <w n="16.2">leur</w> <w n="16.3">azur</w> <w n="16.4">indéfini</w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">Comme</w> <w n="17.2">dans</w> <w n="17.3">l</w>’<w n="17.4">eau</w> <w n="17.5">bleue</w> <w n="17.6">et</w> <w n="17.7">profonde</w>,</l>
					<l n="18" num="5.2"><w n="18.1">Où</w> <w n="18.2">dort</w> <w n="18.3">plus</w> <w n="18.4">d</w>’<w n="18.5">un</w> <w n="18.6">trésor</w> <w n="18.7">coulé</w>,</l>
					<l n="19" num="5.3"><w n="19.1">On</w> <w n="19.2">y</w> <w n="19.3">découvre</w> <w n="19.4">à</w> <w n="19.5">travers</w> <w n="19.6">l</w>’<w n="19.7">onde</w></l>
					<l n="20" num="5.4"><w n="20.1">La</w> <w n="20.2">coupe</w> <w n="20.3">du</w> <w n="20.4">roi</w> <w n="20.5">de</w> <w n="20.6">Thulé</w>.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">Sous</w> <w n="21.2">leur</w> <w n="21.3">transparence</w> <w n="21.4">verdâtre</w>,</l>
					<l n="22" num="6.2"><w n="22.1">Brille</w>, <w n="22.2">parmi</w> <w n="22.3">le</w> <w n="22.4">goémon</w>,</l>
					<l n="23" num="6.3"><w n="23.1">L</w>’<w n="23.2">autre</w> <w n="23.3">perle</w> <w n="23.4">de</w> <w n="23.5">Cléopâtre</w></l>
					<l n="24" num="6.4"><w n="24.1">Près</w> <w n="24.2">de</w> <w n="24.3">l</w>’<w n="24.4">anneau</w> <w n="24.5">de</w> <w n="24.6">Salomon</w>.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">La</w> <w n="25.2">couronne</w> <w n="25.3">au</w> <w n="25.4">gouffre</w> <w n="25.5">lancée</w></l>
					<l n="26" num="7.2"><w n="26.1">Dans</w> <w n="26.2">la</w> <w n="26.3">ballade</w> <w n="26.4">de</w> <w n="26.5">Schiller</w>,</l>
					<l n="27" num="7.3"><w n="27.1">Sans</w> <w n="27.2">qu</w>’<w n="27.3">un</w> <w n="27.4">plongeur</w> <w n="27.5">l</w>’<w n="27.6">ait</w> <w n="27.7">ramassée</w>,</l>
					<l n="28" num="7.4"><w n="28.1">Y</w> <w n="28.2">jette</w> <w n="28.3">encor</w> <w n="28.4">son</w> <w n="28.5">reflet</w> <w n="28.6">clair</w>.</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1">Un</w> <w n="29.2">pouvoir</w> <w n="29.3">magique</w> <w n="29.4">m</w>’<w n="29.5">entraîne</w></l>
					<l n="30" num="8.2"><w n="30.1">Vers</w> <w n="30.2">l</w>’<w n="30.3">abîme</w> <w n="30.4">de</w> <w n="30.5">ce</w> <w n="30.6">regard</w>,</l>
					<l n="31" num="8.3"><w n="31.1">Comme</w> <w n="31.2">au</w> <w n="31.3">sein</w> <w n="31.4">des</w> <w n="31.5">eaux</w> <w n="31.6">la</w> <w n="31.7">sirène</w></l>
					<l n="32" num="8.4"><w n="32.1">Attirait</w> <w n="32.2">Harald</w> <w n="32.3">Harfagar</w>.</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1"><w n="33.1">Mon</w> <w n="33.2">âme</w>, <w n="33.3">avec</w> <w n="33.4">la</w> <w n="33.5">violence</w></l>
					<l n="34" num="9.2"><w n="34.1">D</w>’<w n="34.2">un</w> <w n="34.3">irrésistible</w> <w n="34.4">désir</w>,</l>
					<l n="35" num="9.3"><w n="35.1">Au</w> <w n="35.2">milieu</w> <w n="35.3">du</w> <w n="35.4">gouffre</w> <w n="35.5">s</w>’<w n="35.6">élance</w></l>
					<l n="36" num="9.4"><w n="36.1">Vers</w> <w n="36.2">l</w>’<w n="36.3">ombre</w> <w n="36.4">impossible</w> <w n="36.5">à</w> <w n="36.6">saisir</w>.</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1"><w n="37.1">Montrant</w> <w n="37.2">son</w> <w n="37.3">sein</w>, <w n="37.4">cachant</w> <w n="37.5">sa</w> <w n="37.6">queue</w>,</l>
					<l n="38" num="10.2"><w n="38.1">La</w> <w n="38.2">sirène</w> <w n="38.3">amoureusement</w></l>
					<l n="39" num="10.3"><w n="39.1">Fait</w> <w n="39.2">ondoyer</w> <w n="39.3">sa</w> <w n="39.4">blancheur</w> <w n="39.5">bleue</w></l>
					<l n="40" num="10.4"><w n="40.1">Sous</w> <w n="40.2">l</w>’<w n="40.3">émail</w> <w n="40.4">vert</w> <w n="40.5">du</w> <w n="40.6">flot</w> <w n="40.7">dormant</w>.</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1"><w n="41.1">L</w>’<w n="41.2">eau</w> <w n="41.3">s</w>’<w n="41.4">enfle</w> <w n="41.5">comme</w> <w n="41.6">une</w> <w n="41.7">poitrine</w></l>
					<l n="42" num="11.2"><w n="42.1">Aux</w> <w n="42.2">soupirs</w> <w n="42.3">de</w> <w n="42.4">la</w> <w n="42.5">passion</w> ;</l>
					<l n="43" num="11.3"><w n="43.1">Le</w> <w n="43.2">vent</w>, <w n="43.3">dans</w> <w n="43.4">sa</w> <w n="43.5">conque</w> <w n="43.6">marine</w>,</l>
					<l n="44" num="11.4"><w n="44.1">Murmure</w> <w n="44.2">une</w> <w n="44.3">incantation</w>.</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1">«<w n="45.1">Oh</w> ! <w n="45.2">viens</w> <w n="45.3">dans</w> <w n="45.4">ma</w> <w n="45.5">couche</w> <w n="45.6">de</w> <w n="45.7">nacre</w>,</l>
					<l n="46" num="12.2"><w n="46.1">Mes</w> <w n="46.2">bras</w> <w n="46.3">d</w>’<w n="46.4">onde</w> <w n="46.5">t</w>’<w n="46.6">enlaceront</w> ;</l>
					<l n="47" num="12.3"><w n="47.1">Les</w> <w n="47.2">flots</w>, <w n="47.3">perdant</w> <w n="47.4">leur</w> <w n="47.5">saveur</w> <w n="47.6">âcre</w>,</l>
					<l n="48" num="12.4"><w n="48.1">Sur</w> <w n="48.2">ta</w> <w n="48.3">bouche</w>, <w n="48.4">en</w> <w n="48.5">miel</w> <w n="48.6">couleront</w>.</l>
				</lg>
				<lg n="13">
					<l n="49" num="13.1">«<w n="49.1">Laissant</w> <w n="49.2">bruire</w> <w n="49.3">sur</w> <w n="49.4">nos</w> <w n="49.5">têtes</w>,</l>
					<l n="50" num="13.2"><w n="50.1">La</w> <w n="50.2">mer</w> <w n="50.3">qui</w> <w n="50.4">ne</w> <w n="50.5">peut</w> <w n="50.6">s</w>’<w n="50.7">apaiser</w>,</l>
					<l n="51" num="13.3"><w n="51.1">Nous</w> <w n="51.2">boirons</w> <w n="51.3">l</w>’<w n="51.4">oubli</w> <w n="51.5">des</w> <w n="51.6">tempêtes</w></l>
					<l n="52" num="13.4"><w n="52.1">Dans</w> <w n="52.2">la</w> <w n="52.3">coupe</w> <w n="52.4">de</w> <w n="52.5">mon</w> <w n="52.6">baiser</w>.»</l>
				</lg>
				<lg n="14">
					<l n="53" num="14.1"><w n="53.1">Ainsi</w> <w n="53.2">parle</w> <w n="53.3">la</w> <w n="53.4">voix</w> <w n="53.5">humide</w></l>
					<l n="54" num="14.2"><w n="54.1">De</w> <w n="54.2">ce</w> <w n="54.3">regard</w> <w n="54.4">céruléen</w>,</l>
					<l n="55" num="14.3"><w n="55.1">Et</w> <w n="55.2">mon</w> <w n="55.3">cœur</w>, <w n="55.4">sous</w> <w n="55.5">l</w>’<w n="55.6">onde</w> <w n="55.7">perfide</w>,</l>
					<l n="56" num="14.4"><w n="56.1">Se</w> <w n="56.2">noie</w> <w n="56.3">et</w> <w n="56.4">consomme</w> <w n="56.5">l</w>’<w n="56.6">hymen</w>.</l>
				</lg>
			</div></body></text></TEI>