<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">La comédie de la mort</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>5120 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">GAU_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">La Comédie de la mort</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>inlibroveritas.net</publisher>
						<idno type="URL">http://www.inlibroveritas.net</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>La Comédie de la mort</title>
						<author>Théophile Gautier</author>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k70716q.r=th%C3%A9ophile+gautier.langFR</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>DESESSART, ÉDITEUR</publisher>
							<date when="1838">1838</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1838">1838</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2014-04-11" who="RR">Un vers faux dans GAU54 (Thébaïde), vers 85 a été corrigé avec l’édition Bartillat (préparée par Michel Brix)</change>
			<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="GAU103">
				<head type="main">LA BONNE JOURNÉE</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Ce</w> <w n="1.2">jour</w>, <w n="1.3">je</w> <w n="1.4">l</w>’<w n="1.5">ai</w> <w n="1.6">passé</w> <w n="1.7">ployé</w> <w n="1.8">sur</w> <w n="1.9">mon</w> <w n="1.10">pupitre</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Sans</w> <w n="2.2">jeter</w> <w n="2.3">une</w> <w n="2.4">fois</w> <w n="2.5">l</w>’<w n="2.6">œil</w> <w n="2.7">à</w> <w n="2.8">travers</w> <w n="2.9">la</w> <w n="2.10">vitre</w>.</l>
					<l n="3" num="1.3"><w n="3.1">Par</w> <w n="3.2">Apollo</w> ! <w n="3.3">cent</w> <w n="3.4">vers</w> ; <w n="3.5">je</w> <w n="3.6">devrais</w> <w n="3.7">être</w> <w n="3.8">las</w>,</l>
					<l n="4" num="1.4"><w n="4.1">On</w> <w n="4.2">le</w> <w n="4.3">serait</w> <w n="4.4">à</w> <w n="4.5">moins</w> ; <w n="4.6">mais</w> <w n="4.7">je</w> <w n="4.8">ne</w> <w n="4.9">le</w> <w n="4.10">suis</w> <w n="4.11">pas</w> ;</l>
					<l n="5" num="1.5"><w n="5.1">Je</w> <w n="5.2">ne</w> <w n="5.3">sais</w> <w n="5.4">quelle</w> <w n="5.5">joie</w> <w n="5.6">intime</w> <w n="5.7">et</w> <w n="5.8">souveraine</w></l>
					<l n="6" num="1.6"><w n="6.1">Me</w> <w n="6.2">fait</w> <w n="6.3">le</w> <w n="6.4">regard</w> <w n="6.5">vif</w> <w n="6.6">et</w> <w n="6.7">la</w> <w n="6.8">face</w> <w n="6.9">sereine</w>,</l>
					<l n="7" num="1.7"><w n="7.1">Comme</w> <w n="7.2">après</w> <w n="7.3">la</w> <w n="7.4">rosée</w> <w n="7.5">une</w> <w n="7.6">petite</w> <w n="7.7">fleur</w> ;</l>
					<l n="8" num="1.8"><w n="8.1">Mon</w> <w n="8.2">front</w> <w n="8.3">se</w> <w n="8.4">lève</w> <w n="8.5">en</w> <w n="8.6">haut</w> <w n="8.7">avec</w> <w n="8.8">moins</w> <w n="8.9">de</w> <w n="8.10">pâleur</w> ;</l>
					<l n="9" num="1.9"><w n="9.1">Un</w> <w n="9.2">sourire</w> <w n="9.3">d</w>’<w n="9.4">orgueil</w> <w n="9.5">sur</w> <w n="9.6">mes</w> <w n="9.7">lèvres</w> <w n="9.8">rayonne</w>,</l>
					<l n="10" num="1.10"><w n="10.1">Et</w> <w n="10.2">mon</w> <w n="10.3">souffle</w> <w n="10.4">pressé</w> <w n="10.5">plus</w> <w n="10.6">fortement</w> <w n="10.7">résonne</w>.</l>
					<l n="11" num="1.11"><w n="11.1">J</w>’<w n="11.2">ai</w> <w n="11.3">rempli</w> <w n="11.4">mon</w> <w n="11.5">devoir</w> <w n="11.6">comme</w> <w n="11.7">un</w> <w n="11.8">brave</w> <w n="11.9">ouvrier</w>.</l>
					<l n="12" num="1.12"><w n="12.1">Rien</w> <w n="12.2">ne</w> <w n="12.3">m</w>’<w n="12.4">a</w> <w n="12.5">pu</w> <w n="12.6">distraire</w> ; <w n="12.7">en</w> <w n="12.8">vain</w> <w n="12.9">mon</w> <w n="12.10">lévrier</w>,</l>
					<l n="13" num="1.13"><w n="13.1">Entre</w> <w n="13.2">mes</w> <w n="13.3">deux</w> <w n="13.4">genoux</w> <w n="13.5">posant</w> <w n="13.6">sa</w> <w n="13.7">longue</w> <w n="13.8">tête</w>,</l>
					<l n="14" num="1.14"><w n="14.1">Semblait</w> <w n="14.2">me</w> <w n="14.3">dire</w> : — <w n="14.4">En</w> <w n="14.5">chasse</w> ! <w n="14.6">en</w> <w n="14.7">vain</w> <w n="14.8">d</w>’<w n="14.9">un</w> <w n="14.10">air</w> <w n="14.11">de</w> <w n="14.12">fête</w></l>
					<l n="15" num="1.15"><w n="15.1">Le</w> <w n="15.2">ciel</w> <w n="15.3">tout</w> <w n="15.4">bleu</w> <w n="15.5">dardait</w>, <w n="15.6">par</w> <w n="15.7">le</w> <w n="15.8">coin</w> <w n="15.9">du</w> <w n="15.10">carreau</w>,</l>
					<l n="16" num="1.16"><w n="16.1">Un</w> <w n="16.2">filet</w> <w n="16.3">de</w> <w n="16.4">soleil</w> <w n="16.5">jusque</w> <w n="16.6">sur</w> <w n="16.7">mon</w> <w n="16.8">bureau</w> ;</l>
					<l n="17" num="1.17"><w n="17.1">Près</w> <w n="17.2">de</w> <w n="17.3">ma</w> <w n="17.4">pipe</w>, <w n="17.5">en</w> <w n="17.6">vain</w>, <w n="17.7">ma</w> <w n="17.8">joyeuse</w> <w n="17.9">bouteille</w></l>
					<l n="18" num="1.18"><w n="18.1">M</w>’<w n="18.2">étalait</w> <w n="18.3">son</w> <w n="18.4">gros</w> <w n="18.5">ventre</w> <w n="18.6">et</w> <w n="18.7">souriait</w> <w n="18.8">vermeille</w> ;</l>
					<l n="19" num="1.19"><w n="19.1">En</w> <w n="19.2">vain</w> <w n="19.3">ma</w> <w n="19.4">bien</w>-<w n="19.5">aimée</w>, <w n="19.6">avec</w> <w n="19.7">son</w> <w n="19.8">beau</w> <w n="19.9">sein</w> <w n="19.10">nu</w>,</l>
					<l n="20" num="1.20"><w n="20.1">Se</w> <w n="20.2">penchait</w> <w n="20.3">en</w> <w n="20.4">riant</w> <w n="20.5">de</w> <w n="20.6">son</w> <w n="20.7">rire</w> <w n="20.8">ingénu</w> ;</l>
					<l n="21" num="1.21"><w n="21.1">Sur</w> <w n="21.2">mon</w> <w n="21.3">fauteuil</w> <w n="21.4">gothique</w>, <w n="21.5">et</w> <w n="21.6">dans</w> <w n="21.7">ma</w> <w n="21.8">chevelure</w></l>
					<l n="22" num="1.22"><w n="22.1">Répandait</w> <w n="22.2">les</w> <w n="22.3">parfums</w> <w n="22.4">de</w> <w n="22.5">son</w> <w n="22.6">haleine</w> <w n="22.7">pure</w>.</l>
					<l n="23" num="1.23"><w n="23.1">Sourd</w> <w n="23.2">comme</w> <w n="23.3">saint</w> <w n="23.4">Antoine</w> <w n="23.5">à</w> <w n="23.6">la</w> <w n="23.7">tentation</w>,</l>
					<l n="24" num="1.24"><w n="24.1">J</w>’<w n="24.2">ai</w> <w n="24.3">poursuivi</w> <w n="24.4">mon</w> <w n="24.5">œuvre</w> <w n="24.6">avec</w> <w n="24.7">religion</w> ;</l>
					<l n="25" num="1.25"><w n="25.1">L</w>’<w n="25.2">œuvre</w> <w n="25.3">de</w> <w n="25.4">mon</w> <w n="25.5">amour</w> <w n="25.6">qui</w> <w n="25.7">mort</w> <w n="25.8">me</w> <w n="25.9">fera</w> <w n="25.10">vivre</w>,</l>
					<l n="26" num="1.26"><w n="26.1">Et</w> <w n="26.2">ma</w> <w n="26.3">journée</w> <w n="26.4">ajoute</w> <w n="26.5">un</w> <w n="26.6">feuillet</w> <w n="26.7">à</w> <w n="26.8">mon</w> <w n="26.9">livre</w>.</l>
				</lg>
			</div></body></text></TEI>