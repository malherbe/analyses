<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">La comédie de la mort</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>5120 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">GAU_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">La Comédie de la mort</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>inlibroveritas.net</publisher>
						<idno type="URL">http://www.inlibroveritas.net</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>La Comédie de la mort</title>
						<author>Théophile Gautier</author>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k70716q.r=th%C3%A9ophile+gautier.langFR</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>DESESSART, ÉDITEUR</publisher>
							<date when="1838">1838</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1838">1838</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2014-04-11" who="RR">Un vers faux dans GAU54 (Thébaïde), vers 85 a été corrigé avec l’édition Bartillat (préparée par Michel Brix)</change>
			<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="GAU56">
				<head type="main">PASTEL</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">J</w>’<w n="1.2">aime</w> <w n="1.3">à</w> <w n="1.4">vous</w> <w n="1.5">voir</w> <w n="1.6">en</w> <w n="1.7">vos</w> <w n="1.8">cadres</w> <w n="1.9">ovales</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Portraits</w> <w n="2.2">jaunis</w> <w n="2.3">des</w> <w n="2.4">belles</w> <w n="2.5">du</w> <w n="2.6">vieux</w> <w n="2.7">temps</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Tenant</w> <w n="3.2">en</w> <w n="3.3">main</w> <w n="3.4">des</w> <w n="3.5">roses</w> <w n="3.6">un</w> <w n="3.7">peu</w> <w n="3.8">pâles</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Comme</w> <w n="4.2">il</w> <w n="4.3">convient</w> <w n="4.4">à</w> <w n="4.5">des</w> <w n="4.6">fleurs</w> <w n="4.7">de</w> <w n="4.8">cent</w> <w n="4.9">ans</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Le</w> <w n="5.2">vent</w> <w n="5.3">d</w>’<w n="5.4">hiver</w> <w n="5.5">en</w> <w n="5.6">vous</w> <w n="5.7">touchant</w> <w n="5.8">la</w> <w n="5.9">joue</w></l>
					<l n="6" num="2.2"><w n="6.1">A</w> <w n="6.2">fait</w> <w n="6.3">mourir</w> <w n="6.4">vos</w> <w n="6.5">œillets</w> <w n="6.6">et</w> <w n="6.7">vos</w> <w n="6.8">lis</w>,</l>
					<l n="7" num="2.3"><w n="7.1">Vous</w> <w n="7.2">n</w>’<w n="7.3">avez</w> <w n="7.4">plus</w> <w n="7.5">que</w> <w n="7.6">des</w> <w n="7.7">mouches</w> <w n="7.8">de</w> <w n="7.9">boue</w></l>
					<l n="8" num="2.4"><w n="8.1">Et</w> <w n="8.2">sur</w> <w n="8.3">les</w> <w n="8.4">quais</w> <w n="8.5">vous</w> <w n="8.6">gisez</w> <w n="8.7">tout</w> <w n="8.8">salis</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Il</w> <w n="9.2">est</w> <w n="9.3">passé</w> <w n="9.4">le</w> <w n="9.5">doux</w> <w n="9.6">règne</w> <w n="9.7">des</w> <w n="9.8">belles</w> ;</l>
					<l n="10" num="3.2"><w n="10.1">La</w> <w n="10.2">Parabère</w> <w n="10.3">avec</w> <w n="10.4">la</w> <w n="10.5">Pompadour</w></l>
					<l n="11" num="3.3"><w n="11.1">Ne</w> <w n="11.2">trouveraient</w> <w n="11.3">que</w> <w n="11.4">des</w> <w n="11.5">sujets</w> <w n="11.6">rebelles</w>,</l>
					<l n="12" num="3.4"><w n="12.1">Et</w> <w n="12.2">sous</w> <w n="12.3">leur</w> <w n="12.4">tombe</w> <w n="12.5">est</w> <w n="12.6">enterré</w> <w n="12.7">l</w>’<w n="12.8">amour</w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Vous</w>, <w n="13.2">cependant</w>, <w n="13.3">vieux</w> <w n="13.4">portraits</w> <w n="13.5">qu</w>’<w n="13.6">on</w> <w n="13.7">oublie</w>,</l>
					<l n="14" num="4.2"><w n="14.1">Vous</w> <w n="14.2">respirez</w> <w n="14.3">vos</w> <w n="14.4">bouquets</w> <w n="14.5">sans</w> <w n="14.6">parfums</w>,</l>
					<l n="15" num="4.3"><w n="15.1">Et</w> <w n="15.2">souriez</w> <w n="15.3">avec</w> <w n="15.4">mélancolie</w></l>
					<l n="16" num="4.4"><w n="16.1">Au</w> <w n="16.2">souvenir</w> <w n="16.3">de</w> <w n="16.4">vos</w> <w n="16.5">galants</w> <w n="16.6">défunts</w>.</l>
				</lg>
			</div></body></text></TEI>