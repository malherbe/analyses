<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">La comédie de la mort</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>5120 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">GAU_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">La Comédie de la mort</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>inlibroveritas.net</publisher>
						<idno type="URL">http://www.inlibroveritas.net</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>La Comédie de la mort</title>
						<author>Théophile Gautier</author>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k70716q.r=th%C3%A9ophile+gautier.langFR</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>DESESSART, ÉDITEUR</publisher>
							<date when="1838">1838</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1838">1838</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2014-04-11" who="RR">Un vers faux dans GAU54 (Thébaïde), vers 85 a été corrigé avec l’édition Bartillat (préparée par Michel Brix)</change>
			<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="GAU86">
				<head type="main">CHANT DU GRILLON II</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Regardez</w> <w n="1.2">les</w> <w n="1.3">branches</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Comme</w> <w n="2.2">elles</w> <w n="2.3">sont</w> <w n="2.4">blanches</w> ;</l>
					<l n="3" num="1.3"><w n="3.1">Il</w> <w n="3.2">neige</w> <w n="3.3">des</w> <w n="3.4">fleurs</w> !</l>
					<l n="4" num="1.4"><w n="4.1">Riant</w> <w n="4.2">dans</w> <w n="4.3">la</w> <w n="4.4">pluie</w>,</l>
					<l n="5" num="1.5"><w n="5.1">Le</w> <w n="5.2">soleil</w> <w n="5.3">essuie</w></l>
					<l n="6" num="1.6"><w n="6.1">Les</w> <w n="6.2">saules</w> <w n="6.3">en</w> <w n="6.4">pleurs</w>,</l>
					<l n="7" num="1.7"><w n="7.1">Et</w> <w n="7.2">le</w> <w n="7.3">ciel</w> <w n="7.4">reflète</w></l>
					<l n="8" num="1.8"><w n="8.1">Dans</w> <w n="8.2">la</w> <w n="8.3">violette</w>,</l>
					<l n="9" num="1.9"><w n="9.1">Ses</w> <w n="9.2">pures</w> <w n="9.3">couleurs</w>.</l>
				</lg>
				<lg n="2">
					<l n="10" num="2.1"><w n="10.1">La</w> <w n="10.2">nature</w> <w n="10.3">en</w> <w n="10.4">joie</w></l>
					<l n="11" num="2.2"><w n="11.1">Se</w> <w n="11.2">pare</w> <w n="11.3">et</w> <w n="11.4">déploie</w></l>
					<l n="12" num="2.3"><w n="12.1">Son</w> <w n="12.2">manteau</w> <w n="12.3">vermeil</w>.</l>
					<l n="13" num="2.4"><w n="13.1">Le</w> <w n="13.2">paon</w> <w n="13.3">qui</w> <w n="13.4">se</w> <w n="13.5">joue</w>,</l>
					<l n="14" num="2.5"><w n="14.1">Fait</w> <w n="14.2">tourner</w> <w n="14.3">en</w> <w n="14.4">roue</w>,</l>
					<l n="15" num="2.6"><w n="15.1">Sa</w> <w n="15.2">queue</w> <w n="15.3">au</w> <w n="15.4">soleil</w>.</l>
					<l n="16" num="2.7"><w n="16.1">Tout</w> <w n="16.2">court</w>, <w n="16.3">tout</w> <w n="16.4">s</w>’<w n="16.5">agite</w>,</l>
					<l n="17" num="2.8"><w n="17.1">Pas</w> <w n="17.2">un</w> <w n="17.3">lièvre</w> <w n="17.4">au</w> <w n="17.5">gîte</w> ;</l>
					<l n="18" num="2.9"><w n="18.1">L</w>’<w n="18.2">ours</w> <w n="18.3">sort</w> <w n="18.4">du</w> <w n="18.5">sommeil</w>.</l>
				</lg>
				<lg n="3">
					<l n="19" num="3.1"><w n="19.1">La</w> <w n="19.2">mouche</w> <w n="19.3">ouvre</w> <w n="19.4">l</w>’<w n="19.5">aile</w>,</l>
					<l n="20" num="3.2"><w n="20.1">Et</w> <w n="20.2">la</w> <w n="20.3">demoiselle</w></l>
					<l n="21" num="3.3"><w n="21.1">Aux</w> <w n="21.2">prunelles</w> <w n="21.3">d</w>’<w n="21.4">or</w>,</l>
					<l n="22" num="3.4"><w n="22.1">Au</w> <w n="22.2">corset</w> <w n="22.3">de</w> <w n="22.4">guêpe</w>,</l>
					<l n="23" num="3.5"><w n="23.1">Dépliant</w> <w n="23.2">son</w> <w n="23.3">crêpe</w>,</l>
					<l n="24" num="3.6"><w n="24.1">A</w> <w n="24.2">repris</w> <w n="24.3">l</w>’<w n="24.4">essor</w>.</l>
					<l n="25" num="3.7"><w n="25.1">L</w>’<w n="25.2">eau</w> <w n="25.3">gaîment</w> <w n="25.4">babille</w>,</l>
					<l n="26" num="3.8"><w n="26.1">Le</w> <w n="26.2">goujon</w> <w n="26.3">frétille</w>,</l>
					<l n="27" num="3.9"><w n="27.1">Un</w> <w n="27.2">printemps</w> <w n="27.3">encor</w> !</l>
				</lg>
				<lg n="4">
					<l n="28" num="4.1"><w n="28.1">Tout</w> <w n="28.2">se</w> <w n="28.3">cherche</w> <w n="28.4">et</w> <w n="28.5">s</w>’<w n="28.6">aime</w> ;</l>
					<l n="29" num="4.2"><w n="29.1">Le</w> <w n="29.2">crapaud</w> <w n="29.3">lui</w>-<w n="29.4">même</w>,</l>
					<l n="30" num="4.3"><w n="30.1">Les</w> <w n="30.2">aspics</w> <w n="30.3">méchants</w> ;</l>
					<l n="31" num="4.4"><w n="31.1">Toute</w> <w n="31.2">créature</w>,</l>
					<l n="32" num="4.5"><w n="32.1">Selon</w> <w n="32.2">sa</w> <w n="32.3">nature</w> :</l>
					<l n="33" num="4.6"><w n="33.1">La</w> <w n="33.2">feuille</w> <w n="33.3">a</w> <w n="33.4">des</w> <w n="33.5">chants</w> ;</l>
					<l n="34" num="4.7"><w n="34.1">Les</w> <w n="34.2">herbes</w> <w n="34.3">résonnent</w>,</l>
					<l n="35" num="4.8"><w n="35.1">Les</w> <w n="35.2">buissons</w> <w n="35.3">bourdonnent</w> ;</l>
					<l n="36" num="4.9"><w n="36.1">C</w>’<w n="36.2">est</w> <w n="36.3">concert</w> <w n="36.4">aux</w> <w n="36.5">champs</w>.</l>
				</lg>
				<lg n="5">
					<l n="37" num="5.1"><w n="37.1">Moi</w> <w n="37.2">seul</w> <w n="37.3">je</w> <w n="37.4">suis</w> <w n="37.5">triste</w> ;</l>
					<l n="38" num="5.2"><w n="38.1">Qui</w> <w n="38.2">sait</w> <w n="38.3">si</w> <w n="38.4">j</w>’<w n="38.5">existe</w>,</l>
					<l n="39" num="5.3"><w n="39.1">Dans</w> <w n="39.2">mon</w> <w n="39.3">palais</w> <w n="39.4">noir</w> ?</l>
					<l n="40" num="5.4"><w n="40.1">Sous</w> <w n="40.2">la</w> <w n="40.3">cheminée</w>,</l>
					<l n="41" num="5.5"><w n="41.1">Ma</w> <w n="41.2">vie</w> <w n="41.3">enchaînée</w>,</l>
					<l n="42" num="5.6"><w n="42.1">Coule</w> <w n="42.2">sans</w> <w n="42.3">espoir</w>.</l>
					<l n="43" num="5.7"><w n="43.1">Je</w> <w n="43.2">ne</w> <w n="43.3">puis</w>, <w n="43.4">malade</w>,</l>
					<l n="44" num="5.8"><w n="44.1">Chanter</w> <w n="44.2">ma</w> <w n="44.3">ballade</w></l>
					<l n="45" num="5.9"><w n="45.1">Aux</w> <w n="45.2">hôtes</w> <w n="45.3">du</w> <w n="45.4">soir</w>.</l>
				</lg>
				<lg n="6">
					<l n="46" num="6.1"><w n="46.1">Si</w> <w n="46.2">la</w> <w n="46.3">brise</w> <w n="46.4">tiède</w></l>
					<l n="47" num="6.2"><w n="47.1">Au</w> <w n="47.2">vent</w> <w n="47.3">froid</w> <w n="47.4">succède</w> ;</l>
					<l n="48" num="6.3"><w n="48.1">Si</w> <w n="48.2">le</w> <w n="48.3">ciel</w> <w n="48.4">est</w> <w n="48.5">clair</w>,</l>
					<l n="49" num="6.4"><w n="49.1">Moi</w>, <w n="49.2">ma</w> <w n="49.3">cheminée</w></l>
					<l n="50" num="6.5"><w n="50.1">N</w>’<w n="50.2">est</w> <w n="50.3">illuminée</w></l>
					<l n="51" num="6.6"><w n="51.1">Que</w> <w n="51.2">d</w>’<w n="51.3">un</w> <w n="51.4">pâle</w> <w n="51.5">éclair</w> ;</l>
					<l n="52" num="6.7"><w n="52.1">Le</w> <w n="52.2">cercle</w> <w n="52.3">folâtre</w></l>
					<l n="53" num="6.8"><w n="53.1">Abandonne</w> <w n="53.2">l</w>’<w n="53.3">âtre</w> :</l>
					<l n="54" num="6.9"><w n="54.1">Pour</w> <w n="54.2">moi</w> <w n="54.3">c</w>’<w n="54.4">est</w> <w n="54.5">l</w>’<w n="54.6">hiver</w>.</l>
				</lg>
				<lg n="7">
					<l n="55" num="7.1"><w n="55.1">Sur</w> <w n="55.2">la</w> <w n="55.3">cendre</w> <w n="55.4">grise</w>,</l>
					<l n="56" num="7.2"><w n="56.1">La</w> <w n="56.2">pincette</w> <w n="56.3">brise</w></l>
					<l n="57" num="7.3"><w n="57.1">Un</w> <w n="57.2">charbon</w> <w n="57.3">sans</w> <w n="57.4">feu</w>.</l>
					<l n="58" num="7.4"><w n="58.1">Adieu</w> <w n="58.2">les</w> <w n="58.3">paillettes</w>,</l>
					<l n="59" num="7.5"><w n="59.1">Les</w> <w n="59.2">blondes</w> <w n="59.3">aigrettes</w> ;</l>
					<l n="60" num="7.6"><w n="60.1">Pour</w> <w n="60.2">six</w> <w n="60.3">mois</w> <w n="60.4">adieu</w></l>
					<l n="61" num="7.7"><w n="61.1">La</w> <w n="61.2">maîtresse</w> <w n="61.3">bûche</w>,</l>
					<l n="62" num="7.8"><w n="62.1">Où</w> <w n="62.2">sous</w> <w n="62.3">la</w> <w n="62.4">peluche</w>,</l>
					<l n="63" num="7.9"><w n="63.1">Sifflait</w> <w n="63.2">le</w> <w n="63.3">gaz</w> <w n="63.4">bleu</w>.</l>
				</lg>
				<lg n="8">
					<l n="64" num="8.1"><w n="64.1">Dans</w> <w n="64.2">ma</w> <w n="64.3">niche</w> <w n="64.4">creuse</w>,</l>
					<l n="65" num="8.2"><w n="65.1">Ma</w> <w n="65.2">natte</w> <w n="65.3">boiteuse</w></l>
					<l n="66" num="8.3"><w n="66.1">Me</w> <w n="66.2">tient</w> <w n="66.3">en</w> <w n="66.4">prison</w>.</l>
					<l n="67" num="8.4"><w n="67.1">Quand</w> <w n="67.2">l</w>’<w n="67.3">insecte</w> <w n="67.4">rôde</w>,</l>
					<l n="68" num="8.5"><w n="68.1">Comme</w> <w n="68.2">une</w> <w n="68.3">émeraude</w>,</l>
					<l n="69" num="8.6"><w n="69.1">Sous</w> <w n="69.2">le</w> <w n="69.3">vert</w> <w n="69.4">gazon</w>,</l>
					<l n="70" num="8.7"><w n="70.1">Moi</w> <w n="70.2">seul</w> <w n="70.3">je</w> <w n="70.4">m</w>’<w n="70.5">ennuie</w> ;</l>
					<l n="71" num="8.8"><w n="71.1">Un</w> <w n="71.2">mur</w>, <w n="71.3">noir</w> <w n="71.4">de</w> <w n="71.5">suie</w>,</l>
					<l n="72" num="8.9"><w n="72.1">Est</w> <w n="72.2">mon</w> <w n="72.3">horizon</w>.</l>
				</lg>
			</div></body></text></TEI>