<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">La comédie de la mort</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>5120 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">GAU_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">La Comédie de la mort</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>inlibroveritas.net</publisher>
						<idno type="URL">http://www.inlibroveritas.net</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>La Comédie de la mort</title>
						<author>Théophile Gautier</author>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k70716q.r=th%C3%A9ophile+gautier.langFR</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>DESESSART, ÉDITEUR</publisher>
							<date when="1838">1838</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1838">1838</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2014-04-11" who="RR">Un vers faux dans GAU54 (Thébaïde), vers 85 a été corrigé avec l’édition Bartillat (préparée par Michel Brix)</change>
			<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="GAU104">
				<head type="main">L’HIPPOPOTAME</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">L</w>’<w n="1.2">hippopotame</w> <w n="1.3">au</w> <w n="1.4">large</w> <w n="1.5">ventre</w></l>
					<l n="2" num="1.2"><w n="2.1">Habite</w> <w n="2.2">aux</w> <w n="2.3">Jungles</w> <w n="2.4">de</w> <w n="2.5">Java</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Où</w> <w n="3.2">grondent</w>, <w n="3.3">au</w> <w n="3.4">fond</w> <w n="3.5">de</w> <w n="3.6">chaque</w> <w n="3.7">antre</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Plus</w> <w n="4.2">de</w> <w n="4.3">monstres</w> <w n="4.4">qu</w>’<w n="4.5">on</w> <w n="4.6">n</w>’<w n="4.7">en</w> <w n="4.8">rêva</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Le</w> <w n="5.2">boa</w> <w n="5.3">se</w> <w n="5.4">déroule</w> <w n="5.5">et</w> <w n="5.6">siffle</w>,</l>
					<l n="6" num="2.2"><w n="6.1">Le</w> <w n="6.2">tigre</w> <w n="6.3">fait</w> <w n="6.4">son</w> <w n="6.5">hurlement</w> ;</l>
					<l n="7" num="2.3"><w n="7.1">Le</w> <w n="7.2">bufle</w> <w n="7.3">en</w> <w n="7.4">colère</w> <w n="7.5">renifle</w> ;</l>
					<l n="8" num="2.4"><w n="8.1">Il</w> <w n="8.2">dort</w> <w n="8.3">en</w> <w n="8.4">paix</w> <w n="8.5">tranquillement</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Il</w> <w n="9.2">ne</w> <w n="9.3">craint</w> <w n="9.4">ni</w> <w n="9.5">kriss</w> <w n="9.6">ni</w> <w n="9.7">zagaies</w> ;</l>
					<l n="10" num="3.2"><w n="10.1">Il</w> <w n="10.2">regarde</w> <w n="10.3">l</w>’<w n="10.4">homme</w> <w n="10.5">sans</w> <w n="10.6">fuir</w>,</l>
					<l n="11" num="3.3"><w n="11.1">Et</w> <w n="11.2">rit</w> <w n="11.3">des</w> <w n="11.4">balles</w> <w n="11.5">des</w> <w n="11.6">cypaies</w></l>
					<l n="12" num="3.4"><w n="12.1">Qui</w> <w n="12.2">rebondissent</w> <w n="12.3">sur</w> <w n="12.4">son</w> <w n="12.5">cuir</w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Je</w> <w n="13.2">suis</w> <w n="13.3">comme</w> <w n="13.4">l</w>’<w n="13.5">hippopotame</w> ;</l>
					<l n="14" num="4.2"><w n="14.1">De</w> <w n="14.2">ma</w> <w n="14.3">conviction</w> <w n="14.4">couvert</w>,</l>
					<l n="15" num="4.3"><w n="15.1">Forte</w> <w n="15.2">armure</w> <w n="15.3">que</w> <w n="15.4">rien</w> <w n="15.5">n</w>’<w n="15.6">entame</w>,</l>
					<l n="16" num="4.4"><w n="16.1">Je</w> <w n="16.2">vais</w> <w n="16.3">sans</w> <w n="16.4">peur</w> <w n="16.5">par</w> <w n="16.6">le</w> <w n="16.7">désert</w>.</l>
				</lg>
			</div></body></text></TEI>