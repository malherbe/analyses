<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">La comédie de la mort</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>5120 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">GAU_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">La Comédie de la mort</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>inlibroveritas.net</publisher>
						<idno type="URL">http://www.inlibroveritas.net</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>La Comédie de la mort</title>
						<author>Théophile Gautier</author>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k70716q.r=th%C3%A9ophile+gautier.langFR</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>DESESSART, ÉDITEUR</publisher>
							<date when="1838">1838</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1838">1838</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2014-04-11" who="RR">Un vers faux dans GAU54 (Thébaïde), vers 85 a été corrigé avec l’édition Bartillat (préparée par Michel Brix)</change>
			<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="GAU63">
				<head type="main">LA DIVA</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">On</w> <w n="1.2">donnait</w> <w n="1.3">à</w> <w n="1.4">Favart</w> <w n="1.5">Mosé</w>. <w n="1.6">Tamburini</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Le</w> <w n="2.2">basso</w> <w n="2.3">cantante</w>, <w n="2.4">le</w> <w n="2.5">ténor</w> <w n="2.6">Rubini</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Devaient</w> <w n="3.2">jouer</w> <w n="3.3">tous</w> <w n="3.4">deux</w> <w n="3.5">dans</w> <w n="3.6">la</w> <w n="3.7">pièce</w> ; <w n="3.8">et</w> <w n="3.9">la</w> <w n="3.10">salle</w></l>
					<l n="4" num="1.4"><w n="4.1">Quand</w> <w n="4.2">on</w> <w n="4.3">l</w>’<w n="4.4">eût</w> <w n="4.5">élargie</w> <w n="4.6">et</w> <w n="4.7">faite</w> <w n="4.8">colossale</w>,</l>
					<l n="5" num="1.5"><w n="5.1">Grande</w> <w n="5.2">comme</w> <w n="5.3">Saint</w>-<w n="5.4">Charle</w> <w n="5.5">ou</w> <w n="5.6">comme</w> <w n="5.7">la</w> <w n="5.8">Scala</w>,</l>
					<l n="6" num="1.6"><w n="6.1">N</w>’<w n="6.2">aurait</w> <w n="6.3">pu</w> <w n="6.4">contenir</w> <w n="6.5">son</w> <w n="6.6">public</w> <w n="6.7">ce</w> <w n="6.8">soir</w>-<w n="6.9">là</w>.</l>
					<l n="7" num="1.7"><w n="7.1">Moi</w>, <w n="7.2">plus</w> <w n="7.3">heureux</w> <w n="7.4">que</w> <w n="7.5">tous</w>, <w n="7.6">j</w>’<w n="7.7">avais</w> <w n="7.8">tout</w> <w n="7.9">à</w> <w n="7.10">connaître</w>,</l>
					<l n="8" num="1.8"><w n="8.1">Et</w> <w n="8.2">la</w> <w n="8.3">voix</w> <w n="8.4">des</w> <w n="8.5">chanteurs</w> <w n="8.6">et</w> <w n="8.7">l</w>’<w n="8.8">ouvrage</w> <w n="8.9">du</w> <w n="8.10">maître</w>.</l>
					<l n="9" num="1.9"><w n="9.1">Aimant</w> <w n="9.2">peu</w> <w n="9.3">l</w>’<w n="9.4">opéra</w>, <w n="9.5">c</w>’<w n="9.6">est</w> <w n="9.7">hasard</w> <w n="9.8">si</w> <w n="9.9">j</w>’<w n="9.10">y</w> <w n="9.11">vais</w>,</l>
					<l n="10" num="1.10"><w n="10.1">Et</w> <w n="10.2">je</w> <w n="10.3">n</w>’<w n="10.4">avais</w> <w n="10.5">pas</w> <w n="10.6">vu</w> <w n="10.7">le</w> <w n="10.8">Moïse</w> <w n="10.9">français</w> ;</l>
					<l n="11" num="1.11"><w n="11.1">Car</w> <w n="11.2">notre</w> <w n="11.3">idiome</w>, <w n="11.4">à</w> <w n="11.5">nous</w>, <w n="11.6">rauque</w> <w n="11.7">et</w> <w n="11.8">sans</w> <w n="11.9">prosodie</w>,</l>
					<l n="12" num="1.12"><w n="12.1">Fausse</w> <w n="12.2">toute</w> <w n="12.3">musique</w> ; <w n="12.4">et</w> <w n="12.5">la</w> <w n="12.6">note</w> <w n="12.7">hardie</w>,</l>
					<l n="13" num="1.13"><w n="13.1">Contre</w> <w n="13.2">quelque</w> <w n="13.3">mot</w> <w n="13.4">dur</w> <w n="13.5">se</w> <w n="13.6">heurtant</w> <w n="13.7">dans</w> <w n="13.8">son</w> <w n="13.9">vol</w>,</l>
					<l n="14" num="1.14"><w n="14.1">Brise</w> <w n="14.2">ses</w> <w n="14.3">ailes</w> <w n="14.4">d</w>’<w n="14.5">or</w> <w n="14.6">et</w> <w n="14.7">tombe</w> <w n="14.8">sur</w> <w n="14.9">le</w> <w n="14.10">sol</w>.</l>
					<l n="15" num="1.15"><w n="15.1">J</w>’<w n="15.2">étais</w> <w n="15.3">là</w>, <w n="15.4">les</w> <w n="15.5">deux</w> <w n="15.6">bras</w> <w n="15.7">en</w> <w n="15.8">croix</w> <w n="15.9">sur</w> <w n="15.10">la</w> <w n="15.11">poitrine</w>,</l>
					<l n="16" num="1.16"><w n="16.1">Pour</w> <w n="16.2">contenir</w> <w n="16.3">mon</w> <w n="16.4">cœur</w> <w n="16.5">plein</w> <w n="16.6">d</w>’<w n="16.7">extase</w> <w n="16.8">divine</w> ;</l>
					<l n="17" num="1.17"><w n="17.1">Mes</w> <w n="17.2">artères</w> <w n="17.3">chantant</w> <w n="17.4">avec</w> <w n="17.5">un</w> <w n="17.6">sourd</w> <w n="17.7">frisson</w>,</l>
					<l n="18" num="1.18"><w n="18.1">Mon</w> <w n="18.2">oreille</w> <w n="18.3">tendue</w> <w n="18.4">et</w> <w n="18.5">buvant</w> <w n="18.6">chaque</w> <w n="18.7">son</w>,</l>
					<l n="19" num="1.19"><w n="19.1">Attentif</w>, <w n="19.2">comme</w> <w n="19.3">au</w> <w n="19.4">bruit</w> <w n="19.5">de</w> <w n="19.6">la</w> <w n="19.7">grêle</w> <w n="19.8">fanfare</w>,</l>
					<l n="20" num="1.20"><w n="20.1">Un</w> <w n="20.2">cheval</w> <w n="20.3">ombrageux</w> <w n="20.4">qui</w> <w n="20.5">palpite</w> <w n="20.6">et</w> <w n="20.7">s</w>’<w n="20.8">effare</w> ;</l>
					<l n="21" num="1.21"><w n="21.1">Toutes</w> <w n="21.2">les</w> <w n="21.3">voix</w> <w n="21.4">criaient</w>, <w n="21.5">toutes</w> <w n="21.6">les</w> <w n="21.7">mains</w> <w n="21.8">frappaient</w>,</l>
					<l n="22" num="1.22"><w n="22.1">A</w> <w n="22.2">force</w> <w n="22.3">d</w>’<w n="22.4">applaudir</w> <w n="22.5">les</w> <w n="22.6">gants</w> <w n="22.7">blancs</w> <w n="22.8">se</w> <w n="22.9">rompaient</w> ;</l>
					<l n="23" num="1.23"><w n="23.1">Et</w> <w n="23.2">la</w> <w n="23.3">toile</w> <w n="23.4">tomba</w>. <w n="23.5">C</w>’<w n="23.6">était</w> <w n="23.7">le</w> <w n="23.8">premier</w> <w n="23.9">acte</w>.</l>
					<l n="24" num="1.24"><w n="24.1">Alors</w> <w n="24.2">je</w> <w n="24.3">regardai</w> ; <w n="24.4">plus</w> <w n="24.5">nette</w> <w n="24.6">et</w> <w n="24.7">plus</w> <w n="24.8">exacte</w>,</l>
					<l n="25" num="1.25"><w n="25.1">A</w> <w n="25.2">travers</w> <w n="25.3">le</w> <w n="25.4">lorgnon</w> <w n="25.5">dans</w> <w n="25.6">mes</w> <w n="25.7">yeux</w> <w n="25.8">moins</w> <w n="25.9">distraits</w>,</l>
					<l n="26" num="1.26"><w n="26.1">Chaque</w> <w n="26.2">tête</w> <w n="26.3">à</w> <w n="26.4">son</w> <w n="26.5">tour</w> <w n="26.6">passait</w> <w n="26.7">avec</w> <w n="26.8">ses</w> <w n="26.9">traits</w>.</l>
					<l n="27" num="1.27"><w n="27.1">Certes</w>, <w n="27.2">sous</w> <w n="27.3">l</w>’<w n="27.4">éventail</w> <w n="27.5">et</w> <w n="27.6">la</w> <w n="27.7">grille</w> <w n="27.8">dorée</w>,</l>
					<l n="28" num="1.28"><w n="28.1">Roulant</w>, <w n="28.2">dans</w> <w n="28.3">leurs</w> <w n="28.4">doigts</w> <w n="28.5">blancs</w> <w n="28.6">la</w> <w n="28.7">cassolette</w> <w n="28.8">ambrée</w>,</l>
					<l n="29" num="1.29"><w n="29.1">Au</w> <w n="29.2">reflet</w> <w n="29.3">des</w> <w n="29.4">joyaux</w>, <w n="29.5">au</w> <w n="29.6">feu</w> <w n="29.7">des</w> <w n="29.8">diamants</w>,</l>
					<l n="30" num="1.30"><w n="30.1">Avec</w> <w n="30.2">leurs</w> <w n="30.3">colliers</w> <w n="30.4">d</w>’<w n="30.5">or</w> <w n="30.6">et</w> <w n="30.7">tous</w> <w n="30.8">leurs</w> <w n="30.9">ornements</w>,</l>
					<l n="31" num="1.31"><w n="31.1">J</w>’<w n="31.2">en</w> <w n="31.3">vis</w> <w n="31.4">plus</w> <w n="31.5">d</w>’<w n="31.6">une</w> <w n="31.7">belle</w> <w n="31.8">et</w> <w n="31.9">méritant</w> <w n="31.10">éloge</w>,</l>
					<l n="32" num="1.32"><w n="32.1">Du</w> <w n="32.2">moins</w> <w n="32.3">je</w> <w n="32.4">le</w> <w n="32.5">croyais</w>, <w n="32.6">quand</w> <w n="32.7">au</w> <w n="32.8">fond</w> <w n="32.9">d</w>’<w n="32.10">une</w> <w n="32.11">loge</w></l>
					<l n="33" num="1.33"><w n="33.1">J</w>’<w n="33.2">aperçus</w> <w n="33.3">une</w> <w n="33.4">femme</w>. <w n="33.5">Il</w> <w n="33.6">me</w> <w n="33.7">sembla</w> <w n="33.8">d</w>’<w n="33.9">abord</w>,</l>
					<l n="34" num="1.34"><w n="34.1">La</w> <w n="34.2">loge</w> <w n="34.3">lui</w> <w n="34.4">formant</w> <w n="34.5">un</w> <w n="34.6">cadre</w> <w n="34.7">de</w> <w n="34.8">son</w> <w n="34.9">bord</w>,</l>
					<l n="35" num="1.35"><w n="35.1">Que</w> <w n="35.2">c</w>’<w n="35.3">était</w> <w n="35.4">un</w> <w n="35.5">tableau</w> <w n="35.6">de</w> <w n="35.7">Titien</w> <w n="35.8">ou</w> <w n="35.9">Giorgione</w>,</l>
					<l n="36" num="1.36"><w n="36.1">Moins</w> <w n="36.2">la</w> <w n="36.3">fumée</w> <w n="36.4">antique</w> <w n="36.5">et</w> <w n="36.6">moins</w> <w n="36.7">le</w> <w n="36.8">vernis</w> <w n="36.9">jaune</w>,</l>
					<l n="37" num="1.37"><w n="37.1">Car</w> <w n="37.2">elle</w> <w n="37.3">se</w> <w n="37.4">tenait</w> <w n="37.5">dans</w> <w n="37.6">l</w>’<w n="37.7">immobilité</w>,</l>
					<l n="38" num="1.38"><w n="38.1">Regardant</w> <w n="38.2">devant</w> <w n="38.3">elle</w> <w n="38.4">avec</w> <w n="38.5">simplicité</w>,</l>
					<l n="39" num="1.39"><w n="39.1">La</w> <w n="39.2">bouche</w> <w n="39.3">épanouie</w> <w n="39.4">en</w> <w n="39.5">un</w> <w n="39.6">demi</w>-<w n="39.7">sourire</w>,</l>
					<l n="40" num="1.40"><w n="40.1">Et</w> <w n="40.2">comme</w> <w n="40.3">un</w> <w n="40.4">livre</w> <w n="40.5">ouvert</w> <w n="40.6">son</w> <w n="40.7">front</w> <w n="40.8">se</w> <w n="40.9">laissant</w> <w n="40.10">lire</w> ;</l>
					<l n="41" num="1.41"><w n="41.1">Sa</w> <w n="41.2">coiffure</w> <w n="41.3">était</w> <w n="41.4">basse</w>, <w n="41.5">et</w> <w n="41.6">ses</w> <w n="41.7">cheveux</w> <w n="41.8">moirés</w></l>
					<l n="42" num="1.42"><w n="42.1">Descendaient</w> <w n="42.2">vers</w> <w n="42.3">sa</w> <w n="42.4">tempe</w> <w n="42.5">en</w> <w n="42.6">deux</w> <w n="42.7">flots</w> <w n="42.8">séparés</w>.</l>
					<l n="43" num="1.43"><w n="43.1">Ni</w> <w n="43.2">plumes</w>, <w n="43.3">ni</w> <w n="43.4">rubans</w>, <w n="43.5">ni</w> <w n="43.6">gaze</w>, <w n="43.7">ni</w> <w n="43.8">dentelle</w> ;</l>
					<l n="44" num="1.44"><w n="44.1">Pour</w> <w n="44.2">parure</w> <w n="44.3">et</w> <w n="44.4">bijoux</w>, <w n="44.5">sa</w> <w n="44.6">grâce</w> <w n="44.7">naturelle</w> ;</l>
					<l n="45" num="1.45"><w n="45.1">Pas</w> <w n="45.2">d</w>’<w n="45.3">oeillade</w> <w n="45.4">hautaine</w> <w n="45.5">ou</w> <w n="45.6">de</w> <w n="45.7">grand</w> <w n="45.8">air</w> <w n="45.9">vainqueur</w>,</l>
					<l n="46" num="1.46"><w n="46.1">Rien</w> <w n="46.2">que</w> <w n="46.3">le</w> <w n="46.4">repos</w> <w n="46.5">d</w>’<w n="46.6">âme</w> <w n="46.7">et</w> <w n="46.8">la</w> <w n="46.9">bonté</w> <w n="46.10">de</w> <w n="46.11">cœur</w>.</l>
					<l n="47" num="1.47"><w n="47.1">Au</w> <w n="47.2">bout</w> <w n="47.3">de</w> <w n="47.4">quelque</w> <w n="47.5">temps</w>, <w n="47.6">la</w> <w n="47.7">belle</w> <w n="47.8">créature</w>,</l>
					<l n="48" num="1.48"><w n="48.1">Se</w> <w n="48.2">lassant</w> <w n="48.3">d</w>’<w n="48.4">être</w> <w n="48.5">ainsi</w>, <w n="48.6">prit</w> <w n="48.7">une</w> <w n="48.8">autre</w> <w n="48.9">posture</w> :</l>
					<l n="49" num="1.49"><w n="49.1">Le</w> <w n="49.2">col</w> <w n="49.3">un</w> <w n="49.4">peu</w> <w n="49.5">penché</w>, <w n="49.6">le</w> <w n="49.7">menton</w> <w n="49.8">sur</w> <w n="49.9">la</w> <w n="49.10">main</w>,</l>
					<l n="50" num="1.50"><w n="50.1">De</w> <w n="50.2">façon</w> <w n="50.3">à</w> <w n="50.4">montrer</w> <w n="50.5">son</w> <w n="50.6">beau</w> <w n="50.7">profil</w> <w n="50.8">romain</w>,</l>
					<l n="51" num="1.51"><w n="51.1">Son</w> <w n="51.2">épaule</w> <w n="51.3">et</w> <w n="51.4">son</w> <w n="51.5">dos</w> <w n="51.6">aux</w> <w n="51.7">tons</w> <w n="51.8">chauds</w> <w n="51.9">et</w> <w n="51.10">vivaces</w></l>
					<l n="52" num="1.52"><w n="52.1">Où</w> <w n="52.2">l</w>’<w n="52.3">ombre</w> <w n="52.4">avec</w> <w n="52.5">le</w> <w n="52.6">clair</w> <w n="52.7">flottaient</w> <w n="52.8">par</w> <w n="52.9">larges</w> <w n="52.10">masses</w>.</l>
					<l n="53" num="1.53"><w n="53.1">Tout</w> <w n="53.2">perdait</w> <w n="53.3">son</w> <w n="53.4">éclat</w>, <w n="53.5">tout</w> <w n="53.6">tombait</w> <w n="53.7">à</w> <w n="53.8">côté</w></l>
					<l n="54" num="1.54"><w n="54.1">De</w> <w n="54.2">cette</w> <w n="54.3">virginale</w> <w n="54.4">et</w> <w n="54.5">sereine</w> <w n="54.6">beauté</w> ;</l>
					<l n="55" num="1.55"><w n="55.1">Mon</w> <w n="55.2">âme</w> <w n="55.3">tout</w> <w n="55.4">entière</w> <w n="55.5">à</w> <w n="55.6">cet</w> <w n="55.7">aspect</w> <w n="55.8">magique</w>,</l>
					<l n="56" num="1.56"><w n="56.1">Ne</w> <w n="56.2">se</w> <w n="56.3">souvenait</w> <w n="56.4">plus</w> <w n="56.5">d</w>’<w n="56.6">écouter</w> <w n="56.7">la</w> <w n="56.8">musique</w>,</l>
					<l n="57" num="1.57"><w n="57.1">Tant</w> <w n="57.2">cette</w> <w n="57.3">morbidezze</w> <w n="57.4">et</w> <w n="57.5">ce</w> <w n="57.6">laisser</w>-<w n="57.7">aller</w></l>
					<l n="58" num="1.58"><w n="58.1">Était</w> <w n="58.2">chose</w> <w n="58.3">charmante</w> <w n="58.4">et</w> <w n="58.5">douce</w> <w n="58.6">à</w> <w n="58.7">contempler</w>,</l>
					<l n="59" num="1.59"><w n="59.1">Tant</w> <w n="59.2">l</w>’<w n="59.3">œil</w> <w n="59.4">se</w> <w n="59.5">reposait</w> <w n="59.6">avec</w> <w n="59.7">mélancolie</w></l>
					<l n="60" num="1.60"><w n="60.1">Sur</w> <w n="60.2">ce</w> <w n="60.3">pâle</w> <w n="60.4">jasmin</w> <w n="60.5">transplanté</w> <w n="60.6">d</w>’<w n="60.7">Italie</w>.</l>
					<l n="61" num="1.61"><w n="61.1">Moins</w> <w n="61.2">épris</w> <w n="61.3">des</w> <w n="61.4">beaux</w> <w n="61.5">sons</w> <w n="61.6">qu</w>’<w n="61.7">épris</w> <w n="61.8">des</w> <w n="61.9">beaux</w> <w n="61.10">contours</w></l>
					<l n="62" num="1.62"><w n="62.1">Même</w> <w n="62.2">au</w> <hi rend="ital"><w n="62.3">parlar</w> <w n="62.4">Spiegar</w></hi>, <w n="62.5">je</w> <w n="62.6">regardai</w> <w n="62.7">toujours</w> ;</l>
					<l n="63" num="1.63"><w n="63.1">J</w>’<w n="63.2">admirais</w> <w n="63.3">à</w> <w n="63.4">part</w> <w n="63.5">moi</w> <w n="63.6">la</w> <w n="63.7">gracieuse</w> <w n="63.8">ligne</w></l>
					<l n="64" num="1.64"><w n="64.1">Du</w> <w n="64.2">col</w> <w n="64.3">se</w> <w n="64.4">repliant</w> <w n="64.5">comme</w> <w n="64.6">le</w> <w n="64.7">col</w> <w n="64.8">d</w>’<w n="64.9">un</w> <w n="64.10">cygne</w>,</l>
					<l n="65" num="1.65"><w n="65.1">L</w>’<w n="65.2">ovale</w> <w n="65.3">de</w> <w n="65.4">la</w> <w n="65.5">tête</w> <w n="65.6">et</w> <w n="65.7">la</w> <w n="65.8">forme</w> <w n="65.9">du</w> <w n="65.10">front</w>,</l>
					<l n="66" num="1.66"><w n="66.1">La</w> <w n="66.2">main</w> <w n="66.3">pure</w> <w n="66.4">et</w> <w n="66.5">correcte</w>, <w n="66.6">avec</w> <w n="66.7">le</w> <w n="66.8">beau</w> <w n="66.9">bras</w> <w n="66.10">rond</w> ;</l>
					<l n="67" num="1.67"><w n="67.1">Et</w> <w n="67.2">je</w> <w n="67.3">compris</w> <w n="67.4">pourquoi</w>, <w n="67.5">s</w>’<w n="67.6">exilant</w> <w n="67.7">de</w> <w n="67.8">la</w> <w n="67.9">France</w>,</l>
					<l n="68" num="1.68"><w n="68.1">Ingres</w> <w n="68.2">fit</w> <w n="68.3">si</w> <w n="68.4">longtemps</w> <w n="68.5">ses</w> <w n="68.6">amours</w> <w n="68.7">de</w> <w n="68.8">Florence</w>.</l>
					<l n="69" num="1.69"><w n="69.1">Jusqu</w>’<w n="69.2">à</w> <w n="69.3">ce</w> <w n="69.4">jour</w> <w n="69.5">j</w>’<w n="69.6">avais</w> <w n="69.7">en</w> <w n="69.8">vain</w> <w n="69.9">cherché</w> <w n="69.10">le</w> <w n="69.11">beau</w> ;</l>
					<l n="70" num="1.70"><w n="70.1">Ces</w> <w n="70.2">formes</w> <w n="70.3">sans</w> <w n="70.4">puissance</w> <w n="70.5">et</w> <w n="70.6">cette</w> <w n="70.7">fade</w> <w n="70.8">peau</w></l>
					<l n="71" num="1.71"><w n="71.1">Sous</w> <w n="71.2">laquelle</w> <w n="71.3">le</w> <w n="71.4">sang</w> <w n="71.5">ne</w> <w n="71.6">court</w>, <w n="71.7">que</w> <w n="71.8">par</w> <w n="71.9">la</w> <w n="71.10">fièvre</w></l>
					<l n="72" num="1.72"><w n="72.1">Et</w> <w n="72.2">que</w> <w n="72.3">jamais</w> <w n="72.4">soleil</w> <w n="72.5">ne</w> <w n="72.6">mordit</w> <w n="72.7">de</w> <w n="72.8">sa</w> <w n="72.9">lèvre</w> ;</l>
					<l n="73" num="1.73"><w n="73.1">Ce</w> <w n="73.2">dessin</w> <w n="73.3">lâche</w> <w n="73.4">et</w> <w n="73.5">mou</w>, <w n="73.6">ce</w> <w n="73.7">coloris</w> <w n="73.8">blafard</w></l>
					<l n="74" num="1.74"><w n="74.1">M</w>’<w n="74.2">avaient</w> <w n="74.3">fait</w> <w n="74.4">blasphémer</w> <w n="74.5">la</w> <w n="74.6">sainteté</w> <w n="74.7">de</w> <w n="74.8">l</w>’<w n="74.9">art</w>.</l>
					<l n="75" num="1.75"><w n="75.1">J</w>’<w n="75.2">avais</w> <w n="75.3">dit</w> : <w n="75.4">l</w>’<w n="75.5">art</w> <w n="75.6">est</w> <w n="75.7">faux</w>, <w n="75.8">les</w> <w n="75.9">rois</w> <w n="75.10">de</w> <w n="75.11">la</w> <w n="75.12">peinture</w></l>
					<l n="76" num="1.76"><w n="76.1">D</w>’<w n="76.2">un</w> <w n="76.3">habit</w> <w n="76.4">idéal</w> <w n="76.5">revêtent</w> <w n="76.6">la</w> <w n="76.7">nature</w>.</l>
					<l n="77" num="1.77"><w n="77.1">Ces</w> <w n="77.2">tons</w> <w n="77.3">harmonieux</w>, <w n="77.4">ces</w> <w n="77.5">beaux</w> <w n="77.6">linéaments</w>,</l>
					<l n="78" num="1.78"><w n="78.1">N</w>’<w n="78.2">ont</w> <w n="78.3">jamais</w> <w n="78.4">existé</w> <w n="78.5">qu</w>’<w n="78.6">aux</w> <w n="78.7">cerveaux</w> <w n="78.8">des</w> <w n="78.9">amants</w>,</l>
					<l n="79" num="1.79"><w n="79.1">J</w>’<w n="79.2">avais</w> <w n="79.3">dit</w>, <w n="79.4">n</w>’<w n="79.5">ayant</w> <w n="79.6">vu</w> <w n="79.7">que</w> <w n="79.8">la</w> <w n="79.9">laideur</w> <w n="79.10">française</w>,</l>
					<l n="80" num="1.80"><w n="80.1">Raphaël</w> <w n="80.2">a</w> <w n="80.3">menti</w> <w n="80.4">comme</w> <w n="80.5">Paul</w> <w n="80.6">Véronèse</w> !</l>
					<l n="81" num="1.81"><w n="81.1">Vous</w> <w n="81.2">n</w>’<w n="81.3">avez</w> <w n="81.4">pas</w> <w n="81.5">menti</w>, <w n="81.6">non</w>, <w n="81.7">maîtres</w> ; <w n="81.8">voilà</w> <w n="81.9">bien</w></l>
					<l n="82" num="1.82"><w n="82.1">Le</w> <w n="82.2">marbre</w> <w n="82.3">grec</w> <w n="82.4">doré</w> <w n="82.5">par</w> <w n="82.6">l</w>’<w n="82.7">ambre</w> <w n="82.8">italien</w></l>
					<l n="83" num="1.83"><w n="83.1">L</w>’<w n="83.2">œil</w> <w n="83.3">de</w> <w n="83.4">flamme</w>, <w n="83.5">le</w> <w n="83.6">teint</w> <w n="83.7">passionnément</w> <w n="83.8">pâle</w>,</l>
					<l n="84" num="1.84"><w n="84.1">Blond</w> <w n="84.2">comme</w> <w n="84.3">le</w> <w n="84.4">soleil</w>, <w n="84.5">sous</w> <w n="84.6">son</w> <w n="84.7">voile</w> <w n="84.8">de</w> <w n="84.9">hâle</w>,</l>
					<l n="85" num="1.85"><w n="85.1">Dans</w> <w n="85.2">la</w> <w n="85.3">mate</w> <w n="85.4">blancheur</w>, <w n="85.5">les</w> <w n="85.6">noirs</w> <w n="85.7">sourcils</w> <w n="85.8">marqués</w>,</l>
					<l n="86" num="1.86"><w n="86.1">Le</w> <w n="86.2">nez</w> <w n="86.3">sévère</w> <w n="86.4">et</w> <w n="86.5">droit</w>, <w n="86.6">la</w> <w n="86.7">bouche</w> <w n="86.8">aux</w> <w n="86.9">coins</w> <w n="86.10">arqués</w>,</l>
					<l n="87" num="1.87"><w n="87.1">Les</w> <w n="87.2">ailes</w> <w n="87.3">de</w> <w n="87.4">cheveux</w> <w n="87.5">s</w>’<w n="87.6">abattant</w> <w n="87.7">sur</w> <w n="87.8">les</w> <w n="87.9">tempes</w> ;</l>
					<l n="88" num="1.88"><w n="88.1">Et</w> <w n="88.2">tous</w> <w n="88.3">les</w> <w n="88.4">nobles</w> <w n="88.5">traits</w> <w n="88.6">de</w> <w n="88.7">vos</w> <w n="88.8">saintes</w> <w n="88.9">estampes</w>,</l>
					<l n="89" num="1.89"><w n="89.1">Non</w>, <w n="89.2">vous</w> <w n="89.3">n</w>’<w n="89.4">avez</w> <w n="89.5">pas</w> <w n="89.6">fait</w> <w n="89.7">un</w> <w n="89.8">rêve</w> <w n="89.9">de</w> <w n="89.10">beauté</w>,</l>
					<l n="90" num="1.90"><w n="90.1">C</w>’<w n="90.2">est</w> <w n="90.3">la</w> <w n="90.4">vie</w> <w n="90.5">elle</w>-<w n="90.6">même</w> <w n="90.7">et</w> <w n="90.8">la</w> <w n="90.9">réalité</w>.</l>
					<l n="91" num="1.91"><w n="91.1">Votre</w> <w n="91.2">Madone</w> <w n="91.3">est</w> <w n="91.4">là</w> ; <w n="91.5">dans</w> <w n="91.6">sa</w> <w n="91.7">loge</w> <w n="91.8">elle</w> <w n="91.9">pose</w>,</l>
					<l n="92" num="1.92"><w n="92.1">Près</w> <w n="92.2">d</w>’<w n="92.3">elle</w> <w n="92.4">vainement</w> <w n="92.5">l</w>’<w n="92.6">on</w> <w n="92.7">bourdonne</w> <w n="92.8">et</w> <w n="92.9">l</w>’<w n="92.10">on</w> <w n="92.11">cause</w> ;</l>
					<l n="93" num="1.93"><w n="93.1">Elle</w> <w n="93.2">reste</w> <w n="93.3">immobile</w> <w n="93.4">et</w> <w n="93.5">sous</w> <w n="93.6">le</w> <w n="93.7">même</w> <w n="93.8">jour</w>,</l>
					<l n="94" num="1.94"><w n="94.1">Gardant</w> <w n="94.2">comme</w> <w n="94.3">un</w> <w n="94.4">trésor</w> <w n="94.5">l</w>’<w n="94.6">harmonieux</w> <w n="94.7">contour</w>.</l>
					<l n="95" num="1.95"><w n="95.1">Artistes</w> <w n="95.2">souverains</w>, <w n="95.3">en</w> <w n="95.4">copistes</w> <w n="95.5">fidèles</w>,</l>
					<l n="96" num="1.96"><w n="96.1">Vous</w> <w n="96.2">avez</w> <w n="96.3">reproduit</w> <w n="96.4">vos</w> <w n="96.5">superbes</w> <w n="96.6">modèles</w> !</l>
					<l n="97" num="1.97"><w n="97.1">Pourquoi</w> <w n="97.2">découragé</w> <w n="97.3">par</w> <w n="97.4">vos</w> <w n="97.5">divins</w> <w n="97.6">tableaux</w>,</l>
					<l n="98" num="1.98"><w n="98.1">Ai</w>-<w n="98.2">je</w>, <w n="98.3">enfant</w> <w n="98.4">paresseux</w>, <w n="98.5">jeté</w> <w n="98.6">là</w> <w n="98.7">mes</w> <w n="98.8">pinceaux</w>,</l>
					<l n="99" num="1.99"><w n="99.1">Et</w> <w n="99.2">pris</w> <w n="99.3">pour</w> <w n="99.4">vous</w> <w n="99.5">fixer</w> <w n="99.6">le</w> <w n="99.7">crayon</w> <w n="99.8">du</w> <w n="99.9">poëte</w>,</l>
					<l n="100" num="1.100"><w n="100.1">Beaux</w> <w n="100.2">rêves</w>, <w n="100.3">obsesseurs</w> <w n="100.4">de</w> <w n="100.5">mon</w> <w n="100.6">âme</w> <w n="100.7">inquiète</w>,</l>
					<l n="101" num="1.101"><w n="101.1">Doux</w> <w n="101.2">fantômes</w> <w n="101.3">bercés</w> <w n="101.4">dans</w> <w n="101.5">les</w> <w n="101.6">bras</w> <w n="101.7">du</w> <w n="101.8">désir</w>,</l>
					<l n="102" num="1.102"><w n="102.1">Formes</w> <w n="102.2">que</w> <w n="102.3">la</w> <w n="102.4">parole</w> <w n="102.5">en</w> <w n="102.6">vain</w> <w n="102.7">cherche</w> <w n="102.8">à</w> <w n="102.9">saisir</w> !</l>
					<l n="103" num="1.103"><w n="103.1">Pourquoi</w> <w n="103.2">lassé</w> <w n="103.3">trop</w> <w n="103.4">tôt</w> <w n="103.5">dans</w> <w n="103.6">une</w> <w n="103.7">heure</w> <w n="103.8">de</w> <w n="103.9">doute</w>,</l>
					<l n="104" num="1.104"><w n="104.1">Peinture</w> <w n="104.2">bien</w>-<w n="104.3">aimée</w>, <w n="104.4">ai</w>-<w n="104.5">je</w> <w n="104.6">quitté</w> <w n="104.7">ta</w> <w n="104.8">route</w> !</l>
					<l n="105" num="1.105"><w n="105.1">Que</w> <w n="105.2">peuvent</w> <w n="105.3">tous</w> <w n="105.4">nos</w> <w n="105.5">vers</w> <w n="105.6">pour</w> <w n="105.7">rendre</w> <w n="105.8">la</w> <w n="105.9">beauté</w>,</l>
					<l n="106" num="1.106"><w n="106.1">Que</w> <w n="106.2">peuvent</w> <w n="106.3">de</w> <w n="106.4">vains</w> <w n="106.5">mots</w> <w n="106.6">sans</w> <w n="106.7">dessin</w> <w n="106.8">arrêté</w>,</l>
					<l n="107" num="1.107"><w n="107.1">Et</w> <w n="107.2">l</w>’<w n="107.3">épithète</w> <w n="107.4">creuse</w> <w n="107.5">et</w> <w n="107.6">la</w> <w n="107.7">rime</w> <w n="107.8">incolore</w>.</l>
					<l n="108" num="1.108"><w n="108.1">Ah</w> ! <w n="108.2">combien</w> <w n="108.3">je</w> <w n="108.4">regrette</w> <w n="108.5">et</w> <w n="108.6">comme</w> <w n="108.7">je</w> <w n="108.8">déplore</w></l>
					<l n="109" num="1.109"><w n="109.1">De</w> <w n="109.2">ne</w> <w n="109.3">plus</w> <w n="109.4">être</w> <w n="109.5">peintre</w>, <w n="109.6">en</w> <w n="109.7">te</w> <w n="109.8">voyant</w> <w n="109.9">ainsi</w></l>
					<l n="110" num="1.110"><w n="110.1">A</w> <w n="110.2">Mosé</w>, <w n="110.3">dans</w> <w n="110.4">ta</w> <w n="110.5">loge</w>, <w n="110.6">ô</w> <w n="110.7">Julia</w> <w n="110.8">Grisi</w> !</l>
				</lg>
			</div></body></text></TEI>