<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">La comédie de la mort</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>5120 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">GAU_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">La Comédie de la mort</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>inlibroveritas.net</publisher>
						<idno type="URL">http://www.inlibroveritas.net</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>La Comédie de la mort</title>
						<author>Théophile Gautier</author>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k70716q.r=th%C3%A9ophile+gautier.langFR</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>DESESSART, ÉDITEUR</publisher>
							<date when="1838">1838</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1838">1838</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2014-04-11" who="RR">Un vers faux dans GAU54 (Thébaïde), vers 85 a été corrigé avec l’édition Bartillat (préparée par Michel Brix)</change>
			<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="GAU79">
				<head type="main">CE MONDE-CI ET L’AUTRE</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Vos</w> <w n="1.2">premières</w> <w n="1.3">saisons</w> <w n="1.4">à</w> <w n="1.5">peine</w> <w n="1.6">sont</w> <w n="1.7">écloses</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Enfant</w>, <w n="2.2">et</w> <w n="2.3">vous</w> <w n="2.4">avez</w> <w n="2.5">déjà</w> <w n="2.6">vu</w> <w n="2.7">plus</w> <w n="2.8">de</w> <w n="2.9">choses</w></l>
					<l n="3" num="1.3"><w n="3.1">Qu</w>’<w n="3.2">un</w> <w n="3.3">vieillard</w> <w n="3.4">qui</w> <w n="3.5">trébuche</w> <w n="3.6">au</w> <w n="3.7">seuil</w> <w n="3.8">de</w> <w n="3.9">son</w> <w n="3.10">tombeau</w> ;</l>
					<l n="4" num="1.4"><w n="4.1">Tout</w> <w n="4.2">ce</w> <w n="4.3">que</w> <w n="4.4">la</w> <w n="4.5">nature</w> <w n="4.6">a</w> <w n="4.7">de</w> <w n="4.8">grand</w> <w n="4.9">et</w> <w n="4.10">de</w> <w n="4.11">beau</w>,</l>
					<l n="5" num="1.5"><w n="5.1">Tout</w> <w n="5.2">ce</w> <w n="5.3">que</w> <w n="5.4">Dieu</w> <w n="5.5">nous</w> <w n="5.6">fit</w> <w n="5.7">de</w> <w n="5.8">sublimes</w> <w n="5.9">spectacles</w>,</l>
					<l n="6" num="1.6"><w n="6.1">Les</w> <w n="6.2">deux</w> <w n="6.3">mondes</w> <w n="6.4">ensemble</w> <w n="6.5">avec</w> <w n="6.6">tout</w> <w n="6.7">leurs</w> <w n="6.8">miracles</w> :</l>
					<l n="7" num="1.7"><w n="7.1">Que</w> <w n="7.2">n</w>’<w n="7.3">avez</w>-<w n="7.4">vous</w> <w n="7.5">pas</w> <w n="7.6">vu</w> ? <w n="7.7">les</w> <w n="7.8">montagnes</w>, <w n="7.9">la</w> <w n="7.10">mer</w>,</l>
					<l n="8" num="1.8"><w n="8.1">La</w> <w n="8.2">neige</w> <w n="8.3">et</w> <w n="8.4">les</w> <w n="8.5">palmiers</w>, <w n="8.6">le</w> <w n="8.7">printemps</w> <w n="8.8">et</w> <w n="8.9">l</w>’<w n="8.10">hiver</w>,</l>
					<l n="9" num="1.9"><w n="9.1">L</w>’<w n="9.2">Europe</w> <w n="9.3">décrépite</w> <w n="9.4">et</w> <w n="9.5">la</w> <w n="9.6">jeune</w> <w n="9.7">Amérique</w> :</l>
					<l n="10" num="1.10"><w n="10.1">Car</w> <w n="10.2">votre</w> <w n="10.3">peau</w> <w n="10.4">cuivrée</w> <w n="10.5">aux</w> <w n="10.6">ardeurs</w> <w n="10.7">du</w> <w n="10.8">tropique</w>,</l>
					<l n="11" num="1.11"><w n="11.1">Sous</w> <w n="11.2">le</w> <w n="11.3">soleil</w> <w n="11.4">en</w> <w n="11.5">flamme</w> <w n="11.6">et</w> <w n="11.7">les</w> <w n="11.8">cieux</w> <w n="11.9">toujours</w> <w n="11.10">bleus</w>,</l>
					<l n="12" num="1.12"><w n="12.1">S</w>’<w n="12.2">est</w> <w n="12.3">faite</w> <w n="12.4">presque</w> <w n="12.5">blanche</w> <w n="12.6">à</w> <w n="12.7">nos</w> <w n="12.8">étés</w> <w n="12.9">frileux</w>.</l>
					<l n="13" num="1.13"><w n="13.1">Votre</w> <w n="13.2">enfance</w> <w n="13.3">joyeuse</w>, <w n="13.4">a</w> <w n="13.5">passé</w> <w n="13.6">comme</w> <w n="13.7">un</w> <w n="13.8">rêve</w></l>
					<l n="14" num="1.14"><w n="14.1">Dans</w> <w n="14.2">la</w> <w n="14.3">verte</w> <w n="14.4">savane</w> <w n="14.5">et</w> <w n="14.6">sur</w> <w n="14.7">la</w> <w n="14.8">blonde</w> <w n="14.9">grève</w> ;</l>
					<l n="15" num="1.15"><w n="15.1">Le</w> <w n="15.2">vent</w> <w n="15.3">vous</w> <w n="15.4">apportait</w> <w n="15.5">des</w> <w n="15.6">parfums</w> <w n="15.7">inconnus</w> ;</l>
					<l n="16" num="1.16"><w n="16.1">Le</w> <w n="16.2">sauvage</w> <w n="16.3">Océan</w> <w n="16.4">baisait</w> <w n="16.5">vos</w> <w n="16.6">beaux</w> <w n="16.7">pieds</w> <w n="16.8">nus</w>,</l>
					<l n="17" num="1.17"><w n="17.1">Et</w> <w n="17.2">comme</w> <w n="17.3">une</w> <w n="17.4">nourrice</w>, <w n="17.5">au</w> <w n="17.6">seuil</w> <w n="17.7">de</w> <w n="17.8">sa</w> <w n="17.9">demeure</w>,</l>
					<l n="18" num="1.18"><w n="18.1">Chante</w> <w n="18.2">et</w> <w n="18.3">jette</w> <w n="18.4">un</w> <w n="18.5">hochet</w> <w n="18.6">au</w> <w n="18.7">nouveau</w>-<w n="18.8">né</w> <w n="18.9">qui</w> <w n="18.10">pleure</w>,</l>
					<l n="19" num="1.19"><w n="19.1">Quand</w> <w n="19.2">il</w> <w n="19.3">vous</w> <w n="19.4">voyait</w> <w n="19.5">triste</w>, <w n="19.6">il</w> <w n="19.7">poussait</w> <w n="19.8">devant</w> <w n="19.9">vous</w></l>
					<l n="20" num="1.20"><w n="20.1">Ses</w> <w n="20.2">coquilles</w> <w n="20.3">de</w> <w n="20.4">moire</w> <w n="20.5">et</w> <w n="20.6">son</w> <w n="20.7">murmure</w> <w n="20.8">doux</w>.</l>
					<l n="21" num="1.21"><w n="21.1">Pour</w> <w n="21.2">vous</w> <w n="21.3">laisser</w> <w n="21.4">passer</w>, <w n="21.5">jam</w>-<w n="21.6">roses</w> <w n="21.7">et</w> <w n="21.8">lianes</w></l>
					<l n="22" num="1.22"><w n="22.1">Écartaient</w> <w n="22.2">dans</w> <w n="22.3">les</w> <w n="22.4">bois</w> <w n="22.5">leurs</w> <w n="22.6">rideaux</w> <w n="22.7">diaphanes</w> ;</l>
					<l n="23" num="1.23"><w n="23.1">Les</w> <w n="23.2">tamaniers</w> <w n="23.3">en</w> <w n="23.4">fleurs</w> <w n="23.5">vous</w> <w n="23.6">prêtaient</w> <w n="23.7">des</w> <w n="23.8">abris</w> ;</l>
					<l n="24" num="1.24"><w n="24.1">Vous</w> <w n="24.2">aviez</w> <w n="24.3">pour</w> <w n="24.4">jouer</w> <w n="24.5">des</w> <w n="24.6">nids</w> <w n="24.7">de</w> <w n="24.8">colibris</w> ;</l>
					<l n="25" num="1.25"><w n="25.1">Les</w> <w n="25.2">papillons</w> <w n="25.3">dorés</w> <w n="25.4">vous</w> <w n="25.5">éventaient</w> <w n="25.6">de</w> <w n="25.7">l</w>’<w n="25.8">aile</w>,</l>
					<l n="26" num="1.26"><w n="26.1">L</w>’<w n="26.2">oiseau</w>-<w n="26.3">mouche</w> <w n="26.4">valsait</w> <w n="26.5">avec</w> <w n="26.6">la</w> <w n="26.7">demoiselle</w> ;</l>
					<l n="27" num="1.27"><w n="27.1">Les</w> <w n="27.2">magnolias</w> <w n="27.3">penchaient</w> <w n="27.4">la</w> <w n="27.5">tête</w> <w n="27.6">en</w> <w n="27.7">souriant</w> ;</l>
					<l n="28" num="1.28"><w n="28.1">La</w> <w n="28.2">fontaine</w> <w n="28.3">au</w> <w n="28.4">flot</w> <w n="28.5">clair</w> <w n="28.6">s</w>’<w n="28.7">en</w> <w n="28.8">allait</w> <w n="28.9">babillant</w> ;</l>
					<l n="29" num="1.29"><w n="29.1">Les</w> <w n="29.2">bengalis</w> <w n="29.3">coquets</w>, <w n="29.4">se</w> <w n="29.5">mirant</w> <w n="29.6">à</w> <w n="29.7">son</w> <w n="29.8">onde</w>,</l>
					<l n="30" num="1.30"><w n="30.1">Vous</w> <w n="30.2">chantaient</w> <w n="30.3">leur</w> <w n="30.4">romance</w>, <w n="30.5">et</w>, <w n="30.6">seule</w> <w n="30.7">et</w> <w n="30.8">vagabonde</w>,</l>
					<l n="31" num="1.31"><w n="31.1">Vous</w> <w n="31.2">marchiez</w> <w n="31.3">sans</w> <w n="31.4">savoir</w> <w n="31.5">par</w> <w n="31.6">les</w> <w n="31.7">petits</w> <w n="31.8">chemins</w>,</l>
					<l n="32" num="1.32"><w n="32.1">Un</w> <w n="32.2">refrain</w> <w n="32.3">à</w> <w n="32.4">la</w> <w n="32.5">bouche</w> <w n="32.6">et</w> <w n="32.7">des</w> <w n="32.8">fleurs</w> <w n="32.9">dans</w> <w n="32.10">les</w> <w n="32.11">mains</w> !</l>
					<l n="33" num="1.33"><w n="33.1">Aux</w> <w n="33.2">heures</w> <w n="33.3">du</w> <w n="33.4">midi</w>, <w n="33.5">nonchalante</w> <w n="33.6">créole</w>,</l>
					<l n="34" num="1.34"><w n="34.1">Vous</w> <w n="34.2">aviez</w> <w n="34.3">le</w> <w n="34.4">hamac</w> <w n="34.5">et</w> <w n="34.6">la</w> <w n="34.7">sieste</w> <w n="34.8">espagnole</w>,</l>
					<l n="35" num="1.35"><w n="35.1">Et</w> <w n="35.2">la</w> <w n="35.3">bonne</w> <w n="35.4">négresse</w> <w n="35.5">aux</w> <w n="35.6">dents</w> <w n="35.7">blanches</w> <w n="35.8">qui</w> <w n="35.9">rit</w>,</l>
					<l n="36" num="1.36"><w n="36.1">Chassant</w> <w n="36.2">les</w> <w n="36.3">moucherons</w> <w n="36.4">d</w>’<w n="36.5">auprès</w> <w n="36.6">de</w> <w n="36.7">votre</w> <w n="36.8">lit</w>.</l>
					<l n="37" num="1.37"><w n="37.1">Vous</w> <w n="37.2">aviez</w> <w n="37.3">tous</w> <w n="37.4">les</w> <w n="37.5">biens</w>, <w n="37.6">heureuse</w> <w n="37.7">créature</w>,</l>
					<l n="38" num="1.38"><w n="38.1">La</w> <w n="38.2">belle</w> <w n="38.3">liberté</w> <w n="38.4">dans</w> <w n="38.5">la</w> <w n="38.6">belle</w> <w n="38.7">nature</w> :</l>
					<l n="39" num="1.39"><w n="39.1">Et</w> <w n="39.2">puis</w> <w n="39.3">un</w> <w n="39.4">grand</w> <w n="39.5">désir</w> <w n="39.6">d</w>’<w n="39.7">inconnu</w> <w n="39.8">vous</w> <w n="39.9">a</w> <w n="39.10">pris</w>,</l>
					<l n="40" num="1.40"><w n="40.1">Vous</w> <w n="40.2">avez</w> <w n="40.3">voulu</w> <w n="40.4">voir</w> <w n="40.5">et</w> <w n="40.6">la</w> <w n="40.7">France</w> <w n="40.8">et</w> <w n="40.9">Paris</w> ;</l>
					<l n="41" num="1.41"><w n="41.1">La</w> <w n="41.2">brise</w> <w n="41.3">a</w> <w n="41.4">du</w> <w n="41.5">vaisseau</w> <w n="41.6">fait</w> <w n="41.7">onder</w> <w n="41.8">la</w> <w n="41.9">bannière</w>,</l>
					<l n="42" num="1.42"><w n="42.1">Le</w> <w n="42.2">vieux</w> <w n="42.3">monstre</w> <w n="42.4">Océan</w>, <w n="42.5">secouant</w> <w n="42.6">sa</w> <w n="42.7">crinière</w>,</l>
					<l n="43" num="1.43"><w n="43.1">Et</w> <w n="43.2">courbant</w> <w n="43.3">devant</w> <w n="43.4">vous</w> <w n="43.5">sa</w> <w n="43.6">tête</w> <w n="43.7">de</w> <w n="43.8">lion</w></l>
					<l n="44" num="1.44"><w n="44.1">Sur</w> <w n="44.2">son</w> <w n="44.3">épaule</w> <w n="44.4">bleue</w> <w n="44.5">avec</w> <w n="44.6">soumission</w>,</l>
					<l n="45" num="1.45"><w n="45.1">Vous</w> <w n="45.2">a</w> <w n="45.3">jusques</w> <w n="45.4">aux</w> <w n="45.5">bords</w> <w n="45.6">de</w> <w n="45.7">la</w> <w n="45.8">France</w> <w n="45.9">vantée</w>,</l>
					<l n="46" num="1.46"><w n="46.1">Sans</w> <w n="46.2">rugir</w> <w n="46.3">une</w> <w n="46.4">fois</w>, <w n="46.5">fidèlement</w> <w n="46.6">portée</w>.</l>
					<l n="47" num="1.47"><w n="47.1">Après</w> <w n="47.2">celles</w> <w n="47.3">de</w> <w n="47.4">Dieu</w> <w n="47.5">les</w> <w n="47.6">merveilles</w> <w n="47.7">de</w> <w n="47.8">l</w>’<w n="47.9">art</w></l>
					<l n="48" num="1.48"><w n="48.1">Ont</w> <w n="48.2">étonné</w> <w n="48.3">votre</w> <w n="48.4">âme</w> <w n="48.5">avec</w> <w n="48.6">votre</w> <w n="48.7">regard</w>.</l>
					<l n="49" num="1.49"><w n="49.1">Vous</w> <w n="49.2">avez</w> <w n="49.3">vu</w> <w n="49.4">nos</w> <w n="49.5">tours</w>, <w n="49.6">nos</w> <w n="49.7">palais</w>, <w n="49.8">nos</w> <w n="49.9">églises</w>,</l>
					<l n="50" num="1.50"><w n="50.1">Nos</w> <w n="50.2">monuments</w> <w n="50.3">tout</w> <w n="50.4">noirs</w> <w n="50.5">et</w> <w n="50.6">nos</w> <w n="50.7">coupoles</w> <w n="50.8">grises</w>,</l>
					<l n="51" num="1.51"><w n="51.1">Nos</w> <w n="51.2">beaux</w> <w n="51.3">jardins</w> <w n="51.4">royaux</w>, <w n="51.5">où</w>, <w n="51.6">de</w> <w n="51.7">Grèce</w> <w n="51.8">venus</w>,</l>
					<l n="52" num="1.52"><w n="52.1">Étrangers</w> <w n="52.2">comme</w> <w n="52.3">vous</w>, <w n="52.4">frissonnent</w> <w n="52.5">les</w> <w n="52.6">dieux</w> <w n="52.7">nus</w>,</l>
					<l n="53" num="1.53"><w n="53.1">Notre</w> <w n="53.2">ciel</w> <w n="53.3">morne</w> <w n="53.4">et</w> <w n="53.5">froid</w>, <w n="53.6">notre</w> <w n="53.7">horizon</w> <w n="53.8">de</w> <w n="53.9">brume</w>,</l>
					<l n="54" num="1.54"><w n="54.1">Où</w> <w n="54.2">chaque</w> <w n="54.3">maison</w> <w n="54.4">dresse</w> <w n="54.5">une</w> <w n="54.6">gueule</w> <w n="54.7">qui</w> <w n="54.8">fume</w>.</l>
					<l n="55" num="1.55"><w n="55.1">Quel</w> <w n="55.2">spectacle</w> <w n="55.3">pour</w> <w n="55.4">vous</w>, <w n="55.5">ô</w> <w n="55.6">fille</w> <w n="55.7">du</w> <w n="55.8">soleil</w> !</l>
					<l n="56" num="1.56"><w n="56.1">Vous</w> <w n="56.2">toute</w> <w n="56.3">brune</w> <w n="56.4">encor</w> <w n="56.5">de</w> <w n="56.6">son</w> <w n="56.7">baiser</w> <w n="56.8">vermeil</w>.</l>
					<l n="57" num="1.57"><w n="57.1">La</w> <w n="57.2">pluie</w> <w n="57.3">a</w> <w n="57.4">ruisselé</w> <w n="57.5">sur</w> <w n="57.6">vos</w> <w n="57.7">vitres</w> <w n="57.8">jaunies</w>,</l>
					<l n="58" num="1.58"><w n="58.1">Et</w> <w n="58.2">triste</w> <w n="58.3">entre</w> <w n="58.4">vos</w> <w n="58.5">sœurs</w> <w n="58.6">au</w> <w n="58.7">foyer</w> <w n="58.8">réunies</w>,</l>
					<l n="59" num="1.59"><w n="59.1">En</w> <w n="59.2">entendant</w> <w n="59.3">pleurer</w> <w n="59.4">les</w> <w n="59.5">bûches</w> <w n="59.6">dans</w> <w n="59.7">le</w> <w n="59.8">feu</w>,</l>
					<l n="60" num="1.60"><w n="60.1">Vous</w> <w n="60.2">avez</w> <w n="60.3">regretté</w> <w n="60.4">l</w>’<w n="60.5">Amérique</w> <w n="60.6">au</w> <w n="60.7">ciel</w> <w n="60.8">bleu</w>,</l>
					<l n="61" num="1.61"><w n="61.1">Et</w> <w n="61.2">la</w> <w n="61.3">mer</w> <w n="61.4">amoureuse</w> <w n="61.5">avec</w> <w n="61.6">ses</w> <w n="61.7">tièdes</w> <w n="61.8">lames</w>,</l>
					<l n="62" num="1.62"><w n="62.1">Qui</w> <w n="62.2">se</w> <w n="62.3">brodent</w> <w n="62.4">d</w>’<w n="62.5">argent</w> <w n="62.6">et</w> <w n="62.7">chantent</w> <w n="62.8">sous</w> <w n="62.9">les</w> <w n="62.10">rames</w> ;</l>
					<l n="63" num="1.63"><w n="63.1">Les</w> <w n="63.2">beaux</w> <w n="63.3">lataniers</w> <w n="63.4">verts</w>, <w n="63.5">les</w> <w n="63.6">palmiers</w> <w n="63.7">chevelus</w>,</l>
					<l n="64" num="1.64"><w n="64.1">Les</w> <w n="64.2">mangliers</w> <w n="64.3">traînant</w> <w n="64.4">leurs</w> <w n="64.5">bras</w> <w n="64.6">irrésolus</w> ;</l>
					<l n="65" num="1.65"><w n="65.1">Toute</w> <w n="65.2">cette</w> <w n="65.3">nature</w> <w n="65.4">orientale</w> <w n="65.5">et</w> <w n="65.6">chaude</w>,</l>
					<l n="66" num="1.66"><w n="66.1">Où</w> <w n="66.2">chaque</w> <w n="66.3">herbe</w> <w n="66.4">flamboie</w> <w n="66.5">et</w> <w n="66.6">semble</w> <w n="66.7">une</w> <w n="66.8">émeraude</w>,</l>
					<l n="67" num="1.67"><w n="67.1">Et</w> <w n="67.2">vous</w> <w n="67.3">avez</w> <w n="67.4">souffert</w>, <w n="67.5">votre</w> <w n="67.6">cœur</w> <w n="67.7">a</w> <w n="67.8">saigné</w>,</l>
					<l n="68" num="1.68"><w n="68.1">Vos</w> <w n="68.2">yeux</w> <w n="68.3">se</w> <w n="68.4">sont</w> <w n="68.5">levés</w> <w n="68.6">vers</w> <w n="68.7">ce</w> <w n="68.8">ciel</w> <w n="68.9">gris</w>, <w n="68.10">baigné</w></l>
					<l n="69" num="1.69"><w n="69.1">D</w>’<w n="69.2">une</w> <w n="69.3">vapeur</w> <w n="69.4">étrange</w> <w n="69.5">et</w> <w n="69.6">d</w>’<w n="69.7">un</w> <w n="69.8">brouillard</w> <w n="69.9">de</w> <w n="69.10">houille</w> ;</l>
					<l n="70" num="1.70"><w n="70.1">Vers</w> <w n="70.2">ces</w> <w n="70.3">arbres</w> <w n="70.4">chargés</w> <w n="70.5">d</w>’<w n="70.6">un</w> <w n="70.7">feuillage</w> <w n="70.8">de</w> <w n="70.9">rouille</w>,</l>
					<l n="71" num="1.71"><w n="71.1">Et</w> <w n="71.2">vous</w> <w n="71.3">avez</w> <w n="71.4">compris</w>, <w n="71.5">pâle</w> <w n="71.6">fleur</w> <w n="71.7">du</w> <w n="71.8">désert</w>,</l>
					<l n="72" num="1.72"><w n="72.1">Que</w> <w n="72.2">loin</w> <w n="72.3">du</w> <w n="72.4">sol</w> <w n="72.5">natal</w> <w n="72.6">votre</w> <w n="72.7">arôme</w> <w n="72.8">se</w> <w n="72.9">perd</w>,</l>
					<l n="73" num="1.73"><w n="73.1">Qu</w>’<w n="73.2">il</w> <w n="73.3">vous</w> <w n="73.4">faut</w> <w n="73.5">le</w> <w n="73.6">soleil</w> <w n="73.7">et</w> <w n="73.8">la</w> <w n="73.9">blanche</w> <w n="73.10">rosée</w></l>
					<l n="74" num="1.74"><w n="74.1">Dont</w> <w n="74.2">vous</w> <w n="74.3">étiez</w> <w n="74.4">là</w>-<w n="74.5">bas</w> <w n="74.6">toute</w> <w n="74.7">jeune</w> <w n="74.8">arrosée</w> ;</l>
					<l n="75" num="1.75"><w n="75.1">Les</w> <w n="75.2">baisers</w> <w n="75.3">parfumés</w> <w n="75.4">des</w> <w n="75.5">brises</w> <w n="75.6">de</w> <w n="75.7">la</w> <w n="75.8">mer</w>,</l>
					<l n="76" num="1.76"><w n="76.1">La</w> <w n="76.2">place</w> <w n="76.3">libre</w> <w n="76.4">au</w> <w n="76.5">ciel</w>, <w n="76.6">l</w>’<w n="76.7">espace</w> <w n="76.8">et</w> <w n="76.9">le</w> <w n="76.10">grand</w> <w n="76.11">air</w>,</l>
					<l n="77" num="1.77"><w n="77.1">Et</w> <w n="77.2">pour</w> <w n="77.3">s</w>’<w n="77.4">y</w> <w n="77.5">renouer</w>, <w n="77.6">l</w>’<w n="77.7">hymne</w> <w n="77.8">saint</w> <w n="77.9">des</w> <w n="77.10">poëtes</w>,</l>
					<l n="78" num="1.78"><w n="78.1">Au</w> <w n="78.2">fond</w> <w n="78.3">de</w> <w n="78.4">vous</w> <w n="78.5">trouva</w> <w n="78.6">des</w> <w n="78.7">fibres</w> <w n="78.8">toutes</w> <w n="78.9">prêtes</w> ;</l>
					<l n="79" num="1.79"><w n="79.1">Au</w> <w n="79.2">chœur</w> <w n="79.3">mélodieux</w> <w n="79.4">votre</w> <w n="79.5">voix</w> <w n="79.6">put</w> <w n="79.7">s</w>’<w n="79.8">unir</w> ;</l>
					<l n="80" num="1.80"><w n="80.1">Le</w> <w n="80.2">prisme</w> <w n="80.3">du</w> <w n="80.4">regret</w> <w n="80.5">dorant</w> <w n="80.6">le</w> <w n="80.7">souvenir</w></l>
					<l n="81" num="1.81"><w n="81.1">De</w> <w n="81.2">cent</w> <w n="81.3">petits</w> <w n="81.4">détails</w>, <w n="81.5">de</w> <w n="81.6">mille</w> <w n="81.7">circonstances</w>,</l>
					<l n="82" num="1.82"><w n="82.1">Les</w> <w n="82.2">vers</w> <w n="82.3">naissaient</w> <w n="82.4">en</w> <w n="82.5">foule</w> <w n="82.6">et</w> <w n="82.7">se</w> <w n="82.8">groupaient</w> <w n="82.9">par</w> <w n="82.10">stances</w>.</l>
					<l n="83" num="1.83"><w n="83.1">Chaque</w> <w n="83.2">larme</w> <w n="83.3">furtive</w> <w n="83.4">échappée</w> <w n="83.5">à</w> <w n="83.6">vos</w> <w n="83.7">yeux</w></l>
					<l n="84" num="1.84"><w n="84.1">Se</w> <w n="84.2">condensait</w> <w n="84.3">en</w> <w n="84.4">perle</w>, <w n="84.5">en</w> <w n="84.6">joyau</w> <w n="84.7">précieux</w> ;</l>
					<l n="85" num="1.85"><w n="85.1">Dans</w> <w n="85.2">le</w> <w n="85.3">rhythme</w> <w n="85.4">profond</w>, <w n="85.5">votre</w> <w n="85.6">jeune</w> <w n="85.7">pensée</w></l>
					<l n="86" num="1.86"><w n="86.1">Brillait</w> <w n="86.2">plus</w> <w n="86.3">savamment</w>, <w n="86.4">chaque</w> <w n="86.5">jour</w> <w n="86.6">enchâssée</w> ;</l>
					<l n="87" num="1.87"><w n="87.1">Vous</w> <w n="87.2">avez</w> <w n="87.3">pénétré</w> <w n="87.4">les</w> <w n="87.5">mystères</w> <w n="87.6">de</w> <w n="87.7">l</w>’<w n="87.8">art</w> ;</l>
					<l n="88" num="1.88"><w n="88.1">Aussi</w>, <w n="88.2">tout</w> <w n="88.3">éplorée</w>, <w n="88.4">avant</w> <w n="88.5">votre</w> <w n="88.6">départ</w>,</l>
					<l n="89" num="1.89"><w n="89.1">Pour</w> <w n="89.2">vous</w> <w n="89.3">baiser</w> <w n="89.4">au</w> <w n="89.5">front</w>, <w n="89.6">la</w> <w n="89.7">belle</w> <w n="89.8">poésie</w></l>
					<l n="90" num="1.90"><w n="90.1">Vous</w> <w n="90.2">a</w> <w n="90.3">parmi</w> <w n="90.4">vos</w> <w n="90.5">sœurs</w> <w n="90.6">avec</w> <w n="90.7">amour</w> <w n="90.8">choisie</w> :</l>
					<l n="91" num="1.91"><w n="91.1">Pour</w> <w n="91.2">dire</w> <w n="91.3">votre</w> <w n="91.4">cœur</w> <w n="91.5">vous</w> <w n="91.6">avez</w> <w n="91.7">une</w> <w n="91.8">voix</w>,</l>
					<l n="92" num="1.92"><w n="92.1">Entre</w> <w n="92.2">deux</w> <w n="92.3">univers</w> <w n="92.4">Dieu</w> <w n="92.5">vous</w> <w n="92.6">laissait</w> <w n="92.7">le</w> <w n="92.8">choix</w> ;</l>
					<l n="93" num="1.93"><w n="93.1">Vous</w> <w n="93.2">avez</w> <w n="93.3">pris</w> <w n="93.4">de</w> <w n="93.5">l</w>’<w n="93.6">un</w>, <w n="93.7">heureux</w> <w n="93.8">sort</w> <w n="93.9">que</w> <w n="93.10">le</w> <w n="93.11">vôtre</w> !</l>
					<l n="94" num="1.94"><w n="94.1">De</w> <w n="94.2">quoi</w> <w n="94.3">vous</w> <w n="94.4">faire</w> <w n="94.5">aimer</w> <w n="94.6">et</w> <w n="94.7">regretter</w> <w n="94.8">dans</w> <w n="94.9">l</w>’<w n="94.10">autre</w>.</l>
				</lg>
			</div></body></text></TEI>