<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES NOUVELLES, POÉSIES INÉDITES ET POÉSIES POSTHUMES</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2661 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_8</idno>

				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Second</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://gutenberg.org/files/45886/45886-h/45886-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Second</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1890">1890</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1872">1831-1872</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>

			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU312">
				<head type="main">TRADUCTION LITTÉRALE</head>
				<head type="sub_1">Des fragments en vers qui se trouvent dans <lb></lb>L’ÉPICURIEN</head>
				<div type="section" n="1">
					<head type="number">I</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Sur</w> <w n="1.2">l</w>’<w n="1.3">eau</w> <w n="1.4">pure</w> <w n="1.5">du</w> <w n="1.6">lac</w>, <w n="1.7">dans</w> <w n="1.8">la</w> <w n="1.9">lueur</w> <w n="1.10">du</w> <w n="1.11">soir</w>,</l>
						<l n="2" num="1.2"><space quantity="8" unit="char"></space><w n="2.1">Le</w> <w n="2.2">reflet</w> <w n="2.3">d</w>’<w n="2.4">un</w> <w n="2.5">temple</w> <w n="2.6">s</w>’<w n="2.7">allonge</w>.</l>
						<l n="3" num="1.3"><w n="3.1">La</w> <w n="3.2">fille</w> <w n="3.3">de</w> <w n="3.4">Corinthe</w> <w n="3.5">y</w> <w n="3.6">vient</w>, <w n="3.7">et</w> <w n="3.8">va</w> <w n="3.9">s</w>’<w n="3.10">asseoir</w></l>
						<l n="4" num="1.4"><space quantity="8" unit="char"></space><w n="4.1">A</w> <w n="4.2">l</w>’<w n="4.3">escalier</w> <w n="4.4">qui</w> <w n="4.5">dans</w> <w n="4.6">l</w>’<w n="4.7">eau</w> <w n="4.8">plonge</w>.</l>
						<l n="5" num="1.5"><w n="5.1">Elle</w> <w n="5.2">feuillette</w> <w n="5.3">un</w> <w n="5.4">livre</w> <w n="5.5">et</w> <w n="5.6">se</w> <w n="5.7">penche</w> <w n="5.8">en</w> <w n="5.9">rêvant</w>.</l>
						<l n="6" num="1.6"><space quantity="8" unit="char"></space><w n="6.1">Placé</w> <w n="6.2">près</w> <w n="6.3">d</w>’<w n="6.4">elle</w>, <w n="6.5">un</w> <w n="6.6">jeune</w> <w n="6.7">sage</w></l>
						<l n="7" num="1.7"><w n="7.1">Écarte</w> <w n="7.2">ses</w> <w n="7.3">cheveux</w> <w n="7.4">dénoués</w>, <w n="7.5">dont</w> <w n="7.6">le</w> <w n="7.7">vent</w></l>
						<l n="8" num="1.8"><space quantity="8" unit="char"></space><w n="8.1">Fait</w> <w n="8.2">flotter</w> <w n="8.3">l</w>’<w n="8.4">ombre</w> <w n="8.5">sur</w> <w n="8.6">la</w> <w n="8.7">page</w>.</l>
					</lg>
				</div>
				<div type="section" n="2">
					<head type="number">II</head>
					<lg n="1">
						<l n="9" num="1.1"><space quantity="4" unit="char"></space><w n="9.1">Si</w> <w n="9.2">ce</w> <w n="9.3">n</w>’<w n="9.4">était</w> <w n="9.5">cette</w> <w n="9.6">voix</w> <w n="9.7">du</w> <w n="9.8">tombeau</w></l>
						<l n="10" num="1.2"><space quantity="8" unit="char"></space><w n="10.1">Qui</w> <w n="10.2">vient</w> <w n="10.3">chuchoter</w> <w n="10.4">à</w> <w n="10.5">la</w> <w n="10.6">joie</w>,</l>
						<l n="11" num="1.3"><space quantity="4" unit="char"></space><w n="11.1">Ce</w> <w n="11.2">corps</w> <w n="11.3">charmant</w>, <w n="11.4">ce</w> <w n="11.5">visage</w> <w n="11.6">si</w> <w n="11.7">beau</w>,</l>
						<l n="12" num="1.4"><space quantity="8" unit="char"></space><w n="12.1">Ce</w> <w n="12.2">soir</w> <w n="12.3">des</w> <w n="12.4">vers</w> <w n="12.5">seront</w> <w n="12.6">la</w> <w n="12.7">proie</w> ;</l>
						<l n="13" num="1.5"><space quantity="4" unit="char"></space><w n="13.1">Si</w> <w n="13.2">ce</w> <w n="13.3">n</w>’<w n="13.4">était</w> <w n="13.5">cette</w> <w n="13.6">amertume</w> <w n="13.7">au</w> <w n="13.8">cœur</w>,</l>
						<l n="14" num="1.6"><space quantity="4" unit="char"></space><w n="14.1">Dans</w> <w n="14.2">cette</w> <w n="14.3">vie</w>, <w n="14.4">oh</w> ! <w n="14.5">combien</w> <w n="14.6">de</w> <w n="14.7">bonheur</w> !</l>
						<l n="15" num="1.7"><space quantity="4" unit="char"></space><w n="15.1">Comme</w> <w n="15.2">mon</w> <w n="15.3">âme</w>, <w n="15.4">à</w> <w n="15.5">l</w>’<w n="15.6">absorber</w> <w n="15.7">avide</w>,</l>
						<l n="16" num="1.8"><space quantity="4" unit="char"></space><w n="16.1">Ne</w> <w n="16.2">quitterait</w> <w n="16.3">la</w> <w n="16.4">coupe</w> <w n="16.5">d</w>’<w n="16.6">or</w> <w n="16.7">que</w> <w n="16.8">vide</w> !</l>
						<l n="17" num="1.9"><space quantity="4" unit="char"></space><w n="17.1">Dieu</w> <w n="17.2">je</w> <w n="17.3">serais</w>, <w n="17.4">changeant</w> <w n="17.5">la</w> <w n="17.6">terre</w> <w n="17.7">en</w> <w n="17.8">cieux</w>,</l>
						<l n="18" num="1.10"><space quantity="4" unit="char"></space><w n="18.1">Si</w> <w n="18.2">le</w> <w n="18.3">plaisir</w> <w n="18.4">pouvait</w> <w n="18.5">faire</w> <w n="18.6">les</w> <w n="18.7">dieux</w> !</l>
					</lg>
				</div>
				<div type="section" n="3">
					<head type="number">III</head>
					<lg n="1">
						<l n="19" num="1.1"><w n="19.1">Aussi</w> <w n="19.2">loin</w> <w n="19.3">qu</w>’<w n="19.4">aux</w> <w n="19.5">clartés</w> <w n="19.6">du</w> <w n="19.7">plus</w> <w n="19.8">limpide</w> <w n="19.9">azur</w></l>
						<l n="20" num="1.2"><w n="20.1">Que</w> <w n="20.2">jamais</w> <w n="20.3">sur</w> <w n="20.4">la</w> <w n="20.5">sphère</w> <w n="20.6">ait</w> <w n="20.7">tendu</w> <w n="20.8">le</w> <w n="20.9">ciel</w> <w n="20.10">pur</w>,</l>
						<l n="21" num="1.3"><w n="21.1">L</w>’<w n="21.2">œil</w> <w n="21.3">saisit</w> <w n="21.4">des</w> <w n="21.5">objets</w> <w n="21.6">les</w> <w n="21.7">formes</w> <w n="21.8">apparues</w>,</l>
						<l n="22" num="1.4"><w n="22.1">On</w> <w n="22.2">découvre</w> <w n="22.3">toujours</w> <w n="22.4">des</w> <w n="22.5">jardins</w> <w n="22.6">et</w> <w n="22.7">des</w> <w n="22.8">rues</w></l>
						<l n="23" num="1.5"><w n="23.1">Marquant</w> <w n="23.2">de</w> <w n="23.3">leurs</w> <w n="23.4">piliers</w> <w n="23.5">des</w> <w n="23.6">parcours</w> <w n="23.7">infinis</w>,</l>
						<l n="24" num="1.6"><w n="24.1">Des</w> <w n="24.2">temples</w>, <w n="24.3">vaste</w> <w n="24.4">amas</w> <w n="24.5">de</w> <w n="24.6">marbres</w>, <w n="24.7">de</w> <w n="24.8">granits</w>,</l>
						<l n="25" num="1.7"><w n="25.1">Des</w> <w n="25.2">palais</w> <w n="25.3">de</w> <w n="25.4">porphyre</w> <w n="25.5">énormes</w> <w n="25.6">et</w> <w n="25.7">splendides</w>,</l>
						<l n="26" num="1.8"><w n="26.1">Et</w> <w n="26.2">s</w>’<w n="26.3">élançant</w> <w n="26.4">des</w> <w n="26.5">eaux</w> <w n="26.6">de</w> <w n="26.7">hautes</w> <w n="26.8">pyramides</w></l>
						<l n="27" num="1.9"><w n="27.1">Plus</w> <w n="27.2">vieilles</w> <w n="27.3">que</w> <w n="27.4">le</w> <w n="27.5">temps</w>, <w n="27.6">et</w> <w n="27.7">dont</w> <w n="27.8">l</w>’<w n="27.9">Éternité</w></l>
						<l n="28" num="1.10"><w n="28.1">N</w>’<w n="28.2">ébréchera</w> <w n="28.3">jamais</w> <w n="28.4">le</w> <w n="28.5">profil</w> <w n="28.6">respecté</w>.</l>
					</lg>
					<lg n="2">
						<l n="29" num="2.1"><w n="29.1">Cependant</w> <w n="29.2">sur</w> <w n="29.3">le</w> <w n="29.4">lac</w> <w n="29.5">tout</w> <w n="29.6">est</w> <w n="29.7">tumulte</w> <w n="29.8">et</w> <w n="29.9">joie</w>,</l>
						<l n="30" num="2.2"><w n="30.1">Et</w> <w n="30.2">l</w>’<w n="30.3">animation</w> <w n="30.4">largement</w> <w n="30.5">s</w>’<w n="30.6">y</w> <w n="30.7">déploie</w> ;</l>
						<l n="31" num="2.3"><w n="31.1">Le</w> <w n="31.2">commerce</w>, <w n="31.3">l</w>’<w n="31.4">amour</w> <w n="31.5">et</w> <w n="31.6">le</w> <w n="31.7">culte</w> <w n="31.8">des</w> <w n="31.9">dieux</w></l>
						<l n="32" num="2.4"><w n="32.1">Y</w> <w n="32.2">forment</w> <w n="32.3">un</w> <w n="32.4">spectacle</w> <w n="32.5">étrange</w> <w n="32.6">et</w> <w n="32.7">radieux</w>.</l>
						<l n="33" num="2.5"><w n="33.1">Une</w> <w n="33.2">procession</w> <w n="33.3">sur</w> <w n="33.4">les</w> <w n="33.5">marches</w> <w n="33.6">des</w> <w n="33.7">temples</w></l>
						<l n="34" num="2.6"><w n="34.1">Avec</w> <w n="34.2">ses</w> <w n="34.3">prêtres</w> <w n="34.4">blancs</w> <w n="34.5">vêtus</w> <w n="34.6">de</w> <w n="34.7">robes</w> <w n="34.8">amples</w></l>
						<l n="35" num="2.7"><w n="35.1">Se</w> <w n="35.2">développe</w> <w n="35.3">au</w> <w n="35.4">son</w> <w n="35.5">des</w> <w n="35.6">cymbales</w> <w n="35.7">d</w>’<w n="35.8">argent</w>.</l>
						<l n="36" num="2.8"><w n="36.1">Des</w> <w n="36.2">embarcations</w> <w n="36.3">au</w> <w n="36.4">sillon</w> <w n="36.5">diligent</w></l>
						<l n="37" num="2.9"><w n="37.1">Descendent</w> <w n="37.2">vers</w> <w n="37.3">la</w> <w n="37.4">mer</w>, <w n="37.5">venant</w> <w n="37.6">de</w> <w n="37.7">ces</w> <w n="37.8">contrées</w></l>
						<l n="38" num="2.10"><w n="38.1">Qu</w>’<w n="38.2">assourdissent</w> <w n="38.3">du</w> <w n="38.4">Nil</w> <w n="38.5">les</w> <w n="38.6">chutes</w> <w n="38.7">effarées</w>,</l>
						<l n="39" num="2.11"><w n="39.1">Avec</w> <w n="39.2">leur</w> <w n="39.3">cargaison</w> <w n="39.4">riche</w> <w n="39.5">comme</w> <w n="39.6">un</w> <w n="39.7">trésor</w>,</l>
						<l n="40" num="2.12"><w n="40.1">Plumes</w>, <w n="40.2">gemmes</w>, <w n="40.3">parfums</w>, <w n="40.4">ivoire</w> <w n="40.5">et</w> <w n="40.6">poudre</w> <w n="40.7">d</w>’<w n="40.8">or</w>,</l>
						<l n="41" num="2.13"><w n="41.1">Au</w> <w n="41.2">passage</w> <w n="41.3">exhalant</w> <w n="41.4">l</w>’<w n="41.5">odeur</w> <w n="41.6">aromatique</w></l>
						<l n="42" num="2.14"><w n="42.1">Que</w> <w n="42.2">prennent</w> <w n="42.3">les</w> <w n="42.4">vaisseaux</w> <w n="42.5">au</w> <w n="42.6">soleil</w> <w n="42.7">exotique</w>.</l>
					</lg>
					<lg n="3">
						<l n="43" num="3.1"><w n="43.1">Ici</w> <w n="43.2">des</w> <w n="43.3">pèlerins</w>, <w n="43.4">enfants</w> <w n="43.5">de</w> <w n="43.6">tous</w> <w n="43.7">pays</w>,</l>
						<l n="44" num="3.2"><w n="44.1">Avant</w> <w n="44.2">de</w> <w n="44.3">repartir</w> <w n="44.4">pour</w> <w n="44.5">Bubaste</w> <w n="44.6">ou</w> <w n="44.7">Saïs</w>,</l>
						<l n="45" num="3.3"><w n="45.1">Dans</w> <w n="45.2">une</w> <w n="45.3">baie</w> <w n="45.4">ombreuse</w> <w n="45.5">où</w> <w n="45.6">l</w>’<w n="45.7">onde</w> <w n="45.8">est</w> <w n="45.9">plus</w> <w n="45.10">tranquille</w></l>
						<l n="46" num="3.4"><w n="46.1">Poussent</w> <w n="46.2">l</w>’<w n="46.3">esquif</w> <w n="46.4">léger</w> <w n="46.5">avec</w> <w n="46.6">la</w> <w n="46.7">rame</w> <w n="46.8">agile</w>.</l>
						<l n="47" num="3.5"><w n="47.1">D</w>’<w n="47.2">autres</w> <w n="47.3">sous</w> <w n="47.4">les</w> <w n="47.5">lotus</w> <w n="47.6">bercent</w> <w n="47.7">leur</w> <w n="47.8">frais</w> <w n="47.9">sommeil</w>,</l>
						<l n="48" num="3.6"><w n="48.1">Ou</w> <w n="48.2">par</w> <w n="48.3">des</w> <w n="48.4">chants</w> <w n="48.5">joyeux</w> <w n="48.6">se</w> <w n="48.7">tiennent</w> <w n="48.8">en</w> <w n="48.9">éveil</w>.</l>
						<l n="49" num="3.7"><w n="49.1">Plus</w> <w n="49.2">loin</w> <w n="49.3">des</w> <w n="49.4">acacias</w> <w n="49.5">parfument</w> <w n="49.6">de</w> <w n="49.7">leurs</w> <w n="49.8">grappes</w></l>
						<l n="50" num="3.8"><w n="50.1">Une</w> <w n="50.2">plage</w> <w n="50.3">où</w> <w n="50.4">du</w> <w n="50.5">lac</w> <w n="50.6">fendant</w> <w n="50.7">les</w> <w n="50.8">claires</w> <w n="50.9">nappes</w></l>
						<l n="51" num="3.9"><w n="51.1">Folâtre</w> <w n="51.2">un</w> <w n="51.3">jeune</w> <w n="51.4">essaim</w> <w n="51.5">de</w> <w n="51.6">riantes</w> <w n="51.7">beautés</w></l>
						<l n="52" num="3.10"><w n="52.1">En</w> <w n="52.2">attraits</w> <w n="52.3">surpassant</w> <w n="52.4">les</w> <w n="52.5">charmes</w> <w n="52.6">si</w> <w n="52.7">vantés</w></l>
						<l n="53" num="3.11"><w n="53.1">De</w> <w n="53.2">celle</w> <w n="53.3">dont</w> <w n="53.4">la</w> <w n="53.5">chaîne</w> <w n="53.6">aimable</w> <w n="53.7">au</w> <w n="53.8">captif</w> <w n="53.9">même</w></l>
						<l n="54" num="3.12"><w n="54.1">Tint</w> <w n="54.2">deux</w> <w n="54.3">maîtres</w> <w n="54.4">du</w> <w n="54.5">monde</w> <w n="54.6">et</w> <w n="54.7">rompit</w> <w n="54.8">au</w> <w n="54.9">troisième</w>.</l>
					</lg>
				</div>
				<div type="section" n="4">
					<head type="number">IV</head>
					<lg n="1">
						<l ana="unanalyzable" n="55" num="1.1"><space quantity="12" unit="char"></space>.............. Astre dont le rayon</l>
						<l n="56" num="1.2"><w n="56.1">S</w>’<w n="56.2">épanchant</w> <w n="56.3">sur</w> <w n="56.4">le</w> <w n="56.5">monde</w> <w n="56.6">aux</w> <w n="56.7">heures</w> <w n="56.8">taciturnes</w>,</l>
						<l n="57" num="1.3"><w n="57.1">Fait</w> <w n="57.2">éclore</w> <w n="57.3">le</w> <w n="57.4">rêve</w> <w n="57.5">avec</w> <w n="57.6">les</w> <w n="57.7">fleurs</w> <w n="57.8">nocturnes</w>,</l>
						<l n="58" num="1.4"><w n="58.1">Non</w> <w n="58.2">cette</w> <w n="58.3">lune</w> <w n="58.4">froide</w> <w n="58.5">et</w> <w n="58.6">brumeuse</w> <w n="58.7">du</w> <w n="58.8">nord</w>,</l>
						<l n="59" num="1.5"><w n="59.1">Versant</w> <w n="59.2">aux</w> <w n="59.3">jeunes</w> <w n="59.4">cœurs</w>, <w n="59.5">comme</w> <w n="59.6">un</w> <w n="59.7">philtre</w> <w n="59.8">de</w> <w n="59.9">mort</w>,</l>
						<l n="60" num="1.6"><w n="60.1">Le</w> <w n="60.2">sang</w> <w n="60.3">pâle</w> <w n="60.4">et</w> <w n="60.5">glacé</w> <w n="60.6">de</w> <w n="60.7">la</w> <w n="60.8">vestale</w> <w n="60.9">chaste</w> ;</l>
						<l n="61" num="1.7"><w n="61.1">Mais</w> <w n="61.2">l</w>’<w n="61.3">ardente</w> <w n="61.4">Phœbé</w> <w n="61.5">qui</w> <w n="61.6">règne</w> <w n="61.7">dans</w> <w n="61.8">Bubaste</w>,</l>
						<l n="62" num="1.8"><w n="62.1">Et</w> <w n="62.2">ne</w> <w n="62.3">voit</w> <w n="62.4">rien</w>, <w n="62.5">du</w> <w n="62.6">haut</w> <w n="62.7">de</w> <w n="62.8">son</w> <w n="62.9">brillant</w> <w n="62.10">séjour</w>,</l>
						<l n="63" num="1.9"><w n="63.1">Chez</w> <w n="63.2">l</w>’<w n="63.3">homme</w> <w n="63.4">et</w> <w n="63.5">chez</w> <w n="63.6">les</w> <w n="63.7">dieux</w> <w n="63.8">d</w>’<w n="63.9">aussi</w> <w n="63.10">beau</w> <w n="63.11">que</w> <w n="63.12">l</w>’<w n="63.13">amour</w> !</l>
					</lg>
				</div>
				<div type="section" n="5">
					<head type="number">V</head>
					<lg n="1">
						<l n="64" num="1.1"><w n="64.1">Rhodope</w>, <w n="64.2">cette</w> <w n="64.3">nymphe</w> <w n="64.4">à</w> <w n="64.5">la</w> <w n="64.6">beauté</w> <w n="64.7">splendide</w>,</l>
						<l n="65" num="1.2"><w n="65.1">Qui</w> <w n="65.2">vit</w>, <w n="65.3">dit</w>-<w n="65.4">on</w>, <w n="65.5">plongée</w> <w n="65.6">en</w> <w n="65.7">un</w> <w n="65.8">demi</w>-<w n="65.9">sommeil</w>,</l>
						<l n="66" num="1.3"><w n="66.1">Sur</w> <w n="66.2">l</w>’<w n="66.3">or</w> <w n="66.4">et</w> <w n="66.5">les</w> <w n="66.6">bijoux</w> <w n="66.7">inconnus</w> <w n="66.8">au</w> <w n="66.9">soleil</w>,</l>
						<l n="67" num="1.4"><space quantity="8" unit="char"></space><w n="67.1">La</w> <w n="67.2">Dame</w> <w n="67.3">de</w> <w n="67.4">la</w> <w n="67.5">Pyramide</w> !</l>
					</lg>
				</div>
				<div type="section" n="6">
					<head type="number">VI</head>
					<lg n="1">
						<l n="68" num="1.1"><space quantity="12" unit="char"></space><w n="68.1">Vous</w> <w n="68.2">qui</w> <w n="68.3">voulez</w> <w n="68.4">courir</w></l>
						<l n="69" num="1.2"><space quantity="12" unit="char"></space><w n="69.1">La</w> <w n="69.2">terrible</w> <w n="69.3">carrière</w>,</l>
						<l n="70" num="1.3"><space quantity="12" unit="char"></space><w n="70.1">Il</w> <w n="70.2">faut</w> <w n="70.3">vivre</w> <w n="70.4">ou</w> <w n="70.5">mourir</w></l>
						<l n="71" num="1.4"><space quantity="12" unit="char"></space><w n="71.1">Sans</w> <w n="71.2">regard</w> <w n="71.3">en</w> <w n="71.4">arrière</w>.</l>
					</lg>
					<lg n="2">
						<l n="72" num="2.1"><space quantity="12" unit="char"></space><w n="72.1">Vous</w> <w n="72.2">qui</w> <w n="72.3">voulez</w> <w n="72.4">tenter</w></l>
						<l n="73" num="2.2"><space quantity="12" unit="char"></space><w n="73.1">L</w>’<w n="73.2">onde</w>, <w n="73.3">l</w>’<w n="73.4">air</w> <w n="73.5">et</w> <w n="73.6">la</w> <w n="73.7">flamme</w>,</l>
						<l n="74" num="2.3"><space quantity="12" unit="char"></space><w n="74.1">Terreurs</w> <w n="74.2">à</w> <w n="74.3">surmonter</w></l>
						<l n="75" num="2.4"><space quantity="12" unit="char"></space><w n="75.1">Pour</w> <w n="75.2">épurer</w> <w n="75.3">votre</w> <w n="75.4">âme</w>,</l>
					</lg>
					<lg n="3">
						<l n="76" num="3.1"><space quantity="12" unit="char"></space><w n="76.1">Si</w>, <w n="76.2">méprisant</w> <w n="76.3">la</w> <w n="76.4">mort</w>,</l>
						<l n="77" num="3.2"><space quantity="12" unit="char"></space><w n="77.1">Votre</w> <w n="77.2">foi</w> <w n="77.3">reste</w> <w n="77.4">entière</w>,</l>
						<l n="78" num="3.3"><space quantity="12" unit="char"></space><w n="78.1">En</w> <w n="78.2">avant</w> ! — <w n="78.3">le</w> <w n="78.4">cœur</w> <w n="78.5">fort</w></l>
						<l n="79" num="3.4"><space quantity="12" unit="char"></space><w n="79.1">Reverra</w> <w n="79.2">la</w> <w n="79.3">lumière</w>.</l>
					</lg>
					<lg n="4">
						<l n="80" num="4.1"><space quantity="12" unit="char"></space><w n="80.1">Et</w> <w n="80.2">lira</w> <w n="80.3">sur</w> <w n="80.4">l</w>’<w n="80.5">autel</w></l>
						<l n="81" num="4.2"><space quantity="12" unit="char"></space><w n="81.1">Le</w> <w n="81.2">mot</w> <w n="81.3">du</w> <w n="81.4">grand</w> <w n="81.5">mystère</w></l>
						<l n="82" num="4.3"><space quantity="12" unit="char"></space><w n="82.1">Qu</w>’<w n="82.2">au</w> <w n="82.3">profane</w> <w n="82.4">mortel</w></l>
						<l n="83" num="4.4"><space quantity="12" unit="char"></space><w n="83.1">Dérobe</w> <w n="83.2">un</w> <w n="83.3">voile</w> <w n="83.4">austère</w>.</l>
					</lg>
				</div>
				<div type="section" n="7">
					<head type="number">VII</head>
					<lg n="1">
						<l n="84" num="1.1"><space quantity="4" unit="char"></space><w n="84.1">Bois</w> <w n="84.2">cette</w> <w n="84.3">coupe</w> — <w n="84.4">Osiris</w> <w n="84.5">la</w> <w n="84.6">savoure</w></l>
						<l n="85" num="1.2"><space quantity="4" unit="char"></space><w n="85.1">A</w> <w n="85.2">petits</w> <w n="85.3">traits</w> <w n="85.4">dans</w> <w n="85.5">l</w>’<w n="85.6">empire</w> <w n="85.7">des</w> <w n="85.8">morts</w> ;</l>
						<l n="86" num="1.3"><space quantity="4" unit="char"></space><w n="86.1">Il</w> <w n="86.2">la</w> <w n="86.3">fait</w> <w n="86.4">boire</w> <w n="86.5">au</w> <w n="86.6">peuple</w> <w n="86.7">qui</w> <w n="86.8">l</w>’<w n="86.9">entoure</w>,</l>
						<l n="87" num="1.4"><space quantity="4" unit="char"></space><w n="87.1">Chaque</w> <w n="87.2">fantôme</w> <w n="87.3">en</w> <w n="87.4">effleure</w> <w n="87.5">les</w> <w n="87.6">bords</w>.</l>
					</lg>
					<lg n="2">
						<l n="88" num="2.1"><space quantity="4" unit="char"></space><w n="88.1">Bois</w> <w n="88.2">cette</w> <w n="88.3">coupe</w> — <w n="88.4">elle</w> <w n="88.5">est</w>, <w n="88.6">tout</w> <w n="88.7">frais</w>, <w n="88.8">remplie</w></l>
						<l n="89" num="2.2"><space quantity="4" unit="char"></space><w n="89.1">D</w>’<w n="89.2">une</w> <w n="89.3">eau</w> <w n="89.4">puisée</w> <w n="89.5">au</w> <w n="89.6">fleuve</w> <w n="89.7">du</w> <w n="89.8">Léthé</w> ;</l>
						<l n="90" num="2.3"><space quantity="4" unit="char"></space><w n="90.1">En</w> <w n="90.2">la</w> <w n="90.3">vidant</w> <w n="90.4">tout</w> <w n="90.5">le</w> <w n="90.6">passé</w> <w n="90.7">s</w>’<w n="90.8">oublie</w></l>
						<l n="91" num="2.4"><space quantity="4" unit="char"></space><w n="91.1">Comme</w> <w n="91.2">un</w> <w n="91.3">vain</w> <w n="91.4">songe</w> <w n="91.5">au</w> <w n="91.6">matin</w> <w n="91.7">emporté</w> !</l>
					</lg>
					<lg n="3">
						<l n="92" num="3.1"><space quantity="12" unit="char"></space><w n="92.1">Le</w> <w n="92.2">plaisir</w>, <w n="92.3">fausse</w> <w n="92.4">ivresse</w>,</l>
						<l n="93" num="3.2"><space quantity="12" unit="char"></space><w n="93.1">Vin</w> <w n="93.2">mêlé</w> <w n="93.3">de</w> <w n="93.4">poison</w> ;</l>
						<l n="94" num="3.3"><space quantity="12" unit="char"></space><w n="94.1">La</w> <w n="94.2">science</w>, <w n="94.3">maîtresse</w></l>
						<l n="95" num="3.4"><space quantity="12" unit="char"></space><w n="95.1">A</w> <w n="95.2">la</w> <w n="95.3">dure</w> <w n="95.4">leçon</w> ;</l>
					</lg>
					<lg n="4">
						<l n="96" num="4.1"><space quantity="12" unit="char"></space><w n="96.1">L</w>’<w n="96.2">espoir</w> <w n="96.3">brillant</w> <w n="96.4">et</w> <w n="96.5">vide</w>,</l>
						<l n="97" num="4.2"><space quantity="12" unit="char"></space><w n="97.1">Semblable</w> <w n="97.2">aux</w> <w n="97.3">lacs</w> <w n="97.4">amers</w>,</l>
						<l n="98" num="4.3"><space quantity="12" unit="char"></space><w n="98.1">Trompant</w> <w n="98.2">la</w> <w n="98.3">lèvre</w> <w n="98.4">avide</w></l>
						<l n="99" num="4.4"><space quantity="12" unit="char"></space><w n="99.1">Aux</w> <w n="99.2">sables</w> <w n="99.3">des</w> <w n="99.4">déserts</w> ;</l>
					</lg>
					<lg n="5">
						<l n="100" num="5.1"><space quantity="12" unit="char"></space><w n="100.1">L</w>’<w n="100.2">amour</w> <w n="100.3">dont</w> <w n="100.4">la</w> <w n="100.5">main</w> <w n="100.6">noue</w></l>
						<l n="101" num="5.2"><space quantity="12" unit="char"></space><w n="101.1">Des</w> <w n="101.2">liens</w> <w n="101.3">innocents</w></l>
						<l n="102" num="5.3"><space quantity="12" unit="char"></space><w n="102.1">Où</w> <w n="102.2">le</w> <w n="102.3">serpent</w> <w n="102.4">se</w> <w n="102.5">joue</w></l>
						<l n="103" num="5.4"><space quantity="12" unit="char"></space><w n="103.1">En</w> <w n="103.2">replis</w> <w n="103.3">malfaisants</w> ;</l>
					</lg>
					<lg n="6">
						<l n="104" num="6.1"><w n="104.1">Tout</w> <w n="104.2">ce</w> <w n="104.3">que</w> <w n="104.4">tu</w> <w n="104.5">connus</w> <w n="104.6">de</w> <w n="104.7">mauvais</w> <w n="104.8">ou</w> <w n="104.9">d</w>’<w n="104.10">infâme</w></l>
						<l n="105" num="6.2"><w n="105.1">Disparaîtra</w> <w n="105.2">soudain</w> <w n="105.3">dans</w> <w n="105.4">un</w> <w n="105.5">oubli</w> <w n="105.6">profond</w>,</l>
						<l n="106" num="6.3"><w n="106.1">De</w> <w n="106.2">tout</w> <w n="106.3">ressouvenir</w> <w n="106.4">laissant</w> <w n="106.5">pure</w> <w n="106.6">ton</w> <w n="106.7">âme</w></l>
						<l n="107" num="6.4"><w n="107.1">Quand</w> <w n="107.2">ta</w> <w n="107.3">soif</w> <w n="107.4">de</w> <w n="107.5">la</w> <w n="107.6">coupe</w> <w n="107.7">aura</w> <w n="107.8">tari</w> <w n="107.9">le</w> <w n="107.10">fond</w>.</l>
					</lg>
				</div>
				<div type="section" n="8">
					<head type="number">VIII</head>
					<lg n="1">
						<l n="108" num="1.1"><w n="108.1">Bois</w> <w n="108.2">cette</w> <w n="108.3">coupe</w> — <w n="108.4">elle</w> <w n="108.5">est</w> <w n="108.6">pleine</w> <w n="108.7">d</w>’<w n="108.8">un</w> <w n="108.9">divin</w> <w n="108.10">baume</w>.</l>
						<l n="109" num="1.2"><w n="109.1">Quand</w> <w n="109.2">Isis</w> <w n="109.3">vint</w> <w n="109.4">aux</w> <w n="109.5">cieux</w>, <w n="109.6">Horus</w> <w n="109.7">entre</w> <w n="109.8">les</w> <w n="109.9">bras</w>,</l>
						<l n="110" num="1.3"><w n="110.1">Elle</w> <w n="110.2">dit</w> <w n="110.3">à</w> <w n="110.4">son</w> <w n="110.5">fils</w>, <w n="110.6">lui</w> <w n="110.7">montrant</w> <w n="110.8">son</w> <w n="110.9">royaume</w>,</l>
						<l n="111" num="1.4"><space quantity="4" unit="char"></space><w n="111.1">Bois</w> <w n="111.2">cette</w> <w n="111.3">coupe</w> <w n="111.4">et</w> <w n="111.5">toujours</w> <w n="111.6">tu</w> <w n="111.7">vivras</w> !</l>
					</lg>
					<lg n="2">
						<l n="112" num="2.1"><w n="112.1">Je</w> <w n="112.2">te</w> <w n="112.3">dis</w> <w n="112.4">et</w> <w n="112.5">te</w> <w n="112.6">chante</w>, <w n="112.7">ainsi</w> <w n="112.8">que</w> <w n="112.9">la</w> <w n="112.10">déesse</w>,</l>
						<l n="113" num="2.2"><w n="113.1">Toi</w> <w n="113.2">qui</w> <w n="113.3">des</w> <w n="113.4">vastes</w> <w n="113.5">cieux</w> <w n="113.6">un</w> <w n="113.7">jour</w> <w n="113.8">hériteras</w> :</l>
						<l n="114" num="2.3"><w n="114.1">Fusses</w>-<w n="114.2">tu</w> <w n="114.3">dans</w> <w n="114.4">l</w>’<w n="114.5">abîme</w>, <w n="114.6">âme</w> <w n="114.7">et</w> <w n="114.8">corps</w> <w n="114.9">en</w> <w n="114.10">détresse</w>,</l>
						<l n="115" num="2.4"><space quantity="4" unit="char"></space><w n="115.1">Bois</w> <w n="115.2">cette</w> <w n="115.3">coupe</w> <w n="115.4">et</w> <w n="115.5">toujours</w> <w n="115.6">tu</w> <w n="115.7">vivras</w> !</l>
					</lg>
				</div>
				<div type="section" n="9">
					<head type="number">IX</head>
					<lg n="1">
						<l n="116" num="1.1"><w n="116.1">La</w> <w n="116.2">Mémoire</w> <w n="116.3">viendra</w>, <w n="116.4">menant</w> <w n="116.5">le</w> <w n="116.6">chœur</w> <w n="116.7">des</w> <w n="116.8">rêves</w>,</l>
						<l n="117" num="1.2"><w n="117.1">Rêves</w> <w n="117.2">d</w>’<w n="117.3">un</w> <w n="117.4">temps</w> <w n="117.5">plus</w> <w n="117.6">beau</w>, <w n="117.7">plus</w> <w n="117.8">ancien</w> <w n="117.9">et</w> <w n="117.10">plus</w> <w n="117.11">pur</w> ;</l>
						<l n="118" num="1.3"><w n="118.1">Quand</w> <w n="118.2">l</w>’<w n="118.3">âme</w>, <w n="118.4">hôte</w> <w n="118.5">des</w> <w n="118.6">cieux</w>, <w n="118.7">n</w>’<w n="118.8">avait</w> <w n="118.9">pas</w> <w n="118.10">sur</w> <w n="118.11">les</w> <w n="118.12">grèves</w></l>
						<l n="119" num="1.4"><w n="119.1">Laissé</w> <w n="119.2">choir</w> <w n="119.3">le</w> <w n="119.4">duvet</w> <w n="119.5">de</w> <w n="119.6">ses</w> <w n="119.7">ailes</w> <w n="119.8">d</w>’<w n="119.9">azur</w> ;</l>
					</lg>
					<lg n="2">
						<l n="120" num="2.1"><w n="120.1">Souvenirs</w> <w n="120.2">glorieux</w>, <w n="120.3">pareils</w> <w n="120.4">à</w> <w n="120.5">cette</w> <w n="120.6">flamme</w></l>
						<l n="121" num="2.2"><w n="121.1">Que</w> <w n="121.2">lance</w>, <w n="121.3">en</w> <w n="121.4">s</w>’<w n="121.5">éteignant</w>, <w n="121.6">sur</w> <w n="121.7">les</w> <w n="121.8">eaux</w> <w n="121.9">l</w>’<w n="121.10">astre</w> <w n="121.11">d</w>’<w n="121.12">or</w>,</l>
						<l n="122" num="2.3"><w n="122.1">Qui</w> <w n="122.2">montre</w> <w n="122.3">ce</w> <w n="122.4">que</w> <w n="122.5">fut</w> <w n="122.6">et</w> <w n="122.7">ce</w> <w n="122.8">que</w> <w n="122.9">n</w>’<w n="122.10">est</w> <w n="122.11">plus</w> <w n="122.12">l</w>’<w n="122.13">âme</w>,</l>
						<l n="123" num="2.4"><w n="123.1">Mais</w> <w n="123.2">ce</w> <w n="123.3">qu</w>’<w n="123.4">elle</w> <w n="123.5">pourrait</w> <w n="123.6">brillamment</w> <w n="123.7">être</w> <w n="123.8">encor</w>.</l>
					</lg>
				</div>
				<div type="section" n="10">
					<head type="main">X</head>
					<lg n="1">
						<l n="124" num="1.1"><space quantity="8" unit="char"></space><w n="124.1">O</w> <w n="124.2">bel</w> <w n="124.3">arbre</w> <w n="124.4">d</w>’<w n="124.5">Abyssinie</w> !</l>
						<l n="125" num="1.2"><space quantity="8" unit="char"></space><w n="125.1">Nous</w> <w n="125.2">te</w> <w n="125.3">prions</w> <w n="125.4">par</w> <w n="125.5">ton</w> <w n="125.6">fruit</w> <w n="125.7">d</w>’<w n="125.8">or</w>,</l>
						<l n="126" num="1.3"><space quantity="8" unit="char"></space><w n="126.1">Par</w> <w n="126.2">la</w> <w n="126.3">pourpre</w> <w n="126.4">à</w> <w n="126.5">l</w>’<w n="126.6">azur</w> <w n="126.7">unie</w></l>
						<l n="127" num="1.4"><space quantity="8" unit="char"></space><w n="127.1">Dans</w> <w n="127.2">ta</w> <w n="127.3">fleur</w> <w n="127.4">plus</w> <w n="127.5">splendide</w> <w n="127.6">encor</w>,</l>
						<l n="128" num="1.5"><space quantity="8" unit="char"></space><w n="128.1">Par</w> <w n="128.2">la</w> <w n="128.3">muette</w> <w n="128.4">bienvenue</w></l>
						<l n="129" num="1.6"><space quantity="8" unit="char"></space><w n="129.1">Dont</w> <w n="129.2">ta</w> <w n="129.3">ramure</w>, <w n="129.4">en</w> <w n="129.5">s</w>’<w n="129.6">abaissant</w>,</l>
						<l n="130" num="1.7"><space quantity="8" unit="char"></space><w n="130.1">D</w>’<w n="130.2">un</w> <w n="130.3">air</w> <w n="130.4">hospitalier</w> <w n="130.5">salue</w></l>
						<l n="131" num="1.8"><space quantity="8" unit="char"></space><w n="131.1">L</w>’<w n="131.2">étranger</w> <w n="131.3">sous</w> <w n="131.4">ton</w> <w n="131.5">dais</w> <w n="131.6">passant</w>.</l>
					</lg>
					<lg n="2">
						<l n="132" num="2.1"><space quantity="8" unit="char"></space><w n="132.1">O</w> <w n="132.2">bel</w> <w n="132.3">arbre</w> <w n="132.4">d</w>’<w n="132.5">Abyssinie</w> !</l>
						<l n="133" num="2.2"><space quantity="8" unit="char"></space><w n="133.1">Quand</w> <w n="133.2">la</w> <w n="133.3">nuit</w>, <w n="133.4">sans</w> <w n="133.5">lune</w>, <w n="133.6">descend</w>,</l>
						<l n="134" num="2.3"><space quantity="8" unit="char"></space><w n="134.1">Combien</w> <w n="134.2">ta</w> <w n="134.3">rencontre</w> <w n="134.4">est</w> <w n="134.5">bénie</w></l>
						<l n="135" num="2.4"><space quantity="8" unit="char"></space><w n="135.1">Du</w> <w n="135.2">voyageur</w> <w n="135.3">au</w> <w n="135.4">pas</w> <w n="135.5">pesant</w> !</l>
						<l n="136" num="2.5"><space quantity="8" unit="char"></space><w n="136.1">Du</w> <w n="136.2">bout</w> <w n="136.3">caressant</w> <w n="136.4">de</w> <w n="136.5">tes</w> <w n="136.6">branches</w></l>
						<l n="137" num="2.6"><space quantity="8" unit="char"></space><w n="137.1">Tu</w> <w n="137.2">viens</w> <w n="137.3">baiser</w> <w n="137.4">ses</w> <w n="137.5">yeux</w> <w n="137.6">mi</w>-<w n="137.7">clos</w>,</l>
						<l n="138" num="2.7"><space quantity="8" unit="char"></space><w n="138.1">Sur</w> <w n="138.2">lui</w> <w n="138.3">tendrement</w> <w n="138.4">tu</w> <w n="138.5">te</w> <w n="138.6">penches</w></l>
						<l n="139" num="2.8"><space quantity="8" unit="char"></space><w n="139.1">Et</w> <w n="139.2">tu</w> <w n="139.3">lui</w> <w n="139.4">dis</w> : «<w n="139.5">Dors</w> <w n="139.6">en</w> <w n="139.7">repos</w> !»</l>
					</lg>
					<lg n="3">
						<l n="140" num="3.1"><space quantity="8" unit="char"></space><w n="140.1">O</w> <w n="140.2">bel</w> <w n="140.3">arbre</w> <w n="140.4">d</w>’<w n="140.5">Abyssinie</w> !</l>
						<l n="141" num="3.2"><space quantity="4" unit="char"></space><w n="141.1">Ainsi</w>, <w n="141.2">vers</w> <w n="141.3">moi</w>, <w n="141.4">penche</w> <w n="141.5">ton</w> <w n="141.6">front</w> <w n="141.7">qui</w> <w n="141.8">plie</w>.</l>
					</lg>
				</div>
				<div type="section" n="11">
					<head type="number">XI</head>
					<lg n="1">
						<l n="142" num="1.1"><w n="142.1">Par</w> <w n="142.2">une</w> <w n="142.3">de</w> <w n="142.4">ces</w> <w n="142.5">nuits</w> <w n="142.6">où</w> <w n="142.7">l</w>’<w n="142.8">étoile</w> <w n="142.9">d</w>’<w n="142.10">amour</w>,</l>
						<l n="143" num="1.2"><w n="143.1">Isis</w>, <w n="143.2">de</w> <w n="143.3">son</w> <w n="143.4">croissant</w> <w n="143.5">dessinant</w> <w n="143.6">le</w> <w n="143.7">contour</w>,</l>
						<l n="144" num="1.3"><w n="144.1">Dans</w> <w n="144.2">le</w> <w n="144.3">fleuve</w> <w n="144.4">sacré</w> <w n="144.5">mire</w> <w n="144.6">son</w> <w n="144.7">front</w> <w n="144.8">de</w> <w n="144.9">vierge</w>,</l>
						<l n="145" num="1.4"><w n="145.1">Où</w> <w n="145.2">les</w> <w n="145.3">couples</w>, <w n="145.4">guettant</w> <w n="145.5">sa</w> <w n="145.6">lueur</w> <w n="145.7">de</w> <w n="145.8">la</w> <w n="145.9">berge</w>,</l>
						<l n="146" num="1.5"><w n="146.1">Calculent</w> <w n="146.2">en</w> <w n="146.3">quel</w> <w n="146.4">temps</w> <w n="146.5">son</w> <w n="146.6">cours</w> <w n="146.7">recommencé</w></l>
						<l n="147" num="1.6"><w n="147.1">Doit</w> <w n="147.2">la</w> <w n="147.3">remettre</w> <w n="147.4">aux</w> <w n="147.5">bras</w> <w n="147.6">du</w> <w n="147.7">Soleil</w>-<w n="147.8">fiancé</w>.</l>
					</lg>
				</div>
				<div type="section" n="12">
					<head type="number">XII</head>
					<lg n="1">
						<l ana="unanalyzable" n="148" num="1.1"><space quantity="12" unit="char"></space>............ Le fleuve qui naguère</l>
						<l n="149" num="1.2"><w n="149.1">Glissait</w> <w n="149.2">entre</w> <w n="149.3">ses</w> <w n="149.4">bords</w>, <w n="149.5">garni</w> <w n="149.6">des</w> <w n="149.7">deux</w> <w n="149.8">côtés</w></l>
						<l n="150" num="1.3"><w n="150.1">Par</w> <w n="150.2">des</w> <w n="150.3">palais</w> <w n="150.4">de</w> <w n="150.5">marbre</w> <w n="150.6">et</w> <w n="150.7">de</w> <w n="150.8">riches</w> <w n="150.9">cités</w>,</l>
						<l n="151" num="1.4"><w n="151.1">Pareils</w> <w n="151.2">à</w> <w n="151.3">des</w> <w n="151.4">joyaux</w> <w n="151.5">sertis</w> <w n="151.6">dans</w> <w n="151.7">une</w> <w n="151.8">chaîne</w>,</l>
						<l n="152" num="1.5"><w n="152.1">Inondant</w> <w n="152.2">à</w> <w n="152.3">présent</w> <w n="152.4">la</w> <w n="152.5">vallée</w> <w n="152.6">et</w> <w n="152.7">la</w> <w n="152.8">plaine</w>,</l>
						<l n="153" num="1.6"><w n="153.1">Comme</w> <w n="153.2">un</w> <w n="153.3">géant</w> <w n="153.4">qui</w> <w n="153.5">sort</w> <w n="153.6">de</w> <w n="153.7">son</w> <w n="153.8">lit</w> <w n="153.9">brusquement</w>,</l>
						<l n="154" num="1.7"><w n="154.1">S</w>’<w n="154.2">étale</w> <w n="154.3">et</w> <w n="154.4">couvre</w> <w n="154.5">tout</w> <w n="154.6">de</w> <w n="154.7">son</w> <w n="154.8">flot</w> <w n="154.9">écumant</w>.</l>
					</lg>
				</div>
				<closer>
					<dateline>
						<date when="1865">1865</date>.
					</dateline>
				</closer>
			</div></body></text></TEI>