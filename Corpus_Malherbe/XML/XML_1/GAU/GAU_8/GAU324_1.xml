<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES NOUVELLES, POÉSIES INÉDITES ET POÉSIES POSTHUMES</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2661 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_8</idno>

				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Second</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://gutenberg.org/files/45886/45886-h/45886-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Second</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1890">1890</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1872">1831-1872</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>

			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU324">
				<head type="main">LA FUMÉE</head>
				<head type="form">SONNET</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Souvent</w> <w n="1.2">nous</w> <w n="1.3">fuyons</w> <w n="1.4">en</w> <w n="1.5">petit</w> <w n="1.6">coupé</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Car</w> <w n="2.2">chez</w> <w n="2.3">moi</w> <w n="2.4">toujours</w> <w n="2.5">la</w> <w n="2.6">sonnette</w> <w n="2.7">grince</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Et</w> <w n="3.2">les</w> <w n="3.3">visiteurs</w> <w n="3.4">qu</w>’<w n="3.5">en</w> <w n="3.6">vain</w> <w n="3.7">l</w>’<w n="3.8">on</w> <w n="3.9">évince</w></l>
					<l n="4" num="1.4"><w n="4.1">Chassent</w> <w n="4.2">le</w> <w n="4.3">plaisir</w> <w n="4.4">de</w> <w n="4.5">mon</w> <w n="4.6">canapé</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Couple</w> <w n="5.2">par</w> <w n="5.3">l</w>’<w n="5.4">amour</w> <w n="5.5">et</w> <w n="5.6">l</w>’<w n="5.7">hiver</w> <w n="5.8">groupé</w>,</l>
					<l n="6" num="2.2"><w n="6.1">Nous</w> <w n="6.2">nous</w> <w n="6.3">serrons</w> <w n="6.4">bien</w>, <w n="6.5">car</w> <w n="6.6">la</w> <w n="6.7">bise</w> <w n="6.8">pince</w> ;</l>
					<l n="7" num="2.3"><w n="7.1">Sur</w> <w n="7.2">mon</w> <w n="7.3">bras</w> <w n="7.4">se</w> <w n="7.5">cambre</w> <w n="7.6">un</w> <w n="7.7">corps</w> <w n="7.8">souple</w> <w n="7.9">et</w> <w n="7.10">mince</w>,</l>
					<l n="8" num="2.4"><w n="8.1">D</w>’<w n="8.2">un</w> <w n="8.3">châle</w> <w n="8.4">à</w> <w n="8.5">longs</w> <w n="8.6">plis</w> <w n="8.7">bien</w> <w n="8.8">enveloppé</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Dans</w> <w n="9.2">une</w> <w n="9.3">voiture</w> <w n="9.4">au</w> <w n="9.5">pas</w> <w n="9.6">et</w> <w n="9.7">fermée</w>,</l>
					<l n="10" num="3.2"><w n="10.1">Pour</w> <w n="10.2">nous</w> <w n="10.3">embrasser</w>, <w n="10.4">il</w> <w n="10.5">serait</w> <w n="10.6">bourgeois</w>,</l>
					<l n="11" num="3.3"><w n="11.1">De</w> <w n="11.2">baisser</w> <w n="11.3">le</w> <w n="11.4">store</w> <w n="11.5">au</w> <w n="11.6">milieu</w> <w n="11.7">du</w> <w n="11.8">Bois</w> ;</l>
				</lg>
				<lg n="4">
					<l n="12" num="4.1"><w n="12.1">J</w>’<w n="12.2">allume</w> <w n="12.3">un</w> <w n="12.4">cigare</w> <w n="12.5">et</w> <w n="12.6">ma</w> <w n="12.7">bien</w>-<w n="12.8">aimée</w></l>
					<l n="13" num="4.2"><w n="13.1">Un</w> <w n="13.2">papelito</w> <w n="13.3">roulé</w> <w n="13.4">par</w> <w n="13.5">ses</w> <w n="13.6">doigts</w>,</l>
					<l n="14" num="4.3"><w n="14.1">Et</w> <w n="14.2">l</w>’<w n="14.3">Amour</w>, <w n="14.4">pour</w> <w n="14.5">voile</w>, <w n="14.6">a</w> <w n="14.7">cette</w> <w n="14.8">fumée</w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1868">1868</date>.
					</dateline>
				</closer>
			</div></body></text></TEI>