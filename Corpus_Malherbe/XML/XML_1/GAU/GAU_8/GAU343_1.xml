<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES NOUVELLES, POÉSIES INÉDITES ET POÉSIES POSTHUMES</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2661 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_8</idno>

				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Second</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://gutenberg.org/files/45886/45886-h/45886-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Second</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1890">1890</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1872">1831-1872</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>

			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU343">
				<head type="main">SONNET</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">J</w>’<w n="1.2">aimais</w> <w n="1.3">autrefois</w> <w n="1.4">la</w> <w n="1.5">forme</w> <w n="1.6">païenne</w> ;</l>
					<l n="2" num="1.2"><w n="2.1">Je</w> <w n="2.2">m</w>’<w n="2.3">étais</w> <w n="2.4">créé</w>, <w n="2.5">fou</w> <w n="2.6">d</w>’<w n="2.7">antiquité</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Un</w> <w n="3.2">blanc</w> <w n="3.3">idéal</w> <w n="3.4">de</w> <w n="3.5">marbre</w> <w n="3.6">sculpté</w></l>
					<l n="4" num="1.4"><w n="4.1">D</w>’<w n="4.2">hétaïre</w> <w n="4.3">grecque</w> <w n="4.4">ou</w> <w n="4.5">milésienne</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Maintenant</w> <w n="5.2">j</w>’<w n="5.3">adore</w> <w n="5.4">une</w> <w n="5.5">Italienne</w>,</l>
					<l n="6" num="2.2"><w n="6.1">Un</w> <w n="6.2">type</w> <w n="6.3">accompli</w> <w n="6.4">de</w> <w n="6.5">modernité</w>,</l>
					<l n="7" num="2.3"><w n="7.1">Qui</w> <w n="7.2">met</w> <w n="7.3">des</w> <w n="7.4">gilets</w>, <w n="7.5">fume</w> <w n="7.6">et</w> <w n="7.7">prend</w> <w n="7.8">du</w> <w n="7.9">thé</w>,</l>
					<l n="8" num="2.4"><w n="8.1">Et</w> <w n="8.2">qu</w>’<w n="8.3">on</w> <w n="8.4">croit</w> <w n="8.5">Anglaise</w> <w n="8.6">ou</w> <w n="8.7">Parisienne</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">L</w>’<w n="9.2">amour</w>, <w n="9.3">de</w> <w n="9.4">mon</w> <w n="9.5">marbre</w> <w n="9.6">a</w> <w n="9.7">fait</w> <w n="9.8">un</w> <w n="9.9">pastel</w>,</l>
					<l n="10" num="3.2"><w n="10.1">Les</w> <w n="10.2">yeux</w> <w n="10.3">blancs</w> <w n="10.4">ont</w> <w n="10.5">pris</w> <w n="10.6">des</w> <w n="10.7">tons</w> <w n="10.8">de</w> <w n="10.9">turquoise</w>,</l>
					<l n="11" num="3.3"><w n="11.1">La</w> <w n="11.2">lèvre</w> <w n="11.3">a</w> <w n="11.4">rougi</w> <w n="11.5">comme</w> <w n="11.6">une</w> <w n="11.7">framboise</w>,</l>
				</lg>
				<lg n="4">
					<l n="12" num="4.1"><w n="12.1">Et</w> <w n="12.2">mon</w> <w n="12.3">rêve</w> <w n="12.4">grec</w> <w n="12.5">dans</w> <w n="12.6">l</w>’<w n="12.7">or</w> <w n="12.8">d</w>’<w n="12.9">un</w> <w n="12.10">cartel</w>,</l>
					<l n="13" num="4.2"><w n="13.1">Ressemble</w> <w n="13.2">aux</w> <w n="13.3">portraits</w> <w n="13.4">de</w> <w n="13.5">rose</w> <w n="13.6">et</w> <w n="13.7">de</w> <w n="13.8">plâtre</w></l>
					<l n="14" num="4.3"><w n="14.1">Où</w> <w n="14.2">la</w> <w n="14.3">Rosalba</w> <w n="14.4">met</w> <w n="14.5">sa</w> <w n="14.6">fleur</w> <w n="14.7">bleuâtre</w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1870">1870</date>.
					</dateline>
				</closer>
			</div></body></text></TEI>