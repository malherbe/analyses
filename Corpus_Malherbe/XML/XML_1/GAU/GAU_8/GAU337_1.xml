<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES NOUVELLES, POÉSIES INÉDITES ET POÉSIES POSTHUMES</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2661 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_8</idno>

				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Second</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://gutenberg.org/files/45886/45886-h/45886-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Second</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1890">1890</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1872">1831-1872</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>

			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">UN DOUZAIN DE SONNETS</head><div type="poem" key="GAU337">
					<head type="main">SONNET XI</head>
					<head type="sub_1">LA MÉLODIE ET L’ACCOMPAGNEMENT</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">La</w> <w n="1.2">beauté</w>, <w n="1.3">dans</w> <w n="1.4">la</w> <w n="1.5">femme</w>, <w n="1.6">est</w> <w n="1.7">une</w> <w n="1.8">mélodie</w></l>
						<l n="2" num="1.2"><w n="2.1">Dont</w> <w n="2.2">la</w> <w n="2.3">toilette</w> <w n="2.4">n</w>’<w n="2.5">est</w> <w n="2.6">que</w> <w n="2.7">l</w>’<w n="2.8">accompagnement</w>.</l>
						<l n="3" num="1.3"><w n="3.1">Vous</w> <w n="3.2">avez</w> <w n="3.3">la</w> <w n="3.4">beauté</w>. — <w n="3.5">Sur</w> <w n="3.6">ce</w> <w n="3.7">motif</w> <w n="3.8">charmant</w>,</l>
						<l n="4" num="1.4"><w n="4.1">A</w> <w n="4.2">chercher</w> <w n="4.3">des</w> <w n="4.4">accords</w> <w n="4.5">votre</w> <w n="4.6">goût</w> <w n="4.7">s</w>’<w n="4.8">étudie</w> ;</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Tantôt</w> <w n="5.2">c</w>’<w n="5.3">est</w> <w n="5.4">un</w> <w n="5.5">corsage</w> <w n="5.6">à</w> <w n="5.7">la</w> <w n="5.8">coupe</w> <w n="5.9">hardie</w></l>
						<l n="6" num="2.2"><w n="6.1">Qui</w> <w n="6.2">s</w>’<w n="6.3">applique</w> <w n="6.4">au</w> <w n="6.5">contour</w>, <w n="6.6">comme</w> <w n="6.7">un</w> <w n="6.8">baiser</w> <w n="6.9">d</w>’<w n="6.10">amant</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Tantôt</w> <w n="7.2">une</w> <w n="7.3">dentelle</w> <w n="7.4">au</w> <w n="7.5">feston</w> <w n="7.6">écumant</w>,</l>
						<l n="8" num="2.4"><w n="8.1">Une</w> <w n="8.2">fleur</w>, <w n="8.3">un</w> <w n="8.4">bijou</w>, <w n="8.5">qu</w>’<w n="8.6">un</w> <w n="8.7">reflet</w> <w n="8.8">incendie</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">La</w> <w n="9.2">gaze</w> <w n="9.3">et</w> <w n="9.4">le</w> <w n="9.5">satin</w> <w n="9.6">ont</w> <w n="9.7">des</w> <w n="9.8">soirs</w> <w n="9.9">triomphants</w> ;</l>
						<l n="10" num="3.2"><w n="10.1">D</w>’<w n="10.2">autres</w> <w n="10.3">fois</w> <w n="10.4">une</w> <w n="10.5">robe</w>, <w n="10.6">avec</w> <w n="10.7">deux</w> <w n="10.8">plis</w> <w n="10.9">de</w> <w n="10.10">moire</w>,</l>
						<l n="11" num="3.3"><w n="11.1">Aux</w> <w n="11.2">épaules</w> <w n="11.3">vous</w> <w n="11.4">met</w> <w n="11.5">deux</w> <w n="11.6">ailes</w> <w n="11.7">de</w> <w n="11.8">victoire</w>.</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1"><w n="12.1">Mais</w> <w n="12.2">de</w> <w n="12.3">tous</w> <w n="12.4">ces</w> <w n="12.5">atours</w>, <w n="12.6">ajustés</w> <w n="12.7">ou</w> <w n="12.8">bouffants</w>,</l>
						<l n="13" num="4.2"><w n="13.1">Orchestre</w> <w n="13.2">accompagnant</w> <w n="13.3">votre</w> <w n="13.4">grâce</w> <w n="13.5">suprême</w>,</l>
						<l n="14" num="4.3"><w n="14.1">Le</w> <w n="14.2">cœur</w>, <w n="14.3">comme</w> <w n="14.4">d</w>’<w n="14.5">un</w> <w n="14.6">air</w>, <w n="14.7">ne</w> <w n="14.8">relient</w> <w n="14.9">que</w> <w n="14.10">le</w> <w n="14.11">thème</w> !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1869">23 avril 1869</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>