<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES NOUVELLES, POÉSIES INÉDITES ET POÉSIES POSTHUMES</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2661 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_8</idno>

				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Second</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://gutenberg.org/files/45886/45886-h/45886-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Second</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1890">1890</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1872">1831-1872</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>

			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU319">
				<head type="main">A INGRES</head>
				<head type="form">SONNET</head>
				<head type="sub_2">(En réponse à l’envoi d’un fragment de l’<hi rend="ital">Apothéose d’Homère</hi>)</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Du</w> <w n="1.2">plafond</w> <w n="1.3">où</w>, <w n="1.4">les</w> <w n="1.5">pieds</w> <w n="1.6">sur</w> <w n="1.7">le</w> <w n="1.8">blanc</w> <w n="1.9">escabeau</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Trône</w> <w n="2.2">Homère</w>, <w n="2.3">au</w> <w n="2.4">milieu</w> <w n="2.5">de</w> <w n="2.6">l</w>’<w n="2.7">immortelle</w> <w n="2.8">foule</w></l>
					<l n="3" num="1.3"><w n="3.1">Dont</w> <w n="3.2">le</w> <w n="3.3">chœur</w> <w n="3.4">dans</w> <w n="3.5">l</w>’<w n="3.6">azur</w> <w n="3.7">s</w>’<w n="3.8">étage</w> <w n="3.9">et</w> <w n="3.10">se</w> <w n="3.11">déroule</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Pour</w> <w n="4.2">m</w>’<w n="4.3">en</w> <w n="4.4">faire</w> <w n="4.5">présent</w> <w n="4.6">tu</w> <w n="4.7">coupas</w> <w n="4.8">un</w> <w n="4.9">lambeau</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Merci</w>, <w n="5.2">maître</w> <w n="5.3">invaincu</w>, <w n="5.4">prêtre</w> <w n="5.5">fervent</w> <w n="5.6">du</w> <w n="5.7">beau</w>,</l>
					<l n="6" num="2.2"><w n="6.1">Qui</w> <w n="6.2">de</w> <w n="6.3">la</w> <w n="6.4">forme</w> <w n="6.5">pure</w> <w n="6.6">as</w> <w n="6.7">conservé</w> <w n="6.8">le</w> <w n="6.9">moule</w>,</l>
					<l n="7" num="2.3"><w n="7.1">Et</w> <w n="7.2">seul</w>, <w n="7.3">resté</w> <w n="7.4">debout</w> <w n="7.5">dans</w> <w n="7.6">ce</w> <w n="7.7">siècle</w> <w n="7.8">qui</w> <w n="7.9">croule</w>,</l>
					<l n="8" num="2.4"><w n="8.1">De</w> <w n="8.2">l</w>’<w n="8.3">antique</w> <w n="8.4">idéal</w> <w n="8.5">tiens</w> <w n="8.6">toujours</w> <w n="8.7">le</w> <w n="8.8">flambeau</w> !</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Tes</w> <w n="9.2">nobles</w> <w n="9.3">fils</w>, <w n="9.4">Eschyle</w>, <w n="9.5">Euripide</w> <w n="9.6">et</w> <w n="9.7">Sophocle</w>,</l>
					<l n="10" num="3.2"><w n="10.1">Descendus</w> <w n="10.2">de</w> <w n="10.3">ton</w> <w n="10.4">ciel</w> <w n="10.5">pour</w> <w n="10.6">rayonner</w> <w n="10.7">chez</w> <w n="10.8">moi</w>,</l>
					<l n="11" num="3.3"><w n="11.1">Déposent</w> <w n="11.2">leurs</w> <w n="11.3">lauriers</w> <w n="11.4">et</w> <w n="11.5">leurs</w> <w n="11.6">vers</w> <w n="11.7">sur</w> <w n="11.8">un</w> <w n="11.9">socle</w> ;</l>
				</lg>
				<lg n="4">
					<l n="12" num="4.1"><w n="12.1">Et</w> <w n="12.2">mon</w> <w n="12.3">humble</w> <w n="12.4">logis</w>, <w n="12.5">devenu</w>, <w n="12.6">grâce</w> <w n="12.7">à</w> <w n="12.8">toi</w>,</l>
					<l n="13" num="4.2"><w n="13.1">Riche</w> <w n="13.2">comme</w> <w n="13.3">un</w> <w n="13.4">palais</w> <w n="13.5">et</w> <w n="13.6">sacré</w> <w n="13.7">comme</w> <w n="13.8">un</w> <w n="13.9">temple</w>,</l>
					<l n="14" num="4.3"><w n="14.1">Pour</w> <w n="14.2">ces</w> <w n="14.3">hôtes</w> <w n="14.4">divins</w> <w n="14.5">est</w> <w n="14.6">à</w> <w n="14.7">peine</w> <w n="14.8">assez</w> <w n="14.9">ample</w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1866">1866</date>.
					</dateline>
				</closer>
			</div></body></text></TEI>