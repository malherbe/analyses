<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES NOUVELLES, POÉSIES INÉDITES ET POÉSIES POSTHUMES</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2661 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_8</idno>

				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Second</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://gutenberg.org/files/45886/45886-h/45886-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Second</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1890">1890</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1872">1831-1872</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>

			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">APPENDICE</head><div type="poem" key="GAU359">
					<head type="number">III</head>
					<head type="main">QUATRAINS</head>
					<div type="section" n="1">
						<head type="number">1</head>
						<head type="sub">Improvisé sur un portrait</head>
						<head type="sub">DE M<hi rend="sup">lle</hi> SIONA-LÉVY</head>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">Enfant</w>, <w n="1.2">doublement</w> <w n="1.3">applaudie</w>,</l>
							<l n="2" num="1.2"><w n="2.1">Tu</w> <w n="2.2">chantes</w> <w n="2.3">et</w> <w n="2.4">tu</w> <w n="2.5">fais</w> <w n="2.6">des</w> <w n="2.7">vers</w> ;</l>
							<l n="3" num="1.3"><w n="3.1">Et</w> <w n="3.2">ton</w> <w n="3.3">masque</w> <w n="3.4">de</w> <w n="3.5">tragédie</w></l>
							<l n="4" num="1.4"><w n="4.1">Est</w> <w n="4.2">couronné</w> <w n="4.3">de</w> <w n="4.4">lauriers</w> <w n="4.5">verts</w>.</l>
						</lg>
						<closer>
							<dateline>
								<date when="1851">1851</date>.
							</dateline>
						</closer>
					</div>
					<div type="section" n="2">
						<head type="number">2</head>
						<head type="sub">Improvisé sur un portrait</head>
						<head type="sub">DE M<hi rend="sup">me</hi> MADELEINE BROHAN</head>
						<lg n="1">
							<l n="5" num="1.1"><w n="5.1">Type</w> <w n="5.2">charmant</w> <w n="5.3">et</w> <w n="5.4">pur</w> <w n="5.5">dont</w> <w n="5.6">le</w> <w n="5.7">ciel</w> <w n="5.8">est</w> <w n="5.9">avare</w>,</l>
							<l n="6" num="1.2"><w n="6.1">Et</w> <w n="6.2">que</w> <w n="6.3">d</w>’<w n="6.4">un</w> <w n="6.5">fin</w> <w n="6.6">crayon</w> <w n="6.7">l</w>’<w n="6.8">artiste</w> <w n="6.9">copia</w>,</l>
							<l n="7" num="1.3"><w n="7.1">Scribe</w> <w n="7.2">salue</w> <w n="7.3">en</w> <w n="7.4">vous</w> <w n="7.5">sa</w> <w n="7.6">reine</w> <w n="7.7">de</w> <w n="7.8">Navarre</w>,</l>
							<l n="8" num="1.4"><w n="8.1">Musset</w> <w n="8.2">sa</w> <w n="8.3">Marianne</w>, <w n="8.4">et</w> <w n="8.5">Belloy</w> <w n="8.6">sa</w> <w n="8.7">Pia</w>.</l>
						</lg>
						<closer>
							<dateline>
								<date when="1857">1857</date>.
							</dateline>
						</closer>
					</div>
					<div type="section" n="3">
						<head type="number">3</head>
						<head type="sub">Improvisé et placé en tête d’un exemplaire</head>
						<head type="sub">de <hi rend="ital">Émaux et Camées</hi></head>
						<head type="sub">A CLAUDIUS POPELIN, MAÎTRE ÉMAILLEUR</head>
						<lg n="1">
							<l n="9" num="1.1"><w n="9.1">Ce</w> <w n="9.2">livre</w> <w n="9.3">où</w> <w n="9.4">j</w>’<w n="9.5">ai</w> <w n="9.6">mis</w> <w n="9.7">des</w> <hi rend="ital"><w n="9.8">Camées</w></hi></l>
							<l n="10" num="1.2"><w n="10.1">Sculptés</w> <w n="10.2">dans</w> <w n="10.3">l</w>’<w n="10.4">agathe</w> <w n="10.5">des</w> <w n="10.6">mots</w>,</l>
							<l n="11" num="1.3"><w n="11.1">Pour</w> <w n="11.2">voir</w> <w n="11.3">ses</w> <w n="11.4">pages</w> <w n="11.5">acclamées</w></l>
							<l n="12" num="1.4"><w n="12.1">Eût</w> <w n="12.2">eu</w> <w n="12.3">besoin</w> <w n="12.4">de</w> <w n="12.5">tes</w> <hi rend="ital"><w n="12.6">Émaux</w></hi> !</l>
						</lg>
						<closer>
							<dateline>
								<date when="1863">Août 1863</date>.
							</dateline>
						</closer>
					</div>
					<div type="section" n="4">
						<head type="number">4</head>
						<head type="sub">Improvisé</head>
						<head type="sub">SUR UNE ROBE ROSE A POIS NOIRS</head>
						<lg n="1">
							<l n="13" num="1.1"><w n="13.1">Dans</w> <w n="13.2">le</w> <w n="13.3">ciel</w> <w n="13.4">l</w>’<w n="13.5">étoile</w> <w n="13.6">dorée</w></l>
							<l n="14" num="1.2"><w n="14.1">Ne</w> <w n="14.2">luit</w> <w n="14.3">que</w> <w n="14.4">par</w> <w n="14.5">l</w>’<w n="14.6">ombre</w> <w n="14.7">du</w> <w n="14.8">soir</w> ;</l>
							<l n="15" num="1.3"><w n="15.1">Ta</w> <w n="15.2">robe</w>, <w n="15.3">de</w> <w n="15.4">rose</w> <w n="15.5">éclairée</w>,</l>
							<l n="16" num="1.4"><w n="16.1">Change</w> <w n="16.2">l</w>’<w n="16.3">étoile</w> <w n="16.4">en</w> <w n="16.5">astre</w> <w n="16.6">noir</w> !</l>
						</lg>
					</div>
					<div type="section" n="5">
						<head type="number">5</head>
						<head type="sub">AU VICOMTE DE S. L.</head>
						<lg n="1">
							<l n="17" num="1.1"><w n="17.1">Moderne</w> <w n="17.2">est</w> <w n="17.3">le</w> <w n="17.4">palais</w>, <w n="17.5">mais</w> <w n="17.6">le</w> <w n="17.7">blason</w> <w n="17.8">ancien</w></l>
							<l n="18" num="1.2"><w n="18.1">Peint</w> <w n="18.2">par</w> <w n="18.3">Van</w> <w n="18.4">Eyck</w> <w n="18.5">au</w> <w n="18.6">coin</w> <w n="18.7">des</w> <w n="18.8">portraits</w> <w n="18.9">de</w> <w n="18.10">famille</w></l>
							<l n="19" num="1.3"><w n="19.1">Rangés</w> <w n="19.2">en</w> <w n="19.3">ex</w>-<w n="19.4">voto</w> <w n="19.5">sur</w> <w n="19.6">le</w> <w n="19.7">vieil</w> <w n="19.8">or</w> <w n="19.9">qui</w> <w n="19.10">brille</w>,</l>
							<l n="20" num="1.4"><w n="20.1">Le</w> <w n="20.2">jeune</w> <w n="20.3">hôte</w> <w n="20.4">du</w> <w n="20.5">lieu</w> <w n="20.6">le</w> <w n="20.7">revendique</w> <w n="20.8">sien</w>.</l>
						</lg>
						<closer>
							<dateline>
								<date when="1872">Octobre 1872</date>.
							</dateline>
						</closer>
					</div>
				</div></body></text></TEI>