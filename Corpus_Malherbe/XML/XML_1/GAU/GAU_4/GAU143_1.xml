<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2184 vers2184 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Premier</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/files/44180/44180-h/44180-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Premier</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1833">1833</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Quelques erreurs orthographiques ont été repérées avec le correcteur orthogrphique Gedit.</p>
				<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU143">
				<head type="main">MARIA</head>
				<opener>
					<epigraph>
						<cit>
							<quote>
									 … meæ puellæ <lb></lb>
								Flendo turgiduli rubent ocelli.
							</quote>
							<bibl>
								<name>V. Catullus.</name>
							</bibl>
						</cit>
						<cit>
							<quote>
								Ne pleure pas…
							</quote>
							<bibl>
								<name>Dovalle.</name>
							</bibl>
						</cit>
					</epigraph>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">De</w> <w n="1.2">tes</w> <w n="1.3">longs</w> <w n="1.4">cils</w> <w n="1.5">de</w> <w n="1.6">jais</w> <w n="1.7">que</w> <w n="1.8">ta</w> <w n="1.9">main</w> <w n="1.10">blanche</w> <w n="1.11">essuie</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Comme</w> <w n="2.2">des</w> <w n="2.3">gouttes</w> <w n="2.4">d</w>’<w n="2.5">eau</w> <w n="2.6">d</w>’<w n="2.7">un</w> <w n="2.8">arbre</w> <w n="2.9">après</w> <w n="2.10">la</w> <w n="2.11">pluie</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Ou</w> <w n="3.2">comme</w> <w n="3.3">la</w> <w n="3.4">rosée</w>, <w n="3.5">au</w> <w n="3.6">point</w> <w n="3.7">du</w> <w n="3.8">jour</w>, <w n="3.9">des</w> <w n="3.10">fleurs</w></l>
					<l n="4" num="1.4"><w n="4.1">Qu</w>’<w n="4.2">un</w> <w n="4.3">pied</w> <w n="4.4">inattentif</w> <w n="4.5">froisse</w>, <w n="4.6">j</w>’<w n="4.7">ai</w> <w n="4.8">vu</w> <w n="4.9">des</w> <w n="4.10">pleurs</w></l>
					<l n="5" num="1.5"><w n="5.1">Tomber</w> <w n="5.2">et</w> <w n="5.3">ruisseler</w> <w n="5.4">en</w> <w n="5.5">perles</w> <w n="5.6">sur</w> <w n="5.7">ta</w> <w n="5.8">joue</w> :</l>
					<l n="6" num="1.6"><w n="6.1">En</w> <w n="6.2">vain</w> <w n="6.3">de</w> <w n="6.4">la</w> <w n="6.5">gaîté</w> <w n="6.6">l</w>’<w n="6.7">éclair</w> <w n="6.8">à</w> <w n="6.9">présent</w> <w n="6.10">joue</w></l>
					<l n="7" num="1.7"><w n="7.1">Dans</w> <w n="7.2">tes</w> <w n="7.3">yeux</w> <w n="7.4">bruns</w> ; <w n="7.5">en</w> <w n="7.6">vain</w> <w n="7.7">ta</w> <w n="7.8">bouche</w> <w n="7.9">me</w> <w n="7.10">sourit</w> ;</l>
					<l n="8" num="1.8"><w n="8.1">D</w>’<w n="8.2">inquiètes</w> <w n="8.3">terreurs</w> <w n="8.4">agitent</w> <w n="8.5">mon</w> <w n="8.6">esprit</w>.</l>
					<l n="9" num="1.9"><w n="9.1">Qu</w>’<w n="9.2">avais</w>-<w n="9.3">tu</w>, <w n="9.4">Maria</w>, <w n="9.5">toi</w>, <w n="9.6">rieuse</w> <w n="9.7">et</w> <w n="9.8">folâtre</w>,</l>
					<l n="10" num="1.10"><w n="10.1">Toi</w>, <w n="10.2">de</w> <w n="10.3">plaisirs</w> <w n="10.4">bruyants</w> <w n="10.5">et</w> <w n="10.6">de</w> <w n="10.7">danse</w> <w n="10.8">idolâtre</w>,</l>
					<l n="11" num="1.11"><w n="11.1">Le</w> <w n="11.2">soir</w>, <w n="11.3">quand</w> <w n="11.4">le</w> <w n="11.5">soleil</w> <w n="11.6">incline</w> <w n="11.7">à</w> <w n="11.8">l</w>’<w n="11.9">horizon</w>,</l>
					<l n="12" num="1.12"><w n="12.1">La</w> <w n="12.2">première</w> <w n="12.3">à</w> <w n="12.4">fouler</w> <w n="12.5">l</w>’<w n="12.6">émail</w> <w n="12.7">vert</w> <w n="12.8">du</w> <w n="12.9">gazon</w>,</l>
					<l n="13" num="1.13"><w n="13.1">La</w> <w n="13.2">première</w> <w n="13.3">à</w> <w n="13.4">poursuivre</w> <w n="13.5">en</w> <w n="13.6">sa</w> <w n="13.7">rapide</w> <w n="13.8">course</w></l>
					<l n="14" num="1.14"><w n="14.1">La</w> <w n="14.2">demoiselle</w> <w n="14.3">bleue</w> <w n="14.4">aux</w> <w n="14.5">bords</w> <w n="14.6">frais</w> <w n="14.7">de</w> <w n="14.8">la</w> <w n="14.9">source</w>,</l>
					<l n="15" num="1.15"><w n="15.1">A</w> <w n="15.2">chanter</w> <w n="15.3">des</w> <w n="15.4">chansons</w>, <w n="15.5">à</w> <w n="15.6">reprendre</w> <w n="15.7">un</w> <w n="15.8">refrain</w> ?</l>
					<l n="16" num="1.16"><w n="16.1">Toi</w> <w n="16.2">qui</w> <w n="16.3">n</w>’<w n="16.4">as</w> <w n="16.5">jamais</w> <w n="16.6">su</w> <w n="16.7">ce</w> <w n="16.8">qu</w>’<w n="16.9">était</w> <w n="16.10">un</w> <w n="16.11">chagrin</w>,</l>
					<l n="17" num="1.17"><w n="17.1">A</w> <w n="17.2">l</w>’<w n="17.3">écart</w> <w n="17.4">tu</w> <w n="17.5">pleurais</w>. <w n="17.6">Réponds</w>-<w n="17.7">moi</w>, <w n="17.8">quel</w> <w n="17.9">orage</w></l>
					<l n="18" num="1.18"><w n="18.1">Avait</w> <w n="18.2">terni</w> <w n="18.3">l</w>’<w n="18.4">éclat</w> <w n="18.5">de</w> <w n="18.6">ton</w> <w n="18.7">ciel</w> <w n="18.8">sans</w> <w n="18.9">nuage</w> ?</l>
					<l n="19" num="1.19"><w n="19.1">Ton</w> <w n="19.2">passereau</w> <w n="19.3">chéri</w> <w n="19.4">bat</w> <w n="19.5">de</w> <w n="19.6">l</w>’<w n="19.7">aile</w>, <w n="19.8">joyeux</w>,</l>
					<l n="20" num="1.20"><w n="20.1">Les</w> <w n="20.2">barreaux</w> <w n="20.3">de</w> <w n="20.4">sa</w> <w n="20.5">cage</w>, <w n="20.6">et</w> <w n="20.7">sur</w> <w n="20.8">son</w> <w n="20.9">lit</w> <w n="20.10">soyeux</w></l>
					<l n="21" num="1.21"><w n="21.1">Ton</w> <w n="21.2">jeune</w> <w n="21.3">épagneul</w> <w n="21.4">dort</w>, <w n="21.5">tout</w> <w n="21.6">va</w> <w n="21.7">bien</w>, <w n="21.8">et</w> <w n="21.9">tes</w> <w n="21.10">roses</w></l>
					<l n="22" num="1.22"><w n="22.1">Répandent</w> <w n="22.2">leurs</w> <w n="22.3">parfums</w>, <w n="22.4">heureusement</w> <w n="22.5">écloses</w>.</l>
					<l n="23" num="1.23"><w n="23.1">Qu</w>’<w n="23.2">avais</w>-<w n="23.3">tu</w> <w n="23.4">donc</w>, <w n="23.5">enfant</w> ? <w n="23.6">quel</w> <w n="23.7">malheur</w> <w n="23.8">imprévu</w></l>
					<l n="24" num="1.24"><w n="24.1">Te</w> <w n="24.2">faisait</w> <w n="24.3">triste</w> ? — <w n="24.4">Hier</w> <w n="24.5">je</w> <w n="24.6">ne</w> <w n="24.7">t</w>’<w n="24.8">avais</w> <w n="24.9">pas</w> <w n="24.10">vu</w>.</l>
				</lg>
			</div></body></text></TEI>