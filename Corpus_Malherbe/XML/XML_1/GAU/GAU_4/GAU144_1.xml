<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2184 vers2184 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Premier</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/files/44180/44180-h/44180-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Premier</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1833">1833</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Quelques erreurs orthographiques ont été repérées avec le correcteur orthogrphique Gedit.</p>
				<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU144">
				<head type="main">A MON AMI EUGÈNE DE N***</head>
				<opener>
					<epigraph>
						<cit>
							<quote>
								Les parfums les plus doux et les plus belles fleurs <lb></lb>
								Perdoient en un instant leurs charmantes odeurs ; <lb></lb>
								Tous ces mets savoureux dont je chargeois ma table <lb></lb>
								Ne m’ont jamais offert qu’un plaisir peu durable, <lb></lb>
								Oublié le jour même et suivi de regrets. <lb></lb>
								Mais de ces jours heureux, Xanthus, et de ces veilles <lb></lb>
								Où de savans discours ont charmé mes oreilles <lb></lb>
								Il m’en reste des fruits qui ne mourront jamais.
							</quote>
							<bibl>
								<hi rend="ital">Callimaque, traduction de La Porte Duteil.</hi>
							</bibl>
						</cit>
						<cit>
							<quote>
								Vous voyez bien que j’ai mille choses à dire.
							</quote>
							<bibl>
								<hi rend="ital">Hernani.</hi>
							</bibl>
						</cit>
					</epigraph>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Ne</w> <w n="1.2">t</w>’<w n="1.3">en</w> <w n="1.4">va</w> <w n="1.5">pas</w>, <w n="1.6">Eugène</w>, <w n="1.7">il</w> <w n="1.8">n</w>’<w n="1.9">est</w> <w n="1.10">pas</w> <w n="1.11">tard</w> ; <w n="1.12">la</w> <w n="1.13">lune</w></l>
					<l n="2" num="1.2"><w n="2.1">A</w> <w n="2.2">l</w>’<w n="2.3">angle</w> <w n="2.4">du</w> <w n="2.5">carreau</w> <w n="2.6">sur</w> <w n="2.7">l</w>’<w n="2.8">atmosphère</w> <w n="2.9">brune</w></l>
					<l n="3" num="1.3"><w n="3.1">N</w>’<w n="3.2">a</w> <w n="3.3">pas</w> <w n="3.4">encor</w> <w n="3.5">paru</w> : <w n="3.6">nous</w> <w n="3.7">causerons</w> <w n="3.8">un</w> <w n="3.9">peu</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Car</w> <w n="4.2">causer</w> <w n="4.3">est</w> <w n="4.4">bien</w> <w n="4.5">doux</w> <w n="4.6">le</w> <w n="4.7">soir</w>, <w n="4.8">auprès</w> <w n="4.9">du</w> <w n="4.10">feu</w>,</l>
					<l n="5" num="1.5"><w n="5.1">Lorsque</w> <w n="5.2">tout</w> <w n="5.3">est</w> <w n="5.4">tranquille</w> <w n="5.5">et</w> <w n="5.6">qu</w>’<w n="5.7">on</w> <w n="5.8">entend</w> <w n="5.9">à</w> <w n="5.10">peine</w></l>
					<l n="6" num="1.6"><w n="6.1">Entre</w> <w n="6.2">les</w> <w n="6.3">arbres</w> <w n="6.4">nus</w> <w n="6.5">glisser</w> <w n="6.6">la</w> <w n="6.7">froide</w> <w n="6.8">haleine</w></l>
					<l n="7" num="1.7"><w n="7.1">De</w> <w n="7.2">la</w> <w n="7.3">brise</w> <w n="7.4">nocturne</w>, <w n="7.5">et</w> <w n="7.6">la</w> <w n="7.7">chauve</w>-<w n="7.8">souris</w></l>
					<l n="8" num="1.8"><w n="8.1">En</w> <w n="8.2">tournoyant</w> <w n="8.3">dans</w> <w n="8.4">l</w>’<w n="8.5">air</w> <w n="8.6">pousser</w> <w n="8.7">de</w> <w n="8.8">faibles</w> <w n="8.9">cris</w>.</l>
					<l n="9" num="1.9"><w n="9.1">Reste</w> ; <w n="9.2">nous</w> <w n="9.3">causerons</w> <w n="9.4">de</w> <w n="9.5">quelque</w> <w n="9.6">jeune</w> <w n="9.7">fille</w>,</l>
					<l n="10" num="1.10"><w n="10.1">Dont</w> <w n="10.2">la</w> <w n="10.3">lèvre</w> <w n="10.4">sourit</w>, <w n="10.5">dont</w> <w n="10.6">la</w> <w n="10.7">prunelle</w> <w n="10.8">brille</w>,</l>
					<l n="11" num="1.11"><w n="11.1">Et</w> <w n="11.2">que</w> <w n="11.3">nous</w> <w n="11.4">avons</w> <w n="11.5">vue</w>, <w n="11.6">en</w> <w n="11.7">promenant</w> <w n="11.8">un</w> <w n="11.9">jour</w>,</l>
					<l n="12" num="1.12"><w n="12.1">Passer</w> <w n="12.2">devant</w> <w n="12.3">nos</w> <w n="12.4">yeux</w> <w n="12.5">comme</w> <w n="12.6">un</w> <w n="12.7">ange</w> <w n="12.8">d</w>’<w n="12.9">amour</w> ;</l>
					<l n="13" num="1.13"><w n="13.1">De</w> <w n="13.2">nos</w> <w n="13.3">auteurs</w> <w n="13.4">chéris</w>, <w n="13.5">Victor</w> <w n="13.6">et</w> <w n="13.7">Sainte</w>-<w n="13.8">Beuve</w>,</l>
					<l n="14" num="1.14"><w n="14.1">Aigles</w> <w n="14.2">audacieux</w>, <w n="14.3">qui</w> <w n="14.4">d</w>’<w n="14.5">une</w> <w n="14.6">route</w> <w n="14.7">neuve</w></l>
					<l n="15" num="1.15"><w n="15.1">Et</w> <w n="15.2">d</w>’<w n="15.3">obstacles</w> <w n="15.4">semée</w> <w n="15.5">ont</w> <w n="15.6">tenté</w> <w n="15.7">les</w> <w n="15.8">hasards</w>,</l>
					<l n="16" num="1.16"><w n="16.1">Malgré</w> <w n="16.2">les</w> <w n="16.3">coups</w> <w n="16.4">de</w> <w n="16.5">bec</w> <w n="16.6">de</w> <w n="16.7">mille</w> <w n="16.8">geais</w> <w n="16.9">criards</w> ;</l>
					<l n="17" num="1.17"><w n="17.1">Et</w> <w n="17.2">d</w>’<w n="17.3">Alfred</w> <w n="17.4">de</w> <w n="17.5">Vigny</w>, <w n="17.6">qui</w> <w n="17.7">d</w>’<w n="17.8">une</w> <w n="17.9">main</w> <w n="17.10">savante</w></l>
					<l n="18" num="1.18"><w n="18.1">Dessina</w> <w n="18.2">de</w> <w n="18.3">Cinq</w>-<w n="18.4">Mars</w> <w n="18.5">la</w> <w n="18.6">figure</w> <w n="18.7">vivante</w> ;</l>
					<l n="19" num="1.19"><w n="19.1">Et</w> <w n="19.2">d</w>’<w n="19.3">Alfred</w> <w n="19.4">de</w> <w n="19.5">Musset</w> <w n="19.6">et</w> <w n="19.7">d</w>’<w n="19.8">Antoni</w> <w n="19.9">Deschamps</w>,</l>
					<l n="20" num="1.20"><w n="20.1">Et</w> <w n="20.2">d</w>’<w n="20.3">eux</w> <w n="20.4">tous</w> <w n="20.5">dont</w> <w n="20.6">la</w> <w n="20.7">voix</w> <w n="20.8">chante</w> <w n="20.9">de</w> <w n="20.10">nouveaux</w> <w n="20.11">chants</w> ;</l>
					<l n="21" num="1.21"><w n="21.1">Des</w> <w n="21.2">vieux</w> <w n="21.3">qu</w>’<w n="21.4">un</w> <w n="21.5">siècle</w> <w n="21.6">ingrat</w> <w n="21.7">en</w> <w n="21.8">s</w>’<w n="21.9">avançant</w> <w n="21.10">oublie</w>,</l>
					<l n="22" num="1.22"><w n="22.1">Guillaume</w> <w n="22.2">de</w> <w n="22.3">Lorris</w>, <w n="22.4">dont</w> <w n="22.5">l</w>’<w n="22.6">œuvre</w> <w n="22.7">inaccomplie</w>,</l>
					<l n="23" num="1.23"><w n="23.1">Poétique</w> <w n="23.2">héritage</w>, <w n="23.3">aux</w> <w n="23.4">mains</w> <w n="23.5">de</w> <w n="23.6">Clopinel</w></l>
					<l n="24" num="1.24"><w n="24.1">Après</w> <w n="24.2">sa</w> <w n="24.3">mort</w> <w n="24.4">passa</w>, <w n="24.5">monument</w> <w n="24.6">éternel</w></l>
					<l n="25" num="1.25"><w n="25.1">De</w> <w n="25.2">la</w> <w n="25.3">langue</w> <w n="25.4">au</w> <w n="25.5">berceau</w>, <w n="25.6">Pierre</w> <w n="25.7">Vidal</w>, <w n="25.8">trouvère</w></l>
					<l n="26" num="1.26"><w n="26.1">Dont</w> <w n="26.2">le</w> <w n="26.3">luth</w> <w n="26.4">tour</w> <w n="26.5">à</w> <w n="26.6">tour</w> <w n="26.7">gracieux</w> <w n="26.8">et</w> <w n="26.9">sévère</w>,</l>
					<l n="27" num="1.27"><w n="27.1">Sous</w> <w n="27.2">les</w> <w n="27.3">plafonds</w> <w n="27.4">ornés</w> <w n="27.5">de</w> <w n="27.6">nobles</w> <w n="27.7">panonceaux</w>,</l>
					<l n="28" num="1.28"><w n="28.1">Dans</w> <w n="28.2">leurs</w> <w n="28.3">fêtes</w> <w n="28.4">charmait</w> <w n="28.5">les</w> <w n="28.6">comtes</w> <w n="28.7">provençaux</w> ;</l>
					<l n="29" num="1.29"><w n="29.1">Peyrols</w> <w n="29.2">l</w>’<w n="29.3">aventurier</w>, <w n="29.4">qui</w> <w n="29.5">rime</w> <w n="29.6">en</w> <w n="29.7">Palestine</w></l>
					<l n="30" num="1.30"><w n="30.1">Quelque</w> <w n="30.2">amoureux</w> <w n="30.3">tenson</w> <w n="30.4">qu</w>’<w n="30.5">à</w> <w n="30.6">sa</w> <w n="30.7">belle</w> <w n="30.8">il</w> <w n="30.9">destine</w>,</l>
					<l n="31" num="1.31"><w n="31.1">Le</w> <w n="31.2">bon</w> <w n="31.3">Alain</w> <w n="31.4">Chartier</w>, <w n="31.5">Rutebeuf</w> <w n="31.6">le</w> <w n="31.7">conteur</w>,</l>
					<l n="32" num="1.32"><w n="32.1">Sire</w> <w n="32.2">Gasse</w>-<w n="32.3">Brulez</w>, <w n="32.4">Habert</w> <w n="32.5">le</w> <w n="32.6">traducteur</w>,</l>
					<l n="33" num="1.33"><w n="33.1">Maître</w> <w n="33.2">Clément</w> <w n="33.3">Marot</w>, <w n="33.4">madame</w> <w n="33.5">Marguerite</w>,</l>
					<l n="34" num="1.34"><w n="34.1">De</w> <w n="34.2">ses</w> <w n="34.3">jolis</w> <w n="34.4">dizains</w> <w n="34.5">la</w> <w n="34.6">muse</w> <w n="34.7">favorite</w> ;</l>
					<l n="35" num="1.35"><w n="35.1">Villon</w>, <w n="35.2">et</w> <w n="35.3">Rabelais</w>, <w n="35.4">cet</w> <w n="35.5">Homère</w> <w n="35.6">moqueur</w>,</l>
					<l n="36" num="1.36"><w n="36.1">Dont</w> <w n="36.2">le</w> <w n="36.3">sarcasme</w>, <w n="36.4">aigu</w> <w n="36.5">comme</w> <w n="36.6">un</w> <w n="36.7">poignard</w>, <w n="36.8">au</w> <w n="36.9">cœur</w></l>
					<l n="37" num="1.37"><w n="37.1">De</w> <w n="37.2">chaque</w> <w n="37.3">vice</w> <w n="37.4">plonge</w>, <w n="37.5">et</w> <w n="37.6">des</w> <w n="37.7">foudres</w> <w n="37.8">du</w> <w n="37.9">pape</w></l>
					<l n="38" num="1.38"><w n="38.1">N</w>’<w n="38.2">ayant</w> <w n="38.3">cure</w>, <w n="38.4">l</w>’<w n="38.5">atteint</w> <w n="38.6">sous</w> <w n="38.7">la</w> <w n="38.8">pourpre</w> <w n="38.9">ou</w> <w n="38.10">la</w> <w n="38.11">chape</w> :</l>
					<l n="39" num="1.39"><w n="39.1">Car</w> <w n="39.2">nous</w> <w n="39.3">aimons</w> <w n="39.4">tous</w> <w n="39.5">deux</w> <w n="39.6">les</w> <w n="39.7">tours</w> <w n="39.8">hardis</w> <w n="39.9">et</w> <w n="39.10">forts</w>,</l>
					<l n="40" num="1.40"><w n="40.1">Mais</w> <w n="40.2">naïfs</w> <w n="40.3">cependant</w> <w n="40.4">et</w> <w n="40.5">placés</w> <w n="40.6">sans</w> <w n="40.7">efforts</w>,</l>
					<l n="41" num="1.41"><w n="41.1">L</w>’<w n="41.2">originalité</w>, <w n="41.3">la</w> <w n="41.4">puissance</w> <w n="41.5">comique</w></l>
					<l n="42" num="1.42"><w n="42.1">Qu</w>’<w n="42.2">on</w> <w n="42.3">trouve</w> <w n="42.4">en</w> <w n="42.5">ces</w> <w n="42.6">bouquins</w> <w n="42.7">à</w> <w n="42.8">couverture</w> <w n="42.9">antique</w>,</l>
					<l n="43" num="1.43"><w n="43.1">Dont</w> <w n="43.2">la</w> <w n="43.3">marge</w> <w n="43.4">a</w> <w n="43.5">jauni</w> <w n="43.6">sous</w> <w n="43.7">les</w> <w n="43.8">doigts</w> <w n="43.9">studieux</w></l>
					<l n="44" num="1.44"><w n="44.1">De</w> <w n="44.2">vingt</w> <w n="44.3">commentateurs</w>, <w n="44.4">nos</w> <w n="44.5">patients</w> <w n="44.6">aïeux</w>.</l>
					<l n="45" num="1.45"><w n="45.1">Quand</w> <w n="45.2">nous</w> <w n="45.3">aurons</w> <w n="45.4">assez</w> <w n="45.5">causé</w> <w n="45.6">littérature</w>,</l>
					<l n="46" num="1.46"><w n="46.1">Nous</w> <w n="46.2">changerons</w> <w n="46.3">de</w> <w n="46.4">texte</w> <w n="46.5">et</w> <w n="46.6">parlerons</w> <w n="46.7">peinture</w> ;</l>
					<l n="47" num="1.47"><w n="47.1">Je</w> <w n="47.2">te</w> <w n="47.3">dirai</w> <w n="47.4">comment</w> <w n="47.5">Rioult</w>, <w n="47.6">mon</w> <w n="47.7">maître</w>, <w n="47.8">fait</w></l>
					<l n="48" num="1.48"><w n="48.1">Un</w> <w n="48.2">tableau</w> <w n="48.3">qui</w>, <w n="48.4">je</w> <w n="48.5">crois</w>, <w n="48.6">sera</w> <w n="48.7">d</w>’<w n="48.8">un</w> <w n="48.9">grand</w> <w n="48.10">effet</w> :</l>
					<l n="49" num="1.49"><w n="49.1">C</w>’<w n="49.2">est</w> <w n="49.3">un</w> <w n="49.4">ogre</w> <w n="49.5">lascif</w> <w n="49.6">qui</w> <w n="49.7">dans</w> <w n="49.8">ses</w> <w n="49.9">bras</w> <w n="49.10">infâmes</w></l>
					<l n="50" num="1.50"><w n="50.1">A</w> <w n="50.2">son</w> <w n="50.3">repaire</w> <w n="50.4">affreux</w> <w n="50.5">porte</w> <w n="50.6">sept</w> <w n="50.7">jeunes</w> <w n="50.8">femmes</w> ;</l>
					<l n="51" num="1.51"><w n="51.1">Renaud</w> <w n="51.2">de</w> <w n="51.3">Montauban</w>, <w n="51.4">illustre</w> <w n="51.5">paladin</w>,</l>
					<l n="52" num="1.52"><w n="52.1">Le</w> <w n="52.2">suit</w> <w n="52.3">l</w>’<w n="52.4">épée</w> <w n="52.5">au</w> <w n="52.6">poing</w> : <w n="52.7">lui</w>, <w n="52.8">d</w>’<w n="52.9">un</w> <w n="52.10">air</w> <w n="52.11">de</w> <w n="52.12">dédain</w>,</l>
					<l n="53" num="1.53"><w n="53.1">Le</w> <w n="53.2">regarde</w> <w n="53.3">d</w>’<w n="53.4">en</w> <w n="53.5">haut</w> ; <w n="53.6">son</w> <w n="53.7">œil</w> <w n="53.8">sanglant</w> <w n="53.9">et</w> <w n="53.10">louche</w>,</l>
					<l n="54" num="1.54"><w n="54.1">Son</w> <w n="54.2">crâne</w> <w n="54.3">chauve</w> <w n="54.4">et</w> <w n="54.5">plat</w>, <w n="54.6">son</w> <w n="54.7">nez</w> <w n="54.8">rouge</w>, <w n="54.9">sa</w> <w n="54.10">bouche</w></l>
					<l n="55" num="1.55"><w n="55.1">Qui</w> <w n="55.2">ricane</w> <w n="55.3">et</w> <w n="55.4">s</w>’<w n="55.5">entr</w>’<w n="55.6">ouvre</w> <w n="55.7">ainsi</w> <w n="55.8">qu</w>’<w n="55.9">un</w> <w n="55.10">gouffre</w> <w n="55.11">noir</w>,</l>
					<l n="56" num="1.56"><w n="56.1">Le</w> <w n="56.2">rendent</w> <w n="56.3">de</w> <w n="56.4">tout</w> <w n="56.5">point</w> <w n="56.6">très</w>-<w n="56.7">singulier</w> <w n="56.8">à</w> <w n="56.9">voir</w>.</l>
					<l n="57" num="1.57"><w n="57.1">Surprises</w> <w n="57.2">dans</w> <w n="57.3">le</w> <w n="57.4">bain</w> <w n="57.5">les</w> <w n="57.6">sept</w> <w n="57.7">femmes</w> <w n="57.8">sont</w> <w n="57.9">nues</w>,</l>
					<l n="58" num="1.58"><w n="58.1">Leurs</w> <w n="58.2">contours</w> <w n="58.3">veloutés</w>, <w n="58.4">leurs</w> <w n="58.5">formes</w> <w n="58.6">ingénues</w></l>
					<l n="59" num="1.59"><w n="59.1">Et</w> <w n="59.2">leur</w> <w n="59.3">coloris</w> <w n="59.4">frais</w> <w n="59.5">comme</w> <w n="59.6">un</w> <w n="59.7">rêve</w> <w n="59.8">au</w> <w n="59.9">printemps</w>,</l>
					<l n="60" num="1.60"><w n="60.1">Leurs</w> <w n="60.2">cheveux</w> <w n="60.3">en</w> <w n="60.4">désordre</w> <w n="60.5">et</w> <w n="60.6">sur</w> <w n="60.7">leurs</w> <w n="60.8">cous</w> <w n="60.9">flottants</w>,</l>
					<l n="61" num="1.61"><w n="61.1">La</w> <w n="61.2">terreur</w> <w n="61.3">qui</w> <w n="61.4">se</w> <w n="61.5">peint</w> <w n="61.6">dans</w> <w n="61.7">leurs</w> <w n="61.8">yeux</w> <w n="61.9">pleins</w> <w n="61.10">de</w> <w n="61.11">larmes</w>,</l>
					<l n="62" num="1.62"><w n="62.1">Me</w> <w n="62.2">paraissent</w> <w n="62.3">vraiment</w> <w n="62.4">admirables</w> ; <w n="62.5">les</w> <w n="62.6">armes</w></l>
					<l n="63" num="1.63"><w n="63.1">Du</w> <w n="63.2">paladin</w> <w n="63.3">Renaud</w>, <w n="63.4">faites</w> <w n="63.5">d</w>’<w n="63.6">acier</w> <w n="63.7">bruni</w></l>
					<l n="64" num="1.64"><w n="64.1">Étoilé</w> <w n="64.2">de</w> <w n="64.3">clous</w> <w n="64.4">d</w>’<w n="64.5">or</w>, <w n="64.6">sont</w> <w n="64.7">du</w> <w n="64.8">plus</w> <w n="64.9">beau</w> <w n="64.10">fini</w> :</l>
					<l n="65" num="1.65"><w n="65.1">Un</w> <w n="65.2">panache</w> <w n="65.3">s</w>’<w n="65.4">agite</w> <w n="65.5">au</w> <w n="65.6">cimier</w> <w n="65.7">de</w> <w n="65.8">son</w> <w n="65.9">casque</w>,</l>
					<l n="66" num="1.66"><w n="66.1">D</w>’<w n="66.2">un</w> <w n="66.3">dessin</w> <w n="66.4">à</w> <w n="66.5">la</w> <w n="66.6">fois</w> <w n="66.7">élégant</w> <w n="66.8">et</w> <w n="66.9">fantasque</w> ;</l>
					<l n="67" num="1.67"><w n="67.1">Sa</w> <w n="67.2">visière</w> <w n="67.3">est</w> <w n="67.4">levée</w>, <w n="67.5">et</w> <w n="67.6">sur</w> <w n="67.7">son</w> <w n="67.8">corselet</w></l>
					<l n="68" num="1.68"><w n="68.1">Un</w> <w n="68.2">rayon</w> <w n="68.3">de</w> <w n="68.4">soleil</w> <w n="68.5">jette</w> <w n="68.6">un</w> <w n="68.7">brillant</w> <w n="68.8">reflet</w>.</l>
					<l n="69" num="1.69"><w n="69.1">Mais</w> <w n="69.2">à</w> <w n="69.3">ce</w> <w n="69.4">tableau</w> <w n="69.5">plein</w> <w n="69.6">d</w>’<w n="69.7">inventions</w> <w n="69.8">heureuses</w></l>
					<l n="70" num="1.70"><w n="70.1">Je</w> <w n="70.2">préfère</w> <w n="70.3">pourtant</w> <w n="70.4">ses</w> <w n="70.5">petites</w> <w n="70.6">baigneuses</w>,</l>
					<l n="71" num="1.71"><w n="71.1">Vrai</w> <w n="71.2">chef</w>-<w n="71.3">d</w>’<w n="71.4">œuvre</w> <w n="71.5">de</w> <w n="71.6">grâce</w> <w n="71.7">et</w> <w n="71.8">de</w> <w n="71.9">naïveté</w>,</l>
					<l n="72" num="1.72"><w n="72.1">Où</w> <w n="72.2">la</w> <w n="72.3">jeunesse</w> <w n="72.4">brille</w> <w n="72.5">avec</w> <w n="72.6">son</w> <w n="72.7">velouté</w>.</l>
					<l n="73" num="1.73"><w n="73.1">Après</w> <w n="73.2">viendront</w> <w n="73.3">en</w> <w n="73.4">foule</w> <w n="73.5">anciens</w> <w n="73.6">peintres</w> <w n="73.7">de</w> <w n="73.8">Rome</w> :</l>
					<l n="74" num="1.74"><w n="74.1">Pérugin</w>, <w n="74.2">Raphaël</w>, <w n="74.3">homme</w> <w n="74.4">au</w>-<w n="74.5">dessus</w> <w n="74.6">de</w> <w n="74.7">l</w>’<w n="74.8">homme</w> ;</l>
					<l n="75" num="1.75"><w n="75.1">De</w> <w n="75.2">Florence</w>, <w n="75.3">de</w> <w n="75.4">Parme</w> <w n="75.5">et</w> <w n="75.6">de</w> <w n="75.7">Venise</w> <w n="75.8">aussi</w>,</l>
					<l n="76" num="1.76"><w n="76.1">Véronèse</w>, <w n="76.2">Titien</w>, <w n="76.3">Léonard</w> <w n="76.4">de</w> <w n="76.5">Vinci</w>,</l>
					<l n="77" num="1.77"><w n="77.1">Michel</w>-<w n="77.2">Ange</w>, <w n="77.3">Annibal</w> <w n="77.4">Carrache</w>, <w n="77.5">le</w> <w n="77.6">Corrége</w></l>
					<l n="78" num="1.78"><w n="78.1">Et</w> <w n="78.2">d</w>’<w n="78.3">autres</w> <w n="78.4">plus</w> <w n="78.5">nombreux</w> <w n="78.6">que</w> <w n="78.7">les</w> <w n="78.8">flocons</w> <w n="78.9">de</w> <w n="78.10">neige</w></l>
					<l n="79" num="1.79"><w n="79.1">Qui</w> <w n="79.2">s</w>’<w n="79.3">entassent</w> <w n="79.4">l</w>’<w n="79.5">hiver</w> <w n="79.6">au</w> <w n="79.7">front</w> <w n="79.8">des</w> <w n="79.9">Apennins</w> ;</l>
					<l n="80" num="1.80"><w n="80.1">D</w>’<w n="80.2">autres</w> <w n="80.3">auprès</w> <w n="80.4">de</w> <w n="80.5">qui</w> <w n="80.6">nous</w> <w n="80.7">sommes</w> <w n="80.8">tous</w> <w n="80.9">des</w> <w n="80.10">nains</w></l>
					<l n="81" num="1.81"><w n="81.1">Et</w> <w n="81.2">dont</w> <w n="81.3">la</w> <w n="81.4">gloire</w> <w n="81.5">immense</w>, <w n="81.6">en</w> <w n="81.7">vieillissant</w> <w n="81.8">doublée</w>,</l>
					<l n="82" num="1.82"><w n="82.1">Fait</w> <w n="82.2">tomber</w> <w n="82.3">les</w> <w n="82.4">crayons</w> <w n="82.5">de</w> <w n="82.6">notre</w> <w n="82.7">main</w> <w n="82.8">troublée</w>.</l>
					<l n="83" num="1.83"><w n="83.1">Puis</w> <w n="83.2">je</w> <w n="83.3">te</w> <w n="83.4">décrirai</w> <w n="83.5">ce</w> <w n="83.6">tableau</w> <w n="83.7">de</w> <w n="83.8">Rembrandt</w></l>
					<l n="84" num="1.84"><w n="84.1">Qui</w> <w n="84.2">me</w> <w n="84.3">fait</w> <w n="84.4">tant</w> <w n="84.5">plaisir</w>, <w n="84.6">et</w> <w n="84.7">mon</w> <w n="84.8">chat</w> <w n="84.9">Childebrand</w></l>
					<l n="85" num="1.85"><w n="85.1">Sur</w> <w n="85.2">mes</w> <w n="85.3">genoux</w> <w n="85.4">posé</w> <w n="85.5">selon</w> <w n="85.6">son</w> <w n="85.7">habitude</w>,</l>
					<l n="86" num="1.86"><w n="86.1">Levant</w> <w n="86.2">vers</w> <w n="86.3">moi</w> <w n="86.4">la</w> <w n="86.5">tête</w> <w n="86.6">avec</w> <w n="86.7">inquiétude</w>,</l>
					<l n="87" num="1.87"><w n="87.1">Suivra</w> <w n="87.2">les</w> <w n="87.3">mouvements</w> <w n="87.4">de</w> <w n="87.5">mon</w> <w n="87.6">doigt</w>, <w n="87.7">qui</w> <w n="87.8">dans</w> <w n="87.9">l</w>’<w n="87.10">air</w></l>
					<l n="88" num="1.88"><w n="88.1">Esquisse</w> <w n="88.2">mon</w> <w n="88.3">récit</w> <w n="88.4">pour</w> <w n="88.5">le</w> <w n="88.6">rendre</w> <w n="88.7">plus</w> <w n="88.8">clair</w> ;</l>
					<l n="89" num="1.89"><w n="89.1">Et</w> <w n="89.2">nous</w> <w n="89.3">aurons</w> <w n="89.4">encor</w> <w n="89.5">mille</w> <w n="89.6">choses</w> <w n="89.7">à</w> <w n="89.8">dire</w></l>
					<l n="90" num="1.90"><w n="90.1">Lorsque</w> <w n="90.2">tout</w> <w n="90.3">sera</w> <w n="90.4">dit</w> : <w n="90.5">projets</w> <w n="90.6">riants</w>, <w n="90.7">délire</w></l>
					<l n="91" num="1.91"><w n="91.1">De</w> <w n="91.2">jeunesse</w>, <w n="91.3">que</w> <w n="91.4">sais</w>-<w n="91.5">je</w> ? <w n="91.6">un</w> <w n="91.7">souvenir</w> <w n="91.8">d</w>’<w n="91.9">hier</w>,</l>
					<l n="92" num="1.92"><w n="92.1">Le</w> <w n="92.2">présent</w>, <w n="92.3">l</w>’<w n="92.4">avenir</w>, <w n="92.5">mes</w> <w n="92.6">chants</w>, <w n="92.7">dont</w> <w n="92.8">je</w> <w n="92.9">suis</w> <w n="92.10">fier</w></l>
					<l n="93" num="1.93"><w n="93.1">Comme</w> <w n="93.2">des</w> <w n="93.3">plus</w> <w n="93.4">beaux</w> <w n="93.5">chants</w> ; <w n="93.6">et</w> <w n="93.7">ces</w> <w n="93.8">vagues</w> <w n="93.9">ébauches</w></l>
					<l n="94" num="1.94"><w n="94.1">De</w> <w n="94.2">poëmes</w> <w n="94.3">à</w> <w n="94.4">faire</w>, <w n="94.5">incomplètes</w> <w n="94.6">et</w> <w n="94.7">gauches</w>,</l>
					<l n="95" num="1.95"><w n="95.1">Où</w> <w n="95.2">les</w> <w n="95.3">regards</w> <w n="95.4">amis</w> <w n="95.5">un</w> <w n="95.6">instant</w> <w n="95.7">arrêtés</w></l>
					<l n="96" num="1.96"><w n="96.1">Cherchent</w> <w n="96.2">à</w> <w n="96.3">pressentir</w> <w n="96.4">de</w> <w n="96.5">futures</w> <w n="96.6">beautés</w>,</l>
					<l n="97" num="1.97"><w n="97.1">Et</w> <w n="97.2">ces</w> <w n="97.3">légers</w> <w n="97.4">dessins</w> <w n="97.5">où</w> <w n="97.6">je</w> <w n="97.7">tâche</w> <w n="97.8">de</w> <w n="97.9">rendre</w></l>
					<l n="98" num="1.98"><w n="98.1">Ce</w> <w n="98.2">que</w> <w n="98.3">je</w> <w n="98.4">ne</w> <w n="98.5">saurais</w> <w n="98.6">faire</w> <w n="98.7">assez</w> <w n="98.8">bien</w> <w n="98.9">comprendre</w></l>
					<l n="99" num="1.99"><w n="99.1">Par</w> <w n="99.2">mes</w> <w n="99.3">vers</w> ; <w n="99.4">mais</w> <w n="99.5">alors</w>, <w n="99.6">Eugène</w>, <w n="99.7">il</w> <w n="99.8">sera</w> <w n="99.9">tard</w>,</l>
					<l n="100" num="1.100"><w n="100.1">Et</w> <w n="100.2">je</w> <w n="100.3">ne</w> <w n="100.4">pourrai</w> <w n="100.5">plus</w> <w n="100.6">reculer</w> <w n="100.7">ton</w> <w n="100.8">départ</w>.</l>
				</lg>
			</div></body></text></TEI>