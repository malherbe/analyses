<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2184 vers2184 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Premier</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/files/44180/44180-h/44180-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Premier</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1833">1833</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Quelques erreurs orthographiques ont été repérées avec le correcteur orthogrphique Gedit.</p>
				<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU149">
				<head type="main">SOLEIL COUCHANT</head>
				<opener>
					<epigraph>
						<cit>
							<quote>
								Notre-Dame, <lb></lb>
								Que c’est beau !
							</quote>
							<bibl>
								<name>Victor Hugo.</name>
							</bibl>
						</cit>
					</epigraph>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">En</w> <w n="1.2">passant</w> <w n="1.3">sur</w> <w n="1.4">le</w> <w n="1.5">pont</w> <w n="1.6">de</w> <w n="1.7">la</w> <w n="1.8">Tournelle</w>, <w n="1.9">un</w> <w n="1.10">soir</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Je</w> <w n="2.2">me</w> <w n="2.3">suis</w> <w n="2.4">arrêté</w> <w n="2.5">quelques</w> <w n="2.6">instants</w> <w n="2.7">pour</w> <w n="2.8">voir</w></l>
					<l n="3" num="1.3"><w n="3.1">Le</w> <w n="3.2">soleil</w> <w n="3.3">se</w> <w n="3.4">coucher</w> <w n="3.5">derrière</w> <w n="3.6">Notre</w>-<w n="3.7">Dame</w>.</l>
					<l n="4" num="1.4"><w n="4.1">Un</w> <w n="4.2">nuage</w> <w n="4.3">splendide</w> <w n="4.4">à</w> <w n="4.5">l</w>’<w n="4.6">horizon</w> <w n="4.7">de</w> <w n="4.8">flamme</w>,</l>
					<l n="5" num="1.5"><w n="5.1">Tel</w> <w n="5.2">qu</w>’<w n="5.3">un</w> <w n="5.4">oiseau</w> <w n="5.5">géant</w> <w n="5.6">qui</w> <w n="5.7">va</w> <w n="5.8">prendre</w> <w n="5.9">l</w>’<w n="5.10">essor</w>,</l>
					<l n="6" num="1.6"><w n="6.1">D</w>’<w n="6.2">un</w> <w n="6.3">bout</w> <w n="6.4">du</w> <w n="6.5">ciel</w> <w n="6.6">à</w> <w n="6.7">l</w>’<w n="6.8">autre</w> <w n="6.9">ouvrait</w> <w n="6.10">ses</w> <w n="6.11">ailes</w> <w n="6.12">d</w>’<w n="6.13">or</w>,</l>
					<l n="7" num="1.7">— <w n="7.1">Et</w> <w n="7.2">c</w>’<w n="7.3">étaient</w> <w n="7.4">des</w> <w n="7.5">clartés</w> <w n="7.6">à</w> <w n="7.7">baisser</w> <w n="7.8">la</w> <w n="7.9">paupière</w>.</l>
					<l n="8" num="1.8"><w n="8.1">Les</w> <w n="8.2">tours</w> <w n="8.3">au</w> <w n="8.4">front</w> <w n="8.5">orné</w> <w n="8.6">de</w> <w n="8.7">dentelles</w> <w n="8.8">de</w> <w n="8.9">pierre</w>,</l>
					<l n="9" num="1.9"><w n="9.1">Le</w> <w n="9.2">drapeau</w> <w n="9.3">que</w> <w n="9.4">le</w> <w n="9.5">vent</w> <w n="9.6">fouette</w>, <w n="9.7">les</w> <w n="9.8">minarets</w></l>
					<l n="10" num="1.10"><w n="10.1">Qui</w> <w n="10.2">s</w>’<w n="10.3">élèvent</w> <w n="10.4">pareils</w> <w n="10.5">aux</w> <w n="10.6">sapins</w> <w n="10.7">des</w> <w n="10.8">forêts</w>,</l>
					<l n="11" num="1.11"><w n="11.1">Les</w> <w n="11.2">pignons</w> <w n="11.3">tailladés</w> <w n="11.4">que</w> <w n="11.5">surmontent</w> <w n="11.6">des</w> <w n="11.7">anges</w></l>
					<l n="12" num="1.12"><w n="12.1">Aux</w> <w n="12.2">corps</w> <w n="12.3">roides</w> <w n="12.4">et</w> <w n="12.5">longs</w>, <w n="12.6">aux</w> <w n="12.7">figures</w> <w n="12.8">étranges</w>,</l>
					<l n="13" num="1.13"><w n="13.1">D</w>’<w n="13.2">un</w> <w n="13.3">fond</w> <w n="13.4">clair</w> <w n="13.5">ressortaient</w> <w n="13.6">en</w> <w n="13.7">noir</w> ; <w n="13.8">l</w>’<w n="13.9">Archevêché</w>,</l>
					<l n="14" num="1.14"><w n="14.1">Comme</w> <w n="14.2">au</w> <w n="14.3">pied</w> <w n="14.4">de</w> <w n="14.5">sa</w> <w n="14.6">mère</w> <w n="14.7">un</w> <w n="14.8">jeune</w> <w n="14.9">enfant</w> <w n="14.10">couché</w>,</l>
					<l n="15" num="1.15"><w n="15.1">Se</w> <w n="15.2">dessinait</w> <w n="15.3">au</w> <w n="15.4">pied</w> <w n="15.5">de</w> <w n="15.6">l</w>’<w n="15.7">église</w>, <w n="15.8">dont</w> <w n="15.9">l</w>’<w n="15.10">ombre</w></l>
					<l n="16" num="1.16"><w n="16.1">S</w>’<w n="16.2">allongeait</w> <w n="16.3">à</w> <w n="16.4">l</w>’<w n="16.5">entour</w> <w n="16.6">mystérieuse</w> <w n="16.7">et</w> <w n="16.8">sombre</w>.</l>
					<l n="17" num="1.17">— <w n="17.1">Plus</w> <w n="17.2">loin</w>, <w n="17.3">un</w> <w n="17.4">rayon</w> <w n="17.5">rouge</w> <w n="17.6">allumait</w> <w n="17.7">les</w> <w n="17.8">carreaux</w></l>
					<l n="18" num="1.18"><w n="18.1">D</w>’<w n="18.2">une</w> <w n="18.3">maison</w> <w n="18.4">du</w> <w n="18.5">quai</w> ; — <w n="18.6">l</w>’<w n="18.7">air</w> <w n="18.8">était</w> <w n="18.9">doux</w> ; <w n="18.10">les</w> <w n="18.11">eaux</w></l>
					<l n="19" num="1.19"><w n="19.1">Se</w> <w n="19.2">plaignaient</w> <w n="19.3">contre</w> <w n="19.4">l</w>’<w n="19.5">arche</w> <w n="19.6">à</w> <w n="19.7">doux</w> <w n="19.8">bruit</w>, <w n="19.9">et</w> <w n="19.10">la</w> <w n="19.11">vague</w></l>
					<l n="20" num="1.20"><w n="20.1">De</w> <w n="20.2">la</w> <w n="20.3">vieille</w> <w n="20.4">cité</w> <w n="20.5">berçait</w> <w n="20.6">l</w>’<w n="20.7">image</w> <w n="20.8">vague</w> ;</l>
					<l n="21" num="1.21"><w n="21.1">Et</w> <w n="21.2">moi</w>, <w n="21.3">je</w> <w n="21.4">regardais</w> <w n="21.5">toujours</w>, <w n="21.6">ne</w> <w n="21.7">songeant</w> <w n="21.8">pas</w></l>
					<l n="22" num="1.22"><w n="22.1">Que</w> <w n="22.2">la</w> <w n="22.3">nuit</w> <w n="22.4">étoilée</w> <w n="22.5">arrivait</w> <w n="22.6">à</w> <w n="22.7">grands</w> <w n="22.8">pas</w>.</l>
				</lg>
			</div></body></text></TEI>