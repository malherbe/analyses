<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2184 vers2184 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Premier</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/files/44180/44180-h/44180-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Premier</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1833">1833</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Quelques erreurs orthographiques ont été repérées avec le correcteur orthogrphique Gedit.</p>
				<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU125">
				<head type="main">SONNET II</head>
				<opener>
					<epigraph>
						<cit>
							<quote>
								Amour tant vous hai servit <lb></lb>
								Senz pecas et senz failhimen, <lb></lb>
								Et vous sabez quant petit <lb></lb>
								Hai avut de jauzimen.
							</quote>
							<bibl>
								<name>Peyrols.</name>
							</bibl>
						</cit>
						<cit>
							<quote>
								Ne sais tu pas que je n’eus onc <lb></lb>
								D’elle plaisir ny un seul bien.
							</quote>
							<bibl>
								<name>Marot.</name>
							</bibl>
						</cit>
					</epigraph>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Ne</w> <w n="1.2">vous</w> <w n="1.3">détournez</w> <w n="1.4">pas</w>, <w n="1.5">car</w> <w n="1.6">ce</w> <w n="1.7">n</w>’<w n="1.8">est</w> <w n="1.9">point</w> <w n="1.10">d</w>’<w n="1.11">amour</w></l>
					<l n="2" num="1.2"><w n="2.1">Que</w> <w n="2.2">je</w> <w n="2.3">veux</w> <w n="2.4">vous</w> <w n="2.5">parler</w> ; <w n="2.6">que</w> <w n="2.7">le</w> <w n="2.8">passé</w>, <w n="2.9">madame</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Soit</w> <w n="3.2">pour</w> <w n="3.3">nous</w> <w n="3.4">comme</w> <w n="3.5">un</w> <w n="3.6">songe</w> <w n="3.7">envolé</w> <w n="3.8">sans</w> <w n="3.9">retour</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Oubliez</w> <w n="4.2">une</w> <w n="4.3">erreur</w> <w n="4.4">que</w> <w n="4.5">moi</w>-<w n="4.6">même</w> <w n="4.7">je</w> <w n="4.8">blâme</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Mais</w> <w n="5.2">vous</w> <w n="5.3">êtes</w> <w n="5.4">si</w> <w n="5.5">belle</w>, <w n="5.6">et</w> <w n="5.7">sous</w> <w n="5.8">le</w> <w n="5.9">fin</w> <w n="5.10">contour</w></l>
					<l n="6" num="2.2"><w n="6.1">De</w> <w n="6.2">vos</w> <w n="6.3">sourcils</w> <w n="6.4">arqués</w> <w n="6.5">luit</w> <w n="6.6">un</w> <w n="6.7">regard</w> <w n="6.8">de</w> <w n="6.9">flamme</w></l>
					<l n="7" num="2.3"><w n="7.1">Si</w> <w n="7.2">perçant</w>, <w n="7.3">qu</w>’<w n="7.4">on</w> <w n="7.5">ne</w> <w n="7.6">peut</w> <w n="7.7">vous</w> <w n="7.8">avoir</w> <w n="7.9">vue</w> <w n="7.10">un</w> <w n="7.11">jour</w></l>
					<l n="8" num="2.4"><w n="8.1">Sans</w> <w n="8.2">porter</w> <w n="8.3">à</w> <w n="8.4">jamais</w> <w n="8.5">votre</w> <w n="8.6">image</w> <w n="8.7">en</w> <w n="8.8">son</w> <w n="8.9">âme</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Moi</w>, <w n="9.2">mes</w> <w n="9.3">traits</w> <w n="9.4">soucieux</w> <w n="9.5">sont</w> <w n="9.6">couverts</w> <w n="9.7">de</w> <w n="9.8">pâleur</w> ;</l>
					<l n="10" num="3.2"><w n="10.1">Car</w>, <w n="10.2">dès</w> <w n="10.3">mes</w> <w n="10.4">premiers</w> <w n="10.5">ans</w> <w n="10.6">souffrant</w> <w n="10.7">et</w> <w n="10.8">solitaire</w>,</l>
					<l n="11" num="3.3"><w n="11.1">Dans</w> <w n="11.2">mon</w> <w n="11.3">cœur</w> <w n="11.4">je</w> <w n="11.5">nourris</w> <w n="11.6">une</w> <w n="11.7">pensée</w> <w n="11.8">austère</w>,</l>
				</lg>
				<lg n="4">
					<l n="12" num="4.1"><w n="12.1">Et</w> <w n="12.2">mon</w> <w n="12.3">front</w> <w n="12.4">avant</w> <w n="12.5">l</w>’<w n="12.6">âge</w> <w n="12.7">a</w> <w n="12.8">perdu</w> <w n="12.9">cette</w> <w n="12.10">fleur</w></l>
					<l n="13" num="4.2"><w n="13.1">Qui</w> <w n="13.2">s</w>’<w n="13.3">entr</w>’<w n="13.4">ouvre</w> <w n="13.5">vermeille</w> <w n="13.6">au</w> <w n="13.7">printemps</w> <w n="13.8">de</w> <w n="13.9">la</w> <w n="13.10">vie</w>,</l>
					<l n="14" num="4.3"><w n="14.1">Et</w> <w n="14.2">qui</w> <w n="14.3">ne</w> <w n="14.4">revient</w> <w n="14.5">plus</w> <w n="14.6">alors</w> <w n="14.7">qu</w>’<w n="14.8">elle</w> <w n="14.9">est</w> <w n="14.10">ravie</w>.</l>
				</lg>
			</div></body></text></TEI>