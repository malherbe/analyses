<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2184 vers2184 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Premier</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/files/44180/44180-h/44180-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Premier</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1833">1833</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Quelques erreurs orthographiques ont été repérées avec le correcteur orthogrphique Gedit.</p>
				<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU126">
				<head type="main">LA BASILIQUE</head>
				<opener>
					<epigraph>
						<cit>
							<quote>
								The pillared arches were over their head <lb></lb>
							And beneath their feet were the bones of the dead.
							</quote>
							<bibl>
								<hi rend="ital">The lay of last minstrel.</hi>
							</bibl>
						</cit>
						<cit>
							<quote>
								On voit des figures de chevaliers à genoux sur <lb></lb>
								un tombeau, les mains jointes… les arcades obscures <lb></lb>
								de l’église couvrent de leurs ombres ceux qui reposent.
							</quote>
							<bibl>
								<name>Göerres</name>
							</bibl>
						</cit>
					</epigraph>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Il</w> <w n="1.2">est</w> <w n="1.3">une</w> <w n="1.4">basilique</w></l>
					<l n="2" num="1.2"><w n="2.1">Aux</w> <w n="2.2">murs</w> <w n="2.3">moussus</w> <w n="2.4">et</w> <w n="2.5">noircis</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Du</w> <w n="3.2">vieux</w> <w n="3.3">temps</w> <w n="3.4">noble</w> <w n="3.5">relique</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Où</w> <w n="4.2">l</w>’<w n="4.3">âme</w> <w n="4.4">mélancolique</w></l>
					<l n="5" num="1.5"><w n="5.1">Flotte</w> <w n="5.2">en</w> <w n="5.3">pensers</w> <w n="5.4">indécis</w>.</l>
				</lg>
				<lg n="2">
					<l n="6" num="2.1"><w n="6.1">Des</w> <w n="6.2">losanges</w> <w n="6.3">de</w> <w n="6.4">plomb</w> <w n="6.5">ceignent</w></l>
					<l n="7" num="2.2"><w n="7.1">Les</w> <w n="7.2">vitraux</w> <w n="7.3">coloriés</w>,</l>
					<l n="8" num="2.3"><w n="8.1">Où</w> <w n="8.2">les</w> <w n="8.3">feux</w> <w n="8.4">du</w> <w n="8.5">soleil</w> <w n="8.6">teignent</w></l>
					<l n="9" num="2.4"><w n="9.1">Les</w> <w n="9.2">reflets</w> <w n="9.3">errants</w> <w n="9.4">qui</w> <w n="9.5">baignent</w></l>
					<l n="10" num="2.5"><w n="10.1">Les</w> <w n="10.2">plafonds</w> <w n="10.3">armoriés</w>.</l>
				</lg>
				<lg n="3">
					<l n="11" num="3.1"><w n="11.1">Cent</w> <w n="11.2">colonnes</w> <w n="11.3">découpées</w></l>
					<l n="12" num="3.2"><w n="12.1">Par</w> <w n="12.2">de</w> <w n="12.3">bizarres</w> <w n="12.4">ciseaux</w>,</l>
					<l n="13" num="3.3"><w n="13.1">Comme</w> <w n="13.2">des</w> <w n="13.3">faisceaux</w> <w n="13.4">d</w>’<w n="13.5">épées</w></l>
					<l n="14" num="3.4"><w n="14.1">Au</w> <w n="14.2">long</w> <w n="14.3">de</w> <w n="14.4">la</w> <w n="14.5">nef</w> <w n="14.6">groupées</w></l>
					<l n="15" num="3.5"><w n="15.1">Portent</w> <w n="15.2">les</w> <w n="15.3">sveltes</w> <w n="15.4">arceaux</w>.</l>
				</lg>
				<lg n="4">
					<l n="16" num="4.1"><w n="16.1">La</w> <w n="16.2">fantastique</w> <w n="16.3">arabesque</w></l>
					<l n="17" num="4.2"><w n="17.1">Courbe</w> <w n="17.2">ses</w> <w n="17.3">légers</w> <w n="17.4">dessins</w></l>
					<l n="18" num="4.3"><w n="18.1">Autour</w> <w n="18.2">du</w> <w n="18.3">trèfle</w> <w n="18.4">moresque</w>,</l>
					<l n="19" num="4.4"><w n="19.1">De</w> <w n="19.2">l</w>’<w n="19.3">arcade</w> <w n="19.4">gigantesque</w></l>
					<l n="20" num="4.5"><w n="20.1">Et</w> <w n="20.2">de</w> <w n="20.3">la</w> <w n="20.4">niche</w> <w n="20.5">des</w> <w n="20.6">saints</w>.</l>
				</lg>
				<lg n="5">
					<l n="21" num="5.1"><w n="21.1">Dans</w> <w n="21.2">leurs</w> <w n="21.3">armes</w> <w n="21.4">féodales</w>,</l>
					<l n="22" num="5.2"><w n="22.1">Vidames</w> <w n="22.2">et</w> <w n="22.3">chevaliers</w>,</l>
					<l n="23" num="5.3"><w n="23.1">Sont</w> <w n="23.2">là</w>, <w n="23.3">couchés</w> <w n="23.4">sur</w> <w n="23.5">les</w> <w n="23.6">dalles</w></l>
					<l n="24" num="5.4"><w n="24.1">Des</w> <w n="24.2">chapelles</w> <w n="24.3">sépulcrales</w>,</l>
					<l n="25" num="5.5"><w n="25.1">Ou</w> <w n="25.2">debout</w> <w n="25.3">près</w> <w n="25.4">des</w> <w n="25.5">piliers</w>.</l>
				</lg>
				<lg n="6">
					<l n="26" num="6.1"><w n="26.1">Des</w> <w n="26.2">escaliers</w> <w n="26.3">en</w> <w n="26.4">dentelles</w></l>
					<l n="27" num="6.2"><w n="27.1">Montent</w> <w n="27.2">avec</w> <w n="27.3">cent</w> <w n="27.4">détours</w></l>
					<l n="28" num="6.3"><w n="28.1">Aux</w> <w n="28.2">voûtes</w> <w n="28.3">hautes</w> <w n="28.4">et</w> <w n="28.5">frêles</w>,</l>
					<l n="29" num="6.4"><w n="29.1">Mais</w> <w n="29.2">fortes</w> <w n="29.3">comme</w> <w n="29.4">les</w> <w n="29.5">ailes</w></l>
					<l n="30" num="6.5"><w n="30.1">Des</w> <w n="30.2">aigles</w> <w n="30.3">ou</w> <w n="30.4">des</w> <w n="30.5">vautours</w>.</l>
				</lg>
				<lg n="7">
					<l n="31" num="7.1"><w n="31.1">Sur</w> <w n="31.2">l</w>’<w n="31.3">autel</w>, <w n="31.4">riche</w> <w n="31.5">merveille</w>,</l>
					<l n="32" num="7.2"><w n="32.1">Ainsi</w> <w n="32.2">qu</w>’<w n="32.3">une</w> <w n="32.4">étoile</w> <w n="32.5">d</w>’<w n="32.6">or</w>,</l>
					<l n="33" num="7.3"><w n="33.1">Reluit</w> <w n="33.2">la</w> <w n="33.3">lampe</w> <w n="33.4">qui</w> <w n="33.5">veille</w>,</l>
					<l n="34" num="7.4"><w n="34.1">La</w> <w n="34.2">lampe</w> <w n="34.3">qui</w> <w n="34.4">ne</w> <w n="34.5">s</w>’<w n="34.6">éveille</w></l>
					<l n="35" num="7.5"><w n="35.1">Qu</w>’<w n="35.2">au</w> <w n="35.3">moment</w> <w n="35.4">où</w> <w n="35.5">tout</w> <w n="35.6">s</w>’<w n="35.7">endort</w>.</l>
				</lg>
				<lg n="8">
					<l n="36" num="8.1"><w n="36.1">Que</w> <w n="36.2">la</w> <w n="36.3">prière</w> <w n="36.4">est</w> <w n="36.5">fervente</w></l>
					<l n="37" num="8.2"><w n="37.1">Sous</w> <w n="37.2">ces</w> <w n="37.3">voûtes</w>, <w n="37.4">lorsqu</w>’<w n="37.5">en</w> <w n="37.6">feu</w></l>
					<l n="38" num="8.3"><w n="38.1">Le</w> <w n="38.2">ciel</w> <w n="38.3">éclate</w>, <w n="38.4">qu</w>’<w n="38.5">il</w> <w n="38.6">vente</w>,</l>
					<l n="39" num="8.4"><w n="39.1">Et</w> <w n="39.2">qu</w>’<w n="39.3">en</w> <w n="39.4">proie</w> <w n="39.5">à</w> <w n="39.6">l</w>’<w n="39.7">épouvante</w>,</l>
					<l n="40" num="8.5"><w n="40.1">Dans</w> <w n="40.2">chaque</w> <w n="40.3">éclair</w> <w n="40.4">on</w> <w n="40.5">voit</w> <w n="40.6">Dieu</w> ;</l>
				</lg>
				<lg n="9">
					<l n="41" num="9.1"><w n="41.1">Ou</w> <w n="41.2">qu</w>’<w n="41.3">à</w> <w n="41.4">l</w>’<w n="41.5">autel</w> <w n="41.6">de</w> <w n="41.7">Marie</w>,</l>
					<l n="42" num="9.2"><w n="42.1">A</w> <w n="42.2">genoux</w> <w n="42.3">sur</w> <w n="42.4">le</w> <w n="42.5">pavé</w>,</l>
					<l n="43" num="9.3"><w n="43.1">Pour</w> <w n="43.2">une</w> <w n="43.3">vierge</w> <w n="43.4">chérie</w></l>
					<l n="44" num="9.4"><w n="44.1">Qu</w>’<w n="44.2">un</w> <w n="44.3">mal</w> <w n="44.4">cruel</w> <w n="44.5">a</w> <w n="44.6">flétrie</w>,</l>
					<l n="45" num="9.5"><w n="45.1">En</w> <w n="45.2">pleurant</w> <w n="45.3">l</w>’<w n="45.4">on</w> <w n="45.5">dit</w> : <hi rend="ital"><w n="45.6">Ave</w></hi>.</l>
				</lg>
				<lg n="10">
					<l n="46" num="10.1"><w n="46.1">Mais</w> <w n="46.2">chaque</w> <w n="46.3">jour</w> <w n="46.4">qui</w> <w n="46.5">s</w>’<w n="46.6">écoule</w></l>
					<l n="47" num="10.2"><w n="47.1">Ébranle</w> <w n="47.2">ce</w> <w n="47.3">vieux</w> <w n="47.4">vaisseau</w>,</l>
					<l n="48" num="10.3"><w n="48.1">Déjà</w> <w n="48.2">plus</w> <w n="48.3">d</w>’<w n="48.4">un</w> <w n="48.5">mur</w> <w n="48.6">s</w>’<w n="48.7">écroule</w>,</l>
					<l n="49" num="10.4"><w n="49.1">Et</w> <w n="49.2">plus</w> <w n="49.3">d</w>’<w n="49.4">une</w> <w n="49.5">pierre</w> <w n="49.6">roule</w>,</l>
					<l n="50" num="10.5"><w n="50.1">Large</w> <w n="50.2">fragment</w> <w n="50.3">d</w>’<w n="50.4">un</w> <w n="50.5">arceau</w>.</l>
				</lg>
				<lg n="11">
					<l n="51" num="11.1"><w n="51.1">Dans</w> <w n="51.2">la</w> <w n="51.3">grande</w> <w n="51.4">tour</w>, <w n="51.5">la</w> <w n="51.6">cloche</w></l>
					<l n="52" num="11.2"><w n="52.1">Craint</w> <w n="52.2">de</w> <w n="52.3">sonner</w> <w n="52.4">l</w>’<hi rend="ital"><w n="52.5">Angelus</w></hi>;</l>
					<l n="53" num="11.3"><w n="53.1">Partout</w> <w n="53.2">le</w> <w n="53.3">lierre</w> <w n="53.4">s</w>’<w n="53.5">accroche</w>,</l>
					<l n="54" num="11.4"><w n="54.1">Hélas</w> ! <w n="54.2">et</w> <w n="54.3">le</w> <w n="54.4">jour</w> <w n="54.5">approche</w></l>
					<l n="55" num="11.5"><w n="55.1">Où</w> <w n="55.2">je</w> <w n="55.3">ne</w> <w n="55.4">vous</w> <w n="55.5">dirai</w> <w n="55.6">plus</w> :</l>
				</lg>
				<lg n="12">
					<l n="56" num="12.1"><w n="56.1">Il</w> <w n="56.2">est</w> <w n="56.3">une</w> <w n="56.4">basilique</w></l>
					<l n="57" num="12.2"><w n="57.1">Aux</w> <w n="57.2">murs</w> <w n="57.3">moussus</w> <w n="57.4">et</w> <w n="57.5">noircis</w>,</l>
					<l n="58" num="12.3"><w n="58.1">Du</w> <w n="58.2">vieux</w> <w n="58.3">temps</w> <w n="58.4">noble</w> <w n="58.5">relique</w>,</l>
					<l n="59" num="12.4"><w n="59.1">Où</w> <w n="59.2">l</w>’<w n="59.3">âme</w> <w n="59.4">mélancolique</w></l>
					<l n="60" num="12.5"><w n="60.1">Flotte</w> <w n="60.2">en</w> <w n="60.3">pensers</w> <w n="60.4">indécis</w>.</l>
				</lg>
			</div></body></text></TEI>