<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2184 vers2184 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Premier</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/files/44180/44180-h/44180-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Premier</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1833">1833</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Quelques erreurs orthographiques ont été repérées avec le correcteur orthogrphique Gedit.</p>
				<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU162">
				<head type="main">SONNET VI</head>
				<opener>
					<epigraph>
						<cit>
							<quote>
								Merci à toi, à toi merci.
							</quote>
							<bibl>
								<name>Térésa</name>
							</bibl>
						</cit>
					</epigraph>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Avant</w> <w n="1.2">cet</w> <w n="1.3">heureux</w> <w n="1.4">jour</w>, <w n="1.5">j</w>’<w n="1.6">étais</w> <w n="1.7">sombre</w> <w n="1.8">et</w> <w n="1.9">farouche</w>,</l>
					<l n="2" num="1.2">— <w n="2.1">Mon</w> <w n="2.2">sourcil</w> <w n="2.3">se</w> <w n="2.4">tordait</w> <w n="2.5">sur</w> <w n="2.6">mon</w> <w n="2.7">front</w> <w n="2.8">soucieux</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Ainsi</w> <w n="3.2">qu</w>’<w n="3.3">une</w> <w n="3.4">vipère</w> <w n="3.5">en</w> <w n="3.6">fureur</w>, <w n="3.7">et</w> <w n="3.8">mes</w> <w n="3.9">yeux</w></l>
					<l n="4" num="1.4"><w n="4.1">Dardaient</w> <w n="4.2">entre</w> <w n="4.3">mes</w> <w n="4.4">cils</w> <w n="4.5">un</w> <w n="4.6">regard</w> <w n="4.7">fauve</w> <w n="4.8">et</w> <w n="4.9">louche</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Un</w> <w n="5.2">sourire</w> <w n="5.3">infernal</w> <w n="5.4">crispait</w> <w n="5.5">ma</w> <w n="5.6">pâle</w> <w n="5.7">bouche</w>.</l>
					<l n="6" num="2.2"><w n="6.1">A</w> <w n="6.2">cet</w> <w n="6.3">âge</w> <w n="6.4">candide</w> <w n="6.5">où</w> <w n="6.6">tout</w> <w n="6.7">est</w> <w n="6.8">pour</w> <w n="6.9">le</w> <w n="6.10">mieux</w>,</l>
					<l n="7" num="2.3"><w n="7.1">Je</w> <w n="7.2">méprisais</w> <w n="7.3">le</w> <w n="7.4">monde</w> <w n="7.5">et</w> <w n="7.6">reniais</w> <w n="7.7">les</w> <w n="7.8">cieux</w>,</l>
					<l n="8" num="2.4"><w n="8.1">Disant</w> <w n="8.2">tout</w> <w n="8.3">haut</w> : <w n="8.4">Où</w> <w n="8.5">donc</w> <w n="8.6">est</w>-<w n="8.7">il</w>, <w n="8.8">que</w> <w n="8.9">je</w> <w n="8.10">le</w> <w n="8.11">touche</w> ?</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Et</w> <w n="9.2">mon</w> <w n="9.3">ange</w> <w n="9.4">gardien</w> <w n="9.5">à</w> <w n="9.6">son</w> <w n="9.7">front</w> <w n="9.8">blanc</w> <w n="9.9">et</w> <w n="9.10">pur</w></l>
					<l n="10" num="3.2"><w n="10.1">Ramenait</w> <w n="10.2">en</w> <w n="10.3">pleurant</w> <w n="10.4">ses</w> <w n="10.5">deux</w> <w n="10.6">ailes</w> <w n="10.7">d</w>’<w n="10.8">azur</w>,</l>
					<l n="11" num="3.3"><w n="11.1">Et</w> <w n="11.2">n</w>’<w n="11.3">osait</w> <w n="11.4">au</w> <w n="11.5">Seigneur</w> <w n="11.6">porter</w> <w n="11.7">de</w> <w n="11.8">tels</w> <w n="11.9">blasphèmes</w>.</l>
				</lg>
				<lg n="4">
					<l n="12" num="4.1"><w n="12.1">Aux</w> <w n="12.2">saints</w> <w n="12.3">épanchements</w> <w n="12.4">mon</w> <w n="12.5">cœur</w> <w n="12.6">était</w> <w n="12.7">fermé</w>,</l>
					<l n="13" num="4.2">— <w n="13.1">Car</w> <w n="13.2">je</w> <w n="13.3">ne</w> <w n="13.4">savais</w> <w n="13.5">pas</w> <w n="13.6">alors</w> <w n="13.7">combien</w> <w n="13.8">tu</w> <w n="13.9">m</w>’<w n="13.10">aimes</w> ;</l>
					<l n="14" num="4.3"><w n="14.1">Et</w> <w n="14.2">comment</w> <w n="14.3">croire</w> <w n="14.4">en</w> <w n="14.5">Dieu</w> <w n="14.6">quand</w> <w n="14.7">on</w> <w n="14.8">n</w>’<w n="14.9">est</w> <w n="14.10">pas</w> <w n="14.11">aimé</w> !</l>
				</lg>
			</div></body></text></TEI>