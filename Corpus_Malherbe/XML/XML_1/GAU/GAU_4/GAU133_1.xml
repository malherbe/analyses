<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2184 vers2184 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Premier</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/files/44180/44180-h/44180-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Premier</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1833">1833</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Quelques erreurs orthographiques ont été repérées avec le correcteur orthogrphique Gedit.</p>
				<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU133">
				<head type="main">VEILLÉE</head>
				<opener>
					<epigraph>
						<cit>
							<quote>
								Je lis les faits joyeux du bon Pantagruel, <lb></lb>
								Je sais presque par cœur l’histoire véritable <lb></lb>
								Des quatre fils Aymon et de Robert-le-Diable.
							</quote>
							<bibl>
								<name>Grandval</name>, <hi rend="ital">le Vice puni</hi>
							</bibl>
						</cit>
					</epigraph>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Lorsque</w> <w n="1.2">le</w> <w n="1.3">lambris</w> <w n="1.4">craque</w>, <w n="1.5">ébranle</w> <w n="1.6">sourdement</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Que</w> <w n="2.2">de</w> <w n="2.3">la</w> <w n="2.4">cheminée</w> <w n="2.5">il</w> <w n="2.6">jaillit</w> <w n="2.7">par</w> <w n="2.8">moment</w></l>
					<l n="3" num="1.3"><w n="3.1">Des</w> <w n="3.2">sons</w> <w n="3.3">surnaturels</w>, <w n="3.4">qu</w>’<w n="3.5">avec</w> <w n="3.6">un</w> <w n="3.7">bruit</w> <w n="3.8">étrange</w></l>
					<l n="4" num="1.4"><w n="4.1">Pétillent</w> <w n="4.2">les</w> <w n="4.3">tisons</w>, <w n="4.4">entourés</w> <w n="4.5">d</w>’<w n="4.6">une</w> <w n="4.7">frange</w></l>
					<l n="5" num="1.5"><w n="5.1">D</w>’<w n="5.2">un</w> <w n="5.3">feu</w> <w n="5.4">blafard</w> <w n="5.5">et</w> <w n="5.6">pâle</w>, <w n="5.7">et</w> <w n="5.8">que</w> <w n="5.9">des</w> <w n="5.10">vieux</w> <w n="5.11">portraits</w></l>
					<l n="6" num="1.6"><w n="6.1">De</w> <w n="6.2">bizarres</w> <w n="6.3">lueurs</w> <w n="6.4">font</w> <w n="6.5">grimacer</w> <w n="6.6">les</w> <w n="6.7">traits</w> ;</l>
					<l n="7" num="1.7"><w n="7.1">Seul</w>, <w n="7.2">assis</w>, <w n="7.3">loin</w> <w n="7.4">du</w> <w n="7.5">bruit</w>, <w n="7.6">du</w> <w n="7.7">récit</w> <w n="7.8">des</w> <w n="7.9">merveilles</w></l>
					<l n="8" num="1.8"><w n="8.1">D</w>’<w n="8.2">autrefois</w> <w n="8.3">aimez</w>-<w n="8.4">vous</w> <w n="8.5">bercer</w> <w n="8.6">vos</w> <w n="8.7">longues</w> <w n="8.8">veilles</w> ?</l>
					<l n="9" num="1.9"><w n="9.1">C</w>’<w n="9.2">est</w> <w n="9.3">mon</w> <w n="9.4">plaisir</w> <w n="9.5">à</w> <w n="9.6">moi</w> : <w n="9.7">si</w>, <w n="9.8">dans</w> <w n="9.9">un</w> <w n="9.10">vieux</w> <w n="9.11">château</w>,</l>
					<l n="10" num="1.10"><w n="10.1">J</w>’<w n="10.2">ai</w> <w n="10.3">trouvé</w> <w n="10.4">par</w> <w n="10.5">hasard</w> <w n="10.6">quelque</w> <w n="10.7">lourd</w> <w n="10.8">in</w>-<w n="10.9">quarto</w>,</l>
					<l n="11" num="1.11"><w n="11.1">Sur</w> <w n="11.2">les</w> <w n="11.3">rayons</w> <w n="11.4">poudreux</w> <w n="11.5">d</w>’<w n="11.6">une</w> <w n="11.7">armoire</w> <w n="11.8">gothique</w></l>
					<l n="12" num="1.12"><w n="12.1">Dès</w> <w n="12.2">longtemps</w> <w n="12.3">oublié</w>, <w n="12.4">mais</w> <w n="12.5">dont</w> <w n="12.6">la</w> <w n="12.7">marge</w> <w n="12.8">antique</w>,</l>
					<l n="13" num="1.13"><w n="13.1">Couverte</w> <w n="13.2">d</w>’<w n="13.3">ornements</w>, <w n="13.4">de</w> <w n="13.5">fantastiques</w> <w n="13.6">fleurs</w>,</l>
					<l n="14" num="1.14"><w n="14.1">Brille</w>, <w n="14.2">comme</w> <w n="14.3">un</w> <w n="14.4">vitrail</w>, <w n="14.5">des</w> <w n="14.6">plus</w> <w n="14.7">vives</w> <w n="14.8">couleurs</w>,</l>
					<l n="15" num="1.15"><w n="15.1">Je</w> <w n="15.2">ne</w> <w n="15.3">puis</w> <w n="15.4">le</w> <w n="15.5">quitter</w>. <w n="15.6">Lais</w>, <w n="15.7">virelais</w>, <w n="15.8">ballades</w>,</l>
					<l n="16" num="1.16"><w n="16.1">Légendes</w> <w n="16.2">de</w> <w n="16.3">béats</w> <w n="16.4">guérissant</w> <w n="16.5">les</w> <w n="16.6">malades</w>,</l>
					<l n="17" num="1.17"><w n="17.1">Les</w> <w n="17.2">possédés</w> <w n="17.3">du</w> <w n="17.4">diable</w>, <w n="17.5">et</w> <w n="17.6">les</w> <w n="17.7">pauvres</w> <w n="17.8">lépreux</w>,</l>
					<l n="18" num="1.18"><w n="18.1">Par</w> <w n="18.2">un</w> <w n="18.3">signe</w> <w n="18.4">de</w> <w n="18.5">croix</w> ; <w n="18.6">chroniques</w> <w n="18.7">d</w>’<w n="18.8">anciens</w> <w n="18.9">preux</w>,</l>
					<l n="19" num="1.19"><w n="19.1">Mes</w> <w n="19.2">yeux</w> <w n="19.3">dévorent</w> <w n="19.4">tout</w> ; <w n="19.5">c</w>’<w n="19.6">est</w> <w n="19.7">en</w> <w n="19.8">vain</w> <w n="19.9">que</w> <w n="19.10">l</w>’<w n="19.11">horloge</w></l>
					<l n="20" num="1.20"><w n="20.1">Tinte</w> <w n="20.2">par</w> <w n="20.3">douze</w> <w n="20.4">fois</w>, <w n="20.5">que</w> <w n="20.6">le</w> <w n="20.7">hibou</w> <w n="20.8">déloge</w></l>
					<l n="21" num="1.21"><w n="21.1">En</w> <w n="21.2">glapissant</w>, <w n="21.3">blessé</w> <w n="21.4">des</w> <w n="21.5">rayons</w> <w n="21.6">du</w> <w n="21.7">flambeau</w></l>
					<l n="22" num="1.22"><w n="22.1">Qui</w> <w n="22.2">m</w>’<w n="22.3">éclaire</w> ; <w n="22.4">je</w> <w n="22.5">lis</w> : <w n="22.6">sur</w> <w n="22.7">la</w> <w n="22.8">table</w> <w n="22.9">à</w> <w n="22.10">tombeau</w>,</l>
					<l n="23" num="1.23"><w n="23.1">Le</w> <w n="23.2">long</w> <w n="23.3">du</w> <w n="23.4">chandelier</w>, <w n="23.5">cependant</w> <w n="23.6">la</w> <w n="23.7">bougie</w></l>
					<l n="24" num="1.24"><w n="24.1">En</w> <w n="24.2">larges</w> <w n="24.3">nappes</w> <w n="24.4">coule</w>, <w n="24.5">et</w> <w n="24.6">la</w> <w n="24.7">vitre</w> <w n="24.8">rougie</w></l>
					<l n="25" num="1.25"><w n="25.1">Laisse</w> <w n="25.2">voir</w> <w n="25.3">dans</w> <w n="25.4">le</w> <w n="25.5">ciel</w>, <w n="25.6">au</w> <w n="25.7">bord</w> <w n="25.8">de</w> <w n="25.9">l</w>’<w n="25.10">orient</w>,</l>
					<l n="26" num="1.26"><w n="26.1">Le</w> <w n="26.2">soleil</w> <w n="26.3">qui</w> <w n="26.4">se</w> <w n="26.5">lève</w> <w n="26.6">avec</w> <w n="26.7">un</w> <w n="26.8">front</w> <w n="26.9">riant</w>.</l>
				</lg>
			</div></body></text></TEI>