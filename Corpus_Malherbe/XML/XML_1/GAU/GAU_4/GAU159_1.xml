<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2184 vers2184 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Premier</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/files/44180/44180-h/44180-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Premier</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1833">1833</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Quelques erreurs orthographiques ont été repérées avec le correcteur orthogrphique Gedit.</p>
				<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU159">
				<head type="main">SONNET V</head>
				<opener>
					<epigraph>
						<cit>
							<quote>
								C’est mon plaisir ; chacun querre le sien.
							</quote>
							<bibl>
							 <name>P. L. Jacob</name>, <hi rend="ital">bibliophile</hi>.
							</bibl>
						</cit>
						<cit>
							<quote>
								Heureusement que, pour nous consoler de tout <lb></lb>
								cela, il nous reste l’adultère, le tabac de Maryland, <lb></lb>
								et le papel español por cigaritos
							</quote>
							<bibl>
								<name>Petrus Borel</name>, <hi rend="ital">le lycanthrope</hi>.
							</bibl>
						</cit>
						<cit>
							<quote>
								Où trouver le bonheur ?
							</quote>
							<bibl>
								<name>Méry et Barthélemy</name>
							</bibl>
						</cit>
					</epigraph>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Qu</w>’<w n="1.2">est</w>-<w n="1.3">ce</w> <w n="1.4">que</w> <w n="1.5">ce</w> <w n="1.6">bonheur</w> <w n="1.7">dont</w> <w n="1.8">on</w> <w n="1.9">parle</w> ? — <w n="1.10">L</w>’<w n="1.11">avare</w></l>
					<l n="2" num="1.2"><w n="2.1">Au</w> <w n="2.2">fond</w> <w n="2.3">d</w>’<w n="2.4">un</w> <w n="2.5">coffre</w>-<w n="2.6">fort</w> <w n="2.7">empile</w> <w n="2.8">des</w> <w n="2.9">ducats</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Des</w> <w n="3.2">piastres</w>, <w n="3.3">des</w> <w n="3.4">doublons</w>, <w n="3.5">et</w> <w n="3.6">plus</w> <w n="3.7">d</w>’<w n="3.8">or</w> <w n="3.9">qu</w>’<w n="3.10">aux</w> <w n="3.11">Incas</w></l>
					<l n="4" num="1.4"><w n="4.1">Jadis</w> <w n="4.2">avec</w> <w n="4.3">leur</w> <w n="4.4">sang</w> <w n="4.5">n</w>’<w n="4.6">en</w> <w n="4.7">fit</w> <w n="4.8">suer</w> <w n="4.9">Pizarre</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Il</w> <w n="5.2">ne</w> <w n="5.3">voit</w> <w n="5.4">rien</w> <w n="5.5">de</w> <w n="5.6">plus</w>. — <w n="5.7">Le</w> <w n="5.8">far</w>-<w n="5.9">niente</w>, <w n="5.10">un</w> <w n="5.11">cigare</w>,</l>
					<l n="6" num="2.2"><w n="6.1">Voilà</w> <w n="6.2">pour</w> <w n="6.3">l</w>’<w n="6.4">indolent</w>. — <w n="6.5">Le</w> <w n="6.6">songeur</w> <w n="6.7">ne</w> <w n="6.8">fait</w> <w n="6.9">cas</w></l>
					<l n="7" num="2.3"><w n="7.1">Que</w> <w n="7.2">d</w>’<w n="7.3">un</w> <w n="7.4">coin</w> <w n="7.5">retiré</w> <w n="7.6">du</w> <w n="7.7">monde</w> <w n="7.8">et</w> <w n="7.9">du</w> <w n="7.10">fracas</w>,</l>
					<l n="8" num="2.4"><w n="8.1">Où</w> <w n="8.2">l</w>’<w n="8.3">on</w> <w n="8.4">puisse</w> <w n="8.5">à</w> <w n="8.6">loisir</w> <w n="8.7">suivre</w> <w n="8.8">un</w> <w n="8.9">rêve</w> <w n="8.10">bizarre</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">L</w>’<w n="9.2">ambitieux</w> <w n="9.3">le</w> <w n="9.4">met</w> <w n="9.5">dans</w> <w n="9.6">un</w> <w n="9.7">titre</w> <w n="9.8">à</w> <w n="9.9">la</w> <w n="9.10">cour</w>,</l>
					<l n="10" num="3.2"><w n="10.1">Le</w> <w n="10.2">vieux</w> <w n="10.3">dans</w> <w n="10.4">le</w> <w n="10.5">comfort</w>, <w n="10.6">le</w> <w n="10.7">jeune</w> <w n="10.8">dans</w> <w n="10.9">l</w>’<w n="10.10">amour</w>,</l>
					<l n="11" num="3.3">— <w n="11.1">Les</w> <w n="11.2">uns</w> <w n="11.3">à</w> <w n="11.4">pérorer</w>, <w n="11.5">les</w> <w n="11.6">autres</w> <w n="11.7">à</w> <w n="11.8">se</w> <w n="11.9">taire</w>.</l>
				</lg>
				<lg n="4">
					<l n="12" num="4.1"><w n="12.1">Mais</w>, <w n="12.2">étant</w> <w n="12.3">exclusifs</w>, <w n="12.4">ces</w> <w n="12.5">gens</w>-<w n="12.6">là</w> <w n="12.7">jugent</w> <w n="12.8">mal</w> ;</l>
					<l n="13" num="4.2"><w n="13.1">Car</w> <w n="13.2">le</w> <w n="13.3">bonheur</w> <w n="13.4">est</w> <w n="13.5">fait</w> <w n="13.6">de</w> <w n="13.7">trois</w> <w n="13.8">choses</w> <w n="13.9">sur</w> <w n="13.10">terre</w>,</l>
					<l n="14" num="4.3"><w n="14.1">Qui</w> <w n="14.2">sont</w> : — <w n="14.3">Un</w> <w n="14.4">beau</w> <w n="14.5">soleil</w>, <w n="14.6">une</w> <w n="14.7">femme</w>, <w n="14.8">un</w> <w n="14.9">cheval</w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1831">1831</date>
					</dateline>
				</closer>
			</div></body></text></TEI>