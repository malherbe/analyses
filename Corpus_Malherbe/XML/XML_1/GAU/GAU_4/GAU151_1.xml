<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2184 vers2184 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Premier</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/files/44180/44180-h/44180-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Premier</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1833">1833</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Quelques erreurs orthographiques ont été repérées avec le correcteur orthogrphique Gedit.</p>
				<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU151">
				<head type="main">ENFANTILLAGE</head>
				<opener>
					<epigraph>
						<cit>
							<quote>
								Hanneton, vole, vole, vole.
							</quote>
							<bibl>
								<hi rend="ital">Ballade des petites filles.</hi>
							</bibl>
						</cit>
					</epigraph>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Lorsque</w> <w n="1.2">la</w> <w n="1.3">froide</w> <w n="1.4">pluie</w> <w n="1.5">enfin</w> <w n="1.6">s</w>’<w n="1.7">en</w> <w n="1.8">est</w> <w n="1.9">allée</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Et</w> <w n="2.2">que</w> <w n="2.3">le</w> <w n="2.4">ciel</w> <w n="2.5">gaîment</w> <w n="2.6">rouvre</w> <w n="2.7">son</w> <w n="2.8">bel</w> <w n="2.9">œil</w> <w n="2.10">bleu</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Ennuyé</w> <w n="3.2">d</w>’<w n="3.3">être</w> <w n="3.4">au</w> <w n="3.5">gîte</w> <w n="3.6">et</w> <w n="3.7">de</w> <w n="3.8">couver</w> <w n="3.9">le</w> <w n="3.10">feu</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Comme</w> <w n="4.2">les</w> <w n="4.3">moineaux</w> <w n="4.4">francs</w>, <w n="4.5">je</w> <w n="4.6">reprends</w> <w n="4.7">ma</w> <w n="4.8">volée</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">A</w> <w n="5.2">Romainville</w>, — <w n="5.3">ou</w> <w n="5.4">bien</w> <w n="5.5">dans</w> <w n="5.6">les</w> <w n="5.7">prés</w> <w n="5.8">Saint</w>-<w n="5.9">Gervais</w>,</l>
					<l n="6" num="2.2"><w n="6.1">Curieux</w> <w n="6.2">de</w> <w n="6.3">savoir</w> <w n="6.4">si</w> <w n="6.5">l</w>’<w n="6.6">aubépine</w> <w n="6.7">blanche</w></l>
					<l n="7" num="2.3"><w n="7.1">A</w> <w n="7.2">déjà</w> <w n="7.3">fait</w> <w n="7.4">neiger</w> <w n="7.5">son</w> <w n="7.6">givre</w> <w n="7.7">sur</w> <w n="7.8">la</w> <w n="7.9">branche</w>,</l>
					<l n="8" num="2.4"><w n="8.1">Par</w> <w n="8.2">l</w>’<w n="8.3">herbe</w> <w n="8.4">et</w> <w n="8.5">la</w> <w n="8.6">rosée</w>, <w n="8.7">en</w> <w n="8.8">pépiant</w>, <w n="8.9">je</w> <w n="8.10">vais</w>,</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Me</w> <w n="9.2">faisant</w> <w n="9.3">du</w> <w n="9.4">bonheur</w> <w n="9.5">avec</w> <w n="9.6">la</w> <w n="9.7">moindre</w> <w n="9.8">chose</w> :</l>
					<l n="10" num="3.2">— <w n="10.1">D</w>’<w n="10.2">une</w> <w n="10.3">goutte</w> <w n="10.4">d</w>’<w n="10.5">eau</w> <w n="10.6">claire</w>, <w n="10.7">où</w> <w n="10.8">sous</w> <w n="10.9">un</w> <w n="10.10">rayon</w> <w n="10.11">pur</w>,</l>
					<l n="11" num="3.3"><w n="11.1">Se</w> <w n="11.2">baigne</w> <w n="11.3">un</w> <w n="11.4">scarabée</w> <w n="11.5">au</w> <w n="11.6">corselet</w> <w n="11.7">d</w>’<w n="11.8">azur</w> ;</l>
					<l n="12" num="3.4"><w n="12.1">D</w>’<w n="12.2">une</w> <w n="12.3">abeille</w> <w n="12.4">en</w> <w n="12.5">maraude</w> <w n="12.6">au</w> <w n="12.7">cœur</w> <w n="12.8">d</w>’<w n="12.9">une</w> <w n="12.10">fleur</w> <w n="12.11">rose</w>,</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">D</w>’<w n="13.2">un</w> <w n="13.3">brin</w> <w n="13.4">d</w>’<w n="13.5">herbe</w> <w n="13.6">où</w> <w n="13.7">la</w> <w n="13.8">Vierge</w> <w n="13.9">a</w> <w n="13.10">filé</w> <w n="13.11">son</w> <w n="13.12">coton</w>.</l>
					<l n="14" num="4.2">— <w n="14.1">Mais</w> <w n="14.2">plus</w> <w n="14.3">que</w> <w n="14.4">tout</w> <w n="14.5">cela</w> <w n="14.6">j</w>’<w n="14.7">aime</w> <w n="14.8">sous</w> <w n="14.9">les</w> <w n="14.10">charmilles</w>,</l>
					<l n="15" num="4.3"><w n="15.1">Dans</w> <w n="15.2">le</w> <w n="15.3">parc</w> <w n="15.4">Saint</w>-<w n="15.5">Fargeau</w>, <w n="15.6">voir</w> <w n="15.7">les</w> <w n="15.8">petites</w> <w n="15.9">filles</w></l>
					<l n="16" num="4.4"><w n="16.1">Emplir</w> <w n="16.2">leurs</w> <w n="16.3">tabliers</w> <w n="16.4">de</w> <w n="16.5">pain</w> <w n="16.6">de</w> <w n="16.7">hanneton</w>.</l>
				</lg>
			</div></body></text></TEI>