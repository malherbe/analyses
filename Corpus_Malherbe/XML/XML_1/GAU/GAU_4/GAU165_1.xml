<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2184 vers2184 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Premier</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/files/44180/44180-h/44180-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Premier</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1833">1833</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Quelques erreurs orthographiques ont été repérées avec le correcteur orthogrphique Gedit.</p>
				<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU165">
				<head type="main">PARIS</head>
				<opener>
					<epigraph>
						<cit>
							<quote>
								Das drængt und stœsst, das ruscht und klappert <lb></lb>
								Das zischt und quirlt, das zieht und plappert ! <lb></lb>
								Das leuchtet, sprüht, und stinkt und brennt !
							</quote>
							<bibl>
								<name>Gœthe</name>, <hi rend="ital">Faust.</hi>
							</bibl>
						</cit>
						<cit>
							<quote>
								Dans la simplicité de mon cœur enfantin <lb></lb>
								L’œil fixé sur les cieux, j’enviais le destin <lb></lb>
								De l’oiseau voyageur, du nuage qui passe <lb></lb>
								Et fait tant de chemin, et dans ce large espace <lb></lb>
								Voit les mondes sous lui glisser rapidement, <lb></lb>
								Ainsi qu’un météore aux champs du firmament.
							</quote>
							<bibl>
								<name>Eugène DE ***.</name>
							</bibl>
						</cit>
						<cit>
							<quote>
								Hé, Dieu ! que de maisons ! que de beaux bâtiments ! <lb></lb>
							</quote>
							<bibl>
								<name>Estienne de Knobelsdorff.</name>
							</bibl>
						</cit>
						<cit>
							<quote>
									 Salle de réception du diable.
							</quote>
							<bibl>
									 <hi rend="ital">Don Juan</hi>, ch. x, st. 81.
							</bibl>
						</cit>
					</epigraph>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Quand</w> <w n="1.2">il</w> <w n="1.3">voit</w> <w n="1.4">le</w> <w n="1.5">soleil</w>, <w n="1.6">déchirant</w> <w n="1.7">le</w> <w n="1.8">nuage</w>,</l>
					<l n="2" num="1.2"><w n="2.1">De</w> <w n="2.2">splendides</w> <w n="2.3">rayons</w> <w n="2.4">illuminer</w> <w n="2.5">sa</w> <w n="2.6">cage</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Et</w> <w n="3.2">comme</w> <w n="3.3">un</w> <w n="3.4">lion</w> <w n="3.5">d</w>’<w n="3.6">or</w> <w n="3.7">secouer</w>, <w n="3.8">dans</w> <w n="3.9">le</w> <w n="3.10">bleu</w></l>
					<l n="4" num="1.4"><w n="4.1">Qui</w> <w n="4.2">se</w> <w n="4.3">fait</w> <w n="4.4">à</w> <w n="4.5">l</w>’<w n="4.6">entour</w>, <w n="4.7">sa</w> <w n="4.8">crinière</w> <w n="4.9">de</w> <w n="4.10">feu</w>,</l>
					<l n="5" num="1.5"><w n="5.1">L</w>’<w n="5.2">aigle</w> <w n="5.3">prisonnier</w> <w n="5.4">bat</w> <w n="5.5">avec</w> <w n="5.6">son</w> <w n="5.7">aile</w> <w n="5.8">forte</w></l>
					<l n="6" num="1.6"><w n="6.1">Les</w> <w n="6.2">lourds</w> <w n="6.3">barreaux</w> <w n="6.4">de</w> <w n="6.5">fer</w> <w n="6.6">tant</w> <w n="6.7">qu</w>’<w n="6.8">il</w> <w n="6.9">se</w> <w n="6.10">tue</w> <w n="6.11">ou</w> <w n="6.12">sorte</w>.</l>
					<l n="7" num="1.7">— <w n="7.1">Mon</w> <w n="7.2">âme</w> <w n="7.3">est</w> <w n="7.4">faite</w> <w n="7.5">ainsi</w> : <w n="7.6">dans</w> <w n="7.7">mon</w> <w n="7.8">corps</w> <w n="7.9">en</w> <w n="7.10">prison</w>,</l>
					<l n="8" num="1.8"><w n="8.1">Elle</w> <w n="8.2">cherche</w> <w n="8.3">à</w> <w n="8.4">son</w> <w n="8.5">vol</w> <w n="8.6">un</w> <w n="8.7">plus</w> <w n="8.8">large</w> <w n="8.9">horizon</w> ;</l>
					<l n="9" num="1.9"><w n="9.1">Quand</w> <w n="9.2">sur</w> <w n="9.3">elle</w> <w n="9.4">d</w>’<w n="9.5">en</w> <w n="9.6">haut</w> <w n="9.7">la</w> <w n="9.8">sainte</w> <w n="9.9">Poésie</w></l>
					<l n="10" num="1.10"><w n="10.1">Abaisse</w> <w n="10.2">son</w> <w n="10.3">regard</w>, <w n="10.4">de</w> <w n="10.5">grands</w> <w n="10.6">désirs</w> <w n="10.7">saisie</w>,</l>
					<l n="11" num="1.11"><w n="11.1">Elle</w> <w n="11.2">voudrait</w> <w n="11.3">surgir</w> <w n="11.4">jusqu</w>’<w n="11.5">au</w> <w n="11.6">clair</w> <w n="11.7">firmament</w></l>
					<l n="12" num="1.12"><w n="12.1">Afin</w> <w n="12.2">d</w>’<w n="12.3">y</w> <w n="12.4">respirer</w> <w n="12.5">largement</w>, <w n="12.6">librement</w>,</l>
					<l n="13" num="1.13"><w n="13.1">Entre</w> <w n="13.2">la</w> <w n="13.3">terre</w> <w n="13.4">et</w> <w n="13.5">Dieu</w>, <w n="13.6">bien</w> <w n="13.7">par</w> <w n="13.8">delà</w> <w n="13.9">les</w> <w n="13.10">nues</w></l>
					<l n="14" num="1.14"><w n="14.1">Et</w> <w n="14.2">les</w> <w n="14.3">plaines</w> <w n="14.4">d</w>’<w n="14.5">azur</w>, <w n="14.6">régions</w> <w n="14.7">inconnues</w>,</l>
					<l n="15" num="1.15"><w n="15.1">L</w>’<w n="15.2">air</w> <w n="15.3">limpide</w>, <w n="15.4">l</w>’<w n="15.5">air</w> <w n="15.6">vierge</w>, <w n="15.7">où</w> <w n="15.8">jamais</w> <w n="15.9">souffle</w> <w n="15.10">humain</w></l>
					<l n="16" num="1.16"><w n="16.1">Ne</w> <w n="16.2">passe</w>, <w n="16.3">où</w> <w n="16.4">l</w>’<w n="16.5">ange</w> <w n="16.6">seul</w> <w n="16.7">retrouve</w> <w n="16.8">son</w> <w n="16.9">chemin</w> ;</l>
					<l n="17" num="1.17"><w n="17.1">Car</w> <w n="17.2">elle</w> <w n="17.3">manque</w> <w n="17.4">d</w>’<w n="17.5">air</w>, <w n="17.6">mon</w> <w n="17.7">âme</w>, <w n="17.8">dans</w> <w n="17.9">ce</w> <w n="17.10">monde</w></l>
					<l n="18" num="1.18"><w n="18.1">Où</w> <w n="18.2">la</w> <w n="18.3">presse</w> <w n="18.4">en</w> <w n="18.5">tous</w> <w n="18.6">sens</w> <w n="18.7">de</w> <w n="18.8">son</w> <w n="18.9">étreinte</w> <w n="18.10">immonde</w></l>
					<l n="19" num="1.19"><w n="19.1">Une</w> <w n="19.2">société</w> <w n="19.3">qui</w> <w n="19.4">retombe</w> <w n="19.5">au</w> <w n="19.6">chaos</w>,</l>
					<l n="20" num="1.20"><w n="20.1">Du</w> <w n="20.2">rouge</w> <w n="20.3">sur</w> <w n="20.4">la</w> <w n="20.5">joue</w> <w n="20.6">et</w> <w n="20.7">la</w> <w n="20.8">gangrène</w> <w n="20.9">aux</w> <w n="20.10">os</w> !</l>
					<l n="21" num="1.21"><w n="21.1">Il</w> <w n="21.2">lui</w> <w n="21.3">faudrait</w> <w n="21.4">des</w> <w n="21.5">monts</w> <w n="21.6">aux</w> <w n="21.7">cheveux</w> <w n="21.8">blancs</w> <w n="21.9">de</w> <w n="21.10">neige</w>,</l>
					<l n="22" num="1.22"><w n="22.1">De</w> <w n="22.2">grands</w> <w n="22.3">rochers</w> <w n="22.4">à</w> <w n="22.5">pic</w>, <w n="22.6">trônes</w> <w n="22.7">géants</w> <w n="22.8">où</w> <w n="22.9">siège</w>,</l>
					<l n="23" num="1.23"><w n="23.1">Ayant</w> <w n="23.2">pour</w> <w n="23.3">marchepied</w> <w n="23.4">le</w> <w n="23.5">vertige</w> <w n="23.6">et</w> <w n="23.7">l</w>’<w n="23.8">effroi</w>,</l>
					<l n="24" num="1.24"><w n="24.1">La</w> <w n="24.2">majesté</w> <w n="24.3">muette</w> <w n="24.4">et</w> <w n="24.5">sombre</w> <w n="24.6">du</w> <w n="24.7">grand</w> <w n="24.8">Roi</w>.</l>
					<l n="25" num="1.25"><w n="25.1">Il</w> <w n="25.2">lui</w> <w n="25.3">faudrait</w> <w n="25.4">la</w> <w n="25.5">voix</w> <w n="25.6">du</w> <w n="25.7">tonnerre</w> <w n="25.8">qui</w> <w n="25.9">roule</w></l>
					<l n="26" num="1.26"><w n="26.1">Ses</w> <w n="26.2">mugissements</w> <w n="26.3">sourds</w> <w n="26.4">comme</w> <w n="26.5">des</w> <w n="26.6">bruits</w> <w n="26.7">de</w> <w n="26.8">foule</w> ;</l>
					<l n="27" num="1.27"><w n="27.1">Le</w> <w n="27.2">torrent</w> <w n="27.3">qui</w> <w n="27.4">bondit</w> <w n="27.5">entre</w> <w n="27.6">les</w> <w n="27.7">rocs</w> <w n="27.8">qu</w>’<w n="27.9">il</w> <w n="27.10">fond</w>,</l>
					<l n="28" num="1.28"><w n="28.1">Se</w> <w n="28.2">tord</w> <w n="28.3">comme</w> <w n="28.4">un</w> <w n="28.5">damné</w> <w n="28.6">dans</w> <w n="28.7">l</w>’<w n="28.8">abîme</w> <w n="28.9">sans</w> <w n="28.10">fond</w>,</l>
					<l n="29" num="1.29"><w n="29.1">Jette</w> <w n="29.2">ses</w> <w n="29.3">forts</w> <w n="29.4">abois</w> <w n="29.5">qu</w>’<w n="29.6">on</w> <w n="29.7">entend</w> <w n="29.8">d</w>’<w n="29.9">une</w> <w n="29.10">lieue</w>,</l>
					<l n="30" num="1.30"><w n="30.1">Et</w>, <w n="30.2">tout</w> <w n="30.3">échevelé</w>, <w n="30.4">semble</w> <w n="30.5">la</w> <w n="30.6">pâle</w> <w n="30.7">queue</w></l>
					<l n="31" num="1.31"><w n="31.1">Du</w> <w n="31.2">cheval</w> <w n="31.3">de</w> <w n="31.4">la</w> <w n="31.5">mort</w> <w n="31.6">au</w> <w n="31.7">livre</w> <w n="31.8">de</w> <w n="31.9">saint</w> <w n="31.10">Jean</w>.</l>
					<l n="32" num="1.32"><w n="32.1">Il</w> <w n="32.2">lui</w> <w n="32.3">faudrait</w> <w n="32.4">au</w> <w n="32.5">soir</w> <w n="32.6">la</w> <w n="32.7">lune</w> <w n="32.8">voyageant</w>,</l>
					<l n="33" num="1.33"><w n="33.1">Non</w> <w n="33.2">sur</w> <w n="33.3">l</w>’<w n="33.4">angle</w> <w n="33.5">des</w> <w n="33.6">toits</w>, <w n="33.7">mais</w> <w n="33.8">sur</w> <w n="33.9">les</w> <w n="33.10">cimes</w> <w n="33.11">grêles</w></l>
					<l n="34" num="1.34"><w n="34.1">Des</w> <w n="34.2">sapins</w> <w n="34.3">déployant</w> <w n="34.4">leurs</w> <w n="34.5">bras</w> <w n="34.6">comme</w> <w n="34.7">des</w> <w n="34.8">ailes</w>,</l>
					<l n="35" num="1.35"><w n="35.1">Les</w> <w n="35.2">arêtes</w> <w n="35.3">des</w> <w n="35.4">pics</w> <w n="35.5">et</w> <w n="35.6">les</w> <w n="35.7">tours</w> <w n="35.8">du</w> <w n="35.9">manoir</w></l>
					<l n="36" num="1.36"><w n="36.1">De</w> <w n="36.2">leurs</w> <w n="36.3">fronts</w> <w n="36.4">ardoisés</w> <w n="36.5">découpant</w> <w n="36.6">le</w> <w n="36.7">ciel</w> <w n="36.8">noir</w>.</l>
					<l n="37" num="1.37">— <w n="37.1">Elle</w> <w n="37.2">n</w>’<w n="37.3">a</w> <w n="37.4">pas</w> <w n="37.5">cela</w>, <w n="37.6">mon</w> <w n="37.7">âme</w>, <w n="37.8">non</w> <w n="37.9">pas</w> <w n="37.10">même</w></l>
					<l n="38" num="1.38"><w n="38.1">L</w>’<w n="38.2">humble</w> <w n="38.3">petit</w> <w n="38.4">coteau</w>, <w n="38.5">la</w> <w n="38.6">campagne</w> <w n="38.7">qu</w>’<w n="38.8">elle</w> <w n="38.9">aime</w>,</l>
					<l n="39" num="1.39"><w n="39.1">Le</w> <w n="39.2">vallon</w> <w n="39.3">frais</w> <w n="39.4">et</w> <w n="39.5">creux</w>, <w n="39.6">les</w> <w n="39.7">sveltes</w> <w n="39.8">peupliers</w></l>
					<l n="40" num="1.40"><w n="40.1">Dont</w> <w n="40.2">la</w> <w n="40.3">bise</w> <w n="40.4">de</w> <w n="40.5">nuit</w> <w n="40.6">berce</w> <w n="40.7">les</w> <w n="40.8">fronts</w> <w n="40.9">pliés</w>,</l>
					<l n="41" num="1.41"><w n="41.1">La</w> <w n="41.2">chaumière</w> <w n="41.3">des</w> <w n="41.4">bois</w>, <w n="41.5">poussant</w> <w n="41.6">en</w> <w n="41.7">bleus</w> <w n="41.8">nuages</w></l>
					<l n="42" num="1.42"><w n="42.1">Son</w> <w n="42.2">filet</w> <w n="42.3">de</w> <w n="42.4">fumée</w> <w n="42.5">à</w> <w n="42.6">travers</w> <w n="42.7">les</w> <w n="42.8">feuillages</w>,</l>
					<l n="43" num="1.43"><w n="43.1">Et</w> <w n="43.2">dont</w> <w n="43.3">le</w> <w n="43.4">toit</w> <w n="43.5">moussu</w> <w n="43.6">porte</w> <w n="43.7">sur</w> <w n="43.8">son</w> <w n="43.9">velours</w></l>
					<l n="44" num="1.44"><w n="44.1">Des</w> <w n="44.2">fleurs</w> <w n="44.3">tous</w> <w n="44.4">les</w> <w n="44.5">printemps</w>, <w n="44.6">des</w> <w n="44.7">pigeons</w> <w n="44.8">tous</w> <w n="44.9">les</w> <w n="44.10">jours</w> ;</l>
					<l n="45" num="1.45"><w n="45.1">Le</w> <w n="45.2">jardin</w> <w n="45.3">et</w> <w n="45.4">son</w> <w n="45.5">puits</w> <w n="45.6">que</w> <w n="45.7">festonne</w> <w n="45.8">une</w> <w n="45.9">vigne</w>,</l>
					<l n="46" num="1.46"><w n="46.1">Où</w>, <w n="46.2">des</w> <w n="46.3">choux</w> <w n="46.4">à</w> <w n="46.5">propos</w> <w n="46.6">interrompant</w> <w n="46.7">la</w> <w n="46.8">ligne</w>,</l>
					<l n="47" num="1.47"><w n="47.1">Se</w> <w n="47.2">pavane</w> <w n="47.3">un</w> <w n="47.4">rosier</w> <w n="47.5">que</w> <w n="47.6">votre</w> <w n="47.7">main</w> <w n="47.8">sema</w> ;</l>
					<l n="48" num="1.48"><w n="48.1">Asile</w> <w n="48.2">calme</w> <w n="48.3">et</w> <w n="48.4">vert</w> <w n="48.5">comme</w> <w n="48.6">en</w> <w n="48.7">peint</w> <w n="48.8">Hobbéma</w>,</l>
					<l n="49" num="1.49"><w n="49.1">Où</w> <w n="49.2">les</w> <w n="49.3">chuchotements</w> <w n="49.4">dont</w> <w n="49.5">est</w> <w n="49.6">fait</w> <w n="49.7">le</w> <w n="49.8">silence</w></l>
					<l n="50" num="1.50"><w n="50.1">Troublent</w> <w n="50.2">seuls</w> <w n="50.3">du</w> <w n="50.4">rêveur</w> <w n="50.5">la</w> <w n="50.6">douce</w> <w n="50.7">somnolence</w> !</l>
					<l n="51" num="1.51"><w n="51.1">Non</w> <w n="51.2">pas</w> <w n="51.3">même</w> <w n="51.4">cela</w> : <w n="51.5">mais</w> <w n="51.6">la</w> <w n="51.7">ville</w> <w n="51.8">aux</w> <w n="51.9">cent</w> <w n="51.10">bruits</w></l>
					<l n="52" num="1.52"><w n="52.1">Où</w> <w n="52.2">de</w> <w n="52.3">brouillards</w> <w n="52.4">noyés</w> <w n="52.5">les</w> <w n="52.6">jours</w> <w n="52.7">semblent</w> <w n="52.8">des</w> <w n="52.9">nuits</w>,</l>
					<l n="53" num="1.53"><w n="53.1">Où</w> <w n="53.2">parmi</w> <w n="53.3">les</w> <w n="53.4">toits</w> <w n="53.5">bleus</w> <w n="53.6">s</w>’<w n="53.7">enchevêtre</w> <w n="53.8">et</w> <w n="53.9">se</w> <w n="53.10">cogne</w></l>
					<l n="54" num="1.54"><w n="54.1">Un</w> <w n="54.2">soleil</w> <w n="54.3">terne</w> <w n="54.4">et</w> <w n="54.5">mort</w> <w n="54.6">comme</w> <w n="54.7">l</w>’<w n="54.8">œil</w> <w n="54.9">d</w>’<w n="54.10">un</w> <w n="54.11">ivrogne</w> ;</l>
					<l n="55" num="1.55"><w n="55.1">Des</w> <w n="55.2">tuyaux</w> <w n="55.3">hérissant</w> <w n="55.4">le</w> <w n="55.5">faîte</w> <w n="55.6">des</w> <w n="55.7">maisons</w></l>
					<l n="56" num="1.56"><w n="56.1">Que</w> <w n="56.2">bat</w> <w n="56.3">la</w> <w n="56.4">pluie</w> <w n="56.5">à</w> <w n="56.6">flots</w> <w n="56.7">dans</w> <w n="56.8">toutes</w> <w n="56.9">les</w> <w n="56.10">saisons</w>,</l>
					<l n="57" num="1.57"><w n="57.1">Une</w> <w n="57.2">fumée</w> <w n="57.3">ardente</w> <w n="57.4">et</w> <w n="57.5">de</w> <w n="57.6">couleur</w> <w n="57.7">de</w> <w n="57.8">rouille</w></l>
					<l n="58" num="1.58"><w n="58.1">Traînant</w> <w n="58.2">ses</w> <w n="58.3">longs</w> <w n="58.4">anneaux</w> <w n="58.5">sur</w> <w n="58.6">le</w> <w n="58.7">ciel</w> <w n="58.8">qu</w>’<w n="58.9">elle</w> <w n="58.10">souille</w>,</l>
					<l n="59" num="1.59"><w n="59.1">Les</w> <w n="59.2">murs</w> <w n="59.3">repeints</w> <w n="59.4">à</w> <w n="59.5">neuf</w>, <w n="59.6">ou</w> <w n="59.7">noircis</w> <w n="59.8">par</w> <w n="59.9">le</w> <w n="59.10">temps</w>,</l>
					<l n="60" num="1.60"><w n="60.1">Jaunes</w>, <w n="60.2">rouges</w> <w n="60.3">et</w> <w n="60.4">verts</w>, <w n="60.5">semblables</w> <w n="60.6">aux</w> <w n="60.7">tartans</w></l>
					<l n="61" num="1.61"><w n="61.1">Des</w> <w n="61.2">montagnards</w> <w n="61.3">d</w>’<w n="61.4">Écosse</w>, <w n="61.5">et</w> <w n="61.6">les</w> <w n="61.7">vieilles</w> <w n="61.8">églises</w></l>
					<l n="62" num="1.62"><w n="62.1">Au</w> <w n="62.2">sein</w> <w n="62.3">de</w> <w n="62.4">la</w> <w n="62.5">vapeur</w> <w n="62.6">dressant</w> <w n="62.7">leurs</w> <w n="62.8">flèches</w> <w n="62.9">grises</w>,</l>
					<l n="63" num="1.63"><w n="63.1">Et</w> <w n="63.2">leurs</w> <w n="63.3">longs</w> <w n="63.4">arcs</w>-<w n="63.5">boutants</w> <w n="63.6">inclinés</w> <w n="63.7">de</w> <w n="63.8">façon</w></l>
					<l n="64" num="1.64"><w n="64.1">Qu</w>’<w n="64.2">on</w> <w n="64.3">croirait</w> <w n="64.4">à</w> <w n="64.5">les</w> <w n="64.6">voir</w> <w n="64.7">des</w> <w n="64.8">côtes</w> <w n="64.9">de</w> <w n="64.10">poisson</w> ;</l>
					<l n="65" num="1.65"><w n="65.1">Puis</w> <w n="65.2">le</w> <w n="65.3">peuple</w> <w n="65.4">grouillant</w>, <w n="65.5">qui</w> <w n="65.6">se</w> <w n="65.7">heurte</w> <w n="65.8">et</w> <w n="65.9">se</w> <w n="65.10">rue</w>,</l>
					<l n="66" num="1.66"><w n="66.1">Fashionables</w> <w n="66.2">musqués</w>, <w n="66.3">gueux</w> <w n="66.4">à</w> <w n="66.5">mine</w> <w n="66.6">incongrue</w>,</l>
					<l n="67" num="1.67"><w n="67.1">Grisettes</w> <w n="67.2">au</w> <w n="67.3">pied</w> <w n="67.4">leste</w>, <w n="67.5">au</w> <w n="67.6">sourire</w> <w n="67.7">agaçant</w>,</l>
					<l n="68" num="1.68"><w n="68.1">Beaux</w> <w n="68.2">tilburys</w> <w n="68.3">dorés</w> <w n="68.4">comme</w> <w n="68.5">l</w>’<w n="68.6">éclair</w> <w n="68.7">passant</w>,</l>
					<l n="69" num="1.69"><w n="69.1">Charrettes</w>, <w n="69.2">tombereaux</w>, <w n="69.3">ouvrant</w> <w n="69.4">avec</w> <w n="69.5">leurs</w> <w n="69.6">roues</w>,</l>
					<l n="70" num="1.70"><w n="70.1">Comme</w> <w n="70.2">des</w> <w n="70.3">nefs</w> <w n="70.4">dans</w> <w n="70.5">l</w>’<w n="70.6">onde</w>, <w n="70.7">un</w> <w n="70.8">sillon</w> <w n="70.9">dans</w> <w n="70.10">les</w> <w n="70.11">boues</w> ;</l>
					<l n="71" num="1.71">— <w n="71.1">De</w> <w n="71.2">l</w>’<w n="71.3">or</w> <w n="71.4">et</w> <w n="71.5">de</w> <w n="71.6">la</w> <w n="71.7">fange</w>. — <w n="71.8">Incroyable</w> <w n="71.9">chaos</w>,</l>
					<l n="72" num="1.72"><w n="72.1">Babel</w> <w n="72.2">des</w> <w n="72.3">nations</w>, <w n="72.4">mer</w> <w n="72.5">qui</w> <w n="72.6">bout</w> <w n="72.7">sans</w> <w n="72.8">repos</w>,</l>
					<l n="73" num="1.73"><w n="73.1">Chaudière</w> <w n="73.2">de</w> <w n="73.3">damnés</w>, <w n="73.4">cuve</w> <w n="73.5">immense</w> <w n="73.6">où</w> <w n="73.7">fermente</w>,</l>
					<l n="74" num="1.74"><w n="74.1">Vendange</w> <w n="74.2">de</w> <w n="74.3">la</w> <w n="74.4">mort</w>, <w n="74.5">une</w> <w n="74.6">foule</w> <w n="74.7">écumante</w>,</l>
					<l n="75" num="1.75"><w n="75.1">Haillons</w> <w n="75.2">troués</w> <w n="75.3">à</w> <w n="75.4">jour</w> <w n="75.5">comme</w> <w n="75.6">un</w> <w n="75.7">crible</w>, <w n="75.8">où</w> <w n="75.9">le</w> <w n="75.10">vent</w></l>
					<l n="76" num="1.76"><w n="76.1">Glisse</w> <w n="76.2">apportant</w> <w n="76.3">la</w> <w n="76.4">fièvre</w> <w n="76.5">et</w> <w n="76.6">le</w> <w n="76.7">trépas</w> <w n="76.8">souvent</w> ;</l>
					<l n="77" num="1.77"><w n="77.1">Brocarts</w> <w n="77.2">d</w>’<w n="77.3">or</w> <w n="77.4">et</w> <w n="77.5">d</w>’<w n="77.6">argent</w> <w n="77.7">roides</w> <w n="77.8">de</w> <w n="77.9">pierreries</w>,</l>
					<l n="78" num="1.78"><w n="78.1">Des</w> <w n="78.2">yeux</w> <w n="78.3">cernés</w> <w n="78.4">et</w> <w n="78.5">bleus</w>, <w n="78.6">des</w> <w n="78.7">figures</w> <w n="78.8">flétries</w>,</l>
					<l n="79" num="1.79"><w n="79.1">Du</w> <w n="79.2">pain</w> <w n="79.3">dur</w> <w n="79.4">que</w> <w n="79.5">l</w>’<w n="79.6">on</w> <w n="79.7">mange</w> <w n="79.8">à</w> <w n="79.9">la</w> <w n="79.10">sueur</w> <w n="79.11">du</w> <w n="79.12">front</w>,</l>
					<l n="80" num="1.80"><w n="80.1">Oisifs</w> <w n="80.2">de</w> <w n="80.3">leurs</w> <w n="80.4">deux</w> <w n="80.5">mains</w> <w n="80.6">frappant</w> <w n="80.7">leur</w> <w n="80.8">ventre</w> <w n="80.9">rond</w> ;</l>
					<l n="81" num="1.81"><w n="81.1">Perpétuel</w> <w n="81.2">contraste</w>, <w n="81.3">éternelle</w> <w n="81.4">antithèse</w>,</l>
					<l n="82" num="1.82"><w n="82.1">Paris</w>, <w n="82.2">la</w> <w n="82.3">bonne</w> <w n="82.4">ville</w>, <w n="82.5">ou</w> <w n="82.6">plutôt</w> <w n="82.7">la</w> <w n="82.8">mauvaise</w>,</l>
					<l n="83" num="1.83"><w n="83.1">Longs</w> <w n="83.2">grincements</w> <w n="83.3">de</w> <w n="83.4">dents</w> <w n="83.5">et</w> <w n="83.6">beaux</w> <w n="83.7">concerts</w>. <w n="83.8">Voilà</w> !</l>
					<l n="84" num="1.84">— <w n="84.1">Cependant</w> <w n="84.2">moi</w>, <w n="84.3">poëte</w> <w n="84.4">et</w> <w n="84.5">peintre</w>, <w n="84.6">je</w> <w n="84.7">vis</w> <w n="84.8">là</w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1831">1831</date>.
					</dateline>
				</closer>
			</div></body></text></TEI>