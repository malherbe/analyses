<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2184 vers2184 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Premier</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/files/44180/44180-h/44180-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Premier</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1833">1833</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Quelques erreurs orthographiques ont été repérées avec le correcteur orthogrphique Gedit.</p>
				<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU113">
				<head type="main">LE MARAIS</head>
				<head type="sub_1">A MON AMI ARMAND E***</head>
				<opener>
					<epigraph>
						<cit>
							<quote>
							Ainsi près d’un marais on contemple voler <lb></lb>
							Mille oiseaux peinturés. <lb></lb>
							</quote>
							<bibl>
								<name>Amadis Jamyn.</name>
							</bibl>
						</cit>
						<cit>
							<quote>
								En chasse, et chasse heureuse.
							</quote>
							<bibl>
								<name>Alfred de Musset.</name>
							</bibl>
						</cit>
					</epigraph>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">C</w>’<w n="1.2">est</w> <w n="1.3">un</w> <w n="1.4">marais</w> <w n="1.5">dont</w> <w n="1.6">l</w>’<w n="1.7">eau</w> <w n="1.8">dormante</w></l>
					<l n="2" num="1.2"><w n="2.1">Croupit</w>, <w n="2.2">couverte</w> <w n="2.3">d</w>’<w n="2.4">une</w> <w n="2.5">mante</w></l>
					<l n="3" num="1.3"><w n="3.1">Par</w> <w n="3.2">les</w> <w n="3.3">nénuphars</w> <w n="3.4">et</w> <w n="3.5">les</w> <w n="3.6">joncs</w> :</l>
					<l n="4" num="1.4"><w n="4.1">Chaque</w> <w n="4.2">bruit</w> <w n="4.3">sous</w> <w n="4.4">leurs</w> <w n="4.5">nappes</w> <w n="4.6">glauques</w></l>
					<l n="5" num="1.5"><w n="5.1">Fait</w> <w n="5.2">au</w> <w n="5.3">chœur</w> <w n="5.4">des</w> <w n="5.5">grenouilles</w> <w n="5.6">rauques</w></l>
					<l n="6" num="1.6"><w n="6.1">Exécuter</w> <w n="6.2">mille</w> <w n="6.3">plongeons</w> ;</l>
				</lg>
				<lg n="2">
					<l n="7" num="2.1"><w n="7.1">La</w> <w n="7.2">bécassine</w> <w n="7.3">noire</w> <w n="7.4">et</w> <w n="7.5">grise</w></l>
					<l n="8" num="2.2"><w n="8.1">Y</w> <w n="8.2">vole</w> <w n="8.3">quand</w> <w n="8.4">souffle</w> <w n="8.5">la</w> <w n="8.6">bise</w></l>
					<l n="9" num="2.3"><w n="9.1">De</w> <w n="9.2">novembre</w> <w n="9.3">aux</w> <w n="9.4">matins</w> <w n="9.5">glacés</w> ;</l>
					<l n="10" num="2.4"><w n="10.1">Souvent</w>, <w n="10.2">du</w> <w n="10.3">haut</w> <w n="10.4">des</w> <w n="10.5">sombres</w> <w n="10.6">nues</w></l>
					<l n="11" num="2.5"><w n="11.1">Pluviers</w>, <w n="11.2">vanneaux</w>, <w n="11.3">courlis</w> <w n="11.4">et</w> <w n="11.5">grues</w></l>
					<l n="12" num="2.6"><w n="12.1">Y</w> <w n="12.2">tombent</w>, <w n="12.3">d</w>’<w n="12.4">un</w> <w n="12.5">long</w> <w n="12.6">vol</w> <w n="12.7">lassés</w>.</l>
				</lg>
				<lg n="3">
					<l n="13" num="3.1"><w n="13.1">Sous</w> <w n="13.2">les</w> <w n="13.3">lentilles</w> <w n="13.4">d</w>’<w n="13.5">eau</w> <w n="13.6">qui</w> <w n="13.7">rampent</w>,</l>
					<l n="14" num="3.2"><w n="14.1">Les</w> <w n="14.2">canards</w> <w n="14.3">sauvages</w> <w n="14.4">y</w> <w n="14.5">trempent</w></l>
					<l n="15" num="3.3"><w n="15.1">Leurs</w> <w n="15.2">cous</w> <w n="15.3">de</w> <w n="15.4">saphir</w> <w n="15.5">glacés</w> <w n="15.6">d</w>’<w n="15.7">or</w> ;</l>
					<l n="16" num="3.4"><w n="16.1">La</w> <w n="16.2">sarcelle</w> <w n="16.3">à</w> <w n="16.4">l</w>’<w n="16.5">aube</w> <w n="16.6">s</w>’<w n="16.7">y</w> <w n="16.8">baigne</w>,</l>
					<l n="17" num="3.5"><w n="17.1">Et</w>, <w n="17.2">quand</w> <w n="17.3">le</w> <w n="17.4">crépuscule</w> <w n="17.5">règne</w>,</l>
					<l n="18" num="3.6"><w n="18.1">S</w>’<w n="18.2">y</w> <w n="18.3">pose</w> <w n="18.4">entre</w> <w n="18.5">deux</w> <w n="18.6">joncs</w>, <w n="18.7">et</w> <w n="18.8">dort</w>.</l>
				</lg>
				<lg n="4">
					<l n="19" num="4.1"><w n="19.1">La</w> <w n="19.2">cigogne</w> <w n="19.3">dont</w> <w n="19.4">le</w> <w n="19.5">bec</w> <w n="19.6">claque</w>,</l>
					<l n="20" num="4.2"><w n="20.1">L</w>’<w n="20.2">œil</w> <w n="20.3">tourné</w> <w n="20.4">vers</w> <w n="20.5">le</w> <w n="20.6">ciel</w> <w n="20.7">opaque</w>,</l>
					<l n="21" num="4.3"><w n="21.1">Attend</w> <w n="21.2">là</w> <w n="21.3">l</w>’<w n="21.4">instant</w> <w n="21.5">du</w> <w n="21.6">départ</w>,</l>
					<l n="22" num="4.4"><w n="22.1">Et</w> <w n="22.2">le</w> <w n="22.3">héron</w> <w n="22.4">aux</w> <w n="22.5">jambes</w> <w n="22.6">grêles</w>,</l>
					<l n="23" num="4.5"><w n="23.1">Lustrant</w> <w n="23.2">les</w> <w n="23.3">plumes</w> <w n="23.4">de</w> <w n="23.5">ses</w> <w n="23.6">ailes</w>,</l>
					<l n="24" num="4.6"><w n="24.1">Y</w> <w n="24.2">traîne</w> <w n="24.3">sa</w> <w n="24.4">vie</w> <w n="24.5">à</w> <w n="24.6">l</w>’<w n="24.7">écart</w>.</l>
				</lg>
				<lg n="5">
					<l n="25" num="5.1"><w n="25.1">Ami</w>, <w n="25.2">quand</w> <w n="25.3">la</w> <w n="25.4">brume</w> <w n="25.5">d</w>’<w n="25.6">automne</w></l>
					<l n="26" num="5.2"><w n="26.1">Étend</w> <w n="26.2">son</w> <w n="26.3">voile</w> <w n="26.4">monotone</w></l>
					<l n="27" num="5.3"><w n="27.1">Sur</w> <w n="27.2">le</w> <w n="27.3">front</w> <w n="27.4">obscurci</w> <w n="27.5">des</w> <w n="27.6">cieux</w>,</l>
					<l n="28" num="5.4"><w n="28.1">Quand</w> <w n="28.2">à</w> <w n="28.3">la</w> <w n="28.4">ville</w> <w n="28.5">tout</w> <w n="28.6">sommeille</w></l>
					<l n="29" num="5.5"><w n="29.1">Et</w> <w n="29.2">qu</w>’<w n="29.3">à</w> <w n="29.4">peine</w> <w n="29.5">le</w> <w n="29.6">jour</w> <w n="29.7">s</w>’<w n="29.8">éveille</w></l>
					<l n="30" num="5.6"><w n="30.1">A</w> <w n="30.2">l</w>’<w n="30.3">horizon</w> <w n="30.4">silencieux</w>,</l>
				</lg>
				<lg n="6">
					<l n="31" num="6.1"><w n="31.1">Toi</w> <w n="31.2">dont</w> <w n="31.3">le</w> <w n="31.4">plomb</w> <w n="31.5">à</w> <w n="31.6">l</w>’<w n="31.7">hirondelle</w></l>
					<l n="32" num="6.2"><w n="32.1">Toujours</w> <w n="32.2">porte</w> <w n="32.3">une</w> <w n="32.4">mort</w> <w n="32.5">fidèle</w>,</l>
					<l n="33" num="6.3"><w n="33.1">Toi</w> <w n="33.2">qui</w> <w n="33.3">jamais</w> <w n="33.4">à</w> <w n="33.5">trente</w> <w n="33.6">pas</w></l>
					<l n="34" num="6.4"><w n="34.1">N</w>’<w n="34.2">as</w> <w n="34.3">manqué</w> <w n="34.4">le</w> <w n="34.5">lièvre</w> <w n="34.6">rapide</w>,</l>
					<l n="35" num="6.5"><w n="35.1">Ami</w>, <w n="35.2">toi</w>, <w n="35.3">chasseur</w> <w n="35.4">intrépide</w>,</l>
					<l n="36" num="6.6"><w n="36.1">Qu</w>’<w n="36.2">un</w> <w n="36.3">long</w> <w n="36.4">chemin</w> <w n="36.5">n</w>’<w n="36.6">arrête</w> <w n="36.7">pas</w> ;</l>
				</lg>
				<lg n="7">
					<l n="37" num="7.1"><w n="37.1">Avec</w> <w n="37.2">Rasko</w>, <w n="37.3">ton</w> <w n="37.4">chien</w> <w n="37.5">qui</w> <w n="37.6">saute</w></l>
					<l n="38" num="7.2"><w n="38.1">A</w> <w n="38.2">ta</w> <w n="38.3">suite</w> <w n="38.4">dans</w> <w n="38.5">l</w>’<w n="38.6">herbe</w> <w n="38.7">haute</w>,</l>
					<l n="39" num="7.3"><w n="39.1">Avec</w> <w n="39.2">ton</w> <w n="39.3">bon</w> <w n="39.4">fusil</w> <w n="39.5">bronzé</w>,</l>
					<l n="40" num="7.4"><w n="40.1">Ta</w> <w n="40.2">blouse</w> <w n="40.3">et</w> <w n="40.4">tout</w> <w n="40.5">ton</w> <w n="40.6">équipage</w>,</l>
					<l n="41" num="7.5"><w n="41.1">Viens</w> <w n="41.2">t</w>’<w n="41.3">y</w> <w n="41.4">cacher</w> <w n="41.5">près</w> <w n="41.6">du</w> <w n="41.7">rivage</w>,</l>
					<l n="42" num="7.6"><w n="42.1">Derrière</w> <w n="42.2">un</w> <w n="42.3">tronc</w> <w n="42.4">d</w>’<w n="42.5">arbre</w> <w n="42.6">brisé</w>.</l>
				</lg>
				<lg n="8">
					<l n="43" num="8.1"><w n="43.1">Ta</w> <w n="43.2">chasse</w> <w n="43.3">sera</w> <w n="43.4">meurtrière</w> ;</l>
					<l n="44" num="8.2"><w n="44.1">Aux</w> <w n="44.2">mailles</w> <w n="44.3">de</w> <w n="44.4">ta</w> <w n="44.5">carnassière</w></l>
					<l n="45" num="8.3"><w n="45.1">Bien</w> <w n="45.2">des</w> <w n="45.3">pieds</w> <w n="45.4">d</w>’<w n="45.5">oiseaux</w> <w n="45.6">passeront</w>,</l>
					<l n="46" num="8.4"><w n="46.1">Et</w> <w n="46.2">tu</w> <w n="46.3">reviendras</w> <w n="46.4">de</w> <w n="46.5">bonne</w> <w n="46.6">heure</w>,</l>
					<l n="47" num="8.5"><w n="47.1">Avant</w> <w n="47.2">le</w> <w n="47.3">soir</w>, <w n="47.4">en</w> <w n="47.5">ta</w> <w n="47.6">demeure</w>,</l>
					<l n="48" num="8.6"><w n="48.1">La</w> <w n="48.2">joie</w> <w n="48.3">au</w> <w n="48.4">cœur</w>, <w n="48.5">l</w>’<w n="48.6">orgueil</w> <w n="48.7">au</w> <w n="48.8">front</w>.</l>
				</lg>
			</div></body></text></TEI>