<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2184 vers2184 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Premier</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/files/44180/44180-h/44180-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Premier</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1833">1833</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Quelques erreurs orthographiques ont été repérées avec le correcteur orthogrphique Gedit.</p>
				<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU166">
				<head type="main">UN VERS DE WORDSWORTH</head>
				<opener>
					<epigraph>
						<cit>
							<quote>
								Spires whose silent finger points to heaven.
							</quote>
						</cit>
					</epigraph>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Je</w> <w n="1.2">n</w>’<w n="1.3">ai</w> <w n="1.4">jamais</w> <w n="1.5">rien</w> <w n="1.6">lu</w> <w n="1.7">de</w> <w n="1.8">Wordsworth</w>, <w n="1.9">le</w> <w n="1.10">poëte</w></l>
					<l n="2" num="1.2"><w n="2.1">Dont</w> <w n="2.2">parle</w> <w n="2.3">lord</w> <w n="2.4">Byron</w> <w n="2.5">d</w>’<w n="2.6">un</w> <w n="2.7">ton</w> <w n="2.8">si</w> <w n="2.9">plein</w> <w n="2.10">de</w> <w n="2.11">fiel</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Qu</w>’<w n="3.2">un</w> <w n="3.3">seul</w> <w n="3.4">vers</w> ; <w n="3.5">le</w> <w n="3.6">voici</w>, <w n="3.7">car</w> <w n="3.8">je</w> <w n="3.9">l</w>’<w n="3.10">ai</w> <w n="3.11">dans</w> <w n="3.12">la</w> <w n="3.13">tête</w> :</l>
					<l n="4" num="1.4">—<hi rend="ital"><w n="4.1">Clochers</w> <w n="4.2">silencieux</w> <w n="4.3">montrant</w> <w n="4.4">du</w> <w n="4.5">doigt</w> <w n="4.6">le</w> <w n="4.7">ciel</w>.</hi> —</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Il</w> <w n="5.2">servait</w> <w n="5.3">d</w>’<w n="5.4">épigraphe</w>, <w n="5.5">et</w> <w n="5.6">c</w>’<w n="5.7">était</w> <w n="5.8">bien</w> <w n="5.9">étrange</w>,</l>
					<l n="6" num="2.2"><w n="6.1">Au</w> <w n="6.2">chapitre</w> <w n="6.3">premier</w> <w n="6.4">d</w>’<w n="6.5">un</w> <w n="6.6">roman</w> : —<hi rend="ital"><w n="6.7">Louisa</w></hi>, —</l>
					<l n="7" num="2.3"><w n="7.1">Les</w> <w n="7.2">douleurs</w> <w n="7.3">d</w>’<w n="7.4">une</w> <w n="7.5">fille</w>, <w n="7.6">œuvre</w> <w n="7.7">toute</w> <w n="7.8">de</w> <w n="7.9">fange</w></l>
					<l n="8" num="2.4"><w n="8.1">Qu</w>’<w n="8.2">un</w> <w n="8.3">pseudonyme</w> <w n="8.4">auteur</w> <w n="8.5">dans</w> <w n="8.6">l</w>’<hi rend="ital"><w n="8.7">Ane</w> <w n="8.8">mort</w></hi> <w n="8.9">puisa</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Ce</w> <w n="9.2">vers</w> <w n="9.3">frais</w> <w n="9.4">et</w> <w n="9.5">pieux</w>, <w n="9.6">perdu</w> <w n="9.7">dans</w> <w n="9.8">ce</w> <w n="9.9">volume</w></l>
					<l n="10" num="3.2"><w n="10.1">De</w> <w n="10.2">lubriques</w> <w n="10.3">amours</w>, <w n="10.4">me</w> <w n="10.5">fit</w> <w n="10.6">du</w> <w n="10.7">bien</w> <w n="10.8">à</w> <w n="10.9">voir</w> :</l>
					<l n="11" num="3.3"><w n="11.1">C</w>’<w n="11.2">était</w> <w n="11.3">comme</w> <w n="11.4">une</w> <w n="11.5">fleur</w> <w n="11.6">des</w> <w n="11.7">champs</w>, <w n="11.8">comme</w> <w n="11.9">une</w> <w n="11.10">plume</w></l>
					<l n="12" num="3.4"><w n="12.1">De</w> <w n="12.2">colombe</w>, <w n="12.3">tombée</w> <w n="12.4">au</w> <w n="12.5">cœur</w> <w n="12.6">d</w>’<w n="12.7">un</w> <w n="12.8">bourbier</w> <w n="12.9">noir</w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Aussi</w> <w n="13.2">depuis</w> <w n="13.3">ce</w> <w n="13.4">temps</w>, <w n="13.5">lorsque</w> <w n="13.6">la</w> <w n="13.7">rime</w> <w n="13.8">boite</w>,</l>
					<l n="14" num="4.2"><w n="14.1">Que</w> <w n="14.2">Prospéro</w> <w n="14.3">n</w>’<w n="14.4">est</w> <w n="14.5">pas</w> <w n="14.6">obéi</w> <w n="14.7">d</w>’<w n="14.8">Ariel</w>,</l>
					<l n="15" num="4.3"><w n="15.1">Aux</w> <w n="15.2">marges</w> <w n="15.3">du</w> <w n="15.4">papier</w> <w n="15.5">je</w> <w n="15.6">jette</w>, <w n="15.7">à</w> <w n="15.8">gauche</w>, <w n="15.9">à</w> <w n="15.10">droite</w>,</l>
					<l n="16" num="4.4"><w n="16.1">Des</w> <w n="16.2">dessins</w> <w n="16.3">de</w> <w n="16.4">clochers</w> <w n="16.5">montrant</w> <w n="16.6">du</w> <w n="16.7">doigt</w> <w n="16.8">le</w> <w n="16.9">ciel</w>.</l>
				</lg>
			</div></body></text></TEI>