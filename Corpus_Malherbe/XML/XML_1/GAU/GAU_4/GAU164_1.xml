<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2184 vers2184 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Premier</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/files/44180/44180-h/44180-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Premier</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1833">1833</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Quelques erreurs orthographiques ont été repérées avec le correcteur orthogrphique Gedit.</p>
				<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU164">
				<head type="main">SONNET VII</head>
				<opener>
					<epigraph>
						<cit>
							<quote>
								Liberté de juillet ! femme au buste divin, <lb></lb>
								Et dont le corps finit en queue !
							</quote>
							<bibl>
								<name>G. de Nerval</name>
							</bibl>
						</cit>
						<cit>
							<quote>
								E la lor cieca vita è tanto bassa <lb></lb>
								ch’invidiosi son d’ogn’altra sorte.
							</quote>
							<bibl>
								<hi rend="ital">Inferno, canto</hi> III.
							</bibl>
						</cit>
					</epigraph>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Avec</w> <w n="1.2">ce</w> <w n="1.3">siècle</w> <w n="1.4">infâme</w> <w n="1.5">il</w> <w n="1.6">est</w> <w n="1.7">temps</w> <w n="1.8">que</w> <w n="1.9">l</w>’<w n="1.10">on</w> <w n="1.11">rompe</w> ;</l>
					<l n="2" num="1.2"><w n="2.1">Car</w> <w n="2.2">à</w> <w n="2.3">son</w> <w n="2.4">front</w> <w n="2.5">damné</w> <w n="2.6">le</w> <w n="2.7">doigt</w> <w n="2.8">fatal</w> <w n="2.9">a</w> <w n="2.10">mis</w></l>
					<l n="3" num="1.3"><w n="3.1">Comme</w> <w n="3.2">aux</w> <w n="3.3">portes</w> <w n="3.4">d</w>’<w n="3.5">enfer</w> : <w n="3.6">Plus</w> <w n="3.7">d</w>’<w n="3.8">espérance</w> ! — <w n="3.9">Amis</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Ennemis</w>, <w n="4.2">peuples</w>, <w n="4.3">rois</w>, <w n="4.4">tout</w> <w n="4.5">nous</w> <w n="4.6">joue</w> <w n="4.7">et</w> <w n="4.8">nous</w> <w n="4.9">trompe</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Un</w> <w n="5.2">budget</w> <w n="5.3">éléphant</w> <w n="5.4">boit</w> <w n="5.5">notre</w> <w n="5.6">or</w> <w n="5.7">par</w> <w n="5.8">sa</w> <w n="5.9">trompe</w>.</l>
					<l n="6" num="2.2"><w n="6.1">Dans</w> <w n="6.2">leurs</w> <w n="6.3">trônes</w> <w n="6.4">d</w>’<w n="6.5">hier</w> <w n="6.6">encor</w> <w n="6.7">mal</w> <w n="6.8">affermis</w>,</l>
					<l n="7" num="2.3"><w n="7.1">De</w> <w n="7.2">leurs</w> <w n="7.3">aînés</w> <w n="7.4">déchus</w> <w n="7.5">ils</w> <w n="7.6">gardent</w> <w n="7.7">tout</w>, <w n="7.8">hormis</w></l>
					<l n="8" num="2.4"><w n="8.1">La</w> <w n="8.2">main</w> <w n="8.3">prompte</w> <w n="8.4">à</w> <w n="8.5">s</w>’<w n="8.6">ouvrir</w>, <w n="8.7">et</w> <w n="8.8">la</w> <w n="8.9">royale</w> <w n="8.10">pompe</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Cependant</w> <w n="9.2">en</w> <w n="9.3">juillet</w>, <w n="9.4">sous</w> <w n="9.5">le</w> <w n="9.6">ciel</w> <w n="9.7">indigo</w>,</l>
					<l n="10" num="3.2"><w n="10.1">Sur</w> <w n="10.2">les</w> <w n="10.3">pavés</w> <w n="10.4">mouvants</w> <w n="10.5">ils</w> <w n="10.6">ont</w> <w n="10.7">fait</w> <w n="10.8">des</w> <w n="10.9">promesses</w></l>
					<l n="11" num="3.3"><w n="11.1">Autant</w> <w n="11.2">que</w> <w n="11.3">Charles</w> <w n="11.4">dix</w> <w n="11.5">avait</w> <w n="11.6">ouï</w> <w n="11.7">de</w> <w n="11.8">messes</w> !</l>
				</lg>
				<lg n="4">
					<l n="12" num="4.1"><w n="12.1">Seule</w>, <w n="12.2">la</w> <w n="12.3">poésie</w> <w n="12.4">incarnée</w> <w n="12.5">en</w> <w n="12.6">Hugo</w></l>
					<l n="13" num="4.2"><w n="13.1">Ne</w> <w n="13.2">nous</w> <w n="13.3">a</w> <w n="13.4">pas</w> <w n="13.5">déçus</w>, <w n="13.6">et</w> <w n="13.7">de</w> <w n="13.8">palmes</w> <w n="13.9">divines</w></l>
					<l n="14" num="4.3"><w n="14.1">Vers</w> <w n="14.2">l</w>’<w n="14.3">avenir</w> <w n="14.4">tournée</w> <w n="14.5">ombrage</w> <w n="14.6">nos</w> <w n="14.7">ruines</w>.</l>
				</lg>
			</div></body></text></TEI>