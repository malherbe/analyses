<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2184 vers2184 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Premier</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/files/44180/44180-h/44180-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Premier</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1833">1833</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Quelques erreurs orthographiques ont été repérées avec le correcteur orthogrphique Gedit.</p>
				<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU136">
				<head type="main">VOYAGE</head>
				<opener>
					<epigraph>
						<cit>
							<quote>
								Il me faut du nouveau n’en fût-il plus au monde.
							</quote>
							<bibl>
								<name>Jean de La Fontaine.</name>
							</bibl>
						</cit>
						<cit>
							<quote>
								Jam mens prætrepidans avet vagari, <lb></lb>
							Jam læti studio pedes vigescunt.
							</quote>
							<bibl>
								<name>Catulle.</name>
							</bibl>
						</cit>
					</epigraph>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Au</w> <w n="1.2">travers</w> <w n="1.3">de</w> <w n="1.4">la</w> <w n="1.5">vitre</w> <w n="1.6">blanche</w></l>
					<l n="2" num="1.2"><w n="2.1">Le</w> <w n="2.2">soleil</w> <w n="2.3">rit</w>, <w n="2.4">et</w> <w n="2.5">sur</w> <w n="2.6">les</w> <w n="2.7">murs</w></l>
					<l n="3" num="1.3"><w n="3.1">Traçant</w> <w n="3.2">de</w> <w n="3.3">grands</w> <w n="3.4">angles</w>, <w n="3.5">épanche</w></l>
					<l n="4" num="1.4"><w n="4.1">Ses</w> <w n="4.2">rayons</w> <w n="4.3">splendides</w> <w n="4.4">et</w> <w n="4.5">purs</w> :</l>
					<l n="5" num="1.5"><w n="5.1">Par</w> <w n="5.2">un</w> <w n="5.3">si</w> <w n="5.4">beau</w> <w n="5.5">temps</w>, <w n="5.6">à</w> <w n="5.7">la</w> <w n="5.8">ville</w></l>
					<l n="6" num="1.6"><w n="6.1">Rester</w> <w n="6.2">parmi</w> <w n="6.3">la</w> <w n="6.4">foule</w> <w n="6.5">vile</w> !</l>
					<l n="7" num="1.7"><w n="7.1">Je</w> <w n="7.2">veux</w> <w n="7.3">voir</w> <w n="7.4">des</w> <w n="7.5">sites</w> <w n="7.6">nouveaux</w> :</l>
					<l n="8" num="1.8"><w n="8.1">Postillons</w>, <w n="8.2">sellez</w> <w n="8.3">vos</w> <w n="8.4">chevaux</w>.</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1"><w n="9.1">Au</w> <w n="9.2">sein</w> <w n="9.3">d</w>’<w n="9.4">un</w> <w n="9.5">nuage</w> <w n="9.6">de</w> <w n="9.7">poudre</w>,</l>
					<l n="10" num="2.2"><w n="10.1">Par</w> <w n="10.2">un</w> <w n="10.3">galop</w> <w n="10.4">précipité</w>,</l>
					<l n="11" num="2.3"><w n="11.1">Aussi</w> <w n="11.2">promptement</w> <w n="11.3">que</w> <w n="11.4">la</w> <w n="11.5">foudre</w></l>
					<l n="12" num="2.4"><w n="12.1">Comme</w> <w n="12.2">il</w> <w n="12.3">est</w> <w n="12.4">doux</w> <w n="12.5">d</w>’<w n="12.6">être</w> <w n="12.7">emporté</w> !</l>
					<l n="13" num="2.5"><w n="13.1">Le</w> <w n="13.2">sable</w> <w n="13.3">bruit</w> <w n="13.4">sous</w> <w n="13.5">la</w> <w n="13.6">roue</w>,</l>
					<l n="14" num="2.6"><w n="14.1">Le</w> <w n="14.2">vent</w> <w n="14.3">autour</w> <w n="14.4">de</w> <w n="14.5">vous</w> <w n="14.6">se</w> <w n="14.7">joue</w> ;</l>
					<l n="15" num="2.7"><w n="15.1">Je</w> <w n="15.2">veux</w> <w n="15.3">voir</w> <w n="15.4">des</w> <w n="15.5">sites</w> <w n="15.6">nouveaux</w> :</l>
					<l n="16" num="2.8"><w n="16.1">Postillons</w>, <w n="16.2">pressez</w> <w n="16.3">vos</w> <w n="16.4">chevaux</w>.</l>
				</lg>
				<lg n="3">
					<l n="17" num="3.1"><w n="17.1">Les</w> <w n="17.2">arbres</w> <w n="17.3">qui</w> <w n="17.4">bordent</w> <w n="17.5">la</w> <w n="17.6">route</w></l>
					<l n="18" num="3.2"><w n="18.1">Paraissent</w> <w n="18.2">fuir</w> <w n="18.3">rapidement</w>,</l>
					<l n="19" num="3.3"><w n="19.1">Leur</w> <w n="19.2">forme</w> <w n="19.3">obscure</w> <w n="19.4">dont</w> <w n="19.5">l</w>’<w n="19.6">œil</w> <w n="19.7">doute</w></l>
					<l n="20" num="3.4"><w n="20.1">Ne</w> <w n="20.2">se</w> <w n="20.3">dessine</w> <w n="20.4">qu</w>’<w n="20.5">un</w> <w n="20.6">moment</w> ;</l>
					<l n="21" num="3.5"><w n="21.1">Le</w> <w n="21.2">ciel</w>, <w n="21.3">tel</w> <w n="21.4">qu</w>’<w n="21.5">une</w> <w n="21.6">banderole</w>,</l>
					<l n="22" num="3.6"><w n="22.1">Par</w>-<w n="22.2">dessus</w> <w n="22.3">les</w> <w n="22.4">bois</w> <w n="22.5">roule</w> <w n="22.6">et</w> <w n="22.7">vole</w> ;</l>
					<l n="23" num="3.7"><w n="23.1">Je</w> <w n="23.2">veux</w> <w n="23.3">voir</w> <w n="23.4">des</w> <w n="23.5">sites</w> <w n="23.6">nouveaux</w> :</l>
					<l n="24" num="3.8"><w n="24.1">Postillons</w>, <w n="24.2">pressez</w> <w n="24.3">vos</w> <w n="24.4">chevaux</w>.</l>
				</lg>
				<lg n="4">
					<l n="25" num="4.1"><w n="25.1">Chaumières</w>, <w n="25.2">fermes</w> <w n="25.3">isolées</w>,</l>
					<l n="26" num="4.2"><w n="26.1">Vieux</w> <w n="26.2">châteaux</w> <w n="26.3">que</w> <w n="26.4">flanque</w> <w n="26.5">une</w> <w n="26.6">tour</w>,</l>
					<l n="27" num="4.3"><w n="27.1">Monts</w> <w n="27.2">arides</w>, <w n="27.3">fraîches</w> <w n="27.4">vallées</w>,</l>
					<l n="28" num="4.4"><w n="28.1">Forêts</w> <w n="28.2">se</w> <w n="28.3">suivent</w> <w n="28.4">tour</w> <w n="28.5">à</w> <w n="28.6">tour</w> ;</l>
					<l n="29" num="4.5"><w n="29.1">Parfois</w> <w n="29.2">au</w> <w n="29.3">milieu</w> <w n="29.4">d</w>’<w n="29.5">une</w> <w n="29.6">brume</w>,</l>
					<l n="30" num="4.6"><w n="30.1">Un</w> <w n="30.2">ruisseau</w> <w n="30.3">dont</w> <w n="30.4">la</w> <w n="30.5">chute</w> <w n="30.6">écume</w> ;</l>
					<l n="31" num="4.7"><w n="31.1">Je</w> <w n="31.2">veux</w> <w n="31.3">voir</w> <w n="31.4">des</w> <w n="31.5">sites</w> <w n="31.6">nouveaux</w> :</l>
					<l n="32" num="4.8"><w n="32.1">Postillons</w>, <w n="32.2">pressez</w> <w n="32.3">vos</w> <w n="32.4">chevaux</w>.</l>
				</lg>
				<lg n="5">
					<l n="33" num="5.1"><w n="33.1">Puis</w>, <w n="33.2">une</w> <w n="33.3">hirondelle</w> <w n="33.4">qui</w> <w n="33.5">passe</w>,</l>
					<l n="34" num="5.2"><w n="34.1">Rasant</w> <w n="34.2">la</w> <w n="34.3">grève</w> <w n="34.4">au</w> <w n="34.5">sable</w> <w n="34.6">d</w>’<w n="34.7">or</w>,</l>
					<l n="35" num="5.3"><w n="35.1">Puis</w>, <w n="35.2">semés</w> <w n="35.3">dans</w> <w n="35.4">un</w> <w n="35.5">large</w> <w n="35.6">espace</w>,</l>
					<l n="36" num="5.4"><w n="36.1">Les</w> <w n="36.2">moutons</w> <w n="36.3">d</w>’<w n="36.4">un</w> <w n="36.5">berger</w> <w n="36.6">qui</w> <w n="36.7">dort</w> ;</l>
					<l n="37" num="5.5"><w n="37.1">De</w> <w n="37.2">grandes</w> <w n="37.3">perspectives</w> <w n="37.4">bleues</w>,</l>
					<l n="38" num="5.6"><w n="38.1">Larges</w> <w n="38.2">et</w> <w n="38.3">longues</w> <w n="38.4">de</w> <w n="38.5">vingt</w> <w n="38.6">lieues</w> ;</l>
					<l n="39" num="5.7"><w n="39.1">Je</w> <w n="39.2">veux</w> <w n="39.3">voir</w> <w n="39.4">des</w> <w n="39.5">sites</w> <w n="39.6">nouveaux</w> :</l>
					<l n="40" num="5.8"><w n="40.1">Postillons</w>, <w n="40.2">pressez</w> <w n="40.3">vos</w> <w n="40.4">chevaux</w>.</l>
				</lg>
				<lg n="6">
					<l n="41" num="6.1"><w n="41.1">Une</w> <w n="41.2">montagne</w> : <w n="41.3">l</w>’<w n="41.4">on</w> <w n="41.5">enraye</w>,</l>
					<l n="42" num="6.2"><w n="42.1">Au</w> <w n="42.2">bord</w> <w n="42.3">du</w> <w n="42.4">rapide</w> <w n="42.5">penchant</w></l>
					<l n="43" num="6.3"><w n="43.1">D</w>’<w n="43.2">un</w> <w n="43.3">mont</w> <w n="43.4">dont</w> <w n="43.5">la</w> <w n="43.6">hauteur</w> <w n="43.7">effraye</w> :</l>
					<l n="44" num="6.4"><w n="44.1">Les</w> <w n="44.2">chevaux</w> <w n="44.3">glissent</w> <w n="44.4">en</w> <w n="44.5">marchant</w>,</l>
					<l n="45" num="6.5"><w n="45.1">L</w>’<w n="45.2">essieu</w> <w n="45.3">grince</w>, <w n="45.4">le</w> <w n="45.5">pavé</w> <w n="45.6">fume</w>,</l>
					<l n="46" num="6.6"><w n="46.1">Et</w> <w n="46.2">la</w> <w n="46.3">roue</w> <w n="46.4">un</w> <w n="46.5">instant</w> <w n="46.6">s</w>’<w n="46.7">allume</w> ;</l>
					<l n="47" num="6.7"><w n="47.1">Je</w> <w n="47.2">veux</w> <w n="47.3">voir</w> <w n="47.4">des</w> <w n="47.5">sites</w> <w n="47.6">nouveaux</w> :</l>
					<l n="48" num="6.8"><w n="48.1">Postillons</w>, <w n="48.2">pressez</w> <w n="48.3">vos</w> <w n="48.4">chevaux</w>.</l>
				</lg>
				<lg n="7">
					<l n="49" num="7.1"><w n="49.1">La</w> <w n="49.2">côte</w> <w n="49.3">raide</w> <w n="49.4">est</w> <w n="49.5">descendue</w>.</l>
					<l n="50" num="7.2"><w n="50.1">Recouverte</w> <w n="50.2">de</w> <w n="50.3">sable</w> <w n="50.4">fin</w>,</l>
					<l n="51" num="7.3"><w n="51.1">La</w> <w n="51.2">route</w>, <w n="51.3">à</w> <w n="51.4">chaque</w> <w n="51.5">instant</w> <w n="51.6">perdue</w>,</l>
					<l n="52" num="7.4"><w n="52.1">S</w>’<w n="52.2">étend</w> <w n="52.3">comme</w> <w n="52.4">un</w> <w n="52.5">ruban</w> <w n="52.6">sans</w> <w n="52.7">fin</w>.</l>
					<l n="53" num="7.5"><w n="53.1">Que</w> <w n="53.2">cette</w> <w n="53.3">plaine</w> <w n="53.4">est</w> <w n="53.5">monotone</w> !</l>
					<l n="54" num="7.6"><w n="54.1">On</w> <w n="54.2">dirait</w> <w n="54.3">un</w> <w n="54.4">matin</w> <w n="54.5">d</w>’<w n="54.6">automne</w>,</l>
					<l n="55" num="7.7"><w n="55.1">Je</w> <w n="55.2">veux</w> <w n="55.3">voir</w> <w n="55.4">des</w> <w n="55.5">sites</w> <w n="55.6">nouveaux</w> :</l>
					<l n="56" num="7.8"><w n="56.1">Postillons</w>, <w n="56.2">pressez</w> <w n="56.3">vos</w> <w n="56.4">chevaux</w>.</l>
				</lg>
				<lg n="8">
					<l n="57" num="8.1"><w n="57.1">Une</w> <w n="57.2">ville</w> <w n="57.3">d</w>’<w n="57.4">un</w> <w n="57.5">aspect</w> <w n="57.6">sombre</w>,</l>
					<l n="58" num="8.2"><w n="58.1">Avec</w> <w n="58.2">ses</w> <w n="58.3">tours</w> <w n="58.4">et</w> <w n="58.5">ses</w> <w n="58.6">clochers</w></l>
					<l n="59" num="8.3"><w n="59.1">Qui</w> <w n="59.2">montent</w> <w n="59.3">dans</w> <w n="59.4">les</w> <w n="59.5">airs</w>, <w n="59.6">sans</w> <w n="59.7">nombre</w>,</l>
					<l n="60" num="8.4"><w n="60.1">Comme</w> <w n="60.2">des</w> <w n="60.3">mâts</w> <w n="60.4">ou</w> <w n="60.5">des</w> <w n="60.6">rochers</w>,</l>
					<l n="61" num="8.5"><w n="61.1">Où</w> <w n="61.2">mille</w> <w n="61.3">lumières</w> <w n="61.4">flamboient</w></l>
					<l n="62" num="8.6"><w n="62.1">Au</w> <w n="62.2">sein</w> <w n="62.3">des</w> <w n="62.4">ombres</w> <w n="62.5">qui</w> <w n="62.6">la</w> <w n="62.7">noient</w> ;</l>
					<l n="63" num="8.7"><w n="63.1">Je</w> <w n="63.2">veux</w> <w n="63.3">voir</w> <w n="63.4">des</w> <w n="63.5">sites</w> <w n="63.6">nouveaux</w> :</l>
					<l n="64" num="8.8"><w n="64.1">Postillons</w>, <w n="64.2">pressez</w> <w n="64.3">vos</w> <w n="64.4">chevaux</w> !</l>
				</lg>
				<lg n="9">
					<l n="65" num="9.1"><w n="65.1">Mais</w> <w n="65.2">ils</w> <w n="65.3">sont</w> <w n="65.4">las</w>, <w n="65.5">et</w> <w n="65.6">leurs</w> <w n="65.7">narines</w>,</l>
					<l n="66" num="9.2"><w n="66.1">Rouges</w> <w n="66.2">de</w> <w n="66.3">sang</w>, <w n="66.4">soufflent</w> <w n="66.5">du</w> <w n="66.6">feu</w> ;</l>
					<l n="67" num="9.3"><w n="67.1">L</w>’<w n="67.2">écume</w> <w n="67.3">inonde</w> <w n="67.4">leurs</w> <w n="67.5">poitrines</w></l>
					<l n="68" num="9.4"><w n="68.1">Il</w> <w n="68.2">faut</w> <w n="68.3">nous</w> <w n="68.4">arrêter</w> <w n="68.5">un</w> <w n="68.6">peu</w>.</l>
					<l n="69" num="9.5"><w n="69.1">Halte</w> ! <w n="69.2">demain</w>, <w n="69.3">plus</w> <w n="69.4">vite</w> <w n="69.5">encore</w>,</l>
					<l n="70" num="9.6"><w n="70.1">Aussitôt</w> <w n="70.2">que</w> <w n="70.3">poindra</w> <w n="70.4">l</w>’<w n="70.5">aurore</w>,</l>
					<l n="71" num="9.7"><w n="71.1">Postillons</w>, <w n="71.2">pressez</w> <w n="71.3">vos</w> <w n="71.4">chevaux</w>,</l>
					<l n="72" num="9.8"><w n="72.1">Je</w> <w n="72.2">veux</w> <w n="72.3">voir</w> <w n="72.4">des</w> <w n="72.5">sites</w> <w n="72.6">nouveaux</w>.</l>
				</lg>
			</div></body></text></TEI>