<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2184 vers2184 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Premier</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/files/44180/44180-h/44180-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Premier</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1833">1833</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Quelques erreurs orthographiques ont été repérées avec le correcteur orthogrphique Gedit.</p>
				<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU108">
				<head type="main">MÉDITATION</head>
				<opener>
					<epigraph>
						<cit>
							<quote>
								… Ce monde où les meilleures choses <lb></lb>
							Ont le pire destin.
							</quote>
							<bibl>
								<name>Malherbe</name>.
							</bibl>
						</cit>
				</epigraph>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Virginité</w> <w n="1.2">du</w> <w n="1.3">cœur</w>, <w n="1.4">hélas</w> ! <w n="1.5">sitôt</w> <w n="1.6">ravie</w> !</l>
					<l n="2" num="1.2"><w n="2.1">Songes</w> <w n="2.2">riants</w>, <w n="2.3">projets</w> <w n="2.4">de</w> <w n="2.5">bonheur</w> <w n="2.6">et</w> <w n="2.7">d</w>’<w n="2.8">amour</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Fraîches</w> <w n="3.2">illusions</w> <w n="3.3">du</w> <w n="3.4">matin</w> <w n="3.5">de</w> <w n="3.6">la</w> <w n="3.7">vie</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Pourquoi</w> <w n="4.2">ne</w> <w n="4.3">pas</w> <w n="4.4">durer</w> <w n="4.5">jusqu</w>’<w n="4.6">à</w> <w n="4.7">la</w> <w n="4.8">fin</w> <w n="4.9">du</w> <w n="4.10">jour</w> ?</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Pourquoi</w> ?… <w n="5.2">Ne</w> <w n="5.3">voit</w>-<w n="5.4">on</w> <w n="5.5">pas</w> <w n="5.6">qu</w>’<w n="5.7">à</w> <w n="5.8">midi</w> <w n="5.9">la</w> <w n="5.10">rosée</w></l>
					<l n="6" num="2.2"><w n="6.1">De</w> <w n="6.2">ses</w> <w n="6.3">larmes</w> <w n="6.4">d</w>’<w n="6.5">argent</w> <w n="6.6">n</w>’<w n="6.7">enrichit</w> <w n="6.8">plus</w> <w n="6.9">les</w> <w n="6.10">fleurs</w>,</l>
					<l n="7" num="2.3"><w n="7.1">Que</w> <w n="7.2">l</w>’<w n="7.3">anémone</w> <w n="7.4">frêle</w>, <w n="7.5">au</w> <w n="7.6">vent</w> <w n="7.7">froid</w> <w n="7.8">exposée</w>,</l>
					<l n="8" num="2.4"><w n="8.1">Avant</w> <w n="8.2">le</w> <w n="8.3">soir</w> <w n="8.4">n</w>’<w n="8.5">a</w> <w n="8.6">plus</w> <w n="8.7">ses</w> <w n="8.8">brillantes</w> <w n="8.9">couleurs</w> ?</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Ne</w> <w n="9.2">voit</w>-<w n="9.3">on</w> <w n="9.4">pas</w> <w n="9.5">qu</w>’<w n="9.6">une</w> <w n="9.7">onde</w>, <w n="9.8">à</w> <w n="9.9">sa</w> <w n="9.10">source</w> <w n="9.11">limpide</w>,</l>
					<l n="10" num="3.2"><w n="10.1">En</w> <w n="10.2">passant</w> <w n="10.3">par</w> <w n="10.4">la</w> <w n="10.5">fange</w> <w n="10.6">y</w> <w n="10.7">perd</w> <w n="10.8">sa</w> <w n="10.9">pureté</w> ;</l>
					<l n="11" num="3.3"><w n="11.1">Que</w> <w n="11.2">d</w>’<w n="11.3">un</w> <w n="11.4">ciel</w> <w n="11.5">d</w>’<w n="11.6">abord</w> <w n="11.7">pur</w> <w n="11.8">un</w> <w n="11.9">nuage</w> <w n="11.10">rapide</w></l>
					<l n="12" num="3.4"><w n="12.1">Bientôt</w> <w n="12.2">ternit</w> <w n="12.3">l</w>’<w n="12.4">éclat</w> <w n="12.5">et</w> <w n="12.6">la</w> <w n="12.7">sérénité</w> ?</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Le</w> <w n="13.2">monde</w> <w n="13.3">est</w> <w n="13.4">fait</w> <w n="13.5">ainsi</w> : <w n="13.6">loi</w> <w n="13.7">suprême</w> <w n="13.8">et</w> <w n="13.9">funeste</w> !</l>
					<l n="14" num="4.2"><w n="14.1">Comme</w> <w n="14.2">l</w>’<w n="14.3">ombre</w> <w n="14.4">d</w>’<w n="14.5">un</w> <w n="14.6">songe</w> <w n="14.7">au</w> <w n="14.8">bout</w> <w n="14.9">de</w> <w n="14.10">peu</w> <w n="14.11">d</w>’<w n="14.12">instants</w></l>
					<l n="15" num="4.3"><w n="15.1">Ce</w> <w n="15.2">qui</w> <w n="15.3">charme</w> <w n="15.4">s</w>’<w n="15.5">en</w> <w n="15.6">va</w>, <w n="15.7">ce</w> <w n="15.8">qui</w> <w n="15.9">fait</w> <w n="15.10">peine</w> <w n="15.11">reste</w> :</l>
					<l n="16" num="4.4"><w n="16.1">La</w> <w n="16.2">rose</w> <w n="16.3">vit</w> <w n="16.4">une</w> <w n="16.5">heure</w> <w n="16.6">et</w> <w n="16.7">le</w> <w n="16.8">cyprès</w> <w n="16.9">cent</w> <w n="16.10">ans</w>.</l>
				</lg>
			</div></body></text></TEI>