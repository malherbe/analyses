<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2184 vers2184 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Premier</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/files/44180/44180-h/44180-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Premier</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1833">1833</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Quelques erreurs orthographiques ont été repérées avec le correcteur orthogrphique Gedit.</p>
				<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU122">
				<head type="main">FAR NIENTE</head>
				<opener>
					<epigraph>
						<cit>
							<quote>
								Quant à son temps bien le sut disposer : <lb></lb>
								Deux parts en fit dont il souloit passer <lb></lb>
								L’une à dormir et l’autre à ne rien faire.
							</quote>
							<bibl>
								<name>Jean de la Fontaine.</name>
							</bibl>
						</cit>
					</epigraph>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Quand</w> <w n="1.2">je</w> <w n="1.3">n</w>’<w n="1.4">ai</w> <w n="1.5">rien</w> <w n="1.6">à</w> <w n="1.7">faire</w>, <w n="1.8">et</w> <w n="1.9">qu</w>’<w n="1.10">à</w> <w n="1.11">peine</w> <w n="1.12">un</w> <w n="1.13">nuage</w></l>
					<l n="2" num="1.2"><w n="2.1">Dans</w> <w n="2.2">les</w> <w n="2.3">champs</w> <w n="2.4">bleus</w> <w n="2.5">du</w> <w n="2.6">ciel</w>, <w n="2.7">flocon</w> <w n="2.8">de</w> <w n="2.9">laine</w>, <w n="2.10">nage</w>,</l>
					<l n="3" num="1.3"><w n="3.1">J</w>’<w n="3.2">aime</w> <w n="3.3">à</w> <w n="3.4">m</w>’<w n="3.5">écouter</w> <w n="3.6">vivre</w>, <w n="3.7">et</w> <w n="3.8">libre</w> <w n="3.9">de</w> <w n="3.10">soucis</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Loin</w> <w n="4.2">des</w> <w n="4.3">chemins</w> <w n="4.4">poudreux</w>, <w n="4.5">à</w> <w n="4.6">demeurer</w> <w n="4.7">assis</w></l>
					<l n="5" num="1.5"><w n="5.1">Sur</w> <w n="5.2">un</w> <w n="5.3">moelleux</w> <w n="5.4">tapis</w> <w n="5.5">de</w> <w n="5.6">fougère</w> <w n="5.7">et</w> <w n="5.8">de</w> <w n="5.9">mousse</w>,</l>
					<l n="6" num="1.6"><w n="6.1">Au</w> <w n="6.2">bord</w> <w n="6.3">des</w> <w n="6.4">bois</w> <w n="6.5">touffus</w> <w n="6.6">où</w> <w n="6.7">la</w> <w n="6.8">chaleur</w> <w n="6.9">s</w>’<w n="6.10">émousse</w> ;</l>
					<l n="7" num="1.7"><w n="7.1">Là</w>, <w n="7.2">pour</w> <w n="7.3">tuer</w> <w n="7.4">le</w> <w n="7.5">temps</w>, <w n="7.6">j</w>’<w n="7.7">observe</w> <w n="7.8">la</w> <w n="7.9">fourmi</w></l>
					<l n="8" num="1.8"><w n="8.1">Qui</w>, <w n="8.2">pensant</w> <w n="8.3">au</w> <w n="8.4">retour</w> <w n="8.5">de</w> <w n="8.6">l</w>’<w n="8.7">hiver</w> <w n="8.8">ennemi</w>,</l>
					<l n="9" num="1.9"><w n="9.1">Pour</w> <w n="9.2">son</w> <w n="9.3">grenier</w> <w n="9.4">dérobe</w> <w n="9.5">un</w> <w n="9.6">grain</w> <w n="9.7">d</w>’<w n="9.8">orge</w> <w n="9.9">à</w> <w n="9.10">la</w> <w n="9.11">gerbe</w>,</l>
					<l n="10" num="1.10"><w n="10.1">Le</w> <w n="10.2">puceron</w> <w n="10.3">qui</w> <w n="10.4">grimpe</w> <w n="10.5">et</w> <w n="10.6">se</w> <w n="10.7">pend</w> <w n="10.8">au</w> <w n="10.9">brin</w> <w n="10.10">d</w>’<w n="10.11">herbe</w>,</l>
					<l n="11" num="1.11"><w n="11.1">La</w> <w n="11.2">chenille</w> <w n="11.3">traînant</w> <w n="11.4">ses</w> <w n="11.5">anneaux</w> <w n="11.6">veloutés</w>,</l>
					<l n="12" num="1.12"><w n="12.1">La</w> <w n="12.2">limace</w> <w n="12.3">baveuse</w> <w n="12.4">aux</w> <w n="12.5">sillons</w> <w n="12.6">argentés</w>,</l>
					<l n="13" num="1.13"><w n="13.1">Et</w> <w n="13.2">le</w> <w n="13.3">frais</w> <w n="13.4">papillon</w> <w n="13.5">qui</w> <w n="13.6">de</w> <w n="13.7">fleurs</w> <w n="13.8">en</w> <w n="13.9">fleurs</w> <w n="13.10">vole</w>.</l>
					<l n="14" num="1.14"><w n="14.1">Ensuite</w> <w n="14.2">je</w> <w n="14.3">regarde</w>, <w n="14.4">amusement</w> <w n="14.5">frivole</w>,</l>
					<l n="15" num="1.15"><w n="15.1">La</w> <w n="15.2">lumière</w> <w n="15.3">brisant</w> <w n="15.4">dans</w> <w n="15.5">chacun</w> <w n="15.6">de</w> <w n="15.7">mes</w> <w n="15.8">cils</w>,</l>
					<l n="16" num="1.16"><w n="16.1">Palissade</w> <w n="16.2">opposée</w> <w n="16.3">à</w> <w n="16.4">ses</w> <w n="16.5">rayons</w> <w n="16.6">subtils</w>,</l>
					<l n="17" num="1.17"><w n="17.1">Les</w> <w n="17.2">sept</w> <w n="17.3">couleurs</w> <w n="17.4">du</w> <w n="17.5">prisme</w>, <w n="17.6">ou</w> <w n="17.7">le</w> <w n="17.8">duvet</w> <w n="17.9">qui</w> <w n="17.10">flotte</w></l>
					<l n="18" num="1.18"><w n="18.1">En</w> <w n="18.2">l</w>’<w n="18.3">air</w>, <w n="18.4">comme</w> <w n="18.5">sur</w> <w n="18.6">l</w>’<w n="18.7">onde</w> <w n="18.8">un</w> <w n="18.9">vaisseau</w> <w n="18.10">sans</w> <w n="18.11">pilote</w> ;</l>
					<l n="19" num="1.19"><w n="19.1">Et</w> <w n="19.2">lorsque</w> <w n="19.3">je</w> <w n="19.4">suis</w> <w n="19.5">las</w> <w n="19.6">je</w> <w n="19.7">me</w> <w n="19.8">laisse</w> <w n="19.9">endormir</w></l>
					<l n="20" num="1.20"><w n="20.1">Au</w> <w n="20.2">murmure</w> <w n="20.3">de</w> <w n="20.4">l</w>’<w n="20.5">eau</w> <w n="20.6">qu</w>’<w n="20.7">un</w> <w n="20.8">caillou</w> <w n="20.9">fait</w> <w n="20.10">gémir</w>,</l>
					<l n="21" num="1.21"><w n="21.1">Ou</w> <w n="21.2">j</w>’<w n="21.3">écoute</w> <w n="21.4">chanter</w> <w n="21.5">près</w> <w n="21.6">de</w> <w n="21.7">moi</w> <w n="21.8">la</w> <w n="21.9">fauvette</w>,</l>
					<l n="22" num="1.22"><w n="22.1">Et</w> <w n="22.2">là</w>-<w n="22.3">haut</w> <w n="22.4">dans</w> <w n="22.5">l</w>’<w n="22.6">azur</w> <w n="22.7">gazouiller</w> <w n="22.8">l</w>’<w n="22.9">alouette</w>.</l>
				</lg>
			</div></body></text></TEI>