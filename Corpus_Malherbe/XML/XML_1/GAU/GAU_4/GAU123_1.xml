<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2184 vers2184 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Premier</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/files/44180/44180-h/44180-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Premier</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1833">1833</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Quelques erreurs orthographiques ont été repérées avec le correcteur orthogrphique Gedit.</p>
				<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU123">
				<head type="main">STANCES</head>
				<opener>
					<epigraph>
						<cit>
							<quote>
							La jeune fille rieuse.
							</quote>
							<bibl>
								<name>Victor Hugo.</name>
							</bibl>
						</cit>
					</epigraph>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Vous</w> <w n="1.2">ne</w> <w n="1.3">connaissez</w> <w n="1.4">pas</w> <w n="1.5">les</w> <w n="1.6">molles</w> <w n="1.7">rêveries</w></l>
					<l n="2" num="1.2"><w n="2.1">Où</w> <w n="2.2">l</w>’<w n="2.3">âme</w> <w n="2.4">se</w> <w n="2.5">complaît</w> <w n="2.6">et</w> <w n="2.7">s</w>’<w n="2.8">arrête</w> <w n="2.9">longtemps</w>,</l>
					<l n="3" num="1.3"><w n="3.1">De</w> <w n="3.2">même</w> <w n="3.3">que</w> <w n="3.4">l</w>’<w n="3.5">abeille</w>, <w n="3.6">en</w> <w n="3.7">un</w> <w n="3.8">soir</w> <w n="3.9">de</w> <w n="3.10">printemps</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Sur</w> <w n="4.2">quelque</w> <w n="4.3">bouton</w> <w n="4.4">d</w>’<w n="4.5">or</w>, <w n="4.6">étoile</w> <w n="4.7">des</w> <w n="4.8">prairies</w> ;</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Vous</w> <w n="5.2">ne</w> <w n="5.3">connaissez</w> <w n="5.4">pas</w> <w n="5.5">cet</w> <w n="5.6">inquiet</w> <w n="5.7">désir</w></l>
					<l n="6" num="2.2"><w n="6.1">Qui</w> <w n="6.2">fait</w> <w n="6.3">rougir</w> <w n="6.4">souvent</w> <w n="6.5">une</w> <w n="6.6">joue</w> <w n="6.7">ingénue</w>,</l>
					<l n="7" num="2.3"><w n="7.1">Ce</w> <w n="7.2">besoin</w> <w n="7.3">d</w>’<w n="7.4">habiter</w> <w n="7.5">une</w> <w n="7.6">sphère</w> <w n="7.7">inconnue</w>,</l>
					<l n="8" num="2.4"><w n="8.1">D</w>’<w n="8.2">embrasser</w> <w n="8.3">un</w> <w n="8.4">fantôme</w> <w n="8.5">impossible</w> <w n="8.6">à</w> <w n="8.7">saisir</w> ;</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Ces</w> <w n="9.2">attendrissements</w>, <w n="9.3">ces</w> <w n="9.4">soupirs</w> <w n="9.5">et</w> <w n="9.6">ces</w> <w n="9.7">larmes</w></l>
					<l n="10" num="3.2"><w n="10.1">Sans</w> <w n="10.2">cause</w>, <w n="10.3">qu</w>’<w n="10.4">on</w> <w n="10.5">voudrait</w>, <w n="10.6">mais</w> <w n="10.7">en</w> <w n="10.8">vain</w>, <w n="10.9">réprimer</w>,</l>
					<l n="11" num="3.3"><w n="11.1">Cette</w> <w n="11.2">vague</w> <w n="11.3">langueur</w> <w n="11.4">et</w> <w n="11.5">ce</w> <w n="11.6">doux</w> <w n="11.7">mal</w> <w n="11.8">d</w>’<w n="11.9">aimer</w>,</l>
					<l n="12" num="3.4"><w n="12.1">Pour</w> <w n="12.2">un</w> <w n="12.3">objet</w> <w n="12.4">chéri</w> <w n="12.5">ces</w> <w n="12.6">mortelles</w> <w n="12.7">alarmes</w> ;</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Vous</w> <w n="13.2">ne</w> <w n="13.3">connaissez</w> <w n="13.4">rien</w>, <w n="13.5">rien</w> <w n="13.6">que</w> <w n="13.7">folle</w> <w n="13.8">gaîté</w> ;</l>
					<l n="14" num="4.2"><w n="14.1">Sur</w> <w n="14.2">votre</w> <w n="14.3">lèvre</w> <w n="14.4">rose</w> <w n="14.5">un</w> <w n="14.6">frais</w> <w n="14.7">sourire</w> <w n="14.8">vole</w> ;</l>
					<l n="15" num="4.3"><w n="15.1">Votre</w> <w n="15.2">entretien</w> <w n="15.3">naïf</w>, <w n="15.4">sérieux</w> <w n="15.5">ou</w> <w n="15.6">frivole</w>,</l>
					<l n="16" num="4.4"><w n="16.1">Est</w> <w n="16.2">égal</w> <w n="16.3">et</w> <w n="16.4">serein</w> <w n="16.5">comme</w> <w n="16.6">un</w> <w n="16.7">beau</w> <w n="16.8">jour</w> <w n="16.9">d</w>’<w n="16.10">été</w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">Sur</w> <w n="17.2">votre</w> <w n="17.3">main</w> <w n="17.4">jamais</w> <w n="17.5">votre</w> <w n="17.6">front</w> <w n="17.7">ne</w> <w n="17.8">se</w> <w n="17.9">pose</w>,</l>
					<l n="18" num="5.2"><w n="18.1">Brûlant</w>, <w n="18.2">chargé</w> <w n="18.3">d</w>’<w n="18.4">ennuis</w>, <w n="18.5">ne</w> <w n="18.6">pouvant</w> <w n="18.7">soutenir</w></l>
					<l n="19" num="5.3"><w n="19.1">Le</w> <w n="19.2">poids</w> <w n="19.3">d</w>’<w n="19.4">un</w> <w n="19.5">douloureux</w> <w n="19.6">et</w> <w n="19.7">cruel</w> <w n="19.8">souvenir</w> ;</l>
					<l n="20" num="5.4"><w n="20.1">Votre</w> <w n="20.2">cœur</w> <w n="20.3">virginal</w> <w n="20.4">en</w> <w n="20.5">lui</w>-<w n="20.6">même</w> <w n="20.7">repose</w>.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">Avenir</w> <w n="21.2">et</w> <w n="21.3">présent</w>, <w n="21.4">tout</w> <w n="21.5">rit</w> <w n="21.6">dans</w> <w n="21.7">vos</w> <w n="21.8">destins</w> ;</l>
					<l n="22" num="6.2"><w n="22.1">Vous</w> <w n="22.2">n</w>’<w n="22.3">avez</w> <w n="22.4">pas</w> <w n="22.5">encore</w> <w n="22.6">aimé</w> <w n="22.7">sans</w> <w n="22.8">être</w> <w n="22.9">aimée</w>,</l>
					<l n="23" num="6.3"><w n="23.1">Ni</w>, <w n="23.2">retenant</w> <w n="23.3">à</w> <w n="23.4">peine</w> <w n="23.5">une</w> <w n="23.6">larme</w> <w n="23.7">enflammée</w>,</l>
					<l n="24" num="6.4"><w n="24.1">Épié</w> <w n="24.2">d</w>’<w n="24.3">un</w> <w n="24.4">regard</w> <w n="24.5">les</w> <w n="24.6">aveux</w> <w n="24.7">incertains</w>.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">Jeune</w> <w n="25.2">fille</w>, <w n="25.3">vos</w> <w n="25.4">yeux</w> <w n="25.5">ignorent</w> <w n="25.6">l</w>’<w n="25.7">insomnie</w> ;</l>
					<l n="26" num="7.2"><w n="26.1">Une</w> <w n="26.2">pensée</w> <w n="26.3">ardente</w> <w n="26.4">et</w> <w n="26.5">qui</w> <w n="26.6">revient</w> <w n="26.7">toujours</w></l>
					<l n="27" num="7.3"><w n="27.1">Ne</w> <w n="27.2">trouble</w> <w n="27.3">pas</w> <w n="27.4">vos</w> <w n="27.5">nuits</w> <w n="27.6">tristes</w> <w n="27.7">comme</w> <w n="27.8">vos</w> <w n="27.9">jours</w> ;</l>
					<l n="28" num="7.4"><w n="28.1">Votre</w> <w n="28.2">vie</w> <w n="28.3">en</w> <w n="28.4">sa</w> <w n="28.5">fleur</w> <w n="28.6">n</w>’<w n="28.7">a</w> <w n="28.8">pas</w> <w n="28.9">été</w> <w n="28.10">ternie</w>.</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1">Ainsi</w> <w n="29.2">qu</w>’<w n="29.3">un</w> <w n="29.4">ruisseau</w> <w n="29.5">clair</w> <w n="29.6">où</w> <w n="29.7">se</w> <w n="29.8">mirent</w> <w n="29.9">les</w> <w n="29.10">cieux</w>,</l>
					<l n="30" num="8.2"><w n="30.1">Dont</w> <w n="30.2">le</w> <w n="30.3">cours</w> <w n="30.4">lentement</w> <w n="30.5">par</w> <w n="30.6">les</w> <w n="30.7">prés</w> <w n="30.8">se</w> <w n="30.9">déroule</w>,</l>
					<l n="31" num="8.3"><w n="31.1">Votre</w> <w n="31.2">existence</w> <w n="31.3">pure</w> <w n="31.4">et</w> <w n="31.5">limpide</w> <w n="31.6">s</w>’<w n="31.7">écoule</w>,</l>
					<l n="32" num="8.4"><w n="32.1">Heureuse</w> <w n="32.2">d</w>’<w n="32.3">un</w> <w n="32.4">bonheur</w> <w n="32.5">calme</w> <w n="32.6">et</w> <w n="32.7">silencieux</w>.</l>
				</lg>
			</div></body></text></TEI>