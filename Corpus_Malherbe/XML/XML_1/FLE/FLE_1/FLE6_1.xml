<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Friperies</title>
				<title type="medium">Une édition électronique</title>
				<author key="FLE">
					<name>
						<forename>Fernand</forename>
						<surname>FLEURET</surname>
					</name>
					<date from="1883" to="1945">1883-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>455 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">FLE_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Friperies</title>
						<author>Fernand Fleuret</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/friperies00fleu</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Friperies</title>
								<author>Fernand Fleuret</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>CHEZ EUGÈNE REY, LIBRAIRE</publisher>
									<date when="1907">1907</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			</fileDesc>
		<profileDesc>
			<creation>
				<date when="1907">1907</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Cette édition électronique ne comprend pas la préface de l’auteur.</p>
				<p>L’édition qui est à l’origine de la version électronique n’est pas indiquée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-08-30" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2019-08-30" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="FLE6">
				<head type="main">AMOUR DU PASSÉ LÉGENDAIRE</head>
				<opener>
					<epigraph>
						<cit>
							<quote>
								« …N’as-lu pas désiré, ma sœur au regard de<lb></lb>
								jadis, qu’en un de mes poèmes apparussent ces<lb></lb>
								mots « la grâce des choses fanées ?… »				
							</quote>
							<bibl>
								<name>Stéphane Mallarmé.</name>
							</bibl>
						</cit>
						<cit>
							<quote>
								« Je devine à travers un murmure,<lb></lb>
								Le conteur subtil des voix anciennes… » 	
							</quote>
							<bibl>
								<name>Paul Verlaine.</name>
							</bibl>
						</cit>
					</epigraph>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Les</w> <w n="1.2">images</w> <w n="1.3">qu</w>’<w n="1.4">on</w> <w n="1.5">haïrait</w> <w n="1.6">pour</w> <w n="1.7">maladroites</w></l>
					<l n="2" num="1.2"><w n="2.1">Avec</w> <w n="2.2">leur</w> <w n="2.3">gaucherie</w> <w n="2.4">populaire</w> <w n="2.5">et</w> <w n="2.6">précise</w>.</l>
					<l n="3" num="1.3"><w n="3.1">Si</w> <w n="3.2">l</w>’<w n="3.3">on</w> <w n="3.4">n</w>’<w n="3.5">avait</w> <w n="3.6">la</w> <w n="3.7">foi</w>, <w n="3.8">du</w> <w n="3.9">temps</w> <w n="3.10">des</w> <w n="3.11">almanachs</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Au</w> <w n="4.2">bon</w> <w n="4.3">Dieu</w> <w n="4.4">bleu</w> <w n="4.5">de</w> <w n="4.6">Prusse</w> <w n="4.7">en</w> <w n="4.8">robe</w> <w n="4.9">de</w> <w n="4.10">prélat</w> ;</l>
					<l n="5" num="1.5"><w n="5.1">Puis</w> <w n="5.2">les</w> <w n="5.3">vieilles</w> <w n="5.4">photos</w> <w n="5.5">et</w> <w n="5.6">leurs</w> <w n="5.7">modes</w> <w n="5.8">massives</w> !</l>
					<l n="6" num="1.6">— <w n="6.1">Les</w> <w n="6.2">images</w> <w n="6.3">qu</w>’<w n="6.4">on</w> <w n="6.5">haïrait</w> <w n="6.6">pour</w> <w n="6.7">maladroites</w>…</l>
				</lg>
				<lg n="2">
					<l n="7" num="2.1"><w n="7.1">Les</w> <w n="7.2">objets</w>, <w n="7.3">où</w> <w n="7.4">revit</w> <w n="7.5">l</w>’<w n="7.6">esprit</w> <w n="7.7">qui</w> <w n="7.8">les</w> <w n="7.9">conçut</w>.</l>
					<l n="8" num="2.2"><w n="8.1">Qu</w>’<w n="8.2">embue</w> <w n="8.3">encor</w> <w n="8.4">l</w>’<w n="8.5">ombre</w> <w n="8.6">des</w> <w n="8.7">mains</w> <w n="8.8">qui</w> <w n="8.9">les</w> <w n="8.10">palpèrent</w>.</l>
					<l n="9" num="2.3"><w n="9.1">Où</w> <w n="9.2">se</w> <w n="9.3">pose</w> <w n="9.4">un</w> <w n="9.5">tépide</w> <w n="9.6">et</w> <w n="9.7">jaloux</w> <w n="9.8">faix</w> <w n="9.9">de</w> <w n="9.10">doigts</w></l>
					<l n="10" num="2.4"><w n="10.1">Pour</w> <w n="10.2">parer</w> <w n="10.3">des</w> <w n="10.4">baisers</w> <w n="10.5">insidieux</w> <w n="10.6">et</w> <w n="10.7">froids</w></l>
					<l n="11" num="2.5"><w n="11.1">Qu</w>’<w n="11.2">aime</w> <w n="11.3">à</w> <w n="11.4">plaquer</w> <w n="11.5">tant</w> <w n="11.6">méchamment</w> <w n="11.7">la</w> <w n="11.8">nuit</w> <w n="11.9">lunaire</w>…</l>
					<l n="12" num="2.6">— <w n="12.1">Les</w> <w n="12.2">objets</w>, <w n="12.3">où</w> <w n="12.4">revit</w> <w n="12.5">l</w>’<w n="12.6">esprit</w> <w n="12.7">qui</w> <w n="12.8">les</w> <w n="12.9">conçut</w>…</l>
				</lg>
				<lg n="3">
					<l n="13" num="3.1"><w n="13.1">Ce</w> <w n="13.2">monde</w>, <w n="13.3">dont</w> <w n="13.4">la</w> <w n="13.5">cendre</w> <w n="13.6">est</w> <w n="13.7">du</w> <w n="13.8">fard</w> ! <w n="13.9">et</w> <w n="13.10">les</w> <w n="13.11">cloches</w></l>
					<l n="14" num="3.2"><w n="14.1">Nostalgiques</w> <w n="14.2">des</w> <w n="14.3">crinolines</w>, <w n="14.4">qui</w> <w n="14.5">m</w>’<w n="14.6">appellent</w></l>
					<l n="15" num="3.3"><w n="15.1">Avec</w> <w n="15.2">d</w>’<w n="15.3">amples</w> <w n="15.4">rumeurs</w> <w n="15.5">de</w> <w n="15.6">soie</w> <w n="15.7">et</w> <w n="15.8">de</w> <w n="15.9">satin</w>,</l>
					<l n="16" num="3.4"><w n="16.1">En</w> <w n="16.2">des</w> <w n="16.3">pays</w> <w n="16.4">d</w>’<w n="16.5">hortensias</w> <w n="16.6">et</w> <w n="16.7">boulingrins</w></l>
					<l n="17" num="3.5"><w n="17.1">Et</w> <w n="17.2">de</w> <w n="17.3">roucoulements</w> <w n="17.4">lascifs</w> <w n="17.5">de</w> <w n="17.6">tourterelles</w> !</l>
					<l n="18" num="3.6">— <w n="18.1">Ce</w> <w n="18.2">monde</w>, <w n="18.3">dont</w> <w n="18.4">la</w> <w n="18.5">cendre</w> <w n="18.6">est</w> <w n="18.7">du</w> <w n="18.8">fard</w> ! <w n="18.9">et</w> <w n="18.10">ces</w> <w n="18.11">cloches</w>.</l>
				</lg>
				<lg n="4">
					<l n="19" num="4.1"><w n="19.1">Amour</w> <w n="19.2">de</w> <w n="19.3">tout</w> <w n="19.4">cela</w>. <w n="19.5">Passé</w> ! <w n="19.6">et</w> <w n="19.7">des</w> <w n="19.8">maisons</w></l>
					<l n="20" num="4.2"><w n="20.1">Où</w> <w n="20.2">la</w> <w n="20.3">mort</w> <w n="20.4">vient</w> <w n="20.5">flairer</w> <w n="20.6">avec</w> <w n="20.7">bruit</w> <w n="20.8">sous</w> <w n="20.9">les</w> <w n="20.10">portes</w></l>
					<l n="21" num="4.3"><w n="21.1">Si</w> <w n="21.2">rien</w> <w n="21.3">ne</w> <w n="21.4">fut</w> <w n="21.5">trahi</w> <w n="21.6">de</w> <w n="21.7">son</w> <w n="21.8">dû</w> <w n="21.9">ancien</w>.</l>
					<l n="22" num="4.4"><w n="22.1">Et</w> <w n="22.2">grogne</w> <w n="22.3">à</w> <w n="22.4">la</w> <w n="22.5">chanson</w> <w n="22.6">des</w> <w n="22.7">gonds</w> <w n="22.8">musiciens</w></l>
					<l n="23" num="4.5"><w n="23.1">Qui</w> <w n="23.2">saluent</w> <w n="23.3">les</w> <w n="23.4">cent</w> <w n="23.5">ans</w> <w n="23.6">des</w> <w n="23.7">remugles</w> <w n="23.8">qui</w> <w n="23.9">sortent</w> !</l>
					<l n="24" num="4.6"><w n="24.1">Amour</w> <w n="24.2">du</w> <w n="24.3">Passé</w> <w n="24.4">légendaire</w> <w n="24.5">et</w> <w n="24.6">des</w> <w n="24.7">maisons</w>…</l>
				</lg>
			</div></body></text></TEI>