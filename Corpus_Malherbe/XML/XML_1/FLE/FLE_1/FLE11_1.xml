<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Friperies</title>
				<title type="medium">Une édition électronique</title>
				<author key="FLE">
					<name>
						<forename>Fernand</forename>
						<surname>FLEURET</surname>
					</name>
					<date from="1883" to="1945">1883-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>455 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">FLE_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Friperies</title>
						<author>Fernand Fleuret</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/friperies00fleu</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Friperies</title>
								<author>Fernand Fleuret</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>CHEZ EUGÈNE REY, LIBRAIRE</publisher>
									<date when="1907">1907</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			</fileDesc>
		<profileDesc>
			<creation>
				<date when="1907">1907</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Cette édition électronique ne comprend pas la préface de l’auteur.</p>
				<p>L’édition qui est à l’origine de la version électronique n’est pas indiquée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-08-30" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2019-08-30" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="FLE11">
				<head type="main">PORTRAIT</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Vous</w> <w n="1.2">vîntes</w> <w n="1.3">en</w> <w n="1.4">ce</w> <w n="1.5">cadre</w> <w n="1.6">et</w> <w n="1.7">n</w>’<w n="1.8">en</w> <w n="1.9">partîtes</w> <w n="1.10">pas</w>…</l>
					<l n="2" num="1.2"><w n="2.1">Le</w> <w n="2.2">Temps</w> <w n="2.3">allait</w> <w n="2.4">changeant</w> <w n="2.5">ses</w> <w n="2.6">décors</w> <w n="2.7">de</w> <w n="2.8">théâtre</w>.</l>
					<l n="3" num="1.3"><w n="3.1">Et</w>, <w n="3.2">comme</w> <w n="3.3">un</w> <w n="3.4">temple</w> <w n="3.5">vide</w>, <w n="3.6">au</w> <w n="3.7">rite</w> <w n="3.8">de</w> <w n="3.9">ses</w> <w n="3.10">pas</w>.</l>
					<l n="4" num="1.4"><w n="4.1">Résonnait</w> <w n="4.2">près</w> <w n="4.3">de</w> <w n="4.4">vous</w> <w n="4.5">la</w> <w n="4.6">pendule</w> <w n="4.7">d</w>’<w n="4.8">albâtre</w>…</l>
					<l n="5" num="1.5"><w n="5.1">Ceux</w> <w n="5.2">qui</w> <w n="5.3">vous</w> <w n="5.4">attendaient</w> <w n="5.5">s</w>’<w n="5.6">en</w> <w n="5.7">furent</w> <w n="5.8">vieillement</w></l>
					<l n="6" num="1.6"><w n="6.1">Par</w> <w n="6.2">la</w> <w n="6.3">Route</w> <w n="6.4">des</w> <w n="6.5">jours</w>, <w n="6.6">de</w> <w n="6.7">Toussaints</w> <w n="6.8">jalonnée</w>.</l>
					<l n="7" num="1.7"><w n="7.1">Et</w> <w n="7.2">vous</w> <w n="7.3">restâtes</w> <w n="7.4">là</w>, <w n="7.5">dans</w> <w n="7.6">l</w>’<w n="7.7">oubli</w> <w n="7.8">des</w> <w n="7.9">années</w>,</l>
					<l n="8" num="1.8"><w n="8.1">De</w> <w n="8.2">l</w>’<w n="8.3">ombre</w> <w n="8.4">de</w> <w n="8.5">jadis</w> <w n="8.6">aux</w> <w n="8.7">plis</w> <w n="8.8">des</w> <w n="8.9">vêtements</w>.</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1"><w n="9.1">Tombé</w> <w n="9.2">l</w>’<w n="9.3">Empire</w>, <w n="9.4">avec</w> <w n="9.5">les</w> <w n="9.6">Épaules</w> <w n="9.7">tombantes</w> !</l>
					<l n="10" num="2.2"><w n="10.1">Et</w> <w n="10.2">l</w>’<hi rend="ital"><w n="10.3">Air</w> <w n="10.4">du</w> <w n="10.5">Val</w> <w n="10.6">d</w>’<w n="10.7">Andorre</w></hi>, <w n="10.8">et</w> <w n="10.9">le</w> <hi rend="ital"><w n="10.10">Pas</w> <w n="10.11">des</w> <w n="10.12">Lanciers</w></hi> !</l>
					<l n="11" num="2.3"><w n="11.1">O</w> <w n="11.2">Friperies</w> ! <w n="11.3">l</w>’<w n="11.4">orgue</w> <w n="11.5">rural</w> <w n="11.6">tout</w> <w n="11.7">seul</w> <w n="11.8">vous</w> <w n="11.9">chante</w>,</l>
					<l n="12" num="2.4"><w n="12.1">Baden</w>-<w n="12.2">Bade</w> <w n="12.3">survit</w>… <w n="12.4">en</w> <w n="12.5">tailles</w> <w n="12.6">sur</w> <w n="12.7">acier</w> !</l>
					<l n="13" num="2.5">— <w n="13.1">Vous</w> <w n="13.2">saviez</w> <w n="13.3">que</w>, <w n="13.4">férus</w> <w n="13.5">des</w> <w n="13.6">amples</w> <w n="13.7">crinolines</w></l>
					<l n="14" num="2.6"><w n="14.1">Et</w> <w n="14.2">du</w> <w n="14.3">double</w> <w n="14.4">bouquet</w> <w n="14.5">lilial</w> <w n="14.6">de</w> <w n="14.7">vos</w> <w n="14.8">mains</w>.</l>
					<l n="15" num="2.7"><w n="15.1">Des</w> <w n="15.2">Rêveurs</w> <w n="15.3">s</w>’<w n="15.4">assoieraient</w> <w n="15.5">près</w> <w n="15.6">des</w> <w n="15.7">Foyers</w> <w n="15.8">éteints</w>,</l>
					<l n="16" num="2.8"><w n="16.1">Le</w> <w n="16.2">cœur</w> <w n="16.3">tendre</w> <w n="16.4">et</w> <w n="16.5">gonflé</w> <w n="16.6">comme</w> <w n="16.7">une</w> <w n="16.8">mandoline</w> ;</l>
					<l n="17" num="2.9"><w n="17.1">Que</w> <w n="17.2">Ceux</w>-<w n="17.3">là</w> <w n="17.4">du</w> <w n="17.5">Futur</w>, <w n="17.6">par</w> <w n="17.7">la</w> <w n="17.8">Laideur</w> <w n="17.9">heurtés</w>,</l>
					<l n="18" num="2.10"><w n="18.1">Ramasseraient</w> <w n="18.2">leur</w> <w n="18.3">vie</w> <w n="18.4">ainsi</w> <w n="18.5">qu</w>’<w n="18.6">un</w> <w n="18.7">faix</w> <w n="18.8">de</w> <w n="18.9">hardes</w></l>
					<l n="19" num="2.11"><w n="19.1">Et</w> <w n="19.2">s</w>’<w n="19.3">en</w> <w n="19.4">iraient</w> <w n="19.5">vers</w> <w n="19.6">l</w>’<w n="19.7">âge</w> <w n="19.8">où</w> <w n="19.9">vous</w> <w n="19.10">avez</w> <w n="19.11">été</w>.</l>
					<l n="20" num="2.12"><w n="20.1">Avec</w> <w n="20.2">l</w>’<w n="20.3">Amour</w>, <w n="20.4">enfant</w> <w n="20.5">pesant</w> <w n="20.6">qui</w> <w n="20.7">les</w> <w n="20.8">retarde</w> !</l>
					<l n="21" num="2.13"><w n="21.1">Ah</w> ! <w n="21.2">souriez</w>, <w n="21.3">de</w> <w n="21.4">votre</w> <w n="21.5">toile</w>, <w n="21.6">soyez</w>-<w n="21.7">moi</w></l>
					<l n="22" num="2.14"><w n="22.1">Douce</w> <w n="22.2">comme</w> <w n="22.3">la</w> <w n="22.4">mort</w> <w n="22.5">et</w> <w n="22.6">les</w> <w n="22.7">vers</w> <w n="22.8">de</w> <w n="22.9">Verlaine</w>,</l>
					<l n="23" num="2.15"><w n="23.1">O</w> <w n="23.2">Vague</w> ! <w n="23.3">ô</w> <w n="23.4">si</w> <w n="23.5">Présente</w> <w n="23.6">et</w> <w n="23.7">pourtant</w> <w n="23.8">si</w> <w n="23.9">Lointaine</w> !</l>
					<l n="24" num="2.16"><w n="24.1">O</w> <w n="24.2">Toi</w>, <w n="24.3">qui</w> <w n="24.4">demeuras</w> <w n="24.5">en</w> <w n="24.6">ce</w> <w n="24.7">cadre</w> <w n="24.8">pour</w> <w n="24.9">moi</w> !</l>
				</lg>
				<lg n="3">
					<l n="25" num="3.1"><w n="25.1">Toi</w> <w n="25.2">seule</w> <w n="25.3">sais</w> <w n="25.4">chanter</w> <w n="25.5">les</w> <w n="25.6">berceuses</w> <w n="25.7">que</w> <w n="25.8">j</w>’<w n="25.9">aime</w> ;</l>
					<l n="26" num="3.2"><w n="26.1">Toi</w> <w n="26.2">seule</w> <w n="26.3">sais</w> <w n="26.4">chérir</w> <w n="26.5">les</w> <w n="26.6">choses</w> <w n="26.7">d</w>’<w n="26.8">autrefois</w> :</l>
					<l n="27" num="3.3"><w n="27.1">Mets</w> <w n="27.2">mon</w> <w n="27.3">âme</w> <w n="27.4">à</w> <w n="27.5">ton</w> <w n="27.6">cou</w>, <w n="27.7">comme</w> <w n="27.8">une</w> <w n="27.9">écharpe</w> <w n="27.10">blême</w></l>
					<l n="28" num="3.4"><w n="28.1">Qui</w> <w n="28.2">t</w>’<w n="28.3">entoure</w> <w n="28.4">d</w>’<w n="28.5">amour</w> <w n="28.6">et</w> <w n="28.7">soit</w> <w n="28.8">souple</w> <w n="28.9">à</w> <w n="28.10">tes</w> <w n="28.11">doigts</w>…</l>
				</lg>
				<lg n="4">
					<l n="29" num="4.1"><w n="29.1">Je</w> <w n="29.2">mourrai</w> <w n="29.3">sans</w> <w n="29.4">te</w> <w n="29.5">voir</w>, <w n="29.6">à</w> <w n="29.7">ton</w> <w n="29.8">cadre</w> <w n="29.9">fidèle</w>.</l>
					<l n="30" num="4.2"><w n="30.1">Descendre</w> <w n="30.2">me</w> <w n="30.3">donner</w> <w n="30.4">l</w>’<w n="30.5">hyménéen</w> <w n="30.6">baiser</w>…</l>
					<l n="31" num="4.3"><w n="31.1">Ma</w> <w n="31.2">vie</w> <w n="31.3">est</w> <w n="31.4">là</w>, <w n="31.5">je</w> <w n="31.6">la</w> <w n="31.7">veux</w> <w n="31.8">là</w> <w n="31.9">et</w> <w n="31.10">l</w>’<w n="31.11">y</w> <w n="31.12">laisser</w>,</l>
					<l n="32" num="4.4"><w n="32.1">Jaloux</w>, <w n="32.2">jaloux</w> <w n="32.3">de</w> <w n="32.4">ceux</w> <w n="32.5">qui</w> <w n="32.6">viendront</w> <w n="32.7">après</w> <w n="32.8">elle</w> !</l>
				</lg>
			</div></body></text></TEI>