<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Friperies</title>
				<title type="medium">Une édition électronique</title>
				<author key="FLE">
					<name>
						<forename>Fernand</forename>
						<surname>FLEURET</surname>
					</name>
					<date from="1883" to="1945">1883-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>455 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">FLE_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Friperies</title>
						<author>Fernand Fleuret</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/friperies00fleu</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Friperies</title>
								<author>Fernand Fleuret</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>CHEZ EUGÈNE REY, LIBRAIRE</publisher>
									<date when="1907">1907</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			</fileDesc>
		<profileDesc>
			<creation>
				<date when="1907">1907</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Cette édition électronique ne comprend pas la préface de l’auteur.</p>
				<p>L’édition qui est à l’origine de la version électronique n’est pas indiquée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-08-30" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2019-08-30" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="FLE3">
				<head type="main">HARSOIR, QUE JE SONGEOIS</head>
				<opener>
					<epigraph>
						<cit>
							<quote>
								« L’invincible destin lui enchaîne les mains<lb></lb>
								la tenant prisonnière et tout ce qu’on propose<lb></lb>
								Sagement la Fortune autrement en dispose. » 
							</quote>
							<bibl>(Amours diverses de <name>P. de Ronsard</name>.)</bibl>
						</cit>
					</epigraph>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Harsoir</w>, <w n="1.2">que</w> <w n="1.3">je</w> <w n="1.4">songeois</w> <w n="1.5">près</w> <w n="1.6">ma</w> <w n="1.7">vitre</w> <w n="1.8">nocturne</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Je</w> <w n="2.2">vy</w>, <w n="2.3">sans</w> <w n="2.4">vertugade</w> <w n="2.5">et</w> <w n="2.6">sans</w> <w n="2.7">n</w>’<w n="2.8">en</w> <w n="2.9">dessus</w> <w n="2.10">soy</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Une</w> <w n="3.2">vierge</w> <w n="3.3">chenue</w>, <w n="3.4">et</w> <w n="3.5">que</w> <w n="3.6">nul</w> <w n="3.7">n</w>’<w n="3.8">embrassoit</w>.</l>
					<l n="4" num="1.4"><w n="4.1">Pleurer</w> <w n="4.2">en</w> <w n="4.3">son</w> <w n="4.4">miroër</w> <w n="4.5">son</w> <w n="4.6">tétin</w> <w n="4.7">taciturne</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">— <w n="5.1">Las</w> ! (<w n="5.2">fy</w>-<w n="5.3">je</w>) <w n="5.4">au</w> <w n="5.5">plus</w> <w n="5.6">gaillard</w> <w n="5.7">on</w> <w n="5.8">descouvre</w> <w n="5.9">un</w> <w n="5.10">Minturne</w></l>
					<l n="6" num="2.2"><w n="6.1">Ou</w>, <w n="6.2">dans</w> <w n="6.3">les</w> <w n="6.4">froids</w> <w n="6.5">roseaux</w>, <w n="6.6">il</w> <w n="6.7">s</w>’<w n="6.8">éplore</w> <w n="6.9">à</w> <w n="6.10">requoy</w> ;</l>
					<l n="7" num="2.3"><w n="7.1">Las</w> ! (<w n="7.2">fy</w>-<w n="7.3">je</w>) <w n="7.4">tout</w> <w n="7.5">chaqu</w>’<w n="7.6">un</w> <w n="7.7">est</w> <w n="7.8">puceau</w> <w n="7.9">d</w>’<w n="7.10">un</w> <w n="7.11">endroit</w> ;</l>
					<l n="8" num="2.4"><w n="8.1">Marche</w> <w n="8.2">encor</w>’ <w n="8.3">le</w> <w n="8.4">pié</w> <w n="8.5">nud</w>, <w n="8.6">qui</w> <w n="8.7">resvoit</w> <w n="8.8">le</w> <w n="8.9">cothurne</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">La</w> <w n="9.2">Vie</w> <w n="9.3">nous</w> <w n="9.4">fiance</w> <w n="9.5">à</w> <w n="9.6">nostre</w> <w n="9.7">mauvais</w> <w n="9.8">gré</w> ;</l>
					<l n="10" num="3.2"><w n="10.1">La</w> <w n="10.2">moitié</w> <w n="10.3">est</w> <w n="10.4">désir</w>, <w n="10.5">l</w>’<w n="10.6">autre</w> <w n="10.7">moitié</w> <w n="10.8">regret</w>.</l>
					<l n="11" num="3.3"><w n="11.1">Du</w> <w n="11.2">temps</w> <w n="11.3">qu</w>’<w n="11.4">ont</w> <w n="11.5">à</w> <w n="11.6">blanchir</w> <w n="11.7">le</w> <w n="11.8">chef</w> <w n="11.9">et</w> <w n="11.10">la</w> <w n="11.11">moustache</w> ;</l>
				</lg>
				<lg n="4">
					<l n="12" num="4.1"><w n="12.1">Tel</w> <w n="12.2">qui</w> <w n="12.3">naist</w> <w n="12.4">buscheron</w> <w n="12.5">grandit</w> <w n="12.6">loin</w> <w n="12.7">de</w> <w n="12.8">l</w>’<w n="12.9">essart</w> ;</l>
					<l n="13" num="4.2"><w n="13.1">Tel</w> <w n="13.2">qui</w> <w n="13.3">n</w>’<w n="13.4">est</w> <w n="13.5">point</w> <w n="13.6">Hector</w> <w n="13.7">mignotte</w> <w n="13.8">une</w> <w n="13.9">Andromache</w> ;</l>
					<l n="14" num="4.3"><w n="14.1">Ceste</w> <w n="14.2">eust</w> <w n="14.3">valu</w> <w n="14.4">Cassandre</w> <w n="14.5">et</w> <w n="14.6">manqua</w> <w n="14.7">d</w>’<w n="14.8">un</w> <w n="14.9">Ronsard</w>.</l>
				</lg>
			</div></body></text></TEI>