<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Friperies</title>
				<title type="medium">Une édition électronique</title>
				<author key="FLE">
					<name>
						<forename>Fernand</forename>
						<surname>FLEURET</surname>
					</name>
					<date from="1883" to="1945">1883-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>455 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">FLE_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Friperies</title>
						<author>Fernand Fleuret</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/friperies00fleu</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Friperies</title>
								<author>Fernand Fleuret</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>CHEZ EUGÈNE REY, LIBRAIRE</publisher>
									<date when="1907">1907</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			</fileDesc>
		<profileDesc>
			<creation>
				<date when="1907">1907</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Cette édition électronique ne comprend pas la préface de l’auteur.</p>
				<p>L’édition qui est à l’origine de la version électronique n’est pas indiquée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-08-30" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2019-08-30" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="FLE1">
				<head type="main">SOMPTUAIRE</head>
				<lg n="1">
					<l n="1" num="1.1">— <w n="1.1">Mes</w> <w n="1.2">livres</w>, <w n="1.3">vous</w> <w n="1.4">serez</w> <w n="1.5">de</w> <w n="1.6">petites</w> <w n="1.7">armoires</w></l>
					<l n="2" num="1.2"><w n="2.1">Où</w>, <w n="2.2">soigneux</w>, <w n="2.3">je</w> <w n="2.4">plierai</w> <w n="2.5">mes</w> <w n="2.6">robes</w> <w n="2.7">de</w> <w n="2.8">pensée</w></l>
					<l n="3" num="1.3"><w n="3.1">Afin</w> <w n="3.2">d</w>’<w n="3.3">en</w> <w n="3.4">préserver</w> <w n="3.5">les</w> <w n="3.6">susceptibles</w> <w n="3.7">moires</w></l>
					<l n="4" num="1.4"><w n="4.1">Et</w> <w n="4.2">la</w> <w n="4.3">couleur</w> <w n="4.4">du</w> <w n="4.5">temps</w> <w n="4.6">où</w> <w n="4.7">les</w> <w n="4.8">aurai</w> <w n="4.9">tissées</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">— <w n="5.1">Si</w> <w n="5.2">tout</w> <w n="5.3">passe</w>, <w n="5.4">la</w> <w n="5.5">mode</w> <w n="5.6">et</w> <w n="5.7">ses</w> <w n="5.8">Ostentatrices</w>,</l>
					<l n="6" num="2.2"><w n="6.1">Le</w> <w n="6.2">ver</w>, <w n="6.3">sournoisement</w>, <w n="6.4">prélibe</w> <w n="6.5">le</w> <w n="6.6">grimoire</w> ;</l>
					<l n="7" num="2.3"><w n="7.1">Des</w> <w n="7.2">papillons</w>, <w n="7.3">tramant</w> <w n="7.4">leurs</w> <w n="7.5">petites</w> <w n="7.6">pelisses</w>.</l>
					<l n="8" num="2.4"><w n="8.1">Vont</w> <w n="8.2">se</w> <w n="8.3">poudrer</w> <w n="8.4">de</w> <w n="8.5">gris</w> <w n="8.6">aux</w> <w n="8.7">cendres</w> <w n="8.8">de</w> <w n="8.9">Mémoire</w>…</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">— <w n="9.1">Qu</w>’<w n="9.2">importe</w> <w n="9.3">qu</w>’<w n="9.4">après</w> <w n="9.5">moi</w> <w n="9.6">mes</w> <w n="9.7">parures</w> <w n="9.8">ne</w> <w n="9.9">durent</w> ?</l>
					<l n="10" num="3.2"><w n="10.1">Comme</w> <w n="10.2">de</w> <w n="10.3">mon</w> <w n="10.4">vivant</w>, <w n="10.5">qu</w>’<w n="10.6">importe</w> <w n="10.7">qu</w>’<w n="10.8">on</w> <w n="10.9">me</w> <w n="10.10">voie</w> ?</l>
					<l n="11" num="3.3"><w n="11.1">Quand</w> <w n="11.2">ma</w> <w n="11.3">grâce</w> <w n="11.4">me</w> <w n="11.5">plait</w>, <w n="11.6">ma</w> <w n="11.7">psyché</w> <w n="11.8">la</w> <w n="11.9">renvoie</w>.</l>
					<l n="12" num="3.4"><w n="12.1">Gloire</w> <w n="12.2">bornée</w> <w n="12.3">aux</w> <w n="12.4">quatre</w> <w n="12.5">coins</w> <w n="12.6">de</w> <w n="12.7">sa</w> <w n="12.8">bordure</w>…</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">— <w n="13.1">Narcisse</w> <w n="13.2">eut</w> <w n="13.3">le</w> <w n="13.4">baiser</w> <w n="13.5">d</w>’<w n="13.6">une</w> <w n="13.7">eau</w> <w n="13.8">vaine</w> <w n="13.9">et</w> <w n="13.10">trop</w> <w n="13.11">pure</w>,</l>
					<l n="14" num="4.2"><w n="14.1">Et</w> <w n="14.2">ta</w> <w n="14.3">robe</w> <w n="14.4">est</w> <w n="14.5">vouée</w> <w n="14.6">aux</w> <w n="14.7">froides</w> <w n="14.8">renoncules</w>…</l>
				</lg>
				<lg n="5">
					<l n="15" num="5.1">— <w n="15.1">Si</w> <w n="15.2">ma</w> <w n="15.3">mort</w> <w n="15.4">n</w>’<w n="15.5">a</w> <w n="15.6">l</w>’<w n="15.7">honneur</w> <w n="15.8">d</w>’<w n="15.9">un</w> <w n="15.10">sanglot</w> <w n="15.11">majuscule</w>,</l>
					<l n="16" num="5.2"><w n="16.1">L</w>’<w n="16.2">épave</w>, <w n="16.3">quelquefois</w>, <w n="16.4">fait</w> <w n="16.5">plaindre</w> <w n="16.6">ceux</w> <w n="16.7">qui</w> <w n="16.8">furent</w>.</l>
				</lg>
			</div></body></text></TEI>