<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">L’ÉTERNELLE CHANSON</title>
				<title type="medium">Une édition électronique</title>
				<author key="BEA">
					<name>
						<forename>Henri</forename>
						<surname>BEAUCLAIR</surname>
					</name>
					<date from="1860" to="1919">1860-1919</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>192 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BEA_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Horizontales</title>
						<author>Henri Beauclair</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/henribeauclairleshorizontales.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>L’Éternelle chanson</title>
						<author>Henri Beauclair</author>
						<imprint>
							<publisher>Léon Vanier,Paris</publisher>
							<date when="1884">1884</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1884">1884</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les balises de pagination ont été supprimées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BEA2">
				<head type="number">II</head>
				<head type="main">Jamais !</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Pourquoi</w> <w n="1.2">donc</w> <w n="1.3">répondre</w> : «<w n="1.4">Jamais</w> !»</l>
					<l n="2" num="1.2"><w n="2.1">O</w> <w n="2.2">jeune</w> <w n="2.3">et</w> <w n="2.4">blonde</w> <w n="2.5">charmeresse</w> ?</l>
					<l n="3" num="1.3"><w n="3.1">Quand</w> <w n="3.2">je</w> <w n="3.3">t</w>’<w n="3.4">ai</w> <w n="3.5">dit</w> <w n="3.6">que</w> <w n="3.7">je</w> <w n="3.8">t</w>’<w n="3.9">aimais</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Pourquoi</w> <w n="4.2">donc</w> <w n="4.3">répondre</w> : <w n="4.4">Jamais</w>.</l>
					<l n="5" num="1.5"><w n="5.1">C</w>’<w n="5.2">est</w> <w n="5.3">un</w> <w n="5.4">mot</w>, <w n="5.5">je</w> <w n="5.6">te</w> <w n="5.7">le</w> <w n="5.8">promets</w>,</l>
					<l n="6" num="1.6"><w n="6.1">Qui</w> <w n="6.2">n</w>’<w n="6.3">ira</w> <w n="6.4">pas</w> <w n="6.5">à</w> <w n="6.6">son</w> <w n="6.7">adresse</w>.</l>
					<l n="7" num="1.7"><w n="7.1">Pourquoi</w> <w n="7.2">donc</w> <w n="7.3">répondre</w> : «<w n="7.4">Jamais</w> !»</l>
					<l n="8" num="1.8"><w n="8.1">O</w> <w n="8.2">jeune</w> <w n="8.3">et</w> <w n="8.4">blonde</w> <w n="8.5">charmeresse</w> ?</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1"><w n="9.1">Jamais</w>. <w n="9.2">Le</w> <w n="9.3">mot</w> <w n="9.4">est</w> <w n="9.5">si</w> <w n="9.6">vilain</w>,</l>
					<l n="10" num="2.2"><w n="10.1">Ne</w> <w n="10.2">le</w> <w n="10.3">répète</w> <w n="10.4">plus</w>, <w n="10.5">ô</w> <w n="10.6">Lise</w>.</l>
					<l n="11" num="2.3"><w n="11.1">En</w> <w n="11.2">sais</w> <w n="11.3">tu</w> <w n="11.4">de</w> <w n="11.5">moins</w> <w n="11.6">civil</w>, <w n="11.7">hein</w> ?</l>
					<l n="12" num="2.4"><w n="12.1">Jamais</w>. <w n="12.2">Le</w> <w n="12.3">mot</w> <w n="12.4">est</w> <w n="12.5">si</w> <w n="12.6">vilain</w>.</l>
					<l n="13" num="2.5"><w n="13.1">Et</w>, <w n="13.2">tu</w> <w n="13.3">l</w>’<w n="13.4">as</w> <w n="13.5">écrit</w>, <w n="13.6">sur</w> <w n="13.7">vélin</w>.</l>
					<l n="14" num="2.6"><w n="14.1">Un</w> <w n="14.2">tel</w> <w n="14.3">aplomb</w> <w n="14.4">me</w> <w n="14.5">scandalise</w>.</l>
					<l n="15" num="2.7"><w n="15.1">Jamais</w>. <w n="15.2">Le</w> <w n="15.3">mot</w> <w n="15.4">est</w> <w n="15.5">si</w> <w n="15.6">vilain</w>,</l>
					<l n="16" num="2.8"><w n="16.1">Ne</w> <w n="16.2">le</w> <w n="16.3">répète</w> <w n="16.4">plus</w>, <w n="16.5">ô</w> <w n="16.6">Lise</w> !</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="3">
					<l n="17" num="3.1"><w n="17.1">Un</w> <w n="17.2">vieux</w> <w n="17.3">proverbe</w> <w n="17.4">nous</w> <w n="17.5">l</w>’<w n="17.6">apprend</w>,</l>
					<l n="18" num="3.2"><w n="18.1">Il</w> <w n="18.2">ne</w> <w n="18.3">faut</w> <w n="18.4">pas</w> <w n="18.5">dire</w> : <w n="18.6">Turlure</w>,</l>
					<l n="19" num="3.3"><w n="19.1">Je</w> <w n="19.2">ne</w> <w n="19.3">boirai</w> <w n="19.4">pas</w> <w n="19.5">au</w> <w n="19.6">torrent</w>.</l>
					<l n="20" num="3.4"><w n="20.1">Un</w> <w n="20.2">vieux</w> <w n="20.3">proverbe</w> <w n="20.4">nous</w> <w n="20.5">l</w>’<w n="20.6">apprend</w>,</l>
					<l n="21" num="3.5"><w n="21.1">Tel</w>, <w n="21.2">naguères</w> <w n="21.3">intempérant</w></l>
					<l n="22" num="3.6"><w n="22.1">Boit</w> <w n="22.2">de</w> <w n="22.3">l</w>’<w n="22.4">eau</w> <w n="22.5">dont</w> <w n="22.6">il</w> <w n="22.7">n</w>’<w n="22.8">avait</w> <w n="22.9">cure</w>.</l>
					<l n="23" num="3.7"><w n="23.1">Un</w> <w n="23.2">vieux</w> <w n="23.3">proverbe</w> <w n="23.4">nous</w> <w n="23.5">l</w>’<w n="23.6">apprend</w>,</l>
					<l n="24" num="3.8"><w n="24.1">Il</w> <w n="24.2">ne</w> <w n="24.3">faut</w> <w n="24.4">pas</w> <w n="24.5">dire</w> : <w n="24.6">Turlure</w>.</l>
				</lg>
				<lg n="4">
					<l n="25" num="4.1"><w n="25.1">La</w> <w n="25.2">musique</w> <w n="25.3">adoucit</w> <w n="25.4">les</w> <w n="25.5">mœurs</w> ;</l>
					<l n="26" num="4.2"><w n="26.1">Lise</w>, <w n="26.2">écoute</w> <w n="26.3">ma</w> <w n="26.4">sérénade</w>,</l>
					<l n="27" num="4.3"><w n="27.1">Et</w> <w n="27.2">tu</w> <w n="27.3">n</w>’<w n="27.4">auras</w> <w n="27.5">plus</w> <w n="27.6">de</w> <w n="27.7">rigueurs</w>.</l>
					<l n="28" num="4.4"><w n="28.1">La</w> <w n="28.2">musique</w> <w n="28.3">adoucit</w> <w n="28.4">les</w> <w n="28.5">mœurs</w>,</l>
					<l n="29" num="4.5"><w n="29.1">Je</w> <w n="29.2">vais</w> <w n="29.3">chanter</w> : «<w n="29.4">Ouvre</w>… <w n="29.5">ou</w> <w n="29.6">je</w> <w n="29.7">meurs</w>»</l>
					<l n="30" num="4.6">— <w n="30.1">Trémolos</w> <w n="30.2">à</w> <w n="30.3">la</w> <w n="30.4">cantonade</w></l>
					<l n="31" num="4.7"><w n="31.1">La</w> <w n="31.2">musique</w> <w n="31.3">adoucit</w> <w n="31.4">les</w> <w n="31.5">mœurs</w> ;</l>
					<l n="32" num="4.8"><w n="32.1">Lise</w>, <w n="32.2">écoute</w> <w n="32.3">ma</w> <w n="32.4">sérénade</w>.</l>
				</lg>
			</div></body></text></TEI>