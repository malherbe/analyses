<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES HORIZONTALES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BEA">
					<name>
						<forename>Henri</forename>
						<surname>BEAUCLAIR</surname>
					</name>
					<date from="1860" to="1919">1860-1919</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Saisie du texte</resp>
					<name id="SP">
						<forename>S.</forename>
						<surname>Pestel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Relecture du texte</resp>
					<name id="AG">
						<forename>A.</forename>
						<surname>Guézou</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>404 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BEA_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Horizontales</title>
						<author>Henri Beauclair</author>
					</titleStmt>
					<publicationStmt>
						<publisher>LA BIBLIOTHÈQUE ÉLECTRONIQUE DE LISIEUX</publisher>
						<pubPlace>Lisieux</pubPlace>
						<address>
							<addrLine>Place de la République</addrLine>
							<addrLine>B.P. 27216 — 14107 Lisieux cedex</addrLine>
							<addrLine>FRANCE</addrLine>
						</address>
						<idno type="URL">http://www.bmlisieux.com/archives/horizont.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Horizontales</title>
								<author>Henri Beauclair</author>
								<edition>2e édition en partie originale</edition>
								<imprint>
									<publisher>Léon Vanier, Paris</publisher>
									<date when="1886">1886</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1885">1885</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les balises de pagination ont été supprimées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BEA7">
				<head type="number">I</head>
				<head type="main">Les Feux du Ciel de Lit</head>
				<opener>
					<epigraph>
						<cit>
							<quote>
								Et le vent, soupirant sous le frais sycomore <lb></lb>
								Allait, tout parfumé, de Sodome à Gomorrhe.
							</quote>
							<bibl>
								<name>VICTOR HUGO</name>, <hi rend="ital">Orientales</hi>.
							</bibl>
						</cit>
					</epigraph>
				</opener>
				<div type="section" n="1">
					<head type="number">I</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Or</w>, <w n="1.2">le</w> <w n="1.3">calme</w> <w n="1.4">du</w> <w n="1.5">soir</w> <w n="1.6">et</w> <w n="1.7">l</w>’<w n="1.8">ombre</w> <w n="1.9">étant</w> <w n="1.10">venus</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Comme</w> <w n="2.2">au</w> <w n="2.3">ciel</w> <w n="2.4">scintillait</w> <w n="2.5">l</w>’<w n="2.6">Étoile</w> <w n="2.7">de</w> <w n="2.8">Vénus</w>,</l>
						<l n="3" num="1.3"><space unit="char" quantity="8"></space><w n="3.1">Paris</w>, <w n="3.2">prince</w> <w n="3.3">de</w> <w n="3.4">la</w> <w n="3.5">Débauche</w>,</l>
						<l n="4" num="1.4"><w n="4.1">S</w>’<w n="4.2">étendit</w> <w n="4.3">sur</w> <w n="4.4">son</w> <w n="4.5">lit</w> <w n="4.6">de</w> <w n="4.7">velours</w> <w n="4.8">et</w> <w n="4.9">cria</w> :</l>
						<l n="5" num="1.5">— <w n="5.1">Fini</w> <w n="5.2">le</w> <w n="5.3">sérieux</w> ! <w n="5.4">Toi</w>, <w n="5.5">viens</w>, <w n="5.6">Luxuria</w>,</l>
						<l n="6" num="1.6"><space unit="char" quantity="8"></space><w n="6.1">Compagne</w> <w n="6.2">fidèle</w>, <w n="6.3">à</w> <w n="6.4">ma</w> <w n="6.5">gauche</w> !</l>
					</lg>
					<lg n="2">
						<l n="7" num="2.1"><w n="7.1">Luxuria</w> <w n="7.2">s</w>’<w n="7.3">assit</w> <w n="7.4">auprès</w> <w n="7.5">du</w> <w n="7.6">vieux</w> <w n="7.7">Paris</w>.</l>
						<l n="8" num="2.2">— <w n="8.1">Hé</w> <w n="8.2">bien</w> ! <w n="8.3">Luxuria</w>, <w n="8.4">tu</w> <w n="8.5">n</w>’<w n="8.6">as</w> <w n="8.7">donc</w> <w n="8.8">rien</w> <w n="8.9">appris</w></l>
						<l n="9" num="2.3"><space unit="char" quantity="8"></space><w n="9.1">De</w> <w n="9.2">neuf</w> ? <w n="9.3">Toujours</w> <w n="9.4">même</w> <w n="9.5">rengaîne</w> ?</l>
						<l n="10" num="2.4"><w n="10.1">Et</w> <w n="10.2">nous</w> <w n="10.3">allons</w> <w n="10.4">encore</w>, <w n="10.5">ainsi</w> <w n="10.6">que</w> <w n="10.7">chaque</w> <w n="10.8">nuit</w>,</l>
						<l n="11" num="2.5"><w n="11.1">Boire</w> <w n="11.2">au</w> <w n="11.3">même</w> <w n="11.4">flacon</w> ? <w n="11.5">Vraiment</w>, <w n="11.6">un</w> <w n="11.7">soir</w> <w n="11.8">d</w>’<w n="11.9">ennui</w>,</l>
						<l n="12" num="2.6"><space unit="char" quantity="8"></space><w n="12.1">J</w>’<w n="12.2">irai</w> <w n="12.3">me</w> <w n="12.4">jeter</w> <w n="12.5">à</w> <w n="12.6">la</w> <w n="12.7">Seine</w> !</l>
					</lg>
					<lg n="3">
						<l n="13" num="3.1">— <w n="13.1">Ah</w> ! <w n="13.2">vous</w> <w n="13.3">ne</w> <w n="13.4">savez</w> <w n="13.5">pas</w> <w n="13.6">goûter</w> <w n="13.7">votre</w> <w n="13.8">bonheur</w>,</l>
						<l n="14" num="3.2"><w n="14.1">Répondit</w> <w n="14.2">sa</w> <w n="14.3">compagne</w>. <w n="14.4">Il</w> <w n="14.5">est</w> <w n="14.6">vrai</w>, <w n="14.7">doux</w> <w n="14.8">seigneur</w>,</l>
						<l n="15" num="3.3"><space unit="char" quantity="8"></space><w n="15.1">Que</w> <w n="15.2">la</w> <w n="15.3">rose</w> <w n="15.4">est</w> <w n="15.5">toujours</w> <w n="15.6">la</w> <w n="15.7">rose</w>,</l>
						<l n="16" num="3.4"><w n="16.1">Pourtant</w>, <w n="16.2">depuis</w> <w n="16.3">qu</w>’<w n="16.4">Adam</w> <w n="16.5">avec</w> <w n="16.6">Ève</w> <w n="16.7">a</w> <w n="16.8">rêvé</w>,</l>
						<l n="17" num="3.5"><w n="17.1">Malgré</w> <w n="17.2">toute</w> <w n="17.3">recherche</w>, <w n="17.4">on</w> <w n="17.5">n</w>’<w n="17.6">a</w> <w n="17.7">pas</w> <w n="17.8">mieux</w> <w n="17.9">trouvé</w>,</l>
						<l n="18" num="3.6"><space unit="char" quantity="8"></space><w n="18.1">Et</w> <w n="18.2">c</w>’<w n="18.3">est</w> <w n="18.4">toujours</w> <w n="18.5">la</w> <w n="18.6">même</w> <w n="18.7">chose</w> !</l>
					</lg>
					<lg n="4">
						<l n="19" num="4.1">— <w n="19.1">Sufficit</w> ! <w n="19.2">dit</w> <w n="19.3">Paris</w> : <w n="19.4">d</w>’<w n="19.5">ailleurs</w>, <w n="19.6">je</w> <w n="19.7">suis</w> <w n="19.8">dispos</w>,</l>
						<l n="20" num="4.2"><w n="20.1">C</w>’<w n="20.2">est</w> <w n="20.3">la</w> <w n="20.4">loi</w> <w n="20.5">que</w> <w n="20.6">le</w> <w n="20.7">loup</w> <w n="20.8">dévore</w> <w n="20.9">les</w> <w n="20.10">troupeaux</w></l>
						<l n="21" num="4.3"><space unit="char" quantity="8"></space><w n="21.1">De</w> <w n="21.2">moutons</w>, <w n="21.3">dans</w> <w n="21.4">la</w> <w n="21.5">plaine</w> <w n="21.6">immense</w> !</l>
						<l n="22" num="4.4"><w n="22.1">A</w> <w n="22.2">moi</w> <w n="22.3">loup</w>, <w n="22.4">les</w> <w n="22.5">beautés</w> <w n="22.6">brebis</w> ! —<w n="22.7">Luxuria</w></l>
						<l n="23" num="4.5"><w n="23.1">Se</w> <w n="23.2">levant</w>, <w n="23.3">souleva</w> <w n="23.4">les</w> <w n="23.5">rideaux</w> <w n="23.6">et</w> <w n="23.7">cria</w> :</l>
						<l n="24" num="4.6"><space unit="char" quantity="8"></space>— <w n="24.1">Vivat</w> ! <w n="24.2">que</w> <w n="24.3">la</w> <w n="24.4">fête</w> <w n="24.5">commence</w> !</l>
					</lg>
				</div>
				<div type="section" n="2">
					<head type="number">II</head>
					<lg n="1">
						<l n="25" num="1.1"><w n="25.1">Alors</w>, <w n="25.2">un</w> <w n="25.3">défilé</w> <w n="25.4">superbe</w> <w n="25.5">commença</w>,</l>
						<l n="26" num="1.2"><w n="26.1">Et</w> <w n="26.2">sous</w> <w n="26.3">le</w> <w n="26.4">ciel</w> <w n="26.5">de</w> <w n="26.6">lit</w>, <w n="26.7">tout</w> <w n="26.8">entier</w> <w n="26.9">il</w> <w n="26.10">passa</w>.</l>
					</lg>
				</div>
				<div type="section" n="3">
					<head type="number">III</head>
					<lg n="1">
						<l n="27" num="1.1"><space unit="char" quantity="10"></space><w n="27.1">Elles</w> <w n="27.2">trottinent</w>, <w n="27.3">par</w> <w n="27.4">groupes</w>,</l>
						<l n="28" num="1.2"><space unit="char" quantity="10"></space><w n="28.1">Joyeuses</w>, <w n="28.2">folles</w>, <w n="28.3">le</w> <w n="28.4">soir</w>,</l>
						<l n="29" num="1.3"><space unit="char" quantity="10"></space><w n="29.1">Tortillant</w> <w n="29.2">leurs</w> <w n="29.3">maigres</w> <w n="29.4">croupes</w>,</l>
						<l n="30" num="1.4"><space unit="char" quantity="10"></space><w n="30.1">Encombrant</w> <w n="30.2">tout</w> <w n="30.3">le</w> <w n="30.4">trottoir</w>.</l>
						<l n="31" num="1.5"><space unit="char" quantity="10"></space><w n="31.1">Elles</w> <w n="31.2">sortent</w> <w n="31.3">des</w> <w n="31.4">passages</w>,</l>
						<l n="32" num="1.6"><space unit="char" quantity="10"></space><w n="32.1">Plumes</w> <w n="32.2">en</w> <w n="32.3">apprentissages</w></l>
						<l n="33" num="1.7"><space unit="char" quantity="10"></space><w n="33.1">Ou</w> <w n="33.2">bien</w> <w n="33.3">fleurs</w> — <w n="33.4">à</w> <w n="33.5">moitié</w> <w n="33.6">sages</w>,</l>
						<l n="34" num="1.8"><space unit="char" quantity="10"></space><w n="34.1">Et</w> <w n="34.2">ne</w> <w n="34.3">demandant</w> <w n="34.4">qu</w>’<w n="34.5">à</w> <w n="34.6">choir</w>.</l>
						<l n="35" num="1.9"><space unit="char" quantity="10"></space><w n="35.1">Blanchisseuses</w> <w n="35.2">et</w> <w n="35.3">cousettes</w>,</l>
						<l n="36" num="1.10"><space unit="char" quantity="10"></space><w n="36.1">elles</w> <w n="36.2">trottent</w>, <w n="36.3">elles</w> <w n="36.4">vont</w>,</l>
						<l n="37" num="1.11"><space unit="char" quantity="10"></space><w n="37.1">Tout</w> <w n="37.2">en</w> <w n="37.3">faisant</w> <w n="37.4">des</w> <w n="37.5">risettes</w>.</l>
						<l n="38" num="1.12"><space unit="char" quantity="10"></space><w n="38.1">Sur</w> <w n="38.2">le</w> <w n="38.3">boulevard</w> <w n="38.4">profond</w>,</l>
						<l n="39" num="1.13"><space unit="char" quantity="10"></space><w n="39.1">Elles</w> <w n="39.2">trottent</w> <w n="39.3">et</w> <w n="39.4">sur</w> <w n="39.5">elles</w>,</l>
						<l n="40" num="1.14"><space unit="char" quantity="10"></space><w n="40.1">Peu</w> <w n="40.2">craintives</w> <w n="40.3">tourterelles</w>,</l>
						<l n="41" num="1.15"><space unit="char" quantity="10"></space><w n="41.1">Le</w> <w n="41.2">vautour</w> <w n="41.3">aux</w> <w n="41.4">noires</w> <w n="41.5">ailes</w>,</l>
						<l n="42" num="1.16"><space unit="char" quantity="10"></space><w n="42.1">Paillard</w>, <w n="42.2">vieux</w> <w n="42.3">ou</w> <w n="42.4">jeune</w>, <w n="42.5">fond</w> !</l>
					</lg>
					<lg n="2">
						<l n="43" num="2.1"><w n="43.1">Or</w> <w n="43.2">une</w> <w n="43.3">large</w> <w n="43.4">voix</w> <w n="43.5">du</w> <w n="43.6">fond</w> <w n="43.7">du</w> <w n="43.8">lit</w> <w n="43.9">venue</w> :</l>
						<l n="44" num="2.2">— <w n="44.1">Bravo</w>, <w n="44.2">Luxuria</w> ! <w n="44.3">ça</w> <w n="44.4">va</w> <w n="44.5">bien</w>, <w n="44.6">continue</w> !</l>
					</lg>
				</div>
				<div type="section" n="4">
					<head type="number">IV</head>
					<lg n="1">
						<l n="45" num="1.1"><w n="45.1">Des</w> <w n="45.2">fiacres</w>. <w n="45.3">Il</w> <w n="45.4">en</w> <w n="45.5">vint</w> <w n="45.6">à</w> <w n="45.7">ne</w> <w n="45.8">plus</w> <w n="45.9">les</w> <w n="45.10">compter</w>.</l>
						<l n="46" num="1.2"><w n="46.1">Alors</w> <w n="46.2">Luxuria</w> <w n="46.3">dans</w> <w n="46.4">l</w>’<w n="46.5">ombre</w> <w n="46.6">fit</w> <w n="46.7">monter</w></l>
						<l n="47" num="1.3"><space unit="char" quantity="8"></space><w n="47.1">Toutes</w> <w n="47.2">les</w> <w n="47.3">femmes</w> <w n="47.4">adultères</w>.</l>
						<l n="48" num="1.4"><w n="48.1">D</w>’<w n="48.2">aucunes</w>, <w n="48.3">en</w> <w n="48.4">tremblant</w>, <w n="48.5">franchissaient</w> <w n="48.6">les</w> <w n="48.7">degrés</w>,</l>
						<l n="49" num="1.5"><w n="49.1">Pâles</w>, <w n="49.2">avec</w> <w n="49.3">des</w> <w n="49.4">yeux</w> <w n="49.5">de</w> <w n="49.6">fièvres</w> <w n="49.7">dévorés</w>,</l>
						<l n="50" num="1.6"><space unit="char" quantity="8"></space><w n="50.1">D</w>’<w n="50.2">autres</w>, <w n="50.3">avec</w> <w n="50.4">des</w> <w n="50.5">airs</w> <w n="50.6">austères</w>.</l>
					</lg>
					<lg n="2">
						<l n="51" num="2.1"><w n="51.1">Tout</w> <w n="51.2">y</w> <w n="51.3">passa</w>, <w n="51.4">la</w> <w n="51.5">goule</w> <w n="51.6">ardente</w>, <w n="51.7">au</w> <w n="51.8">corps</w> <w n="51.9">de</w> <w n="51.10">feu</w>,</l>
						<l n="52" num="2.2"><w n="52.1">L</w>’<w n="52.2">épouse</w> <w n="52.3">langoureuse</w> <w n="52.4">au</w> <w n="52.5">front</w> <w n="52.6">pur</w>, <w n="52.7">à</w> <w n="52.8">l</w>’<w n="52.9">œil</w> <w n="52.10">bleu</w>,</l>
						<l n="53" num="2.3"><space unit="char" quantity="8"></space><w n="53.1">Qui</w> <w n="53.2">pèche</w> <w n="53.3">en</w> <w n="53.4">disant</w> <w n="53.5">ses</w> <w n="53.6">prières</w> !</l>
						<l n="54" num="2.4"><w n="54.1">Des</w> <w n="54.2">femmes</w> <w n="54.3">de</w> <w n="54.4">trente</w> <w n="54.5">ans</w>, <w n="54.6">divines</w>, <w n="54.7">ô</w> <w n="54.8">Balzac</w> !</l>
						<l n="55" num="2.5"><w n="55.1">D</w>’<w n="55.2">autres</w> — <w n="55.3">oh</w> ! <w n="55.4">monstrueux</w> ! — <w n="55.5">qui</w> <w n="55.6">n</w>’<w n="55.7">avaient</w> <w n="55.8">pas</w> <w n="55.9">le</w> <w n="55.10">sac</w></l>
						<l n="56" num="2.6"><space unit="char" quantity="8"></space><w n="56.1">Pour</w> <w n="56.2">leurs</w> <w n="56.3">dettes</w> <w n="56.4">de</w> <w n="56.5">couturières</w> !</l>
					</lg>
					<lg n="3">
						<l n="57" num="3.1">— <w n="57.1">Fichtre</w> ! <w n="57.2">cria</w> <w n="57.3">Paris</w>, <w n="57.4">ça</w> <w n="57.5">va</w> <w n="57.6">de</w> <w n="57.7">mieux</w> <w n="57.8">en</w> <w n="57.9">mieux</w>,</l>
						<l n="58" num="3.2"><w n="58.1">Mais</w>, <w n="58.2">maintenant</w>, <w n="58.3">il</w> <w n="58.4">faut</w> <w n="58.5">nous</w> <w n="58.6">servir</w> <w n="58.7">du</w> <w n="58.8">joyeux</w> !</l>
					</lg>
				</div>
				<div type="section" n="5">
					<head type="number">V</head>
					<lg n="1">
						<l n="59" num="1.1"><w n="59.1">Et</w>, <w n="59.2">par</w> <w n="59.3">une</w> <w n="59.4">portière</w> <w n="59.5">à</w> <w n="59.6">demi</w>-<w n="59.7">soulevée</w></l>
						<l n="60" num="1.2"><w n="60.1">Entrèrent</w> <w n="60.2">des</w> <w n="60.3">clameurs</w> <w n="60.4">folles</w>, <w n="60.5">et</w> <w n="60.6">les</w> <w n="60.7">accords</w></l>
						<l n="61" num="1.3"><w n="61.1">Cascadeurs</w> <w n="61.2">et</w> <w n="61.3">vibrants</w> <w n="61.4">de</w> <w n="61.5">l</w>’<w n="61.6">Évohé</w> <w n="61.7">d</w>’<w n="61.8">Orphée</w>.</l>
						<l n="62" num="1.4"><w n="62.1">L</w>’<w n="62.2">alcôve</w> <w n="62.3">reçut</w> <w n="62.4">une</w> <w n="62.5">avalanche</w> <w n="62.6">de</w> <w n="62.7">corps</w> !</l>
					</lg>
					<lg n="2">
						<l n="63" num="2.1"><space unit="char" quantity="12"></space><w n="63.1">Toute</w> <w n="63.2">la</w> <w n="63.3">confrérie</w></l>
						<l n="64" num="2.2"><space unit="char" quantity="12"></space><w n="64.1">Qui</w> <w n="64.2">rôtit</w> <w n="64.3">le</w> <w n="64.4">balai</w>,</l>
						<l n="65" num="2.3"><space unit="char" quantity="12"></space><w n="65.1">Dames</w> <w n="65.2">de</w> <w n="65.3">brasserie</w></l>
						<l n="66" num="2.4"><space unit="char" quantity="12"></space><w n="66.1">Et</w> <w n="66.2">du</w> <w n="66.3">corps</w> <w n="66.4">de</w> <w n="66.5">ballet</w>,</l>
					</lg>
					<lg n="3">
						<l n="67" num="3.1"><space unit="char" quantity="12"></space><w n="67.1">Petites</w> <w n="67.2">cabotines</w></l>
						<l n="68" num="3.2"><space unit="char" quantity="12"></space><w n="68.1">Et</w> <w n="68.2">chanteuses</w> <w n="68.3">des</w> <w n="68.4">chœurs</w>,</l>
						<l n="69" num="3.3"><space unit="char" quantity="12"></space><w n="69.1">Celles</w> <w n="69.2">dont</w> <w n="69.3">les</w> <w n="69.4">bottines</w></l>
						<l n="70" num="3.4"><space unit="char" quantity="12"></space><w n="70.1">Écrasent</w> <w n="70.2">tant</w> <w n="70.3">de</w> <w n="70.4">cœurs</w>,</l>
					</lg>
					<lg n="4">
						<l n="71" num="4.1"><space unit="char" quantity="12"></space><w n="71.1">Prima</w> <w n="71.2">donna</w> ! <w n="71.3">Divettes</w>,</l>
						<l n="72" num="4.2"><space unit="char" quantity="12"></space><w n="72.1">Les</w> <w n="72.2">étoiles</w> <w n="72.3">qu</w>’<w n="72.4">on</w> <w n="72.5">sert</w>,</l>
						<l n="73" num="4.3"><space unit="char" quantity="12"></space><w n="73.1">Pour</w> <w n="73.2">chanter</w> <w n="73.3">les</w> <w n="73.4">fauvettes</w>,</l>
						<l n="74" num="4.4"><space unit="char" quantity="12"></space><w n="74.1">Dans</w> <w n="74.2">tout</w> <w n="74.3">café</w> <w n="74.4">concert</w>.</l>
					</lg>
					<lg n="5">
						<l n="75" num="5.1"><space unit="char" quantity="12"></space><w n="75.1">Les</w> <w n="75.2">belles</w> <w n="75.3">qui</w> <w n="75.4">ne</w> <w n="75.5">filent</w></l>
						<l n="76" num="5.2"><space unit="char" quantity="12"></space><w n="76.1">Pas</w> <w n="76.2">plus</w> <w n="76.3">que</w> <w n="76.4">les</w> <w n="76.5">lys</w> <w n="76.6">blancs</w></l>
						<l n="77" num="5.3"><space unit="char" quantity="12"></space><w n="77.1">Et</w>, <w n="77.2">chaque</w> <w n="77.3">soir</w>, <w n="77.4">défilent</w></l>
						<l n="78" num="5.4"><space unit="char" quantity="12"></space><w n="78.1">En</w> <w n="78.2">quête</w> <w n="78.3">de</w> <w n="78.4">galants</w>.</l>
					</lg>
					<lg n="6">
						<l n="79" num="6.1"><space unit="char" quantity="12"></space><w n="79.1">Entrent</w> <w n="79.2">avec</w> <w n="79.3">furie</w></l>
						<l n="80" num="6.2"><space unit="char" quantity="12"></space><w n="80.1">Dans</w> <w n="80.2">l</w>’<w n="80.3">alcôve</w> <w n="80.4">au</w> <w n="80.5">complet</w>,</l>
						<l n="81" num="6.3"><space unit="char" quantity="12"></space><w n="81.1">Toute</w> <w n="81.2">la</w> <w n="81.3">confrérie</w></l>
						<l n="82" num="6.4"><space unit="char" quantity="12"></space><w n="82.1">Fait</w> <w n="82.2">flamber</w> <w n="82.3">le</w> <w n="82.4">balai</w> !</l>
					</lg>
				</div>
				<div type="section" n="6">
					<head type="number">VI</head>
					<lg n="1">
						<l n="83" num="1.1"><w n="83.1">Minuit</w> <w n="83.2">sonnait</w> <w n="83.3">alors</w>, <w n="83.4">et</w> <w n="83.5">de</w> <w n="83.6">l</w>’<w n="83.7">alcôve</w> <w n="83.8">claire</w></l>
						<l n="84" num="1.2"><w n="84.1">Montaient</w> <w n="84.2">des</w> <w n="84.3">pleurs</w> <w n="84.4">de</w> <w n="84.5">joie</w> <w n="84.6">et</w> <w n="84.7">des</w> <w n="84.8">cris</w> <w n="84.9">de</w> <w n="84.10">colère</w>.</l>
						<l n="85" num="1.3"><w n="85.1">La</w> <w n="85.2">Folie</w> <w n="85.3">érotique</w> <w n="85.4">étreignait</w> <w n="85.5">les</w> <w n="85.6">cerveaux</w>,</l>
						<l n="86" num="1.4"><w n="86.1">Et</w> <w n="86.2">les</w> <w n="86.3">lèvres</w> <w n="86.4">cherchaient</w>, <w n="86.5">par</w> <w n="86.6">des</w> <w n="86.7">baisers</w> <w n="86.8">nouveaux</w>,</l>
						<l n="87" num="1.5"><w n="87.1">A</w> <w n="87.2">calmer</w> <w n="87.3">un</w> <w n="87.4">instant</w> <w n="87.5">l</w>’<w n="87.6">ardeur</w> <w n="87.7">des</w> <w n="87.8">fièvres</w> <w n="87.9">chaudes</w>.</l>
						<l n="88" num="1.6"><w n="88.1">Les</w> <w n="88.2">bras</w> <w n="88.3">entrelacés</w>, <w n="88.4">marquises</w> <w n="88.5">et</w> <w n="88.6">ribaudes</w>,</l>
						<l n="89" num="1.7"><w n="89.1">Cousettes</w> <w n="89.2">et</w> <w n="89.3">catins</w>, <w n="89.4">sous</w> <w n="89.5">le</w> <w n="89.6">commandement</w></l>
						<l n="90" num="1.8"><w n="90.1">Superbe</w> <w n="90.2">et</w> <w n="90.3">triomphal</w> <w n="90.4">de</w> <w n="90.5">Paris</w>, <w n="90.6">doucement</w></l>
						<l n="91" num="1.9"><w n="91.1">Se</w> <w n="91.2">mirent</w> <w n="91.3">à</w> <w n="91.4">danser</w> <w n="91.5">la</w> <w n="91.6">valse</w> <w n="91.7">lesbienne</w>.</l>
					</lg>
					<lg n="2">
						<l n="92" num="2.1">— <w n="92.1">Ça</w> <w n="92.2">va</w> <w n="92.3">bien</w>, <w n="92.4">dit</w> <w n="92.5">Paris</w>, <w n="92.6">quelle</w> <w n="92.7">joie</w> <w n="92.8">est</w> <w n="92.9">la</w> <w n="92.10">mienne</w> !</l>
						<l n="93" num="2.2"><w n="93.1">Et</w> <w n="93.2">Paris</w> <w n="93.3">était</w> <w n="93.4">las</w>, <w n="93.5">pourtant</w>, <w n="93.6">et</w> <w n="93.7">son</w> <w n="93.8">archet</w></l>
						<l n="94" num="2.3"><w n="94.1">A</w> <w n="94.2">marquer</w> <w n="94.3">la</w> <w n="94.4">cadence</w>, <w n="94.5">à</w> <w n="94.6">son</w> <w n="94.7">côté</w> <w n="94.8">penchait</w>.</l>
					</lg>
					<lg n="3">
						<l n="95" num="3.1"><w n="95.1">Les</w> <w n="95.2">valseuses</w>, <w n="95.3">avec</w> <w n="95.4">leurs</w> <w n="95.5">paupières</w> <w n="95.6">mi</w>-<w n="95.7">closes</w>,</l>
						<l n="96" num="3.2"><w n="96.1">En</w> <w n="96.2">passant</w>, <w n="96.3">effeuillaient</w>, <w n="96.4">le</w> <w n="96.5">long</w> <w n="96.6">du</w> <w n="96.7">lit</w>, <w n="96.8">des</w> <w n="96.9">roses</w>.</l>
						<l n="97" num="3.3"><w n="97.1">Leur</w> <w n="97.2">grâce</w> <w n="97.3">était</w> <w n="97.4">sans</w> <w n="97.5">force</w> <w n="97.6">et</w> <w n="97.7">leur</w> <w n="97.8">sourire</w> <w n="97.9">vain</w>.</l>
						<l n="98" num="3.4"><w n="98.1">Or</w>, <w n="98.2">Paris</w> <w n="98.3">fit</w> <w n="98.4">venir</w>, <w n="98.5">pour</w> <w n="98.6">lui</w> <w n="98.7">verser</w> <w n="98.8">du</w> <w n="98.9">vin</w>,</l>
						<l n="99" num="3.5"><w n="99.1">Voulant</w> <w n="99.2">redoubler</w> <w n="99.3">ses</w> <w n="99.4">étreintes</w> <w n="99.5">fatiguées</w>,</l>
						<l n="100" num="3.6"><w n="100.1">De</w> <w n="100.2">pâles</w> <w n="100.3">jeunes</w> <w n="100.4">gens</w> <w n="100.5">aux</w> <w n="100.6">hanches</w> <w n="100.7">disloquées</w>.</l>
						<l n="101" num="3.7"><w n="101.1">Et</w>, <w n="101.2">dans</w> <w n="101.3">l</w>’<w n="101.4">alcôve</w> <w n="101.5">où</w> <w n="101.6">gît</w> <w n="101.7">l</w>’<w n="101.8">hystérique</w> <w n="101.9">Paris</w>,</l>
						<l n="102" num="3.8"><w n="102.1">Terribles</w>, <w n="102.2">sont</w> <w n="102.3">partis</w> <w n="102.4">à</w> <w n="102.5">l</w>’<w n="102.6">instant</w> <w n="102.7">de</w> <w n="102.8">grands</w> <w n="102.9">cris</w> :</l>
					</lg>
					<lg n="4">
						<l n="103" num="4.1">«<w n="103.1">Encor</w> ! <w n="103.2">Luxuria</w> ! <w n="103.3">je</w> <w n="103.4">suis</w> <w n="103.5">très</w> <w n="103.6">à</w> <w n="103.7">mon</w> <w n="103.8">aise</w> !»</l>
					</lg>
					<lg n="5">
						<l n="104" num="5.1"><w n="104.1">Toute</w> <w n="104.2">la</w> <w n="104.3">vieille</w> <w n="104.4">garde</w> <w n="104.5">entra</w> <w n="104.6">dans</w> <w n="104.7">la</w> <w n="104.8">fournaise</w> !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1884">Octobre 1884.</date>
							</dateline>
					</closer>
				</div>
			</div></body></text></TEI>