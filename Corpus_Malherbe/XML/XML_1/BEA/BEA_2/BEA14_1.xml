<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES HORIZONTALES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BEA">
					<name>
						<forename>Henri</forename>
						<surname>BEAUCLAIR</surname>
					</name>
					<date from="1860" to="1919">1860-1919</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Saisie du texte</resp>
					<name id="SP">
						<forename>S.</forename>
						<surname>Pestel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Relecture du texte</resp>
					<name id="AG">
						<forename>A.</forename>
						<surname>Guézou</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>404 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BEA_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Horizontales</title>
						<author>Henri Beauclair</author>
					</titleStmt>
					<publicationStmt>
						<publisher>LA BIBLIOTHÈQUE ÉLECTRONIQUE DE LISIEUX</publisher>
						<pubPlace>Lisieux</pubPlace>
						<address>
							<addrLine>Place de la République</addrLine>
							<addrLine>B.P. 27216 — 14107 Lisieux cedex</addrLine>
							<addrLine>FRANCE</addrLine>
						</address>
						<idno type="URL">http://www.bmlisieux.com/archives/horizont.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Horizontales</title>
								<author>Henri Beauclair</author>
								<edition>2e édition en partie originale</edition>
								<imprint>
									<publisher>Léon Vanier, Paris</publisher>
									<date when="1886">1886</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1885">1885</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les balises de pagination ont été supprimées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BEA14">
				<head type="number">VIII</head>
				<head type="main">Lesbina</head>
				<opener>
					<epigraph>
						<cit>
							<quote>N’ai-je pas pour toi, belle juive</quote>
							<bibl>
								<name>V. HUGO</name>, <hi rend="ital">Orientales</hi>.
							</bibl>
						</cit>
					</epigraph>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">N</w>’<w n="1.2">ai</w>-<w n="1.3">je</w> <w n="1.4">pas</w> <w n="1.5">pour</w> <w n="1.6">toi</w>, <w n="1.7">ma</w> <w n="1.8">charmante</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Dis</w>-<w n="2.2">le</w> <w n="2.3">moi</w>, <w n="2.4">fait</w> <w n="2.5">encore</w> <w n="2.6">assez</w> ?</l>
					<l n="3" num="1.3"><w n="3.1">O</w> <w n="3.2">ma</w> <w n="3.3">délicieuse</w> <w n="3.4">amante</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Toujours</w> <w n="4.2">quelque</w> <w n="4.3">ennui</w> <w n="4.4">te</w> <w n="4.5">tourmente</w></l>
					<l n="5" num="1.5"><w n="5.1">Quand</w> <w n="5.2">tes</w> <w n="5.3">yeux</w> <w n="5.4">sont</w> <w n="5.5">ainsi</w> <w n="5.6">baissés</w>.</l>
				</lg>
				<lg n="2">
					<l n="6" num="2.1"><w n="6.1">Oh</w> ! <w n="6.2">regarde</w>-<w n="6.3">moi</w>, <w n="6.4">bien</w> <w n="6.5">en</w> <w n="6.6">face</w>,</l>
					<l n="7" num="2.2"><w n="7.1">Et</w> <w n="7.2">réponds</w>-<w n="7.3">moi</w> <w n="7.4">très</w> <w n="7.5">franchement</w>,</l>
					<l n="8" num="2.3"><w n="8.1">Dis</w>-<w n="8.2">moi</w>, <w n="8.3">que</w> <w n="8.4">veux</w>-<w n="8.5">tu</w> <w n="8.6">que</w> <w n="8.7">je</w> <w n="8.8">fasse</w> ?</l>
					<l n="9" num="2.4">— <w n="9.1">Es</w>-<w n="9.2">tu</w> <w n="9.3">jalouse</w> ? <w n="9.4">Que</w> <w n="9.5">je</w> <w n="9.6">chasse</w></l>
					<l n="10" num="2.5"><w n="10.1">Dès</w> <w n="10.2">ce</w> <w n="10.3">soir</w>, <w n="10.4">mon</w> <w n="10.5">dernier</w> <w n="10.6">amant</w> ?</l>
				</lg>
				<lg n="3">
					<l n="11" num="3.1"><w n="11.1">Ah</w> ! <w n="11.2">tu</w> <w n="11.3">souris</w> <w n="11.4">et</w> <w n="11.5">ta</w> <w n="11.6">main</w> <w n="11.7">presse</w></l>
					<l n="12" num="3.2"><w n="12.1">Plus</w> <w n="12.2">doucement</w> <w n="12.3">encor</w> <w n="12.4">ma</w> <w n="12.5">main</w>.</l>
					<l n="13" num="3.3"><w n="13.1">Commande</w>, <w n="13.2">belle</w> <w n="13.3">enchanteresse</w>.</l>
					<l n="14" num="3.4"><w n="14.1">Puisque</w> <w n="14.2">tu</w> <w n="14.3">le</w> <w n="14.4">veux</w>, <w n="14.5">ma</w> <w n="14.6">maîtresse</w>,</l>
					<l n="15" num="3.5"><w n="15.1">Il</w> <w n="15.2">ne</w> <w n="15.3">reviendra</w> <w n="15.4">plus</w>, <w n="15.5">demain</w>.</l>
				</lg>
				<lg n="4">
					<l n="16" num="4.1"><w n="16.1">Je</w> <w n="16.2">suis</w> <w n="16.3">toute</w> <w n="16.4">à</w> <w n="16.5">toi</w>, <w n="16.6">que</w> <w n="16.7">m</w>’<w n="16.8">importe</w>,</l>
					<l n="17" num="4.2"><w n="17.1">Lorsque</w> <w n="17.2">je</w> <w n="17.3">baise</w> <w n="17.4">ton</w> <w n="17.5">front</w> <w n="17.6">blanc</w>,</l>
					<l n="18" num="4.3"><w n="18.1">Que</w> <w n="18.2">cet</w> <w n="18.3">homme</w> <w n="18.4">soit</w> <w n="18.5">à</w> <w n="18.6">ma</w> <w n="18.7">porte</w>,</l>
					<l n="19" num="4.4"><w n="19.1">Et</w> <w n="19.2">qu</w>’<w n="19.3">il</w> <w n="19.4">m</w>’<w n="19.5">adore</w>, <w n="19.6">et</w> <w n="19.7">qu</w>’<w n="19.8">il</w> <w n="19.9">m</w>’<w n="19.10">apporte</w></l>
					<l n="20" num="4.5"><w n="20.1">Son</w> <w n="20.2">cœur</w> <w n="20.3">à</w> <w n="20.4">broyer</w>, <w n="20.5">en</w> <w n="20.6">tremblant</w> !</l>
				</lg>
				<lg n="5">
					<l n="21" num="5.1"><w n="21.1">Perle</w> ! <w n="21.2">Diamant</w> ! <w n="21.3">O</w> <w n="21.4">fleur</w> <w n="21.5">pure</w> !</l>
					<l n="22" num="5.2"><w n="22.1">Jure</w> <w n="22.2">que</w> <w n="22.3">tes</w> <w n="22.4">seins</w> <w n="22.5">adorés</w></l>
					<l n="23" num="5.3"><w n="23.1">Et</w> <w n="23.2">tes</w> <w n="23.3">lèvres</w>, <w n="23.4">grenade</w> <w n="23.5">mûre</w>,</l>
					<l n="24" num="5.4"><w n="24.1">Ne</w> <w n="24.2">subiront</w> <w n="24.3">pas</w> <w n="24.4">la</w> <w n="24.5">souillure</w></l>
					<l n="25" num="5.5"><w n="25.1">Vile</w> <w n="25.2">des</w> <w n="25.3">mâles</w> <w n="25.4">abhorrés</w> !</l>
				</lg>
				<lg n="6">
					<l n="26" num="6.1"><w n="26.1">Laisse</w>-<w n="26.2">moi</w> <w n="26.3">dénouer</w> <w n="26.4">tes</w> <w n="26.5">tresses</w></l>
					<l n="27" num="6.2"><w n="27.1">Et</w> <w n="27.2">dégrafer</w> <w n="27.3">tes</w> <w n="27.4">vêtements</w>,</l>
					<l n="28" num="6.3"><w n="28.1">Pour</w> <w n="28.2">les</w> <w n="28.3">extatiques</w> <w n="28.4">ivresses</w>.</l>
					<l n="29" num="6.4"><w n="29.1">Il</w> <w n="29.2">nous</w> <w n="29.3">faut</w> <w n="29.4">de</w> <w n="29.5">douces</w> <w n="29.6">caresses</w></l>
					<l n="30" num="6.5"><w n="30.1">Et</w> <w n="30.2">de</w> <w n="30.3">tendres</w> <w n="30.4">enlacements</w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1883">Novembre 1883.</date>
						</dateline>
				</closer>
			</div></body></text></TEI>