<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">DERNIÈRES CHANSONS</title>
				<title type="medium">Une édition électronique</title>
				<author key="BOU">
					<name>
						<forename>Louis</forename>
						<surname>BOUILHET</surname>
					</name>
					<date from="1822" to="1869">1822-1869</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2222 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BOU_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>DERNIÈRES CHANSONS</title>
						<title>POÉSIES POSTHUMES</title>
						<author>Louis Bouilhet</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">M253</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>DERNIÈRES CHANSONS</title>
								<title>POÉSIES POSTHUMES</title>
								<author>Louis Bouilhet</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>M. Levy</publisher>
									<date when="1872">1872</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>ŒUVRES DE LOUIS BOUILHET</title>
						<title>FESTONS ET ASTRAGALES, MELÆNIS, DERNIÈRES CHANSONS</title>
						<author>Louis Bouilhet</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ALPHONSE LEMERRE ÉDITEUR</publisher>
							<date when="1891">1891</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1869">1869</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>Les balises de pagination ont été supprimées.</p>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules en début de vers ont été restituées.</p>
					<p>Les chiffres romains de la numérotation des poèmes ont été rétablis.</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-09-20" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.4.xsd)</change>
				<change when="2021-09-20" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BOU103">
				<head type="number">XXXVI</head>
				<head type="main">LE NID ET LE CADRAN</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Près</w> <w n="1.2">du</w> <w n="1.3">cadran</w> <w n="1.4">sonore</w> <w n="1.5">où</w> <w n="1.6">l</w>’<w n="1.7">heure</w> <w n="1.8">se</w> <w n="1.9">balance</w>,</l>
					<l n="2" num="1.2"><w n="2.1">L</w>’<w n="2.2">hirondelle</w> <w n="2.3">a</w> <w n="2.4">bâti</w> <w n="2.5">son</w> <w n="2.6">fragile</w> <w n="2.7">berceau</w> ;</l>
					<l n="3" num="1.3"><w n="3.1">Entendez</w>-<w n="3.2">vous</w> <w n="3.3">deux</w> <w n="3.4">bruits</w> <w n="3.5">monter</w> <w n="3.6">dans</w> <w n="3.7">le</w> <w n="3.8">silence</w> ?</l>
					<l n="4" num="1.4"><w n="4.1">La</w> <w n="4.2">voix</w> <w n="4.3">du</w> <w n="4.4">temps</w> <w n="4.5">se</w> <w n="4.6">mêle</w> <w n="4.7">aux</w> <w n="4.8">chansons</w> <w n="4.9">de</w> <w n="4.10">l</w>’<w n="4.11">oiseau</w> ;</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Sombre</w> <w n="5.2">avertissement</w> <w n="5.3">de</w> <w n="5.4">l</w>’<w n="5.5">heure</w> <w n="5.6">qui</w> <w n="5.7">s</w>’<w n="5.8">envole</w>,</l>
					<l n="6" num="2.2"><w n="6.1">Hymne</w> <w n="6.2">charmant</w> <w n="6.3">du</w> <w n="6.4">nid</w> <w n="6.5">qui</w> <w n="6.6">palpite</w> <w n="6.7">d</w>’<w n="6.8">amour</w>,</l>
					<l n="7" num="2.3"><w n="7.1">Duo</w> <w n="7.2">mystérieux</w> <w n="7.3">à</w> <w n="7.4">la</w> <w n="7.5">haute</w> <w n="7.6">parole</w>,</l>
					<l n="8" num="2.4"><w n="8.1">Que</w> <w n="8.2">Dieu</w> <w n="8.3">fait</w> <w n="8.4">retentir</w> <w n="8.5">sur</w> <w n="8.6">le</w> <w n="8.7">front</w> <w n="8.8">de</w> <w n="8.9">la</w> <w n="8.10">tour</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Comment</w> <w n="9.2">donc</w> <w n="9.3">osas</w>-<w n="9.4">tu</w>, <w n="9.5">voyageuse</w> <w n="9.6">hirondelle</w>,</l>
					<l n="10" num="3.2"><w n="10.1">Aux</w> <w n="10.2">mains</w> <w n="10.3">de</w> <w n="10.4">l</w>’<w n="10.5">oiseleur</w> <w n="10.6">suspendre</w> <w n="10.7">ton</w> <w n="10.8">destin</w> ?</l>
					<l n="11" num="3.3"><w n="11.1">Quand</w> <w n="11.2">l</w>’<w n="11.3">hôte</w> <w n="11.4">au</w> <w n="11.5">front</w> <w n="11.6">morose</w> <w n="11.7">habite</w> <w n="11.8">la</w> <w n="11.9">tourelle</w>,</l>
					<l n="12" num="3.4"><w n="12.1">Comment</w> <w n="12.2">conter</w> <w n="12.3">ta</w> <w n="12.4">joie</w> <w n="12.5">aux</w> <w n="12.6">brises</w> <w n="12.7">du</w> <w n="12.8">matin</w> ?</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Chante</w>, <w n="13.2">chante</w> <w n="13.3">au</w> <w n="13.4">soleil</w> <w n="13.5">ta</w> <w n="13.6">ballade</w> <w n="13.7">amoureuse</w> !</l>
					<l n="14" num="4.2"><w n="14.1">Les</w> <w n="14.2">jours</w> <w n="14.3">n</w>’<w n="14.4">ont</w> <w n="14.5">pas</w> <w n="14.6">pour</w> <w n="14.7">toi</w> <w n="14.8">de</w> <w n="14.9">tristes</w> <w n="14.10">lendemains</w>.</l>
					<l n="15" num="4.3"><w n="15.1">C</w>’<w n="15.2">est</w> <w n="15.3">à</w> <w n="15.4">nous</w> <w n="15.5">de</w> <w n="15.6">pâlir</w> <w n="15.7">quand</w> <w n="15.8">l</w>’<w n="15.9">heure</w> <w n="15.10">à</w> <w n="15.11">la</w> <w n="15.12">voix</w> <w n="15.13">creuse</w></l>
					<l n="16" num="4.4"><w n="16.1">Mesure</w> <w n="16.2">à</w> <w n="16.3">coups</w> <w n="16.4">pressés</w> <w n="16.5">l</w>’<w n="16.6">orchestre</w> <w n="16.7">des</w> <w n="16.8">humains</w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">Chante</w>, <w n="17.2">nid</w> <w n="17.3">de</w> <w n="17.4">l</w>’<w n="17.5">oiseau</w> ! <w n="17.6">J</w>’<w n="17.7">aime</w> <w n="17.8">à</w> <w n="17.9">voir</w> <w n="17.10">sous</w> <w n="17.11">la</w> <w n="17.12">nue</w></l>
					<l n="18" num="5.2"><w n="18.1">Rire</w> <w n="18.2">à</w> <w n="18.3">côté</w> <w n="18.4">du</w> <w n="18.5">temps</w> <w n="18.6">ta</w> <w n="18.7">calme</w> <w n="18.8">volupté</w></l>
					<l n="19" num="5.3"><w n="19.1">Et</w> <w n="19.2">flotter</w> <w n="19.3">dans</w> <w n="19.4">les</w> <w n="19.5">cieux</w> <w n="19.6">mollement</w> <w n="19.7">suspendue</w></l>
					<l n="20" num="5.4"><w n="20.1">Ta</w> <w n="20.2">minute</w> <w n="20.3">joyeuse</w> <w n="20.4">à</w> <w n="20.5">son</w> <w n="20.6">éternité</w>.</l>
				</lg>
			</div></body></text></TEI>