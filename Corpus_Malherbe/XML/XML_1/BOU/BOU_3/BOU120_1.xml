<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">DERNIÈRES CHANSONS</title>
				<title type="medium">Une édition électronique</title>
				<author key="BOU">
					<name>
						<forename>Louis</forename>
						<surname>BOUILHET</surname>
					</name>
					<date from="1822" to="1869">1822-1869</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2222 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BOU_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>DERNIÈRES CHANSONS</title>
						<title>POÉSIES POSTHUMES</title>
						<author>Louis Bouilhet</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">M253</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>DERNIÈRES CHANSONS</title>
								<title>POÉSIES POSTHUMES</title>
								<author>Louis Bouilhet</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>M. Levy</publisher>
									<date when="1872">1872</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>ŒUVRES DE LOUIS BOUILHET</title>
						<title>FESTONS ET ASTRAGALES, MELÆNIS, DERNIÈRES CHANSONS</title>
						<author>Louis Bouilhet</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ALPHONSE LEMERRE ÉDITEUR</publisher>
							<date when="1891">1891</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1869">1869</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>Les balises de pagination ont été supprimées.</p>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules en début de vers ont été restituées.</p>
					<p>Les chiffres romains de la numérotation des poèmes ont été rétablis.</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-09-20" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.4.xsd)</change>
				<change when="2021-09-20" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BOU120">
				<head type="number">LIII</head>
				<head type="main">à UNE JEUNE FILLE</head>
				<head type="sub_2">(traduit d’ANACRÉON.)</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">La</w> <w n="1.2">fille</w> <w n="1.3">de</w> <w n="1.4">Tantale</w>, <w n="1.5">en</w> <w n="1.6">sa</w> <w n="1.7">forme</w> <w n="1.8">nouvelle</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Sur</w> <w n="2.2">les</w> <w n="2.3">bords</w> <w n="2.4">phrygiens</w> <w n="2.5">devint</w> <w n="2.6">pierre</w>, <w n="2.7">dit</w>-<w n="2.8">on</w> ;</l>
					<l n="3" num="1.3"><w n="3.1">Et</w> <w n="3.2">les</w> <w n="3.3">dieux</w> <w n="3.4">ont</w> <w n="3.5">donné</w> <w n="3.6">le</w> <w n="3.7">vol</w> <w n="3.8">de</w> <w n="3.9">l</w>’<w n="3.10">hirondelle</w></l>
					<l n="4" num="1.4"><space unit="char" quantity="8"></space><w n="4.1">À</w> <w n="4.2">la</w> <w n="4.3">fille</w> <w n="4.4">de</w> <w n="4.5">Pandion</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Que</w> <w n="5.2">je</w> <w n="5.3">sois</w> <w n="5.4">ton</w> <w n="5.5">miroir</w>, <w n="5.6">pour</w> <w n="5.7">que</w> <w n="5.8">vers</w> <w n="5.9">moi</w> <w n="5.10">sans</w> <w n="5.11">cesse</w></l>
					<l n="6" num="2.2"><w n="6.1">Tu</w> <w n="6.2">penches</w> <w n="6.3">ton</w> <w n="6.4">beau</w> <w n="6.5">front</w> <w n="6.6">orné</w> <w n="6.7">par</w> <w n="6.8">les</w> <w n="6.9">amours</w> !</l>
					<l n="7" num="2.3"><w n="7.1">Que</w> <w n="7.2">je</w> <w n="7.3">sois</w> <w n="7.4">ta</w> <w n="7.5">tunique</w>, <w n="7.6">ô</w> <w n="7.7">ma</w> <w n="7.8">blanche</w> <w n="7.9">maîtresse</w>,</l>
					<l n="8" num="2.4"><space unit="char" quantity="8"></space><w n="8.1">Pour</w> <w n="8.2">que</w> <w n="8.3">tu</w> <w n="8.4">me</w> <w n="8.5">portes</w> <w n="8.6">toujours</w> !</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Que</w> <w n="9.2">je</w> <w n="9.3">sois</w> <w n="9.4">dans</w> <w n="9.5">ton</w> <w n="9.6">bain</w> <w n="9.7">l</w>’<w n="9.8">onde</w> <w n="9.9">pure</w> <w n="9.10">et</w> <w n="9.11">choisie</w></l>
					<l n="10" num="3.2"><w n="10.1">Pour</w> <w n="10.2">presser</w> <w n="10.3">ton</w> <w n="10.4">beau</w> <w n="10.5">corps</w> <w n="10.6">dans</w> <w n="10.7">mes</w> <w n="10.8">plis</w> <w n="10.9">amoureux</w> !</l>
					<l n="11" num="3.3"><w n="11.1">Que</w> <w n="11.2">je</w> <w n="11.3">sois</w> <w n="11.4">le</w> <w n="11.5">parfum</w>, <w n="11.6">que</w> <w n="11.7">je</w> <w n="11.8">sois</w> <w n="11.9">l</w>’<w n="11.10">ambroisie</w></l>
					<l n="12" num="3.4"><space unit="char" quantity="8"></space><w n="12.1">Pour</w> <w n="12.2">embaumer</w> <w n="12.3">tes</w> <w n="12.4">longs</w> <w n="12.5">cheveux</w> !</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Que</w> <w n="13.2">je</w> <w n="13.3">sois</w> <w n="13.4">le</w> <w n="13.5">collier</w> <w n="13.6">qui</w> <w n="13.7">sur</w> <w n="13.8">ton</w> <w n="13.9">sein</w> <w n="13.10">ruisselle</w> !</l>
					<l n="14" num="4.2"><w n="14.1">Le</w> <w n="14.2">lien</w> <w n="14.3">de</w> <w n="14.4">ta</w> <w n="14.5">gorge</w> <w n="14.6">aux</w> <w n="14.7">suaves</w> <w n="14.8">appas</w> !</l>
					<l n="15" num="4.3"><w n="15.1">Que</w> <w n="15.2">je</w> <w n="15.3">sois</w> <w n="15.4">seulement</w> <w n="15.5">ta</w> <w n="15.6">sandale</w>, <w n="15.7">ô</w> <w n="15.8">ma</w> <w n="15.9">belle</w>,</l>
					<l n="16" num="4.4"><space unit="char" quantity="8"></space><w n="16.1">Pour</w> <w n="16.2">être</w> <w n="16.3">foulé</w> <w n="16.4">sous</w> <w n="16.5">tes</w> <w n="16.6">pas</w> !</l>
				</lg>
			</div></body></text></TEI>