<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">DERNIÈRES CHANSONS</title>
				<title type="medium">Une édition électronique</title>
				<author key="BOU">
					<name>
						<forename>Louis</forename>
						<surname>BOUILHET</surname>
					</name>
					<date from="1822" to="1869">1822-1869</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2222 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BOU_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>DERNIÈRES CHANSONS</title>
						<title>POÉSIES POSTHUMES</title>
						<author>Louis Bouilhet</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">M253</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>DERNIÈRES CHANSONS</title>
								<title>POÉSIES POSTHUMES</title>
								<author>Louis Bouilhet</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>M. Levy</publisher>
									<date when="1872">1872</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>ŒUVRES DE LOUIS BOUILHET</title>
						<title>FESTONS ET ASTRAGALES, MELÆNIS, DERNIÈRES CHANSONS</title>
						<author>Louis Bouilhet</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ALPHONSE LEMERRE ÉDITEUR</publisher>
							<date when="1891">1891</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1869">1869</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>Les balises de pagination ont été supprimées.</p>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules en début de vers ont été restituées.</p>
					<p>Les chiffres romains de la numérotation des poèmes ont été rétablis.</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-09-20" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.4.xsd)</change>
				<change when="2021-09-20" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BOU117">
				<head type="number">L</head>
				<head type="main">ÉTUDE ANTIQUE</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Il</w> <w n="1.2">est</w> <w n="1.3">jeune</w>, <w n="1.4">il</w> <w n="1.5">est</w> <w n="1.6">pâle</w> ‒ <w n="1.7">et</w> <w n="1.8">beau</w> <w n="1.9">comme</w> <w n="1.10">une</w> <w n="1.11">fille</w>.</l>
					<l n="2" num="1.2"><w n="2.1">Ses</w> <w n="2.2">longs</w> <w n="2.3">cheveux</w> <w n="2.4">flottants</w> <w n="2.5">d</w>’<w n="2.6">un</w> <w n="2.7">noeud</w> <w n="2.8">d</w>’<w n="2.9">or</w> <w n="2.10">sont</w> <w n="2.11">liés</w>,</l>
					<l n="3" num="1.3"><w n="3.1">La</w> <w n="3.2">perle</w> <w n="3.3">orientale</w> <w n="3.4">à</w> <w n="3.5">son</w> <w n="3.6">cothurne</w> <w n="3.7">brille</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Il</w> <w n="4.2">danse</w> ‒ <w n="4.3">et</w>, <w n="4.4">secouant</w> <w n="4.5">sa</w> <w n="4.6">torche</w> <w n="4.7">qui</w> <w n="4.8">petille</w>,</l>
					<l n="5" num="1.5"><w n="5.1">À</w> <w n="5.2">l</w>’<w n="5.3">entour</w> <w n="5.4">de</w> <w n="5.5">son</w> <w n="5.6">cou</w> <w n="5.7">fait</w> <w n="5.8">claquer</w> <w n="5.9">ses</w> <w n="5.10">colliers</w>.</l>
				</lg>
				<lg n="2">
					<l n="6" num="2.1"><w n="6.1">Tout</w> <w n="6.2">frotté</w> <w n="6.3">de</w> <w n="6.4">parfums</w> <w n="6.5">et</w> <w n="6.6">la</w> <w n="6.7">tête</w> <w n="6.8">luisante</w>,</l>
					<l n="7" num="2.2"><w n="7.1">Il</w> <w n="7.2">passe</w> <w n="7.3">en</w> <w n="7.4">souriant</w> <w n="7.5">et</w> <w n="7.6">montre</w> <w n="7.7">ses</w> <w n="7.8">bras</w> <w n="7.9">nus</w>.</l>
					<l n="8" num="2.3"><w n="8.1">Un</w> <w n="8.2">lait</w> <w n="8.3">pur</w> <w n="8.4">a</w> <w n="8.5">lavé</w> <w n="8.6">sa</w> <w n="8.7">main</w> <w n="8.8">éblouissante</w>,</l>
					<l n="9" num="2.4"><w n="9.1">Et</w> <w n="9.2">de</w> <w n="9.3">sa</w> <w n="9.4">joue</w> <w n="9.5">en</w> <w n="9.6">fleur</w> <w n="9.7">la</w> <w n="9.8">puberté</w> <w n="9.9">naissante</w></l>
					<l n="10" num="2.5"><w n="10.1">Tombe</w> <w n="10.2">aux</w> <w n="10.3">pinces</w> <w n="10.4">de</w> <w n="10.5">fer</w> <w n="10.6">du</w> <w n="10.7">barbier</w> <w n="10.8">Licinus</w>.</l>
				</lg>
				<lg n="3">
					<l n="11" num="3.1"><w n="11.1">Près</w> <w n="11.2">des</w> <w n="11.3">musiciens</w> <w n="11.4">dont</w> <w n="11.5">la</w> <w n="11.6">flûte</w> <w n="11.7">soupire</w>,</l>
					<l n="12" num="3.2"><w n="12.1">De</w> <w n="12.2">la</w> <w n="12.3">scène</w>, <w n="12.4">en</w> <w n="12.5">rêvant</w>, <w n="12.6">il</w> <w n="12.7">écoute</w> <w n="12.8">le</w> <w n="12.9">bruit</w> ;</l>
					<l n="13" num="3.3"><w n="13.1">Ou</w>, <w n="13.2">laissant</w> <w n="13.3">sur</w> <w n="13.4">ses</w> <w n="13.5">pas</w> <w n="13.6">les</w> <w n="13.7">senteurs</w> <w n="13.8">de</w> <w n="13.9">la</w> <w n="13.10">myrrhe</w>,</l>
					<l n="14" num="3.4"><w n="14.1">Il</w> <w n="14.2">se</w> <w n="14.3">mêle</w> <w n="14.4">au</w> <w n="14.5">troupeau</w> <w n="14.6">des</w> <w n="14.7">femmes</w> <w n="14.8">en</w> <w n="14.9">délire</w>,</l>
					<l n="15" num="3.5"><w n="15.1">Que</w> <w n="15.2">le</w> <w n="15.3">fanal</w> <w n="15.4">des</w> <w n="15.5">bains</w> <w n="15.6">attire</w> <w n="15.7">dans</w> <w n="15.8">la</w> <w n="15.9">nuit</w>.</l>
				</lg>
				<lg n="4">
					<l n="16" num="4.1"><w n="16.1">S</w>’<w n="16.2">il</w> <w n="16.3">a</w> <w n="16.4">de</w> <w n="16.5">ses</w> <w n="16.6">sourcils</w> <w n="16.7">peint</w> <w n="16.8">le</w> <w n="16.9">cercle</w> <w n="16.10">d</w>’<w n="16.11">ébène</w>,</l>
					<l n="17" num="4.2"><w n="17.1">Ce</w> <w n="17.2">n</w>’<w n="17.3">est</w> <w n="17.4">pas</w> <w n="17.5">pour</w> <w n="17.6">Néëre</w> <w n="17.7">ou</w> <w n="17.8">Lesbie</w> <w n="17.9">aux</w> <w n="17.10">bras</w> <w n="17.11">blancs</w>.</l>
					<l n="18" num="4.3"><w n="18.1">Jamais</w>, <w n="18.2">jamais</w> <w n="18.3">sa</w> <w n="18.4">main</w> <w n="18.5">chaude</w> <w n="18.6">de</w> <w n="18.7">votre</w> <w n="18.8">haleine</w>,</l>
					<l n="19" num="4.4"><w n="19.1">Vierges</w>, <w n="19.2">n</w>’<w n="19.3">a</w> <w n="19.4">dénoué</w> <w n="19.5">la</w> <w n="19.6">ceinture</w> <w n="19.7">de</w> <w n="19.8">laine</w></l>
					<l n="20" num="4.5"><w n="20.1">Que</w> <w n="20.2">la</w> <w n="20.3">pudeur</w> <w n="20.4">timide</w> <w n="20.5">attachait</w> <w n="20.6">à</w> <w n="20.7">vos</w> <w n="20.8">flancs</w>.</l>
				</lg>
				<lg n="5">
					<l n="21" num="5.1"><w n="21.1">Pour</w> <w n="21.2">lui</w>, <w n="21.3">le</w> <w n="21.4">proconsul</w> <w n="21.5">épuisera</w> <w n="21.6">l</w>’<w n="21.7">empire</w> ;</l>
					<l n="22" num="5.2"><w n="22.1">Le</w> <w n="22.2">prêtre</w> <w n="22.3">comme</w> <w n="22.4">aux</w> <w n="22.5">dieux</w> <w n="22.6">lui</w> <w n="22.7">donnerait</w> <w n="22.8">l</w>’<w n="22.9">encens</w> ;</l>
					<l n="23" num="5.3"><w n="23.1">Le</w> <w n="23.2">poëte</w> <w n="23.3">l</w>’<w n="23.4">appelle</w> <w n="23.5">ou</w> <w n="23.6">Mopsus</w> <w n="23.7">ou</w> <w n="23.8">Tytire</w>,</l>
					<l n="24" num="5.4"><w n="24.1">Et</w> <w n="24.2">lui</w> <w n="24.3">glisse</w> <w n="24.4">en</w> <w n="24.5">secret</w>, <w n="24.6">sur</w> <w n="24.7">ses</w> <w n="24.8">tables</w> <w n="24.9">de</w> <w n="24.10">cire</w>,</l>
					<l n="25" num="5.5"><w n="25.1">Le</w> <w n="25.2">distique</w> <w n="25.3">amoureux</w>, <w n="25.4">aux</w> <w n="25.5">dactyles</w> <w n="25.6">dansants</w>.</l>
				</lg>
				<lg n="6">
					<l n="26" num="6.1"><w n="26.1">Par</w> <w n="26.2">la</w> <w n="26.3">ville</w>, <w n="26.4">en</w> <w n="26.5">tous</w> <w n="26.6">lieux</w>, <w n="26.7">autour</w> <w n="26.8">de</w> <w n="26.9">lui</w> <w n="26.10">bourdonne</w></l>
					<l n="27" num="6.2"><w n="27.1">L</w>’<w n="27.2">essaim</w> <w n="27.3">des</w> <w n="27.4">jeunes</w> <w n="27.5">gens</w> <w n="27.6">aux</w> <w n="27.7">regards</w> <w n="27.8">enflammés</w>…</l>
					<l n="28" num="6.3"><w n="28.1">Et</w> <w n="28.2">le</w> <w n="28.3">sage</w> <w n="28.4">lui</w>-<w n="28.5">même</w>, <w n="28.6">en</w> <w n="28.7">s</w>’<w n="28.8">arrêtant</w>, <w n="28.9">frissonne</w></l>
					<l n="29" num="6.4"><w n="29.1">Quand</w> <w n="29.2">son</w> <w n="29.3">ombre</w> <w n="29.4">chancelle</w> <w n="29.5">et</w> <w n="29.6">que</w> <w n="29.7">son</w> <w n="29.8">luth</w> <w n="29.9">résonne</w></l>
					<l n="30" num="6.5"><w n="30.1">Au</w> <w n="30.2">fauve</w> <w n="30.3">soupirail</w> <w n="30.4">des</w> <w n="30.5">bouges</w> <w n="30.6">enfumés</w>.</l>
				</lg>
			</div></body></text></TEI>