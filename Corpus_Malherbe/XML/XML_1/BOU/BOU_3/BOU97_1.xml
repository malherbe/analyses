<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">DERNIÈRES CHANSONS</title>
				<title type="medium">Une édition électronique</title>
				<author key="BOU">
					<name>
						<forename>Louis</forename>
						<surname>BOUILHET</surname>
					</name>
					<date from="1822" to="1869">1822-1869</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2222 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BOU_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>DERNIÈRES CHANSONS</title>
						<title>POÉSIES POSTHUMES</title>
						<author>Louis Bouilhet</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">M253</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>DERNIÈRES CHANSONS</title>
								<title>POÉSIES POSTHUMES</title>
								<author>Louis Bouilhet</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>M. Levy</publisher>
									<date when="1872">1872</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>ŒUVRES DE LOUIS BOUILHET</title>
						<title>FESTONS ET ASTRAGALES, MELÆNIS, DERNIÈRES CHANSONS</title>
						<author>Louis Bouilhet</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ALPHONSE LEMERRE ÉDITEUR</publisher>
							<date when="1891">1891</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1869">1869</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>Les balises de pagination ont été supprimées.</p>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules en début de vers ont été restituées.</p>
					<p>Les chiffres romains de la numérotation des poèmes ont été rétablis.</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-09-20" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.4.xsd)</change>
				<change when="2021-09-20" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BOU97">
				<head type="number">XXX</head>
				<head type="main">LIED NORMAND</head>
				<head type="sub_2">(reconstruit avec les débris d’une vieille chanson normande.)</head>
				<lg n="1">
					<l n="1" num="1.1"><space unit="char" quantity="10"></space><w n="1.1">Sous</w> <w n="1.2">le</w> <w n="1.3">chèvrefeuil</w>,</l>
					<l n="2" num="1.2"><space unit="char" quantity="10"></space><w n="2.1">Je</w> <w n="2.2">vidais</w> <w n="2.3">bouteille</w>.</l>
					<l n="3" num="1.3"><space unit="char" quantity="10"></space><w n="3.1">Trois</w> <w n="3.2">amis</w> <w n="3.3">en</w> <w n="3.4">deuil</w></l>
					<l n="4" num="1.4"><space unit="char" quantity="10"></space><w n="4.1">M</w>’<w n="4.2">ont</w> <w n="4.3">dit</w> <w n="4.4">à</w> <w n="4.5">l</w>’<w n="4.6">oreille</w> :</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><space unit="char" quantity="2"></space> ‒ <w n="5.1">eh</w> ! <w n="5.2">Bon</w> ! <w n="5.3">Bon</w> ! <w n="5.4">Bon</w> ! ‒ <w n="5.5">qu</w>’<w n="5.6">on</w> <w n="5.7">nous</w> <w n="5.8">verse</w> <w n="5.9">encor</w> !</l>
					<l n="6" num="2.2"><w n="6.1">Le</w> <w n="6.2">vin</w>, <w n="6.3">c</w>’<w n="6.4">est</w> <w n="6.5">du</w> <w n="6.6">sang</w> ! ‒ <w n="6.7">le</w> <w n="6.8">cidre</w>, <w n="6.9">de</w> <w n="6.10">l</w>’<w n="6.11">or</w> !</l>
				</lg>
				<lg n="3">
					<l n="7" num="3.1"><space unit="char" quantity="10"></space>« <w n="7.1">Prends</w> <w n="7.2">bien</w> <w n="7.3">garde</w> <w n="7.4">à</w> <w n="7.5">toi</w>,</l>
					<l n="8" num="3.2"><space unit="char" quantity="10"></space><w n="8.1">On</w> <w n="8.2">te</w> <w n="8.3">fauche</w> <w n="8.4">l</w>’<w n="8.5">herbe</w>. »</l>
					<l n="9" num="3.3"><space unit="char" quantity="10"></space> ‒ « <w n="9.1">Je</w> <w n="9.2">n</w>’<w n="9.3">ai</w> <w n="9.4">pas</w> <w n="9.5">d</w>’<w n="9.6">effroi</w>,</l>
					<l n="10" num="3.4"><space unit="char" quantity="10"></space><w n="10.1">J</w>’<w n="10.2">ai</w> <w n="10.3">rentré</w> <w n="10.4">ma</w> <w n="10.5">gerbe</w> ! »</l>
				</lg>
				<lg n="4">
					<l n="11" num="4.1"><space unit="char" quantity="2"></space> ‒ <w n="11.1">eh</w> ! <w n="11.2">Bon</w> ! <w n="11.3">Bon</w> ! <w n="11.4">Bon</w> ! ‒ <w n="11.5">qu</w>’<w n="11.6">on</w> <w n="11.7">nous</w> <w n="11.8">verse</w> <w n="11.9">encor</w> !</l>
					<l n="12" num="4.2"><w n="12.1">Le</w> <w n="12.2">vin</w>, <w n="12.3">c</w>’<w n="12.4">est</w> <w n="12.5">du</w> <w n="12.6">sang</w> ! ‒ <w n="12.7">le</w> <w n="12.8">cidre</w>, <w n="12.9">de</w> <w n="12.10">l</w>’<w n="12.11">or</w> !</l>
				</lg>
				<lg n="5">
					<l n="13" num="5.1"><space unit="char" quantity="10"></space><w n="13.1">Il</w> <w n="13.2">prend</w> <w n="13.3">son</w> <w n="13.4">cheval</w>,</l>
					<l n="14" num="5.2"><space unit="char" quantity="10"></space><w n="14.1">Sa</w> <w n="14.2">bride</w> <w n="14.3">et</w> <w n="14.4">sa</w> <w n="14.5">selle</w>,</l>
					<l n="15" num="5.3"><space unit="char" quantity="10"></space><w n="15.1">Et</w> <w n="15.2">court</w>, <w n="15.3">par</w> <w n="15.4">le</w> <w n="15.5">val</w>,</l>
					<l n="16" num="5.4"><space unit="char" quantity="10"></space><w n="16.1">Au</w> <w n="16.2">seuil</w> <w n="16.3">de</w> <w n="16.4">sa</w> <w n="16.5">belle</w>.</l>
				</lg>
				<lg n="6">
					<l n="17" num="6.1"><space unit="char" quantity="2"></space> ‒ <w n="17.1">eh</w> ! <w n="17.2">Bon</w> ! <w n="17.3">Bon</w> ! <w n="17.4">Bon</w> ! ‒ <w n="17.5">qu</w>’<w n="17.6">on</w> <w n="17.7">nous</w> <w n="17.8">verse</w> <w n="17.9">encor</w> !</l>
					<l n="18" num="6.2"><w n="18.1">Le</w> <w n="18.2">vin</w>, <w n="18.3">c</w>’<w n="18.4">est</w> <w n="18.5">du</w> <w n="18.6">sang</w> ! ‒ <w n="18.7">le</w> <w n="18.8">cidre</w>, <w n="18.9">de</w> <w n="18.10">l</w>’<w n="18.11">or</w> !</l>
				</lg>
				<lg n="7">
					<l n="19" num="7.1"><space unit="char" quantity="10"></space><w n="19.1">Y</w> <w n="19.2">trouve</w> <w n="19.3">un</w> <w n="19.4">garçon</w>,</l>
					<l n="20" num="7.2"><space unit="char" quantity="10"></space><w n="20.1">Qui</w> <w n="20.2">faisait</w> <w n="20.3">ripaille</w>,</l>
					<l n="21" num="7.3"><space unit="char" quantity="10"></space><w n="21.1">Lequel</w> <w n="21.2">eut</w> <w n="21.3">frisson</w>,</l>
					<l n="22" num="7.4"><space unit="char" quantity="10"></space><w n="22.1">De</w> <w n="22.2">peur</w> <w n="22.3">de</w> <w n="22.4">bataille</w>.</l>
				</lg>
				<lg n="8">
					<l n="23" num="8.1"><space unit="char" quantity="2"></space> ‒ <w n="23.1">eh</w> ! <w n="23.2">Bon</w> ! <w n="23.3">Bon</w> ! <w n="23.4">Bon</w> ! ‒ <w n="23.5">qu</w>’<w n="23.6">on</w> <w n="23.7">nous</w> <w n="23.8">verse</w> <w n="23.9">encor</w> !</l>
					<l n="24" num="8.2"><w n="24.1">Le</w> <w n="24.2">vin</w>, <w n="24.3">c</w>’<w n="24.4">est</w> <w n="24.5">du</w> <w n="24.6">sang</w> ! ‒ <w n="24.7">le</w> <w n="24.8">cidre</w>, <w n="24.9">de</w> <w n="24.10">l</w>’<w n="24.11">or</w> !</l>
				</lg>
				<lg n="9">
					<l n="25" num="9.1"><space unit="char" quantity="10"></space> ‒ « <w n="25.1">Reste</w> <w n="25.2">désormais</w></l>
					<l n="26" num="9.2"><space unit="char" quantity="10"></space><w n="26.1">Près</w> <w n="26.2">de</w> <w n="26.3">cette</w> <w n="26.4">femme</w> ;</l>
					<l n="27" num="9.3"><space unit="char" quantity="10"></space><w n="27.1">Tu</w> <w n="27.2">n</w>’<w n="27.3">auras</w> <w n="27.4">jamais</w></l>
					<l n="28" num="9.4"><space unit="char" quantity="10"></space><w n="28.1">L</w>’<w n="28.2">orgueil</w> <w n="28.3">de</w> <w n="28.4">mon</w> <w n="28.5">âme</w> ! »</l>
				</lg>
				<lg n="10">
					<l n="29" num="10.1"><space unit="char" quantity="2"></space> ‒ <w n="29.1">eh</w> ! <w n="29.2">Bon</w> ! <w n="29.3">Bon</w> ! <w n="29.4">Bon</w> ! ‒ <w n="29.5">qu</w>’<w n="29.6">on</w> <w n="29.7">nous</w> <w n="29.8">verse</w> <w n="29.9">encor</w> !</l>
					<l n="30" num="10.2"><w n="30.1">Le</w> <w n="30.2">vin</w>, <w n="30.3">c</w>’<w n="30.4">est</w> <w n="30.5">du</w> <w n="30.6">sang</w> ! ‒ <w n="30.7">le</w> <w n="30.8">cidre</w>, <w n="30.9">de</w> <w n="30.10">l</w>’<w n="30.11">or</w> !</l>
				</lg>
				<lg n="11">
					<l n="31" num="11.1"><space unit="char" quantity="10"></space> ‒ « <w n="31.1">Au</w> <w n="31.2">fond</w> <w n="31.3">de</w> <w n="31.4">son</w> <w n="31.5">coeur</w>,</l>
					<l n="32" num="11.2"><space unit="char" quantity="10"></space><w n="32.1">J</w>’<w n="32.2">ai</w> <w n="32.3">cueilli</w> <w n="32.4">naguère</w></l>
					<l n="33" num="11.3"><space unit="char" quantity="10"></space><w n="33.1">Une</w> <w n="33.2">belle</w> <w n="33.3">fleur</w></l>
					<l n="34" num="11.4"><space unit="char" quantity="10"></space><w n="34.1">Qu</w>’<w n="34.2">on</w> <w n="34.3">ne</w> <w n="34.4">trouve</w> <w n="34.5">guère</w>.</l>
				</lg>
				<lg n="12">
					<l n="35" num="12.1"><space unit="char" quantity="2"></space> ‒ <w n="35.1">eh</w> ! <w n="35.2">Bon</w> ! <w n="35.3">Bon</w> ! <w n="35.4">Bon</w> ! ‒ <w n="35.5">qu</w>’<w n="35.6">on</w> <w n="35.7">nous</w> <w n="35.8">verse</w> <w n="35.9">encor</w> !</l>
					<l n="36" num="12.2"><w n="36.1">Le</w> <w n="36.2">vin</w>, <w n="36.3">c</w>’<w n="36.4">est</w> <w n="36.5">du</w> <w n="36.6">sang</w> ! ‒ <w n="36.7">le</w> <w n="36.8">cidre</w>, <w n="36.9">de</w> <w n="36.10">l</w>’<w n="36.11">or</w> !</l>
				</lg>
				<lg n="13">
					<l n="37" num="13.1"><space unit="char" quantity="10"></space>« <w n="37.1">J</w>’<w n="37.2">ai</w> <w n="37.3">couché</w> <w n="37.4">trois</w> <w n="37.5">ans</w>,</l>
					<l n="38" num="13.2"><space unit="char" quantity="10"></space><w n="38.1">La</w> <w n="38.2">nuit</w> <w n="38.3">avec</w> <w n="38.4">elle</w>,</l>
					<l n="39" num="13.3"><space unit="char" quantity="10"></space><w n="39.1">Dans</w> <w n="39.2">de</w> <w n="39.3">beaux</w> <w n="39.4">draps</w> <w n="39.5">blancs</w>,</l>
					<l n="40" num="13.4"><space unit="char" quantity="10"></space><w n="40.1">Garnis</w> <w n="40.2">de</w> <w n="40.3">dentelle</w>.</l>
				</lg>
				<lg n="14">
					<l n="41" num="14.1"><space unit="char" quantity="2"></space> ‒ <w n="41.1">eh</w> ! <w n="41.2">Bon</w> ! <w n="41.3">Bon</w> ! <w n="41.4">Bon</w> ! ‒ <w n="41.5">qu</w>’<w n="41.6">on</w> <w n="41.7">nous</w> <w n="41.8">verse</w> <w n="41.9">encor</w> !</l>
					<l n="42" num="14.2"><w n="42.1">Le</w> <w n="42.2">vin</w>, <w n="42.3">c</w>’<w n="42.4">est</w> <w n="42.5">du</w> <w n="42.6">sang</w> ! ‒ <w n="42.7">le</w> <w n="42.8">cidre</w>, <w n="42.9">de</w> <w n="42.10">l</w>’<w n="42.11">or</w> !</l>
				</lg>
				<lg n="15">
					<l n="43" num="15.1"><space unit="char" quantity="10"></space>« <w n="43.1">Reste</w> <w n="43.2">et</w> <w n="43.3">sois</w> <w n="43.4">joyeux</w> ! »</l>
					<l n="44" num="15.2"><space unit="char" quantity="10"></space> ‒ <w n="44.1">j</w>’<w n="44.2">ai</w> <w n="44.3">trois</w> <w n="44.4">enfants</w> <w n="44.5">d</w>’<w n="44.6">elle</w></l>
					<l n="45" num="15.3"><space unit="char" quantity="10"></space><w n="45.1">L</w>’<w n="45.2">un</w> <w n="45.3">est</w> <w n="45.4">à</w> <w n="45.5">Bayeux</w>,</l>
					<l n="46" num="15.4"><space unit="char" quantity="10"></space><w n="46.1">L</w>’<w n="46.2">autre</w> <w n="46.3">à</w> <w n="46.4">la</w> <w n="46.5">Rochelle</w> !</l>
				</lg>
				<lg n="16">
					<l n="47" num="16.1"><space unit="char" quantity="2"></space> ‒ <w n="47.1">eh</w> ! <w n="47.2">Bon</w> ! <w n="47.3">Bon</w> ! <w n="47.4">Bon</w> ! ‒ <w n="47.5">qu</w>’<w n="47.6">on</w> <w n="47.7">nous</w> <w n="47.8">verse</w> <w n="47.9">encor</w> !</l>
					<l n="48" num="16.2"><w n="48.1">Le</w> <w n="48.2">vin</w>, <w n="48.3">c</w>’<w n="48.4">est</w> <w n="48.5">du</w> <w n="48.6">sang</w> ! ‒ <w n="48.7">le</w> <w n="48.8">cidre</w>, <w n="48.9">de</w> <w n="48.10">l</w>’<w n="48.11">or</w> !</l>
				</lg>
				<lg n="17">
					<l n="49" num="17.1"><space unit="char" quantity="10"></space>« <w n="49.1">Le</w> <w n="49.2">troisième</w>, <w n="49.3">ici</w>,</l>
					<l n="50" num="17.2"><space unit="char" quantity="10"></space><w n="50.1">Dessous</w> <w n="50.2">les</w> <w n="50.3">charmilles</w> !</l>
					<l n="51" num="17.3"><space unit="char" quantity="10"></space><w n="51.1">Fait</w> <w n="51.2">tout</w> <w n="51.3">son</w> <w n="51.4">souci</w></l>
					<l n="52" num="17.4"><space unit="char" quantity="10"></space><w n="52.1">De</w> <w n="52.2">courir</w> <w n="52.3">les</w> <w n="52.4">filles</w> !…</l>
				</lg>
				<lg n="18">
					<l n="53" num="18.1"><space unit="char" quantity="2"></space> ‒ <w n="53.1">eh</w> ! <w n="53.2">Bon</w> ! <w n="53.3">Bon</w> ! <w n="53.4">Bon</w> ! ‒ <w n="53.5">qu</w>’<w n="53.6">on</w> <w n="53.7">nous</w> <w n="53.8">verse</w> <w n="53.9">encor</w> !</l>
					<l n="54" num="18.2"><w n="54.1">Le</w> <w n="54.2">vin</w>, <w n="54.3">c</w>’<w n="54.4">est</w> <w n="54.5">du</w> <w n="54.6">sang</w> ! ‒ <w n="54.7">le</w> <w n="54.8">cidre</w>, <w n="54.9">de</w> <w n="54.10">l</w>’<w n="54.11">or</w> !</l>
				</lg>
				<lg n="19">
					<l n="55" num="19.1"><space unit="char" quantity="10"></space>« ‒ <w n="55.1">reste</w>, <w n="55.2">beau</w> <w n="55.3">vainqueur</w> !</l>
					<l n="56" num="19.2"><space unit="char" quantity="10"></space> ‒ <w n="56.1">moi</w> <w n="56.2">qui</w> <w n="56.3">suis</w> <w n="56.4">leur</w> <w n="56.5">père</w>,</l>
					<l n="57" num="19.3"><space unit="char" quantity="10"></space><w n="57.1">J</w>’<w n="57.2">ai</w> <w n="57.3">noyé</w> <w n="57.4">mon</w> <w n="57.5">coeur</w></l>
					<l n="58" num="19.4"><space unit="char" quantity="10"></space><w n="58.1">Au</w> <w n="58.2">fond</w> <w n="58.3">d</w>’<w n="58.4">un</w> <w n="58.5">grand</w> <w n="58.6">verre</w> ! »</l>
				</lg>
				<lg n="20">
					<l n="59" num="20.1"><space unit="char" quantity="2"></space> ‒ <w n="59.1">eh</w> ! <w n="59.2">Bon</w> ! <w n="59.3">Bon</w> ! <w n="59.4">Bon</w> ! ‒ <w n="59.5">qu</w>’<w n="59.6">on</w> <w n="59.7">nous</w> <w n="59.8">verse</w> <w n="59.9">encor</w> !</l>
					<l n="60" num="20.2"><w n="60.1">Le</w> <w n="60.2">vin</w>, <w n="60.3">c</w>’<w n="60.4">est</w> <w n="60.5">du</w> <w n="60.6">sang</w> ! ‒ <w n="60.7">le</w> <w n="60.8">cidre</w>, <w n="60.9">de</w> <w n="60.10">l</w>’<w n="60.11">or</w> !</l>
				</lg>
			</div></body></text></TEI>