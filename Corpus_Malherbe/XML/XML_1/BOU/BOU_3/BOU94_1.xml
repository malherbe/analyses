<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">DERNIÈRES CHANSONS</title>
				<title type="medium">Une édition électronique</title>
				<author key="BOU">
					<name>
						<forename>Louis</forename>
						<surname>BOUILHET</surname>
					</name>
					<date from="1822" to="1869">1822-1869</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2222 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BOU_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>DERNIÈRES CHANSONS</title>
						<title>POÉSIES POSTHUMES</title>
						<author>Louis Bouilhet</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">M253</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>DERNIÈRES CHANSONS</title>
								<title>POÉSIES POSTHUMES</title>
								<author>Louis Bouilhet</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>M. Levy</publisher>
									<date when="1872">1872</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>ŒUVRES DE LOUIS BOUILHET</title>
						<title>FESTONS ET ASTRAGALES, MELÆNIS, DERNIÈRES CHANSONS</title>
						<author>Louis Bouilhet</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ALPHONSE LEMERRE ÉDITEUR</publisher>
							<date when="1891">1891</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1869">1869</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>Les balises de pagination ont été supprimées.</p>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules en début de vers ont été restituées.</p>
					<p>Les chiffres romains de la numérotation des poèmes ont été rétablis.</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-09-20" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.4.xsd)</change>
				<change when="2021-09-20" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BOU94">
				<head type="number">XXVII</head>
				<head type="main">L’OISELEUR</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Les</w> <w n="1.2">plaines</w>, <w n="1.3">au</w> <w n="1.4">loin</w>, <w n="1.5">de</w> <w n="1.6">fleurs</w> <w n="1.7">sont</w> <w n="1.8">brodées</w>.</l>
					<l n="2" num="1.2"><w n="2.1">Parmi</w> <w n="2.2">les</w> <w n="2.3">oiseaux</w> <w n="2.4">et</w> <w n="2.5">les</w> <w n="2.6">papillons</w>,</l>
					<l n="3" num="1.3"><w n="3.1">J</w>’<w n="3.2">entends</w> <w n="3.3">bourdonner</w> <w n="3.4">l</w>’<w n="3.5">essaim</w> <w n="3.6">des</w> <w n="3.7">idées</w></l>
					<l n="4" num="1.4"><w n="4.1">Qui</w> <w n="4.2">flotte</w> <w n="4.3">au</w> <w n="4.4">soleil</w> <w n="4.5">en</w> <w n="4.6">blancs</w> <w n="4.7">tourbillons</w> !</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Comme</w> <w n="5.2">un</w> <w n="5.3">aigrefin</w> <w n="5.4">méditant</w> <w n="5.5">ses</w> <w n="5.6">crimes</w>,</l>
					<l n="6" num="2.2"><w n="6.1">Sans</w> <w n="6.2">perdre</w> <w n="6.3">un</w> <w n="6.4">moment</w>, <w n="6.5">j</w>’<w n="6.6">apprête</w>, <w n="6.7">en</w> <w n="6.8">sournois</w>,</l>
					<l n="7" num="2.3"><w n="7.1">Un</w> <w n="7.2">beau</w> <w n="7.3">trébuchet</w> <w n="7.4">fait</w> <w n="7.5">avec</w> <w n="7.6">des</w> <w n="7.7">rimes</w> ;</l>
					<l n="8" num="2.4"><w n="8.1">Et</w> <w n="8.2">j</w>’<w n="8.3">attends</w>, ‒ <w n="8.4">caché</w> <w n="8.5">dans</w> <w n="8.6">le</w> <w n="8.7">fond</w> <w n="8.8">des</w> <w n="8.9">bois</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Toutes</w> !… <w n="9.2">les</w> <w n="9.3">voici</w> <w n="9.4">toutes</w> !… <w n="9.5">à</w> <w n="9.6">la</w> <w n="9.7">file</w> !</l>
					<l n="10" num="3.2"><w n="10.1">Hésitant</w> <w n="10.2">un</w> <w n="10.3">peu</w>, <w n="10.4">n</w>’<w n="10.5">osant</w> <w n="10.6">approcher</w>.</l>
					<l n="11" num="3.3"><w n="11.1">Parfois</w> <w n="11.2">un</w> <w n="11.3">manant</w> <w n="11.4">qui</w> <w n="11.5">sort</w> <w n="11.6">de</w> <w n="11.7">la</w> <w n="11.8">ville</w></l>
					<l n="12" num="3.4"><w n="12.1">Vient</w>, <w n="12.2">d</w>’<w n="12.3">un</w> <w n="12.4">bruit</w> <w n="12.5">de</w> <w n="12.6">pas</w>, <w n="12.7">les</w> <w n="12.8">effaroucher</w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Moi</w>, <w n="13.2">je</w> <w n="13.3">reste</w> <w n="13.4">là</w>, <w n="13.5">sans</w> <w n="13.6">voix</w>, <w n="13.7">sans</w> <w n="13.8">haleine</w>,</l>
					<l n="14" num="4.2"><w n="14.1">L</w>’<w n="14.2">oreille</w> <w n="14.3">et</w> <w n="14.4">les</w> <w n="14.5">yeux</w> <w n="14.6">sur</w> <w n="14.7">mon</w> <w n="14.8">traquenard</w>.</l>
					<l n="15" num="4.3"><w n="15.1">Si</w> <w n="15.2">la</w> <w n="15.3">gibecière</w> <w n="15.4">est</w> <w n="15.5">à</w> <w n="15.6">moitié</w> <w n="15.7">pleine</w>,</l>
					<l n="16" num="4.4"><w n="16.1">Je</w> <w n="16.2">rentre</w> <w n="16.3">au</w> <w n="16.4">logis</w>, <w n="16.5">plus</w> <w n="16.6">fier</w> <w n="16.7">qu</w>’<w n="16.8">un</w> <w n="16.9">renard</w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">Et</w> <w n="17.2">c</w>’<w n="17.3">est</w> <w n="17.4">sous</w> <w n="17.5">mes</w> <w n="17.6">doigts</w> <w n="17.7">un</w> <w n="17.8">bruit</w> <w n="17.9">d</w>’<w n="17.10">étincelles</w>,</l>
					<l n="18" num="5.2"><w n="18.1">Quand</w> <w n="18.2">j</w>’<w n="18.3">ouvre</w> <w n="18.4">le</w> <w n="18.5">sac</w> <w n="18.6">où</w> <w n="18.7">tient</w> <w n="18.8">mon</w> <w n="18.9">trésor</w>,</l>
					<l n="19" num="5.3"><w n="19.1">Et</w> <w n="19.2">que</w> <w n="19.3">je</w> <w n="19.4">les</w> <w n="19.5">prends</w>, <w n="19.6">par</w> <w n="19.7">le</w> <w n="19.8">bout</w> <w n="19.9">des</w> <w n="19.10">ailes</w>,</l>
					<l n="20" num="5.4"><w n="20.1">Pour</w> <w n="20.2">les</w> <w n="20.3">enfermer</w> <w n="20.4">dans</w> <w n="20.5">leurs</w> <w n="20.6">cages</w> <w n="20.7">d</w>’<w n="20.8">or</w> !…</l>
				</lg>
			</div></body></text></TEI>