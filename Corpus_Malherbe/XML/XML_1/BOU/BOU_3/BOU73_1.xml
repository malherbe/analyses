<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">DERNIÈRES CHANSONS</title>
				<title type="medium">Une édition électronique</title>
				<author key="BOU">
					<name>
						<forename>Louis</forename>
						<surname>BOUILHET</surname>
					</name>
					<date from="1822" to="1869">1822-1869</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2222 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BOU_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>DERNIÈRES CHANSONS</title>
						<title>POÉSIES POSTHUMES</title>
						<author>Louis Bouilhet</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">M253</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>DERNIÈRES CHANSONS</title>
								<title>POÉSIES POSTHUMES</title>
								<author>Louis Bouilhet</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>M. Levy</publisher>
									<date when="1872">1872</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>ŒUVRES DE LOUIS BOUILHET</title>
						<title>FESTONS ET ASTRAGALES, MELÆNIS, DERNIÈRES CHANSONS</title>
						<author>Louis Bouilhet</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ALPHONSE LEMERRE ÉDITEUR</publisher>
							<date when="1891">1891</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1869">1869</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>Les balises de pagination ont été supprimées.</p>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules en début de vers ont été restituées.</p>
					<p>Les chiffres romains de la numérotation des poèmes ont été rétablis.</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-09-20" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.4.xsd)</change>
				<change when="2021-09-20" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BOU73">
				<head type="number">VI</head>
				<head type="main">OH ! SERAIT-CE VRAI, MA BELLE</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Oh</w> ! <w n="1.2">Serait</w>-<w n="1.3">ce</w> <w n="1.4">vrai</w>, <w n="1.5">ma</w> <w n="1.6">belle</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Ce</w> <w n="2.2">qu</w>’<w n="2.3">un</w> <w n="2.4">prêtre</w> <w n="2.5">m</w>’<w n="2.6">a</w> <w n="2.7">conté</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Qu</w>’<w n="3.2">une</w> <w n="3.3">torture</w> <w n="3.4">éternelle</w></l>
					<l n="4" num="1.4"><w n="4.1">Suit</w> <w n="4.2">la</w> <w n="4.3">douce</w> <w n="4.4">volupté</w>,</l>
					<l n="5" num="1.5"><w n="5.1">Que</w> <w n="5.2">la</w> <w n="5.3">blanche</w> <w n="5.4">main</w> <w n="5.5">des</w> <w n="5.6">femmes</w></l>
					<l n="6" num="1.6"><w n="6.1">Sans</w> <w n="6.2">cesse</w> <w n="6.3">attire</w> <w n="6.4">nos</w> <w n="6.5">âmes</w></l>
					<l n="7" num="1.7"><w n="7.1">Au</w> <w n="7.2">fond</w> <w n="7.3">des</w> <w n="7.4">gouffres</w> <w n="7.5">ardents</w>,</l>
					<l n="8" num="1.8"><w n="8.1">Et</w> <w n="8.2">qu</w>’<w n="8.3">au</w> <w n="8.4">ténébreux</w> <w n="8.5">empire</w></l>
					<l n="9" num="1.9"><w n="9.1">On</w> <w n="9.2">doit</w> <w n="9.3">payer</w> <w n="9.4">un</w> <w n="9.5">sourire</w></l>
					<l n="10" num="1.10"><w n="10.1">Par</w> <w n="10.2">des</w> <w n="10.3">grincements</w> <w n="10.4">de</w> <w n="10.5">dents</w> ?</l>
				</lg>
				<lg n="2">
					<l n="11" num="2.1"><w n="11.1">Ta</w> <w n="11.2">lèvre</w> <w n="11.3">en</w> <w n="11.4">doux</w> <w n="11.5">mots</w> <w n="11.6">abonde</w></l>
					<l n="12" num="2.2"><w n="12.1">Et</w> <w n="12.2">tu</w> <w n="12.3">riras</w> <w n="12.4">de</w> <w n="12.5">mes</w> <w n="12.6">fers</w>,</l>
					<l n="13" num="2.3"><w n="13.1">Juliette</w>, <w n="13.2">dans</w> <w n="13.3">ce</w> <w n="13.4">monde</w>,</l>
					<l n="14" num="2.4"><w n="14.1">Astarté</w>, <w n="14.2">dans</w> <w n="14.3">les</w> <w n="14.4">enfers</w> !</l>
					<l n="15" num="2.5"><w n="15.1">Oui</w>, ‒ <w n="15.2">je</w> <w n="15.3">le</w> <w n="15.4">sens</w>, <w n="15.5">dans</w> <w n="15.6">mon</w> <w n="15.7">âme</w> ‒</l>
					<l n="16" num="2.6"><w n="16.1">Satan</w> <w n="16.2">pour</w> <w n="16.3">soeur</w> <w n="16.4">te</w> <w n="16.5">réclame</w></l>
					<l n="17" num="2.7"><w n="17.1">Aux</w> <w n="17.2">rivages</w> <w n="17.3">embrasés</w> ;</l>
					<l n="18" num="2.8"><w n="18.1">Car</w> <w n="18.2">ton</w> <w n="18.3">regard</w> <w n="18.4">est</w> <w n="18.5">de</w> <w n="18.6">flamme</w>,</l>
					<l n="19" num="2.9"><w n="19.1">Et</w> <w n="19.2">brûlants</w> <w n="19.3">sont</w> <w n="19.4">tes</w> <w n="19.5">baisers</w> !</l>
				</lg>
				<lg n="3">
					<l n="20" num="3.1"><w n="20.1">Calmes</w> <w n="20.2">dans</w> <w n="20.3">leur</w> <w n="20.4">allégresse</w>,</l>
					<l n="21" num="3.2"><w n="21.1">Jamais</w> <w n="21.2">les</w> <w n="21.3">élus</w> <w n="21.4">aux</w> <w n="21.5">cieux</w></l>
					<l n="22" num="3.3"><w n="22.1">N</w>’<w n="22.2">ont</w> <w n="22.3">bu</w> <w n="22.4">cette</w> <w n="22.5">ardente</w> <w n="22.6">ivresse</w></l>
					<l n="23" num="3.4"><w n="23.1">Qui</w> <w n="23.2">petille</w> <w n="23.3">dans</w> <w n="23.4">tes</w> <w n="23.5">yeux</w> ;</l>
					<l n="24" num="3.5"><w n="24.1">Pour</w> <w n="24.2">eux</w> <w n="24.3">jamais</w>, <w n="24.4">ô</w> <w n="24.5">ma</w> <w n="24.6">belle</w>,</l>
					<l n="25" num="3.6"><w n="25.1">Tant</w> <w n="25.2">d</w>’<w n="25.3">amour</w> <w n="25.4">ne</w> <w n="25.5">chargea</w> <w n="25.6">l</w>’<w n="25.7">aile</w></l>
					<l n="26" num="3.7"><w n="26.1">Du</w> <w n="26.2">timide</w> <w n="26.3">séraphin</w>,</l>
					<l n="27" num="3.8"><w n="27.1">Et</w> <w n="27.2">l</w>’<w n="27.3">éternelle</w> <w n="27.4">ambroisie</w></l>
					<l n="28" num="3.9"><w n="28.1">Contient</w> <w n="28.2">moins</w> <w n="28.3">de</w> <w n="28.4">poésie</w></l>
					<l n="29" num="3.10"><w n="29.1">Qu</w>’<w n="29.2">une</w> <w n="29.3">goutte</w> <w n="29.4">de</w> <w n="29.5">ton</w> <w n="29.6">vin</w> !</l>
				</lg>
				<lg n="4">
					<l n="30" num="4.1"><w n="30.1">Démon</w> ! <w n="30.2">Démon</w> ! <w n="30.3">Que</w> <w n="30.4">m</w>’<w n="30.5">importe</w></l>
					<l n="31" num="4.2"><w n="31.1">Que</w> <w n="31.2">par</w> <w n="31.3">une</w> <w n="31.4">dure</w> <w n="31.5">loi</w></l>
					<l n="32" num="4.3"><w n="32.1">Le</w> <w n="32.2">ciel</w> <w n="32.3">me</w> <w n="32.4">ferme</w> <w n="32.5">sa</w> <w n="32.6">porte</w></l>
					<l n="33" num="4.4"><w n="33.1">Si</w> <w n="33.2">j</w>’<w n="33.3">ai</w> <w n="33.4">l</w>’<w n="33.5">enfer</w> <w n="33.6">avec</w> <w n="33.7">toi</w> ?</l>
					<l n="34" num="4.5"><w n="34.1">Fille</w> <w n="34.2">des</w> <w n="34.3">sombres</w> <w n="34.4">phalanges</w>,</l>
					<l n="35" num="4.6"><w n="35.1">Rions</w> <w n="35.2">des</w> <w n="35.3">craintes</w> <w n="35.4">étranges</w></l>
					<l n="36" num="4.7"><w n="36.1">Qui</w> <w n="36.2">planent</w> <w n="36.3">sur</w> <w n="36.4">les</w> <w n="36.5">tombeaux</w> ;</l>
					<l n="37" num="4.8"><w n="37.1">J</w>’<w n="37.2">aurais</w> <w n="37.3">plutôt</w> <w n="37.4">peur</w> <w n="37.5">des</w> <w n="37.6">anges</w>,</l>
					<l n="38" num="4.9"><w n="38.1">Quand</w> <w n="38.2">les</w> <w n="38.3">diables</w> <w n="38.4">sont</w> <w n="38.5">si</w> <w n="38.6">beaux</w> !</l>
				</lg>
			</div></body></text></TEI>