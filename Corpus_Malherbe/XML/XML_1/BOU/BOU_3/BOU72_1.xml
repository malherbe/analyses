<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">DERNIÈRES CHANSONS</title>
				<title type="medium">Une édition électronique</title>
				<author key="BOU">
					<name>
						<forename>Louis</forename>
						<surname>BOUILHET</surname>
					</name>
					<date from="1822" to="1869">1822-1869</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2222 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BOU_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>DERNIÈRES CHANSONS</title>
						<title>POÉSIES POSTHUMES</title>
						<author>Louis Bouilhet</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">M253</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>DERNIÈRES CHANSONS</title>
								<title>POÉSIES POSTHUMES</title>
								<author>Louis Bouilhet</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>M. Levy</publisher>
									<date when="1872">1872</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>ŒUVRES DE LOUIS BOUILHET</title>
						<title>FESTONS ET ASTRAGALES, MELÆNIS, DERNIÈRES CHANSONS</title>
						<author>Louis Bouilhet</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ALPHONSE LEMERRE ÉDITEUR</publisher>
							<date when="1891">1891</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1869">1869</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>Les balises de pagination ont été supprimées.</p>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules en début de vers ont été restituées.</p>
					<p>Les chiffres romains de la numérotation des poèmes ont été rétablis.</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-09-20" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.4.xsd)</change>
				<change when="2021-09-20" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BOU72">
				<head type="number">V</head>
				<head type="main">À ROSETTE</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Mai</w> <w n="1.2">sourit</w> <w n="1.3">au</w> <w n="1.4">firmament</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Mai</w>, <w n="2.2">le</w> <w n="2.3">mois</w> <w n="2.4">des</w> <w n="2.5">douces</w> <w n="2.6">choses</w> ;</l>
					<l n="3" num="1.3"><w n="3.1">Ton</w> <w n="3.2">aveu</w> <w n="3.3">le</w> <w n="3.4">plus</w> <w n="3.5">charmant</w></l>
					<l n="4" num="1.4"><w n="4.1">Est</w> <w n="4.2">venu</w> <w n="4.3">le</w> <w n="4.4">jour</w> <w n="4.5">des</w> <w n="4.6">roses</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Pour</w> <w n="5.2">témoins</w> <w n="5.3">de</w> <w n="5.4">ce</w> <w n="5.5">bonheur</w></l>
					<l n="6" num="2.2"><w n="6.1">Nous</w> <w n="6.2">avons</w> <w n="6.3">pris</w>, <w n="6.4">ô</w> <w n="6.5">ma</w> <w n="6.6">belle</w>,</l>
					<l n="7" num="2.3"><w n="7.1">Le</w> <w n="7.2">premier</w> <w n="7.3">lilas</w> <w n="7.4">en</w> <w n="7.5">fleur</w>,</l>
					<l n="8" num="2.4"><w n="8.1">Et</w> <w n="8.2">la</w> <w n="8.3">première</w> <w n="8.4">hirondelle</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Le</w> <w n="9.2">vallon</w> <w n="9.3">sait</w> <w n="9.4">notre</w> <w n="9.5">amour</w>,</l>
					<l n="10" num="3.2"><w n="10.1">Les</w> <w n="10.2">grands</w> <w n="10.3">bois</w> <w n="10.4">sont</w> <w n="10.5">nos</w> <w n="10.6">complices</w> ;</l>
					<l n="11" num="3.3"><w n="11.1">Les</w> <w n="11.2">lis</w> <w n="11.3">gardent</w>, <w n="11.4">loin</w> <w n="11.5">du</w> <w n="11.6">jour</w>,</l>
					<l n="12" num="3.4"><w n="12.1">Ton</w> <w n="12.2">secret</w>, <w n="12.3">dans</w> <w n="12.4">leurs</w> <w n="12.5">calices</w> !</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Les</w> <w n="13.2">papillons</w> <w n="13.3">nuancés</w></l>
					<l n="14" num="4.2"><w n="14.1">Et</w> <w n="14.2">les</w> <w n="14.3">vertes</w> <w n="14.4">demoiselles</w></l>
					<l n="15" num="4.3"><w n="15.1">Portent</w> <w n="15.2">tes</w> <w n="15.3">serments</w> <w n="15.4">tracés</w></l>
					<l n="16" num="4.4"><w n="16.1">Sur</w> <w n="16.2">la</w> <w n="16.3">poudre</w> <w n="16.4">de</w> <w n="16.5">leurs</w> <w n="16.6">ailes</w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">L</w>’<w n="17.2">étreinte</w> <w n="17.3">des</w> <w n="17.4">lierres</w> <w n="17.5">frais</w>,</l>
					<l n="18" num="5.2"><w n="18.1">Verts</w> <w n="18.2">chaînons</w> <w n="18.3">que</w> <w n="18.4">rien</w> <w n="18.5">ne</w> <w n="18.6">brise</w>,</l>
					<l n="19" num="5.3"><w n="19.1">Figure</w>, <w n="19.2">dans</w> <w n="19.3">les</w> <w n="19.4">forêts</w>,</l>
					<l n="20" num="5.4"><w n="20.1">L</w>’<w n="20.2">ardeur</w> <w n="20.3">que</w> <w n="20.4">tu</w> <w n="20.5">m</w>’<w n="20.6">as</w> <w n="20.7">promise</w>.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">Et</w> <w n="21.2">pour</w> <w n="21.3">qu</w>’<w n="21.4">à</w> <w n="21.5">notre</w> <w n="21.6">dessein</w></l>
					<l n="22" num="6.2"><w n="22.1">Ton</w> <w n="22.2">souvenir</w> <w n="22.3">soit</w> <w n="22.4">fidèle</w>,</l>
					<l n="23" num="6.3"><w n="23.1">Sur</w> <w n="23.2">les</w> <w n="23.3">rondeurs</w> <w n="23.4">de</w> <w n="23.5">ton</w> <w n="23.6">sein</w>,</l>
					<l n="24" num="6.4"><w n="24.1">Tous</w> <w n="24.2">les</w> <w n="24.3">nids</w> <w n="24.4">ont</w> <w n="24.5">pris</w> <w n="24.6">modèle</w>.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">Oh</w> ! <w n="25.2">Ne</w> <w n="25.3">trahis</w> <w n="25.4">pas</w> <w n="25.5">ta</w> <w n="25.6">foi</w> !</l>
					<l n="26" num="7.2"><w n="26.1">Regarde</w>, <w n="26.2">mon</w> <w n="26.3">coeur</w>, <w n="26.4">regarde</w> :</l>
					<l n="27" num="7.3"><w n="27.1">Tout</w> <w n="27.2">l</w>’<w n="27.3">azur</w> <w n="27.4">a</w> <w n="27.5">l</w>’<w n="27.6">oeil</w> <w n="27.7">sur</w> <w n="27.8">toi</w>,</l>
					<l n="28" num="7.4"><w n="28.1">Et</w> <w n="28.2">tout</w> <w n="28.3">le</w> <w n="28.4">printemps</w> <w n="28.5">te</w> <w n="28.6">garde</w> !</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1">Si</w> <w n="29.2">tu</w> <w n="29.3">venais</w> <w n="29.4">à</w> <w n="29.5">mentir</w>,</l>
					<l n="30" num="8.2"><w n="30.1">Les</w> <w n="30.2">muguets</w>, <w n="30.3">aux</w> <w n="30.4">fines</w> <w n="30.5">branches</w>,</l>
					<l n="31" num="8.3"><w n="31.1">Feraient</w> <w n="31.2">tous</w>, <w n="31.3">pour</w> <w n="31.4">m</w>’<w n="31.5">avertir</w>,</l>
					<l n="32" num="8.4"><w n="32.1">Tinter</w> <w n="32.2">leurs</w> <w n="32.3">clochettes</w> <w n="32.4">blanches</w> ;</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1"><w n="33.1">Les</w> <w n="33.2">limaçons</w> <w n="33.3">consternés</w>,</l>
					<l n="34" num="9.2"><w n="34.1">Comme</w> <w n="34.2">des</w> <w n="34.3">prophètes</w> <w n="34.4">mornes</w>,</l>
					<l n="35" num="9.3"><w n="35.1">Par</w> <w n="35.2">les</w> <w n="35.3">chemins</w> <w n="35.4">détournés</w>,</l>
					<l n="36" num="9.4"><w n="36.1">Me</w> <w n="36.2">suivraient</w> <w n="36.3">avec</w> <w n="36.4">des</w> <w n="36.5">cornes</w> ;</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1"><w n="37.1">Et</w> <w n="37.2">les</w> <w n="37.3">oiseaux</w>, <w n="37.4">dans</w> <w n="37.5">la</w> <w n="37.6">nuit</w>,</l>
					<l n="38" num="10.2"><w n="38.1">Se</w> <w n="38.2">heurtant</w> <w n="38.3">à</w> <w n="38.4">ma</w> <w n="38.5">fenêtre</w>,</l>
					<l n="39" num="10.3"><w n="39.1">Me</w> <w n="39.2">rapporteraient</w> <w n="39.3">le</w> <w n="39.4">bruit</w></l>
					<l n="40" num="10.4"><w n="40.1">De</w> <w n="40.2">ta</w> <w n="40.3">rigueur</w> <w n="40.4">prête</w> <w n="40.5">à</w> <w n="40.6">naître</w> !</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1"><w n="41.1">Hélas</w> ! <w n="41.2">Hélas</w> ! <w n="41.3">Les</w> <w n="41.4">beaux</w> <w n="41.5">jours</w></l>
					<l n="42" num="11.2"><w n="42.1">N</w>’<w n="42.2">ont</w> <w n="42.3">qu</w>’<w n="42.4">un</w> <w n="42.5">temps</w>, <w n="42.6">comme</w> <w n="42.7">les</w> <w n="42.8">roses</w>.</l>
					<l n="43" num="11.3"><w n="43.1">J</w>’<w n="43.2">ai</w> <w n="43.3">peur</w> <w n="43.4">des</w> <w n="43.5">grands</w> <w n="43.6">étés</w> <w n="43.7">lourds</w></l>
					<l n="44" num="11.4"><w n="44.1">Et</w> <w n="44.2">des</w> <w n="44.3">grands</w> <w n="44.4">hivers</w> <w n="44.5">moroses</w> !</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1"><w n="45.1">Ces</w> <w n="45.2">mois</w>-<w n="45.3">là</w> <w n="45.4">n</w>’<w n="45.5">ont</w> <w n="45.6">rien</w> <w n="45.7">promis</w>,</l>
					<l n="46" num="12.2"><w n="46.1">Et</w> <w n="46.2">tous</w> <w n="46.3">les</w> <w n="46.4">crimes</w> <w n="46.5">s</w>’<w n="46.6">y</w> <w n="46.7">peuvent</w>,</l>
					<l n="47" num="12.3"><w n="47.1">Sans</w> <w n="47.2">que</w> <w n="47.3">les</w> <w n="47.4">blés</w> <w n="47.5">endormis</w></l>
					<l n="48" num="12.4"><w n="48.1">Ou</w> <w n="48.2">les</w> <w n="48.3">glaçons</w> <w n="48.4">froids</w> <w n="48.5">s</w>’<w n="48.6">émeuvent</w>.</l>
				</lg>
				<lg n="13">
					<l n="49" num="13.1"><w n="49.1">Ô</w> <w n="49.2">mon</w> <w n="49.3">ange</w> ! <w n="49.4">ô</w> <w n="49.5">mon</w> <w n="49.6">trésor</w> !</l>
					<l n="50" num="13.2"><w n="50.1">Cher</w> <w n="50.2">bonheur</w> <w n="50.3">que</w> <w n="50.4">Dieu</w> <w n="50.5">me</w> <w n="50.6">donne</w>,</l>
					<l n="51" num="13.3"><w n="51.1">Jure</w>-<w n="51.2">moi</w> <w n="51.3">d</w>’<w n="51.4">aimer</w> <w n="51.5">encor</w>,</l>
					<l n="52" num="13.4"><w n="52.1">Lorsque</w> <w n="52.2">jaunira</w> <w n="52.3">l</w>’<w n="52.4">automne</w> !</l>
				</lg>
				<lg n="14">
					<l n="53" num="14.1"><w n="53.1">Jure</w>-<w n="53.2">moi</w> !… ‒ <w n="53.3">mais</w> <w n="53.4">tu</w> <w n="53.5">souris</w></l>
					<l n="54" num="14.2"><w n="54.1">De</w> <w n="54.2">mes</w> <w n="54.3">alarmes</w> <w n="54.4">trop</w> <w n="54.5">fortes</w>…</l>
					<l n="55" num="14.3"><w n="55.1">Viens</w> !… <w n="55.2">les</w> <w n="55.3">rameaux</w> <w n="55.4">sont</w> <w n="55.5">fleuris</w>,</l>
					<l n="56" num="14.4"><w n="56.1">Oublions</w> <w n="56.2">les</w> <w n="56.3">feuilles</w> <w n="56.4">mortes</w> !</l>
				</lg>
			</div></body></text></TEI>