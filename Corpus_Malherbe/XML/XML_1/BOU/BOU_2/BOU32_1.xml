<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES. FESTONS ET ASTRAGALES</title>
				<title type="sub"></title>
				<title type="medium">Une édition électronique</title>
				<author key="BOU">
					<name>
						<forename>Louis</forename>
						<surname>BOUILHET</surname>
					</name>
					<date from="1822" to="1869">1822-1869</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2963 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BOU_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>POÉSIES. FESTONS ET ASTRAGALES</title>
						<author>Louis Bouilhet</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k9737201m</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES. FESTONS ET ASTRAGALES</title>
								<author>Louis Bouilhet</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE NOUVELLE</publisher>
									<date when="1859">1859</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1859">1859</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-09-20" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.4.xsd)</change>
				<change when="2021-09-20" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BOU32">
				<head type="main">LES FLAMBEAUX</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Du</w> <w n="1.2">sage</w> <w n="1.3">qui</w> <w n="1.4">médite</w> <w n="1.5">et</w> <w n="1.6">pèse</w>, <w n="1.7">en</w> <w n="1.8">soupirant</w>,</l>
					<l n="2" num="1.2"><space unit="char" quantity="12"></space><w n="2.1">Les</w> <w n="2.2">choses</w> <w n="2.3">de</w> <w n="2.4">la</w> <w n="2.5">vie</w>,</l>
					<l n="3" num="1.3"><w n="3.1">L</w>’<w n="3.2">huile</w> <w n="3.3">onctueuse</w>, <w n="3.4">au</w> <w n="3.5">bord</w> <w n="3.6">du</w> <w n="3.7">vase</w> <w n="3.8">transparent</w>,</l>
					<l n="4" num="1.4"><space unit="char" quantity="12"></space><w n="4.1">Éclaire</w> <w n="4.2">l</w>’<w n="4.3">insomnie</w> !</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Couronné</w> <w n="5.2">de</w> <w n="5.3">verveine</w>, <w n="5.4">et</w> <w n="5.5">tout</w> <w n="5.6">léger</w> <w n="5.7">d</w>’<w n="5.8">espoir</w>,</l>
					<l n="6" num="2.2"><space unit="char" quantity="12"></space><w n="6.1">Entre</w> <w n="6.2">ses</w> <w n="6.3">mains</w> <w n="6.4">joyeuses</w>,</l>
					<l n="7" num="2.3"><w n="7.1">L</w>’<w n="7.2">hyménée</w>, <w n="7.3">en</w> <w n="7.4">chantant</w>, <w n="7.5">secoue</w> <w n="7.6">au</w> <w n="7.7">vent</w> <w n="7.8">du</w> <w n="7.9">soir</w></l>
					<l n="8" num="2.4"><space unit="char" quantity="12"></space><w n="8.1">Les</w> <w n="8.2">torches</w> <w n="8.3">résineuses</w> !</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Berçant</w> <w n="9.2">sur</w> <w n="9.3">les</w> <w n="9.4">festin</w> <w n="9.5">son</w> <w n="9.6">gracieux</w> <w n="9.7">essor</w>,</l>
					<l n="10" num="3.2"><space unit="char" quantity="12"></space><w n="10.1">La</w> <w n="10.2">lampe</w> <w n="10.3">parfumée</w></l>
					<l n="11" num="3.3"><w n="11.1">Semble</w> <w n="11.2">voguer</w> <w n="11.3">dans</w> <w n="11.4">l</w>’<w n="11.5">air</w>, <w n="11.6">comme</w> <w n="11.7">un</w> <w n="11.8">navire</w> <w n="11.9">d</w>’<w n="11.10">or</w></l>
					<l n="12" num="3.4"><space unit="char" quantity="12"></space><w n="12.1">A</w> <w n="12.2">la</w> <w n="12.3">poupe</w> <w n="12.4">enflammée</w> !</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">La</w> <w n="13.2">taverne</w>, <w n="13.3">accroupie</w> <w n="13.4">au</w> <w n="13.5">pied</w> <w n="13.6">du</w> <w n="13.7">Quirinal</w>,</l>
					<l n="14" num="4.2"><space unit="char" quantity="12"></space><w n="14.1">Rayonne</w> <w n="14.2">sur</w> <w n="14.3">la</w> <w n="14.4">rue</w>,</l>
					<l n="15" num="4.3"><w n="15.1">Et</w> <w n="15.2">fait</w> <w n="15.3">voir</w> <w n="15.4">au</w> <w n="15.5">passant</w>, <w n="15.6">sous</w> <w n="15.7">son</w> <w n="15.8">rouge</w> <w n="15.9">fanal</w>,</l>
					<l n="16" num="4.4"><space unit="char" quantity="12"></space><w n="16.1">La</w> <w n="16.2">courtisane</w> <w n="16.3">nue</w> !</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">Le</w> <w n="17.2">feu</w> <w n="17.3">de</w> <w n="17.4">l</w>’<w n="17.5">atrium</w>, <w n="17.6">en</w> <w n="17.7">ses</w> <w n="17.8">bonds</w> <w n="17.9">indécis</w>,</l>
					<l n="18" num="5.2"><space unit="char" quantity="12"></space><w n="18.1">Tremble</w>, <w n="18.2">sous</w> <w n="18.3">le</w> <w n="18.4">portique</w>,</l>
					<l n="19" num="5.3"><w n="19.1">Et</w> <w n="19.2">jette</w> <w n="19.3">un</w> <w n="19.4">gai</w> <w n="19.5">reflet</w> <w n="19.6">aux</w> <w n="19.7">pénates</w> <w n="19.8">assis</w></l>
					<l n="20" num="5.4"><space unit="char" quantity="12"></space><w n="20.1">Près</w> <w n="20.2">du</w> <w n="20.3">foyer</w> <w n="20.4">antique</w> !</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">Le</w> <w n="21.2">hardi</w> <w n="21.3">nautonnier</w> <w n="21.4">qui</w>, <w n="21.5">sur</w> <w n="21.6">les</w> <w n="21.7">flots</w> <w n="21.8">amers</w>,</l>
					<l n="22" num="6.2"><space unit="char" quantity="12"></space><w n="22.1">Creuse</w> <w n="22.2">un</w> <w n="22.3">sillon</w> <w n="22.4">d</w>’<w n="22.5">écume</w>,</l>
					<l n="23" num="6.3"><w n="23.1">A</w> <w n="23.2">le</w> <w n="23.3">phare</w> <w n="23.4">éclatant</w>, <w n="23.5">dont</w> <w n="23.6">la</w> <w n="23.7">brise</w> <w n="23.8">des</w> <w n="23.9">mers</w></l>
					<l n="24" num="6.4"><space unit="char" quantity="12"></space><w n="24.1">Tord</w> <w n="24.2">l</w>’<w n="24.3">aigrette</w> <w n="24.4">qui</w> <w n="24.5">fume</w> !</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">Les</w> <w n="25.2">dieux</w> <w n="25.3">ont</w> <w n="25.4">les</w> <w n="25.5">soleils</w> <w n="25.6">qui</w> <w n="25.7">gravitent</w>, <w n="25.8">sans</w> <w n="25.9">bruit</w>,</l>
					<l n="26" num="7.2"><space unit="char" quantity="12"></space><w n="26.1">Loin</w> <w n="26.2">du</w> <w n="26.3">monde</w> <w n="26.4">où</w> <w n="26.5">nous</w> <w n="26.6">sommes</w> ;</l>
					<l n="27" num="7.3"><w n="27.1">Mais</w> <w n="27.2">le</w> <w n="27.3">puissant</w> <w n="27.4">César</w>, <w n="27.5">pour</w> <w n="27.6">éclairer</w> <w n="27.7">sa</w> <w n="27.8">nuit</w>,</l>
					<l n="28" num="7.4"><space unit="char" quantity="12"></space><w n="28.1">Fait</w> <w n="28.2">allumer</w> <w n="28.3">des</w> <w n="28.4">hommes</w> !</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1">Il</w> <w n="29.2">ordonne</w> : <w n="29.3">et</w>, <w n="29.4">soudain</w>, <w n="29.5">comme</w> <w n="29.6">d</w>’<w n="29.7">un</w> <w n="29.8">linceul</w> <w n="29.9">noir</w>,</l>
					<l n="30" num="8.2"><space unit="char" quantity="12"></space><w n="30.1">Couverte</w> <w n="30.2">de</w> <w n="30.3">résine</w>,</l>
					<l n="31" num="8.3"><w n="31.1">La</w> <w n="31.2">victime</w> <w n="31.3">enflammée</w> <w n="31.4">illumine</w>, <w n="31.5">le</w> <w n="31.6">soir</w>,</l>
					<l n="32" num="8.4"><space unit="char" quantity="12"></space><w n="32.1">Les</w> <w n="32.2">jardins</w> <w n="32.3">de</w> <w n="32.4">Sabine</w> !</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1"><w n="33.1">On</w> <w n="33.2">entend</w> <w n="33.3">dans</w> <w n="33.4">les</w> <w n="33.5">airs</w>, <w n="33.6">parmi</w> <w n="33.7">les</w> <w n="33.8">chants</w> <w n="33.9">joyeux</w>,</l>
					<l n="34" num="9.2"><space unit="char" quantity="12"></space><w n="34.1">Monter</w> <w n="34.2">les</w> <w n="34.3">cris</w> <w n="34.4">sans</w> <w n="34.5">nombre</w></l>
					<l n="35" num="9.3"><w n="35.1">De</w> <w n="35.2">ces</w> <w n="35.3">flambeaux</w> <w n="35.4">vivants</w> <w n="35.5">qui</w> <w n="35.6">luttent</w> <w n="35.7">sous</w> <w n="35.8">les</w> <w n="35.9">feux</w></l>
					<l n="36" num="9.4"><space unit="char" quantity="12"></space><w n="36.1">Et</w> <w n="36.2">qui</w> <w n="36.3">hurlent</w> <w n="36.4">dans</w> <w n="36.5">l</w>’<w n="36.6">ombre</w> !</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1"><w n="37.1">Sabine</w>, <w n="37.2">cependant</w>, <w n="37.3">guide</w> <w n="37.4">un</w> <w n="37.5">rapide</w> <w n="37.6">char</w>,</l>
					<l n="38" num="10.2"><space unit="char" quantity="12"></space><w n="38.1">Par</w> <w n="38.2">la</w> <w n="38.3">longue</w>. <w n="38.4">avenue</w>,</l>
					<l n="39" num="10.3"><w n="39.1">Ou</w> <w n="39.2">laisse</w> <w n="39.3">errer</w> <w n="39.4">ses</w> <w n="39.5">doigts</w> <w n="39.6">sur</w> <w n="39.7">le</w> <w n="39.8">luth</w> <w n="39.9">de</w> <w n="39.10">César</w>,</l>
					<l n="40" num="10.4"><space unit="char" quantity="12"></space><w n="40.1">Rêveuse</w> <w n="40.2">et</w> <w n="40.3">demi</w>-<w n="40.4">nue</w> !</l>
				</lg>
			</div></body></text></TEI>