<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES. FESTONS ET ASTRAGALES</title>
				<title type="sub"></title>
				<title type="medium">Une édition électronique</title>
				<author key="BOU">
					<name>
						<forename>Louis</forename>
						<surname>BOUILHET</surname>
					</name>
					<date from="1822" to="1869">1822-1869</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2963 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BOU_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>POÉSIES. FESTONS ET ASTRAGALES</title>
						<author>Louis Bouilhet</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k9737201m</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES. FESTONS ET ASTRAGALES</title>
								<author>Louis Bouilhet</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE NOUVELLE</publisher>
									<date when="1859">1859</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1859">1859</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-09-20" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.4.xsd)</change>
				<change when="2021-09-20" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BOU29">
				<head type="main">A PRADIER</head>
				<div type="section" n="1">
					<head type="number">I</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Pradier</w>, <w n="1.2">ta</w> <w n="1.3">tombe</w> <w n="1.4">est</w> <w n="1.5">close</w>, <w n="1.6">et</w> <w n="1.7">la</w> <w n="1.8">foule</w> <w n="1.9">écoulée</w></l>
						<l n="2" num="1.2"><w n="2.1">A</w> <w n="2.2">quitté</w> <w n="2.3">le</w> <w n="2.4">gazon</w> <w n="2.5">des</w> <w n="2.6">morts</w> <w n="2.7">silencieux</w> ;</l>
						<l n="3" num="1.3"><w n="3.1">La</w> <w n="3.2">muse</w> <w n="3.3">maintenant</w> <w n="3.4">de</w> <w n="3.5">sa</w> <w n="3.6">douleur</w> <w n="3.7">voilée</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Va</w> <w n="4.2">commencer</w> <w n="4.3">pour</w> <w n="4.4">toi</w> <w n="4.5">l</w>’<w n="4.6">hymne</w> <w n="4.7">religieux</w> !</l>
						<l n="5" num="1.5"><w n="5.1">D</w>’<w n="5.2">autres</w> <w n="5.3">ont</w> <w n="5.4">mis</w> <w n="5.5">leur</w> <w n="5.6">nom</w> <w n="5.7">sur</w> <w n="5.8">la</w> <w n="5.9">strophe</w> <w n="5.10">légère</w>,</l>
						<l n="6" num="1.6"><w n="6.1">D</w>’<w n="6.2">autres</w> <w n="6.3">ont</w> <w n="6.4">la</w> <w n="6.5">couleur</w>, <w n="6.6">ou</w> <w n="6.7">la</w> <w n="6.8">note</w> <w n="6.9">au</w> <w n="6.10">son</w> <w n="6.11">pur</w>,</l>
						<l n="7" num="1.7"><w n="7.1">Mais</w> <w n="7.2">ta</w> <w n="7.3">pensée</w>, <w n="7.4">ô</w> <w n="7.5">maître</w>, <w n="7.6">est</w> <w n="7.7">de</w> <w n="7.8">bronze</w> <w n="7.9">ou</w> <w n="7.10">de</w> <w n="7.11">pierre</w>,</l>
						<l n="8" num="1.8"><w n="8.1">Et</w>, <w n="8.2">comme</w> <w n="8.3">un</w> <w n="8.4">corps</w> <w n="8.5">vivant</w>, <w n="8.6">jette</w> <w n="8.7">son</w> <w n="8.8">ombre</w> <w n="8.9">au</w> <w n="8.10">mur</w>.</l>
					</lg>
					<lg n="2">
						<l n="9" num="2.1"><w n="9.1">Le</w> <w n="9.2">bloc</w> <w n="9.3">âpre</w> <w n="9.4">et</w> <w n="9.5">rugueux</w>, <w n="9.6">sous</w> <w n="9.7">ta</w> <w n="9.8">main</w> <w n="9.9">souveraine</w>,</l>
						<l n="10" num="2.2"><w n="10.1">Ondulait</w> <w n="10.2">comme</w> <w n="10.3">un</w> <w n="10.4">dos</w> <w n="10.5">de</w> <w n="10.6">léopard</w> <w n="10.7">dompté</w> ;</l>
						<l n="11" num="2.3"><w n="11.1">Et</w> <w n="11.2">la</w> <w n="11.3">forme</w>, <w n="11.4">à</w> <w n="11.5">ta</w> <w n="11.6">voix</w>, <w n="11.7">touchant</w> <w n="11.8">le</w> <w n="11.9">socle</w> <w n="11.10">à</w> <w n="11.11">peine</w>,</l>
						<l n="12" num="2.4"><w n="12.1">S</w>’<w n="12.2">élançait</w> <w n="12.3">dans</w> <w n="12.4">sa</w> <w n="12.5">grâce</w> <w n="12.6">et</w> <w n="12.7">sa</w> <w n="12.8">virginité</w>.</l>
						<l n="13" num="2.5"><w n="13.1">Quand</w> <w n="13.2">les</w> <w n="13.3">marteaux</w> <w n="13.4">sonnaient</w> <w n="13.5">en</w> <w n="13.6">cadence</w> <w n="13.7">rapide</w>,</l>
						<l n="14" num="2.6"><w n="14.1">Quand</w> <w n="14.2">l</w>’<w n="14.3">atelier</w> <w n="14.4">vivait</w>, <w n="14.5">fourmillant</w> <w n="14.6">et</w> <w n="14.7">joyeux</w>,</l>
						<l n="15" num="2.7"><w n="15.1">Et</w> <w n="15.2">que</w>, <w n="15.3">couvrant</w> <w n="15.4">les</w> <w n="15.5">murs</w> <w n="15.6">de</w> <w n="15.7">sa</w> <w n="15.8">neige</w> <w n="15.9">solide</w>,</l>
						<l n="16" num="2.8"><w n="16.1">La</w> <w n="16.2">poussière</w> <w n="16.3">du</w> <w n="16.4">marbre</w> <w n="16.5">étincelait</w> <w n="16.6">aux</w> <w n="16.7">yeux</w>,</l>
					</lg>
					<lg n="3">
						<l n="17" num="3.1"><w n="17.1">C</w>’<w n="17.2">était</w> <w n="17.3">ton</w> <w n="17.4">heure</w> <w n="17.5">à</w> <w n="17.6">toi</w> ! <w n="17.7">ta</w> <w n="17.8">passion</w> ! <w n="17.9">ta</w> <w n="17.10">vie</w> !</l>
						<l n="18" num="3.2"><w n="18.1">A</w> <w n="18.2">ton</w> <w n="18.3">front</w> <w n="18.4">élargi</w> <w n="18.5">le</w> <w n="18.6">sang</w> <w n="18.7">battait</w> <w n="18.8">plus</w> <w n="18.9">fort</w>,</l>
						<l n="19" num="3.3"><w n="19.1">Et</w> <w n="19.2">ton</w> <w n="19.3">âme</w> <w n="19.4">flottait</w>, <w n="19.5">dans</w> <w n="19.6">l</w>’<w n="19.7">idéal</w> <w n="19.8">ravie</w>,</l>
						<l n="20" num="3.4"><w n="20.1">Comme</w> <w n="20.2">un</w> <w n="20.3">vaisseau</w> <w n="20.4">qui</w> <w n="20.5">chante</w> <w n="20.6">en</w> <w n="20.7">s</w>’<w n="20.8">éloignant</w> <w n="20.9">du</w> <w n="20.10">port</w> !</l>
						<l n="21" num="3.5"><w n="21.1">Tu</w> <w n="21.2">t</w>’<w n="21.3">exilais</w> <w n="21.4">du</w> <w n="21.5">monde</w> <w n="21.6">au</w> <w n="21.7">milieu</w> <w n="21.8">des</w> <w n="21.9">déesses</w>,</l>
						<l n="22" num="3.6"><w n="22.1">Chœur</w> <w n="22.2">immobile</w> <w n="22.3">et</w> <w n="22.4">blanc</w> <w n="22.5">qui</w> <w n="22.6">souriait</w> <w n="22.7">toujours</w>,</l>
						<l n="23" num="3.7"><w n="23.1">Bacchantes</w> <w n="23.2">au</w> <w n="23.3">sein</w> <w n="23.4">nu</w>, <w n="23.5">Dianes</w> <w n="23.6">chasseresses</w>,</l>
						<l n="24" num="3.8"><w n="24.1">Et</w> <w n="24.2">nymphes</w> <w n="24.3">dans</w> <w n="24.4">le</w> <w n="24.5">bain</w> <w n="24.6">tordant</w> <w n="24.7">leurs</w> <w n="24.8">cheveux</w> <w n="24.9">lourds</w> !</l>
					</lg>
					<lg n="4">
						<l n="25" num="4.1"><w n="25.1">La</w> <w n="25.2">beauté</w> <w n="25.3">qui</w> <w n="25.4">périt</w>, <w n="25.5">le</w> <w n="25.6">sentiment</w> <w n="25.7">qui</w> <w n="25.8">passe</w>,</l>
						<l n="26" num="4.2"><w n="26.1">S</w>’<w n="26.2">arrêtaient</w> <w n="26.3">dans</w> <w n="26.4">ton</w> <w n="26.5">œuvre</w> <w n="26.6">immortels</w>, <w n="26.7">radieux</w>…</l>
						<l n="27" num="4.3"><w n="27.1">Car</w> <w n="27.2">tu</w> <w n="27.3">sors</w>, <w n="27.4">ô</w> <w n="27.5">Pradier</w> ! <w n="27.6">de</w> <w n="27.7">cette</w> <w n="27.8">forte</w> <w n="27.9">race</w></l>
						<l n="28" num="4.4"><w n="28.1">Qui</w> <w n="28.2">peupla</w> <w n="28.3">le</w> <w n="28.4">ciel</w> <w n="28.5">vide</w> <w n="28.6">et</w> <w n="28.7">nous</w> <w n="28.8">tailla</w> <w n="28.9">des</w> <w n="28.10">dieux</w> !</l>
					</lg>
				</div>
				<div type="section" n="2">
					<head type="number">II</head>
					<lg n="1">
						<l n="29" num="1.1"><w n="29.1">Amis</w> ; <w n="29.2">ne</w> <w n="29.3">pleurons</w> <w n="29.4">pas</w> ! <w n="29.5">au</w> <w n="29.6">pays</w> <w n="29.7">bleu</w> <w n="29.8">des</w> <w n="29.9">âmes</w>,</l>
						<l n="30" num="1.2"><w n="30.1">Il</w> <w n="30.2">est</w>, <w n="30.3">il</w> <w n="30.4">est</w> <w n="30.5">peut</w>-<w n="30.6">être</w> <w n="30.7">un</w> <w n="30.8">asile</w> <w n="30.9">écarté</w></l>
						<l n="31" num="1.3"><w n="31.1">Où</w> <w n="31.2">les</w> <w n="31.3">maîtres</w> <w n="31.4">divins</w> <w n="31.5">qu</w>’<w n="31.6">ici</w>-<w n="31.7">bas</w> <w n="31.8">nous</w> <w n="31.9">aimâmes</w></l>
						<l n="32" num="1.4"><w n="32.1">Vivent</w> <w n="32.2">pleins</w> <w n="32.3">de</w> <w n="32.4">jeunesse</w> <w n="32.5">et</w> <w n="32.6">de</w> <w n="32.7">sérénité</w>.</l>
						<l n="33" num="1.5"><w n="33.1">Leur</w> <w n="33.2">front</w> <w n="33.3">calme</w> <w n="33.4">est</w> <w n="33.5">orné</w> <w n="33.6">de</w> <w n="33.7">guirlandes</w> <w n="33.8">fleuries</w>,</l>
						<l n="34" num="1.6"><w n="34.1">Le</w> <w n="34.2">soleil</w> <w n="34.3">de</w> <w n="34.4">l</w>’<w n="34.5">idée</w> <w n="34.6">inonde</w> <w n="34.7">leur</w> <w n="34.8">regard</w>.</l>
						<l n="35" num="1.7"><w n="35.1">Ils</w> <w n="35.2">suivent</w> <w n="35.3">lentement</w> <w n="35.4">de</w> <w n="35.5">longues</w> <w n="35.6">galeries</w>,</l>
						<l n="36" num="1.8"><w n="36.1">Et</w> <w n="36.2">vont</w> <w n="36.3">causant</w> <w n="36.4">entre</w> <w n="36.5">eux</w>, <w n="36.6">de</w> <w n="36.7">la</w> <w n="36.8">forme</w> <w n="36.9">et</w> <w n="36.10">de</w> <w n="36.11">l</w>’<w n="36.12">art</w> !</l>
					</lg>
					<lg n="2">
						<l n="37" num="2.1"><w n="37.1">Sculpteurs</w>, <w n="37.2">musiciens</w>, <w n="37.3">et</w> <w n="37.4">peintres</w> <w n="37.5">et</w> <w n="37.6">poëtes</w>,.</l>
						<l n="38" num="2.2"><w n="38.1">Ils</w> <w n="38.2">sont</w> <w n="38.3">là</w> <w n="38.4">tous</w>, <w n="38.5">rêvant</w> <w n="38.6">au</w> <w n="38.7">passé</w> <w n="38.8">glorieux</w> ;</l>
						<l n="39" num="2.3"><w n="39.1">L</w>’<w n="39.2">œuvre</w> <w n="39.3">de</w> <w n="39.4">leur</w> <w n="39.5">génie</w> <w n="39.6">a</w> <w n="39.7">peuplé</w> <w n="39.8">ces</w> <w n="39.9">retraites</w>,</l>
						<l n="40" num="2.4"><w n="40.1">Et</w> <w n="40.2">leurs</w> <w n="40.3">créations</w> <w n="40.4">s</w>’<w n="40.5">agitent</w> <w n="40.6">autour</w> <w n="40.7">d</w>’<w n="40.8">eux</w>.</l>
					</lg>
					<lg n="3">
						<l n="41" num="3.1"><w n="41.1">Polyclète</w> <w n="41.2">y</w> <w n="41.3">sourit</w> <w n="41.4">près</w> <w n="41.5">de</w> <w n="41.6">Junon</w> <w n="41.7">la</w> <w n="41.8">belle</w> ;</l>
						<l n="42" num="3.2"><w n="42.1">A</w> <w n="42.2">tes</w> <w n="42.3">pieds</w>, <w n="42.4">ô</w> <w n="42.5">Vénus</w> ! <w n="42.6">Cléomène</w> <w n="42.7">est</w> <w n="42.8">assis</w> ;</l>
						<l n="43" num="3.3"><w n="43.1">Le</w> <w n="43.2">satyre</w>, <w n="43.3">échappé</w> <w n="43.4">des</w> <w n="43.5">mains</w> <w n="43.6">de</w> <w n="43.7">Praxitèle</w>,</l>
						<l n="44" num="3.4"><w n="44.1">Ouvre</w> <w n="44.2">sa</w> <w n="44.3">bouche</w> <w n="44.4">avide</w> <w n="44.5">aux</w> <w n="44.6">raisins</w> <w n="44.7">de</w> <w n="44.8">Zeuxis</w> ;</l>
					</lg>
					<lg n="4">
						<l n="45" num="4.1"><w n="45.1">Stasicrate</w>, <w n="45.2">en</w> <w n="45.3">sueur</w>, <w n="45.4">sculpte</w> <w n="45.5">au</w> <w n="45.6">loin</w> <w n="45.7">sa</w> <w n="45.8">montagne</w>,</l>
						<l n="46" num="4.2"><w n="46.1">Mu</w>’<w n="46.2">on</w> <w n="46.3">suit</w>, <w n="46.4">dans</w> <w n="46.5">les</w> <w n="46.6">prés</w>, <w n="46.7">ses</w> <w n="46.8">génisses</w> <w n="46.9">d</w>’<w n="46.10">airain</w>,</l>
						<l n="47" num="4.3"><w n="47.1">Et</w> <w n="47.2">le</w> <w n="47.3">vieil</w> <w n="47.4">Amphion</w>, <w n="47.5">chantant</w> <w n="47.6">par</w> <w n="47.7">la</w> <w n="47.8">campagne</w>,</l>
						<l n="48" num="4.4"><w n="48.1">Fait</w> <w n="48.2">danser</w> <w n="48.3">les</w> <w n="48.4">rochers</w> <w n="48.5">sur</w> <w n="48.6">le</w> <w n="48.7">mode</w> <w n="48.8">thébain</w> !</l>
					</lg>
					<lg n="5">
						<l n="49" num="5.1"><w n="49.1">C</w>’<w n="49.2">est</w> <w n="49.3">là</w> <w n="49.4">qu</w>’<w n="49.5">il</w> <w n="49.6">est</w> <w n="49.7">monté</w> <w n="49.8">parmi</w> <w n="49.9">les</w> <w n="49.10">statuaires</w> ;</l>
						<l n="50" num="5.2"><w n="50.1">Il</w> <w n="50.2">habite</w> <w n="50.3">un</w> <w n="50.4">beau</w> <w n="50.5">temple</w>, <w n="50.6">aux</w> <w n="50.7">murs</w> <w n="50.8">étincelants</w>,</l>
						<l n="51" num="5.3"><w n="51.1">Et</w>, <w n="51.2">timides</w> <w n="51.3">encor</w>, <w n="51.4">près</w> <w n="51.5">des</w> <w n="51.6">déesses</w> <w n="51.7">fières</w>,</l>
						<l n="52" num="5.4"><w n="52.1">Nissia</w>, <w n="52.2">puis</w> <w n="52.3">Sapho</w>, <w n="52.4">s</w>’<w n="52.5">avancent</w> <w n="52.6">à</w> <w n="52.7">pas</w> <w n="52.8">lents</w> !</l>
					</lg>
					<lg n="6">
						<l n="53" num="6.1"><w n="53.1">Entrez</w> !… <w n="53.2">vous</w> <w n="53.3">qui</w> <w n="53.4">mêlez</w> <w n="53.5">aux</w> <w n="53.6">lignes</w> <w n="53.7">solennelles</w></l>
						<l n="54" num="6.2"><w n="54.1">Les</w> <w n="54.2">langueurs</w> <w n="54.3">du</w> <w n="54.4">contour</w> <w n="54.5">et</w> <w n="54.6">le</w> <w n="54.7">pli</w> <w n="54.8">gracieux</w>,</l>
						<l n="55" num="6.3"><w n="55.1">Filles</w> <w n="55.2">des</w> <w n="55.3">temps</w> <w n="55.4">nouveaux</w>, <w n="55.5">vous</w> <w n="55.6">êtes</w> <w n="55.7">immortelles</w>,</l>
						<l n="56" num="6.4"><w n="56.1">A</w> <w n="56.2">côté</w> <w n="56.3">des</w>. <w n="56.4">Vénus</w> <w n="56.5">Pradier</w> <w n="56.6">vous</w> <w n="56.7">place</w> <w n="56.8">aux</w> <w n="56.9">cieux</w> !</l>
					</lg>
				</div>
			</div></body></text></TEI>