<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES. FESTONS ET ASTRAGALES</title>
				<title type="sub"></title>
				<title type="medium">Une édition électronique</title>
				<author key="BOU">
					<name>
						<forename>Louis</forename>
						<surname>BOUILHET</surname>
					</name>
					<date from="1822" to="1869">1822-1869</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2963 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BOU_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>POÉSIES. FESTONS ET ASTRAGALES</title>
						<author>Louis Bouilhet</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k9737201m</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES. FESTONS ET ASTRAGALES</title>
								<author>Louis Bouilhet</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE NOUVELLE</publisher>
									<date when="1859">1859</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1859">1859</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-09-20" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.4.xsd)</change>
				<change when="2021-09-20" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BOU50">
				<head type="main">LES RAISINS <lb></lb>AU CLAIR DE LUNE</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Dans</w> <w n="1.2">la</w> <w n="1.3">vigne</w>, <w n="1.4">au</w> <w n="1.5">mur</w> <w n="1.6">étalée</w>,</l>
					<l n="2" num="1.2"><w n="2.1">La</w> <w n="2.2">lune</w> <w n="2.3">glisse</w> <w n="2.4">lentement</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Et</w>, <w n="3.2">sous</w> <w n="3.3">la</w> <w n="3.4">feuille</w> <w n="3.5">dentelée</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Caresse</w> <w n="4.2">le</w> <w n="4.3">raisin</w> <w n="4.4">dormant</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Tout</w> <w n="5.2">à</w> <w n="5.3">coup</w> <w n="5.4">la</w> <w n="5.5">grappe</w> <w n="5.6">en</w> <w n="5.7">alerte</w></l>
					<l n="6" num="2.2"><w n="6.1">S</w>’<w n="6.2">éveille</w> <w n="6.3">et</w> <w n="6.4">croit</w> <w n="6.5">le</w> <w n="6.6">jour</w> <w n="6.7">venu</w> ;</l>
					<l n="7" num="2.3"><w n="7.1">Chaque</w> <w n="7.2">grain</w>, <w n="7.3">gonflant</w> <w n="7.4">sa</w> <w n="7.5">peau</w> <w n="7.6">verte</w>,</l>
					<l n="8" num="2.4"><w n="8.1">Frissonne</w> <w n="8.2">au</w> <w n="8.3">vent</w> <w n="8.4">comme</w> <w n="8.5">un</w> <w n="8.6">sein</w> <w n="8.7">nu</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Chaque</w> <w n="9.2">bourgeon</w>, <w n="9.3">rouge</w> <w n="9.4">de</w> <w n="9.5">honte</w>,</l>
					<l n="10" num="3.2"><w n="10.1">Semble</w> <w n="10.2">une</w> <w n="10.3">perle</w> <w n="10.4">de</w> <w n="10.5">corail</w> ;</l>
					<l n="11" num="3.3"><w n="11.1">Le</w> <w n="11.2">tronc</w> <w n="11.3">frémit</w>, <w n="11.4">la</w> <w n="11.5">sève</w> <w n="11.6">monte</w>,</l>
					<l n="12" num="3.4"><w n="12.1">Toute</w> <w n="12.2">la</w> <w n="12.3">vigne</w> <w n="12.4">est</w> <w n="12.5">en</w> <w n="12.6">travail</w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Clarté</w> <w n="13.2">menteuse</w> ! <w n="13.3">erreur</w> <w n="13.4">fatale</w> !</l>
					<l n="14" num="4.2"><w n="14.1">O</w> <w n="14.2">vigne</w>, <w n="14.3">reprends</w> <w n="14.4">ton</w> <w n="14.5">sommeil</w> ;</l>
					<l n="15" num="4.3"><w n="15.1">Ce</w> <w n="15.2">n</w>’<w n="15.3">est</w> <w n="15.4">point</w> <w n="15.5">à</w> <w n="15.6">ce</w> <w n="15.7">reflet</w> <w n="15.8">pâle</w></l>
					<l n="16" num="4.4"><w n="16.1">Que</w> <w n="16.2">ton</w> <w n="16.3">sang</w> <w n="16.4">deviendra</w> <w n="16.5">vermeil</w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">Pampres</w> <w n="17.2">pressés</w>, <w n="17.3">attendez</w> <w n="17.4">l</w>’<w n="17.5">heure</w>,</l>
					<l n="18" num="5.2"><w n="18.1">L</w>’<w n="18.2">aube</w> <w n="18.3">du</w> <w n="18.4">jour</w> <w n="18.5">est</w> <w n="18.6">loin</w> <w n="18.7">encor</w>,</l>
					<l n="19" num="5.3"><w n="19.1">Et</w> <w n="19.2">ce</w> <w n="19.3">rayon</w> <w n="19.4">qui</w> <w n="19.5">vous</w> <w n="19.6">effleure</w></l>
					<l n="20" num="5.4"><w n="20.1">Est</w> <w n="20.2">plus</w> <w n="20.3">froid</w> <w n="20.4">qu</w>’<w n="20.5">un</w> <w n="20.6">baiser</w> <w n="20.7">de</w> <w n="20.8">mort</w> !</l>
				</lg>
			</div></body></text></TEI>