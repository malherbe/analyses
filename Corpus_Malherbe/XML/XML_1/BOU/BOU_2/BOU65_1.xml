<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES. FESTONS ET ASTRAGALES</title>
				<title type="sub"></title>
				<title type="medium">Une édition électronique</title>
				<author key="BOU">
					<name>
						<forename>Louis</forename>
						<surname>BOUILHET</surname>
					</name>
					<date from="1822" to="1869">1822-1869</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2963 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BOU_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>POÉSIES. FESTONS ET ASTRAGALES</title>
						<author>Louis Bouilhet</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k9737201m</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES. FESTONS ET ASTRAGALES</title>
								<author>Louis Bouilhet</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE NOUVELLE</publisher>
									<date when="1859">1859</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1859">1859</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-09-20" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.4.xsd)</change>
				<change when="2021-09-20" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BOU65">
				<head type="main">CEUX QUI VIENNENT</head>
				<opener>
					<salute>A CHARLES D’OSMOY</salute>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">A</w> <w n="1.2">l</w>’<w n="1.3">heure</w> <w n="1.4">où</w> <w n="1.5">le</w> <w n="1.6">sommeil</w> <w n="1.7">commence</w>,</l>
					<l n="2" num="1.2"><w n="2.1">J</w>’<w n="2.2">ai</w> <w n="2.3">fait</w> <w n="2.4">un</w> <w n="2.5">rêve</w>, <w n="2.6">et</w> <w n="2.7">j</w>’<w n="2.8">ai</w> <w n="2.9">cru</w> <w n="2.10">voir</w></l>
					<l n="3" num="1.3"><w n="3.1">S</w>’<w n="3.2">allonger</w> <w n="3.3">une</w> <w n="3.4">plaine</w> <w n="3.5">immense</w></l>
					<l n="4" num="1.4"><w n="4.1">Que</w> <w n="4.2">terminait</w> <w n="4.3">un</w> <w n="4.4">grand</w> <w n="4.5">trou</w> <w n="4.6">noir</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Vers</w> <w n="5.2">le</w> <w n="5.3">gouffre</w> <w n="5.4">qui</w> <w n="5.5">les</w> <w n="5.6">appelle</w>,</l>
					<l n="6" num="2.2"><w n="6.1">Chassés</w> <w n="6.2">par</w> <w n="6.3">un</w> <w n="6.4">destin</w> <w n="6.5">de</w> <w n="6.6">fer</w>,</l>
					<l n="7" num="2.3"><w n="7.1">Hommes</w> <w n="7.2">et</w> <w n="7.3">femmes</w>, <w n="7.4">pèle</w>-<w n="7.5">mêle</w>,</l>
					<l n="8" num="2.4"><w n="8.1">Roulaient</w>, <w n="8.2">comme</w> <w n="8.3">un</w> <w n="8.4">fleuve</w> <w n="8.5">à</w> <w n="8.6">la</w> <w n="8.7">mer</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Et</w> <w n="9.2">derrière</w> <w n="9.3">le</w> <w n="9.4">troupeau</w> <w n="9.5">sombre</w>,</l>
					<l n="10" num="3.2"><w n="10.1">Mes</w> <w n="10.2">yeux</w> <w n="10.3">cherchaient</w>, <w n="10.4">avec</w> <w n="10.5">effort</w>,</l>
					<l n="11" num="3.3"><w n="11.1">Ta</w> <w n="11.2">vieille</w> <w n="11.3">faux</w> <w n="11.4">qui</w> <w n="11.5">luit</w> <w n="11.6">dans</w> <w n="11.7">l</w>’<w n="11.8">ombre</w>,</l>
					<l n="12" num="3.4"><w n="12.1">O</w> <w n="12.2">vieux</w> <w n="12.3">squelette</w> <w n="12.4">de</w> <w n="12.5">la</w> <w n="12.6">mort</w> !</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Je</w> <w n="13.2">ne</w> <w n="13.3">t</w>’<w n="13.4">aperçus</w> <w n="13.5">point</w>, <w n="13.6">camarde</w> !…</l>
					<l n="14" num="4.2"><w n="14.1">Mais</w> <w n="14.2">ce</w> <w n="14.3">que</w> <w n="14.4">je</w> <w n="14.5">vis</w> <w n="14.6">devant</w> <w n="14.7">moi</w></l>
					<l n="15" num="4.3"><w n="15.1">S</w>’<w n="15.2">agiter</w>, <w n="15.3">dans</w> <w n="15.4">la</w> <w n="15.5">nuit</w> <w n="15.6">blafarde</w>,</l>
					<l n="16" num="4.4"><w n="16.1">M</w>’<w n="16.2">a</w> <w n="16.3">paru</w> <w n="16.4">plus</w> <w n="16.5">affreux</w> <w n="16.6">que</w> <w n="16.7">toi</w> !</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">C</w>’<w n="17.2">était</w> <w n="17.3">une</w> <w n="17.4">bruyante</w> <w n="17.5">armée</w></l>
					<l n="18" num="5.2"><w n="18.1">De</w> <w n="18.2">petits</w> <w n="18.3">hommes</w> <w n="18.4">incomplets</w> :</l>
					<l n="19" num="5.3"><w n="19.1">Monde</w> <w n="19.2">exigu</w>, <w n="19.3">peuple</w> <w n="19.4">pygmée</w>,</l>
					<l n="20" num="5.4"><w n="20.1">Portant</w> <w n="20.2">au</w> <w n="20.3">front</w> <w n="20.4">des</w> <w n="20.5">bourrelets</w>.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">Les</w> <w n="21.2">uns</w> <w n="21.3">jetaient</w> <w n="21.4">des</w> <w n="21.5">clameurs</w> <w n="21.6">grêles</w>,</l>
					<l n="22" num="6.2"><w n="22.1">Et</w>, <w n="22.2">des</w> <w n="22.3">deux</w> <w n="22.4">mains</w>, <w n="22.5">ramant</w> <w n="22.6">dans</w> <w n="22.7">l</w>’<w n="22.8">air</w>,</l>
					<l n="23" num="6.3"><w n="23.1">Chancelaient</w> <w n="23.2">sur</w> <w n="23.3">leurs</w> <w n="23.4">jambes</w> <w n="23.5">frêles</w>,</l>
					<l n="24" num="6.4"><w n="24.1">Comme</w> <w n="24.2">des</w> <w n="24.3">barques</w> <w n="24.4">sur</w> <w n="24.5">la</w> <w n="24.6">mer</w>.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">D</w>’<w n="25.2">autres</w>, <w n="25.3">la</w> <w n="25.4">bouche</w> <w n="25.5">de</w> <w n="25.6">lait</w> <w n="25.7">pleine</w>,</l>
					<l n="26" num="7.2"><w n="26.1">Avec</w> <w n="26.2">des</w> <w n="26.3">gestes</w> <w n="26.4">menaçants</w>,</l>
					<l n="27" num="7.3"><w n="27.1">Lançaient</w> <w n="27.2">dans</w> <w n="27.3">la</w> <w n="27.4">mêlée</w> <w n="27.5">humaine</w></l>
					<l n="28" num="7.4"><w n="28.1">Leurs</w> <w n="28.2">chariots</w> <w n="28.3">retentissants</w>.</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1">Les</w> <w n="29.2">derniers</w>, <w n="29.3">plus</w> <w n="29.4">faibles</w> <w n="29.5">encore</w>,</l>
					<l n="30" num="8.2"><w n="30.1">Se</w> <w n="30.2">traînant</w> <w n="30.3">de</w> <w n="30.4">tous</w> <w n="30.5">les</w> <w n="30.6">côtés</w>,</l>
					<l n="31" num="8.3"><w n="31.1">Semblaient</w> <w n="31.2">des</w> <w n="31.3">larves</w> <w n="31.4">près</w> <w n="31.5">d</w>’<w n="31.6">éclore</w>,</l>
					<l n="32" num="8.4"><w n="32.1">Dans</w> <w n="32.2">leurs</w> <w n="32.3">langes</w> <w n="32.4">emmaillotés</w>.</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1"><w n="33.1">Ils</w> <w n="33.2">criaient</w> : « <w n="33.3">Notre</w> <w n="33.4">heure</w> <w n="33.5">est</w> <w n="33.6">venue</w> !</l>
					<l n="34" num="9.2"><w n="34.1">A</w> <w n="34.2">nous</w> <w n="34.3">la</w> <w n="34.4">terre</w> <w n="34.5">des</w> <w n="34.6">vivants</w> !… »</l>
					<l n="35" num="9.3"><w n="35.1">Et</w> <w n="35.2">tous</w> <w n="35.3">les</w> <w n="35.4">hochets</w>, <w n="35.5">sous</w> <w n="35.6">la</w> <w n="35.7">nue</w>,</l>
					<l n="36" num="9.4"><w n="36.1">Secouaient</w> <w n="36.2">leurs</w> <w n="36.3">grelots</w> <w n="36.4">mouvants</w> ;</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1"><w n="37.1">Et</w> <w n="37.2">les</w> <w n="37.3">voix</w> <w n="37.4">exterminatrices</w></l>
					<l n="38" num="10.2"><w n="38.1">Frappant</w> <w n="38.2">du</w> <w n="38.3">ciel</w> <w n="38.4">les</w> <w n="38.5">noirs</w> <w n="38.6">arceaux</w>,</l>
					<l n="39" num="10.3"><w n="39.1">Entonnaient</w>, <w n="39.2">sur</w> <w n="39.3">l</w>’<w n="39.4">air</w> <w n="39.5">des</w> <w n="39.6">nourrices</w>,</l>
					<l n="40" num="10.4"><w n="40.1">La</w> <w n="40.2">Marseillaise</w> <w n="40.3">des</w> <w n="40.4">berceaux</w>.</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1"><w n="41.1">Pourtant</w>, <w n="41.2">ô</w> <w n="41.3">tendresse</w> <w n="41.4">profonde</w> !</l>
					<l n="42" num="11.2"><w n="42.1">La</w> <w n="42.2">foule</w>, <w n="42.3">un</w> <w n="42.4">pied</w> <w n="42.5">dans</w> <w n="42.6">le</w> <w n="42.7">cercueil</w>,</l>
					<l n="43" num="11.3"><w n="43.1">Vers</w> <w n="43.2">les</w> <w n="43.3">bandits</w> <w n="43.4">à</w> <w n="43.5">tète</w> <w n="43.6">blonde</w></l>
					<l n="44" num="11.4"><w n="44.1">Se</w> <w n="44.2">retournait</w> <w n="44.3">ivre</w> <w n="44.4">d</w>’<w n="44.5">orgueil</w> ;</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1"><w n="45.1">Et</w> <w n="45.2">les</w> <w n="45.3">familles</w> <w n="45.4">insensées</w>,</l>
					<l n="46" num="12.2"><w n="46.1">Avec</w> <w n="46.2">des</w> <w n="46.3">rires</w> <w n="46.4">triomphants</w>,</l>
					<l n="47" num="12.3"><w n="47.1">S</w>’<w n="47.2">en</w> <w n="47.3">allaient</w> <w n="47.4">au</w> <w n="47.5">tombeau</w>, <w n="47.6">poussées</w></l>
					<l n="48" num="12.4"><w n="48.1">Par</w> <w n="48.2">le</w> <w n="48.3">bras</w> <w n="48.4">rose</w> <w n="48.5">des</w> <w n="48.6">enfants</w> !</l>
				</lg>
			</div></body></text></TEI>