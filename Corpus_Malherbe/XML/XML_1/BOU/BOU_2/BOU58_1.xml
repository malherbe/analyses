<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES. FESTONS ET ASTRAGALES</title>
				<title type="sub"></title>
				<title type="medium">Une édition électronique</title>
				<author key="BOU">
					<name>
						<forename>Louis</forename>
						<surname>BOUILHET</surname>
					</name>
					<date from="1822" to="1869">1822-1869</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2963 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BOU_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>POÉSIES. FESTONS ET ASTRAGALES</title>
						<author>Louis Bouilhet</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k9737201m</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES. FESTONS ET ASTRAGALES</title>
								<author>Louis Bouilhet</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE NOUVELLE</publisher>
									<date when="1859">1859</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1859">1859</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-09-20" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.4.xsd)</change>
				<change when="2021-09-20" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BOU58">
				<head type="main">JOUR SANS SOLEIL</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">La</w> <w n="1.2">brume</w> <w n="1.3">a</w> <w n="1.4">noyé</w> <w n="1.5">l</w>’<w n="1.6">horizon</w> <w n="1.7">blafard</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Les</w> <w n="2.2">vents</w> <w n="2.3">font</w> <w n="2.4">le</w> <w n="2.5">bruit</w> <w n="2.6">d</w>’<w n="2.7">un</w> <w n="2.8">taureau</w> <w n="2.9">qui</w> <w n="2.10">beugle</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Et</w>, <w n="3.2">sur</w> <w n="3.3">les</w> <w n="3.4">prés</w> <w n="3.5">nus</w>, <w n="3.6">le</w> <w n="3.7">ciel</w> <w n="3.8">sans</w> <w n="3.9">regard</w></l>
					<l n="4" num="1.4"><w n="4.1">S</w>’<w n="4.2">ouvre</w>, <w n="4.3">vide</w> <w n="4.4">et</w> <w n="4.5">blanc</w> <w n="4.6">comme</w> <w n="4.7">un</w> <w n="4.8">œil</w> <w n="4.9">d</w>’<w n="4.10">aveugle</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Ce</w> <w n="5.2">n</w>’<w n="5.3">est</w> <w n="5.4">pas</w> <w n="5.5">la</w> <w n="5.6">nuit</w>, <w n="5.7">ce</w> <w n="5.8">n</w>’<w n="5.9">est</w> <w n="5.10">pas</w> <w n="5.11">le</w> <w n="5.12">jour</w> ;</l>
					<l n="6" num="2.2"><w n="6.1">Du</w> <w n="6.2">zénith</w> <w n="6.3">glacé</w>, <w n="6.4">je</w> <w n="6.5">sens</w>, <w n="6.6">comme</w> <w n="6.7">un</w> <w n="6.8">givre</w>,</l>
					<l n="7" num="2.3"><w n="7.1">Tomber</w> <w n="7.2">sur</w> <w n="7.3">mon</w> <w n="7.4">cœur</w>, <w n="7.5">qui</w> <w n="7.6">n</w>’<w n="7.7">a</w> <w n="7.8">plus</w> <w n="7.9">d</w>’<w n="7.10">amour</w>,</l>
					<l n="8" num="2.4"><w n="8.1">Le</w> <w n="8.2">dégoût</w> <w n="8.3">d</w>’<w n="8.4">être</w> <w n="8.5">homme</w> <w n="8.6">et</w> <w n="8.7">l</w>’<w n="8.8">ennui</w> <w n="8.9">de</w> <w n="8.10">vivre</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Les</w> <w n="9.2">temps</w> <w n="9.3">sont</w> <w n="9.4">passés</w> <w n="9.5">où</w>, <w n="9.6">sous</w> <w n="9.7">le</w> <w n="9.8">ciel</w> <w n="9.9">bleu</w>,</l>
					<l n="10" num="3.2"><w n="10.1">Sonnait</w> <w n="10.2">dans</w> <w n="10.3">ma</w> <w n="10.4">chair</w> <w n="10.5">le</w> <w n="10.6">galop</w> <w n="10.7">des</w> <w n="10.8">fièvres</w> ;</l>
					<l n="11" num="3.3"><w n="11.1">Toute</w> <w n="11.2">joie</w> <w n="11.3">est</w> <w n="11.4">morte</w> <w n="11.5">ou</w> <w n="11.6">m</w>’<w n="11.7">a</w> <w n="11.8">dit</w> : <w n="11.9">adieu</w> !</l>
					<l n="12" num="3.4"><w n="12.1">J</w>’<w n="12.2">ai</w> <w n="12.3">le</w> <w n="12.4">doute</w> <w n="12.5">à</w> <w n="12.6">l</w>’<w n="12.7">âme</w> <w n="12.8">et</w> <w n="12.9">le</w> <w n="12.10">fiel</w> <w n="12.11">aux</w> <w n="12.12">lèvres</w>…</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Dormez</w> <w n="13.2">dans</w> <w n="13.3">la</w> <w n="13.4">nue</w>, <w n="13.5">ô</w> <w n="13.6">rayons</w> <w n="13.7">sacrés</w> !</l>
					<l n="14" num="4.2"><w n="14.1">Plus</w> <w n="14.2">de</w> <w n="14.3">souvenir</w> <w n="14.4">et</w> <w n="14.5">plus</w> <w n="14.6">d</w>’<w n="14.7">espérance</w> !</l>
					<l n="15" num="4.3"><w n="15.1">Mon</w> <w n="15.2">cœur</w>, <w n="15.3">loin</w> <w n="15.4">de</w> <w n="15.5">vous</w>, <w n="15.6">descend</w> <w n="15.7">par</w> <w n="15.8">degrés</w>,</l>
					<l n="16" num="4.4"><w n="16.1">Sous</w> <w n="16.2">l</w>’<w n="16.3">océan</w> <w n="16.4">froid</w> <w n="16.5">de</w> <w n="16.6">l</w>’<w n="16.7">indifférence</w> !…</l>
				</lg>
			</div></body></text></TEI>