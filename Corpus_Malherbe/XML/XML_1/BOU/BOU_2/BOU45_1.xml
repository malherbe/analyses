<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES. FESTONS ET ASTRAGALES</title>
				<title type="sub"></title>
				<title type="medium">Une édition électronique</title>
				<author key="BOU">
					<name>
						<forename>Louis</forename>
						<surname>BOUILHET</surname>
					</name>
					<date from="1822" to="1869">1822-1869</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2963 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BOU_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>POÉSIES. FESTONS ET ASTRAGALES</title>
						<author>Louis Bouilhet</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k9737201m</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES. FESTONS ET ASTRAGALES</title>
								<author>Louis Bouilhet</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE NOUVELLE</publisher>
									<date when="1859">1859</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1859">1859</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-09-20" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.4.xsd)</change>
				<change when="2021-09-20" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BOU45">
				<head type="main">LE GALET</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Rond</w>, <w n="1.2">luisant</w> <w n="1.3">et</w> <w n="1.4">poli</w> <w n="1.5">sous</w> <w n="1.6">la</w> <w n="1.7">vague</w> <w n="1.8">marine</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Océan</w>, <w n="2.2">je</w> <w n="2.3">l</w>’<w n="2.4">ai</w> <w n="2.5">pris</w> <w n="2.6">parmi</w> <w n="2.7">tes</w> <w n="2.8">flots</w> <w n="2.9">amers</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Ce</w> <w n="3.2">caillou</w> <w n="3.3">blanc</w> <w n="3.4">avec</w> <w n="3.5">sa</w> <w n="3.6">frange</w> <w n="3.7">purpurine</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Comme</w> <w n="4.2">un</w> <w n="4.3">bijou</w> <w n="4.4">tombé</w> <w n="4.5">du</w> <w n="4.6">vaste</w> <w n="4.7">écrin</w> <w n="4.8">des</w> <w n="4.9">mers</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Mille</w> <w n="5.2">ans</w>, <w n="5.3">il</w> <w n="5.4">a</w> <w n="5.5">roulé</w> <w n="5.6">sur</w> <w n="5.7">le</w> <w n="5.8">bord</w> <w n="5.9">de</w> <w n="5.10">cette</w> <w n="5.11">onde</w>,</l>
					<l n="6" num="2.2"><w n="6.1">Les</w> <w n="6.2">flots</w> <w n="6.3">jaloux</w>, <w n="6.4">mille</w> <w n="6.5">ans</w>, <w n="6.6">l</w>’<w n="6.7">ont</w> <w n="6.8">ramené</w> <w n="6.9">vers</w> <w n="6.10">toi</w> ;</l>
					<l n="7" num="2.3"><w n="7.1">Et</w> <w n="7.2">peut</w>-<w n="7.3">être</w>, <w n="7.4">Océan</w>, <w n="7.5">sous</w> <w n="7.6">ta</w> <w n="7.7">houle</w> <w n="7.8">profonde</w>,</l>
					<l n="8" num="2.4"><w n="8.1">Tune</w> <w n="8.2">l</w>’<w n="8.3">avais</w> <w n="8.4">poli</w> <w n="8.5">que</w> <w n="8.6">pour</w> <w n="8.7">qu</w>’<w n="8.8">il</w> <w n="8.9">vînt</w> <w n="8.10">à</w> <w n="8.11">moi</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Je</w> <w n="9.2">l</w>’<w n="9.3">ai</w> <w n="9.4">pris</w>, <w n="9.5">ruisselant</w>. <w n="9.6">d</w>’<w n="9.7">une</w> <w n="9.8">écume</w> <w n="9.9">embaumée</w></l>
					<l n="10" num="3.2">(<w n="10.1">Tel</w> <w n="10.2">un</w> <w n="10.3">avare</w> <w n="10.4">prend</w> <w n="10.5">un</w> <w n="10.6">trésor</w>), <w n="10.7">et</w> <w n="10.8">joyeux</w>,</l>
					<l n="11" num="3.3"><w n="11.1">O</w> <w n="11.2">mer</w>, <w n="11.3">je</w> <w n="11.4">l</w>’<w n="11.5">emportai</w> <w n="11.6">loin</w> <w n="11.7">de</w> <w n="11.8">ta</w> <w n="11.9">rive</w> <w n="11.10">aimée</w>,</l>
					<l n="12" num="3.4"><w n="12.1">Comme</w> <w n="12.2">un</w> <w n="12.3">gage</w> <w n="12.4">d</w>’<w n="12.5">ami</w> <w n="12.6">qui</w> <w n="12.7">nous</w> <w n="12.8">fait</w> <w n="12.9">ses</w> <w n="12.10">adieux</w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Et</w> <w n="13.2">depuis</w>, <w n="13.3">quand</w> <w n="13.4">parfois</w> <w n="13.5">je</w> <w n="13.6">le</w> <w n="13.7">contemple</w> <w n="13.8">encore</w>,</l>
					<l n="14" num="4.2"><w n="14.1">Frémissant</w>, <w n="14.2">éperdu</w>, <w n="14.3">je</w> <w n="14.4">crois</w> <w n="14.5">tenir</w> <w n="14.6">soudain</w></l>
					<l n="15" num="4.3"><w n="15.1">Avec</w> <w n="15.2">ses</w> <w n="15.3">bruits</w>, <w n="15.4">ses</w> <w n="15.5">flots</w> <w n="15.6">et</w> <w n="15.7">sa</w> <w n="15.8">trompe</w> <w n="15.9">sonore</w>,</l>
					<l n="16" num="4.4"><w n="16.1">Tout</w> <w n="16.2">le</w> <w n="16.3">grand</w> <w n="16.4">Océan</w>,<w n="16.5">dans</w> <w n="16.6">le</w> <w n="16.7">fond</w> <w n="16.8">de</w> <w n="16.9">ma</w> <w n="16.10">main</w> !</l>
				</lg>
			</div></body></text></TEI>