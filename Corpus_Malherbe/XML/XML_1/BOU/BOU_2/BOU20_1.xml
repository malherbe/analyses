<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES. FESTONS ET ASTRAGALES</title>
				<title type="sub"></title>
				<title type="medium">Une édition électronique</title>
				<author key="BOU">
					<name>
						<forename>Louis</forename>
						<surname>BOUILHET</surname>
					</name>
					<date from="1822" to="1869">1822-1869</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2963 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BOU_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>POÉSIES. FESTONS ET ASTRAGALES</title>
						<author>Louis Bouilhet</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k9737201m</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES. FESTONS ET ASTRAGALES</title>
								<author>Louis Bouilhet</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE NOUVELLE</publisher>
									<date when="1859">1859</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1859">1859</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-09-20" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.4.xsd)</change>
				<change when="2021-09-20" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BOU20">
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Quand</w> <w n="1.2">vous</w> <w n="1.3">m</w>’<w n="1.4">avez</w> <w n="1.5">quitté</w>, <w n="1.6">boudeuse</w> <w n="1.7">et</w> <w n="1.8">mutinée</w></l>
					<l n="2" num="1.2"><w n="2.1">Secouant</w> <w n="2.2">mes</w> <w n="2.3">baisers</w>, <w n="2.4">comme</w> <w n="2.5">un</w> <w n="2.6">arbre</w> <w n="2.7">ses</w> <w n="2.8">fleurs</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Je</w> <w n="3.2">restai</w> <w n="3.3">seul</w>, <w n="3.4">debout</w>, <w n="3.5">près</w> <w n="3.6">de</w> <w n="3.7">la</w> <w n="3.8">cheminée</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Me</w> <w n="4.2">forçant</w> <w n="4.3">au</w> <w n="4.4">sourire</w>, <w n="4.5">et</w> <w n="4.6">me</w> <w n="4.7">sentant</w> <w n="4.8">des</w> <w n="4.9">pleurs</w> ;</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">C</w>’<w n="5.2">était</w> <w n="5.3">le</w> <w n="5.4">premier</w> <w n="5.5">doute</w> <w n="5.6">et</w> <w n="5.7">le</w> <w n="5.8">premier</w> <w n="5.9">nuage</w></l>
					<l n="6" num="2.2"><w n="6.1">Dans</w> <w n="6.2">ce</w> <w n="6.3">beau</w> <w n="6.4">ciel</w> <w n="6.5">d</w>’<w n="6.6">amour</w> <w n="6.7">qu</w>’<w n="6.8">un</w> <w n="6.9">souffle</w> <w n="6.10">peut</w> <w n="6.11">ternir</w>,</l>
					<l n="7" num="2.3"><w n="7.1">Et</w> <w n="7.2">me</w> <w n="7.3">croyant</w> <w n="7.4">bien</w> <w n="7.5">fort</w>, <w n="7.6">et</w> <w n="7.7">me</w> <w n="7.8">posant</w> <w n="7.9">en</w> <w n="7.10">sage</w>,</l>
					<l n="8" num="2.4"><w n="8.1">J</w>’<w n="8.2">avais</w> <w n="8.3">raillé</w> <w n="8.4">vos</w> <w n="8.5">saints</w> <w n="8.6">que</w> <w n="8.7">j</w>’<w n="8.8">aurais</w> <w n="8.9">dû</w> <w n="8.10">bénir</w> ;</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">A</w> <w n="9.2">vos</w> <w n="9.3">preuves</w> <w n="9.4">de</w> <w n="9.5">Dieu</w>, <w n="9.6">mon</w> <w n="9.7">oreille</w> <w n="9.8">était</w> <w n="9.9">sourde</w>,</l>
					<l n="10" num="3.2"><w n="10.1">Je</w> <w n="10.2">heurtais</w> <w n="10.3">votre</w> <w n="10.4">foi</w> <w n="10.5">d</w>’<w n="10.6">un</w> <w n="10.7">sarcasme</w> <w n="10.8">moqueur</w>…</l>
					<l n="11" num="3.3"><w n="11.1">L</w> <w n="11.2">homme</w> <w n="11.3">est</w> <w n="11.4">lâche</w> <w n="11.5">et</w> <w n="11.6">brutal</w>, <w n="11.7">l</w>’<w n="11.8">homme</w> <w n="11.9">a</w> <w n="11.10">la</w> <w n="11.11">main</w> <w n="11.12">trop</w> <w n="11.13">lourde</w></l>
					<l n="12" num="3.4"><w n="12.1">Pour</w> <w n="12.2">toucher</w> <w n="12.3">à</w> <w n="12.4">votre</w> <w n="12.5">aile</w>, <w n="12.6">ô</w> <w n="12.7">croyances</w> <w n="12.8">du</w> <w n="12.9">cœur</w> !</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Pardon</w>, <w n="13.2">j</w>’<w n="13.3">en</w> <w n="13.4">suis</w> <w n="13.5">puni</w> <w n="13.6">plus</w> <w n="13.7">qu</w>’<w n="13.8">on</w> <w n="13.9">ne</w> <w n="13.10">saurait</w> <w n="13.11">dire</w> !</l>
					<l n="14" num="4.2"><w n="14.1">J</w>’<w n="14.2">ai</w> <w n="14.3">vu</w> <w n="14.4">jaillir</w> <w n="14.5">l</w>’<w n="14.6">éclair</w> <w n="14.7">de</w> <w n="14.8">vos</w> <w n="14.9">grands</w> <w n="14.10">yeux</w> <w n="14.11">si</w> <w n="14.12">doux</w> ;</l>
					<l n="15" num="4.3"><w n="15.1">Pour</w> <w n="15.2">garder</w> <w n="15.3">ma</w> <w n="15.4">raison</w>, <w n="15.5">j</w>’<w n="15.6">ai</w> <w n="15.7">perdu</w> <w n="15.8">maint</w> <w n="15.9">sourire</w>,</l>
					<l n="16" num="4.4"><w n="16.1">Ah</w> ! <w n="16.2">montrez</w>-<w n="16.3">moi</w> <w n="16.4">l</w>’<w n="16.5">autel</w>, <w n="16.6">que</w> <w n="16.7">j</w>’<w n="16.8">y</w> <w n="16.9">tombe</w> <w n="16.10">à</w> <w n="16.11">genoux</w> !</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">Votre</w> <w n="17.2">loi</w> ? <w n="17.3">j</w>’<w n="17.4">y</w> <w n="17.5">consens</w> ! <w n="17.6">Votre</w> <w n="17.7">Dieu</w> ? <w n="17.8">je</w> <w n="17.9">l</w>’<w n="17.10">adore</w> !</l>
					<l n="18" num="5.2"><w n="18.1">A</w> <w n="18.2">vos</w> <w n="18.3">saints</w> <w n="18.4">préférés</w> <w n="18.5">j</w>’<w n="18.6">offre</w> <w n="18.7">mes</w> <w n="18.8">encensoirs</w>,</l>
					<l n="19" num="5.3"><w n="19.1">Même</w> <w n="19.2">on</w> <w n="19.3">vous</w> <w n="19.4">passera</w>, <w n="19.5">pour</w> <w n="19.6">deux</w> <w n="19.7">baisers</w> <w n="19.8">encore</w>,</l>
					<l n="20" num="5.4"><w n="20.1">Vos</w> <w n="20.2">dominicains</w> <w n="20.3">blancs</w> <w n="20.4">et</w> <w n="20.5">vos</w> <w n="20.6">jésuites</w> <w n="20.7">noirs</w> !</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">Dans</w> <w n="21.2">votre</w> <w n="21.3">amour</w> <w n="21.4">profond</w>, <w n="21.5">je</w> <w n="21.6">vais</w> <w n="21.7">creuser</w> <w n="21.8">ma</w> <w n="21.9">grotte</w>,</l>
					<l n="22" num="6.2"><w n="22.1">Et</w>, <w n="22.2">loin</w> <w n="22.3">dès</w> <w n="22.4">bruits</w> <w n="22.5">du</w> <w n="22.6">monde</w>, <w n="22.7">entre</w> <w n="22.8">vos</w> <w n="22.9">bras</w> <w n="22.10">de</w> <w n="22.11">lait</w>,</l>
					<l n="23" num="6.3"><w n="23.1">L</w>’<w n="23.2">ermite</w>, <w n="23.3">chaque</w> <w n="23.4">jour</w>, <w n="23.5">de</w> <w n="23.6">sa</w> <w n="23.7">lèvre</w> <w n="23.8">dévote</w>,</l>
					<l n="24" num="6.4"><w n="24.1">Sur</w> <w n="24.2">l</w>’<w n="24.3">émail</w> <w n="24.4">de</w> <w n="24.5">vos</w> <w n="24.6">dents</w> <w n="24.7">dira</w> <w n="24.8">son</w> <w n="24.9">chapelet</w> !</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">J</w>’<w n="25.2">irais</w>, <w n="25.3">prêtre</w> <w n="25.4">docile</w> <w n="25.5">à</w> <w n="25.6">toute</w> <w n="25.7">fantaisie</w>,</l>
					<l n="26" num="7.2"><w n="26.1">Avec</w> <w n="26.2">le</w> <w n="26.3">gui</w> <w n="26.4">du</w> <w n="26.5">chêne</w> <w n="26.6">ou</w> <w n="26.7">la</w> <w n="26.8">tiare</w> <w n="26.9">d</w>’<w n="26.10">or</w>,</l>
					<l n="27" num="7.3"><w n="27.1">Du</w> <w n="27.2">Teutatès</w> <w n="27.3">de</w> <w n="27.4">Gaule</w> <w n="27.5">au</w> <w n="27.6">Bhagavat</w> <w n="27.7">d</w>’<w n="27.8">Asie</w>,</l>
					<l n="28" num="7.4"><w n="28.1">Des</w> <w n="28.2">cabires</w> <w n="28.3">persans</w> <w n="28.4">aux</w> <w n="28.5">dieux</w> <w n="28.6">glacés</w> <w n="28.7">du</w> <w n="28.8">nord</w> !</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1">Que</w> <w n="29.2">s</w>’<w n="29.3">il</w> <w n="29.4">vous</w> <w n="29.5">fait</w> <w n="29.6">plaisir</w> <w n="29.7">d</w>’<w n="29.8">être</w> <w n="29.9">mahométane</w>,</l>
					<l n="30" num="8.2"><w n="30.1">Allah</w> !… <w n="30.2">de</w> <w n="30.3">Mahomet</w> <w n="30.4">j</w>’<w n="30.5">espère</w> <w n="30.6">les</w> <w n="30.7">sept</w> <w n="30.8">deux</w> !</l>
					<l n="31" num="8.3"><w n="31.1">Si</w> <w n="31.2">vous</w> <w n="31.3">aimez</w> <w n="31.4">Brahma</w>, <w n="31.5">je</w> <w n="31.6">serai</w> <w n="31.7">le</w> <w n="31.8">brahmane</w> !</l>
					<l n="32" num="8.4"><w n="32.1">Mon</w> <w n="32.2">culte</w> <w n="32.3">est</w> <w n="32.4">ta</w> <w n="32.5">croyance</w>, <w n="32.6">et</w> <w n="32.7">mes</w> <w n="32.8">dieux</w> <w n="32.9">sont</w> <w n="32.10">tes</w> <w n="32.11">dieux</w> !</l>
				</lg>
			</div></body></text></TEI>