<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES. FESTONS ET ASTRAGALES</title>
				<title type="sub"></title>
				<title type="medium">Une édition électronique</title>
				<author key="BOU">
					<name>
						<forename>Louis</forename>
						<surname>BOUILHET</surname>
					</name>
					<date from="1822" to="1869">1822-1869</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2963 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BOU_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>POÉSIES. FESTONS ET ASTRAGALES</title>
						<author>Louis Bouilhet</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k9737201m</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES. FESTONS ET ASTRAGALES</title>
								<author>Louis Bouilhet</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE NOUVELLE</publisher>
									<date when="1859">1859</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1859">1859</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-09-20" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.4.xsd)</change>
				<change when="2021-09-20" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BOU51">
				<head type="main">LES LARMES DE LA VIGNE</head>
				<div n="1" type="section">
					<head type="number">I</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Mars</w> <w n="1.2">est</w> <w n="1.3">venu</w>, <w n="1.4">la</w> <w n="1.5">vigne</w> <w n="1.6">pleure</w> :</l>
						<l n="2" num="1.2"><w n="2.1">Le</w> <w n="2.2">vent</w> <w n="2.3">du</w> <w n="2.4">nord</w>, <w n="2.5">passant</w> <w n="2.6">brutal</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Fait</w>, <w n="3.2">sur</w> <w n="3.3">les</w> <w n="3.4">branches</w> <w n="3.5">qu</w>’<w n="3.6">il</w> <w n="3.7">effleure</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Rouler</w> <w n="4.2">des</w> <w n="4.3">perles</w> <w n="4.4">de</w> <w n="4.5">cristal</w> ;</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Et</w>, <w n="5.2">peu</w> <w n="5.3">sensible</w> <w n="5.4">à</w> <w n="5.5">tes</w> <w n="5.6">alarmes</w>,</l>
						<l n="6" num="2.2"><w n="6.1">Au</w> <w n="6.2">flanc</w> <w n="6.3">des</w> <w n="6.4">côtes</w> <w n="6.5">sans</w> <w n="6.6">chemins</w>,</l>
						<l n="7" num="2.3"><w n="7.1">La</w> <w n="7.2">terre</w> <w n="7.3">boit</w> <w n="7.4">tes</w> <w n="7.5">grandes</w> <w n="7.6">larmes</w>,</l>
						<l n="8" num="2.4"><w n="8.1">Consolatrice</w> <w n="8.2">des</w> <w n="8.3">humains</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Oh</w> ! <w n="9.2">dis</w>-<w n="9.3">nous</w>, <w n="9.4">se</w> <w n="9.5">peut</w>-<w n="9.6">il</w> <w n="9.7">qu</w>’<w n="9.8">on</w> <w n="9.9">voie</w>,</l>
						<l n="10" num="3.2"><w n="10.1">Pour</w> <w n="10.2">calmer</w> <w n="10.3">nos</w> <w n="10.4">âpres</w> <w n="10.5">douleurs</w>,</l>
						<l n="11" num="3.3"><w n="11.1">Sortir</w> <w n="11.2">un</w> <w n="11.3">jour</w> <w n="11.4">des</w> <w n="11.5">flots</w> <w n="11.6">de</w> <w n="11.7">joie</w></l>
						<l n="12" num="3.4"><w n="12.1">De</w> <w n="12.2">tes</w> <w n="12.3">rameaux</w> <w n="12.4">gonflés</w> <w n="12.5">de</w>. <w n="12.6">pleurs</w> ?</l>
					</lg>
				</div>
				<div n="2" type="section">
					<head type="number">II</head>
					<lg n="1">
						<l n="13" num="1.1"><w n="13.1">Toute</w> <w n="13.2">joie</w> <w n="13.3">a</w> <w n="13.4">sa</w> <w n="13.5">source</w> <w n="13.6">amère</w> :</l>
						<l n="14" num="1.2"><w n="14.1">Poëte</w>, <w n="14.2">ne</w> <w n="14.3">t</w>’<w n="14.4">étonne</w> <w n="14.5">pas</w></l>
						<l n="15" num="1.3"><w n="15.1">Si</w> <w n="15.2">je</w> <w n="15.3">suis</w> <w n="15.4">triste</w>, <w n="15.5">moi</w>, <w n="15.6">la</w> <w n="15.7">mère</w></l>
						<l n="16" num="1.4"><w n="16.1">De</w> <w n="16.2">l</w>’<w n="16.3">ivresse</w> <w n="16.4">et</w> <w n="16.5">des</w> <w n="16.6">gais</w> <w n="16.7">repas</w>.</l>
					</lg>
					<lg n="2">
						<l n="17" num="2.1"><w n="17.1">Le</w> <w n="17.2">ciel</w>, <w n="17.3">jaloux</w> <w n="17.4">du</w> <w n="17.5">vin</w> <w n="17.6">qui</w> <w n="17.7">charme</w>,</l>
						<l n="18" num="2.2"><w n="18.1">A</w> <w n="18.2">taxé</w> <w n="18.3">mon</w> <w n="18.4">philtre</w> <w n="18.5">puissant</w>,</l>
						<l n="19" num="2.3"><w n="19.1">Et</w> <w n="19.2">je</w> <w n="19.3">paie</w> <w n="19.4">aux</w> <w n="19.5">dieux</w> <w n="19.6">une</w> <w n="19.7">larme</w></l>
						<l n="20" num="2.4"><w n="20.1">Pour</w> <w n="20.2">chaque</w> <w n="20.3">goutte</w> <w n="20.4">de</w> <w n="20.5">mon</w> <w n="20.6">sang</w>.</l>
					</lg>
					<lg n="3">
						<l n="21" num="3.1"><w n="21.1">Toi</w>-<w n="21.2">même</w>, <w n="21.3">à</w> <w n="21.4">l</w>’<w n="21.5">heure</w> <w n="21.6">du</w> <w n="21.7">délire</w>,</l>
						<l n="22" num="3.2"><w n="22.1">N</w>’<w n="22.2">entends</w>-<w n="22.3">tu</w> <w n="22.4">pas</w> <w n="22.5">avec</w> <w n="22.6">effroi</w></l>
						<l n="23" num="3.3"><w n="23.1">Monter</w>, <w n="23.2">aux</w> <w n="23.3">strettes</w> <w n="23.4">de</w> <w n="23.5">ta</w> <w n="23.6">lyre</w>,</l>
						<l n="24" num="3.4"><w n="24.1">Tous</w> <w n="24.2">les</w> <w n="24.3">sanglots</w> <w n="24.4">qui</w> <w n="24.5">sont</w> <w n="24.6">en</w> <w n="24.7">toi</w> ?</l>
					</lg>
				</div>
			</div></body></text></TEI>