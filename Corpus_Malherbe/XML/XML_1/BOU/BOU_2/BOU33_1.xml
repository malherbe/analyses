<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES. FESTONS ET ASTRAGALES</title>
				<title type="sub"></title>
				<title type="medium">Une édition électronique</title>
				<author key="BOU">
					<name>
						<forename>Louis</forename>
						<surname>BOUILHET</surname>
					</name>
					<date from="1822" to="1869">1822-1869</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2963 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BOU_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>POÉSIES. FESTONS ET ASTRAGALES</title>
						<author>Louis Bouilhet</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k9737201m</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES. FESTONS ET ASTRAGALES</title>
								<author>Louis Bouilhet</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE NOUVELLE</publisher>
									<date when="1859">1859</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1859">1859</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-09-20" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.4.xsd)</change>
				<change when="2021-09-20" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BOU33">
				<head type="main">LE DANSEUR BATHYLLE</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">La</w> <w n="1.2">belle</w> <w n="1.3">Métella</w>, <w n="1.4">femme</w> <w n="1.5">du</w> <w n="1.6">vieux</w> <w n="1.7">préteur</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Est</w> <w n="2.2">pâle</w> <w n="2.3">maintenant</w>, <w n="2.4">et</w>. <w n="2.5">porte</w> <w n="2.6">dans</w> <w n="2.7">son</w> <w n="2.8">cœur</w></l>
					<l n="3" num="1.3"><space unit="char" quantity="8"></space><w n="3.1">Un</w> <w n="3.2">mal</w> <w n="3.3">secret</w> <w n="3.4">qui</w> <w n="3.5">la</w> <w n="3.6">déchire</w> ;</l>
					<l n="4" num="1.4"><w n="4.1">Par</w> <w n="4.2">le</w> <w n="4.3">bois</w> <w n="4.4">d</w>’<w n="4.5">orangers</w> <w n="4.6">qui</w> <w n="4.7">borde</w> <w n="4.8">sa</w> <w n="4.9">villa</w>,</l>
					<l n="5" num="1.5"><w n="5.1">Elle</w> <w n="5.2">marche</w> <w n="5.3">au</w> <w n="5.4">hasard</w>, <w n="5.5">la</w> <w n="5.6">belle</w> <w n="5.7">Métella</w>,</l>
					<l n="6" num="1.6"><space unit="char" quantity="8"></space><w n="6.1">Comme</w> <w n="6.2">une</w> <w n="6.3">bacchante</w> <w n="6.4">en</w> <w n="6.5">délire</w>.</l>
				</lg>
				<lg n="2">
					<l n="7" num="2.1"><w n="7.1">Pour</w> <w n="7.2">sonder</w> <w n="7.3">jusqu</w>’<w n="7.4">au</w> <w n="7.5">fond</w> <w n="7.6">l</w>’<w n="7.7">avenir</w> <w n="7.8">incertain</w>,</l>
					<l n="8" num="2.2"><w n="8.1">Vingt</w> <w n="8.2">fois</w> <w n="8.3">l</w>’<w n="8.4">urne</w> <w n="8.5">d</w>’<w n="8.6">albâtre</w> <w n="8.7">où</w> <w n="8.8">roule</w> <w n="8.9">le</w> <w n="8.10">destin</w></l>
					<l n="9" num="2.3"><space unit="char" quantity="8"></space><w n="9.1">Sous</w> <w n="9.2">ses</w> <w n="9.3">doigts</w> <w n="9.4">tremblants</w> <w n="9.5">s</w>’<w n="9.6">est</w> <w n="9.7">vidée</w> ;</l>
					<l n="10" num="2.4"><w n="10.1">Et</w> <w n="10.2">vingt</w> <w n="10.3">fois</w> <w n="10.4">Métella</w>, <w n="10.5">chez</w> <w n="10.6">les</w> <w n="10.7">magiciens</w>,</l>
					<l n="11" num="2.5"><w n="11.1">A</w> <w n="11.2">mêlé</w>, <w n="11.3">dans</w> <w n="11.4">la</w> <w n="11.5">nuit</w>, <w n="11.6">les</w> <w n="11.7">sorts</w> <w n="11.8">campaniens</w></l>
					<l n="12" num="2.6"><space unit="char" quantity="8"></space><w n="12.1">Aux</w> <w n="12.2">enchantements</w> <w n="12.3">de</w> <w n="12.4">Chaldée</w> !</l>
				</lg>
				<lg n="3">
					<l n="13" num="3.1"><w n="13.1">Elle</w> <w n="13.2">aime</w>, <w n="13.3">et</w> <w n="13.4">ce</w> <w n="13.5">n</w>’<w n="13.6">est</w> <w n="13.7">pas</w> <w n="13.8">le</w> <w n="13.9">chevalier</w> <w n="13.10">romain</w>,</l>
					<l n="14" num="3.2"><w n="14.1">Bien</w> <w n="14.2">qu</w>’<w n="14.3">il</w> <w n="14.4">soit</w> <w n="14.5">jeune</w> <w n="14.6">et</w> <w n="14.7">fier</w>, <w n="14.8">et</w> <w n="14.9">qu</w>’<w n="14.10">il</w> <w n="14.11">presse</w>,’<w n="14.12">en</w> <w n="14.13">chemin</w>,</l>
					<l n="15" num="3.3"><space unit="char" quantity="8"></space><w n="15.1">Une</w> <w n="15.2">cavale</w> <w n="15.3">au</w> <w n="15.4">frein</w> <w n="15.5">sonore</w>,</l>
					<l n="16" num="3.4"><w n="16.1">Qu</w>’<w n="16.2">il</w> <w n="16.3">ait</w> <w n="16.4">sa</w> <w n="16.5">place</w> <w n="16.6">au</w> <w n="16.7">cirque</w>, <w n="16.8">auprès</w> <w n="16.9">des</w> <w n="16.10">sénateurs</w>,</l>
					<l n="17" num="3.5"><w n="17.1">Que</w> <w n="17.2">sa</w> <w n="17.3">bague</w> <w n="17.4">étincelle</w>, <w n="17.5">et</w> <w n="17.6">qu</w>’<w n="17.7">au</w> <w n="17.8">jour</w> <w n="17.9">des</w> <w n="17.10">honneurs</w>,</l>
					<l n="18" num="3.6"><space unit="char" quantity="8"></space><w n="18.1">D</w>’<w n="18.2">olivier</w> <w n="18.3">son</w> <w n="18.4">front</w> <w n="18.5">se</w> <w n="18.6">décore</w> !</l>
				</lg>
				<lg n="4">
					<l n="19" num="4.1"><w n="19.1">Ce</w> <w n="19.2">n</w>’<w n="19.3">est</w> <w n="19.4">pas</w> <w n="19.5">le</w> <w n="19.6">consul</w>, <w n="19.7">au</w> <w n="19.8">long</w> <w n="19.9">manteau</w> <w n="19.10">rayé</w></l>
					<l n="20" num="4.2"><w n="20.1">Si</w> <w n="20.2">beau</w> <w n="20.3">qu</w>’<w n="20.4">à</w> <w n="20.5">son</w> <w n="20.6">aspect</w>, <w n="20.7">du</w> <w n="20.8">peuple</w> <w n="20.9">émerveillé</w></l>
					<l n="21" num="4.3"><space unit="char" quantity="8"></space><w n="21.1">Tombe</w> <w n="21.2">le</w> <w n="21.3">murmure</w> <w n="21.4">frivole</w>,</l>
					<l n="22" num="4.4"><w n="22.1">Alors</w> <w n="22.2">que</w> <w n="22.3">précédé</w> <w n="22.4">du</w> <w n="22.5">licteur</w> <w n="22.6">éclatant</w>,</l>
					<l n="23" num="4.5"><w n="23.1">Avec</w> <w n="23.2">sa</w> <w n="23.3">robe</w> <w n="23.4">blanche</w>, <w n="23.5">il</w> <w n="23.6">balaye</w>, <w n="23.7">en</w> <w n="23.8">montant</w>,</l>
					<l n="24" num="4.6"><space unit="char" quantity="8"></space><w n="24.1">Les</w> <w n="24.2">blancs</w> <w n="24.3">degrés</w> <w n="24.4">du</w> <w n="24.5">capitole</w> !</l>
				</lg>
				<lg n="5">
					<l n="25" num="5.1"><w n="25.1">Ce</w> <w n="25.2">n</w>’<w n="25.3">est</w> <w n="25.4">pas</w> <w n="25.5">le</w> <w n="25.6">tribun</w>, <w n="25.7">l</w>’<w n="25.8">homme</w> <w n="25.9">au</w> <w n="25.10">pouvoir</w> <w n="25.11">hautain</w>,</l>
					<l n="26" num="5.2"><w n="26.1">Qui</w> <w n="26.2">d</w>’<w n="26.3">un</w> <w n="26.4">mot</w> <w n="26.5">de</w> <w n="26.6">sa</w> <w n="26.7">bouche</w> <w n="26.8">arrête</w> <w n="26.9">le</w> <w n="26.10">destin</w>,</l>
					<l n="27" num="5.3"><space unit="char" quantity="8"></space><w n="27.1">Ni</w> <w n="27.2">l</w>’<w n="27.3">édile</w> <w n="27.4">aux</w> <w n="27.5">dons</w> <w n="27.6">magnifiques</w>,</l>
					<l n="28" num="5.4"><w n="28.1">Ni</w> <w n="28.2">le</w> <w n="28.3">riche</w> <w n="28.4">patron</w>, <w n="28.5">de</w> <w n="28.6">qui</w> <w n="28.7">mille</w> <w n="28.8">clients</w></l>
					<l n="29" num="5.5"><w n="29.1">Autour</w> <w n="29.2">de</w> <w n="29.3">la</w> <w n="29.4">sportule</w> <w n="29.5">humbles</w> <w n="29.6">et</w> <w n="29.7">suppliants</w></l>
					<l n="30" num="5.6"><space unit="char" quantity="8"></space><w n="30.1">Sans</w> <w n="30.2">cesse</w> <w n="30.3">assiègent</w> <w n="30.4">les</w> <w n="30.5">portiques</w>.</l>
				</lg>
				<lg n="6">
					<l n="31" num="6.1"><w n="31.1">Si</w> <w n="31.2">Métella</w> <w n="31.3">soupire</w> <w n="31.4">et</w> <w n="31.5">n</w>’<w n="31.6">a</w> <w n="31.7">plus</w> <w n="31.8">de</w> <w n="31.9">sommeil</w>,</l>
					<l n="32" num="6.2"><w n="32.1">Ce</w> <w n="32.2">n</w>’<w n="32.3">est</w> <w n="32.4">point</w> <w n="32.5">le</w> <w n="32.6">soldat</w> <w n="32.7">bruni</w> <w n="32.8">par</w> <w n="32.9">le</w> <w n="32.10">soleil</w></l>
					<l n="33" num="6.3"><space unit="char" quantity="8"></space><w n="33.1">Qui</w> <w n="33.2">trouble</w> <w n="33.3">sa</w> <w n="33.4">nuit</w> <w n="33.5">inquiète</w>,</l>
					<l n="34" num="6.4"><w n="34.1">Ni</w> <w n="34.2">le</w> <w n="34.3">poëte</w> <w n="34.4">grec</w> <w n="34.5">aux</w> <w n="34.6">vers</w> <w n="34.7">ingénieux</w>,</l>
					<l n="35" num="6.5"><w n="35.1">Ni</w> <w n="35.2">l</w>’<w n="35.3">esclave</w> <w n="35.4">gaulois</w>, <w n="35.5">prince</w> <w n="35.6">par</w> <w n="35.7">ses</w> <w n="35.8">aïeux</w>,</l>
					<l n="36" num="6.6"><space unit="char" quantity="8"></space><w n="36.1">Qui</w> <w n="36.2">porte</w> <w n="36.3">une</w> <w n="36.4">urne</w> <w n="36.5">sur</w> <w n="36.6">sa</w> <w n="36.7">tête</w>.</l>
				</lg>
				<lg n="7">
					<l n="37" num="7.1"><w n="37.1">L</w>’<w n="37.2">image</w> <w n="37.3">qui</w> <w n="37.4">bondit</w> <w n="37.5">sous</w> <w n="37.6">ses</w> <w n="37.7">yeux</w> <w n="37.8">enflammés</w>,</l>
					<l n="38" num="7.2"><w n="38.1">C</w>’<w n="38.2">est</w> <w n="38.3">le</w> <w n="38.4">danseur</w> <w n="38.5">Bathylle</w>, <w n="38.6">aux</w> <w n="38.7">cheveux</w> <w n="38.8">parfumés</w>,</l>
					<l n="39" num="7.3"><space unit="char" quantity="8"></space><w n="39.1">Bathylle</w> <w n="39.2">aux</w> <w n="39.3">poses</w> <w n="39.4">languissantes</w>,</l>
					<l n="40" num="7.4"><w n="40.1">Bathylle</w> <w n="40.2">qui</w> <w n="40.3">s</w>’<w n="40.4">envole</w>, <w n="40.5">et</w> <w n="40.6">qui</w> <w n="40.7">glisse</w>, <w n="40.8">et</w> <w n="40.9">qui</w> <w n="40.10">fuit</w>,</l>
					<l n="41" num="7.5"><w n="41.1">Et</w> <w n="41.2">fait</w> <w n="41.3">battre</w> <w n="41.4">le</w> <w n="41.5">cœur</w> <w n="41.6">des</w> <w n="41.7">matrones</w>, <w n="41.8">au</w> <w n="41.9">bruit</w></l>
					<l n="42" num="7.6"><space unit="char" quantity="8"></space><w n="42.1">De</w> <w n="42.2">ses</w> <w n="42.3">cymbales</w> <w n="42.4">frémissantes</w>.</l>
				</lg>
				<lg n="8">
					<l n="43" num="8.1"><w n="43.1">Bathylle</w> <w n="43.2">qu</w>’<w n="43.3">aux</w> <w n="43.4">Romains</w> <w n="43.5">la</w> <w n="43.6">Grèce</w>, <w n="43.7">un</w> <w n="43.8">jour</w>, <w n="43.9">céda</w>,</l>
					<l n="44" num="8.2"><w n="44.1">Si</w> <w n="44.2">gracieux</w>, <w n="44.3">alors</w> <w n="44.4">qu</w>’<w n="44.5">il</w> <w n="44.6">danse</w> <w n="44.7">la</w> <w n="44.8">Léda</w>,</l>
					<l n="45" num="8.3"><space unit="char" quantity="8"></space><w n="45.1">Sous</w> <w n="45.2">une</w> <w n="45.3">tunique</w> <w n="45.4">de</w> <w n="45.5">femme</w> !</l>
					<l n="46" num="8.4"><w n="46.1">Ou</w> <w n="46.2">quand</w> <w n="46.3">son</w> <w n="46.4">corps</w> <w n="46.5">mobile</w>, <w n="46.6">en</w> <w n="46.7">cercle</w> <w n="46.8">se</w> <w n="46.9">tordant</w>,</l>
					<l n="47" num="8.5"><w n="47.1">Tourne</w> <w n="47.2">comme</w> <w n="47.3">une</w> <w n="47.4">roue</w>, <w n="47.5">et</w> <w n="47.6">dans</w> <w n="47.7">son</w> <w n="47.8">vol</w> <w n="47.9">ardent</w></l>
					<l n="48" num="8.6"><space unit="char" quantity="8"></space><w n="48.1">De</w> <w n="48.2">tout</w> <w n="48.3">un</w> <w n="48.4">peuple</w> <w n="48.5">emporte</w> <w n="48.6">l</w>’<w n="48.7">âme</w> !</l>
				</lg>
				<lg n="9">
					<l n="49" num="9.1"><w n="49.1">Mais</w> <w n="49.2">Bathylle</w> <w n="49.3">est</w> <w n="49.4">cruel</w>, <w n="49.5">et</w> <w n="49.6">ne</w> <w n="49.7">se</w> <w n="49.8">donne</w> <w n="49.9">pas</w>,</l>
					<l n="50" num="9.2"><w n="50.1">Il</w> <w n="50.2">veut</w> <w n="50.3">un</w> <w n="50.4">sang</w> <w n="50.5">illustre</w> <w n="50.6">et</w> <w n="50.7">de</w> <w n="50.8">nobles</w> <w n="50.9">appas</w></l>
					<l n="51" num="9.3"><space unit="char" quantity="8"></space><w n="51.1">Pour</w> <w n="51.2">une</w> <w n="51.3">faveur</w> <w n="51.4">qu</w>’<w n="51.5">il</w> <w n="51.6">accorde</w> ;</l>
					<l n="52" num="9.4"><w n="52.1">Et</w> <w n="52.2">plus</w> <w n="52.3">d</w>’<w n="52.4">un</w> <w n="52.5">sénateur</w> <w n="52.6">aux</w> <w n="52.7">antiques</w> <w n="52.8">aïeux</w></l>
					<l n="53" num="9.5"><w n="53.1">Triomphant</w> <w n="53.2">d</w>’<w n="53.3">être</w> <w n="53.4">père</w>, <w n="53.5">élève</w> <w n="53.6">sous</w> <w n="53.7">ses</w> <w n="53.8">yeux</w>,</l>
					<l n="54" num="9.6"><space unit="char" quantity="8"></space><w n="54.1">Quelque</w> <w n="54.2">petit</w> <w n="54.3">danseur</w> <w n="54.4">de</w> <w n="54.5">corde</w>.</l>
				</lg>
			</div></body></text></TEI>