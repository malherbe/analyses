<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES. FESTONS ET ASTRAGALES</title>
				<title type="sub"></title>
				<title type="medium">Une édition électronique</title>
				<author key="BOU">
					<name>
						<forename>Louis</forename>
						<surname>BOUILHET</surname>
					</name>
					<date from="1822" to="1869">1822-1869</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2963 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BOU_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>POÉSIES. FESTONS ET ASTRAGALES</title>
						<author>Louis Bouilhet</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k9737201m</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES. FESTONS ET ASTRAGALES</title>
								<author>Louis Bouilhet</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE NOUVELLE</publisher>
									<date when="1859">1859</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1859">1859</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-09-20" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.4.xsd)</change>
				<change when="2021-09-20" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BOU54">
				<head type="main">A R***</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Je</w> <w n="1.2">ne</w> <w n="1.3">suis</w> <w n="1.4">pas</w> <w n="1.5">le</w> <w n="1.6">Christ</w>, <w n="1.7">ô</w> <w n="1.8">pâle</w> <w n="1.9">Madeleine</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Pour</w> <w n="2.2">que</w> <w n="2.3">tes</w> <w n="2.4">longs</w> <w n="2.5">cheveux</w> <w n="2.6">caressent</w> <w n="2.7">mes</w> <w n="2.8">pieds</w> <w n="2.9">nus</w> ;</l>
					<l n="3" num="1.3"><w n="3.1">Je</w> <w n="3.2">marche</w>, <w n="3.3">ainsi</w> <w n="3.4">que</w> <w n="3.5">toi</w>, <w n="3.6">dans</w> <w n="3.7">le</w> <w n="3.8">doute</w> <w n="3.9">et</w> <w n="3.10">la</w> <w n="3.11">peine</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Voyageur</w> <w n="4.2">égaré</w> <w n="4.3">par</w> <w n="4.4">les</w> <w n="4.5">chemins</w> <w n="4.6">perdus</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Je</w> <w n="5.2">ne</w> <w n="5.3">te</w> <w n="5.4">dirai</w> <w n="5.5">pas</w> <w n="5.6">les</w>’<w n="5.7">paroles</w> <w n="5.8">divines</w></l>
					<l n="6" num="2.2"><w n="6.1">Qu</w>’<w n="6.2">il</w> <w n="6.3">jetait</w>, <w n="6.4">comme</w> <w n="6.5">un</w> <w n="6.6">baume</w>, <w n="6.7">à</w> <w n="6.8">tous</w> <w n="6.9">les</w> <w n="6.10">cœurs</w> <w n="6.11">souffrants</w>,</l>
					<l n="7" num="2.3"><w n="7.1">Quand</w>, <w n="7.2">suivi</w> <w n="7.3">de</w> <w n="7.4">la</w> <w n="7.5">foule</w>, <w n="7.6">il</w> <w n="7.7">montait</w> <w n="7.8">les</w> <w n="7.9">collines</w>,</l>
					<l n="8" num="2.4"><w n="8.1">Ou</w> <w n="8.2">qu</w>’<w n="8.3">il</w> <w n="8.4">se</w> <w n="8.5">promenait</w> <w n="8.6">près</w> <w n="8.7">des</w> <w n="8.8">lacs</w> <w n="8.9">transparents</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Je</w> <w n="9.2">n</w>’<w n="9.3">ai</w> <w n="9.4">pas</w>, <w n="9.5">comme</w> <w n="9.6">lui</w>, <w n="9.7">cette</w> <w n="9.8">auréole</w> <w n="9.9">pure</w></l>
					<l n="10" num="3.2"><w n="10.1">Qui</w> <w n="10.2">d</w>’<w n="10.3">un</w> <w n="10.4">reflet</w> <w n="10.5">d</w>’<w n="10.6">en</w> <w n="10.7">haut</w> <w n="10.8">dorait</w> <w n="10.9">ses</w> <w n="10.10">blonds</w> <w n="10.11">cheveux</w>,</l>
					<l n="11" num="3.3"><w n="11.1">Et</w> <w n="11.2">je</w> <w n="11.3">ne</w> <w n="11.4">porte</w> <w n="11.5">point</w>, <w n="11.6">pendue</w> <w n="11.7">à</w> <w n="11.8">ma</w> <w n="11.9">ceinture</w>,</l>
					<l n="12" num="3.4"><w n="12.1">La</w> <w n="12.2">clef</w> <w n="12.3">de</w> <w n="12.4">diamant</w> <w n="12.5">qui</w> <w n="12.6">peut</w> <w n="12.7">t</w>’<w n="12.8">ouvrir</w> <w n="12.9">les</w> <w n="12.10">cieux</w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Je</w> <w n="13.2">suis</w> <w n="13.3">un</w> <w n="13.4">des</w> <w n="13.5">derniers</w> <w n="13.6">au</w> <w n="13.7">désert</w> <w n="13.8">de</w> <w n="13.9">la</w> <w n="13.10">vie</w>,</l>
					<l n="14" num="4.2"><w n="14.1">Sous</w> <w n="14.2">ma</w> <w n="14.3">tente</w> <w n="14.4">d</w>’<w n="14.5">un</w> <w n="14.6">jour</w> <w n="14.7">s</w>’<w n="14.8">est</w> <w n="14.9">assis</w> <w n="14.10">le</w> <w n="14.11">malheur</w> ;</l>
					<l n="15" num="4.3"><w n="15.1">Mais</w> <w n="15.2">je</w> <w n="15.3">t</w>’<w n="15.4">ai</w>, <w n="15.5">comme</w> <w n="15.6">Christ</w>, <w n="15.7">pardonné</w> <w n="15.8">ta</w> <w n="15.9">folie</w>,</l>
					<l n="16" num="4.4"><w n="16.1">Et</w> <w n="16.2">demain</w>, <w n="16.3">si</w> <w n="16.4">tu</w> <w n="16.5">veux</w>, <w n="16.6">je</w> <w n="16.7">t</w>’<w n="16.8">ouvrirai</w> <w n="16.9">mon</w> <w n="16.10">cœur</w> !</l>
				</lg>
			</div></body></text></TEI>