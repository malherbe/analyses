<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES. FESTONS ET ASTRAGALES</title>
				<title type="sub"></title>
				<title type="medium">Une édition électronique</title>
				<author key="BOU">
					<name>
						<forename>Louis</forename>
						<surname>BOUILHET</surname>
					</name>
					<date from="1822" to="1869">1822-1869</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2963 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BOU_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>POÉSIES. FESTONS ET ASTRAGALES</title>
						<author>Louis Bouilhet</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k9737201m</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES. FESTONS ET ASTRAGALES</title>
								<author>Louis Bouilhet</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE NOUVELLE</publisher>
									<date when="1859">1859</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1859">1859</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-09-20" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.4.xsd)</change>
				<change when="2021-09-20" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BOU53">
				<head type="main">PORTRAIT</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Je</w> <w n="1.2">ne</w> <w n="1.3">sais</w> <w n="1.4">pas</w> <w n="1.5">ton</w> <w n="1.6">nom</w>, <w n="1.7">comtesse</w> <w n="1.8">ou</w> <w n="1.9">bien</w> <w n="1.10">marquise</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Dont</w> <w n="2.2">le</w> <w n="2.3">portrait</w> <w n="2.4">charmant</w> <w n="2.5">rit</w> <w n="2.6">dans</w> <w n="2.7">ce</w> <w n="2.8">cadre</w> <w n="2.9">d</w>’<w n="2.10">or</w> ;</l>
					<l n="3" num="1.3"><w n="3.1">Mais</w> <w n="3.2">nulle</w>, <w n="3.3">en</w> <w n="3.4">sa</w> <w n="3.5">beauté</w>, <w n="3.6">n</w>’<w n="3.7">eut</w> <w n="3.8">plus</w> <w n="3.9">de</w> <w n="3.10">grâce</w> <w n="3.11">exquise</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Au</w> <w n="4.2">temps</w> <w n="4.3">qu</w>’<w n="4.4">on</w> <w n="4.5">était</w> <w n="4.6">jeune</w> <w n="4.7">et</w> <w n="4.8">qu</w>’<w n="4.9">on</w> <w n="4.10">aimait</w> <w n="4.11">encor</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Tes</w> <w n="5.2">cheveux</w> <w n="5.3">à</w> <w n="5.4">frimas</w>, <w n="5.5">où</w> <w n="5.6">le</w> <w n="5.7">zéphyr</w> <w n="5.8">se</w> <w n="5.9">joue</w>,</l>
					<l n="6" num="2.2"><w n="6.1">Effleurent</w> <w n="6.2">mollement</w> <w n="6.3">ton</w> <w n="6.4">visage</w> <w n="6.5">vermeil</w>,</l>
					<l n="7" num="2.3"><w n="7.1">Car</w> <w n="7.2">le</w> <w n="7.3">pastel</w> <w n="7.4">du</w> <w n="7.5">maître</w> <w n="7.6">a</w> <w n="7.7">semé</w> <w n="7.8">sur</w> <w n="7.9">ta</w> <w n="7.10">joue</w></l>
					<l n="8" num="2.4"><w n="8.1">L</w>’<w n="8.2">incarnat</w> <w n="8.3">velouté</w> <w n="8.4">d</w>’<w n="8.5">une</w> <w n="8.6">pèche</w> <w n="8.7">au</w> <w n="8.8">soleil</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Mille</w> <w n="9.2">amours</w> <w n="9.3">sont</w> <w n="9.4">nichés</w> <w n="9.5">sous</w> <w n="9.6">tes</w> <w n="9.7">narines</w> <w n="9.8">rose</w>,</l>
					<l n="10" num="3.2"><w n="10.1">Mille</w> <w n="10.2">autres</w> <w n="10.3">sont</w> <w n="10.4">blottis</w> <w n="10.5">dans</w> <w n="10.6">tes</w> <w n="10.7">yeux</w> <w n="10.8">irisés</w>,</l>
					<l n="11" num="3.3"><w n="11.1">Tandis</w> <w n="11.2">que</w> <w n="11.3">Cupidon</w>, <w n="11.4">sur</w> <w n="11.5">tes</w> <w n="11.6">lèvres</w> <w n="11.7">mi</w>-<w n="11.8">closes</w>,</l>
					<l n="12" num="3.4"><w n="12.1">Appelle</w> <w n="12.2">au</w> <w n="12.3">pâturage</w> <w n="12.4">un</w> <w n="12.5">troupeau</w> <w n="12.6">de</w> <w n="12.7">baisers</w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Et</w> <w n="13.2">le</w> <w n="13.3">ruban</w> <w n="13.4">bleu</w>-<w n="13.5">ciel</w>, <w n="13.6">dont</w> <w n="13.7">ta</w> <w n="13.8">robe</w> <w n="13.9">est</w> <w n="13.10">fermée</w>,</l>
					<l n="14" num="4.2"><w n="14.1">Semble</w>, <w n="14.2">au</w> <w n="14.3">long</w> <w n="14.4">du</w> <w n="14.5">corsage</w>, <w n="14.6">étaler</w> <w n="14.7">à</w> <w n="14.8">plaisir</w>^</l>
					<l n="15" num="4.3"><w n="15.1">De</w> <w n="15.2">ta</w> <w n="15.3">taille</w> <w n="15.4">divine</w> <w n="15.5">à</w> <w n="15.6">ta</w> <w n="15.7">gorge</w> <w n="15.8">embaumée</w>,</l>
					<l n="16" num="4.4"><w n="16.1">Une</w> <w n="16.2">échelle</w> <w n="16.3">d</w>’<w n="16.4">azur</w> <w n="16.5">où</w> <w n="16.6">monte</w> <w n="16.7">le</w> <w n="16.8">désir</w> !…</l>
				</lg>
			</div></body></text></TEI>