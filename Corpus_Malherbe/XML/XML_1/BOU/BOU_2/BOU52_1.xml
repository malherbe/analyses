<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES. FESTONS ET ASTRAGALES</title>
				<title type="sub"></title>
				<title type="medium">Une édition électronique</title>
				<author key="BOU">
					<name>
						<forename>Louis</forename>
						<surname>BOUILHET</surname>
					</name>
					<date from="1822" to="1869">1822-1869</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2963 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BOU_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>POÉSIES. FESTONS ET ASTRAGALES</title>
						<author>Louis Bouilhet</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k9737201m</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES. FESTONS ET ASTRAGALES</title>
								<author>Louis Bouilhet</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE NOUVELLE</publisher>
									<date when="1859">1859</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1859">1859</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-09-20" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.4.xsd)</change>
				<change when="2021-09-20" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BOU52">
				<head type="main">CHATTERIE</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Je</w> <w n="1.2">la</w> <w n="1.3">vis</w> <w n="1.4">seule</w>, <w n="1.5">aux</w> <w n="1.6">derniers</w> <w n="1.7">rangs</w> <w n="1.8">assise</w> ;</l>
					<l n="2" num="1.2"><w n="2.1">Des</w> <w n="2.2">feux</w> <w n="2.3">du</w> <w n="2.4">lustre</w> <w n="2.5">éclairée</w> <w n="2.6">à</w> <w n="2.7">demi</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Elle</w> <w n="3.2">courbait</w>, <w n="3.3">comme</w> <w n="3.4">un</w> <w n="3.5">chat</w> <w n="3.6">endormi</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Son</w> <w n="4.2">dos</w> <w n="4.3">frileux</w>, <w n="4.4">sous</w> <w n="4.5">sa</w> <w n="4.6">fourrure</w> <w n="4.7">grise</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Sa</w> <w n="5.2">main</w> <w n="5.3">mignarde</w>, <w n="5.4">aux</w> <w n="5.5">gestes</w> <w n="5.6">ambigus</w>,</l>
					<l n="6" num="2.2"><w n="6.1">Dans</w> <w n="6.2">un</w> <w n="6.3">gant</w> <w n="6.4">paille</w> <w n="6.5">avait</w> <w n="6.6">rentré</w> <w n="6.7">ses</w> <w n="6.8">griffes</w> ;</l>
					<l n="7" num="2.3"><w n="7.1">Ses</w> <w n="7.2">longs</w> <w n="7.3">yeux</w> <w n="7.4">verts</w>, <w n="7.5">comme</w> <w n="7.6">deux</w> <w n="7.7">escogriffes</w>,</l>
					<l n="8" num="2.4"><w n="8.1">Dévotement</w> <w n="8.2">fermaient</w> <w n="8.3">leurs</w> <w n="8.4">cils</w> <w n="8.5">aigus</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">A</w> <w n="9.2">peine</w>, <w n="9.3">au</w> <w n="9.4">bord</w> <w n="9.5">de</w> <w n="9.6">ses</w> <w n="9.7">lèvres</w> <w n="9.8">félines</w>,</l>
					<l n="10" num="3.2"><w n="10.1">Passait</w> <w n="10.2">le</w> <w n="10.3">bout</w> <w n="10.4">des</w> <w n="10.5">petits</w> <w n="10.6">crocs</w> <w n="10.7">d</w>’<w n="10.8">émail</w>,</l>
					<l n="11" num="3.3"><w n="11.1">Et</w> <w n="11.2">son</w> <w n="11.3">nez</w> <w n="11.4">mince</w>, <w n="11.5">au</w> <w n="11.6">rose</w> <w n="11.7">soupirail</w>,</l>
					<l n="12" num="3.4"><w n="12.1">D</w>’<w n="12.2">un</w> <w n="12.3">souffle</w> <w n="12.4">frais</w> <w n="12.5">baignait</w> <w n="12.6">ses</w> <w n="12.7">barbes</w> <w n="12.8">fines</w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Soudain</w> <w n="13.2">la</w> <w n="13.3">belle</w> (<w n="13.4">un</w> <w n="13.5">homme</w> <w n="13.6">était</w> <w n="13.7">entré</w>)</l>
					<l n="14" num="4.2"><w n="14.1">Sembla</w> <w n="14.2">frémir</w> <w n="14.3">sous</w> <w n="14.4">ses</w> <w n="14.5">noires</w> <w n="14.6">dentelles</w>,</l>
					<l n="15" num="4.3"><w n="15.1">Et</w> <w n="15.2">j</w>’<w n="15.3">entendis</w> <w n="15.4">comme</w> <w n="15.5">un</w> <w n="15.6">bruit</w> <w n="15.7">d</w>’<w n="15.8">étincelles</w></l>
					<l n="16" num="4.4"><w n="16.1">Qui</w> <w n="16.2">s</w>’<w n="16.3">échappait</w> <w n="16.4">de</w> <w n="16.5">son</w> <w n="16.6">jupon</w> <w n="16.7">moiré</w> !…</l>
				</lg>
			</div></body></text></TEI>