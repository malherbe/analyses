<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES. FESTONS ET ASTRAGALES</title>
				<title type="sub"></title>
				<title type="medium">Une édition électronique</title>
				<author key="BOU">
					<name>
						<forename>Louis</forename>
						<surname>BOUILHET</surname>
					</name>
					<date from="1822" to="1869">1822-1869</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2963 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BOU_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>POÉSIES. FESTONS ET ASTRAGALES</title>
						<author>Louis Bouilhet</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k9737201m</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES. FESTONS ET ASTRAGALES</title>
								<author>Louis Bouilhet</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE NOUVELLE</publisher>
									<date when="1859">1859</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1859">1859</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-09-20" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.4.xsd)</change>
				<change when="2021-09-20" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BOU10">
				<head type="main">A UNE PETITE FILLE</head>
				<head type="sub_1">ÉLEVÉE AU BORD DE LA MER</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Pourquoi</w> <w n="1.2">pleurer</w>, <w n="1.3">ma</w> <w n="1.4">petite</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Lorsque</w> <w n="2.2">le</w> <w n="2.3">jour</w> <w n="2.4">est</w> <w n="2.5">fini</w> ?</l>
					<l n="3" num="1.3"><w n="3.1">Fais</w> <w n="3.2">silence</w> ! <w n="3.3">et</w> <w n="3.4">dors</w> <w n="3.5">bien</w> <w n="3.6">vite</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Comme</w> <w n="4.2">un</w> <w n="4.3">oiseau</w> <w n="4.4">dans</w> <w n="4.5">son</w> <w n="4.6">nid</w> !</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Au</w> <w n="5.2">bruit</w> <w n="5.3">des</w> <w n="5.4">vents</w> <w n="5.5">de</w> <w n="5.6">décembre</w>,</l>
					<l n="6" num="2.2"><w n="6.1">Songe</w>, <w n="6.2">songe</w>, <w n="6.3">entre</w> <w n="6.4">tes</w> <w n="6.5">draps</w>,</l>
					<l n="7" num="2.3"><w n="7.1">Comme</w> <w n="7.2">il</w> <w n="7.3">fait</w> <w n="7.4">bon</w> <w n="7.5">dans</w> <w n="7.6">ta</w> <w n="7.7">chambre</w>,</l>
					<l n="8" num="2.4"><w n="8.1">Et</w> <w n="8.2">comme</w> <w n="8.3">on</w> <w n="8.4">a</w> <w n="8.5">froid</w> <w n="8.6">là</w>-<w n="8.7">bas</w> !</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Loin</w> <w n="9.2">des</w> <w n="9.3">flots</w> <w n="9.4">et</w> <w n="9.5">du</w> <w n="9.6">rivage</w>,</l>
					<l n="10" num="3.2"><w n="10.1">Dans</w> <w n="10.2">mon</w> <w n="10.3">pays</w>, <w n="10.4">quelquefois</w>,</l>
					<l n="11" num="3.3"><w n="11.1">Un</w> <w n="11.2">enfant</w> <w n="11.3">qui</w> <w n="11.4">n</w>’<w n="11.5">est</w> <w n="11.6">pas</w> <w n="11.7">sage</w></l>
					<l n="12" num="3.4"><w n="12.1">Est</w> <w n="12.2">pris</w> <w n="12.3">par</w> <w n="12.4">le</w> <w n="12.5">loup</w> <w n="12.6">des</w> <w n="12.7">bois</w> ;</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Mais</w> <w n="13.2">ici</w> !… <w n="13.3">quelle</w> <w n="13.4">voix</w> <w n="13.5">gronde</w></l>
					<l n="14" num="4.2"><w n="14.1">Et</w> <w n="14.2">se</w> <w n="14.3">roule</w>, <w n="14.4">dans</w> <w n="14.5">la</w> <w n="14.6">nuit</w> ?…</l>
					<l n="15" num="4.3"><w n="15.1">C</w>’<w n="15.2">est</w> <w n="15.3">la</w> <w n="15.4">mer</w>, <w n="15.5">la</w> <w n="15.6">mer</w> <w n="15.7">profonde</w> !…</l>
					<l n="16" num="4.4"><w n="16.1">Jeanne</w>, <w n="16.2">ne</w> <w n="16.3">fais</w> <w n="16.4">point</w> <w n="16.5">de</w> <w n="16.6">bruit</w> !</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">Dès</w> <w n="17.2">que</w> <w n="17.3">Dieu</w>, <w n="17.4">sous</w> <w n="17.5">le</w> <w n="17.6">ciel</w> <w n="17.7">sombre</w>,</l>
					<l n="18" num="5.2"><w n="18.1">Rallume</w> <w n="18.2">ses</w> <w n="18.3">astres</w> <w n="18.4">d</w>’<w n="18.5">or</w>,</l>
					<l n="19" num="5.3"><w n="19.1">Les</w> <w n="19.2">flots</w> <w n="19.3">écoutent</w>, <w n="19.4">dans</w> <w n="19.5">l</w>’<w n="19.6">ombre</w>,</l>
					<l n="20" num="5.4"><w n="20.1">Si</w> <w n="20.2">le</w> <w n="20.3">petit</w> <w n="20.4">enfant</w> <w n="20.5">dort</w> ;</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">Ton</w> <w n="21.2">cri</w> <w n="21.3">qu</w>’<w n="21.4">on</w> <w n="21.5">pourrait</w> <w n="21.6">entendre</w></l>
					<l n="22" num="6.2"><w n="22.1">Au</w> <w n="22.2">fond</w> <w n="22.3">de</w> <w n="22.4">l</w>’<w n="22.5">abîme</w> <w n="22.6">amer</w></l>
					<l n="23" num="6.3"><w n="23.1">Ferait</w> <w n="23.2">venir</w> <w n="23.3">pour</w> <w n="23.4">te</w> <w n="23.5">prendre</w>,</l>
					<l n="24" num="6.4"><w n="24.1">Les</w> <w n="24.2">grands</w> <w n="24.3">poissons</w> <w n="24.4">de</w> <w n="24.5">la</w> <w n="24.6">mer</w> !</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">Ils</w> <w n="25.2">ont</w> <w n="25.3">des</w> <w n="25.4">écailles</w> <w n="25.5">bleues</w>,</l>
					<l n="26" num="7.2"><w n="26.1">Des</w> <w n="26.2">yeux</w> <w n="26.3">ronds</w>, <w n="26.4">ouverts</w> <w n="26.5">toujours</w>,</l>
					<l n="27" num="7.3"><w n="27.1">Et</w>, <w n="27.2">du</w> <w n="27.3">revers</w> <w n="27.4">de</w> <w n="27.5">leurs</w> <w n="27.6">queues</w>,</l>
					<l n="28" num="7.4"><w n="28.1">Font</w> <w n="28.2">couler</w> <w n="28.3">les</w> <w n="28.4">vaisseaux</w> <w n="28.5">lourds</w>.</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1">Ils</w> <w n="29.2">viendraient</w>, <w n="29.3">au</w> <w n="29.4">clair</w> <w n="29.5">de</w> <w n="29.6">lune</w>,</l>
					<l n="30" num="8.2"><w n="30.1">Se</w> <w n="30.2">traînant</w> <w n="30.3">sur</w> <w n="30.4">le</w> <w n="30.5">galet</w>,</l>
					<l n="31" num="8.3"><w n="31.1">Frotter</w> <w n="31.2">leur</w> <w n="31.3">narine</w> <w n="31.4">brune</w></l>
					<l n="32" num="8.4"><w n="32.1">A</w> <w n="32.2">la</w> <w n="32.3">barre</w> <w n="32.4">du</w> <w n="32.5">volet</w> !…</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1"><w n="33.1">Puis</w>, <w n="33.2">malgré</w> <w n="33.3">ta</w> <w n="33.4">voix</w> <w n="33.5">timide</w>,</l>
					<l n="34" num="9.2"><w n="34.1">Par</w> <w n="34.2">la</w> <w n="34.3">chambre</w> <w n="34.4">se</w> <w n="34.5">roulant</w>,</l>
					<l n="35" num="9.3"><w n="35.1">Quelque</w> <w n="35.2">bête</w> <w n="35.3">au</w> <w n="35.4">dos</w> <w n="35.5">humide</w></l>
					<l n="36" num="9.4"><w n="36.1">T</w>’<w n="36.2">emporterait</w> <w n="36.3">en</w> <w n="36.4">soufflant</w> !</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1"><w n="37.1">Où</w> <w n="37.2">seraient</w> <w n="37.3">ta</w> <w n="37.4">couche</w> <w n="37.5">blanche</w>,</l>
					<l n="38" num="10.2"><w n="38.1">Ton</w> <w n="38.2">oreiller</w> <w n="38.3">de</w> <w n="38.4">satin</w>,</l>
					<l n="39" num="10.3"><w n="39.1">Et</w> <w n="39.2">ta</w> <w n="39.3">mère</w> <w n="39.4">qui</w> <w n="39.5">se</w> <w n="39.6">penche</w></l>
					<l n="40" num="10.4"><w n="40.1">Pour</w> <w n="40.2">t</w>’<w n="40.3">éveiller</w> <w n="40.4">le</w> <w n="40.5">matin</w> ?…</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1"><w n="41.1">Tu</w> <w n="41.2">n</w>’<w n="41.3">aurais</w>, <w n="41.4">pauvre</w> <w n="41.5">Jeannette</w></l>
					<l n="42" num="11.2">( <w n="42.1">Ainsi</w> <w n="42.2">le</w> <w n="42.3">veut</w> <w n="42.4">le</w> <w n="42.5">bon</w> <w n="42.6">Dieu</w> ),</l>
					<l n="43" num="11.3"><w n="43.1">Que</w> <w n="43.2">le</w> <w n="43.3">sable</w> <w n="43.4">pour</w> <w n="43.5">couchette</w>,</l>
					<l n="44" num="11.4"><w n="44.1">Et</w> <w n="44.2">les</w> <w n="44.3">flots</w> <w n="44.4">pour</w> <w n="44.5">rideau</w> <w n="44.6">bleu</w> !</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1"><w n="45.1">Pourquoi</w> <w n="45.2">pleurer</w>, <w n="45.3">ma</w> <w n="45.4">petite</w>,</l>
					<l n="46" num="12.2"><w n="46.1">Lorsque</w> <w n="46.2">le</w> <w n="46.3">jour</w> <w n="46.4">est</w> <w n="46.5">fini</w> ?…</l>
					<l n="47" num="12.3"><w n="47.1">Fais</w> <w n="47.2">silence</w> !… <w n="47.3">et</w> <w n="47.4">dors</w> <w n="47.5">bien</w> <w n="47.6">vite</w>,</l>
					<l n="48" num="12.4"><w n="48.1">Comme</w> <w n="48.2">un</w> <w n="48.3">oiseau</w> <w n="48.4">dans</w> <w n="48.5">son</w> <w n="48.6">nid</w> !…</l>
				</lg>
			</div></body></text></TEI>