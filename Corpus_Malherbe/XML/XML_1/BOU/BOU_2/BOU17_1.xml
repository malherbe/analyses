<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES. FESTONS ET ASTRAGALES</title>
				<title type="sub"></title>
				<title type="medium">Une édition électronique</title>
				<author key="BOU">
					<name>
						<forename>Louis</forename>
						<surname>BOUILHET</surname>
					</name>
					<date from="1822" to="1869">1822-1869</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2963 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BOU_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>POÉSIES. FESTONS ET ASTRAGALES</title>
						<author>Louis Bouilhet</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k9737201m</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES. FESTONS ET ASTRAGALES</title>
								<author>Louis Bouilhet</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE NOUVELLE</publisher>
									<date when="1859">1859</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1859">1859</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-09-20" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.4.xsd)</change>
				<change when="2021-09-20" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BOU17">
				<head type="main">LA LOUVE</head>
				<lg n="1">
					<l n="1" num="1.1"><space unit="char" quantity="10"></space><w n="1.1">Marcia</w>, <w n="1.2">la</w> <w n="1.3">vieille</w> <w n="1.4">louve</w>,</l>
					<l n="2" num="1.2"><space unit="char" quantity="10"></space><w n="2.1">Au</w> <w n="2.2">fond</w> <w n="2.3">de</w> <w n="2.4">son</w> <w n="2.5">antre</w> <w n="2.6">couve</w></l>
					<l n="3" num="1.3"><space unit="char" quantity="10"></space><w n="3.1">Plus</w> <w n="3.2">d</w>’<w n="3.3">une</w> <w n="3.4">jeune</w> <w n="3.5">beauté</w>,</l>
					<l n="4" num="1.4"><space unit="char" quantity="10"></space><w n="4.1">Et</w>, <w n="4.2">quand</w> <w n="4.3">la</w> <w n="4.4">rue</w> <w n="4.5">est</w> <w n="4.6">obscure</w>,</l>
					<l n="5" num="1.5"><space unit="char" quantity="10"></space><w n="5.1">Répand</w> <w n="5.2">au</w> <w n="5.3">loin</w>, <w n="5.4">dans</w> <w n="5.5">Suburre</w>,</l>
					<l n="6" num="1.6"><space unit="char" quantity="10"></space><w n="6.1">Son</w> <w n="6.2">fol</w> <w n="6.3">essaim</w> <w n="6.4">qui</w> <w n="6.5">murmure</w></l>
					<l n="7" num="1.7"><space unit="char" quantity="10"></space><w n="7.1">Par</w> <w n="7.2">les</w> <w n="7.3">chaudes</w> <w n="7.4">nuits</w> <w n="7.5">d</w>’<w n="7.6">été</w> !</l>
				</lg>
				<lg n="2">
					<l n="8" num="2.1"><w n="8.1">Elle</w> <w n="8.2">a</w> <w n="8.3">la</w> <w n="8.4">belle</w> <w n="8.5">Grecque</w>, <w n="8.6">enivrante</w> <w n="8.7">sirène</w>,</l>
					<l n="9" num="2.2"><w n="9.1">La</w> <w n="9.2">fille</w> <w n="9.3">de</w> <w n="9.4">Lesbos</w> <w n="9.5">aux</w> <w n="9.6">soupirs</w> <w n="9.7">cadencés</w>,</l>
					<l n="10" num="2.3"><w n="10.1">Qui</w> <w n="10.2">suspend</w> <w n="10.3">ses</w> <w n="10.4">doigts</w> <w n="10.5">blancs</w> <w n="10.6">à</w> <w n="10.7">sa</w> <w n="10.8">lyre</w> <w n="10.9">d</w>’<w n="10.10">ébène</w>,</l>
					<l n="11" num="2.4"><w n="11.1">Et</w> <w n="11.2">danse</w> <w n="11.3">aux</w> <w n="11.4">carrefours</w> <w n="11.5">la</w> <w n="11.6">danse</w> <w n="11.7">ionienne</w>,</l>
					<l n="12" num="2.5"><w n="12.1">Avec</w> <w n="12.2">un</w> <w n="12.3">bandeau</w> <w n="12.4">d</w>’<w n="12.5">or</w> <w n="12.6">sur</w> <w n="12.7">ses</w> <w n="12.8">cheveux</w> <w n="12.9">tressés</w> !</l>
				</lg>
				<lg n="3">
					<l n="13" num="3.1"><space unit="char" quantity="10"></space><w n="13.1">Elle</w> <w n="13.2">a</w> <w n="13.3">l</w>’<w n="13.4">ardente</w> <w n="13.5">Latine</w>,</l>
					<l n="14" num="3.2"><space unit="char" quantity="10"></space><w n="14.1">Qui</w> <w n="14.2">sous</w> <w n="14.3">une</w> <w n="14.4">mitre</w> <w n="14.5">incline</w></l>
					<l n="15" num="3.3"><space unit="char" quantity="10"></space><w n="15.1">Son</w> <w n="15.2">front</w> <w n="15.3">bruni</w> <w n="15.4">du</w> <w n="15.5">soleil</w>,</l>
					<l n="16" num="3.4"><space unit="char" quantity="10"></space><w n="16.1">Nymphe</w> <w n="16.2">au</w> <w n="16.3">sourire</w> <w n="16.4">magique</w>,</l>
					<l n="17" num="3.5"><space unit="char" quantity="10"></space><w n="17.1">Glissant</w> <w n="17.2">sous</w> <w n="17.3">le</w> <w n="17.4">blanc</w> <w n="17.5">portique</w>,</l>
					<l n="18" num="3.6"><space unit="char" quantity="10"></space><w n="18.1">Avec</w> <w n="18.2">sa</w> <w n="18.3">fauve</w> <w n="18.4">tunique</w></l>
					<l n="19" num="3.7"><space unit="char" quantity="10"></space><w n="19.1">Et</w> <w n="19.2">son</w> <w n="19.3">brodequin</w> <w n="19.4">vermeil</w> !</l>
				</lg>
				<lg n="4">
					<l n="20" num="4.1"><w n="20.1">Elle</w> <w n="20.2">a</w> <w n="20.3">pour</w> <w n="20.4">nos</w> <w n="20.5">plaisirs</w>, <w n="20.6">la</w> <w n="20.7">Gauloise</w> <w n="20.8">superbe</w>,</l>
					<l n="21" num="4.2"><w n="21.1">Le</w> <w n="21.2">front</w> <w n="21.3">ceint</w> <w n="21.4">de</w> <w n="21.5">gui</w> <w n="21.6">pâle</w>, <w n="21.7">aux</w> <w n="21.8">feuillages</w> <w n="21.9">amers</w> ;</l>
					<l n="22" num="4.3"><w n="22.1">Son</w> <w n="22.2">pied</w> <w n="22.3">neigeux</w> <w n="22.4">bondit</w> <w n="22.5">sans</w> <w n="22.6">faire</w> <w n="22.7">plier</w> <w n="22.8">l</w>’<w n="22.9">herbe</w> !</l>
					<l n="23" num="4.4"><w n="23.1">Ses</w> <w n="23.2">longs</w> <w n="23.3">cheveux</w> <w n="23.4">épars</w> <w n="23.5">semblent</w> <w n="23.6">l</w>’<w n="23.7">or</w> <w n="23.8">d</w>’<w n="23.9">une</w> <w n="23.10">gerbe</w>,</l>
					<l n="24" num="4.5"><w n="24.1">Et</w> <w n="24.2">son</w> <w n="24.3">regard</w> <w n="24.4">farouche</w> <w n="24.5">est</w> <w n="24.6">bleu</w> <w n="24.7">comme</w> <w n="24.8">les</w> <w n="24.9">mers</w> !</l>
				</lg>
				<lg n="5">
					<l n="25" num="5.1"><space unit="char" quantity="10"></space><w n="25.1">Elle</w> <w n="25.2">a</w> <w n="25.3">ses</w> <w n="25.4">négresses</w> <w n="25.5">folles</w></l>
					<l n="26" num="5.2"><space unit="char" quantity="10"></space><w n="26.1">Qui</w>, <w n="26.2">sur</w> <w n="26.3">leurs</w> <w n="26.4">noires</w> <w n="26.5">épaules</w>,</l>
					<l n="27" num="5.3"><space unit="char" quantity="10"></space><w n="27.1">Enlacent</w> <w n="27.2">des</w> <w n="27.3">serpents</w> <w n="27.4">verts</w>.</l>
					<l n="28" num="5.4"><space unit="char" quantity="10"></space><w n="28.1">Elle</w> <w n="28.2">a</w> <w n="28.3">l</w>’<w n="28.4">Arabe</w> <w n="28.5">indolente</w></l>
					<l n="29" num="5.5"><space unit="char" quantity="10"></space><w n="29.1">Qui</w>, <w n="29.2">la</w> <w n="29.3">nuit</w>, <w n="29.4">dort</w> <w n="29.5">sous</w> <w n="29.6">la</w> <w n="29.7">tente</w>,</l>
					<l n="30" num="5.6"><space unit="char" quantity="10"></space><w n="30.1">Et</w> <w n="30.2">le</w> <w n="30.3">jour</w> <w n="30.4">boit</w>, <w n="30.5">haletante</w>,</l>
					<l n="31" num="5.7"><space unit="char" quantity="10"></space><w n="31.1">A</w> <w n="31.2">la</w> <w n="31.3">source</w> <w n="31.4">des</w> <w n="31.5">déserts</w> !</l>
				</lg>
				<lg n="6">
					<l n="32" num="6.1">— <w n="32.1">Mais</w> <w n="32.2">la</w> <w n="32.3">plus</w> <w n="32.4">belle</w>, <w n="32.5">amis</w>, <w n="32.6">c</w>’<w n="32.7">est</w> <w n="32.8">la</w> <w n="32.9">blanche</w> <w n="32.10">chrétienne</w>,</l>
					<l n="33" num="6.2"><w n="33.1">Qui</w> <w n="33.2">pleure</w> <w n="33.3">et</w> <w n="33.4">ne</w> <w n="33.5">veut</w> <w n="33.6">pas</w>, <w n="33.7">et</w> <w n="33.8">rougit</w> <w n="33.9">tour</w> <w n="33.10">à</w> <w n="33.11">tour</w>,</l>
					<l n="34" num="6.3"><w n="34.1">Et</w> <w n="34.2">qui</w> <w n="34.3">de</w> <w n="34.4">son</w> <w n="34.5">Dieu</w> <w n="34.6">mort</w> <w n="34.7">pressant</w> <w n="34.8">l</w>’<w n="34.9">image</w> <w n="34.10">vaine</w>,</l>
					<l n="35" num="6.4"><w n="35.1">Demande</w> <w n="35.2">à</w> <w n="35.3">deux</w> <w n="35.4">genoux</w> <w n="35.5">les</w> <w n="35.6">tigres</w> <w n="35.7">de</w> <w n="35.8">l</w>’<w n="35.9">arène</w>,</l>
					<l n="36" num="6.5"><w n="36.1">Quand</w> <w n="36.2">on</w> <w n="36.3">la</w> <w n="36.4">jette</w> <w n="36.5">nue</w> <w n="36.6">aux</w> <w n="36.7">baisers</w> <w n="36.8">de</w> <w n="36.9">l</w>’<w n="36.10">amour</w> !</l>
				</lg>
			</div></body></text></TEI>