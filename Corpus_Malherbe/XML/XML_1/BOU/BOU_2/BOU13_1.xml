<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES. FESTONS ET ASTRAGALES</title>
				<title type="sub"></title>
				<title type="medium">Une édition électronique</title>
				<author key="BOU">
					<name>
						<forename>Louis</forename>
						<surname>BOUILHET</surname>
					</name>
					<date from="1822" to="1869">1822-1869</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2963 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BOU_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>POÉSIES. FESTONS ET ASTRAGALES</title>
						<author>Louis Bouilhet</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k9737201m</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES. FESTONS ET ASTRAGALES</title>
								<author>Louis Bouilhet</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE NOUVELLE</publisher>
									<date when="1859">1859</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1859">1859</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-09-20" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.4.xsd)</change>
				<change when="2021-09-20" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BOU13">
				<head type="main">NÉÉRA</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Corydon</w> <w n="1.2">le</w> <w n="1.3">pasteur</w>, <w n="1.4">assis</w> <w n="1.5">au</w> <w n="1.6">bord</w> <w n="1.7">de</w> <w n="1.8">l</w>’<w n="1.9">onde</w></l>
					<l n="2" num="1.2"><w n="2.1">Un</w> <w n="2.2">soir</w> <w n="2.3">chantait</w> <w n="2.4">cet</w> <w n="2.5">hymne</w> <w n="2.6">à</w> <w n="2.7">Néère</w> <w n="2.8">aux</w> <w n="2.9">longs</w> <w n="2.10">yeux</w> :</l>
				</lg>
				<lg n="2">
					<l n="3" num="2.1">« —<w n="3.1">Tout</w> <w n="3.2">aime</w>, <w n="3.3">ô</w> <w n="3.4">Nééra</w>, <w n="3.5">tout</w> <w n="3.6">aime</w> <w n="3.7">dans</w> <w n="3.8">le</w> <w n="3.9">monde</w>,</l>
					<l n="4" num="2.2"><w n="4.1">Et</w> <w n="4.2">l</w>’<w n="4.3">homme</w> <w n="4.4">a</w> <w n="4.5">su</w> <w n="4.6">l</w>’<w n="4.7">amour</w> <w n="4.8">par</w> <w n="4.9">l</w>’<w n="4.10">exemple</w> <w n="4.11">des</w> <w n="4.12">dieux</w> !</l>
					<l n="5" num="2.3"><w n="5.1">L</w>’<w n="5.2">atelier</w> <w n="5.3">des</w> <w n="5.4">sculpteurs</w> <w n="5.5">est</w> <w n="5.6">plein</w> <w n="5.7">de</w> <w n="5.8">cette</w> <w n="5.9">histoire</w> ;</l>
					<l n="6" num="2.4"><w n="6.1">Les</w> <w n="6.2">marbres</w> <w n="6.3">ont</w> <w n="6.4">manqué</w> <w n="6.5">pour</w> <w n="6.6">l</w>’<w n="6.7">étaler</w> <w n="6.8">au</w> <w n="6.9">jour</w>.</l>
					<l n="7" num="2.5"><w n="7.1">Cachant</w> <w n="7.2">son</w> <w n="7.3">front</w> <w n="7.4">divin</w> <w n="7.5">sous</w> <w n="7.6">des</w> <w n="7.7">cornes</w> <w n="7.8">d</w>’<w n="7.9">ivoire</w>,</l>
					<l n="8" num="2.6"><w n="8.1">Jupiter</w>, <w n="8.2">près</w> <w n="8.3">d</w>’<w n="8.4">Europe</w>, <w n="8.5">a</w> <w n="8.6">mugi</w> <w n="8.7">son</w> <w n="8.8">amour</w> !</l>
					<l n="9" num="2.7"><w n="9.1">Au</w> <w n="9.2">fond</w> <w n="9.3">des</w> <w n="9.4">antres</w> <w n="9.5">frais</w> <w n="9.6">où</w> <w n="9.7">croit</w> <w n="9.8">l</w>’<w n="9.9">algue</w> <w n="9.10">salée</w>,</l>
					<l n="10" num="2.8"><w n="10.1">Parmi</w> <w n="10.2">les</w> <w n="10.3">galets</w> <w n="10.4">blancs</w> <w n="10.5">et</w> <w n="10.6">les</w> <w n="10.7">rouges</w> <w n="10.8">coraux</w>,</l>
					<l n="11" num="2.9"><w n="11.1">Thétis</w> <w n="11.2">abandonna</w>, <w n="11.3">dans</w> <w n="11.4">les</w> <w n="11.5">bras</w> <w n="11.6">de</w> <w n="11.7">Pélée</w>,</l>
					<l n="12" num="2.10"><w n="12.1">Sa</w> <w n="12.2">gorge</w> <w n="12.3">humide</w> <w n="12.4">encor</w> <w n="12.5">de</w> <w n="12.6">l</w>’<w n="12.7">écume</w> <w n="12.8">des</w> <w n="12.9">eaux</w> !</l>
					<l n="13" num="2.11"><w n="13.1">Tout</w> <w n="13.2">aime</w>, <w n="13.3">ô</w> <w n="13.4">Nééra</w>, <w n="13.5">jusqu</w>’<w n="13.6">à</w> <w n="13.7">Phébé</w> <w n="13.8">la</w> <w n="13.9">blonde</w>,</l>
					<l n="14" num="2.12"><w n="14.1">Phébé</w>, <w n="14.2">qui</w> <w n="14.3">hait</w> <w n="14.4">l</w>’<w n="14.5">hymen</w>, <w n="14.6">et</w> <w n="14.7">qu</w>’<w n="14.8">on</w> <w n="14.9">croit</w> <w n="14.10">vierge</w> <w n="14.11">encor</w> ;</l>
					<l n="15" num="2.13"><w n="15.1">J</w>’<w n="15.2">ai</w> <w n="15.3">vu</w>, <w n="15.4">sur</w> <w n="15.5">les</w> <w n="15.6">buissons</w> <w n="15.7">que</w> <w n="15.8">sa</w> <w n="15.9">lumière</w> <w n="15.10">inonde</w>,</l>
					<l n="16" num="2.14"><w n="16.1">Pendre</w> <w n="16.2">son</w> <w n="16.3">blanc</w> <w n="16.4">cothurne</w> <w n="16.5">avec</w> <w n="16.6">son</w> <w n="16.7">carquois</w> <w n="16.8">d</w>’<w n="16.9">or</w> !</l>
					<l n="17" num="2.15"><w n="17.1">Ses</w> <w n="17.2">pieds</w> <w n="17.3">nus</w>, <w n="17.4">en</w> <w n="17.5">silence</w>, <w n="17.6">effleuraient</w> <w n="17.7">la</w> <w n="17.8">bruyère</w>,</l>
					<l n="18" num="2.16"><w n="18.1">Sans</w> <w n="18.2">réveiller</w> <w n="18.3">la</w> <w n="18.4">biche</w> <w n="18.5">ou</w> <w n="18.6">le</w> <w n="18.7">faisan</w> <w n="18.8">vermeil</w>,</l>
					<l n="19" num="2.17"><w n="19.1">Car</w> <w n="19.2">elle</w> <w n="19.3">allait</w> <w n="19.4">trouver</w>, <w n="19.5">près</w> <w n="19.6">de</w> <w n="19.7">la</w> <w n="19.8">source</w> <w n="19.9">claire</w>,</l>
					<l n="20" num="2.18"><w n="20.1">Le</w> <w n="20.2">jeune</w> <w n="20.3">Endymion</w>, <w n="20.4">qu</w>’<w n="20.5">a</w> <w n="20.6">surpris</w> <w n="20.7">le</w> <w n="20.8">sommeil</w> !</l>
					<l n="21" num="2.19"><w n="21.1">Latmus</w> ! <w n="21.2">tes</w> <w n="21.3">noirs</w> <w n="21.4">sommets</w> <w n="21.5">que</w> <w n="21.6">le</w> <w n="21.7">cèdre</w> <w n="21.8">domine</w>,</l>
					<l n="22" num="2.20"><w n="22.1">Tes</w> <w n="22.2">rochers</w> <w n="22.3">ont</w> <w n="22.4">frémi</w> <w n="22.5">quand</w>, <w n="22.6">belle</w> <w n="22.7">de</w> <w n="22.8">pudeur</w>,</l>
					<l n="23" num="2.21"><w n="23.1">La</w> <w n="23.2">déesse</w> <w n="23.3">des</w> <w n="23.4">nuits</w> <w n="23.5">dont</w> <w n="23.6">la</w> <w n="23.7">tête</w> <w n="23.8">s</w>’<w n="23.9">incline</w>,</l>
					<l n="24" num="2.22"><w n="24.1">Argenta</w> <w n="24.2">d</w>’<w n="24.3">un</w> <w n="24.4">baiser</w> <w n="24.5">les</w> <w n="24.6">lèvres</w> <w n="24.7">du</w> <w n="24.8">pasteur</w> !</l>
					<l n="25" num="2.23"><w n="25.1">Vierge</w> ! <w n="25.2">il</w> <w n="25.3">est</w> <w n="25.4">temps</w> <w n="25.5">d</w>’<w n="25.6">aimer</w> <w n="25.7">quand</w> <w n="25.8">on</w> <w n="25.9">est</w> <w n="25.10">jeune</w> <w n="25.11">et</w> <w n="25.12">belle</w> ;</l>
					<l n="26" num="2.24"><w n="26.1">Ne</w> <w n="26.2">sens</w>-<w n="26.3">tu</w> <w n="26.4">rien</w> <w n="26.5">bondir</w> <w n="26.6">dans</w> <w n="26.7">ta</w> <w n="26.8">poitrine</w> <w n="26.9">en</w> <w n="26.10">feu</w> ?…</l>
					<l n="27" num="2.25">— <w n="27.1">Berger</w>, <w n="27.2">dit</w> <w n="27.3">Nééra</w>, <w n="27.4">mon</w> <w n="27.5">cœur</w> <w n="27.6">n</w>’<w n="27.7">est</w> <w n="27.8">pas</w> <w n="27.9">rebelle</w>,</l>
					<l n="28" num="2.26"><w n="28.1">Et</w> <w n="28.2">j</w>’<w n="28.3">attends</w>, <w n="28.4">pour</w> <w n="28.5">faillir</w>, <w n="28.6">qu</w>’<w n="28.7">il</w> <w n="28.8">me</w> <w n="28.9">descende</w> <w n="28.10">un</w> <w n="28.11">Dieu</w> ! »</l>
				</lg>
			</div></body></text></TEI>