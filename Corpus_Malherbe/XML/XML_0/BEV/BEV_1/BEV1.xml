<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES DÉLIQUESCENCES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BEA" sort="1">
					<name>
						<forename>Henri</forename>
						<surname>BEAUCLAIR</surname>
					</name>
					<date from="1860" to="1919">1860-1919</date>
				</author>
				<author key="VIC" sort="2">
					<name>
						<forename>Gabriel</forename>
						<surname>VICAIRE</surname>
					</name>
					<date from="1848" to="1900">1848-1900</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Saisie du texte</resp>
					<name id="SP">
						<forename>S.</forename>
						<surname>Pestel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Relecture du texte</resp>
					<name id="AG">
						<forename>A.</forename>
						<surname>Guézou</surname>
					</name>
				</respStmt>				
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>297 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BEV_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Déliquescences</title>
						<author>Henri Beauclair et Gabriel Vicaire</author>
					</titleStmt>
					<publicationStmt>
						<publisher>LA BIBLIOTHÈQUE ÉLECTRONIQUE DE LISIEUX</publisher>
						<pubPlace>Lisieux</pubPlace>
						<address>
							<addrLine>Place de la République</addrLine>
							<addrLine>B.P. 27216 - 14107 Lisieux cedex</addrLine>
							<addrLine>FRANCE</addrLine>
						</address>
						<idno type="URL">http://www.bmlisieux.com/archives/deliqu01.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Déliquescences</title>
								<author>Henri Beauclair et Gabriel Vicaire</author>
								<imprint>
									<publisher>Les Editions Henri Jonquières et Cie, Paris</publisher>
									<date when="1923">1923</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1885">1885</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les balises de pagination ont été supprimées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BEV1">
				<head type="main">Les Énervés de Jumièges</head>
				<lg n="1">
					<l n="1" num="1.1">L’Horizon s’emplit</l>
					<l n="2" num="1.2">De lueurs flambantes,</l>
					<l n="3" num="1.3">Aux lignes tombantes</l>
					<l n="4" num="1.4">Comme un Ciel de Lit.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">L’Horizon s’envole,</l>
					<l n="6" num="2.2">Rose, Orange et Vert,</l>
					<l n="7" num="2.3">Comme un cœur ouvert</l>
					<l n="8" num="2.4">Qu’un relent désole.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Autour du bateau</l>
					<l n="10" num="3.2">Un remous clapote ;</l>
					<l n="11" num="3.3">La brise tapote</l>
					<l n="12" num="3.4">Son petit manteau,</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">Et, lente, très lente</l>
					<l n="14" num="4.2">En sa pâmoison,</l>
					<l n="15" num="4.3">La frêle prison</l>
					<l n="16" num="4.4">Va sur l’eau dolente.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1">O Doux énervés,</l>
					<l n="18" num="5.2">Que je vous envie</l>
					<l n="19" num="5.3">Le soupçon de vie</l>
					<l n="20" num="5.4">Que vous conservez !</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1">Pas de clameur vaine,</l>
					<l n="22" num="6.2">Pas un mouvement !</l>
					<l n="23" num="6.3">Un susurrement</l>
					<l n="24" num="6.4">Qui bruit à peine !</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1">Vous avez le flou</l>
					<l n="26" num="7.2">Des choses fanées,</l>
					<l n="27" num="7.3">Âmes très vannées,</l>
					<l n="28" num="7.4">Allant Dieu sait où !</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1">Comme sur la grève,</l>
					<l n="30" num="8.2">Le vent des remords,</l>
					<l n="31" num="8.3">Passe, en vos yeux morts,</l>
					<l n="32" num="8.4">Une fleur de rêve !</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1">Et, toujours hanté</l>
					<l n="34" num="9.2">D’un ancien Corrège,</l>
					<l n="35" num="9.3">Je dis : Quand aurai-je</l>
					<l n="36" num="9.4">Votre exquisité ?</l>
				</lg>
			</div></body></text></TEI>