<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ALCOOLS</title>
				<title type="medium">Édition électronique</title>
				<author key="APO">
					<name>
						<forename>Guillaume</forename>
						<surname>APOLLINAIRE</surname>
					</name>
					<date from="1880" to="1918">1880-1918</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2285 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">APO_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">ALCOOLS</title>
						<author>Guillaume Apollinaire</author>
					</titleStmt>
					<publicationStmt>
						<publisher>damienbe.chez.com</publisher>
						<idno type="URL">http://damienbe.chez.com/alcools.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Alcools</title>
						<author>Guillaume Apollinaire</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Gallimard</publisher>
							<date when="1920">1920</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1913">1913</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes qui présentent une trop grande diversité de longueurs métriques sans régularités ont été marqués comme inanalysable.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="APO55">
				<head type="main">Merlin et la vieille femme</head>
				<lg n="1">
					<l n="1" num="1.1">Le soleil ce jour-là s’étalait comme un ventre</l>
					<l n="2" num="1.2">Maternel qui saignait lentement sur le ciel</l>
					<l n="3" num="1.3">La lumière est ma mère ô lumière sanglante</l>
					<l n="4" num="1.4">Les nuages coulaient comme un flux menstruel</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Au carrefour où nulle fleur sinon la rose</l>
					<l n="6" num="2.2">Des vents mais sans épine n’a fleuri l’hiver</l>
					<l n="7" num="2.3">Merlin guettait la vie et l’éternelle cause</l>
					<l n="8" num="2.4">Qui fait mourir et puis renaître l’univers</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Une vieille sur une mule à chape verte</l>
					<l n="10" num="3.2">S’en vint suivant la berge du fleuve en aval</l>
					<l n="11" num="3.3">Et l’antique Merlin dans la plaine déserte</l>
					<l n="12" num="3.4">Se frappait la poitrine en s’écriant Rival</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">O mon être glacé dont le destin m’accable</l>
					<l n="14" num="4.2">Dont ce soleil de chair grelotte veux-tu voir</l>
					<l n="15" num="4.3">Ma Mémoire venir et m’aimer ma semblable</l>
					<l n="16" num="4.4">Et quel fils malheureux et beau je veux avoir</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1">Son geste fit crouler l’orgueil des cataclysmes</l>
					<l n="18" num="5.2">Le soleil en dansant remuait son nombril</l>
					<l n="19" num="5.3">Et soudain le printemps d’amour et d’héroïsme</l>
					<l n="20" num="5.4">Amena par la main un jeune jour d’avril</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1">Les voies qui viennent de l’ouest étaient couvertes</l>
					<l n="22" num="6.2">D’ossements d’herbes drues de destins et de fleurs</l>
					<l n="23" num="6.3">Des monuments tremblants près des charognes vertes</l>
					<l n="24" num="6.4">Quand les vents apportaient des poils et des malheurs</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1">Laissant sa mule à petits pas s’en vint l’amante</l>
					<l n="26" num="7.2">A petits coups le vent défripait ses atours</l>
					<l n="27" num="7.3">Puis les pâles amants joignant leurs mains démentes</l>
					<l n="28" num="7.4">L’entrelacs de leurs doigts fut leur seul laps d’amour</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1">Elle balla mimant un rythme d’existence</l>
					<l n="30" num="8.2">Criant Depuis cent ans j’espérais ton appel</l>
					<l n="31" num="8.3">Les astres de ta vie influaient sur ma danse</l>
					<l n="32" num="8.4">Morgane regardait du haut du mont Gibel</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1">Ah! qu’il fait doux danser quand pour vous se déclare</l>
					<l n="34" num="9.2">Un mirage où tout chante et que les vents d’horreur</l>
					<l n="35" num="9.3">Feignent d’être le rire de la lune hilare</l>
					<l n="36" num="9.4">Et d’effrayer les fantômes avants-coureurs</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1">J’ai fait des gestes blancs parmi les solitudes</l>
					<l n="38" num="10.2">Des lémures couraient peupler les cauchemars</l>
					<l n="39" num="10.3">Mes tournoiements exprimaient les béatitudes</l>
					<l n="40" num="10.4">Qui toutes ne sont rien qu’un pur effet de l’Art</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1">Je n’ai jamais cueilli que la fleur d’aubépine</l>
					<l n="42" num="11.2">Aux printemps finissants qui voulaient défleurir</l>
					<l n="43" num="11.3">Quand les oiseaux de proie proclamaient leurs rapines</l>
					<l n="44" num="11.4">D’agneaux mort-nés et d’enfants-dieux qui vont mourir</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1">Et j’ai vieilli vois-tu pendant ta vie je danse</l>
					<l n="46" num="12.2">Mais j’eusse été tôt lasse et l’aubépine en fleurs</l>
					<l n="47" num="12.3">Cet avril aurait eu la pauvre confidence</l>
					<l n="48" num="12.4">D’un corps de vieille morte en mimant la douleur</l>
				</lg>
				<lg n="13">
					<l n="49" num="13.1">Et leurs mains s’élevaient comme un vol de colombes</l>
					<l n="50" num="13.2">Clarté sur qui la nuit fondit comme un vautour</l>
					<l n="51" num="13.3">Puis Merlin s’en alla vers l’est disant Qu’il monte</l>
					<l n="52" num="13.4">Le fils de ma Mémoire égale de l’Amour</l>
				</lg>
				<lg n="14">
					<l n="53" num="14.1">Qu’il monte de la fange ou soit une ombre d’homme</l>
					<l n="54" num="14.2">Il sera bien mon fils mon ouvrage immortel</l>
					<l n="55" num="14.3">Le front nimbé de feu sur le chemin de Rome</l>
					<l n="56" num="14.4">Il marchera tout seul en regardant le ciel</l>
				</lg>
				<lg n="15">
					<l n="57" num="15.1">La dame qui m’attend se nomme Viviane</l>
					<l n="58" num="15.2">Et vienne le printemps des nouvelles douleurs</l>
					<l n="59" num="15.3">Couché parmi la marjolaine et les pas-d’âne</l>
					<l n="60" num="15.4">Je m’éterniserai sous l’aubépine en fleurs</l>
				</lg>
			</div></body></text></TEI>