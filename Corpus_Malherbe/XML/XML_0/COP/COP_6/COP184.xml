<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">DES VERS FRANÇAIS</title>
				<title type="medium">Édition électronique</title>
				<author key="COP">
					<name>
						<forename>François</forename>
						<surname>COPPÉE</surname>
					</name>
					<date from="1842" to="1908">1842-1908</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2263 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">COP_6</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">DES VERS FRANÇAIS</title>
						<author>François Coppée</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/francoiscoppeedesversfrancais.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Poésies complètes de François Coppée</title>
						<author>François Coppée</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre</publisher>
							<date when="1925">1925</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1906">1906</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="COP184">
				<head type="main">En écoutant l’Orgue</head>
				<lg n="1">
					<l n="1" num="1.1">A ce prêtre, voix très lointaine</l>
					<l n="2" num="1.2">Par qui l’office est célébré,</l>
					<l n="3" num="1.3">L’orgue va répondre. Il déchaîne</l>
					<l n="4" num="1.4">D’abord son tumulte sacré.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Tout à coup il s’apaise et prie,</l>
					<l n="6" num="2.2">Et la foule devant l’autel</l>
					<l n="7" num="2.3">Dans sa mystique rêverie,</l>
					<l n="8" num="2.4">Croit entendre un écho du ciel.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">De nouveau l’effort se rassemble</l>
					<l n="10" num="3.2">De tous les tuyaux de métal,</l>
					<l n="11" num="3.3">Et la cathédrale qui tremble</l>
					<l n="12" num="3.4">S’emplit d’un fracas musical.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">Instrument extraordinaire !</l>
					<l n="14" num="4.2">La nature y met tous ses bruits,</l>
					<l n="15" num="4.3">Le sourd grondement du tonnerre</l>
					<l n="16" num="4.4">Et le soupir du vent des nuits.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1">Sous le double clavier d’ivoire</l>
					<l n="18" num="5.2">S’évoque la vie aux cent voix,</l>
					<l n="19" num="5.3">Les trompettes de la Victoire</l>
					<l n="20" num="5.4">Comme l’idylle et ses hautbois.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1">Mais chut !… Le prêtre psalmodie</l>
					<l n="22" num="6.2">Devant le tabernacle d’or,</l>
					<l n="23" num="6.3">Puis se tait… De flots d’harmonie</l>
					<l n="24" num="6.4">L’orgue inonde l’église encor.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1">Et je songe en la nef obscure</l>
					<l n="26" num="7.2">Où je priais, distrait un peu :</l>
					<l n="27" num="7.3">C’est la Vie et c’est la Nature</l>
					<l n="28" num="7.4">Répondant aux ordres de Dieu !</l>
				</lg>
			</div></body></text></TEI>