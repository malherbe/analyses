<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE RELIQUAIRE</title>
				<title type="medium">Édition électronique</title>
				<author key="COP">
					<name>
						<forename>François</forename>
						<surname>COPPÉE</surname>
					</name>
					<date from="1842" to="1908">1842-1908</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>421 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2019</date>
				<idno type="local">COP_7</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE RELIQUAIRE</title>
						<author>François Coppée</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/francoiscoppeelereliquaire.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition papier d’origine.</p>
					</sourceDesc>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title type="main">Œuvres complètes</title>
								<author>François Coppée</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Librairie ALEXANDRE HOUSSIAUX</publisher>
									<date when="1885">1885</date>
								</imprint>
								<biblScope unit="tome">Poésie, tome 1</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1866">1866</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-05-25" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2019-05-25" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="COP187">
				<head type="main">PROLOGUE</head>
				<lg n="1">
					<l n="1" num="1.1"><hi rend="ital">Comme les prêtres catholiques,</hi></l>
					<l n="2" num="1.2"><hi rend="ital">Sous les rideaux de pourpre, autour</hi></l>
					<l n="3" num="1.3"><hi rend="ital">De la châsse où sont les reliques,</hi></l>
				</lg>
				<lg n="2">
					<l n="4" num="2.1"><hi rend="ital">Brillent, dans leur mystique amour,</hi></l>
					<l n="5" num="2.2"><hi rend="ital">Les longs cierges aux flammes pures,</hi></l>
					<l n="6" num="2.3"><hi rend="ital">Fauves la nuit, pâles le jour,</hi></l>
				</lg>
				<lg n="3">
					<l n="7" num="3.1"><hi rend="ital">Qui jettent des lueurs obscures</hi></l>
					<l n="8" num="3.2"><hi rend="ital">Sur les bijoux tristes et noirs</hi></l>
					<l n="9" num="3.3"><hi rend="ital">Perdus dans l’or des ciselures ;</hi></l>
				</lg>
				<lg n="4">
					<l n="10" num="4.1"><hi rend="ital">Et de même que, tous les soirs,</hi></l>
					<l n="11" num="4.2"><hi rend="ital">Ils font autour du reliquaire</hi></l>
					<l n="12" num="4.3"><hi rend="ital">Fumer les légers encensoirs ;</hi></l>
				</lg>
				<lg n="5">
					<l n="13" num="5.1"><hi rend="ital">Dédaignant la douleur vulgaire</hi></l>
					<l n="14" num="5.2"><hi rend="ital">Qui pousse des cris importuns,</hi></l>
					<l n="15" num="5.3"><hi rend="ital">Dans ces poèmes je veux faire</hi></l>
				</lg>
				<lg n="6">
					<l n="16" num="6.1"><hi rend="ital">A tous mes beaux rêves défunts,</hi></l>
					<l n="17" num="6.2"><hi rend="ital">A toutes mes chères reliques,</hi></l>
					<l n="18" num="6.3"><hi rend="ital">Une chapelle de parfums</hi></l>
				</lg>
				<lg n="7">
					<l n="19" num="7.1"><hi rend="ital">Et de cierges mélancoliques.</hi></l>
				</lg>
			</div></body></text></TEI>