<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE CAHIER ROUGE</title>
				<title type="medium">Édition électronique</title>
				<author key="COP">
					<name>
						<forename>François</forename>
						<surname>COPPÉE</surname>
					</name>
					<date from="1842" to="1908">1842-1908</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1198 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">COP_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE CAHIER ROUGE</title>
						<author>François Coppée</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/francoiscopeelecahierrouge.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Œuvres complètes de François Coppée</title>
						<author>François Coppée</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie L. Hébert</publisher>
							<date when="1885">1885</date>
						</imprint>
						<biblScope unit="tome">Poésie, tome 2</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1874">1874</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="COP42">
				<head type="main">Le Vieux Soulier</head>
				<opener>
					<salute>A Jocelyn Bargoin</salute>
				</opener>
				<lg n="1">
					<l n="1" num="1.1">EN mai, par une pure et chaude après-midi,</l>
					<l n="2" num="1.2">Je cheminais au bord du doux fleuve attiédi</l>
					<l n="3" num="1.3">Où se réfléchissait la fuite d’un nuage.</l>
					<l n="4" num="1.4">Je suivais lentement le chemin de halage</l>
					<l n="5" num="1.5">Tout en fleurs, qui descend en pente vers les eaux.</l>
					<l n="6" num="1.6">Des peupliers à droite, à gauche des roseaux ;</l>
					<l n="7" num="1.7">Devant moi, les détours de la rivière en marche</l>
					<l n="8" num="1.8">Et, fermant l’horizon, un pont d’une seule arche.</l>
					<l n="9" num="1.9">Le courant murmurait, en inclinant les joncs,</l>
					<l n="10" num="1.10">Et les poissons, avec leurs sauts et leurs plongeons,</l>
					<l n="11" num="1.11">Sans cesse le ridaient de grands cercles de moire.</l>
					<l n="12" num="1.12">Le loriot et la fauvette à tête noire</l>
					<l n="13" num="1.13">Se répondaient parmi les arbres en rideau ;</l>
					<l n="14" num="1.14">Et ces chansons des nids joyeux et ce bruit d’eau</l>
					<l n="15" num="1.15">Accompagnaient ma douce et lente flânerie.</l>
				</lg>
				<lg n="2">
					<l n="16" num="2.1">Soudain, dans le gazon de la berge fleurie,</l>
					<l n="17" num="2.2">Parmi les boutons d’or qui criblaient le chemin,</l>
					<l n="18" num="2.3">J’aperçus à mes pieds, — premier vestige humain</l>
					<l n="19" num="2.4">Que j’eusse rencontré dans ce lieu solitaire, —</l>
					<l n="20" num="2.5">Sous l’herbe et se mêlant déjà presque à la terre,</l>
					<l n="21" num="2.6">Un soulier laissé là par quelque mendiant.</l>
				</lg>
				<lg n="3">
					<l n="22" num="3.1">C’était un vieux soulier, sale, ignoble, effrayant,</l>
					<l n="23" num="3.2">Éculé du talon, bâillant de la semelle,</l>
					<l n="24" num="3.3">Laid comme la misère et sinistre comme elle,</l>
					<l n="25" num="3.4">Qui jadis fut sans doute usé par un soldat,</l>
					<l n="26" num="3.5">Puis, chez le savetier, bien qu’en piteux état,</l>
					<l n="27" num="3.6">Fut à quelque rôdeur vendu dans une échoppe ;</l>
					<l n="28" num="3.7">Un de ces vieux souliers qui font le tour d’Europe</l>
					<l n="29" num="3.8">Et qu’un jour, tout meurtri, sanglant, estropié,</l>
					<l n="30" num="3.9">Le pied ne quitte pas, mais qui quittent le pied.</l>
				</lg>
				<lg n="4">
					<l n="31" num="4.1">Quel poème navrant dans cette morne épave !</l>
					<l n="32" num="4.2">Le boulet du forçat ou le fer de l’esclave</l>
					<l n="33" num="4.3">Sont-ils plus lourds que toi, soulier du vagabond ?</l>
					<l n="34" num="4.4">Pourquoi t’a-t-on laissé sous cette arche de pont ?</l>
					<l n="35" num="4.5">L’eau doit être profonde ici ? Cette rivière</l>
					<l n="36" num="4.6">N’a-t-elle pas été mauvaise conseillère</l>
					<l n="37" num="4.7">Au voyageur si las et de si loin venu ?</l>
					<l n="38" num="4.8">Réponds ! S’en alla-t-il, en traînant son pied nu,</l>
					<l n="39" num="4.9">Mendier des sabots à la prochaine auberge ?</l>
					<l n="40" num="4.10">Ou bien, après t’avoir perdu sur cette berge,</l>
					<l n="41" num="4.11">Ce pauvre, abandonné même par ses haillons,</l>
					<l n="42" num="4.12">Est-il allé savoir au sein des tourbillons</l>
					<l n="43" num="4.13">Si l’on n’a plus besoin, quand on dort dans le fleuve,</l>
					<l n="44" num="4.14">De costume décent et de chaussure neuve ?</l>
				</lg>
				<lg n="5">
					<l n="45" num="5.1">En vain je me défends du dégoût singulier</l>
					<l n="46" num="5.2">Que j’éprouve à l’aspect de ce mauvais soulier,</l>
					<l n="47" num="5.3">Trouvé sur mon chemin, tout seul, dans la campagne.</l>
					<l n="48" num="5.4">Il est infâme, il a l’air de venir du bagne ;</l>
					<l n="49" num="5.5">Il est rouge, l’averse ayant lavé le cuir ;</l>
					<l n="50" num="5.6">Et je rêve de meurtre, et j’entends quelqu’un fuir</l>
					<l n="51" num="5.7">Loin d’un homme râlant dans une rue obscure</l>
					<l n="52" num="5.8">Et dont les clous sanglants ont broyé la figure !</l>
				</lg>
				<lg n="6">
					<l n="53" num="6.1">Abominable objet sous mes pas rencontré,</l>
					<l n="54" num="6.2">Rebut du scélérat ou du désespéré,</l>
					<l n="55" num="6.3">Tu donnes le frisson. Tout en toi me rappelle,</l>
					<l n="56" num="6.4">Devant les fleurs, devant la nature si belle,</l>
					<l n="57" num="6.5">Devant les cieux où court le doux vent aromal,</l>
					<l n="58" num="6.6">Devant le bon soleil, l’éternité du mal.</l>
					<l n="59" num="6.7">Tu me dis devant eux, triste témoin sincère,</l>
					<l n="60" num="6.8">Que le monde est rempli de vice et de misère</l>
					<l n="61" num="6.9">Et que ceux dont les pieds saignent sur les chemins,</l>
					<l n="62" num="6.10">O malheur ! sont bien près d’ensanglanter leurs mains.</l>
					<l n="63" num="6.11">— Sois maudit, instrument de crime ou de torture !</l>
					<l n="64" num="6.12">Mais qu’est-ce que cela peut faire à la nature ?</l>
					<l n="65" num="6.13">Voyez, il disparaît sous l’herbe des sillons ;</l>
					<l n="66" num="6.14">Hideux, il ne fait pas horreur aux papillons ;</l>
					<l n="67" num="6.15">La terre le reprend, il verdit sous la mousse,</l>
					<l n="68" num="6.16">Et dans le vieux soulier une fleur des champs pousse.</l>
				</lg>
			</div></body></text></TEI>