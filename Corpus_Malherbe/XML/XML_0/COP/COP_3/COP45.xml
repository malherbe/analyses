<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE CAHIER ROUGE</title>
				<title type="medium">Édition électronique</title>
				<author key="COP">
					<name>
						<forename>François</forename>
						<surname>COPPÉE</surname>
					</name>
					<date from="1842" to="1908">1842-1908</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1198 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">COP_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE CAHIER ROUGE</title>
						<author>François Coppée</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/francoiscopeelecahierrouge.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Œuvres complètes de François Coppée</title>
						<author>François Coppée</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie L. Hébert</publisher>
							<date when="1885">1885</date>
						</imprint>
						<biblScope unit="tome">Poésie, tome 2</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1874">1874</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="COP45">
				<head type="main">Fantaisie nostalgique</head>
				<opener>
					<salute>A Sully-Prudhomme</salute>
				</opener>
				<lg n="1">
					<l n="1" num="1.1">D’être ou de n’être pas je n’ai point eu le choix,</l>
					<l n="2" num="1.2">Mais, dans ce siècle vide, ennuyeux et bourgeois,</l>
					<l n="3" num="1.3">Je suis comme un enfant volé par des tziganes,</l>
					<l n="4" num="1.4">Qui chassa les oiseaux avec des sarbacanes,</l>
					<l n="5" num="1.5">Et devint saltimbanque et joueur de guzla.</l>
					<l n="6" num="1.6">Longtemps il n’a mangé que le pain qu’il vola,</l>
					<l n="7" num="1.7">Et, comme un loup, il n’eut que les bois pour repaire.</l>
					<l n="8" num="1.8">Puis, un beau jour, il est retrouvé par son père,</l>
					<l n="9" num="1.9">Un magnat, tout couvert de fourrure et d’acier,</l>
					<l n="10" num="1.10">Portant l’aigrette blanche à son bonnet princier.</l>
					<l n="11" num="1.11">Le vieil homme l’emporte en sanglotant de joie.</l>
					<l n="12" num="1.12">On habille l’enfant de velours et de soie ;</l>
					<l n="13" num="1.13">Il couche sur la plume et mange dans de l’or.</l>
					<l n="14" num="1.14">Quand il rentre au château, le nain sonne du cor,</l>
					<l n="15" num="1.15">Et, monté comme lui sur un genet d’Espagne,</l>
					<l n="16" num="1.16">Un antique écuyer balafré l’accompagne.</l>
					<l n="17" num="1.17">Un clerc, très patient, lui donne des leçons.</l>
					<l n="18" num="1.18">Son père, en son fauteuil tout chargé d’écussons,</l>
					<l n="19" num="1.19">L’attire quelquefois tendrement, puis se penche</l>
					<l n="20" num="1.20">Et longtemps le caresse avec sa barbe blanche.</l>
					<l n="21" num="1.21">Des femmes, dont les yeux sont doux comme les mains,</l>
					<l n="22" num="1.22">Baisent son front hâlé par le vent des chemins</l>
					<l n="23" num="1.23">Et détachent pour lui le bijou qui l’occupe,</l>
					<l n="24" num="1.24">Ne sachant pas qu’il sent leurs genoux sous la jupe</l>
					<l n="25" num="1.25">Et qu’au pays bohème où l’enfant voyagea,</l>
					<l n="26" num="1.26">Avant d’avoir quinze ans on est homme déjà.</l>
					<l n="27" num="1.27">Mais ni les beaux habits, ni les tables chargées</l>
					<l n="28" num="1.28">De gâteaux délicats, de fruits et de dragées,</l>
					<l n="29" num="1.29">Ni le vieil écuyer qui lui dit ses combats,</l>
					<l n="30" num="1.30">Ni les propos du clerc qui le flatte tout bas,</l>
					<l n="31" num="1.31">Ni les doux oreillers de la profonde alcôve,</l>
					<l n="32" num="1.32">Ni le palefroi blanc harnaché de cuir fauve,</l>
					<l n="33" num="1.33">Ni les jeux féminins qui font bouillir son sang,</l>
					<l n="34" num="1.34">Ni son père qui rit et pleure en l’embrassant,</l>
					<l n="35" num="1.35">Rien ne peut empêcher que son cœur ne se serre</l>
					<l n="36" num="1.36">Alors qu’il se souvient de sa libre misère.</l>
					<l n="37" num="1.37">Ah ! qu’il aimerait mieux le fruit à peine mûr</l>
					<l n="38" num="1.38">Qu’on dérobe et qu’on mange, à cheval sur un mur,</l>
					<l n="39" num="1.39">Le revers du fossé pour dormir, et la source</l>
					<l n="40" num="1.40">Pour laver ses pieds nus fatigués d’une course,</l>
					<l n="41" num="1.41">Mais du moins le plein ciel et le vaste horizon !</l>
					<l n="42" num="1.42">— Parfois, sur le rempart de sa noble prison,</l>
					<l n="43" num="1.43">On le voit, poursuivant sa chimère innocente,</l>
					<l n="44" num="1.44">Caresser de ses doigts une guitare absente</l>
					<l n="45" num="1.45">Et, les regards au ciel, le seul pays natal,</l>
					<l n="46" num="1.46">Se chanter à voix basse un air oriental.</l>
				</lg>
			</div></body></text></TEI>