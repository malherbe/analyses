<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE CAHIER ROUGE</title>
				<title type="medium">Édition électronique</title>
				<author key="COP">
					<name>
						<forename>François</forename>
						<surname>COPPÉE</surname>
					</name>
					<date from="1842" to="1908">1842-1908</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1198 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">COP_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE CAHIER ROUGE</title>
						<author>François Coppée</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/francoiscopeelecahierrouge.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Œuvres complètes de François Coppée</title>
						<author>François Coppée</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie L. Hébert</publisher>
							<date when="1885">1885</date>
						</imprint>
						<biblScope unit="tome">Poésie, tome 2</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1874">1874</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="COP72">
				<head type="main">Théophile Gautier</head>
				<head type="form">ÉLÉGIAQUE</head>
				<head type="sub_1">Pour Le Livre : <hi rend="ital">Le Tombeau De Théophile Gautier.</hi></head>
				<lg n="1">
					<l n="1" num="1.1">Maitre, l’envieux n’a pu satisfaire</l>
					<l n="2" num="1.2">Sur toi son cruel et lâche désir.</l>
					<l n="3" num="1.3">Ton nom restera pareil à la sphère,</l>
					<l n="4" num="1.4">Qui n’a pas de point par où la saisir.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Pourtant il fallait nier quelque chose</l>
					<l n="6" num="2.2">A l’œuvre parfaite où tu mis ton sceau.</l>
					<l n="7" num="2.3">Splendeur et parfum, c’est trop pour la rose ;</l>
					<l n="8" num="2.4">Ailes et chansons, c’est trop pour l’oiseau.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Ils ont dit : « Ces vers sont trop purs. Le mètre,</l>
					<l n="10" num="3.2">La rime et le style y sont sans défauts.</l>
					<l n="11" num="3.3">C’en est fait de l’art qui consiste à mettre</l>
					<l n="12" num="3.4">Une émotion sincère en vers faux. »</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">Tu leur prodiguais tes odes nouvelles</l>
					<l n="14" num="4.2">Embaumant l’avril et couleur du ciel.</l>
					<l n="15" num="4.3">Eux, ils répétaient : « Ces fleurs sont trop belles.</l>
					<l n="16" num="4.4">Tout cela doit être artificiel. »</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1">Et, poussant bien fort de longs cris d’alarmes,</l>
					<l n="18" num="5.2">Ils t’ont refusé blessure et tourments,</l>
					<l n="19" num="5.3">Parce que ton sang, parce que tes larmes</l>
					<l n="20" num="5.4">Étaient des rubis et des diamants.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1">L’artiste grandit, la critique tombe.</l>
					<l n="22" num="6.2">Mais nous, tes fervents, ô maître vainqueur,</l>
					<l n="23" num="6.3">Nous voulons écrire aux murs de ta tombe</l>
					<l n="24" num="6.4">Que ton clair génie eut aussi du cœur.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1">Nous savons le coin où se réfugie,</l>
					<l n="26" num="7.2">Sous les fleurs de pourpre et d’or enfoui,</l>
					<l n="27" num="7.3">Le discret parfum de ton élégie,</l>
					<l n="28" num="7.4">Bleu myosotis frais épanoui.</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1">Oui, nous l’envions, ce sceptre de rose</l>
					<l n="30" num="8.2">Sur un jeune sein morte un soir de bal ;</l>
					<l n="31" num="8.3">Et notre tristesse est souvent éclose</l>
					<l n="32" num="8.4">En nous rappelant l’air du carnaval.</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1">Nous avons aussi perdu notre amante ;</l>
					<l n="34" num="9.2">Nous l’avons poussé, ce soupir amer</l>
					<l n="35" num="9.3">Du pêcheur qui pleure et qui se lamente,</l>
					<l n="36" num="9.4">Seul et sans amour, d’aller sur la mer.</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1">Celle que tout bas tu nommes petite,</l>
					<l n="38" num="10.2">Celle à qui tu dis : « Le monde est méchant, »</l>
					<l n="39" num="10.3">Nous a bien prouvé, l’enfant hypocrite,</l>
					<l n="40" num="10.4">Qu’elle avait un cœur, en nous trahissant.</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1">De ses yeux d’azur la larme tombée,</l>
					<l n="42" num="11.2">Diamant du cœur par ta main serti,</l>
					<l n="43" num="11.3">Nous l’avons tous bue, à la dérobée,</l>
					<l n="44" num="11.4">Sur un billet doux qui nous a menti.</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1">Et sur les joujoux laissés par la morte,</l>
					<l n="46" num="12.2">Aujourd’hui muets et si gais jadis,</l>
					<l n="47" num="12.3">Nous prions encor pour que Dieu supporte</l>
					<l n="48" num="12.4">Le bruit des enfants dans le Paradis.</l>
				</lg>
			</div></body></text></TEI>