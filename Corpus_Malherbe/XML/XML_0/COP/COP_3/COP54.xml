<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE CAHIER ROUGE</title>
				<title type="medium">Édition électronique</title>
				<author key="COP">
					<name>
						<forename>François</forename>
						<surname>COPPÉE</surname>
					</name>
					<date from="1842" to="1908">1842-1908</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1198 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">COP_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE CAHIER ROUGE</title>
						<author>François Coppée</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/francoiscopeelecahierrouge.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Œuvres complètes de François Coppée</title>
						<author>François Coppée</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie L. Hébert</publisher>
							<date when="1885">1885</date>
						</imprint>
						<biblScope unit="tome">Poésie, tome 2</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1874">1874</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="COP54">
				<head type="main">Pour toujours !</head>
				<lg n="1">
					<l n="1" num="1.1">L’Espoir divin qu’à deux on parvient à former</l>
					<l n="2" num="1.2"><space quantity="12" unit="char"></space>Et qu’à deux on partage,</l>
					<l n="3" num="1.3">L’espoir d’aimer longtemps, d’aimer toujours, d’aimer</l>
					<l n="4" num="1.4"><space quantity="12" unit="char"></space>Chaque jour davantage ;</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Le désir éternel, chimérique et touchant,</l>
					<l n="6" num="2.2"><space quantity="12" unit="char"></space>Que les amants soupirent</l>
					<l n="7" num="2.3">A l’instant adorable où, tout en se cherchant,</l>
					<l n="8" num="2.4"><space quantity="12" unit="char"></space>Leurs lèvres se respirent ;</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Ce désir décevant, ce cher espoir trompeur,</l>
					<l n="10" num="3.2"><space quantity="12" unit="char"></space>Jamais nous n’en parlâmes ;</l>
					<l n="11" num="3.3">Et je souffre de voir que nous en ayons peur,</l>
					<l n="12" num="3.4"><space quantity="12" unit="char"></space>Bien qu’il soit dans nos âmes.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">Lorsque je te murmure, amant interrogé,</l>
					<l n="14" num="4.2"><space quantity="12" unit="char"></space><space quantity="12" unit="char"></space>Une douce réponse,</l>
					<l n="15" num="4.3">C’est le mot : « Pour toujours ! » sur les lèvres que j’ai,</l>
					<l n="16" num="4.4"><space quantity="12" unit="char"></space>Sans que je le prononce ;</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1">Et bien qu’un cher écho le dise dans ton cœur,</l>
					<l n="18" num="5.2"><space quantity="12" unit="char"></space>Ton silence est le même,</l>
					<l n="19" num="5.3">Alors que sur ton sein, en mourant de langueur,</l>
					<l n="20" num="5.4"><space quantity="12" unit="char"></space>Je jure que je t’aime.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1">Qu’importe le passé ? Qu’importe l’avenir ?</l>
					<l n="22" num="6.2"><space quantity="12" unit="char"></space>La chose la meilleure,</l>
					<l n="23" num="6.3">C’est croire que jamais elle ne doit finir,</l>
					<l n="24" num="6.4"><space quantity="12" unit="char"></space>L’illusion d’une heure.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1">Et quand je te dirai : « Pour toujours ! » ne fais rien</l>
					<l n="26" num="7.2"><space quantity="12" unit="char"></space>Qui dissipe ce songe,</l>
					<l n="27" num="7.3">Et que plus tendrement ton baiser sur le mien</l>
					<l n="28" num="7.4"><space quantity="12" unit="char"></space>S’appuie et se prolonge !</l>
				</lg>
			</div></body></text></TEI>