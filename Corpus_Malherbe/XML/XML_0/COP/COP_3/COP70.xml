<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE CAHIER ROUGE</title>
				<title type="medium">Édition électronique</title>
				<author key="COP">
					<name>
						<forename>François</forename>
						<surname>COPPÉE</surname>
					</name>
					<date from="1842" to="1908">1842-1908</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1198 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">COP_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE CAHIER ROUGE</title>
						<author>François Coppée</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/francoiscopeelecahierrouge.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Œuvres complètes de François Coppée</title>
						<author>François Coppée</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie L. Hébert</publisher>
							<date when="1885">1885</date>
						</imprint>
						<biblScope unit="tome">Poésie, tome 2</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1874">1874</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="COP70">
				<head type="main">Presque une fable</head>
				<lg n="1">
					<l n="1" num="1.1">Un liseron, madame, aimait une fauvette.</l>
					<l n="2" num="1.2">— Vous pardonnerez bien cette idée au poète</l>
					<l n="3" num="1.3">Qu’une plante puisse être éprise d’un oiseau. —</l>
					<l n="4" num="1.4">Un liseron des bois, éclos près d’un ruisseau,</l>
					<l n="5" num="1.5">Au fond du parc, au bout du vieux mur plein de brèches,</l>
					<l n="6" num="1.6">Et qui, triste, rampait parmi les feuilles sèches,</l>
					<l n="7" num="1.7">Écoutant cette voix d’oiseau dans un tilleul,</l>
					<l n="8" num="1.8">Était au désespoir de fleurir pour lui seul.</l>
					<l n="9" num="1.9">Il voulut essayer, s’il en avait la force,</l>
					<l n="10" num="1.10">D’enlacer ce grand arbre à la rugueuse écorce</l>
					<l n="11" num="1.11">Et de grimper là-haut, là-haut, près de ce nid.</l>
					<l n="12" num="1.12">Il croyait, l’innocent, que quelque chose unit</l>
					<l n="13" num="1.13">Ce qui pousse et fleurit à ce qui vole et chante.</l>
					<l n="14" num="1.14">— Moi, son ambition me semble assez touchante,</l>
					<l n="15" num="1.15">Madame. Vous savez que les amants sont fous</l>
					<l n="16" num="1.16">Et ce qu’ils tenteraient pour être auprès de vous. —</l>
					<l n="17" num="1.17">Comme le chasseur grec, pour surprendre Diane,</l>
					<l n="18" num="1.18">Suivait le son lointain du cor, l’humble liane,</l>
					<l n="19" num="1.19">De ses clochetons bleus semant le chapelet,</l>
					<l n="20" num="1.20">Monta donc vers l’oiseau que son chant décelait.</l>
					<l n="21" num="1.21">Atteindre la fauvette et la charmer, quel rêve !</l>
					<l n="22" num="1.22">Hélas ! c’était trop beau ; car la goutte de sève</l>
					<l n="23" num="1.23">Que la terre donnait à ce frêle sarment</l>
					<l n="24" num="1.24">S’épuisait. Il montait, toujours plus lentement ;</l>
					<l n="25" num="1.25">Chaque matin, sa fleur devenait plus débile ;</l>
					<l n="26" num="1.26">Puis, bien que liseron, il était malhabile,</l>
					<l n="27" num="1.27">Lui, né dans l’herbe courte où vivent les fourmis,</l>
					<l n="28" num="1.28">A gravir ces sommets aux écureuils permis.</l>
					<l n="29" num="1.29">Là, le vent est trop rude et l’ombre est trop épaisse.</l>
					<l n="30" num="1.30">— Mais tous les amoureux sont de la même espèce,</l>
					<l n="31" num="1.31">Madame, — et vers le nid d’où venait cette voix</l>
					<l n="32" num="1.32">Montait, montait toujours le liseron des bois.</l>
					<l n="33" num="1.33">Enfin, comme il touchait au but de son voyage,</l>
					<l n="34" num="1.34">Il ne put supporter la fraîcheur du feuillage</l>
					<l n="35" num="1.35">Et mourut, en donnant, le jour de son trépas,</l>
					<l n="36" num="1.36">Une dernière fleur que l’oiseau ne vit pas.</l>
					<l n="37" num="1.37">— Comment ? vous soupirez et vous baissez la tête,</l>
					<l n="38" num="1.38">Madame…Un liseron adore une fauvette.</l>
				</lg>
			</div></body></text></TEI>