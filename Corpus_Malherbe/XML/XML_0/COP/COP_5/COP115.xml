<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Sonnets intimes et Poèmes inédits</title>
				<title type="medium">Édition électronique</title>
				<author key="COP">
					<name>
						<forename>François</forename>
						<surname>COPPÉE</surname>
					</name>
					<date from="1842" to="1908">1842-1908</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1824 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">COP_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Sonnets intimes et Poèmes inédits</title>
						<author>François Coppée</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/francoiscoppesonetsintimes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Poésies complètes de François Coppée</title>
						<author>François Coppée</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre</publisher>
							<date when="1925">1925</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1925">1925</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			<change when="2017-11-30" who="RR">Les vers incomplets du refrain du poème "RONDE D’ENFANTS AUX TUILERIES" ("Les lilas passeront, etc.") ont été remplacés par la strophe du refrain complet.</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PREMIÈRE PARTIE</head><div type="poem" key="COP115">
					<head type="main">A LEURS MAJESTÉS <lb></lb>L’EMPEREUR ET L’IMPÉRATRICE DE RUSSIE</head>
					<lg n="1">
						<l n="1" num="1.1">Dans cet asile calme où le culte des lettres</l>
						<l n="2" num="1.2">Nous fut fidèlement transmis par les vieux maîtres</l>
						<l n="3" num="1.3">Ainsi que le flambeau de l’antique coureur,</l>
						<l n="4" num="1.4">A ce foyer, dans cette atmosphère sereine,</l>
						<l n="5" num="1.5">Bienvenue à la jeune et belle Souveraine !</l>
						<l n="6" num="1.6"><space quantity="8" unit="char"></space>Bienvenue au noble Empereur !</l>
					</lg>
					<lg n="2">
						<l n="7" num="2.1">Votre chère présence est partout acclamée</l>
						<l n="8" num="2.2">Par l’imposante voix du peuple et de l’armée</l>
						<l n="9" num="2.3">Émus de sentiments profonds et solennels ;</l>
						<l n="10" num="2.4">Et, sur la foule heureuse et de respect saisie,</l>
						<l n="11" num="2.5">Vous voyez les couleurs de France et de Russie</l>
						<l n="12" num="2.6"><space quantity="8" unit="char"></space>Palpiter en plis fraternels.</l>
					</lg>
					<lg n="3">
						<l n="13" num="3.1">Tous les vœux des Français vont, Sire, au fils auguste</l>
						<l n="14" num="3.2">Du magnanime Tsar, d’Alexandre le Juste ;</l>
						<l n="15" num="3.3">Car en vous son esprit pacifique est vivant.</l>
						<l n="16" num="3.4">Vous, Madame, devant vos yeux purs et sincères,</l>
						<l n="17" num="3.5">Dans les groupes charmés, vous entendez les mères</l>
						<l n="18" num="3.6"><space quantity="8" unit="char"></space>Vous bénir, vous et votre enfant.</l>
					</lg>
					<lg n="4">
						<l n="19" num="4.1">Ici s’éteint le bruit dont un peuple s’enivre.</l>
						<l n="20" num="4.2">Nous pouvons seulement vous présenter le livre</l>
						<l n="21" num="4.3">Qui garde ce trésor : la langue des aïeux ;</l>
						<l n="22" num="4.4">Mais, chez nous, c’est la France encor qui vous accueille,</l>
						<l n="23" num="4.5">Et vous lirez le mot : « amitié » sur la feuille</l>
						<l n="24" num="4.6"><space quantity="8" unit="char"></space>Qu’elle place devant vos yeux.</l>
					</lg>
					<lg n="5">
						<l n="25" num="5.1">Puis nous évoquerons notre gloire passée,</l>
						<l n="26" num="5.2">Nos devanciers fameux, princes de la pensée,</l>
						<l n="27" num="5.3">Corneille, Bossuet, tant d’autres noms si beaux,</l>
						<l n="28" num="5.4">Avec l’orgueil de voir nos souvenirs splendides</l>
						<l n="29" num="5.5">Honorés par vous, Sire, ainsi qu’aux Invalides</l>
						<l n="30" num="5.6"><space quantity="8" unit="char"></space>Vous saluez nos vieux drapeaux.</l>
					</lg>
					<lg n="6">
						<l n="31" num="6.1">Enfin, bien à regret, — l’heure si tôt s’écoule, —</l>
						<l n="32" num="6.2">Nous vous rendrons tous deux à l’amour de la foule,</l>
						<l n="33" num="6.3">Au grand Paris offrant son âme en ses clameurs,</l>
						<l n="34" num="6.4">Mais pour vous suivre aussi dans cette ardente fête</l>
						<l n="35" num="6.5">Où vous êtes portés, comme a dit un poète,</l>
						<l n="36" num="6.6"><space quantity="8" unit="char"></space>En triomphe sur tous les cœurs.</l>
					</lg>
				</div></body></text></TEI>