<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Sonnets intimes et Poèmes inédits</title>
				<title type="medium">Édition électronique</title>
				<author key="COP">
					<name>
						<forename>François</forename>
						<surname>COPPÉE</surname>
					</name>
					<date from="1842" to="1908">1842-1908</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1824 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">COP_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Sonnets intimes et Poèmes inédits</title>
						<author>François Coppée</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/francoiscoppesonetsintimes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Poésies complètes de François Coppée</title>
						<author>François Coppée</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre</publisher>
							<date when="1925">1925</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1925">1925</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			<change when="2017-11-30" who="RR">Les vers incomplets du refrain du poème "RONDE D’ENFANTS AUX TUILERIES" ("Les lilas passeront, etc.") ont été remplacés par la strophe du refrain complet.</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">DEUXIÈME PARTIE</head><div type="poem" key="COP145">
					<head type="main">A LA MÉMOIRE DE COROT</head>
					<lg n="1">
						<l n="1" num="1.1">Lorsque le printemps, cette année,</l>
						<l n="2" num="1.2">Revint sur les ailes d’avril,</l>
						<l n="3" num="1.3">La campagne fut étonnée</l>
						<l n="4" num="1.4">Et songea : « Que me manque-t-il ? »</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">En s’ouvrant, la première rose,</l>
						<l n="6" num="2.2">Quand vint s’y poser le bourdon,</l>
						<l n="7" num="2.3">Dit, triste, à l’insecte morose</l>
						<l n="8" num="2.4">« Ce mois de mai, qu’avons-nous donc ?</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">Les bleuets et les campanules</l>
						<l n="10" num="3.2">Furent moins joyeux, cette fois.</l>
						<l n="11" num="3.3">Les rossignols, aux crépuscules,</l>
						<l n="12" num="3.4">Eurent des sanglots dans la voix.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">Les aubépins que le vent frôle</l>
						<l n="14" num="4.2">Jetèrent moins gaîment leurs fleurs ;</l>
						<l n="15" num="4.3">Les bois soupirèrent ; le saule</l>
						<l n="16" num="4.4">Sembla verser bien plus de pleurs ;</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">Après une halte plus prompte,</l>
						<l n="18" num="5.2">L’oiseau s’envola des lilas.</l>
						<l n="19" num="5.3">Tout enfin semblait avoir honte</l>
						<l n="20" num="5.4">De sa joie et disait : « Hélas ! »</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1">— Hélas ! si la campagne est prise</l>
						<l n="22" num="6.2">De ce mystérieux souci,</l>
						<l n="23" num="6.3">C’est qu’un bonhomme en blouse grise</l>
						<l n="24" num="6.4">Ne revient plus, ce printemps-ci.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1">Oui, c’est sans doute qu’elle pense</l>
						<l n="26" num="7.2">Que Corot, que son vieil ami,</l>
						<l n="27" num="7.3">Pour faire une aussi longue absence,</l>
						<l n="28" num="7.4">Doit être à jamais endormi ;</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1">C’est qu’à la ville, bien loin d’elle,</l>
						<l n="30" num="8.2">Nous avons cloué ce cercueil,</l>
						<l n="31" num="8.3">Et que de son amant fidèle</l>
						<l n="32" num="8.4">La nature a droit d’être en deuil.</l>
					</lg>
					<closer>
						<dateline>
							<date when="1875">1875.</date>
						</dateline>
					</closer>
				</div></body></text></TEI>