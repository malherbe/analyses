<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Sonnets intimes et Poèmes inédits</title>
				<title type="medium">Édition électronique</title>
				<author key="COP">
					<name>
						<forename>François</forename>
						<surname>COPPÉE</surname>
					</name>
					<date from="1842" to="1908">1842-1908</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1824 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">COP_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Sonnets intimes et Poèmes inédits</title>
						<author>François Coppée</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/francoiscoppesonetsintimes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Poésies complètes de François Coppée</title>
						<author>François Coppée</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre</publisher>
							<date when="1925">1925</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1925">1925</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			<change when="2017-11-30" who="RR">Les vers incomplets du refrain du poème "RONDE D’ENFANTS AUX TUILERIES" ("Les lilas passeront, etc.") ont été remplacés par la strophe du refrain complet.</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">DEUXIÈME PARTIE</head><div type="poem" key="COP125">
					<head type="main">CHOSE VUE SUR LE GRAND CHEMIN</head>
					<lg n="1">
						<l n="1" num="1.1">Heureux l’enfant pour qui le don n’est pas l’aumône !</l>
						<l n="2" num="1.2">L’été flambe, le blé mûrit, la plaine est jaune.</l>
						<l n="3" num="1.3">Une enfant en haillons, pieds nus sur le chemin,</l>
						<l n="4" num="1.4">A cueilli des bleuets qu’elle tient à la main.</l>
						<l n="5" num="1.5">Elle a quatre ans ; on la laisse errer sur la route.</l>
						<l n="6" num="1.6">Mendiante, non pas, mais bien pauvre ; et, sans doute,</l>
						<l n="7" num="1.7">Chez elle, on est souvent sans pain à la maison.</l>
						<l n="8" num="1.8">L’enfant n’y songe pas. C’est la belle saison.</l>
						<l n="9" num="1.9">Elle est libre. Ses pieds, dans la poussière ardente,</l>
						<l n="10" num="1.10">Ont chaud. Elle a cueilli des fleurs, elle est contente,</l>
						<l n="11" num="1.11">Et, près des peupliers dont tremble le rideau,</l>
						<l part="I" n="12" num="1.12">Elle marche. </l>
					</lg>
					<lg n="2">
						<l part="F" n="12">Bercée au trot de son landau,</l>
						<l n="13" num="2.1">Une dame, à côté de sa petite fille,</l>
						<l n="14" num="2.2">Est prise de pitié pour l’errante en guenille</l>
						<l n="15" num="2.3">Et veut semer un peu de bonheur en passant.</l>
						<l part="I" n="16" num="2.4">« Halte un instant, cocher. » </l>
						<l part="F" n="16" num="2.4">La petite descend</l>
						<l n="17" num="2.5">Et présente un louis à l’enfant demi-nue.</l>
						<l n="18" num="2.6">Toutes deux ont quatre ans. L’ignorance ingénue</l>
						<l n="19" num="2.7">Qui vit dans leurs yeux clairs ne peut encor savoir</l>
						<l n="20" num="2.8">Le sens de ces deux mots — donner et recevoir —</l>
						<l n="21" num="2.9">Ni que la charité contient un peu d’offense.</l>
						<l n="22" num="2.10">O candeur adorable et sainte de l’enfance !</l>
						<l n="23" num="2.11">L’une tend sans façon le louis comme un sou,</l>
						<l n="24" num="2.12">L’autre l’accepte ainsi qu’un étrange joujou</l>
						<l n="25" num="2.13">Et le prend simplement parce qu’on le lui donne</l>
						<l n="26" num="2.14">Puis, cédant à son tour à l’instinct d’être bonne,</l>
						<l n="27" num="2.15">Elle offre ses bleuets dans la moisson cueillis.</l>
					</lg>
					<lg n="3">
						<l n="28" num="3.1">Alors la riche enfant, trouvant bien plus jolis</l>
						<l n="29" num="3.2">Que l’or ces bleuets purs comme les yeux d’un ange,</l>
						<l n="30" num="3.3">Et surprise devant le généreux échange,</l>
						<l n="31" num="3.4">Et très reconnaissante, et le cœur tout saisi,</l>
						<l n="32" num="3.5">Embrasse l’enfant pauvre en lui disant : « Merci. »</l>
					</lg>
					<closer>
						<dateline>
							<date when="1895">16 décembre 1895.</date>
						</dateline>
					</closer>
				</div></body></text></TEI>