<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÈMES MODERNES</title>
				<title type="medium">Édition électronique</title>
				<author key="COP">
					<name>
						<forename>François</forename>
						<surname>COPPÉE</surname>
					</name>
					<date from="1842" to="1908">1842-1908</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1442 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">COP_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÈMES MODERNES</title>
						<author>François Coppée</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/francoiscopeepoesiesmodernes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Œuvres complètes de François Coppée</title>
						<author>François Coppée</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie L. Hébert</publisher>
							<date when="1885">1885</date>
						</imprint>
						<biblScope unit="tome">Poésie, tome 1</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1869">1867-1869</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="COP37">
					<head type="main">L’Attente</head>
					<opener>
						<salute>À Auguste Vacquerie</salute>
					</opener>
					<lg n="1">
						<l n="1" num="1.1">Au bout du vieux canal plein de mâts, juste en face</l>
						<l n="2" num="1.2">De l’Océan et dans la dernière maison,</l>
						<l n="3" num="1.3">Assise à sa fenêtre, et quelque temps qu’il fasse,</l>
						<l n="4" num="1.4">Elle se tient, les yeux fixés sur l’horizon.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">Bien qu’elle ait la pâleur des éternels veuvages,</l>
						<l n="6" num="2.2">Sa robe est claire ; et bien que les soucis pesants</l>
						<l n="7" num="2.3">Aient sur ses traits flétris exercé leurs ravages,</l>
						<l n="8" num="2.4">Ses vêtements sont ceux des filles de seize ans.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">Car depuis bien des jours, patiente vigie,</l>
						<l n="10" num="3.2">Dès l’instant où la mer bleuit dans le matin</l>
						<l n="11" num="3.3">Jusqu’à ce qu’elle soit par le couchant rougie,</l>
						<l n="12" num="3.4">Elle est assise là, regardant au lointain.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">Chaque aurore elle voit une tardive étoile</l>
						<l n="14" num="4.2">S’éteindre, et chaque soir le soleil s’enfoncer</l>
						<l n="15" num="4.3">A cette place où doit reparaître la voile</l>
						<l n="16" num="4.4">Qu’elle vit là, jadis, pâlir et s’effacer.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">Son cœur de fiancée, immuable et fidèle,</l>
						<l n="18" num="5.2">Attend toujours, certain de l’espoir partagé,</l>
						<l n="19" num="5.3">Loyal ; et rien en elle, aussi bien qu’autour d’elle,</l>
						<l n="20" num="5.4">Depuis dix ans qu’il est parti, rien n’a changé.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1">Les quelques doux vieillards qui lui rendent visite,</l>
						<l n="22" num="6.2">En la voyant avec ses bandeaux réguliers,</l>
						<l n="23" num="6.3">Son ruban mince où pend sa médaille bénite,</l>
						<l n="24" num="6.4">Son corsage à la vierge et ses petits souliers,</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1">La croiraient une enfant ingénue et qui boude,</l>
						<l n="26" num="7.2">Si parfois ses doigts purs, ivoirins et tremblants,</l>
						<l n="27" num="7.3">Alors que sur sa main fiévreuse elle s’accoude,</l>
						<l n="28" num="7.4">Ne livraient le secret des premiers cheveux blancs.</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1">Partout le souvenir de l’absent se rencontre</l>
						<l n="30" num="8.2">En mille objets fanés et déjà presque anciens</l>
						<l n="31" num="8.3">Cette lunette en cuivre est à lui, cette montre</l>
						<l n="32" num="8.4">Est la sienne, et ces vieux instruments sont les siens.</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1">Il a laissé, de peur d’encombrer sa cabine,</l>
						<l n="34" num="9.2">Ces gros livres poudreux dans leur oubli profond,</l>
						<l n="35" num="9.3">Et c’est lui qui tua d’un coup de carabine</l>
						<l n="36" num="9.4">Le monstrueux lézard qui s’étale au plafond.</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1">Ces mille riens, décor naïf de la muraille,</l>
						<l n="38" num="10.2">Naguère, il les a tous apportés de très loin.</l>
						<l n="39" num="10.3">Seule, comme un témoin inclément et qui raille,</l>
						<l n="40" num="10.4">Une carte navale est pendue en un coin ;</l>
					</lg>
					<lg n="11">
						<l n="41" num="11.1">Sur le tableau jaunâtre, entre ses noires tringles,</l>
						<l n="42" num="11.2">Les vents et les courants se croisent à l’envi :</l>
						<l n="43" num="11.3">Et la succession des petites épingles</l>
						<l n="44" num="11.4">N’a pas marqué longtemps le voyage suivi.</l>
					</lg>
					<lg n="12">
						<l n="45" num="12.1">Elle conduit jusqu’à la ligne tropicale</l>
						<l n="46" num="12.2">Le navire vainqueur du flux et du reflux,</l>
						<l n="47" num="12.3">Puis cesse brusquement à la dernière escale,</l>
						<l n="48" num="12.4">Celle d’où le marin, hélas ! n’écrivit plus.</l>
					</lg>
					<lg n="13">
						<l n="49" num="13.1">Et ce point justement où sa trace s’arrête</l>
						<l n="50" num="13.2">Est celui qu’un burin savant fit le plus noir :</l>
						<l n="51" num="13.3">C’est l’obscur rendez-vous des flots où la tempête</l>
						<l n="52" num="13.4">Creuse un inexorable et profond entonnoir.</l>
					</lg>
					<lg n="14">
						<l n="53" num="14.1">Mais elle ne voit pas le tableau redoutable</l>
						<l n="54" num="14.2">Et feuillette, l’esprit ailleurs, du bout des doigts,</l>
						<l n="55" num="14.3">Les planches d’un herbier éparses sur la table,</l>
						<l n="56" num="14.4">Fleurs pâles qu’il cueillit aux Indes autrefois.</l>
					</lg>
					<lg n="15">
						<l n="57" num="15.1">Jusqu’au soir sa pensée extatique et sereine</l>
						<l n="58" num="15.2">Songe au chemin qu’il fait en mer pour revenir,</l>
						<l n="59" num="15.3">Ou parfois, évoquant des jours meilleurs, égrène</l>
						<l n="60" num="15.4">Le chapelet mystique et doux du souvenir ;</l>
					</lg>
					<lg n="16">
						<l n="61" num="16.1">Et, quand sur l’Océan la nuit met son mystère,</l>
						<l n="62" num="16.2">Calme et fermant les yeux, elle rêve du chant</l>
						<l n="63" num="16.3">Des matelots joyeux d’apercevoir la terre,</l>
						<l n="64" num="16.4">Et d’un navire d’or dans le soleil couchant.</l>
					</lg>
				</div></body></text></TEI>