<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÈMES MODERNES</title>
				<title type="medium">Édition électronique</title>
				<author key="COP">
					<name>
						<forename>François</forename>
						<surname>COPPÉE</surname>
					</name>
					<date from="1842" to="1908">1842-1908</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1442 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">COP_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÈMES MODERNES</title>
						<author>François Coppée</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/francoiscopeepoesiesmodernes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Œuvres complètes de François Coppée</title>
						<author>François Coppée</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie L. Hébert</publisher>
							<date when="1885">1885</date>
						</imprint>
						<biblScope unit="tome">Poésie, tome 1</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1869">1867-1869</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="COP38">
					<head type="main">Le Père</head>
					<opener>
						<salute>À Victor Azam</salute>
					</opener>
					<lg n="1">
						<l n="1" num="1.1">Il rentrait toujours ivre et battait sa maîtresse.</l>
						<l n="2" num="1.2">Deux sombres forgerons, le Vice et la Détresse,</l>
						<l n="3" num="1.3">Avaient rivé la chaîne à ces deux malheureux.</l>
						<l n="4" num="1.4">Cette femme était chez cet homme — c’est affreux ! —</l>
						<l n="5" num="1.5">Seulement par l’effroi de coucher dans la rue.</l>
						<l n="6" num="1.6">L’ivrogne la trouvait toujours aigre et bourrue</l>
						<l n="7" num="1.7">Le soir, et la frappait. Leurs cris et leurs jurons</l>
						<l n="8" num="1.8">Faisaient connaître l’heure aux gens des environs.</l>
						<l n="9" num="1.9">Puis c’était un silence effrayant dans leur chambre.</l>
						<l n="10" num="1.10">— Un jour que par l’horreur, par la faim, par décembre,</l>
						<l n="11" num="1.11">Ce couple épouvantable était plus assailli,</l>
						<l n="12" num="1.12">Il leur naquit un fils, berceau mal accueilli,</l>
						<l n="13" num="1.13">Humble front baptisé par un baiser morose,</l>
						<l n="14" num="1.14">Hélas ! et qui n’était pas moins pur ni moins rose.</l>
						<l n="15" num="1.15">L’homme revint encore ivre le lendemain,</l>
						<l n="16" num="1.16">Mais, s’arrêtant au seuil, ne leva point la main</l>
						<l n="17" num="1.17">Sur sa femme, depuis que c’était une mère.</l>
						<l n="18" num="1.18">Le regard noir de haine et la parole amère,</l>
						<l n="19" num="1.19">Celle-ci se tourna vers son horrible amant</l>
						<l n="20" num="1.20">Qui la voyait bercer son fils farouchement,</l>
						<l part="I" n="21" num="1.21">Et, raillant, lui cria : </l>
						<l part="F" n="21" num="1.21">« Frappe donc ! Qui t’arrête ?</l>
						<l n="22" num="1.22">Notre homme, j’attendais ton retour. Je suis prête.</l>
						<l n="23" num="1.23">L’hiver est-il moins dur ? le pain est-il moins cher ?</l>
						<l n="24" num="1.24">Dis ! et n’es-tu pas ivre aujourd’hui comme hier ? »</l>
						<l n="25" num="1.25">Mais le père, accablé, ne parut point l’entendre,</l>
						<l n="26" num="1.26">Et, fixant sur son fils un œil stupide et tendre,</l>
						<l n="27" num="1.27">Craintif, ainsi qu’un homme accusé se défend,</l>
						<l part="I" n="28" num="1.28">Il murmura : </l>
						<l part="F" n="28" num="1.28">« J’ai peur de réveiller l’enfant ! »</l>
					</lg>
				</div></body></text></TEI>