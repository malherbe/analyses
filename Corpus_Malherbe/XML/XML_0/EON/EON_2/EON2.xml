<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le tiers livre de Monelle</title>
				<title type="medium">Édition électronique</title>
				<author key="EON">
					<name>
						<forename>Francis</forename>
						<surname>Éon</surname>
					</name>
					<date from="1879" to="1942">1879-1942</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>72 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">EON_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le tiers livre de Monelle</title>
						<author>Francis Éon</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k3383493g</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le tiers livre de Monelle</title>
								<author>Francis Éon</author>
								<imprint>
									<pubPlace></pubPlace>
									<publisher>Le Divan</publisher>
									<date when="1944">1944</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1944">1944</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le texte a été saisi à la main. Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-03" who="RR">La note en bas de première page a été reportée en fin de poème.</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="EON2">
		<head type="main">LE TIERS LIVRE DE MONELLE<ref type="noteAnchor"><hi rend="sup">(I)</hi></ref></head>
			<div type="section" n="1">
				<head type="main">(<hi rend="ital">L’Esprit</hi>)</head>
					<lg n="1">
						<l n="1" num="1.1"><hi rend="smallcap">VERS</hi> vous une pensée a frémi comme une aile</l>
						<l n="2" num="1.2">Se rassemble, et bientôt s’élance sans retard</l>
						<l n="3" num="1.3">De l’esprit de ces bois chacune aura sa part :</l>
						<l n="4" num="1.4">La bruyère à Simone et la menthe à Monelle</l>
					</lg>
			</div>
			<div type="section" n="2">
				<head type="main">(<hi rend="ital">Le Feu</hi>)</head>
					<lg n="1">
						<l n="5" num="1.1">L’azur incandescent consume le village,</l>
						<l n="6" num="1.2">Au barrage les eaux ne sont plus que vapeurs.</l>
						<l n="7" num="1.3">Simone ! La saison en flammes reste sage,</l>
						<l n="8" num="1.4">Semblable au feu secret qui brûle dans nos cœurs.</l>
					</lg>
			</div>
			<div type="section" n="3">
				<head type="main">(<hi rend="ital">Le Cactus</hi>)</head>
					<lg n="1">
						<l n="9" num="1.1">Monelle, je vois bien que vous êtes jalouse</l>
						<l n="10" num="1.2">Du quatrain que j’ai fait pour Simone ce soir.</l>
						<l n="11" num="1.3">Voici le vôtre. Et je vous aime. Je vais voir</l>
						<l n="12" num="1.4">Si le cactus en feu fleurit sur la pelouse.</l>
					</lg>
			</div>
			<div type="section" n="4">
				<head type="main">(<hi rend="ital">La Sauge</hi>)</head>
					<lg n="1">
						<l n="13" num="1.1">Je dédie à Simone une touffe tremblante</l>
						<l n="14" num="1.2">De sauge paysanne, âme d’un pur matin.</l>
						<l n="15" num="1.3">Adressez-vous, pour les vertus de cette plante,</l>
						<l n="16" num="1.4">Au curé de Persac ou même au sacristain.</l>
					</lg>
			</div>
			<div type="section" n="5">
				<head type="main">(<hi rend="ital">L’École buissonnière</hi>)</head>
					<lg n="1">
						<l n="17" num="1.1">Monelle, je voudrais vous promener au long</l>
						<l n="18" num="1.2">Des hauts buissons mêlés de ronce et de ramures,</l>
						<l n="19" num="1.3">Et parmi les sentiers couverts de ce vallon</l>
						<l n="20" num="1.4">Baiser vos belles mains noires du sang des mûres.</l>
					</lg>
			</div>
			<div type="section" n="6">
				<head type="main">(<hi rend="ital">La Rosée</hi>)</head>
					<lg n="1">
						<l n="21" num="1.1">À présent vous voilà joliment barbouillée,</l>
						<l n="22" num="1.2">Simone qui courez dans ces chemins de chèvres !</l>
						<l n="23" num="1.3">Mais de rosée encore une mûre mouillée</l>
						<l n="24" num="1.4">Se donne, et mon baiser l’écrase sous vos lèvres.</l>
					</lg>
			</div>
			<div type="section" n="7">
				<head type="main">(<hi rend="ital">Le Jeu</hi>)</head>
					<lg n="1">
						<l n="25" num="1.1">Monelle, taisez-vous, qu’avez-vous donc à rire ?</l>
						<l n="26" num="1.2">Certes je vous entends très bien. Vous prétendez</l>
						<l n="27" num="1.3">Connaître un autre jeu meilleur encore (ou pire)</l>
						<l n="28" num="1.4">À quoi vos sentiments se sont vite accordés.</l>
					</lg>
			</div>
			<div type="section" n="8">
				<head type="main">(<hi rend="ital">Midi</hi>)</head>
					<lg n="1">
						<l n="29" num="1.1">Si le brutal excès de cette heure vous blesse,</l>
						<l n="30" num="1.2">L’ombre de mes grands buis en berceaux vous attend ;</l>
						<l n="31" num="1.3">Et là vous prouverez à votre pénitent</l>
						<l n="32" num="1.4">QU’AMOUR, Simone, est synonyme de NOBLESSE.</l>
					</lg>
			</div>
			<div type="section" n="9">
				<head type="main">(<hi rend="ital">Le Nuage</hi>)</head>
					<lg n="1">
						<l n="33" num="1.1">Monelle, vous boudez ? vous aurais-je fâchée ?</l>
						<l n="34" num="1.2">Vous faites la rétive et le mauvais esprit.</l>
						<l n="35" num="1.3">Mon Dieu ! que vous voilà lointaine, et détachée…</l>
						<l n="36" num="1.4">Mais déjà j’entrevois Simone, qui me rit.</l>
					</lg>
			</div>
			<div type="section" n="10">
				<head type="main">(<hi rend="ital">Mémoire</hi>)</head>
					<lg n="1">
						<l n="37" num="1.1">Monelle bien-allante, amour, te souviens-tu</l>
						<l n="38" num="1.2">Comme tu frissonnais de toute ta personne,</l>
						<l n="39" num="1.3">Ce dimanche de juin, sous le frêne tortu,</l>
						<l n="40" num="1.4">Au jardin où brûlaient les roses de Simone ?</l>
					</lg>
			</div>
			<div type="section" n="11">
				<head type="main">(<hi rend="ital">La Minerve</hi>)</head>
					<lg n="1">
						<l n="41" num="1.1">Je tiens de votre grâce et de votre largesse,</l>
						<l n="42" num="1.2">Et de vos mains — une Minerve de Poitiers.</l>
						<l n="43" num="1.3">C’est à vous que je dois, Simone, ma sagesse :</l>
						<l n="44" num="1.4">Vous m’avez donné mieux que vous ne promettiez.</l>
					</lg>
			</div>
			<div type="section" n="12">
				<head type="main">(<hi rend="ital">Château l’Archer</hi>)</head>
					<lg n="1">
						<l n="45" num="1.1">Cette église et ce gros donjon que vous aimez,</l>
						<l n="46" num="1.2">Mon beau rêve jaloux, Monelle, les dessine.</l>
						<l n="47" num="1.3">Je vous pense là-bas, je vois (les yeux fermés)</l>
						<l n="48" num="1.4">Une chambre, et son mur qu’embrasse une glycine.</l>
					</lg>
			</div>
			<div type="section" n="13">
				<head type="main">(<hi rend="ital">Dimanche</hi>)</head>
					<lg n="1">
						<l n="49" num="1.1">Simone, au loin de vous le dimanche se traîne</l>
						<l n="50" num="1.2">Morne sous le timbre étouffé des heures lasses…</l>
						<l n="51" num="1.3">— Soudain une pensée exacte et souveraine</l>
						<l n="52" num="1.4">Se lève, et son rayonnement brise les glaces.</l>
					</lg>
			</div>
			<div type="section" n="14">
				<head type="main">(<hi rend="ital">La bienvenue</hi>)</head>
					<lg n="1">
						<l n="53" num="1.1">Vous venez sur ma route, et c’est le bon chemin,</l>
						<l n="54" num="1.2">Monelle aux souffles chauds de septembre bercée.</l>
						<l n="55" num="1.3">C’est vous. Je le savais. Je vous baise la main.</l>
						<l n="56" num="1.4">Un choc au fond de moi vous avait annoncée.</l>
					</lg>
			</div>
			<div type="section" n="15">
				<head type="main">(<hi rend="ital">De l’ombre</hi>)</head>
					<lg n="1">
						<l n="57" num="1.1">De l’ombre nous aimons, Simone, le mystère.</l>
						<l n="58" num="1.2">Un jour trop éclatant à soi-même se nuit.</l>
						<l n="59" num="1.3">Longtemps je vous respire et vous sentez la terre</l>
						<l n="60" num="1.4">Chaude et mouillée après un orage de nuit.</l>
					</lg>
			</div>
			<div type="section" n="16">
				<head type="main">(<hi rend="ital">L’Alerte</hi>)</head>
					<lg n="1">
						<l n="61" num="1.1">Vous fléchissez, Monelle, et que le ciel est tendre,</l>
						<l n="62" num="1.2">Monelle ! et captieux sur la ville en péril.</l>
						<l n="63" num="1.3">Mais reprise déjà vous me dites : « Septembre,</l>
						<l n="64" num="1.4">« Je l’aime ! et je sais bien qu’il rime avec avril. »</l>
					</lg>
			</div>
			<div type="section" n="17">
				<head type="main">(<hi rend="ital">L’Abri</hi>)</head>
					<lg n="1">
						<l n="65" num="1.1">Mais Simone songeuse écoute un bruit lointain,</l>
						<l n="66" num="1.2">Et sans entendre plus Monelle un peu vexée,</l>
						<l n="67" num="1.3">S’avoue (et cependant sûre de son destin ! )</l>
						<l n="68" num="1.4">Dans cette cave, et d’axe en axe, désaxée.</l>
					</lg>
			</div>
			<div type="section" n="18">
				<head type="main">(<hi rend="ital">Don du Poème</hi>)</head>
					<lg n="1">
						<l n="69" num="1.1">Ainsi, pour vous, douces compagnes de mon rêve,</l>
						<l n="70" num="1.2">Filles claires ! je chante et chante sans remord.</l>
						<l n="71" num="1.3">Et trouvez dans ce chant qu’il faut bien que j’achève</l>
						<l n="72" num="1.4">La vie — enfin victorieuse de la mort.</l>
					</lg>
			</div>
				<closer>
					<dateline>
						<placeName>Champ-Noir,</placeName>
						<date when="1943">août 1943.</date> <lb></lb>
						<placeName>Poitiers,</placeName>
						<date when="1943">septembre 1943.</date>
					</dateline>
					<note type="footnote" id="(I)">Le premier <hi rend="ital">Livre de Monelle</hi> reste à Marcel Schwob. L’<hi rend="ital">Autre<lb></lb>
						Livre de Monelle</hi>, le second, a paru au <hi rend="ital">Divan</hi> de mars 1939.
					</note>
				</closer>
		</div></body></text></TEI>