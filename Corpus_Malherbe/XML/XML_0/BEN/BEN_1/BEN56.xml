<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Poésies de Benserade</title>
				<title type="medium">Une édition électronique</title>
				<author key="BEN">
					<name>
						<forename>Isaac</forename>
						<nameLink>de</nameLink>
						<surname>BENSERADE</surname>
					</name>
					<date from="1613" to="1691">1613-1691</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2994 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BEN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Poésies de Benserade</title>
						<author>Isaac de Benserade</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<date when="2020">Exporté de Wikisource le 02/08/2020</date>
						<idno type="URL">https://fr.wikisource.org/wiki/Po%C3%A9sies_de_Benserade</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies de Benserade</title>
								<author>Isaac de Benserade</author>
								<editor>Poésies publiées par Octave Uzanne</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Librairie des bibliophiles</publisher>
									<date when="1875">1875</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les œuvres</title>
						<author>Isaac de Benserade</author>
						<imprint>
							<pubPlace>Genève-Paris</pubPlace>
							<publisher>Slatkine</publisher>
							<date when="1981">1981</date>
						</imprint>
						<biblScope unit="tome">1</biblScope>
					</monogr>
					<note>Édition de référence pour les corrections métriques (fac-similé de l’édition de 1698)</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1697">1697</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur ne sont pas incluses.</p>
				<p>La justification à droite des poèmes en bouts-rimés a été remplacée par un tiret long devant le mot-rime.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<normalization>
					<p>Les faux points de suspension (séquence de 3 ou 4 points) ont été remplacés par le signe typographique approprié.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-08-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-08-08" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">STANCES, SONNETS, ÉPIGRAMMES, ETC.</head><div type="poem" key="BEN56">
					<head type="main">À Mademoiselle de Guerchy, <lb></lb>luy envoyant la copie d’une Joüissance.</head>
					<head type="form">STANCES.</head>
					<lg n="1">
						<l n="1" num="1.1"><space unit="char" quantity="8"></space>BELLE Guerchy, je vous les donne,</l>
						<l n="2" num="1.2"><space unit="char" quantity="8"></space>Ces vers que vous désirez tant ;</l>
						<l n="3" num="1.3">Ils ne sont pas fort beaux, mais pour votre personne,</l>
						<l n="4" num="1.4">Qui ne souhaiteroit d’en pouvoir faire autant ?</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><space unit="char" quantity="8"></space>Au reste, ne trouvez étrange</l>
						<l n="6" num="2.2"><space unit="char" quantity="8"></space>Mon scrupule, et gardez-vous bien</l>
						<l n="7" num="2.3">De dire que ce sont vers à vôtre louange,</l>
						<l n="8" num="2.4">Car je vous maintiendrois tout franc qu’il n’en est rien.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><space unit="char" quantity="8"></space>Et ne vous faites point de fête</l>
						<l n="10" num="3.2"><space unit="char" quantity="8"></space>En une telle occasion ;</l>
						<l n="11" num="3.3">Ce seroit faire un tour qui seroit malhonnête,</l>
						<l n="12" num="3.4">Et qui vous tourneroit à grande confusion.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><space unit="char" quantity="8"></space>Il ne faut pas, ne vous déplaise,</l>
						<l n="14" num="4.2"><space unit="char" quantity="8"></space>S’enrichir d’injustes acquêts :</l>
						<l n="15" num="4.3">L’adresse est pour une autre, et seriez-vous bien aise</l>
						<l n="16" num="4.4">Que quelqu’un en chemin détroussât vos pacquets ?</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><space unit="char" quantity="8"></space>Les biens d’autruy ne sont pas vôtres,</l>
						<l n="18" num="5.2"><space unit="char" quantity="8"></space>Mais comme on est parfois jaloux,</l>
						<l n="19" num="5.3">Je m’offre de bon cœur à vous en faire d’autres</l>
						<l n="20" num="5.4">Sur le même sujet qui seront tous pour vous.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><space unit="char" quantity="8"></space>Qu’est-ce que par vôtre prière</l>
						<l n="22" num="6.2"><space unit="char" quantity="8"></space>Ne feroit un pauvre garçon ?</l>
						<l n="23" num="6.3">Vous n’avez seulement qu’à fournir la matière,</l>
						<l n="24" num="6.4">Il vous en coûtera fort peu pour la façon.</l>
					</lg>
				</div></body></text></TEI>