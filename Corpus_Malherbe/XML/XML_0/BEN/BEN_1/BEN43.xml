<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Poésies de Benserade</title>
				<title type="medium">Une édition électronique</title>
				<author key="BEN">
					<name>
						<forename>Isaac</forename>
						<nameLink>de</nameLink>
						<surname>BENSERADE</surname>
					</name>
					<date from="1613" to="1691">1613-1691</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2994 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BEN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Poésies de Benserade</title>
						<author>Isaac de Benserade</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<date when="2020">Exporté de Wikisource le 02/08/2020</date>
						<idno type="URL">https://fr.wikisource.org/wiki/Po%C3%A9sies_de_Benserade</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies de Benserade</title>
								<author>Isaac de Benserade</author>
								<editor>Poésies publiées par Octave Uzanne</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Librairie des bibliophiles</publisher>
									<date when="1875">1875</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les œuvres</title>
						<author>Isaac de Benserade</author>
						<imprint>
							<pubPlace>Genève-Paris</pubPlace>
							<publisher>Slatkine</publisher>
							<date when="1981">1981</date>
						</imprint>
						<biblScope unit="tome">1</biblScope>
					</monogr>
					<note>Édition de référence pour les corrections métriques (fac-similé de l’édition de 1698)</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1697">1697</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur ne sont pas incluses.</p>
				<p>La justification à droite des poèmes en bouts-rimés a été remplacée par un tiret long devant le mot-rime.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<normalization>
					<p>Les faux points de suspension (séquence de 3 ou 4 points) ont été remplacés par le signe typographique approprié.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-08-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-08-08" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">STANCES, SONNETS, ÉPIGRAMMES, ETC.</head><div type="poem" key="BEN43">
					<head type="main">À Madame de Hautefort.</head>
					<head type="form">STANCES.</head>
					<lg n="1">
						<l n="1" num="1.1">D’OÙ naît sur vôtre teint cette fraîcheur nouvelle,</l>
						<l n="2" num="1.2">Qui vous fait éclater, mieux que vous n’éclatiez ?</l>
						<l n="3" num="1.3">Je vous trouve plus grasse, et vous trouve plus belle</l>
						<l n="4" num="1.4"><space unit="char" quantity="12"></space>Encor que vous n’étiez.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">Vous avez éprouvé le tracas et la peine ;</l>
						<l n="6" num="2.2">Maintenant vous goûtez un repos assez doux ;</l>
						<l n="7" num="2.3">C’en est là le sujet, vous étiez chez la reine,</l>
						<l n="8" num="2.4"><space unit="char" quantity="12"></space>Et vous êtes chez vous.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">Vôtre vie est changée, et vous en menez une</l>
						<l n="10" num="3.2">À qui dans la bassesse, un beau loisir est joint ;</l>
						<l n="11" num="3.3">Si le soin de la cour profite à la fortune,</l>
						<l n="12" num="3.4"><space unit="char" quantity="12"></space>Il nuit à l’embonpoint.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">Vous obligiez les gens d’une ardeur sans seconde,</l>
						<l n="14" num="4.2">Et, dans l’empressement dont vous parliez pour eux,</l>
						<l n="15" num="4.3">Vous travailliez, ce semble, à faire que le monde</l>
						<l n="16" num="4.4"><space unit="char" quantity="12"></space>N’eût plus de malheureux.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">C’étoit vôtre plus chère et plus noble avanture</l>
						<l n="18" num="5.2">De remplir les besoins, et combler les souhaits ;</l>
						<l n="19" num="5.3">Si ce malheur est noble, il est d’une nature</l>
						<l n="20" num="5.4"><space unit="char" quantity="12"></space>À ne finir jamais.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1">Au lieu que vous n’avez, au séjour où vous êtes,</l>
						<l n="22" num="6.2">Ni troubles dans l’esprit, ni fatigues au corps ;</l>
						<l n="23" num="6.3">Vos méditations y sont libres et nettes</l>
						<l n="24" num="6.4"><space unit="char" quantity="12"></space>De crainte et de remords.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1">On vous a renvoyée à vôtre solitude,</l>
						<l n="26" num="7.2">Comme on fit dans le tems du dernier de nos rois ;</l>
						<l n="27" num="7.3">Et ce coup de malheur vous semble aussi peu rude</l>
						<l n="28" num="7.4"><space unit="char" quantity="12"></space>Que la première fois.</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1">Sans doute la Fortune, à tout autre invincible,</l>
						<l n="30" num="8.2">Ayant différemment vôtre esprit éprouvé,</l>
						<l n="31" num="8.3">A cherché quelque endroit où vous fussiez sensible,</l>
						<l n="32" num="8.4"><space unit="char" quantity="12"></space>Et n’en a point trouvé.</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1">Sa rigueur n’a rien pû, non plus que son amorce ;</l>
						<l n="34" num="9.2">Quelque bien, quelque mal, qu’elle ait pû vous offrir,</l>
						<l n="35" num="9.3">Toûjours également, et de la même force,</l>
						<l n="36" num="9.4"><space unit="char" quantity="12"></space>Vous l’avez pû souffrir.</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1">Vôtre âme qui n’est pas de la trempe commune,</l>
						<l n="38" num="10.2">Et dont les mouvemens sont sublimes et droits,</l>
						<l n="39" num="10.3">Fait aussi peu de cas du vent de la Fortune</l>
						<l n="40" num="10.4"><space unit="char" quantity="12"></space>Que des soupirs des rois.</l>
					</lg>
					<lg n="11">
						<l n="41" num="11.1">L’endroit le plus sensible, où la douleur vous presse,</l>
						<l n="42" num="11.2">Et qui peut ébranler un courage constant,</l>
						<l n="43" num="11.3">Est de n’être plus bien auprés d’une maîtresse,</l>
						<l n="44" num="11.4"><space unit="char" quantity="12"></space>Qui vous chérissoit tant.</l>
					</lg>
					<lg n="12">
						<l n="45" num="12.1">Que ne peut contre vous dire la Renommée ?</l>
						<l n="46" num="12.2">La reine a toujours eu des sentimens si doux,</l>
						<l n="47" num="12.3">Elle a tant de bonté, vous a tant estimée ;</l>
						<l n="48" num="12.4"><space unit="char" quantity="12"></space>Et ne veut plus de vous.</l>
					</lg>
					<lg n="13">
						<l n="49" num="13.1">Son procédé n’a rien que de saint, que d’auguste ;</l>
						<l n="50" num="13.2">Un sujet sans raison n’en est pas assailly ;</l>
						<l n="51" num="13.3">Les rois n’ont jamais tort, et leur colère est juste,</l>
						<l n="52" num="13.4"><space unit="char" quantity="12"></space>Quoiqu’on n’ait pas failly.</l>
					</lg>
					<lg n="14">
						<l n="53" num="14.1">Encore que sur vous sa main s’appesantisse,</l>
						<l n="54" num="14.2">Portez avec respect ses vénérables coups,</l>
						<l n="55" num="14.3">Et demeurez d’accord qu’elle a de la justice,</l>
						<l n="56" num="14.4"><space unit="char" quantity="12"></space>Puisqu’elle a du courroux.</l>
					</lg>
					<lg n="15">
						<l n="57" num="15.1">Il faut tout espérer de sa bonté suprême,</l>
						<l n="58" num="15.2">Sinon vivre en repos loin de cette bonté,</l>
						<l n="59" num="15.3">Et vous bâtir un port dessus le rocher même</l>
						<l n="60" num="15.4"><space unit="char" quantity="12"></space>Où vous avez heurté.</l>
					</lg>
					<lg n="16">
						<l n="61" num="16.1">De là quand vous verrez, après vôtre naufrage,</l>
						<l n="62" num="16.2">Toucher à cent écueils, cent vaisseaux égarez,</l>
						<l n="63" num="16.3">Vous en aimerez mieux, à cause de l’orage,</l>
						<l n="64" num="16.4"><space unit="char" quantity="12"></space>L’endroit où vous serez.</l>
					</lg>
					<lg n="17">
						<l n="65" num="17.1">Ce grand éclat n’est pas ce que le peuple pense ;</l>
						<l n="66" num="17.2">La cour a des dégoûts, et traîne un repentir ;</l>
						<l n="67" num="17.3">Jusques là, que beaucoup ont quitté la puissance</l>
						<l n="68" num="17.4"><space unit="char" quantity="12"></space>Qui vous en fait sortir.</l>
					</lg>
					<lg n="18">
						<l n="69" num="18.1">Ainsi vous passerez des jours très-agréables</l>
						<l n="70" num="18.2">Dans un calme profond, et si délicieux,</l>
						<l n="71" num="18.3">Que même vôtre exil parmy les raisonnables,</l>
						<l n="72" num="18.4"><space unit="char" quantity="12"></space>Fera des envieux.</l>
					</lg>
					<lg n="19">
						<l n="73" num="19.1">Comme il faut bien user de l’âge qui s’écoule,</l>
						<l n="74" num="19.2">Et ménager le tems qui ne peut revenir,</l>
						<l n="75" num="19.3">Dieu de sa propre main vous tire de la foule</l>
						<l n="76" num="19.4"><space unit="char" quantity="12"></space>Pour vous entretenir.</l>
					</lg>
					<lg n="20">
						<l n="77" num="20.1">C’est ce commerce étroit, qui fait durer vos charmes,</l>
						<l n="78" num="20.2">Et les rend plus brillans, au plus fort du malheur,</l>
						<l n="79" num="20.3">Qui pique votre esprit, et luy fournit des armes</l>
						<l n="80" num="20.4"><space unit="char" quantity="12"></space>À vaincre sa douleur.</l>
					</lg>
					<lg n="21">
						<l n="81" num="21.1">Et c’est là d’où vous vient cette fraîcheur nouvelle,</l>
						<l n="82" num="21.2">Qui vous fait éclater mieux que vous n’éclatiez,</l>
						<l n="83" num="21.3">Qui rend vos yeux plus vifs, et qui vous rend plus belle</l>
						<l n="84" num="21.4"><space unit="char" quantity="12"></space>Encor plus que vous n’étiez.</l>
					</lg>
				</div></body></text></TEI>