<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Poésies de Benserade</title>
				<title type="medium">Une édition électronique</title>
				<author key="BEN">
					<name>
						<forename>Isaac</forename>
						<nameLink>de</nameLink>
						<surname>BENSERADE</surname>
					</name>
					<date from="1613" to="1691">1613-1691</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2994 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BEN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Poésies de Benserade</title>
						<author>Isaac de Benserade</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<date when="2020">Exporté de Wikisource le 02/08/2020</date>
						<idno type="URL">https://fr.wikisource.org/wiki/Po%C3%A9sies_de_Benserade</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies de Benserade</title>
								<author>Isaac de Benserade</author>
								<editor>Poésies publiées par Octave Uzanne</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Librairie des bibliophiles</publisher>
									<date when="1875">1875</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les œuvres</title>
						<author>Isaac de Benserade</author>
						<imprint>
							<pubPlace>Genève-Paris</pubPlace>
							<publisher>Slatkine</publisher>
							<date when="1981">1981</date>
						</imprint>
						<biblScope unit="tome">1</biblScope>
					</monogr>
					<note>Édition de référence pour les corrections métriques (fac-similé de l’édition de 1698)</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1697">1697</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur ne sont pas incluses.</p>
				<p>La justification à droite des poèmes en bouts-rimés a été remplacée par un tiret long devant le mot-rime.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<normalization>
					<p>Les faux points de suspension (séquence de 3 ou 4 points) ont été remplacés par le signe typographique approprié.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-08-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-08-08" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">STANCES, SONNETS, ÉPIGRAMMES, ETC.</head><div type="poem" key="BEN40">
					<head type="main">Pour les Filles de la Reine.</head>
					<head type="form">STANCES.</head>
					<lg n="1">
						<l n="1" num="1.1">BELLES, dont les regards vont dépeupler l’État,</l>
						<l n="2" num="1.2"><space unit="char" quantity="8"></space>Aprés l’avoir mis dans les chaînes,</l>
						<l n="3" num="1.3">Qui servez nôtre Reine avecque tant d’éclat,</l>
						<l n="4" num="1.4"><space unit="char" quantity="8"></space>Et que l’on sert toutes en Reines ;</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">Je suis bien glorieux que vous comptiez mes pas,</l>
						<l n="6" num="2.2"><space unit="char" quantity="8"></space>Pour mieux prendre garde à mes chûtes,</l>
						<l n="7" num="2.3">Et qu’entre vous, mon cœur augmente vos débats,</l>
						<l n="8" num="2.4"><space unit="char" quantity="8"></space>Et fasse une de vos disputes.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">L’une, assure que j’ai de l’inclination,</l>
						<l n="10" num="3.2"><space unit="char" quantity="8"></space>Et l’autre de l’indifférence ;</l>
						<l n="11" num="3.3">Ainsi l’une me plaît de sa présomption,</l>
						<l n="12" num="3.4"><space unit="char" quantity="8"></space>Et l’autre de sa deffiance.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">Je suis prêt pour vous plaire, à confirmer ces bruits ;</l>
						<l n="14" num="4.2"><space unit="char" quantity="8"></space>Oüy, j’ai des passions secrètes ;</l>
						<l n="15" num="4.3">Vous ne m’ôteriez pas de la peine où je suis,</l>
						<l n="16" num="4.4"><space unit="char" quantity="8"></space>Comme moi de celle où vous êtes.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">J’aime, et je porte un cœur sensible à tous les coups</l>
						<l n="18" num="5.2"><space unit="char" quantity="8"></space>Des beaux objets que je contemple ;</l>
						<l n="19" num="5.3">Et puis je ne vois rien, qui s’aprochant de vous,</l>
						<l n="20" num="5.4"><space unit="char" quantity="8"></space>Ne m’en favorise l’exemple.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1">Je n’entends que sanglots, pour vôtre cruauté</l>
						<l n="22" num="6.2"><space unit="char" quantity="8"></space>Qui refuse la moindre œillade,</l>
						<l n="23" num="6.3">Et parmi tant de maux, j’aurois trop de santé,</l>
						<l n="24" num="6.4"><space unit="char" quantity="8"></space>Si je n’étois un peu malade.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1">Bien mieux que l’intérêt, vos charmes à la cour</l>
						<l n="26" num="7.2"><space unit="char" quantity="8"></space>Attirent la foule importune ;</l>
						<l n="27" num="7.3">Et dans le cabinet, on tient plus à l’amour,</l>
						<l n="28" num="7.4"><space unit="char" quantity="8"></space>Qu’on ne s’attache à la fortune.</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1">On s’y plaint tout le jour, on s’y plaint tout le soir,</l>
						<l n="30" num="8.2"><space unit="char" quantity="8"></space>On y languit, on y frissonne ;</l>
						<l n="31" num="8.3">Et chacun s’y réchauffe à l’entour d’un espoir</l>
						<l n="32" num="8.4"><space unit="char" quantity="8"></space>Qui ne réussit à personne.</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1">Parmi tant de soupirs, si brûlans et si doux,</l>
						<l n="34" num="9.2"><space unit="char" quantity="8"></space>Et dont vous tenez peu de compte,</l>
						<l n="35" num="9.3">On sçait bien, qu’un soupir qui ne va point à vous,</l>
						<l n="36" num="9.4"><space unit="char" quantity="8"></space>Doit en chemin mourir de honte.</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1">Mais aussi les mieux faits, et les meilleurs esprits,</l>
						<l n="38" num="10.2"><space unit="char" quantity="8"></space>Vous ont présenté leurs franchises ;</l>
						<l n="39" num="10.3">Et moi qui les connois, de qui serois-je pris,</l>
						<l n="40" num="10.4"><space unit="char" quantity="8"></space>Puisque vous êtes toutes prises ?</l>
					</lg>
					<lg n="11">
						<l n="41" num="11.1">Ce n’est pas qu’entre nous, sans un peu de rigueur,</l>
						<l n="42" num="11.2"><space unit="char" quantity="8"></space>Ma raison ne se pût abattre,</l>
						<l n="43" num="11.3">Et si je m’en croyois, dans le fond de mon cœur,</l>
						<l n="44" num="11.4"><space unit="char" quantity="8"></space>Je me ferois tenir à quatre.</l>
					</lg>
					<lg n="12">
						<l n="45" num="12.1">Je vois du blond, du brun, qui pourroit m’attacher,</l>
						<l n="46" num="12.2"><space unit="char" quantity="8"></space>De la douceur, de l’innocence,</l>
						<l n="47" num="12.3">Du jeune, du brillant ; et même à bien chercher</l>
						<l n="48" num="12.4"><space unit="char" quantity="8"></space>J’y trouverois de la prudence.</l>
					</lg>
					<lg n="13">
						<l n="49" num="13.1">Mais j’ai trop à choisir, et je crains l’embarras</l>
						<l n="50" num="13.2"><space unit="char" quantity="8"></space>Qu’aux amans vôtre joug prépare :</l>
						<l n="51" num="13.3">Quand vous gagez que j’aime, et que je n’aime pas,</l>
						<l n="52" num="13.4"><space unit="char" quantity="8"></space>Vous voulez que je me déclare.</l>
					</lg>
					<lg n="14">
						<l n="53" num="14.1">Je mettrai, s’il vous plaît, mes vœux en autre lieu,</l>
						<l n="54" num="14.2"><space unit="char" quantity="8"></space>Et jure à vos beautez parfaites</l>
						<l n="55" num="14.3">De prendre la soutane, et rendre grâce à Dieu</l>
						<l n="56" num="14.4"><space unit="char" quantity="8"></space>Pour les grâces qu’il vous a faites.</l>
					</lg>
					<lg n="15">
						<l n="57" num="15.1">Le monde trop long-temps tint mon cœur en dépôt,</l>
						<l n="58" num="15.2"><space unit="char" quantity="8"></space>Je fuis ces dangereuses routes ;</l>
						<l n="59" num="15.3">Et j’espère d’avoir les Ordres assez-tôt,</l>
						<l n="60" num="15.4"><space unit="char" quantity="8"></space>Pour vous pouvoir marier toutes.</l>
					</lg>
				</div></body></text></TEI>