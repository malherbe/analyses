<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Poésies de Benserade</title>
				<title type="medium">Une édition électronique</title>
				<author key="BEN">
					<name>
						<forename>Isaac</forename>
						<nameLink>de</nameLink>
						<surname>BENSERADE</surname>
					</name>
					<date from="1613" to="1691">1613-1691</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2994 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BEN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Poésies de Benserade</title>
						<author>Isaac de Benserade</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<date when="2020">Exporté de Wikisource le 02/08/2020</date>
						<idno type="URL">https://fr.wikisource.org/wiki/Po%C3%A9sies_de_Benserade</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies de Benserade</title>
								<author>Isaac de Benserade</author>
								<editor>Poésies publiées par Octave Uzanne</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Librairie des bibliophiles</publisher>
									<date when="1875">1875</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les œuvres</title>
						<author>Isaac de Benserade</author>
						<imprint>
							<pubPlace>Genève-Paris</pubPlace>
							<publisher>Slatkine</publisher>
							<date when="1981">1981</date>
						</imprint>
						<biblScope unit="tome">1</biblScope>
					</monogr>
					<note>Édition de référence pour les corrections métriques (fac-similé de l’édition de 1698)</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1697">1697</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur ne sont pas incluses.</p>
				<p>La justification à droite des poèmes en bouts-rimés a été remplacée par un tiret long devant le mot-rime.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<normalization>
					<p>Les faux points de suspension (séquence de 3 ou 4 points) ont été remplacés par le signe typographique approprié.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-08-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-08-08" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">STANCES, SONNETS, ÉPIGRAMMES, ETC.</head><div type="poem" key="BEN26">
					<head type="main">Jalousie.</head>
					<head type="form">STANCES.</head>
					<lg n="1">
						<l n="1" num="1.1">J’AVOIS la fièvre ardente, et, comme en frénésie,</l>
						<l n="2" num="1.2">Dedans mon triste lit j’en sentois les assauts ;</l>
						<l n="3" num="1.3"><space unit="char" quantity="8"></space>Cependant une jalousie</l>
						<l n="4" num="1.4"><space unit="char" quantity="8"></space>Étoit le plus grand de mes maux.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">Un rival prend son temps, choisit son avantage,</l>
						<l n="6" num="2.2">Et vient voir la beauté qui cause mon ennuy ;</l>
						<l n="7" num="2.3"><space unit="char" quantity="8"></space>Il est sot et me fait ombrage,</l>
						<l n="8" num="2.4"><space unit="char" quantity="8"></space>Car elle est sotte comme luy.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">Bien mieux que ses discours mon mal la persuade ;</l>
						<l n="10" num="3.2">Et, si je perds le fruit qui devoit être mien,</l>
						<l n="11" num="3.3"><space unit="char" quantity="8"></space>C’est parce que je suis malade,</l>
						<l n="12" num="3.4"><space unit="char" quantity="8"></space>Et que l’autre se porte bien.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">Elle ne fit jamais de si grossière faute ;</l>
						<l n="14" num="4.2">Cet esprit, qui ne peut former un bon dessein,</l>
						<l n="15" num="4.3"><space unit="char" quantity="8"></space>Croit qu’un badin qui danse et saute</l>
						<l n="16" num="4.4"><space unit="char" quantity="8"></space>Vaut un honnête homme mal sain.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">Elle vient à mon lit, elle me plaint sans cesse,</l>
						<l n="18" num="5.2">Et voudroit, me voyant de tous mes sens perclus,</l>
						<l n="19" num="5.3"><space unit="char" quantity="8"></space>Me faire passer pour tristesse</l>
						<l n="20" num="5.4"><space unit="char" quantity="8"></space>Son désordre et ses yeux battus.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1">Pour mieux dissimuler, elle en veut à ses charmes,</l>
						<l n="22" num="6.2">Et cependant, au point qu’elle pleure mon mal,</l>
						<l n="23" num="6.3"><space unit="char" quantity="8"></space>Je lis dans ses yeux tout en larmes.</l>
						<l n="24" num="6.4"><space unit="char" quantity="8"></space>Un rendez-vous à mon rival.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1">Cette affectation au dernier point me blesse ;</l>
						<l n="26" num="7.2">Et lors, si je pouvois, étant bien amoureux,</l>
						<l n="27" num="7.3"><space unit="char" quantity="8"></space>Faire vertu de ma foiblesse.</l>
						<l n="28" num="7.4"><space unit="char" quantity="8"></space>Combien je serois généreux.</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1">Mais le Ciel, dont je suy la fatale ordonnance,</l>
						<l n="30" num="8.2">Luy qui ne les veut pas obliger à demy,</l>
						<l n="31" num="8.3"><space unit="char" quantity="8"></space>Veut encor que mon impuissance</l>
						<l n="32" num="8.4"><space unit="char" quantity="8"></space>S’entende avec mon ennemy.</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1">Tout le monde est aux champs, il est seul avec elle,</l>
						<l n="34" num="9.2">Et peut bien triompher de sa jeune pudeur,</l>
						<l n="35" num="9.3"><space unit="char" quantity="8"></space>S’il brûle autant pour l’infidelle</l>
						<l n="36" num="9.4"><space unit="char" quantity="8"></space>Qu’elle ressent pour luy d’ardeur.</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1">Je ne le puis nier, ce fut avec justice</l>
						<l n="38" num="10.2">Qu’elle eût pitié de moy quand il en fut saison ;</l>
						<l n="39" num="10.3"><space unit="char" quantity="8"></space>Mais elle peut bien par caprice</l>
						<l n="40" num="10.4"><space unit="char" quantity="8"></space>Ce qu’elle fit lors par raison.</l>
					</lg>
					<lg n="11">
						<l n="41" num="11.1">Hélas ! il me souvient qu’au fort de mon martyre,</l>
						<l n="42" num="11.2">À pas lens et craintifs, dans l’ombre de la nuit,</l>
						<l n="43" num="11.3"><space unit="char" quantity="8"></space>J’allois afin de le luy dire</l>
						<l n="44" num="11.4"><space unit="char" quantity="8"></space>Sans faire scandale ni bruit.</l>
					</lg>
					<lg n="12">
						<l n="45" num="12.1">Mais, bien que d’elle-même un autre ait la victoire,</l>
						<l n="46" num="12.2">Qu’elle perde à son gré la honte et la pudeur,</l>
						<l n="47" num="12.3"><space unit="char" quantity="8"></space>Pourveu qu’elle n’ait pas la gloire</l>
						<l n="48" num="12.4"><space unit="char" quantity="8"></space>De nous faire perdre le cœur.</l>
					</lg>
					<lg n="13">
						<l n="49" num="13.1">Quoique je me courrouce et que je me dépite,</l>
						<l n="50" num="13.2">Me désespérer tant et me plaindre si haut,</l>
						<l n="51" num="13.3"><space unit="char" quantity="8"></space>Ce n’est que prêcher son mérite</l>
						<l n="52" num="13.4"><space unit="char" quantity="8"></space>Et que publier mon défaut.</l>
					</lg>
					<lg n="14">
						<l n="53" num="14.1">Il faut bien qu’à son rang tout le monde l’adore ;</l>
						<l n="54" num="14.2">Pas un de cet honneur ne doit être privé,</l>
						<l n="55" num="14.3"><space unit="char" quantity="8"></space>Et j’ay tort d’y prétendre encore,</l>
						<l n="56" num="14.4"><space unit="char" quantity="8"></space>Puisque mon règne est achevé.</l>
					</lg>
				</div></body></text></TEI>