<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Poésies de Benserade</title>
				<title type="medium">Une édition électronique</title>
				<author key="BEN">
					<name>
						<forename>Isaac</forename>
						<nameLink>de</nameLink>
						<surname>BENSERADE</surname>
					</name>
					<date from="1613" to="1691">1613-1691</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2994 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BEN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Poésies de Benserade</title>
						<author>Isaac de Benserade</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<date when="2020">Exporté de Wikisource le 02/08/2020</date>
						<idno type="URL">https://fr.wikisource.org/wiki/Po%C3%A9sies_de_Benserade</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies de Benserade</title>
								<author>Isaac de Benserade</author>
								<editor>Poésies publiées par Octave Uzanne</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Librairie des bibliophiles</publisher>
									<date when="1875">1875</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les œuvres</title>
						<author>Isaac de Benserade</author>
						<imprint>
							<pubPlace>Genève-Paris</pubPlace>
							<publisher>Slatkine</publisher>
							<date when="1981">1981</date>
						</imprint>
						<biblScope unit="tome">1</biblScope>
					</monogr>
					<note>Édition de référence pour les corrections métriques (fac-similé de l’édition de 1698)</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1697">1697</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur ne sont pas incluses.</p>
				<p>La justification à droite des poèmes en bouts-rimés a été remplacée par un tiret long devant le mot-rime.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<normalization>
					<p>Les faux points de suspension (séquence de 3 ou 4 points) ont été remplacés par le signe typographique approprié.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-08-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-08-08" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">STANCES, SONNETS, ÉPIGRAMMES, ETC.</head><div type="poem" key="BEN90">
					<head type="form">STANCES.</head>
					<head type="main">L’Amant indifférent.</head>
					<lg n="1">
						<l n="1" num="1.1">NON, je ne monte point à ce point d’insolence,</l>
						<l n="2" num="1.2">Que d’oser sans raison, d’un courage effronté,</l>
						<l n="3" num="1.3">Soûtenir de mes mœurs la trop grande licence,</l>
						<l n="4" num="1.4"><space unit="char" quantity="12"></space>Et la lasciveté.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">Je ne les cèle plus, mes vœux illégitimes,</l>
						<l n="6" num="2.2">Si la confession amoindrit le péché ;</l>
						<l n="7" num="2.3">Insensé que je suis, je découvre des crimes</l>
						<l n="8" num="2.4"><space unit="char" quantity="12"></space>Que tout autre eût caché.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">Quoique je les haïsse, et que je les déteste,</l>
						<l n="10" num="3.2">Mon âme encor pour eux a des désirs puissans :</l>
						<l n="11" num="3.3">C’est un subtil venin, c’est une douce peste</l>
						<l n="12" num="3.4"><space unit="char" quantity="12"></space>Qui veut charmer mes sens.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">Ah ! qu’il est difficile, et que l’on a de peine</l>
						<l n="14" num="4.2">À supporter un faix qu’on voudroit décharger !</l>
						<l n="15" num="4.3">Je nourris dans le cœur une espérance vaine</l>
						<l n="16" num="4.4"><space unit="char" quantity="12"></space>De m’en voir dégager.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">Je combats vainement, ma passion trop forte</l>
						<l n="18" num="5.2">A réduit ma raison à ne plus résister,</l>
						<l n="19" num="5.3">Et ressemble au vaisseau que le courant emporte</l>
						<l n="20" num="5.4"><space unit="char" quantity="12"></space>Sans pouvoir s’arrêter.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1">Il n’est point de beauté ni de grâce certaine</l>
						<l n="22" num="6.2">Que je puisse nommer l’objet de mes amours ;</l>
						<l n="23" num="6.3">Toutes sortes d’appas, me causant de la peine,</l>
						<l n="24" num="6.4"><space unit="char" quantity="12"></space>Font que j’aime toûjours.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1">Si quelqu’objet sur moy jette des yeux modestes,</l>
						<l n="26" num="7.2">Sa pudeur me ravit, il devient mon vainqueur,</l>
						<l n="27" num="7.3">Et de si doux regards sont les flâmes funestes</l>
						<l n="28" num="7.4"><space unit="char" quantity="12"></space>Qui m’embrasent le cœur.</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1">Si je trouve au contraire une Dame affétée,</l>
						<l n="30" num="8.2">Son langage un peu libre a pour moy des appas,</l>
						<l n="31" num="8.3">Et la place me plaît, qui peut être emportée</l>
						<l n="32" num="8.4"><space unit="char" quantity="12"></space>Sans beaucoup de combats.</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1">Si j’en vois par hazard quelqu’une dédaigneuse,</l>
						<l n="34" num="9.2">D’une antique Sabine imitant le parler,</l>
						<l n="35" num="9.3">Je croy qu’elle le veut, mais qu’une âme orgueilleuse</l>
						<l n="36" num="9.4"><space unit="char" quantity="12"></space>La fait dissimuler.</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1">Une docte me plaît, j’y trouve mille charmes,</l>
						<l n="38" num="10.2">J’adore incontinent ses rares qualitez :</l>
						<l n="39" num="10.3">Une innocente encor me fait rendre les armes</l>
						<l n="40" num="10.4"><space unit="char" quantity="12"></space>Par ses simplicitez.</l>
					</lg>
					<lg n="11">
						<l n="41" num="11.1">S’il est quelque beauté si fort passionnée,</l>
						<l n="42" num="11.2">Que de n’estimer rien que les vers que je fais,</l>
						<l n="43" num="11.3">Aussi-tost sous ses loix mon âme est enchaînée,</l>
						<l n="44" num="11.4"><space unit="char" quantity="12"></space>Et j’aime à qui je plais.</l>
					</lg>
					<lg n="12">
						<l n="45" num="12.1">S’il s’en rencontre aussi dont l’humeur plus sévère,</l>
						<l n="46" num="12.2">Blâme des vers qu’une autre aura trouvez charmans,</l>
						<l n="47" num="12.3">Je voudrois me venger d’un si doux adversaire</l>
						<l n="48" num="12.4"><space unit="char" quantity="12"></space>Par mille embrassemens.</l>
					</lg>
					<lg n="13">
						<l n="49" num="13.1">L’une en se promenant chemine avec molesse,</l>
						<l n="50" num="13.2">Et d’un pied négligent elle forme ses pas ;</l>
						<l n="51" num="13.3">Son mouvement me plaît, et sa feinte paresse</l>
						<l n="52" num="13.4"><space unit="char" quantity="12"></space>A pour moy des appas.</l>
					</lg>
					<lg n="14">
						<l n="53" num="14.1">L’autre d’un fier regard paroît inexorable,</l>
						<l n="54" num="14.2">Et d’un pas mesuré marche superbement ;</l>
						<l n="55" num="14.3">Mais elle pourra bien devenir plus traitable</l>
						<l n="56" num="14.4"><space unit="char" quantity="12"></space>Dans les bras d’un Amant.</l>
					</lg>
					<lg n="15">
						<l n="57" num="15.1">Parce que celle-là, si l’on n’est une souche,</l>
						<l n="58" num="15.2">Avec sa douce voix nous sçait si bien charmer,</l>
						<l n="59" num="15.3">Je voudrois dérober des baisers à sa bouche</l>
						<l n="60" num="15.4"><space unit="char" quantity="12"></space>Sans la faire fermer.</l>
					</lg>
					<lg n="16">
						<l n="61" num="16.1">Cette autre icy fait plaindre en cent façons nouvelles</l>
						<l n="62" num="16.2">Les cordes que ses doigts sçavent si bien toucher ;</l>
						<l n="63" num="16.3">Pour n’aimer pas des mains si doctes et si belles</l>
						<l n="64" num="16.4"><space unit="char" quantity="12"></space>Il faut être un rocher.</l>
					</lg>
					<lg n="17">
						<l n="65" num="17.1">Celle-cy me ravit par un geste agréable,</l>
						<l n="66" num="17.2">Qui fait suivre à ses bras la mesure des sons,</l>
						<l n="67" num="17.3">Et fait tourner le corps d’un art émerveillable</l>
						<l n="68" num="17.4"><space unit="char" quantity="12"></space>En cent doctes façons.</l>
					</lg>
					<lg n="18">
						<l n="69" num="18.1">Toy, qui peux égaler par ta haute stature</l>
						<l n="70" num="18.2">Ces femmes de Héros que l’Antiquité vit,</l>
						<l n="71" num="18.3">Tu peux par ces faveurs que t’a fait la Nature</l>
						<l n="72" num="18.4"><space unit="char" quantity="12"></space>Occuper tout un lit.</l>
					</lg>
					<lg n="19">
						<l n="73" num="19.1">L’autre, quoiqu’un peu courte, a pourtant du mérite,</l>
						<l n="74" num="19.2">Elle en est plus subtile au lascif mouvement ;</l>
						<l n="75" num="19.3">L’une et l’autre me plaît, la grande et la petite</l>
						<l n="76" num="19.4"><space unit="char" quantity="12"></space>Me rendent leur Amant.</l>
					</lg>
					<lg n="20">
						<l n="77" num="20.1">Elle n’a mis nul soin à laver son visage,</l>
						<l n="78" num="20.2">On ne remarque en elle aucun ajustement :</l>
						<l n="79" num="20.3">Aussi-tost mon esprit songe quel avantage</l>
						<l n="80" num="20.4"><space unit="char" quantity="12"></space>Donne cet ornement.</l>
					</lg>
					<lg n="21">
						<l n="81" num="21.1">Cette autre plus parée a consulté sa glace,</l>
						<l n="82" num="21.2">Pour donner à ses yeux de nouvelles clartez :</l>
						<l n="83" num="21.3">N’est-ce pas un grand point de sçavoir avec grâce</l>
						<l n="84" num="21.4"><space unit="char" quantity="12"></space>Étaler ses beautez ?</l>
					</lg>
					<lg n="22">
						<l n="85" num="22.1">La Blanche me remplit d’une ardeur non-commune,</l>
						<l n="86" num="22.2">Pour la Blonde aussi-tost je mets les armes bas,</l>
						<l n="87" num="22.3">Et mes yeux mêmement dedans la couleur brune</l>
						<l n="88" num="22.4"><space unit="char" quantity="12"></space>Rencontrent des appas.</l>
					</lg>
					<lg n="23">
						<l n="89" num="23.1">Des cheveux noirs pendans dessus un col d’ivoire</l>
						<l n="90" num="23.2">Ont des attraits puissans qui peuvent tout charmer,</l>
						<l n="91" num="23.3">Et ce fut de Léda la chevelure noire</l>
						<l n="92" num="23.4"><space unit="char" quantity="12"></space>Qui la fit tant aimer.</l>
					</lg>
					<lg n="24">
						<l n="93" num="24.1">S’ils sont dorez, l’Aurore en avoit de semblables,</l>
						<l n="94" num="24.2">C’est par ses seuls rayons qu’on la voit éclater ;</l>
						<l n="95" num="24.3">Enfin je m’accommode à tout autant de fables</l>
						<l n="96" num="24.4"><space unit="char" quantity="12"></space>Qu’on en peut inventer.</l>
					</lg>
					<lg n="25">
						<l n="97" num="25.1">Dessus moy, la jeunesse a beaucoup de puissance ;</l>
						<l n="98" num="25.2">Un âge un peu plus meur a dequoy m’enflâmer :</l>
						<l n="99" num="25.3">L’une par sa beauté, l’autre par sa prudence,</l>
						<l n="100" num="25.4"><space unit="char" quantity="12"></space>Me peut aussi charmer.</l>
					</lg>
					<lg n="26">
						<l n="101" num="26.1">Enfin, s’il est encor quelques beautez nouvelles,</l>
						<l n="102" num="26.2">Qui puissent dans les cœurs porter la passion,</l>
						<l n="103" num="26.3">Mon amour y prétend, et pour toutes les Belles</l>
						<l n="104" num="26.4"><space unit="char" quantity="12"></space>J’ay de l’ambition.</l>
					</lg>
				</div></body></text></TEI>