<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Poésies de Benserade</title>
				<title type="medium">Une édition électronique</title>
				<author key="BEN">
					<name>
						<forename>Isaac</forename>
						<nameLink>de</nameLink>
						<surname>BENSERADE</surname>
					</name>
					<date from="1613" to="1691">1613-1691</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2994 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BEN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Poésies de Benserade</title>
						<author>Isaac de Benserade</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<date when="2020">Exporté de Wikisource le 02/08/2020</date>
						<idno type="URL">https://fr.wikisource.org/wiki/Po%C3%A9sies_de_Benserade</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies de Benserade</title>
								<author>Isaac de Benserade</author>
								<editor>Poésies publiées par Octave Uzanne</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Librairie des bibliophiles</publisher>
									<date when="1875">1875</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les œuvres</title>
						<author>Isaac de Benserade</author>
						<imprint>
							<pubPlace>Genève-Paris</pubPlace>
							<publisher>Slatkine</publisher>
							<date when="1981">1981</date>
						</imprint>
						<biblScope unit="tome">1</biblScope>
					</monogr>
					<note>Édition de référence pour les corrections métriques (fac-similé de l’édition de 1698)</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1697">1697</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur ne sont pas incluses.</p>
				<p>La justification à droite des poèmes en bouts-rimés a été remplacée par un tiret long devant le mot-rime.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<normalization>
					<p>Les faux points de suspension (séquence de 3 ou 4 points) ont été remplacés par le signe typographique approprié.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-08-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-08-08" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">STANCES, SONNETS, ÉPIGRAMMES, ETC.</head><div type="poem" key="BEN86">
					<head type="main">Sur une nouvelle affection, <lb></lb>aprés la mort d’une Maîtresse.</head>
					<head type="form">STANCES.</head>
					<lg n="1">
						<l n="1" num="1.1">DE qui me plaindrai-je en ce jour,</l>
						<l n="2" num="1.2">Ou de la mort ou de l’amour,</l>
						<l n="3" num="1.3">Qui tous deux traversent ma vie,</l>
						<l n="4" num="1.4">Si les Astres infortunez</l>
						<l n="5" num="1.5">Veulent qu’au trépas de Silvie</l>
						<l n="6" num="1.6">Tous mes maux ne soient pas bornez ?</l>
					</lg>
					<lg n="2">
						<l n="7" num="2.1">Mort aux plaisirs, vif aux douleurs,</l>
						<l n="8" num="2.2">Je croyois dans l’eau de mes pleurs</l>
						<l n="9" num="2.3">Éteindre ma vie et ma flâme ;</l>
						<l n="10" num="2.4">Quand la beauté qui m’asservit,</l>
						<l n="11" num="2.5">D’un regard me rendit mon âme,</l>
						<l n="12" num="2.6">Et de l’autre me la ravit.</l>
					</lg>
					<lg n="3">
						<l n="13" num="3.1">Bel œil jadis si plein d’appas,</l>
						<l n="14" num="3.2">Qui dors en la nuit du trépas</l>
						<l n="15" num="3.3">Sur les bords du rivage sombre,</l>
						<l n="16" num="3.4">Ne trouble pas ton doux sommeil,</l>
						<l n="17" num="3.5">Si l’amour veut qu’au lieu d’une ombre</l>
						<l n="18" num="3.6">Désormais j’adore un soleil.</l>
					</lg>
					<lg n="4">
						<l n="19" num="4.1">Je crus que perdant ton flambeau,</l>
						<l n="20" num="4.2">Mon cœur amoureux du tombeau</l>
						<l n="21" num="4.3">N’auroit des feux que de ta cendre ;</l>
						<l n="22" num="4.4">Et que cette noire maison</l>
						<l n="23" num="4.5">Où la Parque t’a fait descendre</l>
						<l n="24" num="4.6">Seroit mon unique prison.</l>
					</lg>
					<lg n="5">
						<l n="25" num="5.1">Mais un seul rayon de ces yeux</l>
						<l n="26" num="5.2">Qui troublent la gloire des Dieux,</l>
						<l n="27" num="5.3">M’ôta le titre d’invincible ;</l>
						<l n="28" num="5.4">J’accrus ma honte en résistant,</l>
						<l n="29" num="5.5">Et pour n’être pas insensible</l>
						<l n="30" num="5.6">Il me falloit être inconstant.</l>
					</lg>
					<lg n="6">
						<l n="31" num="6.1">Un trait de feu, m’ouvrant le sein,</l>
						<l n="32" num="6.2">Changea mon fidèle dessein,</l>
						<l n="33" num="6.3">Ma raison se trouva ravie ;</l>
						<l n="34" num="6.4">Je fus surpris de sa clarté,</l>
						<l n="35" num="6.5">Et contraint pour sauver ma vie,</l>
						<l n="36" num="6.6">D’abandonner ma liberté.</l>
					</lg>
					<lg n="7">
						<l n="37" num="7.1">Source divine de mes feux,</l>
						<l n="38" num="7.2">Souffre l’hommage de mes vœux,</l>
						<l n="39" num="7.3">De mes soûpirs et de mes larmes ;</l>
						<l n="40" num="7.4">Reçois mon âme sous ta loy,</l>
						<l n="41" num="7.5">Et permets que j’offre à tes charmes</l>
						<l n="42" num="7.6">Ce qui déjà n’est plus à moy.</l>
					</lg>
					<lg n="8">
						<l n="43" num="8.1">Permets qu’un misérable amant</l>
						<l n="44" num="8.2">Puisse être jusqu’au monument</l>
						<l n="45" num="8.3">Tributaire de ta couronne,</l>
						<l n="46" num="8.4">Et traite ce cœur qui se rend</l>
						<l n="47" num="8.5">Comme une place qui se donne,</l>
						<l n="48" num="8.6">Et non comme une qui se prend.</l>
					</lg>
				</div></body></text></TEI>