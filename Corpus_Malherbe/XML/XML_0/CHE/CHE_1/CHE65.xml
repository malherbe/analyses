<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ŒUVRES POÉTIQUES</title>
				<title type="sub_2">tome I</title>
				<title type="medium">Édition électronique</title>
				<author key="CHE">
					<name>
						<forename>André</forename>
						<nameLink>de</nameLink>
						<surname>CHÉNIER</surname>
					</name>
					<date from="1762" to="1794">1762-1794</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>5334 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2011">2011</date>
				<idno type="local">CHE_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">ŒUVRES POÉTIQUES</title>
						<title type="sub">tome I</title>
						<author>André de Chénier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k5457953r.r=Oeuvres%20po%C3%A9tiques%20de%20Andr%C3%A9%20de%20Ch%C3%A9nier</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>ŒUVRES POÉTIQUES</title>
								<author>André de Chénier</author>
								<editor>Avec une notice et des notes de Raoul Guillard</editor>
								<imprint>
									<publisher>Alphonse Lemerre, éditeur</publisher>
									<date when="1899">1899</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1790">1790</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Tomaison et mise en forme conformes à l’édition de 1856. La préface n’est pas reprise</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-01" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ÉGLOGUES</head><div type="poem" key="CHE65">
					<head type="number">XXXI</head>
					<head type="main">Épilogue</head>
					<lg n="1">
						<l n="1" num="1.1">Ma muse pastorale aux regards des Français</l>
						<l n="2" num="1.2">Osait ne point rougir d’habiter les forêts.</l>
						<l n="3" num="1.3">Elle eût voulu montrer aux belles de nos villes</l>
						<l n="4" num="1.4">La champêtre innocence et les plaisirs tranquilles ;</l>
						<l n="5" num="1.5">Et ramenant Palès des climats étrangers,</l>
						<l n="6" num="1.6">Faire entendre à la Seine enfin de vrais bergers.</l>
						<l n="7" num="1.7">Elle a vu, me suivant dans mes courses rustiques,</l>
						<l n="8" num="1.8">Tous les lieux illustrés par des chants bucoliques.</l>
						<l n="9" num="1.9">Ses pas de l’Arcadie ont visité les bois,</l>
						<l n="10" num="1.10">Et ceux du Mincius, que Virgile autrefois</l>
						<l n="11" num="1.11">Vit à ses doux accents incliner leur feuillage ;</l>
						<l n="12" num="1.12">Et d’Hermus aux flots d’or l’harmonieux rivage,</l>
						<l n="13" num="1.13">Où Bion, de Vénus répétant les douleurs</l>
						<l n="14" num="1.14">Du beau sang d’Adonis a fait naître des fleurs,</l>
						<l n="15" num="1.15">Vous, Aréthuse aussi, que de toute fontaine</l>
						<l n="16" num="1.16">Théocrite et Moschus firent la souveraine</l>
						<l n="17" num="1.17">Et les bords montueux de ce lac enchanté,</l>
						<l n="18" num="1.18">Des vallons de Zurich pure divinité,</l>
						<l n="19" num="1.19">Qui du sage Gessner à ses nymphes avides</l>
						<l n="20" num="1.20">Murmure les chansons sous leurs antres humides,</l>
						<l n="21" num="1.21">Elle s’est abreuvée à ces savantes eaux</l>
						<l n="22" num="1.22">Et partout sur leurs bords a coupé des roseaux.</l>
						<l n="23" num="1.23">Puisse-t-elle en avoir pris sur les mêmes tiges</l>
						<l n="24" num="1.24">Que ces chanteurs divins, dont les doctes prestiges</l>
						<l n="25" num="1.25">Ont aux fleuves charmés fait oublier leur cours,</l>
						<l n="26" num="1.26">Aux troupeaux l’herbe tendre, au pasteur ses amours !</l>
						<l n="27" num="1.27">De ces roseaux liés par des nœuds de fougère</l>
						<l n="28" num="1.28">Elle osait composer sa flûte bocagère</l>
						<l n="29" num="1.29">Et voulait, sous ses doigts exhalant de doux sons,</l>
						<l n="30" num="1.30">Chanter Pomone et Pan, les ruisseaux, les moissons,</l>
						<l n="31" num="1.31">Les vierges aux doux yeux, et les grottes muettes,</l>
						<l n="32" num="1.32">Et de l’âge d’amour les ardeurs inquiètes.</l>
					</lg>
				</div></body></text></TEI>