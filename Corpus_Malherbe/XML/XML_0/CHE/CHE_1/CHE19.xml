<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ŒUVRES POÉTIQUES</title>
				<title type="sub_2">tome I</title>
				<title type="medium">Édition électronique</title>
				<author key="CHE">
					<name>
						<forename>André</forename>
						<nameLink>de</nameLink>
						<surname>CHÉNIER</surname>
					</name>
					<date from="1762" to="1794">1762-1794</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>5334 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2011">2011</date>
				<idno type="local">CHE_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">ŒUVRES POÉTIQUES</title>
						<title type="sub">tome I</title>
						<author>André de Chénier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k5457953r.r=Oeuvres%20po%C3%A9tiques%20de%20Andr%C3%A9%20de%20Ch%C3%A9nier</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>ŒUVRES POÉTIQUES</title>
								<author>André de Chénier</author>
								<editor>Avec une notice et des notes de Raoul Guillard</editor>
								<imprint>
									<publisher>Alphonse Lemerre, éditeur</publisher>
									<date when="1899">1899</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1790">1790</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Tomaison et mise en forme conformes à l’édition de 1856. La préface n’est pas reprise</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-01" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ÉLÉGIES ANTIQUES</head><div type="poem" key="CHE19">
					<head type="number">III</head>
					<head type="main">La Jeune Tarentine</head>
					<lg n="1">
						<l n="1" num="1.1">Pleurez, doux alcyons ! ô vous oiseaux sacrés,</l>
						<l n="2" num="1.2">Oiseaux chers à Thétis, doux alcyons, pleurez !</l>
						<l n="3" num="1.3">Elle a vécu, Myrto, la jeune Tarentine !</l>
						<l n="4" num="1.4">Un vaisseau la portait aux bords de Camarine :</l>
						<l n="5" num="1.5">Là, l’hymen, les chansons, les flûtes, lentement</l>
						<l n="6" num="1.6">Devaient la reconduire au seuil de son amant.</l>
						<l n="7" num="1.7">Une clef vigilante a, pour cette journée,</l>
						<l n="8" num="1.8">Dans le cèdre enfermé sa robe d’hyménée,</l>
						<l n="9" num="1.9">Et l’or dont au festin ses bras seraient parés,</l>
						<l n="10" num="1.10">Et pour ses blonds cheveux les parfums préparés.</l>
						<l n="11" num="1.11">Mais seule, sur la proue, invoquant les étoiles,</l>
						<l n="12" num="1.12">Le vent impétueux qui soufflait dans les voiles</l>
						<l n="13" num="1.13">L’enveloppe : étonnée et loin des matelots,</l>
						<l n="14" num="1.14">Elle crie, elle tombe, elle est au sein des flots.</l>
						<l n="15" num="1.15">Elle est au sein des flots, la jeune Tarentine</l>
						<l n="16" num="1.16">Son beau corps a roulé sous la vague marine.</l>
						<l n="17" num="1.17">Thétis, les yeux en pleurs, dans le creux d’un rocher,</l>
						<l n="18" num="1.18">Aux monstres dévorants eut soin de le cacher.</l>
						<l n="19" num="1.19">Par ses ordres bientôt les belles Néréides</l>
						<l n="20" num="1.20">L’élèvent au-dessus des demeures humides,</l>
						<l n="21" num="1.21">Le portent au rivage, et dans ce monument</l>
						<l n="22" num="1.22">L’ont au cap du Zéphyr déposé mollement ;</l>
						<l n="23" num="1.23">Puis de loin, à grands cris appelant leurs compagnes,</l>
						<l n="24" num="1.24">Et les nymphes des bois, des sources, des montagnes,</l>
						<l n="25" num="1.25">Toutes, frappant leur sein et traînant un long deuil,</l>
						<l n="26" num="1.26">Répétèrent, hélas ! autour de son cercueil :</l>
					</lg>
					<lg n="2">
						<l n="27" num="2.1">« Hélas ! chez ton amant tu n’es point ramenée ;</l>
						<l n="28" num="2.2">Tu n’as point revêtu ta robe d’hyménée ;</l>
						<l n="29" num="2.3">L’or autour de tes bras n’a point serré de nœuds ;</l>
						<l n="30" num="2.4">Les doux parfums n’ont point coulé sur tes cheveux. »</l>
					</lg>
				</div></body></text></TEI>