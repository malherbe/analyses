<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ŒUVRES POÉTIQUES</title>
				<title type="sub_2">tome I</title>
				<title type="medium">Édition électronique</title>
				<author key="CHE">
					<name>
						<forename>André</forename>
						<nameLink>de</nameLink>
						<surname>CHÉNIER</surname>
					</name>
					<date from="1762" to="1794">1762-1794</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>5334 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2011">2011</date>
				<idno type="local">CHE_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">ŒUVRES POÉTIQUES</title>
						<title type="sub">tome I</title>
						<author>André de Chénier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k5457953r.r=Oeuvres%20po%C3%A9tiques%20de%20Andr%C3%A9%20de%20Ch%C3%A9nier</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>ŒUVRES POÉTIQUES</title>
								<author>André de Chénier</author>
								<editor>Avec une notice et des notes de Raoul Guillard</editor>
								<imprint>
									<publisher>Alphonse Lemerre, éditeur</publisher>
									<date when="1899">1899</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1790">1790</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Tomaison et mise en forme conformes à l’édition de 1856. La préface n’est pas reprise</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-01" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">POÉSIES DIVERSES</head><div type="poem" key="CHE96">
					<head type="number">IX</head>
					<head type="main">La Seine</head>
					<lg n="1">
						<l n="1" num="1.1">Ainsi, vainqueur de Troie et des vents et des flots,</l>
						<l n="2" num="1.2">D’un navire emprunté pressant les matelots,</l>
						<l n="3" num="1.3">Le fils du vieux Laërte arrive en sa patrie,</l>
						<l n="4" num="1.4">Baise en pleurant le sol de son île chérie ;</l>
						<l n="5" num="1.5">Il reconnaît le port couronné de rochers</l>
						<l n="6" num="1.6">Où le vieillard des mers accueille les nochers,</l>
						<l n="7" num="1.7">Et que l’olive épaisse entoure de son ombre ;</l>
						<l n="8" num="1.8">Il retrouve la source et l’antre humide et sombre</l>
						<l n="9" num="1.9">Où l’abeille murmure ; où, pour charmer les yeux,</l>
						<l n="10" num="1.10">Teints de pourpre et d’azur, des tissus précieux</l>
						<l n="11" num="1.11">Se forment sous les mains des naïades sacrées ;</l>
						<l n="12" num="1.12">Et dans ses premiers vœux ces nymphes adorées</l>
						<l n="13" num="1.13">(Que ses yeux n’osaient plus espérer de revoir)</l>
						<l n="14" num="1.14">De vivre, de régner lui permettent l’espoir.</l>
					</lg>
					<lg n="2">
						<l n="15" num="2.1">Oh ! des fleuves français brillante souveraine,</l>
						<l n="16" num="2.2">Salut ! ma longue course à tes bords me ramène,</l>
						<l n="17" num="2.3">Moi que ta nymphe pure en son lit de roseaux</l>
						<l n="18" num="2.4">Fit errer tant de fois au doux bruit de ses eaux ;</l>
						<l n="19" num="2.5">Moi qui la vis couler plus lente et plus facile,</l>
						<l n="20" num="2.6">Quand ma bouche animait la flûte de Sicile ;</l>
						<l n="21" num="2.7">Moi, quand l’amour trahi me fit verser des pleurs,</l>
						<l n="22" num="2.8">Qui l’entendis gémir et pleurer mes douleurs</l>
						<l n="23" num="2.9">Tout mon cortège antique, aux chansons langoureuses,</l>
						<l n="24" num="2.10">Revole comme moi vers tes rives heureuses.</l>
						<l n="25" num="2.11">Promptes dans tous mes pas à me suivre en tous lieux,</l>
						<l n="26" num="2.12">Le rire sur la bouche et les pleurs dans les yeux,</l>
						<l n="27" num="2.13">Partout autour de moi mes jeunes Élégies</l>
						<l n="28" num="2.14">Promenaient les éclats de leurs folles orgies,</l>
						<l n="29" num="2.15">Et, les cheveux épars, se tenant par la main,</l>
						<l n="30" num="2.16">De leur danse élégante égayaient mon chemin.</l>
						<l n="31" num="2.17">Il est bien doux d’avoir dans sa vie innocente</l>
						<l n="32" num="2.18">Une Muse naïve et de haines exempte,</l>
						<l n="33" num="2.19">Dont l’honnête candeur ne garde aucun secret ;</l>
						<l n="34" num="2.20">Où l’on puisse au hasard, sans crainte, sans apprêt,</l>
						<l n="35" num="2.21">Sûr de ne point rougir en voyant la lumière,</l>
						<l n="36" num="2.22">Répandre, dévoiler son âme tout entière.</l>
						<l n="37" num="2.23">C’est ainsi, promené sur tout cet univers,</l>
						<l n="38" num="2.24">Que mon cœur vagabond laisse tomber des vers.</l>
						<l n="39" num="2.25">De ses pensers errants vive et rapide image,</l>
						<l n="40" num="2.26">Chaque chanson nouvelle a son nouveau langage,</l>
						<l n="41" num="2.27">Et des rêves nouveaux un nouveau sentiment :</l>
						<l n="42" num="2.28">Tous sont divers et tous furent vrais un moment.</l>
					</lg>
					<lg n="3">
						<l n="43" num="3.1">Mais que les premiers pas ont d’alarmes craintives !</l>
						<l n="44" num="3.2">Nymphe de Seine, on dit que Paris sur tes rives,</l>
						<l n="45" num="3.3">Fait asseoir vingt conseils de critiques nombreux,</l>
						<l n="46" num="3.4">Du Pinde partagé despotes soupçonneux.</l>
						<l n="47" num="3.5">Affaiblis de leurs yeux la vigilance amère ;</l>
						<l n="48" num="3.6">Dis-leur que, sans s’armer d’un front dur et sévère,</l>
						<l n="49" num="3.7">Ils peuvent négliger les pas et les douceurs</l>
						<l n="50" num="3.8">D’une Muse timide, et qui, parmi ses sœurs,</l>
						<l n="51" num="3.9">Rivale de personne et sans demander grâce,</l>
						<l n="52" num="3.10">Vient, le regard baissé, solliciter sa place ;</l>
						<l n="53" num="3.11">Dont la main est sans tache et n’a connu jamais</l>
						<l n="54" num="3.12">Le fiel dont la satire envenime ses traits.</l>
					</lg>
				</div></body></text></TEI>