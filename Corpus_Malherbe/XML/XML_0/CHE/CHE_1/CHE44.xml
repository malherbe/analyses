<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ŒUVRES POÉTIQUES</title>
				<title type="sub_2">tome I</title>
				<title type="medium">Édition électronique</title>
				<author key="CHE">
					<name>
						<forename>André</forename>
						<nameLink>de</nameLink>
						<surname>CHÉNIER</surname>
					</name>
					<date from="1762" to="1794">1762-1794</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>5334 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2011">2011</date>
				<idno type="local">CHE_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">ŒUVRES POÉTIQUES</title>
						<title type="sub">tome I</title>
						<author>André de Chénier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k5457953r.r=Oeuvres%20po%C3%A9tiques%20de%20Andr%C3%A9%20de%20Ch%C3%A9nier</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>ŒUVRES POÉTIQUES</title>
								<author>André de Chénier</author>
								<editor>Avec une notice et des notes de Raoul Guillard</editor>
								<imprint>
									<publisher>Alphonse Lemerre, éditeur</publisher>
									<date when="1899">1899</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1790">1790</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Tomaison et mise en forme conformes à l’édition de 1856. La préface n’est pas reprise</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-01" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ÉGLOGUES</head><div type="poem" key="CHE44">
					<head type="number">X</head>
					<lg n="1">
						<l n="1" num="1.1">Toi ! de Mopsus ami ! Non loin de Bérécynthe,</l>
						<l n="2" num="1.2">Certain satyre, un jour, trouva la flûte sainte</l>
						<l n="3" num="1.3">Dont Hyagnis calmait ou rendait furieux</l>
						<l n="4" num="1.4">Le cortège énervé de la mère des dieux.</l>
						<l n="5" num="1.5">Il appelle aussitôt du Sangar au Méandre</l>
						<l n="6" num="1.6">Les nymphes de l’Asie, et leur dit de l’entendre,</l>
						<l n="7" num="1.7">Que tout l’art d’Hyagnis n’était que dans ce bui ;</l>
						<l n="8" num="1.8">Qu’il a, grâce au destin, des doigts tout comme lui.</l>
						<l n="9" num="1.9">On s’assied. Le voilà qui se travaille et sue,</l>
						<l n="10" num="1.10">Souffle, agite ses doigts, tord sa lèvre touffue,</l>
						<l n="11" num="1.11">Enfle sa joue épaisse, et fait tant qu’à la fin</l>
						<l n="12" num="1.12">Le buis résonne et pousse un cri rauque et chagrin.</l>
						<l n="13" num="1.13">L’auditoire étonné se lève, non sans rire.</l>
						<l n="14" num="1.14">Les éloges railleurs fondent sur le satyre</l>
						<l n="15" num="1.15">Qui pleure, et des Chiens même, en fuyant vers le bois,</l>
						<l n="16" num="1.16">Évite comme il peut les dents et les abois…</l>
						<l n="17" num="1.17">Ne te souvient-il plus que les bois de Célène</l>
						<l n="18" num="1.18">Virent punir jadis une audace aussi vaine ?</l>
						<l n="19" num="1.19">Si Marsyas aussi n’eût bravé ses vainqueurs,</l>
						<l n="20" num="1.20">Ni son père Hyagnis, ni les nymphes ses sœurs,</l>
						<l n="21" num="1.21">Olympe son ami, les satyres ses frères,</l>
						<l n="22" num="1.22">N’auraient pleuré des dieux les victoires sévères,</l>
						<l n="23" num="1.23">Et ne l’auraient point vu, ceint d’humides roseaux,</l>
						<l n="24" num="1.24">Errer dans la Phrygie en transparentes eaux.</l>
					</lg>
					<lg n="2">
						<gap resp="edition" hand="RR" reason="missing">.............................................................................</gap>
						<l n="25" num="2.1">Soit que son souffle anime un simple chalumeau,</l>
						<l n="26" num="2.2">Ou qu’il fasse courir sa lèvre harmonieuse</l>
						<l n="27" num="2.3">Sur neuf roseaux que joint la cire industrieuse,</l>
						<l n="28" num="2.4">Soit quand la flûte droite où voltigent ses doigts</l>
						<l n="29" num="2.5">Vient puiser dans sa bouche une facile voix,</l>
						<l n="30" num="2.6">Ou quand il fait parler, sur ses lèvres pressée,</l>
						<l n="31" num="2.7">La flûte oblique, chère aux grottes du Lycée.</l>
					</lg>
					<lg n="3">
						<gap resp="edition" hand="RR" reason="missing">.............................................................................</gap>
						<gap resp="edition" hand="RR" reason="missing">.............................................................................</gap>
						<l n="32" num="3.1">Non ; même sans chercher d’amoureuses promesses,</l>
						<l n="33" num="3.2">Sans vouloir de Vénus connaître les caresses,</l>
						<l n="34" num="3.3">D’être belle toujours vous prenez quelques soins ;</l>
						<l n="35" num="3.4">Vous voulez plaire même à qui vous plaît le moins.</l>
						<l n="36" num="3.5">O chaste déité qu’adore le Pirée,</l>
						<l n="37" num="3.6">Tu jettes l’instrument, fils de ta main sacrée,</l>
						<l n="38" num="3.7">Tu brises cette flûte où, pour charmer les dieux,</l>
						<l n="39" num="3.8">Respire en sons légers ton souffle harmonieux ;</l>
						<l n="40" num="3.9">Tu rougis de la voir dans une onde fidèle</l>
						<l n="41" num="3.10">Altérer la beauté de ta joue immortelle.</l>
						<l rhyme="none" n="42" num="3.11">Syrinx parle et respire aux lèvres du pasteur…</l>
					</lg>
					<lg n="4">
						<l n="43" num="4.1">Syrinx, que tes roseaux, à mordre insidieux</l>
						<l n="44" num="4.2">Gardent bien d’outrager ses doigts industrieux.</l>
					</lg>
				</div></body></text></TEI>