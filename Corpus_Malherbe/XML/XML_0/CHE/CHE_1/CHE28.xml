<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ŒUVRES POÉTIQUES</title>
				<title type="sub_2">tome I</title>
				<title type="medium">Édition électronique</title>
				<author key="CHE">
					<name>
						<forename>André</forename>
						<nameLink>de</nameLink>
						<surname>CHÉNIER</surname>
					</name>
					<date from="1762" to="1794">1762-1794</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>5334 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2011">2011</date>
				<idno type="local">CHE_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">ŒUVRES POÉTIQUES</title>
						<title type="sub">tome I</title>
						<author>André de Chénier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k5457953r.r=Oeuvres%20po%C3%A9tiques%20de%20Andr%C3%A9%20de%20Ch%C3%A9nier</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>ŒUVRES POÉTIQUES</title>
								<author>André de Chénier</author>
								<editor>Avec une notice et des notes de Raoul Guillard</editor>
								<imprint>
									<publisher>Alphonse Lemerre, éditeur</publisher>
									<date when="1899">1899</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1790">1790</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Tomaison et mise en forme conformes à l’édition de 1856. La préface n’est pas reprise</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-01" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">IDYLLES</head><div type="poem" key="CHE28">
					<head type="number">II</head>
					<head type="main">Mnazile et Chloé</head>
					<lg n="1">
						<head type="main">CHLOÉ.</head>
						<l n="1" num="1.1">Fleurs, bocage sonore, et mobiles roseaux</l>
						<l n="2" num="1.2">Où murmure Zéphyre au murmure des eaux,</l>
						<l n="3" num="1.3">Parlez ! le beau Mnazile est-il sous vos ombrages ?</l>
						<l n="4" num="1.4">Il visite souvent vos paisibles rivages.</l>
						<l n="5" num="1.5">Souvent j’écoute, et l’air qui gémit dans vos bois</l>
						<l n="6" num="1.6">A mon oreille au loin vient apporter sa voix.</l>
					</lg>
					<lg n="2">
						<head type="main">MNAZILE.</head>
						<l n="7" num="2.1">Onde, mère des fleurs, naïade transparente</l>
						<l n="8" num="2.2">Qui pressez mollement cette enceinte odorante,</l>
						<l n="9" num="2.3">Amenez-y Chloé, l’amour de mes regards !</l>
						<l n="10" num="2.4">Vos bords m’offrent souvent ses vestiges épars.</l>
						<l n="11" num="2.5">Souvent ma bouche vient, sous vos sombres allées,</l>
						<l n="12" num="2.6">Baiser l’herbe et les fleurs que ses pas ont foulées.</l>
					</lg>
					<lg n="3">
						<head type="main">CHLOÉ.</head>
						<l n="13" num="3.1">Oh ! s’il pouvait savoir quel amoureux ennui</l>
						<l n="14" num="3.2">Me rend cher ce bocage où je rêve de lui !</l>
						<l n="15" num="3.3">Peut-être je devrais d’un souris favorable</l>
						<l n="16" num="3.4">L’inviter, l’engager à me trouver aimable.</l>
					</lg>
					<lg n="4">
						<head type="main">MNAZILE.</head>
						<l n="17" num="4.1">Si, pour m’encourager, quelque dieu bienfaiteur</l>
						<l n="18" num="4.2">Lui disait que son nom fait palpiter mon cœur !</l>
						<l n="19" num="4.3">J’aurais dû l’inviter, d’une voix douce et tendre,</l>
						<l n="20" num="4.4">A se laisser aimer, à m’aimer, à m’entendre.</l>
					</lg>
					<lg n="5">
						<head type="main">CHLOÉ.</head>
						<l n="21" num="5.1">Ah ! je l’ai vu ; c’est lui. Dieu ! je vais lui parler !</l>
						<l n="22" num="5.2">O ma bouche ! ô mes yeux ! gardez de vous troubler.</l>
					</lg>
					<lg n="6">
						<head type="main">MNAZILE.</head>
						<l n="23" num="6.1">Le feuillage a frémi. Quelque robe légère…</l>
						<l n="24" num="6.2">C’est elle ! ô mes regards ! ayez soin de vous taire.</l>
					</lg>
					<lg n="7">
						<head type="main">CHLOÉ.</head>
						<l n="25" num="7.1">Quoi ! Mnazile est ici ! Seule, errante, mes pas</l>
						<l n="26" num="7.2">Cherchaient ici le frais et ne t’y croyaient pas.</l>
					</lg>
					<lg n="8">
						<head type="main">MNAZILE.</head>
						<l n="27" num="8.1">Seul, au bord de ces flots que le tilleul couronne,</l>
						<l n="28" num="8.2">J’avais fui le soleil et n’attendais personne.</l>
					</lg>
				</div></body></text></TEI>