<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ŒUVRES POÉTIQUES</title>
				<title type="sub_2">tome I</title>
				<title type="medium">Édition électronique</title>
				<author key="CHE">
					<name>
						<forename>André</forename>
						<nameLink>de</nameLink>
						<surname>CHÉNIER</surname>
					</name>
					<date from="1762" to="1794">1762-1794</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>5334 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2011">2011</date>
				<idno type="local">CHE_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">ŒUVRES POÉTIQUES</title>
						<title type="sub">tome I</title>
						<author>André de Chénier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k5457953r.r=Oeuvres%20po%C3%A9tiques%20de%20Andr%C3%A9%20de%20Ch%C3%A9nier</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>ŒUVRES POÉTIQUES</title>
								<author>André de Chénier</author>
								<editor>Avec une notice et des notes de Raoul Guillard</editor>
								<imprint>
									<publisher>Alphonse Lemerre, éditeur</publisher>
									<date when="1899">1899</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1790">1790</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Tomaison et mise en forme conformes à l’édition de 1856. La préface n’est pas reprise</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-01" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ÉLÉGIES ANTIQUES</head><div type="poem" key="CHE26">
					<head type="number">X</head>
					<head type="main">Dryas</head>
					<lg n="1">
						<l n="1" num="1.1">Tout est-il prêt ? partons. Oui, le mât est dressé ;</l>
						<l n="2" num="1.2">Adieu donc ; sur les bancs le rameur est placé ;</l>
						<l n="3" num="1.3">La voile, ouverte aux vents, s’enfle et s’agite et flotte ;</l>
						<l n="4" num="1.4">Déjà le gouvernail tourne aux mains du pilote.</l>
						<l n="5" num="1.5">Insensé ! vainement le serrant dans leurs bras,</l>
						<l n="6" num="1.6">Femme, enfants, tout se jette au-devant de ses pas ;</l>
						<l n="7" num="1.7">Il monte, on lève l’ancre. Élevé sur la poupe,</l>
						<l n="8" num="1.8">Il remplit et couronne une écumante coupe,</l>
						<l n="9" num="1.9">Prie, et la verse aux dieux qui commandent aux flots.</l>
						<l n="10" num="1.10">Tout retentit de cris, adieux des matelots.</l>
						<l n="11" num="1.11">Sur sa famille en pleurs il tourne encor la vue,</l>
						<l n="12" num="1.12">Et des yeux et des mains longtemps il les salue.</l>
						<l n="13" num="1.13">Insensé ! vainement une fois averti !</l>
						<l n="14" num="1.14">On détache le câble ; il part ; il est parti !</l>
						<l n="15" num="1.15">Car il ne voyait pas que bientôt sur sa tête</l>
						<l n="16" num="1.16">L’automne impétueux amassant la tempête</l>
						<l n="17" num="1.17">L’attendait au passage, et là, loin de tout bord,</l>
						<l n="18" num="1.18">Lui préparait bientôt le naufrage et la mort.</l>
						<l n="19" num="1.19">« Dieux de la mer Égée, ô vents, ô dieux humides,</l>
						<l n="20" num="1.20">Glaucus et Palémon, et blanches Néréides,</l>
						<l n="21" num="1.21">Sauvez, sauvez Dryas. Déjà voisin du port,</l>
						<l n="22" num="1.22">Entre la terre et moi je rencontre la mort.</l>
						<l n="23" num="1.23">Mon navire est brisé. Sous les ondes avares</l>
						<l n="24" num="1.24">Tous les miens ont péri. Dieux ! rendez-moi mes lares !</l>
						<l n="25" num="1.25">Dieux ! entendez les cris d’un père et d’un époux !</l>
						<l n="26" num="1.26">Sauvez, sauvez Dryas, il s’abandonne à vous. »</l>
						<l n="27" num="1.27">Il dit, plonge, et, perdant au sein de la tourmente</l>
						<l n="28" num="1.28">La planche, sous ses pieds fugitive et flottante,</l>
						<l n="29" num="1.29">Nage, et lutte, et ses bras et ses efforts nombreux,</l>
						<l n="30" num="1.30">Et la vague en roulant sur les sables pierreux,</l>
						<l n="31" num="1.31">Blême, expirant, couvert d’une écume salée,</l>
						<l n="32" num="1.32">Le vomit. Sa famille errante, échevelée,</l>
						<l n="33" num="1.33">Qui perçait l’air de cris et se frappait le sein,</l>
						<l n="34" num="1.34">Court, le saisit, l’entraîne, et, le fer à la main,</l>
						<l n="35" num="1.35">Rendant grâces aux flots d’avoir sauvé sa tête,</l>
						<l n="36" num="1.36">Offre une brebis noire à la noire tempête.</l>
						<gap resp="edition" hand="RR" reason="missing">.............................................................................</gap>
						<l n="37" num="1.37">J’étais père, et je meurs victime du naufrage.</l>
						<l n="38" num="1.38">Adieu, ma femme, adieu, mes chers enfants. O toi,</l>
						<l n="39" num="1.39">Nautonier, de retour, si tu tiens le rivage,</l>
						<l n="40" num="1.40">Reste avec tes enfants, sois plus sage que moi.</l>
					</lg>
				</div></body></text></TEI>