<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ŒUVRES POÉTIQUES</title>
				<title type="sub_2">tome I</title>
				<title type="medium">Édition électronique</title>
				<author key="CHE">
					<name>
						<forename>André</forename>
						<nameLink>de</nameLink>
						<surname>CHÉNIER</surname>
					</name>
					<date from="1762" to="1794">1762-1794</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>5334 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2011">2011</date>
				<idno type="local">CHE_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">ŒUVRES POÉTIQUES</title>
						<title type="sub">tome I</title>
						<author>André de Chénier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k5457953r.r=Oeuvres%20po%C3%A9tiques%20de%20Andr%C3%A9%20de%20Ch%C3%A9nier</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>ŒUVRES POÉTIQUES</title>
								<author>André de Chénier</author>
								<editor>Avec une notice et des notes de Raoul Guillard</editor>
								<imprint>
									<publisher>Alphonse Lemerre, éditeur</publisher>
									<date when="1899">1899</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1790">1790</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Tomaison et mise en forme conformes à l’édition de 1856. La préface n’est pas reprise</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-01" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ÉLÉGIES ANTIQUES</head><div type="poem" key="CHE17">
					<head type="number">I</head>
					<head type="main">Le Jeune Malade</head>
					<lg n="1">
						<l n="1" num="1.1">APOLLON, dieu sauveur, dieu des savants mystères,</l>
						<l n="2" num="1.2">Dieu de la vie, et dieu des plantes salutaires,</l>
						<l n="3" num="1.3">Dieu vainqueur de Python, dieu jeune et triomphant,</l>
						<l n="4" num="1.4">Prends pitié de mon fils, de mon unique enfant !</l>
						<l n="5" num="1.5">Prends pitié de sa mère aux larmes condamnée,</l>
						<l n="6" num="1.6">Qui ne vit que pour lui, qui meurt abandonnée,</l>
						<l n="7" num="1.7">Qui n’a pas dû rester pour voir mourir son fils !</l>
						<l n="8" num="1.8">Dieu jeune, viens aider sa jeunesse. Assoupis,</l>
						<l n="9" num="1.9">Assoupis dans son sein cette fièvre brûlante</l>
						<l n="10" num="1.10">Qui dévore la fleur de sa vie innocente.</l>
						<l n="11" num="1.11">Apollon ! si jamais, échappé du tombeau,</l>
						<l n="12" num="1.12">Il retourne au Ménale avoir soin du troupeau,</l>
						<l n="13" num="1.13">Ces mains, ces vieilles mains orneront ta statue</l>
						<l n="14" num="1.14">De ma coupe d’onyx à tes pieds suspendue ;</l>
						<l n="15" num="1.15">Et, chaque été nouveau, d’un jeune taureau blanc</l>
						<l n="16" num="1.16">La hache à ton autel fera couler le sang.</l>
					</lg>
					<lg n="2">
						<l n="17" num="2.1">« Eh bien, mon fils, es-tu toujours impitoyable ?</l>
						<l n="18" num="2.2">Ton funeste silence est-il inexorable ?</l>
						<l n="19" num="2.3">Enfant, tu veux mourir ? Tu veux, dans ses vieux ans,</l>
						<l n="20" num="2.4">Laisser ta mère seule avec ses cheveux blancs ?</l>
						<l n="21" num="2.5">Tu veux que ce soit moi qui ferme ta paupière ?</l>
						<l n="22" num="2.6">Que j’unisse ta cendre à celle de ton père ?</l>
						<l n="23" num="2.7">C’est toi qui me devais ces soins religieux,</l>
						<l n="24" num="2.8">Et ma tombe attendait tes pleurs et tes adieux.</l>
						<l n="25" num="2.9">Parle, parle, mon fils ! quel chagrin te consume ?</l>
						<l n="26" num="2.10">Les maux qu’on dissimule en ont plus d’amertume.</l>
						<l n="27" num="2.11">Ne lèveras-tu point ces yeux appesantis ?</l>
					</lg>
					<lg n="3">
						<l n="28" num="3.1">« — Ma mère, adieu ; je meurs, et tu n’as plus de fils.</l>
						<l n="29" num="3.2">Non, tu n’as plus de fils, ma mère bien-aimée.</l>
						<l n="30" num="3.3">Je te perds. Une plaie ardente, envenimée,</l>
						<l n="31" num="3.4">Me ronge ; avec effort je respire, et je crois</l>
						<l n="32" num="3.5">Chaque fois respirer pour la dernière fois.</l>
						<l n="33" num="3.6">Je ne parlerai pas. Adieu ; ce lit me blesse,</l>
						<l n="34" num="3.7">Ce tapis qui me couvre accable ma faiblesse ;</l>
						<l n="35" num="3.8">Tout me pèse et me lasse. Aide-moi, je me meurs.</l>
						<l n="36" num="3.9">Tourne-moi sur le flanc. Ah ! j’expire ! ô douleurs !</l>
						<l n="37" num="3.10">— Tiens, mon unique enfant, mon fils, prends ce breuvage ;</l>
						<l n="38" num="3.11">Sa chaleur te rendra ta force et ton courage.</l>
						<l n="39" num="3.12">La mauve, le dictame ont, avec les pavots,</l>
						<l n="40" num="3.13">Mêlé leurs sucs puissants qui donnent le repos ;</l>
						<l n="41" num="3.14">Sur le vase bouillant, attendrie à mes larmes,</l>
						<l n="42" num="3.15">Une Thessalienne a composé des charmes.</l>
						<l n="43" num="3.16">Ton corps débile a vu trois retours du soleil</l>
						<l n="44" num="3.17">Sans connaître Cérès, ni tes yeux le sommeil.</l>
						<l n="45" num="3.18">Prends, mon fils, laisse-toi fléchir à ma prière ;</l>
						<l n="46" num="3.19">C’est ta mère, ta vieille inconsolable mère</l>
						<l n="47" num="3.20">Qui pleure, qui jadis te guidait pas à pas,</l>
						<l n="48" num="3.21">T’asseyait sur son sein, te portait dans ses bras ;</l>
						<l n="49" num="3.22">Que tu disais aimer, qui t’apprit à le dire,</l>
						<l n="50" num="3.23">Qui chantait, et souvent te forçait à sourire</l>
						<l n="51" num="3.24">Lorsque tes jeunes dents, par de vives douleurs,</l>
						<l n="52" num="3.25">De tes yeux enfantins faisaient couler des pleurs.</l>
						<l n="53" num="3.26">Tiens, presse de ta lèvre, hélas ! pâle et glacée,</l>
						<l n="54" num="3.27">Par qui cette mamelle était jadis pressée.</l>
						<l n="55" num="3.28">Que ce suc te nourrisse et vienne à ton secours,</l>
						<l n="56" num="3.29">Comme autrefois mon lait nourrit tes premiers jours.</l>
					</lg>
					<lg n="4">
						<l n="57" num="4.1">« — O coteaux d’Érymanthe ! ô vallons ! ô bocage !</l>
						<l n="58" num="4.2">O vent sonore et frais qui troublait le feuillage,</l>
						<l n="59" num="4.3">Et faisait frémir l’onde, et sur leur jeune sein</l>
						<l n="60" num="4.4">Agitait les replis de leur robe de lin !</l>
						<l n="61" num="4.5">De légères beautés troupe agile et dansante…</l>
						<l n="62" num="4.6">Tu sais, tu sais, ma mère ? aux bords de l’Érymanthe,</l>
						<l n="63" num="4.7">Là, ni loups ravisseurs, ni serpents, ni poisons…</l>
						<l n="64" num="4.8">O visage divin ! ô fêtes ! ô chansons !</l>
						<l n="65" num="4.9">Des pas entrelacés, des fleurs, une onde pure,</l>
						<l n="66" num="4.10">Aucun lieu n’est si beau dans toute la nature.</l>
						<l n="67" num="4.11">Dieux ! ces bras et ces flancs, ces cheveux, ces pieds nus,</l>
						<l n="68" num="4.12">Si blancs, si délicats ! je ne te verrai plus !</l>
						<l n="69" num="4.13">Oh ! portez, portez-moi sur les bords d’Érymanthe ;</l>
						<l n="70" num="4.14">Que je la voie encor, cette vierge dansante !</l>
						<l n="71" num="4.15">Oh ! que je voie au loin la fumée à longs flots</l>
						<l n="72" num="4.16">S’élever de ce toit au bord de cet enclos…</l>
						<l n="73" num="4.17">Assise à tes côtés, ses discours, sa tendresse,</l>
						<l n="74" num="4.18">Sa voix ! trop heureux père ! enchante ta vieillesse.</l>
						<l n="75" num="4.19">Dieux ! par-dessus la haie élevée en remparts,</l>
						<l n="76" num="4.20">Je la vois, à pas lents, en longs cheveux épars,</l>
						<l n="77" num="4.21">Seule, sur un tombeau, pensive, inanimée,</l>
						<l n="78" num="4.22">S’arrêter et pleurer sa mère bien-aimée.</l>
						<l n="79" num="4.23">Oh ! que tes yeux sont doux ! que ton visage est beau !</l>
						<l n="80" num="4.24">Viendras-tu point aussi pleurer sur mon tombeau ?</l>
						<l n="81" num="4.25">Viendras-tu point aussi, la plus belle des belles,</l>
						<l n="82" num="4.26">Dire sur mon tombeau : « Les Parques sont cruelles !</l>
					</lg>
					<lg n="5">
						<l n="83" num="5.1">« — Ah ! mon fils, c’est l’amour, c’est l’amour insensé</l>
						<l n="84" num="5.2">Qui t’a jusqu’à ce point cruellement blessé ?</l>
						<l n="85" num="5.3">Ah ! mon malheureux fils ! Oui, faibles que nous sommes,</l>
						<l n="86" num="5.4">C’est toujours cet amour qui tourmente les hommes.</l>
						<l n="87" num="5.5">S’ils pleurent en secret, qui lira dans leur cœur</l>
						<l n="88" num="5.6">Verra que c’est toujours cet amour en fureur.</l>
						<l n="89" num="5.7">Mais, mon fils, mais dis-moi, quelle belle dansante,</l>
						<l n="90" num="5.8">Quelle vierge as-tu vue au bord de l’Érymanthe ?</l>
						<l n="91" num="5.9">N’es-tu pas riche et beau ? du moins quand la douleur</l>
						<l n="92" num="5.10">N’avait point de ta joue éteint la jeune fleur !</l>
						<l n="93" num="5.11">Parle. Est-ce cette Églé, fille du roi des ondes,</l>
						<l n="94" num="5.12">Ou cette jeune Irène aux longues tresses blondes ?</l>
						<l n="95" num="5.13">Ou ne sera-ce point cette fière beauté</l>
						<l n="96" num="5.14">Dont j’entends le beau nom chaque jour répété,</l>
						<l n="97" num="5.15">Dont j’apprends que partout les belles sont jalouses ?</l>
						<l n="98" num="5.16">Qu’aux temples, aux festins, les mères, les épouses,</l>
						<l n="99" num="5.17">Ne sauraient voir, dit-on, sans peine et sans effroi ?</l>
						<l n="100" num="5.18">Cette belle Daphné ?… — Dieux ! ma mère, tais-toi,</l>
						<l n="101" num="5.19">Tais-toi. Dieux ! qu’as-tu dit ? Elle est fière, inflexible ;</l>
						<l n="102" num="5.20">Comme les immortels, elle est belle et terrible !</l>
						<l n="103" num="5.21">Mille amants l’ont aimée ; ils l’ont aimée en vain.</l>
						<l n="104" num="5.22">Comme eux j’aurais trouvé quelque refus hautain.</l>
						<l n="105" num="5.23">Non, garde que jamais elle soit informée…</l>
						<l n="106" num="5.24">Mais, ô mort ! ô tourment ! ô mère bien-aimée !</l>
						<l n="107" num="5.25">Tu vois dans quels ennuis dépérissent mes jours.</l>
						<l n="108" num="5.26">Ma mère bien-aimée, ah ! viens à mon secours ;</l>
						<l n="109" num="5.27">Je meurs ; va la trouver : que tes traits, que ton âge,</l>
						<l n="110" num="5.28">De sa mère à ses yeux offrent la sainte image.</l>
						<l n="111" num="5.29">Tiens, prends cette corbeille et nos fruits les plus beaux,</l>
						<l n="112" num="5.30">Prends notre Amour d’ivoire, honneur de ces hameaux,</l>
						<l n="113" num="5.31">Prends la coupe d’onyx à Corinthe ravie,</l>
						<l n="114" num="5.32">Prends mes jeunes chevreaux, prends mon cœur, prends ma vie,</l>
						<l n="115" num="5.33">Jette tout à ses pieds ; apprends-lui qui je suis ;</l>
						<l n="116" num="5.34">Dis-lui que je me meurs, que tu n’as plus de fils.</l>
						<l n="117" num="5.35">Tombe aux pieds du vieillard, gémis, implore, presse ;</l>
						<l n="118" num="5.36">Adjure deux et mers, dieu, temple, autel, déesse.</l>
						<l n="119" num="5.37">Pars ; et si tu reviens sans les avoir fléchis,</l>
						<l n="120" num="5.38">Adieu, ma mère, adieu, tu n’auras plus de fils.</l>
					</lg>
					<lg n="6">
						<l n="121" num="6.1">« — J’aurai toujours un fils, va, la belle Espérance</l>
						<l n="122" num="6.2">Me dit… » Elle s’incline, et, dans un doux silence,</l>
						<l n="123" num="6.3">Elle couvre ce front, terni par les douleurs,</l>
						<l n="124" num="6.4">De baisers maternels entremêlés de pleurs.</l>
						<l n="125" num="6.5">Puis elle sort en hâte, inquiète et tremblante,</l>
						<l n="126" num="6.6">Sa démarche est de crainte et d’âge chancelante.</l>
						<l n="127" num="6.7">Elle arrive ; et bientôt revenant sur ses pas,</l>
						<l n="128" num="6.8">Haletante, de loin : « Mon cher fils, tu vivras,</l>
						<l n="129" num="6.9">Tu vivras. » Elle vient s’asseoir près de la couche.</l>
						<l n="130" num="6.10">Le vieillard la suivait, le sourire à la bouche.</l>
						<l n="131" num="6.11">La jeune belle aussi, rouge et le front baissé,</l>
						<l n="132" num="6.12">Vient, jette sur le lit un coup d’œil. L’insensé</l>
						<l n="133" num="6.13">Tremble ; sous ses tapis il veut cacher sa tête.</l>
						<l n="134" num="6.14">« Ami, depuis trois jours tu n’es d’aucune fête,</l>
						<l n="135" num="6.15">Dit-elle ; que fais-tu ? Pourquoi veux-tu mourir ?</l>
						<l n="136" num="6.16">Tu souffres. On me dit que je peux te guérir ;</l>
						<l n="137" num="6.17">Vis, et formons ensemble une seule famille :</l>
						<l n="138" num="6.18">Que mon père ait un fils et ta mère une fille. »</l>
					</lg>
				</div></body></text></TEI>