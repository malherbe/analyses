<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ŒUVRES POÉTIQUES</title>
				<title type="sub_2">tome I</title>
				<title type="medium">Édition électronique</title>
				<author key="CHE">
					<name>
						<forename>André</forename>
						<nameLink>de</nameLink>
						<surname>CHÉNIER</surname>
					</name>
					<date from="1762" to="1794">1762-1794</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>5334 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2011">2011</date>
				<idno type="local">CHE_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">ŒUVRES POÉTIQUES</title>
						<title type="sub">tome I</title>
						<author>André de Chénier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k5457953r.r=Oeuvres%20po%C3%A9tiques%20de%20Andr%C3%A9%20de%20Ch%C3%A9nier</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>ŒUVRES POÉTIQUES</title>
								<author>André de Chénier</author>
								<editor>Avec une notice et des notes de Raoul Guillard</editor>
								<imprint>
									<publisher>Alphonse Lemerre, éditeur</publisher>
									<date when="1899">1899</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1790">1790</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Tomaison et mise en forme conformes à l’édition de 1856. La préface n’est pas reprise</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-01" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ÉLÉGIES ANTIQUES</head><div type="poem" key="CHE24">
					<head type="number">VIII</head>
					<lg n="1">
						<l n="1" num="1.1">Tu gémis, sur l’Ida, mourante, échevelée,</l>
						<l n="2" num="1.2">O reine, ô de Minos épouse désolée !</l>
						<l n="3" num="1.3">Heureuse si jamais, dans ses riches travaux,</l>
						<l n="4" num="1.4">Cérès n’eût pour le joug élevé des troupeaux !</l>
						<l n="5" num="1.5">Certe, aux antres d’Amnise, assez votre Lucine</l>
						<l n="6" num="1.6">Donnait de beaux neveux aux mères de Gortyne ;</l>
						<l n="7" num="1.7">Certes, vous élevez, aux gymnases crétois,</l>
						<l n="8" num="1.8">D’autres jeunes troupeaux plus dignes de ton choix.</l>
						<l n="9" num="1.9">Tu voles épier sous quelle yeuse obscure,</l>
						<l n="10" num="1.10">Tranquille, il ruminait son antique pâture,</l>
						<l n="11" num="1.11">Quel lit de fleurs reçut ses membres nonchalants,</l>
						<l n="12" num="1.12">Quelle onde a ranimé l’albâtre de ses flancs.</l>
						<l n="13" num="1.13">O nymphes, entourez, fermez, nymphes de Crète,</l>
						<l n="14" num="1.14">De ces vallons, fermez, entourez la retraite,</l>
						<l n="15" num="1.15">Si peut-être vers lui des vestiges épars</l>
						<l n="16" num="1.16">Ne viendront point guider mes pas et mes regards.</l>
						<l n="17" num="1.17">Insensée ! à travers ronces, forêts, montagnes,</l>
						<l n="18" num="1.18">Elle court. O fureur ! dans les vertes campagnes,</l>
						<l n="19" num="1.19">Une belle génisse à son superbe amant</l>
						<l n="20" num="1.20">Adressait devant elle un doux mugissement.</l>
						<l n="21" num="1.21">« La perfide mourra. Jupiter la demande. »</l>
						<l n="22" num="1.22">Elle-même à son front attache la guirlande,</l>
						<l n="23" num="1.23">L’entraîne, et sur l’autel prenant le fer vengeur :</l>
						<l n="24" num="1.24">« Sois belle maintenant, et plais à mon vainqueur. »</l>
						<l n="25" num="1.25">Elle frappe. Et sa haine, à la flamme lustrale,</l>
						<l n="26" num="1.26">Rit de voir palpiter le cœur de sa rivale.</l>
					</lg>
				</div></body></text></TEI>