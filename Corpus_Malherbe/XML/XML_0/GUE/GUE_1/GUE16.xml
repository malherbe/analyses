<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Cœur Solitaire</title>
				<title type="medium">Édition électronique</title>
				<author key="GUE">
					<name>
						<forename>Charles</forename>
						<surname>GUÉRIN</surname>
					</name>
					<date from="1873" to="1907">1873-1907</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2116 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">GUE_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Cœur Solitaire</title>
						<author>Charles Guérin</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">https://www.poesies.net/charlesguerinlecœursolitaire.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition papier d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Le Cœur Solitaire</title>
						<author>Charles Guérin</author>
						<idno type="URI">https://archive.org/details/lecoeursolitair00gu</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LES ÉDITIONS G. GRÈS ET Cie</publisher>
							<date when="1922">1922</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Le Cœur Solitaire</title>
						<author>Charles Guérin</author>
						<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k54873c.r=%22charles%20gu%C3%A9rin%22%22le%20coeur%20solitaire%22?rk=64378;0</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Mercure de France</publisher>
							<date when="1904">1904</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1895">1895</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les majuscules en début de vers ont été rétablies.</p>
					<p>Les guillemets simples ont été remplacés par des guillemets français.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-11-08" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-11-08" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">III</head><head type="main_part">FENÊTRES SUR LA VIE</head><div type="poem" key="GUE16" rhyme="none">
					<head type="number">XVI</head>
					<lg n="1">
						<l n="1" num="1.1">Le ciel est pur, l’eau transparente, et l’air du soir</l>
						<l n="2" num="1.2">Léger comme un baiser fugitif sur ma joue.</l>
						<l n="3" num="1.3">Le vent dans mes cheveux semble un enfant qui joue,</l>
						<l n="4" num="1.4">Et je vais, parmi l’herbe encor chaude, m’asseoir</l>
						<l n="5" num="1.5">En face du limpide et pensif horizon.</l>
						<l n="6" num="1.6">Haleine de la nuit qui dévale, silence,</l>
						<l n="7" num="1.7">Espace où d’un agile essor l’âme s’élance,</l>
						<l n="8" num="1.8">Solitude ! Une cloche aux derniers nids répond.</l>
						<l n="9" num="1.9">Dans les champs où le soir traîne ses voiles bleus</l>
						<l n="10" num="1.10">Un attelage au pas sûr et lent se balance.</l>
						<l n="11" num="1.11">Le soleil met de l’or sur les cornes des boeufs,</l>
						<l n="12" num="1.12">Le soleil qui descend au delà des collines</l>
						<l n="13" num="1.13">Verse l’adieu de ses rayons mélancoliques</l>
						<l n="14" num="1.14">Sur la mouvante mer sans fin des moissons mûres</l>
						<l n="15" num="1.15">Dont la houle, semblable au vaste rêve humain,</l>
						<l n="16" num="1.16">Roule, se gonfle et meurt et frissonne et murmure</l>
						<l n="17" num="1.17">Vers la ligne de ciel qu’elle n’atteint jamais.</l>
						<l n="18" num="1.18">Je regarde, et mon front retombe dans mes mains</l>
						<l n="19" num="1.19">Comme un fruit délicat s’abrite sous ses feuilles.</l>
						<l n="20" num="1.20">Ô silence des soirs d’été, profonde paix</l>
						<l n="21" num="1.21">Où, comme en un miroir, l’esprit qui se recueille</l>
						<l n="22" num="1.22">Voit flotter l’horizon nocturne du passé !</l>
						<l n="23" num="1.23">Nous nous sommes aimés un jour, et ce fut vain</l>
						<l n="24" num="1.24">Comme un rosier sur un tombeau. Je me souviens ;</l>
						<l n="25" num="1.25">J’écoute bourdonner en moi l’amour ancien ;</l>
						<l n="26" num="1.26">J’ai peur de cette guêpe impossible à chasser.</l>
						<l n="27" num="1.27">Coeur lacéré, pareil à l’arbre qui renforce</l>
						<l n="28" num="1.28">En vieillissant les noms gravés sur son écorce,</l>
						<l n="29" num="1.29">Quand pourrons-nous aimer sans mémoire ? En quel lit</l>
						<l n="30" num="1.30">Saurai-je enfin trouver le véritable oubli ?</l>
						<l n="31" num="1.31">Le soleil sur les blés et les coteaux se couche,</l>
						<l n="32" num="1.32">Mais ses rayons mourants me rendront-ils la bouche</l>
						<l n="33" num="1.33">Comme eux voluptueuse et large et tiède et rouge</l>
						<l n="34" num="1.34">De l’amante qui m’a pressé dans ses bras forts ?</l>
						<l n="35" num="1.35">Les épis aux lourds flots fauves me rendront-ils</l>
						<l n="36" num="1.36">Ses cheveux déroulés sur elle en ondes d’or ?</l>
						<l n="37" num="1.37">Les coteaux dont la nuit découpe le profil</l>
						<l n="38" num="1.38">N’ont pas l’inflexion charmante de son corps,</l>
						<l n="39" num="1.39">Et les parfums de fleurs que ces plaines exhalent</l>
						<l n="40" num="1.40">Ne sont pas doux à respirer comme son âme.</l>
					</lg>
					<lg n="2">
						<l n="41" num="2.1">Un éclair de chaleur fouille le crépuscule :</l>
						<l n="42" num="2.2">Ainsi le souvenir me déchire et me brûle.</l>
					</lg>
					<lg n="3">
						<l n="43" num="3.1">Dans ces soirs de splendeur pacifique où l’on souffre</l>
						<l n="44" num="3.2">À sentir sa bassesse et sa pauvreté d’homme,</l>
						<l n="45" num="3.3">Où l’esprit aveuglé de lumière tâtonne,</l>
						<l n="46" num="3.4">Où le coeur enivré d’azur et d’air étouffe,</l>
						<l n="47" num="3.5">On a des mots d’enfant qui pleurent et supplient</l>
						<l n="48" num="3.6">Vers ce vaste univers qu’on voudrait croire Dieu.</l>
						<l n="49" num="3.7">« Ah ! Dit-on, remplir l’orbe immense de la vie,</l>
						<l n="50" num="3.8">Ouvrir comme l’étoile un jour sur d’autres cieux,</l>
						<l n="51" num="3.9">Comme le roc porter le fer, l’or et le feu,</l>
						<l n="52" num="3.10">Tressaillir au printemps nouveau, pousser des feuilles,</l>
						<l n="53" num="3.11">Être la brume, l’eau du puits, le fruit qu’on cueille,</l>
						<l n="54" num="3.12">Vivre enfin sans se voir vivre ! Puissante mère,</l>
						<l n="55" num="3.13">Prends-moi, terre des morts, terre des blés, ô terre !</l>
						<l n="56" num="3.14">Mêle mon corps vivant à ta grande poussière !  »</l>
						<l n="57" num="3.15">Mais la nature avec orgueil poursuit son rêve :</l>
						<l n="58" num="3.16">Elle n’alliera pas notre sang à ses sèves.</l>
						<l n="59" num="3.17">Le jeune avril, le bel été, le vieil automne</l>
						<l n="60" num="3.18">Mènent leur ronde autour du linceul de l’hiver</l>
						<l n="61" num="3.19">Sans savoir qu’ils font naître, aimer et souffrir l’homme.</l>
						<l n="62" num="3.20">Dans sa joie égoïste et pleine, l’univers</l>
						<l n="63" num="3.21">Reste sourd au désir fraternel de la chair ;</l>
						<l n="64" num="3.22">L’âme contre le noir grillage qui l’enferme</l>
						<l n="65" num="3.23">S’élance, oiseau captif, et se brise les ailes :</l>
						<l n="66" num="3.24">Nous ne connaîtrons rien de la mère éternelle.</l>
						<l n="67" num="3.25">Et nous aurons un mal secret de la voir belle,</l>
						<l n="68" num="3.26">D’épier vainement l’obscur travail des germes</l>
						<l n="69" num="3.27">Dont la sourde harmonie échappe à nos oreilles ;</l>
						<l n="70" num="3.28">Nous ne mûrirons pas dans les grappes des treilles,</l>
						<l n="71" num="3.29">Ni dans le fruit, ni dans le blé, ni dans la pierre ;</l>
						<l n="72" num="3.30">L’eau nous refusera son étreinte, le chêne</l>
						<l n="73" num="3.31">Ne nous livrera pas la nymphe qu’il enchaîne,</l>
						<l n="74" num="3.32">Et si nous prions trop le soleil, sa lumière</l>
						<l n="75" num="3.33">Calcinera nos yeux à travers nos paupières.</l>
					</lg>
					<lg n="4">
						<l n="76" num="4.1">La nature, d’un geste ennuyé de marâtre,</l>
						<l n="77" num="4.2">Écarte notre soif de ses larges mamelles.</l>
						<l n="78" num="4.3">Elle va ; nos amours, nos rêves et nos peines</l>
						<l n="79" num="4.4">Étendus à ses pieds craquent comme les faînes</l>
						<l n="80" num="4.5">Éclatent sous les pas indifférents du pâtre.</l>
						<l n="81" num="4.6">Et pourtant, sous le ciel des soirs d’été sans fin,</l>
						<l n="82" num="4.7">Encor, toujours, jusqu’à la nuit où le destin</l>
						<l n="83" num="4.8">Voudra fermer les yeux à l’humanité lasse,</l>
						<l n="84" num="4.9">D’autres viendront, pareils à moi dans leur chair veuve,</l>
						<l n="85" num="4.10">Le coeur amer d’un vieil amour resté vivace,</l>
						<l n="86" num="4.11">Voir, parmi les corbeaux qui volent vers sa face,</l>
						<l n="87" num="4.12">Le soleil se coucher sur des moissons heureuses.</l>
					</lg>
				</div></body></text></TEI>