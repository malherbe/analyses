<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Cœur Solitaire</title>
				<title type="medium">Édition électronique</title>
				<author key="GUE">
					<name>
						<forename>Charles</forename>
						<surname>GUÉRIN</surname>
					</name>
					<date from="1873" to="1907">1873-1907</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2116 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">GUE_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Cœur Solitaire</title>
						<author>Charles Guérin</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">https://www.poesies.net/charlesguerinlecœursolitaire.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition papier d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Le Cœur Solitaire</title>
						<author>Charles Guérin</author>
						<idno type="URI">https://archive.org/details/lecoeursolitair00gu</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LES ÉDITIONS G. GRÈS ET Cie</publisher>
							<date when="1922">1922</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Le Cœur Solitaire</title>
						<author>Charles Guérin</author>
						<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k54873c.r=%22charles%20gu%C3%A9rin%22%22le%20coeur%20solitaire%22?rk=64378;0</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Mercure de France</publisher>
							<date when="1904">1904</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1895">1895</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les majuscules en début de vers ont été rétablies.</p>
					<p>Les guillemets simples ont été remplacés par des guillemets français.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-11-08" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-11-08" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">III</head><head type="main_part">FENÊTRES SUR LA VIE</head><div type="poem" key="GUE17" rhyme="none">
					<head type="number">XVII</head>
					<lg n="1">
						<l n="1" num="1.1">Charme indéfinissable et fin, le soir d’été</l>
						<l n="2" num="1.2">Se glisse, souffles, fleurs et voix, par les fenêtres.</l>
						<l n="3" num="1.3">Comme sa paix se pose en baume sur les lèvres !</l>
						<l n="4" num="1.4">Comme son calme apprend aux âmes la bonté !</l>
						<l n="5" num="1.5">Il est profond, il est limpide, son azur</l>
						<l n="6" num="1.6">Enseigne que, miroir du ciel, le coeur soit pur ;</l>
						<l n="7" num="1.7">Il est le visiteur invisible qui passe,</l>
						<l n="8" num="1.8">Se penche, et dont les doigts de douceur entrelacent,</l>
						<l n="9" num="1.9">Comme aux roses des murs ils mêlent les glycines,</l>
						<l n="10" num="1.10">Notre sourire humain à nos douleurs divines.</l>
						<l n="11" num="1.11">Il parle ; il nous remplit de tendresses confuses,</l>
						<l n="12" num="1.12">Pour rien, pour la chanson d’un pauvre qui s’éloigne,</l>
						<l n="13" num="1.13">Ou pour une fumée au ciel, pour une voile</l>
						<l n="14" num="1.14">Sur la mer. Le soir seul est notre hôte, on refuse</l>
						<l n="15" num="1.15">D’ouvrir aux passions qui frappent à la porte ;</l>
						<l n="16" num="1.16">L’âme qui laisse au loin s’affaiblir leurs voix fortes</l>
						<l n="17" num="1.17">Répand un chaste éclat d’étoile solitaire,</l>
						<l n="18" num="1.18">Mais devant la muette ivresse de la terre,</l>
						<l n="19" num="1.19">Devant le Dieu caché qui déborde la vie,</l>
						<l n="20" num="1.20">On pleure, on s’agenouille, on joint les mains, on prie.</l>
					</lg>
					<lg n="2">
						<l n="21" num="2.1">Souffles, voix… on croyait écouter Dieu qui parle,</l>
						<l n="22" num="2.2">Quand le seul vieil instinct charnel, hélas ! Chuchote.</l>
						<l n="23" num="2.3">Le soir est plein de bras ouverts, de lèvres chaudes,</l>
						<l n="24" num="2.4">D’yeux trop grands qu’on voudrait fermer avec des larmes.</l>
						<l n="25" num="2.5">Des murmures venus du fond de l’ombre appellent…</l>
						<l n="26" num="2.6">Le jardin défaillant cède à l’universelle</l>
						<l n="27" num="2.7">Volupté qui ravit les sphères dans leurs orbes.</l>
						<l n="28" num="2.8">La brise en effeuillant des roses fuit, agite</l>
						<l n="29" num="2.9">La treille ; l’air bleuit, les rossignols accordent</l>
						<l n="30" num="2.10">Le fébrile cristal de leurs flûtes magiques ;</l>
						<l n="31" num="2.11">L’herbe ondule au vent, l’eau bruit, et de la cime</l>
						<l n="32" num="2.12">Aux branches basses, l’arbre éperdu balbutie</l>
						<l n="33" num="2.13">Des mots que le désir secret de l’homme achève.</l>
						<l n="34" num="2.14">L’heure est comme une vierge avant les noces ; puis</l>
						<l n="35" num="2.15">La dernière clarté remonte au ciel : la nuit</l>
						<l n="36" num="2.16">Frissonnante descend sur le jardin qui rêve ;</l>
						<l n="37" num="2.17">Elle se pose, endort l’herbe ; l’arbre s’apaise ;</l>
						<l n="38" num="2.18">Et désormais, parmi l’immobile feuillage,</l>
						<l n="39" num="2.19">Le coeur ivre et gonflé reste seul inquiet.</l>
						<l n="40" num="2.20">Hélas ! Aimer, aimer encore, aimer toujours…</l>
						<l n="41" num="2.21">On lutte à peine, et sur l’appel de la chair lâche,</l>
						<l n="42" num="2.22">Sincèrement, comme on pleurait, comme on priait,</l>
						<l n="43" num="2.23">On reprend la chanson impure de l’amour.</l>
						<l n="44" num="2.24">Fièvre du sang qui va créer, mélancolie</l>
						<l n="45" num="2.25">De l’âme qui se sent mortelle et se délie</l>
						<l n="46" num="2.26">Et se fond dans un lourd sanglot de volupté !</l>
						<l n="47" num="2.27">Vers l’immense tristesse et l’immense bonté,</l>
						<l n="48" num="2.28">Vers la femme, fruit d’or où brûle tout l’été,</l>
						<l n="49" num="2.29">On tend ses mains enfin plus simplement humaines,</l>
						<l n="50" num="2.30">Et la nuit bienheureuse alors, paisible et pâle,</l>
						<l n="51" num="2.31">Emportant la terrestre idylle sous son aile,</l>
						<l n="52" num="2.32">Autour de ses tremblants enclos d’étoiles mène</l>
						<l n="53" num="2.33">Le choeur mystérieux des heures nuptiales.</l>
					</lg>
				</div></body></text></TEI>