<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Cœur Solitaire</title>
				<title type="medium">Édition électronique</title>
				<author key="GUE">
					<name>
						<forename>Charles</forename>
						<surname>GUÉRIN</surname>
					</name>
					<date from="1873" to="1907">1873-1907</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2116 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">GUE_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Cœur Solitaire</title>
						<author>Charles Guérin</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">https://www.poesies.net/charlesguerinlecœursolitaire.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition papier d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Le Cœur Solitaire</title>
						<author>Charles Guérin</author>
						<idno type="URI">https://archive.org/details/lecoeursolitair00gu</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LES ÉDITIONS G. GRÈS ET Cie</publisher>
							<date when="1922">1922</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Le Cœur Solitaire</title>
						<author>Charles Guérin</author>
						<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k54873c.r=%22charles%20gu%C3%A9rin%22%22le%20coeur%20solitaire%22?rk=64378;0</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Mercure de France</publisher>
							<date when="1904">1904</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1895">1895</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les majuscules en début de vers ont été rétablies.</p>
					<p>Les guillemets simples ont été remplacés par des guillemets français.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-11-08" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-11-08" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">VI</head><head type="main_part">A ÉMile Krantz</head><div type="poem" key="GUE52" rhyme="none">
					<head type="number">LII</head>
					<lg n="1">
						<l n="1" num="1.1"><space unit="char" quantity="8"></space>Puisque l’ennui, pauvre homme,</l>
						<l n="2" num="1.2">Te jette encore à de nouveaux voyages,</l>
						<l n="3" num="1.3"><space unit="char" quantity="8"></space>Emporte au moins dans l’âme</l>
						<l n="4" num="1.4">L’adieu doré des beaux jours de l’automne.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">Comme un baiser l’après-midi s’achève ;</l>
						<l n="6" num="2.2"><space unit="char" quantity="8"></space>La brise est large et pure,</l>
						<l n="7" num="2.3">Et toute voix se fond dans le murmure</l>
						<l n="8" num="2.4"><space unit="char" quantity="8"></space>Religieux des chênes.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><space unit="char" quantity="8"></space>Le meunier passe, écoute,</l>
						<l n="10" num="3.2">Son grelot clair tinte au loin sur la route ;</l>
						<l n="11" num="3.3"><space unit="char" quantity="8"></space>Et l’eau mélodieuse</l>
						<l n="12" num="3.4">Se baise et rit dans les canaux d’yeuse.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><space unit="char" quantity="8"></space>L’ombre descend les berges.</l>
						<l n="14" num="4.2">Déjà les champs sont pleins de brumes bleues ;</l>
						<l n="15" num="4.3"><space unit="char" quantity="8"></space>Le ruisseau dans les herbes</l>
						<l n="16" num="4.4">Y fait briller ses reflets de couleuvre.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><space unit="char" quantity="8"></space>Ton esprit se recueille ;</l>
						<l n="18" num="5.2">Secrètement le cristal de ton âme</l>
						<l n="19" num="5.3">S’émeut des cris d’oiseaux, d’un pas de femme</l>
						<l n="20" num="5.4"><space unit="char" quantity="8"></space>Qui craque dans les feuilles…</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><space unit="char" quantity="8"></space>Regagne la maison ;</l>
						<l n="22" num="6.2">Comme toujours elle est blanche, humble et calme,</l>
						<l n="23" num="6.3">Et sur son mur les branches du platane</l>
						<l n="24" num="6.4"><space unit="char" quantity="8"></space>Entrecroisent leurs ombres.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1">Le vieux soleil, tombant en molles nappes,</l>
						<l n="26" num="7.2"><space unit="char" quantity="8"></space>Bénit les vieilles pierres ;</l>
						<l n="27" num="7.3">La vigne jaune à sa tiède lumière</l>
						<l n="28" num="7.4"><space unit="char" quantity="8"></space>Mûrit encor des grappes ;</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1"><space unit="char" quantity="8"></space>Et dans la chambre basse,</l>
						<l n="30" num="8.2">Au souffle d’air qui pousse la fenêtre,</l>
						<l n="31" num="8.3">Obscurément celles qui t’ont vu naître</l>
						<l n="32" num="8.4"><space unit="char" quantity="8"></space>Parlent de leurs voix lasses.</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1">En maniant leurs crochets à dentelle</l>
						<l n="34" num="9.2"><space unit="char" quantity="8"></space>Aux derniers feux du jour :</l>
						<l n="35" num="9.3">« Reverrons-nous notre enfant ? « disent-elles ;</l>
						<l n="36" num="9.4"><space unit="char" quantity="8"></space>Et la tristesse, aux joues</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1"><space unit="char" quantity="8"></space>Des aïeules pensives,</l>
						<l n="38" num="10.2">Suspend des pleurs lourds comme les années ;</l>
						<l n="39" num="10.3">Le soir ainsi fait trembler sa rosée</l>
						<l n="40" num="10.4"><space unit="char" quantity="8"></space>Sur les roses tardives.</l>
					</lg>
					<lg n="11">
						<l n="41" num="11.1"><space unit="char" quantity="8"></space>Séjour, heures paisibles !</l>
						<l n="42" num="11.2">Demain pourtant les premières étoiles</l>
						<l n="43" num="11.3">Verront ton navire arrondir ses voiles</l>
						<l n="44" num="11.4"><space unit="char" quantity="8"></space>Et voguer vers les îles.</l>
					</lg>
					<lg n="12">
						<l n="45" num="12.1">Avant d’aller traîner ta vieille peine</l>
						<l n="46" num="12.2"><space unit="char" quantity="8"></space>Sur de lointains rivages,</l>
						<l n="47" num="12.3">Écoute encor la voix grave des chênes,</l>
						<l n="48" num="12.4"><space unit="char" quantity="8"></space>Contemple le village.</l>
					</lg>
					<lg n="13">
						<l n="49" num="13.1"><space unit="char" quantity="8"></space>Harmonie et douceur !</l>
						<l n="50" num="13.2">Le toit natal fume dans la lumière…</l>
						<l n="51" num="13.3"><space unit="char" quantity="8"></space>Parle, dis-moi, mon frère,</l>
						<l n="52" num="13.4">Pourquoi si loin chercher la paix du coeur ?</l>
					</lg>
				</div></body></text></TEI>