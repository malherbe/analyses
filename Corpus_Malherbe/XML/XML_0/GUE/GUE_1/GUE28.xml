<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Cœur Solitaire</title>
				<title type="medium">Édition électronique</title>
				<author key="GUE">
					<name>
						<forename>Charles</forename>
						<surname>GUÉRIN</surname>
					</name>
					<date from="1873" to="1907">1873-1907</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2116 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">GUE_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Cœur Solitaire</title>
						<author>Charles Guérin</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">https://www.poesies.net/charlesguerinlecœursolitaire.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition papier d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Le Cœur Solitaire</title>
						<author>Charles Guérin</author>
						<idno type="URI">https://archive.org/details/lecoeursolitair00gu</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LES ÉDITIONS G. GRÈS ET Cie</publisher>
							<date when="1922">1922</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Le Cœur Solitaire</title>
						<author>Charles Guérin</author>
						<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k54873c.r=%22charles%20gu%C3%A9rin%22%22le%20coeur%20solitaire%22?rk=64378;0</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Mercure de France</publisher>
							<date when="1904">1904</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1895">1895</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les majuscules en début de vers ont été rétablies.</p>
					<p>Les guillemets simples ont été remplacés par des guillemets français.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-11-08" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-11-08" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">IV</head><head type="main_part">MÉLANCOLIES A VIOLLIS</head><div type="poem" key="GUE28">
					<head type="number">XXVIII</head>
					<lg n="1">
						<l n="1" num="1.1">La maison serait blanche et le jardin sonore</l>
						<l n="2" num="1.2"><space unit="char" quantity="8"></space>De bruits d’eaux vives et d’oiseaux,</l>
						<l n="3" num="1.3">Et le lierre du mur qui regarde l’aurore</l>
						<l n="4" num="1.4"><space unit="char" quantity="8"></space>Broderait d’ombres les rideaux</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">Du lit tiède où, mêlés comme deux tourterelles,</l>
						<l n="6" num="2.2"><space unit="char" quantity="8"></space>Las d’un voluptueux sommeil,</l>
						<l n="7" num="2.3">Nous souririons, heureux de nous sentir des ailes</l>
						<l n="8" num="2.4"><space unit="char" quantity="8"></space>Aux premiers rayons du soleil.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">Cette maison n’aurait sous l’auvent qu’un étage</l>
						<l n="10" num="3.2"><space unit="char" quantity="8"></space>Au balcon noyé de jasmins.</l>
						<l n="11" num="3.3">Les fleurs, le miel, ô mon amie, et le laitage</l>
						<l n="12" num="3.4"><space unit="char" quantity="8"></space>Aromatiseraient tes mains.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">Un fleuve baignerait nos vergers, et sa rive</l>
						<l n="14" num="4.2"><space unit="char" quantity="8"></space>Cacherait parmi les roseaux</l>
						<l n="15" num="4.3">Une barque bercée et dont la rame oisive</l>
						<l n="16" num="4.4"><space unit="char" quantity="8"></space>Miroite en divisant les eaux.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">Nous resterions longtemps assis sur la terrasse,</l>
						<l n="18" num="5.2"><space unit="char" quantity="8"></space>Le soir, lorsqu’entre ciel et champ</l>
						<l n="19" num="5.3">Le piétinant troupeau pressé des brebis passe</l>
						<l n="20" num="5.4"><space unit="char" quantity="8"></space>Dans la lumière du couchant ;</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1">Et nos coeurs répondraient à l’angelus qui sonne</l>
						<l n="22" num="6.2">Avec la foi des coeurs à qui la vie est bonne.</l>
					</lg>
					<lg n="7">
						<l n="23" num="7.1">Plus tard, sur le balcon rempli d’ombre, muets,</l>
						<l n="24" num="7.2">L’oreille ouverte au bruit des trains dans la vallée,</l>
						<l n="25" num="7.3">Goûtant tout ce qu’un sage amour contient de paix,</l>
						<l n="26" num="7.4">Nos âmes se fondraient dans la nuit étoilée.</l>
					</lg>
					<lg n="8">
						<l n="27" num="8.1">Écoutant nos enfants dormir derrière nous,</l>
						<l n="28" num="8.2">Pâle dans tes cheveux libres où l’air se joue,</l>
						<l n="29" num="8.3">Ta main fraîche liée aux miennes : « qu’il est doux,</l>
						<l n="30" num="8.4">Qu’il est doux, dirais-tu, les cils contre ma joue,</l>
					</lg>
					<lg n="9">
						<l n="31" num="9.1">Quand on sait où poser la tête, d’être las !  »</l>
						<l n="32" num="9.2">Mes lèvres fermeraient ta paupière endormie.</l>
						<l n="33" num="9.3">Cher asile, jardin, maison rustique… hélas !</l>
						<l n="34" num="9.4">Car nous rêvons quand il faut vivre, ô mon amie !</l>
					</lg>
				</div></body></text></TEI>