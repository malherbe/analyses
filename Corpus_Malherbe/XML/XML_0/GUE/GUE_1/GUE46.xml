<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Cœur Solitaire</title>
				<title type="medium">Édition électronique</title>
				<author key="GUE">
					<name>
						<forename>Charles</forename>
						<surname>GUÉRIN</surname>
					</name>
					<date from="1873" to="1907">1873-1907</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2116 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">GUE_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Cœur Solitaire</title>
						<author>Charles Guérin</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">https://www.poesies.net/charlesguerinlecœursolitaire.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition papier d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Le Cœur Solitaire</title>
						<author>Charles Guérin</author>
						<idno type="URI">https://archive.org/details/lecoeursolitair00gu</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LES ÉDITIONS G. GRÈS ET Cie</publisher>
							<date when="1922">1922</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Le Cœur Solitaire</title>
						<author>Charles Guérin</author>
						<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k54873c.r=%22charles%20gu%C3%A9rin%22%22le%20coeur%20solitaire%22?rk=64378;0</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Mercure de France</publisher>
							<date when="1904">1904</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1895">1895</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les majuscules en début de vers ont été rétablies.</p>
					<p>Les guillemets simples ont été remplacés par des guillemets français.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-11-08" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-11-08" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">V</head><head type="main_part">A LA MEMOIRE DE SAMAIN</head><div type="poem" key="GUE46">
					<head type="number">XLVI</head>
					<opener>
						<salute>A José-Maria De Hérédia.</salute>
					</opener>
					<lg n="1">
						<l n="1" num="1.1">Être le jeune Adam, grâce et force première,</l>
						<l n="2" num="1.2">Dont les yeux lourds encor s’ouvrent à la lumière.</l>
						<l n="3" num="1.3">Il s’étonne, se tait, regarde autour de lui,</l>
						<l n="4" num="1.4">Marche avec les lenteurs d’un enfant ébloui,</l>
						<l n="5" num="1.5">Se voit nu, se caresse et s’admire, et soudain,</l>
						<l n="6" num="1.6">Enivré par l’odeur des sèves de l’éden,</l>
						<l n="7" num="1.7">Gravit d’un jarret prompt les collines bleuâtres</l>
						<l n="8" num="1.8">Parmi l’herbe mouillée et les troupeaux sans pâtres,</l>
						<l n="9" num="1.9">Il s’arrête à goûter l’ombre de la forêt,</l>
						<l n="10" num="1.10">Et, couchant, près d’une eau qui murmure en secret,</l>
						<l n="11" num="1.11">Son corps souple où la ronce inoffensive glisse,</l>
						<l n="12" num="1.12">Il respire l’air neuf de l’aube avec délice,</l>
						<l n="13" num="1.13">Chevauche le rayon, chante, éveille l’écho,</l>
						<l n="14" num="1.14">Sourit à l’inconnu qu’il voit, dans le ruisseau,</l>
						<l n="15" num="1.15">Vermeil et languissant de bonheur, lui sourire.</l>
						<l n="16" num="1.16">Un large papillon qui se pose l’attire,</l>
						<l n="17" num="1.17">Deux chevreaux affrontés le charment par leurs jeux.</l>
						<l n="18" num="1.18">Mais le rouge zénith épanouit ses feux.</l>
						<l n="19" num="1.19">Adam que le désir caresse de son aile</l>
						<l n="20" num="1.20">Étreint entre ses bras la terre maternelle.</l>
						<l n="21" num="1.21">Sar chair où le limon se mêle avec le jour</l>
						<l n="22" num="1.22">Appelle sourdement une épouse et l’amour ;</l>
						<l n="23" num="1.23">Il meurtrit de baisers l’arbre, embrasse l’écorce,</l>
						<l n="24" num="1.24">S’épuise à déplorer son inutile force…</l>
						<l n="25" num="1.25">Bientôt la lente nuit, le remplissant d’horreur,</l>
						<l n="26" num="1.26">Recouvre d’une ruche obscure la rumeur</l>
						<l n="27" num="1.27">Que font les eaux des mers et le vent des ravines.</l>
						<l n="28" num="1.28">La lune, pâle encor, change sur les collines</l>
						<l n="29" num="1.29">Toute rosée en perle et toute fleur en lys.</l>
						<l n="30" num="1.30">La brise porte au loin des échos affaiblis</l>
						<l n="31" num="1.31">De ramages, d’appels amoureux, de murmures.</l>
						<l n="32" num="1.32">Le flot paresseux roule un ciel d’étoiles pures,</l>
						<l n="33" num="1.33">Et, sous les voûtes d’ombre où les grands animaux</l>
						<l n="34" num="1.34">D’un front lourd en passant écartent les rameaux,</l>
						<l n="35" num="1.35">Le jeune Adam, muet d’ivresse et d’épouvante,</l>
						<l n="36" num="1.36">Dans ses flancs douloureux sent vivre Ève naissante.</l>
						<l n="37" num="1.37">Adam, le front rougi du soleil levant, rêve</l>
						<l n="38" num="1.38">Auprès du corps humide et voluptueux d’Ève.</l>
						<l n="39" num="1.39">Ève dort sur un lit fléchissant de roseaux,</l>
						<l n="40" num="1.40">Dans l’azur frais, au bruit du feuillage et des eaux.</l>
						<l n="41" num="1.41">Ève est nue, Ève est blanche, Ève a les lignes pures</l>
						<l n="42" num="1.42">Des longs cygnes cambrés aux neigeuses voilures.</l>
						<l n="43" num="1.43">Comme une aile elle agite un bras, puis l’autre, et rit.</l>
						<l n="44" num="1.44">Sa bouche, rose en feu, lente à s’ouvrir, fleurit.</l>
						<l n="45" num="1.45">Ève est nue ; elle dort. Sa chevelure blonde</l>
						<l n="46" num="1.46">Sur ses formes répand les mollesses d’une onde.</l>
						<l n="47" num="1.47">Son haleine paisible élève un double fruit</l>
						<l n="48" num="1.48">Gonflé qui tour à tour se rapproche et se fuit.</l>
						<l n="49" num="1.49">Adam parcourt des yeux sa fille et la convoite.</l>
						<l n="50" num="1.50">Il ose la flatter d’une main maladroite.</l>
						<l n="51" num="1.51">Jamais dans la nature heureuse il n’a connu</l>
						<l n="52" num="1.52">Le délice qu’il goûte à toucher ce sein nu.</l>
						<l n="53" num="1.53">Les cheveux qu’il respire ont une odeur obscure ;</l>
						<l n="54" num="1.54">Ni le miel, ni le col des cygnes qu’il capture,</l>
						<l n="55" num="1.55">Ni la feuille du lys, ni l’enivrant pollen,</l>
						<l n="56" num="1.56">Ne lui semblent si doux sous le ciel de l’éden</l>
						<l n="57" num="1.57">Que cette large fleur de chair épanouie.</l>
						<l n="58" num="1.58">Il la caresse encor d’une vue éblouie,</l>
						<l n="59" num="1.59">Y porte son désir de contour en contour ;</l>
						<l n="60" num="1.60">Enfin, docile aux lois secrètes de l’amour</l>
						<l n="61" num="1.61">Qui font qu’un même nid cache deux tourterelles</l>
						<l n="62" num="1.62">Et que les fleurs de loin se fécondent entre elles,</l>
						<l n="63" num="1.63">Adam, fort de la joie immortelle du sang,</l>
						<l n="64" num="1.64">Presse le corps promis, et déjà rougissant,</l>
						<l n="65" num="1.65">D’un long baiser qui laisse une trace vermeille.</l>
						<l n="66" num="1.66">Ève, les bras ouverts au jeune époux, s’éveille.</l>
					</lg>
				</div></body></text></TEI>