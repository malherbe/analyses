<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Cœur Solitaire</title>
				<title type="medium">Édition électronique</title>
				<author key="GUE">
					<name>
						<forename>Charles</forename>
						<surname>GUÉRIN</surname>
					</name>
					<date from="1873" to="1907">1873-1907</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2116 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">GUE_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Cœur Solitaire</title>
						<author>Charles Guérin</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">https://www.poesies.net/charlesguerinlecœursolitaire.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition papier d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Le Cœur Solitaire</title>
						<author>Charles Guérin</author>
						<idno type="URI">https://archive.org/details/lecoeursolitair00gu</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LES ÉDITIONS G. GRÈS ET Cie</publisher>
							<date when="1922">1922</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Le Cœur Solitaire</title>
						<author>Charles Guérin</author>
						<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k54873c.r=%22charles%20gu%C3%A9rin%22%22le%20coeur%20solitaire%22?rk=64378;0</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Mercure de France</publisher>
							<date when="1904">1904</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1895">1895</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les majuscules en début de vers ont été rétablies.</p>
					<p>Les guillemets simples ont été remplacés par des guillemets français.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-11-08" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-11-08" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">VIII</head><head type="main_part">L’INQUIETUDE DE DIEU</head><div type="poem" key="GUE59" rhyme="none">
					<head type="number">LIX</head>
					<lg n="1">
						<l n="1" num="1.1">Jardinier, jardinier, que ta maison soit gaie,</l>
						<l n="2" num="1.2">Ton rucher en rumeur et ta chambre à fruits pleine,</l>
						<l n="3" num="1.3">Et que le thym s’argente au fil de ta fontaine !</l>
						<l n="4" num="1.4">Si quelque mendiant pleure contre ta haie,</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">Ouvre un coeur attentif au pauvre homme et l’accueille ;</l>
						<l n="6" num="2.2">Ses larmes béniront ta bêche avant la tâche :</l>
						<l n="7" num="2.3">Que ta vie, ô mon fils, sous tes actes se cache,</l>
						<l n="8" num="2.4">Odorante senteur de rose entre les feuilles.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">Sois simple. Prie à l’heure où rentrent les colombes.</l>
						<l n="10" num="3.2">Laisse la foi paisible avec le soir qui tombe</l>
						<l n="11" num="3.3">Grandir en toi comme un pan d’ombre sur le sable ;</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1">Et Dieu te fasse pur et bon, Dieu veuille rendre</l>
						<l n="13" num="4.2">L’âme qu’on voit au fond de tes yeux clairs semblable</l>
						<l n="14" num="4.3">Au caillou blanc qui luit sous une eau transparente.</l>
					</lg>
				</div></body></text></TEI>