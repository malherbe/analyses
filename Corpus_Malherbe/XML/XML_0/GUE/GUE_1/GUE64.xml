<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Cœur Solitaire</title>
				<title type="medium">Édition électronique</title>
				<author key="GUE">
					<name>
						<forename>Charles</forename>
						<surname>GUÉRIN</surname>
					</name>
					<date from="1873" to="1907">1873-1907</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2116 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">GUE_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Cœur Solitaire</title>
						<author>Charles Guérin</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">https://www.poesies.net/charlesguerinlecœursolitaire.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition papier d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Le Cœur Solitaire</title>
						<author>Charles Guérin</author>
						<idno type="URI">https://archive.org/details/lecoeursolitair00gu</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LES ÉDITIONS G. GRÈS ET Cie</publisher>
							<date when="1922">1922</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Le Cœur Solitaire</title>
						<author>Charles Guérin</author>
						<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k54873c.r=%22charles%20gu%C3%A9rin%22%22le%20coeur%20solitaire%22?rk=64378;0</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Mercure de France</publisher>
							<date when="1904">1904</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1895">1895</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les majuscules en début de vers ont été rétablies.</p>
					<p>Les guillemets simples ont été remplacés par des guillemets français.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-11-08" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-11-08" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">VIII</head><head type="main_part">L’INQUIETUDE DE DIEU</head><div type="poem" key="GUE64" rhyme="none">
					<head type="number">LXIV</head>
					<lg n="1">
						<l n="1" num="1.1">Entrerai-je, ce soir, seigneur, dans ta maison,</l>
						<l n="2" num="1.2">Sans craindre que ma chair, vouée aux oeuvres viles,</l>
						<l n="3" num="1.3">Apporte le relent de luxure des villes</l>
						<l n="4" num="1.4">À la candeur des jupes d’ombre en oraison ?</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">Je songe à d’autres jupes d’ombre qui sont douces</l>
						<l n="6" num="2.2">Pour endormir l’effroi des poètes malades,</l>
						<l n="7" num="2.3">À des doigts alourdis d’anneaux aux pierres troubles,</l>
						<l n="8" num="2.4">Troubles comme des yeux menteurs, comme mon âme.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">Entrerai-je, ce soir, seigneur, dans ta maison,</l>
						<l n="10" num="3.2">Si mon haleine tord l’humble flamme des cierges,</l>
						<l n="11" num="3.3">Si ma prière même inquiète les vierges,</l>
						<l n="12" num="3.4">Eau claire où s’élargit la chute d’un poison ?</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">Je songe à des toisons souples de courtisanes</l>
						<l n="14" num="4.2">Où les désespérés enfouissent leur songe,</l>
						<l n="15" num="4.3">Bonnes toisons qui font la nuit sur les visages,</l>
						<l n="16" num="4.4">Lourdes comme l’amour, sourdes comme des tombes.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">Que votre main soit rude et juste et me châtie,</l>
						<l n="18" num="5.2">Seigneur, seigneur, moi qui voudrais tant vous aimer !</l>
						<l n="19" num="5.3">Laissez, lasse de cris, ma bouche se fermer,</l>
						<l n="20" num="5.4">Pour la rouvrir vous-même ensuite avec l’hostie.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1">Je songe aux nuits de joie ivres et douloureuses</l>
						<l n="22" num="6.2">Où ma soif, accoudée à des tables mauvaises,</l>
						<l n="23" num="6.3">Se versait les boissons de flamme dont s’abreuvent</l>
						<l n="24" num="6.4">Ceux que serre à la gorge un ancien sacrilège.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1">Je viens vers vous, du fond de mon iniquité,</l>
						<l n="26" num="7.2">Je viens vers vous, seigneur, à qui les enfants parlent,</l>
						<l n="27" num="7.3">De tout mon bon vouloir et de toutes mes larmes,</l>
						<l n="28" num="7.4">Être triste avec vous, moi qui vous attristai.</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1">L’immémorial faix de péchés, le fardeau</l>
						<l n="30" num="8.2">De luxure et d’orgueil creuse mes reins qui saignent.</l>
						<l n="31" num="8.3">Aux margelles des puits nulle samaritaine</l>
						<l n="32" num="8.4">N’a tendu vers ma soif ses paumes pleines d’eau.</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1">Oubliez que je fus des serviteurs indignes ;</l>
						<l n="34" num="9.2">Et dans l’ombre que font les collines, le soir,</l>
						<l n="35" num="9.3">Celui qui cherche l’âtre et la pierre où s’asseoir</l>
						<l n="36" num="9.4">Sentira qu’un pardon se couche sur les vignes.</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1">La nuit tombe et m’arrête où dort votre maison ;</l>
						<l n="38" num="10.2">Les ramiers se sont tus, mais les fontaines chantent,</l>
						<l n="39" num="10.3">Fraîcheur obscure, en palpitant pour que j’y trempe</l>
						<l n="40" num="10.4">Mes mains, l’aridité de ma bouche et mon front.</l>
					</lg>
					<lg n="11">
						<l n="41" num="11.1">L’eau froide et pure emportera vers les ténèbres</l>
						<l n="42" num="11.2">Le souvenir fiévreux d’un passé de caresses,</l>
						<l n="43" num="11.3">La mémoire des voix, des regards et des gestes,</l>
						<l n="44" num="11.4">Et le souffle de feu qui brûle encor mes lèvres.</l>
					</lg>
					<lg n="12">
						<l n="45" num="12.1">Faites, seigneur, miséricorde à ma faiblesse,</l>
						<l n="46" num="12.2">À cette toute faiblesse des pauvres âmes</l>
						<l n="47" num="12.3">Qui n’ont pleuré que pour la chair tiède des femmes.</l>
						<l n="48" num="12.4">Que je souffre, seigneur, des ronces qui vous blessent ;</l>
					</lg>
					<lg n="13">
						<l n="49" num="13.1">Que la croupe des boucs crispés sur le portail</l>
						<l n="50" num="13.2">Serve d’éternel lieu d’exil à mes péchés,</l>
						<l n="51" num="13.3">Et que la palme offerte aux coeurs purifiés</l>
						<l n="52" num="13.4">Exalte en moi l’azur des vierges du vitrail.</l>
					</lg>
					<lg n="14">
						<l n="53" num="14.1">Je serai digne alors de gravir, humble et pâle,</l>
						<l n="54" num="14.2">Le seuil de gloire où les rois même parlent bas,</l>
						<l n="55" num="14.3">Et mon coeur et mes pieds nus ne sentiront pas</l>
						<l n="56" num="14.4">Le froid de la divine espérance et des dalles.</l>
					</lg>
					<lg n="15">
						<l n="57" num="15.1">… cette prière, hélas ! N’est-ce pas seulement</l>
						<l n="58" num="15.2">Le glas que sur soi-même agite une âme simple</l>
						<l n="59" num="15.3">À qui les yeux naïfs de ses chagrins d’enfant</l>
						<l n="60" num="15.4">Ont souri tristement du plus loin de leurs limbes ?</l>
					</lg>
					<lg n="16">
						<l n="61" num="16.1">N’est-ce pas le glas lourd du vain rêve que font</l>
						<l n="62" num="16.2">Dans leurs soirs douloureux les vieilles fois qui meurent :</l>
						<l n="63" num="16.3">Entrerai-je, nocturne et las, dans la maison</l>
						<l n="64" num="16.4">Où le maître de vie ineffable demeure ?</l>
					</lg>
				</div></body></text></TEI>