<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Cœur Solitaire</title>
				<title type="medium">Édition électronique</title>
				<author key="GUE">
					<name>
						<forename>Charles</forename>
						<surname>GUÉRIN</surname>
					</name>
					<date from="1873" to="1907">1873-1907</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2116 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">GUE_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Cœur Solitaire</title>
						<author>Charles Guérin</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">https://www.poesies.net/charlesguerinlecœursolitaire.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition papier d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Le Cœur Solitaire</title>
						<author>Charles Guérin</author>
						<idno type="URI">https://archive.org/details/lecoeursolitair00gu</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LES ÉDITIONS G. GRÈS ET Cie</publisher>
							<date when="1922">1922</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Le Cœur Solitaire</title>
						<author>Charles Guérin</author>
						<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k54873c.r=%22charles%20gu%C3%A9rin%22%22le%20coeur%20solitaire%22?rk=64378;0</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Mercure de France</publisher>
							<date when="1904">1904</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1895">1895</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les majuscules en début de vers ont été rétablies.</p>
					<p>Les guillemets simples ont été remplacés par des guillemets français.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-11-08" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-11-08" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">VII</head><head type="main_part">AUTOMNE, A MR LAFARGUE</head><div type="poem" key="GUE54" rhyme="none">
					<head type="number">LIV</head>
					<lg n="1">
						<l n="1" num="1.1">Ô veille de Toussaint et dernier soir d’octobre !</l>
						<l n="2" num="1.2">Le ciel est une ruche où bourdonnent les cloches,</l>
						<l n="3" num="1.3">Et le soleil pâlit sur le jardin doré :</l>
						<l n="4" num="1.4">De même, à l’occident large et pur de ma vie,</l>
						<l n="5" num="1.5">Dans un suprême adieu d’amour je descendrai.</l>
						<l n="6" num="1.6">La glycine, crispée, avec mélancolie</l>
						<l n="7" num="1.7">Se balance au perron de la maison natale,</l>
						<l n="8" num="1.8">Et, des arbres, du sol, des massifs nus, s’exhale</l>
						<l n="9" num="1.9">L’amer et froid parfum du vieil âge des choses.</l>
						<l n="10" num="1.10">Je viens, boutons de miel, de chair, de nacre mauve,</l>
						<l n="11" num="1.11">Vous cueillir pour ma belle enfant, roses tardives ;</l>
						<l n="12" num="1.12">Car mes doigts prévoyants, demain, arquant les tiges,</l>
						<l n="13" num="1.13">Confieront les rosiers délicats à la terre.</l>
						<l n="14" num="1.14">Des cristaux meurtriers de l’hiver, nulle main</l>
						<l n="15" num="1.15">Ne sut garder ta sève, arbuste solitaire,</l>
						<l n="16" num="1.16">Fier rosier qu’étoilaient des roses merveilleuses :</l>
						<l n="17" num="1.17">Tu n’es plus qu’un bois sec, inutile au jardin ;</l>
						<l n="18" num="1.18">Et les printemps pressés comme les flots d’un fleuve,</l>
						<l n="19" num="1.19">Les printemps lumineux et riches qui fécondent</l>
						<l n="20" num="1.20">Dans les sillons du ciel d’obscurs germes de mondes,</l>
						<l n="21" num="1.21">Et comme un front humain aux battements du rêve</l>
						<l n="22" num="1.22">Font palpiter le coeur de l’arbre sous la sève,</l>
						<l n="23" num="1.23">Tous les printemps, souffles d’air chauds et soleils d’or,</l>
						<l n="24" num="1.24">Ne rendront pas ses fleurs de chair au rosier mort.</l>
					</lg>
					<lg n="2">
						<l n="25" num="2.1">Dans le jardin jauni des anciennes années,</l>
						<l n="26" num="2.2">Parfois, quand le jour las tend ses bras à la nuit,</l>
						<l n="27" num="2.3">La belle enfant qui fut jadis ma bien-aimée</l>
						<l n="28" num="2.4">Passe en glissant d’un pas léger le long des buis.</l>
					</lg>
					<lg n="3">
						<l n="29" num="3.1">Elle s’arrête auprès du rosier nu, lui parle,</l>
						<l n="30" num="3.2">Lève les cils, remplit d’étoiles ses yeux pâles,</l>
						<l n="31" num="3.3">Et sourit dans son rêve aux calices rosés</l>
						<l n="32" num="3.4">Où ses lèvres, un jour, apprirent le baiser.</l>
					</lg>
				</div></body></text></TEI>