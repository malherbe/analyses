<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Ïambes et poèmes</title>
				<title type="medium">Édition électronique</title>
				<author key="BRB">
					<name>
						<forename>Auguste</forename>
						<surname>BARBIER</surname>
					</name>
					<date from="1805" to="1882">1805-1882</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement métrique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4203 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BRB_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Iambes et poèmes</title>
						<author>Auguste BARBIER</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L947</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Iambes et poèmes</title>
								<author>Auguste BARBIER</author>
								<idno type="URL">https://archive.org/details/iambesetpomes03barbgoog</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Paul Masgana</publisher>
									<date when="1840">1840</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1831">1831</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ÏAMBES</head><div type="poem" key="BRB3">
					<head type="main">LA CURÉE</head>
					<div type="section" n="1">
						<head type="number">I</head>
						<lg n="1">
							<l n="1" num="1.1">Oh ! Lorsqu’un lourd soleil chauffait les grandes dalles</l>
							<l n="2" num="1.2"><space quantity="10" unit="char"></space>Des ponts et de nos quais déserts,</l>
							<l n="3" num="1.3">Que les cloches hurlaient, que la grêle des balles</l>
							<l n="4" num="1.4"><space quantity="10" unit="char"></space>Sifflait et pleuvait par les airs ;</l>
							<l n="5" num="1.5">Que dans Paris entier, comme la mer qui monte,</l>
							<l n="6" num="1.6"><space quantity="10" unit="char"></space>Le peuple soulevé grondait,</l>
							<l n="7" num="1.7">Et qu’au lugubre accent des vieux canons de fonte</l>
							<l n="8" num="1.8"><space quantity="10" unit="char"></space>La marseillaise répondait,</l>
							<l n="9" num="1.9">Certe, on ne voyait pas, comme au jour où nous sommes,</l>
							<l n="10" num="1.10"><space quantity="10" unit="char"></space>Tant d’uniformes à la fois :</l>
							<l n="11" num="1.11">C’était sous des haillons que battaient les cœurs d’hommes ;</l>
							<l n="12" num="1.12"><space quantity="10" unit="char"></space>C’était alors de sales doigts</l>
							<l n="13" num="1.13">Qui chargeaient les mousquets et renvoyaient la foudre ;</l>
							<l n="14" num="1.14"><space quantity="10" unit="char"></space>C’était la bouche aux vils jurons</l>
							<l n="15" num="1.15">Qui mâchait la cartouche, et qui, noire de poudre,</l>
							<l n="16" num="1.16"><space quantity="10" unit="char"></space>Criait aux citoyens : mourons !</l>
						</lg>
					</div>
					<div type="section" n="2">
						<head type="number">II</head>
						<lg n="1">
							<l n="17" num="1.1">Quant à tous ces beaux fils aux tricolores flammes,</l>
							<l n="18" num="1.2"><space quantity="10" unit="char"></space>Au beau linge, au frac élégant,</l>
							<l n="19" num="1.3">Ces hommes en corsets, ces visages de femmes,</l>
							<l n="20" num="1.4"><space quantity="10" unit="char"></space>Héros du boulevard de Gand,</l>
							<l n="21" num="1.5">Que faisaient-ils, tandis qu’à travers la mitraille,</l>
							<l n="22" num="1.6"><space quantity="10" unit="char"></space>Et sous le sabre détesté,</l>
							<l n="23" num="1.7">La grande populace et la sainte canaille</l>
							<l n="24" num="1.8"><space quantity="10" unit="char"></space>Se ruaient à l’immortalité ?</l>
							<l n="25" num="1.9">Tandis que tout Paris se jonchait de merveilles,</l>
							<l n="26" num="1.10"><space quantity="10" unit="char"></space>Ces messieurs tremblaient dans leur peau,</l>
							<l n="27" num="1.11">Pâles, suant la peur, et la main aux oreilles,</l>
							<l n="28" num="1.12"><space quantity="10" unit="char"></space>Accroupis derrière un rideau.</l>
						</lg>
					</div>
					<div type="section" n="3">
						<head type="number">III</head>
						<lg n="1">
							<l n="29" num="1.1">C’est que la liberté n’est pas une comtesse</l>
							<l n="30" num="1.2"><space quantity="10" unit="char"></space>Du noble faubourg saint-Germain,</l>
							<l n="31" num="1.3">Une femme qu’un cri fait tomber en faiblesse,</l>
							<l n="32" num="1.4"><space quantity="10" unit="char"></space>Qui met du blanc et du carmin :</l>
							<l n="33" num="1.5">C’est une forte femme aux puissantes mamelles,</l>
							<l n="34" num="1.6"><space quantity="10" unit="char"></space>À la voix rauque, aux durs appas,</l>
							<l n="35" num="1.7">Qui, du brun sur la peau, du feu dans les prunelles,</l>
							<l n="36" num="1.8"><space quantity="10" unit="char"></space>Agile et marchant à grands pas,</l>
							<l n="37" num="1.9">Se plaît aux cris du peuple, aux sanglantes mêlées,</l>
							<l n="38" num="1.10"><space quantity="10" unit="char"></space>Aux longs roulements des tambours,</l>
							<l n="39" num="1.11">À l’odeur de la poudre, aux lointaines volées</l>
							<l n="40" num="1.12"><space quantity="10" unit="char"></space>Des cloches et des canons sourds ;</l>
							<l n="41" num="1.13">Qui ne prend ses amours que dans la populace,</l>
							<l n="42" num="1.14"><space quantity="10" unit="char"></space>Qui ne prête son large flanc</l>
							<l n="43" num="1.15">Qu’à des gens forts comme elle, et qui veut qu’on l’embrasse</l>
							<l n="44" num="1.16"><space quantity="10" unit="char"></space>Avec des bras rouges de sang.</l>
						</lg>
					</div>
					<div type="section" n="4">
						<head type="number">IV</head>
						<lg n="1">
							<l n="45" num="1.1">C’est la vierge fougueuse, enfant de la bastille,</l>
							<l n="46" num="1.2"><space quantity="10" unit="char"></space>Qui jadis, lorsqu’elle apparut</l>
							<l n="47" num="1.3">Avec son air hardi, ses allures de fille,</l>
							<l n="48" num="1.4"><space quantity="10" unit="char"></space>Cinq ans mit tout le peuple en rût ;</l>
							<l n="49" num="1.5">Qui, plus tard, entonnant une marche guerrière,</l>
							<l n="50" num="1.6"><space quantity="10" unit="char"></space>Lasse de ses premiers amants,</l>
							<l n="51" num="1.7">Jeta là son bonnet, et devint vivandière</l>
							<l n="52" num="1.8"><space quantity="10" unit="char"></space>D’un capitaine de vingt ans :</l>
							<l n="53" num="1.9">C’est cette femme, enfin, qui, toujours belle et nue,</l>
							<l n="54" num="1.10"><space quantity="10" unit="char"></space>Avec l’écharpe aux trois couleurs,</l>
							<l n="55" num="1.11">Dans nos murs mitraillés tout à coup reparue,</l>
							<l n="56" num="1.12"><space quantity="10" unit="char"></space>Vient de sécher nos yeux en pleurs,</l>
							<l n="57" num="1.13">De remettre en trois jours une haute couronne</l>
							<l n="58" num="1.14"><space quantity="10" unit="char"></space>Aux mains des français soulevés,</l>
							<l n="59" num="1.15">D’écraser une armée et de broyer un trône</l>
							<l n="60" num="1.16"><space quantity="10" unit="char"></space>Avec quelques tas de pavés.</l>
						</lg>
					</div>
					<div type="section" n="5">
						<head type="number">V</head>
						<lg n="1">
							<l n="61" num="1.1">Mais, ô honte ! Paris, si beau dans sa colère,</l>
							<l n="62" num="1.2"><space quantity="10" unit="char"></space>Paris, si plein de majesté</l>
							<l n="63" num="1.3">Dans ce jour de tempête où le vent populaire</l>
							<l n="64" num="1.4"><space quantity="10" unit="char"></space>Déracina la royauté ;</l>
							<l n="65" num="1.5">Paris, si magnifique avec ses funérailles,</l>
							<l n="66" num="1.6"><space quantity="10" unit="char"></space>Ses débris d’hommes, ses tombeaux,</l>
							<l n="67" num="1.7">Ses chemins dépavés et ses pans de murailles</l>
							<l n="68" num="1.8"><space quantity="10" unit="char"></space>Troués comme de vieux drapeaux ;</l>
							<l n="69" num="1.9">Paris, cette cité de lauriers toute ceinte,</l>
							<l n="70" num="1.10"><space quantity="10" unit="char"></space>Dont le monde entier est jaloux,</l>
							<l n="71" num="1.11">Que les peuples émus appellent tous la sainte,</l>
							<l n="72" num="1.12"><space quantity="10" unit="char"></space>Et qu’ils ne nomment qu’à genoux,</l>
							<l n="73" num="1.13">Paris n’est maintenant qu’une sentine impure,</l>
							<l n="74" num="1.14"><space quantity="10" unit="char"></space>Un égout sordide et boueux,</l>
							<l n="75" num="1.15">Où mille noirs courants de limon et d’ordure</l>
							<l n="76" num="1.16"><space quantity="10" unit="char"></space>Viennent traîner leurs flots honteux ;</l>
							<l n="77" num="1.17">Un taudis regorgeant de faquins sans courage,</l>
							<l n="78" num="1.18"><space quantity="10" unit="char"></space>D’effrontés coureurs de salons,</l>
							<l n="79" num="1.19">Qui vont de porte en porte, et d’étage en étage,</l>
							<l n="80" num="1.20"><space quantity="10" unit="char"></space>Gueusant quelque bout de galons ;</l>
							<l n="81" num="1.21">Une halle cynique aux clameurs insolentes,</l>
							<l n="82" num="1.22"><space quantity="10" unit="char"></space>Où chacun cherche à déchirer</l>
							<l n="83" num="1.23">Un misérable coin des guenilles sanglantes</l>
							<l n="84" num="1.24"><space quantity="10" unit="char"></space>Du pouvoir qui vient d’expirer.</l>
						</lg>
					</div>
					<div type="section" n="6">
						<head type="number">VI</head>
						<lg n="1">
							<l n="85" num="1.1">Ainsi, quand dans sa bauge aride et solitaire</l>
							<l n="86" num="1.2"><space quantity="10" unit="char"></space>Le sanglier, frappé de mort,</l>
							<l n="87" num="1.3">Est là, tout palpitant, étendu sur la terre,</l>
							<l n="88" num="1.4"><space quantity="10" unit="char"></space>Et sous le soleil qui le mord ;</l>
							<l n="89" num="1.5">Lorsque, blanchi de bave et la langue tirée,</l>
							<l n="90" num="1.6"><space quantity="10" unit="char"></space>Ne bougeant plus en ses liens,</l>
							<l n="91" num="1.7">Il meurt, et que la trompe a sonné la curée</l>
							<l n="92" num="1.8"><space quantity="10" unit="char"></space>À toute la meute des chiens,</l>
							<l n="93" num="1.9">Toute la meute, alors, comme une vague immense</l>
							<l n="94" num="1.10"><space quantity="10" unit="char"></space>Bondit ; alors chaque mâtin</l>
							<l n="95" num="1.11">Hurle en signe de joie, et prépare d’avance</l>
							<l n="96" num="1.12"><space quantity="10" unit="char"></space>Ses larges crocs pour le festin ;</l>
							<l n="97" num="1.13">Et puis vient la cohue, et les abois féroces</l>
							<l n="98" num="1.14"><space quantity="10" unit="char"></space>Roulent de vallons en vallons ;</l>
							<l n="99" num="1.15">Chiens courants et limiers, et dogues, et molosses,</l>
							<l n="100" num="1.16"><space quantity="10" unit="char"></space>Tout se lance, et tout crie : allons !</l>
							<l n="101" num="1.17">Quand le sanglier tombe et roule sur l’arène,</l>
							<l n="102" num="1.18"><space quantity="10" unit="char"></space>Allons ! Allons ! Les chiens sont rois !</l>
							<l n="103" num="1.19">Le cadavre est à nous ; payons-nous notre peine,</l>
							<l n="104" num="1.20"><space quantity="10" unit="char"></space>Nos coups de dents et nos abois.</l>
							<l n="105" num="1.21">Allons ! Nous n’avons plus de valet qui nous fouaille</l>
							<l n="106" num="1.22"><space quantity="10" unit="char"></space>Et qui se pende à notre cou :</l>
							<l n="107" num="1.23">Du sang chaud, de la chair, allons, faisons ripaille,</l>
							<l n="108" num="1.24"><space quantity="10" unit="char"></space>Et gorgeons-nous tout notre soûl !</l>
							<l n="109" num="1.25">Et tous, comme ouvriers que l’on met à la tâche,</l>
							<l n="110" num="1.26"><space quantity="10" unit="char"></space>Fouillent ces flancs à plein museau,</l>
							<l n="111" num="1.27">Et de l’ongle et des dents travaillent sans relâche,</l>
							<l n="112" num="1.28"><space quantity="10" unit="char"></space>Car chacun en veut un morceau ;</l>
							<l n="113" num="1.29">Car il faut au chenil que chacun d’eux revienne</l>
							<l n="114" num="1.30"><space quantity="10" unit="char"></space>Avec un os demi-rongé,</l>
							<l n="115" num="1.31">Et que, trouvant au seuil son orgueilleuse chienne,</l>
							<l n="116" num="1.32"><space quantity="10" unit="char"></space>Jalouse et le poil allongé,</l>
							<l n="117" num="1.33">Il lui montre sa gueule encor rouge, et qui grogne,</l>
							<l n="118" num="1.34"><space quantity="10" unit="char"></space>Son os dans les dents arrêté,</l>
							<l n="119" num="1.35">Et lui crie, en jetant son quartier de charogne :</l>
							<l n="120" num="1.36"><space quantity="10" unit="char"></space>« voici ma part de royauté ! »</l>
						</lg>
						<closer>
							<dateline>
								<date when="1830">août 1830.</date>
							</dateline>
						</closer>
					</div>
				</div></body></text></TEI>