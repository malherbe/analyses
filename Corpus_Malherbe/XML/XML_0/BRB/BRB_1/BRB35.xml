<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Ïambes et poèmes</title>
				<title type="medium">Édition électronique</title>
				<author key="BRB">
					<name>
						<forename>Auguste</forename>
						<surname>BARBIER</surname>
					</name>
					<date from="1805" to="1882">1805-1882</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement métrique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4203 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BRB_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Iambes et poèmes</title>
						<author>Auguste BARBIER</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L947</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Iambes et poèmes</title>
								<author>Auguste BARBIER</author>
								<idno type="URL">https://archive.org/details/iambesetpomes03barbgoog</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Paul Masgana</publisher>
									<date when="1840">1840</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1831">1831</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">IL PIANTO</head><div type="poem" key="BRB35">
					<head type="main">L’ADIEU</head>
					<lg n="1">
						<l n="1" num="1.1">Ah ! Quel que soit le deuil jeté sur cette terre</l>
						<l n="2" num="1.2">Qui par deux fois du monde a changé le destin,</l>
						<l n="3" num="1.3">Quels que soient ses malheurs et sa longue misère,</l>
						<l n="4" num="1.4">On ne peut la quitter sans peine et sans chagrin.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">Ainsi, près de sortir du céleste jardin,</l>
						<l n="6" num="2.2">Je me retourne encor sur les cimes hautaines,</l>
						<l n="7" num="2.3">Pour contempler de là son horizon divin</l>
						<l n="8" num="2.4">Et longtemps m’enivrer de ses grâces lointaines :</l>
						<l n="9" num="2.5">Et puis le froid me prend et me glace les veines</l>
						<l n="10" num="2.6">Et tout mon cœur soupire, oh ! Comme si j’avais,</l>
						<l n="11" num="2.7">Aux champs de l’Italie et dans ses larges plaines,</l>
						<l n="12" num="2.8">De mes jours effeuillé le rameau le plus frais,</l>
					</lg>
					<lg n="3">
						<l n="13" num="3.1">Et sur le sein vermeil de la brune déesse</l>
						<l n="14" num="3.2">Épuisé pour toujours ma vie et ma jeunesse.</l>
					</lg>
					<ab type="star"></ab>
					<lg n="4">
						<l n="15" num="4.1">Divine Juliette au cercueil étendue,</l>
						<l n="16" num="4.2">Toi qui n’es qu’endormie et que l’on croit perdue,</l>
						<l n="17" num="4.3">Italie, ô beauté ! Si malgré ta pâleur,</l>
						<l n="18" num="4.4">Tes membres ont encor gardé de la chaleur ;</l>
						<l n="19" num="4.5">Si du sang généreux coule encor dans ta veine ;</l>
						<l n="20" num="4.6">Si le monstre qui semble avoir bu ton haleine,</l>
						<l n="21" num="4.7">La mort, planant sur toi comme un heureux amant,</l>
						<l n="22" num="4.8">Pour toujours ne t’a pas clouée au monument ;</l>
						<l n="23" num="4.9">Si tu n’es pas enfin son entière conquête ;</l>
						<l n="24" num="4.10">Alors quelque beau jour tu lèveras la tête,</l>
						<l n="25" num="4.11">Et, privés bien longtemps du soleil, tes grands yeux</l>
						<l n="26" num="4.12">S’ouvriront pour revoir le pur éclat des cieux :</l>
						<l n="27" num="4.13">Puis ton corps ranimé par la chaude lumière,</l>
						<l n="28" num="4.14">Se dressera tout droit sur la funèbre pierre.</l>
						<l n="29" num="4.15">Alors, être plaintif, ne pouvant marcher seul,</l>
						<l n="30" num="4.16">Et tout embarrassé des longs plis du linceul,</l>
						<l n="31" num="4.17">Tu chercheras dans l’ombre une épaule adorée ;</l>
						<l n="32" num="4.18">Et, les deux pieds sortis de la tombe sacrée,</l>
						<l n="33" num="4.19">Tu voudras un soutien pour faire quelques pas.</l>
						<l n="34" num="4.20">Alors à l’étranger, oh ! Ne tends point les bras :</l>
						<l n="35" num="4.21">Car ce qui n’est pas toi, ni la Grèce ta mère,</l>
						<l n="36" num="4.22">Ce qui ne parle point ton langage sur terre,</l>
						<l n="37" num="4.23">Et ce qui ne vit pas sous ton ciel enchanteur,</l>
						<l n="38" num="4.24">Bien souvent est barbare et frappé de laideur.</l>
						<l n="39" num="4.25">L’étranger ne viendrait sur ta couche de lave,</l>
						<l n="40" num="4.26">Que pour te garrotter comme une blanche esclave ;</l>
						<l n="41" num="4.27">L’étranger corrompu, s’il te donnait la main,</l>
						<l n="42" num="4.28">Avilirait ton front et flétrirait ton sein.</l>
						<l n="43" num="4.29">Belle ressuscitée, ô princesse chérie,</l>
						<l n="44" num="4.30">N’arrête tes yeux noirs qu’au sol de la patrie ;</l>
						<l n="45" num="4.31">Dans tes fils réunis cherche ton Roméo,</l>
						<l n="46" num="4.32">Noble et douce Italie, ô mère du vrai beau !</l>
					</lg>
				</div></body></text></TEI>