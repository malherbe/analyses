<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Ïambes et poèmes</title>
				<title type="medium">Édition électronique</title>
				<author key="BRB">
					<name>
						<forename>Auguste</forename>
						<surname>BARBIER</surname>
					</name>
					<date from="1805" to="1882">1805-1882</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement métrique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4203 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BRB_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Iambes et poèmes</title>
						<author>Auguste BARBIER</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L947</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Iambes et poèmes</title>
								<author>Auguste BARBIER</author>
								<idno type="URL">https://archive.org/details/iambesetpomes03barbgoog</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Paul Masgana</publisher>
									<date when="1840">1840</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1831">1831</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ÏAMBES</head><div type="poem" key="BRB16">
					<head type="main">LE PROGRÈS</head>
					<lg n="1">
						<l n="1" num="1.1">À quoi servent, grand dieu ! Les leçons de l’histoire</l>
						<l n="2" num="1.2"><space quantity="10" unit="char"></space>Pour l’avenir des citoyens,</l>
						<l n="3" num="1.3">Et tous les faits notés dans une page noire</l>
						<l n="4" num="1.4"><space quantity="10" unit="char"></space>Par la main des historiens,</l>
						<l n="5" num="1.5">Si les mêmes excès et les mêmes misères</l>
						<l n="6" num="1.6"><space quantity="10" unit="char"></space>Reparaissent dans tous les temps,</l>
						<l n="7" num="1.7">Et si de tous les temps les exemples des pères</l>
						<l n="8" num="1.8"><space quantity="10" unit="char"></space>Sont imités par leurs enfants ?</l>
					</lg>
					<lg n="2">
						<l n="9" num="2.1">Ô pauvres insensés ! Qui, le front ceint de chêne</l>
						<l n="10" num="2.2"><space quantity="10" unit="char"></space>Devant l’univers enchanté,</l>
						<l n="11" num="2.3">Voilà six ans bientôt, entonnions d’une haleine</l>
						<l n="12" num="2.4"><space quantity="10" unit="char"></space>L’hymne brûlant de liberté !</l>
						<l n="13" num="2.5">Nous chantions tous en chœur, dans une sainte ivresse,</l>
						<l n="14" num="2.6"><space quantity="10" unit="char"></space>La vierge pure comme l’or,</l>
						<l n="15" num="2.7">Sans penser que plus tard l’immortelle déesse</l>
						<l n="16" num="2.8"><space quantity="10" unit="char"></space>Devait tant nous coûter encor.</l>
						<l n="17" num="2.9">Nous rêvions un ciel doux, un ciel exempt d’orages,</l>
						<l n="18" num="2.10"><space quantity="10" unit="char"></space>Un éternel et vaste azur,</l>
						<l n="19" num="2.11">Tandis que sur nos fronts s’amassaient les nuages :</l>
						<l n="20" num="2.12"><space quantity="10" unit="char"></space>L’avenir devenait obscur.</l>
					</lg>
					<lg n="3">
						<l n="21" num="3.1">Et nous avons revu ce qu’avaient vu nos pères,</l>
						<l n="22" num="3.2"><space quantity="10" unit="char"></space>Le sang humain dans les ruisseaux,</l>
						<l n="23" num="3.3">Et l’angoisse des nuits glaçant le cœur des mères,</l>
						<l n="24" num="3.4"><space quantity="10" unit="char"></space>Quand le plomb battait les carreaux ;</l>
						<l n="25" num="3.5">Le régicide infect aux vengeances infâmes</l>
						<l n="26" num="3.6"><space quantity="10" unit="char"></space>Et ses stupides attentats,</l>
						<l n="27" num="3.7">La baïonnette ardente entrant au sein des femmes,</l>
						<l n="28" num="3.8"><space quantity="10" unit="char"></space>Les enfants percés dans leurs bras :</l>
						<l n="29" num="3.9">Enfin les vieux forfaits d’une époque cruelle</l>
						<l n="30" num="3.10"><space quantity="10" unit="char"></space>Se sont tous relevés, hélas !</l>
						<l n="31" num="3.11">Pour nous faire douter qu’en sa marche éternelle</l>
						<l n="32" num="3.12"><space quantity="10" unit="char"></space>Le monde ait avancé d’un pas.</l>
					</lg>
				</div></body></text></TEI>