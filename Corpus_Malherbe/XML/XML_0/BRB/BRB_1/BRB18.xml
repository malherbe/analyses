<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Ïambes et poèmes</title>
				<title type="medium">Édition électronique</title>
				<author key="BRB">
					<name>
						<forename>Auguste</forename>
						<surname>BARBIER</surname>
					</name>
					<date from="1805" to="1882">1805-1882</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement métrique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4203 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BRB_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Iambes et poèmes</title>
						<author>Auguste BARBIER</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L947</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Iambes et poèmes</title>
								<author>Auguste BARBIER</author>
								<idno type="URL">https://archive.org/details/iambesetpomes03barbgoog</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Paul Masgana</publisher>
									<date when="1840">1840</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1831">1831</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ÏAMBES</head><div type="poem" key="BRB18">
					<head type="main">LA CUVE</head>
					<lg n="1">
						<l n="1" num="1.1">Il est, il est sur terre une infernale cuve,</l>
						<l n="2" num="1.2">On la nomme Paris ; c’est une large étuve,</l>
						<l n="3" num="1.3">Une fosse de pierre aux immenses contours</l>
						<l n="4" num="1.4">Qu’une eau jaune et terreuse enferme à triples tours ;</l>
						<l n="5" num="1.5">C’est un volcan fumeux et toujours en haleine</l>
						<l n="6" num="1.6">Qui remue à longs flots de la matière humaine ;</l>
						<l n="7" num="1.7">Un précipice ouvert à la corruption</l>
						<l n="8" num="1.8">Où la fange descend de toute nation,</l>
						<l n="9" num="1.9">Et qui de temps en temps, plein d’une vase immonde,</l>
						<l n="10" num="1.10">Soulevant ses bouillons déborde sur le monde.</l>
					</lg>
					<lg n="2">
						<l n="11" num="2.1">Là, dans ce trou boueux, le timide soleil</l>
						<l n="12" num="2.2">Vient poser rarement un pied blanc et vermeil ;</l>
						<l n="13" num="2.3">Là les bourdonnements nuit et jour dans la brume</l>
						<l n="14" num="2.4">Montent sur la cité comme une vaste écume ;</l>
						<l n="15" num="2.5">Là personne ne dort, là toujours le cerveau</l>
						<l n="16" num="2.6">Travaille, et, comme l’arc, tend son rude cordeau.</l>
						<l n="17" num="2.7">On y vit un sur trois, on y meurt de débauche ;</l>
						<l n="18" num="2.8">Jamais, le front huilé, la mort ne vous y fauche,</l>
						<l n="19" num="2.9">Car les saints monuments ne restent dans ce lieu</l>
						<l n="20" num="2.10">Que pour dire : autrefois il existait un Dieu.</l>
					</lg>
					<lg n="3">
						<l n="21" num="3.1">Là tant d’autels debout ont roulé de leurs bases,</l>
						<l n="22" num="3.2">Tant d’astres ont pâli sans achever leurs phases,</l>
						<l n="23" num="3.3">Tant de cultes naissants sont tombés sans mûrir,</l>
						<l n="24" num="3.4">Tant de grandes vertus là s’en vinrent pourrir,</l>
						<l n="25" num="3.5">Tant de chars meurtriers creusèrent leur ornière,</l>
						<l n="26" num="3.6">Tant de pouvoirs honteux rougirent la poussière,</l>
						<l n="27" num="3.7">De révolutions au vol sombre et puissant</l>
						<l n="28" num="3.8">Crevèrent coup sur coup leurs nuages de sang,</l>
						<l n="29" num="3.9">Que l’homme, ne sachant où rattacher sa vie,</l>
						<l n="30" num="3.10">Au seul amour de l’or se livre avec furie.</l>
						<l n="31" num="3.11">Misère ! Après mille ans de bouleversements,</l>
						<l n="32" num="3.12">De secousses sans nombre et de vains errements,</l>
						<l n="33" num="3.13">De cultes abolis et de trônes superbes</l>
						<l n="34" num="3.14">Dans les sables perdus, et couchés dans les herbes,</l>
						<l n="35" num="3.15">Le temps, ce vieux coureur, ce vieillard sans pitié,</l>
						<l n="36" num="3.16">Qui va par toute terre écrasant sous le pié</l>
						<l n="37" num="3.17">Les immenses cités regorgeantes de vices,</l>
						<l n="38" num="3.18">Le temps, qui balaya Rome et ses immondices,</l>
						<l n="39" num="3.19">Retrouve encore, après deux mille ans de chemin,</l>
						<l n="40" num="3.20">Un abîme aussi noir que le cuvier romain.</l>
					</lg>
					<lg n="4">
						<l n="41" num="4.1">Toujours même fracas, toujours même délire,</l>
						<l n="42" num="4.2">Même foule de mains à partager l’empire,</l>
						<l n="43" num="4.3">Toujours même troupeau de pâles sénateurs,</l>
						<l n="44" num="4.4">Même flots d’intrigants et de vils corrupteurs,</l>
						<l n="45" num="4.5">Même dérision du prêtre et des oracles,</l>
						<l n="46" num="4.6">Même appétit des jeux, même soif des spectacles,</l>
						<l n="47" num="4.7">Toujours même impudeur, même luxe effronté,</l>
						<l n="48" num="4.8">En chair vive et en os même immoralité ;</l>
						<l n="49" num="4.9">Mêmes débordements, mêmes crimes énormes,</l>
						<l n="50" num="4.10">Moins l’air de l’Italie et la beauté des formes.</l>
					</lg>
					<lg n="5">
						<l n="51" num="5.1">La race de Paris, c’est le pâle voyou</l>
						<l n="52" num="5.2">Au corps chétif, au teint jaune comme un vieux sou ;</l>
						<l n="53" num="5.3">C’est cet enfant criard que l’on voit à toute heure</l>
						<l n="54" num="5.4">Paresseux et flanant, et loin de sa demeure</l>
						<l n="55" num="5.5">Battant les maigres chiens, ou le long des grands murs</l>
						<l n="56" num="5.6">Charbonnant en sifflant mille croquis impurs ;</l>
						<l n="57" num="5.7">Cet enfant ne croit pas, il crache sur sa mère,</l>
						<l n="58" num="5.8">Le nom du ciel pour lui n’est qu’une farce amère ;</l>
						<l n="59" num="5.9">C’est le libertinage enfin en raccourci ;</l>
						<l n="60" num="5.10">Sur un front de quinze ans c’est le vice endurci.</l>
					</lg>
					<lg n="6">
						<l n="61" num="6.1">Et pourtant il est brave, il affronte la foudre,</l>
						<l n="62" num="6.2">Comme un vieux grenadier il mange de la poudre,</l>
						<l n="63" num="6.3">Il se jette au canon en criant : liberté !</l>
						<l n="64" num="6.4">Sous la balle et le fer il tombe avec beauté.</l>
						<l n="65" num="6.5">Mais que l’émeute aussi passe devant sa porte,</l>
						<l n="66" num="6.6">Soudain l’instinct du mal le saisit et l’emporte,</l>
						<l n="67" num="6.7">Le voilà grossissant les bandes de vauriens,</l>
						<l n="68" num="6.8">Molestant le repos des tremblants citoyens,</l>
						<l n="69" num="6.9">Et hurlant, et le front barbouillé de poussière,</l>
						<l n="70" num="6.10">Prêt à jeter à Dieu le blasphème et la pierre.</l>
					</lg>
					<lg n="7">
						<l n="71" num="7.1">Ô race de Paris, race au cœur dépravé,</l>
						<l n="72" num="7.2">Race ardente à mouvoir du fer ou du pavé !</l>
						<l n="73" num="7.3">Mer, dont la grande voix fait trembler sur les trônes</l>
						<l n="74" num="7.4">Ainsi que des fiévreux tous les porte-couronnes !</l>
						<l n="75" num="7.5">Flot hardi qui trois jours s’en va battre les cieux,</l>
						<l n="76" num="7.6">Et qui retombe après, plat et silencieux !</l>
						<l n="77" num="7.7">Race unique en ce monde ! Effrayant assemblage</l>
						<l n="78" num="7.8">Des élans du jeune homme et des crimes de l’âge</l>
						<l n="79" num="7.9">Race qui joue avec le mal et le trépas ;</l>
						<l n="80" num="7.10">Le monde entier t’admire et ne te comprend pas !</l>
					</lg>
					<lg n="8">
						<l n="81" num="8.1">Il est, il est sur terre une infernale cuve,</l>
						<l n="82" num="8.2">On la nomme Paris ; c’est une large étuve,</l>
						<l n="83" num="8.3">Une fosse de pierre aux immenses contours</l>
						<l n="84" num="8.4">Qu’une eau jaune et terreuse enferme à triple tours ;</l>
						<l n="85" num="8.5">C’est un volcan fumeux et toujours en haleine</l>
						<l n="86" num="8.6">Qui remue à longs flots de la matière humaine ;</l>
						<l n="87" num="8.7">Un précipice ouvert à la corruption</l>
						<l n="88" num="8.8">Où la fange descend de toute nation,</l>
						<l n="89" num="8.9">Et qui de temps en temps, plein d’une vase immonde,</l>
						<l n="90" num="8.10">Soulevant ses bouillons déborde sur le monde.</l>
					</lg>
					<closer>
						<dateline>
							<date when="1831">octobre 1831.</date>
						</dateline>
					</closer>
				</div></body></text></TEI>