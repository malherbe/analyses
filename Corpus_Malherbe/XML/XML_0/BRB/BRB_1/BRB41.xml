<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Ïambes et poèmes</title>
				<title type="medium">Édition électronique</title>
				<author key="BRB">
					<name>
						<forename>Auguste</forename>
						<surname>BARBIER</surname>
					</name>
					<date from="1805" to="1882">1805-1882</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement métrique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4203 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BRB_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Iambes et poèmes</title>
						<author>Auguste BARBIER</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L947</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Iambes et poèmes</title>
								<author>Auguste BARBIER</author>
								<idno type="URL">https://archive.org/details/iambesetpomes03barbgoog</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Paul Masgana</publisher>
									<date when="1840">1840</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1831">1831</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LAZARE</head><div type="poem" key="BRB41">
					<head type="main">LES BELLES COLLINES D’IRLANDE</head>
					<lg n="1">
						<l n="1" num="1.1">Le jour où j’ai quitté le sol de mes aïeux,</l>
						<l n="2" num="1.2">La verdoyante érin et ses belles collines,</l>
						<l n="3" num="1.3">Ah ! Pour moi ce jour-là fut un jour malheureux.</l>
						<l n="4" num="1.4">Là, les vents embaumés inondent les poitrines ;</l>
						<l n="5" num="1.5">Tout est si beau, si doux, les sentiers, les ruisseaux,</l>
						<l n="6" num="1.6">Les eaux que les rochers distillent aux prairies,</l>
						<l n="7" num="1.7">Et la rosée en perle attachée aux rameaux !</l>
						<l n="8" num="1.8">Ô terre de mon cœur, ô collines chéries !</l>
					</lg>
					<lg n="2">
						<l n="9" num="2.1">Et pourtant, pauvres gens, pêle-mêle et nus pieds,</l>
						<l n="10" num="2.2">Sur le pont des vaisseaux près de mettre à la voile,</l>
						<l n="11" num="2.3">Hommes, femmes, enfants, nous allons par milliers</l>
						<l n="12" num="2.4">Chercher aux cieux lointains une meilleure étoile.</l>
						<l n="13" num="2.5">La famine nous ronge au milieu de nos champs,</l>
						<l n="14" num="2.6">Et pour nous les cités regorgent de misère ;</l>
						<l n="15" num="2.7">Nos corps nus et glacés n’ont pour tous vêtements</l>
						<l n="16" num="2.8">Que les haillons troués de la riche Angleterre.</l>
					</lg>
					<lg n="3">
						<l n="17" num="3.1">Pourquoi d’autres que nous mangent-ils les moissons</l>
						<l n="18" num="3.2">Que nos bras en sueur semèrent dans nos plaines ?</l>
						<l n="19" num="3.3">Pourquoi d’autres ont-ils pour habits les toisons</l>
						<l n="20" num="3.4">Dont nos lacs ont lavé les magnifiques laines ?</l>
						<l n="21" num="3.5">Pourquoi ne pouvons-nous rester au même coin,</l>
						<l n="22" num="3.6">Et, tous enfants, puiser à la même mamelle ?</l>
						<l n="23" num="3.7">Pourquoi les moins heureux s’en vont-ils le plus loin ?</l>
						<l n="24" num="3.8">Et pourquoi quittons-nous la terre maternelle ?</l>
					</lg>
					<lg n="4">
						<l n="25" num="4.1">Ah ! Depuis bien longtemps tel est le vent fatal</l>
						<l n="26" num="4.2">Qui loin des champs aimés nous incline la tête,</l>
						<l n="27" num="4.3">Le destin ennemi qui fait du nid natal</l>
						<l n="28" num="4.4">De notre belle terre un pays de tempête,</l>
						<l n="29" num="4.5">Le mépris et la haine… ô ma patrie, hélas !</l>
						<l n="30" num="4.6">Pèserait-on si fort sur tes plages fécondes</l>
						<l n="31" num="4.7">Que ton beau sol un jour s’affaisserait bien bas,</l>
						<l n="32" num="4.8">Et que la verte érin s’en irait sous les ondes !</l>
					</lg>
					<lg n="5">
						<l n="33" num="5.1">Mais heureux les troupeaux qui paissent vagabonds</l>
						<l n="34" num="5.2">Les pâtures de trèfle en nos fraîches vallées ;</l>
						<l n="35" num="5.3">Heureux les chers oiseaux qui chantent leurs chansons</l>
						<l n="36" num="5.4">Dans les bois frissonnants où passent leurs volées.</l>
						<l n="37" num="5.5">Oh ! Les vents sont bien doux dans nos prés murmurants,</l>
						<l n="38" num="5.6">Et les meules de foin ont des odeurs divines ;</l>
						<l n="39" num="5.7">L’oseille et les cressons garnissent les courants</l>
						<l n="40" num="5.8">De tous vos clairs ruisseaux, ô mes belles collines !</l>
					</lg>
				</div></body></text></TEI>