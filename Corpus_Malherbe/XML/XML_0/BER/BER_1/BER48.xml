<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Chansons</title>
				<title type="sub_2">Tome I</title>
				<title type="medium">Une édition électronique</title>
				<author key="BER">
					<name>
						<forename>Pierre-Jean</forename>
						<nameLink>de</nameLink>
						<surname>BÉRANGER</surname>
					</name>
					<date from="1780" to="1857">1780-1857</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3963 vers92 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Chansons</title>
						<author>Pierre-Jean De Béranger</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/pierreberanger.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Chansons</title>
								<author>Pierre-Jean de Béranger</author>
								<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k54921665.r=pierre-jean+de+b%C3%A9ranger.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Perrotin, Libraire</publisher>
									<date when="1867">1867</date>
								</imprint>
								<biblScope unit="tome">I</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1815">1815</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BER48">
				<head type="main">LES INFIDÉLITÉS DE LISETTE</head>
				<head type="tune">AIR : Ermite, bon Ermite</head>
				<lg n="1">
					<l n="1" num="1.1">Lisette, dont l’empire</l>
					<l n="2" num="1.2">S’étend jusqu’à mon vin,</l>
					<l n="3" num="1.3">J’éprouve le martyre</l>
					<l n="4" num="1.4">D’en demander en vain.</l>
					<l n="5" num="1.5">Pour souffrir qu’à mon âge</l>
					<l n="6" num="1.6">Les coups me soient comptés,</l>
					<l n="7" num="1.7">Ai-je compté, volage,</l>
					<l n="8" num="1.8">Tes infidélités ?</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1">Lisette, ma Lisette,</l>
					<l n="10" num="2.2">Tu m’as trompé toujours :</l>
					<l n="11" num="2.3">Mais vive la grisette !</l>
					<l n="12" num="2.4"><space unit="char" quantity="4"/>Je veux, Lisette,</l>
					<l n="13" num="2.5"><space unit="char" quantity="2"/>Boire à nos amours.</l>
				</lg>
				<lg n="3">
					<l n="14" num="3.1">Lindor, par son audace,</l>
					<l n="15" num="3.2">Met ta ruse en défaut ;</l>
					<l n="16" num="3.3">Il te parle à voix basse,</l>
					<l n="17" num="3.4">Il soupire tout haut.</l>
					<l n="18" num="3.5">Du tendre espoir qu’il fonde</l>
					<l n="19" num="3.6">Il m’instruisit d’abord.</l>
					<l n="20" num="3.7">De peur que je n’en gronde,</l>
					<l n="21" num="3.8">Verse au moins jusqu’au bord.</l>
				</lg>
				<lg n="4">
					<l n="22" num="4.1">Lisette, ma Lisette,</l>
					<l n="23" num="4.2">Tu m’as trompé toujours :</l>
					<l n="24" num="4.3">Mais vive la grisette !</l>
					<l n="25" num="4.4"><space unit="char" quantity="4"/>Je veux, Lisette,</l>
					<l n="26" num="4.5"><space unit="char" quantity="2"/>Boire à nos amours.</l>
				</lg>
				<lg n="5">
					<l n="27" num="5.1">Avec l’heureux Clitandre</l>
					<l n="28" num="5.2">Lorsque je te surpris,</l>
					<l n="29" num="5.3">Vous comptiez d’un air tendre</l>
					<l n="30" num="5.4">Les baisers qu’il t’a pris.</l>
					<l n="31" num="5.5">Ton humeur peu sévère</l>
					<l n="32" num="5.6">En comptant les doubla ;</l>
					<l n="33" num="5.7">Remplis encor mon verre</l>
					<l n="34" num="5.8">Pour tous ces baisers-là.</l>
				</lg>
				<lg n="6">
					<l n="35" num="6.1">Lisette, ma Lisette,</l>
					<l n="36" num="6.2">Tu m’as trompé toujours :</l>
					<l n="37" num="6.3">Mais vive la grisette !</l>
					<l n="38" num="6.4"><space unit="char" quantity="4"/>Je veux, Lisette,</l>
					<l n="39" num="6.5"><space unit="char" quantity="2"/>Boire à nos amours.</l>
				</lg>
				<lg n="7">
					<l n="40" num="7.1">Mondor, qui toujours donne</l>
					<l n="41" num="7.2">Et rubans et bijoux,</l>
					<l n="42" num="7.3">Devant moi te chiffonne</l>
					<l n="43" num="7.4">Sans te mettre en courroux.</l>
					<l n="44" num="7.5">J’ai vu sa main hardie</l>
					<l n="45" num="7.6">S’égarer sur ton sein ;</l>
					<l n="46" num="7.7">Verse jusqu’à la lie</l>
					<l n="47" num="7.8">Pour un si grand larcin.</l>
				</lg>
				<lg n="8">
					<l n="48" num="8.1">Lisette, ma Lisette,</l>
					<l n="49" num="8.2">Tu m’as trompé toujours :</l>
					<l n="50" num="8.3">Mais vive la grisette !</l>
					<l n="51" num="8.4"><space unit="char" quantity="4"/>Je veux, Lisette,</l>
					<l n="52" num="8.5"><space unit="char" quantity="2"/>Boire à nos amours.</l>
				</lg>
				<lg n="9">
					<l n="53" num="9.1">Certain soir je pénètre</l>
					<l n="54" num="9.2">Dans ta chambre, et sans bruit</l>
					<l n="55" num="9.3">Je vois par la fenêtre</l>
					<l n="56" num="9.4">Un voleur qui s’enfuit.</l>
					<l n="57" num="9.5">Je l’avais, dès la veille,</l>
					<l n="58" num="9.6">Fait fuir de ton boudoir.</l>
					<l n="59" num="9.7">Ah ! Qu’une autre bouteille</l>
					<l n="60" num="9.8">M’empêche de tout voir !</l>
				</lg>
				<lg n="10">
					<l n="61" num="10.1">Lisette, ma Lisette,</l>
					<l n="62" num="10.2">Tu m’as trompé toujours :</l>
					<l n="63" num="10.3">Mais vive la grisette !</l>
					<l n="64" num="10.4"><space unit="char" quantity="4"/>Je veux, Lisette,</l>
					<l n="65" num="10.5"><space unit="char" quantity="2"/>Boire à nos amours.</l>
				</lg>
				<lg n="11">
					<l n="66" num="11.1">Tous, comblés de tes graces,</l>
					<l n="67" num="11.2">Mes amis sont les tiens,</l>
					<l n="68" num="11.3">Et ceux dont tu te lasses,</l>
					<l n="69" num="11.4">C’est moi qui les soutiens.</l>
					<l n="70" num="11.5">Qu’avec ceux-là, traîtresse,</l>
					<l n="71" num="11.6">Le vin me soit permis :</l>
					<l n="72" num="11.7">Sois toujours ma maîtresse,</l>
					<l n="73" num="11.8">Et gardons nos amis.</l>
				</lg>
				<lg n="12">
					<l n="74" num="12.1">Lisette, ma Lisette,</l>
					<l n="75" num="12.2">Tu m’as trompé toujours :</l>
					<l n="76" num="12.3">Mais vive la grisette !</l>
					<l n="77" num="12.4"><space unit="char" quantity="4"/>Je veux, Lisette,</l>
					<l n="78" num="12.5"><space unit="char" quantity="2"/>Boire à nos amours.</l>
				</lg>
			</div></body></text></TEI>