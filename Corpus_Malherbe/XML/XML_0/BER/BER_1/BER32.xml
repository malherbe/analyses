<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Chansons</title>
				<title type="sub_2">Tome I</title>
				<title type="medium">Une édition électronique</title>
				<author key="BER">
					<name>
						<forename>Pierre-Jean</forename>
						<nameLink>de</nameLink>
						<surname>BÉRANGER</surname>
					</name>
					<date from="1780" to="1857">1780-1857</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3963 vers92 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Chansons</title>
						<author>Pierre-Jean De Béranger</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/pierreberanger.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Chansons</title>
								<author>Pierre-Jean de Béranger</author>
								<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k54921665.r=pierre-jean+de+b%C3%A9ranger.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Perrotin, Libraire</publisher>
									<date when="1867">1867</date>
								</imprint>
								<biblScope unit="tome">I</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1815">1815</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BER32">
				<head type="main">MA DERNIÈRE CHANSON PEUT-ÊTRE</head>
				<opener>
					<dateline>
						<date when="1814">Fin de janvier 1814</date>
					</dateline>
				</opener>
				<head type="tune">AIR : Eh quoi ! vous someillez encore (<hi rend="ital">de Fanchon</hi>)</head>
				<lg n="1">
					<l n="1" num="1.1">Je n’eus jamais d’indifférence</l>
					<l n="2" num="1.2">Pour la gloire du nom français.</l>
					<l n="3" num="1.3">L’étranger envahit la France,</l>
					<l n="4" num="1.4">Et je maudis tous ses succès.</l>
					<l n="5" num="1.5">Mais, bien que la douleur honore,</l>
					<l n="6" num="1.6">Que servira d’avoir gémi ?</l>
					<l n="7" num="1.7">Puisqu’ici nous rions encore,</l>
					<l n="8" num="1.8">Autant de pris sur l’ennemi !</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1">Quand plus d’un brave aujourd’hui tremble,</l>
					<l n="10" num="2.2">Moi, poltron, je ne tremble pas.</l>
					<l n="11" num="2.3">Heureux que Bacchus nous rassemble</l>
					<l n="12" num="2.4">Pour trinquer à ce gai repas !</l>
					<l n="13" num="2.5">Amis, c’est le dieu que j’implore ;</l>
					<l n="14" num="2.6">Par lui mon cœur est affermi.</l>
					<l n="15" num="2.7">Buvons gaîment, buvons encore :</l>
					<l n="16" num="2.8">Autant de pris sur l’ennemi !</l>
				</lg>
				<lg n="3">
					<l n="17" num="3.1">Mes créanciers sont des corsaires</l>
					<l n="18" num="3.2">Contre moi toujours soulevés.</l>
					<l n="19" num="3.3">J’allais mettre ordre à mes affaires,</l>
					<l n="20" num="3.4">Quand j’appris ce que vous savez.</l>
					<l n="21" num="3.5">Gens que l’avarice dévore,</l>
					<l n="22" num="3.6">Pour votre or soudain j’ai frémi.</l>
					<l n="23" num="3.7">Prêtez-m’en donc, prêtez encore :</l>
					<l n="24" num="3.8">Autant de pris sur l’ennemi !</l>
				</lg>
				<lg n="4">
					<l n="25" num="4.1">Je possède jeune maîtresse,</l>
					<l n="26" num="4.2">Qui va courir bien des dangers.</l>
					<l n="27" num="4.3">Au fond je crois que la traîtresse</l>
					<l n="28" num="4.4">Désire un peu les étrangers.</l>
					<l n="29" num="4.5">Certains excès que l’on déplore</l>
					<l n="30" num="4.6">Ne l’épouvantent qu’à demi.</l>
					<l n="31" num="4.7">Mais cette nuit me reste encore :</l>
					<l n="32" num="4.8">Autant de pris sur l’ennemi !</l>
				</lg>
				<lg n="5">
					<l n="33" num="5.1">Amis, s’il n’est plus d’espérance,</l>
					<l n="34" num="5.2">Jurons, au risque du trépas,</l>
					<l n="35" num="5.3">Que pour l’ennemi de la France</l>
					<l n="36" num="5.4">Nos voix ne résonneront pas.</l>
					<l n="37" num="5.5">Mais il ne faut point qu’on ignore</l>
					<l n="38" num="5.6">Qu’en chantant le cygne a fini.</l>
					<l n="39" num="5.7">Toujours français, chantons encore :</l>
					<l n="40" num="5.8">Autant de pris sur l’ennemi !</l>
				</lg>
			</div></body></text></TEI>