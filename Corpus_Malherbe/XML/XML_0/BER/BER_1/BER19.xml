<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Chansons</title>
				<title type="sub_2">Tome I</title>
				<title type="medium">Une édition électronique</title>
				<author key="BER">
					<name>
						<forename>Pierre-Jean</forename>
						<nameLink>de</nameLink>
						<surname>BÉRANGER</surname>
					</name>
					<date from="1780" to="1857">1780-1857</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3963 vers92 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Chansons</title>
						<author>Pierre-Jean De Béranger</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/pierreberanger.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Chansons</title>
								<author>Pierre-Jean de Béranger</author>
								<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k54921665.r=pierre-jean+de+b%C3%A9ranger.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Perrotin, Libraire</publisher>
									<date when="1867">1867</date>
								</imprint>
								<biblScope unit="tome">I</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1815">1815</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BER19">
				<head type="main">LES GUEUX</head>
				<opener>
					<dateline>
						<date when="1812">1812</date>
					</dateline>
				</opener>
				<head type="tune">AIR : Première ronde du Départ pour Saint-Malo</head>
				<lg n="1">
					<l n="1" num="1.1"><space unit="char" quantity="6"/>Les gueux, les gueux,</l>
					<l n="2" num="1.2"><space unit="char" quantity="4"/>Sont les gens heureux ;</l>
					<l n="3" num="1.3"><space unit="char" quantity="4"/>Ils s’aiment entre eux.</l>
					<l n="4" num="1.4"><space unit="char" quantity="6"/>Vivent les gueux !</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Des gueux chantons la louange.</l>
					<l n="6" num="2.2">Que de gueux hommes de bien !</l>
					<l n="7" num="2.3">Il faut qu’enfin l’esprit venge</l>
					<l n="8" num="2.4">L’honnête homme qui n’a rien.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><space unit="char" quantity="6"/>Les gueux, les gueux,</l>
					<l n="10" num="3.2"><space unit="char" quantity="4"/>Sont les gens heureux ;</l>
					<l n="11" num="3.3"><space unit="char" quantity="4"/>Ils s’aiment entre eux.</l>
					<l n="12" num="3.4"><space unit="char" quantity="6"/>Vivent les gueux !</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">Oui, le bonheur est facile</l>
					<l n="14" num="4.2">Au sein de la pauvreté :</l>
					<l n="15" num="4.3">J’en atteste l’évangile ;</l>
					<l n="16" num="4.4">J’en atteste ma gaîté.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><space unit="char" quantity="6"/>Les gueux, les gueux,</l>
					<l n="18" num="5.2"><space unit="char" quantity="4"/>Sont les gens heureux ;</l>
					<l n="19" num="5.3"><space unit="char" quantity="4"/>Ils s’aiment entre eux.</l>
					<l n="20" num="5.4"><space unit="char" quantity="6"/>Vivent les gueux !</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1">Au Parnasse la misère</l>
					<l n="22" num="6.2">Long-temps a régné, dit-on.</l>
					<l n="23" num="6.3">Quels biens possédait Homère ?</l>
					<l n="24" num="6.4">Une besace, un bâton.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><space unit="char" quantity="6"/>Les gueux, les gueux,</l>
					<l n="26" num="7.2"><space unit="char" quantity="4"/>Sont les gens heureux ;</l>
					<l n="27" num="7.3"><space unit="char" quantity="4"/>Ils s’aiment entre eux.</l>
					<l n="28" num="7.4"><space unit="char" quantity="6"/>Vivent les gueux !</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1">Vous qu’afflige la détresse,</l>
					<l n="30" num="8.2">Croyez que plus d’un héros,</l>
					<l n="31" num="8.3">Dans le soulier qui le blesse,</l>
					<l n="32" num="8.4">Peut regretter ses sabots.</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1"><space unit="char" quantity="6"/>Les gueux, les gueux,</l>
					<l n="34" num="9.2"><space unit="char" quantity="4"/>Sont les gens heureux ;</l>
					<l n="35" num="9.3"><space unit="char" quantity="4"/>Ils s’aiment entre eux.</l>
					<l n="36" num="9.4"><space unit="char" quantity="6"/>Vivent les gueux !</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1">Du faste qui vous étonne</l>
					<l n="38" num="10.2">L’exil punit plus d’un grand ;</l>
					<l n="39" num="10.3"><space unit="char" quantity="2"/>Diogène, dans sa tonne,</l>
					<l n="40" num="10.4">Brave en paix un conquérant.</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1"><space unit="char" quantity="6"/>Les gueux, les gueux,</l>
					<l n="42" num="11.2"><space unit="char" quantity="4"/>Sont les gens heureux ;</l>
					<l n="43" num="11.3"><space unit="char" quantity="4"/>Ils s’aiment entre eux.</l>
					<l n="44" num="11.4"><space unit="char" quantity="6"/>Vivent les gueux !</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1">D’un palais l’éclat vous frappe,</l>
					<l n="46" num="12.2">Mais l’ennui vient y gémir.</l>
					<l n="47" num="12.3">On peut bien manger sans nappe ;</l>
					<l n="48" num="12.4">Sur la paille on peut dormir.</l>
				</lg>
				<lg n="13">
					<l n="49" num="13.1"><space unit="char" quantity="6"/>Les gueux, les gueux,</l>
					<l n="50" num="13.2"><space unit="char" quantity="4"/>Sont les gens heureux ;</l>
					<l n="51" num="13.3"><space unit="char" quantity="4"/>Ils s’aiment entre eux.</l>
					<l n="52" num="13.4"><space unit="char" quantity="6"/>Vivent les gueux !</l>
				</lg>
				<lg n="14">
					<l n="53" num="14.1">Quel dieu se plaît et s’agite</l>
					<l n="54" num="14.2">Sur ce grabat qu’il fleurit ?</l>
					<l n="55" num="14.3">C’est l’amour qui rend visite</l>
					<l n="56" num="14.4">À la pauvreté qui rit.</l>
				</lg>
				<lg n="15">
					<l n="57" num="15.1"><space unit="char" quantity="6"/>Les gueux, les gueux,</l>
					<l n="58" num="15.2"><space unit="char" quantity="4"/>Sont les gens heureux ;</l>
					<l n="59" num="15.3"><space unit="char" quantity="4"/>Ils s’aiment entre eux.</l>
					<l n="60" num="15.4"><space unit="char" quantity="6"/>Vivent les gueux !</l>
				</lg>
				<lg n="16">
					<l n="61" num="16.1">L’amitié que l’on regrette</l>
					<l n="62" num="16.2">N’a point quitté nos climats ;</l>
					<l n="63" num="16.3">Elle trinque à la guinguette,</l>
					<l n="64" num="16.4">Assise entre deux soldats.</l>
				</lg>
				<lg n="17">
					<l n="65" num="17.1"><space unit="char" quantity="6"/>Les gueux, les gueux,</l>
					<l n="66" num="17.2"><space unit="char" quantity="4"/>Sont les gens heureux ;</l>
					<l n="67" num="17.3"><space unit="char" quantity="4"/>Ils s’aiment entre eux.</l>
					<l n="68" num="17.4"><space unit="char" quantity="6"/>Vivent les gueux !</l>
				</lg>
			</div></body></text></TEI>