<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Chansons</title>
				<title type="sub_2">Tome I</title>
				<title type="medium">Une édition électronique</title>
				<author key="BER">
					<name>
						<forename>Pierre-Jean</forename>
						<nameLink>de</nameLink>
						<surname>BÉRANGER</surname>
					</name>
					<date from="1780" to="1857">1780-1857</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3963 vers92 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Chansons</title>
						<author>Pierre-Jean De Béranger</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/pierreberanger.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Chansons</title>
								<author>Pierre-Jean de Béranger</author>
								<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k54921665.r=pierre-jean+de+b%C3%A9ranger.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Perrotin, Libraire</publisher>
									<date when="1867">1867</date>
								</imprint>
								<biblScope unit="tome">I</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1815">1815</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BER71">
				<head type="main">LES ROMANS</head>
				<opener>
					<salute>À SOPHIE,QUI ME PRIAIT DE COMPOSER <lb/>UN ROMAN POUR LA DISTRAIRE</salute>
				</opener>
				<head type="tune">AIR : J’ai vu par-tout dans mes voyages</head>
				<lg n="1">
					<l n="1" num="1.1">Tu veux que pour toi je compose</l>
					<l n="2" num="1.2">Un long roman qui fasse effet.</l>
					<l n="3" num="1.3">À tes vœux ma raison s’oppose ;</l>
					<l n="4" num="1.4">Un long roman n’est plus mon fait.</l>
					<l n="5" num="1.5">Quand l’homme est loin de son aurore,</l>
					<l n="6" num="1.6">Tous les romans deviennent courts ;</l>
					<l n="7" num="1.7">Et je ne puis long-temps encore</l>
					<l n="8" num="1.8">Prolonger celui des amours.</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1">Heureux qui peut dans sa maîtresse</l>
					<l n="10" num="2.2">Trouver l’amitié d’une sœur !</l>
					<l n="11" num="2.3">Des plaisirs je te dois l’ivresse,</l>
					<l n="12" num="2.4">Et des tendres soins la douceur.</l>
					<l n="13" num="2.5">Des héros, des prétendus sages</l>
					<l n="14" num="2.6">Les longs romans, qui font pitié,</l>
					<l n="15" num="2.7">Ne vaudront jamais quelques pages</l>
					<l n="16" num="2.8">Du doux roman de l’amitié.</l>
				</lg>
				<lg n="3">
					<l n="17" num="3.1">Triste roman que notre histoire !</l>
					<l n="18" num="3.2">Mais, Sophie, au sein des amours,</l>
					<l n="19" num="3.3">De ton destin, j’aime à le croire,</l>
					<l n="20" num="3.4">Les plaisirs charmeront le cours.</l>
					<l n="21" num="3.5">Ah ! Puisses-tu, vive et jolie,</l>
					<l n="22" num="3.6">Long-temps te couronner de fleurs,</l>
					<l n="23" num="3.7">Et sur le roman de la vie</l>
					<l n="24" num="3.8">Ne jamais répandre de pleurs !</l>
				</lg>
			</div></body></text></TEI>