<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Chansons</title>
				<title type="sub_2">Tome I</title>
				<title type="medium">Une édition électronique</title>
				<author key="BER">
					<name>
						<forename>Pierre-Jean</forename>
						<nameLink>de</nameLink>
						<surname>BÉRANGER</surname>
					</name>
					<date from="1780" to="1857">1780-1857</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3963 vers92 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Chansons</title>
						<author>Pierre-Jean De Béranger</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/pierreberanger.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Chansons</title>
								<author>Pierre-Jean de Béranger</author>
								<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k54921665.r=pierre-jean+de+b%C3%A9ranger.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Perrotin, Libraire</publisher>
									<date when="1867">1867</date>
								</imprint>
								<biblScope unit="tome">I</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1815">1815</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BER16">
				<head type="main">MADAME GRÉGOIRE</head>
				<head type="tune">AIR : C’est le gros Thomas</head>
				<lg n="1">
					<l n="1" num="1.1"><space unit="char" quantity="6"/>C’était de mon temps</l>
					<l n="2" num="1.2">Que brillait Madame Grégoire.</l>
					<l n="3" num="1.3"><space unit="char" quantity="6"/>J’allais à vingt ans</l>
					<l n="4" num="1.4">Dans son cabaret rire et boire ;</l>
					<l n="5" num="1.5"><space unit="char" quantity="4"/>Elle attirait les gens</l>
					<l n="6" num="1.6"><space unit="char" quantity="4"/>Par des airs engageants.</l>
					<l n="7" num="1.7">Plus d’un brun à large poitrine</l>
					<l n="8" num="1.8">Avait là crédit sur la mine.</l>
					<l n="9" num="1.9"><space unit="char" quantity="6"/>Ah ! Comme on entrait</l>
					<l n="10" num="1.10"><space unit="char" quantity="4"/>Boire à son cabaret !</l>
				</lg>
				<lg n="2">
					<l n="11" num="2.1"><space unit="char" quantity="6"/>D’un certain époux</l>
					<l n="12" num="2.2">Bien qu’elle pleurât la mémoire,</l>
					<l n="13" num="2.3"><space unit="char" quantity="6"/>Personne de nous</l>
					<l n="14" num="2.4">N’avait connu défunt Grégoire ;</l>
					<l n="15" num="2.5"><space unit="char" quantity="4"/>Mais à le remplacer</l>
					<l n="16" num="2.6"><space unit="char" quantity="4"/>Qui n’eût voulu penser ?</l>
					<l n="17" num="2.7">Heureux l’écot où la commère</l>
					<l n="18" num="2.8">Apportait sa pinte et son verre !</l>
					<l n="19" num="2.9"><space unit="char" quantity="6"/>Ah ! Comme on entrait</l>
					<l n="20" num="2.10"><space unit="char" quantity="4"/>Boire à son cabaret !</l>
				</lg>
				<lg n="3">
					<l n="21" num="3.1"><space unit="char" quantity="6"/>Je crois voir encor</l>
					<l n="22" num="3.2">Son gros rire aller jusqu’aux larmes,</l>
					<l n="23" num="3.3"><space unit="char" quantity="6"/>et sous sa croix d’or</l>
					<l n="24" num="3.4">L’ampleur de ses pudiques charmes.</l>
					<l n="25" num="3.5"><space unit="char" quantity="4"/>Sur tous ses agréments</l>
					<l n="26" num="3.6"><space unit="char" quantity="4"/>Consultez ses amants :</l>
					<l n="27" num="3.7">Au comptoir la sensible brune</l>
					<l n="28" num="3.8">Leur rendait deux pièces pour une.</l>
					<l n="29" num="3.9"><space unit="char" quantity="6"/>Ah ! Comme on entrait</l>
					<l n="30" num="3.10"><space unit="char" quantity="4"/>Boire à son cabaret !</l>
				</lg>
				<lg n="4">
					<l n="31" num="4.1"><space unit="char" quantity="6"/>Des buveurs grivois</l>
					<l n="32" num="4.2">Les femmes lui cherchaient querelle.</l>
					<l n="33" num="4.3"><space unit="char" quantity="6"/>Que j’ai vu de fois</l>
					<l n="34" num="4.4">Des galants se battre pour elle !</l>
					<l n="35" num="4.5"><space unit="char" quantity="4"/>La garde et les amours</l>
					<l n="36" num="4.6"><space unit="char" quantity="4"/>Se chamaillant toujours,</l>
					<l n="37" num="4.7">Elle, en femme des plus capables,</l>
					<l n="38" num="4.8">Dans son lit cachait les coupables.</l>
					<l n="39" num="4.9"><space unit="char" quantity="6"/>Ah ! Comme on entrait</l>
					<l n="40" num="4.10"><space unit="char" quantity="4"/>Boire à son cabaret !</l>
				</lg>
				<lg n="5">
					<l n="41" num="5.1"><space unit="char" quantity="6"/>Quand ce fut mon tour</l>
					<l n="42" num="5.2">D’être en tout le maître chez elle,</l>
					<l n="43" num="5.3"><space unit="char" quantity="6"/>C’était chaque jour</l>
					<l n="44" num="5.4">Pour mes amis fête nouvelle.</l>
					<l n="45" num="5.5"><space unit="char" quantity="4"/>Je ne suis point jaloux :</l>
					<l n="46" num="5.6"><space unit="char" quantity="4"/>Nous nous arrangions tous.</l>
					<l n="47" num="5.7">L’hôtesse, poussant à la vente,</l>
					<l n="48" num="5.8">Nous livrait jusqu’à la servante.</l>
					<l n="49" num="5.9"><space unit="char" quantity="6"/>Ah ! Comme on entrait</l>
					<l n="50" num="5.10"><space unit="char" quantity="4"/>Boire à son cabaret !</l>
				</lg>
				<lg n="6">
					<l n="51" num="6.1"><space unit="char" quantity="6"/>Tout est bien changé :</l>
					<l n="52" num="6.2">N’ayant plus rien à mettre en perce,</l>
					<l n="53" num="6.3"><space unit="char" quantity="6"/>Elle a pris congé</l>
					<l n="54" num="6.4">Et des plaisirs et du commerce.</l>
					<l n="55" num="6.5"><space unit="char" quantity="4"/>Que je regrette, hélas !</l>
					<l n="56" num="6.6"><space unit="char" quantity="4"/>Sa cave et ses appas !</l>
					<l n="57" num="6.7">Long-temps encor chaque pratique</l>
					<l n="58" num="6.8">S’écrîra devant sa boutique :</l>
					<l n="59" num="6.9"><space unit="char" quantity="6"/>Ah ! Comme on entrait</l>
					<l n="60" num="6.10"><space unit="char" quantity="4"/>Boire à son cabaret !</l>
				</lg>
			</div></body></text></TEI>