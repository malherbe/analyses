<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Chansons</title>
				<title type="sub_2">Tome I</title>
				<title type="medium">Une édition électronique</title>
				<author key="BER">
					<name>
						<forename>Pierre-Jean</forename>
						<nameLink>de</nameLink>
						<surname>BÉRANGER</surname>
					</name>
					<date from="1780" to="1857">1780-1857</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3963 vers92 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Chansons</title>
						<author>Pierre-Jean De Béranger</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/pierreberanger.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Chansons</title>
								<author>Pierre-Jean de Béranger</author>
								<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k54921665.r=pierre-jean+de+b%C3%A9ranger.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Perrotin, Libraire</publisher>
									<date when="1867">1867</date>
								</imprint>
								<biblScope unit="tome">I</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1815">1815</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BER52">
				<head type="main">LA BOUTEILLE VOLÉE</head>
				<head type="tune">AIR : La fête des bonnes gens</head>
				<lg n="1">
					<l n="1" num="1.1"><space unit="char" quantity="2"/>Sans bruit, dans ma retraite,</l>
					<l n="2" num="1.2">Hier l’amour pénétra,</l>
					<l n="3" num="1.3"><space unit="char" quantity="2"/>Courut à ma cachette,</l>
					<l n="4" num="1.4">Et de mon vin s’empara.</l>
					<l n="5" num="1.5">Depuis lors ma voix sommeille ;</l>
					<l n="6" num="1.6">Adieu tous mes joyeux sons.</l>
					<l n="7" num="1.7">Amour, rends-moi ma bouteille,</l>
					<l n="8" num="1.8">Ma bouteille et mes chansons.</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1"><space unit="char" quantity="2"/>Iris, dame et coquette,</l>
					<l n="10" num="2.2">À ce larcin l’a poussé.</l>
					<l n="11" num="2.3"><space unit="char" quantity="2"/>Je n’ai plus la recette</l>
					<l n="12" num="2.4">Qui soulage un cœur blessé.</l>
					<l n="13" num="2.5">C’est pour gémir que je veille,</l>
					<l n="14" num="2.6">En proie aux jaloux soupçons.</l>
					<l n="15" num="2.7">Amour, rends-moi ma bouteille,</l>
					<l n="16" num="2.8">Ma bouteille et mes chansons.</l>
				</lg>
				<lg n="3">
					<l n="17" num="3.1"><space unit="char" quantity="2"/>Épicurien aimable,</l>
					<l n="18" num="3.2">À verser frais m’invitant,</l>
					<l n="19" num="3.3"><space unit="char" quantity="2"/>Un vieil ami de table</l>
					<l n="20" num="3.4">Me tend son verre en chantant ;</l>
					<l n="21" num="3.5">Un autre vient à l’oreille</l>
					<l n="22" num="3.6">Me demander des leçons.</l>
					<l n="23" num="3.7">Amour, rends-moi ma bouteille,</l>
					<l n="24" num="3.8">Ma bouteille et mes chansons.</l>
				</lg>
				<lg n="4">
					<l n="25" num="4.1"><space unit="char" quantity="2"/>Tant qu’Iris eut contre elle</l>
					<l n="26" num="4.2">Ce bon vin si regretté,</l>
					<l n="27" num="4.3"><space unit="char" quantity="2"/>Grisette folle et belle</l>
					<l n="28" num="4.4">Tenait mon cœur en gaîté.</l>
					<l n="29" num="4.5">Lison n’a point sa pareille</l>
					<l n="30" num="4.6">Pour vivre avec des garçons.</l>
					<l n="31" num="4.7">Amour, rends-moi ma bouteille,</l>
					<l n="32" num="4.8">Ma bouteille et mes chansons.</l>
				</lg>
				<lg n="5">
					<l n="33" num="5.1"><space unit="char" quantity="2"/>Mais le filou se livre :</l>
					<l n="34" num="5.2">Joyeux, il vient à ma voix ;</l>
					<l n="35" num="5.3"><space unit="char" quantity="2"/>De mon vin il est ivre,</l>
					<l n="36" num="5.4">Et n’en a bu que deux doigts.</l>
					<l n="37" num="5.5">Qu’Iris soit une merveille,</l>
					<l n="38" num="5.6">Je me ris de ses façons :</l>
					<l n="39" num="5.7">Amour me rend ma bouteille,</l>
					<l n="40" num="5.8">Ma bouteille et mes chansons.</l>
				</lg>
			</div></body></text></TEI>