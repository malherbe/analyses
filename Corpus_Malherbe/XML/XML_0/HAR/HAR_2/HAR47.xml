<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">L’Âme nue</title>
				<title type="medium">Édition électronique</title>
				<author key="HAR">
					<name>
						<forename>Edmond</forename>
						<surname>HARAUCOURT</surname>
					</name>
					<date from="1856" to="1941">1856-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2991 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">HAR_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">L’Âme nue</title>
						<author>Edmond Haraucourt</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/L%E2%80%99%C3%82me_nue</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L’Âme nue</title>
								<author>Edmond Haraucourt</author>
								<idno type="URI">https://fr.wikisource.org/w/index.php?title=Fichier:Haraucourt_-_L%E2%80%99%C3%82me_nue,_1885.djvu</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1885">1885</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1885">1885</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Extraction à partir d’une version non définitive du texte sur wikisource.</p>
				<p>Les poèmes manquants ont été ajoutés à partir de la page d’édition du texte sur wikisource.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-10-29" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-10-30" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">I</head><head type="main_part">LA VIE EXTÉRIEURE</head><head type="sub_part">LES LOIS — LES CULTES — LES FORMES</head><head type="main_subpart">LES LOIS</head><div type="poem" key="HAR47">
						<head type="main">CLAIR DE LUNE</head>
						<opener>
							<salute>À ÉMILE GITER</salute>
						</opener>
						<lg n="1">
							<l n="1" num="1.1">Jadis, aux jours du Feu, quand la Terre, en hurlant,</l>
							<l n="2" num="1.2">Roulait son bloc fluide à travers le ciel blanc,</l>
							<l n="3" num="1.3">Elle enfla par degrés sa courbe originelle,</l>
							<l n="4" num="1.4">Puis, dans un vaste effort, creva ses flancs ignés.</l>
							<l n="5" num="1.5">Et lança, vers le flux des mondes déjà nés,</l>
							<l n="6" num="1.6"><space unit="char" quantity="8"></space> La Lune qui germait en elle.</l>
						</lg>
						<lg n="2">
							<l n="7" num="2.1">Alors, dans la splendeur des siècles éclatants,</l>
							<l n="8" num="2.2">Sans relâche, sans fin, à toute heure du temps,</l>
							<l n="9" num="2.3">La mère, ivre d’amour, contemplait dans sa force</l>
							<l n="10" num="2.4">L’astre enfant qui courait comme un jeune soleil :</l>
							<l n="11" num="2.5">Il flambait. Un froid vint l’engourdir de sommeil</l>
							<l n="12" num="2.6"><space unit="char" quantity="8"></space>  Et pétrifia son écorce.</l>
						</lg>
						<lg n="3">
							<l n="13" num="3.1">Puis, ce fut l’âge blond des tiédeurs et des vents :</l>
							<l n="14" num="3.2">La Lune se peupla de murmures vivants ;</l>
							<l n="15" num="3.3">Elle eut des mers sans fond et des fleuves sans nombre,</l>
							<l n="16" num="3.4">Des troupeaux, des cités, des pleurs, des cris joyeux ;</l>
							<l n="17" num="3.5">Elle eut l’amour ; elle eut ses arts, ses lois, ses dieux,</l>
							<l n="18" num="3.6"><space unit="char" quantity="8"></space> Et, lentement, rentra dans l’ombre.</l>
						</lg>
						<lg n="4">
							<l n="19" num="4.1">Depuis, rien ne sent plus son baiser jeune et chaud ;</l>
							<l n="20" num="4.2">La Terre qui vieillit la cherche encor là-haut :</l>
							<l n="21" num="4.3">Tout est nu. Mais, le soir, passe un globe éphémère,</l>
							<l n="22" num="4.4">Et l’on dirait, à voir sa forme errer sans bruit,</l>
							<l n="23" num="4.5">L’âme d’un enfant mort qui reviendrait la nuit</l>
							<l n="24" num="4.6"><space unit="char" quantity="8"></space>Pour regarder dormir sa mère.</l>
						</lg>
					</div></body></text></TEI>