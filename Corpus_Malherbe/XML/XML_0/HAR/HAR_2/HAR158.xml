<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">L’Âme nue</title>
				<title type="medium">Édition électronique</title>
				<author key="HAR">
					<name>
						<forename>Edmond</forename>
						<surname>HARAUCOURT</surname>
					</name>
					<date from="1856" to="1941">1856-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2991 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">HAR_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">L’Âme nue</title>
						<author>Edmond Haraucourt</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/L%E2%80%99%C3%82me_nue</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L’Âme nue</title>
								<author>Edmond Haraucourt</author>
								<idno type="URI">https://fr.wikisource.org/w/index.php?title=Fichier:Haraucourt_-_L%E2%80%99%C3%82me_nue,_1885.djvu</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1885">1885</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1885">1885</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Extraction à partir d’une version non définitive du texte sur wikisource.</p>
				<p>Les poèmes manquants ont été ajoutés à partir de la page d’édition du texte sur wikisource.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-10-29" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-10-30" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">MER DE GLACE</head><div type="poem" key="HAR158">
					<head type="main">MER DE GLACE</head>
					<opener>
						<salute>À CYPRIEN GODEBSKI</salute>
					</opener>
					<lg n="1">
						<l n="1" num="1.1">C’est une vaste mer sans mouvement, qui dort</l>
						<l n="2" num="1.2">Sous l’immensité blême et terne d’un ciel mort.</l>
					</lg>
					<lg n="2">
						<l n="3" num="2.1">Les voix folles du vent, d’un seul coup, se sont tues.</l>
						<l n="4" num="2.2">Les flots polis et bleus ont l’air de leurs statues.</l>
					</lg>
					<lg n="3">
						<l n="5" num="3.1">Un hiver indicible a congelé les flancs</l>
						<l n="6" num="3.2">Des vagues qui hurlaient jadis en troupeaux blancs :</l>
					</lg>
					<lg n="4">
						<l n="7" num="4.1">Et l’océan durci par les froideurs polaires,</l>
						<l n="8" num="4.2">A sculpturalement arrêté ses colères.</l>
					</lg>
					<lg n="5">
						<l n="9" num="5.1">Un calme convulsif hérisse le glacier</l>
						<l n="10" num="5.2">Qui tord ses pics et ses lames couleur d’acier.</l>
					</lg>
					<lg n="6">
						<l n="11" num="6.1">Comme les yeux d’un mort qui s’ouvrent dans la tombe,</l>
						<l n="12" num="6.2">Il luit, glauque et vitreux, sous le brouillard qui tombe.</l>
					</lg>
					<lg n="7">
						<l n="13" num="7.1">Jamais aucun soleil ne flambe sur sa paix ;</l>
						<l n="14" num="7.2">Aucun souffle animant ne court dans l’air épais.</l>
					</lg>
					<lg n="8">
						<l n="15" num="8.1">C’est fini de bondir sous le fouet des orages</l>
						<l n="16" num="8.2">Et de battre les rocs du débris des naufrages !</l>
					</lg>
					<lg n="9">
						<l n="17" num="9.1">C’est fini de jeter de l’écume et du bruit,</l>
						<l n="18" num="9.2">De cracher au soleil et d’alarmer la nuit !</l>
					</lg>
					<lg n="10">
						<l n="19" num="10.1">Une torpeur funèbre emplit cette nature</l>
						<l n="20" num="10.2">Que l’ancien souvenir des tempêtes torture :</l>
					</lg>
					<lg n="11">
						<l n="21" num="11.1">Et la neige a versé sa blanche floraison</l>
						<l n="22" num="11.2">Sur les crêtes dont l’angle écorche l’horizon…</l>
					</lg>
					<lg n="12">
						<l n="23" num="12.1">— Tels sont mes vers, figés dans leur gravité morne,</l>
						<l n="24" num="12.2">Et qui hurlaient en moi comme une mer sans borne.</l>
					</lg>
					<lg n="13">
						<l n="25" num="13.1">Ils hurlaient des chagrins dont j’ai pensé mourir :</l>
						<l n="26" num="13.2">Mais quand je les écris j’ai fini de souffrir.</l>
					</lg>
				</div></body></text></TEI>