<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">L’Âme nue</title>
				<title type="medium">Édition électronique</title>
				<author key="HAR">
					<name>
						<forename>Edmond</forename>
						<surname>HARAUCOURT</surname>
					</name>
					<date from="1856" to="1941">1856-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2991 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">HAR_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">L’Âme nue</title>
						<author>Edmond Haraucourt</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/L%E2%80%99%C3%82me_nue</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L’Âme nue</title>
								<author>Edmond Haraucourt</author>
								<idno type="URI">https://fr.wikisource.org/w/index.php?title=Fichier:Haraucourt_-_L%E2%80%99%C3%82me_nue,_1885.djvu</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1885">1885</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1885">1885</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Extraction à partir d’une version non définitive du texte sur wikisource.</p>
				<p>Les poèmes manquants ont été ajoutés à partir de la page d’édition du texte sur wikisource.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-10-29" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-10-30" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">I</head><head type="main_part">LA VIE EXTÉRIEURE</head><head type="sub_part">LES LOIS — LES CULTES — LES FORMES</head><head type="main_subpart">LES CULTES</head><div type="poem" key="HAR83">
						<head type="main">MAGNIFICAT</head>
						<opener>
							<salute>À THÉODORE DE BANVILLE</salute>
						</opener>
						<lg n="1">
							<l n="1" num="1.1">Les dogmes sont perdus qui consolaient la terre ;</l>
							<l n="2" num="1.2">Les âmes des rêveurs sont des étangs bourbeux</l>
							<l n="3" num="1.3">D’où monte vers la brume un sanglot solitaire</l>
							<l n="4" num="1.4">Comme un cri de crapaud écrasé par des bœufs.</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1">Et l’antique tristesse élargit son empire,</l>
							<l n="6" num="2.2">Ajoutant jour par jour les regrets aux regrets ;</l>
							<l n="7" num="2.3">Et chacun de nos maux nous en engendre un pire</l>
							<l n="8" num="2.4">Ainsi que les forêts qui naissent des forêts.</l>
						</lg>
						<lg n="3">
							<l n="9" num="3.1">Tout s’en va. La raison tremble, l’amour s’effare,</l>
							<l n="10" num="3.2">Et le monde, toujours plus souffrant et plus vieux,</l>
							<l n="11" num="3.3">Entassant ses chagrins, grossit comme un avare</l>
							<l n="12" num="3.4">Le trésor de douleurs légué par les aïeux.</l>
						</lg>
						<lg n="4">
							<l n="13" num="4.1">Donc, puisque nous voilà tout nus dans la nature,</l>
							<l n="14" num="4.2">Orphelins de la foi, seuls avec nos rancœurs,</l>
							<l n="15" num="4.3">Salut à toi, Beauté, religion future,</l>
							<l n="16" num="4.4">Dernier secours des dieux, recours dernier des cœurs !</l>
						</lg>
						<lg n="5">
							<l n="17" num="5.1">Beauté, vertu palpable, esprit de la matière,</l>
							<l n="18" num="5.2">Sœur de la vérité, vierge mère de l’art ;</l>
							<l n="19" num="5.3">Beauté, splendeur du bronze et gloire de la pierre,</l>
							<l n="20" num="5.4">Culte saint des fervents qui sont venus trop tard !</l>
						</lg>
						<lg n="6">
							<l n="21" num="6.1">Âme des corps sans âme et règle sans caprice ;</l>
							<l n="22" num="6.2">Germe et terme de tout ; force, but et moyen ;</l>
							<l n="23" num="6.3">Loi douce qui défends que l’univers périsse,</l>
							<l n="24" num="6.4">Suprême et seul amour qui fasses croire au bien !</l>
						</lg>
						<lg n="7">
							<l n="25" num="7.1">Sagesse des couleurs, mysticité des choses ;</l>
							<l n="26" num="7.2">Majesté de la vie et sacre de la chair ;</l>
							<l n="27" num="7.3">Terre promise, Éden des yeux, Paradis roses,</l>
							<l n="28" num="7.4">Astre qui nous conduis et rends le soir plus cher !</l>
						</lg>
						<lg n="8">
							<l n="29" num="8.1">Arc-en-ciel apparu sur l’orage de larmes</l>
							<l n="30" num="8.2">Que versait notre angoisse en attendant sa fin ;</l>
							<l n="31" num="8.3">Aurore de la joie et couchant des alarmes,</l>
							<l n="32" num="8.4">Manne d’idéal pur dont notre rêve a faim !</l>
						</lg>
						<lg n="9">
							<l n="33" num="9.1">C’est toi le vrai sauveur et toi le vrai messie,</l>
							<l n="34" num="9.2">Rédemption des sens, crèche des voluptés,</l>
							<l n="35" num="9.3">Verbe que promettait l’antique prophétie,</l>
							<l n="36" num="9.4">Seul don de Jéhovah à ses déshérités !</l>
						</lg>
						<lg n="10">
							<l n="37" num="10.1">Salut ! Nous dresserons dans des châsses d’ivoire</l>
							<l n="38" num="10.2">De blancs socles d’argent sous tes pieds immortels,</l>
							<l n="39" num="10.3">Et l’homme, ayant des dieux auxquels il puisse croire,</l>
							<l n="40" num="10.4">Rajeunira son cœur en baisant tes autels.</l>
						</lg>
						<lg n="11">
							<l n="41" num="11.1">Nous, tes prêtres émus, apôtres et prophètes,</l>
							<l n="42" num="11.2">Chanterons l’hosannah sur des rythmes joyeux ;</l>
							<l n="43" num="11.3">Les vierges tresseront des myrtes pour tes fêtes,</l>
							<l n="44" num="11.4">Et la paix fleurissante embaumera les cieux !</l>
						</lg>
						<lg n="12">
							<l n="45" num="12.1">Mais si je meurs trop tôt pour saluer ton temple</l>
							<l n="46" num="12.2">Et voir grandir nos fils dans l’amour de ta loi,</l>
							<l n="47" num="12.3">J’aurai du moins l’orgueil et l’honneur de l’exemple,</l>
							<l n="48" num="12.4">Moi qui brûle ma vie à n’adorer que toi !</l>
						</lg>
					</div></body></text></TEI>