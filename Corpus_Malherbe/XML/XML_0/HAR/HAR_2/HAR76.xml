<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">L’Âme nue</title>
				<title type="medium">Édition électronique</title>
				<author key="HAR">
					<name>
						<forename>Edmond</forename>
						<surname>HARAUCOURT</surname>
					</name>
					<date from="1856" to="1941">1856-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2991 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">HAR_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">L’Âme nue</title>
						<author>Edmond Haraucourt</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/L%E2%80%99%C3%82me_nue</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L’Âme nue</title>
								<author>Edmond Haraucourt</author>
								<idno type="URI">https://fr.wikisource.org/w/index.php?title=Fichier:Haraucourt_-_L%E2%80%99%C3%82me_nue,_1885.djvu</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1885">1885</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1885">1885</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Extraction à partir d’une version non définitive du texte sur wikisource.</p>
				<p>Les poèmes manquants ont été ajoutés à partir de la page d’édition du texte sur wikisource.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-10-29" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-10-30" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">I</head><head type="main_part">LA VIE EXTÉRIEURE</head><head type="sub_part">LES LOIS — LES CULTES — LES FORMES</head><head type="main_subpart">LES CULTES</head><div type="poem" key="HAR76">
						<head type="main">FILLE DU MAL</head>
						<opener>
							<salute>À MAURICE BARRÈS</salute>
						</opener>
						<lg n="1">
							<l n="1" num="1.1">— « Dieu ne me connaît pas : je suis l’œuvre de l’homme ;</l>
							<l n="2" num="1.2">Mais l’homme en me créant a dit : « Fille du ciel, »</l>
							<l n="3" num="1.3">Et c’est Fille du ciel que le peuple me nomme.</l>
						</lg>
						<lg n="2">
							<l n="4" num="2.1">Pour poser sur le monde un ordre artificiel,</l>
							<l n="5" num="2.2">J’ai déformé vos corps, refondu l’âme humaine</l>
							<l n="6" num="2.3">Et mis devant l’amour un mur pénitentiel.</l>
						</lg>
						<lg n="3">
							<l n="7" num="3.1">La nature me fuit ; l’homme est mon seul domaine ;</l>
							<l n="8" num="3.2">Et tout ce qui surgit de mal au nom du bien,</l>
							<l n="9" num="3.3">C’est mon vœu qui l’enfante et ma loi qui le mène.</l>
						</lg>
						<lg n="4">
							<l n="10" num="4.1">Je hais le beau, le vrai, le nu ; je n’aime rien.</l>
							<l n="11" num="4.2">Partout, de tout, je suis la constante ennemie</l>
							<l n="12" num="4.3">Qui se lève sur vous comme un fouet sur le chien.</l>
						</lg>
						<lg n="5">
							<l n="13" num="5.1">Je fais pâlir vos fleurs dans la vierge anémie,</l>
							<l n="14" num="5.2">Filles aux yeux bistrés, blêmes adolescents,</l>
							<l n="15" num="5.3">Et j’emporte au tombeau votre angoisse endormie.</l>
						</lg>
						<lg n="6">
							<l n="16" num="6.1">Je stérilise l’âme et torture les sens ;</l>
							<l n="17" num="6.2">Je glace tout espoir, je châtre toute envie,</l>
							<l n="18" num="6.3">Et le vice hypocrite allume mon encens.</l>
						</lg>
						<lg n="7">
							<l n="19" num="7.1">Je trouble et je dissous, j’infirme et je dévie ;</l>
							<l n="20" num="7.2">J’appelle le remords dès qu’un cœur a battu,</l>
							<l n="21" num="7.3">Et, si je le pouvais, j’étoufferais la vie !</l>
						</lg>
						<lg n="8">
							<l n="22" num="8.1">Mon but est la douleur, et mon nom la Vertu.</l>
						</lg>
					</div></body></text></TEI>