<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">L’Âme nue</title>
				<title type="medium">Édition électronique</title>
				<author key="HAR">
					<name>
						<forename>Edmond</forename>
						<surname>HARAUCOURT</surname>
					</name>
					<date from="1856" to="1941">1856-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2991 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">HAR_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">L’Âme nue</title>
						<author>Edmond Haraucourt</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/L%E2%80%99%C3%82me_nue</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L’Âme nue</title>
								<author>Edmond Haraucourt</author>
								<idno type="URI">https://fr.wikisource.org/w/index.php?title=Fichier:Haraucourt_-_L%E2%80%99%C3%82me_nue,_1885.djvu</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1885">1885</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1885">1885</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Extraction à partir d’une version non définitive du texte sur wikisource.</p>
				<p>Les poèmes manquants ont été ajoutés à partir de la page d’édition du texte sur wikisource.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-10-29" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-10-30" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">II</head><head type="main_part">LA VIE INTÉRIEURELE SOIR</head><head type="sub_part">L’AUBE — MIDI — LE SOIR</head><div type="poem" key="HAR156">
						<head type="main">RÉSIPISCENCE</head>
						<opener>
							<salute>À MADAME M. GODEBSKA</salute>
						</opener>
						<lg n="1">
							<l n="1" num="1.1">Mon rêve, ô rêve, tu pleures ?</l>
							<l n="2" num="1.2">C’est eux tous qui t’ont chassé.</l>
							<l n="3" num="1.3">Je me souviens du passé,</l>
							<l n="4" num="1.4">Des vieilles, des belles heures…</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1">Oh ! pitié ! J’ai trop lutté</l>
							<l n="6" num="2.2">Contre le moi d’un autre âge,</l>
							<l n="7" num="2.3">J’ai trop porté, dans ma rage,</l>
							<l n="8" num="2.4">Cet orgueil de révolté.</l>
						</lg>
						<lg n="3">
							<l n="9" num="3.1">— J’ignore toutes les haines ;</l>
							<l n="10" num="3.2">Je suis bon pour les mauvais,</l>
							<l n="11" num="3.3">Et je dis que je vous hais,</l>
							<l n="12" num="3.4">Moi qui peine pour vos peines.</l>
						</lg>
						<lg n="4">
							<l n="13" num="4.1">Je dis que j’aime ma chair</l>
							<l n="14" num="4.2">Quand je sais ma chair immonde ;</l>
							<l n="15" num="4.3">J’insulte et nargue le monde,</l>
							<l n="16" num="4.4">Et tout ce qui naît m’est cher.</l>
						</lg>
						<lg n="5">
							<l n="17" num="5.1">Je ne suis qu’un fou mystique</l>
							<l n="18" num="5.2">Épris d’un songe trop pur :</l>
							<l n="19" num="5.3">Je vis seul, derrière un mur</l>
							<l n="20" num="5.4">Bâti de terreur pudique.</l>
						</lg>
						<lg n="6">
							<l n="21" num="6.1">On a tant ri de mon cœur</l>
							<l n="22" num="6.2">Qu’un jour j’en ai ri moi-même…</l>
							<l n="23" num="6.3">Ah ! sangloter un blasphème,</l>
							<l n="24" num="6.4">Pleurer un rire moqueur !</l>
						</lg>
						<lg n="7">
							<l n="25" num="7.1">Alors j’ai dit à ma harde</l>
							<l n="26" num="7.2">De me consoler un peu :</l>
							<l n="27" num="7.3">J’ai sali la femme et Dieu.</l>
							<l n="28" num="7.4">— Et mon rêve me regarde !</l>
						</lg>
					</div></body></text></TEI>