<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">L’Âme nue</title>
				<title type="medium">Édition électronique</title>
				<author key="HAR">
					<name>
						<forename>Edmond</forename>
						<surname>HARAUCOURT</surname>
					</name>
					<date from="1856" to="1941">1856-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2991 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">HAR_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">L’Âme nue</title>
						<author>Edmond Haraucourt</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/L%E2%80%99%C3%82me_nue</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L’Âme nue</title>
								<author>Edmond Haraucourt</author>
								<idno type="URI">https://fr.wikisource.org/w/index.php?title=Fichier:Haraucourt_-_L%E2%80%99%C3%82me_nue,_1885.djvu</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1885">1885</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1885">1885</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Extraction à partir d’une version non définitive du texte sur wikisource.</p>
				<p>Les poèmes manquants ont été ajoutés à partir de la page d’édition du texte sur wikisource.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-10-29" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-10-30" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">II</head><head type="main_part">LA VIE INTÉRIEURELE SOIR</head><head type="sub_part">L’AUBE — MIDI — LE SOIR</head><div type="poem" key="HAR153">
						<head type="main">HYMENÆÉ</head>
						<lg n="1">
							<l n="1" num="1.1">Comme une lavandière agreste, à pleines mains,</l>
							<l n="2" num="1.2">Tord les grands linges froids qui pleuvent goutte à goutte,</l>
							<l n="3" num="1.3">Ô femme, si la vie et le mensonge humains</l>
							<l n="4" num="1.4">Ont desséché ton cœur jusqu’au mépris du doute ;</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1">Si tu n’as plus de vœux pour les bonheurs d’autrui,</l>
							<l n="6" num="2.2">D’effroi pour tes dangers, de larmes pour tes peines ;</l>
							<l n="7" num="2.3">Si le mal qu’on te fait glisse sur ton ennui</l>
							<l n="8" num="2.4">Sans pouvoir secouer le sommeil de tes haines ;</l>
						</lg>
						<lg n="3">
							<l n="9" num="3.1">Si ton âme embrumée, ô fille d’Ossian,</l>
							<l n="10" num="3.2">Par un dégoût dont rien ne doit plus la distraire</l>
							<l n="11" num="3.3">S’est prise pour la mort d’un culte patient :</l>
							<l n="12" num="3.4">Pourquoi donc as-tu peur de moi ? Je suis ton frère !</l>
						</lg>
						<lg n="4">
							<l n="13" num="4.1">Hymen, Hymenæé ! C’est l’heure des baisers…</l>
							<l n="14" num="4.2">Viens-nous-en : les corbeaux croassent sur les tombes.</l>
							<l n="15" num="4.3">Hymen ! Et que l’autel ressemble aux épousés !</l>
							<l n="16" num="4.4">Hymen ! Et que le nid soit digne des colombes !</l>
						</lg>
						<lg n="5">
							<l n="17" num="5.1">Au fond des nuits ! Je sais un grand bois où l’hiver</l>
							<l n="18" num="5.2">Suspend des fruits de glace autour des fleurs de givre,</l>
							<l n="19" num="5.3">Un bois désespéré, paradis de l’enfer,</l>
							<l n="20" num="5.4">Où noir, nu, chaque tronc dort crispé comme un guivre.</l>
						</lg>
						<lg n="6">
							<l n="21" num="6.1">Nous nous en irons loin, très loin, tous deux, tout seuls,</l>
							<l n="22" num="6.2">Nous accoupler sans bruit sous les cieux sans étoiles :</l>
							<l n="23" num="6.3">La neige déploiera ses tranquilles linceuls</l>
							<l n="24" num="6.4">Pour mettre à notre lit des draps blancs et des voiles.</l>
						</lg>
						<lg n="7">
							<l n="25" num="7.1">Nos râles attendris effraieront les hiboux,</l>
							<l n="26" num="7.2">Les crapauds gémiront de nous voir nous étreindre,</l>
							<l n="27" num="7.3">Et la lune, veilleuse extatique des fous,</l>
							<l n="28" num="7.4">Vers le vague horizon descendra pour nous plaindre.</l>
						</lg>
						<lg n="8">
							<l n="29" num="8.1">Alors, peut-être, enfin, pour la dernière fois,</l>
							<l n="30" num="8.2">Envahis par l’angoisse et l’horreur des cieux blêmes,</l>
							<l n="31" num="8.3">Émus de la tristesse amicale des bois,</l>
							<l n="32" num="8.4">Nous trouverons des pleurs à verser sur nous-mêmes !</l>
						</lg>
						<ab type="star">⁂</ab>
						<lg n="9">
							<l n="33" num="9.1">Oh ! t’avoir rencontrée aux jours de nos candeurs,</l>
							<l n="34" num="9.2">Sauvage, avec des yeux aussi clairs que tes rêves,</l>
							<l n="35" num="9.3">Ignorant comme moi le monde et ses laideurs,</l>
							<l n="36" num="9.4">Et mêlant tes chansons à la chanson des grèves !</l>
						</lg>
						<lg n="10">
							<l n="37" num="10.1">Avoir sur ton front brun débrouillé tes cheveux,</l>
							<l n="38" num="10.2">Baisé tes cils câlins et tes lèvres dociles,</l>
							<l n="39" num="10.3">Et t’avoir dit : « Veux-tu ? » Tu m’aurais dit : « Je veux. »</l>
							<l n="40" num="10.4">Et nous serions partis ensemble pour des îles.</l>
						</lg>
						<lg n="11">
							<l n="41" num="11.1">Par delà le grand champ des mers aux sillons bleus,</l>
							<l n="42" num="11.2">Nous aurions, sans souci du temps et de l’espace,</l>
							<l n="43" num="11.3">Cherché le dernier coin des Édens fabuleux</l>
							<l n="44" num="11.4">Où les petits oiseaux n’ont pas peur quand on passe.</l>
						</lg>
						<lg n="12">
							<l n="45" num="12.1">Nous aurions empli l’air d’un bonheur sans jaloux,</l>
							<l n="46" num="12.2">Communiquant la joie et faisant la lumière ;</l>
							<l n="47" num="12.3">La terre, autour de nous, eût gardé comme nous</l>
							<l n="48" num="12.4">L’éternel renouveau de sa beauté première.</l>
						</lg>
						<lg n="13">
							<l n="49" num="13.1">Ah ! la douceur de vivre indiciblement pur !</l>
							<l n="50" num="13.2">Faire son avenir semblable à son enfance,</l>
							<l n="51" num="13.3">Rester une âme en fleur quand l’esprit devient mûr,</l>
							<l n="52" num="13.4">Et vieillir doucement sans crainte et sans défense :</l>
						</lg>
						<lg n="14">
							<l n="53" num="14.1">Vieillir sans comparer les temps à d’autres temps,</l>
							<l n="54" num="14.2">Croire en Dieu, croire en soi, croire en tout ce qu’on aime !</l>
							<l n="55" num="14.3">— Seigneur, Seigneur ! Prenez pitié des pénitents</l>
							<l n="56" num="14.4">Et versez sur nos cœurs le pardon du blasphème !</l>
						</lg>
					</div></body></text></TEI>