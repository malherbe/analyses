<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">L’Âme nue</title>
				<title type="medium">Édition électronique</title>
				<author key="HAR">
					<name>
						<forename>Edmond</forename>
						<surname>HARAUCOURT</surname>
					</name>
					<date from="1856" to="1941">1856-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2991 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">HAR_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">L’Âme nue</title>
						<author>Edmond Haraucourt</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/L%E2%80%99%C3%82me_nue</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L’Âme nue</title>
								<author>Edmond Haraucourt</author>
								<idno type="URI">https://fr.wikisource.org/w/index.php?title=Fichier:Haraucourt_-_L%E2%80%99%C3%82me_nue,_1885.djvu</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1885">1885</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1885">1885</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Extraction à partir d’une version non définitive du texte sur wikisource.</p>
				<p>Les poèmes manquants ont été ajoutés à partir de la page d’édition du texte sur wikisource.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-10-29" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-10-30" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">I</head><head type="main_part">LA VIE EXTÉRIEURE</head><head type="sub_part">LES LOIS — LES CULTES — LES FORMES</head><head type="main_subpart">LES CULTES</head><div type="poem" key="HAR72">
						<head type="main">LE CHARRON</head>
						<opener>
							<salute>À CONSTANT COQUELIN</salute>
						</opener>
						<lg n="1">
							<l n="1" num="1.1">Necker est expulsé du royaume. À Versailles,</l>
							<l n="2" num="1.2">L’Étrangère et la cour rêvent de représailles,</l>
							<l n="3" num="1.3">Besenval a les murs et quatre régiments.</l>
							<l n="4" num="1.4">Le vieux Broglie, avec trente mille Allemands,</l>
							<l n="5" num="1.5">Tient la plaine, et la tient en province conquise,</l>
							<l n="6" num="1.6">Saccageant, n’attendant qu’un vœu de la marquise</l>
							<l n="7" num="1.7">Pour étrangler Paris d’un seul coup de lacet.</l>
						</lg>
						<lg n="2">
							<l n="8" num="2.1">Donc, la ville, on l’affame, et son bon roi le sait ;</l>
							<l n="9" num="2.2">Le peuple, on le trahit ; la patrie, on la pille.</l>
							<l n="10" num="2.3">Alors un cri tonna dans l’air : « À la Bastille ! »</l>
							<l n="11" num="2.4">Et formidablement tout Paris se leva.</l>
							<l n="12" num="2.5">Point de canons, point de fusils. N’importe : on va.</l>
							<l n="13" num="2.6">On veut. Poussant son flux et remuant sa houle,</l>
							<l n="14" num="2.7">Ce flot des volontés, cette mer d’âmes, roule.</l>
							<l n="15" num="2.8">À chaque rue, aux quais, aux ponts, aux carrefours,</l>
							<l n="16" num="2.9">Multipliant sa masse écrasante, et toujours</l>
							<l n="17" num="2.10">Plus profonde, et toujours plus dense et plus serrée,</l>
							<l n="18" num="2.11">Elle élargit l’ampleur de sa lourde marée.</l>
							<l n="19" num="2.12">L’air tremble ; et tout au fond des horizons, là-bas,</l>
							<l n="20" num="2.13">Un retentissement effroyable de pas,</l>
							<l n="21" num="2.14">Sous la clarté des cieux, gronde comme un tonnerre.</l>
						</lg>
						<lg n="3">
							<l n="22" num="3.1">Il peina deux mille ans, ce Peuple débonnaire :</l>
							<l n="23" num="3.2">Il en est las, et l’heure a sonné de finir.</l>
							<l n="24" num="3.3">C’est le Passé, c’est le Présent, c’est l’Avenir</l>
							<l n="25" num="3.4">Qui vont : c’est l’unanime humanité qui marche ;</l>
							<l n="26" num="3.5">Et la mer de vengeance apporte aussi son arche,</l>
							<l n="27" num="3.6">Arche sainte arrachée au déluge des rois :</l>
							<l part="I" n="28" num="3.7">La Liberté ! </l>
							<l part="F" n="28" num="3.7">Sinistre, avec ses hauts murs droits,</l>
							<l n="29" num="3.8">La Bastille, debout, dans sa robe de pierre,</l>
							<l n="30" num="3.9">Hausse rigidement sa masse calme et fière</l>
						</lg>
						<lg n="4">
							<l n="31" num="4.1">Sur laquelle Justice et Haine n’ont rien pu.</l>
							<l n="32" num="4.2">Le bloc royal attend : tel un lion repu,</l>
							<l n="33" num="4.3">Superbe, et tout entier ramassé sur son torse,</l>
							<l n="34" num="4.4">Dort dans la majesté terrible de sa force.</l>
						</lg>
						<lg n="5">
							<l n="35" num="5.1">L’Océan d’hommes va, déferle au pied des tours,</l>
							<l n="36" num="5.2">Reflue, et, noircissant au loin les alentours,</l>
							<l n="37" num="5.3">S’étale en nappes, chaud comme un torrent de lave.</l>
							<l n="38" num="5.4">Aux créneaux, les canons dardent leur grand œil cave ;</l>
							<l n="39" num="5.5">Les meurtrières sont luisantes de fusils,</l>
							<l n="40" num="5.6">Et, guettant les élus qu’elle a déjà choisis,</l>
							<l n="41" num="5.7">La mort veille. Hurlant de rage et d’impuissance,</l>
							<l n="42" num="5.8">L’orage humain se jette, et recule, et s’élance,</l>
							<l n="43" num="5.9">Et fait tourbillonner le remous de ses flots</l>
							<l n="44" num="5.10">Qu’il brise au choc des murs invinciblement clos.</l>
						</lg>
						<lg n="6">
							<l n="45" num="6.1">Or, dans ce grondement de fureur populaire,</l>
							<l n="46" num="6.2">Un homme s’avança ; sans un cri, sans colère,</l>
							<l n="47" num="6.3">Calme, s’étant frayé doucement un chemin.</l>
							<l n="48" num="6.4">Il franchit les fossés, une hache à la main.</l>
							<l n="49" num="6.5">Et seul, les deux bras nus, vint prendre la Bastille.</l>
						</lg>
						<lg n="7">
							<l n="50" num="7.1">On le vit sur le mur et les pieds dans la grille</l>
							<l n="51" num="7.2">Chercher son équilibre au haut du pont-levis.</l>
							<l n="52" num="7.3">Il se mit à son œuvre : et, détournant les vis,</l>
							<l n="53" num="7.4">Faisant sauter les clous hors des poutres de chênes,</l>
							<l n="54" num="7.5">Broyant les gonds, tranchant l’anneau rouillé des chaînes,</l>
							<l n="55" num="7.6">Il travailla longtemps, car l’ouvrage était dur.</l>
							<l part="I" n="56" num="7.7">— Feu ! </l>
							<l part="F" n="56" num="7.7">Les balles heurtaient et déchiraient le mur</l>
							<l n="57" num="7.8">Et faisaient des trous ronds dans la blouse volante.</l>
							<l part="I" n="58" num="7.9">— Feu ! </l>
							<l part="F" n="58" num="7.9">Tout autour de lui la mort passait, sifflante,</l>
							<l n="59" num="7.10">Et ses souffles vibrants l’effleuraient tout entier.</l>
							<l n="60" num="7.11">Mais le charron, sans plus frémir qu’à son chantier,</l>
							<l n="61" num="7.12">Levait et rabaissait sa hache, lent et grave.</l>
						</lg>
						<lg n="8">
							<l n="62" num="8.1">Ô jours ! Race des forts ! Siècle où l’on était brave,</l>
							<l n="63" num="8.2">Âge auguste où le sol enfantait des Titans !</l>
							<l n="64" num="8.3">Le vil Peuple, oublié dans l’abîme des temps,</l>
							<l n="65" num="8.4">Se dressait tout à coup de sa terre féconde,</l>
							<l n="66" num="8.5">Et, la justice en main, balayait le vieux monde !</l>
							<l n="67" num="8.6">Salut à vous, manants, roturiers et vilains !</l>
						</lg>
						<lg n="9">
							<l n="68" num="9.1">Inutiles héros dont nos champs étaient pleins,</l>
							<l n="69" num="9.2">Salut ! Athlètes nés et conçus dans l’épreuve,</l>
							<l n="70" num="9.3">Vaillants régénérés de l’humanité neuve !</l>
							<l n="71" num="9.4">— Nous partons, nous, les fils d’un monde agonisant</l>
							<l n="72" num="9.5">Dont les siècles vécus ont épuisé le sang…</l>
							<l n="73" num="9.6">Peuple, peuple ! Sur les débris des nobles races,</l>
							<l n="74" num="9.7">Germez, multipliez, croissez, rameaux vivaces !</l>
							<l n="75" num="9.8">Épanouissez-vous sous le ciel libre et pur !</l>
							<l n="76" num="9.9">Serfs de l’ère passée et rois du temps futur,</l>
							<l n="77" num="9.10">Voilà que ce charron a commencé la tâche,</l>
							<l n="78" num="9.11">Et taille l’avenir humain à coups de hache !</l>
						</lg>
						<lg n="10">
							<l n="79" num="10.1">Le pont-levis grinça sur ses gonds. Un moment,</l>
							<l n="80" num="10.2">Dans l’air, il hésita, puis, d’un bloc, lourdement,</l>
							<l n="81" num="10.3">Tomba, dans le bruit sourd d’un monde qui se brise.</l>
							<l part="I" n="82" num="10.4">« En avant ! En avant ! » </l>
							<l part="F" n="82" num="10.4">Rois, la Bastille est prise.</l>
						</lg>
						<lg n="11">
							<l n="83" num="11.1">— Le charron rabaissa sa manche. Il dit : « Voilà, »</l>
							<l n="84" num="11.2">Puis, simple, ayant défait vingt siècles, s’en alla.</l>
						</lg>
					</div></body></text></TEI>