<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">L’Âme nue</title>
				<title type="medium">Édition électronique</title>
				<author key="HAR">
					<name>
						<forename>Edmond</forename>
						<surname>HARAUCOURT</surname>
					</name>
					<date from="1856" to="1941">1856-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2991 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">HAR_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">L’Âme nue</title>
						<author>Edmond Haraucourt</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/L%E2%80%99%C3%82me_nue</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L’Âme nue</title>
								<author>Edmond Haraucourt</author>
								<idno type="URI">https://fr.wikisource.org/w/index.php?title=Fichier:Haraucourt_-_L%E2%80%99%C3%82me_nue,_1885.djvu</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1885">1885</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1885">1885</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Extraction à partir d’une version non définitive du texte sur wikisource.</p>
				<p>Les poèmes manquants ont été ajoutés à partir de la page d’édition du texte sur wikisource.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-10-29" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-10-30" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">I</head><head type="main_part">LA VIE EXTÉRIEURE</head><head type="sub_part">LES LOIS — LES CULTES — LES FORMES</head><head type="main_subpart">LES LOIS</head><div type="poem" key="HAR43">
						<head type="main">CHANSON À BOIRE</head>
						<lg n="1">
							<l n="1" num="1.1">Par Bacchus et Noé, je crois que je suis ivre !</l>
							<l n="2" num="1.2">J’aurai donc, pour un soir, connu l’amour de vivre,</l>
							<l n="3" num="1.3">Reconquis mes gaîtés, mes douceurs et ma foi,</l>
							<l n="4" num="1.4">Et posé ma croix lourde aux rochers du calvaire…</l>
							<l n="5" num="1.5">— Or, pourquoi ? Pour un peu de mousse dans du verre,</l>
							<l n="6" num="1.6"><space unit="char" quantity="8"></space>Et je deviens meilleur que moi !</l>
						</lg>
						<lg n="2">
							<l n="7" num="2.1">Ô ma pensée ! Orgueil unique de mon être !</l>
							<l n="8" num="2.2">Que vaux-tu donc, si tout te fait changer ou naître ?</l>
							<l n="9" num="2.3">C’est toi qui rutilais dans l’éclat des cristaux</l>
							<l n="10" num="2.4">Et scandais en chantant le hoquet des bouteilles ;</l>
							<l n="11" num="2.5">C’est toi qui mûrissais dans les grappes vermeilles,</l>
							<l n="12" num="2.6"><space unit="char" quantity="8"></space>Sur le flanc lointain des coteaux !</l>
						</lg>
						<lg n="3">
							<l n="13" num="3.1">Aux mois d’automne, aux mois rubiconds des vendanges,</l>
							<l n="14" num="3.2">C’est mon cœur qu’on foulait dans les pressoirs des granges ;</l>
							<l n="15" num="3.3">Et quand la vie intime et chaude crépitait</l>
							<l n="16" num="3.4">Sous la pulpe des fruits qui bout au fond des cuves,</l>
							<l n="17" num="3.5">Quand l’air lourd des hangars se saturait d’effluves,</l>
							<l n="18" num="3.6"><space unit="char" quantity="8"></space>C’est mon rêve qui fermentait…</l>
						</lg>
						<lg n="4">
							<l n="19" num="4.1">Mon rêve ! Fils bâtard des forces que j’héberge !</l>
							<l n="20" num="4.2">Dieu les accouple en moi comme dans une auberge,</l>
							<l n="21" num="4.3">Puis, né de la matière aveugle et du hasard,</l>
							<l n="22" num="4.4">Un feu court dans mon sang comme un torrent de lave,</l>
							<l n="23" num="4.5">Et libre, en moi, sans moi, sous mon crâne d’esclave,</l>
							<l n="24" num="4.6"><space unit="char" quantity="8"></space>S’allume le brasier de l’art !</l>
						</lg>
						<lg n="5">
							<l n="25" num="5.1">Ma volonté, néant, et mes cultes, fumée !</l>
							<l n="26" num="5.2">Je suis moyen ; je suis la brute désarmée ;</l>
							<l n="27" num="5.3">Je suis le point fatal où s’accomplit la loi,</l>
							<l n="28" num="5.4">Furtive éclosion d’un germe involontaire,</l>
							<l n="29" num="5.5">Atome, inconscience errant dans le mystère :</l>
							<l n="30" num="5.6"><space unit="char" quantity="8"></space>Rien n’est à moi, pas même moi !</l>
						</lg>
						<lg n="6">
							<l n="31" num="6.1">Semblable au bois qui brûle, au bruit vain des tempêtes,</l>
							<l n="32" num="6.2">Aux nuages, aux blés fauchés, semblable aux bêtes,</l>
							<l n="33" num="6.3">Je tourne dans la roue immense du destin.</l>
							<l n="34" num="6.4">Je vais sans voir : je suis le frère du brin d’herbe ;</l>
							<l n="35" num="6.5">Et s’il plaît au zéphyr d’écraser ma superbe,</l>
							<l n="36" num="6.6"><space unit="char" quantity="8"></space> C’est fini du soir au matin !</l>
						</lg>
						<lg n="7">
							<l n="37" num="7.1">Mon corps se renouvelle avec le vent qui passe ;</l>
							<l n="38" num="7.2">Je nais et meurs un peu chaque jour, et l’espace</l>
							<l n="39" num="7.3">Me tient comme la mer tiendrait un grain de sel.</l>
							<l n="40" num="7.4">Je suis la goutte d’eau dans le déluge énorme ;</l>
							<l n="41" num="7.5">Je suis un des creusets sans nombre, où se transforme</l>
							<l n="42" num="7.6"><space unit="char" quantity="8"></space>  L’être de l’Être universel.</l>
						</lg>
						<lg n="8">
							<l n="43" num="8.1">Et j’ai beau m’épuiser à crier vers les nues :</l>
							<l n="44" num="8.2">— « Soleils des cieux profonds, planètes inconnues,</l>
							<l n="45" num="8.3">« J’arrive, attendez-moi : car j’étouffe ici-bas ;</l>
							<l n="46" num="8.4">« J’ai soumis la matière et ses lois à mon signe ! »</l>
							<l n="47" num="8.5">— La terre fait mûrir le raisin dans ma vigne</l>
							<l n="48" num="8.6"><space unit="char" quantity="8"></space>   Et la terre ne m’entend pas.</l>
						</lg>
						<lg n="9">
							<l n="49" num="9.1">Mais elle va sonner, l’heure des glas funèbres</l>
							<l n="50" num="9.2">Où l’orgueil dessillé voit clair dans les ténèbres :</l>
							<l n="51" num="9.3">Les Règnes, doucement, reprendront mes lambeaux ;</l>
							<l n="52" num="9.4">Ils en feront des fleurs pour nourrir les abeilles,</l>
							<l n="53" num="9.5">Et mon sang rajeuni coulera dans les treilles</l>
							<l n="54" num="9.6"><space unit="char" quantity="8"></space>Pour griser des peuples nouveaux !</l>
						</lg>
					</div></body></text></TEI>