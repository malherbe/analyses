<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">L’Âme nue</title>
				<title type="medium">Édition électronique</title>
				<author key="HAR">
					<name>
						<forename>Edmond</forename>
						<surname>HARAUCOURT</surname>
					</name>
					<date from="1856" to="1941">1856-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2991 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">HAR_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">L’Âme nue</title>
						<author>Edmond Haraucourt</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/L%E2%80%99%C3%82me_nue</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L’Âme nue</title>
								<author>Edmond Haraucourt</author>
								<idno type="URI">https://fr.wikisource.org/w/index.php?title=Fichier:Haraucourt_-_L%E2%80%99%C3%82me_nue,_1885.djvu</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1885">1885</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1885">1885</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Extraction à partir d’une version non définitive du texte sur wikisource.</p>
				<p>Les poèmes manquants ont été ajoutés à partir de la page d’édition du texte sur wikisource.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-10-29" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-10-30" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">II</head><head type="main_part">LA VIE INTÉRIEURELE SOIR</head><head type="sub_part">L’AUBE — MIDI — LE SOIR</head><div type="poem" key="HAR145">
						<head type="main">LA CITÉ MORTE</head>
						<opener>
							<salute>À GEORGES LORIN</salute>
						</opener>
						<lg n="1">
							<l n="1" num="1.1">Sous la tranquillité d’un ciel platement bleu</l>
							<l n="2" num="1.2">Où l’air dort, sans chaleur et sans force vitale,</l>
							<l n="3" num="1.3">Une ville déserte, aux murs pâlis, s’étale,</l>
							<l n="4" num="1.4">Triste comme la Mort et grande comme Dieu.</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1">Sans ruines, debout sur la terre très plane,</l>
							<l n="6" num="2.2">Elle dresse les blocs carrés de ses maisons,</l>
							<l n="7" num="2.3">Et, sinistre, envahit au loin les horizons</l>
							<l n="8" num="2.4">Sur qui la froide horreur des solitudes plane.</l>
						</lg>
						<lg n="3">
							<l n="9" num="3.1">Ses boulevards sans fin fendent l’immensité</l>
							<l n="10" num="3.2">Où nul frisson vivant ne vibre et se balance,</l>
							<l n="11" num="3.3">Et tous pareils, tous droits, courent dans le silence,</l>
							<l n="12" num="3.4">Coupant à coins égaux l’uniforme cité.</l>
						</lg>
						<lg n="4">
							<l n="13" num="4.1">Tout se ressemble : un art rigide et monotone,</l>
							<l n="14" num="4.2">Reniant les palais et les temples bénits,</l>
							<l n="15" num="4.3">Sur un dessin unique a taillé les granits</l>
							<l n="16" num="4.4">Dont la façade lisse a des teintes d’automne.</l>
						</lg>
						<lg n="5">
							<l n="17" num="5.1">Les maisons, mornes sœurs, par groupes familiers,</l>
							<l n="18" num="5.2">Massent leurs angles durs et leurs toits en terrasses,</l>
							<l n="19" num="5.3">Catafalques de pierre où moisirent des races,</l>
							<l n="20" num="5.4">Et qu’un ennui pesant aligne par milliers.</l>
						</lg>
						<lg n="6">
							<l n="21" num="6.1">Point d’herbes ; point de fleurs ; point d’arbre aux feuilles vertes</l>
							<l n="22" num="6.2">Tout s’est pétrifié dans un sommeil géant,</l>
							<l n="23" num="6.3">Et l’on croit voir bâiller les noirceurs du néant</l>
							<l n="24" num="6.4">Dans le cadre profond des fenêtres ouvertes.</l>
						</lg>
						<lg n="7">
							<l n="25" num="7.1">Rien n’est clos : c’est d’un coup que la vie a quitté</l>
							<l n="26" num="7.2">Ce monde fantastique où bruissait la foule ;</l>
							<l n="27" num="7.3">Les grands seuils inusés attendent qu’on les foule :</l>
							<l n="28" num="7.4">Mais ceux qui passaient là sont dans l’éternité.</l>
						</lg>
						<lg n="8">
							<l n="29" num="8.1">— Ô mon cœur ! Cité vide, inerte et désolée,</l>
							<l n="30" num="8.2">Tu vas dormir sans trêve et tant que je vivrai ;</l>
							<l n="31" num="8.3">Mon ennui veillera sur ton sommeil navré,</l>
							<l n="32" num="8.4">Comme un marbre plaintif au bord d’un mausolée.</l>
						</lg>
						<lg n="9">
							<l n="33" num="9.1">Dors dans l’oubli calmant des rêves que j’aimais :</l>
							<l n="34" num="9.2">Nous attendrons la mort qui rajeunit les choses,</l>
							<l n="35" num="9.3">Puisque tous nos espoirs, ouvrant leurs ailes roses,</l>
							<l n="36" num="9.4">Dans leur vol effrayé sont partis pour jamais !</l>
						</lg>
					</div></body></text></TEI>