<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">L’Âme nue</title>
				<title type="medium">Édition électronique</title>
				<author key="HAR">
					<name>
						<forename>Edmond</forename>
						<surname>HARAUCOURT</surname>
					</name>
					<date from="1856" to="1941">1856-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2991 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">HAR_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">L’Âme nue</title>
						<author>Edmond Haraucourt</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/L%E2%80%99%C3%82me_nue</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L’Âme nue</title>
								<author>Edmond Haraucourt</author>
								<idno type="URI">https://fr.wikisource.org/w/index.php?title=Fichier:Haraucourt_-_L%E2%80%99%C3%82me_nue,_1885.djvu</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1885">1885</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1885">1885</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Extraction à partir d’une version non définitive du texte sur wikisource.</p>
				<p>Les poèmes manquants ont été ajoutés à partir de la page d’édition du texte sur wikisource.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-10-29" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-10-30" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">I</head><head type="main_part">LA VIE EXTÉRIEURE</head><head type="sub_part">LES LOIS — LES CULTES — LES FORMES</head><head type="main_subpart">LES LOIS</head><div type="poem" key="HAR42">
						<head type="main">L’IMMUABLE</head>
						<opener>
							<salute>À AUGUSTE SAINTE-BEUVE</salute>
						</opener>
						<lg n="1">
							<l n="1" num="1.1">Ah ! les mondes éteints et les globes détruits !</l>
							<l n="2" num="1.2">Rêve, et nombre la poudre innombrable des astres</l>
							<l n="3" num="1.3">Qui, croulant tour à tour dans le chaos des nuits,</l>
							<l n="4" num="1.4">Ont fécondé les cieux en semant leurs désastres !</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1">Tout passe au vent des jours ! Lorsque les temps sont mûrs,</l>
							<l n="6" num="2.2">La mort vient balayer les cités jadis pleines</l>
							<l n="7" num="2.3">Qui couvraient l’horizon des ombres de leurs murs,</l>
							<l n="8" num="2.4">Et qui peuplaient de bruit l’immensité des plaines.</l>
						</lg>
						<lg n="3">
							<l n="9" num="3.1">Les bois qui s’étageaient sur la pente des monts</l>
							<l n="10" num="3.2">S’affaissent ; l’Océan submerge les prairies,</l>
							<l n="11" num="3.3">Tandis que, surgissant du sable et des limons,</l>
							<l n="12" num="3.4">Les continents nouveaux sortent des mers taries.</l>
						</lg>
						<lg n="4">
							<l n="13" num="4.1">Les pôles dégelés roulent vers le soleil ;</l>
							<l n="14" num="4.2">Les dieux qu’on adorait sont remplacés par d’autres ;</l>
							<l n="15" num="4.3">Les empires houleux s’endorment sans réveil,</l>
							<l n="16" num="4.4">Et les cultes vieillis lapident leurs apôtres.</l>
						</lg>
						<lg n="5">
							<l n="17" num="5.1">Les lois chassent les lois dont un peuple était fier ;</l>
							<l n="18" num="5.2">Un fleuve de mépris vient en laver les traces :</l>
							<l n="19" num="5.3">Nous punirons demain ce qui fut juste hier,</l>
							<l n="20" num="5.4">Et nos propres vertus feront rougir des races.</l>
						</lg>
						<lg n="6">
							<l n="21" num="6.1">Chaque fleur qui fleurit porte déjà son deuil ;</l>
							<l n="22" num="6.2">Le vrai n’existe pas : nous changeons et tout change.</l>
							<l n="23" num="6.3">L’immortel n’est qu’un mot créé par notre orgueil</l>
							<l n="24" num="6.4">Rêvant pour oublier qu’il est né sur la fange.</l>
						</lg>
						<lg n="7">
							<l n="25" num="7.1">Tout, les masses sans nom et les fronts radieux,</l>
							<l n="26" num="7.2">Ce qui fut notre amour ou qui fit notre envie,</l>
							<l n="27" num="7.3">L’œuvre de notre esprit comme l’œuvre des Dieux,</l>
							<l n="28" num="7.4">Tout revient au néant qui doit nourrir la vie !</l>
						</lg>
						<lg n="8">
							<l n="29" num="8.1">Et le cercle éternel tourne dans l’infini,</l>
							<l n="30" num="8.2">Entraînant sans repos la matière et les formes,</l>
							<l n="31" num="8.3">Et toujours, puis encor, l’univers rajeuni</l>
							<l n="32" num="8.4">Naît pour mourir et meurt pour naître, au gré des Normes.</l>
						</lg>
						<lg n="9">
							<l n="33" num="9.1">Seule, et fière, et debout, sans ployer devant rien,</l>
							<l n="34" num="9.2">Au milieu du fatal effondrement des âges,</l>
							<l n="35" num="9.3">La Raison nous regarde hésiter vers le bien,</l>
							<l n="36" num="9.4">Et sereine, immuable, elle compte les sages.</l>
						</lg>
					</div></body></text></TEI>