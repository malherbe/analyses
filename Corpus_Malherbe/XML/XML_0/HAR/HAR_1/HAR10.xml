<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">La Légende des Sexes</title>
				<title type="sub">Poëmes Hystériques</title>
				<title type="medium">Édition électronique</title>
				<author key="HAR">
					<name>
						<forename>Edmond</forename>
						<surname>HARAUCOURT</surname>
					</name>
					<date from="1856" to="1941">1856-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1404 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">HAR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">La Légende des sexes, poëmes hystériques</title>
						<author>Edmond Haraucourt</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https ://fr.wikisource.org/wiki/La_L%C3%A9gende_des_sexes,_po%C3%ABmes_hyst%C3%A9riques</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>La Légende des sexes, poëmes hystériques</title>
								<author>Edmond Haraucourt</author>
								<idno type="URI">https ://fr.wikisource.org/wiki/Fichier :Haraucourt_-_La_L%C3%A9gende_des_sexes,_po%C3%ABmes_hyst%C3%A9riques,_1882.djvu</idno>
								<imprint>
									<pubPlace>Bruxelles</pubPlace>
									<publisher>Imprimé à Bruxelles pour l’auteur</publisher>
									<date when="1882">1882</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>La Légende des sexes, poëmes hystériques</title>
						<author>Edmond Haraucourt</author>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/btv1b8618380c</idno>
						<imprint>
							<pubPlace>Bruxelles</pubPlace>
							<publisher>ÉDITION PRIVILE, REVUE PAR L’AUTEUR</publisher>
							<date when="1893">1893</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1882">1882</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Extraction à partir d’une version non définitive du texte sur wikisource.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2022-05-28" who="RR">Une correction introduite à partir de l’édition imprimée de 1893</change>
				<change when="2017-10-30" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-10-30" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">La légende des sexes</head><div type="poem" key="HAR10">
					<head type="main">À UNE VIERGE</head>
					<lg n="1">
						<l n="1" num="1.1">Je veux cueillir la fleur de ta virginité,</l>
						<l n="2" num="1.2">Savourer à longs traits les sucs de son calice :</l>
						<l n="3" num="1.3">Comme le frelon ivre et lourd d’avoir fêté</l>
						<l n="4" num="1.4">Les corolles, je veux que ma lèvre pâlisse</l>
						<l n="5" num="1.5">À boire tes mortels parfums de volupté.</l>
					</lg>
					<lg n="2">
						<l n="6" num="2.1">Fais chanter ton baiser : c’est le roi des poèmes.</l>
						<l n="7" num="2.2">J’apporte l’Infini que ton rêve a cherché :</l>
						<l n="8" num="2.3">C’est moi qui t’ouvrirai le Ciel, puisque tu m’aimes,</l>
						<l n="9" num="2.4">Et tu la connaîtras, l’extase du péché</l>
						<l n="10" num="2.5">Qui fait les cœurs pâmés et qui fait les fronts blêmes !</l>
					</lg>
					<lg n="3">
						<l n="11" num="3.1">C’est la fin, c’est le but sacré de tous nos vœux ;</l>
						<l n="12" num="3.2">C’est le levier du monde et le ressort de l’âme ;</l>
						<l n="13" num="3.3">C’est la Force qui crée et fait dire : « Je veux ! »</l>
						<l n="14" num="3.4">C’est le sceptre que Dieu mit aux mains de la femme,</l>
						<l n="15" num="3.5">Et la couronne d’or qui luit dans ses cheveux.</l>
					</lg>
					<lg n="4">
						<l n="16" num="4.1">C’est elle qui me fait ta chose et ton esclave,</l>
						<l n="17" num="4.2">Courbe à tes pieds mon col et mes genoux brisés,</l>
						<l n="18" num="4.3">Et fait bouillir mon sang comme un torrent de lave.</l>
						<l n="19" num="4.4">Mère des Univers et fille des Baisers,</l>
						<l n="20" num="4.5">Elle ne souille pas, la divine : elle lave !</l>
					</lg>
					<lg n="5">
						<l n="21" num="5.1">Car c’est l’amour qui rend meilleur. Et rien n’est vrai</l>
						<l n="22" num="5.2">Hormis la volupté qui te créa si belle,</l>
						<l n="23" num="5.3">Qui me versa le vin dont je t’enivrerai,</l>
						<l n="24" num="5.4">Qui fit ta lèvre rose et noire ta prunelle,</l>
						<l n="25" num="5.5">Qui fit que je t’adore et que j’en ai pleuré.</l>
					</lg>
					<lg n="6">
						<l n="26" num="6.1">Viens au ciel, Ange, viens au ciel ! Tu veux ; j’implore :</l>
						<l n="27" num="6.2">Silence à ton orgueil ! Viens, et quand tu sauras</l>
						<l n="28" num="6.3">Quel est ce paradis que ta jeunesse ignore,</l>
						<l n="29" num="6.4">Tes désirs suppliants me prendront dans leurs bras</l>
						<l n="30" num="6.5">Pour me baiser la bouche et murmurer : « Encore ! »</l>
					</lg>
				</div></body></text></TEI>