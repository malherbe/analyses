<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">La Légende des Sexes</title>
				<title type="sub">Poëmes Hystériques</title>
				<title type="medium">Édition électronique</title>
				<author key="HAR">
					<name>
						<forename>Edmond</forename>
						<surname>HARAUCOURT</surname>
					</name>
					<date from="1856" to="1941">1856-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1404 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">HAR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">La Légende des sexes, poëmes hystériques</title>
						<author>Edmond Haraucourt</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https ://fr.wikisource.org/wiki/La_L%C3%A9gende_des_sexes,_po%C3%ABmes_hyst%C3%A9riques</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>La Légende des sexes, poëmes hystériques</title>
								<author>Edmond Haraucourt</author>
								<idno type="URI">https ://fr.wikisource.org/wiki/Fichier :Haraucourt_-_La_L%C3%A9gende_des_sexes,_po%C3%ABmes_hyst%C3%A9riques,_1882.djvu</idno>
								<imprint>
									<pubPlace>Bruxelles</pubPlace>
									<publisher>Imprimé à Bruxelles pour l’auteur</publisher>
									<date when="1882">1882</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>La Légende des sexes, poëmes hystériques</title>
						<author>Edmond Haraucourt</author>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/btv1b8618380c</idno>
						<imprint>
							<pubPlace>Bruxelles</pubPlace>
							<publisher>ÉDITION PRIVILE, REVUE PAR L’AUTEUR</publisher>
							<date when="1893">1893</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1882">1882</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Extraction à partir d’une version non définitive du texte sur wikisource.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2022-05-28" who="RR">Une correction introduite à partir de l’édition imprimée de 1893</change>
				<change when="2017-10-30" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-10-30" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">La légende des sexes</head><div type="poem" key="HAR19">
					<head type="main">L’OBSESSION</head>
					<opener>
						<salute>À Charles Morice.</salute>
					</opener>
					<lg n="1">
						<l n="1" num="1.1">Ô vase de volupté,</l>
						<l n="2" num="1.2">Je t’aime, Femme, Beauté !</l>
						<l n="3" num="1.3">Je suis un Faune hanté</l>
						<l n="4" num="1.4"><space unit="char" quantity="6"></space>Par la luxure :</l>
						<l n="5" num="1.5">Brute vouée au plaisir,</l>
						<l n="6" num="1.6">Chair condamnée à gésir</l>
						<l n="7" num="1.7">Sous la meule du désir</l>
						<l n="8" num="1.8"><space unit="char" quantity="6"></space>Qui me pressure.</l>
					</lg>
					<lg n="2">
						<l n="9" num="2.1">Un rut fou tient mon destin :</l>
						<l n="10" num="2.2">Mais j’adore le festin</l>
						<l n="11" num="2.3">Que du soir jusqu’au matin</l>
						<l n="12" num="2.4"><space unit="char" quantity="6"></space>Mon sang arrose ;</l>
						<l n="13" num="2.5">Je suis le joyeux martyr</l>
						<l n="14" num="2.6">Qui se grise de sentir</l>
						<l n="15" num="2.7">Sa chair vive s’engloutir</l>
						<l n="16" num="2.8"><space unit="char" quantity="6"></space>Sous la dent rose.</l>
					</lg>
					<lg n="3">
						<l n="17" num="3.1">Chaque femme, je la veux !</l>
						<l n="18" num="3.2">Des talons jusqu’aux cheveux</l>
						<l n="19" num="3.3">J’emprisonne dans mes vœux</l>
						<l n="20" num="3.4"><space unit="char" quantity="6"></space>Les inconnues :</l>
						<l n="21" num="3.5">Sous leurs jupons empesés</l>
						<l n="22" num="3.6">Mes rêves inapaisés</l>
						<l n="23" num="3.7">Glissent de sournois baisers</l>
						<l n="24" num="3.8"><space unit="char" quantity="6"></space>Vers leurs peaux nues.</l>
					</lg>
					<lg n="4">
						<l n="25" num="4.1">Je déshabille leurs seins :</l>
						<l n="26" num="4.2">Mes caresses, par essaims,</l>
						<l n="27" num="4.3">S’abattent sur les coussins</l>
						<l n="28" num="4.4"><space unit="char" quantity="6"></space>De leurs poitrines ;</l>
						<l n="29" num="4.5">Je me vautre sur leurs flancs,</l>
						<l n="30" num="4.6">Ivre des parfums troublants</l>
						<l n="31" num="4.7">Qui montent des ventres blancs</l>
						<l n="32" num="4.8"><space unit="char" quantity="6"></space>À mes narines.</l>
					</lg>
					<lg n="5">
						<l n="33" num="5.1">Vous aussi. Nymphes, splendeurs</l>
						<l n="34" num="5.2">Que pour mes fauves ardeurs</l>
						<l n="35" num="5.3">L’art du pinceau sans pudeurs</l>
						<l n="36" num="5.4"><space unit="char" quantity="6"></space>A dévêtues :</l>
						<l n="37" num="5.5">Vos formes, obstinément,</l>
						<l n="38" num="5.6">Me tirent comme un aimant ;</l>
						<l n="39" num="5.7">J’ai de longs regards d’amant</l>
						<l n="40" num="5.8"><space unit="char" quantity="6"></space>Pour les statues.</l>
					</lg>
					<lg n="6">
						<l n="41" num="6.1">Doux, je promène ma main</l>
						<l n="42" num="6.2">Aux rondeurs du marbre humain,</l>
						<l n="43" num="6.3">Et j’y cherche le chemin</l>
						<l n="44" num="6.4"><space unit="char" quantity="6"></space>Où vont mes lèvres.</l>
						<l n="45" num="6.5">Ma langue en fouille les plis ;</l>
						<l n="46" num="6.6">Et sur les torses polis,</l>
						<l n="47" num="6.7">Buvant les divins oublis,</l>
						<l n="48" num="6.8"><space unit="char" quantity="6"></space>J’endors mes fièvres.</l>
					</lg>
					<lg n="7">
						<l n="49" num="7.1">— Ainsi, toujours tourmenté</l>
						<l n="50" num="7.2">Par des soifs de volupté,</l>
						<l n="51" num="7.3">J’emplis de lubricité</l>
						<l n="52" num="7.4"><space unit="char" quantity="6"></space>Mes vers eux-mêmes ;</l>
						<l n="53" num="7.5">Et quand mes nerfs sont lassés,</l>
						<l n="54" num="7.6">Quand ma bête crie : assez,</l>
						<l n="55" num="7.7">J’onanise mes pensers</l>
						<l n="56" num="7.8"><space unit="char" quantity="6"></space>Dans mes poèmes !</l>
					</lg>
				</div></body></text></TEI>