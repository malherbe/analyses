<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Jeunes Croyances</title>
				<title type="sub">Le Rachat de la Tour</title>
				<title type="medium">Édition électronique</title>
				<author key="AIC">
					<name>
						<forename>Jean</forename>
						<surname>AICARD</surname>
					</name>
					<date from="1848" to="1921">1848-1921</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1274 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">AIC_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Jeunes Croyances</title>
						<author>Jean Aicard</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/Les_Jeunes_Croyances</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Jeunes Croyances</title>
								<author>Jean Aicard</author>
								<idno type="URI">https://archive.org/details/lesjeunescroyan00aicagoog</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Alphonse Lemerre Éditeur</publisher>
									<date when="1867">1867</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1867">1867</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Extraction à partir d’une version non définitive du texte sur wikisource.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-11-01" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-11-01" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">IV</head><div type="poem" key="AIC50">
					<head type="number">XVI</head>
					<head type="main">LIBERTÉ, ÉGALITÉ, FRATERNITÉ</head>
					<opener>
						<salute><hi rend="ital">À mon ami Michel Reynaud.</hi></salute>
						<epigraph>
							<cit>
								<quote>
									Tout homme est prêtre.
								</quote>
							</cit>
						</epigraph>
					</opener>
					<lg n="1">
						<l n="1" num="1.1">Quand nous saurons bien tous que nous sommes des frères,</l>
						<l n="2" num="1.2">Quand l’amour coulera dans le sang de nos cœurs ;</l>
						<l n="3" num="1.3">Debout sur les engins des haines et des guerres,</l>
						<l n="4" num="1.4">Quand vainqueurs et vaincus s’embrasseront, vainqueurs ;</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">Quand, reniant le trône, un roi dira : « J’abdique !</l>
						<l n="6" num="2.2">J’abdique les hauteurs… je dois régner d’en bas ! »</l>
						<l n="7" num="2.3">Quand on aura compris la sainte République,</l>
						<l n="8" num="2.4">Quand les peuples n’auront ni prêtres ni soldats !</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">Quand on ne verra plus sous les splendeurs célestes</l>
						<l n="10" num="3.2">Le théâtre forain, l’auberge aux toits branlants ;</l>
						<l n="11" num="3.3">Quand les forts et les grands n’auront plus sur leurs vestes</l>
						<l n="12" num="3.4">Les tatouages d’or des bouffons ambulants !</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">Quand l’homme bénira Dieu, créateur des mondes,</l>
						<l n="14" num="4.2">Ou dira : « Je ne puis monter jusqu’à la foi !</l>
						<l n="15" num="4.3">Ô Dieu qui t’es voilé de ténèbres profondes,</l>
						<l n="16" num="4.4">Laisse-moi seul ! je vais, sans plus songer à toi ! »</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">Quand les foules, bien haut par l’Esprit emportées,</l>
						<l n="18" num="5.2">Jetteront dans l’oubli l’inutile douleur,</l>
						<l n="19" num="5.3">Quand douteurs et croyants, et sublimes athées</l>
						<l n="20" num="5.4">Éclairciront les nuits de l’esprit par le cœur !</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1">Quand la science et l’art par leurs portes divines</l>
						<l n="22" num="6.2">Montreront l’inconnu : la Vie ou le Néant !</l>
						<l n="23" num="6.3">Quand tous les cœurs auront dans toutes les poitrines</l>
						<l n="24" num="6.4">La régularité des flux de l’Océan !</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1">Quand nous marcherons tous dans la même pensée,</l>
						<l n="26" num="7.2">Cherchant un seul but, même en des chemins divers ;</l>
						<l n="27" num="7.3">Quand vers ce but sera sans relâche fixée</l>
						<l n="28" num="7.4">Toute la volonté ferme de l’Univers !</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1">Alors viendra la Paix, la grande Nourricière !</l>
						<l n="30" num="8.2">Alors plus de patrie ! un seul peuple de dieux !</l>
						<l n="31" num="8.3">L’Égalité luira vivante sur la terre !</l>
						<l n="32" num="8.4">La Liberté vivra splendide sous les cieux !</l>
					</lg>
					<closer>
						<dateline>
							<placeName>Toulon</placeName>,
							<date when="1866">20 septembre 1866</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>