<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Jeunes Croyances</title>
				<title type="sub">Le Rachat de la Tour</title>
				<title type="medium">Édition électronique</title>
				<author key="AIC">
					<name>
						<forename>Jean</forename>
						<surname>AICARD</surname>
					</name>
					<date from="1848" to="1921">1848-1921</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1274 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">AIC_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Jeunes Croyances</title>
						<author>Jean Aicard</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/Les_Jeunes_Croyances</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Jeunes Croyances</title>
								<author>Jean Aicard</author>
								<idno type="URI">https://archive.org/details/lesjeunescroyan00aicagoog</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Alphonse Lemerre Éditeur</publisher>
									<date when="1867">1867</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1867">1867</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Extraction à partir d’une version non définitive du texte sur wikisource.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-11-01" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-11-01" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">IV</head><div type="poem" key="AIC40">
					<head type="number">VI</head>
					<head type="main">À LAMARTINE</head>
					<lg n="1">
						<l n="1" num="1.1">Le temps heureux n’est plus où rayonnait la Grèce,</l>
						<l n="2" num="1.2">Où Périclès vivait, étoile du plein jour !</l>
						<l n="3" num="1.3">Où les peuples, ardents de force et de jeunesse,</l>
						<l n="4" num="1.4">Voyant un Dieu partout, sentaient partout l’amour !</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">Le temps, le temps est mort des couronnes civiques,</l>
						<l n="6" num="2.2">Où l’on n’oubliait plus le poëte vainqueur !</l>
						<l n="7" num="2.3">Il est bien mort, ce temps des vieilles républiques</l>
						<l n="8" num="2.4">Qui payaient largement les cœurs avec le cœur !</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">L’orgie en ses festins n’a même plus de roses !</l>
						<l n="10" num="3.2">Les âmes sont de cire, et les fleurs de métal ;</l>
						<l n="11" num="3.3">Des dieux et de l’amour il nous reste deux choses :</l>
						<l n="12" num="3.4">La pâle indifférence et le désir brutal !</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">Les jeunes d’aujourd’hui vaudraient-ils ceux d’Athènes ?</l>
						<l n="14" num="4.2">Eux qu’on voit, dédaigneux du juste en cheveux blancs,</l>
						<l n="15" num="4.3">Récolter ces moissons hâtives de leurs graines :</l>
						<l n="16" num="4.4">Des nouveau-nés déjà blêmes et tout tremblants !</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">D’autres l’ont dit : plus rien ne bat dans les poitrines !</l>
						<l n="18" num="5.2">Et s’il est quelque part, triste, sur les sommets,</l>
						<l n="19" num="5.3">Un héros de jadis, meurtri de nos ruines,</l>
						<l n="20" num="5.4">Et tel que notre temps n’en verra plus jamais !</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1">S’il reste un grand poëte et s’il reste un grand homme,</l>
						<l n="22" num="6.2">Ô miracle ! si grand qu’en un dernier effort,</l>
						<l n="23" num="6.3">La foule, par hasard, s’en souvienne et le nomme,</l>
						<l n="24" num="6.4">Un dormeur, réveillé, l’insulte, et se rendort !</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1">Ah ! comme il faut vouloir, pour garder l’espérance !…</l>
						<l n="26" num="7.2">Père, des bruits confus sont venus jusqu’à moi ;</l>
						<l n="27" num="7.3">On a cru t’émouvoir et troubler ton silence,</l>
						<l n="28" num="7.4">Mais, te sachant trop haut, j’ai répondu pour toi.</l>
					</lg>
					<closer>
						<dateline>
							<placeName>Paris</placeName>,
							<date when="1867">25 avril 1867</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>