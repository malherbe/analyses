<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA VERDURE DORÉE</title>
				<title type="medium">Édition électronique</title>
				<author key="DER">
					<name>
						<forename>Tristan</forename>
						<surname>DERÈME</surname>
					</name>
					<date from="1889" to="1941">1889-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2644 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">DER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA VERDURE DORÉE</title>
						<author>Tristan Derème</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/tristanderemelaverduredoree.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">LA VERDURE DORÉE</title>
						<author>Tristan Derème</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ÉDITIONS ÉMILE-PAUL FRÈRES</publisher>
							<date when="1908">1908</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1922">1922</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique
					après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DER147">
				<head type="number">CXLVII</head>
				<opener>
					<salute>A Georges Le Cardonnel.</salute>
				</opener>
				<lg n="1">
					<l n="1" num="1.1">Quelque rose que tu cueilles,</l>
					<l n="2" num="1.2">Une nuit la fanera ;</l>
					<l n="3" num="1.3">Le vent fait voler les feuilles,</l>
					<l n="4" num="1.4">Les amours, <subst hand="RR" reason="analysis" type="phonemization"><del>etc</del><add rend="hidden">et cétéra</add></subst>…</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Et pourtant j’aime les roses,</l>
					<l n="6" num="2.2">Le feuillage et les amours</l>
					<l n="7" num="2.3">Et bien d’autres belles choses</l>
					<l n="8" num="2.4">Qui ne durent pas toujours.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Durer, durer… Rien ne dure.</l>
					<l n="10" num="3.2">Accourez, comparaisons !</l>
					<l n="11" num="3.3">Rappelons que la verdure</l>
					<l n="12" num="3.4">Pas ne dure trois saisons.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">Tout passe et cela n’est pas ce</l>
					<l n="14" num="4.2">Que les gens n’ont dit assez ;</l>
					<l n="15" num="4.3">Ils ont écrit que tout passe</l>
					<l n="16" num="4.4">Et leurs livres sont passés,</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1">Sauf certains ; et les miens, Muses,</l>
					<l n="18" num="5.2">Dureront-ils plus longtemps</l>
					<l n="19" num="5.3">Qu’une voix de cornemuse</l>
					<l n="20" num="5.4">Qui se perd sur les étangs ?</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1">Mais qu’importe ? Toutes choses,</l>
					<l n="22" num="6.2">Ne durent-elles qu’un jour,</l>
					<l n="23" num="6.3">Les poèmes et les roses</l>
					<l n="24" num="6.4">Et les feuilles et l’amour,</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1">Toutes choses ne sont-elles</l>
					<l n="26" num="7.2">Les rameaux jaunes ou verts</l>
					<l n="27" num="7.3">Des guirlandes éternelles</l>
					<l n="28" num="7.4">Que déroule l’univers ?</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1">Toutes choses sont liées,</l>
					<l n="30" num="8.2">La mollesse et le tambour,</l>
					<l n="31" num="8.3">Les poèmes, les feuillées</l>
					<l n="32" num="8.4">Et les grâces de l’amour,</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1">Et chacune tient sa place</l>
					<l n="34" num="9.2">Dans cet hymne qui depuis</l>
					<l n="35" num="9.3">L’aube éternelle entrelace</l>
					<l n="36" num="9.4">Les chants des jours et des nuits.</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1">Quelque rose que tu cueilles,</l>
					<l n="38" num="10.2">Une nuit la fanera</l>
					<l n="39" num="10.3">Mais la rose avec ses feuilles,</l>
					<l n="40" num="10.4">C’est la vie. <subst hand="RR" reason="analysis" type="phonemization"><del>Etc</del><add rend="hidden">et cétéra</add></subst>…</l>
				</lg>
			</div></body></text></TEI>