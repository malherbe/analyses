<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA VERDURE DORÉE</title>
				<title type="medium">Édition électronique</title>
				<author key="DER">
					<name>
						<forename>Tristan</forename>
						<surname>DERÈME</surname>
					</name>
					<date from="1889" to="1941">1889-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2644 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">DER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA VERDURE DORÉE</title>
						<author>Tristan Derème</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/tristanderemelaverduredoree.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">LA VERDURE DORÉE</title>
						<author>Tristan Derème</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ÉDITIONS ÉMILE-PAUL FRÈRES</publisher>
							<date when="1908">1908</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1922">1922</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique
					après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DER114">
				<head type="number">CXIV</head>
				<lg n="1">
					<l n="1" num="1.1">Pour goûter au charme unique</l>
					<l n="2" num="1.2">Qui jaillit de ton baiser,</l>
					<l n="3" num="1.3">Diogène le Cynique</l>
					<l n="4" num="1.4">Courrait se faire raser.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">S’ils savaient ton regard, ivres</l>
					<l n="6" num="2.2">Les sages silencieux</l>
					<l n="7" num="2.3">Verraient s’ouvrir dans leurs livres</l>
					<l n="8" num="2.4">Des pivoines et des yeux.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Et, mordus de quelles fièvres !</l>
					<l n="10" num="3.2">Secouant toge ou veston,</l>
					<l n="11" num="3.3">Et sautant comme des lièvres,</l>
					<l n="12" num="3.4">Ils crieraient : Où la voit-on ?</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">Dans leurs veines : escarboucles</l>
					<l n="14" num="4.2">Liquides et plomb fondu,</l>
					<l n="15" num="4.3">Tu les verrais pour tes boucles</l>
					<l n="16" num="4.4">Poussant un cri éperdu,</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1">Tous, contemporains d’Ulysse</l>
					<l n="18" num="5.2">Et disciples de Bergson,</l>
					<l n="19" num="5.3">Se rouler nus, ô délice !</l>
					<l n="20" num="5.4">Sur des peaux de hérisson</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1">Car pour mordre à la grenade</l>
					<l n="22" num="6.2">Rouge des désirs ouverts,</l>
					<l n="23" num="6.3">Ils oublieraient la monade.</l>
					<l n="24" num="6.4">Le noumène et l’Univers.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1">Ah ! Vivons ! La page écrite</l>
					<l n="26" num="7.2">Ne vaut pas les lèvres qu’on</l>
					<l n="27" num="7.3">Mord ! Vers l’amour ! Démocrite,</l>
					<l n="28" num="7.4">Spinoza, Hegel, Bacon,</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><subst hand="RR" reason="analysis" type="phonemization"><del>M</del><add rend="hidden">Monsieur</add></subst>. Durkheim, Xénophane,</l>
					<l n="30" num="8.2">Vers l’amour, vous chanteriez,</l>
					<l n="31" num="8.3">Sans archet ni colophane</l>
					<l n="32" num="8.4">Violons extasiés !</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1">Ah ! que pèsent hypothèses,</l>
					<l n="34" num="9.2">Postulats, systèmes, lois ?</l>
					<l n="35" num="9.3">Ne faut-il que tu te taises</l>
					<l n="36" num="9.4">Ou que tu joignes ta voix</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1">Au grand tumulte des choses,</l>
					<l n="38" num="10.2">À l’amour qui fait ployer</l>
					<l n="39" num="10.3">Les cigognes et les roses,</l>
					<l n="40" num="10.4">La tulipe et l’épervier ?</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1">Viens, et contre moi pressée,</l>
					<l n="42" num="11.2">Aux brutes fais-moi pareil</l>
					<l n="43" num="11.3">En écrasant ma pensée</l>
					<l n="44" num="11.4">Sous la grâce d’un orteil.</l>
				</lg>
			</div></body></text></TEI>