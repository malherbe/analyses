<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA VERDURE DORÉE</title>
				<title type="medium">Édition électronique</title>
				<author key="DER">
					<name>
						<forename>Tristan</forename>
						<surname>DERÈME</surname>
					</name>
					<date from="1889" to="1941">1889-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2644 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">DER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA VERDURE DORÉE</title>
						<author>Tristan Derème</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/tristanderemelaverduredoree.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">LA VERDURE DORÉE</title>
						<author>Tristan Derème</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ÉDITIONS ÉMILE-PAUL FRÈRES</publisher>
							<date when="1908">1908</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1922">1922</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique
					après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DER89">
				<head type="number">LXXXIX</head>
				<lg n="1">
					<l n="1" num="1.1">En vain tu mets tes doigts sur mes yeux inquiets</l>
					<l n="2" num="1.2">Et me caches les prés, les branches et le ciel,</l>
					<l n="3" num="1.3"><space quantity="12" unit="char"></space>Ô doux amour, ô toi qui es</l>
					<l n="4" num="1.4"><space quantity="12" unit="char"></space>Du féminin au pluriel !</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Exiges-tu que d’une voix endolorie</l>
					<l n="6" num="2.2">Je dise les brasiers que ton sourire allume,</l>
					<l n="7" num="2.3"><space quantity="12" unit="char"></space>En un livre de la série</l>
					<l n="8" num="2.4"><space quantity="12" unit="char"></space>In-<subst hand="RR" reason="analysis" type="phonemization"><del>16</del><add rend="hidden">seize</add></subst> à <subst hand="RR" reason="analysis" type="phonemization"><del>3</del><add rend="hidden">trois</add></subst>fr<add hand="RR" reason="analysis" type="completion" rend="hidden">ancs</add>. le volume ;</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Ou que, trempant encor ma plume dans mon cœur,</l>
					<l n="10" num="3.2">Car déjà ton regard a séché l’encrier,</l>
					<l n="11" num="3.3"><space quantity="12" unit="char"></space>Je te figure, archer vainqueur,</l>
					<l n="12" num="3.4"><space quantity="12" unit="char"></space>Coiffé de myrte et de laurier ?</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">Ma plume ne sert plus qu’à débourrer la pipe</l>
					<l n="14" num="4.2">Que je fume le soir sous les saules moroses</l>
					<l n="15" num="4.3"><space quantity="12" unit="char"></space>En songeant à l’ombre qui fripe</l>
					<l n="16" num="4.4"><space quantity="12" unit="char"></space>Les espérances et les roses.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1">Laisse-moi. Je me plais à voir glisser le vol</l>
					<l n="18" num="5.2">Sur l’immobile azur des pigeons gris et blancs ;</l>
					<l n="19" num="5.3"><space quantity="12" unit="char"></space>À quoi bon nouer à mon col</l>
					<l n="20" num="5.4"><space quantity="12" unit="char"></space>Tes bras perfides et tremblants ?</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1">Tu verras en sanglots mainte âme évanouie</l>
					<l n="22" num="6.2">Baiser tes pieds en implorant la servitude ;</l>
					<l n="23" num="6.3"><space quantity="12" unit="char"></space>Moi, j’ouvrirai mon parapluie</l>
					<l n="24" num="6.4"><space quantity="12" unit="char"></space>Pour danser dans la solitude.</l>
				</lg>
			</div></body></text></TEI>