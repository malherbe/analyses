<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">MORT ET PRINTEMPS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2448 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_10</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruemortetprintemps.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>MORT ET PRINTEMPS</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Albert Messein</publisher>
							<date when="1932">1932</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques.</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1932">1932</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les titres des poèmes ont été corrigés en fonction de l’édition de référence.</p>
				<p>Les parties manquantes des poèmes "À SAINT GERMAIN" et "À LA NUIT" ont été ajoutées</p>
				<p>Des corrections métriques qui s’imposent ont été faites (erreurs d’édition).</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les retraits introduits automatiquement ont été modifiés conformément à l’édition de référence.</p>
				<p>Des corrections métriques ont été faites ; introduction de licences poétiques ; certe, encor...</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-09-02" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-09-02" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLR896">
				<head type="main">LE MONSTRE</head>
				<lg n="1">
					<l n="1" num="1.1">Barques à voiles de Honfleur</l>
					<l n="2" num="1.2">Qui dans le port battiez des ailes</l>
					<l n="3" num="1.3">Quand vous y reveniez, fidèles,</l>
					<l n="4" num="1.4">Après le large sans couleur,</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Vos départs diurnes, nocturnes,</l>
					<l n="6" num="2.2">Splendidement silencieux</l>
					<l n="7" num="2.3">De grands archanges taciturnes</l>
					<l n="8" num="2.4">Enfants des vagues et des cieux,</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Les formes que prenaient vos toiles</l>
					<l n="10" num="3.2">D’après les caprices du vent</l>
					<l n="11" num="3.3">Sous le soleil, sous les étoiles,</l>
					<l n="12" num="3.4">Blancheur de l’arrière à l’avant,</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">Et, quand le temps veut qu’on louvoie,</l>
					<l n="14" num="4.2">Vos retours titubants un peu,</l>
					<l n="15" num="4.3">Tout cela, poésie et joie</l>
					<l n="16" num="4.4">De notre ville au reflet bleu,</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1">Tout cela, rythme des marées,</l>
					<l n="18" num="5.2">Très vieux conte bleu de la mer,</l>
					<l n="19" num="5.3">Fini ! Barques adultérées,</l>
					<l n="20" num="5.4">Recevez mon regret amer.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1">Les moteurs, ces bêtes nouvelles,</l>
					<l n="22" num="6.2">Grondent entre vos flancs de bois.</l>
					<l n="23" num="6.3">O ! le silence d’autrefois !</l>
					<l n="24" num="6.4">Pourquoi porter encor vos ailes ?</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1">Vous n’en avez plus nul besoin.</l>
					<l n="26" num="7.2">Elles ne sont rien qu’un vestige,</l>
					<l n="27" num="7.3">Vous aussi, prises de vertige,</l>
					<l n="28" num="7.4">Vous voulez aller vite et loin.</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1">Départs, retours, affreux tapage,</l>
					<l n="30" num="8.2">Pétrole qui pue et salit,</l>
					<l n="31" num="8.3">Voiles qui restent dans leur pli</l>
					<l n="32" num="8.4">Quelque soit le sens du nuage,</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1">Le souffle du siècle a passé,</l>
					<l n="34" num="9.2">Universelle frénésie,</l>
					<l n="35" num="9.3">Pour tuer toute poésie.</l>
					<l n="36" num="9.4">Soit ! <hi rend="ital">Requiescat in pace</hi> !</l>
				</lg>
			</div></body></text></TEI>