<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">MORT ET PRINTEMPS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2448 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_10</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruemortetprintemps.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>MORT ET PRINTEMPS</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Albert Messein</publisher>
							<date when="1932">1932</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques.</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1932">1932</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les titres des poèmes ont été corrigés en fonction de l’édition de référence.</p>
				<p>Les parties manquantes des poèmes "À SAINT GERMAIN" et "À LA NUIT" ont été ajoutées</p>
				<p>Des corrections métriques qui s’imposent ont été faites (erreurs d’édition).</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les retraits introduits automatiquement ont été modifiés conformément à l’édition de référence.</p>
				<p>Des corrections métriques ont été faites ; introduction de licences poétiques ; certe, encor...</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-09-02" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-09-02" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">THE SKULL</head><div type="poem" key="DLR878">
					<head type="number">VIII</head>
					<head type="main">DISTIQUES</head>
					<lg n="1">
						<l n="1" num="1.1">Squelette, notre maître à tous,</l>
						<l n="2" num="1.2">Maigre captif au fond de nous,</l>
					</lg>
					<lg n="2">
						<l n="3" num="2.1">Squelette, constante présence</l>
						<l n="4" num="2.2">Qui t’effaces jusqu’à l’absence,</l>
					</lg>
					<lg n="3">
						<l n="5" num="3.1">Un domino, — la chair, la peau, —</l>
						<l n="6" num="3.2">Te couvre de son oripeau,</l>
					</lg>
					<lg n="4">
						<l n="7" num="4.1">Sous quoi, de manières discrètes,</l>
						<l n="8" num="4.2">Tu ne fais voir que tes arêtes.</l>
					</lg>
					<lg n="5">
						<l n="9" num="5.1">Cheville ouvrière, pourtant,</l>
						<l n="10" num="5.2">Seul solide, seul important,</l>
					</lg>
					<lg n="6">
						<l n="11" num="6.1">Pauvre squelette qu’on libère</l>
						<l n="12" num="6.2">Seulement à six pieds sous terre,</l>
					</lg>
					<lg n="7">
						<l n="13" num="7.1">Être muet, aveugle et sourd,</l>
						<l n="14" num="7.2">Toi qui ne vois jamais le jour,</l>
					</lg>
					<lg n="8">
						<l n="15" num="8.1">La chair orgueilleuse et fantasque</l>
						<l n="16" num="8.2">A beau te couvrir de ce masque,</l>
					</lg>
					<lg n="9">
						<l n="17" num="9.1">L’apparent rire de tes dents</l>
						<l n="18" num="9.2">Révèle le reste en dedans.</l>
					</lg>
					<lg n="10">
						<l n="19" num="10.1">Et cependant la chair t’ignore.</l>
						<l n="20" num="10.2">Elle te hait, elle t’abhorre !</l>
					</lg>
					<lg n="11">
						<l n="21" num="11.1">Parent pauvre écarté du jeu,</l>
						<l n="22" num="11.2">Chaste ascète en un mauvais lieu,</l>
					</lg>
					<lg n="12">
						<l n="23" num="12.1">Invisible, tu te promènes</l>
						<l n="24" num="12.2">Parmi les amours et les haines.</l>
					</lg>
					<lg n="13">
						<l n="25" num="13.1">C’est en vain, timide holà,</l>
						<l n="26" num="13.2">Que tu dis parfois : « Je suis là ! »</l>
					</lg>
					<lg n="14">
						<l n="27" num="14.1">Au dur fantôme qui la hante,</l>
						<l n="28" num="14.2">La chair molle, la chair changeante</l>
					</lg>
					<lg n="15">
						<l n="29" num="15.1">Répond : « je sens battre mon cœur,</l>
						<l n="30" num="15.2">Je vis ! Tais-toi ! Tu me fais peur ! »</l>
					</lg>
					<lg n="16">
						<l n="31" num="16.1">Mais va ! Ton élégance blanche</l>
						<l n="32" num="16.2">A son heure aura sa revanche,</l>
					</lg>
					<lg n="17">
						<l n="33" num="17.1">Car l’usurpatrice, au tombeau,</l>
						<l n="34" num="17.2">Cèdera lambeau par lambeau.</l>
					</lg>
					<lg n="18">
						<l n="35" num="18.1">Car, lentement, sa pourriture</l>
						<l n="36" num="18.2">Délivrera ton armature,</l>
					</lg>
					<lg n="19">
						<l n="37" num="19.1">Et, couché dans le dernier lit</l>
						<l n="38" num="19.2">Sur l’oreiller du grand oubli,</l>
					</lg>
					<lg n="20">
						<l n="39" num="20.1">Toi, sous la pierre délaissée</l>
						<l n="40" num="20.2">Dont la date s’est effacée,</l>
					</lg>
					<lg n="21">
						<l n="41" num="21.1">Tu crieras du fond de la mort :</l>
						<l n="42" num="21.2">« Petit bonhomme vit encor ! »</l>
					</lg>
				</div></body></text></TEI>