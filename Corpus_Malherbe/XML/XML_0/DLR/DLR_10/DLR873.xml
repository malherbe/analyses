<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">MORT ET PRINTEMPS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2448 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_10</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruemortetprintemps.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>MORT ET PRINTEMPS</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Albert Messein</publisher>
							<date when="1932">1932</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques.</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1932">1932</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les titres des poèmes ont été corrigés en fonction de l’édition de référence.</p>
				<p>Les parties manquantes des poèmes "À SAINT GERMAIN" et "À LA NUIT" ont été ajoutées</p>
				<p>Des corrections métriques qui s’imposent ont été faites (erreurs d’édition).</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les retraits introduits automatiquement ont été modifiés conformément à l’édition de référence.</p>
				<p>Des corrections métriques ont été faites ; introduction de licences poétiques ; certe, encor...</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-09-02" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-09-02" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">THE SKULL</head><div type="poem" key="DLR873">
					<head type="number">III</head>
					<head type="main">DIALOGUE</head>
					<lg n="1">
						<l n="1" num="1.1">Bien que très occupée à vivre,</l>
						<l n="2" num="1.2">J’examinais ce crâne humain</l>
						<l n="3" num="1.3"><space unit="char" quantity="12"></space>Dans ma main,</l>
						<l n="4" num="1.4">Et le déchiffrais comme un livre.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">— Oh ! Combien j’ai pitié de toi… !</l>
						<l n="6" num="2.2">Commença notre dialogue.</l>
						<l n="7" num="2.3"><space unit="char" quantity="12"></space>Mais lui, rogue :</l>
						<l n="8" num="2.4">— Sais-tu comme je te plains, moi ?</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">Muscles, nerfs, organes, viscères,</l>
						<l n="10" num="3.2">Toute pleine de battements,</l>
						<l n="11" num="3.3"><space unit="char" quantity="12"></space>De ferments,</l>
						<l n="12" num="3.4">Et de fluides délétères,</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">Il te faut les soins et l’effort</l>
						<l n="14" num="4.2">Pour que rien en toi ne dévie,</l>
						<l n="15" num="4.3"><space unit="char" quantity="12"></space>Car la vie</l>
						<l n="16" num="4.4">Est en constant danger de mort.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">Bien que me regarder t’attriste,</l>
						<l n="18" num="5.2">Moi, sec et dur comme un rocher,</l>
						<l n="19" num="5.3"><space unit="char" quantity="12"></space>Arraché,</l>
						<l n="20" num="5.4">Des pourritures, je persiste.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1">Mais toi, changée à tout instant,</l>
						<l n="22" num="6.2">Sans cesse tu te décomposes,</l>
						<l n="23" num="6.3"><space unit="char" quantity="12"></space>Regrettant</l>
						<l n="24" num="6.4">Enfance et jeunesse, ces roses.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1">Toi tu meurs. Moi je suis la Mort.</l>
						<l n="26" num="7.2">Je suis dans l’absolu. Regarde !</l>
						<l n="27" num="7.3"><space unit="char" quantity="12"></space>Rien ne mord</l>
						<l n="28" num="7.4">Sur ma face hilarde et camarde.</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1">Pauvres vivants ! Votre au delà,</l>
						<l n="30" num="8.2">Intact pour des siècles encore,</l>
						<l n="31" num="8.3"><space unit="char" quantity="12"></space>Le voilà !</l>
						<l n="32" num="8.4">C’est le Dieu que votre âme adore !</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1">— Ainsi donc ta solidité,</l>
						<l n="34" num="9.2">Dis-je, est la fin de l’aventure,</l>
						<l n="35" num="9.3"><space unit="char" quantity="12"></space>Vérité</l>
						<l n="36" num="9.4">Qui ris de toute ta denture ?</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1">Laisse-moi te montrer mes dents,</l>
						<l n="38" num="10.2">Ce commencement du squelette.</l>
						<l n="39" num="10.3"><space unit="char" quantity="12"></space>En dedans</l>
						<l n="40" num="10.4">Je le sens tout entier qui guette.</l>
					</lg>
					<lg n="11">
						<l n="41" num="11.1">J’ai peur de mon futur portrait.</l>
						<l n="42" num="11.2">J’ai peur de cet ossement stable,</l>
						<l n="43" num="11.3"><space unit="char" quantity="12"></space>Monstre abstrait</l>
						<l n="44" num="11.4">Que j’ai posé là sur ma table.</l>
					</lg>
					<lg n="12">
						<l n="45" num="12.1">J’ai peur de toi, final destin,</l>
						<l n="46" num="12.2">O macabre globe terrestre,</l>
						<l n="47" num="12.3"><space unit="char" quantity="12"></space>Monde éteint,</l>
						<l n="48" num="12.4">Toi que, vivante, je séquestre !</l>
					</lg>
					<lg n="13">
						<l n="49" num="13.1">Mais que m’importe ! J’ai le temps</l>
						<l n="50" num="13.2">D’attendre la métamorphose !</l>
						<l n="51" num="13.3"><space unit="char" quantity="12"></space>… Et la chose</l>
						<l n="52" num="13.4">M’a répondu : « Pas bien longtemps ! »</l>
					</lg>
				</div></body></text></TEI>