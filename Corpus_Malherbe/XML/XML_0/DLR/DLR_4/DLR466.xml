<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FIGURE DE PROUE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2836 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/delaruemardruepoesies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">La Figure de Proue</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/stream/lafiguredeproue00dela/lafiguredeproue00dela_djvu.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>La Figure de Proue</title>
								<author>Lucie Delarue-Mardrus</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Fasquelle</publisher>
									<date when="1908">1908</date>
								</imprint>
							</monogr>
							<note>Source des poèmes ajoutés et édition de référence pour les corrections métriques.</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1908">1908</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
				<p>Des poèmes ont été ajoutés afin d’être conforme à l’édition de référence (source : archive.org)</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2023-06-24" who="RR">Quelques corrections d’erreurs de numérisation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">AU PORT</head><div type="poem" key="DLR466">
					<head type="main">TRIOMPHE</head>
					<lg n="1">
						<l n="1" num="1.1">Le cimetière où dort un peu de Normandie</l>
						<l n="2" num="1.2">Repose, étroit et vert, dans la fin de l’été.</l>
						<l n="3" num="1.3">Il nous plaît, visitant chaque pierre tiédie,</l>
						<l n="4" num="1.4">Lire les noms des morts sans immortalité.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">Le secret des défunts et de leur chair changeante.</l>
						<l n="6" num="2.2">Tout ce matériel et variable après</l>
						<l n="7" num="2.3">Est sous l’herbe, les fleurs, les croix et les regrets.</l>
						<l n="8" num="2.4">La terre est fourbe et cache bien ce qui la hante.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">Sur un tertre envahi, le nom s’est effacé.</l>
						<l n="10" num="3.2">Nulle couronne, au vent qui passe, ne cliquette.</l>
						<l n="11" num="3.3">Celui qui gît ici n’est plus rien qu’un squelette.</l>
						<l n="12" num="3.4">Cadavre du cadavre et passé du passé…</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">— O fatale ! O banale ! O toi l’insatiable</l>
						<l n="14" num="4.2">A qui l’amour fournit tant d’êtres, tant de morts,</l>
						<l n="15" num="4.3">Que ton abîme ouvert ne me soit redevable</l>
						<l n="16" num="4.4">Que de mon seul fragile, étroit et tendre corps !</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">Je n’apporterai point l’offrande maternelle,</l>
						<l n="18" num="5.2">La chair humaine qui naîtrait de ma beauté</l>
						<l n="19" num="5.3">A l’éternelle mort de la Vie éternelle.</l>
						<l n="20" num="5.4">Je triomphe de toi par ma stérilité.</l>
					</lg>
				</div></body></text></TEI>