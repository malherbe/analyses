<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FIGURE DE PROUE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2836 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/delaruemardruepoesies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">La Figure de Proue</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/stream/lafiguredeproue00dela/lafiguredeproue00dela_djvu.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>La Figure de Proue</title>
								<author>Lucie Delarue-Mardrus</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Fasquelle</publisher>
									<date when="1908">1908</date>
								</imprint>
							</monogr>
							<note>Source des poèmes ajoutés et édition de référence pour les corrections métriques.</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1908">1908</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
				<p>Des poèmes ont été ajoutés afin d’être conforme à l’édition de référence (source : archive.org)</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2023-06-24" who="RR">Quelques corrections d’erreurs de numérisation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">BARBARESQUES</head><div type="poem" key="DLR382">
					<head type="main">A LA LOUANGE DES PORTS DE MER</head>
					<lg n="1">
						<l n="1" num="1.1">Vous vivez en mon cœur, ports de mer, ports de mer</l>
						<l n="2" num="1.2">Arrondis et calmés devant le large amer.</l>
					</lg>
					<lg n="2">
						<l n="3" num="2.1">Autour des paquebots arrêtés sur leurs quilles,</l>
						<l n="4" num="2.2">Cette odeur de goudron, d’ordure et de coquilles,</l>
					</lg>
					<lg n="3">
						<l n="5" num="3.1">Cette odeur rude du départ et du retour,</l>
						<l n="6" num="3.2">Je la respire, sur vos quais, avec amour.</l>
					</lg>
					<lg n="4">
						<l n="7" num="4.1">J’aime le clapotis qui berce et qui soulève</l>
						<l n="8" num="4.2">En vous, tant de reflets, de commerce et de rêve,</l>
					</lg>
					<lg n="5">
						<l n="9" num="5.1">Et l’esprit du voyage erre à travers vos mâts</l>
						<l n="10" num="5.2">Dont craquent doucement les sous-bois délicats.</l>
					</lg>
					<lg n="6">
						<l n="11" num="6.1">Beaux ports, beaux ports de mer de mes villes diverses</l>
						<l n="12" num="6.2">Dans le bleu méridional ou les averses,</l>
					</lg>
					<lg n="7">
						<l n="13" num="7.1">Beaux ports où Ton peut voir se balancer de près</l>
						<l n="14" num="7.2">Le soleil pris, le soir, dans le haut des agrès,</l>
					</lg>
					<lg n="8">
						<l n="15" num="8.1">Je vous chéris du fond de ma première enfance</l>
						<l n="16" num="8.2">Qui devinait déjà la joie et la souffrance.</l>
					</lg>
					<lg n="9">
						<l n="17" num="9.1">Les barques de Honfleur qui se marquent <subst hand="RR" reason="analysis" type="phonemization"><add rend="hidden">ach</add><del>H</del></subst>. O.</l>
						<l n="18" num="9.2">Partaient sans bruit à l’heure où la mer monte haut.</l>
					</lg>
					<lg n="10">
						<l n="19" num="10.1">Elles partaient vers l’inconnu qui tente et brille.</l>
						<l n="20" num="10.2">Avec l’obscur désir d’une petite fille,</l>
					</lg>
					<lg n="11">
						<l n="21" num="11.1">Alors que j’ignorais encor que mon destin</l>
						<l n="22" num="11.2">Me donnerait la mer, le risque et le butin.</l>
					</lg>
					<lg n="12">
						<l n="23" num="12.1">Or, puisque maintenant se gonfle ma poitrine</l>
						<l n="24" num="12.2">De grand enthousiasme et de brise marine.</l>
					</lg>
					<lg n="13">
						<l n="25" num="13.1">Salut à vous ! J’ai pris aussi mon large vol</l>
						<l n="26" num="13.2">Devers un autre ciel, devers un autre sol,</l>
					</lg>
					<lg n="14">
						<l n="27" num="14.1">O vous qui m’accueillez au bout de tout voyage,</l>
						<l n="28" num="14.2">Beaux ports, beaux ports de mon bonheur et de mon âge</l>
					</lg>
				</div></body></text></TEI>