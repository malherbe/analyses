<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FIGURE DE PROUE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2836 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/delaruemardruepoesies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">La Figure de Proue</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/stream/lafiguredeproue00dela/lafiguredeproue00dela_djvu.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>La Figure de Proue</title>
								<author>Lucie Delarue-Mardrus</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Fasquelle</publisher>
									<date when="1908">1908</date>
								</imprint>
							</monogr>
							<note>Source des poèmes ajoutés et édition de référence pour les corrections métriques.</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1908">1908</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
				<p>Des poèmes ont été ajoutés afin d’être conforme à l’édition de référence (source : archive.org)</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2023-06-24" who="RR">Quelques corrections d’erreurs de numérisation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PREMIER ISLAM</head><div type="poem" key="DLR360">
					<head type="main">FUMERIE D’ÉTÉ</head>
					<div type="section" n="1">
						<head type="number">I</head>
						<lg n="1">
						<l n="1" num="1.1">La maison est obscure au fond de la chaleur.</l>
						<l n="2" num="1.2">Comme, profondément, je respire l’étoile</l>
						<l n="3" num="1.3">De tabac, l’existence est à travers un voile,</l>
						<l n="4" num="1.4">Hormis l’étoile en feu qui ravage mon cœur.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">Je suis d’avance mûre en longs plis. Je possède</l>
						<l n="6" num="2.2">Des sens orientaux rêvés par l’Occident.</l>
						<l n="7" num="2.3">Dans ma bouche, déjà, la .mort montre les dents.</l>
						<l n="8" num="2.4">Mais l’été m’engourdit d’un bercement si tiède !</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">C’est l’absence. Ce sont les jours coloniaux.</l>
						<l n="10" num="3.2">On ne pourra jamais revenir de ces choses ;</l>
						<l n="11" num="3.3">On est la cantharide ivre au creux d’une rose…</l>
						<l n="12" num="3.4">Au retour, nous serons étrangers jusqu’aux os.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">Qu’on se taise. Je vis d’ouate et de silence.</l>
						<l n="14" num="4.2">Maintenant, maintenant, saurais-je d’où je sors ?</l>
						<l n="15" num="4.3">Est-ce que je finis ? Est-ce que je commence ?</l>
						<l n="16" num="4.4">— Qui me fera jamais lever d’entre les morts ?</l>
					</lg>
				</div>
					<div type="section" n="2">
						<head type="number">II</head>
						<lg n="1">
							<l n="17" num="1.1">Petite cigarette en or d’extrême été</l>
							<l n="18" num="1.2">Au bout de quoi le monde flanche,</l>
							<l n="19" num="1.3">A cause de ton feu mon être est tourmenté</l>
							<l n="20" num="1.4">Par un songe de ma peau blanche.</l>
						</lg>
						<lg n="2">
							<l n="21" num="2.1">Loin d’ici, des drapés sur des visages bruns.</l>
							<l n="22" num="2.2">Des jours les plus chauds de la terre,</l>
							<l n="23" num="2.3">Des pâmoisons de fleurs couveuses de parfums.</l>
							<l n="24" num="2.4">J’ai rêvé de rhum solitaire.</l>
						</lg>
						<lg n="3">
							<l n="25" num="3.1">Parce que les regards humains qui me voyaient</l>
							<l n="26" num="3.2">Ne pouvaient pas voir mes merveilles.</l>
							<l n="27" num="3.3">J’ai voulu de grands soirs marins qui louvoyaient</l>
							<l n="28" num="3.4">D’Anglais seul avec ses bouteilles,</l>
						</lg>
						<lg n="4">
							<l n="29" num="4.1">Pour, loin à tout jamais des mondes, sur un flot,</l>
							<l n="30" num="4.2">Parmi l’odeur saumâtre, vivre</l>
							<l n="31" num="4.3">Dans la cabine saure et contre le hublot</l>
							<l n="32" num="4.4">Ineffable d’un bateau ivre.</l>
						</lg>
					</div>
				</div></body></text></TEI>