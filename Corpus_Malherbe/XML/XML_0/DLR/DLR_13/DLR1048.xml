<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CHOIX DE POÈMES</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1917 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">DLR_13</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Choix de poèmes</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie Alphonse Lemerre</publisher>
							<date when="1951">1951</date>
						</imprint>
					</monogr>
					<note>Édition numérisée à partir d’un ouvrage prêté par la BPU de neuchatel</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1951">1951</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La partie en prose (FRAGMENT D’UN TEXTE DE L’AUTEUR PRÉFAÇANT DEUX POÈMES D’EDGAR POE) n’est incluse.</p>
				<p>Les indications de la première édition d’un poème données dans la table des matières ont été reportées comme sous-titre des poèmes. L’année d’édition a été ajoutée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell (correction d’erreur d’océrisation).</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-01-14" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-01-14" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CHAPITRE PREMIER</head><head type="main_part">L’Estuaire</head><head type="sub_part">SOUFFLES DE TEMPÊTE, Fasquelle, 1918</head><div type="poem" key="DLR1048">
					<head type="main">L’Étreinte marine</head>
					<head type="sub_2">OCCIDENT, Ed. de la Revue Blanche, 1901</head>
					<lg n="1">
						<l n="1" num="1.1">Une voix sous-marine enfle l’inflexion</l>
						<l n="2" num="1.2">De ta bouche et la mer est glauque tout entière</l>
						<l n="3" num="1.3">De rouler ta chair pâle en son remous profond.</l>
					</lg>
					<lg n="2">
						<l n="4" num="2.1">Et la queue enroulée à ta stature altière</l>
						<l n="5" num="2.2">Fait rouler sa splendeur au ciel plein de couchant,</l>
						<l n="6" num="2.3">Et, parmi les varechs où tu fais ta litière,</l>
					</lg>
					<lg n="3">
						<l n="7" num="3.1">Moi qui passe le long des eaux, j’ouïs ton chant</l>
						<l n="8" num="3.2">Toujours, et, sans te voir jamais, je te suppose</l>
						<l n="9" num="3.3">Dans ton hybride grâce et ton geste alléchant.</l>
					</lg>
					<lg n="4">
						<l n="10" num="4.1">Je sais l’eau qui ruisselle à ta nudité rose,</l>
						<l n="11" num="4.2">Visqueuse et te salant journellement la chair</l>
						<l n="12" num="4.3">Où cette. flore étrange et vivante est éclose ;</l>
					</lg>
					<lg n="5">
						<l n="13" num="5.1">Et tes doigts dont chacun pèse du. chaton clair</l>
						<l n="14" num="5.2">Que vint y incruster l’aigue ou le coquillage</l>
						<l n="15" num="5.3">Et ta tête coiffée au hasard de la mer ;</l>
					</lg>
					<lg n="6">
						<l n="16" num="6.1">La blanche bave dont bouillonne ton sillage,</l>
						<l n="17" num="6.2">L’astérie à ton front et tes flancs gras d’oursins</l>
						<l n="18" num="6.3">Et la perle que prit ton oreille au passage ;</l>
					</lg>
					<lg n="7">
						<l n="19" num="7.1">Et comment est plaquée en rond entre tes seins</l>
						<l n="20" num="7.2">La méduse ou le poulpe aux grêles tentacules,</l>
						<l n="21" num="7.3">Et tes colliers d’écume humides et succincts.</l>
					</lg>
					<lg n="8">
						<l n="22" num="8.1">Je te sais, ô sirène occulte qui circules</l>
						<l n="23" num="8.2">Dans les flux et reflux que hante mon loisir</l>
						<l n="24" num="8.3">Triste et grave, les soirs, parmi les crépuscules,</l>
					</lg>
					<lg n="9">
						<l n="25" num="9.1">Jumelle de mon âme austère et sans plaisir,</l>
						<l n="26" num="9.2">Sirène de ma mer natale et quotidienne,</l>
						<l n="27" num="9.3">O sirène de mon perpétuel désir !</l>
					</lg>
					<lg n="10">
						<l n="28" num="10.1">O chevelure ! ô hanche enflée avec la mienne,</l>
						<l n="29" num="10.2">Seins arrondis avec mes seins au va-et-vient</l>
						<l n="30" num="10.3">De la mer, ô fards clairs, ô toi, chair neustrienne !</l>
					</lg>
					<lg n="11">
						<l n="31" num="11.1">Quand pourrai-Je sentir ton cœur contre le mien</l>
						<l n="32" num="11.2">Battre sous ta poitrine humide de marée.</l>
						<l n="33" num="11.3">Et fermer mon manteau lourd sur ton corps païen,</l>
					</lg>
					<lg n="12">
						<l n="34" num="12.1">Pour t’avoir nue ainsi qu’une anguille effarée</l>
						<l n="35" num="12.2">A moi, dans le frisson mouillé des goëmons,</l>
						<l n="36" num="12.3">Et posséder enfin ta bouche désirée ?</l>
					</lg>
					<lg n="13">
						<l n="37" num="13.1">Ou quel soir, descendue en silence des monts</l>
						<l n="38" num="13.2">Et des forêts vers toi, dans tes bras maritimes</l>
						<l n="39" num="13.3">Viendras-tu m’emporter pour, d’avals en amonts,</l>
					</lg>
					<lg n="14">
						<l n="40" num="14.1">Balancer notre étreinte au remous des abîmes ?</l>
					</lg>
				</div></body></text></TEI>