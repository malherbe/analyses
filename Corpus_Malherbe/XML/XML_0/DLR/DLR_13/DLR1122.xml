<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CHOIX DE POÈMES</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1917 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">DLR_13</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Choix de poèmes</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie Alphonse Lemerre</publisher>
							<date when="1951">1951</date>
						</imprint>
					</monogr>
					<note>Édition numérisée à partir d’un ouvrage prêté par la BPU de neuchatel</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1951">1951</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La partie en prose (FRAGMENT D’UN TEXTE DE L’AUTEUR PRÉFAÇANT DEUX POÈMES D’EDGAR POE) n’est incluse.</p>
				<p>Les indications de la première édition d’un poème données dans la table des matières ont été reportées comme sous-titre des poèmes. L’année d’édition a été ajoutée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell (correction d’erreur d’océrisation).</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-01-14" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-01-14" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">TRADUCTIONS <lb></lb>en vers français <lb></lb>par <lb></lb>LUCIE DELARUE-MARDRUS</head><head type="main_subpart">Six Poèmes d’Emily Brontë</head><div type="poem" key="DLR1122">
						<head type="main">Le Souvenir</head>
						<lg n="1">
							<l n="1" num="1.1">Froid dans la terre, et la neige sur toi,</l>
							<l n="2" num="1.2">Loin, dans la tombe où l’on a dû te mettre,</l>
							<l n="3" num="1.3">T’ai-je oublié, mon seul amour à moi,</l>
							<l n="4" num="1.4">Malgré le temps qui sépare les êtres ?</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1">Quand je suis seule, est-ce que ma douleur</l>
							<l n="6" num="2.2">Ne s’en va pas à travers la montagne</l>
							<l n="7" num="2.3">Plier son aile, au nord, dans la campagne</l>
							<l n="8" num="2.4">Où la bruyère a recouvert ton cœur ?</l>
						</lg>
						<lg n="3">
							<l n="9" num="3.1">Froid dans la terre, et quinze hivers de glace,</l>
							<l n="10" num="3.2">Parmi ces monts, transformés en printemps…</l>
							<l n="11" num="3.3">Certes, fidèle, et qui garde ta place</l>
							<l n="12" num="3.4">Après avoir tant changé, souffert tant !</l>
						</lg>
						<lg n="4">
							<l n="13" num="4.1">Mon doux amour du passé, va, pardonne</l>
							<l n="14" num="4.2">Si je t’oublie en ce monde mouvant !</l>
							<l n="15" num="4.3">D’autres désirs, d’autres espoirs vivants</l>
							<l n="16" num="4.4">Ne t’enlèvent point ce que je te donne.</l>
						</lg>
						<lg n="5">
							<l n="17" num="5.1">Nulle clarté n’a plus brillé jamais :</l>
							<l n="18" num="5.2">Plus rien, pour moi, du haut du ciel ne tombe.</l>
							<l n="19" num="5.3">Tout mon bonheur est fini désormais,</l>
							<l n="20" num="5.4">Tout mon bonheur avec toi dans la tombe.</l>
						</lg>
						<lg n="6">
							<l n="21" num="6.1">Mais, terminés les jours des songes d’or,</l>
							<l n="22" num="6.2">Le désespoir lui-même, qui nous broie,</l>
							<l n="23" num="6.3">N’ayant pas su me conduire à ma mort</l>
							<l n="24" num="6.4">J’ai donc vécu sans un secours de joie.</l>
						</lg>
						<lg n="7">
							<l n="25" num="7.1">J’ai refoulé les inutiles pleurs,</l>
							<l n="26" num="7.2">Sevré mon âme en quête de la tienne,</l>
							<l n="27" num="7.3">Et ce désir d’habiter, sous les fleurs,</l>
							<l n="28" num="7.4">Ta tombe qui déjà m’est plus que mienne.</l>
						</lg>
						<lg n="8">
							<l n="29" num="8.1">Et depuis lors je n’ose plus céder</l>
							<l n="30" num="8.2">Au souvenir plein d’un amer délice</l>
							<l n="31" num="8.3">Car, si je bois à ce divin calice,</l>
							<l n="32" num="8.4">Comment donc vivre en un monde vidé ?</l>
						</lg>
					</div></body></text></TEI>