<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CHOIX DE POÈMES</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1917 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">DLR_13</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Choix de poèmes</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie Alphonse Lemerre</publisher>
							<date when="1951">1951</date>
						</imprint>
					</monogr>
					<note>Édition numérisée à partir d’un ouvrage prêté par la BPU de neuchatel</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1951">1951</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La partie en prose (FRAGMENT D’UN TEXTE DE L’AUTEUR PRÉFAÇANT DEUX POÈMES D’EDGAR POE) n’est incluse.</p>
				<p>Les indications de la première édition d’un poème données dans la table des matières ont été reportées comme sous-titre des poèmes. L’année d’édition a été ajoutée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell (correction d’erreur d’océrisation).</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-01-14" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-01-14" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CHAPITRE II</head><head type="main_part">Honfleur</head><div type="poem" key="DLR1051">
					<head type="main">Honfleur</head>
					<head type="sub_2">SOUFFLES DE TEMPÊTE, Fasquelle, 1918</head>
					<lg n="1">
						<l n="1" num="1.1">L’ombre d’un grand nuage est sur l’eau comme une île.</l>
						<l n="2" num="1.2">L’estuaire est plus beau qu’aucune fiction.</l>
						<l n="3" num="1.3"><space unit="char" quantity="8"></space>La vieille navigation</l>
						<l n="4" num="1.4"><space unit="char" quantity="8"></space>Bat des ailes parmi la ville.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">Après les toits salés commence le grand foin,</l>
						<l n="6" num="2.2">Et les fermes sont là dans le bleu des herbages.</l>
						<l n="7" num="2.3"><space unit="char" quantity="8"></space>L’odeur des pommes vient de loin</l>
						<l n="8" num="2.4"><space unit="char" quantity="8"></space>Se joindre au goudron des cordages.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">Je n’ai pas vu la fin de mes ravissements,</l>
						<l n="10" num="3.2">Honfleur tout en ardoise où pourtant je suis née,</l>
						<l n="11" num="3.3"><space unit="char" quantity="8"></space>O ville riche d’éléments,</l>
						<l n="12" num="3.4"><space unit="char" quantity="8"></space>Nombreuse, bien assaisonnée.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">Sont-ce tes toits vieillots gui se pressent si fort,</l>
						<l n="14" num="4.2">Ta petite marine et ta campagne verte</l>
						<l n="15" num="4.3"><space unit="char" quantity="8"></space>Que je chéris, ou bien ton port</l>
						<l n="16" num="4.4"><space unit="char" quantity="8"></space>Qui te fait toujours entr ’ouverte ?</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">Rien que de bon, de pur, pour cette ville-ci !</l>
						<l n="18" num="5.2">Moi qui suis pour jamais vouée à la chimère,</l>
						<l n="19" num="5.3"><space unit="char" quantity="8"></space>Je l’aime simplement, ainsi</l>
						<l n="20" num="5.4"><space unit="char" quantity="8"></space>Qu’on aime son père et sa mère.</l>
					</lg>
				</div></body></text></TEI>