<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CHOIX DE POÈMES</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1917 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">DLR_13</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Choix de poèmes</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie Alphonse Lemerre</publisher>
							<date when="1951">1951</date>
						</imprint>
					</monogr>
					<note>Édition numérisée à partir d’un ouvrage prêté par la BPU de neuchatel</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1951">1951</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La partie en prose (FRAGMENT D’UN TEXTE DE L’AUTEUR PRÉFAÇANT DEUX POÈMES D’EDGAR POE) n’est incluse.</p>
				<p>Les indications de la première édition d’un poème données dans la table des matières ont été reportées comme sous-titre des poèmes. L’année d’édition a été ajoutée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell (correction d’erreur d’océrisation).</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-01-14" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-01-14" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CHAPITRE VI</head><head type="main_part">Religion</head><div type="poem" key="DLR1080">
					<head type="main">Laudes</head>
					<head type="sub_2">LES SEPT DOULEURS D’OCTOBRE, Ferenczi, 1930</head>
					<lg n="1">
						<l n="1" num="1.1">Si je croyais en vous, si je croyais en vous,</l>
						<l n="2" num="1.2"><space unit="char" quantity="8"></space>Je serais sans cesse à genoux.</l>
					</lg>
					<lg n="2">
						<l n="3" num="2.1">Je n’aurais pas assez de ma plus grande lyre</l>
						<l n="4" num="2.2"><space unit="char" quantity="8"></space>Pour tout ce qu’il faudrait vous dire.</l>
					</lg>
					<lg n="3">
						<l n="5" num="3.1">Je vous dirais : Merci pour le vent, pour la mer ;</l>
						<l n="6" num="3.2"><space unit="char" quantity="8"></space>Pour le ciel ténébreux ou clair.</l>
					</lg>
					<lg n="4">
						<l n="7" num="4.1">Merci pour les prés verts rebrodés de corolles,</l>
						<l n="8" num="4.2"><space unit="char" quantity="8"></space>Le soleil, les averses molles.</l>
					</lg>
					<lg n="5">
						<l n="9" num="5.1">Merci pour les parfums, merci pour les couleurs,</l>
						<l n="10" num="5.2"><space unit="char" quantity="8"></space>Pour les oiseaux_ et pour les fleurs.</l>
					</lg>
					<lg n="6">
						<l n="11" num="6.1">Merci pour les saisons dont chacune m’étonne,</l>
						<l n="12" num="6.2"><space unit="char" quantity="8"></space>Et merci surtout pour l’automne.</l>
					</lg>
					<lg n="7">
						<l n="13" num="7.1">Merci pour la beauté regardée en tous lieux,</l>
						<l n="14" num="7.2"><space unit="char" quantity="8"></space>Et de m’avoir donné des yeux.</l>
					</lg>
					<lg n="8">
						<l n="15" num="8.1">Merci, mon Dieu, merci de m’avoir ainsi faite</l>
						<l n="16" num="8.2"><space unit="char" quantity="8"></space>Que je sois sur terre un poète.</l>
					</lg>
					<lg n="9">
						<l n="17" num="9.1">Merci pour mon amour passionné de l’art,</l>
						<l n="18" num="9.2"><space unit="char" quantity="8"></space>Merci pour ma vie à l’écart,</l>
					</lg>
					<lg n="10">
						<l n="19" num="10.1">Pour ces deux mains qui font chanter dans la musique</l>
						<l n="20" num="10.2"><space unit="char" quantity="8"></space>Ce qui me reste de physique,</l>
					</lg>
					<lg n="11">
						<l n="21" num="11.1">Pour cette hypnose unie à la lucidité,</l>
						<l n="22" num="11.2"><space unit="char" quantity="8"></space>Pour cet amour de la bonté,</l>
					</lg>
					<lg n="12">
						<l n="23" num="12.1">Pour ce détachement qui s’affirme sans cesse</l>
						<l n="24" num="12.2"><space unit="char" quantity="8"></space>Devant la fin de la jeunesse,</l>
					</lg>
					<lg n="13">
						<l n="25" num="13.1">Pour la mysticité d’un cœur étrange et fort</l>
						<l n="26" num="13.2"><space unit="char" quantity="8"></space>Que toujours a charmé la mort,</l>
					</lg>
					<lg n="14">
						<l n="27" num="14.1">Pour tout cela merci, pour tout cela louange</l>
						<l n="28" num="14.2"><space unit="char" quantity="8"></space>Sur l’invisible luth de l’ange,</l>
					</lg>
					<lg n="15">
						<l n="29" num="15.1">Et pardon, et pardon jusqu’au fond de mon cœur,</l>
						<l n="30" num="15.2"><space unit="char" quantity="8"></space>Mon Dieu, d’aimer tant la douleur !</l>
					</lg>
				</div></body></text></TEI>