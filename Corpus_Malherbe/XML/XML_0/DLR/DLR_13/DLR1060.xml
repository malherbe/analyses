<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CHOIX DE POÈMES</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1917 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">DLR_13</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Choix de poèmes</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie Alphonse Lemerre</publisher>
							<date when="1951">1951</date>
						</imprint>
					</monogr>
					<note>Édition numérisée à partir d’un ouvrage prêté par la BPU de neuchatel</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1951">1951</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La partie en prose (FRAGMENT D’UN TEXTE DE L’AUTEUR PRÉFAÇANT DEUX POÈMES D’EDGAR POE) n’est incluse.</p>
				<p>Les indications de la première édition d’un poème données dans la table des matières ont été reportées comme sous-titre des poèmes. L’année d’édition a été ajoutée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell (correction d’erreur d’océrisation).</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-01-14" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-01-14" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CHAPITRE III</head><head type="main_part">Chevauchées</head><div type="poem" key="DLR1060">
					<head type="main">Éloge de mon Cheval</head>
					<head type="sub_2">LA FIGURE DE PROUE, Fasquelle, 1908</head>
					<lg n="1">
						<l n="1" num="1.1">Mon cheval au poitrail solide, à l’œil de feu,</l>
						<l n="2" num="1.2"><space unit="char" quantity="4"></space>Frère joyeux de mon âme animale,</l>
						<l n="3" num="1.3">Ton sang arabe bout comme le mien, beau mâle,</l>
						<l n="4" num="1.4"><space unit="char" quantity="8"></space>Et tu comprends si bien le jeu !</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">Voici notre statue haute et momentanée.</l>
						<l n="6" num="2.2"><space unit="char" quantity="4"></space>Chaque jour pour nous est le jour des bonds</l>
						<l n="7" num="2.3"><space unit="char" quantity="8"></space>Et des caprices furibonds</l>
						<l n="8" num="2.4"><space unit="char" quantity="4"></space>Vite oubliés au bout de la journée.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">Ton galop violent obéit à mon cri,</l>
						<l n="10" num="3.2"><space unit="char" quantity="8"></space>Nous vivons d’ivresses pareilles ;</l>
						<l n="11" num="3.3">Et je vois l’existence entre tes deux oreilles,</l>
						<l n="12" num="3.4"><space unit="char" quantity="4"></space>Sensibles. à tout comme mon esprit.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">La même passion passe dans nos narines,</l>
						<l n="14" num="4.2"><space unit="char" quantity="8"></space>Le même vent dans nos cheveux.</l>
						<l n="15" num="4.3">Je fais ce qui te plaît et toi ce que je veux,</l>
						<l n="16" num="4.4"><space unit="char" quantity="4"></space>Et la liberté gonfle nos poitrines.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">Le tout puissant pouvoir s’équilibre entre nous :</l>
						<l n="18" num="5.2"><space unit="char" quantity="4"></space>Ma vie est livrée à ton dos farouche,</l>
						<l n="19" num="5.3"><space unit="char" quantity="8"></space>Ma volonté mate ta bouche,</l>
						<l n="20" num="5.4"><space unit="char" quantity="4"></space>Et ta force est prise entre mes genoux.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1">Que si, présentement, l’ombre multiple et une</l>
						<l n="22" num="6.2"><space unit="char" quantity="8"></space>Descend avec le feu des soirs,</l>
						<l n="23" num="6.3">Dis ? Prenons notre trot vers la nouvelle lune</l>
						<l n="24" num="6.4"><space unit="char" quantity="4"></space>Cornue au-dessus des bois déjà noirs.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1">Rythmons des quatre pieds notre vol qui s’élance,</l>
						<l n="26" num="7.2"><space unit="char" quantity="4"></space>Si tu veux gagner le but d’un seul trait,</l>
						<l n="27" num="7.3">Et battons vivement la mesure en silence</l>
						<l n="28" num="7.4"><space unit="char" quantity="8"></space>Dans les sentiers de la forêt.</l>
					</lg>
				</div></body></text></TEI>