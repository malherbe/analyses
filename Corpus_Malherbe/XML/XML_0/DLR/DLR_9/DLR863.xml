<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">LES SEPT DOULEURS D’OCTOBRE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>Delarue-Mardrus</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2316 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_9</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Sept douleurs d’Octobre</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/luciedelaruemardrueferveur.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Sept douleurs d’Octobre</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>J. Ferenczi et fils, éditeurs</publisher>
							<date when="1930">1930</date>
						</imprint>
					</monogr>
					<note>Édition de référence</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1930">1930</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Il manque de très nombreux poèmes dans le texte source.</p>
				<p>Les nombreux poèmes manquants ont été ajoutés à partir d’une numérisation de l’édition de référence.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-11-28" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">VIII</head><head type="main_part">BONHEURS</head><div type="poem" key="DLR863">
					<head type="main">ART POÉTIQUE</head>
					<lg n="1">
						<l n="1" num="1.1">Si le dieu visite ta tête,</l>
						<l n="2" num="1.2">Sois d’abord un bon ouvrier,</l>
						<l n="3" num="1.3"><space unit="char" quantity="10"></space>Le métier</l>
						<l n="4" num="1.4">Étant l’autre aile du poète.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">Apprends de suite à respecter</l>
						<l n="6" num="2.2">Les lois, mathématique pure.</l>
						<l n="7" num="2.3"><space unit="char" quantity="10"></space>Sans mesure</l>
						<l n="8" num="2.4">La musique ne peut chanter.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">Crains verbiages, antithèses,</l>
						<l n="10" num="3.2">Images mises bout à bout.</l>
						<l n="11" num="3.3"><space unit="char" quantity="10"></space>Avant tout</l>
						<l n="12" num="3.4">Sois sobre, et trouves-y tes aises.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">Sois simple aussi comme bonjour.</l>
						<l n="14" num="4.2">(Ce n’est déjà pas si commode !)</l>
						<l n="15" num="4.3"><space unit="char" quantity="10"></space>Que ton mode</l>
						<l n="16" num="4.4">Ne laisse pas l’idée à court.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">Relègue-nous l’adverbe en marge,</l>
						<l n="18" num="5.2">Et surtout fuis le lénitif</l>
						<l n="19" num="5.3"><space unit="char" quantity="10"></space>Adjectif</l>
						<l n="20" num="5.4">Et son abusive surcharge.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1">Invente ton rythme au besoin.</l>
						<l n="22" num="6.2">Les fièvres, langueurs et colères</l>
						<l n="23" num="6.3"><space unit="char" quantity="10"></space>Des artères</l>
						<l n="24" num="6.4">Ne se vont pas chercher si loin.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1">Fatras livresque, erreur insigne !</l>
						<l n="26" num="7.2">C’est sous un lin candide et droit</l>
						<l n="27" num="7.3"><space unit="char" quantity="10"></space>Que se voit,</l>
						<l n="28" num="7.4">Dans toute sa beauté, la ligne.</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1">Enfin ne sois original</l>
						<l n="30" num="8.2">Que sans t’en douter ; car la pose,</l>
						<l n="31" num="8.3"><space unit="char" quantity="10"></space>Fausse rose,</l>
						<l n="32" num="8.4">Est la raison de tout le mal.</l>
					</lg>
				</div></body></text></TEI>