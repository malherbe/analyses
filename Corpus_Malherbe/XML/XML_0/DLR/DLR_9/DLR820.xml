<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">LES SEPT DOULEURS D’OCTOBRE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>Delarue-Mardrus</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2316 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_9</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Sept douleurs d’Octobre</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/luciedelaruemardrueferveur.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Sept douleurs d’Octobre</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>J. Ferenczi et fils, éditeurs</publisher>
							<date when="1930">1930</date>
						</imprint>
					</monogr>
					<note>Édition de référence</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1930">1930</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Il manque de très nombreux poèmes dans le texte source.</p>
				<p>Les nombreux poèmes manquants ont été ajoutés à partir d’une numérisation de l’édition de référence.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-11-28" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">V</head><head type="main_part">CI-GÎT</head><div type="poem" key="DLR820">
					<head type="main">CI-GÎT</head>
					<lg n="1">
						<l n="1" num="1.1">Sous le ciel de la Somme où l’infernale haine</l>
						<l n="2" num="1.2"><space unit="char" quantity="8"></space>Se couvre déjà de moisi,</l>
						<l n="3" num="1.3">Une humble croix de bois est debout sur la plaine,</l>
						<l n="4" num="1.4"><space unit="char" quantity="8"></space>Guère plus haute qu’un fusil.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">C’est toi, petit soldat inconnu ? Ma tendresse</l>
						<l n="6" num="2.2"><space unit="char" quantity="8"></space>Qui, par hasard, passe par là,</l>
						<l n="7" num="2.3">Apporte un long regard, un rêve, une caresse</l>
						<l n="8" num="2.4"><space unit="char" quantity="8"></space>A ton solitaire au-delà.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">Coiffant le vide clair, ton casque bleu se rouille</l>
						<l n="10" num="3.2"><space unit="char" quantity="8"></space>Sur se deux bouts de bois croisés.</l>
						<l n="11" num="3.3">Le soleil chauffe ou bien la pluie affreuse mouille</l>
						<l n="12" num="3.4"><space unit="char" quantity="8"></space>Les champs de bataille apaisés.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">Et, dans l’immensité muette et sépulcrale</l>
						<l n="14" num="4.2"><space unit="char" quantity="8"></space>Où jadis on sema le blé,</l>
						<l n="15" num="4.3">Devant ce tertre où dort une jeune âme mâle,</l>
						<l n="16" num="4.4"><space unit="char" quantity="8"></space>J’entends le silence parler.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">« Ils ont dit que c’était un sort digne d’envie</l>
						<l n="18" num="5.2"><space unit="char" quantity="8"></space>D’être un héros sous une croix,</l>
						<l n="19" num="5.3">Mais moi, je pleure ici d’avoir troqué ma vie</l>
						<l n="20" num="5.4"><space unit="char" quantity="8"></space>Contre deux pauvres bouts de bois.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1">« Cette croix-là, debout encore comme un homme</l>
						<l n="22" num="6.2"><space unit="char" quantity="8"></space>Et portant casque comme lui,</l>
						<l n="23" num="6.3">Cette croix-là c’est moi, sous le ciel de la Somme,</l>
						<l n="24" num="6.4"><space unit="char" quantity="8"></space>Lieu de peur, d’angoisse et d’ennui.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1">« Cette croix-là, tu vois, a presque des épaules</l>
						<l n="26" num="7.2"><space unit="char" quantity="8"></space>Et se coiffe du lourd fer bleu,</l>
						<l n="27" num="7.3">Disant que, jusqu’au bout, ils ont tenu leurs rôles,</l>
						<l n="28" num="7.4"><space unit="char" quantity="8"></space>Ceux-là qu’on envoyait au feu.</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1">« Mais moi, j’avais deux yeux vivants dans un visage,</l>
						<l n="30" num="8.2"><space unit="char" quantity="8"></space>Mes épaules de chair et d’os,</l>
						<l n="31" num="8.3">Les jambes et les bras alertes de mon âge,</l>
						<l n="32" num="8.4"><space unit="char" quantity="8"></space>Mon cœur, mon sexe, doux fardeaux.</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1">« J’étais comme vous tous, vivants ! J’étais des vôtres,</l>
						<l n="34" num="9.2"><space unit="char" quantity="8"></space>Et j’entendais parler ma voix.</l>
						<l n="35" num="9.3">On m’a tué, couché sous terre ; et cette croix</l>
						<l n="36" num="9.4"><space unit="char" quantity="8"></space>Dit : « Aimez-vous les uns les autres ! »</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1">O passante ! Être humain qui n’a pas dans le sang</l>
						<l n="38" num="10.2"><space unit="char" quantity="8"></space>Le démon mâle de la guerre,</l>
						<l n="39" num="10.3">Femme, femme, ô douceur ! Femme, cœur frémissant,</l>
						<l n="40" num="10.4"><space unit="char" quantity="8"></space>Écoute ce que dit la terre !</l>
					</lg>
					<lg n="11">
						<l n="41" num="11.1">« Écoute, jusqu’au bout du lointain violet,</l>
						<l n="42" num="11.2"><space unit="char" quantity="8"></space>La plaine crier par les bouches</l>
						<l n="43" num="11.3">De tous ces trous qu’on fait les mitrailles farouches</l>
						<l n="44" num="11.4"><space unit="char" quantity="8"></space>Dont survit le fracas muet.</l>
					</lg>
					<lg n="12">
						<l n="45" num="12.1">« La guerre !… Avoir commis ce gigantesque crime !</l>
						<l n="46" num="12.2"><space unit="char" quantity="8"></space>N’avions-nous pas assez de maux ?</l>
						<l n="47" num="12.3">Il ne faut plus tuer les hommes pour des mots,</l>
						<l n="48" num="12.4"><space unit="char" quantity="8"></space>Il ne faut plus gorger l’abîme.</l>
					</lg>
					<lg n="13">
						<l n="49" num="13.1">« Hélas ! Je n’étais pas, moi soldat, un héros,</l>
						<l n="50" num="13.2"><space unit="char" quantity="8"></space>Mais un humain qui chante et pleure,</l>
						<l n="51" num="13.3">Un humain simplement, garçon au cœur bien gros</l>
						<l n="52" num="13.4"><space unit="char" quantity="8"></space>D’être mis là pour qu’il y meure.</l>
					</lg>
					<lg n="14">
						<l n="53" num="14.1">« De quel pays ? Qu’importe ! Un fort et jeune gars</l>
						<l n="54" num="14.2"><space unit="char" quantity="8"></space>Qui respirait l’air qu’on respire.</l>
						<l n="55" num="14.3"><hi rend="ital">Héros</hi> ! C’est un beau mot que l’on aime bien dire,</l>
						<l n="56" num="14.4"><space unit="char" quantity="8"></space>Mais moi je ne revivrai pas.</l>
					</lg>
					<lg n="15">
						<l n="57" num="15.1">« Voici ma croix ; autour, la plaine desséchée :</l>
						<l n="58" num="15.2"><space unit="char" quantity="8"></space>Et j’avais un beau sang qui bout.</l>
						<l n="59" num="15.3">La guerre m’a jeté vivant, dans sa tranchée,</l>
						<l n="60" num="15.4"><space unit="char" quantity="8"></space>Et j’y suis resté, voilà tout !</l>
					</lg>
					<lg n="16">
						<l n="61" num="16.1">« Mais maintenant, il faut que l’avenir répare</l>
						<l n="62" num="16.2"><space unit="char" quantity="8"></space>Ce qu’on fit au mort endormi.</l>
						<l n="63" num="16.3">Vengez-nous ! Vengez-nous de la guerre barbare,</l>
						<l n="64" num="16.4"><space unit="char" quantity="8"></space>Vengez-nous du seul ennemi !</l>
					</lg>
					<lg n="17">
						<l n="65" num="17.1">« A moi, femmes ! A moi, mères, filles épouses !</l>
						<l n="66" num="17.2"><space unit="char" quantity="8"></space>Criez que vous ne voulez plus !</l>
						<l n="67" num="17.3">Nous sommes morts… O vous, n’êtes-vous pas jalouses</l>
						<l n="68" num="17.4"><space unit="char" quantity="8"></space>De ces croix le long des talus ?</l>
					</lg>
					<lg n="18">
						<l n="69" num="18.1">« Avez-vous arraché de vos pauvres entrailles</l>
						<l n="70" num="18.2"><space unit="char" quantity="8"></space>Un fils, un homme, ce trésor,</l>
						<l n="71" num="18.3">Pour qu’il devienne un jour ce débris des batailles,</l>
						<l n="72" num="18.4"><space unit="char" quantity="8"></space>Un soldat inconnu qui dort ?</l>
					</lg>
					<lg n="19">
						<l n="73" num="19.1">« A moi, science, amour, art, musique, pensées !</l>
						<l n="74" num="19.2"><space unit="char" quantity="8"></space>Ne souffrez plus, sous le ciel clair,</l>
						<l n="75" num="19.3">Qu’on retrouve jamais des croix de bois, dressées</l>
						<l n="76" num="19.4"><space unit="char" quantity="8"></space>Sur nos os et sur notre chair ! »</l>
					</lg>
						<ab type="star">* * *</ab>
					<lg n="20">
						<l n="77" num="20.1">… J’écoutais cette voix, remplir le paysage</l>
						<l n="78" num="20.2"><space unit="char" quantity="8"></space>Qui montre, sous le ciel câlin,</l>
						<l n="79" num="20.3">Le sol fécond de France, éventré par la rage</l>
						<l n="80" num="20.4"><space unit="char" quantity="8"></space>Du sombre démon masculin.</l>
					</lg>
					<lg n="21">
						<l n="81" num="21.1">Et, quittant pour toujours l’effroyable étendue,</l>
						<l n="82" num="21.2"><space unit="char" quantity="8"></space>Enfer éteint, charnier maudit,</l>
						<l n="83" num="21.3">J’ai répondu : « Soldat, pauvre tombe perdue,</l>
						<l n="84" num="21.4"><space unit="char" quantity="8"></space>J’ai compris ce que tu mas dit. »</l>
					</lg>
				</div></body></text></TEI>