<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">LES SEPT DOULEURS D’OCTOBRE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>Delarue-Mardrus</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2316 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_9</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Sept douleurs d’Octobre</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/luciedelaruemardrueferveur.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Sept douleurs d’Octobre</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>J. Ferenczi et fils, éditeurs</publisher>
							<date when="1930">1930</date>
						</imprint>
					</monogr>
					<note>Édition de référence</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1930">1930</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Il manque de très nombreux poèmes dans le texte source.</p>
				<p>Les nombreux poèmes manquants ont été ajoutés à partir d’une numérisation de l’édition de référence.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-11-28" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">VIII</head><head type="main_part">BONHEURS</head><div type="poem" key="DLR853">
					<head type="main">O TEMPS !…</head>
					<lg n="1">
						<l n="1" num="1.1">O temps ! Je redeviens poétique et sans sexe</l>
						<l n="2" num="1.2">Comme aux jours merveilleux du premier souvenir,</l>
						<l n="3" num="1.3">Quand j’ignorais encor que l’on m’allait bannir</l>
						<l n="4" num="1.4"><space unit="char" quantity="8"></space>Dans un monde bas et complexe.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">Comment séraphin-né, fille de séraphins,</l>
						<l n="6" num="2.2">Suis-je donc devenue une femme ! une femme</l>
						<l n="7" num="2.3">Avec tous ces tourments, tous ces drames dans l’âme,</l>
						<l n="8" num="2.4"><space unit="char" quantity="8"></space>Ces commencements et ces fins ?</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">Je n’aurai nul regret de cette âpre jeunesse</l>
						<l n="10" num="3.2">Qui cherchait, qui cherchait et qui ne trouvait pas.</l>
						<l n="11" num="3.3">J’abandonne, empressée, avec un geste las,</l>
						<l n="12" num="3.4"><space unit="char" quantity="8"></space>Ma jeunesse, ce droit d’aînesse.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">Mes passions battaient, lames sur le récif,</l>
						<l n="14" num="4.2">Et je ne savais plus les candeurs et les charmes.</l>
						<l n="15" num="4.3">Mais je gardais du moins ce cœur inoffensif</l>
						<l n="16" num="4.4"><space unit="char" quantity="8"></space>Parmi les bonheurs et les larmes,</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">Ce cœur inoffensif qui n’est point de ce temps</l>
						<l n="18" num="5.2">De lutte sans merci, d’impudeur et de ruse,</l>
						<l n="19" num="5.3">Ce cœur inoffensif que repaît et qu’amuse</l>
						<l n="20" num="5.4"><space unit="char" quantity="8"></space>Son rêve aux récits palpitants.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1">Vivre seule en rêvant et rêvant… Je retrouve</l>
						<l n="22" num="6.2">L’hypnose de l’enfance étonnée aux grands yeux.</l>
						<l n="23" num="6.3">Je ne suis plus, parmi les loups, la jeune louve</l>
						<l n="24" num="6.4"><space unit="char" quantity="8"></space>Amoureuse, hurlant comme eux.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1">Le monde redevient mystérieux et chaste.</l>
						<l n="26" num="7.2">Art à l’intérieur et nature au dehors.</l>
						<l n="27" num="7.3">Je vis ! Je vis heureuse et seule de ma caste,</l>
						<l n="28" num="7.4"><space unit="char" quantity="8"></space>Avec ma famille de morts.</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1">O mon front éclaté de travail et d’étude,</l>
						<l n="30" num="8.2">Que de jeunesse encor et de charme pour nous !</l>
						<l n="31" num="8.3">O bonheur d’être entrée encor jeune au très doux</l>
						<l n="32" num="8.4"><space unit="char" quantity="8"></space>Carmel de cette solitude !</l>
					</lg>
				</div></body></text></TEI>