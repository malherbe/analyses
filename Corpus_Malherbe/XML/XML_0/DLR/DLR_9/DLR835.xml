<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">LES SEPT DOULEURS D’OCTOBRE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>Delarue-Mardrus</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2316 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_9</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Sept douleurs d’Octobre</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/luciedelaruemardrueferveur.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Sept douleurs d’Octobre</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>J. Ferenczi et fils, éditeurs</publisher>
							<date when="1930">1930</date>
						</imprint>
					</monogr>
					<note>Édition de référence</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1930">1930</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Il manque de très nombreux poèmes dans le texte source.</p>
				<p>Les nombreux poèmes manquants ont été ajoutés à partir d’une numérisation de l’édition de référence.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-11-28" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">VI</head><head type="main_part">SPECTRES</head><div type="poem" key="DLR835">
					<head type="main">LA MESSAGÈRE</head>
					<lg n="1">
						<l n="1" num="1.1">Mon âme, dis ? Ne t’est-il pas possible</l>
						<l n="2" num="1.2">Dès maintenant, avant même la mort,</l>
						<l n="3" num="1.3">D’être, dehors, esprit sans corps, qui sort</l>
						<l n="4" num="1.4"><space unit="char" quantity="4"></space>Et qui hante dans l’invisible ?</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">Tout doucement et comme chaque soir</l>
						<l n="6" num="2.2">S’est étendu le corps las du grand fauve</l>
						<l n="7" num="2.3">Dans la profonde et ténébreuse alcôve</l>
						<l n="8" num="2.4"><space unit="char" quantity="4"></space>Où je dors dans un néant noir.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">— Tout doucement, ai-je dit à mon âme,</l>
						<l n="10" num="3.2">Quitte mon corps et t’en vas par la nuit.</l>
						<l n="11" num="3.3">Ne sache pas si la lune reluit</l>
						<l n="12" num="3.4"><space unit="char" quantity="4"></space>Ou bien si la tempête clame,</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">Car tu seras loin du sang et des os</l>
						<l n="14" num="4.2">Qui, faiblement, ont peur, étant fragiles,</l>
						<l n="15" num="4.3">Et raseras, de deux ailes agiles,</l>
						<l n="16" num="4.4"><space unit="char" quantity="4"></space>Les prés, les champs, les bois, les eaux.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">Tu glisseras sans crainte, quoique seule,</l>
						<l n="18" num="5.2">Par les chemins que je connais si bien.</l>
						<l n="19" num="5.3">Tu t’en iras jusqu’à ma morte aïeule</l>
						<l n="20" num="5.4"><space unit="char" quantity="4"></space>Enterrée, et qui n’est plus rien.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1">Sans bruit, sans bruit, comme vont les colombes,</l>
						<l n="22" num="6.2">Tu passeras le portail vermoulu</l>
						<l n="23" num="6.3">Du cimetière aux quatre seules tombes,</l>
						<l n="24" num="6.4"><space unit="char" quantity="4"></space>Et diras en entrant : « Salut ! »</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1">Tu descendras vers ma morte couchée,</l>
						<l n="26" num="7.2">La dame étrange et vieille d’autrefois.</l>
						<l n="27" num="7.3">Tu lui diras avec des mots sans voix</l>
						<l n="28" num="7.4"><space unit="char" quantity="4"></space>Ce pourquoi je t’ai dépêchée.</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1">« Quelqu’un, ô morte, a compris maintenant,</l>
						<l n="30" num="8.2">Et te le dit jusqu’au fond de la terre,</l>
						<l n="31" num="8.3">Ton cœur amer, ton grand délaissement</l>
						<l n="32" num="8.4"><space unit="char" quantity="4"></space>Et tout ton orageux mystère.</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1">« Quelqu’un des tiens te demande pardon,</l>
						<l n="34" num="9.2">Quelqu’un des tiens, ton nom et ta famille,</l>
						<l n="35" num="9.3">Quelqu’un d’étrange, une petite-fille</l>
						<l n="36" num="9.4"><space unit="char" quantity="4"></space>Qui reçut ton esprit en don.</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1">« C’est ta folie, ô morte, qui l’inspire !</l>
						<l n="38" num="10.2">Son chant est fait de ta voix de jadis.</l>
						<l n="39" num="10.3">Elle ne fit que d’accorder la lyre</l>
						<l n="40" num="10.4"><space unit="char" quantity="4"></space>Que, sans cordes, tu lui tendis.</l>
					</lg>
					<lg n="11">
						<l n="41" num="11.1">« Réveille-toi ! J’apporte lai nouvelle.</l>
						<l n="42" num="11.2">Réveille-toi ! Je frappe à ton cercueil.</l>
						<l n="43" num="11.3">Réveille-toi ! Ta mort a fait un deuil.</l>
						<l n="44" num="11.4"><space unit="char" quantity="4"></space>Réveille-toi, nuit éternelle !</l>
					</lg>
					<lg n="12">
						<l n="45" num="12.1">« Réveille-toi ! J’ai passé par tes fleurs.</l>
						<l n="46" num="12.2">Réveille-toi de la part d’une dame.</l>
						<l n="47" num="12.3">Réveille-toi ! Je t’apporte ses pleurs.</l>
						<l n="48" num="12.4"><space unit="char" quantity="4"></space>Réveille-toi ! Je suis son âme ! »</l>
					</lg>
					<lg n="13">
						<l n="49" num="13.1">Ainsi dit-elle, et vers moi s’en revint,</l>
						<l n="50" num="13.2">Et je me vis dans mon alcôve close,</l>
						<l n="51" num="13.3">Corps un moment sans âme, qui, d’instinct,</l>
						<l n="52" num="13.4"><space unit="char" quantity="4"></space>Avait pris la funèbre pose,</l>
					</lg>
					<lg n="14">
						<l n="53" num="14.1">Les pieds rejoints, anguleux sous le drap,</l>
						<l n="54" num="14.2">Les doigts croisés, rituelle attitude</l>
						<l n="55" num="14.3">Qu’un jour aussi quelqu’un me donnera</l>
						<l n="56" num="14.4"><space unit="char" quantity="4"></space>Pour la suprême solitude.</l>
					</lg>
				</div></body></text></TEI>