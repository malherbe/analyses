<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">SOUFFLES DE TEMPÊTE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3271 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_6</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruesoufledetempete.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poèmes de guerre extraits de Souffles de tempêtes (1918)</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poetesses.blog4ever.com</publisher>
						<idno type="URL">https://poetesses.blog4ever.com/nouveaux-poemes-de-guerre-dans-souffles-de-tempete-1918</idno>
					</publicationStmt>
					<sourceDesc>
						<p>Textes complémentaires à ceux du site précédent</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title>SOUFFLES DE TEMPÊTE</title>
					<author>Lucie Delarue-Mardrus</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>E. Fasquelle</publisher>
						<date when="1918">1918</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1918">1918</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes manquants ( "Mes Bonnes routes" -> fin du recueil") ont été ajoutés à partir des poèmes du site : https://poetesses.blog4ever.com.</p>
				<p>Les textes des deux sources numériques comportent de nombreuses erreurs de numérisation ; une relecture complète s’impose.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les corrections métriques ont été faites à partir d’une version numérisée du texte effectuée par la Bibliothèque de Caen le mer.</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-01-31" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-01-31" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">III</head><head type="main_part">LE SPHINX</head><div type="poem" key="DLR577">
					<head type="main">HÉLIOPOLIS</head>
					<lg n="1">
						<l n="1" num="1.1">L’obélisque où le temps a joint d’informes traces</l>
						<l n="2" num="1.2">Aux signes éternels des siècles disparus,</l>
						<l n="3" num="1.3">Tout en berçant son ombre au gré des maïs drus</l>
						<l n="4" num="1.4">Garde dans son granit l’esprit des vieilles races.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">Sur le sol où, jadis, fut Héliopolis,</l>
						<l n="6" num="2.2">J’ai vu vivre au soleil l’Égypte bleue et noire.</l>
						<l n="7" num="2.3">Fantômes du présent et spectres de l’histoire</l>
						<l n="8" num="2.4">Ont surgi pour mes yeux à travers ce maïs.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">Des demeures s’ouvraient ici, sacerdotales,</l>
						<l n="10" num="3.2">Ici s’émerveilla le regard de Platon,</l>
						<l n="11" num="3.3">Ici se profila Bonaparte aux yeux pâles…</l>
						<l n="12" num="3.4">Qu’en reste-t-il ? Ce lin, ce maïs, ce coton.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">Cependant, comme au temps des gloires, le Nil règne,</l>
						<l n="14" num="4.2">Le même Nil fécond, père du pays vert,</l>
						<l n="15" num="4.3">L’intarissable Nil, veine ouverte qui saigne</l>
						<l n="16" num="4.4">Et nourrit de son flot rythmique le désert.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">Voici la même cange au mât penché qui glisse,</l>
						<l n="18" num="5.2">Et le même sifflet d’épervier dans le ciel.</l>
						<l n="19" num="5.3">Et, primitif autant que les ruches à miel,</l>
						<l n="20" num="5.4">Ce village de terre au peuple sombre et lisse.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1">Certes, la rumeur gronde, au bout des horizons,</l>
						<l n="22" num="6.2">L’insolente rumeur des capitales neuves,</l>
						<l n="23" num="6.3">Et leur foule se meut selon d’autres raisons,</l>
						<l n="24" num="6.4">Et les dieux, en mourant, font les ruines veuves.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1">Mais qu’une Fellaha pieds nus, au voile noir,</l>
						<l n="26" num="7.2">Passe, droite, portant sur sa tête son urne,</l>
						<l n="27" num="7.3">Puis se penche sans bruit sur l’eau déjà nocturne</l>
						<l n="28" num="7.4">Et puise doucement tous les reflets du soir ;</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1">Qu’un petit berger chante en conduisant ses buffles</l>
						<l n="30" num="8.2">A travers les palmiers, sur le sol sablonneux ;</l>
						<l n="31" num="8.3">Que ses bêtes vers lui lèvent leurs tristes mufles ;</l>
						<l n="32" num="8.4">Que des chameaux chargés s’avancent deux à deux,</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1">Alors nous relisons les anciens chapitres,</l>
						<l n="34" num="9.2">Car ce peuple a gardé, sous le même soleil,</l>
						<l n="35" num="9.3">Ses mœurs, ses vêtements et son type, pareil</l>
						<l n="36" num="9.4">A celui des Ramsès que l’on voit sous des vitres.</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1">Pour moi, ressuscitée après l’immense oubli,</l>
						<l n="38" num="10.2">Je porte une momie en moi. Je me promène</l>
						<l n="39" num="10.3">Hors le bois peint et l’or sacré du dernier lit,</l>
						<l n="40" num="10.4">Et reconnais partout mon ancien domaine.</l>
					</lg>
					<lg n="11">
						<l n="41" num="11.1">Est-ce que je dormais dans mon pays brumeux</l>
						<l n="42" num="11.2">Comme dans un humide et mauvais sarcophage ?</l>
						<l n="43" num="11.3">O Nil ! O temples morts ! Éternel paysage,</l>
						<l n="44" num="11.4">Égypte ! C’est ici le pays de mes yeux !</l>
					</lg>
				</div></body></text></TEI>