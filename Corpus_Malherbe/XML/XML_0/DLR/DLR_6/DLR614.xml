<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">SOUFFLES DE TEMPÊTE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3271 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_6</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruesoufledetempete.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poèmes de guerre extraits de Souffles de tempêtes (1918)</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poetesses.blog4ever.com</publisher>
						<idno type="URL">https://poetesses.blog4ever.com/nouveaux-poemes-de-guerre-dans-souffles-de-tempete-1918</idno>
					</publicationStmt>
					<sourceDesc>
						<p>Textes complémentaires à ceux du site précédent</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title>SOUFFLES DE TEMPÊTE</title>
					<author>Lucie Delarue-Mardrus</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>E. Fasquelle</publisher>
						<date when="1918">1918</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1918">1918</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes manquants ( "Mes Bonnes routes" -> fin du recueil") ont été ajoutés à partir des poèmes du site : https://poetesses.blog4ever.com.</p>
				<p>Les textes des deux sources numériques comportent de nombreuses erreurs de numérisation ; une relecture complète s’impose.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les corrections métriques ont été faites à partir d’une version numérisée du texte effectuée par la Bibliothèque de Caen le mer.</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-01-31" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-01-31" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">VI</head><head type="main_part">PROPHÉTIQUES</head><div type="poem" key="DLR614">
					<head type="main">RÊVERIE</head>
					<lg n="1">
						<l n="1" num="1.1">Riches de doux trésors, l’espérance et l’amour,</l>
						<l n="2" num="1.2">Les foules vont bêlant en troupeaux nostalgiques.</l>
						<l n="3" num="1.3">Quelque soit leur destin les humains sont tragiques</l>
						<l n="4" num="1.4"><space unit="char" quantity="8"></space>Ils savent qu’ils mourront un jour.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">La plus insigne brute est encore divine</l>
						<l n="6" num="2.2">Près des férocités qui mènent l’animal.</l>
						<l n="7" num="2.3">Le malaise de vivre est là. Rien n’est banal.</l>
						<l n="8" num="2.4"><space unit="char" quantity="8"></space>Une âme dans tous se devine.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">Ils travaillent. Parfois ils ont de la gaieté !</l>
						<l n="10" num="3.2">Poésie, art, musique… Ils chantent ! Quel courage !</l>
						<l n="11" num="3.3">Ils ont l’ambition et la méchanceté</l>
						<l n="12" num="3.4"><space unit="char" quantity="8"></space>Parmi les malheurs qui font rage.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">Ils grouillent en enfer, et beaucoup croient en Dieu.</l>
						<l n="14" num="4.2">L’espèce, obstinément, répare ses guenilles :</l>
						<l n="15" num="4.3">Sachant ce qu’est la vie ils refont des familles,</l>
						<l n="16" num="4.4"><space unit="char" quantity="8"></space>Et les suicidés sont peu.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">Comme s’il n’était pas de tremblement de terre</l>
						<l n="18" num="5.2">Et comme si la foudre avait perdu ses traits,</l>
						<l n="19" num="5.3">Parmi peste et famine ils inventent la guerre,</l>
						<l n="20" num="5.4"><space unit="char" quantity="8"></space>Et s’en vont répétant : <hi rend="ital">progrès</hi>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1">C’est ainsi ! Naturelle ou qu’ils l’aient inventée</l>
						<l n="22" num="6.2">Tant de douleur les tord jusqu’au jour du trépas</l>
						<l n="23" num="6.3">Qu« on se demande si, la leur ayant ôtée,</l>
						<l n="24" num="6.4"><space unit="char" quantity="8"></space>Ils ne la réclameront pas.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1">O bêtise et génie ! Humains, mes pauvres frères</l>
						<l n="26" num="7.2">Que j’aime et que je hais si passionnément,</l>
						<l n="27" num="7.3">Laissez-moi suivre au loin vos cohortes amères</l>
						<l n="28" num="7.4"><space unit="char" quantity="8"></space>D’un éternel étonnement.</l>
					</lg>
					<closer>
						<dateline>
							<placeName>Paris</placeName>,
							<date when="1914">20 juin 1914</date>
						</dateline>
					</closer>
				</div></body></text></TEI>