<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">SOUFFLES DE TEMPÊTE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3271 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_6</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruesoufledetempete.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poèmes de guerre extraits de Souffles de tempêtes (1918)</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poetesses.blog4ever.com</publisher>
						<idno type="URL">https://poetesses.blog4ever.com/nouveaux-poemes-de-guerre-dans-souffles-de-tempete-1918</idno>
					</publicationStmt>
					<sourceDesc>
						<p>Textes complémentaires à ceux du site précédent</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title>SOUFFLES DE TEMPÊTE</title>
					<author>Lucie Delarue-Mardrus</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>E. Fasquelle</publisher>
						<date when="1918">1918</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1918">1918</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes manquants ( "Mes Bonnes routes" -> fin du recueil") ont été ajoutés à partir des poèmes du site : https://poetesses.blog4ever.com.</p>
				<p>Les textes des deux sources numériques comportent de nombreuses erreurs de numérisation ; une relecture complète s’impose.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les corrections métriques ont été faites à partir d’une version numérisée du texte effectuée par la Bibliothèque de Caen le mer.</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-01-31" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-01-31" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">VI</head><head type="main_part">PROPHÉTIQUES</head><div type="poem" key="DLR617">
					<head type="main">IN EXCELSIS</head>
					<lg n="1">
						<l n="1" num="1.1">O poètes passés qui mourûtes jaloux</l>
						<l n="2" num="1.2"><space unit="char" quantity="8"></space>Des aigles et des hirondelles,</l>
						<l n="3" num="1.3">Vous qui vous épuisez à demander des ailes,</l>
						<l n="4" num="1.4"><space unit="char" quantity="8"></space>Voyez ! Les ailes sont à nous !</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">Après la cathédrale et les arts et les livres,</l>
						<l n="6" num="2.2"><space unit="char" quantity="8"></space>Nous nous sentions devenir vieux.</l>
						<l n="7" num="2.3">Mais voici de nouveau que nos esprits sont ivres</l>
						<l n="8" num="2.4"><space unit="char" quantity="8"></space>Comme aux temps les plus fabuleux.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">Plus haut que les clochers et que les cathédrales,</l>
						<l n="10" num="3.2"><space unit="char" quantity="8"></space>Plus haut que les flèches des tours,</l>
						<l n="11" num="3.3">Nous montons, à travers les ciels ardents ou pâles,</l>
						<l n="12" num="3.4"><space unit="char" quantity="8"></space>Et tous les chagrins sont moins lourds.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">Le grand siècle, c’est nous ! C’est nous la Renaissance</l>
						<l n="14" num="4.2"><space unit="char" quantity="8"></space>La gloire ?… Elle est de notre temps.</l>
						<l n="15" num="4.3">Oui, c’est nous les héros modestes et contents,</l>
						<l n="16" num="4.4"><space unit="char" quantity="8"></space>Oui, c’est nous la plus belle France.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">Ce que nous avons fait, sans effort, d’un élan,</l>
						<l n="18" num="5.2"><space unit="char" quantity="8"></space>Napoléon n’a pu le faire,</l>
						<l n="19" num="5.3">Car nous avons conquis, après tout, l’Angleterre,</l>
						<l n="20" num="5.4"><space unit="char" quantity="8"></space>D’un simple revers de volant.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1">Nous avons dépassé les bêtes empennées</l>
						<l n="22" num="6.2"><space unit="char" quantity="8"></space>Au creux du plus profond azur.</l>
						<l n="23" num="6.3">Il n’y a plus d’obstacle, il n’y a plus de mur,</l>
						<l n="24" num="6.4"><space unit="char" quantity="8"></space>Il n’y a plus de Pyrénées.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1">Les héros, aujourd’hui, croissent comme des fleurs.</l>
						<l n="26" num="7.2"><space unit="char" quantity="8"></space>Nous pouvons redresse l’échine.</l>
						<l n="27" num="7.3">Nous possédons enfin l’idéal des rêveurs</l>
						<l n="28" num="7.4"><space unit="char" quantity="8"></space>Réalisé par la machine.</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1">Haut la tête ! Écoutons passer le bruit du vol !</l>
						<l n="30" num="8.2"><space unit="char" quantity="8"></space>L’air s’emplit de frissons étranges.</l>
						<l n="31" num="8.3">Nous avons repeuplé le ciel de ses archanges,</l>
						<l n="32" num="8.4"><space unit="char" quantity="8"></space>Nos pieds ne touchent plus au sol.</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1">Et vous, hommes-oiseaux, race forte et légère,</l>
						<l n="34" num="9.2"><space unit="char" quantity="8"></space>Si parfois vous prenez à bord</l>
						<l n="35" num="9.3">L’invisible, fatale et lourde passagère,</l>
						<l n="36" num="9.4"><space unit="char" quantity="8"></space>La compagne sans yeux, la mort,</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1">Si du haut du zénith elle vous fait descendre</l>
						<l n="38" num="10.2"><space unit="char" quantity="8"></space>Vertigineusement en bas,</l>
						<l n="39" num="10.3">Songez que le phénix renaîtra de sa cendre</l>
						<l n="40" num="10.4"><space unit="char" quantity="8"></space>Et que vous ne périrez pas,</l>
					</lg>
					<lg n="11">
						<l n="41" num="11.1">Car, de votre sang clair, surgiront des apôtres</l>
						<l n="42" num="11.2"><space unit="char" quantity="8"></space>Prêts à reprendre votre essor,</l>
						<l n="43" num="11.3">Et si ce n’est pas vous, ce sont d’autres et d’autres</l>
						<l n="44" num="11.4"><space unit="char" quantity="8"></space>Qui monteront plus haut encor !</l>
					</lg>
					<closer>
						<dateline>
							Écrit en
							<date when="1913">1913</date>
							</dateline>
					</closer>
				</div></body></text></TEI>