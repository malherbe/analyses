<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">SOUFFLES DE TEMPÊTE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3271 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_6</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruesoufledetempete.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poèmes de guerre extraits de Souffles de tempêtes (1918)</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poetesses.blog4ever.com</publisher>
						<idno type="URL">https://poetesses.blog4ever.com/nouveaux-poemes-de-guerre-dans-souffles-de-tempete-1918</idno>
					</publicationStmt>
					<sourceDesc>
						<p>Textes complémentaires à ceux du site précédent</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title>SOUFFLES DE TEMPÊTE</title>
					<author>Lucie Delarue-Mardrus</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>E. Fasquelle</publisher>
						<date when="1918">1918</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1918">1918</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes manquants ( "Mes Bonnes routes" -> fin du recueil") ont été ajoutés à partir des poèmes du site : https://poetesses.blog4ever.com.</p>
				<p>Les textes des deux sources numériques comportent de nombreuses erreurs de numérisation ; une relecture complète s’impose.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les corrections métriques ont été faites à partir d’une version numérisée du texte effectuée par la Bibliothèque de Caen le mer.</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-01-31" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-01-31" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">V</head><head type="main_part">ARRIÈRES-SAISONS</head><div type="poem" key="DLR600">
					<head type="main">A RAPHAËL SCHWARTZ</head>
					<lg n="1">
						<l n="1" num="1.1">Est-ce la vérité qu’avec un peu de terre</l>
						<l n="2" num="1.2">Vous avez fait surgir mon double inquiétant ?</l>
						<l n="3" num="1.3">Voici donc ma statue et tout ce qui l’attend,</l>
						<l n="4" num="1.4">Car avec elle est né son destin de mystère.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">Prête splendidement pour le bronze futur,</l>
						<l n="6" num="2.2">Prisonnière du rythme où vous l’avez campée,</l>
						<l n="7" num="2.3">Quel sera l’avenir de l’insigne poupée,</l>
						<l n="8" num="2.4">Œuvre d’un ébauchoir enthousiaste et pur ?</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">Ainsi mon corps drapé qui marche, mon visage,</l>
						<l n="10" num="3.2">Mes mains de berger grec, mes deux petits pieds nus,</l>
						<l n="11" num="3.3">Et mon large regard plein de cale et d’orage,</l>
						<l n="12" num="3.4">Sous vas patients doigts lentement sont venus.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">Ma statue ! Elle est là, debout. Je la regarde,</l>
						<l n="14" num="4.2">Cette fragilité faite tout comme moi.</l>
						<l n="15" num="4.3">Elle vivra pourtant bien après moi. Hagarde,</l>
						<l n="16" num="4.4">Je tremble, en y songeant, d’un pathétique émoi.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">L’éternelle santé, l’éternelle jeunesse</l>
						<l n="18" num="5.2">La fixent pour toujours, et moi je vieillirai.</l>
						<l n="19" num="5.3">Elle est le témoin vrai de mon âge doré.</l>
						<l n="20" num="5.4">Un jour s’affirmera mon triste droit d’aînesse.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1">Sont-ce vraiment mes yeux et ma bouche et mon nez,</l>
						<l n="22" num="6.2">Sont-ce mes mains, mes pieds ? Est-ce mon attitude,</l>
						<l n="23" num="6.3">Est-ce mon dur orgueil, ma sombre quiétude</l>
						<l n="24" num="6.4">Qu’étudieront tant d’yeux encor loin d’être nés ?</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1">Nous voici tout vivants. Votre œuvre, là, s’élève,</l>
						<l n="26" num="7.2">Neuve, et si chaude encor du travail de vos doigts,</l>
						<l n="27" num="7.3">Fille de mon grand rêve et de votre grand rêve…</l>
						<l n="28" num="7.4">Et ceux des temps futurs penseront : <hi rend="ital">autrefois</hi>.</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1">Ils diront : « Elle fut une femme célèbre ! »</l>
						<l n="30" num="8.2">Ce ne sera que moi présente, cependant.</l>
						<l n="31" num="8.3">Ils ne sentiront pas battre mon cœur ardent.</l>
						<l n="32" num="8.4">Mon simple cœur humain sous le bronze funèbre.</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1">Faut-il que l’art survive à la réalité</l>
						<l n="34" num="9.2">Moi qui suis un esprit je deviendrai poussière,</l>
						<l n="35" num="9.3">Et cette image-ci qui n’est qu’un peu de terre</l>
						<l n="36" num="9.4">Va triomphalement vers l’immortalité.</l>
					</lg>
				</div></body></text></TEI>