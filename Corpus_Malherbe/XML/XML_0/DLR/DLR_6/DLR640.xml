<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">SOUFFLES DE TEMPÊTE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3271 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_6</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruesoufledetempete.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poèmes de guerre extraits de Souffles de tempêtes (1918)</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poetesses.blog4ever.com</publisher>
						<idno type="URL">https://poetesses.blog4ever.com/nouveaux-poemes-de-guerre-dans-souffles-de-tempete-1918</idno>
					</publicationStmt>
					<sourceDesc>
						<p>Textes complémentaires à ceux du site précédent</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title>SOUFFLES DE TEMPÊTE</title>
					<author>Lucie Delarue-Mardrus</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>E. Fasquelle</publisher>
						<date when="1918">1918</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1918">1918</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes manquants ( "Mes Bonnes routes" -> fin du recueil") ont été ajoutés à partir des poèmes du site : https://poetesses.blog4ever.com.</p>
				<p>Les textes des deux sources numériques comportent de nombreuses erreurs de numérisation ; une relecture complète s’impose.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les corrections métriques ont été faites à partir d’une version numérisée du texte effectuée par la Bibliothèque de Caen le mer.</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-01-31" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-01-31" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">VII</head><head type="main_part">LA GUERRE</head><div type="poem" key="DLR640">
					<head type="main">CE QUATORZE JUILLET…</head>
					<lg n="1">
						<l n="1" num="1.1">Ce quatorze juillet de l’an mil neuf cent seize</l>
						<l n="2" num="1.2">Qui fêta les vivants et qui fêta les morts</l>
						<l n="3" num="1.3">Annonce le jour, beau comme la Marseillaise,</l>
						<l n="4" num="1.4">Où, devant l’univers, nous serons les plus forts.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">Par nos grands carrefours et par nos belles places,</l>
						<l n="6" num="2.2">Angleterre, Belgique et Russie ont passé.</l>
						<l n="7" num="2.3">Ce jour-là, sous nos yeux, la guerre a fiancé</l>
						<l n="8" num="2.4">L’avenir au présent et les races aux races.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">Des troupes de combat marchaient aux quatre vents</l>
						<l n="10" num="3.2">Du formidable cri des foules accourues.</l>
						<l n="11" num="3.3">Et, tandis qu’on jetait des bouquets aux vivants,</l>
						<l n="12" num="3.4">Invisibles, des morts défilaient par les rues.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">Et tout : canon, ruine, incendie, affres, pleurs,</l>
						<l n="14" num="4.2">La guerre, cette course insensée à l’abîme,</l>
						<l n="15" num="4.3">Tout devenait soudain des baisers et des fleurs.</l>
						<l n="16" num="4.4">Chaque rose, en tombant, rachetait un grand crime.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">Paris ivre disait : « Je vous aime !… » aux soldats.</l>
						<l n="18" num="5.2">Des drapeaux palpitaient sous des vols de pétales.</l>
						<l n="19" num="5.3">On ne savait plus rien des mitrailles, du glas,</l>
						<l n="20" num="5.4">Et qu’on avait au loin tué des cathédrales.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1">Et, traînant leurs canons sonores dans du bleu,</l>
						<l n="22" num="6.2">Nos artilleurs, enfants de la foudre qui gronde,</l>
						<l n="23" num="6.3">Ne savaient plus, aux sons de : <hi rend="ital">Auprès de ma blonde</hi>,</l>
						<l n="24" num="6.4">S’ils allaient à l’amour ou s’ils allaient au feu.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1">En entendant cela, nous avons dit : « Les nôtres ! »</l>
						<l n="26" num="7.2">Ce chant, c’était la France, au pas ferme et léger.</l>
						<l n="27" num="7.3">Mais, regardant aussi défiler l’étranger,</l>
						<l n="28" num="7.4">Nous avons salué les héroïques Autres.</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1">Eux tous, ils s’enfonçaient à même la cité,</l>
						<l n="30" num="8.2">Leurs couleurs chatoyant parmi la pierre grise,</l>
						<l n="31" num="8.3">Ils étaient des vainqueurs dans une ville prise,</l>
						<l n="32" num="8.4">Puisqu’ils avaient sauvé, Paris, en vérité !</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1">Nos, nous tendions les mains. C’étaient nos camarades.</l>
						<l n="34" num="9.2">L’ombre du casque nu d’où pointe le menton,</l>
						<l n="35" num="9.3">Les armes, tout cela rejoignait Marathon,</l>
						<l n="36" num="9.4">Athènes, Salamine, et Rome, et les Croisades.</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1">‒ Merci d’avoir passé, vous qu’on n’entrevoyait</l>
						<l n="38" num="10.2">Que de loin, dans le sang où se défend la France.</l>
						<l n="39" num="10.3">Votre marche à l’honneur, ce quatorze juillet.</l>
						<l n="40" num="10.4">Ce n’est pas la victoire encor : c’est l’espérance.</l>
					</lg>
				</div></body></text></TEI>