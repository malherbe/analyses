<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">SOUFFLES DE TEMPÊTE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3271 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_6</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruesoufledetempete.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poèmes de guerre extraits de Souffles de tempêtes (1918)</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poetesses.blog4ever.com</publisher>
						<idno type="URL">https://poetesses.blog4ever.com/nouveaux-poemes-de-guerre-dans-souffles-de-tempete-1918</idno>
					</publicationStmt>
					<sourceDesc>
						<p>Textes complémentaires à ceux du site précédent</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title>SOUFFLES DE TEMPÊTE</title>
					<author>Lucie Delarue-Mardrus</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>E. Fasquelle</publisher>
						<date when="1918">1918</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1918">1918</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes manquants ( "Mes Bonnes routes" -> fin du recueil") ont été ajoutés à partir des poèmes du site : https://poetesses.blog4ever.com.</p>
				<p>Les textes des deux sources numériques comportent de nombreuses erreurs de numérisation ; une relecture complète s’impose.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les corrections métriques ont été faites à partir d’une version numérisée du texte effectuée par la Bibliothèque de Caen le mer.</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-01-31" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-01-31" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">VII</head><head type="main_part">LA GUERRE</head><div type="poem" key="DLR642">
					<head type="main">BLEUETS</head>
					<lg n="1">
						<l n="1" num="1.1">Gentils bleuets, garçons qui portez nom de fleur,</l>
						<l n="2" num="1.2">Nous nous inclinerons très bas devant votre âge,</l>
						<l n="3" num="1.3">Adolescents d’hier, jeunesse plus que sage,</l>
						<l n="4" num="1.4">Vous que nous ne pouvons regarder au visage</l>
						<l n="5" num="1.5"><space unit="char" quantity="8"></space>Sans un respect plein de douleur.</l>
					</lg>
					<lg n="2">
						<l n="6" num="2.1">Nous ne vous savions pas marqués du fatal signe.</l>
						<l n="7" num="2.2">Petits qui grandissiez si passionnément,</l>
						<l n="8" num="2.3">Ni qu’à l’âge où l’on va devenir un amant</l>
						<l n="9" num="2.4">Vous songeriez tout bas à votre testament,</l>
						<l n="10" num="2.5"><space unit="char" quantity="8"></space>Avant que de vous mettre en ligne.</l>
					</lg>
					<lg n="3">
						<l n="11" num="3.1">Vos mères vous serraient, joyeuses, dans leurs bras,</l>
						<l n="12" num="3.2">Sans entendre venir le destin effroyable.</l>
						<l n="13" num="3.3">A peine entriez-vous dans la jeunesse aimable,,</l>
						<l n="14" num="3.4">Avec grand appétit, comme on se met à table,</l>
						<l n="15" num="3.5"><space unit="char" quantity="8"></space>Que les canons sonnaient le glas.</l>
					</lg>
					<lg n="4">
						<l n="16" num="4.1">Canons, injuste glas de la jeunesse mâle !</l>
						<l n="17" num="4.2">Encore rougissants sous leur beau casque bleu,</l>
						<l n="18" num="4.3">Héros à taille mince, enfants voués au feu,</l>
						<l n="19" num="4.4">Quand ils crient: « Au revoir ! » Nous murmurons: « Adieu ! »</l>
						<l n="20" num="4.5"><space unit="char" quantity="8"></space>Devant leur phalange bleu pâle.</l>
					</lg>
					<lg n="5">
						<l n="21" num="5.1">Ce ne sont plus vos fils, mères, mais des troupiers !</l>
						<l n="22" num="5.2">Nous génération si tristement aînée,</l>
						<l n="23" num="5.3">Regardant du côté de la troupe emmenée,</l>
						<l n="24" num="5.4">Nous voyons revenir, foule humble et consternée,</l>
						<l n="25" num="5.5"><space unit="char" quantity="8"></space>Du sang qui rampe vers nos pieds.</l>
					</lg>
				</div></body></text></TEI>