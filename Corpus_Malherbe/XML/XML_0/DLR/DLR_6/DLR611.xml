<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">SOUFFLES DE TEMPÊTE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3271 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_6</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruesoufledetempete.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poèmes de guerre extraits de Souffles de tempêtes (1918)</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poetesses.blog4ever.com</publisher>
						<idno type="URL">https://poetesses.blog4ever.com/nouveaux-poemes-de-guerre-dans-souffles-de-tempete-1918</idno>
					</publicationStmt>
					<sourceDesc>
						<p>Textes complémentaires à ceux du site précédent</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title>SOUFFLES DE TEMPÊTE</title>
					<author>Lucie Delarue-Mardrus</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>E. Fasquelle</publisher>
						<date when="1918">1918</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1918">1918</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes manquants ( "Mes Bonnes routes" -> fin du recueil") ont été ajoutés à partir des poèmes du site : https://poetesses.blog4ever.com.</p>
				<p>Les textes des deux sources numériques comportent de nombreuses erreurs de numérisation ; une relecture complète s’impose.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les corrections métriques ont été faites à partir d’une version numérisée du texte effectuée par la Bibliothèque de Caen le mer.</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-01-31" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-01-31" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">V</head><head type="main_part">ARRIÈRES-SAISONS</head><div type="poem" key="DLR611">
					<head type="main">LA GRANDE SOLITUDE</head>
					<lg n="1">
						<l n="1" num="1.1">La grande solitude où mon âme s’affine,</l>
						<l n="2" num="1.2">Je l’absorbe sans cesse et jusqu’au fond de moi,</l>
						<l n="3" num="1.3"><space unit="char" quantity="8"></space>Comme d’autres de la morphine.</l>
					</lg>
					<lg n="2">
						<l n="4" num="2.1">L’automne, immensément, m’enveloppe d’émoi,</l>
						<l n="5" num="2.2">Et le jour suit son cours tranquille, et l’heure sonne,</l>
						<l n="6" num="2.3"><space unit="char" quantity="8"></space>Et je n’attends rien ni personne.</l>
					</lg>
					<lg n="3">
						<l n="7" num="3.1">Au soleil du dehors il me vient du bonheur,</l>
						<l n="8" num="3.2">Dans le silence pur de quelque vieille route,</l>
						<l n="9" num="3.3"><space unit="char" quantity="8"></space>A me sentir si seule toute.</l>
					</lg>
					<lg n="4">
						<l n="10" num="4.1">Et les nuits, bien souvent, quand d’autres auraient peur,</l>
						<l n="11" num="4.2">Parmi l’ombre sans bruit de ma maison hantée,</l>
						<l n="12" num="4.3"><space unit="char" quantity="8"></space>Je sens mon ivresse montée,</l>
					</lg>
					<lg n="5">
						<l n="13" num="5.1">Si haut montée, en vérité, que le désir</l>
						<l n="14" num="5.2">Me prend subitement de rire du plaisir</l>
						<l n="15" num="5.3"><space unit="char" quantity="8"></space>Que me font mes songes étranges.</l>
					</lg>
					<lg n="6">
						<l n="16" num="6.1">Alors viennent s’asseoir avec moi près du feu</l>
						<l n="17" num="6.2">Les invités de mon esprit, humains un peu,</l>
						<l n="18" num="6.3"><space unit="char" quantity="8"></space>Mais sacrés par la mort archanges,</l>
					</lg>
					<lg n="7">
						<l n="19" num="7.1">Qui vécurent aussi de grande passion,</l>
						<l n="20" num="7.2">Et dont l’âme, par mots ou musique exprimée,</l>
						<l n="21" num="7.3"><space unit="char" quantity="8"></space>Parle à mon âme bien aimée,</l>
					</lg>
					<lg n="8">
						<l n="22" num="8.1">Qui surent comme moi l’intoxication</l>
						<l n="23" num="8.2">D’être seul, merveilleux et seul, d’être poète</l>
						<l n="24" num="8.3"><space unit="char" quantity="8"></space>Et d’avoir dans les mains sa tête.</l>
					</lg>
					<lg n="9">
						<l n="25" num="9.1">‒ Ainsi, muette, et loin des êtres décevants,</l>
						<l n="26" num="9.2">Notre réunion se passe sans vivants</l>
						<l n="27" num="9.3"><space unit="char" quantity="8"></space>Que moi, qui suis presque une morte,</l>
					</lg>
					<lg n="10">
						<l n="28" num="10.1">ET je me dis qu’un jour, esprit, je reviendrai</l>
						<l n="29" num="10.2">Pour enchante le songe et l’automne doré</l>
						<l n="30" num="10.3"><space unit="char" quantity="8"></space>D’un futur rêveur de ma sorte.</l>
					</lg>
				</div></body></text></TEI>