<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">OCCIDENT</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3151 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Occident</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">http://www.poesies.net/luciedelaruemardrueoccident.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Occident</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LA REVUE BLANCHE</publisher>
							<date when="1901">1901</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1901">1901</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
					<p>Certains retraits de vers ont été supprimés conformément à l’édition de référence.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
				<change when="2023-06-24" who="RR">Quelques corrections d’erreurs de numérisation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">VESPÉRALES</head><div type="poem" key="DLR106">
					<head type="main">FREDONS</head>
					<lg n="1">
						<l n="1" num="1.1">Fredonnez, les deux mains traînant au piano</l>
						<l n="2" num="1.2"><space unit="char" quantity="8"/>Et les yeux noyés dans le vague,</l>
						<l n="3" num="1.3">Si vagues vos grands yeux vagues comme la vague</l>
						<l n="4" num="1.4">Et si vagues vos doigts libres de tout anneau !</l>
						<l n="5" num="1.5">Fredonnez, les deux mains traînant au piano.</l>
					</lg>
					<lg n="2">
						<l n="6" num="2.1">Caresse aux doigts, d’ivoire, et de charme à vos lèvres,</l>
						<l n="7" num="2.2"><space unit="char" quantity="8"/>Musicale frôlée au cœur,</l>
						<l n="8" num="2.3">Chant tout bas, si majeur, si mineur, si moqueur,</l>
						<l n="9" num="2.4">Menuet et sanglot perlé de notes mièvres,</l>
						<l n="10" num="2.5">Caresse aux doigts, d’ivoire, et de timbre à vos lèvres ;</l>
					</lg>
					<lg n="3">
						<l n="11" num="3.1">Charme comme d’entendre à travers la cloison</l>
						<l n="12" num="3.2"><space unit="char" quantity="8"/>Chanter une voix inconnue,</l>
						<l n="13" num="3.3">Charme d’une chanson on ne sait d’où venue</l>
						<l n="14" num="3.4">Par qui sont dans les yeux les larmes sans raison,</l>
						<l n="15" num="3.5">Charme comme d’entendre à travers la cloison ;</l>
					</lg>
					<lg n="4">
						<l n="16" num="4.1">Trouble d’un baiser pris et dont on ne regoûte,</l>
						<l n="17" num="4.2"><space unit="char" quantity="8"/>Énervement de l’incomplet,</l>
						<l n="18" num="4.3">Fantôme comme aux eaux les choses en reflet,</l>
						<l n="19" num="4.4">Volupté du regret, du souvenir, du doute,</l>
						<l n="20" num="4.5">Trouble d’un baiser pris et dont on ne regoûte.</l>
					</lg>
					<lg n="5">
						<l n="21" num="5.1">Fredons où l’âme écoute en cherchant à saisir,</l>
						<l n="22" num="5.2"><space unit="char" quantity="8"/>Vos fredons si légers, si tristes !</l>
						<l n="23" num="5.3">A cause du mystère en eux fredons artistes,</l>
						<l n="24" num="5.4">Fredons chers pour laisser après eux un désir,</l>
						<l n="25" num="5.5">Fredons où l’âme écoute en cherchant à saisir.</l>
					</lg>
				</div></body></text></TEI>