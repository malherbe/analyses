<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">OCCIDENT</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3151 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Occident</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">http://www.poesies.net/luciedelaruemardrueoccident.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Occident</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LA REVUE BLANCHE</publisher>
							<date when="1901">1901</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1901">1901</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
					<p>Certains retraits de vers ont été supprimés conformément à l’édition de référence.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
				<change when="2023-06-24" who="RR">Quelques corrections d’erreurs de numérisation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">L’ENCENSOIR</head><div type="poem" key="DLR89">
					<head type="main">A UNE</head>
					<lg n="1">
						<l n="1" num="1.1">Proie un soir de mon rêve, ô ma pâleur, ma brune,</l>
						<l n="2" num="1.2">Ma grande ! je me veux encor dans des demains</l>
						<l n="3" num="1.3">Le coude à tes genoux rejoints pour, une à une,</l>
						<l n="4" num="1.4">Compter à tes dix doigts les bagues de tes mains</l>
						<l n="5" num="1.5">Dont les chatons changeants couvent des clairs de lune.</l>
						<l n="6" num="1.6">Ta robe d’or pompeuse et lourde de son tour</l>
						<l n="7" num="1.7">Fleurira largement ses corolles d’étoffe</l>
						<l n="8" num="1.8">Et ton chef balanceur de strophe et d’antistrophe</l>
						<l n="9" num="1.9">Secouera ta coiffure haute comme une tour ;</l>
						<l n="10" num="1.10">Et tu te lèveras aussi parmi la foule</l>
						<l n="11" num="1.11">Banale, tout à moi qui seule comprendrai,</l>
						<l n="12" num="1.12">Avec ton regard noir hautainement filtré</l>
						<l n="13" num="1.13">Sur cette foule, et lourd du mépris de sa houle,</l>
					</lg>
					<lg n="2">
						<l n="14" num="2.1">Tu parleras avec le souffle de ton cœur,</l>
						<l n="15" num="2.2">Et de ton art à fleur de tes lèvres, à fleur</l>
						<l n="16" num="2.3">De ta beauté, flûtant en mots de calme et d’ombre</l>
						<l n="17" num="2.4">Et souriant un rire attendri de bonté,</l>
						<l n="18" num="2.5">Et tremblant de tendresse et t’enflant d’âme sombre</l>
						<l n="19" num="2.6">Comme un violoncelle où pleure un andanté,</l>
						<l n="20" num="2.7">Et t’amplifiant plus en voix qui monte et gronde</l>
						<l n="21" num="2.8">Et clame des vers gros d’appels et de fureurs</l>
						<l n="22" num="2.9">Et hurle !… Et dans ton geste et cette voix profonde,</l>
						<l n="23" num="2.10">Il y aura des cris de haine avant-coureurs ;</l>
						<l n="24" num="2.11">La Liberté farouche agitant dans ses voltes</l>
						<l n="25" num="2.12">A bout de bras, le grand drapeau fou des révoltes,</l>
						<l n="26" num="2.13">La Marseillaise plein la poitrine ; et encor</l>
						<l n="27" num="2.14">Il y aura Sapho brisant sa lyre d’or,</l>
						<l n="28" num="2.15">Il y aura Carmen blême de tragédie</l>
						<l n="29" num="2.16">Intime, les deux yeux dévorés d’incendie,</l>
						<l n="30" num="2.17">Tout le sanglot, tout le sursaut, tous les frissons</l>
						<l n="31" num="2.18">Et le vent furieux rebroussant les moissons</l>
						<l n="32" num="2.19">Et des serpentements de sirène mêlée</l>
						<l n="33" num="2.20">A la marée avec son geste large ouvert</l>
						<l n="34" num="2.21">Frémissant jusqu’au bout de la traîne étalée,</l>
						<l n="35" num="2.22">Terrible et rauque en toi comme toute la mer !</l>
					</lg>
				</div></body></text></TEI>