<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">OCCIDENT</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3151 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Occident</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">http://www.poesies.net/luciedelaruemardrueoccident.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Occident</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LA REVUE BLANCHE</publisher>
							<date when="1901">1901</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1901">1901</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
					<p>Certains retraits de vers ont été supprimés conformément à l’édition de référence.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
				<change when="2023-06-24" who="RR">Quelques corrections d’erreurs de numérisation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">EN PLEIN VENT</head><div type="poem" key="DLR1">
					<head type="main">HYMNE PRINTANIER</head>
					<lg n="1">
						<l n="1" num="1.1">Bonne nature, as-tu des baisers pour les lèvres ?</l>
						<l n="2" num="1.2"><space unit="char" quantity="8"/>Des épaules pour les fronts lourds ?</l>
						<l n="3" num="1.3">Des fleurs pour la beauté ? des fraîcheurs pour les fièvres ?</l>
						<l n="4" num="1.4"><space unit="char" quantity="8"/>Et, pour les membres restés gourds</l>
						<l n="5" num="1.5">De sortir de l’hiver aux froides fantaisies,</l>
						<l n="6" num="1.6">Ton printemps répand-il d’étirantes tiédeurs ?</l>
						<l n="7" num="1.7">As-tu des coins cachés pour les chagrins en pleurs ?</l>
						<l n="8" num="1.8">La narine béante avide d’ambroisies</l>
						<l n="9" num="1.9">Fleurera-t-elle en toi sa satisfaction ?</l>
						<l n="10" num="1.10"><space unit="char" quantity="8"/>As-tu pour le rire et la joie</l>
						<l n="11" num="1.11">Des pourpres dont l’ampleur magnifique s’éploie,</l>
						<l n="12" num="1.12"><space unit="char" quantity="8"/>Et des deuils pour l’affliction ?</l>
					</lg>
					<lg n="2">
						<l n="13" num="2.1">Ces baisers, ces tiédeurs, ces fraîcheurs, ces corolles,</l>
						<l n="14" num="2.2"><space unit="char" quantity="8"/>Ces recoins secrets pleins d’accueil,</l>
						<l n="15" num="2.3">Ces parfums aussi doux que de bonnes paroles</l>
						<l n="16" num="2.4"><space unit="char" quantity="8"/>Et cette pourpre et ce grand deuil,</l>
						<l n="17" num="2.5">Si vraiment tu les as, Nature, ô maternelle,</l>
						<l n="18" num="2.6">Si ce n’est pas un songe, à moi donc tous ces biens !</l>
						<l n="19" num="2.7">Le printemps tout entier gonfle mon cœur ; je viens</l>
						<l n="20" num="2.8">A ta coupe qu’emplit la jeunesse éternelle,</l>
						<l n="21" num="2.9">Et j’y veux étancher la soif que je ressens,</l>
						<l n="22" num="2.10"><space unit="char" quantity="8"/>Et j’y veux, oubliant mes peines,</l>
						<l n="23" num="2.11">Sentir le renouveau m’envahir jusqu’aux veines</l>
						<l n="24" num="2.12"><space unit="char" quantity="8"/>De grands espoirs adolescents.</l>
					</lg>
					<lg n="3">
						<l n="25" num="3.1">Enveloppe ma joie avec de belles robes</l>
						<l n="26" num="3.2"><space unit="char" quantity="8"/>Que sur moi développeront</l>
						<l n="27" num="3.3">Le rouge des couchants et le clair bleu des aubes ;</l>
						<l n="28" num="3.4"><space unit="char" quantity="8"/>Voile ma douleur, si mon front</l>
						<l n="29" num="3.5">Persiste à conserver ses tristesses inertes,</l>
						<l n="30" num="3.6">Dans les grands crêpes noirs de tes nuits sans clartés.</l>
						<l n="31" num="3.7">Ah ! je m’enivrerai parmi les ombres vertes</l>
						<l n="32" num="3.8">Des grands arbres qui font, ainsi que des doigts gais,</l>
						<l n="33" num="3.9"><space unit="char" quantity="8"/>Choir leur floraison sur les faces,</l>
						<l n="34" num="3.10">Et, comme des amis, je presserai leurs masses</l>
						<l n="35" num="3.11"><space unit="char" quantity="8"/>Entre mes deux bras fatigués !</l>
					</lg>
					<lg n="4">
						<l n="36" num="4.1">Laisse-moi me coucher ainsi que joue à joue,</l>
						<l n="37" num="4.2"><space unit="char" quantity="8"/>Calme comme à l’heure où l’on dort,</l>
						<l n="38" num="4.3">Dans l’herbe où des blancheurs d’ombelles font la roue,</l>
						<l n="39" num="4.4"><space unit="char" quantity="8"/>Lourdes du poids d’un bourdon d’or ;</l>
						<l n="40" num="4.5">Laisse-moi respirer ton haleine champêtre</l>
						<l n="41" num="4.6">Où passe la douceur de quelque souffle humain ;</l>
						<l n="42" num="4.7">Laisse-moi me pencher, un bouquet à la main,</l>
						<l n="43" num="4.8">Sur tes étangs profonds où je ris d’apparaître,</l>
						<l n="44" num="4.9">Pour boire à plein gosier leur liquide cristal,</l>
						<l n="45" num="4.10"><space unit="char" quantity="8"/>Tendre à tes sources mes deux paumes,</l>
						<l n="46" num="4.11">Écouter tous tes chants, goûter tous les arômes,</l>
						<l n="47" num="4.12"><space unit="char" quantity="8"/>O Sève ! ô Printemps triomphal !</l>
					</lg>
				</div></body></text></TEI>