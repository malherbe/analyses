<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">OCCIDENT</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3151 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Occident</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">http://www.poesies.net/luciedelaruemardrueoccident.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Occident</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LA REVUE BLANCHE</publisher>
							<date when="1901">1901</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1901">1901</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
					<p>Certains retraits de vers ont été supprimés conformément à l’édition de référence.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
				<change when="2023-06-24" who="RR">Quelques corrections d’erreurs de numérisation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">EN PLEIN VENT</head><div type="poem" key="DLR10">
					<head type="main">RIMES SEPTEMBRALES</head>
					<lg n="1">
						<l n="1" num="1.1">Belle, voici venir les heures lentes, lentes…</l>
						<l n="2" num="1.2">Pose ton front rêveur au glauque des carreaux</l>
						<l n="3" num="1.3">Et dilate bien tes prunelles vigilantes.</l>
					</lg>
					<lg n="2">
						<l n="4" num="2.1">Que tes yeux voient roussir dans les sentiers ruraux</l>
						<l n="5" num="2.2">La branche folle qui, triste que l’été meure,</l>
						<l n="6" num="2.3">Balance au vent un vol posé de passeraux.</l>
					</lg>
					<lg n="3">
						<l n="7" num="3.1">Voici le temps où l’arbre avec ses feuilles pleure</l>
						<l n="8" num="3.2">Des pleurs larges tombés par les pâles midis</l>
						<l n="9" num="3.3">Et par les minuits clairs qu’un rai de lune effleure ;</l>
					</lg>
					<lg n="4">
						<l n="10" num="4.1">Pleurs larges, neige jaune aux ornières, tandis</l>
						<l n="11" num="4.2">Qu’y mettront tes pas lents un frôlement de soie</l>
						<l n="12" num="4.3">Dont fuira l’essaim noir des corbeaux alourdis ;</l>
					</lg>
					<lg n="5">
						<l n="13" num="5.1">Pleurs larges reparus dans l’or des feux de joie</l>
						<l n="14" num="5.2">Que brûlent à la fois trois bûches de Noël</l>
						<l n="15" num="5.3">Quand la famille au soir près de l’âtre s’éploie ;</l>
					</lg>
					<lg n="6">
						<l n="16" num="6.1">Pleurs larges effarés quand la rage du ciel</l>
						<l n="17" num="6.2">Hurlant aux volets clos et dans la cheminée</l>
						<l n="18" num="6.3">Interrompt l’aïeul dans son conte habituel…</l>
					</lg>
					<lg n="7">
						<l n="19" num="7.1">Que tes yeux voient déjà la campagne fanée,</l>
						<l n="20" num="7.2">L’aube retardataire et les soirs attristants</l>
						<l n="21" num="7.3">Vite venus, disant qu’agonise l’année ;</l>
					</lg>
					<lg n="8">
						<l n="22" num="8.1">Puis prépare-toi pour les songes mal contents</l>
						<l n="23" num="8.2">Que t’apporte le froid comme aux vieilles branlantes</l>
						<l n="24" num="8.3">Malgré ta joue où luit le rouge des vingt ans,</l>
					</lg>
					<lg n="9">
						<l n="25" num="9.1">Car voici s’avancer les heures lentes, lentes…</l>
					</lg>
				</div></body></text></TEI>