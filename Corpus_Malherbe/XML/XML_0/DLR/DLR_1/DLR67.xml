<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">OCCIDENT</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3151 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Occident</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">http://www.poesies.net/luciedelaruemardrueoccident.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Occident</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LA REVUE BLANCHE</publisher>
							<date when="1901">1901</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1901">1901</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
					<p>Certains retraits de vers ont été supprimés conformément à l’édition de référence.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
				<change when="2023-06-24" who="RR">Quelques corrections d’erreurs de numérisation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">L’ÂME DES RUES</head><div type="poem" key="DLR67">
					<head type="main">PLACES</head>
					<opener>
						<salute>A Mlle Marie Bengesco.</salute>
					</opener>
					<div type="section" n="1">
						<head type="number">I</head>
						<lg n="1">
							<l n="1" num="1.1">J’aime que sur la place où traîne le couchant</l>
							<l n="2" num="1.2">Monte, parmi le bruit des foules, le doux chant</l>
							<l n="3" num="1.3">Des eaux claires sonnant au bronze des fontaines</l>
							<l n="4" num="1.4">Et que, centre au rempart des églises hautaines,</l>
							<l n="5" num="1.5">Dont le fleuve en passant fait ruisseler le seuil,</l>
							<l n="6" num="1.6">Et des lourds monuments des des arches d’orgueil,</l>
							<l n="7" num="1.7">L’obélisque fluet s’érige sous l’égide</l>
							<l n="8" num="1.8">Des huit villes siégeant dans leur robe rigide,</l>
							<l n="9" num="1.9">Qui, sur le crépuscule où meurent les contours,</l>
							<l n="10" num="1.10">Profilent en vigueur leurs chefs coiffés de tours ;</l>
							<l n="11" num="1.11">Et qu’auprès, scintillant comme un ballet d’étoiles,</l>
							<l n="12" num="1.12">Tournoie en titubant, névrosé jusqu’aux moelles,</l>
							<l n="13" num="1.13">Génial, amer, gai, charmant, terrible, gris,</l>
							<l n="14" num="1.14">Le grand léviathan écaillé d’or, Paris !</l>
						</lg>
					</div>
					<div type="section" n="2">
						<head type="number">II</head>
						<lg n="1">
							<l n="15" num="1.1">L’ample courbe des arcs de triomphe anguleux</l>
							<l n="16" num="1.2">Dont les ornements durs crèvent le ciel houleux,</l>
							<l n="17" num="1.3">Encadre le couchant qui monte et qui flamboie</l>
							<l n="18" num="1.4">Au loin comme un énorme et muet feu de joie,</l>
						</lg>
						<lg n="2">
							<l n="19" num="2.1">Fond pur où les troncs noirs détachent leurs profils</l>
							<l n="20" num="2.2">Avec tous leurs rameaux fluets comme des fils,</l>
							<l n="21" num="2.3">Atmosphère dorée où s’élancent les flèches</l>
							<l n="22" num="2.4">Des clochers et que boit la pierre à pleines brèches,</l>
						</lg>
						<lg n="3">
							<l n="23" num="3.1">Où les carreaux de vitre et la clarté des eaux</l>
							<l n="24" num="3.2">Redisent la splendeur du ciel, où les oiseaux</l>
							<l n="25" num="3.3">Laissent, portant aux nids leur butin minuscule,</l>
							<l n="26" num="3.4">Au travers de leur vol passer le crépuscule…</l>
						</lg>
						<lg n="4">
							<l n="27" num="4.1">O pureté des cieux ! ô silence ! ô douceur !</l>
							<l n="28" num="4.2">Immense calme où peut se retremper le cœur,</l>
							<l n="29" num="4.3">Mourir la chair, l’esprit revenir à la règle</l>
							<l n="30" num="4.4">Et l’âme déployer son envergure d’aigle !</l>
						</lg>
					</div>
				</div></body></text></TEI>