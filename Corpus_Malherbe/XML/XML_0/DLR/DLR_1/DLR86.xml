<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">OCCIDENT</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3151 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Occident</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">http://www.poesies.net/luciedelaruemardrueoccident.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Occident</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LA REVUE BLANCHE</publisher>
							<date when="1901">1901</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1901">1901</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
					<p>Certains retraits de vers ont été supprimés conformément à l’édition de référence.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
				<change when="2023-06-24" who="RR">Quelques corrections d’erreurs de numérisation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">L’ENCENSOIR</head><div type="poem" key="DLR86">
					<head type="main">HÉROICA</head>
					<lg n="1">
						<l n="1" num="1.1">O belle ! je voudrais comme quelque Athéné</l>
						<l n="2" num="1.2">Te voir surgir du fond de mes rimes guerrières,</l>
						<l n="3" num="1.3">Tout l’être d’un atour héroïque adorné.</l>
					</lg>
					<lg n="2">
						<l n="4" num="2.1">Car il siérait fort bien à tes grandes manières,</l>
						<l n="5" num="2.2">A ce port belliqueux que fuient les nonchaloirs</l>
						<l n="6" num="2.3">De vêtir l’appareil des époques premières ;</l>
					</lg>
					<lg n="3">
						<l n="7" num="3.1">Car nul chef, redressé pour de rudes vouloirs,</l>
						<l n="8" num="3.2">Ne te vaudrait coiffant du casque des batailles</l>
						<l n="9" num="3.3">Le casque ténébreux de tes longs cheveux noirs ;</l>
					</lg>
					<lg n="4">
						<l n="10" num="4.1">Car ton torse vivrait à l’aise dans les mailles</l>
						<l n="11" num="4.2">De la cotte moulant la gloire de tes seins</l>
						<l n="12" num="4.3">Parmi le chatoiement poissonneux des écailles ;</l>
					</lg>
					<lg n="5">
						<l n="13" num="5.1">Car ton col est de ceux qui pourraient être ceints</l>
						<l n="14" num="5.2">Par le quadruple tour des colliers de Palmyre</l>
						<l n="15" num="5.3">Que, sans ployer, portait la reine aux grands desseins ;</l>
					</lg>
					<lg n="6">
						<l n="16" num="6.1">Car enfin, comme au temps que notre rêve admire,</l>
						<l n="17" num="6.2">Au front de quelque horde on te voit aisément</l>
						<l n="18" num="6.3">Étinceler ainsi qu’un vivant point de mire.</l>
					</lg>
					<lg n="7">
						<l n="19" num="7.1">Ah ! pendant qu’attiré comme par un aimant</l>
						<l n="20" num="7.2">Mon regard obstiné te suit et te contemple,</l>
						<l n="21" num="7.3">Laisse mon fol esprit s’abuser un moment !</l>
					</lg>
					<lg n="8">
						<l n="22" num="8.1">Laisse-moi remplir l’air avec ton appel ample</l>
						<l n="23" num="8.2">Menant tes escadrons aux sauvages refrains</l>
						<l n="24" num="8.3">Vers la destruction de la ville et du temple ;</l>
					</lg>
					<lg n="9">
						<l n="25" num="9.1">Laisse-moi, cependant que se cambrent tes reins</l>
						<l n="26" num="9.2">Mêler le cliquetis de tes armes brandies</l>
						<l n="27" num="9.3">Au choc des joyaux lourds sortis de tes écrins ;</l>
					</lg>
					<lg n="10">
						<l n="28" num="10.1">J’allume tes yeux noirs au feu des incendies,</l>
						<l n="29" num="10.2">J’étale la blancheur féroce de tes dents</l>
						<l n="30" num="10.3">Dans un rire annonçant de proches tragédies,</l>
					</lg>
					<lg n="11">
						<l n="31" num="11.1">Je mets derrière toi des horizons ardents,</l>
						<l n="32" num="11.2">Devant toi le butin qu’indique ton grand geste</l>
						<l n="33" num="11.3">A qui répond le chœur de mille cris stridents,</l>
					</lg>
					<lg n="12">
						<l n="34" num="12.1">Pour, parmi ce décor fictif de plaine agreste</l>
						<l n="35" num="12.2">Que foulent au galop les pieds des étalons</l>
						<l n="36" num="12.3">A la suite desquels nulle moisson ne reste,</l>
					</lg>
					<lg n="13">
						<l n="37" num="13.1">Terrible, frémissant de la nuque aux talons,</l>
						<l n="38" num="13.2">La poigne exaspérée au manche de l’épée,</l>
						<l n="39" num="13.3">La course déployant au vent tes cheveux longs,</l>
					</lg>
					<lg n="14">
						<l n="40" num="14.1">Animer de toi seule une immense épopée !</l>
					</lg>
				</div></body></text></TEI>