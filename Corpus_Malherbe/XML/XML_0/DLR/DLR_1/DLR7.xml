<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">OCCIDENT</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3151 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Occident</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">http://www.poesies.net/luciedelaruemardrueoccident.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Occident</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LA REVUE BLANCHE</publisher>
							<date when="1901">1901</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1901">1901</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
					<p>Certains retraits de vers ont été supprimés conformément à l’édition de référence.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
				<change when="2023-06-24" who="RR">Quelques corrections d’erreurs de numérisation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">EN PLEIN VENT</head><div type="poem" key="DLR7">
					<head type="main">LE VENT DANS LES ROSEAUX</head>
					<lg n="1">
						<l n="1" num="1.1">Le vent chante à mi-voix des chansons bucoliques</l>
						<l n="2" num="1.2"><space unit="char" quantity="8"/>Dans les roseaux mélancoliques.</l>
					</lg>
					<lg n="2">
						<l n="3" num="2.1">Les roseaux au passage ont pris sa grande voix,</l>
						<l n="4" num="2.2"><space unit="char" quantity="8"/>Un par un et tous à la fois.</l>
					</lg>
					<lg n="3">
						<l n="5" num="3.1">Et l’un succède à l’un qui tremblote et déclame</l>
						<l n="6" num="3.2"><space unit="char" quantity="8"/>Et tient sa place dans la gamme.</l>
					</lg>
					<lg n="4">
						<l n="7" num="4.1">Goutte de bruit, son pur, son frais, chaque roseau</l>
						<l n="8" num="4.2"><space unit="char" quantity="8"/>Imite une gorge d’oiseau ;</l>
					</lg>
					<lg n="5">
						<l n="9" num="5.1">Mais au loin fait l’envol de leurs notes furtives</l>
						<l n="10" num="5.2"><space unit="char" quantity="8"/>Un chœur de flûtes primitives.</l>
					</lg>
					<lg n="6">
						<l n="11" num="6.1">Chantez l’heure qui passe, aube, vesprée et nuit,</l>
						<l n="12" num="6.2">Bleu frôlement de l’aube au bord du lointain sombre,</l>
						<l n="13" num="6.3">La vesprée au ciel pur, taches d’or, taches d’ombre,</l>
						<l n="14" num="6.4">Et la paix des couchants où la pourpre reluit ;</l>
					</lg>
					<lg n="7">
						<l n="15" num="7.1">Chantez l’idylle assis ou bien agenouillée</l>
						<l n="16" num="7.2">Qui sourit, couple heureux du geste qui le tient,</l>
						<l n="17" num="7.3">Et les bouquets cueillis et le baiser païen</l>
						<l n="18" num="7.4">Simple, pudique et clos comme une fleur mouillée ;</l>
					</lg>
					<lg n="8">
						<l n="19" num="8.1">Chantez la pastorale agreste, les troupeaux</l>
						<l n="20" num="8.2">Assoupis parmi l’herbe où crissent les cigales,</l>
						<l n="21" num="8.3">Les bergers, célébrant de leurs flûtes égales,</l>
						<l n="22" num="8.4">Deux à deux, trois à trois, la torpeur du repos ;</l>
					</lg>
					<lg n="9">
						<l n="23" num="9.1">Chantez les vieillards lents assis au seuil des portes,</l>
						<l n="24" num="9.2">L’enfance et la jeunesse et le rire et les pleurs ;</l>
						<l n="25" num="9.3">Chantez les cheveux blonds et noirs où sont les fleurs,</l>
						<l n="26" num="9.4">Chantez les cheveux blancs coiffés de feuilles mortes.</l>
					</lg>
					<lg n="10">
						<l n="27" num="10.1"><space unit="char" quantity="8"/>Un par un et tous à la fois,</l>
						<l n="28" num="10.2">Frôlement sous le vent au loin, mi-bruit, mi-voix,</l>
						<l n="29" num="10.3">Roseaux dont fait l’envol de vos notes furtives</l>
						<l n="30" num="10.4"><space unit="char" quantity="8"/>Un chœur de flûtes primitives…</l>
					</lg>
				</div></body></text></TEI>