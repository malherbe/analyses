<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">OCCIDENT</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3151 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Occident</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">http://www.poesies.net/luciedelaruemardrueoccident.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Occident</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LA REVUE BLANCHE</publisher>
							<date when="1901">1901</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1901">1901</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
					<p>Certains retraits de vers ont été supprimés conformément à l’édition de référence.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
				<change when="2023-06-24" who="RR">Quelques corrections d’erreurs de numérisation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PAROLES I</head><div type="poem" key="DLR53">
					<head type="main">LE POÈME DE LA VIE ET DE LA MORT</head>
					<div type="section" n="1">
						<head type="main">L’ÊTRE</head>
						<lg n="1">
							<l n="1" num="1.1">Je me suis éveillé dans l’aurore naissante</l>
							<l n="2" num="1.2"><space unit="char" quantity="8"/>Qui luit au ciel son joyau clair ;</l>
							<l n="3" num="1.3">Je me suis éveillé dans la tiédeur de l’air</l>
							<l n="4" num="1.4">Qui sans cesse refait la terre adolescente ;</l>
							<l n="5" num="1.5">Je me suis éveillé dans l’aurore naissante,</l>
							<l n="6" num="1.6">Dans les flocons d’avril qui fleurissent la sente</l>
							<l n="7" num="1.7"><space unit="char" quantity="8"/>Et dans la beauté de ma chair.</l>
						</lg>
						<lg n="2">
							<l n="8" num="2.1">Et je tends mes deux bras lyriques vers les choses,</l>
							<l n="9" num="2.2"><space unit="char" quantity="8"/>Car mon âme, de l’aube au soir,</l>
							<l n="10" num="2.3">Pour ses torrents de vie appelle un déversoir ;</l>
							<l n="11" num="2.4">Car l’exaltation gonfle mes lèvres closes</l>
							<l n="12" num="2.5">Et je tends mes deux bras lyriques vers les choses,</l>
							<l n="13" num="2.6">Avides de brûler dans des apothéoses</l>
							<l n="14" num="2.7"><space unit="char" quantity="8"/>Ainsi qu’un vivant encensoir.</l>
						</lg>
						<lg n="3">
							<l n="15" num="3.1">J’ai soif de la beauté, j’ai soif de la lumière</l>
							<l n="16" num="3.2"><space unit="char" quantity="8"/>Et de la force et de l’ampleur</l>
							<l n="17" num="3.3">Assez pour contenir l’univers dans mon cœur ;</l>
							<l n="18" num="3.4">Voulant plus que la joie humaine coutumière</l>
							<l n="19" num="3.5">J’ai soif de la beauté, j’ai soif de la lumière,</l>
							<l n="20" num="3.6">Et je vais les saisir dans l’étreinte première,</l>
							<l n="21" num="3.7"><space unit="char" quantity="8"/>De ma belle jeunesse en fleur.</l>
						</lg>
						<lg n="4">
							<l n="22" num="4.1">Mon âme s’ouvre à tous, profonde et fraternelle,</l>
							<l n="23" num="4.2"><space unit="char" quantity="8"/>Ma chair s’offre à la volupté.</l>
							<l n="24" num="4.3">Ah ! que viennent l’amour, la beauté, la bonté !</l>
							<l n="25" num="4.4">Car ! pour participer à la joie éternelle,</l>
							<l n="26" num="4.5">Mon âme s’ouvre à tous profonde et fraternelle,</l>
							<l n="27" num="4.6">Var je sens palpiter comme un aigle son aile</l>
							<l n="28" num="4.7"><space unit="char" quantity="8"/>L’espoir dont mon être est hanté !</l>
						</lg>
					</div>
					<div type="section" n="2">
						<head type="main">LE PREMIER SPECTRE</head>
						<lg n="1">
							<l part="I" n="29" num="1.1">Je suis là.</l>
						</lg>
					</div>
					<div type="section" n="3">
						<head type="main">LE SECOND SPECTRE</head>
						<lg n="1">
							<l part="M" n="29">Je suis là.</l>
						</lg>
					</div>
					<div type="section" n="4">
						<head type="main">L’ÊTRE</head>
						<lg n="1">
							<l part="F" n="29">Quelles sont ces deux ombres ?</l>
							<l n="30" num="1.1">Elles s’assoient ainsi que deux visiteurs sombres</l>
							<l n="31" num="1.2">Et muets, toutes deux à ma porte. L’une a,</l>
							<l n="32" num="1.3">A même la figure, un masque d’incarnat</l>
							<l n="33" num="1.4">Et qui rit ; et sa robe est d’étoffe fleurie ;</l>
							<l n="34" num="1.5">Une couronne prise à travers champs marie</l>
							<l n="35" num="1.6">Le vif de des couleurs au noir de ses cheveux.</l>
							<l n="36" num="1.7">Et l’autre a répandu sus ses membres nerveux</l>
							<l n="37" num="1.8">Une étoffe de lin imiteuse de lange,</l>
							<l n="38" num="1.9">Pendant que rien ne luit sur son visage étrange</l>
							<l n="39" num="1.10">Qui n’a ni yeux, ni nez, ni bouche, que ses dents ;</l>
							<l n="40" num="1.11">Elle semble cacher dans ses drapés prudents</l>
							<l n="41" num="1.12">Quelque arme à tranchant clair dont je ne me rends compte.</l>
						</lg>
					</div>
					<div type="section" n="5">
						<head type="main">LE PREMIER SPECTRE</head>
						<lg n="1">
							<l n="42" num="1.1">Nous sommes là tous deux pour te conter un conte,</l>
							<l n="43" num="1.2">Mais, avant que nos voix te parlent tour à tour,</l>
							<l n="44" num="1.3">Lève la belle robe où se fond mon contour,</l>
							<l n="45" num="1.4">Fleure les belles fleurs dont ma tête se noue,</l>
							<l n="46" num="1.5">Écarte le beau masque appliqué sur ma joue</l>
							<l n="47" num="1.6">Et, sous ma robe, vois mes blessures saigner,</l>
							<l n="48" num="1.7">Tous mes calices frais prêts à t’empoisonner</l>
							<l n="49" num="1.8">Et son mon masque gai sangloter ma figure.</l>
							<l part="I" n="50" num="1.9">Je suis la Vie.</l>
						</lg>
					</div>
					<div type="section" n="6">
						<head type="main">LE SECOND SPECTRE</head>
						<lg n="1">
							<l part="F" n="50">Et moi, soulève ma vêture</l>
							<l n="51" num="1.1">Pauvre qui ferait croire un corps sous ses plis faux ;</l>
						</lg>
						<lg n="2">
							<l n="52" num="2.1">Tu n’y vois qu’un squelette étriqué ; mais la faulx</l>
							<l n="53" num="2.2">Que j’y cachais t’éclate aux yeux, arme qui reste</l>
							<l n="54" num="2.3">Terriblement rivée au hasard de mon geste.</l>
							<l part="I" n="55" num="2.4">Je suis la Mort.</l>
						</lg>
					</div>
					<div type="section" n="7">
						<head type="main">L’ÊTRE</head>
						<lg n="1">
							<l part="F" n="55">O couple affreux ! Spectres jumeaux !</l>
							<l n="56" num="1.1">Quelle histoire d’horreur va sortir de vos mots !</l>
						</lg>
					</div>
					<div type="section" n="8">
						<head type="main">LE PREMIER SPECTRE</head>
						<lg n="1">
							<l n="57" num="1.1">Vois ! Des cortèges vont sans but ; ah les cortèges,</l>
							<l n="58" num="1.2"><space unit="char" quantity="8"/>Les mornes, les pareils toujours !</l>
							<l n="59" num="1.3">Par villes et par champs, par les nuits, par les jours,</l>
							<l n="60" num="1.4"><space unit="char" quantity="8"/>Par les printemps et par les neiges !</l>
						</lg>
						<lg n="2">
							<l n="61" num="2.1">Vois ! Ce sont des bras fous tragiquement tordus</l>
							<l n="62" num="2.2"><space unit="char" quantity="8"/>Et des bouches d’où le cri monte,</l>
							<l n="63" num="2.3">Cri de révolte et cri de deuil, misère et honte,</l>
							<l n="64" num="2.4"><space unit="char" quantity="8"/>Désirs et doutes éperdus.</l>
						</lg>
						<lg n="3">
							<l n="65" num="3.1">Vois ! la faim râle au fond des taudis, et le vice</l>
							<l n="66" num="3.2"><space unit="char" quantity="8"/>Emplit bouges et lupanars,</l>
							<l n="67" num="3.3">Et la maladie âpre aux milles cauchemars</l>
							<l n="68" num="3.4"><space unit="char" quantity="8"/>Grouille et déborde de l’hospice ;</l>
						</lg>
						<lg n="4">
							<l n="69" num="4.1">Vois ! les adieux, l’orgueil à bas, l’amour trahi</l>
							<l n="70" num="4.2"><space unit="char" quantity="8"/>Hurlent, poussés vers les suicides,</l>
							<l n="71" num="4.3">Vers le plaisir tueur de mémoires lucides,</l>
							<l n="72" num="4.4"><space unit="char" quantity="8"/>Vers l’alcool recéleur d’oubli ;</l>
						</lg>
						<lg n="5">
							<l n="73" num="5.1">Les refuges cherchés gardent leurs portes closes,</l>
							<l n="74" num="5.2"><space unit="char" quantity="8"/>L’amour est un leurre et l’art ment,</l>
							<l n="75" num="5.3">La musique et les vers sont un nouveau tourment</l>
							<l n="76" num="5.4"><space unit="char" quantity="8"/>Où resanglotent les névroses,</l>
						</lg>
						<lg n="6">
							<l n="77" num="6.1">Et l’Idéal, idole au geste solennel,</l>
							<l n="78" num="6.2"><space unit="char" quantity="8"/>Debout et le chef dans les nues,</l>
							<l n="79" num="6.3">Répond aux piétés des foules accourues</l>
							<l n="80" num="6.4"><space unit="char" quantity="8"/>Par un « à quoi bon ? » éternel !</l>
						</lg>
					</div>
					<div type="section" n="9">
						<head type="main">L’ÊTRE</head>
						<lg n="1">
							<l part="I" n="81" num="1.1">Au secours ! Au secours !…</l>
						</lg>
					</div>
					<div type="section" n="10">
						<head type="main">LE SPECTRE</head>
						<lg n="1">
							<l part="F" n="81">Écoute la sentence</l>
							<l n="82" num="1.1"><space unit="char" quantity="8"/>Épouvantable jusqu’au bout :</l>
							<l n="83" num="1.2">Tu resteras toujours vivant, toujours debout</l>
							<l n="84" num="1.3"><space unit="char" quantity="8"/>Malgré l’enfer de l’existence,</l>
						</lg>
						<lg n="2">
							<l n="85" num="2.1">Marqué tout à la fois dans ta chair et ton cœur</l>
							<l n="86" num="2.2"><space unit="char" quantity="8"/>Par la grande misère humaine,</l>
							<l n="87" num="2.3">Rides du lourd péché, de l’espérance vaine</l>
							<l n="88" num="2.4"><space unit="char" quantity="8"/>Et de l’inutile labeur,</l>
						</lg>
						<lg n="3">
							<l n="89" num="3.1">Et, flagellé, rempli d’horreur et d’anémie,</l>
							<l n="90" num="3.2"><space unit="char" quantity="8"/>Dans le silence et l’abandon,</l>
							<l n="91" num="3.3">Sombre, tu couveras une haine sans nom</l>
							<l n="92" num="3.4"><space unit="char" quantity="8"/>Pour ton ambiance ennemie !</l>
						</lg>
						<lg n="4">
							<l n="93" num="4.1">Et maintenant, adieu ! vers l’avenir maudit</l>
							<l n="94" num="4.2"><space unit="char" quantity="8"/>Dont l’effroi déjà te trépane,</l>
							<l n="95" num="4.3">Déambule, pantin ! navigue, barque en panne !</l>
							<l n="96" num="4.4"><space unit="char" quantity="8"/>Pour moi, je me rassieds. J’ai dit !</l>
						</lg>
					</div>
					<div type="section" n="11">
						<head type="main">L’ÊTRE</head>
						<lg n="1">
							<l n="97" num="1.1">O bonne mort ! ô mort douce et pleine de grâce,</l>
							<l n="98" num="1.2">C’est vers toi, dans l’horreur folle qui me terrasse</l>
							<l n="99" num="1.3">Que, les yeux ruisselants de trop d’affliction,</l>
							<l n="100" num="1.4">Je tends mes bras chercheurs de consolation,</l>
							<l n="101" num="1.5">Éternelle présence à qui mon pas se rive,</l>
							<l n="102" num="1.6">Seul but où diriger mon atroce dérive,</l>
							<l n="103" num="1.7">O toi l’unique, ô toi l’immanquable, la sœur,</l>
							<l n="104" num="1.8">Prends-moi comme un enfant qui pleure sur ton cœur</l>
							<l n="105" num="1.9">Et conte-moi tout bas la croyance future ;</l>
							<l n="106" num="1.10">Car, puisque rien n’a pu dans toute la nature</l>
							<l n="107" num="1.11">Assouvir le désir dont j’étais dévoré,</l>
							<l n="108" num="1.12">Puisque je reste en deuil de mon espoir doré</l>
							<l n="109" num="1.13">Puisque pour cette soif dont mon âme déssèche</l>
							<l n="110" num="1.14">Je n’ai pu nulle part trouver de source fraîche,</l>
						</lg>
						<lg n="2">
							<l n="111" num="2.1">A moi l’espoir qui fait renaître les cœurs morts !</l>
							<l n="112" num="2.2">A moi la bonne paix hanteuse d’âmes veuves !</l>
							<l n="113" num="2.3">A moi le baume en qui les esprits et les corps</l>
							<l n="114" num="2.4">Se guérissent du mal profond de trop d’épreuves !</l>
						</lg>
						<lg n="3">
							<l n="115" num="3.1">A moi la joie après la mort, remplacement</l>
							<l n="116" num="3.2">Du bonheur que cherchait mon âme printanière,</l>
							<l n="117" num="3.3">Seule source où pourra boire éternellement</l>
							<l n="118" num="3.4">Mon éternelle soif de Vie et de Lumière !</l>
						</lg>
						<lg n="4">
							<l n="119" num="4.1">Ah ! puisqu’il FAUT connaître ici-bas la douleur,</l>
							<l n="120" num="4.2">Puisque la loi fatale est pour nous tous la même,</l>
							<l n="121" num="4.3">Donne-moi la douleur où l’on met tous son cœur,</l>
							<l n="122" num="4.4">Donne-moi la douleur au fond de qui l’on aime :</l>
						</lg>
						<lg n="5">
							<l n="123" num="5.1">Fais que mon désespoir se fonde en piété,</l>
							<l n="124" num="5.2">Fais qu’âme et chair je sois une double victime</l>
							<l n="125" num="5.3">D’un holocauste fait à la Divinité</l>
							<l n="126" num="5.4">Grand d’être volontaire, énergique, anonyme ;</l>
						</lg>
						<lg n="6">
							<l n="127" num="6.1">Que je serve d’enclume à ce divin marteau,</l>
							<l n="128" num="6.2">Que mon infimité se grandisse et rehausse</l>
							<l n="129" num="6.3">D’obéir tout entière à l’Infini, plutôt</l>
							<l n="130" num="6.4">Qu’à cette vanité terrestre, inepte et fausse,</l>
						</lg>
						<lg n="7">
							<l n="131" num="7.1">Et parmi le chagrin, la souffrance et l’ennui,</l>
							<l n="132" num="7.2">Dans ce cortège humain qui languit et qui pleure,</l>
							<l n="133" num="7.3">S’il faut vivre, je vis ! Mais que ce soit pour lui,</l>
							<l n="134" num="7.4">Dieu ! Dieu, mon seul espoir, mon but et ma demeure !</l>
						</lg>
					</div>
					<div type="section" n="12">
						<head type="main">LE SECOND SPECTRE</head>
						<lg n="1">
							<l n="135" num="1.1">Clame ton impuissance ou prie humble et tout bas,</l>
							<l n="136" num="1.2">Le muet Infini ne te répondra pas.</l>
							<l n="137" num="1.3">Le suprême dédain de cette offre sublime</l>
							<l n="138" num="1.4">De sacrifice auguste, austère, entier, INTIME,</l>
							<l n="139" num="1.5">Tombe, avec ce silence implacable, sur toi.</l>
							<l n="140" num="1.6">Il n’y a ni l’espoir, ni le but, ni le toit</l>
							<l n="141" num="1.7">Derrière le secret de la voûte infinie.</l>
							<l n="142" num="1.8">Pour moi, je t’apprendrai la peur de l’agonie,</l>
							<l n="143" num="1.9">Le remords de la fin, la terreur de l’après,</l>
							<l n="144" num="1.10">Toutes ces affres qui, soit de loin, soit de près</l>
							<l n="145" num="1.11">Te guettent, puisqu’il faut que tout être succombe ;</l>
							<l n="146" num="1.12">Je t’apprendrai l’horreur de l’oubli sur ta tombe,</l>
							<l n="147" num="1.13">Seconde mort à qui nul n’échappe ici-bas.</l>
							<l n="148" num="1.14">Mais, où ton âme ira, tu ne le sauras pas.</l>
							<l n="149" num="1.15">Que le monde sur toi laisse tomber sa porte,</l>
							<l n="150" num="1.16">Je ne te dirai pas les lieux où je t’emporte.</l>
							<l n="151" num="1.17">Maintenant, tends au ciel ton bras désespéré ;</l>
							<l n="152" num="1.18">Cherches-y le prétexte et la raison ; muré,</l>
							<l n="153" num="1.19">Lève sur cet espace ouvert ton œil avide,</l>
							<l n="154" num="1.20">Et tu n’y verras rien qu’un formidable vide,</l>
							<l n="155" num="1.21">Cependant qu’à tes pieds monte le mauvais bruit</l>
							<l n="156" num="1.22">Du monde qu’à présent toute ton âme est fuit,</l>
							<l n="157" num="1.23">Hideux de sa douleur et de sa gaîté pire</l>
							<l n="158" num="1.24">Comme un sanglot noyé dans un éclat de rire !</l>
						</lg>
					</div>
					<div type="section" n="13">
						<head type="main">L’ÊTRE</head>
						<lg n="1">
							<l n="159" num="1.1">L’horreur de ton discours est plus profonde encor…</l>
							<l n="160" num="1.2">Au secours ! Au secours !… Ah la vie et la mort !…</l>
							<l n="161" num="1.3">Ah ! spectres !… Où vous fuir ? Où cacher ma détresse,</l>
							<l n="162" num="1.4">O vide en qui ma tête impuissante se dresse ?…</l>
							<l n="163" num="1.5">Rien !… Rien… nuit, solitude et silence… O mon cœur,</l>
							<l n="164" num="1.6">Quelle épouvante !… Où fuir ?… J’ai peur ! J’ai peur ! J’ai peur !</l>
						</lg>
					</div>
				</div></body></text></TEI>