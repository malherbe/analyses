<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">OCCIDENT</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3151 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Occident</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">http://www.poesies.net/luciedelaruemardrueoccident.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Occident</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LA REVUE BLANCHE</publisher>
							<date when="1901">1901</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1901">1901</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
					<p>Certains retraits de vers ont été supprimés conformément à l’édition de référence.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
				<change when="2023-06-24" who="RR">Quelques corrections d’erreurs de numérisation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">VESPÉRALES</head><div type="poem" key="DLR119">
					<head type="main">VILLE DU SOIR</head>
					<lg n="1">
						<l rhyme="none" n="1" num="1.1">La ville s’illumine au lointain à fleur d’eau</l>
						<l n="2" num="1.2"><space unit="char" quantity="8"/>Tout au bout de la mer nocturne ;</l>
						<l n="3" num="1.3">Lève ta tête basse, ô triste,ô taciturne !</l>
					</lg>
					<lg n="2">
						<l rhyme="none" n="4" num="2.1">Le couchant furieux s’apaise à l’horizon</l>
						<l n="5" num="2.2"><space unit="char" quantity="8"/>Au-dessus de la mer éteinte ;</l>
						<l n="6" num="2.3"><space unit="char" quantity="8"/>Regarde, ô cœur gonflé de plaintes !</l>
					</lg>
					<lg n="3">
						<l rhyme="none" n="7" num="3.1">Lève la tête, vois ! Ainsi qu’un long collier</l>
						<l n="8" num="3.2"><space unit="char" quantity="8"/>La ville brille dans le soir,</l>
						<l n="9" num="3.3">Contredite dans l’eau comme dans un miroir.</l>
					</lg>
					<lg n="4">
						<l rhyme="none" n="10" num="4.1">Les tours luisent, voici les châteaux qui s’allument,</l>
						<l n="11" num="4.2"><space unit="char" quantity="8"/>Voici les palais qui flamboient ;</l>
						<l n="12" num="4.3"><space unit="char" quantity="8"/>Ah ! de la joie, ah ! de la joie !…</l>
					</lg>
					<lg n="5">
						<l rhyme="none" n="13" num="5.1">C’est la ville du bout du monde, c’est Thulé,</l>
						<l n="14" num="5.2"><space unit="char" quantity="8"/>C’est le conte bleu, c’est le rêve ;</l>
						<l n="15" num="5.3">Appareillons ! Partons vers ces lointaines grèves !</l>
					</lg>
					<lg n="6">
						<l rhyme="none" n="16" num="6.1">Puisque notre vaisseau tourne déjà sa proue</l>
						<l n="17" num="6.2">Et comme pour un vol gonfle déjà ses voiles</l>
						<l n="18" num="6.3"><space unit="char" quantity="8"/>Vers ce là-bas riche d’étoiles,</l>
					</lg>
					<lg n="7">
						<l rhyme="none" n="19" num="7.1">Monte et partons ! Tes yeux pleureront de bonheur</l>
						<l n="20" num="7.2"><space unit="char" quantity="8"/>En grosses larmes sur la mer.</l>
						<l n="21" num="7.3">Nous avons tant souffert, nous avons tant souffert !</l>
					</lg>
					<lg n="8">
						<l rhyme="none" n="22" num="8.1">Mais nous aborderons la Ville où nous attendent</l>
						<l n="23" num="8.2"><space unit="char" quantity="8"/>Nos désirs irréalisés</l>
						<l n="24" num="8.3"><space unit="char" quantity="8"/>Et nos pauvres espoirs brisés.</l>
					</lg>
					<lg n="9">
						<l rhyme="none" n="25" num="9.1">Nous allons vivre !… Ah dis, lève-toi, chante-moi</l>
						<l n="26" num="9.2"><space unit="char" quantity="8"/>Quelque hymne les deux bras ouverts</l>
						<l n="27" num="9.3">Dans ton manteau de deuil qui traîne dans la mer ;</l>
					</lg>
					<lg n="10">
						<l rhyme="none" n="28" num="10.1">Car voici qu’on peut voir déjà monter la tour</l>
						<l n="29" num="10.2"><space unit="char" quantity="8"/>Où l’être te fait signe enfin</l>
						<l n="30" num="10.3"><space unit="char" quantity="8"/>Dont tu eus si soif et si faim ;</l>
					</lg>
					<lg n="11">
						<l rhyme="none" n="31" num="11.1">Toute puissance d’homme et tendresse de femme,</l>
						<l n="32" num="11.2"><space unit="char" quantity="8"/>Voici sa bouche pour ta bouche,</l>
						<l n="33" num="11.3">Ses deux bras insensés pour ton amour farouche,</l>
					</lg>
					<lg n="12">
						<l rhyme="none" n="34" num="12.1">Sa bonne épaule pour ton front lourd de pensée,</l>
						<l n="35" num="12.2"><space unit="char" quantity="8"/>Son cœur pour ton cœur gros de sève,</l>
						<l n="36" num="12.3"><space unit="char" quantity="8"/>Ses deux yeux profonds pour tes rêves ;</l>
					</lg>
					<lg n="13">
						<l rhyme="none" n="37" num="13.1">Sa voix parle, son œil voit, son oreille entend,</l>
						<l n="38" num="13.2"><space unit="char" quantity="8"/>Toute une âme compréhensive</l>
						<l n="39" num="13.3">Veille ; attendant ton âme, en sa beauté pensive ;</l>
					</lg>
					<lg n="14">
						<l n="40" num="14.1">Et c’est ta paix et c’est ton amour, c’est ta joie,</l>
						<l n="41" num="14.2"><space unit="char" quantity="8"/>Ah ! de la joie, Ah de la joie !…</l>
						<l n="42" num="14.3">Cette cité qui met ton désir sur la mer,</l>
					</lg>
					<lg n="15">
						<l n="43" num="15.1">O cœur pudique, ô cœur pervers, ô cœur amer !…</l>
					</lg>
				</div></body></text></TEI>