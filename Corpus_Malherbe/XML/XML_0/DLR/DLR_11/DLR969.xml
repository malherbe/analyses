<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">TEMPS PRÉSENTS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>818 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_11</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title type="main">TEMPS PRÉSENTS</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Les Cahiers d’art et d’amitié, P. Mourousy</publisher>
							<date when="1939">1939</date>
						</imprint>
					</monogr>
					<note>Édition numérisée sur demande (BnF)</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1939">1939</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>.....</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLR969">
				<head type="main">BALLADE DES ROMBIÈRES</head>
				<lg n="1">
					<l n="1" num="1.1">Corps déjetés et figure assortie,</l>
					<l n="2" num="1.2">Quand elles vont, suscitant nos burins,</l>
					<l n="3" num="1.3">Dans le Saint-Lieu tendre devers l’hostie</l>
					<l n="4" num="1.4">Leur lèvre blême où se dressent des crins,</l>
					<l n="5" num="1.5">Les anges blancs ne sont-ils pas chagrins ?</l>
					<l n="6" num="1.6">Mort, ce pendant, leur prépare des bières,</l>
					<l n="7" num="1.7">Et fossoyeurs leur creusent des terrains.</l>
					<l n="8" num="1.8">— Mais que le ciel confonde les rombières !</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1">Plus ne pouvant être de la partie,</l>
					<l n="10" num="2.2">L’âge, partout, leur ayant mis des freins,</l>
					<l n="11" num="2.3">Dans leur fureur d’aller vers la sortie,</l>
					<l n="12" num="2.4">Contre jeunesse aux minois purpurins</l>
					<l n="13" num="2.5">Vont caquetant comme de vieux serins.</l>
					<l n="14" num="2.6">Bientôt, pourtant, ne boiront vins ni bières,</l>
					<l n="15" num="2.7">Quand leur fumier fera germer des grains.</l>
					<l n="16" num="2.8">— Mais que le ciel confonde les rombières !</l>
				</lg>
				<lg n="3">
					<l n="17" num="3.1">Ne comptons plus que sur épizootie !</l>
					<l n="18" num="3.2">Lors, les démons s’instaurant leurs parrains,</l>
					<l n="19" num="3.3">Elles ceindront la couronne d’ortie</l>
					<l n="20" num="3.4">Qui siéra mieux que leurs vieux galurins.</l>
					<l n="21" num="3.5">Et, chevauchant balais à quatre brins,</l>
					<l n="22" num="3.6">Ces paquets d’os conservés à Plombières</l>
					<l n="23" num="3.7">Dans les enfers iront chauffer leurs reins.</l>
					<l n="24" num="3.8">— Mais que le ciel confonde les rombières !</l>
				</lg>
				<lg n="4">
					<head type="main">ENVOI</head>
					<l n="25" num="4.1">Dames du fiel, régentes des tourbières,</l>
					<l n="26" num="4.2">Empérières des infernaux purins,</l>
					<l n="27" num="4.3">J’ai deviné vos souhaits souterrains.</l>
					<l n="28" num="4.4">— Mais que le ciel confonde les rombières !</l>
				</lg>
			</div></body></text></TEI>