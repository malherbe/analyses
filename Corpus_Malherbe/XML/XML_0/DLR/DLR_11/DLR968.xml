<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">TEMPS PRÉSENTS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>818 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_11</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title type="main">TEMPS PRÉSENTS</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Les Cahiers d’art et d’amitié, P. Mourousy</publisher>
							<date when="1939">1939</date>
						</imprint>
					</monogr>
					<note>Édition numérisée sur demande (BnF)</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1939">1939</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>.....</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLR968">
				<head type="main">PRÉFACE</head>
				<lg n="1">
					<l n="1" num="1.1">Ne pas croire surtout de Mardrus-Delarue</l>
					<l n="2" num="1.2">Que ce petit bouquin fut écrit tout de go.</l>
					<l n="3" num="1.3">Bien au contraire ! Étant de purisme férue,</l>
					<l n="4" num="1.4">Elle exige qu’un vers soit de l’or en lingot</l>
					<l n="5" num="1.5">Et ne peut supporter le style visigoth,</l>
					<l n="6" num="1.6">Ni qu’un poème soit, de nos jours, une loque.</l>
					<l n="7" num="1.7">… Et, maintenant, à nous et l’<hi rend="ital">item</hi> et l’<hi rend="ital">ergo</hi>,</l>
					<l n="8" num="1.8">Car ces ballades-ci parlent de notre époque.</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1">Politique, impôts, sports, le giton et la grue,</l>
					<l n="10" num="2.2">Les juifs, et la rombière au profil de magot,</l>
					<l n="11" num="2.3">L’avion dans le ciel, les croquants dans la rue,</l>
					<l n="12" num="2.4">Tout un stock va vers vous voguer à plein cargo</l>
					<l n="13" num="2.5">Sur quoi mettra la mort le suprême embargo.</l>
					<l n="14" num="2.6">Que de sujets offerts à quiconque se moque !</l>
					<l n="15" num="2.7">Plus d’un couplet, d’ailleurs, sentira le fagot,</l>
					<l n="16" num="2.8">Car ces ballades-ci parlent de notre époque.</l>
				</lg>
				<lg n="3">
					<l n="17" num="3.1">La ballade a ceci pour elle, accorte et drue,</l>
					<l n="18" num="3.2">Qu’elle admet au besoin le gros mot et l’argot.</l>
					<l n="19" num="3.3">Ironique avant tout et parfois incongrue,</l>
					<l n="20" num="3.4">Cette perle n’est point pour les pieds du nigaud.</l>
					<l n="21" num="3.5">Sa science, aux rimeurs, donne le vertigo ?…</l>
					<l n="22" num="3.6">Nonobstant catachrèse et malgré synecdoque,</l>
					<l n="23" num="3.7">Tant d’art, heureusement, n’exclut aucun ragot,</l>
					<l n="24" num="3.8">Car ces ballades-ci parlent de noire époque.</l>
				</lg>
				<lg n="4">
					<head type="main">ENVOI</head>
					<l n="25" num="4.1">Que le présent ouvrage, ô lecteur, ne te choque !</l>
					<l n="26" num="4.2">Il use, j’en conviens, du bec et de l’ergot</l>
					<l n="27" num="4.3">Et n’épargne bourgeois, canaille ni gogo…</l>
					<l n="28" num="4.4">— Mais ces ballades-ci parlent de notre époque.</l>
				</lg>
			</div></body></text></TEI>