<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÈMES MIGNONS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1223 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_8</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÈMES MIGNONS</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruepoesies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÈMES MIGNONS</title>
								<author>Lucie Delarue-Mardrus</author>
								<imprint>
									<pubPlace>Honfleur</pubPlace>
									<publisher>Éditions de la Lieutenance</publisher>
									<date when="2002">2002</date>
								</imprint>
							</monogr>
							<note>L’édition de 2002 est la source de cette version électronique comme le prouve le choix des poèmes et les erreurs d’édition.</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
				<sourceDesc>
					<biblStruct>
						<monogr>
							<title>POÈMES MIGNONS</title>
							<author>Lucie Delarue-Mardrus</author>
							<imprint>
								<pubPlace>Paris</pubPlace>
								<publisher>LIBRAIRIE GEDALGE</publisher>
								<date when="1929">1929</date>
							</imprint>
						</monogr>
						<note>Édition de référence (numérisée)</note>
					</biblStruct>
				</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1929">1929</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le texte a été mis en conformité avec l’édition originale de 1929.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les retraits introduits automatiquement ont été modifiés conformément à l’édition de référence.</p>
				<p>Les poèmes manquants de l’édition 2002 qui est à l’origine de la version électronique disponible on été ajoutés afin d’être conforme à l’édition de 1929.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLR755">
				<head type="main">Violette et Papillon</head>
				<div type="section" n="1">
					<head type="number">I</head>
					<lg n="1">
						<l n="1" num="1.1">Fleurs de Pâques, pâquerettes,</l>
						<l n="2" num="1.2">Empesez vos collerettes !</l>
						<l n="3" num="1.3">Et vous, les jolis boutons d’or,</l>
						<l n="4" num="1.4">Tendez vos tout petits bois d’or !</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">Car demoiselle Violette,</l>
						<l n="6" num="2.2"><space unit="char" quantity="8"></space>Dans un rayon</l>
						<l n="7" num="2.3"><space unit="char" quantity="8"></space>Fait sa toilette,</l>
						<l n="8" num="2.4">Pour épouser le papillon.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">Sa feuille verte est son ombrelle,</l>
						<l n="10" num="3.2"><space unit="char" quantity="8"></space>Très gentiment,</l>
						<l n="11" num="3.3"><space unit="char" quantity="8"></space>Bleu diamant,</l>
						<l n="12" num="3.4">Une goutte d’eau la fait belle.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">Son fin corsage sans corset</l>
						<l n="14" num="4.2"><space unit="char" quantity="8"></space>C’est son calice,</l>
						<l n="15" num="4.3"><space unit="char" quantity="8"></space>Chacun le sait,</l>
						<l n="16" num="4.4">Et sa robe est sa tige lisse.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">Pour la question du parfum,</l>
						<l n="18" num="5.2"><space unit="char" quantity="8"></space>Le sien lui reste.</l>
						<l n="19" num="5.3"><space unit="char" quantity="8"></space>Il est modeste,</l>
						<l n="20" num="5.4">Mais bien plus suave qu’aucun.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1">Voilà donc la toilette faite !</l>
						<l n="22" num="6.2"><space unit="char" quantity="8"></space>C’est grâce à vous,</l>
						<l n="23" num="6.3"><space unit="char" quantity="8"></space>jaune coucous</l>
						<l n="24" num="6.4">Et la fiancée est parfaite.</l>
					</lg>
				</div>
				<div type="section" n="2">
					<head type="number">II</head>
					<lg n="1">
						<l n="25" num="1.1">Or le papillon très aimant,</l>
						<l n="26" num="1.2"><space unit="char" quantity="8"></space>Battant de l’aile,</l>
						<l n="27" num="1.3"><space unit="char" quantity="8"></space>S’en vient vers elle</l>
						<l n="28" num="1.4">Aussi beau qu’un Prince Charmant.</l>
					</lg>
					<lg n="2">
						<l n="29" num="2.1">Debout tout comme des personnes</l>
						<l n="30" num="2.2"><space unit="char" quantity="8"></space>Sous le grand ciel,</l>
						<l n="31" num="2.3"><space unit="char" quantity="8"></space>Douces et bonnes,</l>
						<l n="32" num="2.4">Les fleurs saluent, pleines de miel.</l>
					</lg>
					<lg n="3">
						<l n="33" num="3.1">En cortèges et caravanes,</l>
						<l n="34" num="3.2"><space unit="char" quantity="8"></space>Frelons ronfleurs</l>
						<l n="35" num="3.3"><space unit="char" quantity="8"></space>Autour des fleurs</l>
						<l n="36" num="3.4">Sont de mignons aéroplanes.</l>
					</lg>
					<lg n="4">
						<l n="37" num="4.1">O carillons, carillonnez !</l>
						<l n="38" num="4.2"><space unit="char" quantity="8"></space>O sœurs des cloches,</l>
						<l n="39" num="4.3"><space unit="char" quantity="8"></space>Clochettes proches,</l>
						<l n="40" num="4.4">Sonnez au vent par milliers !</l>
					</lg>
					<lg n="5">
						<l n="41" num="5.1">Voici, pour compléter la noce,</l>
						<l n="42" num="5.2"><space unit="char" quantity="8"></space>Un bon curé.</l>
						<l n="43" num="5.3"><space unit="char" quantity="8"></space>C’est, tout doré,</l>
						<l n="44" num="5.4">Le limaçon gonflant sa bosse.</l>
					</lg>
					<lg n="6">
						<l n="45" num="6.1">Ayant pour adjoint le bourdon,</l>
						<l n="46" num="6.2"><space unit="char" quantity="8"></space>Sans rien qui frise</l>
						<l n="47" num="6.3"><space unit="char" quantity="8"></space>Sa barbe grise,</l>
						<l n="48" num="6.4">Monsieur le maire est un chardon.</l>
					</lg>
					<lg n="7">
						<l n="49" num="7.1">Et, pendant la cérémonie,</l>
						<l n="50" num="7.2"><space unit="char" quantity="8"></space>Le gris grillon</l>
						<l n="51" num="7.3"><space unit="char" quantity="8"></space>Au violon</l>
						<l n="52" num="7.4">Fait entendre un flot d’harmonie.</l>
					</lg>
				</div>
				<div type="section" n="3">
					<head type="number">III</head>
					<lg n="1">
						<l n="53" num="1.1">Sitôt le repas nuptial,</l>
						<l n="54" num="1.2"><space unit="char" quantity="8"></space>Voici la danse !</l>
						<l n="55" num="1.3"><space unit="char" quantity="8"></space>Le bal commence,</l>
						<l n="56" num="1.4">Insectes et fleurs, tous au bal !</l>
					</lg>
					<lg n="2">
						<l n="57" num="2.1">Oui ! Les pétales et les ailes</l>
						<l n="58" num="2.2"><space unit="char" quantity="8"></space>Entrelacés,</l>
						<l n="59" num="2.3"><space unit="char" quantity="8"></space>Tournez, valsez,</l>
						<l n="60" num="2.4">Comme messieurs et demoiselles !</l>
					</lg>
					<lg n="3">
						<l n="61" num="3.1">Le soir vient… Dans l’herbe d’avril</l>
						<l n="62" num="3.2"><space unit="char" quantity="8"></space>Luisante et douce,</l>
						<l n="63" num="3.3"><space unit="char" quantity="8"></space>Chacun se pousse :</l>
						<l n="64" num="3.4">« Où donc est-elle ? Où donc est-il ? »</l>
					</lg>
					<lg n="4">
						<l n="65" num="4.1">Car loin des rumeurs de la fête</l>
						<l n="66" num="4.2"><space unit="char" quantity="8"></space>Ils sont partis,</l>
						<l n="67" num="4.3"><space unit="char" quantity="8"></space>Les deux petits,</l>
						<l n="68" num="4.4">Le papillon, la violette…</l>
					</lg>
					<lg n="5">
						<l n="69" num="5.1">C’est fini ! Fermez-vous, ô fleurs.</l>
						<l n="70" num="5.2"><space unit="char" quantity="8"></space>Adieu, merveilles !</l>
						<l n="71" num="5.3"><space unit="char" quantity="8"></space>Partez abeilles,</l>
						<l n="72" num="5.4">Ronflez, tous les petits moteurs !</l>
					</lg>
					<lg n="6">
						<l n="73" num="6.1">Fleurs de Pâques, pâquerettes,</l>
						<l n="74" num="6.2">Repliez vos collerettes,</l>
						<l n="75" num="6.3">Et vous, les jolis boutons d’or,</l>
						<l n="76" num="6.4">Videz vos tout petits bols d’or !</l>
					</lg>
				</div>
			</div></body></text></TEI>