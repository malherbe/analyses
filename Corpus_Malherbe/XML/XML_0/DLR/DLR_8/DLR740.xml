<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÈMES MIGNONS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1223 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_8</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÈMES MIGNONS</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruepoesies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÈMES MIGNONS</title>
								<author>Lucie Delarue-Mardrus</author>
								<imprint>
									<pubPlace>Honfleur</pubPlace>
									<publisher>Éditions de la Lieutenance</publisher>
									<date when="2002">2002</date>
								</imprint>
							</monogr>
							<note>L’édition de 2002 est la source de cette version électronique comme le prouve le choix des poèmes et les erreurs d’édition.</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
				<sourceDesc>
					<biblStruct>
						<monogr>
							<title>POÈMES MIGNONS</title>
							<author>Lucie Delarue-Mardrus</author>
							<imprint>
								<pubPlace>Paris</pubPlace>
								<publisher>LIBRAIRIE GEDALGE</publisher>
								<date when="1929">1929</date>
							</imprint>
						</monogr>
						<note>Édition de référence (numérisée)</note>
					</biblStruct>
				</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1929">1929</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le texte a été mis en conformité avec l’édition originale de 1929.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les retraits introduits automatiquement ont été modifiés conformément à l’édition de référence.</p>
				<p>Les poèmes manquants de l’édition 2002 qui est à l’origine de la version électronique disponible on été ajoutés afin d’être conforme à l’édition de 1929.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLR740">
				<head type="main">Le Ciel</head>
				<lg n="1">
					<l n="1" num="1.1">Je sais comment c’est dans le ciel :</l>
					<l n="2" num="1.2">C’est un jardin de neige blanche</l>
					<l n="3" num="1.3">Rempli de sapins de Noël.</l>
					<l n="4" num="1.4">Une bougie à chaque branche,</l>
					<l n="5" num="1.5">Sous chaque feuillage un joujou,</l>
					<l n="6" num="1.6">Des étoiles partout, partout,</l>
					<l n="7" num="1.7">Des fils d’or et d’argent, du givre</l>
					<l n="8" num="1.8">Qu’on peut enrouler à son doigt,</l>
					<l n="9" num="1.9">Enfin tout ce qu’il faut pour vivre.</l>
				</lg>
				<lg n="2">
					<l n="10" num="2.1">Là, les poules qu’on aperçoit</l>
					<l n="11" num="2.2">Ne pondent que des œufs de Pâques,</l>
					<l n="12" num="2.3">Et, quand on pêche dans les flaques,</l>
					<l n="13" num="2.4">On ramène au bout de son fil,</l>
					<l n="14" num="2.5">De ravissants poissons d’avril.</l>
				</lg>
				<lg n="3">
					<l n="15" num="3.1">Dans ce jardin, on se promène</l>
					<l n="16" num="3.2">En choisissant tout ce qu’on veut.</l>
					<l n="17" num="3.3">On a toujours la bouche pleine,</l>
					<l n="18" num="3.4">Et c’es§ toujours l’heure du jeu.</l>
					<l n="19" num="3.5">On y est toujours en vacances,</l>
					<l n="20" num="3.6">Car c’est dimanche tout le mois.</l>
				</lg>
				<lg n="4">
					<l n="21" num="4.1">On fait des balades immenses</l>
					<l n="22" num="4.2">Sur le dos des chevaux de bois.</l>
					<l n="23" num="4.3">Quand on croit qu’il vient des orages,</l>
					<l n="24" num="4.4">C’es qu’il va grêler des bonbons.</l>
					<l n="25" num="4.5">La nuit, on dort dans des nuages</l>
					<l n="26" num="4.6">Bien plus doux que des édredons,</l>
					<l n="27" num="4.7">Et quand on s’éveille à l’aurore</l>
					<l n="28" num="4.8">On se met à chanter en chœur :</l>
					<l n="29" num="4.9">« Encore ! Encore ! Encore ! Encore ! »</l>
					<l n="30" num="4.10">Tant on a de plaisir au cœur !</l>
				</lg>
				<lg n="5">
					<l n="31" num="5.1">On a le bon Dieu pour grand-père,</l>
					<l n="32" num="5.2">On sait tout sans jamais rien faire,</l>
					<l n="33" num="5.3">Et l’on s’amuse énormément.</l>
					<l n="34" num="5.4">Voilà la vie au firmament.</l>
				</lg>
				<lg n="6">
					<l n="35" num="6.1">‒ Dites, vous me croyez, j’espère ?</l>
				</lg>
			</div></body></text></TEI>