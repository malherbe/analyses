<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">HORIZONS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1794 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Horizons</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>BIBLIOTHÈQUE-CHARPENTIER, EUGÈNE FASQUELLE, ÉDITEUR</publisher>
							<date when="1905">1905</date>
						</imprint>
					</monogr>
					<note>Édition numérisée sur demande (BnF)</note>
				</biblStruct>
			</sourceDesc>
			</fileDesc>
		<profileDesc>
			<creation>
				<date when="1905">1905</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2023-06-24" who="RR">Quelques corrections d’erreurs de numérisation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LA MORT</head><div type="poem" key="DLR295">
					<head type="main">LE POÈME DE LA GUERRE DES VIVANTS ET DES MORTS</head>
					<lg n="1">
						<l n="1" num="1.1">Vivants chargés de chair et squelettes terreux</l>
						<l n="2" num="1.2">Se sont rués un jour les uns contre les autres</l>
						<l n="3" num="1.3">Au fond de ma pensée intime pleine d’eux,</l>
						<l n="4" num="1.4">Et j’entendais leurs cris de violents apôtres.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">Ces ennemis qui n’ont de pareil que les dents,</l>
						<l n="6" num="2.2">Se les montrant de près, cognaient une armature</l>
						<l n="7" num="2.3">D’os bruns où pend encore un peu de pourriture,</l>
						<l n="8" num="2.4">Contre la force enjeu des corps outrecuidants.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">La guerre piétinait le bord blessé des fosses,</l>
						<l n="10" num="3.2">Et la rage montait du fol rassemblement,</l>
						<l n="11" num="3.3">Et les têtes de mort ouvraient sauvagement</l>
						<l n="12" num="3.4">La vérité des trous sur les prunelles fausses.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">Et les vivants disaient : « Nous sommes la beauté !</l>
						<l n="14" num="4.2">» Nous mangeons la lumière et l’air. Voici nos joues !</l>
						<l n="15" num="4.3">» Nous bâillons et rions sur les hideuses moues</l>
						<l n="16" num="4.4">» Que vous faites, au fond de votre éternité ! »</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">Et les morts répondaient : « Mieux vaut notre grimace</l>
						<l n="18" num="5.2">» Que la vôtre ! Amenez ceux qui n’en peuvent plus.</l>
						<l n="19" num="5.3">» Voici vos mal tournés, vos tristes, vos vaincus,</l>
						<l n="20" num="5.4">» Et toutes nos dents rient de voir la vie qui passe ! »</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1">Les vivants disaient : « Tout plutôt que votre lit</l>
						<l n="22" num="6.2">» De silence ! Mieux vaut notre douleur qui crie ;</l>
						<l n="23" num="6.3">» Mieux vaut toute la chair malade que pourrie,</l>
						<l n="24" num="6.4">» Mieux vaut le désespoir lui-même que l’oubli. »</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1">Les morts disaient : « L’oubli n’est pas notre partage.</l>
						<l n="26" num="7.2">» Au fond de votre peur nous nous réfugions :</l>
						<l n="27" num="7.3">» Sans forme, sans couleur, sans paroles, sans âge,</l>
						<l n="28" num="7.4">» Nous sommes votre angoisse et vos religions.</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1">» Nous sommes le Passé, nous sommes Babylone,</l>
						<l n="30" num="8.2">» Nous sommes tout, Histoire et Fable et Souvenir. »</l>
						<l n="31" num="8.3">Et les vivants hurlaient : « Nous sommes la colonne</l>
						<l n="32" num="8.4">» Brûlante qui soutient le monde : l’Avenir ! »</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1">Les morts : « Nous sommes plus que l’avenir. Nous sommes</l>
						<l n="34" num="9.2">» La fin. Vous n’avez plus aucun pouvoir sur nous.</l>
						<l n="35" num="9.3">» Car nous avons été des femmes et des hommes :</l>
						<l n="36" num="9.4">» Nous savons ! Mais pour vous, vous doutez à genoux. »</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1">Et les vivants : « Gomment garderions-nous un doute ?</l>
						<l n="38" num="10.2">» Ne demeurez-vous pas inertes et couchés</l>
						<l n="39" num="10.3">» Quand nous sommes debout avec nos sept péchés,</l>
						<l n="40" num="10.4">» Nous, vivants, sur la route, et vous, morts, sous la route ?</l>
					</lg>
					<lg n="11">
						<l n="41" num="11.1">» Oui, vous êtes la fin, la terreur du trépas,</l>
						<l n="42" num="11.2">» L’inconcevable rêve et sa noire démence ;</l>
						<l n="43" num="11.3">» Mais parmi nous aussi, le regard qui commence</l>
						<l n="44" num="11.4">» Des nouveau-nés, est plein de ce qu’on ne sait pas.</l>
					</lg>
					<lg n="12">
						<l n="45" num="12.1">» Tout le mystère vit dans nos instincts perplexes.</l>
						<l n="46" num="12.2">» Mais vous, qu’avez-vous fait de l’orgueil, des ennuis,</l>
						<l n="47" num="12.3">» Des larmes sans raison au cœur des belles nuits,</l>
						<l n="48" num="12.4">» De la joie et du mal d’aimer ? Où sont vos sexes ?</l>
					</lg>
					<lg n="13">
						<l n="49" num="13.1">» L’âme est finie avec la sensualité…</l>
						<l n="50" num="13.2">» Rendormez-vous, vieux os des abstractions creuses ! »</l>
						<l n="51" num="13.3">Les morts disaient : « Pourtant c’est nous l’Éternité</l>
						<l n="52" num="13.4">» Dont vous parlez toujours aux heures amoureuses. »</l>
					</lg>
					<lg n="14">

						<l n="53" num="14.1">Et les vivants ont dit : « Et qu’importe l’horreur</l>
						<l n="54" num="14.2">» Au bout de tout chemin de vos mains assassines ’ !</l>
						<l n="55" num="14.3">» Nous marchons en tenant à la bouche une fleur. »</l>
						<l n="56" num="14.4">Les morts ont dit : « Et nous, nous mordons ses racines ! »</l>
					</lg>
					<lg n="15">
						<l n="57" num="15.1">Alors tous les vivants ont élevé les bras</l>
						<l n="58" num="15.2">Et follement crié ceci : « Vive la Vie ! »</l>
						<l n="59" num="15.3">Mais les morts ont clamé : « La vie est asservie</l>
						<l n="60" num="15.4">« A la mort. Sans la mort vous ne l’aimeriez pas. »</l>
					</lg>
				</div></body></text></TEI>