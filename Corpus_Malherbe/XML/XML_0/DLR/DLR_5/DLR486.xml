<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">PAR VENTS ET MARÉES</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>Delarue-Mardrus</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, OCR, Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1987 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2020</date>
				<idno type="local">DLR_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title type="main">PAR VENTS ET MARÉES</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>BIBLIOTHÈQUE-CHARPENTIER</publisher>
							<date when="1910">1910</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1910">1910</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les retraits introduits automatiquement ont été modifiés conformément à l’édition de référence.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-09-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-09-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LA MER</head><div type="poem" key="DLR486">
					<head type="main">CHANSONS DES BARQUES DE NUIT</head>
					<lg n="1">
						<l n="1" num="1.1">A la pêche toujours fidèles,</l>
						<l n="2" num="1.2">Sur la mer où plus rien ne luit,</l>
						<l n="3" num="1.3">Nous entrouvrons nos grandes ailes</l>
						<l n="4" num="1.4">Comme des papillons de nuit.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">Chacune de nous reste seule</l>
						<l n="6" num="2.2">Sur la tranquillité des flots,</l>
						<l n="7" num="2.3">Sans nul feu que le brûle-gueule</l>
						<l n="8" num="2.4">De quelqu’un de ses matelots.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">Dans l’immensité taciturne,</l>
						<l n="10" num="3.2">Loin des fanaux et des reflets,</l>
						<l n="11" num="3.3">Nous prenons le poisson nocturne</l>
						<l n="12" num="3.4">Dans nos invisibles filets.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">Mais dans le port qui brille et bouge,</l>
						<l n="14" num="4.2">Le phare, des barques prend soin,</l>
						<l n="15" num="4.3">Et sa lumière verte et rouge</l>
						<l n="16" num="4.4">Nous rassure toutes de loin.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">Car si l’heure vient, d’un coup d’aile</l>
						<l n="18" num="5.2">Chacun des papillons de nuit</l>
						<l n="19" num="5.3">Retourne à la bonne chandelle</l>
						<l n="20" num="5.4">Qui pour le ramener reluit.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1">La mer monte. Au large ! A la barque</l>
						<l n="22" num="6.2">La belle’pêche nous attend.</l>
						<l n="23" num="6.3">Déjà la»grande voile s’arque,</l>
						<l n="24" num="6.4">Le foc sur le beaupré se tend.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1">Au dur travail toujours fidèles,</l>
						<l n="26" num="7.2">Sur la mer où plus rien ne luit,</l>
						<l n="27" num="7.3">Ouvrons-nous de toutes nos ailes,</l>
						<l n="28" num="7.4">Comme des papillons de nuit !</l>
					</lg>
				</div></body></text></TEI>