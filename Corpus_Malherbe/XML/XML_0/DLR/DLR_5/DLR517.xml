<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">PAR VENTS ET MARÉES</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>Delarue-Mardrus</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, OCR, Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1987 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2020</date>
				<idno type="local">DLR_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title type="main">PAR VENTS ET MARÉES</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>BIBLIOTHÈQUE-CHARPENTIER</publisher>
							<date when="1910">1910</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1910">1910</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les retraits introduits automatiquement ont été modifiés conformément à l’édition de référence.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-09-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-09-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CINQ POÈMES POUR LA FRANCE</head><div type="poem" key="DLR517">
					<head type="number">I</head>
					<head type="main">INITIATION</head>
					<lg n="1">
						<l n="1" num="1.1">J’avais dit au revoir avec indifférence</l>
						<l n="2" num="1.2"><space unit="char" quantity="8"></space>À des horizons trop pareils.</l>
						<l n="3" num="1.3">Le loin nie faisait signe et j’ai vu ses soleils.</l>
						<l n="4" num="1.4"><space unit="char" quantity="8"></space>Et je n’aime plus que ma France.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">D’avoir tant soupiré vers ses feuillages frais,</l>
						<l n="6" num="2.2"><space unit="char" quantity="8"></space>Vers sa lumière tempérée,</l>
						<l n="7" num="2.3">Ma passion pour elle en mon cœur est entrée</l>
						<l n="8" num="2.4"><space unit="char" quantity="8"></space>En même temps que mes regrets.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">Ceux-là se guériront par le lointain voyage</l>
						<l n="10" num="3.2"><space unit="char" quantity="8"></space>Qui n’aiment pas leur horizon.</l>
						<l n="11" num="3.3">On en revient mûri de cœur et de raison,</l>
						<l n="12" num="3.4"><space unit="char" quantity="8"></space>Ayant un peu plus que son âge.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">Pars au loin ! Le soleil bande son plus grand arc,</l>
						<l n="14" num="4.2"><space unit="char" quantity="8"></space>’Le froid darde ses pointes sèches.</l>
						<l n="15" num="4.3">Alors, dans ton esprit criblé de tant de flèches,</l>
						<l n="16" num="4.4"><space unit="char" quantity="8"></space>Ta France s’offre comme un parc.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">Parc d’ombre et de repos. Sous le feu, sous la glace</l>
						<l n="18" num="5.2"><space unit="char" quantity="8"></space>Nous’ nous sentons d’un autre sang.</l>
						<l n="19" num="5.3">O langage natal, que doux est ton accent,</l>
						<l n="20" num="5.4"><space unit="char" quantity="8"></space>O France, notre unique place !</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1">Par la pensée et l’art et le cœur et la peau,</l>
						<l n="22" num="6.2"><space unit="char" quantity="8"></space>Je suis fille de ma patrie.</l>
						<l n="23" num="6.3">Puissé-je aimer toujours, d’une âme enfin mûrie,</l>
						<l n="24" num="6.4"><space unit="char" quantity="8"></space>Les trois couleurs de mon drapeau !</l>
					</lg>
				</div></body></text></TEI>