<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">PAR VENTS ET MARÉES</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>Delarue-Mardrus</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, OCR, Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1987 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2020</date>
				<idno type="local">DLR_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title type="main">PAR VENTS ET MARÉES</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>BIBLIOTHÈQUE-CHARPENTIER</publisher>
							<date when="1910">1910</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1910">1910</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les retraits introduits automatiquement ont été modifiés conformément à l’édition de référence.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-09-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-09-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CINQ POÈMES POUR LA FRANCE</head><div type="poem" key="DLR519">
					<head type="number">III</head>
					<head type="main">POUR LE DRAPEAU</head>
					<lg n="1">
						<l n="1" num="1.1">Bien, blanc, rouge au-dessus du flot des foules mornes,</l>
						<l n="2" num="1.2"><space unit="char" quantity="8"></space>C’est le drapeau ! C’est le drapeau !</l>
						<l n="3" num="1.3">Saluez ! C’est la liberté, mise en impôt,</l>
						<l n="4" num="1.4">C’est la routine sans pitié, ce sont les bornes !</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">Le régiment exact l’entoure, obéissant,</l>
						<l n="6" num="2.2"><space unit="char" quantity="8"></space>Comme un seul grand monstre qui rampe :</l>
						<l n="7" num="2.3">Tous ces hommes, autour de la fatale hampe,</l>
						<l n="8" num="2.4">Ont l’air d’avoir trempé leurs jambes dans le sang.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">Ils ne sont rien, sinon des centaines de mille,</l>
						<l n="10" num="3.2"><space unit="char" quantity="8"></space>Quand chacun d’eux était quelqu’un.</l>
						<l n="11" num="3.3">Soldats-, forçats sans crime et plèbe sans tribun,</l>
						<l n="12" num="3.4">La chiffe aux trois couleurs flotte et vous assimile !</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">Et vous suivez cela, gris de soumission,</l>
						<l n="14" num="4.2"><space unit="char" quantity="8"></space>Enfants de la misère humaine !</l>
						<l n="15" num="4.3">Vous suivez la musique en cuivre qui vous mène</l>
						<l n="16" num="4.4">Attaquer sans rancune, haïr sans passion ?</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">Vous passez ! Ma raison se dresse, furieuse,</l>
						<l n="18" num="5.2"><space unit="char" quantity="8"></space>Pour vous crier : « Ne suivez pas ! »</l>
						<l n="19" num="5.3">Et tout à coup mon cœur, au bruit sourd de vos pas,</l>
						<l n="20" num="5.4">Se tend et bat une chamade glorieuse.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1">— Tu n’es pas ma raison, mon cœur ! Cesse ! Obéis !</l>
						<l n="22" num="6.2"><space unit="char" quantity="8"></space>Arrête ces battements ivres !</l>
						<l n="23" num="6.3">Pourquoi dis-tu que ce drapeau c’est mon pays,</l>
						<l n="24" num="6.4">Sa couleur, son odeur, ses arts, ses chants, ses. livres ?</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1">Pourquoi dis-tu que-c’est ce que j’ai dans la peau ?</l>
						<l n="26" num="7.2"><space unit="char" quantity="8"></space>Je suis d’une plus vaste race !</l>
						<l n="27" num="7.3">Ma raison ! Ma raison ! A moi !… Le Drapeau passe</l>
						<l n="28" num="7.4">Et voici que j’envoie un baiser au drapeau !</l>
					</lg>
				</div></body></text></TEI>