<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">A MAMAN</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1250 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_7</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title type="main">A MAMAN</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LIBRAIRIE CHARPENTIER ET FASQUELLE</publisher>
							<date when="1920">1920</date>
						</imprint>
					</monogr>
					<note>
						Édition numérisée.
						Merci à la bibliothèque de l’université de Denver (USA)
						qui a bien voulu prêté un exemplaire de cet ouvrage ; aucune bibliothèque
						de France n’ayant accepté le prêt ou la reproduction sur place.
					</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1920">1920</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>.....</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-11-28" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">IV</head><head type="main_part">SOUVENIRS</head><div type="poem" key="DLR686">
					<head type="main">VERDURES</head>
					<div n="1" type="section">
						<head type="number">I</head>
						<lg n="1">
							<l n="1" num="1.1">Où fut mon âme avant que d’être faite chair ?</l>
							<l n="2" num="1.2">Je ne sais pas mon nom. Suis-je garçon ou fille ?</l>
							<l n="3" num="1.3">Deux ans… Trois ans… Je vois ! Un rond de soleil brille</l>
							<l n="4" num="1.4">Dans mon premier jardin qu’emplit toute la mer</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1">Il semble qu’en ce corps de deux ou trois années</l>
							<l n="6" num="2.2">Un grand passé subsiste, informe paradis.</l>
							<l n="7" num="2.3">Parmi le va et vient de mes cinq sœurs aînées,</l>
							<l n="8" num="2.4">La vie humaine monte à mes yeux agrandis</l>
						</lg>
						<lg n="3">
							<l n="9" num="3.1">La pelouse, dans l’ombre, est haute, décoiffée.</l>
							<l n="10" num="3.2">Tout ce que j’aperçois est de l’immensité.</l>
							<l n="11" num="3.3">Un peu du merveilleux <hi rend="ital">d’avant</hi> fleurit l’été.</l>
							<l n="12" num="3.4">Dans chaque rose dort une petite fée</l>
						</lg>
						<lg n="4">
							<l n="13" num="4.1">Il fait beau. Pourquoi donc cette angoisse ? Pourquoi ?</l>
							<l n="14" num="4.2">Cette fleur dans ma main, qu’est-ce qui me l’a mise ?</l>
							<l n="15" num="4.3">Je suis à cette place où mes sœurs m’ont assise.</l>
							<l n="16" num="4.4">Si grande sur l’allée, est-ce l’ombre du toit ?</l>
						</lg>
						<lg n="5">
							<l n="17" num="5.1">Je vais pleurer, ici, petite, si petite.</l>
							<l n="18" num="5.2">Ne va-t-on pas venir à mon secours, vraiment</l>
							<l n="19" num="5.3">Un pas… Deux bras tendus… Oh ! prends-moi, prends-moi vite,</l>
							<l n="20" num="5.4">Je te vois, je te sens… C’est toi, maman !… Maman !…</l>
						</lg>
						<lg n="6">
							<l n="21" num="6.1">Être sur tes genoux, oui, oui… De tout mon être</l>
							<l n="22" num="6.2">C’est cela, seulement cela que j’ai voulu.</l>
							<l n="23" num="6.3">Car tes genoux, maman, c’est encor l’absolu</l>
							<l n="24" num="6.4">Où l’on était si bien, hélas ! avant de naître…</l>
						</lg>
					</div>
					<div n="2" type="section">
						<head type="number">II</head>
						<lg n="1">
							<l n="25" num="1.1">Comme il fait chaud ! Je suis une petite fille.</l>
							<l n="26" num="1.2">Un tilleul vaste embaume au-dessus du gazon.</l>
							<l n="27" num="1.3">Les arbres sont chargés de ciel et d’horizon,</l>
							<l n="28" num="1.4">Leur grande ombre a des trous où le soleil scintille</l>
						</lg>
						<lg n="2">
							<l n="29" num="2.1">O beau temps ! La verdure est renversée en moi.</l>
							<l n="30" num="2.2">Je suis comme un miroir tremblant où tout remue,</l>
							<l n="31" num="2.3">Que le parc est profond ! Combien je suis émue !</l>
							<l n="32" num="2.4">Mais je n’ai que dix ans… J’ignore mon émoi</l>
						</lg>
						<lg n="3">
							<l n="33" num="3.1">Petit être angoissé que le soleil rassure,</l>
							<l n="34" num="3.2">Je vis ce jour, si long qu’il n’aura pas de nuit.</l>
							<l n="35" num="3.3">Mes sœurs ! Les jardiniers fauchent l’herbe, aujourd’hui !</l>
							<l n="36" num="3.4">Sentez-vous dans le vent la petite odeur sûre ?</l>
						</lg>
						<lg n="4">
							<l n="37" num="4.1">Maman, à la maison, coud en pensant à nous.</l>
							<l n="38" num="4.2">Nous savons qu’elle est là derrière les persiennes.</l>
							<l n="39" num="4.3">Dans l’ombre, son ouvrage est blanc sur ses genoux</l>
							<l n="40" num="4.4">Son silence est chargé de guêpes musiciennes</l>
						</lg>
						<lg n="5">
							<l n="41" num="5.1">Nous sentons, à travers les bonds que nous faisons</l>
							<l n="42" num="5.2">Dans cette herbe coupée à goût de pimprenelle,</l>
							<l n="43" num="5.3">Qu’elle est là, notre mère, et qu’elle est éternelle</l>
							<l n="44" num="5.4">Comme l’air, le soleil, le ciel et les saisons.</l>
						</lg>
						<lg n="6">
							<l n="45" num="6.1">Sur nos sommeils, réveils, études, folles danses,</l>
							<l n="46" num="6.2">A jamais sont ouverts, sous deux simples bandeaux,</l>
							<l n="47" num="6.3">A jamais sont ouverts ses yeux de trois nuances,</l>
							<l n="48" num="6.4">A jamais est penché son frêle petit dos</l>
						</lg>
						<lg n="7">
							<l n="49" num="7.1">Son âme est là, mêlée à la verte féerie,</l>
							<l n="50" num="7.2">Au milieu de nos jeux qui crient à pleins poumons</l>
							<l n="51" num="7.3">Et c’est si naturel, sa présence chérie,</l>
							<l n="52" num="7.4">Que nous ne savons pas même que nous l’aimons.</l>
						</lg>
					</div>
					<div n="3" type="section">
						<head type="number">III</head>
						<lg n="1">
							<l n="53" num="1.1"><space unit="char" quantity="8"></space>Me voici presque adolescente.</l>
							<l n="54" num="1.2">Je joue encor, rieuse et les cheveux au vent,</l>
							<l n="55" num="1.3">Avec, sombre déjà, cette âme effervescente</l>
							<l n="56" num="1.4"><space unit="char" quantity="8"></space>Qui va rêvant, rêvant, rêvant</l>
						</lg>
						<lg n="2">
							<l n="57" num="2.1"><space unit="char" quantity="8"></space>L’automne a des jaunes funèbres.</l>
							<l n="58" num="2.2">La mer aux cent couleurs y chante à pleine voix.</l>
							<l n="59" num="2.3">Toute la poésie attend dans mes vertèbres.</l>
							<l n="60" num="2.4"><space unit="char" quantity="8"></space>La chasse court à travers bois</l>
						</lg>
						<lg n="3">
							<l n="61" num="3.1"><space unit="char" quantity="8"></space>Enfant hanté qu’un rêve agite,</l>
							<l n="62" num="3.2">J’aime, à travers mes jeux, l’automne au désespoir.</l>
							<l n="63" num="3.3">Parmi mes grandes sœurs, au fond du vieux manoir,</l>
							<l n="64" num="3.4"><space unit="char" quantity="8"></space>Notre mère est toute petite</l>
						</lg>
						<lg n="4">
							<l n="65" num="4.1"><space unit="char" quantity="8"></space>Les yeux sauvagement au loin,</l>
							<l n="66" num="4.2">J’écoute les accents de la naissante lyre.</l>
							<l n="67" num="4.3">Notre mère… Mon cœur ne s’y attarde point,</l>
							<l n="68" num="4.4"><space unit="char" quantity="8"></space>Pas plus qu’à l’air que je respire</l>
						</lg>
						<lg n="5">
							<l n="69" num="5.1"><space unit="char" quantity="8"></space>Ses soins, son amour, sont les mets</l>
							<l n="70" num="5.2">Dont la table, toujours, fut amplement servie</l>
							<l n="71" num="5.3">Nécessaire et fatale, elle est, comme la vie,</l>
							<l n="72" num="5.4"><space unit="char" quantity="8"></space>Présente, et ne mourra jamais.</l>
						</lg>
					</div>
					<div n="4" type="section">
						<head type="number">IV</head>
						<lg n="1">
							<l n="73" num="1.1"><space unit="char" quantity="8"></space>C’est la jeune fille tragique</l>
							<l n="74" num="1.2"><space unit="char" quantity="8"></space>Qui hante sans cesse les flots.</l>
							<l n="75" num="1.3"><space unit="char" quantity="8"></space>Son cœur a d’immenses sanglots.</l>
							<l n="76" num="1.4"><space unit="char" quantity="8"></space>C’est la jeune fille tragique</l>
							<l n="77" num="1.5"><space unit="char" quantity="8"></space>Que mène un rêve nostalgique</l>
						</lg>
						<lg n="2">
							<l n="78" num="2.1"><space unit="char" quantity="8"></space>Elle va, parlant à la mer,</l>
							<l n="79" num="2.2"><space unit="char" quantity="8"></space>Cette fraternelle sirène.</l>
							<l n="80" num="2.3"><space unit="char" quantity="8"></space>Son âme n’est jamais sereine.</l>
							<l n="81" num="2.4"><space unit="char" quantity="8"></space>Elle va, parlant à la mer.</l>
							<l n="82" num="2.5"><space unit="char" quantity="8"></space>Elle est ivre de sel amer</l>
						</lg>
						<lg n="3">
							<l n="83" num="3.1"><space unit="char" quantity="8"></space>Elle les prévoit, les défaites,</l>
							<l n="84" num="3.2"><space unit="char" quantity="8"></space>— Oui, l’horreur de tout ce qu’on voit</l>
							<l n="85" num="3.3"><space unit="char" quantity="8"></space>Mais elle a vingt ans. Elle croit</l>
							<l n="86" num="3.4"><space unit="char" quantity="8"></space>Que les humains sont des poètes.</l>
							<l n="87" num="3.5"><space unit="char" quantity="8"></space>Elle les prévoit, les défaites</l>
						</lg>
						<lg n="4">
							<l n="88" num="4.1"><space unit="char" quantity="8"></space>Étrange, elle passe au lointain.</l>
							<l n="89" num="4.2"><space unit="char" quantity="8"></space>Son cou porte un collier de baies.</l>
							<l n="90" num="4.3"><space unit="char" quantity="8"></space>Elle est belle, le long des haies.</l>
							<l n="91" num="4.4"><space unit="char" quantity="8"></space>Étrange, elle passe au lointain,</l>
							<l n="92" num="4.5"><space unit="char" quantity="8"></space>Sœur du printemps et du matin</l>
						</lg>
						<lg n="5">
							<l n="93" num="5.1"><space unit="char" quantity="8"></space>Et toi, toujours dans la demeure,</l>
							<l n="94" num="5.2"><space unit="char" quantity="8"></space>Mère, tu vieillis pas à pas.</l>
							<l n="95" num="5.3"><space unit="char" quantity="8"></space>L’enfant ne s’en aperçoit pas.</l>
							<l n="96" num="5.4"><space unit="char" quantity="8"></space>Et toi, toujours dans la demeure,</l>
							<l n="97" num="5.5"><space unit="char" quantity="8"></space>Tu vis, encor bien loin de l’Heure</l>
						</lg>
						<lg n="6">
							<l n="98" num="6.1"><space unit="char" quantity="8"></space>— Le sait-elle, que tu vieillis,</l>
							<l n="99" num="6.2"><space unit="char" quantity="8"></space>Que doucement ta force s’use ?</l>
							<l n="100" num="6.3"><space unit="char" quantity="8"></space>Ta dernière enfant, cette muse</l>
							<l n="101" num="6.4"><space unit="char" quantity="8"></space>Le sait-elle, que tu vieillis,</l>
							<l n="102" num="6.5"><space unit="char" quantity="8"></space>Tout à ses rêves inouïs ?…</l>
						</lg>
						<lg n="7">
							<l n="103" num="7.1"><space unit="char" quantity="8"></space>Modeste, bonne, douce, intime,</l>
							<l n="104" num="7.2"><space unit="char" quantity="8"></space>Elle sait seulement de toi</l>
							<l n="105" num="7.3"><space unit="char" quantity="8"></space>Que c’est toi le foyer, le toit,</l>
							<l n="106" num="7.4"><space unit="char" quantity="8"></space>L’être bon, doux, modeste, intime</l>
							<l n="107" num="7.5"><space unit="char" quantity="8"></space>— Et que, cela, c’est légitime.</l>
						</lg>
					</div>
					<div n="5" type="section">
						<head type="number">V</head>
						<lg n="1">
							<l n="108" num="1.1">O jeunesse qui vas, de toi-même occupée !</l>
							<l n="109" num="1.2"><space unit="char" quantity="8"></space>Au jour venu, j’ai su ! J’ai su</l>
							<l n="110" num="1.3"><space unit="char" quantity="8"></space>Mon bonheur dans le parc moussu,</l>
							<l n="111" num="1.4"><space unit="char" quantity="8"></space>Quand la vie enfin m’a frappée</l>
						</lg>
						<lg n="2">
							<l n="112" num="2.1">Un certain soir, alors que j’ai pour vivre aussi,</l>
							<l n="113" num="2.2"><space unit="char" quantity="8"></space>Quitté ma maison, ma grisaille,</l>
							<l n="114" num="2.3"><space unit="char" quantity="8"></space>Quand j’ai connu cette heure-ci</l>
							<l n="115" num="2.4"><space unit="char" quantity="8"></space>Qui vous arrache les entrailles,</l>
						</lg>
						<lg n="3">
							<l n="116" num="3.1">Comme, ce certain soir, j’ai bien senti, maman,</l>
							<l n="117" num="3.2"><space unit="char" quantity="8"></space>Que ma chair venait de la tienne,</l>
							<l n="118" num="3.3"><space unit="char" quantity="8"></space>Et quelle tendresse ancienne</l>
							<l n="119" num="3.4"><space unit="char" quantity="8"></space>Me ligottait à toi, vraiment !</l>
						</lg>
						<lg n="4">
							<l n="120" num="4.1">Après, après, ce fut un immense silence</l>
							<l n="121" num="4.2"><space unit="char" quantity="8"></space>Mon cœur vivait ailleurs, ôté.</l>
							<l n="122" num="4.3"><space unit="char" quantity="8"></space>Maintenant c’est l’éternité,</l>
							<l n="123" num="4.4"><space unit="char" quantity="8"></space>Ce silence bien plus immense</l>
						</lg>
						<lg n="5">
							<l n="124" num="5.1">Moi qui ne disais rien, toi qui devinais tout,</l>
							<l n="125" num="5.2"><space unit="char" quantity="8"></space>C’était un miracle, il faut croire.</l>
							<l n="126" num="5.3"><space unit="char" quantity="8"></space>— Dire que, pour toi, mon histoire</l>
							<l n="127" num="5.4"><space unit="char" quantity="8"></space>N’aura pas été jusqu’au bout !</l>
						</lg>
					</div>
					<div n="6" type="section">
						<head type="number">VI</head>
						<lg n="1">
							<l n="128" num="1.1"><space unit="char" quantity="8"></space>C’est un parc frais et ténébreux</l>
							<l n="129" num="1.2"><space unit="char" quantity="8"></space>Sur des lointains couleur de perle.</l>
							<l n="130" num="1.3"><space unit="char" quantity="8"></space>La verdure a des dessous bleus</l>
							<l n="131" num="1.4"><space unit="char" quantity="8"></space>Où chante la gaieté d’un merle</l>
						</lg>
						<lg n="2">
							<l n="132" num="2.1"><space unit="char" quantity="8"></space>Ce cimetière printanier,</l>
							<l n="133" num="2.2"><space unit="char" quantity="8"></space>Pieusement j’y suis venue.</l>
							<l n="134" num="2.3"><space unit="char" quantity="8"></space>Des lilas sont dans l’avenue</l>
							<l n="135" num="2.4"><space unit="char" quantity="8"></space>Comme des fleurs hors d’un panier,</l>
						</lg>
						<lg n="3">
							<l n="136" num="3.1"><space unit="char" quantity="8"></space>Les tombeaux blancs, dans l’ombre verte,</l>
							<l n="137" num="3.2"><space unit="char" quantity="8"></space>En ce lieu du parfait sommeil,</l>
							<l n="138" num="3.3"><space unit="char" quantity="8"></space>Tremblent de taches de soleil.</l>
							<l n="139" num="3.4"><space unit="char" quantity="8"></space>Le mois de mai triomphe, certe !</l>
						</lg>
						<lg n="4">
							<l n="140" num="4.1"><space unit="char" quantity="8"></space>Parmi les sépulcres je viens</l>
							<l n="141" num="4.2"><space unit="char" quantity="8"></space>Saluer celui de ma mère.</l>
							<l n="142" num="4.3"><space unit="char" quantity="8"></space>Et, le front bas, je me souviens</l>
							<l n="143" num="4.4"><space unit="char" quantity="8"></space>Avec une tendresse amère.</l>
						</lg>
					</div>
				</div></body></text></TEI>