<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">A MAMAN</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1250 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_7</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title type="main">A MAMAN</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LIBRAIRIE CHARPENTIER ET FASQUELLE</publisher>
							<date when="1920">1920</date>
						</imprint>
					</monogr>
					<note>
						Édition numérisée.
						Merci à la bibliothèque de l’université de Denver (USA)
						qui a bien voulu prêté un exemplaire de cet ouvrage ; aucune bibliothèque
						de France n’ayant accepté le prêt ou la reproduction sur place.
					</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1920">1920</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>.....</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-11-28" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">III</head><head type="main_part">EN DEUIL</head><div type="poem" key="DLR673">
					<head type="main">EN DEUIL</head>
					<lg n="1">
						<l n="1" num="1.1">Le cri horrible de ma vie,</l>
						<l n="2" num="1.2">Depuis le temps que je le tais,</l>
						<l n="3" num="1.3">Le voici donc : moi, j’apportais,</l>
						<l n="4" num="1.4">Grande vivante inassouvie,</l>
						<l n="5" num="1.5">Toute ma merveille à la vie</l>
					</lg>
					<lg n="2">
						<l n="6" num="2.1">Quel fut mon destin enchanté ?</l>
						<l n="7" num="2.2">Ma jeunesse fut-elle heureuse ?</l>
						<l n="8" num="2.3">On dit : célèbre… glorieuse…</l>
						<l n="9" num="2.4">La gloire ? Ah ! quelle saleté !</l>
						<l n="10" num="2.5"><hi rend="ital">Haro</hi> sur tout être enchanté !</l>
					</lg>
					<lg n="3">
						<l n="11" num="3.1">Les dires de leur hérésie</l>
						<l n="12" num="3.2">Ont fait souvent rougir mon front,</l>
						<l n="13" num="3.3">Mes yeux pleurer. Horreur ! ils ont</l>
						<l n="14" num="3.4">Interprété ma poésie !</l>
						<l n="15" num="3.5">Oh ! leur venimeuse hérésie !</l>
					</lg>
					<lg n="4">
						<l n="16" num="4.1">J’avais mon secret en dedans,</l>
						<l n="17" num="4.2">Interne réponse à leur crime,</l>
						<l n="18" num="4.3">Ma solitaire vie intime,</l>
						<l n="19" num="4.4">Ma chère enfance aux belles dents,</l>
						<l n="20" num="4.5">Tous mes souvenirs en dedans</l>
					</lg>
					<lg n="5">
						<l n="21" num="5.1">Et voici ce tournant de page.</l>
						<l n="22" num="5.2">Une morte a pris mon trésor.</l>
						<l n="23" num="5.3">Mon enfance est frappée à mort,</l>
						<l n="24" num="5.4">Et, subitement, j’ai mon âge.</l>
						<l n="25" num="5.5">— Que seule à ce tournant de page !</l>
					</lg>
					<lg n="6">
						<l n="26" num="6.1">Mon âge !… Tout mon cœur dit :Non !</l>
						<l n="27" num="6.2">Pour aller du côté de l’ombre,</l>
						<l n="28" num="6.3">Pas d’enfant, pas de compagnon ;</l>
						<l n="29" num="6.4">Moi-même, camarade sombre</l>
						<l n="30" num="6.5">Aux bonheurs d’ici disant : Non !</l>
					</lg>
					<lg n="7">
						<l n="31" num="7.1">Ainsi l’heure est enfin venue</l>
						<l n="32" num="7.2">D’être moins jeune que toujours ;</l>
						<l n="33" num="7.3">Connue, hélas ! et méconnue,</l>
						<l n="34" num="7.4">Je dois voir s’assombrir mes jours,</l>
						<l n="35" num="7.5">Et cette mort est survenue !</l>
					</lg>
					<lg n="8">
						<l n="36" num="8.1">Mon nom reste à jamais public,</l>
						<l n="37" num="8.2">M’ôtant le charme d’être seule.</l>
						<l n="38" num="8.3">La foule aux yeux de basilic</l>
						<l n="39" num="8.4">A mis à mon cou cette meule,</l>
						<l n="40" num="8.5">Mon nom, mon pesant nom public</l>
					</lg>
					<lg n="9">
						<l n="41" num="9.1">L’ironie où plonge ma vie</l>
						<l n="42" num="9.2">Me fait sourire amèrement.</l>
						<l n="43" num="9.3">Tandis que ma route dévie,</l>
						<l n="44" num="9.4">Tandis que j’appelle maman,</l>
						<l n="45" num="9.5">On ricane autour de ma vie</l>
					</lg>
					<lg n="10">
						<l n="46" num="10.1">Tout mon être désespéré</l>
						<l n="47" num="10.2">Demande à reposer sous terre</l>
						<l n="48" num="10.3">Je veux aller où j’oublierai,</l>
						<l n="49" num="10.4">Je veux aller avec ma mère.</l>
						<l n="50" num="10.5">Mon cœur est trop désespéré</l>
					</lg>
					<lg n="11">
						<l n="51" num="11.1">Je veux retourner auprès d’elle,</l>
						<l n="52" num="11.2">Comme elle avoir froid à jamais.</l>
						<l n="53" num="11.3">Je veux sa ténèbre éternelle,</l>
						<l n="54" num="11.4">Je veux son néant. Je l’aimais,</l>
						<l n="55" num="11.5">Je veux retourner auprès d’elle</l>
					</lg>
					<lg n="12">
						<l n="56" num="12.1">Je veux tout quitter pour la mort,</l>
						<l n="57" num="12.2">Tout quitter, me quitter moi-même.</l>
						<l n="58" num="12.3">Je crie et pleure sur le bord</l>
						<l n="59" num="12.4">De cette profondeur suprême ;</l>
						<l n="60" num="12.5">La mort, la mort, je veux la mort !</l>
					</lg>
				</div></body></text></TEI>