<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">A MAMAN</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1250 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_7</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title type="main">A MAMAN</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LIBRAIRIE CHARPENTIER ET FASQUELLE</publisher>
							<date when="1920">1920</date>
						</imprint>
					</monogr>
					<note>
						Édition numérisée.
						Merci à la bibliothèque de l’université de Denver (USA)
						qui a bien voulu prêté un exemplaire de cet ouvrage ; aucune bibliothèque
						de France n’ayant accepté le prêt ou la reproduction sur place.
					</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1920">1920</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>.....</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-11-28" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">II</head><head type="main_part">DE PROFUNDIS</head><div type="poem" key="DLR667">
					<head type="main">MORTE…</head>
					<lg n="1">
						<l n="1" num="1.1">… Morte… La souffrance abattue.</l>
						<l n="2" num="1.2">Le calme en elle pour toujours.</l>
						<l n="3" num="1.3">Sur son lit elle va, deux jours,</l>
						<l n="4" num="1.4">Devenir sa propre statue</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">Longue idole, dévotions</l>
						<l n="6" num="2.2">Vers les mains de cire, la tête</l>
						<l n="7" num="2.3">D’ivoire ; murmure de fête</l>
						<l n="8" num="2.4">Et de félicitations</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">Quoi ?… Quelle épouvante physique ?</l>
						<l n="10" num="3.2">— Voici la mort, vite des fleurs !</l>
						<l n="11" num="3.3">Pour ce visage sans couleurs,</l>
						<l n="12" num="3.4">Cierges, bouquets, encens, musique !</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">Venez tous la voir ! C’est maman !</l>
						<l n="14" num="4.2">Très précieuse et bien coiffée</l>
						<l n="15" num="4.3">Elle sourit, car la mort-fée</l>
						<l n="16" num="4.4">L’a faite Belle au Bois Dormant</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">Pendant deux longs jours, autour d’elle</l>
						<l n="18" num="5.2">On est venu pour la fêter.</l>
						<l n="19" num="5.3">Elle était si coquette et belle,</l>
						<l n="20" num="5.4">Que j’en concevais vanité</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1">J’oubliais comme elle était morte,</l>
						<l n="22" num="6.2">Avec quels yeux, toute l’horreur.</l>
						<l n="23" num="6.3">Je lui souriais… — Oh ! stupeur</l>
						<l n="24" num="6.4">Du cercueil debout à la porte !</l>
					</lg>
				</div></body></text></TEI>