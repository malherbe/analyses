<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">A MAMAN</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1250 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_7</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title type="main">A MAMAN</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LIBRAIRIE CHARPENTIER ET FASQUELLE</publisher>
							<date when="1920">1920</date>
						</imprint>
					</monogr>
					<note>
						Édition numérisée.
						Merci à la bibliothèque de l’université de Denver (USA)
						qui a bien voulu prêté un exemplaire de cet ouvrage ; aucune bibliothèque
						de France n’ayant accepté le prêt ou la reproduction sur place.
					</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1920">1920</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>.....</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-11-28" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">III</head><head type="main_part">EN DEUIL</head><div type="poem" key="DLR676">
					<head type="main">ILS DISENT…</head>
					<lg n="1">
						<l n="1" num="1.1">Ils disent : « Tout le monde, après tout, perd sa mère</l>
						<l n="2" num="1.2"><space unit="char" quantity="8"></space>Ce n’est pas extraordinaire. »</l>
						<l n="3" num="1.3">Et, devant le regard de leur banalité,</l>
						<l n="4" num="1.4"><space unit="char" quantity="8"></space>On songe : « Oui !… Formalité. »</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">Ainsi donc il n’est pas d’éternelle épouvante</l>
						<l n="6" num="2.2"><space unit="char" quantity="8"></space>Au fond de votre âme vivante ?</l>
						<l n="7" num="2.3">Vos mères, lorsque l’âge a raidi leurs genoux,</l>
						<l n="8" num="2.4"><space unit="char" quantity="8"></space>Sont mortes aussi devant vous ?</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">Ce n’est sans doute pas pour nous la même chose</l>
						<l n="10" num="3.2"><space unit="char" quantity="8"></space>— Frêle, humble et jolie, une rose,</l>
						<l n="11" num="3.3">Rose toujours cachée et toujours parfumant,</l>
						<l n="12" num="3.4"><space unit="char" quantity="8"></space>Voilà ce qu’elle était, maman</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">Rougissante d’avoir en elle tous les doutes,</l>
						<l n="14" num="4.2"><space unit="char" quantity="8"></space>Elle était notre enfant à toutes,</l>
						<l n="15" num="4.3">Et, chacune de nous, l’aimant à sa façon,</l>
						<l n="16" num="4.4"><space unit="char" quantity="8"></space>Lui faisait, je crois, la leçon</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">Si petite au milieu de tant de grandes filles</l>
						<l n="18" num="5.2"><space unit="char" quantity="8"></space>Qui recommençaient des familles,</l>
						<l n="19" num="5.3">On n’aura jamais su le fond mystérieux</l>
						<l n="20" num="5.4"><space unit="char" quantity="8"></space>De son regard aux larges yeux</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1">Intimidée et douce, un peu froide, humble et bonne,</l>
						<l n="22" num="6.2"><space unit="char" quantity="8"></space>Était-ce une grande personne ?</l>
						<l n="23" num="6.3">Quant à moi, je sais bien comme mon cœur se fend,</l>
						<l n="24" num="6.4"><space unit="char" quantity="8"></space>Et que j’ai perdu mon enfant.</l>
					</lg>
				</div></body></text></TEI>