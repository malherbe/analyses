<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">A MAMAN</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1250 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_7</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title type="main">A MAMAN</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LIBRAIRIE CHARPENTIER ET FASQUELLE</publisher>
							<date when="1920">1920</date>
						</imprint>
					</monogr>
					<note>
						Édition numérisée.
						Merci à la bibliothèque de l’université de Denver (USA)
						qui a bien voulu prêté un exemplaire de cet ouvrage ; aucune bibliothèque
						de France n’ayant accepté le prêt ou la reproduction sur place.
					</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1920">1920</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>.....</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-11-28" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">VII</head><head type="main_part">A LA MORT</head><div type="poem" key="DLR701">
					<head type="main">A LA MORT</head>
					<lg n="1">
						<l n="1" num="1.1">Grande mort sans merci, fatale, égalitaire,</l>
						<l n="2" num="1.2">Toi que, toute la vie, émus, nous côtoyons,</l>
						<l n="3" num="1.3"><space unit="char" quantity="8"></space>Qui nous enfonces dans la terre</l>
						<l n="4" num="1.4">Et que nous voulons croire un foyer de rayons,</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">Où, survivants d’un jour, nous conduisons les nôtres</l>
						<l n="6" num="2.2">En pleurant derrière eux tristement, tristement,</l>
						<l n="7" num="2.3"><space unit="char" quantity="8"></space>Comme si les uns, et les autres</l>
						<l n="8" num="2.4">Nous n’étions pas pareils épouvantablement,</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">Mort, horizon borné des humains, mauvaise heure</l>
						<l n="10" num="3.2">Drame atroce et banal par où l’on doit passer,</l>
						<l n="11" num="3.3"><space unit="char" quantity="8"></space>Mort, parmi tout ce qui nous leurre,</l>
						<l n="12" num="3.4">Seul avenir certain auquel on peut penser,</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">Charnier commun, vil abattoir, mort animale</l>
						<l n="14" num="4.2">Que notre pauvre espoir a nommée au-delà,</l>
						<l n="15" num="4.3"><space unit="char" quantity="8"></space>Qui feras un visage pâle,</l>
						<l n="16" num="4.4">Une énigme sans fin du rustaud que voilà,</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">Au bout de la pensée humaine impasse noire</l>
						<l n="18" num="5.2">Où la nature a mis l’inflexible veto,</l>
						<l n="19" num="5.3"><space unit="char" quantity="8"></space>Mort qui nous couches tard ou tôt,</l>
						<l n="20" num="5.4">Ne laissant pour veiller sur nous que la Mémoire,</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1">O mort sans souffle, mort où nous aurons si froid</l>
						<l n="22" num="6.2">Après tant de tiédeur vivante qui respire,</l>
						<l n="23" num="6.3"><space unit="char" quantity="8"></space>Lieu de délivrance et d’effroi</l>
						<l n="24" num="6.4">Qui semble tour à tour le meilleur et le pire,</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1">Puisque, nous préparant à n’être que lambeaux,</l>
						<l n="26" num="7.2">Et marchant pas à pas vers ton grand cimetière,</l>
						<l n="27" num="7.3"><space unit="char" quantity="8"></space>Nous pouvons, nous, futurs tombeaux,</l>
						<l n="28" num="7.4">L’honorer, assassin que hait notre matière,</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1">Mort guetteuse qui vas nous arrêter le cœur,</l>
						<l n="30" num="8.2">Puisqu’amour, lyrisme, art, bonté, beauté, beaux zèles,</l>
						<l n="31" num="8.3"><space unit="char" quantity="8"></space>Tout ce qui nous donne des ailes</l>
						<l n="32" num="8.4">Chante quand même et vibre et prie et n’a pas peur,</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1">Puisque, sachant sa fin, l’homme sans cesse crée</l>
						<l n="34" num="9.2">Sans écouter au loin les appels de ton glas,</l>
						<l n="35" num="9.3"><space unit="char" quantity="8"></space>Puisque l’au-delà d’ici-bas</l>
						<l n="36" num="9.4">Est l’immortel témoin d’une race sacrée</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1">O mort ! Quoi que tu sois, survivance ou néant,</l>
						<l n="38" num="10.2">Voici notre œuvre : amour, beauté, souffrance ou joie.</l>
						<l n="39" num="10.3"><space unit="char" quantity="8"></space>Tu peux ouvrir ton trou béant,</l>
						<l n="40" num="10.4">Les élus d’entre nous ne seront pas ta proie !</l>
					</lg>
				</div></body></text></TEI>