<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">A MAMAN</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1250 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_7</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title type="main">A MAMAN</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LIBRAIRIE CHARPENTIER ET FASQUELLE</publisher>
							<date when="1920">1920</date>
						</imprint>
					</monogr>
					<note>
						Édition numérisée.
						Merci à la bibliothèque de l’université de Denver (USA)
						qui a bien voulu prêté un exemplaire de cet ouvrage ; aucune bibliothèque
						de France n’ayant accepté le prêt ou la reproduction sur place.
					</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1920">1920</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>.....</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-11-28" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">VI</head><head type="main_part">MÉDITATIONS</head><div type="poem" key="DLR700">
					<head type="main">AU-DELÀ</head>
					<lg n="1">
						<l n="1" num="1.1">L’au-delà, l’au-delà dont on parle sans cesse,</l>
						<l n="2" num="1.2"><space unit="char" quantity="8"></space>Est derrière nous, non devant.</l>
						<l n="3" num="1.3">Ton au-delà, maman, c’est l’ancienne jeunesse,</l>
						<l n="4" num="1.4">Lorsque tu respirais dans le monde vivant</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">Ton au-delà, ce sont nos souvenirs d’enfance,</l>
						<l n="6" num="2.2"><space unit="char" quantity="8"></space>Ta bonté, tes soins, tes grands yeux.</l>
						<l n="7" num="2.3">Ce sont les chers passés auxquels chacune pense</l>
						<l n="8" num="2.4">Et qui font si charmant le grand deuil douloureux</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">Ton au-delà, ce sont des jardins, des campagnes,</l>
						<l n="10" num="3.2"><space unit="char" quantity="8"></space>Ce sont nos maisons dans les bois,</l>
						<l n="11" num="3.3">Paris aussi, l’hiver. C’est le frais autrefois,</l>
						<l n="12" num="3.4">Alors que nous étions tes petites compagnes</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">Ton au-delà, ce sont tes gestes familiers</l>
						<l n="14" num="4.2"><space unit="char" quantity="8"></space>Tels qu’ils sont restés dans notre âme.</l>
						<l n="15" num="4.3">De cette jeune mère à cette vieille dame,</l>
						<l n="16" num="4.4">Ce sont tous tes instants par milliers et milliers</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">Ton au-delà, c’est toi vivante. Ce n’est pas</l>
						<l n="18" num="5.2"><space unit="char" quantity="8"></space>La mort, ce néant qu’on ignore.</l>
						<l n="19" num="5.3">Ton au-delà, maman, c’est ce qu’on aime encore.</l>
						<l n="20" num="5.4">Ton au-delà, c’est toi. Ce n’est pas ton trépas.</l>
					</lg>
				</div></body></text></TEI>