<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">FERVEUR</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2031 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Ferveur</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">http://www.poesies.net/luciedelaruemardrueferveur.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Ferveur</title>
						<author>Lucie Delarue-Mardrus</author>
						<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k9691679h?rk=21459;2</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LA REVUE BLANCHE</publisher>
							<date when="1902">1902</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1902">1902</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2023-06-24" who="RR">Quelques corrections d’erreurs de numérisation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">DANS LA VIE</head><div type="poem" key="DLR235">
					<head type="main">DANS LA VIE</head>
					<lg n="1">
						<l n="1" num="1.1">La soif qui conduisait ton destin anxieux</l>
						<l n="2" num="1.2">L’a guidé pas à pas vers les sources cachées ;</l>
						<l n="3" num="1.3">Tous tes passés sont morts comme des fleurs fauchées,</l>
						<l n="4" num="1.4">Au tournant où des yeux ont rencontré tes yeux.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">Et tu ris aujourd’hui, droite parmi tes traînes,</l>
						<l n="6" num="2.2">De mener au plaisir ce faste de paon blanc,</l>
					</lg>
					<lg n="3">
						<l n="7" num="3.1">Tandis qu’une pensée au front, un cœur au flanc,</l>
						<l n="8" num="3.2">Tu sens brûles an toi l’huile des lampes pleines.</l>
					</lg>
					<lg n="4">
						<l n="9" num="4.1">Avant les battements funèbres de la fin,</l>
						<l n="10" num="4.2">Tu songes aux bonheurs qui t’auront assouvie,</l>
						<l n="11" num="4.3">Tu cours vers l’avenir, haletante de vie,</l>
						<l n="12" num="4.4">Comme un enfant joueur bondit dans le jardin.</l>
					</lg>
					<lg n="5">
						<l n="13" num="5.1">Sous son calme château, ta coiffure dressée</l>
						<l n="14" num="5.2">Cache l’abstraction grave de ton esprit</l>
						<l n="15" num="5.3">Que les heures sans joie ont lentement mûri</l>
						<l n="16" num="5.4">Et fait plus savoureux qu’une pêche blessée.</l>
					</lg>
					<lg n="6">
						<l n="17" num="6.1">Mais si, pensive encor d’avoir longtemps souffert,</l>
						<l n="18" num="6.2">Tu te complais penchée aux profondeurs des causes,</l>
						<l n="19" num="6.3">L’Été sait dominer ton rêve de ses roses</l>
						<l n="20" num="6.4">Lourdes de leur fraîcheur mariée à ta chair.</l>
					</lg>
					<lg n="7">
						<l n="21" num="7.1">Tu vas, portant ta tête ainsi qu’une corolle,</l>
						<l n="22" num="7.2">Ou la livrant au creux fatigué des coussins ;</l>
						<l n="23" num="7.3">Ton tressaillant vouloir forme tous les desseins</l>
						<l n="24" num="7.4">Et ta bouche ivre au vent jette toute parole…</l>
					</lg>
					<lg n="8">
						<l n="25" num="8.1">Va ! tends tes bras à tout pour que tout soit ton bien !</l>
						<l n="26" num="8.2">Hante, après la campagne aux heures parfumées,</l>
						<l n="27" num="8.3">La Ville trépidante et ses nuits allumées :</l>
						<l n="28" num="8.4">Que ton cœur soit solitaire et soit saturnien.</l>
					</lg>
					<lg n="9">
						<l n="29" num="9.1">Et parfois, comme on monte un monument de cartes,</l>
						<l n="30" num="9.2">Que ta pitié construise un temple de Bonté</l>
						<l n="31" num="9.3">Avec l’horreur au loin de cette humanité</l>
						<l n="32" num="9.4">Que ne peuvent changer nos décrets et nos chartes.</l>
					</lg>
					<lg n="10">
						<l n="33" num="10.1">Conserve au fond de toi, tel un noyau, l’orgueil</l>
						<l n="34" num="10.2">De hautement penser près de celui qui t’aime ;</l>
						<l n="35" num="10.3">Laisse vivre, dardée au plus noir de toi-même,</l>
						<l n="36" num="10.4">Ta conscience ouverte et fixe comme un œil,</l>
					</lg>
					<lg n="11">
						<l n="37" num="11.1">‒ Sachant bien que pour nous rien n’est plus nécessaire</l>
						<l n="38" num="11.2">Que d’aimer <choice hand="RR" reason="analysis" type="false_verse"><sic rend="hidden">toutes choses</sic><corr source="none">toute chose</corr></choice> avec des cœurs charnels</l>
						<l n="39" num="11.3">Où chante, malgré tout, l’espoir d’être éternels,</l>
						<l n="40" num="11.4">Comme une inconsciente et sublime prière.</l>
					</lg>
				</div></body></text></TEI>