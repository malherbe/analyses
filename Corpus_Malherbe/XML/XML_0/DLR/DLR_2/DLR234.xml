<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">FERVEUR</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2031 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Ferveur</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">http://www.poesies.net/luciedelaruemardrueferveur.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Ferveur</title>
						<author>Lucie Delarue-Mardrus</author>
						<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k9691679h?rk=21459;2</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LA REVUE BLANCHE</publisher>
							<date when="1902">1902</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1902">1902</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2023-06-24" who="RR">Quelques corrections d’erreurs de numérisation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">REGARDS</head><div type="poem" key="DLR234">
					<head type="main">MARCHE NORMANDE</head>
					<lg n="1">
						<l n="1" num="1.1">Hors le présent heureux dont mon cœur est épris,</l>
						<l n="2" num="1.2">Lorsque je vois tomber les couchants équivoques</l>
						<l n="3" num="1.3">Dans la bénignité de ton fleuve, ô Paris !</l>
					</lg>
					<lg n="2">
						<l n="4" num="2.1">Il se réveille en moi, ‒ grouillants d’ours et de phoques, ‒</l>
						<l n="5" num="2.2">D’agressifs, ancestraux et durs septentrions</l>
						<l n="6" num="2.3">Et des barques blessant la Seine de leurs coques.</l>
					</lg>
					<lg n="3">
						<l n="7" num="3.1">Et je crie en mon cœur filial, nous crions</l>
						<l n="8" num="3.2">Vers tes mille quartiers, tes palais et tes arches,</l>
						<l n="9" num="3.3">Et préparons nos poings chargés de horions.</l>
					</lg>
					<lg n="4">
						<l n="10" num="4.1">Le vent où chantent clair nos gutturales marches</l>
						<l n="11" num="4.2">Hérisse sur nos caps nos cheveux courts et roux,</l>
						<l n="12" num="4.3">Et nous espérons fort ensanglante tes marches,</l>
					</lg>
					<lg n="5">
						<l n="13" num="5.1">Étant d’un terroir plein de ronces et de houx,</l>
						<l n="14" num="5.2">Où saignent largement les aubes boréales</l>
						<l n="15" num="5.3">Et dont les hommes sont brutaux comme des loups.</l>
					</lg>
					<lg n="6">
						<l n="16" num="6.1">Et, si nous n’avons pas la dorure des hâles</l>
						<l n="17" num="6.2">Qu’on prend à la cuisson du soleil des Midis,</l>
						<l n="18" num="6.3">Des volontés de fer crispent nos faces pâles ;</l>
					</lg>
					<lg n="7">
						<l n="19" num="7.1">C’est pourquoi tu mettras entre nos doigts hardis</l>
						<l n="20" num="7.2">La rançon qui fera retourner notre horde</l>
						<l n="21" num="7.3">A ses pays, croyance et rude paradis,</l>
					</lg>
					<lg n="8">
						<l n="22" num="8.1">Car si nous t’admirons, ville qu’un fleuve borde,</l>
						<l n="23" num="8.2">Nous préférons encore à tes lourdes splendeurs,</l>
						<l n="24" num="8.3">Contents de son horreur et que son froid nous morde,</l>
					</lg>
					<lg n="9">
						<l n="25" num="9.1">Notre neige fatale aux barbares blancheurs !</l>
					</lg>
				</div></body></text></TEI>