<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">FERVEUR</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2031 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Ferveur</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">http://www.poesies.net/luciedelaruemardrueferveur.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Ferveur</title>
						<author>Lucie Delarue-Mardrus</author>
						<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k9691679h?rk=21459;2</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LA REVUE BLANCHE</publisher>
							<date when="1902">1902</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1902">1902</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2023-06-24" who="RR">Quelques corrections d’erreurs de numérisation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">POÈMES TERRESTRES</head><div type="poem" key="DLR138">
					<head type="main">RÉVEIL</head>
					<lg n="1">
						<l n="1" num="1.1">Que l’engourdissement des choses et des êtres</l>
						<l n="2" num="1.2">Tressaille d’un frisson précurseur de réveil !</l>
						<l n="3" num="1.3">Sous chaque porte brûle un filet de soleil,</l>
						<l n="4" num="1.4">Le beau ciel bleu de mars entre par les fenêtres.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">Allons avec nos doigts tachés d’encre au jardin,</l>
						<l n="6" num="2.2">Mêler nos cœurs troublés à la terre inquiète ;</l>
						<l n="7" num="2.3">Si l’air y est resté sans parfum, dès de main</l>
						<l n="8" num="2.4">Tout le printemps tiendra dans une violette.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">Les gazons n’ont encor de fleurs ni de bourdons,</l>
						<l n="10" num="3.2">Mais de l’herbe est poussée entre les pierres sèches,</l>
						<l n="11" num="3.3">Et, tendrement pliés, quelques cotylédons</l>
						<l n="12" num="3.4">Crèvent le sol épais avec leurs têtes fraîches.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">Déjà chaque bourgeon goudronné s’est ouvert ;</l>
						<l n="14" num="4.2">Un sourd travail émeut le plus dur épiderme ;</l>
						<l n="15" num="4.3">Les vieux marrons tombés risquent un mince germe</l>
						<l n="16" num="4.4">Plein de précaution et rampant comme un vers.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">Je songe sur la pierre où je me suis assise ;</l>
						<l n="18" num="5.2">Le Printemps est miré dans mes yeux matinaux ;</l>
						<l n="19" num="5.3">Autour de mon repos, la saison indécise</l>
						<l n="20" num="5.4">Fait de tous les côtés piailler les oiseaux.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1">Le beau temps délicat chauffe ma gorge nue</l>
						<l n="22" num="6.2">Où repose ma voix, douce comme un pigeon.</l>
						<l n="23" num="6.3">Je sens avec mon cœur, au fond de l’étendue,</l>
						<l n="24" num="6.4">Le pauvre cœur humain claquer comme un bourgeon…</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1">Hélas !… l’air déjà tiède où le printemps progresse,</l>
						<l n="26" num="7.2">Où les sens sont surpris d’un premier abandon,</l>
						<l n="27" num="7.3">N’aura-t-il pas un peu de paix et de pardon</l>
						<l n="28" num="7.4">Pour tout ce qui sanglote au monde de détresse ?…</l>
					</lg>
				</div></body></text></TEI>