<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">FERVEUR</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2031 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Ferveur</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">http://www.poesies.net/luciedelaruemardrueferveur.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Ferveur</title>
						<author>Lucie Delarue-Mardrus</author>
						<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k9691679h?rk=21459;2</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LA REVUE BLANCHE</publisher>
							<date when="1902">1902</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1902">1902</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2023-06-24" who="RR">Quelques corrections d’erreurs de numérisation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">DÉCLARATIONS</head><div type="poem" key="DLR214">
					<head type="main">LE FLEUVE</head>
					<lg n="1">
						<l n="1" num="1.1">Notre amour grave et pur a vogué sur le fleuve</l>
						<l n="2" num="1.2">Et la main dans la main par des soirs descendus,</l>
						<l n="3" num="1.3">Au lent rythme de l’eau, colère que la meuve</l>
						<l n="4" num="1.4">Le labour des bateaux aux avants suraigus.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">Nos yeux ont contemplé l’eau sombre, approfondie</l>
						<l n="6" num="2.2">Par des reflets tordant leurs durables éclairs,</l>
						<l n="7" num="2.3">Et les horizons pleins de l’âpre tragédie</l>
						<l n="8" num="2.4">Des couchers empourprant les villes et les mers.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">Et la cité sur l’eau penchait, surabondante,</l>
						<l n="10" num="3.2">Ses quartiers noirs grouillants de labeur et d’horreur</l>
						<l n="11" num="3.3">Et d’où montait, ainsi que tel rêve de Dante,</l>
						<l n="12" num="3.4">Un cri désespéré vers notre cher bonheur.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">… Ah ! ne plus traverser cette misère humaine !</l>
						<l n="14" num="4.2">N’y être plus tous deux follement contrastés,</l>
						<l n="15" num="4.3">Mais connaître la barque improbable qui mène</l>
						<l n="16" num="4.4">En pleine fable, au cœur d’exultantes cités !</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">N’avoir plus dans le cœur la grande inquiétude,</l>
						<l n="18" num="5.2">Quand d’autres sont si mal, de nous sentir si bien,</l>
						<l n="19" num="5.3">D’entendre geindre autour de notre solitude</l>
						<l n="20" num="5.4">La souffrance du monde et de n’y pouvoir rien !</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1">Cher ! Cher ! Presse-moi bien contre ton âme claire,</l>
						<l n="22" num="6.2">Que je n’écoute pas, que je n’entende pas,</l>
						<l n="23" num="6.3">Et pour que l’étau doux et fort de tes deux bras</l>
						<l n="24" num="6.4">Étouffe en moi l’horreur de savoir la misère !</l>
					</lg>
				</div></body></text></TEI>