<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="AGT">
					<name>
						<forename>Marie</forename>
						<nameLink>d’</nameLink>
						<surname>AGOULT</surname>
						<addName type="pen_name">Daniel STERN</addName>
					</name>
					<date from="1805" to="1876">1805-1876</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>185 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">AGT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>ESQUISSES MORALES : PENSÉES, RÉFLEXIONS, MAXIMES</title>
						<author>Daniel STERN</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">http://catalogue.bnf.fr/ark:/12148/cb30005117k</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
							<title>ESQUISSES MORALES : PENSÉES, RÉFLEXIONS, MAXIMES</title>
							<author>Daniel STERN</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>CALMANN LÉVY, ÉDITEUR</publisher>
									<date when=" 1880">1880</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1880">1880</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La présente édition ne porte que sur le chapitre consacrée aux poésies.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">POÉSIES</head><div type="poem" key="AGT7">
					<head type="number">VII</head>
					<head type="main">LA STATUE DE GOETHE, A FRANCFORT</head>
					<head type="form">SONNET</head>
					<lg n="1">
						<l n="1" num="1.1">C’était par un long soir de la saison puissante</l>
						<l n="2" num="1.2">Qui prodigue à la terre et le fruit et la fleur,</l>
						<l n="3" num="1.3">Emplit de gerbes d’or le char du moissonneur</l>
						<l n="4" num="1.4">Et gonfle aux ceps ployés la grappe jaunissante.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">Les derniers feux du jour et leur calme splendeur,</l>
						<l n="6" num="2.2">Au loin, du mont Taunus doraient la cime ardente.</l>
						<l n="7" num="2.3">Le bel astre d’amour qui brille au ciel de Dante</l>
						<l n="8" num="2.4">Montait sur la cité de l’antique empereur.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">Sur le haut piédestal où ta gloire s’élève,</l>
						<l n="10" num="3.2">D’un regard de Vénus, doucement, comme en rêve,</l>
						<l n="11" num="3.3">0 Goethe ! s’éclairait ton grand front souverain,</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1">Tandis que de silence et d’ombre revêtue,</l>
						<l n="13" num="4.2">Craintive, je baisais au pied de ta statue</l>
						<l n="14" num="4.3">Le pli rigide et froid de ton manteau d’airain.</l>
					</lg>
				</div></body></text></TEI>