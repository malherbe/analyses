<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Fables de Florian</title>
				<title type="medium">Édition électronique</title>
				<author key="FLO">
					<name>
						<forename>Jean-Pierre Claris</forename>
						<nameLink>de</nameLink>
						<surname>FLORIAN</surname>
					</name>
					<date from="1755" to="1794">1755-1794</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Version initiale du texte électronique (Project Gutenberg)</resp>
					<name id="LV">
						<forename>Laurent</forename>
						<surname>Vogel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3910 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">FLO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Fables de Florian</title>
						<author>Jean-Pierre Claris de Florian</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">https://www.gutenberg.org/ebooks/58251</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Fables de Florian</title>
								<title>MISES DANS UN NOUVEL ORDRE</title>
								<author>Jean-Pierre Claris de Florian</author>
								<edition>Louis-François Jauffret</edition>
								<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k571677</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LA LIBRAIRIE ÉCONOMIQUE</publisher>
									<date when="1801">1800-1801</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1793">1793</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface n’est pas reprise dans cette édition électronique.</p>
				<p>L’astérisque qui précède le titre des fables ajoutées par l’éditeur a été placé en fin de titre.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les notes de fin de page ont été reportées en fin de poème.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">LIVRE TROISIÈME</head><div type="poem" key="FLO46">
					<head type="number">FABLE II</head>
					<head type="main">L’Inondation</head>
					<lg n="1">
						<l n="1" num="1.1">Des laboureurs vivaient paisibles et contens</l>
						<l n="2" num="1.2"><space unit="char" quantity="8"></space>Dans un riche et nombreux village ;</l>
						<l n="3" num="1.3">Dès l’aurore ils allaient travailler à leurs champs,</l>
						<l n="4" num="1.4"><space unit="char" quantity="8"></space>Le soir ils revenaient chantans</l>
						<l n="5" num="1.5"><space unit="char" quantity="8"></space>Au sein d’un tranquille ménage ;</l>
						<l n="6" num="1.6"><space unit="char" quantity="8"></space>Et la nature bonne et sage,</l>
						<l n="7" num="1.7">Pour prix de leurs travaux, leur donnait tous les ans</l>
						<l n="8" num="1.8"><space unit="char" quantity="8"></space>De beaux blés et de beaux enfans.</l>
						<l n="9" num="1.9">Mais il faut bien souffrir, c’est notre destinée.</l>
						<l n="10" num="1.10"><space unit="char" quantity="8"></space>Or il arriva qu’une année,</l>
						<l n="11" num="1.11"><space unit="char" quantity="8"></space>Dans le mois où le blond Phébus</l>
						<l n="12" num="1.12">S’en va faire visite au brûlant Sirius,</l>
						<l n="13" num="1.13"><space unit="char" quantity="8"></space>La terre, de sucs épuisée,</l>
						<l n="14" num="1.14"><space unit="char" quantity="8"></space>Ouvrant de toutes parts son sein,</l>
						<l n="15" num="1.15"><space unit="char" quantity="8"></space>Haletait sous un ciel d’airain.</l>
						<l n="16" num="1.16"><space unit="char" quantity="8"></space>Point de pluie et point de rosée.</l>
						<l n="17" num="1.17">Sur un sol crevassé l’on voit noircir le grain,</l>
						<l n="18" num="1.18">Les épis sont brûlés, et leurs têtes penchées</l>
						<l n="19" num="1.19"><space unit="char" quantity="8"></space>Tombent sur leurs tiges séchées.</l>
						<l n="20" num="1.20"><space unit="char" quantity="8"></space>On trembla de mourir de faim ;</l>
						<l n="21" num="1.21">La commune s’assemble. En hâte on délibère ;</l>
						<l n="22" num="1.22"><space unit="char" quantity="8"></space>Et chacun, comme à l’ordinaire,</l>
						<l n="23" num="1.23"><space unit="char" quantity="8"></space>Parle beaucoup, et rien ne dit.</l>
						<l n="24" num="1.24">Enfin quelques vieillards, gens de sens et d’esprit,</l>
						<l n="25" num="1.25"><space unit="char" quantity="8"></space>Proposèrent un parti sage :</l>
						<l n="26" num="1.26">Mes amis, dirent-ils, d’ici vous pouvez voir</l>
						<l n="27" num="1.27"><space unit="char" quantity="8"></space>Ce mont peu distant du village ;</l>
						<l n="28" num="1.28">Là, se trouve un grand lac, immense réservoir</l>
						<l n="29" num="1.29">Des souterraines eaux qui s’y font un passage.</l>
						<l n="30" num="1.30">Allez saigner ce lac ; mais sachez ménager</l>
						<l n="31" num="1.31"><space unit="char" quantity="8"></space>Un petit nombre de saignées,</l>
						<l n="32" num="1.32">Afin qu’à votre gré vous puissiez diriger</l>
						<l n="33" num="1.33">Ces bienfaisantes eaux dans vos terres baignées.</l>
						<l n="34" num="1.34">Juste quand il faudra nous les arrêterons.</l>
						<l n="35" num="1.35">Prenez bien garde au moins… Oui, oui, courons, courons,</l>
						<l n="36" num="1.36"><space unit="char" quantity="8"></space>S’écrie aussitôt l’assemblée.</l>
						<l n="37" num="1.37"><space unit="char" quantity="8"></space>Et voilà mille jeunes gens</l>
						<l n="38" num="1.38">Armés d’hoyaux, de pics, et d’autres instrumens,</l>
						<l n="39" num="1.39">Qui volent vers le lac : la terre est travaillée</l>
						<l n="40" num="1.40">Tout autour de ses bords ; on perce en cent endroits</l>
						<l n="41" num="1.41"><space unit="char" quantity="18"></space>A la fois ;</l>
						<l n="42" num="1.42">D’un morceau de terrain chaque ouvrier se charge :</l>
						<l n="43" num="1.43"><space unit="char" quantity="8"></space>Courage, allons ! point de repos !</l>
						<l n="44" num="1.44">L’ouverture jamais ne peut être assez large.</l>
						<l n="45" num="1.45">Cela fut bientôt fait. Avant la nuit, les eaux,</l>
						<l n="46" num="1.46">Tombant de tout leur poids sur leur digue affaiblie,</l>
						<l n="47" num="1.47"><space unit="char" quantity="8"></space>De par-tout roulent à grands flots.</l>
						<l n="48" num="1.48">Transports et complimens de la troupe ébahie,</l>
						<l n="49" num="1.49"><space unit="char" quantity="8"></space>Qui s’admire dans ses travaux.</l>
						<l n="50" num="1.50">Le lendemain matin ce ne fut pas de même :</l>
						<l n="51" num="1.51">On voit flotter les blés sur un océan d’eau ;</l>
						<l n="52" num="1.52">Pour sortir du village il faut prendre un bateau ;</l>
						<l n="53" num="1.53">Tout est perdu, noyé. La douleur est extrême,</l>
						<l n="54" num="1.54">On s’en prend aux vieillards ; C’est vous, leur disait-on,</l>
						<l n="55" num="1.55"><space unit="char" quantity="8"></space>Qui nous coûtez notre moisson ;</l>
						<l n="56" num="1.56">Votre maudit conseil… Il était salutaire,</l>
						<l n="57" num="1.57">Répondit un d’entre eux ; mais ce qu’on vient de faire</l>
						<l n="58" num="1.58">Est fort loin du conseil comme de la raison.</l>
						<l n="59" num="1.59">Nous voulions un peu d’eau, vous nous lâchez la bonde ;</l>
						<l n="60" num="1.60">L’excès d’un très-grand bien devient un mal très-grand :</l>
						<l n="61" num="1.61"><space unit="char" quantity="8"></space>Le sage arrose doucement,</l>
						<l n="62" num="1.62"><space unit="char" quantity="8"></space>L’insensé tout de suite inonde.</l>
					</lg>
				</div></body></text></TEI>