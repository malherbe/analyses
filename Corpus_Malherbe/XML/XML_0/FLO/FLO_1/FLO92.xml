<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Fables de Florian</title>
				<title type="medium">Édition électronique</title>
				<author key="FLO">
					<name>
						<forename>Jean-Pierre Claris</forename>
						<nameLink>de</nameLink>
						<surname>FLORIAN</surname>
					</name>
					<date from="1755" to="1794">1755-1794</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Version initiale du texte électronique (Project Gutenberg)</resp>
					<name id="LV">
						<forename>Laurent</forename>
						<surname>Vogel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3910 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">FLO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Fables de Florian</title>
						<author>Jean-Pierre Claris de Florian</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">https://www.gutenberg.org/ebooks/58251</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Fables de Florian</title>
								<title>MISES DANS UN NOUVEL ORDRE</title>
								<author>Jean-Pierre Claris de Florian</author>
								<edition>Louis-François Jauffret</edition>
								<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k571677</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LA LIBRAIRIE ÉCONOMIQUE</publisher>
									<date when="1801">1800-1801</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1793">1793</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface n’est pas reprise dans cette édition électronique.</p>
				<p>L’astérisque qui précède le titre des fables ajoutées par l’éditeur a été placé en fin de titre.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les notes de fin de page ont été reportées en fin de poème.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">LIVRE CINQUIÈME</head><div type="poem" key="FLO92">
					<head type="number">FABLE IV</head>
					<head type="main">La Colombe et son Nourisson</head>
					<lg n="1">
						<l n="1" num="1.1"><space unit="char" quantity="8"></space>Une colombe gémissait</l>
						<l n="2" num="1.2"><space unit="char" quantity="8"></space>De ne pouvoir devenir mère :</l>
						<l n="3" num="1.3">Elle avait fait cent fois tout ce qu’il fallait faire</l>
						<l n="4" num="1.4">Pour en venir à bout, rien ne réussissait.</l>
						<l n="5" num="1.5">Un jour, se promenant dans un bois solitaire,</l>
						<l n="6" num="1.6"><space unit="char" quantity="8"></space>Elle rencontre en un vieux nid</l>
						<l n="7" num="1.7">Un œuf abandonné, point trop gros, point petit,</l>
						<l n="8" num="1.8"><space unit="char" quantity="8"></space>Semblable aux œufs de tourterelle.</l>
						<l n="9" num="1.9"><space unit="char" quantity="8"></space>Ah ! quel bonheur ! s’écria-t-elle :</l>
						<l n="10" num="1.10"><space unit="char" quantity="8"></space>Je pourrai donc enfin couver,</l>
						<l n="11" num="1.11"><space unit="char" quantity="8"></space>Et puis nourrir, puis élever,</l>
						<l n="12" num="1.12">Un enfant qui fera le charme de ma vie !</l>
						<l n="13" num="1.13"><space unit="char" quantity="8"></space>Tous les soins qu’il me coûtera,</l>
						<l n="14" num="1.14"><space unit="char" quantity="8"></space>Les tourmens qu’il me causera,</l>
						<l n="15" num="1.15">Seront encor des biens pour mon âme ravie :</l>
						<l n="16" num="1.16"><space unit="char" quantity="8"></space>Quel plaisir vaut ces soucis-là ?</l>
						<l n="17" num="1.17">Cela dit, dans le nid la colombe établie</l>
						<l n="18" num="1.18">Se met à couver l’œuf, et le couve si bien,</l>
						<l n="19" num="1.19"><space unit="char" quantity="8"></space>Qu’elle ne le quitte pour rien,</l>
						<l n="20" num="1.20">Pas même pour manger ; l’amour nourrit les mères.</l>
						<l n="21" num="1.21">Après vingt et un jours elle voit naître enfin</l>
						<l n="22" num="1.22">Celui dont elle attend son bonheur, son destin,</l>
						<l n="23" num="1.23"><space unit="char" quantity="8"></space>Et ses délices les plus chères.</l>
						<l n="24" num="1.24"><space unit="char" quantity="8"></space>De joie elle est prête à mourir ;</l>
						<l n="25" num="1.25">Auprès de son petit nuit et jour elle veille,</l>
						<l n="26" num="1.26">L’écoute respirer, le regarde dormir,</l>
						<l n="27" num="1.27"><space unit="char" quantity="8"></space>S’épuise pour le mieux nourrir.</l>
						<l n="28" num="1.28"><space unit="char" quantity="8"></space>L’enfant chéri vient à merveille,</l>
						<l n="29" num="1.29"><space unit="char" quantity="8"></space>Son corps grossit en peu de temps :</l>
						<l n="30" num="1.30"><space unit="char" quantity="8"></space>Mais son bec, ses yeux et ses ailes,</l>
						<l n="31" num="1.31"><space unit="char" quantity="8"></space>Diffèrent fort des tourterelles ;</l>
						<l n="32" num="1.32"><space unit="char" quantity="8"></space>La mère les voit ressemblans.</l>
						<l n="33" num="1.33"><space unit="char" quantity="8"></space>A bien élever sa jeunesse</l>
						<l n="34" num="1.34">Elle met tous ses soins, lui prêche la sagesse,</l>
						<l n="35" num="1.35">Et sur-tout l’amitié, lui dit à chaque instant :</l>
						<l n="36" num="1.36"><space unit="char" quantity="8"></space>Pour être heureux, mon cher enfant,</l>
						<l n="37" num="1.37">Il ne faut que deux points, la paix avec soi-même,</l>
						<l n="38" num="1.38">Puis quelques bons amis dignes de nous chérir.</l>
						<l n="39" num="1.39">La vertu de la paix nous fait seule jouir ;</l>
						<l n="40" num="1.40"><space unit="char" quantity="8"></space>Et le secret pour qu’on nous aime,</l>
						<l n="41" num="1.41">C’est d’aimer les premiers, facile et doux plaisir.</l>
						<l n="42" num="1.42"><space unit="char" quantity="8"></space>Ainsi parlait la tourterelle,</l>
						<l n="43" num="1.43"><space unit="char" quantity="8"></space>Quand, au milieu de sa leçon,</l>
						<l n="44" num="1.44"><space unit="char" quantity="8"></space>Un malheureux petit pinson,</l>
						<l n="45" num="1.45">Échappé de son nid, vient s’abattre auprès d’elle.</l>
						<l n="46" num="1.46">Le jeune nourrisson à peine l’apperçoit,</l>
						<l n="47" num="1.47"><space unit="char" quantity="8"></space>Qu’il court à lui : sa mère croit</l>
						<l n="48" num="1.48">Que c’est pour le traiter comme ami, comme frère,</l>
						<l n="49" num="1.49"><space unit="char" quantity="8"></space>Et pour offrir au voyageur</l>
						<l n="50" num="1.50"><space unit="char" quantity="8"></space>Une retraite hospitalière.</l>
						<l n="51" num="1.51">Elle applaudit déjà : mais quelle est sa douleur,</l>
						<l n="52" num="1.52">Lorsqu’elle voit son fils, ce fils dont la jeunesse</l>
						<l n="53" num="1.53">N’entendit que leçons de vertu, de sagesse,</l>
						<l n="54" num="1.54">Saisir le faible oiseau, le plumer, le manger,</l>
						<l n="55" num="1.55">Et garder, au milieu de l’horrible carnage,</l>
						<l n="56" num="1.56">Ce tranquille sang froid, assuré témoignage</l>
						<l n="57" num="1.57">Que le cœur désormais ne peut se corriger !</l>
						<l n="58" num="1.58"><space unit="char" quantity="8"></space>Elle en mourut, la pauvre mère.</l>
						<l n="59" num="1.59">Quel triste prix des soins donnés à cet enfant !</l>
						<l n="60" num="1.60"><space unit="char" quantity="8"></space>Mais c’était le fils d’un milan :</l>
						<l n="61" num="1.61"><space unit="char" quantity="8"></space>Rien ne change le caractère.</l>
					</lg>
				</div></body></text></TEI>