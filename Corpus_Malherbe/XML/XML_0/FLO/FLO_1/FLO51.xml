<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Fables de Florian</title>
				<title type="medium">Édition électronique</title>
				<author key="FLO">
					<name>
						<forename>Jean-Pierre Claris</forename>
						<nameLink>de</nameLink>
						<surname>FLORIAN</surname>
					</name>
					<date from="1755" to="1794">1755-1794</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Version initiale du texte électronique (Project Gutenberg)</resp>
					<name id="LV">
						<forename>Laurent</forename>
						<surname>Vogel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3910 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">FLO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Fables de Florian</title>
						<author>Jean-Pierre Claris de Florian</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">https://www.gutenberg.org/ebooks/58251</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Fables de Florian</title>
								<title>MISES DANS UN NOUVEL ORDRE</title>
								<author>Jean-Pierre Claris de Florian</author>
								<edition>Louis-François Jauffret</edition>
								<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k571677</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LA LIBRAIRIE ÉCONOMIQUE</publisher>
									<date when="1801">1800-1801</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1793">1793</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface n’est pas reprise dans cette édition électronique.</p>
				<p>L’astérisque qui précède le titre des fables ajoutées par l’éditeur a été placé en fin de titre.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les notes de fin de page ont été reportées en fin de poème.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">LIVRE TROISIÈME</head><div type="poem" key="FLO51">
					<head type="number">FABLE VII</head>
					<head type="main">Le Lièvre, ses Amis, <lb></lb>et les deux Chevreuils</head>
					<lg n="1">
						<l n="1" num="1.1"><space unit="char" quantity="8"></space>Un lièvre de bon caractère</l>
						<l n="2" num="1.2"><space unit="char" quantity="8"></space>Voulait avoir beaucoup d’amis.</l>
						<l n="3" num="1.3">Beaucoup ! me direz-vous, c’est une grande affaire ;</l>
						<l n="4" num="1.4"><space unit="char" quantity="8"></space>Un seul est rare en ce pays.</l>
						<l n="5" num="1.5">J’en conviens ; mais mon lièvre avait cette marotte,</l>
						<l n="6" num="1.6"><space unit="char" quantity="8"></space>Et ne savait pas qu’Aristote</l>
						<l n="7" num="1.7">Disait aux jeunes Grecs à son école admis :</l>
						<l n="8" num="1.8"><space unit="char" quantity="8"></space>Mes amis, il n’est point d’amis.</l>
						<l n="9" num="1.9">Sans cesse il s’occupait d’obliger et de plaire ;</l>
						<l n="10" num="1.10">S’il passait un lapin, d’un air doux et civil,</l>
						<l n="11" num="1.11">Vîte il courait à lui : Mon cousin, disait-il,</l>
						<l n="12" num="1.12">J’ai du beau serpolet tout près de ma tanière,</l>
						<l n="13" num="1.13">De déjeûner chez moi faites-moi la faveur.</l>
						<l n="14" num="1.14">S’il voyait un cheval paître dans la campagne,</l>
						<l n="15" num="1.15">Il allait l’aborder : Peut-être monseigneur</l>
						<l n="16" num="1.16">A-t-il besoin de boire ; au pied de la montagne</l>
						<l n="17" num="1.17"><space unit="char" quantity="8"></space>Je connais un lac transparent</l>
						<l n="18" num="1.18">Qui n’est jamais ridé par le moindre zéphyre :</l>
						<l n="19" num="1.19"><space unit="char" quantity="8"></space>Si monseigneur veut, dans l’instant</l>
						<l n="20" num="1.20"><space unit="char" quantity="8"></space>J’aurai l’honneur de l’y conduire.</l>
						<l n="21" num="1.21"><space unit="char" quantity="8"></space>Ainsi, pour tous les animaux,</l>
						<l n="22" num="1.22"><space unit="char" quantity="8"></space>Cerfs, moutons, coursiers, daims, taureaux,</l>
						<l n="23" num="1.23">Complaisant, empressé, toujours rempli de zèle,</l>
						<l n="24" num="1.24">Il voulait de chacun faire un ami fidèle,</l>
						<l n="25" num="1.25">Et s’en croyait aimé parce qu’il les aimait.</l>
						<l n="26" num="1.26">Certain jour que, tranquille en son gîte, il dormait,</l>
						<l n="27" num="1.27">Le bruit du cor l’éveille, il décampe au plus vîte ;</l>
						<l n="28" num="1.28"><space unit="char" quantity="8"></space>Quatre chiens s’élancent après,</l>
						<l n="29" num="1.29"><space unit="char" quantity="8"></space>Un maudit piqueur les excite,</l>
						<l n="30" num="1.30">Et voilà notre lièvre arpentant les guérets.</l>
						<l n="31" num="1.31">Il va, tourne, revient, aux mêmes lieux repasse,</l>
						<l n="32" num="1.32"><space unit="char" quantity="8"></space>Saute, franchit un long espace</l>
						<l n="33" num="1.33">Pour dévoyer les chiens, et, prompt comme l’éclair,</l>
						<l n="34" num="1.34"><space unit="char" quantity="8"></space>Gagne pays, et puis s’arrête :</l>
						<l n="35" num="1.35"><space unit="char" quantity="8"></space>Assis, les deux pattes en l’air,</l>
						<l n="36" num="1.36">L’œil et l’oreille au guet, il élève la tête,</l>
						<l n="37" num="1.37">Cherchant s’il ne voit point quelqu’un de ses amis.</l>
						<l n="38" num="1.38"><space unit="char" quantity="8"></space>Il apperçoit dans des taillis</l>
						<l n="39" num="1.39">Un lapin que toujours il traita comme un frère ;</l>
						<l n="40" num="1.40">Il y court : Par pitié, sauve-moi, lui dit-il,</l>
						<l n="41" num="1.41"><space unit="char" quantity="8"></space>Donne retraite à ma misère,</l>
						<l n="42" num="1.42">Ouvre-moi ton terrier ; tu vois l’affreux péril…</l>
						<l n="43" num="1.43">Ah ! que j’en suis fâché ! répond d’un air tranquille</l>
						<l n="44" num="1.44">Le lapin : je ne puis t’offrir mon logement,</l>
						<l n="45" num="1.45"><space unit="char" quantity="8"></space>Ma femme accouche en ce moment,</l>
						<l n="46" num="1.46">Sa famille et la mienne ont rempli mon asile ;</l>
						<l n="47" num="1.47"><space unit="char" quantity="8"></space>Je te plains bien sincèrement :</l>
						<l n="48" num="1.48">Adieu, mon cher ami. Cela dit, il s’échappe,</l>
						<l n="49" num="1.49"><space unit="char" quantity="8"></space>Et voici la meute qui jappe.</l>
						<l n="50" num="1.50">Le pauvre lièvre part. A quelques pas plus loin,</l>
						<l n="51" num="1.51">Il rencontre un taureau que, cent fois au besoin,</l>
						<l n="52" num="1.52">Il avait obligé ; tendrement il le prie</l>
						<l n="53" num="1.53">D’arrêter un moment cette meute en furie</l>
						<l n="54" num="1.54"><space unit="char" quantity="8"></space>Qui de ses cornes aura peur.</l>
						<l n="55" num="1.55">Hélas ! dit le taureau, ce serait de grand cœur :</l>
						<l n="56" num="1.56"><space unit="char" quantity="8"></space>Mais des génisses la plus belle</l>
						<l n="57" num="1.57">Est seule dans ce bois, je l’entends qui m’appelle ;</l>
						<l n="58" num="1.58">Et tu ne voudrais pas retarder mon bonheur.</l>
						<l n="59" num="1.59">Disant ces mots, il part. Notre lièvre, hors d’haleine,</l>
						<l n="60" num="1.60">Implore vainement un daim, un cerf dix-cors,</l>
						<l n="61" num="1.61">Ses amis les plus sûrs ; ils l’écoutent à peine,</l>
						<l n="62" num="1.62"><space unit="char" quantity="8"></space>Tant ils ont peur du bruit des cors.</l>
						<l n="63" num="1.63">Le pauvre infortuné, sans force et sans courage,</l>
						<l n="64" num="1.64">Allait se rendre aux chiens, quand, du milieu du bois,</l>
						<l n="65" num="1.65">Deux chevreuils reposant sous le même feuillage</l>
						<l n="66" num="1.66"><space unit="char" quantity="8"></space>Des chasseurs entendent la voix :</l>
						<l n="67" num="1.67">L’un d’eux se lève et part ; la meute sanguinaire</l>
						<l n="68" num="1.68"><space unit="char" quantity="8"></space>Quitte le lièvre et court après.</l>
						<l n="69" num="1.69"><space unit="char" quantity="8"></space>En vain le piqueur en colère</l>
						<l n="70" num="1.70">Crie, et jure, et se fâche : à travers les forêts</l>
						<l n="71" num="1.71"><space unit="char" quantity="8"></space>Le chevreuil emmène la chasse,</l>
						<l n="72" num="1.72">Va faire un long circuit, et revient au buisson</l>
						<l n="73" num="1.73"><space unit="char" quantity="8"></space>Où l’attendait son compagnon,</l>
						<l n="74" num="1.74"><space unit="char" quantity="8"></space>Qui dans l’instant part à sa place.</l>
						<l n="75" num="1.75">Celui-ci fait de même, et, pendant tout le jour,</l>
						<l n="76" num="1.76">Les deux chevreuils lancés et quittés tour-à-tour</l>
						<l n="77" num="1.77"><space unit="char" quantity="8"></space>Fatiguent la meute obstinée.</l>
						<l n="78" num="1.78"><space unit="char" quantity="8"></space>Enfin les chasseurs tout honteux</l>
						<l n="79" num="1.79">Prennent le bon parti de retourner chez eux.</l>
						<l n="80" num="1.80"><space unit="char" quantity="8"></space>Déjà la retraite est sonnée,</l>
						<l n="81" num="1.81">Et les chevreuils rejoints. Le lièvre palpitant</l>
						<l n="82" num="1.82">S’approche, et leur raconte, en les félicitant,</l>
						<l n="83" num="1.83">Que ses nombreux amis, dans ce péril extrême,</l>
						<l n="84" num="1.84">L’avaient abandonné. Je n’en suis pas surpris,</l>
						<l n="85" num="1.85">Répond un des chevreuils : à quoi bon tant d’amis ?</l>
						<l n="86" num="1.86"><space unit="char" quantity="8"></space>Un seul suffit quand il nous aime.</l>
					</lg>
				</div></body></text></TEI>