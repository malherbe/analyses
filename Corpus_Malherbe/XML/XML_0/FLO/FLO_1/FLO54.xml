<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Fables de Florian</title>
				<title type="medium">Édition électronique</title>
				<author key="FLO">
					<name>
						<forename>Jean-Pierre Claris</forename>
						<nameLink>de</nameLink>
						<surname>FLORIAN</surname>
					</name>
					<date from="1755" to="1794">1755-1794</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Version initiale du texte électronique (Project Gutenberg)</resp>
					<name id="LV">
						<forename>Laurent</forename>
						<surname>Vogel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3910 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">FLO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Fables de Florian</title>
						<author>Jean-Pierre Claris de Florian</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">https://www.gutenberg.org/ebooks/58251</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Fables de Florian</title>
								<title>MISES DANS UN NOUVEL ORDRE</title>
								<author>Jean-Pierre Claris de Florian</author>
								<edition>Louis-François Jauffret</edition>
								<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k571677</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LA LIBRAIRIE ÉCONOMIQUE</publisher>
									<date when="1801">1800-1801</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1793">1793</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface n’est pas reprise dans cette édition électronique.</p>
				<p>L’astérisque qui précède le titre des fables ajoutées par l’éditeur a été placé en fin de titre.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les notes de fin de page ont été reportées en fin de poème.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">LIVRE TROISIÈME</head><div type="poem" key="FLO54">
					<head type="number">FABLE X</head>
					<head type="main">Le Renard déguisé</head>
					<lg n="1">
						<l n="1" num="1.1">Un renard plein d’esprit, d’adresse, de prudence,</l>
						<l n="2" num="1.2">A la cour d’un lion servait depuis long-temps ;</l>
						<l n="3" num="1.3"><space unit="char" quantity="8"></space>Les succès les plus éclatans</l>
						<l n="4" num="1.4">Avaient prouvé son zèle et son intelligence.</l>
						<l n="5" num="1.5">Pour peu qu’on l’employât, toute affaire allait bien.</l>
						<l n="6" num="1.6">On le louait beaucoup, mais sans lui donner rien ;</l>
						<l n="7" num="1.7">Et l’habile renard était dans l’indigence.</l>
						<l n="8" num="1.8"><space unit="char" quantity="8"></space>Lassé de servir des ingrats,</l>
						<l n="9" num="1.9">De réussir toujours sans en être plus gras,</l>
						<l n="10" num="1.10">Il s’enfuit de la cour ; dans un bois solitaire</l>
						<l n="11" num="1.11"><space unit="char" quantity="8"></space>Il s’en va trouver son grand-père,</l>
						<l n="12" num="1.12">Vieux renard retiré, qui jadis fut visir.</l>
						<l n="13" num="1.13">Là, contant ses exploits, et puis les injustices,</l>
						<l n="14" num="1.14"><space unit="char" quantity="8"></space>Les dégoûts, qu’il eut à souffrir,</l>
						<l n="15" num="1.15">Il demande pourquoi de si nombreux services</l>
						<l n="16" num="1.16"><space unit="char" quantity="8"></space>N’ont jamais pu rien obtenir.</l>
						<l n="17" num="1.17">Le bon homme renard, avec sa voix cassée,</l>
						<l n="18" num="1.18">Lui dit : Mon cher enfant, la semaine passée,</l>
						<l n="19" num="1.19">Un blaireau, mon cousin, est mort dans ce terrier :</l>
						<l n="20" num="1.20"><space unit="char" quantity="8"></space>C’est moi qui suis son héritier,</l>
						<l n="21" num="1.21">J’ai conservé sa peau : mets-la dessus la tienne,</l>
						<l n="22" num="1.22">Et retourne à la cour. Le renard avec peine</l>
						<l n="23" num="1.23">Se soumit au conseil ; affublé de la peau</l>
						<l n="24" num="1.24"><space unit="char" quantity="8"></space>De feu son cousin le blaireau,</l>
						<l n="25" num="1.25">Il va se regarder dans l’eau d’une fontaine,</l>
						<l n="26" num="1.26">Se trouve l’air d’un sot, tel qu’était le cousin.</l>
						<l n="27" num="1.27">Tout honteux, de la cour il reprend le chemin.</l>
						<l n="28" num="1.28">Mais, quelques mois après, dans un riche équipage,</l>
						<l n="29" num="1.29">Entouré de valets, d’esclaves, de flatteurs,</l>
						<l n="30" num="1.30"><space unit="char" quantity="8"></space>Comblé de dons et de faveurs,</l>
						<l n="31" num="1.31">Il vient de sa fortune au vieillard faire hommage :</l>
						<l n="32" num="1.32">Il était grand visir. Je te l’avais bien dit,</l>
						<l n="33" num="1.33"><space unit="char" quantity="8"></space>S’écrie alors le vieux grand-père ;</l>
						<l n="34" num="1.34">Mon ami, chez les grands quiconque voudra plaire</l>
						<l n="35" num="1.35"><space unit="char" quantity="8"></space>Doit d’abord cacher son esprit.</l>
					</lg>
				</div></body></text></TEI>