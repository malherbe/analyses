<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Fables de Florian</title>
				<title type="medium">Édition électronique</title>
				<author key="FLO">
					<name>
						<forename>Jean-Pierre Claris</forename>
						<nameLink>de</nameLink>
						<surname>FLORIAN</surname>
					</name>
					<date from="1755" to="1794">1755-1794</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Version initiale du texte électronique (Project Gutenberg)</resp>
					<name id="LV">
						<forename>Laurent</forename>
						<surname>Vogel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3910 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">FLO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Fables de Florian</title>
						<author>Jean-Pierre Claris de Florian</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">https://www.gutenberg.org/ebooks/58251</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Fables de Florian</title>
								<title>MISES DANS UN NOUVEL ORDRE</title>
								<author>Jean-Pierre Claris de Florian</author>
								<edition>Louis-François Jauffret</edition>
								<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k571677</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LA LIBRAIRIE ÉCONOMIQUE</publisher>
									<date when="1801">1800-1801</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1793">1793</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface n’est pas reprise dans cette édition électronique.</p>
				<p>L’astérisque qui précède le titre des fables ajoutées par l’éditeur a été placé en fin de titre.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les notes de fin de page ont été reportées en fin de poème.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">LIVRE PREMIER</head><div type="poem" key="FLO12">
					<head type="number">FABLE XII</head>
					<head type="main">Le Vacher et le Garde-chasse</head>
					<lg n="1">
						<l n="1" num="1.1">Colin gardait un jour les vaches de son père ;</l>
						<l n="2" num="1.2"><space unit="char" quantity="8"></space>Colin n’avait pas de bergère,</l>
						<l n="3" num="1.3">Et s’ennuyait tout seul. Le garde sort du bois :</l>
						<l n="4" num="1.4">Depuis l’aube, dit-il, je cours, dans cette plaine,</l>
						<l n="5" num="1.5">Après un vieux chevreuil que j’ai manqué deux fois,</l>
						<l n="6" num="1.6"><space unit="char" quantity="8"></space>Et qui m’a mis tout hors d’haleine.</l>
						<l n="7" num="1.7"><space unit="char" quantity="8"></space>Il vient de passer par là bas,</l>
						<l n="8" num="1.8">Lui répondit Colin : mais, si vous êtes las,</l>
						<l n="9" num="1.9">Reposez-vous, gardez mes vaches à ma place,</l>
						<l n="10" num="1.10"><space unit="char" quantity="8"></space>Et j’irai faire votre chasse ;</l>
						<l n="11" num="1.11">Je réponds du chevreuil. — ma foi, je le veux bien :</l>
						<l n="12" num="1.12">Tiens, voilà mon fusil, prends avec toi mon chien,</l>
						<l n="13" num="1.13"><space unit="char" quantity="8"></space>Va le tuer. Colin s’apprête,</l>
						<l n="14" num="1.14">S’arme, appelle Sultan. Sultan, quoiqu’à regret,</l>
						<l n="15" num="1.15"><space unit="char" quantity="8"></space>Court avec lui vers la forêt.</l>
						<l n="16" num="1.16">Le chien bat les buissons ; il va, vient, sent, arrête,</l>
						<l n="17" num="1.17">Et voilà le chevreuil… Colin impatient</l>
						<l n="18" num="1.18"><space unit="char" quantity="8"></space>Tire aussitôt, manque la bête,</l>
						<l n="19" num="1.19"><space unit="char" quantity="8"></space>Et blesse le pauvre Sultan.</l>
						<l n="20" num="1.20"><space unit="char" quantity="8"></space>A la suite du chien qui crie,</l>
						<l n="21" num="1.21"><space unit="char" quantity="8"></space>Colin revient à la prairie.</l>
						<l n="22" num="1.22"><space unit="char" quantity="8"></space>Il trouve le garde ronflant ;</l>
						<l n="23" num="1.23"><space unit="char" quantity="4"></space>De vaches, point ; elles étaient volées.</l>
						<l n="24" num="1.24">Le malheureux Colin, s’arrachant les cheveux,</l>
						<l n="25" num="1.25">Parcourt en gémissant les monts et les vallées ;</l>
						<l n="26" num="1.26">Il ne voit rien. Le soir, sans vaches, tout honteux,</l>
						<l n="27" num="1.27"><space unit="char" quantity="8"></space>Colin retourne chez son père,</l>
						<l n="28" num="1.28"><space unit="char" quantity="8"></space>Et lui conte en tremblant l’affaire.</l>
						<l n="29" num="1.29">Celui-ci, saisissant un bâton de cormier,</l>
						<l n="30" num="1.30">Corrige son cher fils de ses folles idées,</l>
						<l n="31" num="1.31"><space unit="char" quantity="8"></space>Puis lui dit : Chacun son métier,</l>
						<l n="32" num="1.32"><space unit="char" quantity="8"></space>Les vaches seront bien gardées.</l>
					</lg>
				</div></body></text></TEI>