<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Fables de Florian</title>
				<title type="medium">Édition électronique</title>
				<author key="FLO">
					<name>
						<forename>Jean-Pierre Claris</forename>
						<nameLink>de</nameLink>
						<surname>FLORIAN</surname>
					</name>
					<date from="1755" to="1794">1755-1794</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Version initiale du texte électronique (Project Gutenberg)</resp>
					<name id="LV">
						<forename>Laurent</forename>
						<surname>Vogel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3910 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">FLO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Fables de Florian</title>
						<author>Jean-Pierre Claris de Florian</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">https://www.gutenberg.org/ebooks/58251</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Fables de Florian</title>
								<title>MISES DANS UN NOUVEL ORDRE</title>
								<author>Jean-Pierre Claris de Florian</author>
								<edition>Louis-François Jauffret</edition>
								<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k571677</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LA LIBRAIRIE ÉCONOMIQUE</publisher>
									<date when="1801">1800-1801</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1793">1793</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface n’est pas reprise dans cette édition électronique.</p>
				<p>L’astérisque qui précède le titre des fables ajoutées par l’éditeur a été placé en fin de titre.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les notes de fin de page ont été reportées en fin de poème.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">LIVRE TROISIÈME</head><div type="poem" key="FLO64">
					<head type="number">FABLE XX</head>
					<head type="main">Le Perroquet confiant *</head>
					<lg n="1">
						<l n="1" num="1.1"><hi rend="ital">Cela ne sera rien</hi>, disent certaines gens,</l>
						<l n="2" num="1.2"><space unit="char" quantity="8"></space>Lorsque la tempête est prochaine,</l>
						<l n="3" num="1.3">Pourquoi nous affliger avant que le mal vienne ?</l>
						<l n="4" num="1.4">Pourquoi ? Pour l’éviter, s’il en est encor temps.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><space unit="char" quantity="8"></space>Un capitaine de navire,</l>
						<l n="6" num="2.2"><space unit="char" quantity="8"></space>Fort brave homme, mais peu prudent,</l>
						<l n="7" num="2.3"><space unit="char" quantity="8"></space>Se mit en mer malgré le vent.</l>
						<l n="8" num="2.4"><space unit="char" quantity="8"></space>Le pilote avait beau lui dire</l>
						<l n="9" num="2.5"><space unit="char" quantity="8"></space>Qu’il risquait sa vie et son bien,</l>
						<l n="10" num="2.6"><space unit="char" quantity="8"></space>Notre homme ne faisait qu’en rire,</l>
						<l n="11" num="2.7">Et répétait toujours : <hi rend="ital">Cela ne sera rien</hi>.</l>
						<l n="12" num="2.8"><space unit="char" quantity="8"></space>Un perroquet de l’équipage,</l>
						<l n="13" num="2.9"><space unit="char" quantity="8"></space>A force d’entendre ces mots,</l>
						<l n="14" num="2.10">Les retint, et les dit pendant tout le voyage.</l>
						<l n="15" num="2.11">Le navire égaré voguait au gré des flots,</l>
						<l n="16" num="2.12"><space unit="char" quantity="8"></space>Quand un calme plat vous l’arrête.</l>
						<l n="17" num="2.13"><space unit="char" quantity="8"></space>Les vivres tiraient à leur fin ;</l>
						<l n="18" num="2.14">Point de terre voisine, et bientôt plus de pain.</l>
						<l n="19" num="2.15">Chacun des passagers s’attriste, s’inquiète ;</l>
						<l n="20" num="2.16"><space unit="char" quantity="8"></space>Notre capitaine se tait.</l>
						<l n="21" num="2.17"><hi rend="ital">Cela ne sera rien</hi>, criait le perroquet.</l>
						<l n="22" num="2.18">Le calme continue ; on vit vaille que vaille,</l>
						<l n="23" num="2.19"><space unit="char" quantity="8"></space>Il ne reste plus de volaille :</l>
						<l n="24" num="2.20">On mange les oiseaux, triste et dernier moyen !</l>
						<l n="25" num="2.21">Perruches, cardinaux, catakois, tout y passe ;</l>
						<l n="26" num="2.22"><space unit="char" quantity="8"></space>Le perroquet, la tête basse,</l>
						<l n="27" num="2.23">Disait plus doucement : <hi rend="ital">Cela ne sera rien</hi>.</l>
						<l n="28" num="2.24">Il pouvait encor fuir, sa cage était trouée ;</l>
						<l n="29" num="2.25">Il attendit, il fut étranglé bel et bien,</l>
						<l n="30" num="2.26">Et, mourant, il criait d’une voix enrouée :</l>
						<l n="31" num="2.27"><space unit="char" quantity="8"></space><hi rend="ital">Cela… Cela ne sera rien</hi>.</l>
					</lg>
				</div></body></text></TEI>