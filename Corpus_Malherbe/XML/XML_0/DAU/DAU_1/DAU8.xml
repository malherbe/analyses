<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">LES AMOUREUSES</title>
				<title type="sub">POEMES ET FANTAISIES</title>
				<title type="medium">Édition électronique</title>
				<author key="DAU">
					<name>
						<forename>Alphonse</forename>
						<surname>DAUDET</surname>
					</name>
					<date from="1840" to="1897">1840-1897</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1472 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DAU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LES AMOUREUSES, POEMES ET FANTAISIES</title>
						<author>Alphonse Daudet</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/Les_Amoureuses</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
							<title type="main">LES AMOUREUSES, POEMES ET FANTAISIES</title>
								<author>Alphonse Daudet</author>
								<edition>NOUVELLE ÉDITION</edition>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BIBLIOTHÈQUE-CHARPENTIER, Eugène Fasquelle, éditeur</publisher>
									<date when="1908">1908</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1858">1858</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties en prose ne sont pas reprises dans cette édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Le recueil a été découpé en parties et sous parties conformément à l’édition de 1882.</p>
				<correction>
					<p>Les majuscules des noms propres ont été restituées</p>
				</correction>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-12-17" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-12-17" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LES AMOUREUSES</head><div type="poem" key="DAU8">
					<head type="main">LE 1er MAI 1857</head>
					<head type="sub">MORT D’ALFRED DE MUSSET</head>
					<lg n="1">
						<l n="1" num="1.1">Nature de rêveur, tempérament d’artiste,</l>
						<l n="2" num="1.2">Il est resté toujours triste, horriblement triste.</l>
						<l n="3" num="1.3">Sans savoir ce qu’il veut, sans savoir ce qu’il a,</l>
						<l n="4" num="1.4">Il pleure ; pour un rien, pour ceci, pour cela.</l>
						<l n="5" num="1.5">Aujourd’hui c’est le temps, demain c’est une mouche,</l>
						<l n="6" num="1.6">Un rossignol qui fausse, un papillon qui louche…</l>
						<l n="7" num="1.7">Son corps est un roseau, son âme est une fleur,</l>
						<l n="8" num="1.8">Mais un roseau sans moelle, une fleur sans calice ;</l>
						<l n="9" num="1.9">Il est triste sans cause, il souffre sans douleur,</l>
						<l n="10" num="1.10">Il faudra qu’il en meure, et qu’on l’ensevelisse</l>
						<l n="11" num="1.11">Avec sa nostalgie au flanc, comme un cilice.</l>
					</lg>
					<lg n="2">
						<l n="12" num="2.1">Ne creusez pas son mal ; ne lui demandez rien,</l>
						<l n="13" num="2.2">Vous qui ne portez pas un cœur comme le sien.</l>
						<l n="14" num="2.3">Ne lui demandez rien, ô vous qu’il a choisies</l>
						<l n="15" num="2.4">Dans le ciel de son rêve et de ses fantaisies ;</l>
						<l n="16" num="2.5">C’est un petit enfant, prenez-le dans vos bras,</l>
						<l n="17" num="2.6">Dites-lui. « Mon amour, fais comme tu voudras,</l>
						<l n="18" num="2.7">« Ton mal est un secret, je ne veux pas l’apprendre. »</l>
						<l n="19" num="2.8">Souffrez de sa blessure, en essuyant ses yeux ;</l>
						<l n="20" num="2.9">Souffrez de sa douleur sans jamais la comprendre,</l>
						<l n="21" num="2.10">Car vous ne savez pas comme on guérit les dieux,</l>
						<l n="22" num="2.11">Car vous l’aimeriez moins en le connaissant mieux.</l>
					</lg>
					<lg n="3">
						<l n="23" num="3.1">Parfois, rayon dans l’ombre et perle dans la brume,</l>
						<l n="24" num="3.2">Son visage s’étoile et son regard s’allume ;</l>
						<l n="25" num="3.3">On dirait qu’il attend quelqu’un qui ne vient pas.</l>
						<l n="26" num="3.4">Mais ce n’est jamais toi qu’il cherche entre tes bras,</l>
						<l n="27" num="3.5">Ninette ; – ce qu’il veut, il n’en sait rien lui-même.</l>
						<l n="28" num="3.6">Dans tout ce qu’il espère et dans tout ce qu’il aime,</l>
						<l n="29" num="3.7">Il voit un vide immense et s’use à le combler,</l>
						<l n="30" num="3.8">Jusqu’au jour où, sentant que son âme est atteinte,</l>
						<l n="31" num="3.9">Sentant son âme atteinte et son mal redoubler</l>
						<l n="32" num="3.10">Il soit las de souffler sur une flamme éteinte…</l>
						<l n="33" num="3.11">Et meure de dégoût, de tristesse… et d’absinthe !</l>
					</lg>
				</div></body></text></TEI>