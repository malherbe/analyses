<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">LES AMOUREUSES</title>
				<title type="sub">POEMES ET FANTAISIES</title>
				<title type="medium">Édition électronique</title>
				<author key="DAU">
					<name>
						<forename>Alphonse</forename>
						<surname>DAUDET</surname>
					</name>
					<date from="1840" to="1897">1840-1897</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1472 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DAU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LES AMOUREUSES, POEMES ET FANTAISIES</title>
						<author>Alphonse Daudet</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/Les_Amoureuses</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
							<title type="main">LES AMOUREUSES, POEMES ET FANTAISIES</title>
								<author>Alphonse Daudet</author>
								<edition>NOUVELLE ÉDITION</edition>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BIBLIOTHÈQUE-CHARPENTIER, Eugène Fasquelle, éditeur</publisher>
									<date when="1908">1908</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1858">1858</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties en prose ne sont pas reprises dans cette édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Le recueil a été découpé en parties et sous parties conformément à l’édition de 1882.</p>
				<correction>
					<p>Les majuscules des noms propres ont été restituées</p>
				</correction>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-12-17" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-12-17" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LES AMOUREUSES</head><div type="poem" key="DAU14">
					<head type="main">LES PRUNES</head>
					<div type="section" n="1">
						<head type="number">I</head>
						<lg n="1">
							<l n="1" num="1.1">Si vous voulez savoir comment</l>
							<l n="2" num="1.2">Nous nous aimâmes pour des prunes,</l>
							<l n="3" num="1.3">Je vous le dirai doucement,</l>
							<l n="4" num="1.4">Si vous voulez savoir comment.</l>
							<l n="5" num="1.5">L’amour vient toujours en dormant,</l>
							<l n="6" num="1.6">Chez les bruns comme chez les brunes ;</l>
							<l n="7" num="1.7">En quelques mots voici comment</l>
							<l n="8" num="1.8">Nous nous aimâmes pour des prunes.</l>
						</lg>
					</div>
					<div type="section" n="2">
						<head type="number">II</head>
						<lg n="1">
							<l n="9" num="1.1">Mon oncle avait un grand verger</l>
							<l n="10" num="1.2">Et moi j’avais une cousine ;</l>
							<l n="11" num="1.3">Nous nous aimions sans y songer,</l>
							<l n="12" num="1.4">Mon oncle avait un grand verger.</l>
							<l n="13" num="1.5">Les oiseaux venaient y manger,</l>
							<l n="14" num="1.6">Le printemps faisait leur cuisine ;</l>
							<l n="15" num="1.7">Mon oncle avait un grand verger,</l>
							<l n="16" num="1.8">Et moi j’avais une cousine.</l>
						</lg>
					</div>
					<div type="section" n="3">
						<head type="number">III</head>
						<lg n="1">
							<l n="17" num="1.1">Un matin nous nous promenions</l>
							<l n="18" num="1.2">Dans le verger, avec Mariette :</l>
							<l n="19" num="1.3">Tout gentils, tout frais, tout mignons,</l>
							<l n="20" num="1.4">Un matin nous nous promenions.</l>
							<l n="21" num="1.5">Les cigales et les grillons</l>
							<l n="22" num="1.6">Nous fredonnaient une ariette :</l>
							<l n="23" num="1.7">Un matin nous nous promenions</l>
							<l n="24" num="1.8">Dans le verger avec Mariette.</l>
						</lg>
					</div>
					<div type="section" n="4">
						<head type="number">IV</head>
						<lg n="1">
							<l n="25" num="1.1">De tous côtés, d’ici, de là,</l>
							<l n="26" num="1.2">Les oiseaux chantaient dans les branches,</l>
							<l n="27" num="1.3">En si bémol, en ut, en la,</l>
							<l n="28" num="1.4">De tous côtés, d’ici, de là.</l>
							<l n="29" num="1.5">Les prés en habit de gala</l>
							<l n="30" num="1.6">Étaient pleins de fleurettes blanches.</l>
							<l n="31" num="1.7">De tous côtés, d’ici, de là,</l>
							<l n="32" num="1.8">Les oiseaux chantaient dans les branches.</l>
						</lg>
					</div>
					<div type="section" n="5">
						<head type="number">V</head>
						<lg n="1">
							<l n="33" num="1.1">Fraîche sous son petit bonnet,</l>
							<l n="34" num="1.2">Belle à ravir, et point coquette,</l>
							<l n="35" num="1.3">Ma cousine se démenait,</l>
							<l n="36" num="1.4">Fraîche sous son petit bonnet.</l>
							<l n="37" num="1.5">Elle sautait, allait, venait,</l>
							<l n="38" num="1.6">Comme un volant sur la raquette :</l>
							<l n="39" num="1.7">Fraîche sous son petit bonnet,</l>
							<l n="40" num="1.8">Belle à ravir et point coquette.</l>
						</lg>
					</div>
					<div type="section" n="6">
						<head type="number">VI</head>
						<lg n="1">
							<l n="41" num="1.1">Arrivée au fond du verger,</l>
							<l n="42" num="1.2">Ma cousine lorgne les prunes ;</l>
							<l n="43" num="1.3">Et la gourmande en veut manger,</l>
							<l n="44" num="1.4">Arrivée au fond du verger.</l>
							<l n="45" num="1.5">L’arbre est bas ; sans se déranger</l>
							<l n="46" num="1.6">Elle en fait tomber quelques-unes :</l>
							<l n="47" num="1.7">Arrivée au fond du verger,</l>
							<l n="48" num="1.8">Ma cousine lorgne les prunes.</l>
						</lg>
					</div>
					<div type="section" n="7">
						<head type="number">VII</head>
						<lg n="1">
							<l n="49" num="1.1">Elle en prend une, elle la mord,</l>
							<l n="50" num="1.2">Et, me l’offrant : « Tiens !… » me dit-elle.</l>
							<l n="51" num="1.3">Mon pauvre cœur battait bien fort !</l>
							<l n="52" num="1.4">Elle en prend une, elle la mord.</l>
							<l n="53" num="1.5">Ses petites dents sur le bord</l>
							<l n="54" num="1.6">Avaient fait des points de dentelle…</l>
							<l n="55" num="1.7">Elle en prend une, elle la mord,</l>
							<l n="56" num="1.8">Et, me l’offrant : « Tiens !… » me dit-elle.</l>
						</lg>
					</div>
					<div type="section" n="8">
						<head type="number">VIII</head>
						<lg n="1">
							<l n="57" num="1.1">Ce fut tout, mais ce fut assez ;</l>
							<l n="58" num="1.2">Ce seul fruit disait bien des choses</l>
							<l n="59" num="1.3">(Si j’avais su ce que je sais !…)</l>
							<l n="60" num="1.4">Ce fut tout, mais ce fut assez.</l>
							<l n="61" num="1.5">Je mordis, comme vous pensez,</l>
							<l n="62" num="1.6">Sur la trace des lèvres roses :</l>
							<l n="63" num="1.7">Ce fut tout, mais ce fut assez ;</l>
							<l n="64" num="1.8">Ce seul fruit disait bien des choses.</l>
						</lg>
					</div>
					<div type="section" n="9">
						<head type="number">IX</head>
						<head type="main">À MES LECTRICES</head>
						<lg n="1">
							<l n="65" num="1.1">Oui, mesdames, voilà comment</l>
							<l n="66" num="1.2">Nous nous aimâmes pour des prunes :</l>
							<l n="67" num="1.3">N’allez pas l’entendre autrement ;</l>
							<l n="68" num="1.4">Oui, mesdames, voilà comment.</l>
							<l n="69" num="1.5">Si parmi vous, pourtant, d’aucunes</l>
							<l n="70" num="1.6">Le comprenaient différemment,</l>
							<l n="71" num="1.7">Ma foi, tant pis ! voilà comment</l>
							<l n="72" num="1.8">Nous nous aimâmes pour des prunes.</l>
						</lg>
					</div>
				</div></body></text></TEI>