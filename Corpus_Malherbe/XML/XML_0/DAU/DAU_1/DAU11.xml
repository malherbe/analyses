<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">LES AMOUREUSES</title>
				<title type="sub">POEMES ET FANTAISIES</title>
				<title type="medium">Édition électronique</title>
				<author key="DAU">
					<name>
						<forename>Alphonse</forename>
						<surname>DAUDET</surname>
					</name>
					<date from="1840" to="1897">1840-1897</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1472 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DAU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LES AMOUREUSES, POEMES ET FANTAISIES</title>
						<author>Alphonse Daudet</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/Les_Amoureuses</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
							<title type="main">LES AMOUREUSES, POEMES ET FANTAISIES</title>
								<author>Alphonse Daudet</author>
								<edition>NOUVELLE ÉDITION</edition>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BIBLIOTHÈQUE-CHARPENTIER, Eugène Fasquelle, éditeur</publisher>
									<date when="1908">1908</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1858">1858</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties en prose ne sont pas reprises dans cette édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Le recueil a été découpé en parties et sous parties conformément à l’édition de 1882.</p>
				<correction>
					<p>Les majuscules des noms propres ont été restituées</p>
				</correction>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-12-17" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-12-17" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LES AMOUREUSES</head><div type="poem" key="DAU11">
					<head type="main">À CLAIRETTE</head>
					<lg n="1">
						<l n="1" num="1.1">Croyez-moi, mignonne, avec l’amourette</l>
						<l n="2" num="1.2">Que nous gaspillons à deux, chaque jour</l>
						<l n="3" num="1.3">(Ne vous moquez pas trop de moi, Clairette),</l>
						<l n="4" num="1.4">On pourrait encor faire un peu d’amour.</l>
						<l n="5" num="1.5">On fait de l’amour avec l’amourette.</l>
					</lg>
					<lg n="2">
						<l n="6" num="2.1">Qui sait ? connaissons un peu mieux nos cœurs.</l>
						<l n="7" num="2.2">Qui sait ? cherchons bien…pardon, je m’arrête ;</l>
						<l n="8" num="2.3">Vous avez la bouche et l’œil trop moqueurs</l>
						<l n="9" num="2.4">(Ne vous moquez pas trop de moi, Clairette) :</l>
						<l n="10" num="2.5">Qui sait ? connaissons un peu mieux nos cœurs.</l>
					</lg>
					<lg n="3">
						<l n="11" num="3.1">Voyons, si j’avais dans quelque retraite</l>
						<l n="12" num="3.2">Le nid que je rêve et que j’ai cherché,</l>
						<l n="13" num="3.3">(Ne vous moquez pas trop de moi, Clairette),</l>
						<l n="14" num="3.4">On aime bien mieux quand on est caché.</l>
						<l n="15" num="3.5">Si j’avais un nid dans quelque retraite !</l>
					</lg>
					<lg n="4">
						<l n="16" num="4.1">Un nid ! des vallons bien creux, bien perdus.</l>
						<l n="17" num="4.2">Plus de falbalas, plus de cigarette ;</l>
						<l n="18" num="4.3">Champagne et mâcon seraient défendus,</l>
						<l n="19" num="4.4">(Ne vous moquez pas trop de moi, Clairette)…</l>
						<l n="20" num="4.5">Un nid, des vallons bien creux, bien perdus.</l>
					</lg>
					<lg n="5">
						<l n="21" num="5.1">Quel bonheur de vivre en anachorète,</l>
						<l n="22" num="5.2">Des fleurs et vos yeux pour tout horizon,</l>
						<l n="23" num="5.3">(Ne vous moquez pas trop de moi, Clairette) !</l>
						<l n="24" num="5.4">Par le dieu Plutus, j’ai quelque raison</l>
						<l n="25" num="5.5">Pour désirer vivre en anachorète.</l>
					</lg>
					<lg n="6">
						<l n="26" num="6.1">Eh bien ! cher amour, la nature est prête,</l>
						<l n="27" num="6.2">Le nid vous attend… Comment ! vous riez ?</l>
						<l n="28" num="6.3">(Ne vous moquez pas trop de moi, Clairette),</l>
						<l n="29" num="6.4">C’était pour savoir ce que vous diriez.</l>
					</lg>
				</div></body></text></TEI>