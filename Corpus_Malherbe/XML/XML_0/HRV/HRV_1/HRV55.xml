<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">DILECTION</title>
				<title type="medium">Édition électronique</title>
				<author key="HRV">
					<name>
						<forename>Henriette</forename>
						<surname>HERVÉ</surname>
					</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3324 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">HRV_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title type="main">DILECTION</title>
						<author>Henriette Hervé</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ÉDITIONS MONTAIGNE</publisher>
							<date when="1925">1925</date>
						</imprint>
					</monogr>
					<note>Édition numérisée sur place</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1925">1925</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>.....</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les notes de bas de page ont été reportées en fin de poème.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-05-22" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-05-22" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">TRIPTYQUE</head><div type="poem" key="HRV55" rhyme="none">
					<head type="number">III</head>
					<head type="main">LA CATHÉDRALE</head>
					<head type="sub_1">(Amiens)</head>
					<opener>
						<epigraph>
							<cit>
								<quote>
									"Seigneur j’ai aimé la beauté de votre maison <lb/>
									et le lieu où habite votre gloire"
								</quote>
							</cit>
						</epigraph>
					</opener>
					<lg n="1">
						<l n="1" num="1.1">Aussi loin que tu sois, c’est ici ta maison</l>
						<l n="2" num="1.2"><space unit="char" quantity="8"/>Et le lieu qu’habité ta gloire,</l>
						<l n="3" num="1.3">Car j’ai su te contraindre, à force d’oraisons,</l>
						<l n="4" num="1.4"><space unit="char" quantity="8"/>A t’intégrer à ma mémoire !</l>
						<l n="5" num="1.5">Pleur à pleur, peine à peine, et combat a combat,</l>
						<l n="6" num="1.6">J’ai créé dans mon âme une église idéale</l>
						<l n="7" num="1.7"><space unit="char" quantity="8"/>Où tu reçois, sans intervalles,</l>
						<l n="8" num="1.8">L’hommage de deux chants dont la force est égale</l>
						<l n="9" num="1.9"><space unit="char" quantity="8"/>Dies iræ ! Alléluia !</l>
					</lg>
					<lg n="2">
						<l n="10" num="2.1">Chant de mort, chant de vie, hymnes d’amour tous deux,</l>
						<l n="11" num="2.2"><space unit="char" quantity="8"/>Qui maintenant se réunissent</l>
						<l n="12" num="2.3">Sous la voûte achevée où tu règnes, Beau Dieu !</l>
						<l n="13" num="2.4"><space unit="char" quantity="8"/>Mais dont un seul dit mon supplice</l>
						<l n="14" num="2.5">Aux temps déjà lointains où mon cœur atterré,</l>
						<l n="15" num="2.6">Méconnaissant en soi de fécondes richesses,</l>
						<l n="16" num="2.7"><space unit="char" quantity="8"/>Sans tenter l’effort qui redresse</l>
						<l n="17" num="2.8">N’aspirait qu’au néant et répétait sans cesse</l>
						<l n="18" num="2.9"><space unit="char" quantity="8"/>Dies iræ ! Alléluia !</l>
					</lg>
					<lg n="3">
						<l n="19" num="3.1">C’est ce rythme pesant de désolation</l>
						<l n="20" num="3.2"><space unit="char" quantity="8"/>Qui souleva mon premier geste,</l>
						<l n="21" num="3.3">Qui vint frapper le roc et creuser le sillon</l>
						<l n="22" num="3.4"><space unit="char" quantity="8"/>D’où la basilique céleste</l>
						<l n="23" num="3.5">Devait plus tard lancer ses clochers ajourés !</l>
						<l n="24" num="3.6">C’est lui qui stimulait et ma peine et mon zèle,</l>
						<l n="25" num="3.7"><space unit="char" quantity="8"/>Quand, déployant ses lourdes ailes,</l>
						<l n="26" num="3.8">Il flagellait sans fin tout mon être rebelle !</l>
						<l n="27" num="3.9"><space unit="char" quantity="8"/>Dies iræ ! Alléluia !</l>
					</lg>
					<lg n="4">
						<l n="28" num="4.1">« Je ne veux que mourir !… La mort ne suffit point ! »</l>
						<l n="29" num="4.2"><space unit="char" quantity="8"/>Et le glas suscitait la tâche !</l>
						<l n="30" num="4.3">…Voici la pierre vive et que rien ne disjoint !</l>
						<l n="31" num="4.4"><space unit="char" quantity="8"/>Façonnée à grands coups de hache</l>
						<l n="32" num="4.5">Voici l’assise des douleurs, portant serrés</l>
						<l n="33" num="4.6">Ses arcs-boutants de foi, de rêve, et d’humble audace,</l>
						<l n="34" num="4.7"><space unit="char" quantity="8"/>Voici le parvis de disgrâce,</l>
						<l n="35" num="4.8">Voici l’oblation de mon âme trop lasse !</l>
						<l n="36" num="4.9"><space unit="char" quantity="8"/>Dies iræ ! Dies iræ !</l>
					</lg>
					<lg n="5">
						<l n="37" num="5.1">Voici pourtant encor la force des piliers</l>
						<l n="38" num="5.2"><space unit="char" quantity="8"/>Dans leur persévérance grise !</l>
						<l n="39" num="5.3">Voici l’image et le symbole des pitiés</l>
						<l n="40" num="5.4"><space unit="char" quantity="8"/>Voici les saints qu’on martyrise,</l>
						<l n="41" num="5.5">Et de l’amour souffrant voici les sept degrés</l>
						<l n="42" num="5.6">Et les trois porches d’ombre avec leurs défaillances,</l>
						<l n="43" num="5.7"><space unit="char" quantity="8"/>Voici les narthex du silence,</l>
						<l n="44" num="5.8">Voici le Roi des rois dans sa magnificence !</l>
						<l n="45" num="5.9"><space unit="char" quantity="8"/>Dies iræ ! Dies iræ !</l>
					</lg>
					<lg n="6">
						<l n="46" num="6.1">Voici la flamme haute et les brasiers d’ardeur,</l>
						<l n="47" num="6.2"><space unit="char" quantity="8"/>Voici la torche et l’auréole !</l>
						<l n="48" num="6.3">Voici la lampe éteinte et le feu qui se meurt,</l>
						<l n="49" num="6.4"><space unit="char" quantity="8"/>Offrande sage, offrande folle !</l>
						<l n="50" num="6.5">Voici le mal vivant, voici le verbe vrai,</l>
						<l n="51" num="6.6">La moisson de vertus, la floraison de vices,</l>
						<l n="52" num="6.7"><space unit="char" quantity="8"/>Et les dévastantes blandices</l>
						<l n="53" num="6.8">Des renonciations pleines de paix factice !</l>
						<l n="54" num="6.9"><space unit="char" quantity="8"/>Dies iræ ! Dies iræ !</l>
					</lg>
					<lg n="7">
						<l n="55" num="7.1">Voici l’âme élargie exhaussant en plein ciel</l>
						<l n="56" num="7.2"><space unit="char" quantity="8"/>Ses amples toits de patience,</l>
						<l n="57" num="7.3">Ses clochers de prière et sa flèche d’appel !</l>
						<l n="58" num="7.4"><space unit="char" quantity="8"/>Voici surgir sa triple instance</l>
						<l n="59" num="7.5">L’abside agenouillée et murmurant tout bas,</l>
						<l n="60" num="7.6">Le transept redressé sous un vent de miracle,</l>
						<l n="61" num="7.7"><space unit="char" quantity="8"/>Et, libre enfin de tout obstacle,</l>
						<l n="62" num="7.8">L’immense nef, debout, jetant tours et pinacles !</l>
						<l n="63" num="7.9"><space unit="char" quantity="8"/>Dies iræ ! Dies iræ !</l>
					</lg>
					<lg n="8">
						<l n="64" num="8.1">Car mon âme, étouffant sous l’afflux de l’amour,</l>
						<l n="65" num="8.2"><space unit="char" quantity="8"/>S’est réalisée elle-même</l>
						<l n="66" num="8.3">En se perdant en toi ! Désormais, sans recours</l>
						<l n="67" num="8.4"><space unit="char" quantity="8"/>Au monde extérieur, elle aime !</l>
						<l n="68" num="8.5">Et, laissant autour d’elle, en fabuleux amas,</l>
						<l n="69" num="8.6">Le labeur apparent de ses trois frontispices</l>
						<l n="70" num="8.7"><space unit="char" quantity="8"/>Aux images médiatrices,</l>
						<l n="71" num="8.8">Elle rejoint l’Esprit au sein de l’édifice !</l>
						<l n="72" num="8.9"><space unit="char" quantity="8"/>Alléluia ! Alléluia !</l>
					</lg>
					<lg n="9">
						<l n="73" num="9.1">Elle possède enfin, dans sa simplicité,</l>
						<l n="74" num="9.2"><space unit="char" quantity="8"/>Sans figuration sensible,</l>
						<l n="75" num="9.3">Ta pure pensée, Amour, ta divinité !</l>
						<l n="76" num="9.4"><space unit="char" quantity="8"/>C’est pourquoi ton temple invisible</l>
						<l n="77" num="9.5">N’est, dans sa profondeur, qu’un élan, qu’un éclat,</l>
						<l n="78" num="9.6">Qu’un long effort votif, qu’un vibrant témoignage,</l>
						<l n="79" num="9.7"><space unit="char" quantity="8"/>Et sa nudité sans image</l>
						<l n="80" num="9.8">S’imprègne de ton feu chaque jour davantage !</l>
						<l n="81" num="9.9"><space unit="char" quantity="8"/>Alléluia ! Alléluia !</l>
					</lg>
					<lg n="10">
						<l n="82" num="10.1">Mon âme te contient, Raison de ma raison,</l>
						<l n="83" num="10.2"><space unit="char" quantity="8"/>Fixée au centre de ses voûtes !</l>
						<l n="84" num="10.3">Pensée et sentiment, voilà tes étançons</l>
						<l n="85" num="10.4"><space unit="char" quantity="8"/>Leur force à ta force s’ajoute</l>
						<l n="86" num="10.5">Et vous formez ensemble un triangle adéquat,</l>
						<l n="87" num="10.6">Un tiers-point sans défaut, une ogive parfaite,</l>
						<l n="88" num="10.7"><space unit="char" quantity="8"/>A travers quoi, du sol au faite,</l>
						<l n="89" num="10.8">S’irradiera la cathédrale un jour de fête !</l>
						<l n="90" num="10.9"><space unit="char" quantity="8"/>Alléluia ! Alléluia !</l>
					</lg>
					<lg n="11">
						<l n="91" num="11.1">C’est ta maison… Elle est sans calme et sans repos,</l>
						<l n="92" num="11.2"><space unit="char" quantity="8"/>Ses moindres pierres te célèbrent !</l>
						<l n="93" num="11.3">Elle est sans ornements, sans somptueux vitraux</l>
						<l n="94" num="11.4"><space unit="char" quantity="8"/>Comme elle est aussi sans ténèbres !</l>
						<l n="95" num="11.5">L’élan de ses piliers se perd dans l’Au-delà</l>
						<l n="96" num="11.6">Car leur effort a dépassé mon sacrifice,</l>
						<l n="97" num="11.7"><space unit="char" quantity="8"/>Et sa lumière excitatrice</l>
						<l n="98" num="11.8">Fait tendre encor plus haut les faisceaux qu’ils déplissent !</l>
						<l n="99" num="11.9"><space unit="char" quantity="8"/>Alléluia ! Alléluia</l>
					</lg>
					<lg n="12">
						<l n="100" num="12.1">Mon appel t’y retient et j’y reçois sans fin</l>
						<l n="101" num="12.2"><space unit="char" quantity="8"/>Les dictamen de ta présence !</l>
						<l n="102" num="12.3">Je n’ai plus qu’à suspendre un mystique lien,</l>
						<l n="103" num="12.4"><space unit="char" quantity="8"/>Emblème de cette alliance,</l>
						<l n="104" num="12.5">Autour du sanctuaire… Et, dans tout son éclat,</l>
						<l n="105" num="12.6">Voici, Beau Dieu, la guirlande de feuilles vives,</l>
						<l n="106" num="12.7"><space unit="char" quantity="8"/>Où, d’arceaux en arceaux, s’avive</l>
						<l n="107" num="12.8">L’immarcescible fleur de la vie unitive !</l>
						<l n="108" num="12.9"><space unit="char" quantity="8"/>Alléluia ! Alléluia !</l>
					</lg>
					<lg n="13">
						<l n="109" num="13.1">Mais ce chant qui poursuit son tumulte obstiné ,</l>
						<l n="110" num="13.2"><space unit="char" quantity="8"/>Sauras-tu jamais le comprendre ?</l>
						<l n="111" num="13.3">Il traduit un orgueil aussitôt mort que né,</l>
						<l n="112" num="13.4"><space unit="char" quantity="8"/>Des pleurs qu’on ne veut plus répandre,</l>
						<l n="113" num="13.5">Un travail que chaque heure ébauche et brise, hélas !</l>
						<l n="114" num="13.6">Il est ma volonté mais n’est pas mon courage,</l>
						<l n="115" num="13.7"><space unit="char" quantity="8"/>Et quand vient alterner l’hommage</l>
						<l n="116" num="13.8">C’est son cri triomphal, crois-moi, le plus sauvage !</l>
						<l n="117" num="13.9"><space unit="char" quantity="8"/>Dies iræ ! Alléluia !</l>
					</lg>
				</div></body></text></TEI>