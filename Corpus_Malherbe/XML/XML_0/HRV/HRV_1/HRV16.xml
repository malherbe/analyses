<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">DILECTION</title>
				<title type="medium">Édition électronique</title>
				<author key="HRV">
					<name>
						<forename>Henriette</forename>
						<surname>HERVÉ</surname>
					</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3324 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">HRV_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title type="main">DILECTION</title>
						<author>Henriette Hervé</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ÉDITIONS MONTAIGNE</publisher>
							<date when="1925">1925</date>
						</imprint>
					</monogr>
					<note>Édition numérisée sur place</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1925">1925</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>.....</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les notes de bas de page ont été reportées en fin de poème.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-05-22" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-05-22" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">SUIVANT L’HUMEUR DES JOURS</head><head type="main_subpart">TROIS CHANSONS SUR UN THÈME ANCIEN</head><div type="poem" key="HRV16">
					<head type="main">COMPLAINTE</head>
						<lg n="1">
							<l n="1" num="1.1">Le premier jour… L’aube est sans. doute</l>
							<l n="2" num="1.2">Trop heureuse pour s’effacer</l>
							<l n="3" num="1.3">Et reste aux pommiers de la route !</l>
							<l n="4" num="1.4">Le ciel rit dans l’eau des fossés…</l>
							<l n="5" num="1.5">Un ciel frais éclos… Et j’écoute</l>
							<l n="6" num="1.6">Tous les baisers de fiancés</l>
							<l n="7" num="1.7">Qu’apporte le vent. Passe, tendre</l>
							<l n="8" num="1.8">La journée… Ah ! Seigneur très bon !</l>
							<l n="9" num="1.9">L’aube illumine encor mon front…</l>
							<l n="10" num="1.10"><space unit="char" quantity="6"/>Et c’est doux d’attendre…</l>
							<l rhyme="none" n="11" num="1.11"><space unit="char" quantity="6"/>Laire, laire, Lon</l>
						</lg>
						<lg n="2">
							<l n="12" num="2.1">Le second jour… L’aube a sans doute</l>
							<l n="13" num="2.2">Joué son sort, la nuit, aux dés…</l>
							<l n="14" num="2.3">Elle est rouge et court sur la route !</l>
							<l n="15" num="2.4">En passant, sa joue a fardé</l>
							<l n="16" num="2.5">Tous les arbres des bois… J’écoute…</l>
							<l n="17" num="2.6">Est-ce le cri des possédés ?</l>
							<l n="18" num="2.7">Le père en tremble sur ses jambes !</l>
							<l n="19" num="2.8">Au feu ! au feu ! Seigneur, plains-la !</l>
							<l n="20" num="2.9">…L’aube a brûlé mon toit, hélas !</l>
							<l n="21" num="2.10"><space unit="char" quantity="6"/>Et ma maison flambe…</l>
							<l rhyme="none" n="22" num="2.11"><space unit="char" quantity="6"/>Laire, laire, lon !</l>
						</lg>
						<lg n="3">
							<l n="23" num="3.1">Le dernier jour… l’aube est sans doute</l>
							<l n="24" num="3.2">Bien trop lasse pour se lever…</l>
							<l n="25" num="3.3">Des corbeaux, au loin sur la route,</l>
							<l n="26" num="3.4">Achèvent un cheval crevé.</l>
							<l n="27" num="3.5">Il neige, et j’ai si froid… J’écoute</l>
							<l n="28" num="3.6">Quand la cloche sonne un ave,</l>
							<l n="29" num="3.7">Le glas de l’écho… Passe, lente,</l>
							<l n="30" num="3.8">La journée…. Ah ! j’ai mal, Seigneur !</l>
							<l n="31" num="3.9">Car le froid m’a gelé le cœur</l>
							<l n="32" num="3.10"><space unit="char" quantity="6"/>Et la mort me hante !…</l>
						</lg>
					</div></body></text></TEI>