<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">DILECTION</title>
				<title type="medium">Édition électronique</title>
				<author key="HRV">
					<name>
						<forename>Henriette</forename>
						<surname>HERVÉ</surname>
					</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3324 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">HRV_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title type="main">DILECTION</title>
						<author>Henriette Hervé</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ÉDITIONS MONTAIGNE</publisher>
							<date when="1925">1925</date>
						</imprint>
					</monogr>
					<note>Édition numérisée sur place</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1925">1925</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>.....</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les notes de bas de page ont été reportées en fin de poème.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-05-22" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-05-22" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CHANTS DE PALESTINE</head><div type="poem" key="HRV39">
					<head type="main">LA MOISSON</head>
					<opener>
						<epigraph>
							<cit>
								<quote>« Réveille-toi, ô Sulamite »</quote>
							</cit>
						</epigraph>
					</opener>
					<lg n="1">
						<l n="1" num="1.1">Le soleil de midi brûle les chaumes roux</l>
						<l n="2" num="1.2">Où nous avons coupé la moisson depuis l’aube ;</l>
						<l n="3" num="1.3">C’est, la trêve du jour, la sieste aux rêves fous ;</l>
						<l n="4" num="1.4">A mon cœur embrasé, le sommeil se dérobe…</l>
						<l n="5" num="1.5">Et toi, tu dors, à l’ombre exquise des figuiers,</l>
						<l n="6" num="1.6">Ayant posé ton front sur ton bras replié…</l>
						<l n="7" num="1.7">Les cigales pourtant font résonner leurs sistres,</l>
						<l n="8" num="1.8">Mêlés aux tambourins des frelons vagabonds ;</l>
						<l n="9" num="1.9">Mais rien ne peut troubler le complet abandon</l>
						<l n="10" num="1.10">De ton repos… Tes yeux, que la fatigue bistre,</l>
						<l n="11" num="1.11">Restent fermés aux bruits ; ils sont clos à la voix</l>
						<l n="12" num="1.12">Des désirs déchaînés comme des chiens avides,</l>
						<l n="13" num="1.13">Et dont la violence aboie et gronde en moi !</l>
						<l n="14" num="1.14">Et des champs désertés, de tous ces vergers vides,</l>
						<l n="15" num="1.15">Monte une odeur de fruits entr’ouverts au soleil…</l>
						<l n="16" num="1.16">Ah ! lorsque tu viendras ‒ secouant le sommeil</l>
						<l n="17" num="1.17">Qui te reprend, et par moment t’accable encor ‒</l>
						<l n="18" num="1.18">Pour te désaltérer enfin avec lenteur,</l>
						<l n="19" num="1.19">Et que nonchalamment, tu me tendras l’amphore,</l>
						<l n="20" num="1.20">Je ne pourrai goûter que ta fauve moiteur !…</l>
					</lg>
				</div></body></text></TEI>