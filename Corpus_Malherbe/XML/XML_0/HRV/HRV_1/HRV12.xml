<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">DILECTION</title>
				<title type="medium">Édition électronique</title>
				<author key="HRV">
					<name>
						<forename>Henriette</forename>
						<surname>HERVÉ</surname>
					</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3324 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">HRV_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title type="main">DILECTION</title>
						<author>Henriette Hervé</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ÉDITIONS MONTAIGNE</publisher>
							<date when="1925">1925</date>
						</imprint>
					</monogr>
					<note>Édition numérisée sur place</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1925">1925</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>.....</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les notes de bas de page ont été reportées en fin de poème.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-05-22" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-05-22" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">SUIVANT L’HUMEUR DES JOURS</head><div type="poem" key="HRV12">
				<head type="main">LORSQUE JE SERAI MORTE</head>
					<lg n="1">
						<l n="1" num="1.1">Lorsque je serai morte, ah ! ne m’enfermez pas !</l>
						<l n="2" num="1.2">Laissez-moi sans tombeau, sans grille qui me fasse</l>
						<l n="3" num="1.3">Sentir une barrière au-delà du trépas !</l>
						<l n="4" num="1.4">Ne rationnez plus ma mesure d’espace !</l>
						<l n="5" num="1.5">Puisque vivante j’ai plié devant les lois,</l>
						<l n="6" num="1.6"><space unit="char" quantity="12"/>Morte, libérez-moi !</l>
					</lg>
					<lg n="2">
						<l n="7" num="2.1">Que je n’éprouve plus d’écrasantes contraintes !</l>
						<l n="8" num="2.2">‒ Ne posez pas de dalle au-dessus de mon cœur ‒</l>
						<l n="9" num="2.3">Qu’aucun obscur respect ne m’impose une feinte</l>
						<l n="10" num="2.4">‒ Ne dressez pas de croix, ne versez pas de pleurs ‒</l>
						<l n="11" num="2.5">Et, pour qu’en mon cercueil, j’oublie enfin l’entrave,</l>
						<l n="12" num="2.6"><space unit="char" quantity="12"/>Qu’aucun nom ne s’y grave !</l>
					</lg>
					<lg n="3">
						<l n="13" num="3.1">Laissez se disperser mes cendres au dehors !</l>
						<l n="14" num="3.2">Ne me concédez pas, ainsi que fait -la vie,</l>
						<l n="15" num="3.3">La place exacte et les limites de mon corps,</l>
						<l n="16" num="3.4">Mais que le vent du ciel prenne, emporte, amplifie,</l>
						<l n="17" num="3.5">Ma poussière anonyme et ce qu’elle contint</l>
						<l n="18" num="3.6"><space unit="char" quantity="12"/>De désirs non éteints !</l>
					</lg>
					<lg n="4">
						<l n="19" num="4.1">Quand j’aurai dépouillé cette forme précise</l>
						<l n="20" num="4.2">Je ne connaîtrai plus de devoirs ni de droits !</l>
						<l n="21" num="4.3">J’échapperai dès lors à toutes les emprises</l>
						<l n="22" num="4.4">Aux vôtres, êtres chers ! qui m’imposiez un choix</l>
						<l n="23" num="4.5">Par mon sentiment même ; aux tiennes, conscience,</l>
						<l n="24" num="4.6"><space unit="char" quantity="12"/>Si dure à mes offenses !</l>
					</lg>
					<lg n="5">
						<l n="25" num="5.1">Et je pourrai ne suivre, enfin, que le Hasard !</l>
						<l n="26" num="5.2">Ce Hasard dont l’ivresse, irresponsable et chaude,</l>
						<l n="27" num="5.3">Ne m’a pas terrassée, esprit et corps épars,</l>
						<l n="28" num="5.4">C’est lui qui mènera les nocturnes maraudes</l>
						<l n="29" num="5.5">Où les morts vont glaner les bonheurs défendus</l>
						<l n="30" num="5.6"><space unit="char" quantity="12"/>Que, vifs, ils n’ont pas eus !</l>
					</lg>
					<lg n="6">
						<l n="31" num="6.1">Oui ! moi qui ne voulais m’engager sur les routes</l>
						<l n="32" num="6.2">Qu’après avoir compté leurs tournants périlleux,</l>
						<l n="33" num="6.3">J’irai, sans un scrupule, en poussière dissoute,</l>
						<l n="34" num="6.4">Danser sur le chemin qui me plaira le mieux !</l>
						<l n="35" num="6.5">Vers quoi ?… Vers l’horizon d’un mirage lunaire</l>
						<l n="36" num="6.6"><space unit="char" quantity="12"/>Ou le creux d’une ornière !</l>
					</lg>
					<lg n="7">
						<l n="37" num="7.1">Qu’importe ! Sans prévoir, sans craindre ou mesurer !</l>
						<l n="38" num="7.2">J’irai, comme au printemps les pollens dans la brise !</l>
						<l n="39" num="7.3">Pensent-ils, croyez-vous, lorsqu’ils vont à leur gré</l>
						<l n="40" num="7.4">Mêler leurs poudres d’or de cytise à cytise,</l>
						<l n="41" num="7.5">Ou porter aux pommiers l’amertume des buis</l>
						<l n="42" num="7.6"><space unit="char" quantity="12"/>Sans souci de leurs fruits ?</l>
					</lg>
					<lg n="8">
						<l n="43" num="8.1">C’est comme eux que je veux suivre ma fantaisie !</l>
						<l n="44" num="8.2">Pour que la douce nuit fasse entr’ouvrir mon cœur,</l>
						<l n="45" num="8.3">Que mes désirs, non contenus, se rassasient,</l>
						<l n="46" num="8.4">Que ma cendre impalpable, encor pleine d’ardeurs,</l>
						<l n="47" num="8.5">Mais dépourvue enfin d’an excès de sagesse,</l>
						<l n="48" num="8.6"><space unit="char" quantity="12"/>S’envole et se connaisse !</l>
					</lg>
					<lg n="9">
						<l n="49" num="9.1">Et qu’elle ose, elle qui n’aura plus de remords,</l>
						<l n="50" num="9.2">Qu’elle ose l’action, si simple et si sauvage,</l>
						<l n="51" num="9.3">Dont ma main frémissante a contenu l’effort</l>
						<l n="52" num="9.4">Sans l’accomplir, qu’elle ose enfin ! sur son passage,</l>
						<l n="53" num="9.5">Se poser largement, comme pour conquérir,</l>
						<l n="54" num="9.6"><space unit="char" quantity="12"/>Par droit de son désir !</l>
					</lg>
					<lg n="10">
						<l n="55" num="10.1">Qu’elle aille s’emparer des biens qu’elle convoite,</l>
						<l n="56" num="10.2">La poussière de mon pauvre corps affamé !</l>
						<l n="57" num="10.3">…Qu’elle aille caresser les petits cous tout moites</l>
						<l n="58" num="10.4">Des poupons endormis, et, qu’au contact aimé,</l>
						<l n="59" num="10.5">Elle ait l’illusion d’être encore féconde</l>
						<l n="60" num="10.6"><space unit="char" quantity="12"/>Et d’enfanter un monde !</l>
					</lg>
					<lg n="11">
						<l n="61" num="11.1">Qu’elle aille se glisser aux lèvres des amants</l>
						<l n="62" num="11.2">A l’instant où l’aveu, quel qu’il soit en paroles,</l>
						<l n="63" num="11.3">N’étant plus qu’un instinct, les unit simplement,</l>
						<l n="64" num="11.4">Et que leur long baiser la presse et la console</l>
						<l n="65" num="11.5">De l’étreinte qu’elle a, jusque dans sa pensée,</l>
						<l n="66" num="11.6"><space unit="char" quantity="12"/>Par devoir renoncée !</l>
					</lg>
					<lg n="12">
						<l n="67" num="12.1">Et qu’elle aille, pendant cette nuit, se poser</l>
						<l n="68" num="12.2">Pour partager l’émoi de quelque sombre couple</l>
						<l n="69" num="12.3">Que n’a pu contenter la saveur du baiser,</l>
						<l n="70" num="12.4">Quand l’un d’eux, fou d’amour, aura d’un couteau souple,</l>
						<l n="71" num="12.5">Fendu le cœur étroit, trop calme à son souhait,</l>
						<l n="72" num="12.6"><space unit="char" quantity="12"/>Qu’il adore et qu’il hait !</l>
					</lg>
					<lg n="13">
						<l n="73" num="13.1">Que ma poussière alors s’imprègne goutte à goutte</l>
						<l n="74" num="13.2">De ce beau sang profond qui coule lentement</l>
						<l n="75" num="13.3">Et contient le secret qu’on cherche et qu’on redoute !</l>
						<l n="76" num="13.4">Car j’aurai passé l’heure où soi-même on se ment,</l>
						<l n="77" num="13.5">Et je m’en irai droit vers les troubles délires</l>
						<l n="78" num="13.6"><space unit="char" quantity="12"/>Dont le mystère attire !</l>
					</lg>
					<lg n="14">
						<l n="79" num="14.1">Ah ! vous n’avez connu de moi que la pudeur</l>
						<l n="80" num="14.2">Et la réserve et la tenue et le silence,</l>
						<l n="81" num="14.3">Mais moi je le savais que le cœur de mon cœur</l>
						<l n="82" num="14.4">Défaillait par excès et non pas par carence,</l>
						<l n="83" num="14.5">Et je savais aussi quel strangulant étau</l>
						<l n="84" num="14.6"><space unit="char" quantity="12"/>Enserrait ses sursauts !</l>
					</lg>
					<lg n="15">
						<l n="85" num="15.1">N’est pas libre qui veut ! J’avais ma conscience</l>
						<l n="86" num="15.2">Qui veillait attentive à mes moindres écarts,</l>
						<l n="87" num="15.3">Et j’avais beau haïr sa détestable outrance</l>
						<l n="88" num="15.4">Je l’ai subie !… Alors quand il sera trop tard</l>
						<l n="89" num="15.5">Pour le bonheur, qu’au moins ma cendre se soulève</l>
						<l n="90" num="15.6"><space unit="char" quantity="12"/>Vers l’ombre de ses rêves…</l>
					</lg>
					<lg n="16">
						<l n="91" num="16.1">Ne posez pas de dalle au-dessus de mon cœur !</l>
						<l n="92" num="16.2">‒ Que je n’éprouve plus d’écrasantes contraintes ‒</l>
						<l n="93" num="16.3">Ne dressez, pas de croix, ne versez pas de pleurs !</l>
						<l n="94" num="16.4">‒ Qu’aucun obscur respect ne m’impose une feinte ‒</l>
						<l n="95" num="16.5">Mais, quand je serai morte, absolvez-moi tout bas</l>
						<l n="96" num="16.6"><space unit="char" quantity="12"/>Et ne m’enfermez pas !</l>
					</lg>
				</div></body></text></TEI>