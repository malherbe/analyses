<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">DILECTION</title>
				<title type="medium">Édition électronique</title>
				<author key="HRV">
					<name>
						<forename>Henriette</forename>
						<surname>HERVÉ</surname>
					</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3324 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">HRV_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title type="main">DILECTION</title>
						<author>Henriette Hervé</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ÉDITIONS MONTAIGNE</publisher>
							<date when="1925">1925</date>
						</imprint>
					</monogr>
					<note>Édition numérisée sur place</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1925">1925</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>.....</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les notes de bas de page ont été reportées en fin de poème.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-05-22" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-05-22" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CHANTS DE PALESTINE</head><div type="poem" key="HRV45">
				<head type="main">LE MIROIR D’ARGENT</head>
					<lg n="1">
						<l n="1" num="1.1"><space unit="char" quantity="8"/>Mon bien-aimé, tu m’as fait don</l>
						<l n="2" num="1.2">D’un peigne d’or, d’un voile et de ce collier rond ;</l>
						<l n="3" num="1.3">Mais je ne veux garder, de toutes ces richesses,</l>
						<l n="4" num="1.4"><space unit="char" quantity="8"/>Qu’un souvenir de ta tendresse.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><space unit="char" quantity="8"/>Qu’ai-je besoin de tant d’atours ?</l>
						<l n="6" num="2.2">Ta rose de Sâron, ton muguet des vallées</l>
						<l n="7" num="2.3">Ne saurait pas planter un peigne, ô mon amour !</l>
						<l n="8" num="2.4"><space unit="char" quantity="8"/>Dans ses boucles trop emmêlées…</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><space unit="char" quantity="8"/>Le soir, tu défais mes cheveux</l>
						<l n="10" num="3.2">Pour t’enivrer d’odeur, dans leur toison épaisse</l>
						<l n="11" num="3.3">Pendant la nuit… quand ils glissent entre nous deux…</l>
						<l n="12" num="3.4"><space unit="char" quantity="8"/>Et le matin, tu me les tresses…</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><space unit="char" quantity="8"/>Pour tout collier donne tes bras !</l>
						<l n="14" num="4.2">Que leur étroite étreinte, encor presque sauvage,</l>
						<l n="15" num="4.3">Laisse à mon cou meurtri la pourpre de leurs lacs,</l>
						<l n="16" num="4.4"><space unit="char" quantity="8"/>La trace de leur cher passage !</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><space unit="char" quantity="8"/>Veux-tu que ce voile brodé</l>
						<l n="18" num="5.2">Empêche ton regard de brûler mon visage ?</l>
						<l n="19" num="5.3">Que son frêle tissu vienne encor retarder</l>
						<l n="20" num="5.4"><space unit="char" quantity="8"/>La caresse qui me saccage ?</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><space unit="char" quantity="8"/>Reprends, reprends tous ces présents…</l>
						<l n="22" num="6.2">Je ne veux conserver qu’un petit miroir terne</l>
						<l n="23" num="6.3">Qui luit ainsi que l’onde obscure d’un étang,</l>
						<l n="24" num="6.4"><space unit="char" quantity="8"/>Ou comme l’eau d’une citerne ;</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><space unit="char" quantity="8"/>Car j’aime à le voir s’animer</l>
						<l n="26" num="7.2">Quand vient fleurir, sur sa surface lisse et nue.</l>
						<l n="27" num="7.3">Le mobile reflet de ta bouche charnue</l>
						<l n="28" num="7.4"><space unit="char" quantity="8"/>Qui me sourit… mon bien-aimé !</l>
					</lg>
				</div></body></text></TEI>