<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">DILECTION</title>
				<title type="medium">Édition électronique</title>
				<author key="HRV">
					<name>
						<forename>Henriette</forename>
						<surname>HERVÉ</surname>
					</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3324 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">HRV_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title type="main">DILECTION</title>
						<author>Henriette Hervé</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ÉDITIONS MONTAIGNE</publisher>
							<date when="1925">1925</date>
						</imprint>
					</monogr>
					<note>Édition numérisée sur place</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1925">1925</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>.....</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les notes de bas de page ont été reportées en fin de poème.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-05-22" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-05-22" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">SUIVANT L’HUMEUR DES JOURS</head><div type="poem" key="HRV2">
					<head type="main">J’AVAIS RÊVÉ D’UN GRAND BONHEUR SILENCIEUX</head>
					<lg n="1">
						<l n="1" num="1.1">J’avais rêvé d’un grand bonheur silencieux</l>
						<l n="2" num="1.2">Où nous aurions glissé sans connaître d’adieux,</l>
						<l n="3" num="1.3"><space unit="char" quantity="8"/>Comme les flots de deux rivières</l>
						<l n="4" num="1.4">Coulent un jour d’été sous la pleine lumière,</l>
						<l n="5" num="1.5">Distincts et confondus, murmurants et muets,</l>
						<l n="6" num="1.6"><space unit="char" quantity="8"/>Chaque goutte un monde secret…</l>
						<l n="7" num="1.7">Tu m’aurais possédée alors comme l’eau noie,</l>
						<l n="8" num="1.8">Et de toi seul j’aurais reçu toute ma joie !</l>
					</lg>
					<lg n="2">
						<l n="9" num="2.1">Puis j’ai rêvé de ce bonheur qui tremble un peu,</l>
						<l n="10" num="2.2">Comme au-dessus des champs vacille l’air en feu…</l>
						<l n="11" num="2.3"><space unit="char" quantity="8"/>Mais je savais la moisson prête,</l>
						<l n="12" num="2.4">Tu pouvais la faucher en dépit des tempêtes…</l>
						<l n="13" num="2.5">Je te voyais debout, tout prêt à recevoir</l>
						<l n="14" num="2.6"><space unit="char" quantity="8"/>Les lourds épis gonflés d’espoir</l>
						<l n="15" num="2.7">Où se mêlaient des fleurs et la fraîcheur des herbes…</l>
						<l n="16" num="2.8">Et tes bras seuls pouvaient lier toute la gerbe…</l>
					</lg>
					<lg n="3">
						<l n="17" num="3.1">Et puis, j’ai cru que le bonheur pouvait pleurer…</l>
						<l n="18" num="3.2">Qu’à ton front s’appuierait mon front désespéré,</l>
						<l n="19" num="3.3"><space unit="char" quantity="8"/>Que mes larmes silencieuses</l>
						<l n="20" num="3.4">Fleuriraient sur tes doigts leurs pâles scabieuses,</l>
						<l n="21" num="3.5">Et que, devant la tombe où dormirait l’amour,</l>
						<l n="22" num="3.6"><space unit="char" quantity="8"/>Je sentirais mon cœur trop lourd</l>
						<l n="23" num="3.7">Retentir sur le tien dans un rythme si tendre,</l>
						<l n="24" num="3.8">Que même seule, ah ! j’aurais pu, toujours l’entendre !</l>
					</lg>
					<lg n="4">
						<l n="25" num="4.1">Plus tard, j’ai su que la douleur devait parler…</l>
						<l n="26" num="4.2">J’ai retrouvé les mots de tous les exilés</l>
						<l n="27" num="4.3"><space unit="char" quantity="8"/>Et leur goût de cendre et d’argile…</l>
						<l n="28" num="4.4">Je n’ai pu t’ébranler, ô toi ! force tranquille,</l>
						<l n="29" num="4.5">Qui voulais ignorer jusqu’à tes vrais désirs.</l>
						<l n="30" num="4.6"><space unit="char" quantity="8"/>Qui déchirais pour désunir,</l>
						<l n="31" num="4.7">Et qui fis s’étrangler tout écho de ma plainte,</l>
						<l n="32" num="4.8">Quand pour toi seul, elle sonnait, moins indistincte !</l>
					</lg>
					<lg n="5">
						<l n="33" num="5.1">Alors j’appris que la douleur pouvait crier !</l>
						<l n="34" num="5.2">Et j’ai violé moi-même un secret meurtrier</l>
						<l n="35" num="5.3"><space unit="char" quantity="8"/>Où s’étouffait mon cœur malade !</l>
						<l n="36" num="5.4">Oui, j’ai pleuré, gémi, hurlé, comme on s’évade !…</l>
						<l n="37" num="5.5">Et pourtant je ne sais encore si j’atteins</l>
						<l n="38" num="5.6"><space unit="char" quantity="8"/>L’unique but que je veux mien !…</l>
						<l n="39" num="5.7">D’autres ont entendu clamer mon chant farouche,</l>
						<l n="40" num="5.8">Fait pour toi seul, pour que le dise enfin ta bouche,</l>
					</lg>
					<lg n="6">
						<l n="41" num="6.1">Et j’ai senti qu’on profanait toute douleur !</l>
						<l n="42" num="6.2">Je t’ai sacrifié cette ultime pudeur…</l>
						<l n="43" num="6.3"><space unit="char" quantity="8"/>…Dans chaque strophe qui reflète</l>
						<l n="44" num="6.4">Un peu de mon amour, de mon âme secrète.</l>
						<l n="45" num="6.5">Quand d’autres m’auront vue, ah ! le seul vrai danger</l>
						<l n="46" num="6.6"><space unit="char" quantity="8"/>C’est que leurs souffles étrangers</l>
						<l n="47" num="6.7">Aient terni le miroir chaque jour davantage</l>
						<l n="48" num="6.8">Et que toi seul, tu méconnaisses mon visage,</l>
					</lg>
					<lg n="7">
						<l n="49" num="7.1">Car j’ai rêvé que tu le voies encor plus beau</l>
						<l n="50" num="7.2">Vainqueur, quoique meurtri, sous le souple bandeau</l>
						<l n="51" num="7.3"><space unit="char" quantity="8"/>D’une fille de Mnémosyne…</l>
						<l n="52" num="7.4">J’userai mes genoux à gravir la colline</l>
						<l n="53" num="7.5">Où croissent en plein ciel les lauriers d’Apollon…</l>
						<l n="54" num="7.6"><space unit="char" quantity="8"/>De quel prix je paierai ce don,</l>
						<l n="55" num="7.7">Le sauras-tu jamais !… et voudras-tu nie croire</l>
						<l n="56" num="7.8">Que c’est pour toi, et pour t’offrir un peu de gloire !</l>
					</lg>
					<lg n="8">
						<l n="57" num="8.1">Ne pense pas que ma douleur pût s’oublier !</l>
						<l n="58" num="8.2">Elle a toujours pleuré sur ces brins de lauriers</l>
						<l n="59" num="8.3"><space unit="char" quantity="8"/>Et plus lourde en est leur couronne…</l>
						<l n="60" num="8.4">J’ai voulu la tresser pour dire : « Je la donne… »</l>
						<l n="61" num="8.5">Tout mon secret espoir serait que tu la veuilles</l>
						<l n="62" num="8.6"><space unit="char" quantity="8"/>Et que j’échange enfin ses feuilles</l>
						<l n="63" num="8.7">Pour des myrtes en fleur que tes mains m’offriraient,</l>
						<l n="64" num="8.8">Car leur couronne est à mon cœur la seule vraie…</l>
					</lg>
				</div></body></text></TEI>