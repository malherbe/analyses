<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES FLEURS DU MAL</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAU">
					<name>
						<forename>Charles</forename>
						<surname>BAUDELAIRE</surname>
					</name>
					<date from="1821" to="1867">1821-1867</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="BB">
						<forename>Benoît</forename>
						<surname>Brard</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4145 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2010">2010</date>
				<idno type="local">BAU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Fleurs du mal</title>
						<author>Charles Baudelaire</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">http://fr.wikisource.org/wiki/Les_Fleurs_du_mal_(1868)</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LES FLEURS DU MAL</title>
								<author>Charles Baudelaire</author>
								<imprint>
									<publisher>Michel Lévy</publisher>
									<date when="1868">1868</date>
								</imprint>
							</monogr>
							<note>3ème édition des Fleurs du mal (posthume).</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title>LES FLEURS DU MAL</title>
					<author>Charles Baudelaire</author>
					<editor>Édouard Maynial</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Société Les Belles Lettres</publisher>
						<date when="1952">1952</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1861">1857-1861</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface de Gautier de Théophile Gautier et l’appendice n’ont pas été encodés.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Importation semi-automatique depuis fr.wikisource.org. Les vers ont été importés au kilomètre. La structuration en poèmes et en strophes a été rajoutée manuellement</p>
				<normalization>
					<p>Normalisation des titres.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
					<change when="2011-09-19" who="RR">Modification en fonction de l’édition de référence (Les Belles Lettres) :insertion des pièces censurées et report des pièces ajoutées dans une partie « Supplément". Les pièces manquantes ont été insérées à partir de la version électronique d’Enclitt avec plusieurs corrections.</change>
					<change when="2011-09-22" who="RR">La balise « space" pour le retrait des vers a été placée dans la balise « l" (dans le vers) et non dans « lg" (entre les vers)</change>
					<change when="2011-09-26" who="RR">Insertion du poème en latin « Franciscae meae laudes"</change>
					<change when="2011-09-27" who="RR">modification du poème « les petites vieilles" : De frascati défunt…</change>
					<change when="2011-09-27" who="RR">découpage en strophes de « Le crépuscule du soir"</change>
					<change when="2011-09-27" who="RR">correction inutile de BB dans « Le vin de l’assassin"</change>
					<change when="2011-09-27" who="RR">correction du titre de « Lesbos" et vers « nuits chauds"</change>
					<change when="2011-09-27" who="RR">correction dans « femmes damnées" loin des peuples…</change>
					<change when="2011-09-27" who="RR">suppression du numéro d’ordre ; remplacé par key</change>
					<change when="2016-01-23" who="RR">Utilisation de l’attribut « lang" pour marquer les vers inanalysables par les programmes Malherbe (BAU64 :vers en latin)</change>
					<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
					<change when="2017-06-27" who="RR">Révision des titres et sous-titres pour une cohérence avec la table des matières.</change>
				</listChange>
			</revisionDesc>
	</teiHeader><text><body><head type="main_part">SPLEEN ET IDÉAL</head><div type="poem" key="BAU7">
					<head type="number">VI</head>
					<head type="main">Les Phares</head>
					<lg n="1">
						<l n="1" num="1.1">Rubens, fleuve d’oubli, jardin de la paresse,</l>
						<l n="2" num="1.2">Oreiller de chair fraîche où l’on ne peut aimer,</l>
						<l n="3" num="1.3">Mais où la vie afflue et s’agite sans cesse,</l>
						<l n="4" num="1.4">Comme l’air dans le ciel et la mer dans la mer ;</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">Léonard de Vinci, miroir profond et sombre,</l>
						<l n="6" num="2.2">Où des anges charmants, avec un doux souris</l>
						<l n="7" num="2.3">Tout chargé de mystère, apparaissent à l’ombre</l>
						<l n="8" num="2.4">Des glaciers et des pins qui ferment leur pays ;</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">Rembrandt, triste hôpital tout rempli de murmures,</l>
						<l n="10" num="3.2">Et d’un grand crucifix décoré seulement,</l>
						<l n="11" num="3.3">Où la prière en pleurs s’exhale des ordures,</l>
						<l n="12" num="3.4">Et d’un rayon d’hiver traversé brusquement ;</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">Michel-Ange, lieu vague où l’on voit des Hercules</l>
						<l n="14" num="4.2">Se mêler à des Christs, et se lever tout droits</l>
						<l n="15" num="4.3">Des fantômes puissants qui dans les crépuscules</l>
						<l n="16" num="4.4">Déchirent leur suaire en étirant leurs doigts ;</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">Colères de boxeur, impudences de faune,</l>
						<l n="18" num="5.2">Toi qui sus ramasser la beauté des goujats,</l>
						<l n="19" num="5.3">Grand cœur gonflé d’orgueil, homme débile et jaune,</l>
						<l n="20" num="5.4">Puget, mélancolique empereur des forçats ;</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1">Watteau, ce carnaval où bien des cœurs illustres,</l>
						<l n="22" num="6.2">Comme des papillons, errent en flamboyant,</l>
						<l n="23" num="6.3">Décors frais et léger éclairés par des lustres</l>
						<l n="24" num="6.4">Qui versent la folie à ce bal tournoyant ;</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1">Goya, cauchemar plein de choses inconnues,</l>
						<l n="26" num="7.2">De fœtus qu’on fait cuire au milieu des sabbats,</l>
						<l n="27" num="7.3">De vieilles au miroir et d’enfants toutes nues,</l>
						<l n="28" num="7.4">Pour tenter les démons ajustant bien leurs bas ;</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1">Delacroix, lac de sang hanté des mauvais anges,</l>
						<l n="30" num="8.2">Ombragé par un bois de sapins toujours vert,</l>
						<l n="31" num="8.3">Où, sous un ciel chagrin, des fanfares étranges</l>
						<l n="32" num="8.4">Passent, comme un soupir étouffé de Weber ;</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1">Ces malédictions, ces blasphèmes, ces plaintes,</l>
						<l n="34" num="9.2">Ces extases, ces cris, ces pleurs, ces Te Deum,</l>
						<l n="35" num="9.3">Sont un écho redit par mille labyrinthes ;</l>
						<l n="36" num="9.4">C’est pour les cœurs mortels un divin opium !</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1">C’est un cri répété par mille sentinelles,</l>
						<l n="38" num="10.2">Un ordre renvoyé par mille porte-voix ;</l>
						<l n="39" num="10.3">C’est un phare allumé sur mille citadelles,</l>
						<l n="40" num="10.4">Un appel de chasseurs perdus dans les grands bois !</l>
					</lg>
					<lg n="11">
						<l n="41" num="11.1">Car c’est vraiment, Seigneur, le meilleur témoignage</l>
						<l n="42" num="11.2">Que nous puissions donner de notre dignité</l>
						<l n="43" num="11.3">Que cet ardent sanglot qui roule d’âge en âge</l>
						<l n="44" num="11.4">Et vient mourir au bord de votre éternité !</l>
					</lg>
				</div></body></text></TEI>