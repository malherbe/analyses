<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES FLEURS DU MAL</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAU">
					<name>
						<forename>Charles</forename>
						<surname>BAUDELAIRE</surname>
					</name>
					<date from="1821" to="1867">1821-1867</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="BB">
						<forename>Benoît</forename>
						<surname>Brard</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4145 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2010">2010</date>
				<idno type="local">BAU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Fleurs du mal</title>
						<author>Charles Baudelaire</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">http://fr.wikisource.org/wiki/Les_Fleurs_du_mal_(1868)</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LES FLEURS DU MAL</title>
								<author>Charles Baudelaire</author>
								<imprint>
									<publisher>Michel Lévy</publisher>
									<date when="1868">1868</date>
								</imprint>
							</monogr>
							<note>3ème édition des Fleurs du mal (posthume).</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title>LES FLEURS DU MAL</title>
					<author>Charles Baudelaire</author>
					<editor>Édouard Maynial</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Société Les Belles Lettres</publisher>
						<date when="1952">1952</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1861">1857-1861</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface de Gautier de Théophile Gautier et l’appendice n’ont pas été encodés.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Importation semi-automatique depuis fr.wikisource.org. Les vers ont été importés au kilomètre. La structuration en poèmes et en strophes a été rajoutée manuellement</p>
				<normalization>
					<p>Normalisation des titres.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
					<change when="2011-09-19" who="RR">Modification en fonction de l’édition de référence (Les Belles Lettres) :insertion des pièces censurées et report des pièces ajoutées dans une partie « Supplément". Les pièces manquantes ont été insérées à partir de la version électronique d’Enclitt avec plusieurs corrections.</change>
					<change when="2011-09-22" who="RR">La balise « space" pour le retrait des vers a été placée dans la balise « l" (dans le vers) et non dans « lg" (entre les vers)</change>
					<change when="2011-09-26" who="RR">Insertion du poème en latin « Franciscae meae laudes"</change>
					<change when="2011-09-27" who="RR">modification du poème « les petites vieilles" : De frascati défunt…</change>
					<change when="2011-09-27" who="RR">découpage en strophes de « Le crépuscule du soir"</change>
					<change when="2011-09-27" who="RR">correction inutile de BB dans « Le vin de l’assassin"</change>
					<change when="2011-09-27" who="RR">correction du titre de « Lesbos" et vers « nuits chauds"</change>
					<change when="2011-09-27" who="RR">correction dans « femmes damnées" loin des peuples…</change>
					<change when="2011-09-27" who="RR">suppression du numéro d’ordre ; remplacé par key</change>
					<change when="2016-01-23" who="RR">Utilisation de l’attribut « lang" pour marquer les vers inanalysables par les programmes Malherbe (BAU64 :vers en latin)</change>
					<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
					<change when="2017-06-27" who="RR">Révision des titres et sous-titres pour une cohérence avec la table des matières.</change>
				</listChange>
			</revisionDesc>
	</teiHeader><text><body><head type="main_part">TABLEAUX PARISIENS</head><div type="poem" key="BAU104">
					<head type="number">C</head>
					<head type="main">Danse macabre</head>
					<opener><salute>À <name>Ernest Christophe</name>.</salute></opener>
					<lg n="1">
						<l n="1" num="1.1">Fière, autant qu’un vivant, de sa noble stature,</l>
						<l n="2" num="1.2">Avec son gros bouquet, son mouchoir et ses gants,</l>
						<l n="3" num="1.3">Elle a la nonchalance et la désinvolture</l>
						<l n="4" num="1.4">D’une coquette maigre aux airs extravagants.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">Vit-on jamais au bal une taille plus mince ?</l>
						<l n="6" num="2.2">Sa robe exagérée, en sa royale ampleur,</l>
						<l n="7" num="2.3">S’écroule abondamment sur un pied sec que pince</l>
						<l n="8" num="2.4">Un soulier pomponné, joli comme une fleur.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">La ruche qui se joue au bord des clavicules,</l>
						<l n="10" num="3.2">Comme un ruisseau lascif qui se frotte au rocher,</l>
						<l n="11" num="3.3">Défend pudiquement des lazzi ridicules</l>
						<l n="12" num="3.4">Les funèbres appas qu’elle tient à cacher.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">Ses yeux profonds sont faits de vide et de ténèbres,</l>
						<l n="14" num="4.2">Et son crâne, de fleurs artistement coiffé,</l>
						<l n="15" num="4.3">Oscille mollement sur ses frêles vertèbres.</l>
						<l n="16" num="4.4">— Ô charme d’un néant follement attifé !</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">Aucuns t’appelleront une caricature,</l>
						<l n="18" num="5.2">Qui ne comprennent pas, amants ivres de chair,</l>
						<l n="19" num="5.3">L’élégance sans nom de l’humaine armature.</l>
						<l n="20" num="5.4">Tu réponds, grand squelette, à mon goût le plus cher !</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1">Viens-tu troubler, avec ta puissante grimace,</l>
						<l n="22" num="6.2">La fête de la Vie ? ou quelque vieux désir,</l>
						<l n="23" num="6.3">Éperonnant encor ta vivante carcasse,</l>
						<l n="24" num="6.4">Te pousse-t-il, crédule, au sabbat du Plaisir ?</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1">Au chant des violons, aux flammes des bougies,</l>
						<l n="26" num="7.2">Espères-tu chasser ton cauchemar moqueur,</l>
						<l n="27" num="7.3">Et viens-tu demander au torrent des orgies</l>
						<l n="28" num="7.4">De rafraîchir l’enfer allumé dans ton cœur ?</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1">Inépuisable puits de sottise et de fautes !</l>
						<l n="30" num="8.2">De l’antique douleur éternel alambic !</l>
						<l n="31" num="8.3">À travers le treillis recourbé de tes côtes</l>
						<l n="32" num="8.4">Je vois, errant encor, l’insatiable aspic.</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1">Pour dire vrai, je crains que ta coquetterie</l>
						<l n="34" num="9.2">Ne trouve pas un prix digne de ses efforts ;</l>
						<l n="35" num="9.3">Qui, de ces cœurs mortels, entend la raillerie ?</l>
						<l n="36" num="9.4">Les charmes de l’horreur n’enivrent que les forts !</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1">Le gouffre de tes yeux, plein d’horribles pensées,</l>
						<l n="38" num="10.2">Exhale le vertige, et les danseurs prudents</l>
						<l n="39" num="10.3">Ne contempleront pas sans d’amères nausées</l>
						<l n="40" num="10.4">Le sourire éternel de tes trente-deux dents.</l>
					</lg>
					<lg n="11">
						<l n="41" num="11.1">Pourtant, qui n’a serré dans ses bras un squelette,</l>
						<l n="42" num="11.2">Et qui ne s’est nourri des choses du tombeau ?</l>
						<l n="43" num="11.3">Qu’importe le parfum, l’habit ou la toilette ?</l>
						<l n="44" num="11.4">Qui fait le dégoûté montre qu’il se croit beau.</l>
					</lg>
					<lg n="12">
						<l n="45" num="12.1">Bayadère sans nez, irrésistible gouge,</l>
						<l n="46" num="12.2">Dis donc à ces danseurs qui font les offusqués :</l>
						<l n="47" num="12.3">« Fiers mignons, malgré l’art des poudres et du rouge,</l>
						<l n="48" num="12.4">Vous sentez tous la mort ! Ô squelettes musqués,</l>
					</lg>
					<lg n="13">
						<l n="49" num="13.1">Antinoüs flétris, dandys à face glabre,</l>
						<l n="50" num="13.2">Cadavres vernissés, lovelaces chenus,</l>
						<l n="51" num="13.3">Le branle universel de la danse macabre</l>
						<l n="52" num="13.4">Vous entraîne en des lieux qui ne sont pas connus !</l>
					</lg>
					<lg n="14">
						<l n="53" num="14.1">Des quais froids de la Seine aux bords brûlants du Gange,</l>
						<l n="54" num="14.2">Le troupeau mortel saute et se pâme, sans voir</l>
						<l n="55" num="14.3">Dans un trou du plafond la trompette de l’Ange</l>
						<l n="56" num="14.4">Sinistrement béante ainsi qu’un tromblon noir</l>
					</lg>
					<lg n="15">
						<l n="57" num="15.1">En tout climat, sous ton soleil, la Mort t’admire</l>
						<l n="58" num="15.2">En tes contorsions, risible Humanité,</l>
						<l n="59" num="15.3">Et souvent, comme toi, se parfumant de myrrhe,</l>
						<l n="60" num="15.4">Mêle son ironie à ton insanité ! »</l>
					</lg>
				</div></body></text></TEI>