<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES FLEURS DU MAL</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAU">
					<name>
						<forename>Charles</forename>
						<surname>BAUDELAIRE</surname>
					</name>
					<date from="1821" to="1867">1821-1867</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="BB">
						<forename>Benoît</forename>
						<surname>Brard</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4145 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2010">2010</date>
				<idno type="local">BAU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Fleurs du mal</title>
						<author>Charles Baudelaire</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">http://fr.wikisource.org/wiki/Les_Fleurs_du_mal_(1868)</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LES FLEURS DU MAL</title>
								<author>Charles Baudelaire</author>
								<imprint>
									<publisher>Michel Lévy</publisher>
									<date when="1868">1868</date>
								</imprint>
							</monogr>
							<note>3ème édition des Fleurs du mal (posthume).</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title>LES FLEURS DU MAL</title>
					<author>Charles Baudelaire</author>
					<editor>Édouard Maynial</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Société Les Belles Lettres</publisher>
						<date when="1952">1952</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1861">1857-1861</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface de Gautier de Théophile Gautier et l’appendice n’ont pas été encodés.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Importation semi-automatique depuis fr.wikisource.org. Les vers ont été importés au kilomètre. La structuration en poèmes et en strophes a été rajoutée manuellement</p>
				<normalization>
					<p>Normalisation des titres.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
					<change when="2011-09-19" who="RR">Modification en fonction de l’édition de référence (Les Belles Lettres) :insertion des pièces censurées et report des pièces ajoutées dans une partie « Supplément". Les pièces manquantes ont été insérées à partir de la version électronique d’Enclitt avec plusieurs corrections.</change>
					<change when="2011-09-22" who="RR">La balise « space" pour le retrait des vers a été placée dans la balise « l" (dans le vers) et non dans « lg" (entre les vers)</change>
					<change when="2011-09-26" who="RR">Insertion du poème en latin « Franciscae meae laudes"</change>
					<change when="2011-09-27" who="RR">modification du poème « les petites vieilles" : De frascati défunt…</change>
					<change when="2011-09-27" who="RR">découpage en strophes de « Le crépuscule du soir"</change>
					<change when="2011-09-27" who="RR">correction inutile de BB dans « Le vin de l’assassin"</change>
					<change when="2011-09-27" who="RR">correction du titre de « Lesbos" et vers « nuits chauds"</change>
					<change when="2011-09-27" who="RR">correction dans « femmes damnées" loin des peuples…</change>
					<change when="2011-09-27" who="RR">suppression du numéro d’ordre ; remplacé par key</change>
					<change when="2016-01-23" who="RR">Utilisation de l’attribut « lang" pour marquer les vers inanalysables par les programmes Malherbe (BAU64 :vers en latin)</change>
					<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
					<change when="2017-06-27" who="RR">Révision des titres et sous-titres pour une cohérence avec la table des matières.</change>
				</listChange>
			</revisionDesc>
	</teiHeader><text><body><head type="main_part">FLEURS DU MAL</head><div type="poem" key="BAU120">
					<head type="number">CXVI</head>
					<head type="main">Femmes damnées</head>
					<lg n="1">
						<l n="1" num="1.1">Comme un bétail pensif sur le sable couchées,</l>
						<l n="2" num="1.2">Elles tournent leurs yeux vers l’horizon des mers,</l>
						<l n="3" num="1.3">Et leurs pieds se cherchant et leurs mains rapprochées</l>
						<l n="4" num="1.4">Ont de douces langueurs et des frissons amers.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">Les unes, cœurs épris des longues confidences,</l>
						<l n="6" num="2.2">Dans le fond des bosquets où jasent les ruisseaux,</l>
						<l n="7" num="2.3">Vont épelant l’amour des craintives enfances</l>
						<l n="8" num="2.4">Et creusent le bois vert des jeunes arbrisseaux ;</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">D’autres, comme des sœurs, marchent lentes et graves</l>
						<l n="10" num="3.2">À travers les rochers pleins d’apparitions,</l>
						<l n="11" num="3.3">Où saint Antoine a vu surgir comme des laves</l>
						<l n="12" num="3.4">Les seins nus et pourprés de ses tentations ;</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">Il en est, aux lueurs des résines croulantes,</l>
						<l n="14" num="4.2">Qui dans le creux muet des vieux antres païens</l>
						<l n="15" num="4.3">T’appellent au secours de leurs fièvres hurlantes,</l>
						<l n="16" num="4.4">Ô Bacchus, endormeur des remords anciens !</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">Et d’autres, dont la gorge aime les scapulaires</l>
						<l n="18" num="5.2">Qui, recélant un fouet sous leurs longs vêtements,</l>
						<l n="19" num="5.3">Mêlent, dans le bois sombre et les nuits solitaires,</l>
						<l n="20" num="5.4">L’écume du plaisir aux larmes des tourments.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1">Ô vierges, ô démons, ô monstres, ô martyres,</l>
						<l n="22" num="6.2">De la réalité grands esprits contempteurs,</l>
						<l n="23" num="6.3">Chercheuses d’infini, dévotes et satyres,</l>
						<l n="24" num="6.4">Tantôt pleines de cris, tantôt pleines de pleurs,</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1">Vous que dans votre enfer mon âme a poursuivies,</l>
						<l n="26" num="7.2">Pauvres sœurs, je vous aime autant que je vous plains,</l>
						<l n="27" num="7.3">Pour vos mornes douleurs, vos soifs inassouvies,</l>
						<l n="28" num="7.4">Et les urnes d’amour dont vos grands cœurs sont pleins !</l>
					</lg>
				</div></body></text></TEI>