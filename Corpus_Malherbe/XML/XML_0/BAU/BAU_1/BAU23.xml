<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES FLEURS DU MAL</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAU">
					<name>
						<forename>Charles</forename>
						<surname>BAUDELAIRE</surname>
					</name>
					<date from="1821" to="1867">1821-1867</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="BB">
						<forename>Benoît</forename>
						<surname>Brard</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4145 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2010">2010</date>
				<idno type="local">BAU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Fleurs du mal</title>
						<author>Charles Baudelaire</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">http://fr.wikisource.org/wiki/Les_Fleurs_du_mal_(1868)</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LES FLEURS DU MAL</title>
								<author>Charles Baudelaire</author>
								<imprint>
									<publisher>Michel Lévy</publisher>
									<date when="1868">1868</date>
								</imprint>
							</monogr>
							<note>3ème édition des Fleurs du mal (posthume).</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title>LES FLEURS DU MAL</title>
					<author>Charles Baudelaire</author>
					<editor>Édouard Maynial</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Société Les Belles Lettres</publisher>
						<date when="1952">1952</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1861">1857-1861</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface de Gautier de Théophile Gautier et l’appendice n’ont pas été encodés.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Importation semi-automatique depuis fr.wikisource.org. Les vers ont été importés au kilomètre. La structuration en poèmes et en strophes a été rajoutée manuellement</p>
				<normalization>
					<p>Normalisation des titres.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
					<change when="2011-09-19" who="RR">Modification en fonction de l’édition de référence (Les Belles Lettres) :insertion des pièces censurées et report des pièces ajoutées dans une partie « Supplément". Les pièces manquantes ont été insérées à partir de la version électronique d’Enclitt avec plusieurs corrections.</change>
					<change when="2011-09-22" who="RR">La balise « space" pour le retrait des vers a été placée dans la balise « l" (dans le vers) et non dans « lg" (entre les vers)</change>
					<change when="2011-09-26" who="RR">Insertion du poème en latin « Franciscae meae laudes"</change>
					<change when="2011-09-27" who="RR">modification du poème « les petites vieilles" : De frascati défunt…</change>
					<change when="2011-09-27" who="RR">découpage en strophes de « Le crépuscule du soir"</change>
					<change when="2011-09-27" who="RR">correction inutile de BB dans « Le vin de l’assassin"</change>
					<change when="2011-09-27" who="RR">correction du titre de « Lesbos" et vers « nuits chauds"</change>
					<change when="2011-09-27" who="RR">correction dans « femmes damnées" loin des peuples…</change>
					<change when="2011-09-27" who="RR">suppression du numéro d’ordre ; remplacé par key</change>
					<change when="2016-01-23" who="RR">Utilisation de l’attribut « lang" pour marquer les vers inanalysables par les programmes Malherbe (BAU64 :vers en latin)</change>
					<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
					<change when="2017-06-27" who="RR">Révision des titres et sous-titres pour une cohérence avec la table des matières.</change>
				</listChange>
			</revisionDesc>
	</teiHeader><text><body><head type="main_part">SPLEEN ET IDÉAL</head><div type="poem" key="BAU23">
					<head type="number">XXII</head>
					<head type="main">Hymne à la Beauté</head>
					<lg n="1">
						<l n="1" num="1.1">Viens-tu du ciel profond ou sors-tu de l’abîme,</l>
						<l n="2" num="1.2">O Beauté ? Ton regard, infernal et divin,</l>
						<l n="3" num="1.3">Verse confusément le bienfait et le crime,</l>
						<l n="4" num="1.4">Et l’on peut pour cela te comparer au vin.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">Tu contiens dans ton œil le couchant et l’aurore ;</l>
						<l n="6" num="2.2">Tu répands des parfums comme un soir orageux ;</l>
						<l n="7" num="2.3">Tes baisers sont un philtre et ta bouche une amphore</l>
						<l n="8" num="2.4">Qui font le héros lâche et l’enfant courageux.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">Sors-tu du gouffre noir ou descends-tu des astres ?</l>
						<l n="10" num="3.2">Le Destin charmé suit tes jupons comme un chien ;</l>
						<l n="11" num="3.3">Tu sèmes au hasard la joie et les désastres,</l>
						<l n="12" num="3.4">Et tu gouvernes tout et ne réponds de rien.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">Tu marches sur des morts, Beauté, dont tu te moques,</l>
						<l n="14" num="4.2">De tes bijoux l’Horreur n’est pas le moins charmant,</l>
						<l n="15" num="4.3">Et le Meurtre, parmi tes plus chères breloques,</l>
						<l n="16" num="4.4">Sur ton ventre orgueilleux danse amoureusement.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">L’éphémère ébloui vole vers toi, chandelle,</l>
						<l n="18" num="5.2">Crépite, flambe et dit : Bénissons ce flambeau !</l>
						<l n="19" num="5.3">L’amoureux pantelant incliné sur sa belle</l>
						<l n="20" num="5.4">A l’air d’un moribond caressant son tombeau.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1">Que tu viennes du ciel ou de l’enfer, qu’importe,</l>
						<l n="22" num="6.2">O Beauté ! monstre énorme, effrayant, ingénu !</l>
						<l n="23" num="6.3">Si ton œil, ton souris, ton pied, m’ouvrent la porte</l>
						<l n="24" num="6.4">D’un Infini que j’aime et n’ai jamais connu ?</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1">De Satan ou de Dieu, qu’importe ? Ange ou Sirène,</l>
						<l n="26" num="7.2">Qu’importe, si tu rends, — fée aux yeux de velours,</l>
						<l n="27" num="7.3">Rythme, parfum, lueur, ô mon unique reine ! —</l>
						<l n="28" num="7.4">L’univers moins hideux et les instants moins lourds ?</l>
					</lg>
				</div></body></text></TEI>