<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES FLEURS DU MAL</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAU">
					<name>
						<forename>Charles</forename>
						<surname>BAUDELAIRE</surname>
					</name>
					<date from="1821" to="1867">1821-1867</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="BB">
						<forename>Benoît</forename>
						<surname>Brard</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4145 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2010">2010</date>
				<idno type="local">BAU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Fleurs du mal</title>
						<author>Charles Baudelaire</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">http://fr.wikisource.org/wiki/Les_Fleurs_du_mal_(1868)</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LES FLEURS DU MAL</title>
								<author>Charles Baudelaire</author>
								<imprint>
									<publisher>Michel Lévy</publisher>
									<date when="1868">1868</date>
								</imprint>
							</monogr>
							<note>3ème édition des Fleurs du mal (posthume).</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title>LES FLEURS DU MAL</title>
					<author>Charles Baudelaire</author>
					<editor>Édouard Maynial</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Société Les Belles Lettres</publisher>
						<date when="1952">1952</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1861">1857-1861</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface de Gautier de Théophile Gautier et l’appendice n’ont pas été encodés.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Importation semi-automatique depuis fr.wikisource.org. Les vers ont été importés au kilomètre. La structuration en poèmes et en strophes a été rajoutée manuellement</p>
				<normalization>
					<p>Normalisation des titres.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
					<change when="2011-09-19" who="RR">Modification en fonction de l’édition de référence (Les Belles Lettres) :insertion des pièces censurées et report des pièces ajoutées dans une partie « Supplément". Les pièces manquantes ont été insérées à partir de la version électronique d’Enclitt avec plusieurs corrections.</change>
					<change when="2011-09-22" who="RR">La balise « space" pour le retrait des vers a été placée dans la balise « l" (dans le vers) et non dans « lg" (entre les vers)</change>
					<change when="2011-09-26" who="RR">Insertion du poème en latin « Franciscae meae laudes"</change>
					<change when="2011-09-27" who="RR">modification du poème « les petites vieilles" : De frascati défunt…</change>
					<change when="2011-09-27" who="RR">découpage en strophes de « Le crépuscule du soir"</change>
					<change when="2011-09-27" who="RR">correction inutile de BB dans « Le vin de l’assassin"</change>
					<change when="2011-09-27" who="RR">correction du titre de « Lesbos" et vers « nuits chauds"</change>
					<change when="2011-09-27" who="RR">correction dans « femmes damnées" loin des peuples…</change>
					<change when="2011-09-27" who="RR">suppression du numéro d’ordre ; remplacé par key</change>
					<change when="2016-01-23" who="RR">Utilisation de l’attribut « lang" pour marquer les vers inanalysables par les programmes Malherbe (BAU64 :vers en latin)</change>
					<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
					<change when="2017-06-27" who="RR">Révision des titres et sous-titres pour une cohérence avec la table des matières.</change>
				</listChange>
			</revisionDesc>
	</teiHeader><text><body><head type="main_part">SUPPLÉMENT AUX FLEURS DU MAL</head><div type="poem" key="BAU160">
					<head type="number">XXIV</head>
					<head type="main">Le Calumet de la paix</head>
					<head type="form">Imité de Longfellow</head>
					<div type="section" n="1">
						<head type="number">I</head>
						<lg n="1">
							<l n="1" num="1.1">Or Gitche Manito, le Maître de la vie,</l>
							<l n="2" num="1.2">Le Puissant, descendit dans la verte prairie,</l>
							<l n="3" num="1.3">Dans l’immense prairie aux coteaux montueux ;</l>
							<l n="4" num="1.4">Et là, sur les rochers de la Rouge Carrière,</l>
							<l n="5" num="1.5">Dominant tout l’espace et baigné de lumière,</l>
							<l n="6" num="1.6">Il se tenait debout, vaste et majestueux.</l>
						</lg>
						<lg n="2">
							<l n="7" num="2.1">Alors il convoqua les peuples innombrables,</l>
							<l n="8" num="2.2">Plus nombreux que ne sont les herbes et les sables</l>
							<l n="9" num="2.3">Avec sa main terrible il rompit un morceau</l>
							<l n="10" num="2.4">Du rocher, dont il fit une pipe superbe,</l>
							<l n="11" num="2.5">Puis, au bord du ruisseau, dans une énorme gerbe,</l>
							<l n="12" num="2.6">Pour s’en faire un tuyau, choisit un long roseau.</l>
						</lg>
						<lg n="3">
							<l n="13" num="3.1">Pour la bourrer il prit au saule son écorce ;</l>
							<l n="14" num="3.2">Et lui, le Tout-Puissant, Créateur de la Force,</l>
							<l n="15" num="3.3">Debout, il alluma, comme un divin fanal,</l>
							<l n="16" num="3.4">La Pipe de la Paix. Debout sur la Carrière</l>
							<l n="17" num="3.5">Il fumait, droit, superbe et baigné de lumière.</l>
							<l n="18" num="3.6">Or pour les nations c’était le grand signal.</l>
						</lg>
						<lg n="4">
							<l n="19" num="4.1">Et lentement montait la divine fumée</l>
							<l n="20" num="4.2">Dans l’air doux du matin, onduleuse, embaumée.</l>
							<l n="21" num="4.3">Et d’abord ce ne fut qu’un sillon ténébreux ;</l>
							<l n="22" num="4.4">Puis la vapeur se fit plus bleue et plus épaisse,</l>
							<l n="23" num="4.5">Puis blanchit ; et montant, et grossissant sans cesse,</l>
							<l n="24" num="4.6">Elle alla se briser au dur plafond des cieux.</l>
						</lg>
						<lg n="5">
							<l n="25" num="5.1">Des plus lointains sommets des Montagnes Rocheuses,</l>
							<l n="26" num="5.2">Depuis les lacs du Nord aux ondes tapageuses,</l>
							<l n="27" num="5.3">Depuis Tawasentha, le vallon sans pareil,</l>
							<l n="28" num="5.4">Jusqu’à Tuscaloosa, la forêt parfumée,</l>
							<l n="29" num="5.5">Tous virent le signal et l’immense fumée</l>
							<l n="30" num="5.6">Montant paisiblement dans le matin vermeil.</l>
						</lg>
						<lg n="6">
							<l n="31" num="6.1">Les Prophètes disaient : « Voyez-vous cette bande</l>
							<l n="32" num="6.2">De vapeur, qui, semblable à la main qui commande,</l>
							<l n="33" num="6.3">Oscille et se détache en noir sur le soleil ?</l>
							<l n="34" num="6.4">C’est Gitche Manito, le Maître de la Vie,</l>
							<l n="35" num="6.5">Qui dit aux quatre coins de l’immense prairie :</l>
							<l n="36" num="6.6">« Je vous convoque tous, guerriers, à mon conseil ! »</l>
						</lg>
						<lg n="7">
							<l n="37" num="7.1">Par le chemin des eaux, par la route des plaines,</l>
							<l n="38" num="7.2">Par les quatre côtés d’où soufflent les haleines</l>
							<l n="39" num="7.3">Du vent, tous les guerriers de chaque tribu, tous,</l>
							<l n="40" num="7.4">Comprenant le signal du nuage qui bouge,</l>
							<l n="41" num="7.5">Vinrent docilement à la Carrière Rouge</l>
							<l n="42" num="7.6">Où Gitche Manito leur donnait rendez-vous.</l>
						</lg>
						<lg n="8">
							<l n="43" num="8.1">Les guerriers se tenaient sur la verte prairie,</l>
							<l n="44" num="8.2">Tous équipés en guerre, et la mine aguerrie,</l>
							<l n="45" num="8.3">Bariolés ainsi qu’un feuillage automnal ;</l>
							<l n="46" num="8.4">Et la haine qui fait combattre tous les êtres,</l>
							<l n="47" num="8.5">La haine qui brûlait les yeux de leurs ancêtres</l>
							<l n="48" num="8.6">Incendiait encor leurs yeux d’un feu fatal.</l>
						</lg>
						<lg n="9">
							<l n="49" num="9.1">Et leurs yeux étaient pleins de haine héréditaire.</l>
							<l n="50" num="9.2">Or Gitche Manito, le Maître de la Terre,</l>
							<l n="51" num="9.3">Les considérait tous avec compassion,</l>
							<l n="52" num="9.4">Comme un père très bon, ennemi du désordre,</l>
							<l n="53" num="9.5">Qui voit ses chers petits batailler et se mordre.</l>
							<l n="54" num="9.6">Tel Gitche Manito pour toute nation.</l>
						</lg>
						<lg n="10">
							<l n="55" num="10.1">Il étendit sur eux sa puissante main droite</l>
							<l n="56" num="10.2">Pour subjuguer leur cœur et leur nature étroite,</l>
							<l n="57" num="10.3">Pour rafraîchir leur fièvre à l’ombre de sa main ;</l>
							<l n="58" num="10.4">Puis il leur dit avec sa voix majestueuse,</l>
							<l n="59" num="10.5">Comparable à la voix d’une eau tumultueuse</l>
							<l n="60" num="10.6">Qui tombe et rend un son monstrueux, surhumain :</l>
						</lg>
					</div>
					<div type="section" n="2">
						<head type="number">II</head>
						<lg n="1">
							<l n="61" num="1.1">« Ô ma postérité, déplorable et chérie !</l>
							<l n="62" num="1.2">Ô mes fils ! écoutez la divine raison.</l>
							<l n="63" num="1.3">C’est Gitche Manito, le Maître de la Vie,</l>
							<l n="64" num="1.4">Qui vous parle ! celui qui dans votre patrie</l>
							<l n="65" num="1.5">A mis l’ours, le castor, le renne et le bison.</l>
						</lg>
						<lg n="2">
							<l n="66" num="2.1">Je vous ai fait la chasse et la pêche faciles ;</l>
							<l n="67" num="2.2">Pourquoi donc le chasseur devient-il assassin ?</l>
							<l n="68" num="2.3">Le marais fut par moi peuplé de volatiles ;</l>
							<l n="69" num="2.4">Pourquoi n’êtes-vous pas contents, fils indociles ?</l>
							<l n="70" num="2.5">Pourquoi l’homme fait-il la chasse à son voisin ?</l>
						</lg>
						<lg n="3">
							<l n="71" num="3.1">Je suis vraiment bien las de vos horribles guerres.</l>
							<l n="72" num="3.2">Vos prières, vos vœux mêmes sont des forfaits !</l>
							<l n="73" num="3.3">Le péril est pour vous dans vos humeurs contraires,</l>
							<l n="74" num="3.4">Et c’est dans l’union qu’est votre force. En frères</l>
							<l n="75" num="3.5">Vivez donc, et sachez vous maintenir en paix.</l>
						</lg>
						<lg n="4">
							<l n="76" num="4.1">Bientôt vous recevrez de ma main un Prophète</l>
							<l n="77" num="4.2">Qui viendra vous instruire et souffrir avec vous.</l>
							<l n="78" num="4.3">Sa parole fera de la vie une fête ;</l>
							<l n="79" num="4.4">Mais si vous méprisez sa sagesse parfaite,</l>
							<l n="80" num="4.5">Pauvres enfants maudits, vous disparaîtrez tous !</l>
						</lg>
						<lg n="5">
							<l n="81" num="5.1">Effacez dans les flots vos couleurs meurtrières.</l>
							<l n="82" num="5.2">Les roseaux sont nombreux et le roc est épais ;</l>
							<l n="83" num="5.3">Chacun en peut tirer sa pipe. Plus de guerres,</l>
							<l n="84" num="5.4">Plus de sang ! Désormais vivez comme des frères,</l>
							<l n="85" num="5.5">Et tous, unis, fumez le Calumet de Paix ! »</l>
						</lg>
					</div>
					<div type="section" n="3">
						<head type="number">III</head>
						<lg n="1">
							<l n="86" num="1.1">Et soudain tous, jetant leurs armes sur la terre,</l>
							<l n="87" num="1.2">Lavent dans le ruisseau les couleurs de la guerre</l>
							<l n="88" num="1.3">Qui luisaient sur leurs fronts cruels et triomphants.</l>
							<l n="89" num="1.4">Chacun creuse une pipe et cueille sur la rive</l>
							<l n="90" num="1.5">Un long roseau qu’avec adresse il enjolive.</l>
							<l n="91" num="1.6">Et l’Esprit souriait à ses pauvres enfants !</l>
						</lg>
						<lg n="2">
							<l n="92" num="2.1">Chacun s’en retourna, l’âme calme et ravie,</l>
							<l n="93" num="2.2">Et Gitche Manito, le Maître de la Vie,</l>
							<l n="94" num="2.3">Remonta par la porte entr’ouverte des cieux.</l>
							<l n="95" num="2.4">— À travers la vapeur splendide du nuage</l>
							<l n="96" num="2.5">Le Tout-Puissant montait, content de son ouvrage,</l>
							<l n="97" num="2.6">Immense, parfumé, sublime, radieux !</l>
						</lg>
					</div>
				</div></body></text></TEI>