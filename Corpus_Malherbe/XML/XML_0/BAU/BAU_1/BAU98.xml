<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES FLEURS DU MAL</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAU">
					<name>
						<forename>Charles</forename>
						<surname>BAUDELAIRE</surname>
					</name>
					<date from="1821" to="1867">1821-1867</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="BB">
						<forename>Benoît</forename>
						<surname>Brard</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4145 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2010">2010</date>
				<idno type="local">BAU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Fleurs du mal</title>
						<author>Charles Baudelaire</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">http://fr.wikisource.org/wiki/Les_Fleurs_du_mal_(1868)</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LES FLEURS DU MAL</title>
								<author>Charles Baudelaire</author>
								<imprint>
									<publisher>Michel Lévy</publisher>
									<date when="1868">1868</date>
								</imprint>
							</monogr>
							<note>3ème édition des Fleurs du mal (posthume).</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title>LES FLEURS DU MAL</title>
					<author>Charles Baudelaire</author>
					<editor>Édouard Maynial</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Société Les Belles Lettres</publisher>
						<date when="1952">1952</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1861">1857-1861</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface de Gautier de Théophile Gautier et l’appendice n’ont pas été encodés.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Importation semi-automatique depuis fr.wikisource.org. Les vers ont été importés au kilomètre. La structuration en poèmes et en strophes a été rajoutée manuellement</p>
				<normalization>
					<p>Normalisation des titres.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
					<change when="2011-09-19" who="RR">Modification en fonction de l’édition de référence (Les Belles Lettres) :insertion des pièces censurées et report des pièces ajoutées dans une partie « Supplément". Les pièces manquantes ont été insérées à partir de la version électronique d’Enclitt avec plusieurs corrections.</change>
					<change when="2011-09-22" who="RR">La balise « space" pour le retrait des vers a été placée dans la balise « l" (dans le vers) et non dans « lg" (entre les vers)</change>
					<change when="2011-09-26" who="RR">Insertion du poème en latin « Franciscae meae laudes"</change>
					<change when="2011-09-27" who="RR">modification du poème « les petites vieilles" : De frascati défunt…</change>
					<change when="2011-09-27" who="RR">découpage en strophes de « Le crépuscule du soir"</change>
					<change when="2011-09-27" who="RR">correction inutile de BB dans « Le vin de l’assassin"</change>
					<change when="2011-09-27" who="RR">correction du titre de « Lesbos" et vers « nuits chauds"</change>
					<change when="2011-09-27" who="RR">correction dans « femmes damnées" loin des peuples…</change>
					<change when="2011-09-27" who="RR">suppression du numéro d’ordre ; remplacé par key</change>
					<change when="2016-01-23" who="RR">Utilisation de l’attribut « lang" pour marquer les vers inanalysables par les programmes Malherbe (BAU64 :vers en latin)</change>
					<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
					<change when="2017-06-27" who="RR">Révision des titres et sous-titres pour une cohérence avec la table des matières.</change>
				</listChange>
			</revisionDesc>
	</teiHeader><text><body><head type="main_part">TABLEAUX PARISIENS</head><div type="poem" key="BAU98">
					<head type="number">XCIV</head>
					<head type="main">Les Petites Vieilles</head>
					<opener><salute>À <name>Victor Hugo</name></salute></opener>
					<div type="section" n="1">
						<head type="number">I</head>
						<lg n="1">
							<l n="1" num="1.1">Dans les plis sinueux des vieilles capitales,</l>
							<l n="2" num="1.2">Où tout, même l’horreur, tourne aux enchantements,</l>
							<l n="3" num="1.3">Je guette, obéissant à mes humeurs fatales,</l>
							<l n="4" num="1.4">Des êtres singuliers, décrépits et charmants.</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1">Ces monstres disloqués furent jadis des femmes,</l>
							<l n="6" num="2.2">Éponine ou Laïs ! — Monstres brisés, bossus</l>
							<l n="7" num="2.3">Ou tordus, aimons-les ! ce sont encor des âmes.</l>
							<l n="8" num="2.4">Sous des jupons troués et sous de froids tissus</l>
						</lg>
						<lg n="3">
							<l n="9" num="3.1">Ils rampent, flagellés par les bises iniques,</l>
							<l n="10" num="3.2">Frémissant au fracas roulant des omnibus,</l>
							<l n="11" num="3.3">Et serrant sur leur flanc, ainsi que des reliques,</l>
							<l n="12" num="3.4">Un petit sac brodé de fleurs ou de rébus ;</l>
						</lg>
						<lg n="4">
							<l n="13" num="4.1">Ils trottent, tout pareils à des marionnettes ;</l>
							<l n="14" num="4.2">Se traînent, comme font les animaux blessés,</l>
							<l n="15" num="4.3">Ou dansent, sans vouloir danser, pauvres sonnettes</l>
							<l n="16" num="4.4">Où se pend un Démon sans pitié ! Tout cassés</l>
						</lg>
						<lg n="5">
							<l n="17" num="5.1">Qu’ils sont, ils ont des yeux perçants comme une vrille,</l>
							<l n="18" num="5.2">Luisants comme ces trous où l’eau dort dans la nuit ;</l>
							<l n="19" num="5.3">Ils ont les yeux divins de la petite fille</l>
							<l n="20" num="5.4">Qui s’étonne et qui rit à tout ce qui reluit.</l>
						</lg>
						<lg n="6">
							<l n="21" num="6.1">— Avez-vous observé que maints cercueils de vieilles</l>
							<l n="22" num="6.2">Sont presque aussi petits que celui d’un enfant ?</l>
							<l n="23" num="6.3">La Mort savante met dans ces bières pareilles</l>
							<l n="24" num="6.4">Un symbole d’un goût bizarre et captivant,</l>
						</lg>
						<lg n="7">
							<l n="25" num="7.1">Et lorsque j’entrevois un fantôme débile</l>
							<l n="26" num="7.2">Traversant de Paris le fourmillant tableau,</l>
							<l n="27" num="7.3">Il me semble toujours que cet être fragile</l>
							<l n="28" num="7.4">S’en va tout doucement vers un nouveau berceau ;</l>
						</lg>
						<lg n="8">
							<l n="29" num="8.1">À moins que, méditant sur la géométrie,</l>
							<l n="30" num="8.2">Je ne cherche, à l’aspect de ces membres discords,</l>
							<l n="31" num="8.3">Combien de fois il faut que l’ouvrier varie</l>
							<l n="32" num="8.4">La forme de la boîte où l’on met tous ces corps.</l>
						</lg>
						<lg n="9">
							<l n="33" num="9.1">— Ces yeux sont des puits faits d’un million de larmes,</l>
							<l n="34" num="9.2">Des creusets qu’un métal refroidi pailleta…</l>
							<l n="35" num="9.3">Ces yeux mystérieux ont d’invincibles charmes</l>
							<l n="36" num="9.4">Pour celui que l’austère Infortune allaita !</l>
						</lg>
					</div>
					<div type="section" n="2">
						<head type="number">II</head>
						<lg n="1">
							<l n="37" num="1.1">De Frascati défunt Vestale enamourée ;</l>
							<l n="38" num="1.2">Prêtresse de Thalie, hélas ! dont le souffleur</l>
							<l n="39" num="1.3">Défunt, seul, sait le nom ; célèbre évaporée</l>
							<l n="40" num="1.4">Que Tivoli jadis ombragea dans sa fleur,</l>
						</lg>
						<lg n="2">
							<l n="41" num="2.1">Toutes m’enivrent ! mais parmi ces êtres frêles</l>
							<l n="42" num="2.2">Il en est qui, faisant de la douleur un miel,</l>
							<l n="43" num="2.3">Ont dit au Dévouement qui leur prêtait ses ailes :</l>
							<l n="44" num="2.4">« Hippogriffe puissant, mène-moi jusqu’au ciel ! »</l>
						</lg>
						<lg n="3">
							<l n="45" num="3.1">L’une, par sa patrie au malheur exercée,</l>
							<l n="46" num="3.2">L’autre, que son époux surchargea de douleurs,</l>
							<l n="47" num="3.3">L’autre, par son enfant Madone transpercée,</l>
							<l n="48" num="3.4">Toutes auraient pu faire un fleuve avec leurs pleurs !</l>
						</lg>
					</div>
					<div type="section" n="3">
						<head type="number">III</head>
						<lg n="1">
							<l n="49" num="1.1">Ah ! que j’en ai suivi, de ces petites vieilles !</l>
							<l n="50" num="1.2">Une, entre autres, à l’heure où le soleil tombant</l>
							<l n="51" num="1.3">Ensanglante le ciel de blessures vermeilles,</l>
							<l n="52" num="1.4">Pensive, s’asseyait à l’écart sur un banc,</l>
						</lg>
						<lg n="2">
							<l n="53" num="2.1">Pour entendre un de ces concerts, riches de cuivre,</l>
							<l n="54" num="2.2">Dont les soldats parfois inondent nos jardins,</l>
							<l n="55" num="2.3">Et qui, dans ces soirs d’or où l’on se sent revivre,</l>
							<l n="56" num="2.4">Versent quelque héroïsme au cœur des citadins.</l>
						</lg>
						<lg n="3">
							<l n="57" num="3.1">Celle-là droite encor, fière et sentant la règle,</l>
							<l n="58" num="3.2">Humait avidement ce chant vif et guerrier ;</l>
							<l n="59" num="3.3">Son œil parfois s’ouvrait comme l’œil d’un vieil aigle ;</l>
							<l n="60" num="3.4">Son front de marbre avait l’air fait pour le laurier !</l>
						</lg>
					</div>
					<div type="section" n="4">
						<head type="number">IV</head>
						<lg n="1">
							<l n="61" num="1.1">Telles vous cheminez, stoïques et sans plaintes,</l>
							<l n="62" num="1.2">À travers le chaos des vivantes cités,</l>
							<l n="63" num="1.3">Mères au cœur saignant, courtisanes ou saintes,</l>
							<l n="64" num="1.4">Dont autrefois les noms par tous étaient cités.</l>
						</lg>
						<lg n="2">
							<l n="65" num="2.1">Vous qui fûtes la grâce ou qui fûtes la gloire,</l>
							<l n="66" num="2.2">Nul ne vous reconnaît ! un ivrogne incivil</l>
							<l n="67" num="2.3">Vous insulte en passant d’un amour dérisoire ;</l>
							<l n="68" num="2.4">Sur vos talons gambade un enfant lâche et vil.</l>
						</lg>
						<lg n="3">
							<l n="69" num="3.1">Honteuses d’exister, ombres ratatinées,</l>
							<l n="70" num="3.2">Peureuses, le dos bas, vous côtoyez les murs ;</l>
							<l n="71" num="3.3">Et nul ne vous salue, étranges destinées !</l>
							<l n="72" num="3.4">Débris d’humanité pour l’éternité mûrs !</l>
						</lg>
						<lg n="4">
							<l n="73" num="4.1">Mais moi, moi qui de loin tendrement vous surveille,</l>
							<l n="74" num="4.2">L’œil inquiet, fixé sur vos pas incertains,</l>
							<l n="75" num="4.3">Tout comme si j’étais votre père, ô merveille !</l>
							<l n="76" num="4.4">Je goûte à votre insu des plaisirs clandestins :</l>
						</lg>
						<lg n="5">
							<l n="77" num="5.1">Je vois s’épanouir vos passions novices ;</l>
							<l n="78" num="5.2">Sombres ou lumineux, je vis vos jours perdus ;</l>
							<l n="79" num="5.3">Mon cœur multiplié jouit de tous vos vices !</l>
							<l n="80" num="5.4">Mon âme resplendit de toutes vos vertus !</l>
						</lg>
						<lg n="6">
							<l n="81" num="6.1">Ruines ! ma famille ! ô cerveaux congénères !</l>
							<l n="82" num="6.2">Je vous fais chaque soir un solennel adieu !</l>
							<l n="83" num="6.3">Où serez-vous demain, Èves octogénaires,</l>
							<l n="84" num="6.4">Sur qui pèse la griffe effroyable de Dieu ?</l>
						</lg>
					</div>
				</div></body></text></TEI>