<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES ÉPAVES</title>
				<title type="sub_2">(Édition partielle)</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAU">
					<name>
						<forename>Charles</forename>
						<surname>BAUDELAIRE</surname>
					</name>
					<date from="1821" to="1867">1821-1867</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>165 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2010">2010</date>
				<idno type="local">BAU_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Épaves</title>
						<author>Charles Baudelaire</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">https://fr.wikisource.org/wiki/Les_%C3%89paves_%28Baudelaire%29</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
									<title>Les Épaves</title>
									<author>Charles Baudelaire</author>
								<imprint>
									<pubPlace>Amsterdam</pubPlace>
									<publisher>À l’enseigne du Coq</publisher>
									<date when="1866">1866</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1869">1864-1869</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La présente édition ne contient que les poèmes qui ne sont pas présents dans l’édition de des Fleurs du mal</p>
				<p>Les notes de l’éditeur ne sont pas incluses</p>
			</samplingDecl>
			<editorialDecl>
				<p></p>
				<normalization>
					<p></p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">GALANTERIES</head><div type="poem" key="BAU163">
					<head type="number">XII</head>
					<head type="main">LE MONSTRE</head>
					<head type="sub_1">OU</head>
					<head type="sub_1">LE PARANYMPHE D’UNE NYMPHE MACABRE</head>
						<div type="section" n="1">
							<head type="number">I</head>
							<lg n="1">
								<l n="1" num="1.1">Tu n’es certes pas, ma très-chère,</l>
								<l n="2" num="1.2">Ce que Veuillot nomme un tendron.</l>
								<l n="3" num="1.3">Le jeu, l’amour, la bonne chère,</l>
								<l n="4" num="1.4">Bouillonnent en toi, vieux chaudron !</l>
								<l n="5" num="1.5">Tu n’es plus fraîche, ma très-chère,</l>
							</lg>
							<lg n="2">
								<l n="6" num="2.1">Ma vieille infante ! Et cependant</l>
								<l n="7" num="2.2">Tes caravanes insensées</l>
								<l n="8" num="2.3">T’ont donné ce lustre abondant</l>
								<l n="9" num="2.4">Des choses qui sont très-usées,</l>
								<l n="10" num="2.5">Mais qui séduisent cependant.</l>
							</lg>
							<lg n="3">
								<l n="11" num="3.1">Je ne trouve pas monotone</l>
								<l n="12" num="3.2">La verdeur de tes quarante ans ;</l>
								<l n="13" num="3.3">Je préfère tes fruits, Automne,</l>
								<l n="14" num="3.4">Aux fleurs banales du Printemps !</l>
								<l n="15" num="3.5">Non ! tu n’es jamais monotone !</l>
							</lg>
							<lg n="4">
								<l n="16" num="4.1">Ta carcasse a des agréments</l>
								<l n="17" num="4.2">Et des grâces particulières ;</l>
								<l n="18" num="4.3">Je trouve d’étranges piments</l>
								<l n="19" num="4.4">Dans le creux de tes deux salières ;</l>
								<l n="20" num="4.5">Ta carcasse a des agréments !</l>
							</lg>
							<lg n="5">
								<l n="21" num="5.1">Nargue des amants ridicules</l>
								<l n="22" num="5.2">Du melon et du giraumont !</l>
								<l n="23" num="5.3">Je préfère tes clavicules</l>
								<l n="24" num="5.4">À celles du roi Salomon,</l>
								<l n="25" num="5.5">Et je plains ces gens ridicules !</l>
							</lg>
							<lg n="6">
								<l n="26" num="6.1">Tes cheveux, comme un casque bleu,</l>
								<l n="27" num="6.2">Ombragent ton front de guerrière,</l>
								<l n="28" num="6.3">Qui ne pense et rougit que peu,</l>
								<l n="29" num="6.4">Et puis se sauvent par derrière</l>
								<l n="30" num="6.5">Comme les crins d’un casque bleu.</l>
							</lg>
							<lg n="7">
								<l n="31" num="7.1">Tes yeux qui semblent de la boue,</l>
								<l n="32" num="7.2">Où scintille quelque fanal,</l>
								<l n="33" num="7.3">Ravivés au fard de ta joue,</l>
								<l n="34" num="7.4">Lancent un éclair infernal !</l>
								<l n="35" num="7.5">Tes yeux sont noirs comme la boue !</l>
							</lg>
							<lg n="8">
								<l n="36" num="8.1">Par sa luxure et son dédain</l>
								<l n="37" num="8.2">Ta lèvre amère nous provoque ;</l>
								<l n="38" num="8.3">Cette lèvre, c’est un Éden</l>
								<l n="39" num="8.4">Qui nous attire et qui nous choque.</l>
								<l n="40" num="8.5">Quelle luxure ! et quel dédain !</l>
							</lg>
							<lg n="9">
								<l n="41" num="9.1">Ta jambe musculeuse et sèche</l>
								<l n="42" num="9.2">Sait gravir au haut des volcans,</l>
								<l n="43" num="9.3">Et malgré la neige et la dèche</l>
								<l n="44" num="9.4">Danser les plus fougueux cancans.</l>
								<l n="45" num="9.5">Ta jambe est musculeuse et sèche ;</l>
							</lg>
							<lg n="10">
								<l n="46" num="10.1">Ta peau brûlante et sans douceur,</l>
								<l n="47" num="10.2">Comme celle des vieux gendarmes,</l>
								<l n="48" num="10.3">Ne connaît pas plus la sueur</l>
								<l n="49" num="10.4">Que ton œil ne connaît les larmes.</l>
								<l n="50" num="10.5">(Et pourtant elle a sa douceur !)</l>
							</lg>
						</div>
						<div type="section" n="2">
							<head type="number">II</head>
							<lg n="1">
								<l n="51" num="1.1">Sotte, tu t’en vas droit au Diable !</l>
								<l n="52" num="1.2">Volontiers j’irais avec toi,</l>
								<l n="53" num="1.3">Si cette vitesse effroyable</l>
								<l n="54" num="1.4">Ne me causait pas quelque émoi.</l>
								<l n="55" num="1.5">Va-t’en donc, toute seule, au Diable !</l>
							</lg>
							<lg n="2">
								<l n="56" num="2.1">Mon rein, mon poumon, mon jarret</l>
								<l n="57" num="2.2">Ne me laissent plus rendre hommage</l>
								<l n="58" num="2.3">À ce Seigneur, comme il faudrait.</l>
								<l n="59" num="2.4">« Hélas ! c’est vraiment bien dommage ! »</l>
								<l n="60" num="2.5">Disent mon rein et mon jarret.</l>
							</lg>
							<lg n="3">
								<l n="61" num="3.1">Oh ! très-sincèrement je souffre</l>
								<l n="62" num="3.2">De ne pas aller aux sabbats,</l>
								<l n="63" num="3.3">Pour voir, quand il pète du soufre,</l>
								<l n="64" num="3.4">Comment tu lui baises son cas !</l>
								<l n="65" num="3.5">Oh ! très-sincèrement je souffre !</l>
							</lg>
							<lg n="4">
								<l n="66" num="4.1">Je suis diablement affligé</l>
								<l n="67" num="4.2">De ne pas être ta torchère,</l>
								<l n="68" num="4.3">Et de te demander congé,</l>
								<l n="69" num="4.4">Flambeau d’enfer ! Juge, ma chère,</l>
								<l n="70" num="4.5">Combien je dois être affligé,</l>
							</lg>
							<lg n="5">
								<l n="71" num="5.1">Puisque depuis longtemps je t’aime,</l>
								<l n="72" num="5.2">Étant très-logique ! En effet,</l>
								<l n="73" num="5.3">Voulant du Mal chercher la crème</l>
								<l n="74" num="5.4">Et n’aimer qu’un monstre parfait,</l>
								<l n="75" num="5.5">Vraiment oui ! vieux monstre, je t’aime !</l>
							</lg>
						</div>
					</div></body></text></TEI>