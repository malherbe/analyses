<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Caresses d’Antan</title>
				<title type="medium">Une édition électronique</title>
				<author key="DUC">
					<name>
						<forename>Alexandre</forename>
						<surname>DUCROS</surname>
					</name>
					<date from="1823" to="1906">1823-1906</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2199 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DUC_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Caresses d’Antan</title>
						<author>Alexandre Ducros</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k54577888.r=alexandre%20ducros?rk=236052;4</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Caresses d’Antan</title>
								<author>Alexandre Ducros</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>ALEXANDRE GAUTHERIN, ÉDITEUR</publisher>
									<date when="1896">1847 ‒ 1896</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES NOUVELLES</title>
						<title>1852 ‒ 1885</title>
						<author>Alexandre Ducros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>E. DENTU, ÉDITEUR DE LA SOCIÉTÉ DES GENS DE LETTRES</publisher>
							<date when="1885">1885</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1896">1896</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La partie introductive du recueil n’est pas reprise dans cette édition.</p>
				<p>Le texte liminaire de la partie "La Légende du vers à soie" n’a pas été reprise.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>Certains retraits introduits automatiquement ont été supprimés afin d’être conforme à l’édition de référence</p>
				<normalization>
					<p>Les tirets simples ont été remplacés par des tirets demi-cadratin</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2022-05-26" who="RR">Une correction à partir de la version imprimée de 1885</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DUC47">
				<head type="main">Adriane</head>
				<lg n="1">
					<l n="1" num="1.1">Je la rencontrai dans le bois,</l>
					<l n="2" num="1.2">C’était pour la première fois.</l>
					<l n="3" num="1.3">Le soleil rayonnait superbe.</l>
				</lg>
				<lg n="2">
					<l n="4" num="2.1">Et saluant ses doux rayons,</l>
					<l n="5" num="2.2">Les cigales et les grillons</l>
					<l n="6" num="2.3">Chantaient joyeusement dans l’herbe ;</l>
				</lg>
				<lg n="3">
					<l n="7" num="3.1">Elle avait seize ans et moi, vingt.</l>
					<l n="8" num="3.2">Or, écoutez ce qu’il advint ;</l>
					<l n="9" num="3.3">Elle prit une fleur superbe,</l>
				</lg>
				<lg n="4">
					<l n="10" num="4.1">Me l’offrit avec un regard…</l>
					<l n="11" num="4.2">Le scarabée et le lézard</l>
					<l n="12" num="4.3">Se chauffaient au soleil sur l’herbe.</l>
				</lg>
				<lg n="5">
					<l n="13" num="5.1">J’avais le regard et la fleur,</l>
					<l n="14" num="5.2">Belle était la part de mon cœur ;</l>
					<l n="15" num="5.3">Je m’en fis un joyau superbe.</l>
				</lg>
				<lg n="6">
					<l n="16" num="6.1">Puis, tout à coup, sachant oser,</l>
					<l n="17" num="6.2">Je pris sur sa lèvre un baiser…</l>
					<l n="18" num="6.3">Et nous nous assîmes sur l’herbe.</l>
				</lg>
				<lg n="7">
					<l n="19" num="7.1">Au bout d’une heure nous étions</l>
					<l n="20" num="7.2">De vieux amis et nous allions,</l>
					<l n="21" num="7.3">Seuls, à travers le bois superbe.</l>
				</lg>
				<lg n="8">
					<l n="22" num="8.1">Elle y voyait plus d’un danger…</l>
					<l n="23" num="8.2">Les oiseaux, pour l’encourager,</l>
					<l n="24" num="8.3">Se béquetaient gaîment sur l’herbe.</l>
				</lg>
				<lg n="9">
					<l n="25" num="9.1">Dans le bois s’éteignit le bruit,</l>
					<l n="26" num="9.2">Dans le ciel bleu se fit la nuit,</l>
					<l n="27" num="9.3">Et dans mon cœur un jour superbe !</l>
				</lg>
				<lg n="10">
					<l n="28" num="10.1">Vous tous, n’allez pas m’accuser,</l>
					<l n="29" num="10.2">Pour un regard, pour un baiser,</l>
					<l n="30" num="10.3">Pour une fleur cueillis sur l’herbe !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1865">1865</date>
					</dateline>
				</closer>
			</div></body></text></TEI>