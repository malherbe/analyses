<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Caresses d’Antan</title>
				<title type="medium">Une édition électronique</title>
				<author key="DUC">
					<name>
						<forename>Alexandre</forename>
						<surname>DUCROS</surname>
					</name>
					<date from="1823" to="1906">1823-1906</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2199 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DUC_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Caresses d’Antan</title>
						<author>Alexandre Ducros</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k54577888.r=alexandre%20ducros?rk=236052;4</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Caresses d’Antan</title>
								<author>Alexandre Ducros</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>ALEXANDRE GAUTHERIN, ÉDITEUR</publisher>
									<date when="1896">1847 ‒ 1896</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES NOUVELLES</title>
						<title>1852 ‒ 1885</title>
						<author>Alexandre Ducros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>E. DENTU, ÉDITEUR DE LA SOCIÉTÉ DES GENS DE LETTRES</publisher>
							<date when="1885">1885</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1896">1896</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La partie introductive du recueil n’est pas reprise dans cette édition.</p>
				<p>Le texte liminaire de la partie "La Légende du vers à soie" n’a pas été reprise.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>Certains retraits introduits automatiquement ont été supprimés afin d’être conforme à l’édition de référence</p>
				<normalization>
					<p>Les tirets simples ont été remplacés par des tirets demi-cadratin</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2022-05-26" who="RR">Une correction à partir de la version imprimée de 1885</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DUC64">
				<head type="main">Marie</head>
				<lg n="1">
					<l n="1" num="1.1"><space unit="char" quantity="4"></space>Jeune et belle Marie,</l>
					<l n="2" num="1.2"><space unit="char" quantity="8"></space>De tous chérie,</l>
					<l n="3" num="1.3"><space unit="char" quantity="4"></space>Je voulais d’un bouquet,</l>
					<l n="4" num="1.4"><space unit="char" quantity="8"></space>Frais et coquet,</l>
					<l n="5" num="1.5">A mon tour parer votre fête ;</l>
					<l n="6" num="1.6">Il est trop tard ! — Mais ces couplets,</l>
					<l n="7" num="1.7">Couplets, hélas, bien incomplets,</l>
					<l n="8" num="1.8">Vous portent les vœux du poète.</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1">Que Dieu répande à tout jamais</l>
					<l n="10" num="2.2"><space unit="char" quantity="8"></space>Ses doux bienfaits</l>
					<l n="11" num="2.3">Sur le chemin de votre vie.</l>
					<l n="12" num="2.4"><space unit="char" quantity="8"></space>Malgré l’envie,</l>
					<l n="13" num="2.5">Qu’il vous tresse et vous tresse encor</l>
					<l n="14" num="2.6">Dans nos rudes sentiers, mignonne,</l>
					<l n="15" num="2.7">Pour vous en faire une couronne,</l>
					<l n="16" num="2.8">Des jours mêlés de soie et d’or !</l>
				</lg>
				<lg n="3">
					<l n="17" num="3.1"><space unit="char" quantity="4"></space>Ange, soyez la joie</l>
					<l n="18" num="3.2"><space unit="char" quantity="8"></space>Qui se déploie</l>
					<l n="19" num="3.3">Sur le foyer calme et béni,</l>
					<l n="20" num="3.4"><space unit="char" quantity="8"></space>Comme un doux nid.</l>
					<l n="21" num="3.5">Soyez la voix qui dit : — Espère !</l>
					<l n="22" num="3.6">Le bonheur qui se tient au seuil ;</l>
					<l n="23" num="3.7">Soyez l’amour, soyez l’orgueil,</l>
					<l n="24" num="3.8">De qui vous aime et sait le taire !</l>
				</lg>
				<lg n="4">
					<l n="25" num="4.1">Que votre sourire toujours,</l>
					<l n="26" num="4.2"><space unit="char" quantity="8"></space>Dore les jours,</l>
					<l n="27" num="4.3">D’une tendre mère, ô Marie !</l>
					<l n="28" num="4.4"><space unit="char" quantity="8"></space>Rose fleurie,</l>
					<l n="29" num="4.5">Brillez pour réjouir les yeux,</l>
					<l n="30" num="4.6">Et que partout votre présence</l>
					<l n="31" num="4.7">Exhale un parfum d’innocence ;</l>
					<l n="32" num="4.8">Fleur de la terre, ange des cieux !</l>
				</lg>
				<lg n="5">
					<l n="33" num="5.1"><space unit="char" quantity="4"></space>Que puis-je dire encore ?</l>
					<l n="34" num="5.2"><space unit="char" quantity="8"></space>Vous, qu’on adore,</l>
					<l n="35" num="5.3">Vous, que l’on est heureux d’aimer,</l>
					<l n="36" num="5.4"><space unit="char" quantity="8"></space>Quels vœux former ?</l>
					<l n="37" num="5.5">Pour vous, si parfaite, si bonne,</l>
					<l n="38" num="5.6">Je murmure un <foreign lang="lat">Alléluia</foreign>,</l>
					<l n="39" num="5.7">Et vous dis : — <foreign lang="lat">Ave Maria</foreign> !</l>
					<l n="40" num="5.8">Comme à votre Sainte Patronne.</l>
				</lg>
			</div></body></text></TEI>