<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Caresses d’Antan</title>
				<title type="medium">Une édition électronique</title>
				<author key="DUC">
					<name>
						<forename>Alexandre</forename>
						<surname>DUCROS</surname>
					</name>
					<date from="1823" to="1906">1823-1906</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2199 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DUC_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Caresses d’Antan</title>
						<author>Alexandre Ducros</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k54577888.r=alexandre%20ducros?rk=236052;4</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Caresses d’Antan</title>
								<author>Alexandre Ducros</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>ALEXANDRE GAUTHERIN, ÉDITEUR</publisher>
									<date when="1896">1847 ‒ 1896</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES NOUVELLES</title>
						<title>1852 ‒ 1885</title>
						<author>Alexandre Ducros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>E. DENTU, ÉDITEUR DE LA SOCIÉTÉ DES GENS DE LETTRES</publisher>
							<date when="1885">1885</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1896">1896</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La partie introductive du recueil n’est pas reprise dans cette édition.</p>
				<p>Le texte liminaire de la partie "La Légende du vers à soie" n’a pas été reprise.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>Certains retraits introduits automatiquement ont été supprimés afin d’être conforme à l’édition de référence</p>
				<normalization>
					<p>Les tirets simples ont été remplacés par des tirets demi-cadratin</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2022-05-26" who="RR">Une correction à partir de la version imprimée de 1885</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LES RUBANS DE MARIE</head><head type="sub_part">Simple Histoire</head><div type="poem" key="DUC25">
					<head type="number">IV</head>
					<head type="main">Ruban noir</head>
					<lg n="1">
						<l n="1" num="1.1">Le clairon a sonné, tout s’émeut, le sol tremble,</l>
						<l n="2" num="1.2">On dirait un seul corps en voyant cet ensemble</l>
						<l n="3" num="1.3">De mille bataillons marchant à rangs serrés.</l>
						<l n="4" num="1.4">Le silence est partout ; l’heure de la bataille</l>
						<l n="5" num="1.5">Produit une stupeur que bientôt la mitraille</l>
						<l n="6" num="1.6">Va chasser, en passant, sur ces fronts assurés.</l>
					</lg>
					<lg n="2">
						<l n="7" num="2.1">Ils vaincront ou mourront ! — en avant ! la victoire</l>
						<l n="8" num="2.2">Leur est promise à tous, ils couvriront de gloire</l>
						<l n="9" num="2.3">Et d’immortalité leurs drapeaux triomphants !</l>
						<l n="10" num="2.4">D’où leur vient donc ainsi cette mâle assurance ?</l>
						<l n="11" num="2.5">Qui les guide ? — Un génie a fait par sa présence,</l>
						<l n="12" num="2.6">Passer d’un seul coup d’œil la victoire en leurs rangs.</l>
					</lg>
					<lg n="3">
						<l n="13" num="3.1">Un seul coup d’œil, un geste, un signe, une parole,</l>
						<l n="14" num="3.2">Celle qui fit franchir d’un bond le pont d’Arcole !</l>
						<l n="15" num="3.3">Car ce génie était le vainqueur d’Austerlitz ;</l>
						<l n="16" num="3.4">C’était Napoléon, qui, ravageant la terre,</l>
						<l n="17" num="3.5">Dans son immense orgueil avait rêvé de faire</l>
						<l n="18" num="3.6">Des couronnes des rois des jouets pour son fils !</l>
					</lg>
					<lg n="4">
						<l n="19" num="4.1">En avant ! en avant ! la fanfare résonne,</l>
						<l n="20" num="4.2">Par cent bouches d’airain la mort s’élance et tonne,</l>
						<l n="21" num="4.3">Et le champ de bataille est jonché de mourants !</l>
						<l n="22" num="4.4">En avant, vieux soldat ! quelles sont donc tes craintes ?</l>
						<l n="23" num="4.5">N’entends-tu pas ces cris de victoire ? — Ces plaintes,</l>
						<l n="24" num="4.6">Ce monstrueux concert que font des combattants ?</l>
					</lg>
					<lg n="5">
						<l n="25" num="5.1">Napoléon est là ! son regard, regard d’aigle,</l>
						<l n="26" num="5.2">Mesure tous les plans ; il court, il vient, il règle</l>
						<l n="27" num="5.3">Les chances du succès ; il a vu ta valeur ;</l>
						<l n="28" num="5.4">Il te fait signe, approche et que ton front s’incline,</l>
						<l n="29" num="5.5">Pour étancher ton sang il va sur ta poitrine</l>
						<l n="30" num="5.6">Poser, ô vieux soldat, l’étoile de l’honneur !</l>
					</lg>
					<lg n="6">
						<l n="31" num="6.1">Va te faire tuer maintenant ; — que t’importe ?</l>
						<l n="32" num="6.2">Tu jetteras encor d’une voix assez forte,</l>
						<l n="33" num="6.3">Un cri d’enthousiasme et « Vive l’Empereur !… »</l>
						<l n="34" num="6.4">Mais les rangs ennemis faiblissent et s’affaissent.</l>
						<l n="35" num="6.5">Leurs derniers bataillons devant vous disparaissent,</l>
						<l n="36" num="6.6">Napoléon encor se promène vainqueur !</l>
					</lg>
					<lg n="7">
						<l n="37" num="7.1">Mais que de morts, grand Dieu ! dorment dans la poussière</l>
						<l n="38" num="7.2">Qui pourrait les compter ? leurs corps couvrent la terre !</l>
						<l n="39" num="7.3">A l’appel du clairon ils ne répondront plus !</l>
						<l n="40" num="7.4">Un lourd sommeil de plomb pèse sur leur paupière ;</l>
						<l n="41" num="7.5">Ils ne reverront plus leurs parents, leur chaumière,</l>
						<l n="42" num="7.6">Où depuis si longtemps ils étaient attendus !</l>
					</lg>
					<lg n="8">
						<l n="43" num="8.1">Retournons maintenant à la pauvre Marie.</l>
						<l n="44" num="8.2">Que fait-elle ? elle espère… elle doute… elle prie !</l>
						<l n="45" num="8.3">Un noir pressentiment attriste son amour.</l>
						<l n="46" num="8.4">— « Oh ! s’il était tué ! » se disait-elle, émue.</l>
						<l n="47" num="8.5">Un jour, elle descend en courant ; dans la rue</l>
						<l n="48" num="8.6">Elle avait entendu comme un bruit de tambour ;</l>
					</lg>
					<lg n="9">
						<l n="49" num="9.1">Un régiment passait. — « C’est le sien, cria-t-elle,</l>
						<l n="50" num="9.2">Il revient donc, enfin ! » Et puis, elle chancelle,</l>
						<l n="51" num="9.3">Car Louis n’était pas parmi tous ces soldats.</l>
						<l n="52" num="9.4">Elle s’informe alors, elle demande et pleure ;</l>
						<l n="53" num="9.5">— « Avez-vous vu Louis ? pourquoi donc à cette heure</l>
						<l n="54" num="9.6">N’est-il pas avec vous ? — On ne répondait pas.</l>
					</lg>
					<lg n="10">
						<l n="55" num="10.1">— « Parlez ; dites un mot. J’étais sa sœur chérie,</l>
						<l n="56" num="10.2">Sa fiancée, enfin le bonheur de sa vie…</l>
						<l n="57" num="10.3">Vous voulez m’effrayer, Messieurs ? vous avez tort ;</l>
						<l n="58" num="10.4">Tenez, je ris…, parlez… déjà l’heure s’écoule,</l>
						<l n="59" num="10.5">Pourquoi retardez-vous mon bonheur ? » — De la foule</l>
						<l n="60" num="10.6">Une voix s’éleva disant : — « Louis est mort ! »</l>
					</lg>
					<lg n="11">
						<l n="61" num="11.1">— « Mort ! » — Ce cri de l’enfant fut la seule parole,</l>
						<l n="62" num="11.2">Et puis elle tomba pour se relever folle !</l>
						<l n="63" num="11.3">Lorsque de sa mansarde elle prit le chemin,</l>
						<l n="64" num="11.4">Ses yeux étaient hagards ; pas une plainte amère</l>
						<l n="65" num="11.5">Ne sortait de sa bouche. — Elle embrassa sa mère</l>
						<l n="66" num="11.6">Qui quelques jours après expirait de chagrin.</l>
					</lg>
					<lg n="12">
						<l n="67" num="12.1">Oh ! comme tout était changé dans la mansarde !</l>
						<l n="68" num="12.2">Plus de chants, plus d’ouvrage, et la lueur blafarde</l>
						<l n="69" num="12.3">D’une lampe éclairait tout ce morne abandon.</l>
						<l n="70" num="12.4">Les voisins par pitié secouraient la misère</l>
						<l n="71" num="12.5">De Marie accroupie au foyer solitaire,</l>
						<l n="72" num="12.6">Et qui semblait n’avoir retenu qu’un seul nom !</l>
					</lg>
					<lg n="13">
						<l n="73" num="13.1">Parfois, on l’entendait, debout à la fenêtre,</l>
						<l n="74" num="13.2">Pousser un long éclat de rire ; — « Il va paraître,</l>
						<l n="75" num="13.3">Criait-elle aux passants, — il revient aujourd’hui !</l>
						<l n="76" num="13.4">Ou bien, elle arrêtait un soldat au passage,</l>
						<l n="77" num="13.5">Elle le regardait fixement au visage,</l>
						<l n="78" num="13.6">Et le laissait aller, disant : — « Ce n’est pas lui ! »</l>
					</lg>
					<lg n="14">
						<l n="79" num="14.1">Elle avait enlevé dans un moment lucide</l>
						<l n="80" num="14.2">Son ruban vert, hélas, de tant de pleurs humide !</l>
						<l n="81" num="14.3">A quoi bon désormais l’emblème de l’espoir ?</l>
						<l n="82" num="14.4">Seulement, et parfois, aux belles amoureuses,</l>
						<l n="83" num="14.5">De leur bonheur présent, si fières, si joyeuses,</l>
						<l n="84" num="14.6"><space unit="char" quantity="8"></space>Elle montrait un ruban noir !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1852">1852</date>
						</dateline>
					</closer>
				</div></body></text></TEI>