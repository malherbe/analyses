<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Étrivières</title>
				<title type="medium">Une édition électronique</title>
				<author key="DUC">
					<name>
						<forename>Alexandre</forename>
						<surname>DUCROS</surname>
					</name>
					<date from="1823" to="1906">1823-1906</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2991 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2017</date>
				<idno type="local">DUC_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Étrivières</title>
						<author>Alexandre Ducros</author>
					</titleStmt>
						<publicationStmt>
							<publisher>gallica.bnf.fr</publisher>
							<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k373777b.r=alexandre%20Ducros%20les%20%C3%A9trivi%C3%A8res?rk=21459;2</idno>
						</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Étrivières</title>
								<title>(1867-1885)</title>
								<author>Alexandre Ducros</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. GAUTHERIN</publisher>
									<date when="1898">1898</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Étrivières</title>
						<author>Alexandre Ducros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>IMPRIMERIE ALCAN-LEVY</publisher>
							<date when="1870">1870</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Étrivières</title>
						<title>(1862-1872)</title>
						<author>Alexandre Ducros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>A LECHEVALIER, LIBRAIRE-ÉDITEUR</publisher>
							<date when="1875">1875</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1867" to="1885">1867-1885</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface ainsi que les autres parties liminaires ne sont pas reprises dans cette édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>Certains retraits introduits automatiquement ont été supprimés afin d’être conforme à l’édition de référence</p>
				<normalization>
					<p>Les tirets simples ont été remplacés par des tirets demi-cadratin</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2018-09-05" who="RR">Correction d’un vers dans le poème "Le Maréchal Lebœuf" à partir de l’édition de 1870.</change>
				<change when="2019-04-02" who="RR">Dans l’édition de 1875, le dernier vers de "La Siffétéide" n’est pas traité comme un vers clausule.</change>
				<change when="2019-04-02" who="RR">Correction d’un vers faux dans "Le Roi boit" à partir de l’édition de 1875.</change>
				<change when="2019-04-02" who="RR">Correction d’un vers faux dans "Nous avons la guerre" à partir de l’édition de 1875.</change>

			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">Deuxième Partie</head><head type="sub_part">(1870-1871)</head><div type="poem" key="DUC102">
					<head type="main">Metz Livré !!!</head>
					<lg n="1">
						<l n="1" num="1.1">Quoi ! Metz après Sedan ! Judas après Cartouche !</l>
						<l n="2" num="1.2"><space unit="char" quantity="8"></space>Bazaine après Napoléon</l>
						<l n="3" num="1.3">Des infâmes, toujours ! Le mensonge à la bouche,</l>
						<l n="4" num="1.4"><space unit="char" quantity="8"></space>Et dans le cœur la trahison !</l>
						<l n="5" num="1.5">Les destins veulent-ils que la France succombe ?</l>
						<l n="6" num="1.6"><space unit="char" quantity="8"></space>Quoi ! du livre des nations,</l>
						<l n="7" num="1.7">Veulent-ils l’effacer et murer dans la tombe,</l>
						<l n="8" num="1.8"><space unit="char" quantity="8"></space>L’astre aux éblouissants rayons ?</l>
						<l n="9" num="1.9">Veulent-ils que la nuit se fasse sur le monde,</l>
						<l n="10" num="1.10"><space unit="char" quantity="8"></space>Et que, la France n’étant plus,</l>
						<l n="11" num="1.11">Les peuples se heurtant dans cette nuit profonde,</l>
						<l n="12" num="1.12"><space unit="char" quantity="8"></space>Après le flux aient le reflux ?</l>
						<l n="13" num="1.13">Non, Bonaparte ! non, Bazaine ! non, ô Reîtres !</l>
						<l n="14" num="1.14"><space unit="char" quantity="8"></space>Les destins conduisent ses pas ;</l>
						<l n="15" num="1.15">Et malgré les complots et malgré tous les traîtres,</l>
						<l n="16" num="1.16"><space unit="char" quantity="8"></space>La France ne périra pas !</l>
						<l n="17" num="1.17">Nous sentons redoubler l’ardeur qui nous enflamme.</l>
						<l n="18" num="1.18"><space unit="char" quantity="8"></space>Après un moment de stupeur,</l>
						<l n="19" num="1.19">Le dégoût, le mépris ont envahi notre âme,</l>
						<l n="20" num="1.20"><space unit="char" quantity="8"></space>Mats non pas l’imbécile peur.</l>
						<l n="21" num="1.21">Elle n’a pas de prise au cœur du patriote ;</l>
						<l n="22" num="1.22"><space unit="char" quantity="8"></space>Le péril accroît la valeur !</l>
						<l n="23" num="1.23">Va ! tu trahis gratis, ô louche Iscariote,</l>
						<l n="24" num="1.24"><space unit="char" quantity="8"></space>Et la France et ton Empereur.</l>
						<l n="25" num="1.25">Eh ! oui, ton Empereur, le sire Bonaparte.</l>
						<l n="26" num="1.26"><space unit="char" quantity="8"></space>Ne t’apprit-il pas en effet</l>
						<l n="27" num="1.27">L’art de piper les dés, de bizeauter la carte ?</l>
						<l n="28" num="1.28"><space unit="char" quantity="8"></space>Tu le triches et c’est bien fait</l>
						<l n="29" num="1.29">Tu disais : — « Que m’importe à moi la dynastie ?</l>
						<l n="30" num="1.30"><space unit="char" quantity="8"></space>Elle est pour mon but un moyen,</l>
						<l n="31" num="1.31">Avec plus de succès renouons la partie</l>
						<l n="32" num="1.32"><space unit="char" quantity="8"></space>Où j’ai joué Maximilien ! »</l>
						<l n="33" num="1.33">Et l’homme de Sedan, l’avachi, le despote,</l>
						<l n="34" num="1.34"><space unit="char" quantity="8"></space>Que la Nation a vomi,</l>
						<l n="35" num="1.35">Ne te devinant pas, singe de Bernadotte <ref type="noteAnchor" target="(1)">(1)</ref></l>
						<l n="36" num="1.36"><space unit="char" quantity="8"></space>T’appelait encor son ami !</l>
						<l n="37" num="1.37">Tu manquas le Mexique et tu crias : — « La chance</l>
						<l n="38" num="1.38"><space unit="char" quantity="8"></space>Rend enfin mon rêve vainqueur. »</l>
						<l n="39" num="1.39">Quand tu vis le Teuton sombre, éventrer la France,</l>
						<l n="40" num="1.40"><space unit="char" quantity="8"></space>Pour arriver jusques au cœur !</l>
					</lg>
					<lg n="2">
						<l n="41" num="2.1">O soldats, qui vouliez jusqu’à la dernière heure,</l>
						<l n="42" num="2.2"><space unit="char" quantity="8"></space>Vaincre ou mourir, qu’avez-vous dit ?</l>
						<l n="43" num="2.3">J’entends le vieil honneur qui sanglote, qui pleure,</l>
						<l n="44" num="2.4"><space unit="char" quantity="8"></space>Sous l’outrage de ce bandit !</l>
						<l n="45" num="2.5">Ah ! Le tigre qui flaire en acérant sa griffe</l>
						<l n="46" num="2.6"><space unit="char" quantity="8"></space>Est moins cruel pour le troupeau !</l>
						<l n="47" num="2.7">Pour immoler son Christ, Judas laissait Caïphe</l>
						<l n="48" num="2.8"><space unit="char" quantity="8"></space>Fournir les clous et le marteau ?</l>
						<l n="49" num="2.9">Mais Bazaine-Judas aux tigres sanguinaires,</l>
						<l n="50" num="2.10"><space unit="char" quantity="8"></space>Comme aux Caïphes sans pitié,</l>
						<l n="51" num="2.11">Livre le <foreign lang="LAT">Labarum</foreign> et les armes dernières</l>
						<l n="52" num="2.12"><space unit="char" quantity="8"></space>De son pays crucifié !…</l>
						<l n="53" num="2.13">Dites, est-il un mot, une insulte, un outrage,</l>
						<l n="54" num="2.14"><space unit="char" quantity="8"></space>Pour flétrir ce sinistre gueux ?</l>
						<l n="55" num="2.15">Lâche, est doux ; — traître, est faible, et la haine et la rage</l>
						<l n="56" num="2.16"><space unit="char" quantity="8"></space>Ne trouvent rien d’assez hideux</l>
						<l n="57" num="2.17">Pour le stigmatiser ! — Sur l’opprobre il se hausse</l>
						<l n="58" num="2.18"><space unit="char" quantity="8"></space>Et grouille dans l’impureté ;</l>
						<l n="59" num="2.19">Il écoeure et fait peur ! — Cet homme est un colosse</l>
						<l n="60" num="2.20"><space unit="char" quantity="8"></space>De gigantesque saleté !</l>
						<l n="61" num="2.21">Oh ! mais rassurez-vous, et si l’insulte manque,</l>
						<l n="62" num="2.22"><space unit="char" quantity="8"></space>Attendant l’heure des rachats,</l>
						<l n="63" num="2.23">Citoyens, regardez l’horrible saltimbanque,</l>
						<l n="64" num="2.24"><space unit="char" quantity="8"></space>Et noyez-le dans vos crachats !</l>
					</lg>
					<closer>
						<dateline>
							<placeName>Paris</placeName>,
							<date when="1870">30 Octobre 1870</date>.
						</dateline>
						<note type="footnote" id="1">Le souvenir de Bernadotte devenu roi, hantait le cerveau du misérable Bazaine,</note>
					</closer>
				</div></body></text></TEI>