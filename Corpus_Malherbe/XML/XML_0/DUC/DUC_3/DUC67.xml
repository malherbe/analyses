<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Étrivières</title>
				<title type="medium">Une édition électronique</title>
				<author key="DUC">
					<name>
						<forename>Alexandre</forename>
						<surname>DUCROS</surname>
					</name>
					<date from="1823" to="1906">1823-1906</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2991 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2017</date>
				<idno type="local">DUC_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Étrivières</title>
						<author>Alexandre Ducros</author>
					</titleStmt>
						<publicationStmt>
							<publisher>gallica.bnf.fr</publisher>
							<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k373777b.r=alexandre%20Ducros%20les%20%C3%A9trivi%C3%A8res?rk=21459;2</idno>
						</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Étrivières</title>
								<title>(1867-1885)</title>
								<author>Alexandre Ducros</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. GAUTHERIN</publisher>
									<date when="1898">1898</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Étrivières</title>
						<author>Alexandre Ducros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>IMPRIMERIE ALCAN-LEVY</publisher>
							<date when="1870">1870</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Étrivières</title>
						<title>(1862-1872)</title>
						<author>Alexandre Ducros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>A LECHEVALIER, LIBRAIRE-ÉDITEUR</publisher>
							<date when="1875">1875</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1867" to="1885">1867-1885</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface ainsi que les autres parties liminaires ne sont pas reprises dans cette édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>Certains retraits introduits automatiquement ont été supprimés afin d’être conforme à l’édition de référence</p>
				<normalization>
					<p>Les tirets simples ont été remplacés par des tirets demi-cadratin</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2018-09-05" who="RR">Correction d’un vers dans le poème "Le Maréchal Lebœuf" à partir de l’édition de 1870.</change>
				<change when="2019-04-02" who="RR">Dans l’édition de 1875, le dernier vers de "La Siffétéide" n’est pas traité comme un vers clausule.</change>
				<change when="2019-04-02" who="RR">Correction d’un vers faux dans "Le Roi boit" à partir de l’édition de 1875.</change>
				<change when="2019-04-02" who="RR">Correction d’un vers faux dans "Nous avons la guerre" à partir de l’édition de 1875.</change>

			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PREMIÈRE PARTIE</head><head type="sub_part">(1867-1870)</head><div type="poem" key="DUC67">
					<head type="main">Les Morts Expropriés<ref type="noteAnchor" target="(1)">(1)</ref></head>
					<lg n="1">
						<l n="1" num="1.1">Ainsi donc le cordeau tiré par l’architecte,</l>
						<l n="2" num="1.2">Va déranger les morts ? — Leur sommeil que respecte</l>
						<l n="3" num="1.3">Le passant incliné sur le froid monument,</l>
						<l n="4" num="1.4">Pour épeler un nom avec recueillement,</l>
						<l n="5" num="1.5">Doit être interrompu ? — Donc, la pioche sonore</l>
						<l n="6" num="1.6">Du sombre fossoyeur va retentir encore</l>
						<l n="7" num="1.7">Sur la fosse comblée, et, les morts réveillés,</l>
						<l n="8" num="1.8">Par les trous du cercueil aux ais entre-bâillés,</l>
						<l n="9" num="1.9">Surpris, regarderont si l’Archange lui-même,</l>
						<l n="10" num="1.10">A sonné les appels du jugement suprême,</l>
						<l n="11" num="1.11">Et se demanderont : — « Est-ce donc aujourd’hui,</l>
						<l n="12" num="1.12">Que le Seigneur nous fait paraître devant Lui ? »</l>
						<l n="13" num="1.13">Et, secouant les plis d’un lambeau de suaire,</l>
						<l n="14" num="1.14">Agitant, effarés, dans l’étroit ossuaire,</l>
						<l n="15" num="1.15">Leurs membres décharnés que la mort a roidis,</l>
						<l n="16" num="1.16">Eux-mêmes chanteront un long <foreign lang="LAT"> De Profundis</foreign> !</l>
					</lg>
					<lg n="2">
						<l n="17" num="2.1">Non ! Ce n’est pas le jour où le cercueil doit rendre</l>
						<l n="18" num="2.2">Le corps des trépassés dont il gardait la cendre ;</l>
						<l n="19" num="2.3">O morts ! rassurez-vous, ce n’est pas aujourd’hui,</l>
						<l n="20" num="2.4">Que l’Archange a crié ; — <foreign lang="LAT">Surgite Mortui</foreign> !</l>
						<l n="21" num="2.5">Levez-vous, cependant ! levez-vous ! nos édiles,</l>
						<l n="22" num="2.6">Hier, ont décrété que les morts inutiles,</l>
						<l n="23" num="2.7">Malgré le saint regret, le pieux souvenir,</l>
						<l n="24" num="2.8">Devant l’utilité publique, à l’avenir</l>
						<l n="25" num="2.9">Seraient expropriés ! Oui, les morts ! — On les chasse,</l>
						<l n="26" num="2.10">Du sol où, pour dormir, ils ont payé la place.</l>
						<l n="27" num="2.11">On va rouvrir leur tombe, et les cieux étonnés,</l>
						<l n="28" num="2.12">Verront leurs ossements froidement profanés,</l>
						<l n="29" num="2.13">Par les démolisseurs aux gages de la Ville.</l>
						<l n="30" num="2.14">La réclamation, hélas ! est inutile :</l>
						<l n="31" num="2.15">Il faut un boulevard, sur lequel on pourra</l>
						<l n="32" num="2.16">Bâtir le riche immeuble et qui rapportera.</l>
						<l n="33" num="2.17">On a pris les jardins ? Les cités ouvrières ?</l>
						<l n="34" num="2.18">Votez, pour que l’on prenne aussi les cimetières !</l>
						<l n="35" num="2.19">Vous n’aviez pas songé jusqu’ici que le mort</l>
						<l n="36" num="2.20">Pouvait être au vivant d’un utile rapport ?</l>
						<l n="37" num="2.21">Votez ! pour que la tombe où repose l’ancêtre,</l>
						<l n="38" num="2.22">Se transforme en terrain à huit cents francs le mètre !</l>
					</lg>
					<lg n="3">
						<l n="39" num="3.1">O morts ! en allez-vous devant l’alignement !</l>
						<l n="40" num="3.2">On vous paiera les frais de déménagement !</l>
						<l n="41" num="3.3">Dans un coin du cercueil roulez vos longs suaires,</l>
						<l n="42" num="3.4">La croix, le buis bénit, le livre de prières,</l>
						<l n="43" num="3.5">Que la religion, la tendre piété</l>
						<l n="44" num="3.6">Mirent entre vos mains et pour l’Éternité ;</l>
						<l n="45" num="3.7">Un camion viendra vous charger tout à l’heure !</l>
						<l n="46" num="3.8">N’oubliez rien surtout dans l’obscure demeure ;</l>
						<l n="47" num="3.9">Ni la croix, ni le buis, ni le livre ; — demain</l>
						<l n="48" num="3.10">Quelque maçon pourra sur le bord du chemin,</l>
						<l n="49" num="3.11">Les jeter en chantant, ou bien s’il les ramasse,</l>
						<l n="50" num="3.12">Il les vendra pour boire au chiffonnier qui passe !</l>
						<l n="51" num="3.13">O morts ! vous aviez cru, quand se ferma votre œil,</l>
						<l n="52" num="3.14">Que vous alliez avoir le repos du cercueil ?</l>
						<l n="53" num="3.15">Que vous croisant les bras sous les bandes funèbres,</l>
						<l n="54" num="3.16">Vous alliez, dans la mort aux épaisses ténèbres,</l>
						<l n="55" num="3.17">Connaître enfin la paix inconnue aux vivants,</l>
						<l n="56" num="3.18">Et que, dans vos cercueils, seuls et sans mouvements,</l>
						<l n="57" num="3.19">Vous goûteriez ce calme, énigme poursuivie,</l>
						<l n="58" num="3.20">Que la mort ne veut pas expliquer à la vie ?</l>
						<l n="59" num="3.21">Eh bien, détrompez-vous et sortez du repos !</l>
						<l n="60" num="3.22">Réveillez-vous, ô morts ! et rassemblez vos os !</l>
						<l n="61" num="3.23">Le tracas vous poursuit jusque dans votre tombe ;</l>
						<l n="62" num="3.24">Il faut qu’une autre fois, sur vous la terre tombe !</l>
						<l n="63" num="3.25">Allons ! interrompez, ô morts, votre sommeil,</l>
						<l n="64" num="3.26">Sortez de votre nuit, paraissez au soleil,</l>
						<l n="65" num="3.27">Rouvrez à ses clartés vos grands yeux sans prunelles.</l>
						<l n="66" num="3.28">Remontez au séjour des haines éternelles,</l>
						<l n="67" num="3.29">Vous étiez bien, couchés ? mais c’est assez dormir ;</l>
						<l n="68" num="3.30">O morts ! réveillez-vous ! les maçons vont venir !</l>
						<l n="69" num="3.31">Il faut, entendez-vous ? — il faut leur faire place ;</l>
						<l n="70" num="3.32">Vous les gênez, enfin ! — ils vont venir en masse,</l>
						<l n="71" num="3.33">Le plan dans une main et dans l’autre un marteau,</l>
						<l n="72" num="3.34">Pour ouvrir une voie, effondrer un tombeau !</l>
						<l n="73" num="3.35">Avec un mot en vain vous croyez les confondre ;</l>
						<l n="74" num="3.36">— « Le respect ! » dites-vous ? — « Le plan ! » vont-ils répondre.</l>
						<l n="75" num="3.37">Aussi gardez-vous bien d’invoquer le respect,</l>
						<l n="76" num="3.38">En face de la loi le mort serait suspect !</l>
						<l n="77" num="3.39">Le vote du Sénat déclare, sans réplique,</l>
						<l n="78" num="3.40">La profanation d’utilité publique !</l>
					</lg>
					<lg n="4">
						<l n="79" num="4.1">Ce que n’ont jamais vu tous les siècles passés,</l>
						<l n="80" num="4.2">Le nôtre l’aura vu ; chasser les trépassés !</l>
						<l n="81" num="4.3">Jadis, les citoyens de la Rome païenne,</l>
						<l n="82" num="4.4">A manteau laticlave ou robe plébéienne ;</l>
						<l n="83" num="4.5">Noblesse ou populace, honoraient saintement</l>
						<l n="84" num="4.6">La dépouille des morts ; — leurs mains pieusement,</l>
						<l n="85" num="4.7">La recueillaient dans l’urne avec soin parfumée,</l>
						<l n="86" num="4.8">Et la maison gardait la cendre bien-aimée.</l>
						<l n="87" num="4.9">Le mort ne quittait point le parent qui l’avait</l>
						<l n="88" num="4.10">Tendrement consolé, chéri, lorsqu’il vivait.</l>
						<l n="89" num="4.11">Son souvenir peuplait la maison mortuaire,</l>
						<l n="90" num="4.12">Et défiait l’oubli, — ce deuxième suaire !</l>
						<l n="91" num="4.13">Chacun gardait ses morts, craignant qu’en d’autres lieux,</l>
						<l n="92" num="4.14">On ne sut protéger leurs restes précieux,</l>
						<l n="93" num="4.15">Et non pas seulement du temps où Rome libre,</l>
						<l n="94" num="4.16">Du Gange à l’Hellespont faisait régner le Tibre ?</l>
						<l n="95" num="4.17">Non, je n’en parle pas, mais du temps détesté,</l>
						<l n="96" num="4.18">Où Rome dans les fers pleurait sa liberté.</l>
						<l n="97" num="4.19">On prenait à ses fils et leurs biens et leur vie,</l>
						<l n="98" num="4.20">On menait aux égouts cette race avilie,</l>
						<l n="99" num="4.21">Et les plus durs travaux et les plus dures lois,</l>
						<l n="100" num="4.22">Étaient pour les Romains le prix des vieux exploits !</l>
						<l n="101" num="4.23">Cependant les tyrans de ces vainqueurs du monde,</l>
						<l n="102" num="4.24">Ne heurtèrent jamais leur piété profonde,</l>
						<l n="103" num="4.25">Car ils laissaient en paix la cendre des aïeux ;</l>
						<l n="104" num="4.26">Ils avaient un mérite ; ils redoutaient les Dieux !</l>
					</lg>
					<lg n="5">
						<l n="105" num="5.1">Vous ne redoutez rien, ni les Dieux, ni les hommes,</l>
						<l n="106" num="5.2">O vous, démolisseurs ! Pas même les fantômes</l>
						<l n="107" num="5.3">De ceux que vous aurez chassés de leurs tombeaux !</l>
						<l n="108" num="5.4">S’ils allaient revenir, drapés dans les lambeaux</l>
						<l n="109" num="5.5">Du suaire, tracer de leur doigt de squelettes :</l>
						<l n="110" num="5.6"><hi rend="ital">Mane</hi> ! <hi rend="ital">Thecel</hi> ! <hi rend="ital">Pharès</hi> ! dans vos palais en fêtes ?</l>
					</lg>
					<lg n="6">
						<l n="111" num="6.1">— « Non ! les morts sont bien morts ! — dites-vous. Et d’abord,</l>
						<l n="112" num="6.2">Si nous allons fouiller dans les champs de la mort,</l>
						<l n="113" num="6.3">C’est pour vous, pour vous seuls. Notre sollicitude,</l>
						<l n="114" num="6.4">Fait de votre bien-être une constante étude.</l>
						<l n="115" num="6.5">Ingrats ! vous nous blâmez ? vos neveux nous loueront ;</l>
						<l n="116" num="6.6">C’est un El Dorado dans lequel ils vivront,</l>
						<l n="117" num="6.7">Voyez comme Paris par nos soins se transforme !</l>
						<l n="118" num="6.8">Nous sommes des géants et notre œuvre est énorme !</l>
						<l n="119" num="6.9">L’Europe émerveillée admire nos travaux.</l>
						<l n="120" num="6.10">Nous vous faisons un peuple, un peuple sans rivaux.</l>
						<l n="121" num="6.11">Cessez de murmurer et sachez reconnaître,</l>
						<l n="122" num="6.12">Que nous n’avons qu’un but, un seul, votre bien-être. »</l>
					</lg>
					<lg n="7">
						<l n="123" num="7.1">Merci ! mais s’il vous faut pour cela, pour vos plans</l>
						<l n="124" num="7.2">Fouiller le sol béni de ces pauvres os blancs,</l>
						<l n="125" num="7.3">S’il vous faut morceler la terre des reliques,</l>
						<l n="126" num="7.4">Dirigez vos cordeaux sur des lignes obliques</l>
						<l n="127" num="7.5">Et laissez les tombeaux, nous vous applaudirons,</l>
						<l n="128" num="7.6">Et s’il vous faut des bras nous vous les fournirons.</l>
						<l n="129" num="7.7">Mais tant que vous voudrez profaner les enceintes,</l>
						<l n="130" num="7.8">Où dorment les débris des affections saintes,</l>
						<l n="131" num="7.9">Comment vous applaudir, ô vous, qui sans remords,</l>
						<l n="132" num="7.10">Gâchez votre mortier des cendres de nos morts !</l>
					</lg>
					<closer>
						<dateline>
							<placeName>Paris</placeName>,
							<date when="1868">21 Janvier 1868</date>.
						</dateline>
						<note type="footnote" id="1">Il s’agissait de la création d’un boulevard qui aurait traversé le cimetière Montmartre.</note>
					</closer>
				</div></body></text></TEI>