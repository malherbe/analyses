<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Étrivières</title>
				<title type="medium">Une édition électronique</title>
				<author key="DUC">
					<name>
						<forename>Alexandre</forename>
						<surname>DUCROS</surname>
					</name>
					<date from="1823" to="1906">1823-1906</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2991 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2017</date>
				<idno type="local">DUC_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Étrivières</title>
						<author>Alexandre Ducros</author>
					</titleStmt>
						<publicationStmt>
							<publisher>gallica.bnf.fr</publisher>
							<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k373777b.r=alexandre%20Ducros%20les%20%C3%A9trivi%C3%A8res?rk=21459;2</idno>
						</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Étrivières</title>
								<title>(1867-1885)</title>
								<author>Alexandre Ducros</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. GAUTHERIN</publisher>
									<date when="1898">1898</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Étrivières</title>
						<author>Alexandre Ducros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>IMPRIMERIE ALCAN-LEVY</publisher>
							<date when="1870">1870</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Étrivières</title>
						<title>(1862-1872)</title>
						<author>Alexandre Ducros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>A LECHEVALIER, LIBRAIRE-ÉDITEUR</publisher>
							<date when="1875">1875</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1867" to="1885">1867-1885</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface ainsi que les autres parties liminaires ne sont pas reprises dans cette édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>Certains retraits introduits automatiquement ont été supprimés afin d’être conforme à l’édition de référence</p>
				<normalization>
					<p>Les tirets simples ont été remplacés par des tirets demi-cadratin</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2018-09-05" who="RR">Correction d’un vers dans le poème "Le Maréchal Lebœuf" à partir de l’édition de 1870.</change>
				<change when="2019-04-02" who="RR">Dans l’édition de 1875, le dernier vers de "La Siffétéide" n’est pas traité comme un vers clausule.</change>
				<change when="2019-04-02" who="RR">Correction d’un vers faux dans "Le Roi boit" à partir de l’édition de 1875.</change>
				<change when="2019-04-02" who="RR">Correction d’un vers faux dans "Nous avons la guerre" à partir de l’édition de 1875.</change>

			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PREMIÈRE PARTIE</head><head type="sub_part">(1867-1870)</head><div type="poem" key="DUC71">
					<head type="main">Il est malade !!!</head>
					<head type="tune">(Sur l’air : — Des Fraises)</head>
					<lg n="1">
						<head type="speaker">LES JOURNAUX OFFICIEUX</head>
						<l n="1" num="1.1">Non ! Il n’est pas en péril,</l>
						<l n="2" num="1.2"><space unit="char" quantity="2"></space>Au contraire, il engraisse.</l>
					</lg>
					<lg n="2">
						<head type="speaker">LES PRÉFETS, LES GENDARMES, LES SACRISTAINS</head>
						<l n="3" num="2.1"><foreign lang="LAT">Te Deum</foreign> ! ainsi soit-il …</l>
					</lg>
					<lg n="3">
						<head type="speaker">LA FOULE</head>
						<l n="4" num="3.1">Mais, enfin, comment va-t-il !</l>
					</lg>
					<lg n="4">
						<head type="speaker">LA BOURSE</head>
						<l n="5" num="4.1"><space unit="char" quantity="6"></space>Ça baisse ! (<del hand="RR" reason="analysis" type="irrelevant">ter</del>) <ref type="noteAnchor" target="(1)">(1)</ref></l>
					</lg>
					<lg n="5">
						<head type="speaker">LA FOULE</head>
						<l n="6" num="5.1">Puisque bien portant Il est,</l>
						<l n="7" num="5.2"><space unit="char" quantity="2"></space>Qu’il parle, qu’il réponde….</l>
					</lg>
					<lg n="6">
						<head type="speaker">QUELQUES INTIMES</head>
						<l n="8" num="6.1">On dit qu’un docteur discret,</l>
						<l n="9" num="6.2">Pour connaître son secret,</l>
						<l n="10" num="6.3"><space unit="char" quantity="6"></space>Le Sonde (<del hand="RR" reason="analysis" type="irrelevant">ter</del>)</l>
					</lg>
					<lg n="7">
						<head type="speaker">LA VOIX DU MALADE</head>
						<l n="11" num="7.1">L’air est lourd, il est malsain !…</l>
						<l n="12" num="7.2"><space unit="char" quantity="2"></space>Voyez-vous ce moustique,</l>
						<l n="13" num="7.3">Rôder près du traversin ?…</l>
						<l n="14" num="7.4">Qu’il m’agace, ce cousin… <ref type="noteAnchor" target="2">(2)</ref></l>
						<l n="15" num="7.5"><space unit="char" quantity="6"></space>Ça pique (<del hand="RR" reason="analysis" type="irrelevant">ter</del>)</l>
					</lg>
					<lg n="8">
						<head type="speaker">LE PRINCE JÉRÔME NAPOLÉON</head>
						<l n="16" num="8.1">Pour calmer ce peuple ému,</l>
						<l n="17" num="8.2"><space unit="char" quantity="2"></space>Montrons-nous en personne,</l>
						<l n="18" num="8.3">Car je crois être attendu,</l>
						<l n="19" num="8.4"><space unit="char" quantity="2"></space>A la barrière du…</l>
						<l n="20" num="8.5"><space unit="char" quantity="6"></space>Du trône ! (<del hand="RR" reason="analysis" type="irrelevant">ter</del>) <ref type="noteAnchor" target="(3)">(3)</ref></l>
						<l n="21" num="8.6">J’ai pour gagner à coup sûr,</l>
						<l n="22" num="8.7"><space unit="char" quantity="2"></space>Carreaux, cœurs, piques, trèfles,</l>
						<l n="23" num="8.8">Et quand le fruit sera mûr,</l>
						<l n="24" num="8.9">Je poserai la main sur…</l>
					</lg>
					<lg n="9">
						<head type="speaker">GAVROCHE l’interrompant avec un geste bien connu :</head>
						<l n="25" num="9.1"><space unit="char" quantity="6"></space>Des nèfles ! (<del hand="RR" reason="analysis" type="irrelevant">ter</del>)</l>
					</lg>
					<closer>
						<dateline>
							<placeName>Paris</placeName>,
							<date when="1869">Ier Septembre 1869</date>.
						</dateline>
						<note type="footnote" id="1">
							L’Officiel et les journaux officieux, en parlant de la maladie <lb></lb>
							de Napoléon III, essayaient de rassurer le public. Mais la <lb></lb>
							cote de la Bourse répondait chaque jour, par une baisse rapide, <lb></lb>
							aux affirmations des dits journaux et donnait la mesure de la <lb></lb>
							confiance et de la sécurité que l’on peut avoir et trouver dans <lb></lb>
							un gouvernement personnel, lorsque la fortune publique est à la <lb></lb>
							merci d’une colique royale ou impériale.
						</note>
						<note type="footnote" id="2">
							Ce cousin… C’est-à-dire Jérôme Bonaparte, qui depuis son <lb></lb>
							discours de Chalons, cherchait à renverser Napoléon III.
						</note>
						<note type="footnote" id="3">
							L’Empire tremble sur sa base ; un vent souffle qui menace de <lb></lb>
							le jeter à terre. Mais ce ne sera point pour ton profit, <lb></lb>
							ô Jérôme Bonaparte ! — Plus de trône, ni pour toi, ni pour <lb></lb>
							personne !
						</note>
					</closer>
				</div></body></text></TEI>