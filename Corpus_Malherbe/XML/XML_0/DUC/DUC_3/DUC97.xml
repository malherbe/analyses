<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Étrivières</title>
				<title type="medium">Une édition électronique</title>
				<author key="DUC">
					<name>
						<forename>Alexandre</forename>
						<surname>DUCROS</surname>
					</name>
					<date from="1823" to="1906">1823-1906</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2991 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2017</date>
				<idno type="local">DUC_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Étrivières</title>
						<author>Alexandre Ducros</author>
					</titleStmt>
						<publicationStmt>
							<publisher>gallica.bnf.fr</publisher>
							<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k373777b.r=alexandre%20Ducros%20les%20%C3%A9trivi%C3%A8res?rk=21459;2</idno>
						</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Étrivières</title>
								<title>(1867-1885)</title>
								<author>Alexandre Ducros</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. GAUTHERIN</publisher>
									<date when="1898">1898</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Étrivières</title>
						<author>Alexandre Ducros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>IMPRIMERIE ALCAN-LEVY</publisher>
							<date when="1870">1870</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Étrivières</title>
						<title>(1862-1872)</title>
						<author>Alexandre Ducros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>A LECHEVALIER, LIBRAIRE-ÉDITEUR</publisher>
							<date when="1875">1875</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1867" to="1885">1867-1885</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface ainsi que les autres parties liminaires ne sont pas reprises dans cette édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>Certains retraits introduits automatiquement ont été supprimés afin d’être conforme à l’édition de référence</p>
				<normalization>
					<p>Les tirets simples ont été remplacés par des tirets demi-cadratin</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2018-09-05" who="RR">Correction d’un vers dans le poème "Le Maréchal Lebœuf" à partir de l’édition de 1870.</change>
				<change when="2019-04-02" who="RR">Dans l’édition de 1875, le dernier vers de "La Siffétéide" n’est pas traité comme un vers clausule.</change>
				<change when="2019-04-02" who="RR">Correction d’un vers faux dans "Le Roi boit" à partir de l’édition de 1875.</change>
				<change when="2019-04-02" who="RR">Correction d’un vers faux dans "Nous avons la guerre" à partir de l’édition de 1875.</change>

			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">Deuxième Partie</head><head type="sub_part">(1870-1871)</head><div type="poem" key="DUC97">
					<head type="main">Encore Pierre Bonarparte !</head>
					<lg n="1">
						<l n="1" num="1.1">Il a fui ! Pourquoi donc ?… Après tout, qu’il s’en aille,</l>
						<l n="2" num="1.2"><space unit="char" quantity="8"></space>C’est un Bonaparte de moins.</l>
						<l n="3" num="1.3">Son ombre salissait en passant la muraille,</l>
						<l n="4" num="1.4"><space unit="char" quantity="8"></space>Qu’il rasait pour fuir les témoins.</l>
						<l n="5" num="1.5">Il sortait rarement si ce n’est quand la brume</l>
						<l n="6" num="1.6"><space unit="char" quantity="8"></space>Étendait ses voiles malsains,</l>
						<l n="7" num="1.7">A cette heure où s’en vont dans leur abject costume,</l>
						<l n="8" num="1.8"><space unit="char" quantity="8"></space>Les grinches et les assassins.</l>
						<l n="9" num="1.9">Ceux-ci le connaissaient. — Ils disaient : — « C’est un frère ! »</l>
						<l n="10" num="1.10"><space unit="char" quantity="8"></space>Ils ajoutaient : — « C’est un veinard ;</l>
						<l n="11" num="1.11">Il est bien épaulé ; le bougre peut tout faire,</l>
						<l n="12" num="1.12"><space unit="char" quantity="8"></space>Et se tirer du traquenard ;</l>
						<l n="13" num="1.13">Son cousin — l’Empereur, — un des nôtres, encore !</l>
						<l n="14" num="1.14"><space unit="char" quantity="8"></space>Ferme les yeux sur ses bons tours,</l>
					</lg>
					<lg n="2">
						<l n="15" num="2.1">Comme il a le bras long, il fait que tout s’ignore,</l>
						<l n="16" num="2.2"><space unit="char" quantity="8"></space>On le fait acquitter à Tours ! »</l>
						<l n="17" num="2.3">Il a fui ! — Pourquoi donc ? Lorsque Paris en armes</l>
						<l n="18" num="2.4"><space unit="char" quantity="8"></space>S’apprête à chasser l’étranger ;</l>
						<l n="19" num="2.5">Quand les mères en deuil même séchent leurs larmes,</l>
						<l n="20" num="2.6"><space unit="char" quantity="8"></space>Songeant aux morts qu’il faut venger !</l>
						<l n="21" num="2.7">Quand l’enfant transformé demandant la bataille,</l>
						<l n="22" num="2.8"><space unit="char" quantity="8"></space>L’œil intrépide, front levé,</l>
						<l n="23" num="2.9">Ajuste un grand fusil à sa petite taille,</l>
						<l n="24" num="2.10"><space unit="char" quantity="8"></space>Et fait résonner le pavé ;</l>
						<l n="25" num="2.11">Quand la Patrie enfin, terrible, échevelée,</l>
						<l n="26" num="2.12"><space unit="char" quantity="8"></space>Entonne : — « Aux armes ! Citoyens ! »</l>
						<l n="27" num="2.13">Quand tous voudraient partir dans l’ardente mêlée,</l>
						<l n="28" num="2.14"><space unit="char" quantity="8"></space>Broyer les insolents Prussiens ;</l>
						<l n="29" num="2.15">Quand le péril est grand, quand c’est la mort peut-être</l>
						<l n="30" num="2.16"><space unit="char" quantity="8"></space>Qui sonne en France le tocsin,</l>
						<l n="31" num="2.17">Infidèle à l’honneur, au devoir ! — Lâche, traître,</l>
						<l n="32" num="2.18"><space unit="char" quantity="8"></space>Il fuit, monseigneur l’assassin !</l>
						<l n="33" num="2.19">Que craint-il ? Et pourquoi déserte-t-il la lutte,</l>
						<l n="34" num="2.20"><space unit="char" quantity="8"></space>A l’heure où le pays combat ?</l>
						<l n="35" num="2.21">Ah ! C’est qu’il a déjà flairé, senti la chute,</l>
						<l n="36" num="2.22"><space unit="char" quantity="8"></space>De l’Empire qui se débat ;</l>
						<l n="37" num="2.23">L’Empire conspué, l’Empire impopulaire,</l>
						<l n="38" num="2.24"><space unit="char" quantity="8"></space>Où son bras venait s’appuyer.</l>
						<l n="39" num="2.25">Il a senti passer comme un vent de colère,</l>
						<l n="40" num="2.26"><space unit="char" quantity="8"></space>Le souvenir du <subst hand="RR" reason="analysis" type="phonemization"><del>10</del><add rend="hidden">dix</add></subst> janvier ! <ref type="noteAnchor" target="(1)">(1)</ref></l>
						<l n="41" num="2.27">Tuer dans un salon, derrière une broussaille,</l>
						<l n="42" num="2.28"><space unit="char" quantity="8"></space>Rien de plus simple que cela.</l>
						<l n="43" num="2.29">Mais se battre en plein jour, en ligne de bataille,</l>
						<l n="44" num="2.30"><space unit="char" quantity="8"></space>On s’esquive comme à Zaatcha ! <ref type="noteAnchor" target="2">(2)</ref></l>
						<l n="45" num="2.31">La fuite, n’est-ce pas le refuge suprême</l>
						<l n="46" num="2.32"><space unit="char" quantity="8"></space>Des aventuriers sans pudeur ?</l>
						<l n="47" num="2.33">Il a fui ?… C’est fort bien ! — Il s’est jugé lui-même :</l>
						<l n="48" num="2.34"><space unit="char" quantity="8"></space>Vil assassin et déserteur !</l>
					</lg>
					<closer>
						<dateline>
							<placeName>Paris</placeName>,
							<date when="1870">11 Août 1870</date>.
						</dateline>
						<note type="footnote" id="1">
							C’est le 10 janvier 1870 que Pierre Bonaparte tua notre ami Victor Noir.
						</note>
						<note type="footnote" id="2">
							Étant commandant au bataillon de la Légion étrangère, au mois de mars <lb></lb>
							1849, il demanda à permuter avec un chef de bataillon de l’armée <lb></lb>
							française. La permission n’étant pas accordée assez vite, à son gré, <lb></lb>
							il revint en France avant l’assaut de Zaatcha, auquel son bataillon <lb></lb>
							devait prendre part. Le général d’Hautpoul, ministre de la guerre, <lb></lb>
							le destitua pour avoir abandonné son poste devant l’ennemi. <lb></lb>
							L’Assemblée Législative approuva par un vote cette décision. <lb></lb>
						</note>
					</closer>
				</div></body></text></TEI>