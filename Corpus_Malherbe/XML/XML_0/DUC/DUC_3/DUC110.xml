<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Étrivières</title>
				<title type="medium">Une édition électronique</title>
				<author key="DUC">
					<name>
						<forename>Alexandre</forename>
						<surname>DUCROS</surname>
					</name>
					<date from="1823" to="1906">1823-1906</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2991 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2017</date>
				<idno type="local">DUC_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Étrivières</title>
						<author>Alexandre Ducros</author>
					</titleStmt>
						<publicationStmt>
							<publisher>gallica.bnf.fr</publisher>
							<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k373777b.r=alexandre%20Ducros%20les%20%C3%A9trivi%C3%A8res?rk=21459;2</idno>
						</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Étrivières</title>
								<title>(1867-1885)</title>
								<author>Alexandre Ducros</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. GAUTHERIN</publisher>
									<date when="1898">1898</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Étrivières</title>
						<author>Alexandre Ducros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>IMPRIMERIE ALCAN-LEVY</publisher>
							<date when="1870">1870</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Étrivières</title>
						<title>(1862-1872)</title>
						<author>Alexandre Ducros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>A LECHEVALIER, LIBRAIRE-ÉDITEUR</publisher>
							<date when="1875">1875</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1867" to="1885">1867-1885</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface ainsi que les autres parties liminaires ne sont pas reprises dans cette édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>Certains retraits introduits automatiquement ont été supprimés afin d’être conforme à l’édition de référence</p>
				<normalization>
					<p>Les tirets simples ont été remplacés par des tirets demi-cadratin</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2018-09-05" who="RR">Correction d’un vers dans le poème "Le Maréchal Lebœuf" à partir de l’édition de 1870.</change>
				<change when="2019-04-02" who="RR">Dans l’édition de 1875, le dernier vers de "La Siffétéide" n’est pas traité comme un vers clausule.</change>
				<change when="2019-04-02" who="RR">Correction d’un vers faux dans "Le Roi boit" à partir de l’édition de 1875.</change>
				<change when="2019-04-02" who="RR">Correction d’un vers faux dans "Nous avons la guerre" à partir de l’édition de 1875.</change>

			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">Troisième partie</head><head type="sub_part">(1871-1885)</head><div type="poem" key="DUC110">
					<head type="main">Le printemps de 1871</head>
					<lg n="1">
						<l n="1" num="1.1">Salut, Printemps !</l>
						<l n="2" num="1.2">Les noirs autans,</l>
						<l n="3" num="1.3">Ont fui nos plaines,</l>
					</lg>
					<lg n="2">
						<l n="4" num="2.1">Oh ! viens à nous,</l>
						<l n="5" num="2.2">Clément et doux,</l>
						<l n="6" num="2.3">Et les mains pleines,</l>
					</lg>
					<lg n="3">
						<l n="7" num="3.1">Pleines de fleurs ;</l>
						<l n="8" num="3.2">Adieu, douleurs !</l>
						<l n="9" num="3.3">Adieu, froidures !</l>
					</lg>
					<lg n="4">
						<l n="10" num="4.1">Les amoureux,</l>
						<l n="11" num="4.2">Vont deux à deux,</l>
						<l n="12" num="4.3">Sous les ramures.</l>
					</lg>
					<lg n="5">
						<l n="13" num="5.1">Dans les buissons,</l>
						<l n="14" num="5.2">Les gais pinsons,</l>
						<l n="15" num="5.3">Jettent leurs trilles,</l>
					</lg>
					<lg n="6">
						<l n="16" num="6.1">Et le soleil</l>
						<l n="17" num="6.2">Clair et vermeil,</l>
						<l n="18" num="6.3">Rit aux charmilles.</l>
					</lg>
					<lg n="7">
						<l n="19" num="7.1">Oh ! sois béni,</l>
						<l n="20" num="7.2">Pour chaque nid,</l>
						<l n="21" num="7.3">Chaque fleurette !</l>
					</lg>
					<lg n="8">
						<l n="22" num="8.1">Frais messager,</l>
						<l n="23" num="8.2">Vois ; le verger.</l>
						<l n="24" num="8.3">Se met en fête !</l>
					</lg>
					<lg n="9">
						<l n="25" num="9.1">Salut, Printemps !</l>
						<l n="26" num="9.2">Voici le temps,</l>
						<l n="27" num="9.3">Où, sur les grèves</l>
					</lg>
					<lg n="10">
						<l n="28" num="10.1">Et dans les bois,</l>
						<l n="29" num="10.2">Tout bas, les voix</l>
						<l n="30" num="10.3">Chantent leurs rêves !</l>
					</lg>
					<lg n="11">
						<l n="31" num="11.1">Dans le sillon,</l>
						<l n="32" num="11.2">L’humble grillon,</l>
						<l n="33" num="11.3">Fait sa prière.</l>
					</lg>
					<lg n="12">
						<l n="34" num="12.1">Il dit : — « Merci,</l>
						<l n="35" num="12.2">Printemps ! Voici</l>
						<l n="36" num="12.3">Fleurs et lumière ! »</l>
					</lg>
					<lg n="13">
						<l n="37" num="13.1">Printemps aimé,</l>
						<l n="38" num="13.2">Tout est charmé,</l>
						<l n="39" num="13.3">De ta venue !</l>
					</lg>
					<lg n="14">
						<l n="40" num="14.1">Et tout le dit,</l>
						<l n="41" num="14.2">La fleur au nid ;</l>
						<l n="42" num="14.3">L’onde à la nue !</l>
					</lg>
					<lg n="15">
						<l n="43" num="15.1">Tout est joyeux…</l>
						<l n="44" num="15.2">Mais soucieux,</l>
						<l n="45" num="15.3">Tu nous regardes,</l>
					</lg>
					<lg n="16">
						<l n="46" num="16.1">C’est triste et doux,</l>
						<l n="47" num="16.2">Que parmi nous,</l>
						<l n="48" num="16.3">Tu te hasardes !</l>
					</lg>
					<lg n="17">
						<l n="49" num="17.1">Qu’as-tu Printemps,</l>
						<l n="50" num="17.2">Pourquoi tes chants,</l>
						<l n="51" num="17.3">Sont-ils moroses ?</l>
					</lg>
					<lg n="18">
						<l n="52" num="18.1">Dis d’où provient,</l>
						<l n="53" num="18.2">Le mal qui vient,</l>
						<l n="54" num="18.3">Pâlir tes roses ?</l>
					</lg>
					<lg n="19">
						<head type="main">LE PRINTEMPS</head>
						<l n="55" num="19.1">— « O mes amis !</l>
						<l n="56" num="19.2">Ils sont partis,</l>
						<l n="57" num="19.3">Loin de la France,</l>
					</lg>
					<lg n="20">
						<l n="58" num="20.1">« Mes oiselets,</l>
						<l n="59" num="20.2">Aux gais couplets,</l>
						<l n="60" num="20.3">Pleins d’espérance !</l>
					</lg>
					<lg n="21">
						<l n="61" num="21.1">« Ils ont eu peur,</l>
						<l n="62" num="21.2">De cette horreur.</l>
						<l n="63" num="21.3">Qu’on nomme : « Guerre »</l>
					</lg>
					<lg n="22">
						<l n="64" num="22.1">« Plaine et bosquet,</l>
						<l n="65" num="22.2">Hélas tout n’est</l>
						<l n="66" num="22.3">Qu’un cimetière !</l>
					</lg>
					<lg n="23">
						<l n="67" num="23.1">« Pourquoi chanter ?</l>
						<l n="68" num="23.2">Je viens planter,</l>
						<l n="69" num="23.3">Pour fleurs nouvelles,</l>
					</lg>
					<lg n="24">
						<l n="70" num="24.1">« Fleurs des regrets ;</l>
						<l n="71" num="24.2">Près des cyprès,</l>
						<l n="72" num="24.3">Les immortelles ! »</l>
					</lg>
					<lg n="25">
						<l n="73" num="25.1">Voyez, là-bas,</l>
						<l n="74" num="25.2">La plaine, hélas,</l>
						<l n="75" num="25.3">N’est qu’une, steppe !</l>
					</lg>
					<lg n="26">
						<l n="76" num="26.1">« Sur chaque seuil,</l>
						<l n="77" num="26.2">Dieu même en deuil,</l>
						<l n="78" num="26.3">A mis un crêpe !</l>
					</lg>
					<lg n="27">
						<l n="79" num="27.1">« Quant au matin,</l>
						<l n="80" num="27.2">Sur l’aubépin,</l>
						<l n="81" num="27.3">Brille une eau pure ;</l>
					</lg>
					<lg n="28">
						<l n="82" num="28.1">« Perles des fleurs ;</l>
						<l n="83" num="28.2">Ce sont les pleurs, »</l>
						<l n="84" num="28.3">De la Nature !</l>
					</lg>
					<lg n="29">
						<l n="85" num="29.1">« Plus de chansons !…</l>
						<l n="86" num="29.2">Vers les buissons,</l>
						<l n="87" num="29.3">Voici les veuves,</l>
					</lg>
					<lg n="30">
						<l n="88" num="30.1">« Avec l’enfant ;</l>
						<l n="89" num="30.2">Tous, vont cherchant,</l>
						<l n="90" num="30.3">Des fosses neuves ! »</l>
					</lg>
					<lg n="31">
						<l n="91" num="31.1">O doux Printemps !</l>
						<l n="92" num="31.2">Fleuris nos champs,</l>
						<l n="93" num="31.3">A faire envie !</l>
					</lg>
					<lg n="32">
						<l n="94" num="32.1">Fécond et fort,</l>
						<l n="95" num="32.2">Où fut la mort,</l>
						<l n="96" num="32.3">Refais la vie !</l>
					</lg>
					<closer>
						<dateline>
							<placeName>Paris</placeName>,
							<date when="1871">3 Mai 1871</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>