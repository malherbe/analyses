<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Étrivières</title>
				<title type="medium">Une édition électronique</title>
				<author key="DUC">
					<name>
						<forename>Alexandre</forename>
						<surname>DUCROS</surname>
					</name>
					<date from="1823" to="1906">1823-1906</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2991 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2017</date>
				<idno type="local">DUC_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Étrivières</title>
						<author>Alexandre Ducros</author>
					</titleStmt>
						<publicationStmt>
							<publisher>gallica.bnf.fr</publisher>
							<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k373777b.r=alexandre%20Ducros%20les%20%C3%A9trivi%C3%A8res?rk=21459;2</idno>
						</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Étrivières</title>
								<title>(1867-1885)</title>
								<author>Alexandre Ducros</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. GAUTHERIN</publisher>
									<date when="1898">1898</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Étrivières</title>
						<author>Alexandre Ducros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>IMPRIMERIE ALCAN-LEVY</publisher>
							<date when="1870">1870</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Étrivières</title>
						<title>(1862-1872)</title>
						<author>Alexandre Ducros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>A LECHEVALIER, LIBRAIRE-ÉDITEUR</publisher>
							<date when="1875">1875</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1867" to="1885">1867-1885</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface ainsi que les autres parties liminaires ne sont pas reprises dans cette édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>Certains retraits introduits automatiquement ont été supprimés afin d’être conforme à l’édition de référence</p>
				<normalization>
					<p>Les tirets simples ont été remplacés par des tirets demi-cadratin</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2018-09-05" who="RR">Correction d’un vers dans le poème "Le Maréchal Lebœuf" à partir de l’édition de 1870.</change>
				<change when="2019-04-02" who="RR">Dans l’édition de 1875, le dernier vers de "La Siffétéide" n’est pas traité comme un vers clausule.</change>
				<change when="2019-04-02" who="RR">Correction d’un vers faux dans "Le Roi boit" à partir de l’édition de 1875.</change>
				<change when="2019-04-02" who="RR">Correction d’un vers faux dans "Nous avons la guerre" à partir de l’édition de 1875.</change>

			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PREMIÈRE PARTIE</head><head type="sub_part">(1867-1870)</head><div type="poem" key="DUC80">
					<head type="main">La sœur Saint-Antoine</head>
					<div type="section" n="1">
						<head type="number">I</head>
						<lg n="1">
							<l n="1" num="1.1">Sur le parvis de Notre-Dame,</l>
							<l n="2" num="1.2">Un corbillard stationnait,</l>
							<l n="3" num="1.3">Attendant le corps d’une femme.</l>
							<l n="4" num="1.4">D’une sainte ! — Et chacun pleurait.</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1">La sur Saint-Antoine était morte,</l>
							<l n="6" num="2.2">Après avoir sur son chemin,</l>
							<l n="7" num="2.3">Pendant soixante ans, humble et forte,</l>
							<l n="8" num="2.4">Aux souffrances tendu la main.</l>
						</lg>
						<lg n="3">
							<l n="9" num="3.1">Auprès du lit où l’agonie,</l>
							<l n="10" num="3.2">N’a que l’isolement affreux,</l>
							<l n="11" num="3.3">Sa charité, douce, infinie,</l>
							<l n="12" num="3.4">Versait le baume aux malheureux.</l>
						</lg>
						<lg n="4">
							<l n="13" num="4.1">Elle était la Foi qui console,</l>
							<l n="14" num="4.2">Elle était l’Espoir qui soutient,</l>
							<l n="15" num="4.3">Et son regard et sa parole,</l>
							<l n="16" num="4.4">A qui souffrait faisaient du bien.</l>
						</lg>
						<lg n="5">
							<l n="17" num="5.1">Elle inondait la route amère,</l>
							<l n="18" num="5.2">Des clairs rayons de sa douceur ;</l>
							<l n="19" num="5.3">L’Orphelin lui disait : — « Ma mère ! »</l>
							<l n="20" num="5.4">Et le sans famille : — « Ma sœur ! »</l>
						</lg>
					</div>
					<div type="section" n="2">
						<head type="number">II</head>
						<lg n="1">
							<l n="21" num="1.1">Je n’aime point ces confréries.</l>
							<l n="22" num="1.2">Où des êtres claquemurés,</l>
							<l n="23" num="1.3">Chantent les longues Litanies</l>
							<l n="24" num="1.4">Des extatiques désœuvrés !</l>
						</lg>
						<lg n="2">
							<l n="25" num="2.1">Leur zèle à tous est inutile,</l>
							<l n="26" num="2.2">Et le monde sait ce que vaut</l>
							<l n="27" num="2.3">Cette dévotion stérile,</l>
							<l n="28" num="2.4">Que Dieu rejettera Là-Haut !</l>
						</lg>
						<lg n="3">
							<l n="29" num="3.1">A quoi bon la ferveur qu’affiche,</l>
							<l n="30" num="3.2">Cet ordre pouilleux que voilà,</l>
							<l n="31" num="3.3">Gueusant la pièce avec la miche ?</l>
							<l n="32" num="3.4">Grands paresseux que ces gens-là !</l>
						</lg>
					</div>
					<div type="section" n="3">
						<head type="number">III</head>
						<lg n="1">
							<l n="33" num="1.1">Mais, cet essaim béni de femmes,</l>
							<l n="34" num="1.2">Qui, pleines d’abnégations,</l>
							<l n="35" num="1.3">Ont saintement fermé leurs âmes,</l>
							<l n="36" num="1.4">Au monde, à ses séductions ;</l>
						</lg>
						<lg n="2">
							<l n="37" num="2.1">Qui, fuyant les bruyantes fêtes,</l>
							<l n="38" num="2.2">S’en vont avec humilité,</l>
							<l n="39" num="2.3">Au fond des pieuses retraites,</l>
							<l n="40" num="2.4">Pour secourir la pauvreté ;</l>
						</lg>
						<lg n="3">
							<l n="41" num="3.1">Saintes filles que rien n’effraie,</l>
							<l n="42" num="3.2">Dont rien ne ralentit l’ardeur,</l>
							<l n="43" num="3.3">Ni du corps la béante plaie,</l>
							<l n="44" num="3.4">Ni celle plus grande du cœur ;</l>
						</lg>
						<lg n="4">
							<l n="45" num="4.1">Qu’on les aime, qu’on les respecte !</l>
							<l n="46" num="4.2">Se dévouer, tel fût leur vœu,</l>
							<l n="47" num="4.3">Leur mission n’est point suspecte ;</l>
							<l n="48" num="4.4">Elles sont bien Filles de Dieu !</l>
						</lg>
					</div>
					<closer>
						<dateline>
							<placeName>Paris</placeName>,
							<date when="1870">10 mars 1870</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>