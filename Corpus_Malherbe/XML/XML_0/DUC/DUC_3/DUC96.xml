<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Étrivières</title>
				<title type="medium">Une édition électronique</title>
				<author key="DUC">
					<name>
						<forename>Alexandre</forename>
						<surname>DUCROS</surname>
					</name>
					<date from="1823" to="1906">1823-1906</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2991 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2017</date>
				<idno type="local">DUC_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Étrivières</title>
						<author>Alexandre Ducros</author>
					</titleStmt>
						<publicationStmt>
							<publisher>gallica.bnf.fr</publisher>
							<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k373777b.r=alexandre%20Ducros%20les%20%C3%A9trivi%C3%A8res?rk=21459;2</idno>
						</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Étrivières</title>
								<title>(1867-1885)</title>
								<author>Alexandre Ducros</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. GAUTHERIN</publisher>
									<date when="1898">1898</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Étrivières</title>
						<author>Alexandre Ducros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>IMPRIMERIE ALCAN-LEVY</publisher>
							<date when="1870">1870</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Étrivières</title>
						<title>(1862-1872)</title>
						<author>Alexandre Ducros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>A LECHEVALIER, LIBRAIRE-ÉDITEUR</publisher>
							<date when="1875">1875</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1867" to="1885">1867-1885</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface ainsi que les autres parties liminaires ne sont pas reprises dans cette édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>Certains retraits introduits automatiquement ont été supprimés afin d’être conforme à l’édition de référence</p>
				<normalization>
					<p>Les tirets simples ont été remplacés par des tirets demi-cadratin</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2018-09-05" who="RR">Correction d’un vers dans le poème "Le Maréchal Lebœuf" à partir de l’édition de 1870.</change>
				<change when="2019-04-02" who="RR">Dans l’édition de 1875, le dernier vers de "La Siffétéide" n’est pas traité comme un vers clausule.</change>
				<change when="2019-04-02" who="RR">Correction d’un vers faux dans "Le Roi boit" à partir de l’édition de 1875.</change>
				<change when="2019-04-02" who="RR">Correction d’un vers faux dans "Nous avons la guerre" à partir de l’édition de 1875.</change>

			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">Deuxième Partie</head><head type="sub_part">(1870-1871)</head><div type="poem" key="DUC96">
					<head type="main">Quinze Août</head>
					<head type="sub">Fête de l’Empereur</head>
					<lg n="1">
						<l n="1" num="1.1">Paris est-il désert ? La ville est-elle morte ?</l>
						<l n="2" num="1.2"><space unit="char" quantity="8"></space>Amis, quel silence partout !</l>
						<l n="3" num="1.3">Naguère, une clameur s’élevant grande et forte,</l>
						<l n="4" num="1.4"><space unit="char" quantity="8"></space>A pareil jour criait ; — « Quinze Août ! «</l>
						<l n="5" num="1.5">Partout flottaient au vent banderolle, oriflamme ;</l>
						<l n="6" num="1.6"><space unit="char" quantity="8"></space>Cloches, canons ; bronzes émus,</l>
						<l n="7" num="1.7">De la grande esplanade aux tours de Notre-Dame,</l>
						<l n="8" num="1.8"><space unit="char" quantity="8"></space>Hurlaient : <foreign lang="LAT">Te Deum Laudamus</foreign> !</l>
						<l n="9" num="1.9">De tout côté montaient les chants et les prières,</l>
						<l n="10" num="1.10"><space unit="char" quantity="8"></space>De par l’avis officiel.</l>
						<l n="11" num="1.11">Le soir, les monuments s’habillaient de lumières ;</l>
						<l n="12" num="1.12"><space unit="char" quantity="8"></space>Ruggieri faisait honte au ciel, <ref type="noteAnchor" target="(1)">(1)</ref></l>
						<l n="13" num="1.13">On fêtait l’Empereur. — Aujourd’hui quel contraste !</l>
						<l n="14" num="1.14"><space unit="char" quantity="8"></space>La ville est calme, pas un cri.</l>
						<l n="15" num="1.15">Aux jours d’enivrement succède un jour néfaste ;</l>
						<l n="16" num="1.16"><space unit="char" quantity="8"></space>Le ciel fait honte à Ruggieri !</l>
						<l n="17" num="1.17">Rentrez à l’arsenal vos chandelles romaines,</l>
						<l n="18" num="1.18"><space unit="char" quantity="8"></space>Et vos bombes et vos pétards,</l>
						<l n="19" num="1.19">Les millions de feux des étoiles sereines,</l>
						<l n="20" num="1.20"><space unit="char" quantity="8"></space>Plaisent bien mieux à nos regards.</l>
						<l n="21" num="1.21">Les canons se sont tus, les cloches sont muettes ;</l>
						<l n="22" num="1.22"><space unit="char" quantity="8"></space>Pas de chamades, de rappel,</l>
						<l n="23" num="1.23">Pas d’habits chamarrés, de pompons et d’aigrettes,</l>
						<l n="24" num="1.24"><space unit="char" quantity="8"></space>Sous les balcons, au Carrousel.</l>
						<l n="25" num="1.25">On a clos les volets, chose prudente et sage,</l>
						<l n="26" num="1.26"><space unit="char" quantity="8"></space>Et, sur la porte du château,</l>
						<l n="27" num="1.27">Gavroche peut, chantant : — « L’Empire déménage. »</l>
						<l n="28" num="1.28"><space unit="char" quantity="8"></space>Aller poser un écriteau !</l>
					</lg>
					<closer>
						<dateline>
							<placeName>Paris</placeName>,
							<date when="1870">15 Août 1870</date>.— 10 heures du soir.
						</dateline>
						<note type="footnote" id="1">Ruggieri, artificier des fêtes impériales.</note>
					</closer>
				</div></body></text></TEI>