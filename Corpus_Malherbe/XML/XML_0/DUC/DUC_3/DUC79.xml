<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Étrivières</title>
				<title type="medium">Une édition électronique</title>
				<author key="DUC">
					<name>
						<forename>Alexandre</forename>
						<surname>DUCROS</surname>
					</name>
					<date from="1823" to="1906">1823-1906</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2991 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2017</date>
				<idno type="local">DUC_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Étrivières</title>
						<author>Alexandre Ducros</author>
					</titleStmt>
						<publicationStmt>
							<publisher>gallica.bnf.fr</publisher>
							<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k373777b.r=alexandre%20Ducros%20les%20%C3%A9trivi%C3%A8res?rk=21459;2</idno>
						</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Étrivières</title>
								<title>(1867-1885)</title>
								<author>Alexandre Ducros</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. GAUTHERIN</publisher>
									<date when="1898">1898</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Étrivières</title>
						<author>Alexandre Ducros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>IMPRIMERIE ALCAN-LEVY</publisher>
							<date when="1870">1870</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Étrivières</title>
						<title>(1862-1872)</title>
						<author>Alexandre Ducros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>A LECHEVALIER, LIBRAIRE-ÉDITEUR</publisher>
							<date when="1875">1875</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1867" to="1885">1867-1885</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface ainsi que les autres parties liminaires ne sont pas reprises dans cette édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>Certains retraits introduits automatiquement ont été supprimés afin d’être conforme à l’édition de référence</p>
				<normalization>
					<p>Les tirets simples ont été remplacés par des tirets demi-cadratin</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2018-09-05" who="RR">Correction d’un vers dans le poème "Le Maréchal Lebœuf" à partir de l’édition de 1870.</change>
				<change when="2019-04-02" who="RR">Dans l’édition de 1875, le dernier vers de "La Siffétéide" n’est pas traité comme un vers clausule.</change>
				<change when="2019-04-02" who="RR">Correction d’un vers faux dans "Le Roi boit" à partir de l’édition de 1875.</change>
				<change when="2019-04-02" who="RR">Correction d’un vers faux dans "Nous avons la guerre" à partir de l’édition de 1875.</change>

			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PREMIÈRE PARTIE</head><head type="sub_part">(1867-1870)</head><div type="poem" key="DUC79">
					<head type="main">Le « Schéma » <ref type="noteAnchor" target="(1)">(1)</ref></head>
					<lg n="1">
						<l n="1" num="1.1">— « <hi rend="ital">Buvons, amis, fussent-ils mille</hi>,</l>
						<l n="2" num="1.2">Comme dit un chœur d’Halévy,</l>
						<l n="3" num="1.3"><hi rend="ital">A tous les membres du Concile</hi> ! »</l>
						<l n="4" num="1.4">Veuillot lui-même en est ravi.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">Le Concile est dans l’allégresse,</l>
						<l n="6" num="2.2">Et c’est en vain qu’il s’alarma,</l>
						<l n="7" num="2.3">Le Saint Père aujourd’hui s’empresse,</l>
						<l n="8" num="2.4">De lui remettre le <hi rend="ital">schéma</hi>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">La question est des plus graves,</l>
						<l n="10" num="3.2">Car il s’agit de définir,</l>
						<l n="11" num="3.3">Si l’élu des pieux conclaves,</l>
						<l n="12" num="3.4">Comme un sacristain peut faillir.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">Fouillez dans les bibliothèques,</l>
						<l n="14" num="4.2">Le bon vieux Pie, — O <hi rend="ital">Sancta Crux</hi> !</l>
						<l n="15" num="4.3">Attend, Doctissimes Évêques,</l>
						<l n="16" num="4.4">Que vous prononciez le <foreign lang="LAT">Fiat Lux</foreign>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">Au zèle rien n’est impossible,</l>
						<l n="18" num="5.2">Or, vous, faillibles, vous pouvez</l>
						<l n="19" num="5.3">Déclarer le Pape infaillible,</l>
						<l n="20" num="5.4">Et c’est Rome que vous sauvez !</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1">On pourra rire un peu, — mais, qu’est-ce,</l>
						<l n="22" num="6.2">Alors qu’on sauve avec un mot,</l>
						<l n="23" num="6.3">Les clés du ciel… et de la caisse ?</l>
						<l n="24" num="6.4">De grâce, prononcez bientôt.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1">Car vous ne voyez pas la plaie ;</l>
						<l n="26" num="7.2">Hélas ! l’argent pontifical</l>
						<l n="27" num="7.3">Est mis, comme fausse monnaie,</l>
						<l n="28" num="7.4">A l’index par le droit fiscal, <ref type="noteAnchor" target="2">(2)</ref></l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1">Personne n’en veut !… on redoute</l>
						<l n="30" num="8.2">De voir demain en discrédit,</l>
						<l n="31" num="8.3">Le Pape mis en banqueroute,</l>
						<l n="32" num="8.4">Comme un épicier qui faillit.</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1">Déclarez le Pape infaillible,</l>
						<l n="34" num="9.2">Dans son pieux apostolat,</l>
						<l n="35" num="9.3">Il ne sera pas impossible</l>
						<l n="36" num="9.4">Qu’il obtienne son <hi rend="ital">Concordat</hi>.</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1">Et, comme on ne veut plus qu’on pende,</l>
						<l n="38" num="10.2">Ou qu’on brûle dans un <hi rend="ital">auto</hi>, <ref type="noteAnchor" target="(3)">(3)</ref></l>
						<l n="39" num="10.3">Vous pourrez infliger l’amende,</l>
						<l n="40" num="10.4">A quelques vieux Juifs du Ghetto.</l>
					</lg>
					<lg n="11">
						<l n="41" num="11.1">Et Veuillot qui toujours se fâche,</l>
						<l n="42" num="11.2">Joyeux et prêt à pardonner,</l>
						<l n="43" num="11.3">De Gambon<ref type="noteAnchor" target="(4)">(4)</ref> empruntant la vache,</l>
						<l n="44" num="11.4">Ira se faire… vacciner ! <ref type="noteAnchor" target="(5)">(5)</ref></l>
					</lg>
					<closer>
						<dateline>
							<placeName>Paris</placeName>,
							<date when="1870">9 Mars 1870</date>.
						</dateline>
						<note type="footnote" id="1">
								Schéma (Ské-ma). Rédaction d’une proposition soumise <lb></lb>
								par le Pape aux lumières d’un Concile. <lb></lb>
							</note>
						<note type="footnote" id="2">
								La monnaie Pontificale était refusée par le Fisc, <lb></lb>
								la Banque et le Commerce,
							</note>
						<note type="footnote" id="3">
								Pour auto-da-fé.
						</note>
						<note type="footnote" id="4">
								Charles-Ferdinand Gambon, ancien représentant du <lb></lb>
								peuple en 1848. Il fut condamné à la déportation par <lb></lb>
								la Haute-Cour de Versailles, lors du Coup d’État. <lb></lb>
								Redevenu libre, il refusa l’impôt au gouvernement de <lb></lb>
								Napoléon III. Le fisc, pour se payer, lui fit saisir <lb></lb>
								une vache que Rochefort fit racheter au moyen d’une <lb></lb>
								souscription,
							</note>
						<note type="footnote" id="5">
								On proclamait le vaccin de génisse comme infaillible <lb></lb>
								pour prévenir les effets de la petite vérole ; M. Veuillot <lb></lb>
								n’avait rien à redouter de la terrible maladie : <lb></lb>
								son visage était une véritable écumoire.
							</note>
					</closer>
				</div></body></text></TEI>