<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Capricieuses</title>
				<title type="medium">Une édition électronique</title>
				<author key="DUC">
					<name>
						<forename>Alexandre</forename>
						<surname>DUCROS</surname>
					</name>
					<date from="1823" to="1906">1823-1906</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>830 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DUC_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Capricieuse</title>
						<author>Alexandre Ducros</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k6101950r.r=alexandre%20ducros?rk=193134;0</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Capricieuses</title>
								<author>Alexandre Ducros</author>
								<imprint>
									<pubPlace>Nîmes</pubPlace>
									<publisher>IMPRIMERIE TYPOGRAPHIQUE BALDY ET ROGER</publisher>
									<date when="1854">1854</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1854">1854</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La dédicace en début d’ouvrage n’est pas reprise dans cette édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>Certains retraits de vers ont été supprimés conformément à l’édition de référence.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DUC12">
				<head type="main">LE RÊVE D’UN ANGE</head>
				<opener>
					<salute>A JEAN REBOUL.</salute>
					<epigraph>
						<cit>
							<quote>Pauvre mère, ton fils est mort !</quote>
							<bibl>
								<name><hi rend="ital">L’Ange et l’Enfant</hi>, J. REBOUL.</name>
							</bibl>
						</cit>
					</epigraph>
				</opener>
				<lg n="1">
					<l n="1" num="1.1">Oh ! ma mère ! pourquoi ces pleurs</l>
					<l n="2" num="1.2">Qu’en secret je te vois répandre ?</l>
					<l n="3" num="1.3">Tu me dis, hélas ! que je meurs !…</l>
					<l n="4" num="1.4">Non ; dans le ciel je vais me rendre.</l>
					<l n="5" num="1.5">Est-ce mourir d’aller à Dieu</l>
					<l n="6" num="1.6">Goûter un bonheur sans mélange ?</l>
					<l n="7" num="1.7">Que de fuir ce terrestre lieu</l>
					<l n="8" num="1.8">Pour aller vivre comme un ange ?</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1">Lorsque auprès de moi tu veillais,</l>
					<l n="10" num="2.2">Que la nuit couvrait de son voile</l>
					<l n="11" num="2.3">Le monde, et que je sommeillais,</l>
					<l n="12" num="2.4">Sur mon front une blanche étoile</l>
					<l n="13" num="2.5">Est descendue, et puis, mes yeux</l>
					<l n="14" num="2.6">Ont vu de splendides phalanges.</l>
					<l n="15" num="2.7">Oui, ma mère, j’ai vu les cieux,</l>
					<l n="16" num="2.8">Les cieux remplis de petits anges…</l>
				</lg>
				<lg n="3">
					<l n="17" num="3.1">J’ai vu, mère, j’ai vu Celui</l>
					<l n="18" num="3.2">Dont tu m’as appris la tendresse,</l>
					<l n="19" num="3.3">Que tu m’as dit être l’appui</l>
					<l n="20" num="3.4">Du faible que chacun délaisse ;</l>
					<l n="21" num="3.5">Dans des jardins toujours fleuris,</l>
					<l n="22" num="3.6">J’ai vu bien des choses étranges :</l>
					<l n="23" num="3.7">J’étais, je crois, au paradis</l>
					<l n="24" num="3.8">Et je jouais avec les anges.</l>
				</lg>
				<lg n="4">
					<l n="25" num="4.1">Nous allions sur des buissons d’or</l>
					<l n="26" num="4.2">Cueillir des fleurs toujours nouvelles,</l>
					<l n="27" num="4.3">Que l’on jetait pour prendre encor</l>
					<l n="28" num="4.4">D’autres fleurs mille fois plus belles,</l>
					<l n="29" num="4.5">El nous les tressions en festons</l>
					<l n="30" num="4.6">Aux accords des saintes louanges ;</l>
					<l n="31" num="4.7">Au Dieu d’amour nous les offrions,</l>
					<l n="32" num="4.8">Et Dieu souriait à ses anges.</l>
				</lg>
				<lg n="5">
					<l n="33" num="5.1">Devant ce Dieu tout de bonté</l>
					<l n="34" num="5.2">Je tremblais et n’osais paraître,</l>
					<l n="35" num="5.3">Quand sur ton fils il a jeté</l>
					<l n="36" num="5.4">Un doux regard, et ce bon maître</l>
					<l n="37" num="5.5">M’a dit : — Je l’ai bien entendu :</l>
					<l n="38" num="5.6">Veux-tu pour tes mortelles langes</l>
					<l n="39" num="5.7">Le nymbe d’or ? — J’ai répondu :</l>
					<l n="40" num="5.8">Je veux rester avec tes anges !</l>
				</lg>
				<lg n="6">
					<l n="41" num="6.1">Et quittant alors le bon Dieu,</l>
					<l n="42" num="6.2">Redescendant vers toi, ma mère,</l>
					<l n="43" num="6.3">Je suis venu te dire : adieu !</l>
					<l n="44" num="6.4">Puis m’envoler loin de la terre.</l>
					<l n="45" num="6.5">Mon âme, pour le paradis,</l>
					<l n="46" num="6.6">Va fuir un corps pétri de fange.</l>
					<l n="47" num="6.7">Ici-bas si tu perds un fils,</l>
					<l n="48" num="6.8">Là-haut tu trouveras un ange.</l>
				</lg>
				<closer>
					<dateline>
						<placeName>Nîmes</placeName>,
						<date when="1850">1850</date>.
					</dateline>
				</closer>
			</div></body></text></TEI>