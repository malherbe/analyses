<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Capricieuses</title>
				<title type="medium">Une édition électronique</title>
				<author key="DUC">
					<name>
						<forename>Alexandre</forename>
						<surname>DUCROS</surname>
					</name>
					<date from="1823" to="1906">1823-1906</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>830 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DUC_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Capricieuse</title>
						<author>Alexandre Ducros</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k6101950r.r=alexandre%20ducros?rk=193134;0</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Capricieuses</title>
								<author>Alexandre Ducros</author>
								<imprint>
									<pubPlace>Nîmes</pubPlace>
									<publisher>IMPRIMERIE TYPOGRAPHIQUE BALDY ET ROGER</publisher>
									<date when="1854">1854</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1854">1854</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La dédicace en début d’ouvrage n’est pas reprise dans cette édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>Certains retraits de vers ont été supprimés conformément à l’édition de référence.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DUC3">
				<head type="main">RÊVERIE</head>
				<opener>
					<salute>A Mademoiselle Emma H.</salute>
				</opener>
				<lg n="1">
					<l n="1" num="1.1">Oh ! quand les chants mélodieux</l>
					<l n="2" num="1.2">Frappaient mon oreille ravie,</l>
					<l n="3" num="1.3">Alors je croyais que la vie</l>
					<l n="4" num="1.4">Était plus douce que les cieux.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Ta voix, que ma douleur regrette</l>
					<l n="6" num="2.2">Et que j’écoute en mon sommeil,</l>
					<l n="7" num="2.3">Était les perles que l’on jette</l>
					<l n="8" num="2.4">Et retombent sur le vermeil.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Elle dissipait ma souffrance,</l>
					<l n="10" num="3.2">Elle faisait rêver mon cœur,</l>
					<l n="11" num="3.3">El ce rêve était le bonheur,</l>
					<l n="12" num="3.4">Ce bonheur était l’espérance.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">Je t’écoutais en murmurant</l>
					<l n="14" num="4.2">Tout bas une douce louange ;</l>
					<l n="15" num="4.3">Puis, croyant à la voix d’un ange,</l>
					<l n="16" num="4.4">Je t’adorais en t’écoulant.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1">Et je te disais : Chante encore ;</l>
					<l n="18" num="5.2">Chante ! que la joie ait son tour !</l>
					<l n="19" num="5.3">Enfant, si ma vie est d’un jour,</l>
					<l n="20" num="5.4">Jusqu’au soir prolonge l’aurore.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1">A tes chants mon âme s’endort,</l>
					<l n="22" num="6.2">Et lorsque leur doux bruit s’achève,</l>
					<l n="23" num="6.3">Il recommence dans mon rêve,</l>
					<l n="24" num="6.4">Où j’entends mille harpes d’or.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1">Mais loin de nous tu t’es enfuie :</l>
					<l n="26" num="7.2">Maintenant, tout n’est que douleurs !</l>
					<l n="27" num="7.3">Avec l’absence sont les pleurs,</l>
					<l n="28" num="7.4">Les pleurs que nulle main n’essuie.</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1">Pourquoi partir ?… Où donc es-tu ?</l>
					<l n="30" num="8.2">Chacun te regrette, ô colombe !</l>
					<l n="31" num="8.3">Et sans toi je sens vers la tombe</l>
					<l n="32" num="8.4">S’incliner mon front abattu.</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1">Car, je t’aimais, je te le jure,</l>
					<l n="34" num="9.2">Mais d’un amour saint et profond,</l>
					<l n="35" num="9.3">Plus doux que l’auréole pure</l>
					<l n="36" num="9.4">Que les anges portent au front.</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1">N’étais-tu pas mon bien suprême ?</l>
					<l n="38" num="10.2">Ton cœur ne m’avait-il pas dit</l>
					<l n="39" num="10.3">Ce mot auquel le ciel sourit,</l>
					<l n="40" num="10.4">Ce mot qu’on dit tout bas : Je t’aime !</l>
				</lg>
				<closer>
					<dateline>
						<placeName>Nice</placeName>,
						<date when="1852">janvier 1852</date>.
					</dateline>
				</closer>
			</div></body></text></TEI>