<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Capricieuses</title>
				<title type="medium">Une édition électronique</title>
				<author key="DUC">
					<name>
						<forename>Alexandre</forename>
						<surname>DUCROS</surname>
					</name>
					<date from="1823" to="1906">1823-1906</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>830 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DUC_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Capricieuse</title>
						<author>Alexandre Ducros</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k6101950r.r=alexandre%20ducros?rk=193134;0</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Capricieuses</title>
								<author>Alexandre Ducros</author>
								<imprint>
									<pubPlace>Nîmes</pubPlace>
									<publisher>IMPRIMERIE TYPOGRAPHIQUE BALDY ET ROGER</publisher>
									<date when="1854">1854</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1854">1854</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La dédicace en début d’ouvrage n’est pas reprise dans cette édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>Certains retraits de vers ont été supprimés conformément à l’édition de référence.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DUC5">
				<head type="main">MON RÊVE D’OR</head>
				<head type="form">ROMANCE</head>
				<opener>
					<salute>A Mademoiselle E. H.</salute>
				</opener>
				<lg n="1">
					<l n="1" num="1.1">Comme un léger murmure,</l>
					<l n="2" num="1.2">Comme un chant de bonheur,</l>
					<l n="3" num="1.3">Comme une flamme pure</l>
					<l n="4" num="1.4">Parle ou brille en mon cœur,</l>
					<l n="5" num="1.5">Viens encor, mon beau rêve ;</l>
					<l n="6" num="1.6">Comme un parfum des fleurs,</l>
					<l n="7" num="1.7">Ta présence fait trêve</l>
					<l n="8" num="1.8">A mes sombres douleurs.</l>
					<l n="9" num="1.9"><space unit="char" quantity="4"></space>Ah ! viens encor,</l>
					<l n="10" num="1.10"><space unit="char" quantity="4"></space>Mon rêve d’or !</l>
				</lg>
				<lg n="2">
					<l n="11" num="2.1">Viens, comme une louange,</l>
					<l n="12" num="2.2">Une prière à Dieu,</l>
					<l n="13" num="2.3">Comme l’aile d’un ange,</l>
					<l n="14" num="2.4">Toucher mon front de feu.</l>
					<l n="15" num="2.5">Dans mon âme blessée</l>
					<l n="16" num="2.6">Et pleurant un beau jour,</l>
					<l n="17" num="2.7">Viens comme une pensée,</l>
					<l n="18" num="2.8">Comme un soupir d’amour.</l>
					<l n="19" num="2.9"><space unit="char" quantity="4"></space>Ah ! viens encor,</l>
					<l n="20" num="2.10"><space unit="char" quantity="4"></space>Mon rêve d’or !</l>
				</lg>
				<lg n="3">
					<l n="21" num="3.1">Comme une voix bénie,</l>
					<l n="22" num="3.2">Comme un rayon doré,</l>
					<l n="23" num="3.3">Guide, éclaire ma vie,</l>
					<l n="24" num="3.4">O mon rêve adoré !</l>
					<l n="25" num="3.5">Comme tout ce qui chante,</l>
					<l n="26" num="3.6">Accours dans mon sommeil.</l>
					<l n="27" num="3.7">O rêve qui m’enchante !</l>
					<l n="28" num="3.8">Pourquoi fuir au réveil ?…</l>
					<l n="29" num="3.9"><space unit="char" quantity="4"></space>Ah ! viens encor,</l>
					<l n="30" num="3.10"><space unit="char" quantity="4"></space>Mon rêve d’or !</l>
				</lg>
				<closer>
					<dateline>
						<placeName>Annecy (Savoie)</placeName>,
						<date when="1852">juillet 1852</date>.
						</dateline>
				</closer>
			</div></body></text></TEI>