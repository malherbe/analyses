<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">PREMIÈRES POÉSIES</title>
				<title type="medium">Une édition électronique</title>
				<author key="ACK">
					<name>
						<forename>Louise-Victorine</forename>
						<surname>ACKERMANN</surname>
					</name>
					<date from="1813" to="1890">1813-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>588 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">ACK_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies Diverses</title>
						<author>Louise-Victorine Ackermann</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">http://www.poesies.net/louiseackermaNpoesiesdiverses.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres de L. Ackermann</title>
						<author>Louise-Victorine Ackermann</author>
						<imprint>
							<publisher>Alphonse Lemerre, éditeur</publisher>
							<date when="1885">1885</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1862">1862</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>Les balises de pagination ont été supprimées.</p>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique
					après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="ACK14">
				<head type="number">XIV</head>
				<head type="main">La Lampe D’Héro</head>
				<lg n="1">
					<l n="1" num="1.1">De son bonheur furtif lorsque malgré l’orage</l>
					<l n="2" num="1.2">L’amant d’Héro courait s’enivrer loin du jour,</l>
					<l n="3" num="1.3">Et dans la nuit tentait de gagner à la nage</l>
					<l n="4" num="1.4"><space unit="char" quantity="8"></space>Le bord où l’attendait l’Amour,</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Une lampe envoyait, vigilante et fidèle,</l>
					<l n="6" num="2.2">En ce péril vers lui son rayon vacillant ;</l>
					<l n="7" num="2.3">On eût dit dans les cieux quelque étoile immortelle</l>
					<l n="8" num="2.4"><space unit="char" quantity="8"></space>Oui dévoilait son front tremblant.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">La mer a beau mugir et heurter ses rivages,</l>
					<l n="10" num="3.2">Les vents au sein des airs déchaîner leur effort,</l>
					<l n="11" num="3.3">Les oiseaux effrayés pousser des cris sauvages</l>
					<l n="12" num="3.4"><space unit="char" quantity="8"></space>En voyant approcher la Mort,</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">Tant que du haut sommet de la tour solitaire</l>
					<l n="14" num="4.2">Brille le signe aimé sur l’abîme en fureur,</l>
					<l n="15" num="4.3">Il ne sentira point, le nageur téméraire,</l>
					<l n="16" num="4.4"><space unit="char" quantity="8"></space>Défaillir son bras ni son cœur.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1">Comme à l’heure sinistre où la mer en sa rage</l>
					<l n="18" num="5.2">Menaçait d’engloutir cet enfant d’Abydos,</l>
					<l n="19" num="5.3">Autour de nous dans l’ombre un éternel orage</l>
					<l n="20" num="5.4"><space unit="char" quantity="8"></space>Fait gronder et bondir les flots.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1">Remplissant l’air au loin de ses clameurs funèbres,</l>
					<l n="22" num="6.2">Chaque vague en passant nous entr’ouvre un tombeau ;</l>
					<l n="23" num="6.3">Dans les mêmes dangers et les mêmes ténèbres</l>
					<l n="24" num="6.4"><space unit="char" quantity="8"></space>Nous avons le même flambeau.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1">Le pâle et doux rayon tremble encor dans la brume.</l>
					<l n="26" num="7.2">Le vent l’assaille en vain, vainement les flots sourds</l>
					<l n="27" num="7.3">La dérobent parfois sous un voile d’écume,</l>
					<l n="28" num="7.4"><space unit="char" quantity="8"></space>La clarté reparaît toujours.</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1">Et nous, les yeux levés vers la lueur lointaine,</l>
					<l n="30" num="8.2">Nous fendons pleins d’espoir les vagues en courroux ;</l>
					<l n="31" num="8.3">Au bord du gouffre ouvert la lumière incertaine</l>
					<l n="32" num="8.4"><space unit="char" quantity="8"></space>Semble d’en haut veiller sur nous.</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1">Ô phare de l’Amour ! qui dans la nuit profonde</l>
					<l n="34" num="9.2">Nous guides à travers les écueils d’ici-bas,</l>
					<l n="35" num="9.3">Toi que nous voyons luire entre le ciel et l’onde,</l>
					<l n="36" num="9.4"><space unit="char" quantity="8"></space>Lampe d’Héro, ne t’éteins pas !</l>
				</lg>
			</div></body></text></TEI>