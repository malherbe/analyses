<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES PHILOSOPHIQUES</title>
				<title type="medium">Une édition électronique</title>
				<author key="ACK">
					<name>
						<forename>Louise-Victorine</forename>
						<surname>ACKERMANN</surname>
					</name>
					<date from="1813" to="1890">1813-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1634 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">ACK_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies Philosophiques</title>
						<author>Louise-Victorine Ackermann</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/louiseakermanpoesiesphilosophiques.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres de L. Ackermann</title>
						<author>Louise-Victorine Ackermann</author>
						<imprint>
							<publisher>Alphonse Lemerre, éditeur</publisher>
							<date when="1885">1885</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique
					après application du programme de calcul de la longueur métrique des vers).
				</p>
				<normalization>
					<p>Les balises de pagination ont été supprimées.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="ACK29">
				<head type="number">VII</head>
				<head type="main">Prométhée</head>
				<opener>
					<salute>À Daniel Stern</salute>
					<epigraph>
						<cit>
							<quote>
							Ὀρᾶτε δεσμώτην με δύσποτμον θεὸν, <lb></lb>
							τὸν Διὸς ἐχθρὸν . . . . . . <lb></lb>
							διὰ τὴν λίαν φιλότητα βροτῶν.
							</quote>
							<bibl>
								<name>Eschyle</name>, <hi rend="ital">Prométhée</hi>.
							</bibl>
						</cit>
					</epigraph>
				</opener>
				<lg n="1">
					<l n="1" num="1.1">Frappe encor, Jupiter, accable-moi, mutile</l>
					<l n="2" num="1.2">L’ennemi terrassé que tu sais impuissant !</l>
					<l n="3" num="1.3">Écraser n’est pas vaincre, et ta foudre inutile</l>
					<l n="4" num="1.4"><space unit="char" quantity="12"></space>S’éteindra dans mon sang,</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Avant d’avoir dompté l’héroïque pensée</l>
					<l n="6" num="2.2">Qui fait du vieux Titan un révolté divin ;</l>
					<l n="7" num="2.3">C’est elle qui te brave, et ta rage insensée</l>
					<l n="8" num="2.4">N’a cloué sur ces monts qu’un simulacre vain.</l>
					<l n="9" num="2.5">Tes coups n’auront porté que sur un peu d’argile ;</l>
					<l n="10" num="2.6">Libre dans les liens de cette chair fragile,</l>
					<l n="11" num="2.7">L’âme de Prométhée échappe à ta fureur.</l>
					<l n="12" num="2.8">Sous l’ongle du vautour qui sans fin me dévore,</l>
					<l n="13" num="2.9">Un invisible amour fait palpiter encore</l>
					<l n="14" num="2.10"><space unit="char" quantity="12"></space>Les lambeaux de mon cœur.</l>
				</lg>
				<lg n="3">
					<l n="15" num="3.1">Si ces pics désolés que la tempête assiège</l>
					<l n="16" num="3.2">Ont vu couler parfois sur leur manteau de neige</l>
					<l n="17" num="3.3">Des larmes que mes yeux ne pouvaient retenir,</l>
					<l n="18" num="3.4">Vous le savez, rochers, immuables murailles</l>
					<l n="19" num="3.5">Que d’horreur cependant je sentais tressaillir,</l>
					<l n="20" num="3.6">La source de mes pleurs était dans mes entrailles ;</l>
					<l n="21" num="3.7">C’est la compassion qui les a fait jaillir.</l>
				</lg>
				<lg n="4">
					<l n="22" num="4.1">Ce n’était point assez de mon propre martyre ;</l>
					<l n="23" num="4.2">Ces flancs ouverts, ce sein qu’un bras divin déchire</l>
					<l n="24" num="4.3">Est rempli de pitié pour d’autres malheureux.</l>
					<l n="25" num="4.4">Je les vois engager une lutte éternelle ;</l>
					<l n="26" num="4.5">L’image horrible est là ; j’ai devant la prunelle</l>
					<l n="27" num="4.6">La vision des maux qui vont fondre sur eux.</l>
					<l n="28" num="4.7">Ce spectacle navrant m’obsède et m’exaspère.</l>
					<l n="29" num="4.8">Supplice intolérable et toujours renaissant,</l>
					<l n="30" num="4.9">Mon vrai, mon seul vautour, c’est la pensée amère</l>
					<l n="31" num="4.10">Que rien n’arrachera ces germes de misère</l>
					<l n="32" num="4.11">Que ta haine a semés dans leur chair et leur sang.</l>
				</lg>
				<lg n="5">
					<l n="33" num="5.1">Pourtant, ô Jupiter, l’homme est ta créature ;</l>
					<l n="34" num="5.2">C’est toi qui l’as conçu, c’est toi qui l’as formé,</l>
					<l n="35" num="5.3">Cet être déplorable, infirme, désarmé,</l>
					<l n="36" num="5.4">Pour qui tout est danger, épouvante, torture,</l>
					<l n="37" num="5.5">Qui, dans le cercle étroit de ses jours enfermé,</l>
					<l n="38" num="5.6">Étouffe et se débat, se blesse et se lamente.</l>
					<l n="39" num="5.7">Ah ! quand tu le jetas sur la terre inclémente,</l>
					<l n="40" num="5.8">Tu savais quels fléaux l’y devaient assaillir,</l>
					<l n="41" num="5.9">Qu’on lui disputerait sa place et sa pâture,</l>
					<l n="42" num="5.10">Qu’un souffle l’abattrait, que l’aveugle Nature</l>
					<l n="43" num="5.11">Dans son indifférence allait l’ensevelir.</l>
					<l n="44" num="5.12">Je l’ai trouvé blotti sous quelque roche humide,</l>
					<l n="45" num="5.13">Ou rampant dans les bois, spectre hâve et timide</l>
					<l n="46" num="5.14">Qui n’entendait partout que gronder et rugir,</l>
					<l n="47" num="5.15">Seul affamé, seul triste au grand banquet des êtres,</l>
					<l n="48" num="5.16">Du fond des eaux, du sein des profondeurs champêtres</l>
					<l n="49" num="5.17">Tremblant toujours de voir un ennemi surgir.</l>
				</lg>
				<lg n="6">
					<l n="50" num="6.1">Mais quoi ! sur cet objet de ta haine immortelle,</l>
					<l n="51" num="6.2">Imprudent que j’étais, je me suis attendri ;</l>
					<l n="52" num="6.3">J’allumai la pensée et jetai l’étincelle</l>
					<l n="53" num="6.4">Dans cet obscur limon dont tu l’avais pétri.</l>
					<l n="54" num="6.5">Il n’était qu’ébauché, j’achevai ton ouvrage.</l>
					<l n="55" num="6.6">Plein d’espoir et d’audace, en mes vastes desseins</l>
					<l n="56" num="6.7">J’aurais sans hésiter mis les cieux au pillage,</l>
					<l n="57" num="6.8">Pour le doter après du fruit de mes larcins.</l>
					<l n="58" num="6.9">Je t’ai ravi le feu ; de conquête en conquête</l>
					<l n="59" num="6.10">J’arrachais de tes mains ton sceptre révéré.</l>
					<l n="60" num="6.11">Grand Dieu ! ta foudre à temps éclata sur ma tête ;</l>
					<l n="61" num="6.12">Encore un attentat, l’homme était délivré !</l>
				</lg>
				<lg n="7">
					<l n="62" num="7.1">La voici donc ma faute, exécrable et sublime.</l>
					<l n="63" num="7.2">Compatir, quel forfait ! Se dévouer, quel crime !</l>
					<l n="64" num="7.3">Quoi ! j’aurais, impuni, défiant tes rigueurs,</l>
					<l n="65" num="7.4">Ouvert aux opprimés mes bras libérateurs ?</l>
					<l n="66" num="7.5">Insensé ! m’être ému quand la pitié s’expie !</l>
					<l n="67" num="7.6">Pourtant c’est Prométhée, oui, c’est ce même impie</l>
					<l n="68" num="7.7">Qui naguère t’aidait à vaincre les Titans.</l>
					<l n="69" num="7.8">J’étais à tes côtés dans l’ardente mêlée ;</l>
					<l n="70" num="7.9">Tandis que mes conseils guidaient les combattants,</l>
					<l n="71" num="7.10">Mes coups faisaient trembler la demeure étoilée.</l>
					<l n="72" num="7.11">Il s’agissait pour moi du sort de l’univers :</l>
					<l n="73" num="7.12">Je voulais en finir avec les dieux pervers.</l>
				</lg>
				<lg n="8">
					<l n="74" num="8.1">Ton règne allait m’ouvrir cette ère pacifique</l>
					<l n="75" num="8.2">Que mon cœur transporté saluait de ses vœux.</l>
					<l n="76" num="8.3">En son cours éthéré le soleil magnifique</l>
					<l n="77" num="8.4">N’aurait plus éclairé que des êtres heureux.</l>
					<l n="78" num="8.5">La Terreur s’enfuyait en écartant les ombres</l>
					<l n="79" num="8.6">Qui voilaient ton sourire ineffable et clément,</l>
					<l n="80" num="8.7">Et le réseau d’airain des Nécessités sombres</l>
					<l n="81" num="8.8">Se brisait de lui-même aux pieds d’un maître aimant.</l>
					<l n="82" num="8.9">Tout était joie, amour, essor, efflorescence ;</l>
					<l n="83" num="8.10">Lui-même Dieu n’était que le rayonnement</l>
					<l n="84" num="8.11">De la toute-bonté dans la toute-puissance.</l>
				</lg>
				<lg n="9">
					<l n="85" num="9.1">O mes désirs trompés ! O songe évanoui !</l>
					<l n="86" num="9.2">Des splendeurs d’un tel rêve, encor l’œil ébloui,</l>
					<l n="87" num="9.3">Me retrouver devant l’iniquité céleste.</l>
					<l n="88" num="9.4">Devant un Dieu jaloux qui frappe et qui déteste,</l>
					<l n="89" num="9.5">Et dans mon désespoir me dire avec horreur :</l>
					<l n="90" num="9.6">« Celui qui pouvait tout a voulu la douleur ! » !</l>
				</lg>
				<lg n="10">
					<l n="91" num="10.1">Mais ne t’abuse point ! Sur ce roc solitaire</l>
					<l n="92" num="10.2">Tu ne me verras pas succomber en entier.</l>
					<l n="93" num="10.3">Un esprit de révolte a transformé la terre,</l>
					<l n="94" num="10.4">Et j’ai dès aujourd’hui choisi mon héritier.</l>
					<l n="95" num="10.5">Il poursuivra mon œuvre en marchant sur ma trace,</l>
					<l n="96" num="10.6">Né qu’il est comme moi pour tenter et souffrir.</l>
					<l n="97" num="10.7">Aux humains affranchis je lègue mon audace,</l>
					<l n="98" num="10.8">Héritage sacré qui ne peut plus périr.</l>
					<l n="99" num="10.9">La raison s’affermit, le doute est prêt à naître.</l>
					<l n="100" num="10.10">Enhardis à ce point d’interroger leur maître,</l>
					<l n="101" num="10.11">Des mortels devant eux oseront te citer :</l>
					<l n="102" num="10.12">Pourquoi leurs maux ? Pourquoi ton caprice et ta haine ?</l>
					<l n="103" num="10.13">Oui, ton juge t’attend, — la conscience humaine ;</l>
					<l n="104" num="10.14">Elle ne peut t’absoudre et va te rejeter.</l>
				</lg>
				<lg n="11">
					<l n="105" num="11.1">Le voilà, ce vengeur promis à ma détresse !</l>
					<l n="106" num="11.2">Ah ! quel souffle épuré d’amour et d’allégresse</l>
					<l n="107" num="11.3">En traversant le monde enivrera mon cœur</l>
					<l n="108" num="11.4">Le jour où, moins hardie encor que magnanime,</l>
					<l n="109" num="11.5">Au lieu de l’accuser, ton auguste victime</l>
					<l n="110" num="11.6"><space unit="char" quantity="12"></space>Niera son oppresseur !</l>
				</lg>
				<lg n="12">
					<l n="111" num="12.1">Délivré de la Foi comme d’un mauvais rêve,</l>
					<l n="112" num="12.2">L’homme répudiera les tyrans immortels,</l>
					<l n="113" num="12.3">Et n’ira plus, en proie à des terreurs sans trêve,</l>
					<l n="114" num="12.4">Se courber lâchement au pied de tes autels.</l>
					<l n="115" num="12.5">Las de le trouver sourd, il croira le ciel vide.</l>
					<l n="116" num="12.6">Jetant sur toi son voile éternel et splendide,</l>
					<l n="117" num="12.7">La Nature déjà te cache à son regard ;</l>
					<l n="118" num="12.8">Il ne découvrira dans l’univers sans borne,</l>
					<l n="119" num="12.9">Pour tout Dieu désormais, qu’un couple aveugle et morne,</l>
					<l n="120" num="12.10"><space unit="char" quantity="12"></space>La Force et le Hasard.</l>
				</lg>
				<lg n="13">
					<l n="121" num="13.1">Montre-toi, Jupiter, éclate alors, fulmine,</l>
					<l n="122" num="13.2">Contre ce fugitif à ton joug échappé !</l>
					<l n="123" num="13.3">Refusant dans ses maux de voir ta main divine,</l>
					<l n="124" num="13.4">Par un pouvoir fatal il se dira frappé.</l>
					<l n="125" num="13.5">Il tombera sans peur, sans plainte, sans prière ;</l>
					<l n="126" num="13.6">Et quand tu donnerais ton aigle et ton tonnerre</l>
					<l n="127" num="13.7">Pour l’entendre pousser, au fort de son tourment,</l>
					<l n="128" num="13.8">Un seul cri qui t’atteste, une injure, un blasphème,</l>
					<l n="129" num="13.9"><space unit="char" quantity="2"></space>Il restera muet : ce silence suprême</l>
					<l n="130" num="13.10"><space unit="char" quantity="12"></space>Sera ton châtiment.</l>
				</lg>
				<lg n="14">
					<l n="131" num="14.1">Tu n’auras plus que moi dans ton immense empire</l>
					<l n="132" num="14.2">Pour croire encore en toi, funeste Déité.</l>
					<l n="133" num="14.3">Plutôt nier le jour ou l’air que je respire</l>
					<l n="134" num="14.4">Que ta puissance inique et que ta cruauté.</l>
					<l n="135" num="14.5">Perdu dans cet azur, sur ces hauteurs sublimes,</l>
					<l n="136" num="14.6">Ah ! j’ai vu de trop près tes fureurs et tes crimes ;</l>
					<l n="137" num="14.7">J’ai sous tes coups déjà trop souffert, trop saigné ;</l>
					<l n="138" num="14.8">Le doute est impossible à mon cœur indigné.</l>
					<l n="139" num="14.9">Oui ! tandis que du Mal, œuvre de ta colère,</l>
					<l n="140" num="14.10">Renonçant désormais à sonder le mystère,</l>
					<l n="141" num="14.11">L’esprit humain ailleurs portera son flambeau,</l>
					<l n="142" num="14.12">Seul je saurai le mot de cette énigme obscure,</l>
					<l n="143" num="14.13">Et j’aurai reconnu, pour comble de torture,</l>
					<l n="144" num="14.14"><space unit="char" quantity="12"></space>Un Dieu dans mon bourreau.</l>
				</lg>
				<closer>
					<dateline>
						<placeName>Nice</placeName>,
						<date when="1865">30 novembre 1865</date>
						</dateline>
				</closer>
			</div></body></text></TEI>