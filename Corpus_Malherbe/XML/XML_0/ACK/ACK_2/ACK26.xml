<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES PHILOSOPHIQUES</title>
				<title type="medium">Une édition électronique</title>
				<author key="ACK">
					<name>
						<forename>Louise-Victorine</forename>
						<surname>ACKERMANN</surname>
					</name>
					<date from="1813" to="1890">1813-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1634 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">ACK_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies Philosophiques</title>
						<author>Louise-Victorine Ackermann</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/louiseakermanpoesiesphilosophiques.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres de L. Ackermann</title>
						<author>Louise-Victorine Ackermann</author>
						<imprint>
							<publisher>Alphonse Lemerre, éditeur</publisher>
							<date when="1885">1885</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique
					après application du programme de calcul de la longueur métrique des vers).
				</p>
				<normalization>
					<p>Les balises de pagination ont été supprimées.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="ACK26">
				<head type="number">IV</head>
				<head type="main">L’Amour et la Mort</head>
				<opener>
					<salute>À M. Louis De Ronchaud</salute>
				</opener>
				<div type="section" n="1">
					<head type="number">I</head>
					<lg n="1">
						<l n="1" num="1.1">Regardez-les passer, ces couples éphémères !</l>
						<l n="2" num="1.2">Dans les bras l’un de l’autre enlacés un moment,</l>
						<l n="3" num="1.3">Tous, avant de mêler à jamais leurs poussières,</l>
						<l n="4" num="1.4"><space unit="char" quantity="12"></space>Font le même serment :</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">Toujours ! Un mot hardi que les cieux qui vieillissent</l>
						<l n="6" num="2.2">Avec étonnement entendent prononcer,</l>
						<l n="7" num="2.3">Et qu’osent répéter des lèvres qui pâlissent</l>
						<l n="8" num="2.4"><space unit="char" quantity="12"></space>Et qui vont se glacer.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">Vous qui vivez si peu, pourquoi cette promesse</l>
						<l n="10" num="3.2">Qu’un élan d’espérance arrache à votre cœur,</l>
						<l n="11" num="3.3">Vain défi qu’au néant vous jetez, dans l’ivresse</l>
						<l n="12" num="3.4"><space unit="char" quantity="12"></space>D’un instant de bonheur ?</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">Amants, autour de vous une voix inflexible</l>
						<l n="14" num="4.2">Crie à tout ce qui naît : « Aime et meurs ici-bas ! »</l>
						<l n="15" num="4.3">La mort est implacable et le ciel insensible ;</l>
						<l n="16" num="4.4"><space unit="char" quantity="12"></space>Vous n’échapperez pas.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">Eh bien ! puisqu’il le faut, sans trouble et sans murmure,</l>
						<l n="18" num="5.2">Forts de ce même amour dont vous vous enivrez</l>
						<l n="19" num="5.3">Et perdus dans le sein de l’immense Nature,</l>
						<l n="20" num="5.4"><space unit="char" quantity="12"></space>Aimez donc, et mourez !</l>
					</lg>
				</div>
				<div type="section" n="2">
					<head type="number">II</head>
					<lg n="1">
						<l n="21" num="1.1">Non, non, tout n’est pas dit, vers la beauté fragile</l>
						<l n="22" num="1.2">Quand un charme invincible emporte le désir,</l>
						<l n="23" num="1.3">Sous le feu d’un baiser quand notre pauvre argile</l>
						<l n="24" num="1.4"><space unit="char" quantity="12"></space>A frémi de plaisir.</l>
					</lg>
					<lg n="2">
						<l n="25" num="2.1">Notre serment sacré part d’une âme immortelle ;</l>
						<l n="26" num="2.2">C’est elle qui s’émeut quand frissonne le corps ;</l>
						<l n="27" num="2.3">Nous entendons sa voix et le bruit de son aile</l>
						<l n="28" num="2.4"><space unit="char" quantity="12"></space>Jusque dans nos transports.</l>
					</lg>
					<lg n="3">
						<l n="29" num="3.1">Nous le répétons donc, ce mot qui fait d’envie</l>
						<l n="30" num="3.2">Pâlir au firmament les astres radieux,</l>
						<l n="31" num="3.3">Ce mot qui joint les cœurs et devient, dès la vie,</l>
						<l n="32" num="3.4"><space unit="char" quantity="12"></space>Leur lien pour les cieux.</l>
					</lg>
					<lg n="4">
						<l n="33" num="4.1">Dans le ravissement d’une éternelle étreinte</l>
						<l n="34" num="4.2">Ils passent entraînés, ces couples amoureux,</l>
						<l n="35" num="4.3">Et ne s’arrêtent pas pour jeter avec crainte</l>
						<l n="36" num="4.4"><space unit="char" quantity="12"></space>Un regard autour d’eux.</l>
					</lg>
					<lg n="5">
						<l n="37" num="5.1">Ils demeurent sereins quand tout s’écroule et tombe ;</l>
						<l n="38" num="5.2">Leur espoir est leur joie et leur appui divin ;</l>
						<l n="39" num="5.3">Ils ne trébuchent point lorsque contre une tombe</l>
						<l n="40" num="5.4"><space unit="char" quantity="12"></space>Leur pied heurte en chemin.</l>
					</lg>
					<lg n="6">
						<l n="41" num="6.1">Toi-même, quand tes bois abritent leur délire,</l>
						<l n="42" num="6.2">Quand tu couvres de fleurs et d’ombre leurs sentiers,</l>
						<l n="43" num="6.3">Nature, toi leur mère, aurais-tu ce sourire</l>
						<l n="44" num="6.4"><space unit="char" quantity="12"></space>S’ils mouraient tout entiers ?</l>
					</lg>
					<lg n="7">
						<l n="45" num="7.1">Sous le voile léger de la beauté mortelle</l>
						<l n="46" num="7.2">Trouver l’âme qu’on cherche et qui pour nous éclôt,</l>
						<l n="47" num="7.3">Le temps de l’entrevoir, de s’écrier : « C’est Elle ! »</l>
						<l n="48" num="7.4"><space unit="char" quantity="12"></space>Et la perdre aussitôt,</l>
					</lg>
					<lg n="8">
						<l n="49" num="8.1">Et la perdre à jamais ! Cette seule pensée</l>
						<l n="50" num="8.2">Change en spectre à nos yeux l’image de l’amour.</l>
						<l n="51" num="8.3">Quoi ! ces vœux infinis, cette ardeur insensée</l>
						<l n="52" num="8.4"><space unit="char" quantity="12"></space>Pour un être d’un jour !</l>
					</lg>
					<lg n="9">
						<l n="53" num="9.1">Et toi, serais-tu donc à ce point sans entrailles,</l>
						<l n="54" num="9.2">Grand Dieu qui dois d’en haut tout entendre et tout voir,</l>
						<l n="55" num="9.3">Que tant d’adieux navrants et tant de funérailles</l>
						<l n="56" num="9.4"><space unit="char" quantity="12"></space>Ne puissent t’émouvoir,</l>
					</lg>
					<lg n="10">
						<l n="57" num="10.1">Qu’à cette tombe obscure où tu nous fais descendre</l>
						<l n="58" num="10.2">Tu dises : « Garde-les, leurs cris sont superflus.</l>
						<l n="59" num="10.3">Amèrement en vain l’on pleure sur leur cendre ;</l>
						<l n="60" num="10.4"><space unit="char" quantity="12"></space>Tu ne les rendras plus ! » !</l>
					</lg>
					<lg n="11">
						<l n="61" num="11.1">Mais non ! Dieu qu’on dit bon, tu permets qu’on espère ;</l>
						<l n="62" num="11.2">Unir pour séparer, ce n’est point ton dessein.</l>
						<l n="63" num="11.3">Tout ce qui s’est aimé, fût-ce un jour, sur la terre,</l>
						<l n="64" num="11.4"><space unit="char" quantity="12"></space>Va s’aimer dans ton sein.</l>
					</lg>
				</div>
				<div type="section" n="3">
					<head type="number">III</head>
					<lg n="1">
						<l n="65" num="1.1">Éternité de l’homme, illusion ! chimère !</l>
						<l n="66" num="1.2">Mensonge de l’amour et de l’orgueil humain !</l>
						<l n="67" num="1.3">Il n’a point eu d’hier, ce fantôme éphémère,</l>
						<l n="68" num="1.4"><space unit="char" quantity="12"></space>Il lui faut un demain !</l>
					</lg>
					<lg n="2">
						<l n="69" num="2.1">Pour cet éclair de vie et pour cette étincelle</l>
						<l n="70" num="2.2">Qui brûle une minute en vos cœurs étonnés,</l>
						<l n="71" num="2.3">Vous oubliez soudain la fange maternelle</l>
						<l n="72" num="2.4"><space unit="char" quantity="12"></space>Et vos destins bornés.</l>
					</lg>
					<lg n="3">
						<l n="73" num="3.1">Vous échapperiez donc, ô rêveurs téméraires</l>
						<l n="74" num="3.2">Seuls au Pouvoir fatal qui détruit en créant ?</l>
						<l n="75" num="3.3">Quittez un tel espoir ; tous les limons sont frères</l>
						<l n="76" num="3.4"><space unit="char" quantity="12"></space>En face du néant.</l>
					</lg>
					<lg n="4">
						<l n="77" num="4.1">Vous dites à la Nuit qui passe dans ses voiles :</l>
						<l n="78" num="4.2">« J’aime, et j’espère voir expirer tes flambeaux. »</l>
						<l n="79" num="4.3">La Nuit ne répond rien, mais demain ses étoiles</l>
						<l n="80" num="4.4"><space unit="char" quantity="12"></space>Luiront sur vos tombeaux.</l>
					</lg>
					<lg n="5">
						<l n="81" num="5.1">Vous croyez que l’amour dont l’âpre feu vous presse</l>
						<l n="82" num="5.2">A réservé pour vous sa flamme et ses rayons ;</l>
						<l n="83" num="5.3">La fleur que vous brisez soupire avec ivresse :</l>
						<l n="84" num="5.4"><space unit="char" quantity="12"></space>« Nous aussi nous aimons ! » !</l>
					</lg>
					<lg n="6">
						<l n="85" num="6.1">Heureux, vous aspirez la grande âme invisible</l>
						<l n="86" num="6.2">Qui remplit tout, les bois, les champs de ses ardeurs ;</l>
						<l n="87" num="6.3">La Nature sourit, mais elle est insensible :</l>
						<l n="88" num="6.4"><space unit="char" quantity="12"></space>Que lui font vos bonheurs ?</l>
					</lg>
					<lg n="7">
						<l n="89" num="7.1">Elle n’a qu’un désir, la marâtre immortelle,</l>
						<l n="90" num="7.2">C’est d’enfanter toujours, sans fin, sans trêve, encor.</l>
						<l n="91" num="7.3">Mère avide, elle a pris l’éternité pour elle,</l>
						<l n="92" num="7.4"><space unit="char" quantity="12"></space>Et vous laisse la mort.</l>
					</lg>
					<lg n="8">
						<l n="93" num="8.1">Toute sa prévoyance est pour ce qui va naître ;</l>
						<l n="94" num="8.2">Le reste est confondu dans un suprême oubli.</l>
						<l n="95" num="8.3">Vous, vous avez aimé, vous pouvez disparaître :</l>
						<l n="96" num="8.4"><space unit="char" quantity="12"></space>Son vœu s’est accompli.</l>
					</lg>
					<lg n="9">
						<l n="97" num="9.1">Quand un souffle d’amour traverse vos poitrines,</l>
						<l n="98" num="9.2">Sur des flots de bonheur vous tenant suspendus,</l>
						<l n="99" num="9.3">Aux pieds de la Beauté lorsque des mains divines</l>
						<l n="100" num="9.4"><space unit="char" quantity="12"></space>Vous jettent éperdus ;</l>
					</lg>
					<lg n="10">
						<l n="101" num="10.1">Quand, pressant sur ce cœur qui va bientôt s’éteindre</l>
						<l n="102" num="10.2">Un autre objet souffrant, forme vaine ici-bas,</l>
						<l n="103" num="10.3">Il vous semble, mortels, que vous allez étreindre</l>
						<l n="104" num="10.4"><space unit="char" quantity="12"></space>L’Infini dans vos bras ;</l>
					</lg>
					<lg n="11">
						<l n="105" num="11.1">Ces délires sacrés, ces désirs sans mesure</l>
						<l n="106" num="11.2">Déchaînés dans vos flancs comme d’ardents essaims,</l>
						<l n="107" num="11.3">Ces transports, c’est déjà l’Humanité future</l>
						<l n="108" num="11.4"><space unit="char" quantity="12"></space>Qui s’agite en vos seins.</l>
					</lg>
					<lg n="12">
						<l n="109" num="12.1">Elle se dissoudra, cette argile légère</l>
						<l n="110" num="12.2">Qu’ont émue un instant la joie et la douleur ;</l>
						<l n="111" num="12.3">Les vents vont disperser cette noble poussière</l>
						<l n="112" num="12.4"><space unit="char" quantity="12"></space>Qui fut jadis un cœur.</l>
					</lg>
					<lg n="13">
						<l n="113" num="13.1">Mais d’autres cœurs naîtront qui renoueront la trame</l>
						<l n="114" num="13.2">De vos espoirs brisés, de vos amours éteints,</l>
						<l n="115" num="13.3">Perpétuant vos pleurs, vos rêves, votre flamme,</l>
						<l n="116" num="13.4"><space unit="char" quantity="12"></space>Dans les âges lointains.</l>
					</lg>
					<lg n="14">
						<l n="117" num="14.1">Tous les êtres, formant une chaîne éternelle,</l>
						<l n="118" num="14.2">Se passent, en courant, le flambeau de l’amour.</l>
						<l n="119" num="14.3">Chacun rapidement prend la torche immortelle</l>
						<l n="120" num="14.4"><space unit="char" quantity="12"></space>Et la rend à son tour.</l>
					</lg>
					<lg n="15">
						<l n="121" num="15.1">Aveuglés par l’éclat de sa lumière errante,</l>
						<l n="122" num="15.2">Vous jurez, dans la nuit où le sort vous plongea,</l>
						<l n="123" num="15.3">De la tenir toujours : à votre main mourante</l>
						<l n="124" num="15.4"><space unit="char" quantity="12"></space>Elle échappe déjà.</l>
					</lg>
					<lg n="16">
						<l n="125" num="16.1">Du moins vous aurez vu luire un éclair sublime ;</l>
						<l n="126" num="16.2">Il aura sillonné votre vie un moment ;</l>
						<l n="127" num="16.3">En tombant vous pourrez emporter dans l’abîme</l>
						<l n="128" num="16.4"><space unit="char" quantity="12"></space>Votre éblouissement.</l>
					</lg>
					<lg n="17">
						<l n="129" num="17.1">Et quand il régnerait au fond du ciel paisible</l>
						<l n="130" num="17.2">Un être sans pitié qui contemplât souffrir,</l>
						<l n="131" num="17.3">Si son œil éternel considère, impassible,</l>
						<l n="132" num="17.4"><space unit="char" quantity="12"></space>Le naître et le mourir,</l>
					</lg>
					<lg n="18">
						<l n="133" num="18.1">Sur le bord de la tombe, et sous ce regard même,</l>
						<l n="134" num="18.2">Qu’un mouvement d’amour soit encor votre adieu !</l>
						<l n="135" num="18.3">Oui, faites voir combien l’homme est grand lorsqu’il aime,</l>
						<l n="136" num="18.4"><space unit="char" quantity="12"></space>Et pardonnez à Dieu !</l>
					</lg>
				</div>
			</div></body></text></TEI>