<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES PHILOSOPHIQUES</title>
				<title type="medium">Une édition électronique</title>
				<author key="ACK">
					<name>
						<forename>Louise-Victorine</forename>
						<surname>ACKERMANN</surname>
					</name>
					<date from="1813" to="1890">1813-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1634 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">ACK_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies Philosophiques</title>
						<author>Louise-Victorine Ackermann</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/louiseakermanpoesiesphilosophiques.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres de L. Ackermann</title>
						<author>Louise-Victorine Ackermann</author>
						<imprint>
							<publisher>Alphonse Lemerre, éditeur</publisher>
							<date when="1885">1885</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique
					après application du programme de calcul de la longueur métrique des vers).
				</p>
				<normalization>
					<p>Les balises de pagination ont été supprimées.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="ACK28">
				<head type="number">VI</head>
				<head type="main">Le Nuage</head>
				<opener>
					<salute>À Alfred Holmes</salute>
					<epigraph>
						<cit>
							<quote>I change, but I cannot die.</quote>
							<bibl>
								<name>Shelley</name>, <hi rend="ital">the Cloud</hi>.
							</bibl>
						</cit>
					</epigraph>
				</opener>
				<lg n="1">
					<l n="1" num="1.1">Levez les yeux ! C’est moi qui passe sur vos têtes,</l>
					<l n="2" num="1.2">Diaphane et léger, libre dans le ciel pur ;</l>
					<l n="3" num="1.3">L’aile ouverte, attendant le souffle des tempêtes,</l>
					<l n="4" num="1.4"><space unit="char" quantity="8"></space>Je plonge et nage en plein azur.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Comme un mirage errant, je flotte et je voyage.</l>
					<l n="6" num="2.2">Coloré par l’aurore et le soir tour à tour,</l>
					<l n="7" num="2.3">Miroir aérien, je reflète au passage</l>
					<l n="8" num="2.4"><space unit="char" quantity="8"></space>Les sourires changeants du jour.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Le soleil me rencontre au bout de sa carrière</l>
					<l n="10" num="3.2">Couché sur l’horizon dont j’enflamme le bord ;</l>
					<l n="11" num="3.3">Dans mes flancs transparents le roi de la lumière</l>
					<l n="12" num="3.4"><space unit="char" quantity="8"></space>Lance en fuyant ses flèches d’or.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">Quand la lune, écartant son cortège d’étoiles,</l>
					<l n="14" num="4.2">Jette un regard pensif sur le monde endormi,</l>
					<l n="15" num="4.3">Devant son front glacé je fais courir mes voiles,</l>
					<l n="16" num="4.4"><space unit="char" quantity="8"></space>Ou je les soulève à demi.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1">On croirait voir au loin une flotte qui sombre,</l>
					<l n="18" num="5.2">Quand, d’un bond furieux fendant l’air ébranlé,</l>
					<l n="19" num="5.3">L’ouragan sur ma proue inaccessible et sombre</l>
					<l n="20" num="5.4"><space unit="char" quantity="8"></space>S’assied comme un pilote ailé.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1">Dans les champs de l’éther je livre des batailles ;</l>
					<l n="22" num="6.2">La ruine et la mort ne sont pour moi qu’un jeu.</l>
					<l n="23" num="6.3">Je me charge de grêle, et porte en mes entrailles</l>
					<l n="24" num="6.4"><space unit="char" quantity="8"></space>La foudre et ses hydres de feu.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1">Sur le sol altéré je m’épanche en ondées.</l>
					<l n="26" num="7.2">La terre rit ; je tiens sa vie entre mes mains.</l>
					<l n="27" num="7.3">C’est moi qui gonfle, au sein des terres fécondées,</l>
					<l n="28" num="7.4"><space unit="char" quantity="8"></space>L’épi qui nourrit les humains.</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1">Où j’ai passé, soudain tout verdit, tout pullule ;</l>
					<l n="30" num="8.2">Le sillon que j’enivre enfante avec ardeur.</l>
					<l n="31" num="8.3">Je suis onde et je cours, je suis sève et circule,</l>
					<l n="32" num="8.4"><space unit="char" quantity="8"></space>Caché dans la source ou la fleur.</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1">Un fleuve me recueille, il m’emporte, et je coule</l>
					<l n="34" num="9.2">Comme une veine au cœur des continents profonds.</l>
					<l n="35" num="9.3">Sur les longs pays plats ma nappe se déroule,</l>
					<l n="36" num="9.4"><space unit="char" quantity="8"></space>Ou s’engouffre à travers les monts.</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1">Rien ne m’arrête plus ; dans mon élan rapide</l>
					<l n="38" num="10.2">J’obéis au courant, par le désir poussé,</l>
					<l n="39" num="10.3">Et je vole à mon but comme un grand trait liquide</l>
					<l n="40" num="10.4"><space unit="char" quantity="8"></space>Qu’un bras invisible a lancé.</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1">Océan, ô mon père ! Ouvre ton sein, j’arrive !</l>
					<l n="42" num="11.2">Tes flots tumultueux m’ont déjà répondu ;</l>
					<l n="43" num="11.3">Ils accourent ; mon onde a reculé, craintive,</l>
					<l n="44" num="11.4"><space unit="char" quantity="8"></space>Devant leur accueil éperdu.</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1">En ton lit mugissant ton amour nous rassemble.</l>
					<l n="46" num="12.2">Autour des noirs écueils ou sur le sable fin</l>
					<l n="47" num="12.3">Nous allons, confondus, recommencer ensemble</l>
					<l n="48" num="12.4"><space unit="char" quantity="8"></space>Nos fureurs et nos jeux sans fin.</l>
				</lg>
				<lg n="13">
					<l n="49" num="13.1">Mais le soleil, baissant vers toi son œil splendide,</l>
					<l n="50" num="13.2">M’a découvert bientôt dans tes gouffres amers.</l>
					<l n="51" num="13.3">Son rayon tout puissant baise mon front limpide :</l>
					<l n="52" num="13.4"><space unit="char" quantity="8"></space>J’ai repris le chemin des airs !</l>
				</lg>
				<lg n="14">
					<l n="53" num="14.1">Ainsi, jamais d’arrêt. L’immortelle matière</l>
					<l n="54" num="14.2">Un seul instant encor n’a pu se reposer.</l>
					<l n="55" num="14.3">La Nature ne fait, patiente ouvrière,</l>
					<l n="56" num="14.4"><space unit="char" quantity="8"></space>Que dissoudre et recomposer.</l>
				</lg>
				<lg n="15">
					<l n="57" num="15.1">Tout se métamorphose entre ses mains actives ;</l>
					<l n="58" num="15.2">Partout le mouvement incessant et divers,</l>
					<l n="59" num="15.3">Dans le cercle éternel des formes fugitives,</l>
					<l n="60" num="15.4"><space unit="char" quantity="8"></space>Agitant l’immense univers.</l>
				</lg>
				<closer>
					<dateline>
						<placeName>Nice</placeName>,
						<date when="1871">1871</date>
						</dateline>
				</closer>
				</div></body></text></TEI>