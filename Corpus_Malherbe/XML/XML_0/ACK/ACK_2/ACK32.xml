<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES PHILOSOPHIQUES</title>
				<title type="medium">Une édition électronique</title>
				<author key="ACK">
					<name>
						<forename>Louise-Victorine</forename>
						<surname>ACKERMANN</surname>
					</name>
					<date from="1813" to="1890">1813-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1634 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">ACK_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies Philosophiques</title>
						<author>Louise-Victorine Ackermann</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/louiseakermanpoesiesphilosophiques.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres de L. Ackermann</title>
						<author>Louise-Victorine Ackermann</author>
						<imprint>
							<publisher>Alphonse Lemerre, éditeur</publisher>
							<date when="1885">1885</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique
					après application du programme de calcul de la longueur métrique des vers).
				</p>
				<normalization>
					<p>Les balises de pagination ont été supprimées.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="ACK32">
				<head type="number">X</head>
				<head type="main">L’Homme à la Nature</head>
				<opener>
					<salute>À Madame Juglar</salute>
				</opener>
				<lg n="1">
					<l n="1" num="1.1">Eh bien ! reprends-le donc ce peu de fange obscure</l>
					<l n="2" num="1.2">Qui pour quelques instants s’anima sous ta main ;</l>
					<l n="3" num="1.3">Dans ton dédain superbe, implacable Nature,</l>
					<l n="4" num="1.4"><space unit="char" quantity="8"></space>Brise à jamais le moule humain.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">De ces tristes débris quand tu verrais, ravie,</l>
					<l n="6" num="2.2">D’autres créations éclore à grands essaims,</l>
					<l n="7" num="2.3">Ton Idée éclater en des formes de vie</l>
					<l n="8" num="2.4"><space unit="char" quantity="8"></space>Plus dociles à tes desseins,</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Est-ce à dire que Lui, ton espoir, ta chimère,</l>
					<l n="10" num="3.2">Parce qu’il fut rêvé, puisse un jour exister ?</l>
					<l n="11" num="3.3">Tu crois avoir conçu, tu voudrais être mère ;</l>
					<l n="12" num="3.4"><space unit="char" quantity="8"></space>A l’œuvre ! il s’agit d’enfanter.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">Change en réalité ton attente sublime.</l>
					<l n="14" num="4.2">Mais quoi ! pour les franchir, malgré tous tes élans,</l>
					<l n="15" num="4.3">La distance est trop grande et trop profond l’abîme</l>
					<l n="16" num="4.4"><space unit="char" quantity="8"></space>Entre ta pensée et tes flancs.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1">La mort est le seul fruit qu’en tes crises futures</l>
					<l n="18" num="5.2">Il te sera donné d’atteindre et de cueillir ;</l>
					<l n="19" num="5.3">Toujours nouveaux débris, toujours des créatures</l>
					<l n="20" num="5.4"><space unit="char" quantity="8"></space>Que tu devras ensevelir.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1">Car sur ta route en vain l’âge à l’âge succède ;</l>
					<l n="22" num="6.2">Les tombes, les berceaux ont beau s’accumuler,</l>
					<l n="23" num="6.3">L’Idéal qui te fuit, l’Idéal qui t’obsède,</l>
					<l n="24" num="6.4"><space unit="char" quantity="8"></space>A l’infini pour reculer.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1">L’objet de ta poursuite éternelle et sans trêve</l>
					<l n="26" num="7.2">Demeure un but trompeur à ton vol impuissant</l>
					<l n="27" num="7.3">Et, sous le nimbe ardent du désir et du rêve,</l>
					<l n="28" num="7.4"><space unit="char" quantity="8"></space>N’est qu’un fantôme éblouissant.</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1">Il resplendit de loin, mais reste inaccessible.</l>
					<l n="30" num="8.2">Prodigue de travaux, de luttes, de trépas,</l>
					<l n="31" num="8.3">Ta main me sacrifie à ce fils impossible ;</l>
					<l n="32" num="8.4"><space unit="char" quantity="8"></space>Je meurs, et Lui ne naîtra pas.</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1">Pourtant je suis ton fils aussi ; réel, vivace,</l>
					<l n="34" num="9.2">Je sortis de tes bras des les siècles lointains ;</l>
					<l n="35" num="9.3">Je porte dans mon cœur, je porte sur ma face,</l>
					<l n="36" num="9.4"><space unit="char" quantity="8"></space>Le signe empreint des hauts destins.</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1">Un avenir sans fin s’ouvrait ; dans la carrière</l>
					<l n="38" num="10.2">Le Progrès sur ses pas me pressait d’avancer ;</l>
					<l n="39" num="10.3">Tu n’aurais même encor qu’à lever la barrière :</l>
					<l n="40" num="10.4"><space unit="char" quantity="8"></space>Je suis là, prêt à m’élancer.</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1">Je serais ton sillon ou ton foyer intense ;</l>
					<l n="42" num="11.2">Tu peux selon ton gré m’ouvrir ou m’allumer.</l>
					<l n="43" num="11.3">Une unique étincelle, ô mère ! une semence !</l>
					<l n="44" num="11.4"><space unit="char" quantity="8"></space>Tout s’enflamme ou tout va germer.</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1">Ne suis-je point encor seul à te trouver belle ?</l>
					<l n="46" num="12.2">J’ai compté tes trésors, j’atteste ton pouvoir,</l>
					<l n="47" num="12.3">Et mon intelligence, ô Nature éternelle !</l>
					<l n="48" num="12.4"><space unit="char" quantity="8"></space>T’a tendu ton premier miroir.</l>
				</lg>
				<lg n="13">
					<l n="49" num="13.1">En retour je n’obtiens que dédain et qu’offense.</l>
					<l n="50" num="13.2">Oui, toujours au péril et dans les vains combats !</l>
					<l n="51" num="13.3">Éperdu sur ton sein, sans recours ni défense,</l>
					<l n="52" num="13.4"><space unit="char" quantity="8"></space>Je m’exaspère et me débats.</l>
				</lg>
				<lg n="14">
					<l n="53" num="14.1">Ah ! si du moins ma force eût égalé ma rage,</l>
					<l n="54" num="14.2"><space unit="char" quantity="2"></space>Je l’aurais déchiré ce sein dur et muet :</l>
					<l n="55" num="14.3">Se rendant aux assauts de mon ardeur sauvage,</l>
					<l n="56" num="14.4"><space unit="char" quantity="8"></space>Il m’aurait livré son secret.</l>
				</lg>
				<lg n="15">
					<l n="57" num="15.1">C’en est fait, je succombe, et quand tu dis : « J’aspire ! »</l>
					<l n="58" num="15.2">Je te réponds : « Je souffre ! » infirme, ensanglanté ;</l>
					<l n="59" num="15.3">Et par tout ce qui naît , par tout ce qui respire,</l>
					<l n="60" num="15.4"><space unit="char" quantity="8"></space>Ce cri terrible est répété.</l>
				</lg>
				<lg n="16">
					<l n="61" num="16.1">Oui, je souffre ! et c’est toi, mère, qui m’extermines,</l>
					<l n="62" num="16.2">Tantôt frappant mes flancs, tantôt blessant mon cœur ;</l>
					<l n="63" num="16.3">Mon être tout entier, par toutes ses racines,</l>
					<l n="64" num="16.4"><space unit="char" quantity="8"></space>Plonge sans fond dans la douleur.</l>
				</lg>
				<lg n="17">
					<l n="65" num="17.1">J’offre sous le soleil un lugubre spectacle.</l>
					<l n="66" num="17.2">Ne naissant, ne vivant que pour agoniser.</l>
					<l n="67" num="17.3">L’abîme s’ouvre ici, là se dresse l’obstacle :</l>
					<l n="68" num="17.4"><space unit="char" quantity="8"></space>Ou m’engloutir, ou me briser !</l>
				</lg>
				<lg n="18">
					<l n="69" num="18.1">Mais, jusque sous le coup du désastre suprême,</l>
					<l n="70" num="18.2">Moi, l’homme, je t’accuse à la face des cieux.</l>
					<l n="71" num="18.3">Créatrice, en plein front reçois donc l’anathème</l>
					<l n="72" num="18.4"><space unit="char" quantity="8"></space>De cet atome audacieux.</l>
				</lg>
				<lg n="19">
					<l n="73" num="19.1">Sois maudite, ô marâtre ! en tes œuvres immenses,</l>
					<l n="74" num="19.2">Oui, maudite à ta source et dans tes éléments,</l>
					<l n="75" num="19.3">Pour tous tes abandons, tes oublis, tes démences,</l>
					<l n="76" num="19.4"><space unit="char" quantity="8"></space>Aussi pour tes avortements !</l>
				</lg>
				<lg n="20">
					<l n="77" num="20.1">Que la Force en ton sein s’épuise perte à perte !</l>
					<l n="78" num="20.2">Que la Matière, à bout de nerf et de ressort,</l>
					<l n="79" num="20.3">Reste sans mouvement, et se refuse, inerte,</l>
					<l n="80" num="20.4"><space unit="char" quantity="8"></space>A te suivre dans ton essor !</l>
				</lg>
				<lg n="21">
					<l n="81" num="21.1">Qu’envahissant les cieux, l’Immobilité morne</l>
					<l n="82" num="21.2">Sous un voile funèbre éteigne tout flambeau,</l>
					<l n="83" num="21.3">Puisque d’un univers magnifique et sans borne</l>
					<l n="84" num="21.4"><space unit="char" quantity="8"></space>Tu n’as su faire qu’un tombeau !</l>
				</lg>
				<closer>
					<dateline>
						<placeName>Paris</placeName>,
						<date when="1871">février 1871</date>
						</dateline>
				</closer>
			</div></body></text></TEI>