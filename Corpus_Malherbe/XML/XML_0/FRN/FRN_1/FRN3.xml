<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Inattentions et Sollicitudes</title>
				<title type="medium">Édition électronique</title>
				<author key="FRN">
					<name>
						<forename>Maurice Étienne</forename>
						<surname>LEGRAND</surname>
						<addName type="pen_name">FRANC-NOHAIN</addName>
					</name>
					<date from="1872" to="1934">1872-1934</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>649 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">FRN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Les Inattentions et Sollicitudes</title>
						<author>Franc-Nohain</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k5438658j.texteImage</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Inattentions et Sollicitudes</title>
								<author>Franc-Nohain</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>L. VANIER</publisher>
									<date when="1894">1894</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1894">1894</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ) ont été restituées.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-06-04" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="FRN3">
		<head type="main">CANTILÈNE DES TRAINS<lb></lb>QU’ON MANQUE</head>
			<lg n="1">
				<l n="1" num="1.1">Ce sont les gares, les lointaines gares,</l>
				<l n="2" num="1.2">Où l’on arrive toujours trop tard.</l>
			</lg>
			<lg n="2">
				<l n="3" num="2.1">Belle-maman, embrassez-moi,</l>
				<l n="4" num="2.2">Embrassez-moi encore une fois,</l>
				<l n="5" num="2.3">Et empilons-nous comme des anchois</l>
				<l n="6" num="2.4">Dans le vieil omnibus bourgeois.</l>
			</lg>
			<lg n="3">
				<l n="7" num="3.1">Ouf, brouf !</l>
				<l n="8" num="3.2">Waterpoofs !</l>
				<l n="9" num="3.3">Cannes et parapluies…</l>
				<l n="10" num="3.4">Je ne sais plus du tout où j’en suis.</l>
			</lg>
			<lg n="4">
				<l n="11" num="4.1">Voici venir les hommes d’équipe</l>
				<l n="12" num="4.2">Qui regardent béatement en fumant leurs pipes.</l>
			</lg>
			<lg n="5">
				<l n="13" num="5.1">Le train, le train que j’entends !</l>
				<l n="14" num="5.2">Nous n’arriverons jamais à temps.</l>
				<l n="15" num="5.3">(Certainement.)</l>
			</lg>
			<lg n="6">
				<l n="16" num="6.1">— Monsieur, on ne peut plus enregistrer vos bagages :</l>
				<l n="17" num="6.2">C’est vraiment dommage.</l>
			</lg>
			<lg n="7">
				<l n="18" num="7.1">La cloche du départ, écoutez la cloche :</l>
				<l n="19" num="7.2">Le mécanicien et le chauffeur ont un cœur de roche ;</l>
				<l n="20" num="7.3">Alors, inutile d’agiter notre mouchoir de poche ?</l>
			</lg>
			<lg n="8">
				<l n="21" num="8.1">Ainsi les trains s’en vont rapides et discrets :</l>
				<l n="22" num="8.2">Et l’on est très embêté après.</l>
			</lg>
	</div></body></text></TEI>