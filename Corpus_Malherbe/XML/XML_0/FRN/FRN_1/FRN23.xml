<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Inattentions et Sollicitudes</title>
				<title type="medium">Édition électronique</title>
				<author key="FRN">
					<name>
						<forename>Maurice Étienne</forename>
						<surname>LEGRAND</surname>
						<addName type="pen_name">FRANC-NOHAIN</addName>
					</name>
					<date from="1872" to="1934">1872-1934</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>649 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">FRN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Les Inattentions et Sollicitudes</title>
						<author>Franc-Nohain</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k5438658j.texteImage</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Inattentions et Sollicitudes</title>
								<author>Franc-Nohain</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>L. VANIER</publisher>
									<date when="1894">1894</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1894">1894</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ) ont été restituées.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-06-04" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="FRN23">
		<head type="main">BIFUR</head>
			<lg n="1">
				<l n="1" num="1.1">Par une nuit, triste nuit sans astres ni lune,</l>
				<l n="2" num="1.2">Je partirai, portant sur moi toute ma fortune,</l>
				<l n="3" num="1.3">Et la gare sera quelqu’une.</l>
			</lg>
			<lg n="2">
				<l n="4" num="2.1">Vois-tu l’implacable <hi rend="ital">bifur</hi> ?</l>
			</lg>
			<lg n="3">
				<l n="5" num="3.1">Je veux aller loin, très loin, et loin plus encore,</l>
				<l n="6" num="3.2">Et que mon absence édulcore</l>
				<l n="7" num="3.3">L’amertume des mandragores.</l>
			</lg>
			<lg n="4">
				<l n="8" num="4.1">Je vois l’implacable <hi rend="ital">bifur</hi>.</l>
			</lg>
			<lg n="5">
				<l n="9" num="5.1">Oh ! qui me délivrera de ce doute ;</l>
				<l n="10" num="5.2">Oh ! qui me montrera la route</l>
				<l n="11" num="5.3">Dont nul rancoeur ne nous déboute ?</l>
			</lg>
			<lg n="6">
				<l n="12" num="6.1">Partout l’implacable <hi rend="ital">bifur</hi>.</l>
			</lg>
			<lg n="7">
				<l n="13" num="7.1">C’est le perpétuel recommencement des locomotives,</l>
				<l n="14" num="7.2">On part, on va, on vient, et, quand on arrive,</l>
				<l n="15" num="7.3">On trouve que ce n’était guère la peine, en définitive.</l>
			</lg>
			<lg n="8">
				<l n="16" num="8.1">Toujours l’implacable <hi rend="ital">bifur</hi>.</l>
			</lg>
			<lg n="9">
				<l n="17" num="9.1">Haute-Loire, Charente-Inférieure, Nièvre, Cantal, Orne,</l>
				<l n="18" num="9.2">Départements, Départements, comme c’est morne !</l>
				<l n="19" num="9.3">Alors quoi ? Asseyons-nous et pleurons sur quelque borne.</l>
			</lg>
			<lg n="10">
				<l n="20" num="10.1">Le voilà bien, l’implacable <hi rend="ital">bifur</hi>.</l>
			</lg>
	</div></body></text></TEI>