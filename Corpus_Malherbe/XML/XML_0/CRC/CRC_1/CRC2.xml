<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Poésies complètes</title>
				<title type="medium">Édition électronique</title>
				<author key="CRC">
					<name>
						<forename>Francis</forename>
						<surname>Carco</surname>
					</name>
					<date from="1886" to="1958">1886-1958</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>227 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CRC_1</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Poésies complètes</title>
						<author>Francis Carco</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k3366885p.texteImage</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies complètes</title>
								<author>Francis Carco</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher></publisher>
									<date when="1955">1955</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1955">1955</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CRC2">
			<head type="main">CHANSON</head>
			<lg n="1">
				<l n="1" num="1.1">La fée un soir m’a dit</l>
				<l n="2" num="1.2">Qu’il était une étoile</l>
				<l n="3" num="1.3">Sans voilette ni voile</l>
				<l n="4" num="1.4">Au cœur fort inédit</l>
				<l n="5" num="1.5">Et que cette ingénue,</l>
				<l n="6" num="1.6">Idéalement nue,</l>
				<l n="7" num="1.7">Fêterait ma venue</l>
				<l n="8" num="1.8">À son bleu paradis.</l>
			</lg>
			<lg n="2">
				<l n="9" num="2.1">Elle m’a dit encor :</l>
				<l n="10" num="2.2">« Pour arriver chez elle</l>
				<l n="11" num="2.3">Je te ferai des ailes</l>
				<l n="12" num="2.4">Et des couronnes d’or.</l>
				<l n="13" num="2.5">Loin de la poésie</l>
				<l n="14" num="2.6">De ta liqueur choisie,</l>
				<l n="15" num="2.7">Tu boiras l’ambroisie</l>
				<l n="16" num="2.8">Dans ton verre, à plein bord.</l>
			</lg>
			<lg n="3">
				<l n="17" num="3.1">Le front auréolé</l>
				<l n="18" num="3.2">D’impérissable gloire,</l>
				<l n="19" num="3.3">Tu pourras rire et boire,</l>
				<l n="20" num="3.4">Beau poète envolé.</l>
				<l n="21" num="3.5">Et l’Éternel lui-même,</l>
				<l n="22" num="3.6">À ton clair diadème,</l>
				<l n="23" num="3.7">Joindra l’éclat suprême</l>
				<l n="24" num="3.8">D’un amour étoilé. »</l>
			</lg>
			<lg n="4">
				<l n="25" num="4.1">J’avais déjà quitté</l>
				<l n="26" num="4.2">Mon étroite mansarde</l>
				<l n="27" num="4.3">Quand la lune blafarde</l>
				<l n="28" num="4.4">S’en vint me visiter.</l>
				<l n="29" num="4.5">À la lueur bénie</l>
				<l n="30" num="4.6">De cette vieille amie</l>
				<l n="31" num="4.7">J’ai compris l’ironie</l>
				<l n="32" num="4.8">De la réalité.</l>
			</lg>
		</div></body></text></TEI>