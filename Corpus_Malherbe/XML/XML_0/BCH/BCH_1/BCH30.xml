<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES POËMES DE L’AMOUR ET DE LA MER</title>
				<title type="medium">Édition électronique</title>
				<author key="BCH">
					<name>
						<forename>Maurice</forename>
						<surname>Bouchor</surname>
					</name>
					<date from="1855" to="1929">1855-1929</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3472 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BCH_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
					<title>LES POËMES DE L’AMOUR ET DE LA MER</title>
					<author>Maurice Bouchor</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k9736743c</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
							<title>LES POËMES DE L’AMOUR ET DE LA MER</title>
							<author>Maurice Bouchor</author>
							<imprint>
								<pubPlace>Paris</pubPlace>
								<publisher>Charpentier et Cie, Libraires-éditeurs</publisher>
								<date when="1876">1876</date>
							</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1876">1876</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p></p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">I</head><head type="main_part">LA FLEUR DES EAUX</head><div type="poem" key="BCH30">
					<head type="number">XXIX</head>
					<opener>
						<epigraph>
							<cit>
								<quote>
									Ave Maria ! ’t is the hour of prayer <lb></lb>
									Ave Maria ! ’t is the hour of love
								</quote>
								<bibl>
									(<name>BYRON</name>.)
								</bibl>
							</cit>
						</epigraph>
					</opener>
					<lg n="1">
						<l n="1" num="1.1">On entendait encore au loin, dans l’air du soir,</l>
						<l n="2" num="1.2">Les derniers tintements des cloches de l’église ;</l>
						<l n="3" num="1.3">En face de la mer je la menai s’asseoir</l>
						<l n="4" num="1.4">Et je lui dis : « Il faut aimer, c’est l’heure exquise.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">« La mer silencieuse est comme un lac d’argent</l>
						<l n="6" num="2.2">Et les flots de la mer sont comme des sourires ;</l>
						<l n="7" num="2.3">Mais les cloches du soir font seules de leur chant</l>
						<l n="8" num="2.4">Palpiter l’air salubre et frais que tu respires.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">« C’est l’heure de l’amour qui passe dans les cieux,</l>
						<l n="10" num="3.2">Et voici s’éveiller au vent de l’harmonie</l>
						<l n="11" num="3.3">Les étoiles ouvrant doucement leurs beaux yeux</l>
						<l n="12" num="3.4">Comme des fleurs d’azur dans la plaine infinie.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">« C’est l’heure de l’amour, ma bien-aimée, et c’est</l>
						<l n="14" num="4.2">L’heure de la prière ; à genoux sur la dure,</l>
						<l n="15" num="4.3">Tous les adorateurs de l’éternel, qui sait ?</l>
						<l n="16" num="4.4">L’implorent en tournant le dos à la nature.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">« Mais nous les amoureux des étoiles, les cœurs</l>
						<l n="18" num="5.2">Épanouis au soir comme des fleurs étranges,</l>
						<l n="19" num="5.3">Nous pouvons contempler, loin des regards moqueurs,</l>
						<l n="20" num="5.4">Le ciel où nous voyons distinctement des anges.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1">« Des anges déployant leurs larges ailes d’or,</l>
						<l n="22" num="6.2">Jetant à pleines mains des roses sur la terre,</l>
						<l n="23" num="6.3">Qui chantent à voix haute en prenant leur essor :</l>
						<l n="24" num="6.4">« C’est l’heure de l’amour, l’heure de la prière ! »</l>
					</lg>
					<ab type="star">⁂</ab>
					<lg n="7">
						<l n="25" num="7.1">« Qui veux-tu que je prie, et de quel nom nommer</l>
						<l n="26" num="7.2">Le Fantôme créé par la terreur des hommes ?</l>
						<l n="27" num="7.3">Car nous sommes ici, nous, pour nous trop aimer,</l>
						<l n="28" num="7.4">Comme des amoureux et des fous que nous sommes !</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1">« Et pour religion n’avons-nous pas l’amour ?</l>
						<l n="30" num="8.2">Et nous aimant, perdus sur la plage ignorée</l>
						<l n="31" num="8.3">Dans le silence et les ténèbres d’alentour,</l>
						<l n="32" num="8.4">Nous pouvons dire aussi que c’est l’heure sacrée ! »</l>
					</lg>
				</div></body></text></TEI>