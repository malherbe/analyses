<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES POËMES DE L’AMOUR ET DE LA MER</title>
				<title type="medium">Édition électronique</title>
				<author key="BCH">
					<name>
						<forename>Maurice</forename>
						<surname>Bouchor</surname>
					</name>
					<date from="1855" to="1929">1855-1929</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3472 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BCH_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
					<title>LES POËMES DE L’AMOUR ET DE LA MER</title>
					<author>Maurice Bouchor</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k9736743c</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
							<title>LES POËMES DE L’AMOUR ET DE LA MER</title>
							<author>Maurice Bouchor</author>
							<imprint>
								<pubPlace>Paris</pubPlace>
								<publisher>Charpentier et Cie, Libraires-éditeurs</publisher>
								<date when="1876">1876</date>
							</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1876">1876</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p></p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">III</head><head type="main_part">L’AMOUR DIVIN</head><div type="poem" key="BCH104">
					<head type="number">VI</head>
					<head type="main">AUTREFOIS</head>
					<lg n="1">
						<l n="1" num="1.1">Je me rappelle un soir des temps où j’ai vécu</l>
						<l n="2" num="1.2">Comme un autre, laissant s’épanouir mon âme</l>
						<l n="3" num="1.3">Aux sereines clartés des beaux yeux d’une femme</l>
						<l n="4" num="1.4">Qui m’avait regardé, et qui m’avait vaincu.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">Je me rappelle un soir de cette époque ancienne ;</l>
						<l n="6" num="2.2">Au brusque vent de nuit se tordaient ses cheveux,</l>
						<l n="7" num="2.3">Et je la suppliais du sourire et des yeux,</l>
						<l n="8" num="2.4">Et ma main étreignait si doucement la sienne !</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">Pourtant, bien que le flot murmurât jusqu’à nous,</l>
						<l n="10" num="3.2">Que nous eussions vingt ans et qu’elle fût si belle,</l>
						<l n="11" num="3.3">Comme un ange attristé qui referme son aile,</l>
						<l n="12" num="3.4">Elle ne me dit rien, ce soir de rendez-vous.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">Si ta main tout à coup, chère âme, fut glacée,</l>
						<l n="14" num="4.2">Si ta bouche perdit son rire et ses baisers,</l>
						<l n="15" num="4.3">C’est qu’un frisson mortel nous ayant traversés,</l>
						<l n="16" num="4.4">Nous eûmes tous les deux une même pensée.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">«La mort inévitable et la fatale nuit</l>
						<l n="18" num="5.2">Qui devait, tôt ou tard, peser sur nos paupières</l>
						<l n="19" num="5.3">Et l’éternel sommeil que l’on dort sur les pierres</l>
						<l n="20" num="5.4">Dans la vallée où nul soleil n’a jamais lui.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1">Cette fin de l’amour, comme de toutes choses,</l>
						<l n="22" num="6.2">Ce silence des cœurs qui battaient autrefois…</l>
						<l n="23" num="6.3">Puis, autour du tombeau des pas, des bruits de voix,</l>
						<l n="24" num="6.4">Et le suprême oubli tout parfumé de roses !</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1">La foi des jours anciens a fini par tarir,</l>
						<l n="26" num="7.2">Nul ne pense bondir jusqu’aux cieux d’un coup d’aile ;</l>
						<l n="27" num="7.3">Et ne comprenant rien à la vie immortelle,</l>
						<l n="28" num="7.4">Chaque jour, en vivant, nous nous sentons mourir.</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1">Ainsi, rien ne devait rester de notre extase !</l>
						<l n="30" num="8.2">Nos jours délicieux devaient donc s’échapper</l>
						<l n="31" num="8.3">Comme, goutte après goutte, et pour se dissiper,</l>
						<l n="32" num="8.4">L’eau s’échappe à travers les fêlures d’un vase !</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1">Et tristes, nous songions. Le sifflement aigu</l>
						<l n="34" num="9.2">Des bises se mêlait à la clameur des vagues ;</l>
						<l n="35" num="9.3">Et maintenant, perdu dans des souvenirs vagues,</l>
						<l n="36" num="9.4">Je me rappelle un soir du temps où j’ai vécu.</l>
					</lg>
				</div></body></text></TEI>