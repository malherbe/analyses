<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES POËMES DE L’AMOUR ET DE LA MER</title>
				<title type="medium">Édition électronique</title>
				<author key="BCH">
					<name>
						<forename>Maurice</forename>
						<surname>Bouchor</surname>
					</name>
					<date from="1855" to="1929">1855-1929</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3472 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BCH_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
					<title>LES POËMES DE L’AMOUR ET DE LA MER</title>
					<author>Maurice Bouchor</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k9736743c</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
							<title>LES POËMES DE L’AMOUR ET DE LA MER</title>
							<author>Maurice Bouchor</author>
							<imprint>
								<pubPlace>Paris</pubPlace>
								<publisher>Charpentier et Cie, Libraires-éditeurs</publisher>
								<date when="1876">1876</date>
							</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1876">1876</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p></p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">I</head><head type="main_part">LA FLEUR DES EAUX</head><div type="poem" key="BCH41">
					<head type="number">XL</head>
					<lg n="1">
						<l n="1" num="1.1">Après avoir marché sur la route durcie</l>
						<l n="2" num="1.2">Et sous le morne ciel noir comme du charbon,</l>
						<l n="3" num="1.3">Quand j’arrive chez toi, quand ma bouche transie</l>
						<l n="4" num="1.4">Cherche ta bouche où dort une odeur d’ambroisie,</l>
						<l n="5" num="1.5">Que les baisers sont froids, mais comme ils sentent bon !</l>
					</lg>
					<lg n="2">
						<l n="6" num="2.1">Dans les plis de ta robe alors posant ma tête,</l>
						<l n="7" num="2.2">Je m’enivre longtemps d’amour et de chaleur :</l>
						<l n="8" num="2.3">C’est une heure divine et cependant muette !</l>
						<l n="9" num="2.4">Je vois s’ouvrir tes yeux comme des violettes,</l>
						<l n="10" num="2.5">Et ta lèvre sanglante est l’églantine en fleur.</l>
					</lg>
					<lg n="3">
						<l n="11" num="3.1">Ton beau col et ta joue ont une grâce mièvre</l>
						<l n="12" num="3.2">Et qui n’appelle pas les caresses en vain ;</l>
						<l n="13" num="3.3">Alors me monte au front la rougeur de la fièvre,</l>
						<l n="14" num="3.4">Tes doigts que j’aime tant se posent sur ma lèvre</l>
						<l n="15" num="3.5">Et c’est comme un baiser qui n’aurait pas de fin.</l>
					</lg>
					<lg n="4">
						<l n="16" num="4.1">Le monotone bruit de l’incessante pluie,</l>
						<l n="17" num="4.2">Celui des flots, le vent passant comme un soupir,</l>
						<l n="18" num="4.3">Semblent le bâillement du monde qui s’ennuie ;</l>
						<l n="19" num="4.4">Mais il est une fleur bien fraîche épanouie</l>
						<l n="20" num="4.5">Que je sais où trouver et que je peux cueillir.</l>
					</lg>
				</div></body></text></TEI>