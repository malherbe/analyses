<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES POËMES DE L’AMOUR ET DE LA MER</title>
				<title type="medium">Édition électronique</title>
				<author key="BCH">
					<name>
						<forename>Maurice</forename>
						<surname>Bouchor</surname>
					</name>
					<date from="1855" to="1929">1855-1929</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3472 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BCH_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
					<title>LES POËMES DE L’AMOUR ET DE LA MER</title>
					<author>Maurice Bouchor</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k9736743c</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
							<title>LES POËMES DE L’AMOUR ET DE LA MER</title>
							<author>Maurice Bouchor</author>
							<imprint>
								<pubPlace>Paris</pubPlace>
								<publisher>Charpentier et Cie, Libraires-éditeurs</publisher>
								<date when="1876">1876</date>
							</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1876">1876</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p></p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">II</head><head type="main_part">LA MORT DE L’AMOUR</head><div type="poem" key="BCH52">
					<head type="number">I</head>
					<lg n="1">
						<l n="1" num="1.1">J’étais l’enfant sacré de la grande Nature,</l>
						<l n="2" num="1.2">Je marchais comme un dieu dans les sentiers déserts ;</l>
						<l n="3" num="1.3">Par les prés, par les monts errant à l’aventure,</l>
						<l n="4" num="1.4">J’écoutais tout le jour d’ineffables concerts.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">J’envoyais des baisers aux lointaines étoiles ;</l>
						<l n="6" num="2.2">Et lorsque dans la pourpre ardente du soleil</l>
						<l n="7" num="2.3">Je regardais glisser légèrement les voiles,</l>
						<l n="8" num="2.4">Le cœur me bondissait d’un élan nonpareil :</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">Car la mer où chatoient des milliers d’émeraudes,</l>
						<l n="10" num="3.2">Des saphirs transparents et des rubis en feu,</l>
						<l n="11" num="3.3">La riche mer séduit toutes les têtes chaudes</l>
						<l n="12" num="3.4">Et promène les cœurs sur son infini bleu.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">Le chant des rossignols, le vol des hirondelles,</l>
						<l n="14" num="4.2">Tout appartient à nous avant d’avoir vingt ans ;</l>
						<l n="15" num="4.3">L’amour de la beauté sait nous donner des ailes</l>
						<l n="16" num="4.4">Et nous nous envolons dans la joie, en chantant !</l>
					</lg>
					<ab type="star">⁂</ab>
					<lg n="5">
						<l n="17" num="5.1">Mais le vent a soufflé sur le château des nues,</l>
						<l n="18" num="5.2">Tout s’est évanoui dans la clarté des cieux ;</l>
						<l n="19" num="5.3">Je suis redevenu le vagabond des rues</l>
						<l n="20" num="5.4">Et Paris m’a repris dans ses bras monstrueux.</l>
					</lg>
				</div></body></text></TEI>