<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CHANSONS JOYEUSES</title>
				<title type="sub_2">MISES AU JOUR PAR UN ANE-ONYME, ONISSIME</title>
				<title type="sub_1">NOUVELLE ÉDITION</title>
				<title type="medium">Édition électronique</title>
				<author key="COL">
					<name>
						<forename>Charles</forename>
						<surname>COLLÉ</surname>
					</name>
					<date from="1709" to="1783">1709-1783</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>851 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">COL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>CHANSONS JOYEUSES</title>
						<author>Charles Collé</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="BNF">ark:/12148/bpt6k1073478c</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>CHANSONS JOYEUSES</title>
								<title>MISES AU JOUR PAR UN ANE-ONYME, ONISSIME</title>
								<author>Charles Collé</author>
								<edition>NOUVELLE ÉDITION</edition>
								<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k1073478c?rk=107296;4</idno>
								<imprint>
									<pubPlace>A Paris ; à Londres, et à Ispahan seulement</pubPlace>
									<publisher>De l’Imprimerie de l’Académie de Troyes</publisher>
									<date when="1765">1765</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>RECUEIL COMPLET DES CHANSONS DE COLLÉ</title>
						<author>Charles Collé</author>
						<edition>NOUVELLE ÉDITION , REVUE ET CORRIGÉE</edition>
						<idno type="URI">https://books.google.fr/books?id=W1U7AAAAcAAJ</idno>
						<imprint>
							<pubPlace>HAMBOURG ET PARIS</pubPlace>
							<publisher>CHEZ LES PRINCIPAUX LIBRAIRES</publisher>
							<date when="1864">1864</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1765">1765</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les notes de fin de page ont été reportées en fin de poème/</p>
				<correction>
					<p/>
				</correction>
				<normalization>
					<p> points</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2023-06-23" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.6.xsd)</change>
				<change when="2023-06-23" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique)</change>
				<change when="2023-06-23" who="RR">Corrections d’erreurs de numérisation</change>

			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="COL8">
				<head type="main">L’AMOUR APOTHICAIRE,</head>
				<head type="form">PARODIE.</head>
				<head type="tune">Air : Noté, N°. 8.</head>
				<lg n="1">
					<l n="1" num="1.1">POUR fléchir une None austere,</l>
					<l n="2" num="1.2"><space unit="char" quantity="2"/>Le malin petit Amour</l>
					<l n="3" num="1.3"><space unit="char" quantity="4"/>Ayant dessein un jour</l>
					<l n="4" num="1.4"><space unit="char" quantity="2"/>D’user d’un nouveau détour,</l>
					<l n="5" num="1.5">Prit l’habit d’un Apothicaire :</l>
					<l n="6" num="1.6"><space unit="char" quantity="2"/>En seringue, après cela,</l>
					<l n="7" num="1.7"><space unit="char" quantity="4"/>Son carquois, qu’il toucha,</l>
					<l n="8" num="1.8"><space unit="char" quantity="10"/>Se changea.</l>
				</lg>
				<ab type="star">❉</ab>
				<lg n="2">
					<l n="9" num="2.1">La None, en couvrant son derriere,</l>
					<l n="10" num="2.2"><space unit="char" quantity="2"/>Dit : donnez-moi sagement</l>
					<l n="11" num="2.3"><space unit="char" quantity="4"/>Ce benin lavement,</l>
					<l n="12" num="2.4"><space unit="char" quantity="2"/>Par le trou de ce drap blanc.</l>
					<l n="13" num="2.5"><space unit="char" quantity="8"/>Dès qu’il entra,</l>
					<l n="14" num="2.6"><space unit="char" quantity="2"/>D’abord elle s’écria :</l>
					<l n="15" num="2.7"><space unit="char" quantity="14"/>Ah !</l>
					<l n="16" num="2.8"><space unit="char" quantity="2"/>Prenez-garde, il est bien là.</l>
				</lg>
				<ab type="star">❉</ab>
				<lg n="3">
					<l n="17" num="3.1">Mon Dieu ! je ne sçais ou vous êtes ;</l>
					<l n="18" num="3.2"><space unit="char" quantity="2"/>Mais, quelle agréable ardeur !</l>
					<l n="19" num="3.3"><space unit="char" quantity="4"/>Je sens que la chaleur</l>
					<l n="20" num="3.4"><space unit="char" quantity="2"/>Me pénêtre au fond du cœur.</l>
					<l n="21" num="3.5">Mon enfant, quel bien vous me faites !</l>
					<l n="22" num="3.6"><space unit="char" quantity="2"/>Je me trouve beaucoup mieux,</l>
					<l n="23" num="3.7"><space unit="char" quantity="4"/>L’effet est merveilleux :</l>
					<l n="24" num="3.8"><space unit="char" quantity="10"/>J’en veux deux.</l>
				</lg>
			</div></body></text></TEI>