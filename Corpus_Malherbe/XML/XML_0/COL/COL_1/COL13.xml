<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CHANSONS JOYEUSES</title>
				<title type="sub_2">MISES AU JOUR PAR UN ANE-ONYME, ONISSIME</title>
				<title type="sub_1">NOUVELLE ÉDITION</title>
				<title type="medium">Édition électronique</title>
				<author key="COL">
					<name>
						<forename>Charles</forename>
						<surname>COLLÉ</surname>
					</name>
					<date from="1709" to="1783">1709-1783</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>851 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">COL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>CHANSONS JOYEUSES</title>
						<author>Charles Collé</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="BNF">ark:/12148/bpt6k1073478c</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>CHANSONS JOYEUSES</title>
								<title>MISES AU JOUR PAR UN ANE-ONYME, ONISSIME</title>
								<author>Charles Collé</author>
								<edition>NOUVELLE ÉDITION</edition>
								<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k1073478c?rk=107296;4</idno>
								<imprint>
									<pubPlace>A Paris ; à Londres, et à Ispahan seulement</pubPlace>
									<publisher>De l’Imprimerie de l’Académie de Troyes</publisher>
									<date when="1765">1765</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>RECUEIL COMPLET DES CHANSONS DE COLLÉ</title>
						<author>Charles Collé</author>
						<edition>NOUVELLE ÉDITION , REVUE ET CORRIGÉE</edition>
						<idno type="URI">https://books.google.fr/books?id=W1U7AAAAcAAJ</idno>
						<imprint>
							<pubPlace>HAMBOURG ET PARIS</pubPlace>
							<publisher>CHEZ LES PRINCIPAUX LIBRAIRES</publisher>
							<date when="1864">1864</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1765">1765</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les notes de fin de page ont été reportées en fin de poème/</p>
				<correction>
					<p/>
				</correction>
				<normalization>
					<p> points</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2023-06-23" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.6.xsd)</change>
				<change when="2023-06-23" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique)</change>
				<change when="2023-06-23" who="RR">Corrections d’erreurs de numérisation</change>

			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="COL13">
				<head type="main">VAUDEVILLE</head>
				<head type="sub_1">Pour la Rentrée des Théâtres.</head>
				<head type="tune">Air : Le Carillon de Vendôme ; <lb/> ou <lb/>Orléans Boigency, etc.</head>
				<head type="tune">Noté, N°. 12.</head>
				<lg n="1">
					<l n="1" num="1.1"><space unit="char" quantity="4"/>DANS ce jour, nos Auteurs,</l>
					<l n="2" num="1.2"><space unit="char" quantity="2"/>Nos Acteurs, nos Spectateurs,</l>
					<l n="3" num="1.3">Tout rentre, tout rentre, tout rentre.</l>
				</lg>
				<ab type="star">❉</ab>
				<lg n="2">
					<l n="4" num="2.1"><space unit="char" quantity="4"/>QUEL plaisir singulier,</l>
					<l n="5" num="2.2"><space unit="char" quantity="2"/>De sentir et de crier,</l>
					<l n="6" num="2.3">Je rentre, <subst hand="RR" reason="analysis" type="repetition"><del>etc</del><add>je rentre, je rentre</add></subst>.</l>
				</lg>
				<ab type="star">❉</ab>
				<lg n="3">
					<l n="7" num="3.1"><space unit="char" quantity="4"/>CE plaisir est piquant,</l>
					<l n="8" num="3.2"><space unit="char" quantity="2"/>Puisque le cœur me bat, quand</l>
					<l n="9" num="3.3">Je rentre, <subst hand="RR" reason="analysis" type="repetition"><del>etc</del><add>je rentre, je rentre</add></subst>.</l>
				</lg>
				<ab type="star">❉</ab>
				<lg n="4">
					<l n="10" num="4.1"><space unit="char" quantity="4"/>UN Acteur retiré,</l>
					<l n="11" num="4.2"><space unit="char" quantity="2"/>En est plus considéré,</l>
					<l n="12" num="4.3">S’il rentre, <subst hand="RR" reason="analysis" type="repetition"><del>etc</del><add>s’il rentre, s’il rentre</add></subst>.</l>
				</lg>
				<ab type="star">❉</ab>
				<lg n="5">
					<l n="13" num="5.1"><space unit="char" quantity="4"/>QUAND l’Époux est jaloux,</l>
					<l n="14" num="5.2"><space unit="char" quantity="2"/>On craint de voir cet Époux</l>
					<l n="15" num="5.3">Qui rentre, <subst hand="RR" reason="analysis" type="repetition"><del>etc</del><add>qui rentre, qui rentre</add></subst>.</l>
				</lg>
				<ab type="star">❉</ab>
				<lg n="6">
					<l n="16" num="6.1"><space unit="char" quantity="4"/>MAIS l’Amant est charmant,</l>
					<l n="17" num="6.2"><space unit="char" quantity="2"/>Justement dans le moment</l>
					<l n="18" num="6.3">Qu’il rentre, <subst hand="RR" reason="analysis" type="repetition"><del>etc</del><add>qu’il rentre, qu’il rentre</add></subst>.</l>
				</lg>
			</div></body></text></TEI>