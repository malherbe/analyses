<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CHANSONS JOYEUSES</title>
				<title type="sub_2">MISES AU JOUR PAR UN ANE-ONYME, ONISSIME</title>
				<title type="sub_1">NOUVELLE ÉDITION</title>
				<title type="medium">Édition électronique</title>
				<author key="COL">
					<name>
						<forename>Charles</forename>
						<surname>COLLÉ</surname>
					</name>
					<date from="1709" to="1783">1709-1783</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>851 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">COL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>CHANSONS JOYEUSES</title>
						<author>Charles Collé</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="BNF">ark:/12148/bpt6k1073478c</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>CHANSONS JOYEUSES</title>
								<title>MISES AU JOUR PAR UN ANE-ONYME, ONISSIME</title>
								<author>Charles Collé</author>
								<edition>NOUVELLE ÉDITION</edition>
								<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k1073478c?rk=107296;4</idno>
								<imprint>
									<pubPlace>A Paris ; à Londres, et à Ispahan seulement</pubPlace>
									<publisher>De l’Imprimerie de l’Académie de Troyes</publisher>
									<date when="1765">1765</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>RECUEIL COMPLET DES CHANSONS DE COLLÉ</title>
						<author>Charles Collé</author>
						<edition>NOUVELLE ÉDITION , REVUE ET CORRIGÉE</edition>
						<idno type="URI">https://books.google.fr/books?id=W1U7AAAAcAAJ</idno>
						<imprint>
							<pubPlace>HAMBOURG ET PARIS</pubPlace>
							<publisher>CHEZ LES PRINCIPAUX LIBRAIRES</publisher>
							<date when="1864">1864</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1765">1765</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les notes de fin de page ont été reportées en fin de poème/</p>
				<correction>
					<p/>
				</correction>
				<normalization>
					<p> points</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2023-06-23" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.6.xsd)</change>
				<change when="2023-06-23" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique)</change>
				<change when="2023-06-23" who="RR">Corrections d’erreurs de numérisation</change>

			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="COL51">
				<head type="main">RONDE.</head>
				<head type="tune">Air : Noté, N°. 30.</head>
				<lg n="1">
					<l n="1" num="1.1">A CONFESSE m’en suis allé</l>
					<l n="2" num="1.2"><space unit="char" quantity="4"/>Au Curé de Pompone :</l>
					<l n="3" num="1.3">Le plus gros péché que j’ai fait,</l>
					<l n="4" num="1.4"><space unit="char" quantity="4"/>C’est d’embrasser un homme.</l>
					<l n="5" num="1.5"><space unit="char" quantity="4"/>Ah ! il me souviendra,</l>
					<l n="6" num="1.6"><space unit="char" quantity="10"/>La-ri-ra,</l>
					<l n="7" num="1.7"><space unit="char" quantity="4"/>Du Curé de Pompone.</l>
				</lg>
				<ab type="star">❉</ab>
				<lg n="2">
					<l n="8" num="2.1">LE plus gros péché que j’ai fait,</l>
					<l n="9" num="2.2"><space unit="char" quantity="4"/>C’est d’embrasser un homme.</l>
					<l n="10" num="2.3">Ma Fille, pour ce péché là,</l>
					<l n="11" num="2.4"><space unit="char" quantity="4"/>Il faut aller à Rome.</l>
					<l n="12" num="2.5"><space unit="char" quantity="4"/>Ah ! il m’en souviendra,</l>
					<l n="13" num="2.6"><space unit="char" quantity="10"/>La-ri-ra,</l>
					<l n="14" num="2.7"><space unit="char" quantity="4"/>Du Curé de Pompone.</l>
				</lg>
				<ab type="star">❉</ab>
				<lg n="3">
					<l n="15" num="3.1">MA fille, pour ce péché là,</l>
					<l n="16" num="3.2"><space unit="char" quantity="4"/>Il saut aller à Rome.</l>
					<l n="17" num="3.3">Dites-moi, Monsieur le Curé,</l>
					<l n="18" num="3.4"><space unit="char" quantity="4"/>Y menerai-je l’homme ?</l>
					<l n="19" num="3.5"><space unit="char" quantity="4"/>Ah ! il me souviendra,</l>
					<l n="20" num="3.6"><space unit="char" quantity="10"/>La-ri-ra,</l>
					<l n="21" num="3.7"><space unit="char" quantity="4"/>Du Curé de Pompone.</l>
				</lg>
				<ab type="star">❉</ab>
				<lg n="4">
					<l n="22" num="4.1">DITES-MOI, Monsieur le Curé,</l>
					<l n="23" num="4.2"><space unit="char" quantity="4"/>Y menerai-je l’homme ?</l>
					<l n="24" num="4.3">Ah ! vous prenez goût au péché…</l>
					<l n="25" num="4.4"><space unit="char" quantity="4"/>Je vous entend, friponne.</l>
					<l n="26" num="4.5"><space unit="char" quantity="4"/>Ah ! il me souviendra,</l>
					<l n="27" num="4.6"><space unit="char" quantity="10"/>La-ri-ra,</l>
					<l n="28" num="4.7"><space unit="char" quantity="4"/>Du Curé de Pompone.</l>
				</lg>
				<ab type="star">❉</ab>
				<lg n="5">
					<l n="29" num="5.1">AH ! vous prenez goût au péché…</l>
					<l n="30" num="5.2"><space unit="char" quantity="4"/>Je vous entend, friponne.</l>
					<l n="31" num="5.3">Bien, baisez-moi cinq ou six fois,</l>
					<l n="32" num="5.4"><space unit="char" quantity="4"/>Et je vous le pardonne.</l>
					<l n="33" num="5.5"><space unit="char" quantity="4"/>Ah ! il me, <subst hand="RR" reason="analysis" type="repetition"><del>etc.</del><add>souviendra</add></subst>.</l>
					<l n="34" num="5.6"><space unit="char" quantity="10"/><subst hand="RR" reason="analysis" type="repetition"><del/><add>La-ri-ra,</add></subst>.</l>
					<l n="35" num="5.7"><space unit="char" quantity="4"/><subst hand="RR" reason="analysis" type="repetition"><del/><add>Du Curé de Pompone.</add></subst>.</l>

				</lg>
				<ab type="star">❉</ab>
				<lg n="6">
					<l n="36" num="6.1">BIEN, baisez-moi cinq ou six fois</l>
					<l n="37" num="6.2"><space unit="char" quantity="4"/>Et je vous le pardonne.</l>
					<l n="38" num="6.3">Grand merci, Monsieur le Curé,</l>
					<l n="39" num="6.4"><space unit="char" quantity="4"/>La Pénitence est bonne.</l>
					<l n="40" num="6.5"><space unit="char" quantity="4"/>Ah ! il me souviendra,</l>
					<l n="41" num="6.6"><space unit="char" quantity="10"/>La-ri-ra,</l>
					<l n="42" num="6.7"><space unit="char" quantity="4"/>Du Curé de Pompone.</l>
				</lg>
			</div></body></text></TEI>