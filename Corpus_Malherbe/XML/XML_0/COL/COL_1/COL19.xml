<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CHANSONS JOYEUSES</title>
				<title type="sub_2">MISES AU JOUR PAR UN ANE-ONYME, ONISSIME</title>
				<title type="sub_1">NOUVELLE ÉDITION</title>
				<title type="medium">Édition électronique</title>
				<author key="COL">
					<name>
						<forename>Charles</forename>
						<surname>COLLÉ</surname>
					</name>
					<date from="1709" to="1783">1709-1783</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>851 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">COL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>CHANSONS JOYEUSES</title>
						<author>Charles Collé</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="BNF">ark:/12148/bpt6k1073478c</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>CHANSONS JOYEUSES</title>
								<title>MISES AU JOUR PAR UN ANE-ONYME, ONISSIME</title>
								<author>Charles Collé</author>
								<edition>NOUVELLE ÉDITION</edition>
								<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k1073478c?rk=107296;4</idno>
								<imprint>
									<pubPlace>A Paris ; à Londres, et à Ispahan seulement</pubPlace>
									<publisher>De l’Imprimerie de l’Académie de Troyes</publisher>
									<date when="1765">1765</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>RECUEIL COMPLET DES CHANSONS DE COLLÉ</title>
						<author>Charles Collé</author>
						<edition>NOUVELLE ÉDITION , REVUE ET CORRIGÉE</edition>
						<idno type="URI">https://books.google.fr/books?id=W1U7AAAAcAAJ</idno>
						<imprint>
							<pubPlace>HAMBOURG ET PARIS</pubPlace>
							<publisher>CHEZ LES PRINCIPAUX LIBRAIRES</publisher>
							<date when="1864">1864</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1765">1765</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les notes de fin de page ont été reportées en fin de poème/</p>
				<correction>
					<p/>
				</correction>
				<normalization>
					<p> points</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2023-06-23" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.6.xsd)</change>
				<change when="2023-06-23" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique)</change>
				<change when="2023-06-23" who="RR">Corrections d’erreurs de numérisation</change>

			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="COL19" rhyme="none">
				<head type="main">COMPARAISONS</head>
				<head type="sub_1">GRIVOISES.</head>
				<head type="tune">Air : Noté, N°. 17.</head>
				<lg n="1">
					<l n="1" num="1.1">L’AMOUR est un chien de vaurien,</l>
					<l n="2" num="1.2">Qui sait plus de mal que de bien.</l>
					<l n="3" num="1.3"><space unit="char" quantity="4"/>Habitans de Gallere,</l>
					<l n="4" num="1.4"><space unit="char" quantity="4"/>N’vous plaignez pas d’ramer :</l>
					<l n="5" num="1.5"><space unit="char" quantity="4"/>Votre mal, c’est du suque,</l>
					<l n="6" num="1.6"><space unit="char" quantity="4"/>Près de c’ti’là d’aimer.</l>
				</lg>
				<ab type="star">❉</ab>
				<lg n="2">
					<l n="7" num="2.1">CE fut par un jour de Printems,</l>
					<l n="8" num="2.2">Que je me déclaris Amant,</l>
					<l n="9" num="2.3"><space unit="char" quantity="4"/>Amant d’une Burnette,</l>
					<l n="10" num="2.4"><space unit="char" quantity="4"/>Bell’ comme un Curpidon,</l>
					<l n="11" num="2.5"><space unit="char" quantity="4"/>Portant fine cornette</l>
					<l n="12" num="2.6"><space unit="char" quantity="4"/>Posée en parpillon.</l>
				</lg>
				<ab type="star">❉</ab>
				<lg n="3">
					<l n="13" num="3.1">ALL’ a tous les deux yeux bryans</l>
					<l n="14" num="3.2">Comme des Pierres de Diamans ;</l>
					<l n="15" num="3.3"><space unit="char" quantity="4"/>Et la rouge Écarlatte,</l>
					<l n="16" num="3.4"><space unit="char" quantity="4"/>Que l’on teint aux Gob’lins,</l>
					<l n="17" num="3.5"><space unit="char" quantity="4"/>N’est que d’la couleur jaune</l>
					<l n="18" num="3.6"><space unit="char" quantity="4"/>Auprès de son blanc tein.</l>
				</lg>
				<ab type="star">❉</ab>
				<lg n="4">
					<l n="19" num="4.1">ALL’ a de l’esprit fiérement</l>
					<l n="20" num="4.2">Tout comm’ un Garçon de trente ans ;</l>
					<l n="21" num="4.3"><space unit="char" quantity="4"/>Ça vous magn’ de l’ouvrage,</l>
					<l n="22" num="4.4"><space unit="char" quantity="2"/>Dam’ faut voir comme ça l’tient.</l>
					<l n="23" num="4.5"><space unit="char" quantity="4"/>L’Diable m’emporte, un’ Reine</l>
					<l n="24" num="4.6"><space unit="char" quantity="4"/>N’blanchiroit pas si ben.</l>
				</lg>
				<ab type="star">❉</ab>
				<lg n="5">
					<l n="25" num="5.1">J’SÇAIS ben qui ne tiendroit qu’à moi</l>
					<l n="26" num="5.2">De l’épouser, si all’ vouloit ;</l>
					<l n="27" num="5.3"><space unit="char" quantity="4"/>Son serviteur très-humble</l>
					<l n="28" num="5.4"><space unit="char" quantity="4"/>Attend sa volonté :</l>
					<l n="29" num="5.5"><space unit="char" quantity="4"/>Si ça se fait ben vîte,</l>
					<l n="30" num="5.6"><space unit="char" quantity="4"/>Fort content je serai.</l>
				</lg>
				</div></body></text></TEI>