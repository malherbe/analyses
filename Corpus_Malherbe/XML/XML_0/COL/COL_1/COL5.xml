<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CHANSONS JOYEUSES</title>
				<title type="sub_2">MISES AU JOUR PAR UN ANE-ONYME, ONISSIME</title>
				<title type="sub_1">NOUVELLE ÉDITION</title>
				<title type="medium">Édition électronique</title>
				<author key="COL">
					<name>
						<forename>Charles</forename>
						<surname>COLLÉ</surname>
					</name>
					<date from="1709" to="1783">1709-1783</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>851 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">COL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>CHANSONS JOYEUSES</title>
						<author>Charles Collé</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="BNF">ark:/12148/bpt6k1073478c</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>CHANSONS JOYEUSES</title>
								<title>MISES AU JOUR PAR UN ANE-ONYME, ONISSIME</title>
								<author>Charles Collé</author>
								<edition>NOUVELLE ÉDITION</edition>
								<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k1073478c?rk=107296;4</idno>
								<imprint>
									<pubPlace>A Paris ; à Londres, et à Ispahan seulement</pubPlace>
									<publisher>De l’Imprimerie de l’Académie de Troyes</publisher>
									<date when="1765">1765</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>RECUEIL COMPLET DES CHANSONS DE COLLÉ</title>
						<author>Charles Collé</author>
						<edition>NOUVELLE ÉDITION , REVUE ET CORRIGÉE</edition>
						<idno type="URI">https://books.google.fr/books?id=W1U7AAAAcAAJ</idno>
						<imprint>
							<pubPlace>HAMBOURG ET PARIS</pubPlace>
							<publisher>CHEZ LES PRINCIPAUX LIBRAIRES</publisher>
							<date when="1864">1864</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1765">1765</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les notes de fin de page ont été reportées en fin de poème/</p>
				<correction>
					<p/>
				</correction>
				<normalization>
					<p> points</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2023-06-23" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.6.xsd)</change>
				<change when="2023-06-23" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique)</change>
				<change when="2023-06-23" who="RR">Corrections d’erreurs de numérisation</change>

			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="COL5">
				<head type="main">LA CRAINTIVE RASSURÉE,</head>
				<head type="form">PARODIE.</head>
				<head type="tune">Air : Noté, N°. 5.</head>
				<lg n="1">
					<l n="1" num="1.1">LE gros Lucas, sous son chapeau,</l>
					<l n="2" num="1.2"><space unit="char" quantity="4"/>Cachoit une Fauvette ;</l>
					<l n="3" num="1.3">Et vîte, vîte prend l’oiseau,</l>
					<l n="4" num="1.4"><space unit="char" quantity="4"/>Disoit-il à Lisette ;</l>
					<l n="5" num="1.5">Approche donc, ta main l’aura.</l>
					<l n="6" num="1.6">Mais la solette s’écria ;</l>
					<l n="7" num="1.7"><space unit="char" quantity="4"/>Oui-dà, oui-dà, oui-dà,</l>
					<l n="8" num="1.8">Je ne mettrai pas la main là,</l>
					<l n="9" num="1.9"><space unit="char" quantity="12"/>Là, là ;</l>
					<l n="10" num="1.10"><space unit="char" quantity="4"/>Ho ! ho ! ho ! Ah ! ah ! ah !</l>
					<l n="11" num="1.11">C’est une attrappe que cela,</l>
					<l n="12" num="1.12"><space unit="char" quantity="12"/>Là, là.</l>
				</lg>
				<ab type="star">❉</ab>
				<lg n="2">
					<l n="13" num="2.1">LISON, Lison l’oiseau s’en va…</l>
					<l n="14" num="2.2"><space unit="char" quantity="4"/>S’il s’en va, quel dommage !</l>
					<l n="15" num="2.3">Mais, en est-ce un ? Oui, le voilà…</l>
					<l n="16" num="2.4"><space unit="char" quantity="4"/>Ah ! j’en vois le plumage :</l>
					<l n="17" num="2.5">Qu’il est charmant ! Ne mord-t-il pas ?</l>
					<l n="18" num="2.6">Prête-le moi, mon cher Lucas.</l>
					<l n="19" num="2.7"><space unit="char" quantity="4"/>Oui-dà, oui-dà, oui-dà :</l>
					<l n="20" num="2.8">Voudrois-tu mettre la main là,</l>
					<l n="21" num="2.9"><space unit="char" quantity="12"/>Là, là ?</l>
					<l n="22" num="2.10"><space unit="char" quantity="4"/>Oh ! oh ! oh ! Ah ! ah ! ah !</l>
					<l n="23" num="2.11">C’est une attrappe que cela,</l>
					<l n="24" num="2.12"><space unit="char" quantity="12"/>Là, là.</l>
				</lg>
				<ab type="star">❉</ab>
				<lg n="3">
					<l n="25" num="3.1">LISETTE avoit dans un endroit</l>
					<l n="26" num="3.2"><space unit="char" quantity="4"/>Une cage secrette :</l>
					<l n="27" num="3.3">Lucas l’entr’ouvrit, et tout droit</l>
					<l n="28" num="3.4"><space unit="char" quantity="4"/>D’abord l’oiseau s’y jette.</l>
					<l n="29" num="3.5">Il n’y fut pas, que le voilà</l>
					<l n="30" num="3.6">Qui se rengorgeant lui chanta,</l>
					<l n="31" num="3.7"><space unit="char" quantity="4"/>Oui-dà, oui-dà, oui-dà,</l>
					<l n="32" num="3.8">Ah ! que je me trouve bien là,</l>
					<l n="33" num="3.9"><space unit="char" quantity="12"/>Là, là !</l>
					<l n="34" num="3.10"><space unit="char" quantity="4"/>Oh ! oh ! oh ! Ah ! ah ! ah !</l>
					<l n="35" num="3.11">L’aimable cage que voilà,</l>
					<l n="36" num="3.12"><space unit="char" quantity="12"/>Là, là !</l>
				</lg>
				<ab type="star">❉</ab>
				<lg n="4">
					<l n="37" num="4.1">PAR quatre fois l’oiseau chanta</l>
					<l n="38" num="4.2"><space unit="char" quantity="4"/>Une chanson jolie ;</l>
					<l n="39" num="4.3">Trois fois Lisette partagea</l>
					<l n="40" num="4.4"><space unit="char" quantity="4"/>Sa douce mélodie.</l>
					<l n="41" num="4.5">Mais à la fin il s’enrhuma.</l>
					<l n="42" num="4.6">Lucas alors lui répéta,</l>
					<l n="43" num="4.7"><space unit="char" quantity="4"/>Oui-dà, oui-dà, oui-dà,</l>
					<l n="44" num="4.8">Retirons mon oiseau de-là,</l>
					<l n="45" num="4.9"><space unit="char" quantity="12"/>Là, là ;</l>
					<l n="46" num="4.10"><space unit="char" quantity="4"/>Oh ! oh ! oh ! Ah ! ah ! ah !</l>
					<l n="47" num="4.11">Mon oiseau maigriroit trop là,</l>
					<l n="48" num="4.12"><space unit="char" quantity="12"/>Là, là.</l>
				</lg>
			</div></body></text></TEI>