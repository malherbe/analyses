<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Rapsodies</title>
				<title type="medium">Une édition électronique</title>
				<author key="BOR">
					<name>
						<forename>Pétrus</forename>
						<surname>BOREL</surname>
					</name>
					<date from="1809" to="1859">1809-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1481 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BOR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Rapsodies</title>
						<author>Pétrus Borel</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/petruborelrhapsodies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Rapsodies</title>
						<author>Pétrus Borel</author>
						<imprint>
							<publisher>M. Levy,Paris</publisher>
							<date when="1868">1868</date>
						</imprint>
					</monogr>
				<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">1832</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>Les balises de pagination ont été supprimées.</p>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">RAPSODIES</head><div type="poem" key="BOR10">
						<head type="main">La Corse</head>
						<opener>
							<salute>À NAPOLÉON THOM, peintre.</salute>
							<epigraph>
								<cit>
									<quote>
										C’est tout simplement un peintre, Monseigneur, qui <lb/>
										se nomme Romano, qui vit de larcins faits à la nature, <lb/>
										qui n’a d’autres armoiries que ses pinceaux…

									</quote>
									<bibl>
										<name>SCHILLER</name>, <hi rend="ital">Fiesque, act. II</hi>.
									</bibl>
								</cit>
							</epigraph>
						</opener>
						<lg n="1">
							<l n="1" num="1.1">Le maëstral soufflait : la voûte purpurine</l>
							<l n="2" num="1.2">Brillait de mille feux comme une aventurine ;</l>
							<l n="3" num="1.3">Sur le bord expirait le chant des gondoliers ;</l>
							<l n="4" num="1.4">Un silence de mort planait sur ces campagnes.</l>
							<l n="5" num="1.5">Parfois, on entendait bien loin, dans les montagnes,</l>
							<l n="6" num="1.6"><space unit="char" quantity="8"/>Les sifflements des bandouliers.</l>
						</lg>
						<lg n="2">
							<l n="7" num="2.1">La mer était houleuse ; et la vague plaintive</l>
							<l n="8" num="2.2">Se berçait, et rampait, et saluait la rive,</l>
							<l n="9" num="2.3">Comme ces flots de rois, tous abreuvés de fiel,</l>
							<l n="10" num="2.4">Saluaient le soldat fils de ce roc sauvage.</l>
							<l n="11" num="2.5">— Un barde aurait pu dire au repos de la plage :</l>
							<l n="12" num="2.6"><space unit="char" quantity="8"/>Que la terre écoutait le ciel !</l>
						</lg>
						<lg n="3">
							<l n="13" num="3.1">L’horizon s’appuyait sur l’immense muraille</l>
							<l n="14" num="3.2">De colline, de mont, de rocher, de rocaille,</l>
							<l n="15" num="3.3">Qui sur la Corse au loin s’étend comme un géant,</l>
							<l n="16" num="3.4">Depuis Bonifacio veillant sur la Sardaigne</l>
							<l n="17" num="3.5">Jusques à la Bastia qui dans la mer se baigne,</l>
							<l n="18" num="3.6"><space unit="char" quantity="8"/>Et lève aux cieux un front d’argent.</l>
						</lg>
						<lg n="4">
							<l n="19" num="4.1">Tout dormait, se taisait : assis sur une pierre,</l>
							<l n="20" num="4.2">Auprès du seuil étroit de sa basse chaumière,</l>
							<l n="21" num="4.3">Un vigoureux chasseur, Viterbi le vieillard,</l>
							<l n="22" num="4.4">Homme doux dont le bras ne poignarda personne</l>
							<l n="23" num="4.5">Et dont la chevelure en blanchissant rayonne</l>
							<l n="24" num="4.6"><space unit="char" quantity="8"/>Sous son bonnet de montagnard.</l>
						</lg>
						<lg n="5">
							<l n="25" num="5.1">Avant d’entrer au lit, en ce lieu solitaire,</l>
							<l n="26" num="5.2">Courbé sur son mousquet, les yeux fichés eu terre,</l>
							<l n="27" num="5.3">Il aspirait du soir l’air pur vivifiant ;</l>
							<l n="28" num="5.4">Quand un éclair lointain jetait su large flamme,</l>
							<l n="29" num="5.5">Comme un enfant à Dieu recommandant son âme,</l>
							<l n="30" num="5.6"><space unit="char" quantity="8"/>Il signait son front suppliant.</l>
						</lg>
						<lg n="6">
							<l n="31" num="6.1">Tout à coup, il entend, se lève, écoute encore :</l>
							<l n="32" num="6.2">C’était un bruit de pas sur le chemin sonore.</l>
							<l n="33" num="6.3">— Qui vive ! garde à vous ! répondez ! — Un Français !</l>
							<l n="34" num="6.4">Un ami ! — Malheureux ! si tard en cette gorge,</l>
							<l n="35" num="6.5">Sans armes ! l’étranger, veux-tu que l’on t’égorge ?</l>
							<l n="36" num="6.6"><space unit="char" quantity="8"/>Est-ce la mort que tu cherchais ? — !</l>
						</lg>
						<lg n="7">
							<l n="37" num="7.1">— Je suis un jeune peintre, et, sans inquiétude,</l>
							<l n="38" num="7.2">Je revenais du val où je fais une étude ;</l>
							<l n="39" num="7.3">Signer, je suis Français et non point étranger,</l>
							<l n="40" num="7.4">Je revenais sans peur ; la nuit rien ne m’arrête ;</l>
							<l n="41" num="7.5">Portant sous mon manteau pour tout bien ma palette,</l>
							<l n="42" num="7.6"><space unit="char" quantity="8"/>Mon escarcelle est sans danger ! — !</l>
						</lg>
						<lg n="8">
							<l n="43" num="8.1">— Sais-tu bien que le Corse a soif de la vengeance,</l>
							<l n="44" num="8.2">Et non pas soif de l’or ? Malheur à qui l’offense !</l>
							<l n="45" num="8.3">Si ta mort est jurée, il comptera tes pas ;</l>
							<l n="46" num="8.4">S’il le faut dans les bois, ainsi qu’une hyène,</l>
							<l n="47" num="8.5">Un mois il attendra que sa victime vienne</l>
							<l n="48" num="8.6"><space unit="char" quantity="8"/>Pour se ruer sur son trépas.</l>
						</lg>
						<lg n="9">
							<l n="49" num="9.1">Puisque sans armes, seul, par cette route sombre</l>
							<l n="50" num="9.2">Tu marches, chante au moins, car peut-être dans l’ombre</l>
							<l n="51" num="9.3">Tu pourrais pour un autre être pris des brigands ;</l>
							<l n="52" num="9.4">Marche enchantant ces airs que mon âme aguerrie</l>
							<l n="53" num="9.5">A ton âge aimait tant, ces airs de ta patrie,</l>
							<l n="54" num="9.6"><space unit="char" quantity="8"/>Hymnes funèbres des tyrans ? — !</l>
						</lg>
						<lg n="10">
							<l n="55" num="10.1">Jeune, on ne saurait craindre, on rit de la prudence ;</l>
							<l n="56" num="10.2">Les avis d’un vieillard sont traités de démence :</l>
							<l n="57" num="10.3">Le cœur bouillant de vie est si peu soucieux !</l>
							<l n="58" num="10.4">Aussi ce jeune peintre, à ce que l’on raconte,</l>
							<l n="59" num="10.5">En souriait tout bas, n’en tenant aucun compte,</l>
							<l n="60" num="10.6"><space unit="char" quantity="8"/>Et s’éloigna silencieux.</l>
						</lg>
						<lg n="11">
							<l n="61" num="11.1">Mais tout près d’Oletta sa peur est éveillée :</l>
							<l n="62" num="11.2">Il entend quelque bruit. C’est, dit-il, la feuillée.</l>
							<l n="63" num="11.3">Mais une lame a lui parmi les oliviers ?…</l>
							<l n="64" num="11.4">Suis-je enfant de trembler ! c’est un follet qui passe,</l>
							<l n="65" num="11.5">Et ce long frôlement, et ce bruit de voix basse,</l>
							<l n="66" num="11.6"><space unit="char" quantity="8"/>C’est le murmure des viviers. — !</l>
						</lg>
						<lg n="12">
							<l n="67" num="12.1">A peine replongé dans quelque rêverie,</l>
							<l n="68" num="12.2">Il tomba sous le plomb d’une mousqueterie.</l>
							<l n="69" num="12.3">A son cri déchirant répond un rire affreux ;</l>
							<l n="70" num="12.4">Puis un homme accouru l’achève avec furie.</l>
							<l n="71" num="12.5">Enfer ! qu’ai-je donc fait ? je me trompe de vie !</l>
							<l n="72" num="12.6"><space unit="char" quantity="8"/>Ce n’est pas Viterbi le vieux ! — !</l>
						</lg>
						<lg n="13">
							<l n="73" num="13.1">La rage dans le cœur, il brise son épée,</l>
							<l n="74" num="13.2">Et disparaît soudain sous la roche escarpée…</l>
							<l n="75" num="13.3">Le passant matinal ne vit le lendemain,</l>
							<l n="76" num="13.4">Qu’un manteau teint de sang, des lambeaux de peinture,</l>
							<l n="77" num="13.5">Des ossements rongés, effroyable pâture !</l>
							<l n="78" num="13.6"><space unit="char" quantity="8"/>Un crâne épars sur le chemin.</l>
						</lg>
					</div></body></text></TEI>