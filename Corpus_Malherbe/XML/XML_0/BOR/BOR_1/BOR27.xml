<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Rapsodies</title>
				<title type="medium">Une édition électronique</title>
				<author key="BOR">
					<name>
						<forename>Pétrus</forename>
						<surname>BOREL</surname>
					</name>
					<date from="1809" to="1859">1809-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1481 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BOR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Rapsodies</title>
						<author>Pétrus Borel</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/petruborelrhapsodies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Rapsodies</title>
						<author>Pétrus Borel</author>
						<imprint>
							<publisher>M. Levy,Paris</publisher>
							<date when="1868">1868</date>
						</imprint>
					</monogr>
				<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">1832</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>Les balises de pagination ont été supprimées.</p>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">VILLANELLES</head><div type="poem" key="BOR27">
							<head type="main">La Soif des Amours</head>
							<opener>
								<epigraph>
									<cit>
										<quote>Hélène ; je vous suis tout vendu.</quote>
										<bibl>
											<name>AUGUSTUS MAC KEAT</name>.
										</bibl>
									</cit>
								</epigraph>
							</opener>
							<lg n="1">
								<l n="1" num="1.1"><space unit="char" quantity="2"/>Viens, accours, fille jolie !</l>
								<l n="2" num="1.2"><space unit="char" quantity="2"/>Viens, que j’oublie en ton sein</l>
								<l n="3" num="1.3"><space unit="char" quantity="10"/>Le chagrin,</l>
								<l n="4" num="1.4"><space unit="char" quantity="2"/>Qui, partout, dans cette vie,</l>
								<l n="5" num="1.5"><space unit="char" quantity="2"/>Suit le pauvre pèlerin ;</l>
							</lg>
							<lg n="2">
								<l n="6" num="2.1">Qu’un autre envieux de la gloire</l>
								<l n="7" num="2.2">Dans le tracas coule ses jours ;</l>
								<l n="8" num="2.3"><space unit="char" quantity="10"/>Moi, toujours,</l>
								<l n="9" num="2.4">Riant de ce mot illusoire,</l>
								<l n="10" num="2.5">Je n’ai que la soif des amours !</l>
							</lg>
							<lg n="3">
								<l n="11" num="3.1"><space unit="char" quantity="2"/>Viens, accours, fille jolie !</l>
								<l n="12" num="3.2"><space unit="char" quantity="2"/>Viens, que j’oublie en ton sein</l>
								<l n="13" num="3.3"><space unit="char" quantity="10"/>Le chagrin,</l>
								<l n="14" num="3.4"><space unit="char" quantity="2"/>Qui, partout, dans cette vie,</l>
								<l n="15" num="3.5"><space unit="char" quantity="2"/>Suit le pauvre pèlerin.</l>
							</lg>
							<lg n="4">
								<l n="16" num="4.1">Qu’un buveur, la tasse remplie,</l>
								<l n="17" num="4.2">Aux coteaux consacre ses jours ;</l>
								<l n="18" num="4.3"><space unit="char" quantity="10"/>Moi, toujours,</l>
								<l n="19" num="4.4">Sans goût savourant l’ambroisie,</l>
								<l n="20" num="4.5">Je n’ai que la soif des amours !</l>
							</lg>
							<lg n="5">
								<l n="21" num="5.1"><space unit="char" quantity="2"/>Viens, accours, fille jolie !</l>
								<l n="22" num="5.2"><space unit="char" quantity="2"/>Viens, que j’oublie en ton sein</l>
								<l n="23" num="5.3"><space unit="char" quantity="10"/>Le chagrin,</l>
								<l n="24" num="5.4"><space unit="char" quantity="2"/>Qui, partout, dans cette vie,</l>
								<l n="25" num="5.5"><space unit="char" quantity="2"/>Suit le pauvre pèlerin.</l>
							</lg>
							<lg n="6">
								<l n="26" num="6.1">Qu’un ladre accumulant sans cesse,</l>
								<l n="27" num="6.2">Sur ses trésors traîne ses jours ;</l>
								<l n="28" num="6.3"><space unit="char" quantity="10"/>Moi, toujours,</l>
								<l n="29" num="6.4">Méprisant honneurs et richesse,</l>
								<l n="30" num="6.5">Je n’ai que la soif des amours !</l>
							</lg>
							<lg n="7">
								<l n="31" num="7.1"><space unit="char" quantity="2"/>Viens, accours, fille jolie !</l>
								<l n="32" num="7.2"><space unit="char" quantity="2"/>Viens, que j’oublie en ton sein</l>
								<l n="33" num="7.3"><space unit="char" quantity="10"/>Le chagrin,</l>
								<l n="34" num="7.4"><space unit="char" quantity="2"/>Qui, partout, dans cette vie,</l>
								<l n="35" num="7.5"><space unit="char" quantity="2"/>Suit le pauvre pèlerin.</l>
							</lg>
							<lg n="8">
								<l n="36" num="8.1">Qu’un Anglais trace sur la tombe</l>
								<l n="37" num="8.2">Des vers sombres comme ses jours ;</l>
								<l n="38" num="8.3"><space unit="char" quantity="10"/>Moi, toujours,</l>
								<l n="39" num="8.4">Sur des fleurs ma lyre retombe,</l>
								<l n="40" num="8.5">Je n’ai que la soif des amours !</l>
							</lg>
							<lg n="9">
								<l n="41" num="9.1"><space unit="char" quantity="2"/>Viens, accours, fille jolie !</l>
								<l n="42" num="9.2"><space unit="char" quantity="2"/>Viens, que j’oublie en ton sein</l>
								<l n="43" num="9.3"><space unit="char" quantity="10"/>Le chagrin,</l>
								<l n="44" num="9.4"><space unit="char" quantity="2"/>Qui, partout, dans cette vie,</l>
								<l n="45" num="9.5"><space unit="char" quantity="2"/>Suit le pauvre pèlerin.</l>
							</lg>
							<lg n="10">
								<l n="46" num="10.1">Le temps éteindra sous ses ailes</l>
								<l n="47" num="10.2">Les feux ardents de mes beaux jours ;</l>
								<l n="48" num="10.3"><space unit="char" quantity="10"/>Moi, toujours,</l>
								<l n="49" num="10.4">Je serai galant près des belles,</l>
								<l n="50" num="10.5">Je n’ai que la soif des amours !</l>
							</lg>
							<lg n="11">
								<l n="51" num="11.1"><space unit="char" quantity="2"/>Viens, accours, fille jolie !</l>
								<l n="52" num="11.2"><space unit="char" quantity="2"/>Viens, que j’oublie en ton sein</l>
								<l n="53" num="11.3"><space unit="char" quantity="10"/>Le chagrin,</l>
								<l n="54" num="11.4"><space unit="char" quantity="2"/>Qui, partout, dans cette vie,</l>
								<l n="55" num="11.5"><space unit="char" quantity="2"/>Suit le pauvre pèlerin.</l>
							</lg>
						</div></body></text></TEI>