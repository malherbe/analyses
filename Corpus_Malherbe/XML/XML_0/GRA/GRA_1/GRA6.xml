<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">SEXTINES</title>
				<title type="medium">Édition électronique</title>
				<author key="GRA">
					<name>
						<forename>Ferdinand</forename>
						<nameLink>de</nameLink>
						<surname>GRAMONT</surname>
					</name>
					<date when="1897">1815-1897</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>390 vers39 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2015</date>
				<idno type="local">GRA_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">SEXTINES</title>
						<author>Comte de Gramont</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">http://fr.wikisource.org/wiki/Livre :Gramont_-_Sextines,_1872.djvu</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title type="main">SEXTINES</title>
								<author>Comte de Gramont</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>ALPHONSE LEMERRE ÉDITEUR</publisher>
									<date when="1872">1872</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1872">1872</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-27" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="GRA6">
				<head type="main">SEXTINE VI</head>
				<head type="sub">L’AMOUR PHÉNIX.</head>
					<lg n="1">
						<l n="1" num="1.1">Quand le désir de l’homme, isolé sur la terre,</l>
						<l n="2" num="1.2">Des fragiles amours a broyé le trésor,</l>
						<l n="3" num="1.3">Avant l’heure où l’espoir dans notre âme s’altère,</l>
						<l n="4" num="1.4">O solitude, il faut, sur ta montagne austère,</l>
						<l n="5" num="1.5">Voyageur idéal, qu’il tourne son essor,</l>
						<l n="6" num="1.6">Et pour monter plus haut qu’il se prépare encor.</l>
					</lg>
					<lg n="2">
						<l n="7" num="2.1">Des circuits inféconds son aile lasse encor</l>
						<l n="8" num="2.2">Lui pèse ; mais à peine a disparu la terre.</l>
						<l n="9" num="2.3">Le phénix immortel retrouve son essor.</l>
						<l n="10" num="2.4">De myrrhe et d’aloès il s’est fait un trésor.</l>
						<l n="11" num="2.5">Il dresse son bûcher sous le regard austère</l>
						<l n="12" num="2.6">D’un soleil que jamais le nuage n’altère.</l>
					</lg>
					<lg n="3">
						<l n="13" num="3.1">Le prodige n’a pas de témoin qui l’altère.</l>
						<l n="14" num="3.2">La flamme vient du ciel ; la brise en vient encor ;</l>
						<l n="15" num="3.3">La victime y retourne. En cette épreuve austère</l>
						<l n="16" num="3.4">Elle va dépouiller l’empreinte de la terre.</l>
						<l n="17" num="3.5">Désormais elle sait où chercher son trésor</l>
						<l n="18" num="3.6">Et quel souffle puissant soutiendra son essor.</l>
					</lg>
					<lg n="4">
						<l n="19" num="4.1">Ainsi doit s’accomplir l’orbe de votre essor,</l>
						<l n="20" num="4.2">O vous qui, pour combler l’ardeur qui vous altère</l>
						<l n="21" num="4.3">D’un éternel amour réclamez le trésor.</l>
						<l n="22" num="4.4">Dans le gouffre pourquoi vous replonger encor ?</l>
						<l n="23" num="4.5">Aux futiles moissons que vous promet la terre</l>
						<l n="24" num="4.6">Pourriez-vous asservir votre espérance austère ?</l>
					</lg>
					<lg n="5">
						<l n="25" num="5.1">De vos fiers devanciers suivez l’exemple austère.</l>
						<l n="26" num="5.2">Si plus d’un a laissé, pour guider votre essor,</l>
						<l n="27" num="5.3">Un lumineux sillon au-dessus de la terre,</l>
						<l n="28" num="5.4">Craignez que de l’esprit le regard ne s’altère.</l>
						<l n="29" num="5.5">Que votre âme n’abdique, ou ne se trouve encor</l>
						<l n="30" num="5.6">Indigne d’aspirer au sublime trésor.</l>
					</lg>
					<lg n="6">
						<l n="31" num="6.1">S’il le faut, pour gagner un semblable trésor,</l>
						<l n="32" num="6.2">Laissez tremper vos fronts dans cette neige austère</l>
						<l n="33" num="6.3">Dont les abords du mont se cuirassent encor.</l>
						<l n="34" num="6.4">Aux pointes des rochers suspendant votre essor,</l>
						<l n="35" num="6.5">Que l’onde des torrents seule vous désaltère.</l>
						<l n="36" num="6.6">Ce qu’il faut, avant tout, c’est l’oubli de la terre.</l>
					</lg>
					<lg n="7">
						<l n="37" num="7.1">Car la terre est stérile, et le plus beau trésor</l>
						<l n="38" num="7.2">Qu’elle ait encor promis à la pensée austère</l>
						<l n="39" num="7.3">N’est qu’un bruit dont bientôt doit s’altérer l’essor.</l>
					</lg>
			</div></body></text></TEI>