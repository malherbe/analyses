<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">SEXTINES</title>
				<title type="medium">Édition électronique</title>
				<author key="GRA">
					<name>
						<forename>Ferdinand</forename>
						<nameLink>de</nameLink>
						<surname>GRAMONT</surname>
					</name>
					<date when="1897">1815-1897</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>390 vers39 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2015</date>
				<idno type="local">GRA_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">SEXTINES</title>
						<author>Comte de Gramont</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">http://fr.wikisource.org/wiki/Livre :Gramont_-_Sextines,_1872.djvu</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title type="main">SEXTINES</title>
								<author>Comte de Gramont</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>ALPHONSE LEMERRE ÉDITEUR</publisher>
									<date when="1872">1872</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1872">1872</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-27" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="GRA8">
				<head type="main">SEXTINE VIII.</head>
				<head type="sub">LA PAROLE DES BOIS.</head>
					<lg n="1">
						<l n="1" num="1.1">Oui, j’aimais dans les bois à retrouver ma trace.</l>
						<l n="2" num="1.2">Je voyais, repassant par le même chemin,</l>
						<l n="3" num="1.3">Les saisons de plus près m’y découvrir leur face.</l>
						<l n="4" num="1.4">Les arbres et les fleurs dont je savais la place</l>
						<l n="5" num="1.5">Semblaient me reconnaître et me dire : A demain.</l>
						<l n="6" num="1.6">Le sol prenait pour moi quelque chose d’humain.</l>
					</lg>
					<lg n="2">
						<l n="7" num="2.1">O nature, toujours si douce au cœur humain,</l>
						<l n="8" num="2.2">De quels moments bénis tu me gardes la trace !</l>
						<l n="9" num="2.3">Mais parle-moi d’hier ; que m’importe demain !</l>
						<l n="10" num="2.4">Bien des fois tu m’as vu parcourir ce chemin ;</l>
						<l n="11" num="2.5">Bien des fois je me suis assis à cette place,</l>
						<l n="12" num="2.6">T’écoutant me parler ainsi que face à face.</l>
					</lg>
					<lg n="3">
						<l n="13" num="3.1">Entretien bienfaisant ! L’ombre que sur ma face</l>
						<l n="14" num="3.2">Avait fait s’amasser le froissement humain</l>
						<l n="15" num="3.3">S’éclaircissait ; bientôt je sentais à la place</l>
						<l n="16" num="3.4">S’imprimer ton sourire en lumineuse trace.</l>
						<l n="17" num="3.5">Insoucieux du but, Je suivais mon chemin.</l>
						<l n="18" num="3.6">Ce jour me suffisait et j’oubliais demain.</l>
					</lg>
					<lg n="4">
						<l n="19" num="4.1">Menace plus souvent qu’espérance, demain</l>
						<l n="20" num="4.2">C’est le spectre importun dont la douteuse face,</l>
						<l n="21" num="4.3">Oscillant au milieu des rêves du chemin,</l>
						<l n="22" num="4.4">Aux vulgaires ennuis rappelle l’être humain.</l>
						<l n="23" num="4.5">Ah ! plutôt, hors du monde encor sur votre trace,</l>
						<l n="24" num="4.6">Souvenirs vagabonds, souffrez que je me place !</l>
					</lg>
					<lg n="5">
						<l n="25" num="5.1">Je l’avais bien compris, c’était ici ma place.</l>
						<l n="26" num="5.2">Ces grands bois qui toujours ont même lendemain,</l>
						<l n="27" num="5.3">Ces sentiers où les pas ne laissent qu’une trace</l>
						<l n="28" num="5.4">Insensible, cet air qui vous souffle à la face</l>
						<l n="29" num="5.5">Son arôme épuré de tout miasme humain,</l>
						<l n="30" num="5.6">Voilà ce qui m’eût fait un facile chemin.</l>
					</lg>
					<lg n="6">
						<l n="31" num="6.1">Il a fallu pourtant le quitter ce chemin,</l>
						<l n="32" num="6.2">Vivre dépaysé, triste, hors de ma place,</l>
						<l n="33" num="6.3">Souffrir incessamment du voisinage humain</l>
						<l n="34" num="6.4">Tel qu’en nos jours il est, en pensant que demain</l>
						<l n="35" num="6.5">J’aurais mêmes ennuis, mêmes dégoûts en face,</l>
						<l n="36" num="6.6">Sans pouvoir retourner à mon ancienne trace.</l>
					</lg>
					<lg n="7">
						<l n="37" num="7.1">O trace misérable, obscur et dur chemin</l>
						<l n="38" num="7.2">Où, la face vieillie, un sort jaloux me place,</l>
						<l n="39" num="7.3">Puisse demain finir cet exil inhumain !</l>
					</lg>
			</div></body></text></TEI>