<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">SEXTINES</title>
				<title type="medium">Édition électronique</title>
				<author key="GRA">
					<name>
						<forename>Ferdinand</forename>
						<nameLink>de</nameLink>
						<surname>GRAMONT</surname>
					</name>
					<date when="1897">1815-1897</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>390 vers39 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2015</date>
				<idno type="local">GRA_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">SEXTINES</title>
						<author>Comte de Gramont</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">http://fr.wikisource.org/wiki/Livre :Gramont_-_Sextines,_1872.djvu</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title type="main">SEXTINES</title>
								<author>Comte de Gramont</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>ALPHONSE LEMERRE ÉDITEUR</publisher>
									<date when="1872">1872</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1872">1872</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-27" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="GRA3">
				<head type="main">SEXTINE III,</head>
				<head type="sub">LA CLAIRIÈRE.</head>
					<lg n="1">
						<l n="1" num="1.1">Non loin encor de l’heure où rougit la nuit sombre.</l>
						<l n="2" num="1.2">En la saison des nids et des secondes fleurs,</l>
						<l n="3" num="1.3">J’entrai dans un bosquet, non pour y chercher l’ombre,</l>
						<l n="4" num="1.4">Mais parce qu’on voyait, sous les feuilles sans nombre.</l>
						<l n="5" num="1.5">Palpiter des rayons et d’étranges couleurs,</l>
						<l n="6" num="1.6">Et l’aurore au soleil y disputer ses pleurs.</l>
					</lg>
					<lg n="2">
						<l n="7" num="2.1">Mon sang, dans le trajet, teignit de quelques pleurs</l>
						<l n="8" num="2.2">Les aiguillons du houx et la barrière sombre</l>
						<l n="9" num="2.3">Que l’épine et la ronce aux vineuses couleurs</l>
						<l n="10" num="2.4">Avaient lacée autour de l’asile des fleurs.</l>
						<l n="11" num="2.5">Dans la clairière enfin quel m’apparut leur nombre,</l>
						<l n="12" num="2.6">Alors que du fourré j’atteignis la pénombre !</l>
					</lg>
					<lg n="3">
						<l n="13" num="3.1">Harmonieux réseau de lumières et d’ombre !</l>
						<l n="14" num="3.2">Là tous les diamants de la rosée en pleurs,</l>
						<l n="15" num="3.3">Les perles à foison, les opales sans nombre.</l>
						<l n="16" num="3.4">Dans la neige et dans l’or ou le rubis plus sombre.</l>
						<l n="17" num="3.5">Frémissaient, et, filtrant de la coupe des fleurs.</l>
						<l n="18" num="3.6">Allaient du doux feuillage argenter les couleurs.</l>
					</lg>
					<lg n="4">
						<l n="19" num="4.1">C’est alors qu’une Fée aux charmantes couleurs,</l>
						<l n="20" num="4.2">Sortant comme du tronc d’un grand chêne sans ombre</l>
						<l n="21" num="4.3">Qui défendait du nord le royaume des fleurs.</l>
						<l n="22" num="4.4">Apparut à mes yeux encor vierges de pleurs.</l>
						<l n="23" num="4.5">Elle me dit : « Ainsi tu fuis la route sombre.</l>
						<l n="24" num="4.6">Et de mes ouvriers tu veux grossir le nombre.</l>
					</lg>
					<lg n="5">
						<l n="25" num="5.1">« Contemple mes trésors, et choisis dans le nombre</l>
						<l n="26" num="5.2">Avec art, à loisir assemble leurs couleurs.</l>
						<l n="27" num="5.3">Compose ta guirlande, et, si le vent plus sombre</l>
						<l n="28" num="5.4">En bannit le soleil et les sèche dans l’ombre,</l>
						<l n="29" num="5.5">Répands-y de ton âme et la flamme et les pleurs ;</l>
						<l n="30" num="5.6">Des rayons immortels jailliront de ces fleurs. »</l>
					</lg>
					<lg n="6">
						<l n="31" num="6.1">Je vous cueillis alors, chères et chastes fleurs,</l>
						<l n="32" num="6.2">Et je n’ai plus tenté d’accroître votre nombre.</l>
						<l n="33" num="6.3">Celle-là n’a voulu que mon sang et mes pleurs,</l>
						<l n="34" num="6.4">A qui je destinais vos royales couleurs ;</l>
						<l n="35" num="6.5">Et je suis revenu, pour vous sauver de l’ombre.</l>
						<l n="36" num="6.6">Vers la Fée elle-même, avec le cœur bien sombre.</l>
					</lg>
					<lg n="7">
						<l n="37" num="7.1">Plus sombre en est le deuil qui s’entoure de fleurs</l>
						<l n="38" num="7.2">L’ombre, pour nous calmer, a des oublis sans nombre,</l>
						<l n="39" num="7.3">Mais aux couleurs du jour se ravivent les pleurs.</l>
					</lg>
			</div></body></text></TEI>