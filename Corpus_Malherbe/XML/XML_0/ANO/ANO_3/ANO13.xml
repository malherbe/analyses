<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">Le petit-neveu de Grécourt</title>
				<title type="sub_2">ou</title>
				<title type="sub_1">Étrennes gaillardes</title>
				<title type="medium">Édition électronique</title>
				<author key="ANO">
					<name type="anonymous">(anonyme)</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2164 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref> 
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">ANO_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le petit-neveu de Grécourt ou Étrennes gaillardes</title>
						<author>(anonyme)</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URI">https://www.gutenberg.org/ebooks/60896</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le petit-neveu de Grécourt ou Étrennes gaillardes</title>
								<author>(anonyme)</author>
								<idno type="URI">https://archive.org/details/lepetitneveudegr00pari/mode/2up</idno>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>Isidore LISEUX, Éditeur</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-08-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-08-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="ANO13">
					<head type="main">LA RAGE D’AMOUR</head>
					<head type="form">CONTE</head>
					<lg n="1">
						<l n="1" num="1.1">A Cupidon la jeune et belle Amynthe</l>
						<l n="2" num="1.2">Malgré l’hymen sacrifioit toujours ;</l>
						<l n="3" num="1.3">Son pauvre époux toujours étoit en crainte</l>
						<l n="4" num="1.4">Qu’elle ne fît de nouvelles amours.</l>
						<l n="5" num="1.5">Il ne pouvoit en siller la paupière ;</l>
						<l n="6" num="1.6">Veilles, soucis l’eurent tôt emporté.</l>
						<l n="7" num="1.7">Lui mort, Amynthe, en pleine liberté,</l>
						<l n="8" num="1.8">A son humeur donna belle carrière ;</l>
						<l n="9" num="1.9">On en jasa ; son Curé crut devoir</l>
						<l n="10" num="1.10">L’en avertir : « Vous vous perdez, Madame,</l>
						<l n="11" num="1.11">» Changez de vie, ou c’est fait de votre âme !</l>
						<l n="12" num="1.12">» ‒ Hélas ! Monsieur, je voudrois le pouvoir, »</l>
						<l n="13" num="1.13">Lui répondit la trop fringante veuve ;</l>
						<l n="14" num="1.14">« Mais plaignez-moi, tel est mon ascendant,</l>
						<l n="15" num="1.15">» Que je ne puis avoir l’esprit content,</l>
						<l n="16" num="1.16">» Si chaque mois je n’ai pratique neuve.</l>
						<l n="17" num="1.17">» Cela me vient d’un accident fatal :</l>
						<l n="18" num="1.18">» A quatorze ans d’un chien je fus mordue,</l>
						<l n="19" num="1.19">» Chien enragé. Pour prévenir le mal,</l>
						<l n="20" num="1.20">» L’avis commun fut qu’il me falloit nue</l>
						<l n="21" num="1.21">» Plonger en mer. Nue on me dépouilla.</l>
						<l n="22" num="1.22">» Honteuse alors de me voir sans chemise,</l>
						<l n="23" num="1.23">» Incontinent je portai la main là…</l>
						<l n="24" num="1.24">» Où vous savez, sans jamais lâcher prise.</l>
						<l n="25" num="1.25">» On me plongea ; mais qu’est-il arrivé ?</l>
						<l n="26" num="1.26">« C’est que mon corps, ô pudeur trop funeste !</l>
						<l n="27" num="1.27">» Partout ailleurs du mal fut préservé,</l>
						<l n="28" num="1.28">» Hors cet endroit où la rage me reste. »</l>
					</lg>
				</div></body></text></TEI>