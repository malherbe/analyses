<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">Le petit-neveu de Grécourt</title>
				<title type="sub_2">ou</title>
				<title type="sub_1">Étrennes gaillardes</title>
				<title type="medium">Édition électronique</title>
				<author key="ANO">
					<name type="anonymous">(anonyme)</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2164 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref> 
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">ANO_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le petit-neveu de Grécourt ou Étrennes gaillardes</title>
						<author>(anonyme)</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URI">https://www.gutenberg.org/ebooks/60896</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le petit-neveu de Grécourt ou Étrennes gaillardes</title>
								<author>(anonyme)</author>
								<idno type="URI">https://archive.org/details/lepetitneveudegr00pari/mode/2up</idno>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>Isidore LISEUX, Éditeur</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-08-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-08-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="ANO43">
					<head type="main">LE MARCHAND DE LOTO</head>
					<head type="sub_1">ÉTRENNES AUX DAMES</head>
					<lg n="1">
						<l n="1" num="1.1">A mon loto, soir et matin,</l>
						<l n="2" num="1.2">Sous vos doigts un brillant destin</l>
						<l n="3" num="1.3">Portera des boules heureuses ;</l>
						<l n="4" num="1.4">Ce que j’assure, je le sais :</l>
						<l n="5" num="1.5">Si vous en êtes curieuses,</l>
						<l n="6" num="1.6">Mesdames, faites-en l’essai</l>
						<l rhyme="none" n="7" num="1.7"><space unit="char" quantity="8"></space>A mon loto.</l>
					</lg>
					<lg n="2">
						<l n="8" num="2.1">Un peu de secours fait grand bien ;</l>
						<l n="9" num="2.2">Tant soit peu d’art ne nuit à rien,</l>
						<l n="10" num="2.3">Il faut quelquefois s’en permettre ;</l>
						<l n="11" num="2.4">C’est mon avis ; on ne sauroit</l>
						<l n="12" num="2.5">Le dédaigner et se promettre</l>
						<l n="13" num="2.6">Tout l’avantage qu’on auroit</l>
						<l rhyme="none" n="14" num="2.7"><space unit="char" quantity="8"></space>A mon loto.</l>
					</lg>
					<lg n="3">
						<l n="15" num="3.1">Jamais une joueuse habile</l>
						<l n="16" num="3.2">Ne tint son sachet immobile :</l>
						<l n="17" num="3.3">Il faut l’agiter prestement.</l>
						<l n="18" num="3.4">Il faut que mollement pressée</l>
						<l n="19" num="3.5">Entre ses doigts légèrement</l>
						<l n="20" num="3.6">La boule ait été caressée,</l>
						<l rhyme="none" n="21" num="3.7"><space unit="char" quantity="8"></space>A mon loto.</l>
					</lg>
					<lg n="4">
						<l n="22" num="4.1">Selon son goût ou son talent,</l>
						<l n="23" num="4.2">On a le tirer prompt ou lent :</l>
						<l n="24" num="4.3">Il n’y faut aucune science,</l>
						<l n="25" num="4.4">Ou s’il en faut, il en faut peu ;</l>
						<l n="26" num="4.5">Un quart d’heure d’expérience</l>
						<l n="27" num="4.6">Suffit pour bien jouer le jeu,</l>
						<l rhyme="none" n="28" num="4.7"><space unit="char" quantity="8"></space>A mon loto.</l>
					</lg>
					<lg n="5">
						<l n="29" num="5.1">De celles qu’un ambe contente.</l>
						<l n="30" num="5.2">Il se plaît à tromper l’attente,</l>
						<l n="31" num="5.3">Fi de l’ambe ! il est trop commun.</l>
						<l n="32" num="5.4">D’un terne la chance est mesquine ;</l>
						<l n="33" num="5.5">D’un terne ? Oui, de deux jours l’un,</l>
						<l n="34" num="5.6">Je puis vous répondre d’un quine,</l>
						<l rhyme="none" n="35" num="5.7"><space unit="char" quantity="8"></space>A mon loto.</l>
					</lg>
					<lg n="6">
						<l n="36" num="6.1">Au quaterne, par accident,</l>
						<l n="37" num="6.2">S’il se réduit en attendant,</l>
						<l n="38" num="6.3">La perte est bientôt réparée.</l>
						<l n="39" num="6.4">Le jour qui suit ce jour fatal,</l>
						<l n="40" num="6.5">On peut compter sur la rentrée</l>
						<l n="41" num="6.6">De l’intérêt du capital,</l>
						<l rhyme="none" n="42" num="6.7"><space unit="char" quantity="8"></space>A mon loto.</l>
					</lg>
					<lg n="7">
						<l n="43" num="7.1">Mais de la superbe machine</l>
						<l n="44" num="7.2">Le pouvoir merveilleux décline</l>
						<l n="45" num="7.3">De jour en jour ; c’est son défaut.</l>
						<l n="46" num="7.4">Je vous en préviens, blonde, ou brune ;</l>
						<l n="47" num="7.5">Vous n’avez que le temps qu’il faut,</l>
						<l n="48" num="7.6">Si vous voulez faire fortune</l>
						<l rhyme="none" n="49" num="7.7"><space unit="char" quantity="8"></space>A mon loto.</l>
					</lg>
					<lg n="8">
						<l n="50" num="8.1">Ma demeure est à Vaugirard,</l>
						<l n="51" num="8.2">Tout vis-à-vis maître Abélard,</l>
						<l n="52" num="8.3">Qui montre aux enfants la musique :</l>
						<l n="53" num="8.4">L’on se pourvoit, ou l’on souscrit.</l>
						<l n="54" num="8.5">Sous mon enseigne magnifique,</l>
						<l n="55" num="8.6">En lettres d’or, il est écrit :</l>
						<l rhyme="none" n="56" num="8.7"><space unit="char" quantity="8"></space>AU GRAND LOTO.</l>
					</lg>
				</div></body></text></TEI>