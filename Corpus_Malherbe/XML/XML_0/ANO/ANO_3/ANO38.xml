<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">Le petit-neveu de Grécourt</title>
				<title type="sub_2">ou</title>
				<title type="sub_1">Étrennes gaillardes</title>
				<title type="medium">Édition électronique</title>
				<author key="ANO">
					<name type="anonymous">(anonyme)</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2164 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref> 
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">ANO_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le petit-neveu de Grécourt ou Étrennes gaillardes</title>
						<author>(anonyme)</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URI">https://www.gutenberg.org/ebooks/60896</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le petit-neveu de Grécourt ou Étrennes gaillardes</title>
								<author>(anonyme)</author>
								<idno type="URI">https://archive.org/details/lepetitneveudegr00pari/mode/2up</idno>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>Isidore LISEUX, Éditeur</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-08-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-08-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="ANO38">
					<head type="main">L’OBSTACLE</head>
					<head type="main">CONTE</head>
					<lg n="1">
						<l n="1" num="1.1"><space unit="char" quantity="8"></space>A quoi bon prodiguer les mots ?</l>
						<l n="2" num="1.2"><space unit="char" quantity="8"></space>Tous nos Conteurs, pour l’ordinaire,</l>
						<l n="3" num="1.3"><space unit="char" quantity="8"></space>S’épuisent en avant-propos ;</l>
						<l n="4" num="1.4"><space unit="char" quantity="4"></space>N’en faisons point, allons droit à l’affaire.</l>
						<l n="5" num="1.5"><space unit="char" quantity="8"></space>Un Jouvenceau taillé pour plaire,</l>
						<l n="6" num="1.6"><space unit="char" quantity="8"></space>Après avoir bien soupiré,</l>
						<l n="7" num="1.7"><space unit="char" quantity="8"></space>Menti, promis et conjuré</l>
						<l n="8" num="1.8"><space unit="char" quantity="4"></space>(C’est des amants le langage vulgaire),</l>
						<l n="9" num="1.9">Parvint près de sa belle au moment désiré :</l>
						<l n="10" num="1.10">Il touchoit à son but, quand, par triste aventure,</l>
						<l n="11" num="1.11"><space unit="char" quantity="8"></space>Sans pouvoir avancer d’un pas,</l>
						<l n="12" num="1.12"><space unit="char" quantity="4"></space>Il se démène, il souffle, il sue, il jure ;</l>
						<l n="13" num="1.13"><space unit="char" quantity="4"></space>On peut, je crois jurer en pareil cas.</l>
						<l n="14" num="1.14"><space unit="char" quantity="8"></space>Disons le fait : Dame Nature</l>
						<l n="15" num="1.15">Avoit fermé d’amour la gentille serrure,</l>
						<l n="16" num="1.16"><space unit="char" quantity="8"></space>Si bien que la clef n’entroit pas.</l>
						<l n="17" num="1.17"><space unit="char" quantity="4"></space>Certain barreau… mais on m’entend de reste ;</l>
						<l rhyme="none" n="18" num="1.18">Qu’Amour, jeunes beautés, veuille vous préserver</l>
						<l n="19" num="1.19"><space unit="char" quantity="8"></space>D’un accident aussi funeste !</l>
						<l n="20" num="1.20"><space unit="char" quantity="4"></space>Ainsi soit-il. Venons à notre Amant :</l>
						<l n="21" num="1.21">Le désir de ses sens par l’obstacle s’enflamme.</l>
						<l n="22" num="1.22">Il redouble d’efforts, mais inutilement ;</l>
						<l n="23" num="1.23">D’amour et de colère il enrage en son âme :</l>
						<l n="24" num="1.24">On peut se fourvoyer, quand on marche à tâton.</l>
						<l n="25" num="1.25"><space unit="char" quantity="4"></space>Son chalumeau, déjà baissant d’un ton,</l>
						<l n="26" num="1.26">Dans le sentier voisin… Arrêtons, et pour cause :</l>
						<l n="27" num="1.27"><space unit="char" quantity="8"></space>Car ce sentier… ma foi, je n’ose</l>
						<l n="28" num="1.28"><space unit="char" quantity="4"></space>Vous le nommer ; mais je peux, sans qu’on glose,</l>
						<l n="29" num="1.29">Dire que sa Vénus ne fut plus qu’un Giton.</l>
						<l n="30" num="1.30">A ce nouvel assaut n’étant point préparée,</l>
						<l n="31" num="1.31"><space unit="char" quantity="8"></space>En vain la belle <hi rend="ital">imperforée</hi></l>
						<l n="32" num="1.32">Lui crie : « Arrêtez donc, quel est votre dessein ?</l>
						<l n="33" num="1.33"><space unit="char" quantity="8"></space>» ‒ Rien de plus simple que la chose, »</l>
						<l n="34" num="1.34">Répond le gars ; « chez vous je trouve porte close :</l>
						<l n="35" num="1.35"><space unit="char" quantity="8"></space>» J’écris mon nom chez le voisin. »</l>
					</lg>
				</div></body></text></TEI>