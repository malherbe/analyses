<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">TABLEAUX DE LA NATURE</title>
				<title type="medium">Une édition électronique</title>
				<author key="CHA">
					<name>
						<forename>François-René</forename>
						<nameLink>de</nameLink>
						<surname>CHATEAUBRIAND</surname>
					</name>
					<date from="1768" to="1848">1768-1848</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>333 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">CHA_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Tableaux de la nature</title>
						<author>François-René de Chateaubriand</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">https://www.poesies.net/chateaubriandtableaudelanature.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition papier d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1784" to="1790">1784-1790</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface n’est pas incluse.</p>
				<p>Les notes de l’auteur ont été intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>Des corrections métriques ont été apportées sans autres attestations publiées.</p>
				<p>Les vers de la strophe illisible du poème "SERMENT — LES NOIRS DEVANT LE GIBET DE JOHN BROWN" ont été marqués comme inanalysables.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
 					<p>L’orthographe a été vérifiée avec le correcteur Aspell.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="CHA6">
				<head type="number">VI</head>
				<head type="main">Nuit d’automne</head>
				<lg n="1">
					<l n="1" num="1.1">Mais des nuits d’automne</l>
					<l n="2" num="1.2">Goûtons les douceurs ;</l>
					<l n="3" num="1.3">Qu’aux aimables fleurs</l>
					<l n="4" num="1.4">Succède Pomone.</l>
					<l n="5" num="1.5">Le pâle couchant</l>
					<l n="6" num="1.6">Brille encore à peine ;</l>
					<l n="7" num="1.7">De Vénus, qu’il mène ;</l>
					<l n="8" num="1.8">L’astre va penchant ;</l>
					<l n="9" num="1.9">La lune, emportée</l>
					<l n="10" num="1.10">Vers d’autres climats,</l>
					<l n="11" num="1.11">Ne montrera pas</l>
					<l n="12" num="1.12">Sa face argentée.</l>
					<l n="13" num="1.13">De ces peupliers,</l>
					<l n="14" num="1.14">Au bord des sentiers,</l>
					<l n="15" num="1.15">Les zéphyrs descendent,</l>
					<l n="16" num="1.16">Dans les airs s’étendent,</l>
					<l n="17" num="1.17">Effleurent les eaux,</l>
					<l n="18" num="1.18">Et de ces ormeaux</l>
					<l n="19" num="1.19">Raniment la sève :</l>
					<l n="20" num="1.20">Comme une vapeur,</l>
					<l n="21" num="1.21">La douce fraîcheur</l>
					<l n="22" num="1.22">De ces bois s’élève.</l>
					<l n="23" num="1.23">Sous ces arbres verts,</l>
					<l n="24" num="1.24">Qu’un vent frais balance,</l>
					<l n="25" num="1.25">J’entends en silence</l>
					<l n="26" num="1.26">Leurs légers concerts :</l>
					<l rhyme="none" n="27" num="1.27">Mollement bercée,</l>
					<l rhyme="none" n="28" num="1.28">La voûte pressée</l>
					<l rhyme="none" n="29" num="1.29">En dôme orgueilleux</l>
					<l n="30" num="1.30">Serre son ombrage,</l>
					<l n="31" num="1.31">Et puis s’entrouvrant,</l>
					<l n="32" num="1.32">Du ciel lentement</l>
					<l n="33" num="1.33">Découvrent l’image.</l>
					<l n="34" num="1.34">Là, des nuits l’azur</l>
					<l n="35" num="1.35">Dans un cristal pur</l>
					<l n="36" num="1.36">Déroule ses voiles.</l>
					<l n="37" num="1.37">Et le flot brillant</l>
					<l n="38" num="1.38">Coule en sommeillant</l>
					<l n="39" num="1.39">Sur un lit d’étoiles.</l>
				</lg>
				<lg n="2">
					<l n="40" num="2.1">Oh ! charme nouveau !</l>
					<l n="41" num="2.2">Le son du pipeau</l>
					<l n="42" num="2.3">Dans l’air se déploie,</l>
					<l n="43" num="2.4">Et du fond des bois</l>
					<l n="44" num="2.5">M’apporte à la fois</l>
					<l n="45" num="2.6">L’amour et la joie.</l>
					<l n="46" num="2.7">Près des ruisseaux clairs,</l>
					<l n="47" num="2.8">Au chaume d’Adèle</l>
					<l n="48" num="2.9">Le pasteur fidèle</l>
					<l n="49" num="2.10">Module ses airs.</l>
					<l n="50" num="2.11">Tantôt il soupire,</l>
					<l n="51" num="2.12">Tantôt il désire ;</l>
					<l n="52" num="2.13">Se tait : tour à tour</l>
					<l n="53" num="2.14">Sa simple cadence</l>
					<l n="54" num="2.15">Me peint son amour</l>
					<l n="55" num="2.16">Et son innocence.</l>
					<l n="56" num="2.17">Dans son lit heureux</l>
					<l n="57" num="2.18">La pauvre attentive</l>
					<l n="58" num="2.19">Écoute, pensive,</l>
					<l n="59" num="2.20">Ces sons dangereux :</l>
					<l n="60" num="2.21">Le drap qui la couvre</l>
					<l n="61" num="2.22">Loin d’elle a roulé,</l>
					<l n="62" num="2.23">Et son œil troublé</l>
					<l n="63" num="2.24">Mollement s’entrouvre.</l>
					<l n="64" num="2.25">Tout entière au bruit</l>
					<l n="65" num="2.26">Qui pendant la nuit</l>
					<l n="66" num="2.27">La charme et l’accuse,</l>
					<l n="67" num="2.28">Adèle au vainqueur</l>
					<l n="68" num="2.29">Son aveu refuse</l>
					<l n="69" num="2.30">Et donne son cœur.</l>
				</lg>
			</div></body></text></TEI>