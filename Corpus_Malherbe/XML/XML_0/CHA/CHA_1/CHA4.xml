<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">TABLEAUX DE LA NATURE</title>
				<title type="medium">Une édition électronique</title>
				<author key="CHA">
					<name>
						<forename>François-René</forename>
						<nameLink>de</nameLink>
						<surname>CHATEAUBRIAND</surname>
					</name>
					<date from="1768" to="1848">1768-1848</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>333 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">CHA_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Tableaux de la nature</title>
						<author>François-René de Chateaubriand</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">https://www.poesies.net/chateaubriandtableaudelanature.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition papier d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1784" to="1790">1784-1790</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface n’est pas incluse.</p>
				<p>Les notes de l’auteur ont été intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>Des corrections métriques ont été apportées sans autres attestations publiées.</p>
				<p>Les vers de la strophe illisible du poème "SERMENT — LES NOIRS DEVANT LE GIBET DE JOHN BROWN" ont été marqués comme inanalysables.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
 					<p>L’orthographe a été vérifiée avec le correcteur Aspell.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="CHA4">
				<head type="number">IV</head>
				<head type="main">Le Soir, dans une vallée</head>
				<lg n="1">
					<l n="1" num="1.1">Déjà le soir de sa vapeur bleuâtre</l>
					<l n="2" num="1.2">Enveloppait les champs silencieux ;</l>
					<l n="3" num="1.3">Par le nuage étaient voilés les cieux :</l>
					<l n="4" num="1.4">Je m’avançais vers la pierre grisâtre.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Du haut d’un mont une onde rugissant</l>
					<l n="6" num="2.2">S’élançait : sous de larges sycomores,</l>
					<l n="7" num="2.3">Dans ce désert d’un calme menaçant,</l>
					<l n="8" num="2.4">Roulaient des flots agités et sonores.</l>
					<l n="9" num="2.5">Le noir torrent, redoublant de vigueur,</l>
					<l n="10" num="2.6">Entrait fougueux dans la forêt obscure</l>
					<l n="11" num="2.7">De ces sapins, au port plein de langueur,</l>
					<l n="12" num="2.8">Qui, négligés comme dans la douleur,</l>
					<l n="13" num="2.9">Laissent tomber leur longue chevelure,</l>
					<l n="14" num="2.10">De branche en branche errant à l’aventure.</l>
					<l n="15" num="2.11">Se regardant dans un silence affreux,</l>
					<l n="16" num="2.12">Des rochers nus s’élevaient, ténébreux ;</l>
					<l n="17" num="2.13">Leur front aride et leurs cimes sauvages</l>
					<l n="18" num="2.14">Voyaient glisser et fumer les nuages :</l>
					<l n="19" num="2.15">Leurs longs sommets, en prisme partagés,</l>
					<l n="20" num="2.16">Étaient des eaux et des mousses rongés.</l>
					<l n="21" num="2.17">Des liserons, d’humides capillaires,</l>
					<l n="22" num="2.18">Couvraient les flancs de ces monts solitaires ;</l>
					<l n="23" num="2.19">Plus tristement des lierres encor</l>
					<l n="24" num="2.20">Se suspendaient aux rocs inaccessibles ;</l>
					<l n="25" num="2.21">Et contrasté, teint de couleurs paisibles,</l>
					<l n="26" num="2.22">Le jonc, couvert de ses papillons d’or,</l>
					<l n="27" num="2.23">Riait au vent sur des sites terribles.</l>
				</lg>
				<lg n="3">
					<l n="28" num="3.1">Mais tout s’efface, et surpris de la nuit,</l>
					<l n="29" num="3.2">Couché parmi des bruyères laineuses,</l>
					<l n="30" num="3.3">Sur le courant des ondes orageuses</l>
					<l n="31" num="3.4">Je vais pencher mon front chargé d’ennui.</l>
				</lg>
			</div></body></text></TEI>