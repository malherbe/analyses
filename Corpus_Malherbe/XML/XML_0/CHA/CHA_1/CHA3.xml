<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">TABLEAUX DE LA NATURE</title>
				<title type="medium">Une édition électronique</title>
				<author key="CHA">
					<name>
						<forename>François-René</forename>
						<nameLink>de</nameLink>
						<surname>CHATEAUBRIAND</surname>
					</name>
					<date from="1768" to="1848">1768-1848</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>333 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">CHA_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Tableaux de la nature</title>
						<author>François-René de Chateaubriand</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">https://www.poesies.net/chateaubriandtableaudelanature.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition papier d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1784" to="1790">1784-1790</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface n’est pas incluse.</p>
				<p>Les notes de l’auteur ont été intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>Des corrections métriques ont été apportées sans autres attestations publiées.</p>
				<p>Les vers de la strophe illisible du poème "SERMENT — LES NOIRS DEVANT LE GIBET DE JOHN BROWN" ont été marqués comme inanalysables.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
 					<p>L’orthographe a été vérifiée avec le correcteur Aspell.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="CHA3">
				<head type="number">III</head>
				<head type="main">Le Soir au bord de la mer</head>
				<lg n="1">
					<l n="1" num="1.1">Les bois épais, les sirtes mornes, nues,</l>
					<l n="2" num="1.2">Mêlent leurs bords dans les ombres chenues.</l>
					<l n="3" num="1.3">En scintillant dans le zénith d’azur,</l>
					<l n="4" num="1.4">On voit percer l’étoile solitaire :</l>
					<l n="5" num="1.5">A l’occident, séparé de la terre,</l>
					<l n="6" num="1.6">L’écueil blanchit sous un horizon pur,</l>
					<l n="7" num="1.7">Tandis qu’au nord, sur les mers cristallines,</l>
					<l n="8" num="1.8">Flotte la nue en vapeurs purpurines.</l>
					<l n="9" num="1.9">D’un carmin vif les monts sont dessinés ;</l>
					<l n="10" num="1.10">Du vent du soir se meurt la voix plaintive ;</l>
					<l n="11" num="1.11">Et mollement l’un à l’autre enchaînés,</l>
					<l n="12" num="1.12">Les flots calmés expirent sur la rive.</l>
				</lg>
				<lg n="2">
					<l n="13" num="2.1">Tout est grandeur, pompe, mystère, amour :</l>
					<l n="14" num="2.2">Et la nature, aux derniers feux du jour,</l>
					<l n="15" num="2.3">Avec ses monts, ses forêts magnifiques,</l>
					<l n="16" num="2.4">Son plan sublime et son ordre éternel,</l>
					<l n="17" num="2.5">S’élève ainsi qu’un temple solennel,</l>
					<l n="18" num="2.6">Resplendissant de ses beautés antiques.</l>
					<l n="19" num="2.7">Le sanctuaire où le Dieu s’introduit</l>
					<l n="20" num="2.8">Semble voilé par une sainte nuit ;</l>
					<l n="21" num="2.9">Mais dans les airs la coupole hardie,</l>
					<l n="22" num="2.10">Des arts divins, gracieuse harmonie,</l>
					<l n="23" num="2.11">Offre un contour peint des fraîches couleurs</l>
					<l n="24" num="2.12">De l’arc-en-ciel, de l’aurore et des fleurs.</l>
				</lg>
			</div></body></text></TEI>