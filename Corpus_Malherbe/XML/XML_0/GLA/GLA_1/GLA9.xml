<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Fer rouge</title>
				<title type="medium">Édition électronique</title>
				<author key="GLA">
					<name>
						<forename>Albert</forename>
						<surname>GLATIGNY</surname>
					</name>
					<date from="1839" to="1873">1839-1873</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1218 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GLA_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Fer rouge</title>
						<author>Albert Glatigny</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L928</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le Fer rouge</title>
								<author>Albert Glatigny</author>
								<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k54519653</idno>
								<imprint>
									<pubPlace>Bruxelles</pubPlace>
									<publisher>Poulet-Malassis</publisher>
									<date when="1870">1870</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1870">1870</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>Le formatage strophique a été rétabli.</p>
				<p>Les majuscules en début de vers ont été restituées.</p>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-27" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="GLA9">
				<head type="number">IX</head>
				<head type="main">WILHELMSHŒHE</head>
				<lg n="1">
					<l n="1" num="1.1">C’est un château galant, trianon germanique,</l>
					<l n="2" num="1.2">Qu’un bottier de Cassel, laissant là sa manique,</l>
					<l n="3" num="1.3">Construisit en l’honneur des grâces et des ris.</l>
					<l n="4" num="1.4">Les fourrés sont épais, les sentiers sont fleuris ;</l>
					<l n="5" num="1.5">L’ombre est douce, l’eau court dans le parc, les statues</l>
					<l n="6" num="1.6">Agacent le regard, blanches et court-vêtues.</l>
					<l n="7" num="1.7">On voit bondir parfois de beaux cerfs familiers,</l>
					<l n="8" num="1.8">Et les roses, lançant leurs parfums par milliers,</l>
					<l n="9" num="1.9">Ignorent que l’on parle allemand autour d’elles ;</l>
					<l n="10" num="1.10">Les oiseaux au soleil font un charmant bruit d’ailes.</l>
					<l n="11" num="1.11">On rêve en ce retrait, dans un frais demi-jour,</l>
					<l n="12" num="1.12">Quelque jeune margrave, aux airs de Pompadour,</l>
					<l n="13" num="1.13">Devant un grand miroir ajustant une mouche…</l>
				</lg>
				<lg n="2">
					<l n="14" num="2.1">Non ! C’est un vieux soudard, au front bas, à l’œil louche,</l>
					<l n="15" num="2.2">Qui bâille en regardant les panneaux de Lancret.</l>
					<l n="16" num="2.3">Bazile, en le voyant rire, se convaincrait</l>
					<l n="17" num="2.4">Que sa race hideuse est encor de ce monde ;</l>
					<l n="18" num="2.5">Car s’il a dépouillé la souquenille immonde</l>
					<l n="19" num="2.6">Du vieux maître de chant, pour se chamarrer d’or,</l>
					<l n="20" num="2.7">Ce cuistre n’a pas su se défroquer encor</l>
					<l n="21" num="2.8">De son masque à soufflets et de son œil atone,</l>
					<l n="22" num="2.9">Qu’un rayon de soleil ou de franchise étonne.</l>
					<l n="23" num="2.10">Ce fantoche cassé que, dans ses doigts étroits,</l>
					<l n="24" num="2.11">La démence a saisi, c’est Napoléon Trois.</l>
					<l n="25" num="2.12">La honte ne rougit pas même sa pommette.</l>
					<l n="26" num="2.13">Il mange. Il est heureux pourvu qu’on lui permette</l>
					<l n="27" num="2.14">De s’habiller en chien savant. Il est d’un sang</l>
					<l n="28" num="2.15">Où l’on aime à l’excès les plaques de fer-blanc.</l>
					<l n="29" num="2.16">Que de croix ! Il en a jusques aux jarretières !</l>
					<l n="30" num="2.17">Et pour jouer avec met des heures entières.</l>
					<l n="31" num="2.18">Il a pour ces hochets un sourire enfantin :</l>
					<l n="32" num="2.19">On les met dans sa couche, afin que le matin</l>
					<l part="I" n="33" num="2.20">Il ne pleure pas trop. </l>
					<l part="F" n="33" num="2.20">Il rumine, il digère ;</l>
					<l n="34" num="2.21">Sa conscience est nulle, et son âme est légère ;</l>
					<l n="35" num="2.22">Et cependant, le vent dans les arbres, le soir,</l>
					<l n="36" num="2.23">Gémit lugubrement, et l’homme pourrait voir</l>
					<l n="37" num="2.24">Les morts de Wissembourg et les morts de décembre</l>
					<l n="38" num="2.25">Coller leur face pâle aux vitres de la chambre…</l>
					<l n="39" num="2.26">Il en vient de Cayenne, il en vient des pontons,</l>
					<l n="40" num="2.27">Soldats, proscrits, martyrs, ceux dont nous racontons</l>
					<l n="41" num="2.28">Dans les veilles d’hiver l’histoire épouvantable…</l>
					<l n="42" num="2.29">Alors il se réveille, il s’accroche à la table,</l>
					<l n="43" num="2.30">Et tremblant, effaré, s’écrie avec stupeur :</l>
					<l n="44" num="2.31">« Apportez des flambeaux ! Piétri ! Piétri ! J’ai peur ! »</l>
				</lg>
				<closer>
					<dateline> 5 octobre.</dateline>
				</closer>
			</div></body></text></TEI>