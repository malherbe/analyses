<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Fer rouge</title>
				<title type="medium">Édition électronique</title>
				<author key="GLA">
					<name>
						<forename>Albert</forename>
						<surname>GLATIGNY</surname>
					</name>
					<date from="1839" to="1873">1839-1873</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1218 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GLA_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Fer rouge</title>
						<author>Albert Glatigny</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L928</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le Fer rouge</title>
								<author>Albert Glatigny</author>
								<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k54519653</idno>
								<imprint>
									<pubPlace>Bruxelles</pubPlace>
									<publisher>Poulet-Malassis</publisher>
									<date when="1870">1870</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1870">1870</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>Le formatage strophique a été rétabli.</p>
				<p>Les majuscules en début de vers ont été restituées.</p>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-27" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="GLA20">
				<head type="number">XX</head>
				<head type="main">HÉROS DE L’EMPIRE</head>
				<lg n="1">
					<l n="1" num="1.1">À Lahire, à Turenne, à Villars, à Marceau,</l>
					<l n="2" num="1.2">À ces vaillants de France, héroïque faisceau</l>
					<l n="3" num="1.3">De cœurs purs, de bras fort et de natures fières,</l>
					<l n="4" num="1.4">À tous ces fronts baignés d’éclatantes lumières,</l>
					<l n="5" num="1.5">À tous ces preux sans peur que la patrie en deuil</l>
					<l n="6" num="1.6">Montre encore à l’Europe avec un mâle orgueil,</l>
					<l n="7" num="1.7">À Bayard, à Kléber, à toute cette gloire</l>
					<l n="8" num="1.8">Dont les rayons divins éblouissent l’histoire,</l>
					<l n="9" num="1.9">L’empire maintenant oppose ses héros.</l>
					<l n="10" num="1.10">Comme la République, il a des généraux</l>
					<l n="11" num="1.11">Dignes de lui, roulant du grotesque à l’obscène :</l>
					<l n="12" num="1.12">Mac-Mahon à Sedan venant jouer la scène</l>
					<l n="13" num="1.13">Où Scapin de Géronte escroque le pardon ;</l>
					<l n="14" num="1.14">Leboeuf, ce matamore épique, ce dindon</l>
					<l n="15" num="1.15">Qui glousse et fait la roue au bord d’une tinette ;</l>
					<l n="16" num="1.16">Failly, le bien nommé ; Fleury, le proxénète ;</l>
					<l n="17" num="1.17">Canrobert, ce bandit au langage poissard ;</l>
					<l n="18" num="1.18">Le pion du petit Bonaparte, Frossard ;</l>
					<l n="19" num="1.19">Boyer… que sais-je encor ? J’en passe, et des plus sales,</l>
					<l n="20" num="1.20">Pour qui Toulon trop plein manque de succursales.</l>
					<l n="21" num="1.21">Mais l’orgueil de ce règne incroyable, celui</l>
					<l n="22" num="1.22">Qui résume en lui seul les hontes d’aujourd’hui,</l>
					<l n="23" num="1.23">Auprès de qui, sortant radieux de sa fange,</l>
					<l n="24" num="1.24">Palikao produit presque l’effet d’un ange,</l>
					<l n="25" num="1.25">C’est Bazaine ! Ce nom peut-il s’écrire encor ?</l>
					<l n="26" num="1.26">Ô dieux ! De quel égout ce gueux chamarré d’or</l>
					<l n="27" num="1.27">Est-il sorti ? Dis-nous, France qu’il a trahie,</l>
					<l n="28" num="1.28">Sur ta terre sacrée à cette heure envahie,</l>
					<l n="29" num="1.29">Oh ! Dis-nous quelle chienne errante l’a mis bas ?</l>
					<l n="30" num="1.30">Et tu croyais en lui ! Tu lui tendais les bras,</l>
					<l n="31" num="1.31">Tu lui confiais Metz, la joyeuse pucelle,</l>
					<l n="32" num="1.32">Dont le regard hardi conservait l’étincelle</l>
					<l n="33" num="1.33">Qu’on vit jaillir des yeux en feu du balafré !</l>
					<l n="34" num="1.34">Ô passé glorieux en un jour engouffré !</l>
				</lg>
				<lg n="2">
					<l n="35" num="2.1">Dans le bagne historique on voyait, comme une ombre,</l>
					<l n="36" num="2.2">Errer déjà Bourbon, le connétable sombre,</l>
					<l n="37" num="2.3">Derrière qui, honteux, s’effondraient les palais</l>
					<l n="38" num="2.4">Où ce lâche, plus vil que les derniers valets,</l>
					<l n="39" num="2.5">Avait, rien qu’une nuit, posé sa tête infâme.</l>
					<l n="40" num="2.6">Dans le cercle entouré d’une infernale flamme,</l>
					<l n="41" num="2.7">Où se tordent tous ceux qui portent pour blason</l>
					<l n="42" num="2.8">La pancarte où le juge écrivit : « Trahison, »</l>
					<l n="43" num="2.9">Bourbon a maintenant un compagnon de chaîne :</l>
					<l n="44" num="2.10">Il s’endort, chaque soir, à côté de Bazaine.</l>
					<l n="45" num="2.11">C’est le même argousin qui les mène au travail.</l>
					<l n="46" num="2.12">Et là-bas, — effrayant, lugubre épouvantail, —</l>
					<l n="47" num="2.13">Maximilien mort vient à pas lents, écarte</l>
					<l n="48" num="2.14">Le suaire sanglant, et montre à Bonaparte</l>
					<l n="49" num="2.15">Livide de terreur, stupide et déjà vert,</l>
					<l n="50" num="2.16">Sa poitrine trouée et son crâne entr’ouvert !</l>
				</lg>
				<closer>
					<dateline> 5 novembre.</dateline>
				</closer>
			</div></body></text></TEI>