<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Fer rouge</title>
				<title type="medium">Édition électronique</title>
				<author key="GLA">
					<name>
						<forename>Albert</forename>
						<surname>GLATIGNY</surname>
					</name>
					<date from="1839" to="1873">1839-1873</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1218 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GLA_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Fer rouge</title>
						<author>Albert Glatigny</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L928</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le Fer rouge</title>
								<author>Albert Glatigny</author>
								<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k54519653</idno>
								<imprint>
									<pubPlace>Bruxelles</pubPlace>
									<publisher>Poulet-Malassis</publisher>
									<date when="1870">1870</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1870">1870</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>Le formatage strophique a été rétabli.</p>
				<p>Les majuscules en début de vers ont été restituées.</p>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-27" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="GLA10">
				<head type="number">X</head>
				<head type="main">DANS CES MURS OÙ L’ÉCHO</head>
				<lg n="1">
					<l n="1" num="1.1">Dans ces murs où l’écho répète les hoquets</l>
					<l n="2" num="1.2">De son oncle Jérôme, au milieu des laquais</l>
					<l n="3" num="1.3">Qui lui disent encor : « sire, » le Bonaparte,</l>
					<l n="4" num="1.4">Par instants, fixe un œil abruti sur la carte.</l>
					<l n="5" num="1.5">Il voit les prussiens avancer sur Paris ;</l>
					<l n="6" num="1.6">Il ricane. Un exprès annonce qu’on a pris</l>
					<l n="7" num="1.7">Un village et brûlé dix maisons ; il jubile.</l>
					<l n="8" num="1.8">Laid, vomissant sa joie avec un flot de bile,</l>
					<l n="9" num="1.9">Il dit : « tant mieux ! Ce peuple était trop arrogant. »</l>
					<l n="10" num="1.10">Chaque plaie à ton sein fait rire ce brigand,</l>
					<l n="11" num="1.11">Mère adorée. Il veut se laver de sa honte</l>
					<l n="12" num="1.12">Dans ton sang. Chaque jour, ce lâche fait le compte</l>
					<l n="13" num="1.13">De tes blessures : « tiens ! Encore celle là,</l>
					<l n="14" num="1.14">Et je vais revêtir mon habit de gala,</l>
					<l part="I" n="15" num="1.15">Et je te châtîrai, France maudite ! » </l>
					<l part="F" n="15" num="1.15">Ô dogue</l>
					<l n="16" num="1.16">Édenté ! Corps pourri qui n’es plus qu’une drogue,</l>
					<l n="17" num="1.17">Tu crois donc revenir avec tes prussiens ?</l>
					<l n="18" num="1.18">Ne le souhaite pas. Si jamais tu reviens,</l>
					<l n="19" num="1.19">Tu reviendras ainsi que Tropmann : la moustache</l>
					<l n="20" num="1.20">Rasée, un bonnet vert sur la tête, à l’attache,</l>
					<l n="21" num="1.21">Et Toulon t’ouvrira, pour passer tes hivers,</l>
					<l n="22" num="1.22">Le cabanon de Joze ou bien de Lathauwers !</l>
				</lg>
				<closer>
					<dateline> 4 octobre.</dateline>
				</closer>
			</div></body></text></TEI>