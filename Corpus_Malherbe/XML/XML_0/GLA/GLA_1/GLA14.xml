<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Fer rouge</title>
				<title type="medium">Édition électronique</title>
				<author key="GLA">
					<name>
						<forename>Albert</forename>
						<surname>GLATIGNY</surname>
					</name>
					<date from="1839" to="1873">1839-1873</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1218 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GLA_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Fer rouge</title>
						<author>Albert Glatigny</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L928</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le Fer rouge</title>
								<author>Albert Glatigny</author>
								<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k54519653</idno>
								<imprint>
									<pubPlace>Bruxelles</pubPlace>
									<publisher>Poulet-Malassis</publisher>
									<date when="1870">1870</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1870">1870</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>Le formatage strophique a été rétabli.</p>
				<p>Les majuscules en début de vers ont été restituées.</p>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-27" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="GLA14">
				<head type="number">XIV</head>
				<lg n="1">
					<l n="1" num="1.1">« Ils osent résister ! « disent-ils. Nous osons.</l>
					<l n="2" num="1.2">Parce qu’ils ont chez eux l’homme des trahisons,</l>
					<l n="3" num="1.3">Ils pensent nous trouver abattus. Bêtes brutes !</l>
					<l n="4" num="1.4">Mais ce coup décisif et fatal que vous crûtes</l>
					<l n="5" num="1.5">Nous porter, s’est tourné contre vous justement.</l>
					<l n="6" num="1.6">Décapité d’un roi, le bon peuple allemand</l>
					<l n="7" num="1.7">Peut se croire perdu, crier miséricorde,</l>
					<l n="8" num="1.8">Et tendre un cou docile et soumis à la corde.</l>
					<l n="9" num="1.9">Ils ne comprennent pas que la rébellion</l>
					<l n="10" num="1.10">Souffle en chaque poitrine une âme de lion ;</l>
					<l n="11" num="1.11">Ils ne peuvent fourrer dans leur cervelle obtuse</l>
					<l n="12" num="1.12">Qu’on puisse mettre en jeu ces grands moyens dont use</l>
					<l n="13" num="1.13">Un peuple qui défend ses droits et son foyer ;</l>
					<l n="14" num="1.14">Ils s’étonnent de voir notre main déployer</l>
					<l n="15" num="1.15">L’étendard rayonnant des libertés publiques !</l>
					<l n="16" num="1.16">Ce sont de braves gens froids et mélancoliques</l>
					<l n="17" num="1.17">Qu’un regard du roi change en féroces toutous.</l>
					<l n="18" num="1.18">Ah ! Nous vous renverrons dans votre chenil, tous,</l>
					<l n="19" num="1.19">Les badois, ces croupiers, les gens de la Bavière,</l>
					<l n="20" num="1.20">Les prussiens tendant l’échine à l’étrivière,</l>
					<l n="21" num="1.21">Et, si cela vous plaît, vous ferez un grand-duc</l>
					<l n="22" num="1.22">De ce Napoléon sanguinaire et caduc,</l>
					<l n="23" num="1.23">Tout prêt à déposer sa risible épaulette</l>
					<l n="24" num="1.24">Pour tenir le râteau teuton de la roulette !</l>
				</lg>
				<closer>
					<dateline> 7 octobre.</dateline>
				</closer>
			</div></body></text></TEI>