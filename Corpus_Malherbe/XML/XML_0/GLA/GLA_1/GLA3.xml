<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Fer rouge</title>
				<title type="medium">Édition électronique</title>
				<author key="GLA">
					<name>
						<forename>Albert</forename>
						<surname>GLATIGNY</surname>
					</name>
					<date from="1839" to="1873">1839-1873</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1218 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GLA_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Fer rouge</title>
						<author>Albert Glatigny</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L928</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le Fer rouge</title>
								<author>Albert Glatigny</author>
								<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k54519653</idno>
								<imprint>
									<pubPlace>Bruxelles</pubPlace>
									<publisher>Poulet-Malassis</publisher>
									<date when="1870">1870</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1870">1870</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>Le formatage strophique a été rétabli.</p>
				<p>Les majuscules en début de vers ont été restituées.</p>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-27" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="GLA3">
				<head type="number">III</head>
				<head type="main">CHANT DE LA GAZETTE DE COLOGNE</head>
				<lg n="1">
					<l n="1" num="1.1">Nos pères ont eu cette honte</l>
					<l n="2" num="1.2">De connaître la liberté ;</l>
					<l n="3" num="1.3">Ils étaient ceux que rien ne dompte,</l>
					<l n="4" num="1.4">Ils bravaient l’éclair irrité.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Les miasmes venus de France</l>
					<l n="6" num="2.2">Avaient empoisonné leurs cœurs ;</l>
					<l n="7" num="2.3">On lisait : <hi rend="ital"> paix et délivrance </hi></l>
					<l n="8" num="2.4">Sur leurs jeunes drapeaux vainqueurs.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Leur rire semblait un tonnerre</l>
					<l n="10" num="3.2">Et, comme les feuilles des bois,</l>
					<l n="11" num="3.3">Balayait tout ce qu’on vénère,</l>
					<l n="12" num="3.4">Les princes, les ducs et les rois,</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">Et rien n’était affligeant comme</l>
					<l n="14" num="4.2">Leur orgueil téméraire et vain :</l>
					<l n="15" num="4.3">Ils proclamaient les droits de l’homme</l>
					<l n="16" num="4.4">Supérieurs au droit divin !</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1">Ils osaient dire qu’une altesse</l>
					<l n="18" num="5.2">Diffère du premier venu</l>
					<l n="19" num="5.3">Par un peu de scélératesse</l>
					<l n="20" num="5.4">Et d’aveuglement ingénu !</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1">Mais nous, leurs fils, c’est autre chose !</l>
					<l n="22" num="6.2">Nous sommes de bons chiens couchants ;</l>
					<l n="23" num="6.3">Nous voulons qu’un roi nous impose</l>
					<l n="24" num="6.4">Ses soins paternels et touchants.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1">Nous sommes des sujets d’élite,</l>
					<l n="26" num="7.2">Nous allons, fiers, le front baissé,</l>
					<l n="27" num="7.3">Notre zèle réhabilite</l>
					<l n="28" num="7.4">Aux yeux du maître le passé.</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1">Nous étions allemands, nous sommes</l>
					<l n="30" num="8.2">De bons prussiens ; nous portons</l>
					<l n="31" num="8.3">Notre hommage à des gentilshommes</l>
					<l n="32" num="8.4">Dont les mains tiennent des bâtons.</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1">Rien aujourd’hui ne nous divise.</l>
					<l n="34" num="9.2">Nous sommes heureux, hosanna !</l>
					<l n="35" num="9.3">Et nous avons pris pour devise :</l>
					<l n="36" num="9.4"><hi rend="ital"> Johann Maria Farina ! </hi></l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1">Oh ! Cologne est la ville sainte</l>
					<l n="38" num="10.2">De la choucroute et du tabac ;</l>
					<l n="39" num="10.3">Le vieux Rhin baigne son enceinte,</l>
					<l n="40" num="10.4">Nous revendiquons Offenbach !</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1">Nous exportons de la morale,</l>
					<l n="42" num="11.2">De la peinture, des vieux suifs ;</l>
					<l n="43" num="11.3">Nous avons une cathédrale</l>
					<l n="44" num="11.4">Que nous exploitons en vrais juifs.</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1">Aussi, quand un peuple se lève</l>
					<l n="46" num="12.2">Et réclame ses libertés,</l>
					<l n="47" num="12.3">En voyant l’éclair de son glaive,</l>
					<l n="48" num="12.4">Nous nous sentons tous insultés.</l>
				</lg>
				<lg n="13">
					<l n="49" num="13.1">Dociles comme une machine,</l>
					<l n="50" num="13.2">Prêts à supporter tous les bâts,</l>
					<l n="51" num="13.3">Quand nous plions si bien l’échine,</l>
					<l n="52" num="13.4">Voici qu’on est brave là-bas !</l>
				</lg>
				<lg n="14">
					<l n="53" num="14.1">Trouvant que le droit humain chôme,</l>
					<l n="54" num="14.2">Voici que la France, en fureur,</l>
					<l n="55" num="14.3">Quand nous gardons notre Guillaume,</l>
					<l n="56" num="14.4">Vient de vomir son empereur.</l>
				</lg>
				<lg n="15">
					<l n="57" num="15.1">Un peuple libre sur la carte !</l>
					<l n="58" num="15.2">Un souverain sur le pavé !</l>
					<l n="59" num="15.3">Oh ! Relevons ce Bonaparte,</l>
					<l n="60" num="15.4">Bien qu’il soit de sang mal prouvé.</l>
				</lg>
				<lg n="16">
					<l n="61" num="16.1">Car un prince est bien lamentable</l>
					<l n="62" num="16.2">Lorsque des parchemins joyeux</l>
					<l n="63" num="16.3">N’offrent pas un tas respectable</l>
					<l n="64" num="16.4">De bandits parmi ses aïeux ;</l>
				</lg>
				<lg n="17">
					<l n="65" num="17.1">Lorsque sa généalogie,</l>
					<l n="66" num="17.2">Superbe, n’a pas traversé</l>
					<l n="67" num="17.3">Les siècles disparus, rougie</l>
					<l n="68" num="17.4">Du sang sur l’échafaud versé.</l>
				</lg>
				<lg n="18">
					<l n="69" num="18.1">Toute maison de bonne souche</l>
					<l n="70" num="18.2">A son histoire où le poison</l>
					<l n="71" num="18.3">Joue un rôle sombre et farouche</l>
					<l n="72" num="18.4">Dans les mains de la trahison,</l>
				</lg>
				<lg n="19">
					<l n="73" num="19.1">Et la noblesse n’est sincère</l>
					<l n="74" num="19.2">Qu’autant qu’on dit comment advint,</l>
					<l n="75" num="19.3">Qu’un jour, égrenant son rosaire,</l>
					<l n="76" num="19.4">Vers l’an douze ou treize cent vingt,</l>
				</lg>
				<lg n="20">
					<l n="77" num="20.1">La noble dame châtelaine,</l>
					<l n="78" num="20.2">Son époux allant guerroyer,</l>
					<l n="79" num="20.3">Mêla chastement son haleine</l>
					<l n="80" num="20.4">Au souffle d’un jeune écuyer.</l>
				</lg>
				<lg n="21">
					<l n="81" num="21.1">Nos princes, Dieu les accompagne</l>
					<l n="82" num="21.2">Et les conduise par la main !</l>
					<l n="83" num="21.3">Déjà du temps de Charlemagne,</l>
					<l n="84" num="21.4">Étaient voleurs de grand chemin,</l>
				</lg>
				<lg n="22">
					<l n="85" num="22.1">Et, grâce au ciel ! Les adultères,</l>
					<l n="86" num="22.2">Les faux, les empoisonnements</l>
					<l n="87" num="22.3">Projettent des lueurs austères</l>
					<l n="88" num="22.4">Jusque sur leurs commencements.</l>
				</lg>
				<lg n="23">
					<l n="89" num="23.1">Mais au bout du compte, un roi, même</l>
					<l n="90" num="23.2">Sans meurtrier antique au bout</l>
					<l n="91" num="23.3">D’un passé ténébreux et blême,</l>
					<l n="92" num="23.4">Vaut mieux que pas de roi du tout ;</l>
				</lg>
				<lg n="24">
					<l n="93" num="24.1">Et puis, s’il faut qu’on se départe</l>
					<l n="94" num="24.2">De la saine tradition,</l>
					<l n="95" num="24.3">Bien que récents, les Bonaparte</l>
					<l n="96" num="24.4">Méritent quelque attention ;</l>
				</lg>
				<lg n="25">
					<l n="97" num="25.1">Dix-huit brumaire et deux décembre,</l>
					<l n="98" num="25.2">Double date, double sommet</l>
					<l n="99" num="25.3">Au haut duquel la mort se cambre !</l>
					<l n="100" num="25.4">C’est une race qui promet.</l>
				</lg>
				<lg n="26">
					<l n="101" num="26.1">Ettenheim sent son moyen âge ;</l>
					<l n="102" num="26.2">Hoche brusquement expirant</l>
					<l n="103" num="26.3">Rehausse encor le personnage</l>
					<l n="104" num="26.4">Nommé Napoléon Le Grand.</l>
				</lg>
				<lg n="27">
					<l n="105" num="27.1">Règne donc la famille corse</l>
					<l n="106" num="27.2">Au bec sanglant et carnassier !</l>
					<l n="107" num="27.3">Qu’elle-même allume l’amorce</l>
					<l n="108" num="27.4">Des sinistres canons d’acier !</l>
				</lg>
				<lg n="28">
					<l n="109" num="28.1">Car nous qu’on outrage et qu’on lie,</l>
					<l n="110" num="28.2">Nous qui voulons des majestés,</l>
					<l n="111" num="28.3">Vraiment cela nous humilie</l>
					<l n="112" num="28.4">Que l’on soit libre à nos côtés.</l>
				</lg>
				<lg n="29">
					<l n="113" num="29.1">Car notre abjection profonde</l>
					<l n="114" num="29.2">Pâlirait nécessairement</l>
					<l n="115" num="29.3">Lorsque s’étendrait sur le monde</l>
					<l n="116" num="29.4">L’universel abaissement ;</l>
				</lg>
				<lg n="30">
					<l n="117" num="30.1">Quand les peuples, comme à Cologne,</l>
					<l n="118" num="30.2">Chérissant les affronts soufferts,</l>
					<l n="119" num="30.3">S’écrîraient partout sans vergogne :</l>
					<l n="120" num="30.4">« De l’argent ! Des bâillons ! Des fers ! »</l>
				</lg>
				<lg n="31">
					<l n="121" num="31.1">Ô bons marchands de vulnéraire,</l>
					<l n="122" num="31.2">Soyez infâmes ! Vautrez-vous</l>
					<l n="123" num="31.3">Toujours dans l’ombre funéraire</l>
					<l n="124" num="31.4">De vos rois mystiques et fous !</l>
				</lg>
				<lg n="32">
					<l n="125" num="32.1">Aimez la main qui vous fustige,</l>
					<l n="126" num="32.2">Léchez les pieds les plus fangeux,</l>
					<l n="127" num="32.3">Soyez lâches jusqu’au vertige,</l>
					<l n="128" num="32.4">Valets soumis et nuageux !</l>
				</lg>
				<lg n="33">
					<l n="129" num="33.1">La révolution sacrée</l>
					<l n="130" num="33.2">Jette à la face des bourreaux</l>
					<l n="131" num="33.3">Les trônes brisés, elle crée</l>
					<l n="132" num="33.4">Un peuple de jeunes héros ;</l>
				</lg>
				<lg n="34">
					<l n="133" num="34.1">Et vous assisterez, farouches,</l>
					<l n="134" num="34.2">Au grand réveil des nations,</l>
					<l n="135" num="34.3">Et l’on blessera vos yeux louches</l>
					<l n="136" num="34.4">Avec des flèches de rayons.</l>
				</lg>
				<lg n="35">
					<l n="137" num="35.1">Vous serez contraints de vous taire</l>
					<l n="138" num="35.2">Quand, dans le jour, dans la clarté,</l>
					<l n="139" num="35.3">Nous ferons entendre à la terre</l>
					<l n="140" num="35.4">Le cantique de liberté !</l>
				</lg>
				<lg n="36">
					<l n="141" num="36.1">Alors, maudissant vos entraves,</l>
					<l n="142" num="36.2">Trahis par vos tyrans, meurtris,</l>
					<l n="143" num="36.3">Vous nous tendrez vos mains d’esclaves,</l>
					<l n="144" num="36.4">Et vous pousserez de grands cris ;</l>
				</lg>
				<lg n="37">
					<l n="145" num="37.1">Et la république sereine</l>
					<l n="146" num="37.2">Répondra de sa forte voix :</l>
					<l n="147" num="37.3">« Laquais, dans la nuit souterraine,</l>
					<l n="148" num="37.4">Allez pourrir avec vos rois ! »</l>
				</lg>
				<closer>
					<dateline> 12 septembre.</dateline>
				</closer>
			</div></body></text></TEI>