<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Fer rouge</title>
				<title type="medium">Édition électronique</title>
				<author key="GLA">
					<name>
						<forename>Albert</forename>
						<surname>GLATIGNY</surname>
					</name>
					<date from="1839" to="1873">1839-1873</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1218 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GLA_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Fer rouge</title>
						<author>Albert Glatigny</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L928</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le Fer rouge</title>
								<author>Albert Glatigny</author>
								<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k54519653</idno>
								<imprint>
									<pubPlace>Bruxelles</pubPlace>
									<publisher>Poulet-Malassis</publisher>
									<date when="1870">1870</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1870">1870</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>Le formatage strophique a été rétabli.</p>
				<p>Les majuscules en début de vers ont été restituées.</p>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-27" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="GLA17">
				<head type="number">XVII</head>
				<head type="main">À L’ÎLE DE JERSEY</head>
				<lg n="1">
					<l n="1" num="1.1">Île charmante et douce, ô Jersey ! Qu’en dis-tu ?</l>
					<l n="2" num="1.2">Voilà vingt ans, ton port par les vagues battu,</l>
					<l n="3" num="1.3">Accueillait des proscrits qui t’arrivaient de France.</l>
					<l n="4" num="1.4">Leurs fronts plissés mais non courbés par la souffrance,</l>
					<l n="5" num="1.5">Convenaient aux soldats de notre liberté.</l>
					<l n="6" num="1.6">Ils ne t’abusaient pas. Leur fière pauvreté</l>
					<l n="7" num="1.7">Demandait au travail de payer ton asile.</l>
					<l n="8" num="1.8">À ces vaillants, la vie ardue et difficile,</l>
					<l n="9" num="1.9">La lutte sous le ciel et l’exemple donné !</l>
					<l n="10" num="1.10">Beaucoup sont morts avant, hélas ! Que n’ait sonné</l>
					<l n="11" num="1.11">L’heure de la justice au cadran redoutable.</l>
				</lg>
				<lg n="2">
					<l n="12" num="2.1">Tels qu’un troupeau de bœufs ayant perdu l’étable,</l>
					<l n="13" num="2.2">Voici d’autres proscrits maintenant, mais ceux-là,</l>
					<l n="14" num="2.3">Hormis leur lâcheté, rien ne les exila ;</l>
					<l n="15" num="2.4">Ceux-là sont des fuyards, des filous, des escarpes,</l>
					<l n="16" num="2.5">Des maires dont la boue a souillé les écharpes,</l>
					<l n="17" num="2.6">De vils banqueroutiers esquivés nuitamment,</l>
					<l n="18" num="2.7">Que l’extradition réclame. Sottement,</l>
					<l n="19" num="2.8">Ils se sont dénoncés eux-mêmes par leur fuite.</l>
					<l n="20" num="2.9">Ce sont les proscripteurs d’autrefois et leur suite !</l>
				</lg>
				<lg n="3">
					<l n="21" num="3.1">Ô Jersey ! N’est-ce pas fait pour te bafouer ?</l>
					<l n="22" num="3.2">Où vinrent des martyrs, voir débarquer Rouher ;</l>
					<l n="23" num="3.3">Après Victor Hugo divinisant ta roche,</l>
					<l n="24" num="3.4">Voir pousser le bedon risible de Baroche ;</l>
					<l n="25" num="3.5">Voir Lebœuf, voir Drouin De Lhuys, voir ces valets</l>
					<l n="26" num="3.6">Dont pas un n’a payé même les faux mollets</l>
					<l n="27" num="3.7">Qu’il mettait pour aller aux bals des tuileries !</l>
					<l n="28" num="3.8">Sans doute, composant leurs mines attendries,</l>
					<l n="29" num="3.9">Ils te diront qu’ils sont ruinés, malheureux,</l>
					<l n="30" num="3.10">Que l’échafaud était déjà dressé pour eux,</l>
					<l n="31" num="3.11">Et qu’on les poursuivait, et qu’on voulait leur tête !</l>
					<l n="32" num="3.12">Ne les écoute pas. Ils mentent. La tempête</l>
					<l n="33" num="3.13">Peut-elle s’attaquer à de pareils goujats ?</l>
					<l n="34" num="3.14">C’est par le mépris seul que tu les replongeas,</l>
					<l n="35" num="3.15">Ô France ! Dans leur ombre et leurs louches ténèbres.</l>
					<l n="36" num="3.16">Tacher tes mains du sang de ces gredins funèbres,</l>
					<l n="37" num="3.17">Lever le fer des lois pour abattre un goret,</l>
					<l n="38" num="3.18">La tête de Baroche à prix ! On en rirait.</l>
					<l n="39" num="3.19">Pourquoi tuer Baroche, ô dieux ! La guillotine</l>
					<l n="40" num="3.20">Hésiterait devant ce tas de gélatine</l>
					<l n="41" num="3.21">Que Napoléon Trois avait fait sénateur !</l>
					<l n="42" num="3.22">Mais ces gâteux tombés de toute la hauteur</l>
					<l n="43" num="3.23">De leur chaise-percée, ont l’amour-propre encore</l>
					<l n="44" num="3.24">D’être furieux si le peuple les ignore,</l>
					<l n="45" num="3.25">Et tout en se tenant prudemment à l’abri,</l>
					<l n="46" num="3.26">Ils aimeraient à voir l’horizon assombri</l>
					<l n="47" num="3.27">Les menacer, de loin, d’un tout petit nuage</l>
					<l n="48" num="3.28">Modestement chargé de foudres de louage ;</l>
					<l n="49" num="3.29">Cela les poserait auprès de leur portier ;</l>
					<l n="50" num="3.30">« Oh ! Pauvres gens ! « diraient les bonnes du quartier.</l>
					<l n="51" num="3.31">Non, le silence froid et digne des commères</l>
					<l n="52" num="3.32">Doit faire évanouir ces frivoles chimères.</l>
					<l n="53" num="3.33">On ne les plaindra pas ces sacs à millions,</l>
					<l n="54" num="3.34">Ajaxs en qui Daumier voit des tabellions</l>
					<l n="55" num="3.35">De village, échappés vivants des funambules,</l>
					<l n="56" num="3.36">Proscrits dont la patrie était les vestibules</l>
					<l n="57" num="3.37">De la chambre à coucher du héros de Sedan ;</l>
					<l n="58" num="3.38">Ils doivent tous aller où les neiges d’antan,</l>
					<l n="59" num="3.39">Où les pantins vidés, l’eau sale des cuisines !</l>
				</lg>
				<lg n="4">
					<l n="60" num="4.1">Mais pourtant, ô Jersey ! Si poussant des racines</l>
					<l n="61" num="4.2">Dans ton sol généreux, ces hideux champignons</l>
					<l n="62" num="4.3">Osent parler d’honneur et font les compagnons,</l>
					<l n="63" num="4.4">Conduis-les brusquement devant le cimetière</l>
					<l n="64" num="4.5">Où ceux qui n’ont jamais courbé leur tête altière</l>
					<l n="65" num="4.6">Sont morts pour la justice et pour le droit sacré ;</l>
					<l n="66" num="4.7">Entr’ouvre un seul instant le tombeau vénéré,</l>
					<l n="67" num="4.8">Et poussant de la main ces ombres solennelles,</l>
					<l n="68" num="4.9">Confronte-les avec tous ces polichinelles !</l>
				</lg>
				<closer>
					<dateline> 10 octobre.</dateline>
				</closer>
			</div></body></text></TEI>