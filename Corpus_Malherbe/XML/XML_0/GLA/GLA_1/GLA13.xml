<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Fer rouge</title>
				<title type="medium">Édition électronique</title>
				<author key="GLA">
					<name>
						<forename>Albert</forename>
						<surname>GLATIGNY</surname>
					</name>
					<date from="1839" to="1873">1839-1873</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1218 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GLA_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Fer rouge</title>
						<author>Albert Glatigny</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L928</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le Fer rouge</title>
								<author>Albert Glatigny</author>
								<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k54519653</idno>
								<imprint>
									<pubPlace>Bruxelles</pubPlace>
									<publisher>Poulet-Malassis</publisher>
									<date when="1870">1870</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1870">1870</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>Le formatage strophique a été rétabli.</p>
				<p>Les majuscules en début de vers ont été restituées.</p>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-27" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="GLA13">
				<head type="number">XIII</head>
				<head type="main">GUITARE</head>
				<lg n="1">
					<l n="1" num="1.1">La douce Isabelle d’Espagne</l>
					<l n="2" num="1.2">Songeait dans son appartement,</l>
					<l n="3" num="1.3">Patrocinio, sa compagne,</l>
					<l n="4" num="1.4">Priait le ciel dévotement,</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Quand on vint lui dire : « l’Empire</l>
					<l n="6" num="2.2">De votre ami Napoléon,</l>
					<l n="7" num="2.3">À l’heure où nous parlons, expire,</l>
					<l n="8" num="2.4">Reine de Castille et Léon. »</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Puis on lui conta la déroute</l>
					<l n="10" num="3.2">De Sedan, cette lâcheté</l>
					<l n="11" num="3.3">De l’homme qui fit banqueroute</l>
					<l n="12" num="3.4">À l’honneur, à la dignité ;</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">Comment, pour sauver ses charrettes,</l>
					<l n="14" num="4.2">Son or, ses bagages errants,</l>
					<l n="15" num="4.3">Ce beau fumeur de cigarettes</l>
					<l n="16" num="4.4">S’était écrié : « je me rends ! »</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1">Ce n’est pas un cœur de romaine</l>
					<l n="18" num="5.2">Acceptant les coups les plus lourds,</l>
					<l n="19" num="5.3">Certes, qu’Isabelle promène</l>
					<l n="20" num="5.4">Sous un corsage de velours,</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1">Mais si peu que soit une femme,</l>
					<l n="22" num="6.2">Elle peut encore juger</l>
					<l n="23" num="6.3">Avec mépris l’amant infâme</l>
					<l n="24" num="6.4">De Marguerite Bellanger.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1">Aussi, l’innocente Isabelle,</l>
					<l n="26" num="7.2">Devant son époux ahuri,</l>
					<l n="27" num="7.3">Se dressa fière, presque belle,</l>
					<l n="28" num="7.4">Et se tournant vers Marfori :</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1">« Par saint Jacques de Compostelle !</l>
					<l n="30" num="8.2">Ton âme à tous se décela,</l>
					<l n="31" num="8.3">On connaît ta valeur, dit-elle,</l>
					<l n="32" num="8.4">Mais tu n’aurais pas fait cela ! »</l>
				</lg>
				<closer>
					<dateline> 7 octobre.</dateline>
				</closer>
			</div></body></text></TEI>