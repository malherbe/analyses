<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Imitation De Jésus-Christ</title>
				<title type="medium">Édition électronique</title>
				<author key="COR">
					<name>
						<forename>Pierre</forename>
						<surname>CORNEILLE</surname>
					</name>
					<date from="1606" to="1684">1606-1684</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>13205 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2016</date>
				<idno type="local">COR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Imitation De Jésus-Christ</title>
						<author>Pierre Corneille</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/pierrecorneilleimmitadiondejesuschrist.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Imitation De Jésus-Christ</title>
						<author>Pierre Corneille</author>
						<editor>M. Ch. Marty-Laveaux</editor>
						<edition>nouvelle édition</edition>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k51199.r=pierre%20corneille?rk=85837;2</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie de L. Hachette et Cie</publisher>
							<date when="1862">1862</date>
						</imprint>
						<biblScope unit="tome">Tome 8</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1656">1656</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les chiffres arabes des livres et chapitres ont été remplacés par des chiffres romains</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">LIVRE DEUXIÈME</head><div type="poem" key="COR63">
					<head type="main">CHAPITRE VI</head>
					<head type="sub">Des joies de la bonne conscience.</head>
					<lg n="1">
						<l n="1" num="1.1"><space unit="char" quantity="8"></space>Droite et sincère conscience,</l>
						<l n="2" num="1.2"><space unit="char" quantity="8"></space>Digne gloire des gens de bien,</l>
						<l n="3" num="1.3">Oh ! Que ton témoignage est un doux entretien,</l>
						<l n="4" num="1.4">Et qu’il mêle de joie à notre patience,</l>
						<l n="5" num="1.5"><space unit="char" quantity="8"></space>Quand il ne nous reproche rien !</l>
					</lg>
					<lg n="2">
						<l n="6" num="2.1"><space unit="char" quantity="8"></space>Tu fais souffrir avec courage,</l>
						<l n="7" num="2.2"><space unit="char" quantity="8"></space>Tu fais combattre en sûreté ;</l>
						<l n="8" num="2.3">L’allégresse te suit parmi l’adversité,</l>
						<l n="9" num="2.4">Et contre les assauts du plus cruel orage</l>
						<l n="10" num="2.5"><space unit="char" quantity="8"></space>Tu soutiens la tranquillité.</l>
					</lg>
					<lg n="3">
						<l n="11" num="3.1"><space unit="char" quantity="8"></space>Mais la conscience gâtée</l>
						<l n="12" num="3.2"><space unit="char" quantity="8"></space>Tremble au dedans sous le remords ;</l>
						<l n="13" num="3.3">Sa vaine inquiétude égare ses efforts ;</l>
						<l n="14" num="3.4">Et les noires vapeurs dont elle est agitée</l>
						<l n="15" num="3.5"><space unit="char" quantity="8"></space>Offusquent même ses dehors.</l>
					</lg>
					<lg n="4">
						<l n="16" num="4.1"><space unit="char" quantity="8"></space>Malgré le monde et ses murmures,</l>
						<l n="17" num="4.2"><space unit="char" quantity="8"></space>Homme, tu sauras vivre en paix,</l>
						<l n="18" num="4.3">Si ton cœur est d’accord de tout ce que tu fais,</l>
						<l n="19" num="4.4">Et s’il ne porte point de secrètes censures</l>
						<l n="20" num="4.5"><space unit="char" quantity="8"></space>Sur la chaleur de tes souhaits.</l>
					</lg>
					<lg n="5">
						<l n="21" num="5.1"><space unit="char" quantity="8"></space>Aime les avis qu’il t’envoie,</l>
						<l n="22" num="5.2"><space unit="char" quantity="8"></space>Embrasse leur correction,</l>
						<l n="23" num="5.3">Et pour te bien tenir en ta possession,</l>
						<l n="24" num="5.4">Jamais ne te hasarde à prendre aucune joie</l>
						<l n="25" num="5.5"><space unit="char" quantity="8"></space>Qu’après une bonne action.</l>
					</lg>
					<lg n="6">
						<l n="26" num="6.1"><space unit="char" quantity="8"></space>Méchants, cette vraie allégresse</l>
						<l n="27" num="6.2"><space unit="char" quantity="8"></space>Ne peut entrer en votre cœur :</l>
						<l n="28" num="6.3">Le calme en est banni par la voix du seigneur,</l>
						<l n="29" num="6.4">Et c’est faire une injure à sa parole expresse</l>
						<l n="30" num="6.5"><space unit="char" quantity="8"></space>Que vous vanter d’un tel bonheur.</l>
					</lg>
					<lg n="7">
						<l n="31" num="7.1"><space unit="char" quantity="8"></space>Ne dites point, pour nous séduire,</l>
						<l n="32" num="7.2"><space unit="char" quantity="8"></space>Que vous vivez en pleine paix,</l>
						<l n="33" num="7.3">Que les malheurs sur vous ne tomberont jamais,</l>
						<l n="34" num="7.4">Et qu’aucun assez vain pour prétendre à vous nuire</l>
						<l n="35" num="7.5"><space unit="char" quantity="8"></space>N’en sauroit venir aux effets.</l>
					</lg>
					<lg n="8">
						<l n="36" num="8.1"><space unit="char" quantity="8"></space>Vous mentez, et l’ire divine,</l>
						<l n="37" num="8.2"><space unit="char" quantity="8"></space>Bientôt contrainte d’éclater,</l>
						<l n="38" num="8.3">Dans un triste néant vous va précipiter ;</l>
						<l n="39" num="8.4">Et sous l’affreux débris d’une prompte ruine</l>
						<l n="40" num="8.5"><space unit="char" quantity="8"></space>Tous vos desseins vont avorter.</l>
					</lg>
					<lg n="9">
						<l n="41" num="9.1"><space unit="char" quantity="8"></space>Le juste a des routes diverses :</l>
						<l n="42" num="9.2"><space unit="char" quantity="8"></space>Il aime en Dieu l’affliction,</l>
						<l n="43" num="9.3">Et se souvient toujours parmi l’oppression</l>
						<l n="44" num="9.4">Que prendre quelque gloire à souffrir des traverses,</l>
						<l n="45" num="9.5"><space unit="char" quantity="8"></space>C’est en prendre en sa passion.</l>
					</lg>
					<lg n="10">
						<l n="46" num="10.1"><space unit="char" quantity="8"></space>Il voit celle qui vient des hommes</l>
						<l n="47" num="10.2"><space unit="char" quantity="8"></space>Avec mépris, avec courroux :</l>
						<l n="48" num="10.3">Aussi n’a-t-elle rien qu’il puisse trouver doux ;</l>
						<l n="49" num="10.4">Elle est foible, elle est vaine, ainsi que nous le sommes,</l>
						<l n="50" num="10.5"><space unit="char" quantity="8"></space>Et périssable comme nous.</l>
					</lg>
					<lg n="11">
						<l n="51" num="11.1"><space unit="char" quantity="8"></space>Elle n’est jamais si fidèle</l>
						<l n="52" num="11.2"><space unit="char" quantity="8"></space>Qu’elle ne déçoive à la fin ;</l>
						<l n="53" num="11.3">Et la déloyauté de son éclat malin</l>
						<l n="54" num="11.4">Dans un brillant nuage enveloppe avec elle</l>
						<l n="55" num="11.5"><space unit="char" quantity="8"></space>Un noir amas de long chagrin.</l>
					</lg>
					<lg n="12">
						<l n="56" num="12.1"><space unit="char" quantity="8"></space>Celle des bons, toute secrète,</l>
						<l n="57" num="12.2"><space unit="char" quantity="8"></space>N’a ni pompe, ni faux attraits ;</l>
						<l n="58" num="12.3">Leur seule conscience en forme tous les traits,</l>
						<l n="59" num="12.4">Et la bouche de l’homme, à changer si sujette,</l>
						<l n="60" num="12.5"><space unit="char" quantity="8"></space>Ne la fait ni détruit jamais.</l>
					</lg>
					<lg n="13">
						<l n="61" num="13.1"><space unit="char" quantity="8"></space>De Dieu seul part toute leur joie,</l>
						<l n="62" num="13.2"><space unit="char" quantity="8"></space>De qui la sainte activité,</l>
						<l n="63" num="13.3">Remontant vers sa source avec rapidité,</l>
						<l n="64" num="13.4">S’attache à la grandeur de la main qui l’envoie,</l>
						<l n="65" num="13.5"><space unit="char" quantity="8"></space>Et s’abîme en sa vérité.</l>
					</lg>
					<lg n="14">
						<l n="66" num="14.1"><space unit="char" quantity="8"></space>L’amour de la gloire éternelle</l>
						<l n="67" num="14.2"><space unit="char" quantity="8"></space>Les sait si pleinement saisir,</l>
						<l n="68" num="14.3">Que leur âme est stupide à tout autre plaisir,</l>
						<l n="69" num="14.4">Et que tout ce qu’on voit de gloire temporelle</l>
						<l n="70" num="14.5"><space unit="char" quantity="8"></space>Ne les touche d’aucun desir.</l>
					</lg>
					<lg n="15">
						<l n="71" num="15.1"><space unit="char" quantity="8"></space>Aussi l’issue en est funeste</l>
						<l n="72" num="15.2"><space unit="char" quantity="8"></space>Pour qui ne peut s’en dégager ;</l>
						<l n="73" num="15.3">Et qui de tout son cœur n’aime à la négliger</l>
						<l n="74" num="15.4">Ne peut avoir d’amour pour la gloire céleste,</l>
						<l n="75" num="15.5"><space unit="char" quantity="8"></space>Ou cet amour est bien léger.</l>
					</lg>
					<lg n="16">
						<l n="76" num="16.1"><space unit="char" quantity="8"></space>Douce tranquillité de l’âme,</l>
						<l n="77" num="16.2"><space unit="char" quantity="8"></space>Avant-goût de celle des cieux,</l>
						<l n="78" num="16.3">Tu fermes pour la terre et l’oreille et les yeux ;</l>
						<l n="79" num="16.4">Et qui sait dédaigner la louange et le blâme</l>
						<l n="80" num="16.5"><space unit="char" quantity="8"></space>Sait te posséder en tous lieux.</l>
					</lg>
					<lg n="17">
						<l n="81" num="17.1"><space unit="char" quantity="8"></space>Ton repos est une conquête</l>
						<l n="82" num="17.2"><space unit="char" quantity="8"></space>Dont jouissent en sûreté</l>
						<l n="83" num="17.3">Ceux dont la conscience est sans impureté ;</l>
						<l n="84" num="17.4">Et le cœur est un port où n’entre la tempête</l>
						<l n="85" num="17.5"><space unit="char" quantity="8"></space>Que par la vaine anxiété.</l>
					</lg>
					<lg n="18">
						<l n="86" num="18.1"><space unit="char" quantity="8"></space>Ris donc, mortel, des vains mélanges</l>
						<l n="87" num="18.2"><space unit="char" quantity="8"></space>Qu’ici le monde aime à former :</l>
						<l n="88" num="18.3">Il a beau t’applaudir ou te mésestimer,</l>
						<l n="89" num="18.4">Tu n’en es pas plus saint pour toutes ses louanges,</l>
						<l n="90" num="18.5"><space unit="char" quantity="8"></space>Ni moindre pour t’en voir blâmer.</l>
					</lg>
					<lg n="19">
						<l n="91" num="19.1"><space unit="char" quantity="8"></space>Ce que tu vaux est en toi-même,</l>
						<l n="92" num="19.2"><space unit="char" quantity="8"></space>Tu fais ton prix par tes vertus ;</l>
						<l n="93" num="19.3">Tous les encens d’autrui sont encens superflus ;</l>
						<l n="94" num="19.4">Et ce qu’on est aux yeux du monarque suprême,</l>
						<l n="95" num="19.5"><space unit="char" quantity="8"></space>On l’est partout, et rien de plus.</l>
					</lg>
					<lg n="20">
						<l n="96" num="20.1"><space unit="char" quantity="8"></space>Vois-toi dedans, et considère</l>
						<l n="97" num="20.2"><space unit="char" quantity="8"></space>Le fond de ton intention :</l>
						<l n="98" num="20.3">Qui peut s’y regarder avec attention,</l>
						<l n="99" num="20.4">Soit qu’on parle de lui, soit qu’on veuille s’en taire,</l>
						<l n="100" num="20.5"><space unit="char" quantity="8"></space>N’en prend aucune émotion.</l>
					</lg>
					<lg n="21">
						<l n="101" num="21.1"><space unit="char" quantity="8"></space>L’homme ne voit que le visage,</l>
						<l n="102" num="21.2"><space unit="char" quantity="8"></space>Mais Dieu voit jusqu’au fond du cœur ;</l>
						<l n="103" num="21.3">L’homme des actions voit la vaine splendeur,</l>
						<l n="104" num="21.4">Mais Dieu connoît leur source, et voit dans le courage</l>
						<l n="105" num="21.5"><space unit="char" quantity="8"></space>Ou leur souillure ou leur candeur.</l>
					</lg>
					<lg n="22">
						<l n="106" num="22.1"><space unit="char" quantity="8"></space>Fais toujours bien, et fuis le crime,</l>
						<l n="107" num="22.2"><space unit="char" quantity="8"></space>Sans t’en donner de vanité ;</l>
						<l n="108" num="22.3">Du mépris de toi-même arme ta sainteté :</l>
						<l n="109" num="22.4">Bien vivre et ne s’enfler d’aucune propre estime,</l>
						<l n="110" num="22.5"><space unit="char" quantity="8"></space>C’est la parfaite humilité.</l>
					</lg>
					<lg n="23">
						<l n="111" num="23.1"><space unit="char" quantity="8"></space>La marque d’une âme bien pure</l>
						<l n="112" num="23.2"><space unit="char" quantity="8"></space>Qui hors de Dieu ne cherche rien,</l>
						<l n="113" num="23.3">Et met en ses bontés son unique soutien,</l>
						<l n="114" num="23.4">C’est d’être sans desir qu’aucune créature</l>
						<l n="115" num="23.5"><space unit="char" quantity="8"></space>En dise ou pense quelque bien.</l>
					</lg>
					<lg n="24">
						<l n="116" num="24.1"><space unit="char" quantity="8"></space>Cette sévère négligence</l>
						<l n="117" num="24.2"><space unit="char" quantity="8"></space>Des témoignages du dehors</l>
						<l n="118" num="24.3">Pour l’attacher à Dieu réunit ses efforts,</l>
						<l n="119" num="24.4">Et l’abandonne entière à cette providence</l>
						<l n="120" num="24.5"><space unit="char" quantity="8"></space>Qu’adorent ses heureux transports.</l>
					</lg>
					<lg n="25">
						<l n="121" num="25.1"><space unit="char" quantity="8"></space>« ce n’est pas celui qui se loue,</l>
						<l n="122" num="25.2"><space unit="char" quantity="8"></space>Dit Saint Paul, qui sera sauvé :</l>
						<l n="123" num="25.3">Qui s’approuve soi-même est souvent réprouvé ;</l>
						<l n="124" num="25.4">Et c’est celui-là seul que ce grand maître avoue,</l>
						<l n="125" num="25.5"><space unit="char" quantity="8"></space>Qui pour sa gloire est réservé. »</l>
					</lg>
					<lg n="26">
						<l n="126" num="26.1"><space unit="char" quantity="8"></space>Enfin cheminer dans sa voie,</l>
						<l n="127" num="26.2"><space unit="char" quantity="8"></space>Faire avec lui forte union,</l>
						<l n="128" num="26.3">Ne se lier ailleurs d’aucune affection,</l>
						<l n="129" num="26.4">N’avoir que lui pour but, que son amour pour joie,</l>
						<l n="130" num="26.5"><space unit="char" quantity="8"></space>C’est l’entière perfection.</l>
					</lg>
				</div></body></text></TEI>