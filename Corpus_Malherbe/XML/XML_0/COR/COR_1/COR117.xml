<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Imitation De Jésus-Christ</title>
				<title type="medium">Édition électronique</title>
				<author key="COR">
					<name>
						<forename>Pierre</forename>
						<surname>CORNEILLE</surname>
					</name>
					<date from="1606" to="1684">1606-1684</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>13205 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2016</date>
				<idno type="local">COR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Imitation De Jésus-Christ</title>
						<author>Pierre Corneille</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/pierrecorneilleimmitadiondejesuschrist.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Imitation De Jésus-Christ</title>
						<author>Pierre Corneille</author>
						<editor>M. Ch. Marty-Laveaux</editor>
						<edition>nouvelle édition</edition>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k51199.r=pierre%20corneille?rk=85837;2</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie de L. Hachette et Cie</publisher>
							<date when="1862">1862</date>
						</imprint>
						<biblScope unit="tome">Tome 8</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1656">1656</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les chiffres arabes des livres et chapitres ont été remplacés par des chiffres romains</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">LIVRE TROISIÈME</head><div type="poem" key="COR117">
					<head type="main">CHAPITRE XLVIII</head>
					<head type="sub">Du jour de l’éternité, et des angoisses <lb></lb>de cette vie.</head>
					<lg n="1">
						<l n="1" num="1.1">Ô séjour bienheureux de la cité céleste,</l>
						<l n="2" num="1.2">Où de l’éternité le jour se manifeste,</l>
						<l n="3" num="1.3">Jour que jamais n’offusque aucune obscurité,</l>
						<l n="4" num="1.4">Jour qu’éclaire toujours l’astre de vérité,</l>
						<l n="5" num="1.5">Jour où sans cesse brille une joie épurée,</l>
						<l n="6" num="1.6">Jour où sans cesse règne une paix assurée,</l>
						<l n="7" num="1.7">Jour toujours immuable et dont le saint éclat</l>
						<l n="8" num="1.8">Jamais ne dégénère en un contraire état !</l>
						<l n="9" num="1.9">Que déjà ne luit-il ! Et pour le laisser luire</l>
						<l n="10" num="1.10">Que ne cessent les temps de perdre et de produire !</l>
						<l n="11" num="1.11">Que déjà ne fait place à ce grand avenir</l>
						<l n="12" num="1.12">Tout ce qu’ici leur chute avec eux doit finir !</l>
						<l n="13" num="1.13">Il luit, il luit déjà, mais sa vive lumière</l>
						<l n="14" num="1.14">Aux seuls hôtes du ciel se fait voir toute entière.</l>
						<l n="15" num="1.15">Tant que nous demeurons sur la terre exilés,</l>
						<l n="16" num="1.16">Il n’en tombe sur nous que des rayons voilés ;</l>
						<l n="17" num="1.17">L’éloignement confond ou dissipe l’image</l>
						<l n="18" num="1.18">De ce qui s’en échappe au travers d’un nuage,</l>
						<l n="19" num="1.19">Et tout ce qu’à nos yeux il est permis d’en voir,</l>
						<l n="20" num="1.20">Ce sont traits réfléchis qu’en répand un miroir.</l>
					</lg>
					<lg n="2">
						<l n="21" num="2.1">Ces habitants du ciel en savent les délices,</l>
						<l n="22" num="2.2">Tandis qu’en ces bas lieux nous traînons nos supplices,</l>
						<l n="23" num="2.3">Et qu’un accablement d’amertume et d’ennuis</l>
						<l n="24" num="2.4">De nos jours les plus beaux fait d’effroyables nuits.</l>
						<l n="25" num="2.5">Ces jours, que le temps donne et dérobe lui-même,</l>
						<l n="26" num="2.6">Longs pour qui les connoît, et courts pour qui les aime,</l>
						<l n="27" num="2.7">Ont pour l’un et pour l’autre un tissu de malheurs</l>
						<l n="28" num="2.8">D’où naissent à l’envi l’angoisse et les douleurs.</l>
						<l n="29" num="2.9">Tant que l’homme en jouit, que de péchés le gênent !</l>
						<l n="30" num="2.10">Combien de passions l’assiégent ou l’enchaînent !</l>
						<l n="31" num="2.11">Que de justes frayeurs, que de soucis cuisants</l>
						<l n="32" num="2.12">Lui déchirent le cœur et brouillent tous les sens !</l>
						<l n="33" num="2.13">La curiosité de tous côtés l’engage ;</l>
						<l n="34" num="2.14">La folle vanité le tient en esclavage ;</l>
						<l n="35" num="2.15">Enveloppé d’erreurs, atterré de travaux,</l>
						<l n="36" num="2.16">Entre mille ennemis pressé de mille assauts,</l>
						<l n="37" num="2.17">Le repos l’affoiblit, et le plaisir l’énerve ;</l>
						<l n="38" num="2.18">Tout le cours de sa vie a des maux de réserve ;</l>
						<l n="39" num="2.19">Le riche par ses biens n’en est pas exempté,</l>
						<l n="40" num="2.20">Et le pauvre a pour comble encor sa pauvreté.</l>
					</lg>
					<lg n="3">
						<l n="41" num="3.1">Quand verrai-je, seigneur, finir tant de supplices ?</l>
						<l n="42" num="3.2">Quand cesserai-je d’être un esclave des vices ?</l>
						<l n="43" num="3.3">Quand occuperas-tu, toi seul, mon souvenir ?</l>
						<l n="44" num="3.4">Quand mettrai-je ma joie entière à te bénir ?</l>
						<l n="45" num="3.5">Quand verrai-je en mon cœur une liberté sainte,</l>
						<l n="46" num="3.6">Sans aucun embarras, sans aucune contrainte ?</l>
						<l n="47" num="3.7">Et quand ne se nirai-je en mes ardents transports</l>
						<l n="48" num="3.8">Rien qui pèse à l’esprit, rien qui gêne le corps ?</l>
						<l n="49" num="3.9">Quand viendra cette paix, et profonde et solide,</l>
						<l n="50" num="3.10">Où la sûreté règne, où ton amour préside,</l>
						<l n="51" num="3.11">Paix dedans et dehors, paix sans anxiétés,</l>
						<l n="52" num="3.12">Paix sans trouble, paix ferme enfin de tous côtés ?</l>
					</lg>
					<lg n="4">
						<l n="53" num="4.1">Doux sauveur de mon âme, hélas ! Quand te verrai-je ?</l>
						<l n="54" num="4.2">Quand m’accorderas-tu ce dernier privilége ?</l>
						<l n="55" num="4.3">Quand te pourront mes yeux contempler à loisir,</l>
						<l n="56" num="4.4">Te voir en tout, partout, être mon seul desir ?</l>
						<l n="57" num="4.5">Quand te verrai-je assis sur ton trône de gloire,</l>
						<l n="58" num="4.6">Et quand aurai-je part aux fruits de ta victoire,</l>
						<l n="59" num="4.7">À ce règne sans fin, que ta bénignité</l>
						<l n="60" num="4.8">Prépare à tes élus de toute éternité ?</l>
					</lg>
					<lg n="5">
						<l n="61" num="5.1">Tu sais que je languis, abandonné sur terre</l>
						<l n="62" num="5.2">Aux cruelles fureurs d’une implacable guerre,</l>
						<l n="63" num="5.3">Où toujours je me trouve en pays ennemi,</l>
						<l n="64" num="5.4">Où rien ne me console après avoir gémi,</l>
						<l n="65" num="5.5">Où de mon triste exil les suites importunes</l>
						<l n="66" num="5.6">Ne sont qu’affreux combats et longues infortunes.</l>
					</lg>
					<lg n="6">
						<l n="67" num="6.1">Modère les rigueurs de ce bannissement,</l>
						<l n="68" num="6.2">Verse en mes déplaisirs quelque soulagement :</l>
						<l n="69" num="6.3">Tu sais que c’est pour toi que tout mon cœur soupire ;</l>
						<l n="70" num="6.4">Tu vois que c’est à toi que tout mon cœur aspire ;</l>
						<l n="71" num="6.5">Le monde m’est à charge, et ne fait que grossir</l>
						<l n="72" num="6.6">Ce fardeau de mes maux qu’il tâche d’adoucir :</l>
						<l n="73" num="6.7">Ni de lui ni de moi je ne dois rien attendre ;</l>
						<l n="74" num="6.8">Je veux te posséder, et ne te puis comprendre ;</l>
						<l n="75" num="6.9">Je forme à peine un vol pour m’attacher aux cieux,</l>
						<l n="76" num="6.10">Qu’un souci temporel le ravale en ces lieux ;</l>
						<l n="77" num="6.11">Et de mes passions les forces mal domptées</l>
						<l n="78" num="6.12">Me rendent aux douceurs qu’elles m’avoient prêtées :</l>
						<l n="79" num="6.13">L’esprit prend le dessus, mais le poids de la chair</l>
						<l n="80" num="6.14">Jusqu’au-dessous de tout me force à trébucher.</l>
						<l n="81" num="6.15">Ainsi je me combats et me pèse à moi-même,</l>
						<l n="82" num="6.16">Ainsi de mon dedans le désordre est extrême :</l>
						<l n="83" num="6.17">La chair rappelle en bas, quand l’esprit tire en haut,</l>
						<l n="84" num="6.18">Et la foible partie est celle qui prévaut.</l>
					</lg>
					<lg n="7">
						<l n="85" num="7.1">Que je souffre, seigneur, quand mon âme élevée</l>
						<l n="86" num="7.2">Jusqu’aux pieds de son dieu qui l’a faite et sauvée,</l>
						<l n="87" num="7.3">Un damnable escadron de sentiments honteux</l>
						<l n="88" num="7.4">Vient troubler sa prière et distraire ses vœux !</l>
					</lg>
					<lg n="8">
						<l n="89" num="8.1">Toi, qui seul de mes maux tiens en main le remède,</l>
						<l n="90" num="8.2">En ces extrémités n’éloigne pas ton aide,</l>
						<l n="91" num="8.3">Et ne retire point par un juste courroux</l>
						<l n="92" num="8.4">Le bras qui seul pour moi peut rompre tous leurs coups.</l>
						<l n="93" num="8.5">Lance du haut du ciel un éclat de ta foudre,</l>
						<l n="94" num="8.6">Qui dissipe leur force et les réduise en poudre ;</l>
						<l n="95" num="8.7">Précipite sur eux la grêle de tes dards ;</l>
						<l n="96" num="8.8">Rends-les à leur néant d’un seul de tes regards,</l>
						<l n="97" num="8.9">Et renvoie aux enfers, comme souverain maître,</l>
						<l n="98" num="8.10">Ces fantômes impurs que leur prince fait naître.</l>
					</lg>
					<lg n="9">
						<l n="99" num="9.1">D’autre côté, seigneur, recueille en toi mes sens,</l>
						<l n="100" num="9.2">Ranime, réunis mes desirs languissants ;</l>
						<l n="101" num="9.3">Fais qu’un parfait oubli des choses de la terre</l>
						<l n="102" num="9.4">Tienne à couvert mon cœur de toute cette guerre ;</l>
						<l n="103" num="9.5">Ou si par quelque embûche il se trouve surpris,</l>
						<l n="104" num="9.6">Fais que par les efforts d’un prompt et saint mépris</l>
						<l n="105" num="9.7">Il rejette soudain ces délices fardées</l>
						<l n="106" num="9.8">Dont le vice blanchit ses plus noires idées.</l>
					</lg>
					<lg n="10">
						<l n="107" num="10.1">Viens, viens à mon secours, suprême vérité,</l>
						<l n="108" num="10.2">Que je ne donne entrée à quelque vanité ;</l>
						<l n="109" num="10.3">Viens, céleste douceur, viens occuper la place,</l>
						<l n="110" num="10.4">Et toute impureté fuira devant ta face.</l>
						<l n="111" num="10.5">Cependant fais-moi grâce, et ne t’offense pas</l>
						<l n="112" num="10.6">Si dans le vrai chemin je fais quelques faux pas,</l>
						<l n="113" num="10.7">Si quelquefois de toi mon oraison s’égare,</l>
						<l n="114" num="10.8">Si quelque illusion malgré moi m’en sépare ;</l>
						<l n="115" num="10.9">Car enfin, je l’avoue à ma confusion,</l>
						<l n="116" num="10.10">Je ne cède que trop à cette illusion :</l>
						<l n="117" num="10.11">L’ombre d’un faux plaisir follement retracée</l>
						<l n="118" num="10.12">S’empare à tous moments de toute ma pensée ;</l>
						<l n="119" num="10.13">Je ne suis pas toujours où se trouve mon corps ;</l>
						<l n="120" num="10.14">Souvent j’occupe un lieu dont mon cœur est dehors ;</l>
						<l n="121" num="10.15">Et mon extravagance emportant l’infidèle,</l>
						<l n="122" num="10.16">Je suis bien loin de moi quand il est avec elle.</l>
					</lg>
					<lg n="11">
						<l n="123" num="11.1">L’homme, sans y penser, pense à ce qu’il chérit,</l>
						<l n="124" num="11.2">Ainsi que l’œil de soi tourne à ce qui lui rit.</l>
						<l n="125" num="11.3">Ce qu’aime la nature ou qui plaît par l’usage,</l>
						<l n="126" num="11.4">C’est ce qui le plus tôt nous offre son image,</l>
						<l n="127" num="11.5">Et l’offre rarement, que notre esprit touché</l>
						<l n="128" num="11.6">Ne s’attache sans peine où le cœur est penché.</l>
					</lg>
					<lg n="12">
						<l n="129" num="12.1">Aussi ta bouche même a bien voulu me dire</l>
						<l n="130" num="12.2">Qu’où je mets mon trésor, là mon âme respire :</l>
						<l n="131" num="12.3">Si je le mets au ciel, il m’est doux d’y penser ;</l>
						<l n="132" num="12.4">Si je le mets au monde, il m’y sait rabaisser ;</l>
						<l n="133" num="12.5">De ses prospérités je fais mon allégresse,</l>
						<l n="134" num="12.6">Et ses coups de revers excitent ma tristesse.</l>
					</lg>
					<lg n="13">
						<l n="135" num="13.1">Si les plaisirs des sens saisissent mon amour,</l>
						<l n="136" num="13.2">Ce qui peut les flatter m’occupe nuit et jour ;</l>
						<l n="137" num="13.3">Si j’aime de l’esprit la parfaite science,</l>
						<l n="138" num="13.4">Je fais mon entretien de tout ce qui l’avance :</l>
						<l n="139" num="13.5">Enfin tout ce que j’aime et tout ce qui me plaît</l>
						<l n="140" num="13.6">Me tient comme enchaîné par un doux intérêt,</l>
						<l n="141" num="13.7">J’en parle avec plaisir, avec plaisir j’écoute</l>
						<l n="142" num="13.8">Tout ce qui peut m’instruire à marcher dans sa route,</l>
						<l n="143" num="13.9">Et j’emporte chez moi l’image avec plaisir</l>
						<l n="144" num="13.10">De tout ce qui chatouille et pique mon desir.</l>
					</lg>
					<lg n="14">
						<l n="145" num="14.1">Qu’heureux est donc, ô Dieu, celui dont l’âme pure</l>
						<l n="146" num="14.2">Bannit, pour t’aimer seul, toute la créature,</l>
						<l n="147" num="14.3">Qui se fait violence, et n’osant s’accorder</l>
						<l n="148" num="14.4">Rien de ce que lui-même aime à se demander,</l>
						<l n="149" num="14.5">De la chair et des sens tellement se défie,</l>
						<l n="150" num="14.6">Qu’à force de ferveur l’esprit les crucifie !</l>
						<l n="151" num="14.7">C’est ainsi qu’en son cœur rétablissant la paix,</l>
						<l n="152" num="14.8">Sur le mépris du monde élevant ses souhaits,</l>
						<l n="153" num="14.9">Il t’offre une oraison, il t’offre des louanges</l>
						<l n="154" num="14.10">Dignes de se mêler à celles de tes anges,</l>
						<l n="155" num="14.11">Puisqu’en lui ton amour par ses divins transports</l>
						<l n="156" num="14.12">Étouffe le terrestre et dedans et dehors.</l>
					</lg>
				</div></body></text></TEI>