<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Imitation De Jésus-Christ</title>
				<title type="medium">Édition électronique</title>
				<author key="COR">
					<name>
						<forename>Pierre</forename>
						<surname>CORNEILLE</surname>
					</name>
					<date from="1606" to="1684">1606-1684</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>13205 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2016</date>
				<idno type="local">COR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Imitation De Jésus-Christ</title>
						<author>Pierre Corneille</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/pierrecorneilleimmitadiondejesuschrist.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Imitation De Jésus-Christ</title>
						<author>Pierre Corneille</author>
						<editor>M. Ch. Marty-Laveaux</editor>
						<edition>nouvelle édition</edition>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k51199.r=pierre%20corneille?rk=85837;2</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie de L. Hachette et Cie</publisher>
							<date when="1862">1862</date>
						</imprint>
						<biblScope unit="tome">Tome 8</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1656">1656</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les chiffres arabes des livres et chapitres ont été remplacés par des chiffres romains</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">LIVRE TROISIÈME</head><div type="poem" key="COR77">
					<head type="main">CHAPITRE VIII</head>
					<head type="sub">Du peu d’estime de soi-même en la présence de Dieu.</head>
					<lg n="1">
						<l n="1" num="1.1"><space unit="char" quantity="8"></space>Seigneur, t’oserai-je parler,</l>
						<l n="2" num="1.2"><space unit="char" quantity="4"></space>Moi qui ne suis que cendre et que poussière,</l>
						<l n="3" num="1.3"><space unit="char" quantity="4"></space>Qu’un vil extrait d’une impure matière,</l>
						<l n="4" num="1.4"><space unit="char" quantity="4"></space>Qu’au seul néant on a droit d’égaler ?</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><space unit="char" quantity="8"></space>Si je me prise davantage,</l>
						<l n="6" num="2.2"><space unit="char" quantity="8"></space>Je t’oblige à t’en ressentir,</l>
						<l n="7" num="2.3">Je vois tous mes péchés soudain me démentir,</l>
						<l n="8" num="2.4"><space unit="char" quantity="4"></space>Et contre moi porter un témoignage</l>
						<l n="9" num="2.5"><space unit="char" quantity="8"></space>Où je n’ai rien à repartir.</l>
					</lg>
					<lg n="3">
						<l n="10" num="3.1"><space unit="char" quantity="8"></space>Mais si je m’abaisse et m’obstine</l>
						<l n="11" num="3.2"><space unit="char" quantity="4"></space>À me réduire au néant dont je viens,</l>
						<l n="12" num="3.3">Si toute estime propre en moi se déracine,</l>
						<l n="13" num="3.4"><space unit="char" quantity="4"></space>Et qu’en dépit de tous ses entretiens</l>
						<l n="14" num="3.5">Je rentre en cette poudre où fut mon origine,</l>
						<l n="15" num="3.6"><space unit="char" quantity="8"></space>Ta grâce avec pleine vigueur</l>
						<l n="16" num="3.7"><space unit="char" quantity="8"></space>Est soudain propice à mon âme,</l>
						<l n="17" num="3.8"><space unit="char" quantity="4"></space>Et les rayons de ta céleste flamme</l>
						<l n="18" num="3.9"><space unit="char" quantity="8"></space>Descendent au fond de mon cœur.</l>
						<l n="19" num="3.10"><space unit="char" quantity="8"></space>L’orgueil, contraint à disparoître,</l>
						<l n="20" num="3.11">Ne laisse dans ce cœur aucun vain sentiment</l>
						<l n="21" num="3.12">Qui ne soit abîmé, pour petit qu’il puisse être,</l>
						<l n="22" num="3.13"><space unit="char" quantity="8"></space>Dans cet anéantissement,</l>
						<l n="23" num="3.14"><space unit="char" quantity="8"></space>Sans pouvoir jamais y renaître.</l>
					</lg>
					<lg n="4">
						<l n="24" num="4.1"><space unit="char" quantity="8"></space>Ta clarté m’expose à mes yeux,</l>
						<l n="25" num="4.2">Je me vois tout entier, et j’en vois d’autant mieux</l>
						<l n="26" num="4.3">Quels défauts ont suivi ma honteuse naissance :</l>
						<l n="27" num="4.4">Je vois ce que je suis, je vois ce que je fus,</l>
						<l n="28" num="4.5"><space unit="char" quantity="8"></space>Je vois d’où je viens, et confus</l>
						<l n="29" num="4.6"><space unit="char" quantity="8"></space>De ne voir que de l’impuissance,</l>
						<l n="30" num="4.7">Je m’écrie : « ô mon Dieu, que je m’étois déçu !</l>
						<l n="31" num="4.8"><space unit="char" quantity="4"></space>Je ne suis rien, et n’en avois rien su. »</l>
					</lg>
					<lg n="5">
						<l n="32" num="5.1"><space unit="char" quantity="8"></space>Si tu me laisses à moi-même,</l>
						<l n="33" num="5.2">Je n’ai dans mon néant que foiblesse et qu’effroi ;</l>
						<l n="34" num="5.3">Mais si dans mes ennuis tu jettes l’œil sur moi,</l>
						<l n="35" num="5.4">Soudain je deviens fort, et ma joie est extrême.</l>
					</lg>
					<lg n="6">
						<l n="36" num="6.1"><space unit="char" quantity="8"></space>Merveille, que de ces bas lieux,</l>
						<l n="37" num="6.2">Élevé tout à coup au-dessus du tonnerre,</l>
						<l n="38" num="6.3"><space unit="char" quantity="8"></space>Je vole ainsi jusques aux cieux,</l>
						<l n="39" num="6.4">Moi que mon propre poids rabat toujours en terre !</l>
						<l n="40" num="6.5"><space unit="char" quantity="4"></space>Que tout à coup de saints élancements,</l>
						<l n="41" num="6.6">Tout chargé que je suis d’une masse grossière,</l>
						<l n="42" num="6.7">Jusque dans ces palais de gloire et de lumière</l>
						<l n="43" num="6.8">Me fassent recevoir tes doux embrassements !</l>
					</lg>
					<lg n="7">
						<l n="44" num="7.1"><space unit="char" quantity="8"></space>Ton amour fait tous ces miracles :</l>
						<l n="45" num="7.2">C’est lui qui me prévient sans l’avoir mérité ;</l>
						<l n="46" num="7.3"><space unit="char" quantity="8"></space>C’est lui qui brise les obstacles</l>
						<l n="47" num="7.4">Qui naissent des besoins de mon infirmité ;</l>
						<l n="48" num="7.5"><space unit="char" quantity="8"></space>C’est lui qui soutient ma foiblesse,</l>
						<l n="49" num="7.6"><space unit="char" quantity="8"></space>Et quelque péril qui me presse,</l>
						<l n="50" num="7.7">C’est lui qui m’en préserve et le sait détourner ;</l>
						<l n="51" num="7.8">C’est lui qui m’affranchit, c’est lui qui me retire</l>
						<l n="52" num="7.9"><space unit="char" quantity="8"></space>De tant de malheurs, qu’on peut dire</l>
						<l n="53" num="7.10">Que leur nombre sans lui ne se pourroit borner.</l>
					</lg>
					<lg n="8">
						<l n="54" num="8.1">Ces malheurs, ces périls, ces besoins, ces foiblesses,</l>
						<l n="55" num="8.2">C’est ce que l’amour-propre en nos cœurs a semé,</l>
						<l n="56" num="8.3">C’est ce qu’on a pour fruit de ses molles tendresses,</l>
						<l n="57" num="8.4">Et je me suis perdu quand je me suis aimé ;</l>
						<l n="58" num="8.5"><space unit="char" quantity="8"></space>Mais quand détaché de moi-même,</l>
						<l n="59" num="8.6">Je t’aime purement et ne cherche que toi,</l>
						<l n="60" num="8.7">Je trouve ce que j’aime en un si digne emploi,</l>
						<l n="61" num="8.8">Je me retrouve encor, seigneur, en ce que j’aime ;</l>
						<l n="62" num="8.9">Et ce feu tout divin, plus il sait pénétrer,</l>
						<l n="63" num="8.10">Plus dans mon vrai néant il m’apprend à rentrer.</l>
					</lg>
					<lg n="9">
						<l n="64" num="9.1">Ton amour à t’aimer ainsi me sollicite,</l>
						<l n="65" num="9.2"><space unit="char" quantity="8"></space>Et me rappelle à mon devoir</l>
						<l n="66" num="9.3"><space unit="char" quantity="4"></space>Par des faveurs qui passent mon mérite,</l>
						<l n="67" num="9.4"><space unit="char" quantity="4"></space>Et par des biens plus grands que mon espoir.</l>
					</lg>
					<lg n="10">
						<l n="68" num="10.1"><space unit="char" quantity="8"></space>Je t’en bénis, être suprême,</l>
						<l n="69" num="10.2"><space unit="char" quantity="8"></space>Dont l’immense bénignité</l>
						<l n="70" num="10.3"><space unit="char" quantity="8"></space>Étend sa libéralité</l>
						<l n="71" num="10.4"><space unit="char" quantity="8"></space>Sur l’indigne et sur l’ingrat même.</l>
						<l n="72" num="10.5">Ce torrent que jamais tu ne laisses tarir</l>
						<l n="73" num="10.6"><space unit="char" quantity="8"></space>Ne se lasse point de courir</l>
						<l n="74" num="10.7"><space unit="char" quantity="8"></space>Même vers ceux qui s’en éloignent ;</l>
						<l n="75" num="10.8"><space unit="char" quantity="8"></space>Et souvent sur l’aversion</l>
						<l n="76" num="10.9"><space unit="char" quantity="8"></space>Que les plus endurcis témoignent,</l>
						<l n="77" num="10.10">Il roule les trésors de ton affection.</l>
					</lg>
					<lg n="11">
						<l n="78" num="11.1"><space unit="char" quantity="8"></space>De ces sources inépuisables</l>
						<l n="79" num="11.2"><space unit="char" quantity="8"></space>Fais sur nous déborder les flots ;</l>
						<l n="80" num="11.3"><space unit="char" quantity="8"></space>Rends-nous humbles, rends-nous dévots,</l>
						<l n="81" num="11.4">Rends-nous reconnoissants, rends-nous inébranlables ;</l>
						<l n="82" num="11.5">Relève-nous le cœur sous nos maux abattu,</l>
						<l n="83" num="11.6">Attire-nous à toi par cette sainte amorce,</l>
						<l n="84" num="11.7"><space unit="char" quantity="8"></space>Toi qui seul es notre vertu,</l>
						<l n="85" num="11.8"><space unit="char" quantity="8"></space>Notre salut et notre force.</l>
					</lg>
				</div></body></text></TEI>