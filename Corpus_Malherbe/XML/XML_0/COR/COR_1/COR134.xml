<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Imitation De Jésus-Christ</title>
				<title type="medium">Édition électronique</title>
				<author key="COR">
					<name>
						<forename>Pierre</forename>
						<surname>CORNEILLE</surname>
					</name>
					<date from="1606" to="1684">1606-1684</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>13205 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2016</date>
				<idno type="local">COR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Imitation De Jésus-Christ</title>
						<author>Pierre Corneille</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/pierrecorneilleimmitadiondejesuschrist.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Imitation De Jésus-Christ</title>
						<author>Pierre Corneille</author>
						<editor>M. Ch. Marty-Laveaux</editor>
						<edition>nouvelle édition</edition>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k51199.r=pierre%20corneille?rk=85837;2</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie de L. Hachette et Cie</publisher>
							<date when="1862">1862</date>
						</imprint>
						<biblScope unit="tome">Tome 8</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1656">1656</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les chiffres arabes des livres et chapitres ont été remplacés par des chiffres romains</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">LIVRE QUATRIÈME</head><div type="poem" key="COR134">
					<head type="main">CHAPITRE V</head>
					<head type="sub">De la dignité du sacrement, et de l’état du sacerdoce.</head>
					<lg n="1">
						<l n="1" num="1.1">D’un ange dans les cieux atteins la pureté,</l>
						<l n="2" num="1.2">D’un baptiste au désert joins-y la sainteté ;</l>
						<l n="3" num="1.3">Mais pur à leur égal, mais saint à son exemple,</l>
						<l n="4" num="1.4">Ne crois pas l’être assez pour pouvoir dignement</l>
						<l n="5" num="1.5">Et tenir en tes mains et m’offrir en mon temple</l>
						<l n="6" num="1.6"><space unit="char" quantity="12"></space>Un si grand sacrement.</l>
					</lg>
					<lg n="2">
						<l n="7" num="2.1">Conçois, si tu le peux, quelle est cette faveur</l>
						<l n="8" num="2.2">De tenir en tes mains le corps de ton sauveur,</l>
						<l n="9" num="2.3">Le consacrer toi-même, et le prendre pour viande ;</l>
						<l n="10" num="2.4">Et tu connoîtras lors qu’il n’est mérite humain</l>
						<l n="11" num="2.5">À qui doive l’effet d’une bonté si grande</l>
						<l n="12" num="2.6"><space unit="char" quantity="12"></space>L’arbitre souverain.</l>
					</lg>
					<lg n="3">
						<l n="13" num="3.1">Ce mystère est bien grand, puisque du haut des cieux</l>
						<l n="14" num="3.2">Il fait descendre un dieu jusques en ces bas lieux,</l>
						<l n="15" num="3.3">Et le met en état qu’on le touche et le mange ;</l>
						<l n="16" num="3.4">Du sacerdoce aussi grande est la dignité,</l>
						<l n="17" num="3.5">Puisqu’on reçoit par là ce que jamais de l’ange</l>
						<l n="18" num="3.6"><space unit="char" quantity="12"></space>N’obtint la pureté.</l>
					</lg>
					<lg n="4">
						<l n="19" num="4.1">Prêtres, c’est à vous seuls que, sans vous le devoir,</l>
						<l n="20" num="4.2">Ma main par mon église accorde ce pouvoir,</l>
						<l n="21" num="4.3">Cette émanation de ma vertu céleste :</l>
						<l n="22" num="4.4">À vous seuls appartient de consacrer mon corps,</l>
						<l n="23" num="4.5">D’en faire un sacrifice, et départir au reste</l>
						<l n="24" num="4.6"><space unit="char" quantity="12"></space>Ce qu’il a de trésors.</l>
					</lg>
					<lg n="5">
						<l n="25" num="5.1">En prononçant les mots que je vous ai dictés,</l>
						<l n="26" num="5.2">Suivant mon institut, suivant mes volontés,</l>
						<l n="27" num="5.3">Vous opérez l’effet de votre ministère :</l>
						<l n="28" num="5.4">Un invisible agent concourt d’un pas égal,</l>
						<l n="29" num="5.5">Et tout Dieu que je suis, soudain j’y coopère</l>
						<l n="30" num="5.6"><space unit="char" quantity="12"></space>Comme auteur principal.</l>
					</lg>
					<lg n="6">
						<l n="31" num="6.1">Ma voix toute-puissante, à qui tout est soumis,</l>
						<l n="32" num="6.2">Moi-même me soumet à ce que j’ai promis,</l>
						<l n="33" num="6.3">M’assujettit aux lois de mon ordre suprême ;</l>
						<l n="34" num="6.4">Et ma divinité ne croit point se trahir</l>
						<l n="35" num="6.5">À descendre du ciel pour donner elle-même</l>
						<l n="36" num="6.6"><space unit="char" quantity="12"></space>L’exemple d’obéir.</l>
					</lg>
					<lg n="7">
						<l n="37" num="7.1">Crois-en donc plus ton Dieu que tes aveugles sens,</l>
						<l n="38" num="7.2">Crois-en plus de sa voix les termes tout-puissants,</l>
						<l n="39" num="7.3">Que le rapport trompeur d’aucun signe visible ;</l>
						<l n="40" num="7.4">Et sans que ces dehors te rendent rien suspect,</l>
						<l n="41" num="7.5">Porte à cette action tout ce qui t’est possible</l>
						<l n="42" num="7.6"><space unit="char" quantity="12"></space>D’amour et de respect.</l>
					</lg>
					<lg n="8">
						<l n="43" num="8.1">Pense à toi, prends-y garde, aime, respecte, crains :</l>
						<l n="44" num="8.2">Vois de quel ministère, en t’imposant les mains,</l>
						<l n="45" num="8.3">L’évêque t’a commis le divin exercice :</l>
						<l n="46" num="8.4">Il t’a consacré prêtre, et c’est à toi d’offrir</l>
						<l n="47" num="8.5">Ce doux mémorial de tout l’affreux supplice</l>
						<l n="48" num="8.6"><space unit="char" quantity="12"></space>Qu’il m’a plu de souffrir.</l>
					</lg>
					<lg n="9">
						<l n="49" num="9.1">Songe à t’en acquitter avec fidélité,</l>
						<l n="50" num="9.2">Avec dévotion, avec humilité :</l>
						<l n="51" num="9.3">N’offre point qu’avec foi, n’offre point qu’avec zèle ;</l>
						<l n="52" num="9.4">Songe à régler ta vie, et la règle si bien</l>
						<l n="53" num="9.5">Qu’elle soit sans reproche, et serve de modèle</l>
						<l n="54" num="9.6"><space unit="char" quantity="12"></space>Aux devoirs d’un chrétien.</l>
					</lg>
					<lg n="10">
						<l n="55" num="10.1">Ton rang, loin d’alléger le poids de ton fardeau,</l>
						<l n="56" num="10.2">En redouble la charge, et jusques au tombeau</l>
						<l n="57" num="10.3">Il te met sous le joug d’une loi plus sévère :</l>
						<l n="58" num="10.4">Il te prescrit à suivre un chemin plus étroit,</l>
						<l n="59" num="10.5">Et la perfection que doit ton caractère</l>
						<l n="60" num="10.6"><space unit="char" quantity="12"></space>Veut qu’on marche plus droit.</l>
					</lg>
					<lg n="11">
						<l n="61" num="11.1">Oui, tu dois un exemple au reste des mortels,</l>
						<l n="62" num="11.2">Qui fasse rejaillir du pied de mes autels</l>
						<l n="63" num="11.3">Jusqu’au fond de leurs cœurs une clarté solide ;</l>
						<l n="64" num="11.4">Et toutes les vertus qui brillent ici-bas</l>
						<l n="65" num="11.5">Doivent former d’un prêtre un infaillible guide</l>
						<l n="66" num="11.6"><space unit="char" quantity="12"></space>Pour qui va sur ses pas.</l>
					</lg>
					<lg n="12">
						<l n="67" num="12.1">Loin de suivre le train des hommes du commun,</l>
						<l n="68" num="12.2">Un prêtre doit en fuir le commerce importun,</l>
						<l n="69" num="12.3">De peur d’être souillé de leurs honteux mélanges ;</l>
						<l n="70" num="12.4">Et dans tout ce qu’il fait, un vigilant souci</l>
						<l n="71" num="12.5">Lui doit pour entretien choisir au ciel les anges,</l>
						<l n="72" num="12.6"><space unit="char" quantity="12"></space>Et les parfaits ici.</l>
					</lg>
					<lg n="13">
						<l n="73" num="13.1">Des ornements sacrés lorsqu’il est revêtu,</l>
						<l n="74" num="13.2">Il a de Jésus-Christ l’image et la vertu ;</l>
						<l n="75" num="13.3">Ainsi que son ministre il agit en sa place ;</l>
						<l n="76" num="13.4">Et ce n’est qu’en son nom que les vœux qu’il conçoit</l>
						<l n="77" num="13.5">Pour le peuple et pour lui montent devant la face</l>
						<l n="78" num="13.6"><space unit="char" quantity="12"></space>D’un Dieu qui les reçoit.</l>
					</lg>
					<lg n="14">
						<l n="79" num="14.1">Ces habits sont aussi comme l’expression</l>
						<l n="80" num="14.2">Des plus âpres tourments par qui ma passion</l>
						<l n="81" num="14.3">Pour le salut humain termina ma carrière :</l>
						<l n="82" num="14.4">La croix sur eux empreinte en fait le souvenir,</l>
						<l n="83" num="14.5">Et le prêtre la porte et devant et derrière,</l>
						<l n="84" num="14.6"><space unit="char" quantity="12"></space>Pour mieux le retenir.</l>
					</lg>
					<lg n="15">
						<l n="85" num="15.1">Il la porte devant, afin que son regard</l>
						<l n="86" num="15.2">S’arrêtant fixement sur ce digne étendard,</l>
						<l n="87" num="15.3">Ses ardeurs à le suivre en deviennent plus promptes ;</l>
						<l n="88" num="15.4">Il la porte derrière, afin qu’en ses malheurs</l>
						<l n="89" num="15.5">Il souffre sans ennuis les travaux et les hontes</l>
						<l n="90" num="15.6"><space unit="char" quantity="12"></space>Qui lui viennent d’ailleurs.</l>
					</lg>
					<lg n="16">
						<l n="91" num="16.1">Il la porte devant pour pleurer ses forfaits ;</l>
						<l n="92" num="16.2">Derrière, afin que ceux que son prochain a faits</l>
						<l n="93" num="16.3">De sa compassion tirent aussi des larmes ;</l>
						<l n="94" num="16.4">Et que comme il agit au nom du rédempteur,</l>
						<l n="95" num="16.5">Entre le peuple et Dieu, qui tient en main les armes,</l>
						<l n="96" num="16.6"><space unit="char" quantity="12"></space>Il soit médiateur.</l>
					</lg>
					<lg n="17">
						<l n="97" num="17.1">C’est par cette raison qu’il s’y doit attacher,</l>
						<l n="98" num="17.2">Et que sa fermeté ne doit rien relâcher</l>
						<l n="99" num="17.3">Ni de ses vœux fervents, ni de ses sacrifices,</l>
						<l n="100" num="17.4">Tant qu’il obtienne grâce, et que du souverain</l>
						<l n="101" num="17.5">Il se rende à l’autel les bontés si propices,</l>
						<l n="102" num="17.6"><space unit="char" quantity="12"></space>Qu’il désarme sa main.</l>
					</lg>
					<lg n="18">
						<l n="103" num="18.1">Enfin quand il célèbre, il m’honore, il me sert :</l>
						<l n="104" num="18.2">Tout le ciel applaudit par un sacré concert ;</l>
						<l n="105" num="18.3">Tout l’enfer est confus, l’église édifiée ;</l>
						<l n="106" num="18.4">Il secourt les vivants, des morts il fait la paix,</l>
						<l n="107" num="18.5">Et son âme devient l’heureuse associée</l>
						<l n="108" num="18.6"><space unit="char" quantity="12"></space>Des bons et des parfaits.</l>
					</lg>
				</div></body></text></TEI>