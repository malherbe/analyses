<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Imitation De Jésus-Christ</title>
				<title type="medium">Édition électronique</title>
				<author key="COR">
					<name>
						<forename>Pierre</forename>
						<surname>CORNEILLE</surname>
					</name>
					<date from="1606" to="1684">1606-1684</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>13205 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2016</date>
				<idno type="local">COR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Imitation De Jésus-Christ</title>
						<author>Pierre Corneille</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/pierrecorneilleimmitadiondejesuschrist.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Imitation De Jésus-Christ</title>
						<author>Pierre Corneille</author>
						<editor>M. Ch. Marty-Laveaux</editor>
						<edition>nouvelle édition</edition>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k51199.r=pierre%20corneille?rk=85837;2</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie de L. Hachette et Cie</publisher>
							<date when="1862">1862</date>
						</imprint>
						<biblScope unit="tome">Tome 8</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1656">1656</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les chiffres arabes des livres et chapitres ont été remplacés par des chiffres romains</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">LIVRE DEUXIÈME</head><div type="poem" key="COR65">
					<head type="main">CHAPITRE VIII</head>
					<head type="sub">De l’amitié familière de Jésus-Christ.</head>
					<lg n="1">
						<l n="1" num="1.1">Que ta présence, ô dieu, donne à nos actions</l>
						<l n="2" num="1.2">Sous tes ordres sacrés une vigueur docile !</l>
						<l n="3" num="1.3">Que tout va bien alors ! Que tout semble facile</l>
						<l n="4" num="1.4">À la sainte chaleur de nos intentions !</l>
						<l n="5" num="1.5">Mais quand tu disparois et que ta main puissante</l>
						<l n="6" num="1.6">Avec nos bons desirs n’entre plus au combat,</l>
						<l n="7" num="1.7">Oh ! Que cette vigueur est soudain languissante !</l>
						<l n="8" num="1.8"><space unit="char" quantity="8"></space>Qu’aisément elle s’épouvante,</l>
						<l n="9" num="1.9"><space unit="char" quantity="8"></space>Et qu’un foible ennemi l’abat !</l>
					</lg>
					<lg n="2">
						<l n="10" num="2.1">Les consolations des sens irrésolus</l>
						<l n="11" num="2.2">Tiennent le cœur en trouble et l’âme embarrassée,</l>
						<l n="12" num="2.3">Si Jésus-Christ ne parle au fond de la pensée</l>
						<l n="13" num="2.4">Ce langage secret qu’entendent ses élus ;</l>
						<l n="14" num="2.5">Mais dans nos plus grands maux, à sa moindre parole,</l>
						<l n="15" num="2.6">L’âme prend le dessus de notre infirmité,</l>
						<l n="16" num="2.7">Et le cœur, mieux instruit en cette haute école,</l>
						<l n="17" num="2.8"><space unit="char" quantity="8"></space>Garde un calme qui nous console</l>
						<l n="18" num="2.9"><space unit="char" quantity="8"></space>De toute leur indignité.</l>
					</lg>
					<lg n="3">
						<l n="19" num="3.1">Tu pleurois, Madeleine, et ton frère au tombeau</l>
						<l n="20" num="3.2">Ne souffroit point de trêve à ta douleur fidèle ;</l>
						<l n="21" num="3.3">Mais à peine on te dit : « viens, le maître t’appelle, »</l>
						<l n="22" num="3.4">Que ce mot de tes pleurs fait tarir le ruisseau.</l>
						<l n="23" num="3.5">Tu te lèves, tu pars, et ta douleur suivie</l>
						<l n="24" num="3.6">Des doux empressements d’un amoureux transport,</l>
						<l n="25" num="3.7">Laissant régner la joie en ton âme ravie,</l>
						<l n="26" num="3.8"><space unit="char" quantity="8"></space>Pour chercher l’auteur de la vie,</l>
						<l n="27" num="3.9"><space unit="char" quantity="8"></space>Ne voit plus ce qu’a fait la mort.</l>
					</lg>
					<lg n="4">
						<l n="28" num="4.1">Qu’heureux est ce moment où ce dieu de nos cœurs</l>
						<l n="29" num="4.2">D’un profond déplaisir les élève à la joie !</l>
						<l n="30" num="4.3">Qu’heureux est ce moment où sa bonté déploie</l>
						<l n="31" num="4.4">Sur un gros d’amertume un peu de ses douceurs !</l>
						<l n="32" num="4.5">Sans lui ton âme aride à mille maux t’expose,</l>
						<l n="33" num="4.6">Tu n’es que dureté, qu’impuissance, qu’ennui ;</l>
						<l n="34" num="4.7">Et vraiment fol est l’homme alors qu’il se propose</l>
						<l n="35" num="4.8"><space unit="char" quantity="8"></space>Le vain desir de quelque chose</l>
						<l n="36" num="4.9"><space unit="char" quantity="8"></space>Qu’il faille chercher hors de lui.</l>
					</lg>
					<lg n="5">
						<l n="37" num="5.1">Sais-tu ce que tu perds en son éloignement ?</l>
						<l n="38" num="5.2">Tu perds une présence en vrais biens si féconde,</l>
						<l n="39" num="5.3">Qu’après avoir perdu tous les sceptres du monde,</l>
						<l n="40" num="5.4">Tu perdrois encor plus à la perdre un moment.</l>
						<l n="41" num="5.5">Vois bien ce qu’est ce monde, et te figure stable</l>
						<l n="42" num="5.6">Le plus pompeux éclat qui jamais t’y surprit :</l>
						<l n="43" num="5.7">Que te peut-il donner qui soit considérable,</l>
						<l n="44" num="5.8"><space unit="char" quantity="8"></space>Si les présents dont il t’accable</l>
						<l n="45" num="5.9"><space unit="char" quantity="8"></space>Te séparent de Jésus-Christ ?</l>
					</lg>
					<lg n="6">
						<l n="46" num="6.1">Sa présence est pour nous un charmant paradis,</l>
						<l n="47" num="6.2">C’est un cruel enfer pour nous que son absence,</l>
						<l n="48" num="6.3">Et c’est elle qui fait la plus haute distance</l>
						<l n="49" num="6.4">Du sort des bienheureux à celui des maudits :</l>
						<l n="50" num="6.5">Si tu peux dans sa vue en tous lieux te conduire,</l>
						<l n="51" num="6.6">Tu te mets en état de triompher de tout ;</l>
						<l n="52" num="6.7">Tu n’as plus d’ennemis assez forts pour te nuire,</l>
						<l n="53" num="6.8"><space unit="char" quantity="8"></space>Et s’ils pensent à te détruire,</l>
						<l n="54" num="6.9"><space unit="char" quantity="8"></space>Ils n’en sauroient venir à bout.</l>
					</lg>
					<lg n="7">
						<l n="55" num="7.1">Qui trouve Jésus-Christ trouve un rare trésor,</l>
						<l n="56" num="7.2">Il trouve un bien plus grand que le plus grand empire :</l>
						<l n="57" num="7.3">Qui le perd, perd beaucoup ; et j’ose le redire,</l>
						<l n="58" num="7.4">S’il perdoit tout un monde, il perdroit moins encor.</l>
						<l n="59" num="7.5">Qui le laisse échapper par quelque négligence,</l>
						<l n="60" num="7.6">Regorgeât-il de biens, il est pauvre en effet ;</l>
						<l n="61" num="7.7">Et qui peut avec lui vivre en intelligence,</l>
						<l n="62" num="7.8"><space unit="char" quantity="8"></space>Fût-il noyé dans l’indigence,</l>
						<l n="63" num="7.9"><space unit="char" quantity="8"></space>Il est et riche et satisfait.</l>
					</lg>
					<lg n="8">
						<l n="64" num="8.1">Oh ! Que c’est un grand art que de savoir unir</l>
						<l n="65" num="8.2">Par un saint entretien Jésus à sa foiblesse !</l>
						<l n="66" num="8.3">Oh ! Qu’on a de prudence alors qu’on a l’adresse,</l>
						<l n="67" num="8.4">Quand il entre au dedans, de l’y bien retenir !</l>
						<l n="68" num="8.5">Pour l’attirer chez toi, rends ton âme humble et pure ;</l>
						<l n="69" num="8.6">Sois paisible et dévot, pour l’y voir arrêté ;</l>
						<l n="70" num="8.7">Sa demeure avec nous au zèle se mesure,</l>
						<l n="71" num="8.8"><space unit="char" quantity="8"></space>Et la dévotion assure</l>
						<l n="72" num="8.9"><space unit="char" quantity="8"></space>Ce que gagne l’humilité.</l>
					</lg>
					<lg n="9">
						<l n="73" num="9.1">Mais parmi les douceurs qu’on goûte à l’embrasser</l>
						<l n="74" num="9.2">Il ne faut qu’un moment pour nous ravir sa grâce :</l>
						<l n="75" num="9.3">Pencher vers ces faux biens que le dehors entasse,</l>
						<l n="76" num="9.4">C’est de ton propre cœur toi-même le chasser.</l>
						<l n="77" num="9.5">Que si tu perds l’appui de sa main redoutable,</l>
						<l n="78" num="9.6">Où pourra dans tes maux ton âme avoir recours ?</l>
						<l n="79" num="9.7">Où prendra-t-elle ailleurs un appui véritable,</l>
						<l n="80" num="9.8"><space unit="char" quantity="8"></space>Et qui sera l’ami capable</l>
						<l n="81" num="9.9"><space unit="char" quantity="8"></space>De te prêter quelque secours ?</l>
					</lg>
					<lg n="10">
						<l n="82" num="10.1">Aime : pour vivre heureux il te faut vivre aimé,</l>
						<l n="83" num="10.2">Il te faut des amis qui soient dignes de l’être ;</l>
						<l n="84" num="10.3">Mais si par-dessus eux tu n’aimes ce grand maître,</l>
						<l n="85" num="10.4">Ton cœur d’un long ennui se verra consumé.</l>
						<l n="86" num="10.5">Crois-en ou ta raison ou ton expérience :</l>
						<l n="87" num="10.6">Toutes deux te diront qu’il n’est point d’autre bien,</l>
						<l n="88" num="10.7">Et que c’est au chagrin livrer ta conscience</l>
						<l n="89" num="10.8"><space unit="char" quantity="8"></space>Que prendre joie ou confiance</l>
						<l n="90" num="10.9"><space unit="char" quantity="8"></space>Sur un autre amour que le sien.</l>
					</lg>
					<lg n="11">
						<l n="91" num="11.1">Tu dois plutôt choisir d’attirer sur tes bras</l>
						<l n="92" num="11.2">L’orgueil de tout un monde animé de colère,</l>
						<l n="93" num="11.3">Que d’offenser Jésus, que d’oser lui déplaire,</l>
						<l n="94" num="11.4">Que de vivre un moment et ne le chérir pas.</l>
						<l n="95" num="11.5">Donne-lui tout ton cœur et toutes tes tendresses ;</l>
						<l n="96" num="11.6">Et ne souffrant chez toi personne en même rang,</l>
						<l n="97" num="11.7">Réponds en quelque sorte à ces pleines largesses</l>
						<l n="98" num="11.8"><space unit="char" quantity="8"></space>Qui pour acheter tes caresses</l>
						<l n="99" num="11.9"><space unit="char" quantity="8"></space>Lui firent donner tout son sang.</l>
					</lg>
					<lg n="12">
						<l n="100" num="12.1">Que tous s’entr’aiment donc à cause de Jésus,</l>
						<l n="101" num="12.2">Pour n’aimer que Jésus à cause de lui-même ;</l>
						<l n="102" num="12.3">Rendons cette justice à sa bonté suprême,</l>
						<l n="103" num="12.4">Qui sur tous les amis lui donne le dessus.</l>
						<l n="104" num="12.5">En lui seul, pour lui seul, tous ceux qu’il a fait naître,</l>
						<l n="105" num="12.6">Tant ennemis qu’amis, il les faut tous aimer,</l>
						<l n="106" num="12.7">Et demander pour tous à l’auteur de leur être</l>
						<l n="107" num="12.8"><space unit="char" quantity="8"></space>Et la grâce de le connoître</l>
						<l n="108" num="12.9"><space unit="char" quantity="8"></space>Et l’heur de s’en laisser charmer.</l>
					</lg>
					<lg n="13">
						<l n="109" num="13.1">Ne desire d’amour ni d’estime pour toi</l>
						<l n="110" num="13.2">Qui passant le commun te sépare du reste :</l>
						<l n="111" num="13.3">C’est un droit qui n’est dû qu’à la grandeur céleste</l>
						<l n="112" num="13.4">D’un dieu qui là-haut même est seul égal à soi.</l>
						<l n="113" num="13.5">Ne souhaite régner dans le cœur de personne ;</l>
						<l n="114" num="13.6">Ne fais régner non plus personne dans le tien ;</l>
						<l n="115" num="13.7">Mais qu’au seul Jésus-Christ tout ce cœur s’abandonne,</l>
						<l n="116" num="13.8"><space unit="char" quantity="8"></space>Que Jésus-Christ seul en ordonne</l>
						<l n="117" num="13.9"><space unit="char" quantity="8"></space>Comme chez tous les gens de bien.</l>
					</lg>
					<lg n="14">
						<l n="118" num="14.1">Tire-toi d’esclavage, et sache te purger</l>
						<l n="119" num="14.2">De ces vains embarras que font les créatures ;</l>
						<l n="120" num="14.3">Saches en effacer jusqu’aux moindres teintures,</l>
						<l n="121" num="14.4">Romps jusqu’aux moindres nœuds qui puissent t’engager.</l>
						<l n="122" num="14.5">Dans ce détachement tu trouveras des ailes</l>
						<l n="123" num="14.6">Qui porteront ton cœur jusqu’aux pieds de ton Dieu,</l>
						<l n="124" num="14.7">Pour y voir et goûter ces douceurs immortelles</l>
						<l n="125" num="14.8"><space unit="char" quantity="8"></space>Que dans celui de ses fidèles</l>
						<l n="126" num="14.9"><space unit="char" quantity="8"></space>Sa bonté répand en tout lieu.</l>
					</lg>
					<lg n="15">
						<l n="127" num="15.1">Mais ne crois pas atteindre à cette pureté,</l>
						<l n="128" num="15.2">À moins que de là-haut sa grâce te prévienne,</l>
						<l n="129" num="15.3">À moins qu’elle t’attire, à moins qu’elle soutienne</l>
						<l n="130" num="15.4">Les efforts chancelants de ta légèreté.</l>
						<l n="131" num="15.5">Alors, par le secours de sa pleine efficace,</l>
						<l n="132" num="15.6">Tous autres nœuds brisés, tout autre objet banni,</l>
						<l n="133" num="15.7">Seul hôte de toi-même, et maître de la place,</l>
						<l n="134" num="15.8"><space unit="char" quantity="8"></space>Tu verras cette même grâce</l>
						<l n="135" num="15.9"><space unit="char" quantity="8"></space>T’unir à cet être infini.</l>
					</lg>
					<lg n="16">
						<l n="136" num="16.1">Aussitôt que du ciel dans l’homme elle descend,</l>
						<l n="137" num="16.2">Il n’a plus aucun foible, il peut tout entreprendre ;</l>
						<l n="138" num="16.3">L’impression du bras qui daigne la répandre</l>
						<l n="139" num="16.4">D’infirme qu’il étoit l’a rendu tout-puissant ;</l>
						<l n="140" num="16.5">Mais sitôt que ce bras la retire en arrière,</l>
						<l n="141" num="16.6">L’homme dénué, pauvre, accablé de malheurs,</l>
						<l n="142" num="16.7">Et livré par lui-même à sa foiblesse entière,</l>
						<l n="143" num="16.8"><space unit="char" quantity="8"></space>Semble ne voir plus la lumière</l>
						<l n="144" num="16.9"><space unit="char" quantity="8"></space>Que pour être en proie aux douleurs.</l>
					</lg>
					<lg n="17">
						<l n="145" num="17.1">Ne perds pas toutefois le courage ou l’espoir</l>
						<l n="146" num="17.2">Pour sentir cette grâce ou partie ou moins vive ;</l>
						<l n="147" num="17.3">Mais présente un cœur ferme à tout ce qui t’arrive,</l>
						<l n="148" num="17.4">Et bénis de ton dieu le souverain vouloir.</l>
						<l n="149" num="17.5">Dans quelque excès d’ennuis qu’un tel départ t’engage,</l>
						<l n="150" num="17.6">Souffre tout pour sa gloire attendant le retour,</l>
						<l n="151" num="17.7">Et songe qu’au printemps l’hiver sert de passage,</l>
						<l n="152" num="17.8"><space unit="char" quantity="8"></space>Qu’un profond calme suit l’orage,</l>
						<l n="153" num="17.9"><space unit="char" quantity="8"></space>Et que la nuit fait place au jour.</l>
					</lg>
				</div></body></text></TEI>