<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Imitation De Jésus-Christ</title>
				<title type="medium">Édition électronique</title>
				<author key="COR">
					<name>
						<forename>Pierre</forename>
						<surname>CORNEILLE</surname>
					</name>
					<date from="1606" to="1684">1606-1684</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>13205 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2016</date>
				<idno type="local">COR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Imitation De Jésus-Christ</title>
						<author>Pierre Corneille</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/pierrecorneilleimmitadiondejesuschrist.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Imitation De Jésus-Christ</title>
						<author>Pierre Corneille</author>
						<editor>M. Ch. Marty-Laveaux</editor>
						<edition>nouvelle édition</edition>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k51199.r=pierre%20corneille?rk=85837;2</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie de L. Hachette et Cie</publisher>
							<date when="1862">1862</date>
						</imprint>
						<biblScope unit="tome">Tome 8</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1656">1656</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les chiffres arabes des livres et chapitres ont été remplacés par des chiffres romains</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">LIVRE TROISIÈME</head><div type="poem" key="COR71">
					<head type="main">CHAPITRE II</head>
					<head type="sub">Que la vérité parle au dedans du cœur sans <lb></lb>aucun bruit de paroles.</head>
					<lg n="1">
						<l n="1" num="1.1">Parle, parle, seigneur, ton serviteur écoute :</l>
						<l n="2" num="1.2">Je dis ton serviteur, car enfin je le suis ;</l>
						<l n="3" num="1.3">Je le suis, je veux l’être, et marcher dans ta route</l>
						<l n="4" num="1.4"><space unit="char" quantity="12"></space>Et les jours et les nuits.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">Remplis-moi d’un esprit qui me fasse comprendre</l>
						<l n="6" num="2.2">Ce qu’ordonnent de moi tes saintes volontés,</l>
						<l n="7" num="2.3">Et réduis mes desirs au seul desir d’entendre</l>
						<l n="8" num="2.4"><space unit="char" quantity="12"></space>Tes hautes vérités.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">Mais désarme d’éclairs ta divine éloquence,</l>
						<l n="10" num="3.2">Fais-la couler sans bruit au milieu de mon cœur :</l>
						<l n="11" num="3.3">Qu’elle ait de la rosée et la vive abondance</l>
						<l n="12" num="3.4"><space unit="char" quantity="12"></space>Et l’aimable douceur.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">Vous la craigniez, hébreux, vous croyiez que la foudre,</l>
						<l n="14" num="4.2">Que la mort la suivît, et dût tout désoler,</l>
						<l n="15" num="4.3">Vous qui dans le désert ne pouviez vous résoudre</l>
						<l n="16" num="4.4"><space unit="char" quantity="12"></space>À l’entendre parler.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">« Parle-nous, parle-nous, disiez-vous à Moïse,</l>
						<l n="18" num="5.2">Mais obtiens du seigneur qu’il ne nous parle pas ;</l>
						<l n="19" num="5.3">Des éclats de sa voix la tonnante surprise</l>
						<l n="20" num="5.4"><space unit="char" quantity="12"></space>Seroit notre trépas. »</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1">Je n’ai point ces frayeurs alors que je te prie ;</l>
						<l n="22" num="6.2">Je te fais d’autres vœux que ces fils d’Israël,</l>
						<l n="23" num="6.3">Et plein de confiance, humblement je m’écrie</l>
						<l n="24" num="6.4"><space unit="char" quantity="12"></space>Avec ton Samuel :</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1">« Quoique tu sois le seul qu’ici-bas je redoute,</l>
						<l n="26" num="7.2">C’est toi seul qu’ici-bas je souhaite d’ouïr :</l>
						<l n="27" num="7.3">Parle donc, ô mon dieu ! Ton serviteur écoute,</l>
						<l n="28" num="7.4"><space unit="char" quantity="12"></space>Et te veut obéir. »</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1">Je ne veux ni Moïse à m’enseigner tes voies,</l>
						<l n="30" num="8.2">Ni quelque autre prophète à m’expliquer tes lois ;</l>
						<l n="31" num="8.3">C’est toi qui les instruis, c’est toi qui les envoies,</l>
						<l n="32" num="8.4"><space unit="char" quantity="12"></space>Dont je cherche la voix.</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1">Comme c’est de toi seul qu’ils ont tous ces lumières</l>
						<l n="34" num="9.2">Dont la grâce par eux éclaire notre foi,</l>
						<l n="35" num="9.3">Tu peux bien sans eux tous me les donner entières,</l>
						<l n="36" num="9.4"><space unit="char" quantity="12"></space>Mais eux tous rien sans toi.</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1">Ils peuvent répéter le son de tes paroles,</l>
						<l n="38" num="10.2">Mais il n’est pas en eux d’en conférer l’esprit,</l>
						<l n="39" num="10.3">Et leurs discours sans toi passent pour si frivoles,</l>
						<l n="40" num="10.4"><space unit="char" quantity="12"></space>Que souvent on s’en rit.</l>
					</lg>
					<lg n="11">
						<l n="41" num="11.1">Qu’ils parlent hautement, qu’ils disent des merveilles,</l>
						<l n="42" num="11.2">Qu’ils déclarent ton ordre avec pleine vigueur :</l>
						<l n="43" num="11.3">Si tu ne parles point, ils frappent les oreilles</l>
						<l n="44" num="11.4"><space unit="char" quantity="12"></space>Sans émouvoir le cœur.</l>
					</lg>
					<lg n="12">
						<l n="45" num="12.1">Ils sèment la parole obscure, simple et nue ;</l>
						<l n="46" num="12.2">Mais dans l’obscurité tu rends l’œil clairvoyant,</l>
						<l n="47" num="12.3">Et joins du haut du ciel à la lettre qui tue</l>
						<l n="48" num="12.4"><space unit="char" quantity="12"></space>L’esprit vivifiant.</l>
					</lg>
					<lg n="13">
						<l n="49" num="13.1">Leur bouche sous l’énigme annonce le mystère,</l>
						<l n="50" num="13.2">Mais tu nous en fais voir le sens le plus caché :</l>
						<l n="51" num="13.3">Ils nous prêchent tes lois, mais ton secours fait faire</l>
						<l n="52" num="13.4"><space unit="char" quantity="12"></space>Tout ce qu’ils ont prêché.</l>
					</lg>
					<lg n="14">
						<l n="53" num="14.1">Ils montrent le chemin, mais tu donnes la force</l>
						<l n="54" num="14.2">D’y porter tous nos pas, d’y marcher jusqu’au bout ;</l>
						<l n="55" num="14.3">Et tout ce qui vient d’eux ne passe point l’écorce,</l>
						<l n="56" num="14.4"><space unit="char" quantity="12"></space>Mais tu pénètres tout.</l>
					</lg>
					<lg n="15">
						<l n="57" num="15.1">Ils n’arrosent sans toi que les dehors de l’âme,</l>
						<l n="58" num="15.2">Mais sa fécondité veut ton bras souverain ;</l>
						<l n="59" num="15.3">Et tout ce qui l’éclaire, et tout ce qui l’enflamme</l>
						<l n="60" num="15.4"><space unit="char" quantity="12"></space>Ne part que de ta main.</l>
					</lg>
					<lg n="16">
						<l n="61" num="16.1">Ces prophètes enfin ont beau crier et dire :</l>
						<l n="62" num="16.2">Ce ne sont que des voix, ce ne sont que des cris,</l>
						<l n="63" num="16.3">Si pour en profiter l’esprit qui les inspire</l>
						<l n="64" num="16.4"><space unit="char" quantity="12"></space>Ne touche nos esprits.</l>
					</lg>
					<lg n="17">
						<l n="65" num="17.1">Silence donc, Moïse ! Et toi, parle en sa place,</l>
						<l n="66" num="17.2">Éternelle, immuable, immense vérité ;</l>
						<l n="67" num="17.3">Parle, que je ne meure enfoncé dans la glace</l>
						<l n="68" num="17.4"><space unit="char" quantity="12"></space>De ma stérilité.</l>
					</lg>
					<lg n="18">
						<l n="69" num="18.1">C’est mourir en effet, qu’à ta faveur céleste</l>
						<l n="70" num="18.2">Ne rendre point pour fruit des desirs plus ardents ;</l>
						<l n="71" num="18.3">Et l’avis du dehors n’a rien que de funeste</l>
						<l n="72" num="18.4"><space unit="char" quantity="12"></space>S’il n’échauffe au dedans.</l>
					</lg>
					<lg n="19">
						<l n="73" num="19.1">Cet avis écouté seulement par caprice,</l>
						<l n="74" num="19.2">Connu sans être aimé, cru sans être observé,</l>
						<l n="75" num="19.3">C’est ce qui vraiment tue, et sur quoi ta justice</l>
						<l n="76" num="19.4"><space unit="char" quantity="12"></space>Condamne un réprouvé.</l>
					</lg>
					<lg n="20">
						<l n="77" num="20.1">Parle donc, ô mon dieu ! Ton serviteur fidèle</l>
						<l n="78" num="20.2">Pour écouter ta voix réunit tous ses sens,</l>
						<l n="79" num="20.3">Et trouve les douceurs de la vie éternelle</l>
						<l n="80" num="20.4"><space unit="char" quantity="12"></space>En ses divins accents.</l>
					</lg>
					<lg n="21">
						<l n="81" num="21.1">Parle pour consoler mon âme inquiétée ;</l>
						<l n="82" num="21.2">Parle pour la conduire à quelque amendement ;</l>
						<l n="83" num="21.3">Parle, afin que ta gloire ainsi plus exaltée</l>
						<l n="84" num="21.4"><space unit="char" quantity="12"></space>Croisse éternellement.</l>
					</lg>
				</div></body></text></TEI>