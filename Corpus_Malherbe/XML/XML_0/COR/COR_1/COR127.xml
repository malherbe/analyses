<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Imitation De Jésus-Christ</title>
				<title type="medium">Édition électronique</title>
				<author key="COR">
					<name>
						<forename>Pierre</forename>
						<surname>CORNEILLE</surname>
					</name>
					<date from="1606" to="1684">1606-1684</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>13205 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2016</date>
				<idno type="local">COR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Imitation De Jésus-Christ</title>
						<author>Pierre Corneille</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/pierrecorneilleimmitadiondejesuschrist.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Imitation De Jésus-Christ</title>
						<author>Pierre Corneille</author>
						<editor>M. Ch. Marty-Laveaux</editor>
						<edition>nouvelle édition</edition>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k51199.r=pierre%20corneille?rk=85837;2</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie de L. Hachette et Cie</publisher>
							<date when="1862">1862</date>
						</imprint>
						<biblScope unit="tome">Tome 8</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1656">1656</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les chiffres arabes des livres et chapitres ont été remplacés par des chiffres romains</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">LIVRE TROISIÈME</head><div type="poem" key="COR127">
					<head type="main">CHAPITRE LVIII</head>
					<head type="sub">Qu’il ne faut point vouloir pénétrer les hauts <lb></lb>mystères, ni examiner les secrets jugements de Dieu.</head>
					<lg n="1">
						<l n="1" num="1.1">N’abuse point, mon fils, de tes foibles lumières</l>
						<l n="2" num="1.2">Jusqu’à vouloir percer les plus hautes matières,</l>
						<l n="3" num="1.3">Jusqu’à vouloir entrer dans les profonds secrets</l>
						<l n="4" num="1.4">De l’inégal dehors de mes justes décrets ;</l>
						<l n="5" num="1.5">Ne cherche point à voir quelle raison pressante</l>
						<l n="6" num="1.6">Fait que ma grâce agit ou paroît impuissante,</l>
						<l n="7" num="1.7">Est avare ou prodigue, abandonne ou soutient ;</l>
						<l n="8" num="1.8">N’examine jamais d’où ce partage vient,</l>
						<l n="9" num="1.9">Ni pourquoi l’un ainsi languit dans la misère,</l>
						<l n="10" num="1.10">Et que l’autre est si haut au-dessus du vulgaire :</l>
						<l n="11" num="1.11">Il n’est raisonnement, il n’est effort humain</l>
						<l n="12" num="1.12">Qui puisse pénétrer mon ordre souverain,</l>
						<l n="13" num="1.13">Ni s’éclaircir au vrai par la longue dispute</l>
						<l n="14" num="1.14">D’où vient que je caresse ou que je persécute.</l>
					</lg>
					<lg n="2">
						<l n="15" num="2.1">Quand le vieil ennemi fait ces suggestions,</l>
						<l n="16" num="2.2">Qu’un esprit curieux émeut ces questions,</l>
						<l n="17" num="2.3">Au lieu de perdre temps à leur vouloir répondre,</l>
						<l n="18" num="2.4">Lève les yeux au ciel, et dis pour les confondre :</l>
						<l n="19" num="2.5">« Seigneur, vous êtes juste en tous vos jugements,</l>
						<l n="20" num="2.6">La vérité préside à vos discernements,</l>
						<l n="21" num="2.7">Et l’équité qui règne en vos ordres suprêmes</l>
						<l n="22" num="2.8">Les rend toujours en eux justifiés d’eux-mêmes :</l>
						<l n="23" num="2.9">Qu’il leur plaise abaisser, qu’il leur plaise agrandir,</l>
						<l n="24" num="2.10">On doit trembler sous eux, sans les approfondir,</l>
						<l n="25" num="2.11">Et jamais sans folie on ne peut l’entreprendre,</l>
						<l n="26" num="2.12">Puisque l’esprit humain ne les sauroit comprendre. »</l>
						<l n="27" num="2.13">Ne t’informe non plus qui des saints m’est aux cieux</l>
						<l n="28" num="2.14">Le plus considérable ou le moins précieux,</l>
						<l n="29" num="2.15">Et ne conteste point sur la prééminence</l>
						<l n="30" num="2.16">Que de leur sainteté mérite l’excellence.</l>
						<l n="31" num="2.17">Ces curiosités sont autant d’attentats,</l>
						<l n="32" num="2.18">Qui ne font qu’exciter d’inutiles débats,</l>
						<l n="33" num="2.19">Enfler les cœurs d’orgueil, brouiller les fantaisies,</l>
						<l n="34" num="2.20">Jusqu’aux dissensions pousser les jalousies,</l>
						<l n="35" num="2.21">Lorsque de part et d’autre un cœur passionné</l>
						<l n="36" num="2.22">À préférer son saint porte un zèle obstiné.</l>
					</lg>
					<lg n="3">
						<l n="37" num="3.1">Les contestations de ces recherches vaines</l>
						<l n="38" num="3.2">Ne laissent aucun fruit après beaucoup de peines :</l>
						<l n="39" num="3.3">Ce n’est que se gêner d’un frivole souci,</l>
						<l n="40" num="3.4">Et l’on déplaît aux saints quand on les loue ainsi.</l>
						<l n="41" num="3.5">Jamais avec ce feu mon esprit ne s’accorde :</l>
						<l n="42" num="3.6">Je suis le dieu de paix, et non pas de discorde ;</l>
						<l n="43" num="3.7">Et cette paix consiste en vraie humilité,</l>
						<l n="44" num="3.8">Plus qu’aux vaines douceurs d’avoir tout emporté.</l>
					</lg>
					<lg n="4">
						<l n="45" num="4.1">Je sais qu’en bien des cœurs souvent le zèle imprime</l>
						<l n="46" num="4.2">Pour tel ou tel des saints plus d’ardeur et d’estime ;</l>
						<l n="47" num="4.3">Mais cette ardeur, ce zèle, et cette estime enfin,</l>
					</lg>
					<lg n="5">
						<l n="48" num="5.1">Partent d’un mouvement plus humain que divin.</l>
						<l n="49" num="5.2">C’est de moi seul qu’au ciel ils tiennent tous leur place :</l>
						<l n="50" num="5.3">Je leur donne la gloire, et leur donnai la grâce ;</l>
						<l n="51" num="5.4">Je connois leur mérite, et les ai prévenus</l>
						<l n="52" num="5.5">Par un épanchement de trésors inconnus,</l>
						<l n="53" num="5.6">De bénédictions, de douceurs toujours prêtes</l>
						<l n="54" num="5.7">À redoubler leur force au milieu des tempêtes.</l>
					</lg>
					<lg n="6">
						<l n="55" num="6.1">Je n’ai point attendu la naissance des temps</l>
						<l n="56" num="6.2">Pour chérir mes élus, et les juger constants.</l>
						<l n="57" num="6.3">De toute éternité ma claire prescience</l>
						<l n="58" num="6.4">A su se faire jour dedans leur conscience ;</l>
						<l n="59" num="6.5">De toute éternité j’ai vu tout leur emploi,</l>
						<l n="60" num="6.6">Et j’ai fait choix d’eux tous, et non pas eux de moi.</l>
					</lg>
					<lg n="7">
						<l n="61" num="7.1">Ma grâce les appelle à mon céleste empire,</l>
						<l n="62" num="7.2">Et ma miséricorde après moi les attire ;</l>
						<l n="63" num="7.3">Ma main les a conduits par les tentations ;</l>
						<l n="64" num="7.4">Je les ai remplis seul de consolations ;</l>
						<l n="65" num="7.5">Je leur ai donné seul de la persévérance,</l>
						<l n="66" num="7.6">Et seul j’ai couronné leur humble patience.</l>
					</lg>
					<lg n="8">
						<l n="67" num="8.1">Ainsi je les connois du premier au dernier ;</l>
						<l n="68" num="8.2">Ainsi j’ai pour eux tous un amour singulier ;</l>
						<l n="69" num="8.3">Ainsi de ce qu’ils sont la louange m’est due ;</l>
						<l n="70" num="8.4">Toute la gloire ainsi m’en doit être rendue ;</l>
						<l n="71" num="8.5">Ainsi par-dessus tout doit être en eux béni,</l>
						<l n="72" num="8.6">Par-dessus tout vanté mon amour infini,</l>
						<l n="73" num="8.7">Qui pour montrer l’excès de sa magnificence,</l>
						<l n="74" num="8.8">Les élève à ce point de gloire et de puissance,</l>
						<l n="75" num="8.9">Et sans qu’aucun mérite en eux ait précédé,</l>
						<l n="76" num="8.10">Les prédestine au rang que je leur ai gardé.</l>
					</lg>
					<lg n="9">
						<l n="77" num="9.1">Qui méprise le moindre au plus grand fait outrage,</l>
						<l n="78" num="9.2">Parce que de ma main l’un et l’autre est l’ouvrage :</l>
						<l n="79" num="9.3">On ôte à leur auteur tout ce qu’on ôte à l’un ;</l>
						<l n="80" num="9.4">On l’ôte à tout le reste, et l’opprobre est commun.</l>
						<l n="81" num="9.5">L’ardente charité qui ne fait d’eux qu’une âme</l>
						<l n="82" num="9.6">Les unit tous entre eux par des liens de flamme :</l>
						<l n="83" num="9.7">Tous n’ont qu’un sentiment et qu’une volonté ;</l>
						<l n="84" num="9.8">Tous s’entr’aiment en un par cette charité.</l>
					</lg>
					<lg n="10">
						<l n="85" num="10.1">Je dirai davantage : ils m’aiment plus qu’eux-mêmes ;</l>
						<l n="86" num="10.2">Ravis au-dessus d’eux vers mes bontés suprêmes,</l>
						<l n="87" num="10.3">Après avoir banni la propre affection,</l>
						<l n="88" num="10.4">Ils s’abîment entiers dans ma dilection,</l>
						<l n="89" num="10.5">Et de l’objet aimé possédants la présence,</l>
						<l n="90" num="10.6">Ils trouvent leur repos dans cette jouissance.</l>
						<l n="91" num="10.7">Rien d’un si digne amour ne les peut détourner ;</l>
						<l n="92" num="10.8">Rien vers d’autres objets ne les peut ramener :</l>
						<l n="93" num="10.9">L’immense vérité dont leurs âmes sont pleines</l>
						<l n="94" num="10.10">Par sa vive lumière entretient dans leurs veines</l>
						<l n="95" num="10.11">Et de la charité l’inextinguible feu,</l>
						<l n="96" num="10.12">Et de toute autre ardeur un constant désaveu.</l>
					</lg>
					<lg n="11">
						<l n="97" num="11.1">Que ces hommes charnels, que ces âmes brutales</l>
						<l n="98" num="11.2">Qui leur osent donner des places inégales,</l>
						<l n="99" num="11.3">Ces cœurs qui n’ont pour but que les plaisirs mondains,</l>
						<l n="100" num="11.4">Cessent de discourir de l’état de mes saints.</l>
						<l n="101" num="11.5">L’ardeur qu’ils ont pour eux, ou foible, ou véhémente,</l>
						<l n="102" num="11.6">Au gré de son caprice ôte, déguise, augmente,</l>
						<l n="103" num="11.7">Sans consulter jamais sur leur félicité</l>
						<l n="104" num="11.8">La voix de ma sagesse et de ma vérité.</l>
					</lg>
					<lg n="12">
						<l n="105" num="12.1">L’ignorance en plusieurs fait ce mauvais partage</l>
						<l n="106" num="12.2">Qu’ils font entre mes saints de mon propre héritage,</l>
						<l n="107" num="12.3">Surtout en ces esprits foiblement éclairés,</l>
						<l n="108" num="12.4">Qui de leur propre amour encor mal séparés,</l>
						<l n="109" num="12.5">Ont peine à conserver dans une âme charnelle</l>
						<l n="110" num="12.6">Une dilection toute spirituelle.</l>
						<l n="111" num="12.7">Le penchant naturel de l’humaine amitié</l>
						<l n="112" num="12.8">De leur zèle imprudent fait plus de la moitié :</l>
						<l n="113" num="12.9">Comme ils n’en forment point que leurs sens n’examinent,</l>
						<l n="114" num="12.10">Ce qui se passe en bas, en haut ils l’imaginent,</l>
						<l n="115" num="12.11">Et tel que sur la terre en est l’ordre et le cours,</l>
						<l n="116" num="12.12">Tel le présume au ciel leur aveugle discours.</l>
						<l n="117" num="12.13">Cependant la distance en est incomparable,</l>
						<l n="118" num="12.14">Et pour les imparfaits est si peu concevable,</l>
						<l n="119" num="12.15">Que des illuminés la spéculation</l>
						<l n="120" num="12.16">N’atteint point jusque-là sans révélation.</l>
					</lg>
					<lg n="13">
						<l n="121" num="13.1">Garde bien donc, mon fils, par trop de confiance,</l>
						<l n="122" num="13.2">De sonder des secrets qui passent ta science ;</l>
						<l n="123" num="13.3">Ne porte point si haut ton esprit curieux,</l>
						<l n="124" num="13.4">Et sans vouloir régler le rang qu’on tient aux cieux,</l>
						<l n="125" num="13.5">Réunis seulement tes soins et ta lumière</l>
						<l n="126" num="13.6">Pour y trouver ta place, et fût-ce la dernière.</l>
					</lg>
					<lg n="14">
						<l n="127" num="14.1">Quand tu pourrois connoître avec pleine clarté</l>
						<l n="128" num="14.2">Quels saints en mon royaume ont plus de dignité,</l>
						<l n="129" num="14.3">De quoi t’en serviroit l’entière connoissance,</l>
						<l n="130" num="14.4">Si tu n’en devenois plus humble en ma présence,</l>
						<l n="131" num="14.5">Et si tu n’en prenois une plus forte ardeur</l>
						<l n="132" num="14.6">À publier ma gloire, et bénir ma grandeur ?</l>
					</lg>
					<lg n="15">
						<l n="133" num="15.1">Vois ton peu de mérite et l’excès de tes crimes ;</l>
						<l n="134" num="15.2">Et si tu peux des saints voir les vertus sublimes,</l>
						<l n="135" num="15.3">Vois combien tes défauts et ton manque de soin</l>
						<l n="136" num="15.4">De leur perfection te laissent encor loin :</l>
						<l n="137" num="15.5">Tu feras beaucoup mieux que celui qui conteste</l>
						<l n="138" num="15.6">Touchant leur préférence au royaume céleste,</l>
						<l n="139" num="15.7">Et sur l’emportement de son esprit malsain</l>
						<l n="140" num="15.8">Du moindre et du plus grand décide en souverain.</l>
					</lg>
					<lg n="16">
						<l n="141" num="16.1">Oui, mon fils, il vaut mieux leur rendre tes hommages,</l>
						<l n="142" num="16.2">Les yeux baignés de pleurs implorer leurs suffrages,</l>
						<l n="143" num="16.3">Mendier leur secours, leur offrir d’humbles vœux,</l>
						<l n="144" num="16.4">Que de juger ainsi de leurs secrets et d’eux.</l>
					</lg>
					<lg n="17">
						<l n="145" num="17.1">Puisqu’ils ont tous au ciel de quoi se satisfaire,</l>
						<l n="146" num="17.2">Que les hommes en terre apprennent à se taire,</l>
						<l n="147" num="17.3">Et donnent une bride à la témérité</l>
						<l n="148" num="17.4">Où de leurs vains discours va l’importunité.</l>
					</lg>
					<lg n="18">
						<l n="149" num="18.1">Les saints ont du mérite, et n’en font point de gloire ;</l>
						<l n="150" num="18.2">Ils ne se donnent point l’honneur de leur victoire :</l>
						<l n="151" num="18.3">Comme de mes trésors tout leur bien est sorti,</l>
						<l n="152" num="18.4">Et que ma charité leur a tout départi,</l>
						<l n="153" num="18.5">Ils rapportent le tout au pouvoir adorable</l>
						<l n="154" num="18.6">De cette charité pour eux inépuisable.</l>
					</lg>
					<lg n="19">
						<l n="155" num="19.1">Ils ont un tel amour pour ma divinité,</l>
						<l n="156" num="19.2">Un tel ravissement de ma bénignité,</l>
						<l n="157" num="19.3">Que cette sainte joie en vrais plaisirs féconde,</l>
						<l n="158" num="19.4">Qui toujours les remplit et toujours surabonde,</l>
						<l n="159" num="19.5">Par un regorgement qu’on ne peut expliquer,</l>
						<l n="160" num="19.6">Fait que rien ne leur manque, et ne leur peut manquer.</l>
					</lg>
					<lg n="20">
						<l n="161" num="20.1">Plus ils sont élevés dans ma gloire suprême,</l>
						<l n="162" num="20.2">Plus leur esprit soumis se ravale en lui-même,</l>
						<l n="163" num="20.3">Et mon amour par là redoublant ses attraits,</l>
						<l n="164" num="20.4">Le plus humble d’entre eux m’approche de plus près.</l>
						<l n="165" num="20.5">Aussi devant l’éclat qui partout m’environne</l>
						<l n="166" num="20.6">L’écriture t’apprend qu’ils baissent leur couronne,</l>
						<l n="167" num="20.7">Qu’ils tombent sur leur face aux pieds du saint agneau</l>
						<l n="168" num="20.8">Qui daigna de son sang racheter le troupeau,</l>
						<l n="169" num="20.9">Et qu’ainsi prosternés ils adorent sans cesse</l>
						<l n="170" num="20.10">Du Dieu toujours vivant l’éternelle sagesse.</l>
					</lg>
					<lg n="21">
						<l n="171" num="21.1">Plusieurs veulent savoir ce que chaque saint vaut,</l>
						<l n="172" num="21.2">Et qui d’eux tient au ciel le grade le plus haut,</l>
						<l n="173" num="21.3">Qui sont mal assurés s’ils pourront les y joindre,</l>
						<l n="174" num="21.4">Et s’ils mériteront d’être reçus au moindre.</l>
						<l n="175" num="21.5">C’est beaucoup de se voir le dernier en un lieu</l>
						<l n="176" num="21.6">Où tous sont grands, tous rois, tous vrais enfants de Dieu.</l>
						<l n="177" num="21.7">Le moindre y vaut plus seul que mille rois en terre,</l>
						<l n="178" num="21.8">Et l’orgueil de cent ans frappé de mon tonnerre</l>
						<l n="179" num="21.9">N’a de part qu’au séjour de l’éternelle mort</l>
						<l n="180" num="21.10">Qui du plus vieux pécheur doit terminer le sort.</l>
					</lg>
					<lg n="22">
						<l n="181" num="22.1">Ainsi je dis moi-même autrefois aux apôtres :</l>
						<l n="182" num="22.2">« Si vous voulez au ciel être au-dessus des autres,</l>
						<l n="183" num="22.3">Sachez qu’auparavant il faut se convertir,</l>
						<l n="184" num="22.4">Qu’il faut s’humilier, qu’il faut s’anéantir,</l>
						<l n="185" num="22.5">Se ranger aussi bas que cette foible enfance</l>
						<l n="186" num="22.6">Qui vit soumise à tous par sa propre impuissance :</l>
						<l n="187" num="22.7">Autrement, point d’accès au royaume des cieux.</l>
						<l n="188" num="22.8">Oui, ce petit enfant qui se traîne à vos yeux</l>
						<l n="189" num="22.9">De votre humilité doit être la mesure :</l>
						<l n="190" num="22.10">Rendez-vous ses égaux, ma gloire vous est sûre ;</l>
						<l n="191" num="22.11">L’amour vous y conduit, et l’espoir, et la foi ;</l>
						<l n="192" num="22.12">Mais le plus humble enfin est le plus grand chez moi. »</l>
					</lg>
					<lg n="23">
						<l n="193" num="23.1">Voyez donc, orgueilleux, quelle est votre disgrâce :</l>
						<l n="194" num="23.2">Bien que le ciel soit haut, la porte en est si basse,</l>
						<l n="195" num="23.3">Qu’elle en ferme l’entrée à ceux qui sont trop grands</l>
						<l n="196" num="23.4">Pour se pouvoir réduire à l’égal des enfants.</l>
					</lg>
					<lg n="24">
						<l n="197" num="24.1">Malheur encore à vous, riches pour qui le monde</l>
						<l n="198" num="24.2">En consolations de tous côtés abonde !</l>
						<l n="199" num="24.3">Les pauvres entreront, cependant qu’au dehors</l>
						<l n="200" num="24.4">Vos larmes et vos cris feront de vains efforts.</l>
						<l n="201" num="24.5">Humble, réjouis-toi ; pauvres, prenez courage :</l>
						<l n="202" num="24.6">Le royaume du ciel est votre heureux partage ;</l>
						<l n="203" num="24.7">Il l’est, si toutefois dans votre humilité</l>
						<l n="204" num="24.8">Vous pouvez jusqu’au bout marcher en vérité.</l>
					</lg>
				</div></body></text></TEI>