<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Imitation De Jésus-Christ</title>
				<title type="medium">Édition électronique</title>
				<author key="COR">
					<name>
						<forename>Pierre</forename>
						<surname>CORNEILLE</surname>
					</name>
					<date from="1606" to="1684">1606-1684</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>13205 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2016</date>
				<idno type="local">COR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Imitation De Jésus-Christ</title>
						<author>Pierre Corneille</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/pierrecorneilleimmitadiondejesuschrist.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Imitation De Jésus-Christ</title>
						<author>Pierre Corneille</author>
						<editor>M. Ch. Marty-Laveaux</editor>
						<edition>nouvelle édition</edition>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k51199.r=pierre%20corneille?rk=85837;2</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie de L. Hachette et Cie</publisher>
							<date when="1862">1862</date>
						</imprint>
						<biblScope unit="tome">Tome 8</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1656">1656</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les chiffres arabes des livres et chapitres ont été remplacés par des chiffres romains</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">LIVRE DEUXIÈME</head><div type="poem" key="COR59">
					<head type="main">CHAPITRE II</head>
					<head type="sub">De l’humble soumission.</head>
					<lg n="1">
						<l n="1" num="1.1"><space unit="char" quantity="8"></space>Ne te mets pas beaucoup en peine</l>
						<l n="2" num="1.2"><space unit="char" quantity="8"></space>De toute la nature humaine</l>
						<l n="3" num="1.3">Qui t’aime ou qui te hait, qui te nuit ou te sert :</l>
						<l n="4" num="1.4">Va jusqu’au créateur, mets ton soin à lui plaire,</l>
						<l n="5" num="1.5"><space unit="char" quantity="12"></space>Quoi que tu veuilles faire ;</l>
						<l n="6" num="1.6">Et s’il est avec toi, marche à front découvert.</l>
					</lg>
					<lg n="2">
						<l n="7" num="2.1"><space unit="char" quantity="8"></space>La bonne et saine conscience</l>
						<l n="8" num="2.2"><space unit="char" quantity="8"></space>A toujours Dieu pour sa défense,</l>
						<l n="9" num="2.3">De qui le ferme appui l’empêche de trembler,</l>
						<l n="10" num="2.4">Et reçoit de son bras une si forte garde</l>
						<l n="11" num="2.5"><space unit="char" quantity="12"></space>Quand son œil la regarde,</l>
						<l n="12" num="2.6">Qu’il n’est point de méchant qui la puisse accabler.</l>
					</lg>
					<lg n="3">
						<l n="13" num="3.1"><space unit="char" quantity="8"></space>Quoi qu’il t’arrive de contraire,</l>
						<l n="14" num="3.2"><space unit="char" quantity="8"></space>Apprends à souffrir, à te taire,</l>
						<l n="15" num="3.3">Et tu verras sur toi le secours du seigneur :</l>
						<l n="16" num="3.4">Il a pour t’affranchir mille routes diverses,</l>
						<l n="17" num="3.5"><space unit="char" quantity="12"></space>Et sait dans ces traverses</l>
						<l n="18" num="3.6">Quand et comme il en faut adoucir la rigueur.</l>
					</lg>
					<lg n="4">
						<l n="19" num="4.1"><space unit="char" quantity="8"></space>C’est en sa main forte et bénigne</l>
						<l n="20" num="4.2"><space unit="char" quantity="8"></space>Qu’il faut que l’homme se résigne,</l>
						<l n="21" num="4.3">Quelques maux qu’il prévoie ou puisse ressentir ;</l>
						<l n="22" num="4.4">À lui seul appartient de nous donner de l’aide ;</l>
						<l n="23" num="4.5"><space unit="char" quantity="12"></space>À lui seul le remède</l>
						<l n="24" num="4.6">Qui de confusion nous peut tous garantir.</l>
					</lg>
					<lg n="5">
						<l n="25" num="5.1"><space unit="char" quantity="8"></space>Cependant ce qu’un autre blâme</l>
						<l n="26" num="5.2"><space unit="char" quantity="8"></space>Des taches qui souillent notre âme,</l>
						<l n="27" num="5.3">Souvent assure en nous la vraie humilité :</l>
						<l n="28" num="5.4">Souvent le vain orgueil par là se déracine,</l>
						<l n="29" num="5.5"><space unit="char" quantity="12"></space>L’amour-propre se mine,</l>
						<l n="30" num="5.6">Et fait place aux vertus avec facilité.</l>
					</lg>
					<lg n="6">
						<l n="31" num="6.1"><space unit="char" quantity="8"></space>L’homme qui soi-même s’abaisse,</l>
						<l n="32" num="6.2"><space unit="char" quantity="8"></space>Par l’humble aveu de sa foiblesse,</l>
						<l n="33" num="6.3">Des plus justes fureurs rompt aisément les coups,</l>
						<l n="34" num="6.4">Et satisfait sur l’heure avec si peu de peine,</l>
						<l n="35" num="6.5"><space unit="char" quantity="12"></space>Que la plus âpre haine</l>
						<l n="36" num="6.6">Ne sauroit contre lui conserver de courroux.</l>
					</lg>
					<lg n="7">
						<l n="37" num="7.1"><space unit="char" quantity="8"></space>L’humble seul vit comme il faut vivre :</l>
						<l n="38" num="7.2"><space unit="char" quantity="8"></space>Dieu le protége et le délivre ;</l>
						<l n="39" num="7.3">Il l’aime et le console à chaque événement ;</l>
						<l n="40" num="7.4">Il descend jusqu’à lui pour lui montrer ses traces ;</l>
						<l n="41" num="7.5"><space unit="char" quantity="12"></space>Il le comble de grâces,</l>
						<l n="42" num="7.6">Et l’élève à la gloire après l’abaissement.</l>
					</lg>
					<lg n="8">
						<l n="43" num="8.1"><space unit="char" quantity="8"></space>Il répand sur lui ses lumières</l>
						<l n="44" num="8.2"><space unit="char" quantity="8"></space>Et les connoissances entières</l>
						<l n="45" num="8.3">De ses plus merveilleux et plus profonds secrets ;</l>
						<l n="46" num="8.4">Il l’invite, il l’attire à ce bonheur extrême,</l>
						<l n="47" num="8.5"><space unit="char" quantity="12"></space>Et l’attache à soi-même</l>
						<l n="48" num="8.6">Par la profusion de ses plus doux attraits.</l>
					</lg>
					<lg n="9">
						<l n="49" num="9.1"><space unit="char" quantity="8"></space>L’humble ainsi trouve tout facile,</l>
						<l n="50" num="9.2"><space unit="char" quantity="8"></space>Toujours content, toujours tranquille,</l>
						<l n="51" num="9.3">Quelque confusion qu’il lui faille essuyer ;</l>
						<l n="52" num="9.4">Et comme c’est en Dieu que son repos se fonde</l>
						<l n="53" num="9.5"><space unit="char" quantity="12"></space>Sur le mépris du monde,</l>
						<l n="54" num="9.6">En Dieu malgré le monde il le sait appuyer.</l>
					</lg>
					<lg n="10">
						<l n="55" num="10.1"><space unit="char" quantity="8"></space>Enfin c’est par là qu’on profite,</l>
						<l n="56" num="10.2"><space unit="char" quantity="8"></space>C’est par là que le vrai mérite</l>
						<l n="57" num="10.3">Au reste des vertus se laisse dispenser.</l>
						<l n="58" num="10.4">Quelque éclat qu’à leur prix les tiennes puissent joindre,</l>
						<l n="59" num="10.5"><space unit="char" quantity="12"></space>Tiens-toi de tous le moindre,</l>
						<l n="60" num="10.6">Ou dans le bon chemin ne crois point avancer.</l>
					</lg>
				</div></body></text></TEI>