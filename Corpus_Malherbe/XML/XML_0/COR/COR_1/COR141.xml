<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Imitation De Jésus-Christ</title>
				<title type="medium">Édition électronique</title>
				<author key="COR">
					<name>
						<forename>Pierre</forename>
						<surname>CORNEILLE</surname>
					</name>
					<date from="1606" to="1684">1606-1684</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>13205 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2016</date>
				<idno type="local">COR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Imitation De Jésus-Christ</title>
						<author>Pierre Corneille</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/pierrecorneilleimmitadiondejesuschrist.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Imitation De Jésus-Christ</title>
						<author>Pierre Corneille</author>
						<editor>M. Ch. Marty-Laveaux</editor>
						<edition>nouvelle édition</edition>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k51199.r=pierre%20corneille?rk=85837;2</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie de L. Hachette et Cie</publisher>
							<date when="1862">1862</date>
						</imprint>
						<biblScope unit="tome">Tome 8</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1656">1656</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les chiffres arabes des livres et chapitres ont été remplacés par des chiffres romains</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">LIVRE QUATRIÈME</head><div type="poem" key="COR141">
					<head type="main">CHAPITRE XII</head>
					<head type="sub">Qu’il faut se préparer avec grand soin <lb></lb>à la communion.</head>
					<lg n="1">
						<l n="1" num="1.1">J’aime la pureté par-dessus toute chose :</l>
						<l n="2" num="1.2">Je cherche le cœur net, c’est là que je repose ;</l>
						<l n="3" num="1.3">C’est moi qui donne ici toute la sainteté,</l>
						<l n="4" num="1.4">Et j’en fais bonne part à cette pureté.</l>
						<l n="5" num="1.5">Je l’ai dit autrefois, et je te le répète :</l>
						<l n="6" num="1.6">« Prépare en ta maison une salle bien nette,</l>
						<l n="7" num="1.7">Et nous viendrons soudain, mes disciples et moi,</l>
						<l n="8" num="1.8">Y célébrer la pâque, et la faire avec toi. »</l>
					</lg>
					<lg n="2">
						<l n="9" num="2.1">Si tu veux que j’y vienne établir ma demeure,</l>
						<l n="10" num="2.2">Purge ce vieux levain qui s’enfle d’heure en heure,</l>
						<l n="11" num="2.3">Et par l’austérité d’une sainte rigueur</l>
						<l n="12" num="2.4">Sache purifier le séjour de ton cœur :</l>
						<l n="13" num="2.5">Des vanités du monde exclus-en les tumultes ;</l>
						<l n="14" num="2.6">Des folles passions bannis-en les insultes ;</l>
						<l n="15" num="2.7">Tiens-y-toi solitaire, et tel qu’un passereau</l>
						<l n="16" num="2.8">Qui d’un arbre écarté s’est choisi le coupeau,</l>
						<l n="17" num="2.9">Repasse en ton esprit avec mille amertumes</l>
						<l n="18" num="2.10">Et tes honteux défauts et tes lâches coutumes.</l>
						<l n="19" num="2.11">Quiconque pour un autre a quelque affection</l>
						<l n="20" num="2.12">Prépare un digne lieu pour sa réception,</l>
						<l n="21" num="2.13">Et le soin qu’il en prend est d’autant plus extrême</l>
						<l n="22" num="2.14">Que par là cet ami juge à quel point on l’aime.</l>
					</lg>
					<lg n="3">
						<l n="23" num="3.1">Mais ne présume pas qu’il soit en ton pouvoir</l>
						<l n="24" num="3.2">Par ta propre vertu de me bien recevoir,</l>
						<l n="25" num="3.3">Ni que ton plus grand soin ait en soi le mérite</l>
						<l n="26" num="3.4">De m’apprêter un lieu digne que je l’habite.</l>
						<l n="27" num="3.5">Quand durant tout le temps qu’à tes jours j’ai prescrit</l>
						<l n="28" num="3.6">Il ne te passeroit autre chose en l’esprit,</l>
						<l n="29" num="3.7">Tu verrois que l’esprit qu’une vie y dispose,</l>
						<l n="30" num="3.8">Si je n’y mets la main, ne fait que peu de chose.</l>
					</lg>
					<lg n="4">
						<l n="31" num="4.1">Ma bonté qui t’invite à ce divin repas</l>
						<l n="32" num="4.2">T’y permet un accès qu’elle ne te doit pas ;</l>
						<l n="33" num="4.3">Et comme à cette table elle seule t’appelle,</l>
						<l n="34" num="4.4">Lorsque je t’y reçois, je ne regarde qu’elle.</l>
						<l n="35" num="4.5">Viens-y, mais seulement en me remerciant,</l>
						<l n="36" num="4.6">Tel qu’à celle d’un roi se sied un mendiant,</l>
						<l n="37" num="4.7">Qui n’ayant rien d’égal à de si hautes grâces,</l>
						<l n="38" num="4.8">S’humilie à ses pieds, en adore les traces,</l>
						<l n="39" num="4.9">Et lui fait ce qu’il peut de rétributions</l>
						<l n="40" num="4.10">Par ses remercîments et ses submissions.</l>
					</lg>
					<lg n="5">
						<l n="41" num="5.1">Viens-y, non par coutume, ou par quelque contrainte,</l>
						<l n="42" num="5.2">Mais avec du respect, mais avec de la crainte,</l>
						<l n="43" num="5.3">Mais avec de l’amour, mais avec de la foi,</l>
						<l n="44" num="5.4">Fais avec diligence autant qu’il est en toi ;</l>
						<l n="45" num="5.5">Viens ainsi, prends ainsi le corps d’un dieu qui t’aime,</l>
						<l n="46" num="5.6">Et que tu dois aimer au delà de toi-même.</l>
					</lg>
					<lg n="6">
						<l n="47" num="6.1">Il veut loger en toi, lui qui remplit les cieux ;</l>
						<l n="48" num="6.2">Il descend jusqu’à toi pour t’encourager mieux ;</l>
						<l n="49" num="6.3">Lui-même il te convie à ce banquet céleste ;</l>
						<l n="50" num="6.4">Lui-même il te l’ordonne, et suppléera le reste :</l>
						<l n="51" num="6.5">Si tes défauts sont grands, plus grand est son pouvoir ;</l>
						<l n="52" num="6.6">Approche en confiance, et viens le recevoir.</l>
					</lg>
					<lg n="7">
						<l n="53" num="7.1">Si tu sens qu’un beau feu fonde ta vieille glace,</l>
						<l n="54" num="7.2">Rends grâces à ce dieu qui te fait cette grâce ;</l>
						<l n="55" num="7.3">Non qu’il t’ait pu devoir une telle amitié,</l>
						<l n="56" num="7.4">Mais parce que son œil te regarde en pitié.</l>
						<l n="57" num="7.5">Si ton zèle au contraire impuissant ou languide</l>
						<l n="58" num="7.6">De moment en moment te laisse plus aride,</l>
						<l n="59" num="7.7">Redouble ta prière et tes gémissements</l>
						<l n="60" num="7.8">Pour arracher de lui de meilleurs sentiments :</l>
						<l n="61" num="7.9">Persévère, importune, obstine-toi de sorte</l>
						<l n="62" num="7.10">À pleurer à ses pieds, à frapper à sa porte,</l>
						<l n="63" num="7.11">Qu’il t’ouvre, ou que du moins de ce bien souverain</l>
						<l n="64" num="7.12">Il laisse distiller quelque goutte en ton sein.</l>
					</lg>
					<lg n="8">
						<l n="65" num="8.1">Cette importunité n’est jamais incivile :</l>
						<l n="66" num="8.2">Je te suis nécessaire et tu m’es inutile ;</l>
						<l n="67" num="8.3">Tu ne viens pas à moi pour me sanctifier,</l>
						<l n="68" num="8.4">Mais je m’abaisse à toi pour te justifier,</l>
						<l n="69" num="8.5">Pour te combler de biens, pour te donner la voie</l>
						<l n="70" num="8.6">De croître ton bonheur et d’affermir ta joie.</l>
						<l n="71" num="8.7">Tu viens à mon banquet pour en sortir plus saint,</l>
						<l n="72" num="8.8">Pour rallumer en toi la ferveur qui s’éteint,</l>
						<l n="73" num="8.9">Pour mieux t’unir à moi d’une chaîne éternelle,</l>
						<l n="74" num="8.10">Pour recevoir d’en haut une grâce nouvelle,</l>
						<l n="75" num="8.11">Et pour voir naître en toi de son épanchement</l>
						<l n="76" num="8.12">De plus pressants desirs pour ton amendement.</l>
						<l n="77" num="8.13">Garde de négliger une faveur si grande,</l>
						<l n="78" num="8.14">Tiens-lui ton cœur ouvert, fais-m’en entière offrande ;</l>
						<l n="79" num="8.15">Et m’ayant dignement préparé ce séjour,</l>
						<l n="80" num="8.16">Introduis-y l’objet de ton céleste amour.</l>
					</lg>
					<lg n="9">
						<l n="81" num="9.1">Mais ce n’est pas assez d’y préparer ton âme</l>
						<l n="82" num="9.2">Avec toute l’ardeur d’une céleste flamme :</l>
						<l n="83" num="9.3">Si pour l’y disposer il faut beaucoup de soins,</l>
						<l n="84" num="9.4">Le sacrement reçu n’en demande pas moins,</l>
						<l n="85" num="9.5">Et le recueillement après ce grand remède</l>
						<l n="86" num="9.6">Doit égaler du moins l’ardeur qui le précède.</l>
						<l n="87" num="9.7">Oui, la retraite sainte après le sacrement</l>
						<l n="88" num="9.8">Est un sublime apprêt pour le redoublement,</l>
						<l n="89" num="9.9">Et la communion où la ferveur abonde</l>
						<l n="90" num="9.10">À de plus grands effets prépare la seconde.</l>
					</lg>
					<lg n="10">
						<l n="91" num="10.1">Qui trop tôt s’y relâche en perd soudain le fruit,</l>
						<l n="92" num="10.2">Et se dispose mal à celle qui la suit.</l>
						<l n="93" num="10.3">Tiens-toi dans le silence, et rentre dans toi-même,</l>
						<l n="94" num="10.4">Pour jouir en secret de ce bonheur suprême :</l>
						<l n="95" num="10.5">Si tu sais une fois l’art de le conserver,</l>
						<l n="96" num="10.6">Le monde tout entier ne t’en sauroit priver.</l>
						<l n="97" num="10.7">Mais il faut qu’à moi seul ton cœur entier se donne,</l>
						<l n="98" num="10.8">Pour vivre plus en moi qu’en ta propre personne,</l>
						<l n="99" num="10.9">Sans que tout l’univers sous aucunes couleurs</l>
						<l n="100" num="10.10">T’inquiète l’esprit pour ce qui vient d’ailleurs.</l>
					</lg>
				</div></body></text></TEI>