<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Imitation De Jésus-Christ</title>
				<title type="medium">Édition électronique</title>
				<author key="COR">
					<name>
						<forename>Pierre</forename>
						<surname>CORNEILLE</surname>
					</name>
					<date from="1606" to="1684">1606-1684</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>13205 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2016</date>
				<idno type="local">COR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Imitation De Jésus-Christ</title>
						<author>Pierre Corneille</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/pierrecorneilleimmitadiondejesuschrist.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Imitation De Jésus-Christ</title>
						<author>Pierre Corneille</author>
						<editor>M. Ch. Marty-Laveaux</editor>
						<edition>nouvelle édition</edition>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k51199.r=pierre%20corneille?rk=85837;2</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie de L. Hachette et Cie</publisher>
							<date when="1862">1862</date>
						</imprint>
						<biblScope unit="tome">Tome 8</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1656">1656</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les chiffres arabes des livres et chapitres ont été remplacés par des chiffres romains</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">LIVRE TROISIÈME</head><div type="poem" key="COR107">
					<head type="main">CHAPITRE XXXVIII</head>
					<head type="sub">De la bonne conduite aux choses extérieures, <lb></lb>et du recours à Dieu dans les périls.</head>
					<lg n="1">
						<l n="1" num="1.1">Quelque chose, mon fils, qui t’occupe au dehors,</l>
						<l n="2" num="1.2">Conserve le dedans vraiment libre et tranquille,</l>
						<l n="3" num="1.3">Et te souviens toujours que de ces deux trésors</l>
						<l n="4" num="1.4">La conquête est pénible, et la perte facile.</l>
						<l n="5" num="1.5">En tout temps, en tous lieux, en toutes actions,</l>
						<l n="6" num="1.6">Ce digne épurement de tes intentions</l>
						<l n="7" num="1.7">Doit garder sur toi-même une puissance égale,</l>
						<l n="8" num="1.8">T’élever au-dessus de tous les biens humains,</l>
						<l n="9" num="1.9">Sans permettre jamais que ton cœur se ravale</l>
						<l n="10" num="1.10">Sous l’objet de tes yeux, ou l’œuvre de tes mains.</l>
					</lg>
					<lg n="2">
						<l n="11" num="2.1">Ainsi, maître absolu de tout ce que tu fais,</l>
						<l n="12" num="2.2">Et non plus de tes sens le sujet ou l’esclave,</l>
						<l n="13" num="2.3">Tu te verras partout affranchi pour jamais</l>
						<l n="14" num="2.4">De ce qui t’importune et de ce qui te brave.</l>
						<l n="15" num="2.5">Tu quitteras l’Égypte en véritable hébreu,</l>
						<l n="16" num="2.6">Qu’à travers les déserts la colonne de feu</l>
						<l n="17" num="2.7">Guide, sans s’égarer, vers la terre promise ;</l>
						<l n="18" num="2.8">Et de tous ennemis tes exploits triomphants</l>
						<l n="19" num="2.9">Passeront, en dépit de toute leur surprise,</l>
						<l n="20" num="2.10">Au partage que Dieu destine à ses enfants.</l>
					</lg>
					<lg n="3">
						<l n="21" num="3.1">Mais ces enfants de Dieu, sais-tu bien ce qu’ils sont ?</l>
						<l n="22" num="3.2">Pour être de leur rang, sais-tu ce qu’il faut être ?</l>
						<l n="23" num="3.3">Sais-tu quelle est leur vie, et quels projets ils font ?</l>
						<l n="24" num="3.4">À quelle digne marque il te les faut connoître ?</l>
						<l n="25" num="3.5">De tout ce qui du siècle attire l’amitié</l>
						<l n="26" num="3.6">Ces esprits épurés se font un marchepied,</l>
						<l n="27" num="3.7">Pour voir d’autant plus près l’éclat des biens célestes ;</l>
						<l n="28" num="3.8">Et leur constance est telle à conduire leurs yeux,</l>
						<l n="29" num="3.9">Que quoi qui se présente à leurs regards modestes,</l>
						<l n="30" num="3.10">Le gauche est pour la terre, et le droit pour les cieux.</l>
					</lg>
					<lg n="4">
						<l n="31" num="4.1">Bien loin que des objets le dangereux attrait</l>
						<l n="32" num="4.2">Jusqu’à l’attachement abaisse leur courage,</l>
						<l n="33" num="4.3">Ils savent ramener par un contraire effet</l>
						<l n="34" num="4.4">Leur plus flatteuse amorce au bon et saint usage :</l>
						<l n="35" num="4.5">En vain un vieil abus en grossit le pouvoir ;</l>
						<l n="36" num="4.6">Ils savent les réduire au sincère devoir</l>
						<l n="37" num="4.7">Que l’auteur souverain leur a voulu prescrire ;</l>
						<l n="38" num="4.8">Et comme en faisant tout il n’a rien négligé,</l>
						<l n="39" num="4.9">Ils savent rejeter sous un si juste empire</l>
						<l n="40" num="4.10">Tout ce qu’un long désordre en auroit dégagé.</l>
					</lg>
					<lg n="5">
						<l n="41" num="5.1">Tiens-toi ferme au-dessus de tous événements :</l>
						<l n="42" num="5.2">Que leur extérieur ne puisse te surprendre ;</l>
						<l n="43" num="5.3">Et jamais de ta chair ne prends les sentiments</l>
						<l n="44" num="5.4">Sur ce qu’on te fait voir, ou qu’on te fait entendre.</l>
						<l n="45" num="5.5">De peur d’être ébloui par leur illusion,</l>
						<l n="46" num="5.6">Fais ainsi que Moïse à chaque occasion,</l>
						<l n="47" num="5.7">Viens consulter ton Dieu sur toute ta conduite :</l>
						<l n="48" num="5.8">Sa réponse souvent daignera t’éclairer,</l>
						<l n="49" num="5.9">Et tu n’en sortiras que l’âme mieux instruite</l>
						<l n="50" num="5.10">De tout ce qui se passe, ou qu’il faut espérer.</l>
					</lg>
					<lg n="6">
						<l n="51" num="6.1">Ce grand législateur qui publioit mes lois</l>
						<l n="52" num="6.2">Ainsi sur chaque doute entroit au tabernacle,</l>
						<l n="53" num="6.3">Sur chaque question il écoutoit ma voix,</l>
						<l n="54" num="6.4">Et mes avis reçus, il prononçoit l’oracle.</l>
						<l n="55" num="6.5">De quelques grands périls qu’il fût embarrassé,</l>
						<l n="56" num="6.6">Quelques séditions dont il se vît pressé,</l>
						<l n="57" num="6.7">Il fit de l’oraison son recours ordinaire :</l>
						<l n="58" num="6.8">Entre, entre à son exemple au cabinet du cœur,</l>
						<l n="59" num="6.9">Et pour tirer de moi le conseil nécessaire,</l>
						<l n="60" num="6.10">Du zèle en tes besoins redouble la ferveur.</l>
					</lg>
					<lg n="7">
						<l n="61" num="7.1">Josué son disciple, et les fils d’Israël</l>
						<l n="62" num="7.2">Dont l’imprudence aveugle excéda ces limites,</l>
						<l n="63" num="7.3">Pour n’avoir pas ainsi consulté l’éternel,</l>
						<l n="64" num="7.4">Se virent abusés par les gabaonites :</l>
						<l n="65" num="7.5">Le flatteur apparat d’un discours affecté,</l>
						<l n="66" num="7.6">S’étant saisi d’abord de leur crédulité,</l>
						<l n="67" num="7.7">Mit la compassion où la haine étoit due ;</l>
						<l n="68" num="7.8">Ils perdirent des biens qui leur étoient promis,</l>
						<l n="69" num="7.9">Et le charme imposteur de leur pitié déçue</l>
						<l n="70" num="7.10">Dedans leur propre sein sauva leurs ennemis.</l>
					</lg>
				</div></body></text></TEI>