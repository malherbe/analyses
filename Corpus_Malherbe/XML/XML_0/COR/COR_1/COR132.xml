<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Imitation De Jésus-Christ</title>
				<title type="medium">Édition électronique</title>
				<author key="COR">
					<name>
						<forename>Pierre</forename>
						<surname>CORNEILLE</surname>
					</name>
					<date from="1606" to="1684">1606-1684</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>13205 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2016</date>
				<idno type="local">COR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Imitation De Jésus-Christ</title>
						<author>Pierre Corneille</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/pierrecorneilleimmitadiondejesuschrist.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Imitation De Jésus-Christ</title>
						<author>Pierre Corneille</author>
						<editor>M. Ch. Marty-Laveaux</editor>
						<edition>nouvelle édition</edition>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k51199.r=pierre%20corneille?rk=85837;2</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie de L. Hachette et Cie</publisher>
							<date when="1862">1862</date>
						</imprint>
						<biblScope unit="tome">Tome 8</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1656">1656</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les chiffres arabes des livres et chapitres ont été remplacés par des chiffres romains</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">LIVRE QUATRIÈME</head><div type="poem" key="COR132">
					<head type="main">CHAPITRE III</head>
					<head type="sub">Qu’il est utile de communier souvent.</head>
					<lg n="1">
						<l n="1" num="1.1">Je viens à toi, seigneur, afin de m’enrichir</l>
						<l n="2" num="1.2">Des dons surnaturels qu’il te plaît de nous faire ;</l>
						<l n="3" num="1.3">J’en viens chercher la joie, afin de m’affranchir</l>
						<l n="4" num="1.4">Des longs et noirs chagrins qui suivent ma misère.</l>
						<l n="5" num="1.5">Je cours à ce banquet que ta pleine douceur</l>
						<l n="6" num="1.6"><space unit="char" quantity="8"></space>Tient prêt pour le pauvre pécheur ;</l>
						<l n="7" num="1.7">Je ne puis, je ne dois souhaiter autre chose :</l>
						<l n="8" num="1.8">Toi seul es mon salut et ma rédemption ;</l>
						<l n="9" num="1.9">En toi tout mon espoir se fonde et se repose,</l>
						<l n="10" num="1.10">Tout mon bonheur en toi voit sa perfection.</l>
					</lg>
					<lg n="2">
						<l n="11" num="2.1">Je n’ai point ici-bas d’autre gloire à chercher ;</l>
						<l n="12" num="2.2">Je n’ai point d’autre force en qui prendre assurance ;</l>
						<l n="13" num="2.3">Je n’ai point d’autres biens où je puisse attacher</l>
						<l n="14" num="2.4">La juste ambition de ma persévérance.</l>
						<l n="15" num="2.5">Comble donc aujourd’hui de solides plaisirs</l>
						<l n="16" num="2.6"><space unit="char" quantity="8"></space>Ce cœur, ces amoureux desirs,</l>
						<l n="17" num="2.7">Que pousse jusqu’à toi ton serviteur fidèle :</l>
						<l n="18" num="2.8">Vois les empressements de son humble devoir,</l>
						<l n="19" num="2.9">Et ne rejette pas cette ardeur de son zèle,</l>
						<l n="20" num="2.10">Qu’un vrai respect prépare à te bien recevoir.</l>
					</lg>
					<lg n="3">
						<l n="21" num="3.1">Entre dans ma maison, où j’ose t’inviter ;</l>
						<l n="22" num="3.2">Répands-y les douceurs de ta vertu cachée ;</l>
						<l n="23" num="3.3">Que de ta propre main je puisse mériter</l>
						<l n="24" num="3.4">D’être à jamais béni comme un autre Zachée.</l>
						<l n="25" num="3.5">Daigne m’admettre au rang, par ce comble de biens,</l>
						<l n="26" num="3.6"><space unit="char" quantity="8"></space>Des fils d’Abraham et des tiens :</l>
						<l n="27" num="3.7">C’est le plus cher desir, c’est le seul qui m’enflamme ;</l>
						<l n="28" num="3.8">Et comme tout mon cœur soupire après ton corps,</l>
						<l n="29" num="3.9">Comme il le reconnoît pour sa véritable âme,</l>
						<l n="30" num="3.10">Mon âme pour s’y joindre unit tous ses efforts.</l>
					</lg>
					<lg n="4">
						<l n="31" num="4.1">Donne-toi donc, seigneur, donne-toi tout à moi ;</l>
						<l n="32" num="4.2">Par ce don précieux dégage ta parole :</l>
						<l n="33" num="4.3">Tu me suffiras seul, je trouve tout en toi ;</l>
						<l n="34" num="4.4">Mais sans toi je n’ai rien qui m’aide ou me console.</l>
						<l n="35" num="4.5">Sans toi je ne puis vivre, et tout autre soutien</l>
						<l n="36" num="4.6"><space unit="char" quantity="8"></space>N’est qu’un vain appui, qu’un faux bien ;</l>
						<l n="37" num="4.7">Je ne puis subsister sans tes douces visites ;</l>
						<l n="38" num="4.8">Et mes propres langueurs m’abattroient en chemin,</l>
						<l n="39" num="4.9">Si je me confiois à mon peu de mérites,</l>
						<l n="40" num="4.10">Sans recourir souvent à ce mets tout divin.</l>
					</lg>
					<lg n="5">
						<l n="41" num="5.1">Souviens-toi que ce peuple à qui dans les déserts</l>
						<l n="42" num="5.2">Ta sagesse elle-même annonçoit tes oracles,</l>
						<l n="43" num="5.3">Guéri qu’il fut par toi de mille maux divers,</l>
						<l n="44" num="5.4">Vit ta pitié s’étendre à de plus grands miracles :</l>
						<l n="45" num="5.5">De crainte qu’au retour il ne languît de faim,</l>
						<l n="46" num="5.6"><space unit="char" quantity="8"></space>Tu lui multiplias le pain ;</l>
						<l n="47" num="5.7">Seigneur, fais-en de même avec ta créature,</l>
						<l n="48" num="5.8">Toi qui, pour consoler un peuple mieux aimé,</l>
						<l n="49" num="5.9">Lui veux bien chaque jour servir de nourriture</l>
						<l n="50" num="5.10">Sous les dehors d’un pain où tu t’es enfermé.</l>
					</lg>
					<lg n="6">
						<l n="51" num="6.1">Quiconque en ces bas lieux te reçoit dignement,</l>
						<l n="52" num="6.2">Pain vivant, doux repas de l’âme du fidèle,</l>
						<l n="53" num="6.3">S’établit un partage au haut du firmament,</l>
						<l n="54" num="6.4">Et s’assure un plein droit à la gloire éternelle.</l>
						<l n="55" num="6.5">Mais las ! Que je suis loin d’un état si parfait,</l>
						<l n="56" num="6.6"><space unit="char" quantity="8"></space>Moi que souvent le moindre attrait</l>
						<l n="57" num="6.7">Jusque dans le péché traîne sans répugnance,</l>
						<l n="58" num="6.8">Et qu’une lenteur morne, un sommeil croupissant,</l>
						<l n="59" num="6.9">Tiennent enveloppé de tant de nonchalance,</l>
						<l n="60" num="6.10">Qu’à tous les bons effets je demeure impuissant !</l>
					</lg>
					<lg n="7">
						<l n="61" num="7.1">C’est là ce qui m’impose une nécessité</l>
						<l n="62" num="7.2">De porter, et souvent, mes pleurs aux pieds d’un prêtre ;</l>
						<l n="63" num="7.3">D’élever, et souvent, mes vœux vers ta bonté,</l>
						<l n="64" num="7.4">De recevoir souvent le vrai corps de mon maître.</l>
						<l n="65" num="7.5">Je dois, je dois souvent renouveler mon cœur,</l>
						<l n="66" num="7.6"><space unit="char" quantity="8"></space>Combattre ma vieille langueur,</l>
						<l n="67" num="7.7">Purifier mon âme en ce banquet céleste,</l>
						<l n="68" num="7.8">De peur qu’enseveli sous l’indigne repos</l>
						<l n="69" num="7.9">Où plonge d’un tel bien l’abstinence funeste,</l>
						<l n="70" num="7.10">Je n’échappe à toute heure à tous mes bons propos.</l>
					</lg>
					<lg n="8">
						<l n="71" num="8.1">Notre imbécillité, maîtresse de nos sens,</l>
						<l n="72" num="8.2">Conserve en tous les cœurs un tel penchant aux vices,</l>
						<l n="73" num="8.3">Que l’homme tout entier dès ses plus jeunes ans</l>
						<l n="74" num="8.4">Glisse et court aisément vers leurs molles délices.</l>
						<l n="75" num="8.5">S’il n’avoit ton secours contre tous leurs assauts,</l>
						<l n="76" num="8.6"><space unit="char" quantity="8"></space>Chaque moment croîtroit ses maux :</l>
						<l n="77" num="8.7">C’est la communion qui seule l’en dégage ;</l>
						<l n="78" num="8.8">C’est elle qui lui prête un assuré soutien,</l>
						<l n="79" num="8.9">Dissipe sa paresse, anime son courage,</l>
						<l n="80" num="8.10">Le retire du mal, et l’affermit au bien.</l>
					</lg>
					<lg n="9">
						<l n="81" num="9.1">Si telle est ma foiblesse et ma trépidité</l>
						<l n="82" num="9.2">Au milieu d’un secours de puissance infinie,</l>
						<l n="83" num="9.3">Si j’ai tant de langueur et tant d’aridité</l>
						<l n="84" num="9.4">Alors que je célèbre ou que je communie,</l>
						<l n="85" num="9.5">En quel abîme, ô Dieu, serois-je tôt réduit,</l>
						<l n="86" num="9.6"><space unit="char" quantity="8"></space>Si j’osois me priver du fruit</l>
						<l n="87" num="9.7">Que tu m’offres toi-même en ce divin remède !</l>
						<l n="88" num="9.8">Et dessous quels malheurs me verrois-je abattu,</l>
						<l n="89" num="9.9">Si j’osois me trahir jusqu’à refuser l’aide</l>
						<l n="90" num="9.10">Que ta main y présente à mon peu de vertu !</l>
					</lg>
					<lg n="10">
						<l n="91" num="10.1">Certes, si je ne puis me trouver chaque jour</l>
						<l n="92" num="10.2">En état de t’offrir cet auguste mystère,</l>
						<l n="93" num="10.3">Du moins de temps en temps l’effort de mon amour</l>
						<l n="94" num="10.4">Tâchera d’avoir part à ce don salutaire.</l>
						<l n="95" num="10.5">Tant que l’âme gémit sous l’exil ennuyeux</l>
						<l n="96" num="10.6"><space unit="char" quantity="8"></space>Qui l’emprisonne en ces bas lieux,</l>
						<l n="97" num="10.7">Ce qui plus la console est ta sainte mémoire,</l>
						<l n="98" num="10.8">La repasser souvent, et d’un zèle enflammé,</l>
						<l n="99" num="10.9">Qui n’a point d’autre objet que celui de ta gloire,</l>
						<l n="100" num="10.10">S’unir par ce grand œuvre à son cher bien-aimé.</l>
					</lg>
					<lg n="11">
						<l n="101" num="11.1">Ô merveilleux effet de ton amour pour nous,</l>
						<l n="102" num="11.2">Que toi, source de vie, et première des causes,</l>
						<l n="103" num="11.3">Le créateur de tout, le rédempteur de tous,</l>
						<l n="104" num="11.4">Le souverain arbitre enfin de toutes choses,</l>
						<l n="105" num="11.5">Tu daignes ravaler cette immense grandeur</l>
						<l n="106" num="11.6"><space unit="char" quantity="8"></space>Jusqu’à venir vers un pécheur,</l>
						<l n="107" num="11.7">Jusqu’à le visiter, homme et dieu tout ensemble !</l>
						<l n="108" num="11.8">Tu descends jusqu’à lui pour le rassasier,</l>
						<l n="109" num="11.9">Par un abaissement devant qui le ciel tremble,</l>
						<l n="110" num="11.10">D’un homme tout ensemble et d’un dieu tout entier !</l>
					</lg>
					<lg n="12">
						<l n="111" num="12.1">Heureuse mille fois l’âme qui te reçoit,</l>
						<l n="112" num="12.2">Toi, son espoir unique et son unique maître,</l>
						<l n="113" num="12.3">Avec tous les respects et l’amour qu’elle doit</l>
						<l n="114" num="12.4">À l’excès des bontés que tu lui fais paroître !</l>
						<l n="115" num="12.5">Est-il bouche éloquente, est-il esprit humain</l>
						<l n="116" num="12.6"><space unit="char" quantity="8"></space>Qui ne se consumât en vain</l>
						<l n="117" num="12.7">S’il vouloit exprimer toute son allégresse ?</l>
						<l n="118" num="12.8">Et peut-on concevoir ces hauts ravissements,</l>
						<l n="119" num="12.9">Ces avant-goûts du ciel, que ta pleine tendresse</l>
						<l n="120" num="12.10">Aime à lui prodiguer en ces heureux moments ?</l>
					</lg>
					<lg n="13">
						<l n="121" num="13.1">Qu’elle reçoit alors pour hôte un grand seigneur !</l>
						<l n="122" num="13.2">Qu’elle en prend à bon titre une joie infinie,</l>
						<l n="123" num="13.3">Et brave de ses maux la plus âpre rigueur,</l>
						<l n="124" num="13.4">Voyant l’auteur des biens lui faire compagnie !</l>
						<l n="125" num="13.5">Qu’elle se souvient peu du temps qu’elle a gémi,</l>
						<l n="126" num="13.6"><space unit="char" quantity="8"></space>Quand elle loge un tel ami !</l>
						<l n="127" num="13.7">Qu’elle trouve d’attraits en l’époux qu’elle embrasse !</l>
						<l n="128" num="13.8">Qu’il est grand, qu’il est noble, et digne d’être aimé,</l>
						<l n="129" num="13.9">Puisqu’il n’a rien en soi dont le lustre n’efface</l>
						<l n="130" num="13.10">Tout ce dont ici-bas le desir est charmé !</l>
					</lg>
					<lg n="14">
						<l n="131" num="14.1">Que la terre et les cieux et tout leur ornement</l>
						<l n="132" num="14.2">Apprennent à se taire en ta sainte présence :</l>
						<l n="133" num="14.3">Tout ce qui brille en eux le plus pompeusement</l>
						<l n="134" num="14.4">Vient des profusions de ta magnificence ;</l>
						<l n="135" num="14.5">Tout ce qu’ils ont de beau, tout ce qu’ils ont de bon,</l>
						<l n="136" num="14.6"><space unit="char" quantity="8"></space>Jamais des grandeurs de ton nom</l>
						<l n="137" num="14.7">Ne pourra nous tracer qu’une foible peinture :</l>
						<l n="138" num="14.8">Ta sagesse éternelle a ses trésors à part,</l>
						<l n="139" num="14.9">Le nombre en est sans nombre ainsi que sans mesure,</l>
						<l n="140" num="14.10">Et ne met point de borne aux biens qu’elle départ.</l>
					</lg>
				</div></body></text></TEI>