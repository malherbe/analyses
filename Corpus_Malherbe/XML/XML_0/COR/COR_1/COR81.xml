<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Imitation De Jésus-Christ</title>
				<title type="medium">Édition électronique</title>
				<author key="COR">
					<name>
						<forename>Pierre</forename>
						<surname>CORNEILLE</surname>
					</name>
					<date from="1606" to="1684">1606-1684</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>13205 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2016</date>
				<idno type="local">COR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Imitation De Jésus-Christ</title>
						<author>Pierre Corneille</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/pierrecorneilleimmitadiondejesuschrist.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Imitation De Jésus-Christ</title>
						<author>Pierre Corneille</author>
						<editor>M. Ch. Marty-Laveaux</editor>
						<edition>nouvelle édition</edition>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k51199.r=pierre%20corneille?rk=85837;2</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie de L. Hachette et Cie</publisher>
							<date when="1862">1862</date>
						</imprint>
						<biblScope unit="tome">Tome 8</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1656">1656</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les chiffres arabes des livres et chapitres ont été remplacés par des chiffres romains</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">LIVRE TROISIÈME</head><div type="poem" key="COR81">
					<head type="main">CHAPITRE XII</head>
					<head type="sub">Comme il se faut faire à la patience, et combattre <lb></lb>les passions.</head>
					<lg n="1">
						<l n="1" num="1.1"><space unit="char" quantity="8"></space>À ce que je puis voir, seigneur,</l>
						<l n="2" num="1.2"><space unit="char" quantity="8"></space>J’ai grand besoin de patience</l>
						<l n="3" num="1.3"><space unit="char" quantity="8"></space>Contre la rude expérience</l>
						<l n="4" num="1.4"><space unit="char" quantity="8"></space>Où cette vie engage un cœur.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><space unit="char" quantity="8"></space>Elle n’est qu’un gouffre de maux,</l>
						<l n="6" num="2.2"><space unit="char" quantity="8"></space>D’accidents fâcheux et contraires,</l>
						<l n="7" num="2.3"><space unit="char" quantity="8"></space>Qu’un accablement de misères,</l>
						<l n="8" num="2.4"><space unit="char" quantity="8"></space>D’où naissent travaux sur travaux.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><space unit="char" quantity="8"></space>Je n’y termine aucuns combats</l>
						<l n="10" num="3.2"><space unit="char" quantity="8"></space>Que chaque instant ne renouvelle,</l>
						<l n="11" num="3.3"><space unit="char" quantity="8"></space>Et ma paix y traîne avec elle</l>
						<l n="12" num="3.4"><space unit="char" quantity="8"></space>La guerre attachée à mes pas.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><space unit="char" quantity="8"></space>Les soins même de l’affermir</l>
						<l n="14" num="4.2"><space unit="char" quantity="8"></space>Ne sont en effet qu’une guerre,</l>
						<l n="15" num="4.3"><space unit="char" quantity="8"></space>Et tout mon séjour sur la terre</l>
						<l n="16" num="4.4"><space unit="char" quantity="8"></space>Qu’une occasion de gémir.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">Tu dis vrai, mon enfant ; aussi ne veux-je pas</l>
						<l n="18" num="5.2">Que tu cherches en terre une paix sans combats,</l>
						<l n="19" num="5.3">Un repos sans tumulte, un calme sans orage,</l>
						<l n="20" num="5.4">Où toujours la fortune ait un même visage,</l>
						<l n="21" num="5.5">Et semble par le cours de ses événements</l>
						<l n="22" num="5.6">S’asservir en esclave à tes contentements.</l>
						<l n="23" num="5.7">Je veux te voir en paix, mais parmi les traverses,</l>
						<l n="24" num="5.8">Parmi les changements des fortunes diverses ;</l>
						<l n="25" num="5.9">Je veux y voir ton calme, et que l’adversité</l>
						<l n="26" num="5.10">Te serve à t’affermir dans la tranquillité.</l>
						<l n="27" num="5.11">« Tu ne peux, me dis-tu, souffrir beaucoup de choses ;</l>
						<l n="28" num="5.12">En vain tu t’y résous, en vain tu t’y disposes,</l>
						<l n="29" num="5.13">Tu sens une révolte en ton cœur mutiné</l>
						<l n="30" num="5.14">Contre la patience où tu l’as condamné. »</l>
						<l n="31" num="5.15">Lâche, qu’oses-tu dire ? Ainsi le purgatoire,</l>
						<l n="32" num="5.16">Ainsi ses feux cuisants sont hors de ta mémoire ?</l>
						<l n="33" num="5.17">Auras-tu plus de force ? Ou les présumes-tu</l>
						<l n="34" num="5.18">Plus aisés à souffrir à ce cœur abattu ?</l>
						<l n="35" num="5.19">Apprends que de deux maux il faut choisir le moindre,</l>
						<l n="36" num="5.20">Que tes soins en ce but se doivent tous rejoindre,</l>
						<l n="37" num="5.21">Et que pour éviter les tourments éternels,</l>
						<l n="38" num="5.22">Tu dois traiter tes sens d’infâmes criminels,</l>
						<l n="39" num="5.23">Braver leurs appétits, leur imposer des gênes,</l>
						<l n="40" num="5.24">Préparer ta constance aux misères humaines,</l>
						<l n="41" num="5.25">Les souffrir sans murmure, et recevoir les croix</l>
						<l n="42" num="5.26">Ainsi que des faveurs qui viennent de mon choix.</l>
					</lg>
					<lg n="6">
						<l n="43" num="6.1">Crois-tu les gens du monde exempts d’inquiétude ?</l>
						<l n="44" num="6.2">Ne vois-tu rien pour eux ni d’amer ni de rude ?</l>
						<l n="45" num="6.3">Va chez ces délicats qui n’ont soin que d’unir</l>
						<l n="46" num="6.4">Le choix des voluptés aux moyens d’y fournir :</l>
						<l n="47" num="6.5">Si tu crois y trouver des roses sans épines,</l>
						<l n="48" num="6.6">Tu n’y trouveras point ce que tu t’imagines.</l>
					</lg>
					<lg n="7">
						<l n="49" num="7.1">« Mais ils suivent, dis-tu, leurs inclinations ;</l>
						<l n="50" num="7.2">Leur seule volonté règle leurs actions,</l>
						<l n="51" num="7.3">Et l’excès des plaisirs en un moment consume</l>
						<l n="52" num="7.4">Ce peu qui par hasard s’y coule d’amertume. »</l>
						<l n="53" num="7.5">Eh bien ! Soit, je le veux, ils ont tout à souhait ;</l>
						<l n="54" num="7.6">Mais combien doit durer un bonheur si parfait ?</l>
					</lg>
					<lg n="8">
						<l n="55" num="8.1">Ces riches, que du siècle adore l’imprudence,</l>
						<l n="56" num="8.2">Passent comme fumée avec leur abondance,</l>
						<l n="57" num="8.3">Et de leurs voluptés le plus doux souvenir,</l>
						<l n="58" num="8.4">S’il ne passe avec eux, ne sert qu’à les punir.</l>
						<l n="59" num="8.5">Celles que leur permet une si triste vie</l>
						<l n="60" num="8.6">Sont dignes de pitié beaucoup plus que d’envie :</l>
						<l n="61" num="8.7">Elles vont rarement sans mélange d’ennuis ;</l>
						<l n="62" num="8.8">Leurs jours les plus brillants ont les plus sombres nuits ;</l>
						<l n="63" num="8.9">Souvent mille chagrins empoisonnent leurs charmes,</l>
						<l n="64" num="8.10">Souvent mille terreurs y jettent mille alarmes,</l>
						<l n="65" num="8.11">Et souvent des objets d’où naissent leurs plaisirs</l>
						<l n="66" num="8.12">Ma justice en courroux fait naître leurs soupirs.</l>
						<l n="67" num="8.13">L’impétuosité qui les porte aux délices</l>
						<l n="68" num="8.14">Elle-même à leur joie enchaîne les supplices,</l>
						<l n="69" num="8.15">Et joint aux vains appas d’un peu d’illusion</l>
						<l n="70" num="8.16">Le repentir, le trouble et la confusion.</l>
					</lg>
					<lg n="9">
						<l n="71" num="9.1">Toutes ces voluptés sont courtes et menteuses,</l>
						<l n="72" num="9.2">Toutes n’ont que désordre, et toutes sont honteuses.</l>
						<l n="73" num="9.3">Les hommes cependant n’en aperçoivent rien ;</l>
						<l n="74" num="9.4">Enivrés qu’ils en sont, ils en font tout leur bien :</l>
						<l n="75" num="9.5">Ils suivent en tous lieux, comme bêtes stupides,</l>
						<l n="76" num="9.6">Leurs sens pour souverains, leurs passions pour guides ;</l>
						<l n="77" num="9.7">Et pour l’indigne attrait d’un faux chatouillement,</l>
						<l n="78" num="9.8">Pour un bien passager, un plaisir d’un moment,</l>
						<l n="79" num="9.9">Amoureux d’une vie ingrate et fugitive,</l>
						<l n="80" num="9.10">Ils acceptent pour l’âme une mort toujours vive,</l>
						<l n="81" num="9.11">Où mourant à toute heure, et ne pouvant mourir,</l>
						<l n="82" num="9.12">Ils ne sont immortels que pour toujours souffrir.</l>
					</lg>
					<lg n="10">
						<l n="83" num="10.1">Plus sage à leurs dépens, donne moins de puissance</l>
						<l n="84" num="10.2">Aux brutales fureurs de ta concupiscence ;</l>
					</lg>
					<lg n="11">
						<l n="85" num="11.1">Garde-toi de courir après les voluptés,</l>
						<l n="86" num="11.2">Captive tes desirs, brise tes volontés,</l>
						<l n="87" num="11.3">Mets en moi seul ta joie, et m’en fais une offrande,</l>
						<l n="88" num="11.4">Et je t’accorderai ce que ton cœur demande.</l>
					</lg>
					<lg n="12">
						<l n="89" num="12.1">Oui, ce cœur ainsi libre, ainsi désabusé,</l>
						<l n="90" num="12.2">Ne peut, quoi qu’il demande, en être refusé ;</l>
						<l n="91" num="12.3">Et si tu veux goûter des plaisirs véritables,</l>
						<l n="92" num="12.4">Des consolations et pleines et durables,</l>
						<l n="93" num="12.5">Tu n’as qu’à dédaigner par un noble mépris</l>
						<l n="94" num="12.6">Cet éclat dont le monde éblouit tant d’esprits ;</l>
						<l n="95" num="12.7">Tu n’as qu’à t’arracher à ces voluptés basses</l>
						<l n="96" num="12.8">Qui repoussent des cœurs les effets de mes grâces ;</l>
						<l n="97" num="12.9">Tu n’as qu’à te soustraire à leur malignité,</l>
						<l n="98" num="12.10">Et je te rendrai plus que tu n’auras quitté.</l>
						<l n="99" num="12.11">Plus à leurs faux attraits tu fermeras de portes,</l>
						<l n="100" num="12.12">Plus mes faveurs seront et charmantes et fortes ;</l>
						<l n="101" num="12.13">Et moins la créature aura chez toi d’accès,</l>
						<l n="102" num="12.14">Et plus du créateur les dons auront d’excès.</l>
					</lg>
					<lg n="13">
						<l n="103" num="13.1">Ne crois pas toutefois sans peine et sans tristesse</l>
						<l n="104" num="13.2">À ce détachement élever ta foiblesse :</l>
						<l n="105" num="13.3">Une vieille habitude y voudra résister,</l>
						<l n="106" num="13.4">Mais par une meilleure il faudra la dompter ;</l>
					</lg>
					<lg n="14">
						<l n="107" num="14.1">Ta chair murmurera, mais de tout son murmure</l>
						<l n="108" num="14.2">La ferveur de l’esprit convaincra l’imposture ;</l>
						<l n="109" num="14.3">Enfin le vieux serpent tâchera de t’aigrir</l>
						<l n="110" num="14.4">Contre les moindres maux que tu voudras souffrir ;</l>
						<l n="111" num="14.5">Il fera mille efforts pour brouiller ta conduite ;</l>
						<l n="112" num="14.6">Mais avec l’oraison tu le mettras en fuite,</l>
						<l n="113" num="14.7">Et l’obstination d’un saint et digne emploi</l>
						<l n="114" num="14.8">Ne lui laissera plus aucun pouvoir sur toi.</l>
					</lg>
				</div></body></text></TEI>