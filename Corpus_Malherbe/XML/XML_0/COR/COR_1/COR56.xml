<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Imitation De Jésus-Christ</title>
				<title type="medium">Édition électronique</title>
				<author key="COR">
					<name>
						<forename>Pierre</forename>
						<surname>CORNEILLE</surname>
					</name>
					<date from="1606" to="1684">1606-1684</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>13205 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2016</date>
				<idno type="local">COR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Imitation De Jésus-Christ</title>
						<author>Pierre Corneille</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/pierrecorneilleimmitadiondejesuschrist.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Imitation De Jésus-Christ</title>
						<author>Pierre Corneille</author>
						<editor>M. Ch. Marty-Laveaux</editor>
						<edition>nouvelle édition</edition>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k51199.r=pierre%20corneille?rk=85837;2</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie de L. Hachette et Cie</publisher>
							<date when="1862">1862</date>
						</imprint>
						<biblScope unit="tome">Tome 8</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1656">1656</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les chiffres arabes des livres et chapitres ont été remplacés par des chiffres romains</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">LIVRE PREMIER</head><div type="poem" key="COR56">
					<head type="main">CHAPITRE XXIV</head>
					<head type="sub">Du jugement, et des peines du péché.</head>
					<lg n="1">
						<l n="1" num="1.1">Homme, quoi qu’ici-bas tu veuilles entreprendre,</l>
						<l n="2" num="1.2">Songe à ce compte exact qu’un jour il en faut rendre,</l>
						<l n="3" num="1.3">Et mets devant tes yeux cette dernière fin</l>
						<l n="4" num="1.4">Qui fera ton mauvais ou ton heureux destin.</l>
						<l n="5" num="1.5">Regarde avec quel front tu pourras comparoître</l>
						<l n="6" num="1.6">Devant le tribunal de ton souverain maître,</l>
						<l n="7" num="1.7">Devant ce juste juge à qui rien n’est caché,</l>
						<l n="8" num="1.8">Qui jusque dans ton cœur sait lire ton péché,</l>
						<l n="9" num="1.9">Qu’aucun don n’éblouit, qu’aucune erreur n’abuse,</l>
						<l n="10" num="1.10">Que ne surprend jamais l’adresse d’une excuse,</l>
						<l n="11" num="1.11">Qui rend à tous justice et pèse au même poids</l>
						<l n="12" num="1.12">Ce que font les bergers et ce que font les rois.</l>
					</lg>
					<lg n="2">
						<l n="13" num="2.1">Misérable pécheur, que sauras-tu répondre</l>
						<l n="14" num="2.2">À ce dieu qui sait tout, et viendra te confondre,</l>
						<l n="15" num="2.3">Toi que remplit souvent d’un invincible effroi</l>
						<l n="16" num="2.4">Le courroux passager d’un mortel comme toi ?</l>
						<l n="17" num="2.5">Donne pour ce grand jour, donne ordre à tes affaires,</l>
						<l n="18" num="2.6">Pour ce grand jour, le comble ou la fin des misères,</l>
						<l n="19" num="2.7">Où chacun, trop chargé de son propre fardeau,</l>
						<l n="20" num="2.8">Son propre accusateur et son propre bourreau,</l>
						<l n="21" num="2.9">Répondra par sa bouche, et seul à sa défense,</l>
						<l n="22" num="2.10">N’aura point de secours que de sa pénitence.</l>
					</lg>
					<lg n="3">
						<l n="23" num="3.1">Cours donc avec chaleur aux emplois vertueux :</l>
						<l n="24" num="3.2">Maintenant ton travail peut être fructueux,</l>
						<l n="25" num="3.3">Tes douleurs maintenant peuvent être écoutées,</l>
						<l n="26" num="3.4">Tes larmes jusqu’au ciel être soudain portées,</l>
						<l n="27" num="3.5">Tes soupirs de ton juge apaiser la rigueur,</l>
						<l n="28" num="3.6">Ton repentir lui plaire, et nettoyer ton cœur.</l>
					</lg>
					<lg n="4">
						<l n="29" num="4.1">Oh ! Que la patience est un grand purgatoire</l>
						<l n="30" num="4.2">Pour laver de ce cœur la tache la plus noire !</l>
						<l n="31" num="4.3">Que l’homme le blanchit, lorsqu’il le dompte au point</l>
						<l n="32" num="4.4">De souffrir un outrage et n’en murmurer point !</l>
						<l n="33" num="4.5">Lorsqu’il est plus touché du mal que se procure</l>
						<l n="34" num="4.6">L’auteur de son affront, que de sa propre injure ;</l>
						<l n="35" num="4.7">Lorsqu’il élève au ciel ses innocentes mains</l>
						<l n="36" num="4.8">Pour le même ennemi qui rompt tous ses desseins,</l>
						<l n="37" num="4.9">Qu’avec sincérité promptement il pardonne,</l>
						<l n="38" num="4.10">Qu’il demande pardon de même qu’il le donne,</l>
						<l n="39" num="4.11">Que sa vertu commande à son tempérament,</l>
						<l n="40" num="4.12">Que sa bonté prévaut sur son ressentiment,</l>
						<l n="41" num="4.13">Que lui-même à toute heure il se fait violence</l>
						<l n="42" num="4.14">Pour vaincre de ses sens la mutine insolence,</l>
						<l n="43" num="4.15">Et que pour seul objet partout il se prescrit</l>
						<l n="44" num="4.16">D’assujettir la chair sous les lois de l’esprit !</l>
					</lg>
					<lg n="5">
						<l n="45" num="5.1">Ah ! Qu’il vaudroit bien mieux par de saints exercices</l>
						<l n="46" num="5.2">Purger nos passions, déraciner nos vices,</l>
						<l n="47" num="5.3">Et nous-mêmes en nous à l’envi les punir,</l>
						<l n="48" num="5.4">Qu’en réserver la peine à ce long avenir !</l>
						<l n="49" num="5.5">Mais ce que nous avons d’amour désordonnée,</l>
						<l n="50" num="5.6">Pour cette ingrate chair à nous perdre obstinée,</l>
						<l n="51" num="5.7">Nous-mêmes nous séduit, et l’arme contre nous</l>
						<l n="52" num="5.8">De tout ce que nos sens nous offrent de plus doux.</l>
					</lg>
					<lg n="6">
						<l n="53" num="6.1">Qu’auront à dévorer les éternelles flammes,</l>
						<l n="54" num="6.2">Que cette folle amour où s’emportent les âmes,</l>
						<l n="55" num="6.3">Cet amas de péchés, ce détestable fruit</l>
						<l n="56" num="6.4">Que cette chair aimée au fond des cœurs produit ?</l>
						<l n="57" num="6.5">Plus tu suis ses conseils et te fais ici grâce,</l>
						<l n="58" num="6.6">Plus de matière en toi pour ces flammes s’entasse ;</l>
						<l n="59" num="6.7">Et ta punition que tu veux reculer</l>
						<l n="60" num="6.8">Prépare à l’avenir d’autant plus à brûler.</l>
						<l n="61" num="6.9">Là, par une justice effroyable à l’impie,</l>
						<l n="62" num="6.10">Par où chacun offense, il faudra qu’il l’expie ;</l>
						<l n="63" num="6.11">Les plus grands châtiments y seront attachés</l>
						<l n="64" num="6.12">Aux plus longues douceurs de nos plus grands péchés.</l>
					</lg>
					<lg n="7">
						<l n="65" num="7.1">Dans un profond sommeil la paresse enfoncée</l>
						<l n="66" num="7.2">D’aiguillons enflammés s’y trouvera pressée,</l>
						<l n="67" num="7.3">Et les cœurs que charmoit sa molle oisiveté</l>
						<l n="68" num="7.4">Gémiront sans repos toute l’éternité.</l>
					</lg>
					<lg n="8">
						<l n="69" num="8.1">L’ivrogne et le gourmand recevront leurs supplices</l>
						<l n="70" num="8.2">Du souvenir amer de leurs chères délices,</l>
						<l n="71" num="8.3">Et ces repas traînés jusques au lendemain</l>
						<l n="72" num="8.4">Mêleront leur idée aux rages de la faim.</l>
					</lg>
					<lg n="9">
						<l n="73" num="9.1">Les sales voluptés, dans le milieu d’un gouffre,</l>
						<l n="74" num="9.2">Parmi les puanteurs de la poix et du soufre,</l>
						<l n="75" num="9.3">Laisseront occuper aux plus cruels tourments</l>
						<l n="76" num="9.4">Les lieux les plus flattés de leurs chatouillements.</l>
					</lg>
					<lg n="10">
						<l n="77" num="10.1">L’envieux, qui verra du plus creux de l’abîme</l>
						<l n="78" num="10.2">Le ciel ouvert aux saints et fermé pour son crime,</l>
						<l n="79" num="10.3">D’autant plus furieux, hurlera de douleur</l>
						<l n="80" num="10.4">Pour leur félicité plus que pour son malheur.</l>
						<l n="81" num="10.5">Tout vice aura sa peine à lui seul destinée :</l>
						<l n="82" num="10.6">La superbe à la honte y sera condamnée,</l>
						<l n="83" num="10.7">Et pour punir l’avare avec sévérité,</l>
						<l n="84" num="10.8">La pauvreté qu’il fuit aura sa cruauté.</l>
					</lg>
					<lg n="11">
						<l n="85" num="11.1">Là sera plus amère une heure de souffrance</l>
						<l n="86" num="11.2">Que ne le sont ici cent ans de pénitence ;</l>
						<l n="87" num="11.3">Là jamais d’intervalle ou de soulagement</l>
						<l n="88" num="11.4">N’affoiblit des damnés l’éternel châtiment ;</l>
						<l n="89" num="11.5">Mais ici nos travaux peuvent reprendre haleine,</l>
						<l n="90" num="11.6">Souffrir quelque relâche à la plus juste peine ;</l>
						<l n="91" num="11.7">L’espoir d’en voir la fin à toute heure est permis,</l>
						<l n="92" num="11.8">Tandis qu’on s’en console avecque ses amis.</l>
					</lg>
					<lg n="12">
						<l n="93" num="12.1">Romps-y donc du péché les noires habitudes,</l>
						<l n="94" num="12.2">À force de soupirs, de soins, d’inquiétudes,</l>
						<l n="95" num="12.3">Afin qu’en ce grand jour ce juge rigoureux</l>
						<l n="96" num="12.4">Te mette en sûreté parmi les bienheureux ;</l>
						<l n="97" num="12.5">Car les justes alors avec pleine constance</l>
						<l n="98" num="12.6">Des maux par eux soufferts voudront prendre vengeance,</l>
						<l n="99" num="12.7">Et d’un regard farouche ils paroîtront armés</l>
						<l n="100" num="12.8">Contre les gros pécheurs qui les ont opprimés.</l>
					</lg>
					<lg n="13">
						<l n="101" num="13.1">Tu verras lors assis au nombre de tes juges</l>
						<l n="102" num="13.2">Ceux qui jadis chez toi cherchoient quelques refuges,</l>
						<l n="103" num="13.3">Et tu seras jugé par le juste courroux</l>
						<l n="104" num="13.4">De qui te demandoit la justice à genoux.</l>
					</lg>
					<lg n="14">
						<l n="105" num="14.1">L’humble alors et le pauvre après leur patience</l>
						<l n="106" num="14.2">Rentreront à la vie en paix, en confiance,</l>
						<l n="107" num="14.3">Cependant que le riche avec tout son orgueil,</l>
						<l n="108" num="14.4">Pâle et tremblant d’effroi, sortira du cercueil.</l>
					</lg>
					<lg n="15">
						<l n="109" num="15.1">Lors aura son éclat la sagesse profonde,</l>
						<l n="110" num="15.2">Qui passoit pour folie aux mauvais yeux du monde :</l>
						<l n="111" num="15.3">Une gloire sans fin sera le digne prix</l>
						<l n="112" num="15.4">D’avoir souffert pour Dieu l’opprobre et le mépris.</l>
					</lg>
					<lg n="16">
						<l n="113" num="16.1">Lors tous les déplaisirs endurés sans murmure</l>
						<l n="114" num="16.2">Seront changés en joie inépuisable et pure ;</l>
						<l n="115" num="16.3">Et toute iniquité confondant son auteur</l>
						<l n="116" num="16.4">Lui fermera la bouche et rongera le cœur.</l>
						<l n="117" num="16.5">Point lors, point de dévots sans entière allégresse,</l>
						<l n="118" num="16.6">Point lors de libertins sans profonde tristesse :</l>
						<l n="119" num="16.7">Ceux-là s’élèveront dans les ravissements,</l>
						<l n="120" num="16.8">Ceux-ci s’abîmeront dans les gémissements ;</l>
						<l n="121" num="16.9">Et la chair qu’ici-bas on aura maltraitée,</l>
						<l n="122" num="16.10">Que la règle ou le zèle auront persécutée,</l>
						<l n="123" num="16.11">Goûtera plus alors de solides plaisirs</l>
						<l n="124" num="16.12">Que celle que partout on livre à ses desirs.</l>
					</lg>
					<lg n="17">
						<l n="125" num="17.1">Les lambeaux mal tissus de la robe grossière</l>
						<l n="126" num="17.2">Des plus brillants habits terniront la lumière ;</l>
						<l n="127" num="17.3">Et les princes verront les chaumes préférés</l>
						<l n="128" num="17.4">Au faîte ambitieux de leurs palais dorés.</l>
					</lg>
					<lg n="18">
						<l n="129" num="18.1">La longue patience aura plus d’avantage</l>
						<l n="130" num="18.2">Que tout ce vain pouvoir qu’a le monde en partage ;</l>
						<l n="131" num="18.3">La prompte obéissance et sa simplicité,</l>
						<l n="132" num="18.4">Que tout ce que le siècle a de subtilité.</l>
					</lg>
					<lg n="19">
						<l n="133" num="19.1">La joie et la candeur des bonnes consciences</l>
						<l n="134" num="19.2">Iront lors au-dessus des plus hautes sciences ;</l>
						<l n="135" num="19.3">Et du mépris des biens les plus légers efforts</l>
						<l n="136" num="19.4">Seront de plus grand poids que les plus grands trésors.</l>
					</lg>
					<lg n="20">
						<l n="137" num="20.1">Tu sentiras ton âme alors plus consolée</l>
						<l n="138" num="20.2">D’une oraison dévote à tes soupirs mêlée,</l>
						<l n="139" num="20.3">Que d’avoir fait parade en de pompeux festins</l>
						<l n="140" num="20.4">Du choix le plus exquis des viandes et des vins.</l>
					</lg>
					<lg n="21">
						<l n="141" num="21.1">Tu te trouveras mieux de voir dans la balance</l>
						<l n="142" num="21.2">L’heureuse fermeté d’un rigoureux silence,</l>
						<l n="143" num="21.3">Que d’y voir l’embarras et les distractions</l>
						<l n="144" num="21.4">D’un cœur qui s’abandonne aux conversations ;</l>
						<l n="145" num="21.5">D’y voir de bons effets que de belles paroles,</l>
						<l n="146" num="21.6">Des actes de vertu que des discours frivoles ;</l>
						<l n="147" num="21.7">D’y voir la pénitence avec sa dureté,</l>
						<l n="148" num="21.8">D’y voir l’étroite vie avec son âpreté,</l>
						<l n="149" num="21.9">Que la douce mollesse où flotte vagabonde</l>
						<l n="150" num="21.10">Une âme qui s’endort dans les plaisirs du monde.</l>
					</lg>
					<lg n="22">
						<l n="151" num="22.1">Apprends qu’il faut souffrir quelques petits malheurs,</l>
						<l n="152" num="22.2">Pour t’affranchir alors de ces pleines douleurs :</l>
						<l n="153" num="22.3">Éprouve ici ta force, et fais sur peu de chose</l>
						<l n="154" num="22.4">Un foible essai des maux où l’avenir t’expose.</l>
						<l n="155" num="22.5">Ils seront éternels, et tu crains d’endurer</l>
					</lg>
					<lg n="23">
						<l n="156" num="23.1">Ceux qui n’ont ici-bas qu’un moment à durer !</l>
						<l n="157" num="23.2">Si leurs moindres assauts, leur moindre expérience</l>
						<l n="158" num="23.3">Te jette dans le trouble et dans l’impatience,</l>
						<l n="159" num="23.4">Au milieu des enfers, où ton péché va choir,</l>
						<l n="160" num="23.5">Jusques à quelle rage ira ton désespoir ?</l>
						<l n="161" num="23.6">Souffre, souffre sans bruit, quoi que le ciel t’envoie :</l>
						<l n="162" num="23.7">Tu ne saurois avoir de deux sortes de joie,</l>
						<l n="163" num="23.8">Remplir de tes desirs ici l’avidité,</l>
						<l n="164" num="23.9">Et régner avec Dieu dedans l’éternité.</l>
					</lg>
					<lg n="24">
						<l n="165" num="24.1">Quand depuis ta naissance on auroit vu ta vie</l>
						<l n="166" num="24.2">D’honneurs jusqu’à ce jour et de plaisirs suivie,</l>
						<l n="167" num="24.3">Qu’auroit tout cet amas qui te pût secourir,</l>
						<l n="168" num="24.4">Si dans ce même instant il te falloit mourir ?</l>
					</lg>
					<lg n="25">
						<l n="169" num="25.1">Tout n’est que vanité : gloire, faveurs, richesses,</l>
						<l n="170" num="25.2">Passagères douceurs, trompeuses allégresses ;</l>
						<l n="171" num="25.3">Tout n’est qu’amusement, tout n’est que faux appui,</l>
						<l n="172" num="25.4">Hormis d’aimer Dieu seul, et ne servir que lui.</l>
						<l n="173" num="25.5">Qui de tout son cœur l’aime y borne ses délices ;</l>
						<l n="174" num="25.6">Il ne craint mort, enfer, jugement, ni supplices ;</l>
					</lg>
					<lg n="26">
						<l n="175" num="26.1">De ce parfait amour le salutaire excès</l>
						<l n="176" num="26.2">Près de l’objet aimé lui donne un sûr accès ;</l>
						<l n="177" num="26.3">Mais lorsque le pécheur aime encor que du vice</l>
						<l n="178" num="26.4">La funeste douceur dans son âme se glisse,</l>
						<l n="179" num="26.5">Il n’est pas merveilleux s’il tremble incessamment</l>
						<l n="180" num="26.6">Au seul nom de la mort ou de ce jugement.</l>
					</lg>
					<lg n="27">
						<l n="181" num="27.1">Il est bon toutefois que l’ingrate malice,</l>
						<l n="182" num="27.2">En qui l’amour de Dieu cède aux attraits du vice,</l>
						<l n="183" num="27.3">Du moins cède à son tour à l’effroi des tourments</l>
						<l n="184" num="27.4">Qui l’arrache par force à ses déréglements.</l>
						<l n="185" num="27.5">Si pourtant cette crainte est en toi la maîtresse,</l>
						<l n="186" num="27.6">Sans que celle de Dieu soutienne ta foiblesse,</l>
						<l n="187" num="27.7">Ce mouvement servile, indigne d’un chrétien,</l>
						<l n="188" num="27.8">Dédaignera bientôt les sentiers du vrai bien,</l>
						<l n="189" num="27.9">Et te laissera faire une chute effroyable</l>
						<l n="190" num="27.10">Dans les piéges du monde et les filets du diable.</l>
					</lg>
				</div></body></text></TEI>