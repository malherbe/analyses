<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Imitation De Jésus-Christ</title>
				<title type="medium">Édition électronique</title>
				<author key="COR">
					<name>
						<forename>Pierre</forename>
						<surname>CORNEILLE</surname>
					</name>
					<date from="1606" to="1684">1606-1684</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>13205 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2016</date>
				<idno type="local">COR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Imitation De Jésus-Christ</title>
						<author>Pierre Corneille</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/pierrecorneilleimmitadiondejesuschrist.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Imitation De Jésus-Christ</title>
						<author>Pierre Corneille</author>
						<editor>M. Ch. Marty-Laveaux</editor>
						<edition>nouvelle édition</edition>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k51199.r=pierre%20corneille?rk=85837;2</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie de L. Hachette et Cie</publisher>
							<date when="1862">1862</date>
						</imprint>
						<biblScope unit="tome">Tome 8</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1656">1656</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les chiffres arabes des livres et chapitres ont été remplacés par des chiffres romains</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">LIVRE DEUXIÈME</head><div type="poem" key="COR58">
					<head type="main">CHAPITRE I</head>
					<head type="sub">De la conversation intérieure.</head>
					<lg n="1">
						<l n="1" num="1.1">« Sachez que mon royaume est au dedans de vous, »</l>
						<l n="2" num="1.2"><space unit="char" quantity="12"></space>Dit le céleste époux</l>
						<l n="3" num="1.3"><space unit="char" quantity="8"></space>Aux âmes de ses chers fidèles :</l>
						<l n="4" num="1.4">Élève donc la tienne où l’appelle sa voix,</l>
						<l n="5" num="1.5">Quitte pour lui le monde, et laisse aux criminelles</l>
						<l n="6" num="1.6"><space unit="char" quantity="8"></space>Ce triste canton de rebelles,</l>
						<l n="7" num="1.7">Et tu rencontreras le repos sous ses lois.</l>
					</lg>
					<lg n="2">
						<l n="8" num="2.1">Apprends à mépriser les pompes inconstantes</l>
						<l n="9" num="2.2"><space unit="char" quantity="12"></space>De ces douceurs flottantes</l>
						<l n="10" num="2.3"><space unit="char" quantity="8"></space>Dont le dehors brille à tes yeux ;</l>
						<l n="11" num="2.4">Apprends à recueillir ce qu’une sainte flamme</l>
						<l n="12" num="2.5">Dans un intérieur verse de précieux,</l>
						<l n="13" num="2.6"><space unit="char" quantity="8"></space>Et soudain du plus haut des cieux</l>
						<l n="14" num="2.7">Le royaume de Dieu descendra dans ton âme.</l>
					</lg>
					<lg n="3">
						<l n="15" num="3.1">Car enfin ce royaume est une forte paix</l>
						<l n="16" num="3.2"><space unit="char" quantity="12"></space>Qui de tous les souhaits</l>
						<l n="17" num="3.3"><space unit="char" quantity="8"></space>Bannit la vaine inquiétude ;</l>
						<l n="18" num="3.4">Une stable allégresse, et dont le Saint-Esprit</l>
						<l n="19" num="3.5">Répandant sur les bons l’heureuse certitude,</l>
						<l n="20" num="3.6"><space unit="char" quantity="8"></space>L’impie et noire ingratitude</l>
						<l n="21" num="3.7">Jamais ne la reçut, jamais ne la comprit.</l>
					</lg>
					<lg n="4">
						<l n="22" num="4.1">Jésus viendra chez toi lui-même la répandre,</l>
						<l n="23" num="4.2"><space unit="char" quantity="12"></space>Si ton cœur pour l’attendre</l>
						<l n="24" num="4.3"><space unit="char" quantity="8"></space>Lui dispose un digne séjour :</l>
						<l n="25" num="4.4">La gloire qui lui plaît et la beauté qu’il aime</l>
						<l n="26" num="4.5">De l’éclat du dedans tirent leur plus beau jour ;</l>
						<l n="27" num="4.6"><space unit="char" quantity="8"></space>Et pour te donner son amour</l>
						<l n="28" num="4.7">Il ne veut rien de toi qui soit hors de toi-même.</l>
					</lg>
					<lg n="5">
						<l n="29" num="5.1">Il y fera pleuvoir mille sortes de biens</l>
						<l n="30" num="5.2"><space unit="char" quantity="12"></space>Par les doux entretiens</l>
						<l n="31" num="5.3"><space unit="char" quantity="8"></space>De ses amoureuses visites :</l>
						<l n="32" num="5.4">Un plein épanchement de consolations,</l>
						<l n="33" num="5.5">Un calme inébranlable, une paix sans limites,</l>
						<l n="34" num="5.6"><space unit="char" quantity="8"></space>Et l’abondance des mérites,</l>
						<l n="35" num="5.7">Y suivront à l’envi ses conversations.</l>
					</lg>
					<lg n="6">
						<l n="36" num="6.1">Courage donc, courage, âme sainte : prépare</l>
						<l n="37" num="6.2"><space unit="char" quantity="12"></space>Pour un bonheur si rare</l>
						<l n="38" num="6.3"><space unit="char" quantity="8"></space>Un cœur tout de zèle et de foi ;</l>
						<l n="39" num="6.4">Que ce divin époux daigne à cette même heure,</l>
						<l n="40" num="6.5">S’y voyant seul aimé, seul reconnu pour roi,</l>
						<l n="41" num="6.6"><space unit="char" quantity="8"></space>Entrer chez toi, loger chez toi,</l>
						<l n="42" num="6.7">Et jusqu’à ton départ y faire sa demeure.</l>
					</lg>
					<lg n="7">
						<l n="43" num="7.1">Lui-même il l’a promis : « si quelqu’un veut m’aimer,</l>
						<l n="44" num="7.2"><space unit="char" quantity="12"></space>Il doit se conformer,</l>
						<l n="45" num="7.3"><space unit="char" quantity="8"></space>Dit-il, à ce que je commande ;</l>
						<l n="46" num="7.4">Alors mon père et moi nous serons son appui,</l>
						<l n="47" num="7.5">Nous le garantirons de quoi qu’il appréhende ;</l>
						<l n="48" num="7.6"><space unit="char" quantity="8"></space>Et pour sa sûreté plus grande,</l>
						<l n="49" num="7.7">Nous viendrons jusqu’à lui pour demeurer chez lui.</l>
					</lg>
					<lg n="8">
						<l n="50" num="8.1">Ouvre-lui tout ce cœur ; et quoi qu’on te propose,</l>
						<l n="51" num="8.2"><space unit="char" quantity="12"></space>Tiens-en la porte close</l>
						<l n="52" num="8.3"><space unit="char" quantity="8"></space>À tout autre objet qu’à sa croix :</l>
						<l n="53" num="8.4">Lui seul pour te guérir a d’assurés remèdes,</l>
						<l n="54" num="8.5">Lui seul pour t’enrichir abandonne à ton choix</l>
						<l n="55" num="8.6"><space unit="char" quantity="8"></space>Plus que tous les trésors des rois,</l>
						<l n="56" num="8.7">Et tu possèdes tout lorsque tu le possèdes.</l>
					</lg>
					<lg n="9">
						<l n="57" num="9.1">Il pourvoira lui-même à tes nécessités,</l>
						<l n="58" num="9.2"><space unit="char" quantity="12"></space>Et ses hautes bontés</l>
						<l n="59" num="9.3"><space unit="char" quantity="8"></space>Partout soulageront tes peines ;</l>
						<l n="60" num="9.4">Il te sera fidèle, et son divin pouvoir</l>
						<l n="61" num="9.5">T’en donnera partout des preuves si soudaines,</l>
						<l n="62" num="9.6"><space unit="char" quantity="8"></space>Que les assistances humaines</l>
						<l n="63" num="9.7">N’auront ni temps ni lieu d’amuser ton espoir.</l>
					</lg>
					<lg n="10">
						<l n="64" num="10.1">Des peuples et des grands la faveur est changeante,</l>
						<l n="65" num="10.2"><space unit="char" quantity="12"></space>Et la plus obligeante</l>
						<l n="66" num="10.3"><space unit="char" quantity="8"></space>En moins de rien passe avec eux ;</l>
						<l n="67" num="10.4">Mais celle de Jésus ne connoît point de terme,</l>
						<l n="68" num="10.5">Et s’attache à l’aimé par de si puissants nœuds,</l>
						<l n="69" num="10.6"><space unit="char" quantity="8"></space>Que jusqu’au plein effet des vœux,</l>
						<l n="70" num="10.7">Jusqu’à la fin des maux, elle tient toujours ferme.</l>
					</lg>
					<lg n="11">
						<l n="71" num="11.1">Souviens-toi donc toujours, quand un ami te sert</l>
						<l n="72" num="11.2"><space unit="char" quantity="12"></space>Le plus à cœur ouvert,</l>
						<l n="73" num="11.3"><space unit="char" quantity="8"></space>Que souvent son zèle est stérile ;</l>
						<l n="74" num="11.4">Fais peu de fondement sur son plus haut crédit,</l>
						<l n="75" num="11.5">Et dans le même instant qu’il t’est le plus utile,</l>
						<l n="76" num="11.6"><space unit="char" quantity="8"></space>Crois-le mortel, crois-le fragile,</l>
						<l n="77" num="11.7">Et t’attriste encor moins lorsqu’il te contredit.</l>
					</lg>
					<lg n="12">
						<l n="78" num="12.1">Tel aujourd’hui t’embrasse et soutient ta querelle,</l>
						<l n="79" num="12.2"><space unit="char" quantity="12"></space>Dont l’esprit infidèle</l>
						<l n="80" num="12.3"><space unit="char" quantity="8"></space>Dès demain voudra t’opprimer ;</l>
						<l n="81" num="12.4">Et tel autre aujourd’hui contre toi s’intéresse,</l>
						<l n="82" num="12.5">Que pour toi dès demain tu verras s’animer :</l>
						<l n="83" num="12.6"><space unit="char" quantity="8"></space>Tant pour haïr et pour aimer</l>
						<l n="84" num="12.7">Au gré du moindre vent tourne notre foiblesse !</l>
					</lg>
					<lg n="13">
						<l n="85" num="13.1">Ne t’assure qu’en Dieu, mets-y tout ton amour</l>
						<l n="86" num="13.2"><space unit="char" quantity="12"></space>Jusqu’à ton dernier jour,</l>
						<l n="87" num="13.3"><space unit="char" quantity="8"></space>Tout ton espoir, toute ta crainte :</l>
						<l n="88" num="13.4">Il conduira ta langue, il réglera tes yeux,</l>
						<l n="89" num="13.5">Et de quelque malheur que tu sentes l’atteinte,</l>
						<l n="90" num="13.6"><space unit="char" quantity="8"></space>Jamais il n’entendra ta plainte</l>
						<l n="91" num="13.7">Qu’il ne fasse pour toi ce qu’il verra de mieux.</l>
					</lg>
					<lg n="14">
						<l n="92" num="14.1">L’homme n’a point ici de cité permanente :</l>
						<l n="93" num="14.2"><space unit="char" quantity="12"></space>Où qu’il soit, quoi qu’il tente,</l>
						<l n="94" num="14.3"><space unit="char" quantity="8"></space>Il n’est qu’un malheureux passant ;</l>
						<l n="95" num="14.4">Et si dans les travaux de son pèlerinage,</l>
						<l n="96" num="14.5">L’effort intérieur d’un cœur reconnoissant</l>
						<l n="97" num="14.6"><space unit="char" quantity="8"></space>Ne l’unit au bras tout-puissant,</l>
						<l n="98" num="14.7">Il s’y promet en vain le calme après l’orage.</l>
					</lg>
					<lg n="15">
						<l n="99" num="15.1">Que regardes-tu donc, mortel, autour de toi,</l>
						<l n="100" num="15.2"><space unit="char" quantity="12"></space>Comme si quelque emploi</l>
						<l n="101" num="15.3"><space unit="char" quantity="8"></space>T’y faisoit une paix profonde ?</l>
						<l n="102" num="15.4">C’est au ciel, c’est en Dieu qu’il te faut habiter :</l>
						<l n="103" num="15.5">C’est là, c’est en lui seul qu’un vrai repos se fonde ;</l>
						<l n="104" num="15.6"><space unit="char" quantity="8"></space>Et quoi qu’étale ici le monde,</l>
						<l n="105" num="15.7">Ce n’est qu’avec dédain que l’œil s’y doit prêter.</l>
					</lg>
					<lg n="16">
						<l n="106" num="16.1">Tout ce qu’il te présente y passe comme une ombre,</l>
						<l n="107" num="16.2"><space unit="char" quantity="12"></space>Et toi-même es du nombre</l>
						<l n="108" num="16.3"><space unit="char" quantity="8"></space>De ces fantômes passagers :</l>
						<l n="109" num="16.4">Tu passeras comme eux, et ta chute funeste</l>
						<l n="110" num="16.5">Suivra l’attachement à ces objets légers,</l>
						<l n="111" num="16.6"><space unit="char" quantity="8"></space>Si pour éviter ces dangers</l>
						<l n="112" num="16.7">Tu ne romps avec toi comme avec tout le reste.</l>
					</lg>
					<lg n="17">
						<l n="113" num="17.1">De ce triste séjour où tout n’est que défaut,</l>
						<l n="114" num="17.2"><space unit="char" quantity="12"></space>Jusqu’aux pieds du très-haut,</l>
						<l n="115" num="17.3"><space unit="char" quantity="8"></space>Sache relever ta pensée ;</l>
						<l n="116" num="17.4">Qu’à force de soupirs, de larmes et de vœux,</l>
						<l n="117" num="17.5">Jusques à Jésus-Christ ta prière poussée</l>
						<l n="118" num="17.6"><space unit="char" quantity="8"></space>Lui montre une ardeur empressée,</l>
						<l n="119" num="17.7">D’où sans cesse pour lui partent de nouveaux feux.</l>
					</lg>
					<lg n="18">
						<l n="120" num="18.1">Si tu t’y sens mal propre, et qu’entre tant d’épines</l>
						<l n="121" num="18.2"><space unit="char" quantity="12"></space>Jusqu’aux grandeurs divines</l>
						<l n="122" num="18.3"><space unit="char" quantity="8"></space>Tes forces ne puissent monter,</l>
						<l n="123" num="18.4">S’il faut que sur la terre encor tu les essaies,</l>
						<l n="124" num="18.5">Sa passion t’y donne assez où t’arrêter ;</l>
						<l n="125" num="18.6"><space unit="char" quantity="8"></space>Mais il faut pour la bien goûter</l>
						<l n="126" num="18.7">Affermir ta demeure au milieu de ses plaies.</l>
					</lg>
					<lg n="19">
						<l n="127" num="19.1">Prends ce dévot refuge en toutes tes douleurs,</l>
						<l n="128" num="19.2"><space unit="char" quantity="12"></space>Et tes plus grands malheurs</l>
						<l n="129" num="19.3"><space unit="char" quantity="8"></space>Trouveront une issue aisée :</l>
						<l n="130" num="19.4">Tu sauras négliger quoi qu’il faille souffrir ;</l>
						<l n="131" num="19.5">Les mépris te seront des sujets de risée,</l>
						<l n="132" num="19.6"><space unit="char" quantity="8"></space>Et la médisance abusée</l>
						<l n="133" num="19.7">Ne dira rien de toi dont tu daignes t’aigrir.</l>
					</lg>
					<lg n="20">
						<l n="134" num="20.1">Le monarque du ciel, le maître du tonnerre,</l>
						<l n="135" num="20.2"><space unit="char" quantity="12"></space>Méprisé sur la terre,</l>
						<l n="136" num="20.3"><space unit="char" quantity="8"></space>Dans l’opprobre y finit ses jours ;</l>
						<l n="137" num="20.4">Au milieu de sa peine, au fort de sa misère,</l>
						<l n="138" num="20.5">Il vit tous ses amis lâches, muets et sourds :</l>
						<l n="139" num="20.6"><space unit="char" quantity="8"></space>Tout lui refusa du secours,</l>
						<l n="140" num="20.7">Et tout l’abandonna, jusqu’à son propre père.</l>
					</lg>
					<lg n="21">
						<l n="141" num="21.1">Cet abandon lui plut, il aima ce mépris,</l>
						<l n="142" num="21.2"><space unit="char" quantity="12"></space>Et pour être ton prix</l>
						<l n="143" num="21.3"><space unit="char" quantity="8"></space>Il voulut être ta victime :</l>
						<l n="144" num="21.4">Innocent qu’il étoit, il voulut endurer ;</l>
						<l n="145" num="21.5">Et toi, dont la souffrance est moindre que le crime,</l>
						<l n="146" num="21.6"><space unit="char" quantity="8"></space>Tu t’oses plaindre qu’on t’opprime,</l>
						<l n="147" num="21.7">Et croire que tes maux valent en murmurer !</l>
					</lg>
					<lg n="22">
						<l n="148" num="22.1">Il eut des ennemis, il vit la médisance</l>
						<l n="149" num="22.2"><space unit="char" quantity="12"></space>Noircir en sa présence</l>
						<l n="150" num="22.3"><space unit="char" quantity="8"></space>Ses plus sincères actions ;</l>
						<l n="151" num="22.4">Et tu veux que chacun avec soin te caresse,</l>
						<l n="152" num="22.5">Que chacun soit jaloux de tes affections,</l>
						<l n="153" num="22.6"><space unit="char" quantity="8"></space>Qu’il coure à tes intentions,</l>
						<l n="154" num="22.7">Et pour te mieux servir à l’envi s’intéresse !</l>
					</lg>
					<lg n="23">
						<l n="155" num="23.1">Dans les adversités l’âme fait ses trésors</l>
						<l n="156" num="23.2"><space unit="char" quantity="12"></space>Des misères du corps ;</l>
						<l n="157" num="23.3"><space unit="char" quantity="8"></space>Ce sont les épreuves des bonnes :</l>
						<l n="158" num="23.4">Leur patience amasse alors sans se lasser ;</l>
						<l n="159" num="23.5">Mais où pourra la tienne emporter des couronnes,</l>
						<l n="160" num="23.6"><space unit="char" quantity="8"></space>Si tous les soins que tu te donnes</l>
						<l n="161" num="23.7">N’ont pour but que de fuir ce qui peut l’exercer ?</l>
					</lg>
					<lg n="24">
						<l n="162" num="24.1">Tu vois ton maître en croix, où ton péché le tue,</l>
						<l n="163" num="24.2"><space unit="char" quantity="12"></space>Et tu peux à sa vue</l>
						<l n="164" num="24.3"><space unit="char" quantity="8"></space>Te rebuter de quelque ennui !</l>
						<l n="165" num="24.4">Ah ! Ce n’est pas ainsi qu’on a part à sa gloire ;</l>
						<l n="166" num="24.5">Change, pauvre pécheur, change dès aujourd’hui :</l>
						<l n="167" num="24.6"><space unit="char" quantity="8"></space>Souffre avec lui, souffre pour lui,</l>
						<l n="168" num="24.7">Si tu veux avec lui régner par sa victoire.</l>
					</lg>
					<lg n="25">
						<l n="169" num="25.1">Si tu peux dans son sein une fois pénétrer</l>
						<l n="170" num="25.2"><space unit="char" quantity="12"></space>Jusqu’où savent entrer</l>
						<l n="171" num="25.3"><space unit="char" quantity="8"></space>Les ardeurs d’une amour extrême,</l>
						<l n="172" num="25.4">Si tu peux faire en terre un essai des plaisirs</l>
						<l n="173" num="25.5">Où ce parfait amour abîme un cœur qui l’aime,</l>
						<l n="174" num="25.6"><space unit="char" quantity="8"></space>Tu verras bientôt pour toi-même</l>
						<l n="175" num="25.7">Ta sainte indifférence avoir peu de desirs.</l>
					</lg>
					<lg n="26">
						<l n="176" num="26.1">Il t’importera peu que le monde s’en joue,</l>
						<l n="177" num="26.2"><space unit="char" quantity="12"></space>Et t’offre de la roue</l>
						<l n="178" num="26.3"><space unit="char" quantity="8"></space>Ou le dessus ou le dessous :</l>
						<l n="179" num="26.4">Plus cet amour est fort, plus l’homme se méprise ;</l>
						<l n="180" num="26.5">Les opprobres n’ont rien qui ne lui semble doux,</l>
						<l n="181" num="26.6"><space unit="char" quantity="8"></space>Et plus rudes en sont les coups,</l>
						<l n="182" num="26.7">Plus il voit que de Dieu la main le favorise.</l>
					</lg>
					<lg n="27">
						<l n="183" num="27.1">L’amoureux de Jésus et de la vérité</l>
						<l n="184" num="27.2"><space unit="char" quantity="12"></space>Avec sévérité</l>
						<l n="185" num="27.3"><space unit="char" quantity="8"></space>Au dedans de soi se ramène ;</l>
						<l n="186" num="27.4">Et depuis que son cœur pleinement s’affranchit</l>
						<l n="187" num="27.5">De toute affection désordonnée et vaine,</l>
						<l n="188" num="27.6"><space unit="char" quantity="8"></space>De toute ambition humaine,</l>
						<l n="189" num="27.7">Dans ce retour vers Dieu sans obstacle il blanchit.</l>
					</lg>
					<lg n="28">
						<l n="190" num="28.1">Son âme détachée, et libre autant que pure,</l>
						<l n="191" num="28.2"><space unit="char" quantity="12"></space>Par-dessus la nature</l>
						<l n="192" num="28.3"><space unit="char" quantity="8"></space>Sans peine apprend à s’élever :</l>
						<l n="193" num="28.4">Sitôt que de soi-même il cesse d’être esclave,</l>
						<l n="194" num="28.5">Un ferme et vrai repos chez lui le vient trouver ;</l>
						<l n="195" num="28.6"><space unit="char" quantity="8"></space>Et quand il a pu se braver,</l>
						<l n="196" num="28.7">Il n’a point d’ennemis qu’aisément il ne brave.</l>
					</lg>
					<lg n="29">
						<l n="197" num="29.1">Il sait donner à tout un véritable prix,</l>
						<l n="198" num="29.2"><space unit="char" quantity="12"></space>Sans peser le mépris</l>
						<l n="199" num="29.3"><space unit="char" quantity="8"></space>Ou l’estime qu’en fait le monde :</l>
						<l n="200" num="29.4">Vraiment sage et savant, il peut dire en tout lieu</l>
						<l n="201" num="29.5">Qu’il ne tient point de lui sa doctrine profonde,</l>
						<l n="202" num="29.6"><space unit="char" quantity="8"></space>Et que celle dont il abonde</l>
						<l n="203" num="29.7">Ne se puise jamais qu’en l’école de Dieu.</l>
					</lg>
					<lg n="30">
						<l n="204" num="30.1">Dedans l’intérieur il ordonne sa voie,</l>
						<l n="205" num="30.2"><space unit="char" quantity="12"></space>Et dehors, quoi qu’il voie,</l>
						<l n="206" num="30.3"><space unit="char" quantity="8"></space>Tout est peu de chose à ses yeux :</l>
						<l n="207" num="30.4">Le zèle qui partout règne en sa conscience</l>
						<l n="208" num="30.5">N’attend pour s’exercer ni les temps ni les lieux,</l>
						<l n="209" num="30.6"><space unit="char" quantity="8"></space>Et pour aller de bien en mieux</l>
						<l n="210" num="30.7">Tout lieu, tout temps est propre à son impatience.</l>
					</lg>
					<lg n="31">
						<l n="211" num="31.1">Quelques tentations qui l’osent assaillir,</l>
						<l n="212" num="31.2"><space unit="char" quantity="12"></space>Prompt à se recueillir,</l>
						<l n="213" num="31.3"><space unit="char" quantity="8"></space>En soi-même il fait sa retraite ;</l>
						<l n="214" num="31.4">Et comme il s’y retranche avec facilité,</l>
						<l n="215" num="31.5">Des attraits du dehors la douceur inquiète</l>
						<l n="216" num="31.6"><space unit="char" quantity="8"></space>Jamais jusque-là ne l’arrête</l>
						<l n="217" num="31.7">Qu’il se répande entier sur leur inanité.</l>
					</lg>
					<lg n="32">
						<l n="218" num="32.1">Ni le travail du corps, ni le soin nécessaire</l>
						<l n="219" num="32.2"><space unit="char" quantity="12"></space>D’une pressante affaire</l>
						<l n="220" num="32.3"><space unit="char" quantity="8"></space>Ne l’emporte à se disperser ;</l>
						<l n="221" num="32.4">Dans tous événements ce zèle trouve place ;</l>
						<l n="222" num="32.5">La bonne occasion, il la sait embrasser ;</l>
						<l n="223" num="32.6"><space unit="char" quantity="8"></space>La mauvaise, il la sait passer,</l>
						<l n="224" num="32.7">Et faire son profit de ce qui l’embarrasse.</l>
					</lg>
					<lg n="33">
						<l n="225" num="33.1">Ce bel ordre au dedans en chasse tout souci</l>
						<l n="226" num="33.2"><space unit="char" quantity="12"></space>De ce que font ici</l>
						<l n="227" num="33.3"><space unit="char" quantity="8"></space>Ceux qu’on blâme et ceux qu’on admire :</l>
						<l n="228" num="33.4">Il ferme ainsi la porte à tous empêchements,</l>
						<l n="229" num="33.5">Et sait qu’on n’est distrait du bien où l’âme aspire</l>
						<l n="230" num="33.6"><space unit="char" quantity="8"></space>Qu’autant qu’en soi-même on attire</l>
						<l n="231" num="33.7">D’un vain extérieur les prompts amusements.</l>
					</lg>
					<lg n="34">
						<l n="232" num="34.1">Si la tienne une fois étoit bien dégagée,</l>
						<l n="233" num="34.2"><space unit="char" quantity="12"></space>Bien nette, bien purgée</l>
						<l n="234" num="34.3"><space unit="char" quantity="8"></space>De ces folles impressions,</l>
						<l n="235" num="34.4">Tout la satisferoit, tout lui seroit utile,</l>
						<l n="236" num="34.5">Et Dieu, réunissant tes inclinations,</l>
						<l n="237" num="34.6"><space unit="char" quantity="8"></space>De toutes occupations</l>
						<l n="238" num="34.7">Te feroit en vrais biens une terre fertile.</l>
					</lg>
					<lg n="35">
						<l n="239" num="35.1">Mais n’étant pas encor ni bien mortifié,</l>
						<l n="240" num="35.2"><space unit="char" quantity="12"></space>Ni bien fortifié</l>
						<l n="241" num="35.3"><space unit="char" quantity="8"></space>Contre les douceurs passagères,</l>
						<l n="242" num="35.4">Souvent il te déplaît qu’au lieu de ces vrais biens,</l>
						<l n="243" num="35.5">Tu ne te vois rempli que d’images légères,</l>
						<l n="244" num="35.6"><space unit="char" quantity="8"></space>Dont les promesses mensongères</l>
						<l n="245" num="35.7">Troublent à tous moments la route que tu tiens.</l>
					</lg>
					<lg n="36">
						<l n="246" num="36.1">Ton cœur aime le monde ; et tout ce qui le brouille,</l>
						<l n="247" num="36.2"><space unit="char" quantity="12"></space>Tout ce qui plus le souille,</l>
						<l n="248" num="36.3"><space unit="char" quantity="8"></space>C’est cet impur attachement :</l>
						<l n="249" num="36.4">Rejette ses plaisirs, romps avec leur bassesse ;</l>
						<l n="250" num="36.5">Et ce cœur vers le ciel s’élançant fortement,</l>
						<l n="251" num="36.6"><space unit="char" quantity="8"></space>Saura goûter incessamment</l>
						<l n="252" num="36.7">Du calme intérieur la parfaite allégresse.</l>
					</lg>
				</div></body></text></TEI>