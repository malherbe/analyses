<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Imitation De Jésus-Christ</title>
				<title type="medium">Édition électronique</title>
				<author key="COR">
					<name>
						<forename>Pierre</forename>
						<surname>CORNEILLE</surname>
					</name>
					<date from="1606" to="1684">1606-1684</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>13205 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2016</date>
				<idno type="local">COR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Imitation De Jésus-Christ</title>
						<author>Pierre Corneille</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/pierrecorneilleimmitadiondejesuschrist.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Imitation De Jésus-Christ</title>
						<author>Pierre Corneille</author>
						<editor>M. Ch. Marty-Laveaux</editor>
						<edition>nouvelle édition</edition>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k51199.r=pierre%20corneille?rk=85837;2</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie de L. Hachette et Cie</publisher>
							<date when="1862">1862</date>
						</imprint>
						<biblScope unit="tome">Tome 8</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1656">1656</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les chiffres arabes des livres et chapitres ont été remplacés par des chiffres romains</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">LIVRE DEUXIÈME</head><div type="poem" key="COR67">
					<head type="main">CHAPITRE X</head>
					<head type="sub">De la reconnoissance pour les graces de Dieu.</head>
					<lg n="1">
						<l n="1" num="1.1"><space unit="char" quantity="8"></space>Oh ! Que tu sais mal te connoître,</l>
						<l n="2" num="1.2"><space unit="char" quantity="8"></space>Mortel, et que mal à propos,</l>
						<l n="3" num="1.3">Toi que pour le travail Dieu voulut faire naître,</l>
						<l n="4" num="1.4"><space unit="char" quantity="8"></space>Tu cherches ici du repos !</l>
						<l n="5" num="1.5"><space unit="char" quantity="8"></space>Songe plus à la patience</l>
						<l n="6" num="1.6"><space unit="char" quantity="8"></space>Qu’à cette aimable confiance</l>
						<l n="7" num="1.7">Que versent dans les cœurs ses consolations,</l>
						<l n="8" num="1.8">Et te prépare aux croix que sa justice envoie,</l>
						<l n="9" num="1.9"><space unit="char" quantity="8"></space>Plus qu’à cette innocente joie</l>
						<l n="10" num="1.10">Que mêlent ses bontés aux tribulations.</l>
					</lg>
					<lg n="2">
						<l n="11" num="2.1"><space unit="char" quantity="8"></space>Quels mondains à Dieu si rebelles</l>
						<l n="12" num="2.2"><space unit="char" quantity="8"></space>De leurs âmes voudroient bannir</l>
						<l n="13" num="2.3">Le goût de ces douceurs toutes spirituelles,</l>
						<l n="14" num="2.4"><space unit="char" quantity="8"></space>S’ils pouvoient toujours l’obtenir ?</l>
						<l n="15" num="2.5"><space unit="char" quantity="8"></space>Les pompes que le siècle étale</l>
						<l n="16" num="2.6"><space unit="char" quantity="8"></space>N’ont jamais rien qui les égale :</l>
						<l n="17" num="2.7">Les délices des sens n’en sauroient approcher ;</l>
						<l n="18" num="2.8">Et de quelques appas qu’elles nous semblent pleines,</l>
						<l n="19" num="2.9"><space unit="char" quantity="8"></space>Celles du siècle enfin sont vaines,</l>
						<l n="20" num="2.10">Et la honte s’attache à celles de la chair.</l>
					</lg>
					<lg n="3">
						<l n="21" num="3.1"><space unit="char" quantity="8"></space>Mais les douceurs spirituelles,</l>
						<l n="22" num="3.2"><space unit="char" quantity="8"></space>Seules dignes de nos desirs,</l>
						<l n="23" num="3.3">Seules n’ont rien de bas, et seules toujours belles,</l>
						<l n="24" num="3.4"><space unit="char" quantity="8"></space>Forment de solides plaisirs.</l>
						<l n="25" num="3.5"><space unit="char" quantity="8"></space>C’est la vertu qui les fait naître,</l>
						<l n="26" num="3.6"><space unit="char" quantity="8"></space>Et Dieu, cet adorable maître,</l>
						<l n="27" num="3.7">N’en est jamais avare aux cœurs purs et constants ;</l>
						<l n="28" num="3.8">Mais on n’en jouit pas autant qu’on le souhaite,</l>
						<l n="29" num="3.9"><space unit="char" quantity="8"></space>Et l’âme la moins imparfaite</l>
						<l n="30" num="3.10">Voit la tentation ne cesser pas longtemps.</l>
					</lg>
					<lg n="4">
						<l n="31" num="4.1"><space unit="char" quantity="8"></space>Par trop d’espoir en nos mérites</l>
						<l n="32" num="4.2"><space unit="char" quantity="8"></space>La fausse liberté d’esprit</l>
						<l n="33" num="4.3">S’oppose puissamment à ces douces visites</l>
						<l n="34" num="4.4"><space unit="char" quantity="8"></space>Dont nous régale Jésus-Christ.</l>
						<l n="35" num="4.5"><space unit="char" quantity="8"></space>Lorsque sa grâce nous console,</l>
						<l n="36" num="4.6"><space unit="char" quantity="8"></space>D’un seul accent de sa parole</l>
						<l n="37" num="4.7">Il remplit tout l’excès de sa bénignité ;</l>
						<l n="38" num="4.8">Mais l’homme y répond mal, l’homme l’en désavoue,</l>
						<l n="39" num="4.9"><space unit="char" quantity="8"></space>S’il ne rend grâces, s’il ne loue,</l>
						<l n="40" num="4.10">S’il ne rapporte tout à sa haute bonté.</l>
					</lg>
					<lg n="5">
						<l n="41" num="5.1"><space unit="char" quantity="8"></space>Veux-tu que la grâce divine</l>
						<l n="42" num="5.2"><space unit="char" quantity="8"></space>Coule abondamment dans ton cœur ?</l>
						<l n="43" num="5.3">Fais remonter ses dons jusqu’à son origine ;</l>
						<l n="44" num="5.4"><space unit="char" quantity="8"></space>N’en sois point ingrat à l’auteur.</l>
						<l n="45" num="5.5"><space unit="char" quantity="8"></space>Il fait toujours grâce nouvelle</l>
						<l n="46" num="5.6"><space unit="char" quantity="8"></space>À qui, pour la moindre étincelle,</l>
						<l n="47" num="5.7">Lui témoigne un esprit vraiment reconnoissant ;</l>
						<l n="48" num="5.8">Mais il sait bien aussi remplir cette menace</l>
						<l n="49" num="5.9"><space unit="char" quantity="8"></space>D’ôter au superbe la grâce</l>
						<l n="50" num="5.10">Dont il prodigue à l’humble un effet plus puissant.</l>
					</lg>
					<lg n="6">
						<l n="51" num="6.1"><space unit="char" quantity="8"></space>Loin, consolations funestes,</l>
						<l n="52" num="6.2"><space unit="char" quantity="8"></space>Qui m’ôtez la componction !</l>
						<l n="53" num="6.3">Loin de moi ces pensers qui semblent tous célestes,</l>
						<l n="54" num="6.4"><space unit="char" quantity="8"></space>Et m’enflent de présomption !</l>
						<l n="55" num="6.5"><space unit="char" quantity="8"></space>Dieu n’a pas toujours agréable</l>
						<l n="56" num="6.6"><space unit="char" quantity="8"></space>Tout ce qu’un dévot trouve aimable ;</l>
						<l n="57" num="6.7">Toute élévation n’a pas la sainteté :</l>
						<l n="58" num="6.8">On peut monter bien haut sans atteindre aux couronnes ;</l>
						<l n="59" num="6.9"><space unit="char" quantity="8"></space>Toutes douceurs ne sont pas bonnes,</l>
						<l n="60" num="6.10">Et tous les bons desirs n’ont pas la pureté.</l>
					</lg>
					<lg n="7">
						<l n="61" num="7.1"><space unit="char" quantity="8"></space>J’aime, j’aime bien cette grâce</l>
						<l n="62" num="7.2"><space unit="char" quantity="8"></space>Qui me sait mieux humilier,</l>
						<l n="63" num="7.3">Qui me tient mieux en crainte, et jamais ne se lasse</l>
						<l n="64" num="7.4"><space unit="char" quantity="8"></space>De m’apprendre à mieux m’oublier :</l>
						<l n="65" num="7.5"><space unit="char" quantity="8"></space>Ceux que ses dons daignent instruire,</l>
						<l n="66" num="7.6"><space unit="char" quantity="8"></space>Ceux qui savent où peut réduire</l>
						<l n="67" num="7.7">Le douloureux effet de sa substraction,</l>
						<l n="68" num="7.8">Jamais du bien qu’ils font n’osent prendre la gloire,</l>
						<l n="69" num="7.9"><space unit="char" quantity="8"></space>Jamais n’ôtent de leur mémoire</l>
						<l n="70" num="7.10">Qu’ils ne sont que misère et qu’imperfection.</l>
					</lg>
					<lg n="8">
						<l n="71" num="8.1"><space unit="char" quantity="8"></space>Qu’une sainte reconnoissance</l>
						<l n="72" num="8.2"><space unit="char" quantity="8"></space>Rende donc à Dieu tout le sien ;</l>
						<l n="73" num="8.3">Et n’impute qu’à toi, qu’à ta propre impuissance,</l>
						<l n="74" num="8.4"><space unit="char" quantity="8"></space>Tout ce qui s’y mêle du tien :</l>
						<l n="75" num="8.5"><space unit="char" quantity="8"></space>Je m’explique, et je te veux dire</l>
						<l n="76" num="8.6"><space unit="char" quantity="8"></space>Que des grâces que Dieu t’inspire</l>
						<l n="77" num="8.7">Tu pousses jusqu’à lui d’humbles remercîments,</l>
						<l n="78" num="8.8">Et que te chargeant seul de toutes tes foiblesses,</l>
						<l n="79" num="8.9"><space unit="char" quantity="8"></space>Tu te prosternes, tu confesses</l>
						<l n="80" num="8.10">Qu’il ne te peut devoir que de longs châtiments.</l>
					</lg>
					<lg n="9">
						<l n="81" num="9.1"><space unit="char" quantity="8"></space>Mets-toi dans le plus bas étage,</l>
						<l n="82" num="9.2"><space unit="char" quantity="8"></space>Il te donnera le plus haut :</l>
						<l n="83" num="9.3">C’est par l’humilité que le plus grand courage</l>
						<l n="84" num="9.4"><space unit="char" quantity="8"></space>Montre pleinement ce qu’il vaut.</l>
						<l n="85" num="9.5"><space unit="char" quantity="8"></space>La hauteur même dans le monde</l>
						<l n="86" num="9.6"><space unit="char" quantity="8"></space>Sur ce bas étage se fonde,</l>
						<l n="87" num="9.7">Et le plus haut sans lui n’y sauroit subsister :</l>
						<l n="88" num="9.8">Le plus grand devant Dieu, c’est le moindre en soi-même,</l>
						<l n="89" num="9.9"><space unit="char" quantity="8"></space>Et les vertus que le ciel aime</l>
						<l n="90" num="9.10">Par les ravalements trouvent l’art d’y monter.</l>
					</lg>
					<lg n="10">
						<l n="91" num="10.1"><space unit="char" quantity="8"></space>La gloire des saints ne s’achève</l>
						<l n="92" num="10.2"><space unit="char" quantity="8"></space>Que par le mépris qu’ils en font ;</l>
						<l n="93" num="10.3">Leur abaissement croît autant qu’elle s’élève</l>
						<l n="94" num="10.4"><space unit="char" quantity="8"></space>Et devient toujours plus profond.</l>
						<l n="95" num="10.5"><space unit="char" quantity="8"></space>La vaine gloire a peu de place</l>
						<l n="96" num="10.6"><space unit="char" quantity="8"></space>Dans un cœur où règne la grâce,</l>
						<l n="97" num="10.7">L’amour de la céleste occupe tout le lieu ;</l>
						<l n="98" num="10.8">Et cette propre estime, où se plaît la nature,</l>
						<l n="99" num="10.9"><space unit="char" quantity="8"></space>Ne sauroit trouver d’ouverture</l>
						<l n="100" num="10.10">Dans celui qui se fonde et s’affermit en Dieu.</l>
					</lg>
					<lg n="11">
						<l n="101" num="11.1"><space unit="char" quantity="8"></space>Quand l’homme à cet être sublime</l>
						<l n="102" num="11.2"><space unit="char" quantity="8"></space>Rend tout ce qu’il reçoit de bien,</l>
						<l n="103" num="11.3">D’aucun autre ici-bas il ne cherche l’estime :</l>
						<l n="104" num="11.4"><space unit="char" quantity="8"></space>Ici-bas il ne voit plus rien.</l>
						<l n="105" num="11.5"><space unit="char" quantity="8"></space>Dans le combat, dans la victoire,</l>
						<l n="106" num="11.6"><space unit="char" quantity="8"></space>De tels cœurs ne veulent de gloire</l>
						<l n="107" num="11.7">Que celle que Dieu seul y verse de ses mains :</l>
						<l n="108" num="11.8">Tout leur amour est Dieu, tout leur but sa louange,</l>
						<l n="109" num="11.9"><space unit="char" quantity="8"></space>Tout leur souhait, que sans mélange,</l>
						<l n="110" num="11.10">Elle éclate partout, en eux, en tous les saints.</l>
					</lg>
					<lg n="12">
						<l n="111" num="12.1"><space unit="char" quantity="8"></space>Aussi sa bonté semble croître</l>
						<l n="112" num="12.2"><space unit="char" quantity="8"></space>Des louanges que tu lui rends ;</l>
						<l n="113" num="12.3">Et pour ses moindres dons savoir le reconnoître,</l>
						<l n="114" num="12.4"><space unit="char" quantity="8"></space>C’est en attirer de plus grands.</l>
						<l n="115" num="12.5"><space unit="char" quantity="8"></space>Tiens ses moindres grâces pour grandes,</l>
						<l n="116" num="12.6"><space unit="char" quantity="8"></space>N’en reçois point que tu n’en rendes :</l>
						<l n="117" num="12.7">Crois plus avoir reçu que tu n’as mérité ;</l>
						<l n="118" num="12.8">Estime précieux, estime incomparable</l>
						<l n="119" num="12.9"><space unit="char" quantity="8"></space>Le don le moins considérable,</l>
						<l n="120" num="12.10">Et redouble son prix par ton humilité.</l>
					</lg>
					<lg n="13">
						<l n="121" num="13.1"><space unit="char" quantity="8"></space>Si dans les moindres dons tu passes</l>
						<l n="122" num="13.2"><space unit="char" quantity="8"></space>À considérer leur auteur,</l>
						<l n="123" num="13.3">Verras-tu rien de vil, rien de foible en ses grâces,</l>
						<l n="124" num="13.4"><space unit="char" quantity="8"></space>Rien de contemptible à ton cœur ?</l>
						<l n="125" num="13.5"><space unit="char" quantity="8"></space>On ne peut sans ingratitude</l>
						<l n="126" num="13.6"><space unit="char" quantity="8"></space>Nommer rien de bas ni de rude,</l>
						<l n="127" num="13.7">Quand il vient d’un si grand et si doux souverain ;</l>
						<l n="128" num="13.8">Et lorsqu’il fait pleuvoir des maux et des traverses,</l>
						<l n="129" num="13.9"><space unit="char" quantity="8"></space>Ce ne sont que grâces diverses</l>
						<l n="130" num="13.10">Dont avec pleine joie il faut bénir sa main.</l>
					</lg>
					<lg n="14">
						<l n="131" num="14.1"><space unit="char" quantity="8"></space>Cette charité, toujours vive,</l>
						<l n="132" num="14.2"><space unit="char" quantity="8"></space>Qui n’a que notre bien pour but,</l>
						<l n="133" num="14.3">Dispose avec amour tout ce qui nous arrive,</l>
						<l n="134" num="14.4"><space unit="char" quantity="8"></space>Et fait tout pour notre salut.</l>
						<l n="135" num="14.5"><space unit="char" quantity="8"></space>Montre une âme reconnoissante</l>
						<l n="136" num="14.6"><space unit="char" quantity="8"></space>Quand tu sens la grâce puissante ;</l>
						<l n="137" num="14.7">Sois humble et patient dans sa substraction ;</l>
						<l n="138" num="14.8">Joins, pour la rappeler, les pleurs à la prière,</l>
						<l n="139" num="14.9"><space unit="char" quantity="8"></space>Et de peur de la perdre entière,</l>
						<l n="140" num="14.10">Unis la vigilance à la soumission.</l>
					</lg>
				</div></body></text></TEI>