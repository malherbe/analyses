<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Imitation De Jésus-Christ</title>
				<title type="medium">Édition électronique</title>
				<author key="COR">
					<name>
						<forename>Pierre</forename>
						<surname>CORNEILLE</surname>
					</name>
					<date from="1606" to="1684">1606-1684</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>13205 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2016</date>
				<idno type="local">COR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Imitation De Jésus-Christ</title>
						<author>Pierre Corneille</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/pierrecorneilleimmitadiondejesuschrist.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Imitation De Jésus-Christ</title>
						<author>Pierre Corneille</author>
						<editor>M. Ch. Marty-Laveaux</editor>
						<edition>nouvelle édition</edition>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k51199.r=pierre%20corneille?rk=85837;2</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie de L. Hachette et Cie</publisher>
							<date when="1862">1862</date>
						</imprint>
						<biblScope unit="tome">Tome 8</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1656">1656</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les chiffres arabes des livres et chapitres ont été remplacés par des chiffres romains</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">LIVRE PREMIER</head><div type="poem" key="COR40">
					<head type="main">CHAPITRE VIII</head>
					<head type="sub">Qu’il faut éviter la trop grande familiarité.</head>
					<lg n="1">
						<l n="1" num="1.1">Ne fais point confidence avec toutes personnes,</l>
						<l n="2" num="1.2">Regarde où tu répands les secrets de ton cœur ;</l>
						<l n="3" num="1.3">Prends et suis les conseils de qui craint le seigneur ;</l>
						<l n="4" num="1.4">Choisis tes amitiés, et n’en fais que de bonnes ;</l>
						<l n="5" num="1.5">Hante peu la jeunesse, et de ceux du dehors</l>
						<l n="6" num="1.6"><space unit="char" quantity="8"></space>Souffre rarement les abords.</l>
					</lg>
					<lg n="2">
						<l n="7" num="2.1">Jamais autour du riche à flatter ne t’exerce ;</l>
						<l n="8" num="2.2">Vis sans démangeaison de te montrer aux grands ;</l>
						<l n="9" num="2.3">Vois l’humble, le dévot, le simple, et n’entreprends</l>
						<l n="10" num="2.4">De faire qu’avec eux un long et plein commerce ;</l>
						<l n="11" num="2.5">Et n’y traite surtout que des biens précieux</l>
						<l n="12" num="2.6"><space unit="char" quantity="8"></space>Dont une âme achète les cieux.</l>
					</lg>
					<lg n="3">
						<l n="13" num="3.1">Évite avec grand soin la pratique des femmes,</l>
						<l n="14" num="3.2">Ton ennemi par là peut trouver ton défaut ;</l>
						<l n="15" num="3.3">Recommande en commun aux bontés du très-haut</l>
						<l n="16" num="3.4">Celles dont les vertus embellissent les âmes ;</l>
						<l n="17" num="3.5">Et sans en voir jamais qu’avec un prompt adieu,</l>
						<l n="18" num="3.6"><space unit="char" quantity="8"></space>Aime-les toutes, mais en Dieu.</l>
					</lg>
					<lg n="4">
						<l n="19" num="4.1">Ce n’est qu’avec lui seul, ce n’est qu’avec ses anges</l>
						<l n="20" num="4.2">Que doit un vrai chrétien se rendre familier :</l>
						<l n="21" num="4.3">Porte-lui tout ton cœur, deviens leur écolier ;</l>
						<l n="22" num="4.4">Adore en lui sa gloire, apprends d’eux ses louanges ;</l>
						<l n="23" num="4.5">Et bornant tes desirs à ses dons éternels,</l>
						<l n="24" num="4.6"><space unit="char" quantity="8"></space>Fuis d’être connu des mortels.</l>
					</lg>
					<lg n="5">
						<l n="25" num="5.1">La charité vers tous est toujours nécessaire,</l>
						<l n="26" num="5.2">Mais non pas avec tous un accès trop ouvert :</l>
						<l n="27" num="5.3">La réputation assez souvent s’y perd ;</l>
						<l n="28" num="5.4">Et tel qui plaît de loin, de près cesse de plaire :</l>
						<l n="29" num="5.5">Tant ce brillant éclat qui ne fait qu’éblouir</l>
						<l n="30" num="5.6"><space unit="char" quantity="8"></space>Est sujet à s’évanouir !</l>
					</lg>
					<lg n="6">
						<l n="31" num="6.1">Oui, souvent il arrive, et contre notre envie,</l>
						<l n="32" num="6.2">Que plus on prend de peine à se communiquer,</l>
						<l n="33" num="6.3">Plus cet effort nous trompe, et force à remarquer</l>
						<l n="34" num="6.4">Les désordres secrets qui souillent notre vie,</l>
						<l n="35" num="6.5">Et que ce qu’un grand nom avoit semé de bruit</l>
						<l n="36" num="6.6"><space unit="char" quantity="8"></space>Par la présence est tôt détruit.</l>
					</lg>
				</div></body></text></TEI>