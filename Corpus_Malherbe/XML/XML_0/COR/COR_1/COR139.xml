<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Imitation De Jésus-Christ</title>
				<title type="medium">Édition électronique</title>
				<author key="COR">
					<name>
						<forename>Pierre</forename>
						<surname>CORNEILLE</surname>
					</name>
					<date from="1606" to="1684">1606-1684</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>13205 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2016</date>
				<idno type="local">COR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Imitation De Jésus-Christ</title>
						<author>Pierre Corneille</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/pierrecorneilleimmitadiondejesuschrist.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Imitation De Jésus-Christ</title>
						<author>Pierre Corneille</author>
						<editor>M. Ch. Marty-Laveaux</editor>
						<edition>nouvelle édition</edition>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k51199.r=pierre%20corneille?rk=85837;2</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie de L. Hachette et Cie</publisher>
							<date when="1862">1862</date>
						</imprint>
						<biblScope unit="tome">Tome 8</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1656">1656</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les chiffres arabes des livres et chapitres ont été remplacés par des chiffres romains</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">LIVRE QUATRIÈME</head><div type="poem" key="COR139">
					<head type="main">CHAPITRE X</head>
					<head type="sub">Qu’il ne faut pas aisément quitter la sainte communion.</head>
					<lg n="1">
						<l n="1" num="1.1"><space unit="char" quantity="8"></space>Tu dois avoir souvent recours</l>
						<l n="2" num="1.2">À la source de grâce et de miséricorde,</l>
						<l n="3" num="1.3">Cette fontaine pure où se forme le cours</l>
						<l n="4" num="1.4">D’un torrent de bonté qui sur toi se déborde.</l>
						<l n="5" num="1.5"><space unit="char" quantity="8"></space>Ainsi tu sauras t’affranchir</l>
						<l n="6" num="1.6"><space unit="char" quantity="8"></space>De tout ce qui te fait gauchir</l>
						<l n="7" num="1.7"><space unit="char" quantity="8"></space>Vers les passions et les vices ;</l>
						<l n="8" num="1.8">Ainsi plus vigoureux, ainsi plus vigilant,</l>
						<l n="9" num="1.9">Des attaques du diable et de ses artifices</l>
						<l n="10" num="1.10">Tu braveras la ruse et l’effort insolent.</l>
					</lg>
					<lg n="2">
						<l n="11" num="2.1"><space unit="char" quantity="8"></space>Ce fier ennemi des mortels</l>
						<l n="12" num="2.2">De la communion sait quel bonheur procède,</l>
						<l n="13" num="2.3">Et combien on reçoit au pied de mes autels,</l>
						<l n="14" num="2.4">En ce festin sacré, de fruit et de remède.</l>
						<l n="15" num="2.5"><space unit="char" quantity="8"></space>Il ne perd point d’occasions</l>
						<l n="16" num="2.6"><space unit="char" quantity="8"></space>De semer ses illusions</l>
						<l n="17" num="2.7"><space unit="char" quantity="8"></space>Pour en détourner les fidèles :</l>
						<l n="18" num="2.8">Il en fait son grand œuvre, et met tout son pouvoir</l>
						<l n="19" num="2.9">À ne laisser en l’âme aucunes étincelles</l>
						<l n="20" num="2.10">Qui puissent rallumer l’ardeur de ce devoir.</l>
					</lg>
					<lg n="3">
						<l n="21" num="3.1"><space unit="char" quantity="8"></space>Plus il te voit t’y préparer</l>
						<l n="22" num="3.2">Avec une ferveur d’un saint espoir guidée,</l>
						<l n="23" num="3.3">Plus les fantômes noirs qu’il te vient figurer</l>
						<l n="24" num="3.4">Font un épais nuage et brouillent ton idée.</l>
						<l n="25" num="3.5"><space unit="char" quantity="8"></space>Tu lis dans Job en plus d’un lieu</l>
						<l n="26" num="3.6"><space unit="char" quantity="8"></space>Que parmi les enfants de Dieu</l>
						<l n="27" num="3.7"><space unit="char" quantity="8"></space>Cet esprit ténébreux se coule ;</l>
						<l n="28" num="3.8">C’est contre eux qu’il s’efforce, et sa malignité</l>
						<l n="29" num="3.9">Prend mille objets impurs que devant eux il roule,</l>
						<l n="30" num="3.10">Pour les remplir de crainte ou de perplexité.</l>
					</lg>
					<lg n="4">
						<l n="31" num="4.1"><space unit="char" quantity="8"></space>Il tâche par mille embarras</l>
						<l n="32" num="4.2">De vaincre ou d’affoiblir le zèle qui t’enflamme,</l>
						<l n="33" num="4.3">Et de se rendre maître à force de combats</l>
						<l n="34" num="4.4">De cette aveugle foi qui t’illumine l’âme.</l>
						<l n="35" num="4.5"><space unit="char" quantity="8"></space>Il ne néglige aucun secret</l>
						<l n="36" num="4.6"><space unit="char" quantity="8"></space>Pour t’éloigner de ce banquet,</l>
						<l n="37" num="4.7"><space unit="char" quantity="8"></space>Ou t’en faire approcher plus tiède ;</l>
						<l n="38" num="4.8">Mais il est en ta main de le rendre impuissant :</l>
						<l n="39" num="4.9">Son plus heureux effort n’abat que qui lui cède,</l>
						<l n="40" num="4.10">Et ne peut t’ébranler, si ton cœur n’y consent.</l>
					</lg>
					<lg n="5">
						<l n="41" num="5.1"><space unit="char" quantity="8"></space>Quelques horribles saletés</l>
						<l n="42" num="5.2">Dont contre toi sa rage excite la tempête,</l>
						<l n="43" num="5.3">Tu n’as qu’à te moquer de leurs impuretés,</l>
						<l n="44" num="5.4">Et tu renverseras leurs foudres sur sa tête :</l>
						<l n="45" num="5.5"><space unit="char" quantity="8"></space>Tu n’as qu’à traiter de mépris</l>
						<l n="46" num="5.6"><space unit="char" quantity="8"></space>Ce roi des malheureux esprits,</l>
						<l n="47" num="5.7"><space unit="char" quantity="8"></space>Pour le dépouiller de sa force.</l>
						<l n="48" num="5.8">Ris donc de son insulte, et quelque émotion</l>
						<l n="49" num="5.9">Dont il ose à tes yeux jeter l’indigne amorce,</l>
						<l n="50" num="5.10">Ne te relâche point de la communion.</l>
					</lg>
					<lg n="6">
						<l n="51" num="6.1"><space unit="char" quantity="8"></space>Souvent, à force d’y penser,</l>
						<l n="52" num="6.2">Le soin d’être dévot trop longtemps inquiète ;</l>
						<l n="53" num="6.3">Souvent l’anxiété de se bien confesser</l>
						<l n="54" num="6.4">Enveloppe l’esprit d’une langueur secrète.</l>
						<l n="55" num="6.5"><space unit="char" quantity="8"></space>Fais choix alors de confidents</l>
						<l n="56" num="6.6"><space unit="char" quantity="8"></space>Qui soient éclairés et prudents,</l>
						<l n="57" num="6.7"><space unit="char" quantity="8"></space>Et bannis tout le vain scrupule :</l>
						<l n="58" num="6.8">Il empêche ma grâce, et la précaution</l>
						<l n="59" num="6.9">Que lui fait apporter son effroi ridicule</l>
						<l n="60" num="6.10">Éteint le plus beau feu de la dévotion.</l>
					</lg>
					<lg n="7">
						<l n="61" num="7.1"><space unit="char" quantity="8"></space>Faut-il pour un trouble léger,</l>
						<l n="62" num="7.2">Pour un amusement qu’un vain objet excite,</l>
						<l n="63" num="7.3">Pour une pesanteur qui te vient assiéger,</l>
						<l n="64" num="7.4">Que ta communion se diffère ou se quitte ?</l>
						<l n="65" num="7.5"><space unit="char" quantity="8"></space>Porte tout à ce tribunal,</l>
						<l n="66" num="7.6"><space unit="char" quantity="8"></space>Où, par un bonheur sans égal,</l>
						<l n="67" num="7.7"><space unit="char" quantity="8"></space>Qui s’accuse aussitôt s’épure :</l>
						<l n="68" num="7.8">Pardonne à qui t’offense, et cours aux pieds d’autrui</l>
						<l n="69" num="7.9">Lui demander pardon, si tu lui fis injure ;</l>
						<l n="70" num="7.10">Tu l’obtiendras de moi, si tu le veux de lui.</l>
					</lg>
					<lg n="8">
						<l n="71" num="8.1"><space unit="char" quantity="8"></space>Que peut avoir d’utilité</l>
						<l n="72" num="8.2">De la confession cette folle remise ?</l>
						<l n="73" num="8.3">De quoi te peut servir cette facilité</l>
						<l n="74" num="8.4">À reculer un bien que t’offre mon église ?</l>
						<l n="75" num="8.5"><space unit="char" quantity="8"></space>Vomis tout ce maudit poison,</l>
						<l n="76" num="8.6"><space unit="char" quantity="8"></space>Et pour en purger ta raison</l>
						<l n="77" num="8.7"><space unit="char" quantity="8"></space>Cours en hâte à ce grand remède :</l>
						<l n="78" num="8.8">Tu t’en trouveras mieux, et tu dois redouter</l>
						<l n="79" num="8.9">Qu’à l’obstacle présent quelque autre ne succède,</l>
						<l n="80" num="8.10">Plus fâcheux à souffrir et plus fort à dompter.</l>
					</lg>
					<lg n="9">
						<l n="81" num="9.1"><space unit="char" quantity="8"></space>Remettre ainsi de jour en jour</l>
						<l n="82" num="9.2">Pour te mieux préparer à ce bonheur insigne,</l>
						<l n="83" num="9.3">C’est te priver longtemps de ce gage d’amour,</l>
						<l n="84" num="9.4">Et peut-être à la fin t’en rendre plus indigne.</l>
						<l n="85" num="9.5"><space unit="char" quantity="8"></space>Romps le plus tôt que tu pourras</l>
						<l n="86" num="9.6"><space unit="char" quantity="8"></space>Les chaînes de ces embarras</l>
						<l n="87" num="9.7"><space unit="char" quantity="8"></space>Dont ta propre lenteur t’accable :</l>
						<l n="88" num="9.8">Nourrir l’inquiétude apporte peu de fruit,</l>
						<l n="89" num="9.9">Et l’on s’avance mal quand on refuit ma table</l>
						<l n="90" num="9.10">Pour des empêchements que chaque jour produit.</l>
					</lg>
					<lg n="10">
						<l n="91" num="10.1"><space unit="char" quantity="8"></space>Sais-tu que l’assoupissement</l>
						<l n="92" num="10.2">Où te laisse plonger ta langueur insensible</l>
						<l n="93" num="10.3">T’achemine à grands pas à l’endurcissement,</l>
						<l n="94" num="10.4">Et qu’à force de temps il devient invincible ?</l>
						<l n="95" num="10.5"><space unit="char" quantity="8"></space>Qu’il est de lâches, qu’il en est,</l>
						<l n="96" num="10.6"><space unit="char" quantity="8"></space>Dont la tépidité s’y plaît</l>
						<l n="97" num="10.7"><space unit="char" quantity="8"></space>Jusqu’à le rendre volontaire,</l>
						<l n="98" num="10.8">Et dont la nonchalance aime à prendre aux cheveux</l>
						<l n="99" num="10.9">La moindre occasion d’éloigner un mystère</l>
						<l n="100" num="10.10">Qui les obligeroit d’avoir mieux l’œil sur eux !</l>
					</lg>
					<lg n="11">
						<l n="101" num="11.1"><space unit="char" quantity="8"></space>Oh ! Que foible est leur charité !</l>
						<l n="102" num="11.2">Que leur dévotion est traînante et débile !</l>
						<l n="103" num="11.3">Et que ce zèle est faux dont l’imbécillité</l>
						<l n="104" num="11.4">À quitter un tel bien se trouve si facile !</l>
						<l n="105" num="11.5"><space unit="char" quantity="8"></space>Heureux l’homme qui tous les jours</l>
						<l n="106" num="11.6"><space unit="char" quantity="8"></space>Pour recevoir un tel secours</l>
						<l n="107" num="11.7"><space unit="char" quantity="8"></space>Épure assez sa conscience,</l>
						<l n="108" num="11.8">Et n’en passeroit point sans un si grand appui,</l>
						<l n="109" num="11.9">Si de ses directeurs il en avoit licence,</l>
						<l n="110" num="11.10">Ou qu’il ne craignît point qu’on parlât trop de lui !</l>
					</lg>
					<lg n="12">
						<l n="111" num="12.1"><space unit="char" quantity="8"></space>Quand par un humble sentiment</l>
						<l n="112" num="12.2">Le respect en conseille une sainte abstinence,</l>
						<l n="113" num="12.3">Ou qu’on y voit d’ailleurs un juste empêchement,</l>
						<l n="114" num="12.4">Un homme est à louer de cette révérence ;</l>
						<l n="115" num="12.5"><space unit="char" quantity="8"></space>Mais lorsque parmi ce conseil</l>
						<l n="116" num="12.6"><space unit="char" quantity="8"></space>Il se glisse un morne sommeil,</l>
						<l n="117" num="12.7"><space unit="char" quantity="8"></space>On se doit exciter soi-même,</l>
						<l n="118" num="12.8">Faire tout ce que peut l’humaine infirmité :</l>
						<l n="119" num="12.9">Mon secours est tout prêt, et ma bonté suprême</l>
						<l n="120" num="12.10">Considère surtout la bonne volonté.</l>
					</lg>
					<lg n="13">
						<l n="121" num="13.1"><space unit="char" quantity="8"></space>Alors que ta dévotion</l>
						<l n="122" num="13.2">A pour s’en abstenir des causes légitimes,</l>
						<l n="123" num="13.3">Ton desir vertueux, ta bonne intention,</l>
						<l n="124" num="13.4">Te peuvent en donner les fruits les plus sublimes.</l>
						<l n="125" num="13.5"><space unit="char" quantity="8"></space>Quiconque a Dieu devant les yeux</l>
						<l n="126" num="13.6"><space unit="char" quantity="8"></space>Peut en tout temps, peut en tous lieux</l>
						<l n="127" num="13.7"><space unit="char" quantity="8"></space>Goûter en esprit ce mystère :</l>
						<l n="128" num="13.8">Il n’est obstacle aucun qui l’en puisse empêcher,</l>
						<l n="129" num="13.9">Et c’est toujours pour l’âme un repas salutaire</l>
						<l n="130" num="13.10">Quand, au défaut du corps, elle en sait approcher ;</l>
					</lg>
					<lg n="14">
						<l n="131" num="14.1"><space unit="char" quantity="8"></space>Non que cette communion,</l>
						<l n="132" num="14.2">Qu’il peut faire en tout temps, toute spirituelle,</l>
						<l n="133" num="14.3">Doive monter si haut en son opinion</l>
						<l n="134" num="14.4">Que son esprit content néglige l’actuelle :</l>
						<l n="135" num="14.5"><space unit="char" quantity="8"></space>Il faut que souvent sa ferveur</l>
						<l n="136" num="14.6"><space unit="char" quantity="8"></space>De la bouche comme du cœur</l>
						<l n="137" num="14.7"><space unit="char" quantity="8"></space>Reçoive ce vrai pain des anges,</l>
						<l n="138" num="14.8">Qu’il ait des temps réglés pour un si digne effet,</l>
						<l n="139" num="14.9">Et s’y donne pour but ma gloire et mes louanges,</l>
						<l n="140" num="14.10">Plus que ce qui le flatte et qui le satisfait.</l>
					</lg>
					<lg n="15">
						<l n="141" num="15.1"><space unit="char" quantity="8"></space>Attendant ces jours bienheureux,</l>
						<l n="142" num="15.2">Contemple dans la crèche un Dieu qui s’est fait homme ;</l>
						<l n="143" num="15.3">Repasse en ton esprit mon trépas douloureux ;</l>
						<l n="144" num="15.4">Vois l’œuvre du salut qu’en la croix je consomme :</l>
						<l n="145" num="15.5"><space unit="char" quantity="8"></space>Autant de fois qu’un saint transport</l>
						<l n="146" num="15.6"><space unit="char" quantity="8"></space>Dans ma naissance ou dans ma mort</l>
						<l n="147" num="15.7"><space unit="char" quantity="8"></space>Prendra de quoi croître ta flamme,</l>
						<l n="148" num="15.8">Ton zèle autant de fois saura mystiquement</l>
						<l n="149" num="15.9">D’une invisible main communier ton âme,</l>
						<l n="150" num="15.10">Et recevra le fruit de ce grand sacrement.</l>
					</lg>
					<lg n="16">
						<l n="151" num="16.1"><space unit="char" quantity="8"></space>Qui ne daigne s’y préparer</l>
						<l n="152" num="16.2">Qu’alors qu’il est pressé par quelque grande fête,</l>
						<l n="153" num="16.3">Et que le jour pour lui semble le desirer,</l>
						<l n="154" num="16.4">Y portera souvent une âme fort mal prête.</l>
						<l n="155" num="16.5"><space unit="char" quantity="8"></space>Heureux qui du plus digne apprêt,</l>
						<l n="156" num="16.6"><space unit="char" quantity="8"></space>Sans attache au propre intérêt,</l>
						<l n="157" num="16.7"><space unit="char" quantity="8"></space>Fait son ordinaire exercice,</l>
						<l n="158" num="16.8">Et s’offre en holocauste à son père immortel,</l>
						<l n="159" num="16.9">Quand pour le sacrement ou pour le sacrifice</l>
						<l n="160" num="16.10">Il se met à ma table, ou monte à mon autel !</l>
					</lg>
					<lg n="17">
						<l n="161" num="17.1"><space unit="char" quantity="8"></space>Observe pour dernier avis</l>
						<l n="162" num="17.2">De n’être ni trop long, ni trop court en ta messe :</l>
						<l n="163" num="17.3">Contente ainsi que toi ceux avec qui tu vis,</l>
						<l n="164" num="17.4">Et garde un train commun en qui rien ne les blesse.</l>
						<l n="165" num="17.5"><space unit="char" quantity="8"></space>Un prêtre n’est bon que pour lui,</l>
						<l n="166" num="17.6"><space unit="char" quantity="8"></space>S’il gêne le zèle d’autrui,</l>
						<l n="167" num="17.7"><space unit="char" quantity="8"></space>Faute de suivre la coutume ;</l>
						<l n="168" num="17.8">Et tu dois regarder ce qui profite à tous</l>
						<l n="169" num="17.9">Plus que toute l’ardeur qui dans ton cœur s’allume,</l>
						<l n="170" num="17.10">Et que tous ces élans qui te semblent si doux.</l>
					</lg>
				</div></body></text></TEI>