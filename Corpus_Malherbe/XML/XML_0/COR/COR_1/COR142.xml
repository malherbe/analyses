<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Imitation De Jésus-Christ</title>
				<title type="medium">Édition électronique</title>
				<author key="COR">
					<name>
						<forename>Pierre</forename>
						<surname>CORNEILLE</surname>
					</name>
					<date from="1606" to="1684">1606-1684</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>13205 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2016</date>
				<idno type="local">COR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Imitation De Jésus-Christ</title>
						<author>Pierre Corneille</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/pierrecorneilleimmitadiondejesuschrist.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Imitation De Jésus-Christ</title>
						<author>Pierre Corneille</author>
						<editor>M. Ch. Marty-Laveaux</editor>
						<edition>nouvelle édition</edition>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k51199.r=pierre%20corneille?rk=85837;2</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie de L. Hachette et Cie</publisher>
							<date when="1862">1862</date>
						</imprint>
						<biblScope unit="tome">Tome 8</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1656">1656</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les chiffres arabes des livres et chapitres ont été remplacés par des chiffres romains</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">LIVRE QUATRIÈME</head><div type="poem" key="COR142">
					<head type="main">CHAPITRE XIII</head>
					<head type="sub">Que l’ame dévote doit s’efforcer de tout son cœur <lb></lb>à s’unir à Jésus-Christ dans le sacrement.</head>
					<lg n="1">
						<l n="1" num="1.1"><space unit="char" quantity="8"></space>Qui me la donnera, seigneur,</l>
						<l n="2" num="1.2"><space unit="char" quantity="8"></space>Cette joie où mon âme aspire,</l>
						<l n="3" num="1.3">De pouvoir seul à seul te montrer tout mon cœur,</l>
						<l n="4" num="1.4">Et de jouir de toi comme je le desire ?</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><space unit="char" quantity="8"></space>Que je rirai lors des mépris</l>
						<l n="6" num="2.2"><space unit="char" quantity="8"></space>Qu’auront pour moi les créatures !</l>
						<l n="7" num="2.3">Qu’il m’importera peu si leurs foibles esprits</l>
						<l n="8" num="2.4">Me comblent de faveurs, ou m’accablent d’injures !</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><space unit="char" quantity="8"></space>Je te dirai tout mon secret,</l>
						<l n="10" num="3.2"><space unit="char" quantity="8"></space>Tu me diras le tien de même,</l>
						<l n="11" num="3.3">Tel qu’un ami s’explique avec l’ami discret,</l>
						<l n="12" num="3.4">Tel qu’un amant fidèle entretient ce qu’il aime.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><space unit="char" quantity="8"></space>C’est là, seigneur, tout mon desir,</l>
						<l n="14" num="4.2"><space unit="char" quantity="8"></space>C’est tout ce dont je te conjure,</l>
						<l n="15" num="4.3">Qu’une sainte union à ton seul bon plaisir</l>
						<l n="16" num="4.4">Arrache de mon cœur toute la créature ;</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><space unit="char" quantity="8"></space>Qu’à force de communions,</l>
						<l n="18" num="5.2"><space unit="char" quantity="8"></space>D’offrandes et de sacrifices,</l>
						<l n="19" num="5.3">Élevant jusqu’au ciel toutes mes passions,</l>
						<l n="20" num="5.4">J’apprenne à ne goûter que ses pures délices.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><space unit="char" quantity="8"></space>Quand viendra-t-il, cet heureux jour,</l>
						<l n="22" num="6.2"><space unit="char" quantity="8"></space>Ce moment tout beau, tout céleste,</l>
						<l n="23" num="6.3">Qu’absorbé tout en toi par un parfait amour,</l>
						<l n="24" num="6.4">Je m’oublierai moi-même et fuirai tout le reste ?</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><space unit="char" quantity="8"></space>Viens en moi, tiens-toi tout en moi ;</l>
						<l n="26" num="7.2"><space unit="char" quantity="8"></space>Souffre à tes bontés adorables</l>
						<l n="27" num="7.3">De nous faire à tous deux cette immuable loi,</l>
						<l n="28" num="7.4">Qu’à jamais cet amour nous rende inséparables.</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1"><space unit="char" quantity="8"></space>N’es-tu pas ce cher bien-aimé,</l>
						<l n="30" num="8.2"><space unit="char" quantity="8"></space>Cet époux choisi d’entre mille,</l>
						<l n="31" num="8.3">À qui veut s’attacher mon cœur tout enflammé,</l>
						<l n="32" num="8.4">Tant qu’il respirera dedans ce tronc mobile ?</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1"><space unit="char" quantity="8"></space>N’es-tu pas seul toute ma paix,</l>
						<l n="34" num="9.2"><space unit="char" quantity="8"></space>Paix véritable et souveraine,</l>
						<l n="35" num="9.3">Hors de qui les travaux ne finissent jamais,</l>
						<l n="36" num="9.4">Hors de qui tout plaisir n’est que trouble et que peine ?</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1"><space unit="char" quantity="8"></space>N’es-tu pas cette déité</l>
						<l n="38" num="10.2"><space unit="char" quantity="8"></space>Ineffable, incompréhensible,</l>
						<l n="39" num="10.3">Qui fuyant tout commerce avec l’impiété,</l>
						<l n="40" num="10.4">Au cœur simple, au cœur humble es toujours accessible ?</l>
					</lg>
					<lg n="11">
						<l n="41" num="11.1"><space unit="char" quantity="8"></space>Seigneur, que ton esprit est doux !</l>
						<l n="42" num="11.2"><space unit="char" quantity="8"></space>Que pour tes enfants il est tendre !</l>
						<l n="43" num="11.3">Et que c’est les aimer que de les nourrir tous</l>
						<l n="44" num="11.4">De ce pain que du ciel tu fais pour eux descendre !</l>
					</lg>
					<lg n="12">
						<l n="45" num="12.1"><space unit="char" quantity="8"></space>Est-il une autre nation</l>
						<l n="46" num="12.2"><space unit="char" quantity="8"></space>Si grande, si favorisée,</l>
						<l n="47" num="12.3">Qui possède ses dieux avec telle union,</l>
						<l n="48" num="12.4">Qui trouve leur approche également aisée ?</l>
					</lg>
					<lg n="13">
						<l n="49" num="13.1"><space unit="char" quantity="8"></space>Chaque jour, pour nous soulager,</l>
						<l n="50" num="13.2"><space unit="char" quantity="8"></space>Pour nous porter au bien suprême,</l>
						<l n="51" num="13.3">Tu nous offres à tous ton vrai corps à manger,</l>
						<l n="52" num="13.4">Tu nous donnes à tous à jouir de toi-même.</l>
					</lg>
					<lg n="14">
						<l n="53" num="14.1"><space unit="char" quantity="8"></space>Quel climat est si précieux</l>
						<l n="54" num="14.2"><space unit="char" quantity="8"></space>Sur qui nous n’ayons l’avantage ?</l>
						<l n="55" num="14.3">Et quelle créature obtint jamais des cieux</l>
						<l n="56" num="14.4">Rien d’égal à ce don qui fait notre partage ?</l>
					</lg>
					<lg n="15">
						<l n="57" num="15.1"><space unit="char" quantity="8"></space>Un dieu venir jusqu’en nos cœurs !</l>
						<l n="58" num="15.2"><space unit="char" quantity="8"></space>De sa chair propre nous repaître !</l>
						<l n="59" num="15.3">Ô grâce inexplicable ! ô célestes faveurs !</l>
						<l n="60" num="15.4">Par quels dignes présents puis-je les reconnoître ?</l>
					</lg>
					<lg n="16">
						<l n="61" num="16.1"><space unit="char" quantity="8"></space>Que te rendrai-je, ô Dieu tout bon,</l>
						<l n="62" num="16.2"><space unit="char" quantity="8"></space>Après ce trait d’amour immense ?</l>
						<l n="63" num="16.3">Où pourrai-je trouver de quoi te faire un don</l>
						<l n="64" num="16.4">Qui puisse tenir lieu d’une reconnoissance ?</l>
					</lg>
					<lg n="17">
						<l n="65" num="17.1"><space unit="char" quantity="8"></space>Je l’ai, mon Dieu, j’ai ce de quoi</l>
						<l n="66" num="17.2"><space unit="char" quantity="8"></space>Te faire une agréable offrande ;</l>
						<l n="67" num="17.3">Je n’ai qu’à me donner de tout mon cœur à toi,</l>
						<l n="68" num="17.4">Et je te rendrai tout ce qu’il faut qu’on te rende.</l>
					</lg>
					<lg n="18">
						<l n="69" num="18.1"><space unit="char" quantity="8"></space>Oui, c’est là tout ce que tu veux</l>
						<l n="70" num="18.2"><space unit="char" quantity="8"></space>Pour cette faveur infinie.</l>
						<l n="71" num="18.3">Seigneur, que d’allégresse animera mes vœux,</l>
						<l n="72" num="18.4">Quand je verrai mon âme avec toi bien unie !</l>
					</lg>
					<lg n="19">
						<l n="73" num="19.1"><space unit="char" quantity="8"></space>D’un ton amoureux et divin</l>
						<l n="74" num="19.2"><space unit="char" quantity="8"></space>Tu me diras lors à toute heure :</l>
						<l n="75" num="19.3">« Si tu veux avec moi vivre jusqu’à la fin,</l>
						<l n="76" num="19.4">Avec toi jusqu’au bout je ferai ma demeure. »</l>
					</lg>
					<lg n="20">
						<l n="77" num="20.1"><space unit="char" quantity="8"></space>Et je te répondrai soudain :</l>
						<l n="78" num="20.2"><space unit="char" quantity="8"></space>« si tu m’en veux faire la grâce,</l>
						<l n="79" num="20.3">Seigneur, c’est de ma part mon unique dessein ;</l>
						<l n="80" num="20.4">Fais que d’un si beau nœud jamais je ne me lasse. »</l>
					</lg>
				</div></body></text></TEI>