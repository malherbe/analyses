<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Imitation De Jésus-Christ</title>
				<title type="medium">Édition électronique</title>
				<author key="COR">
					<name>
						<forename>Pierre</forename>
						<surname>CORNEILLE</surname>
					</name>
					<date from="1606" to="1684">1606-1684</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>13205 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2016</date>
				<idno type="local">COR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Imitation De Jésus-Christ</title>
						<author>Pierre Corneille</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/pierrecorneilleimmitadiondejesuschrist.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Imitation De Jésus-Christ</title>
						<author>Pierre Corneille</author>
						<editor>M. Ch. Marty-Laveaux</editor>
						<edition>nouvelle édition</edition>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k51199.r=pierre%20corneille?rk=85837;2</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie de L. Hachette et Cie</publisher>
							<date when="1862">1862</date>
						</imprint>
						<biblScope unit="tome">Tome 8</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1656">1656</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les chiffres arabes des livres et chapitres ont été remplacés par des chiffres romains</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">LIVRE PREMIER</head><div type="poem" key="COR53">
					<head type="main">CHAPITRE XXI</head>
					<head type="sub">De la componction du cœur.</head>
					<lg n="1">
						<l n="1" num="1.1">Si tu veux avancer au chemin de la grâce,</l>
						<l n="2" num="1.2">Dans la crainte de Dieu soutiens tes volontés ;</l>
						<l n="3" num="1.3">Ne sois jamais trop libre, et rends-toi tout de glace</l>
						<l n="4" num="1.4">Pour tout ce que les sens t’offrent de voluptés :</l>
						<l n="5" num="1.5">Dompte sous une exacte et forte discipline</l>
						<l n="6" num="1.6"><space unit="char" quantity="8"></space>Ces inséparables flatteurs</l>
						<l n="7" num="1.7">Que l’amour de toi-même à te séduire obstine,</l>
						<l n="8" num="1.8"><space unit="char" quantity="12"></space>Et dans eux n’examine</l>
						<l n="9" num="1.9">Que la grandeur des maux dont ils sont les auteurs.</l>
					</lg>
					<lg n="2">
						<l n="10" num="2.1">Ainsi fermant la porte à la joie indiscrète</l>
						<l n="11" num="2.2">Sous qui leur faux appas sème un poison caché,</l>
						<l n="12" num="2.3">Tu la tiendras ouverte à la douleur secrète</l>
						<l n="13" num="2.4">Qu’un profond repentir fait naître du péché :</l>
						<l n="14" num="2.5">Cette sainte douleur dans l’âme recueillie</l>
						<l n="15" num="2.6"><space unit="char" quantity="8"></space>Produit mille sortes de biens,</l>
						<l n="16" num="2.7">Que son relâchement vers l’aveugle folie</l>
						<l n="17" num="2.8"><space unit="char" quantity="12"></space>Des plaisirs de la vie</l>
						<l n="18" num="2.9">A bientôt dissipés en de vains entretiens.</l>
					</lg>
					<lg n="3">
						<l n="19" num="3.1">Chose étrange que l’homme accessible à la joie,</l>
						<l n="20" num="3.2">Au milieu des malheurs dont il est enfermé,</l>
						<l n="21" num="3.3">Quelque exilé qu’il soit, quelques périls qu’il voie,</l>
						<l n="22" num="3.4">Par de fausses douceurs aime à se voir charmé !</l>
						<l n="23" num="3.5">Ah ! S’il peut consentir qu’une telle allégresse</l>
						<l n="24" num="3.6"><space unit="char" quantity="8"></space>Tienne ses sens épanouis,</l>
						<l n="25" num="3.7">Il n’en voit pas la suite, et sa propre foiblesse,</l>
						<l n="26" num="3.8"><space unit="char" quantity="12"></space>Qu’il reçoit pour maîtresse,</l>
						<l n="27" num="3.9">Dérobe sa misère à ses yeux éblouis.</l>
					</lg>
					<lg n="4">
						<l n="28" num="4.1">Oui, sa légèreté que tout desir enflamme,</l>
						<l n="29" num="4.2">Et le peu de souci qu’il prend de ses défauts,</l>
						<l n="30" num="4.3">L’ayant rendu stupide aux intérêts de l’âme,</l>
						<l n="31" num="4.4">Ne lui permettent pas d’en ressentir les maux :</l>
						<l n="32" num="4.5">Ainsi, pour grands qu’ils soient, jamais il n’en soupire,</l>
						<l n="33" num="4.6"><space unit="char" quantity="8"></space>Faute de les considérer ;</l>
						<l n="34" num="4.7">Plus il en est blessé, plus lui-même il s’admire,</l>
						<l n="35" num="4.8"><space unit="char" quantity="12"></space>Et souvent ose rire</l>
						<l n="36" num="4.9">Lorsque de tous côtés il a de quoi pleurer.</l>
					</lg>
					<lg n="5">
						<l n="37" num="5.1">Homme, apprends qu’il n’est point ni de liberté vraie,</l>
						<l n="38" num="5.2">Ni de plaisir parfait qu’en la crainte de Dieu,</l>
						<l n="39" num="5.3">Et que la conscience et sans tache et sans plaie</l>
						<l n="40" num="5.4">À de pareils trésors seule peut donner lieu.</l>
						<l n="41" num="5.5">Toute autre liberté n’est qu’un long esclavage</l>
						<l n="42" num="5.6"><space unit="char" quantity="8"></space>Qui cache ou qui dore ses fers ;</l>
						<l n="43" num="5.7">Et tout autre plaisir ne laisse en ton courage</l>
						<l n="44" num="5.8"><space unit="char" quantity="12"></space>Qu’un prompt dégoût pour gage</l>
						<l n="45" num="5.9">Du tourment immortel qui l’attend aux enfers.</l>
					</lg>
					<lg n="6">
						<l n="46" num="6.1">Heureux qui peut bannir de toutes ses pensées</l>
						<l n="47" num="6.2">Les vains amusements de la distraction !</l>
						<l n="48" num="6.3">Heureux qui peut tenir ses forces ramassées</l>
						<l n="49" num="6.4">Dans le recueillement de la componction !</l>
						<l n="50" num="6.5">Mais plus heureux encor celui qui se dépouille</l>
						<l n="51" num="6.6"><space unit="char" quantity="8"></space>De tout indigne et lâche emploi,</l>
						<l n="52" num="6.7">Qui pour ne rien souffrir qui lui pèse ou le souille,</l>
						<l n="53" num="6.8"><space unit="char" quantity="12"></space>Fuit ce qui le chatouille,</l>
						<l n="54" num="6.9">Et pour mieux servir Dieu se rend maître de soi !</l>
					</lg>
					<lg n="7">
						<l n="55" num="7.1">Combats donc fortement contre l’inquiétude</l>
						<l n="56" num="7.2">Où te jettent du monde et l’amour et le bruit :</l>
						<l n="57" num="7.3">L’habitude se vainc par une autre habitude,</l>
						<l n="58" num="7.4">Et les hommes jamais ne cherchent qui les fuit.</l>
						<l n="59" num="7.5">Néglige leur commerce, et romps l’intelligence</l>
						<l n="60" num="7.6"><space unit="char" quantity="8"></space>Qui te lie encore avec eux,</l>
						<l n="61" num="7.7">Et bientôt à leur tour, te rendant par vengeance</l>
						<l n="62" num="7.8"><space unit="char" quantity="12"></space>La même négligence,</l>
						<l n="63" num="7.9">Ils t’abandonneront à tout ce que tu veux.</l>
					</lg>
					<lg n="8">
						<l n="64" num="8.1">N’attire point sur toi les affaires des autres,</l>
						<l n="65" num="8.2">Ne t’embarrasse point des intérêts des grands :</l>
						<l n="66" num="8.3">Notre propre besoin nous charge assez des nôtres ;</l>
						<l n="67" num="8.4">Tu te dois le premier les soins que tu leur rends.</l>
						<l n="68" num="8.5">Tiens sur toi l’œil ouvert, et toi-même t’éclaire</l>
						<l n="69" num="8.6"><space unit="char" quantity="8"></space>Avant qu’éclairer tes amis ;</l>
						<l n="70" num="8.7">Et quand tu peux donner un conseil salutaire</l>
						<l n="71" num="8.8"><space unit="char" quantity="12"></space>Qui les porte à bien faire,</l>
						<l n="72" num="8.9">Donne-t’en le plus ample et le plus prompt avis.</l>
					</lg>
					<lg n="9">
						<l n="73" num="9.1">Pour te voir éloigné de la faveur des hommes,</l>
						<l n="74" num="9.2">Ne crois point avoir lieu de justes déplaisirs :</l>
						<l n="75" num="9.3">Elle ne produit rien, en l’exil où nous sommes,</l>
						<l n="76" num="9.4">Qu’un espoir décevant et de vagues desirs.</l>
						<l n="77" num="9.5">Ce qui doit t’attrister, ce dont tu dois te plaindre,</l>
						<l n="78" num="9.6"><space unit="char" quantity="8"></space>C’est de ne te régler pas mieux,</l>
						<l n="79" num="9.7">C’est de sentir ton feu s’amortir et s’éteindre</l>
						<l n="80" num="9.8"><space unit="char" quantity="12"></space>Avant qu’il puisse atteindre</l>
						<l n="81" num="9.9">Où doit aller celui d’un vrai religieux.</l>
					</lg>
					<lg n="10">
						<l n="82" num="10.1">Souvent il est plus sûr, tant que l’homme respire,</l>
						<l n="83" num="10.2">Qu’il sente peu de joie en son cœur s’épancher,</l>
						<l n="84" num="10.3">Surtout de ces douceurs que le dehors inspire,</l>
						<l n="85" num="10.4">Et qui naissent en lui du sang et de la chair.</l>
						<l n="86" num="10.5">Que si Dieu rarement sur notre longue peine</l>
						<l n="87" num="10.6"><space unit="char" quantity="8"></space>Répand sa consolation,</l>
						<l n="88" num="10.7">La faute en est à nous, dont la prudence vaine</l>
						<l n="89" num="10.8"><space unit="char" quantity="12"></space>Cherche un peu trop l’humaine,</l>
						<l n="90" num="10.9">Et ne s’attache point à la componction.</l>
					</lg>
					<lg n="11">
						<l n="91" num="11.1">Reconnois-toi, mortel, indigne des tendresses</l>
						<l n="92" num="11.2">Que départ aux élus la divine bonté ;</l>
						<l n="93" num="11.3">Et des afflictions regarde les rudesses</l>
						<l n="94" num="11.4">Comme des traitements dus à ta lâcheté.</l>
						<l n="95" num="11.5">L’homme vraiment atteint de la douleur profonde</l>
						<l n="96" num="11.6"><space unit="char" quantity="8"></space>Qu’enfante un plein recueillement,</l>
						<l n="97" num="11.7">Ne trouve qu’amertume aux voluptés du monde,</l>
						<l n="98" num="11.8"><space unit="char" quantity="12"></space>Et voit qu’il ne les fonde</l>
						<l n="99" num="11.9">Que sur de longs périls que déguise un moment.</l>
					</lg>
					<lg n="12">
						<l n="100" num="12.1">Le moyen donc qu’il puisse y trouver quelques charmes,</l>
						<l n="101" num="12.2">Soit qu’il se considère, ou qu’il regarde autrui,</l>
						<l n="102" num="12.3">S’il n’y peut voir partout que des sujets de larmes,</l>
						<l n="103" num="12.4">N’y voyant que des croix pour tout autre et pour lui ?</l>
						<l n="104" num="12.5">Plus il le sait connoître, et plus la vie entière</l>
						<l n="105" num="12.6"><space unit="char" quantity="8"></space>Lui semble un amas de malheurs ;</l>
						<l n="106" num="12.7">Et plus du haut du ciel il reçoit de lumière,</l>
						<l n="107" num="12.8"><space unit="char" quantity="12"></space>Plus il voit de matière</l>
						<l n="108" num="12.9">Dessus toute la terre à de justes douleurs.</l>
					</lg>
					<lg n="13">
						<l n="109" num="13.1">Sacrés ressentiments, réflexions perçantes,</l>
						<l n="110" num="13.2">Qui dans un cœur navré versez d’heureux regrets,</l>
						<l n="111" num="13.3">Que vous trouvez souvent d’occasions pressantes</l>
						<l n="112" num="13.4">Parmi tant de péchés et publics et secrets !</l>
						<l n="113" num="13.5">Mais, hélas ! Ces tyrans de l’âme criminelle</l>
						<l n="114" num="13.6"><space unit="char" quantity="8"></space>L’enchaînent si bien en ces lieux,</l>
						<l n="115" num="13.7">Qu’il est bien malaisé que vous arrachiez d’elle</l>
						<l n="116" num="13.8"><space unit="char" quantity="12"></space>Quelque soupir fidèle</l>
						<l n="117" num="13.9">Qui la puisse élever un moment vers les cieux.</l>
					</lg>
					<lg n="14">
						<l n="118" num="14.1">Pense plus à la mort, que tu vois assurée,</l>
						<l n="119" num="14.2">Qu’à la vaine longueur de tes jours incertains,</l>
						<l n="120" num="14.3">Et tu ressentiras dans ton âme épurée</l>
						<l n="121" num="14.4">Une ferveur plus forte et des desirs plus saints.</l>
						<l n="122" num="14.5">Si ton cœur chaque jour mettoit dans la balance</l>
						<l n="123" num="14.6"><space unit="char" quantity="8"></space>Ou le purgatoire ou l’enfer,</l>
						<l n="124" num="14.7">Il n’est point de travail, il n’est point de souffrance</l>
						<l n="125" num="14.8"><space unit="char" quantity="12"></space>Où soudain ta constance</l>
						<l n="126" num="14.9">Ne portât sans effroi l’ardeur d’en triompher.</l>
					</lg>
					<lg n="15">
						<l n="127" num="15.1">Mais nous n’en concevons qu’une légère image,</l>
						<l n="128" num="15.2">Dont les traits impuissants ne vont point jusqu’au cœur ;</l>
						<l n="129" num="15.3">Nous aimons ce qui flatte, et consumons notre âge</l>
						<l n="130" num="15.4">Dans l’assoupissement d’une froide langueur :</l>
						<l n="131" num="15.5">Aussi le corps se plaint, le corps gémit sans cesse,</l>
						<l n="132" num="15.6"><space unit="char" quantity="8"></space>Accablé sous les moindres croix,</l>
						<l n="133" num="15.7">Parce que de l’esprit la honteuse mollesse</l>
						<l n="134" num="15.8"><space unit="char" quantity="12"></space>N’agit qu’avec foiblesse,</l>
						<l n="135" num="15.9">Et refuse son aide à soutenir leur poids.</l>
					</lg>
					<lg n="16">
						<l n="136" num="16.1">Demande donc à Dieu pour faveur singulière</l>
						<l n="137" num="16.2">L’esprit fortifiant de la componction ;</l>
						<l n="138" num="16.3">Avec le roi prophète élève ta prière,</l>
						<l n="139" num="16.4">Et dis à son exemple avec submission :</l>
						<l n="140" num="16.5">« Nourrissez-moi de pleurs, seigneur, pour témoignage</l>
						<l n="141" num="16.6"><space unit="char" quantity="8"></space>Que vous me voulez consoler.</l>
						<l n="142" num="16.7">Détrempez-en mon pain, mêlez-en mon breuvage,</l>
						<l n="143" num="16.8"><space unit="char" quantity="12"></space>Et de tout mon visage</l>
						<l n="144" num="16.9">Jour et nuit à grands flots faites-les distiller. »</l>
					</lg>
				</div></body></text></TEI>