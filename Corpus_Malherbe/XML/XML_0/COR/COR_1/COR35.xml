<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Imitation De Jésus-Christ</title>
				<title type="medium">Édition électronique</title>
				<author key="COR">
					<name>
						<forename>Pierre</forename>
						<surname>CORNEILLE</surname>
					</name>
					<date from="1606" to="1684">1606-1684</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>13205 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2016</date>
				<idno type="local">COR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Imitation De Jésus-Christ</title>
						<author>Pierre Corneille</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/pierrecorneilleimmitadiondejesuschrist.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Imitation De Jésus-Christ</title>
						<author>Pierre Corneille</author>
						<editor>M. Ch. Marty-Laveaux</editor>
						<edition>nouvelle édition</edition>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k51199.r=pierre%20corneille?rk=85837;2</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie de L. Hachette et Cie</publisher>
							<date when="1862">1862</date>
						</imprint>
						<biblScope unit="tome">Tome 8</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1656">1656</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les chiffres arabes des livres et chapitres ont été remplacés par des chiffres romains</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">LIVRE PREMIER</head><div type="poem" key="COR35">
					<head type="main">CHAPITRE III</head>
					<head type="sub">De la doctrine de la vérité.</head>
					<lg n="1">
						<l n="1" num="1.1">Qu’heureux est le mortel que la vérité même</l>
						<l n="2" num="1.2">Conduit de sa main propre au chemin qui lui plaît !</l>
						<l n="3" num="1.3">Qu’heureux est qui la voit dans sa beauté suprême,</l>
						<l n="4" num="1.4"><space unit="char" quantity="12"></space>Sans voile et sans emblème,</l>
						<l n="5" num="1.5"><space unit="char" quantity="12"></space>Et telle enfin qu’elle est !</l>
					</lg>
					<lg n="2">
						<l n="6" num="2.1">Nos sens sont des trompeurs, dont les fausses images</l>
						<l n="7" num="2.2">À notre entendement n’offrent rien d’assuré,</l>
						<l n="8" num="2.3">Et ne lui font rien voir qu’à travers cent nuages</l>
						<l n="9" num="2.4"><space unit="char" quantity="12"></space>Qui jettent mille ombrages</l>
						<l n="10" num="2.5"><space unit="char" quantity="12"></space>Dans l’œil mal éclairé.</l>
					</lg>
					<lg n="3">
						<l n="11" num="3.1">De quoi sert une longue et subtile dispute</l>
						<l n="12" num="3.2">Sur des obscurités où l’esprit est déçu ?</l>
						<l n="13" num="3.3">De quoi sert qu’à l’envi chacun s’en persécute,</l>
						<l n="14" num="3.4"><space unit="char" quantity="12"></space>Si Dieu jamais n’impute</l>
						<l n="15" num="3.5"><space unit="char" quantity="12"></space>De n’en avoir rien su ?</l>
					</lg>
					<lg n="4">
						<l n="16" num="4.1">Grande perte de temps et plus grande foiblesse</l>
						<l n="17" num="4.2">De s’aveugler soi-même et quitter le vrai bien,</l>
						<l n="18" num="4.3">Pour consumer sa vie à pointiller sans cesse</l>
						<l n="19" num="4.4"><space unit="char" quantity="12"></space>Sur le genre et l’espèce,</l>
						<l n="20" num="4.5"><space unit="char" quantity="12"></space>Qui ne servent à rien.</l>
					</lg>
					<lg n="5">
						<l n="21" num="5.1">Touche, verbe éternel, ces âmes curieuses :</l>
						<l n="22" num="5.2">Celui que ta parole une fois a frappé,</l>
						<l n="23" num="5.3">De tant d’opinions vaines, ambitieuses,</l>
						<l n="24" num="5.4"><space unit="char" quantity="12"></space>Et souvent dangereuses,</l>
						<l n="25" num="5.5"><space unit="char" quantity="12"></space>Est bien développé.</l>
					</lg>
					<lg n="6">
						<l n="26" num="6.1">Ce verbe donne seul l’être à toutes les causes ;</l>
						<l n="27" num="6.2">Il nous parle de tout, tout nous parle de lui ;</l>
						<l n="28" num="6.3">Il tient de tout en soi les natures encloses ;</l>
						<l n="29" num="6.4"><space unit="char" quantity="12"></space>Il est de toutes choses</l>
						<l n="30" num="6.5"><space unit="char" quantity="12"></space>Le principe et l’appui.</l>
					</lg>
					<lg n="7">
						<l n="31" num="7.1">Aucun sans son secours ne sauroit se défendre</l>
						<l n="32" num="7.2">D’un million d’erreurs qui courent l’assiéger,</l>
						<l n="33" num="7.3">Et depuis qu’un esprit refuse de l’entendre,</l>
						<l n="34" num="7.4"><space unit="char" quantity="12"></space>Quoi qu’il pense comprendre,</l>
						<l n="35" num="7.5"><space unit="char" quantity="12"></space>Il n’en peut bien juger.</l>
					</lg>
					<lg n="8">
						<l n="36" num="8.1">Mais qui rapporte tout à ce verbe immuable,</l>
						<l n="37" num="8.2">Qui voit tout en lui seul, en lui seul aime tout,</l>
						<l n="38" num="8.3">À la plus rude attaque il est inébranlable,</l>
						<l n="39" num="8.4"><space unit="char" quantity="12"></space>Et sa paix ferme et stable</l>
						<l n="40" num="8.5"><space unit="char" quantity="12"></space>En vient soudain à bout !</l>
					</lg>
					<lg n="9">
						<l n="41" num="9.1">O dieu de vérité, pour qui seul je soupire,</l>
						<l n="42" num="9.2">Unis-moi donc à toi par de forts et doux nœuds !</l>
						<l n="43" num="9.3">Je me lasse d’ouïr, je me lasse de lire,</l>
						<l n="44" num="9.4"><space unit="char" quantity="12"></space>Mais non pas de te dire :</l>
						<l n="45" num="9.5"><space unit="char" quantity="12"></space>« c’est toi seul que je veux. »</l>
					</lg>
					<lg n="10">
						<l n="46" num="10.1">Parle seul à mon âme, et qu’aucune prudence,</l>
						<l n="47" num="10.2">Qu’aucun autre docteur ne m’explique tes lois ;</l>
						<l n="48" num="10.3">Que toute créature à ta sainte présence</l>
						<l n="49" num="10.4"><space unit="char" quantity="12"></space>S’impose le silence,</l>
						<l n="50" num="10.5"><space unit="char" quantity="12"></space>Et laisse agir ta voix.</l>
					</lg>
					<lg n="11">
						<l n="51" num="11.1">Plus l’esprit se fait simple et plus il se ramène</l>
						<l n="52" num="11.2">Dans un intérieur dégagé des objets,</l>
						<l n="53" num="11.3">Plus lors sa connoissance est diffuse et certaine,</l>
						<l n="54" num="11.4"><space unit="char" quantity="12"></space>Et s’élève sans peine</l>
						<l n="55" num="11.5"><space unit="char" quantity="12"></space>Jusqu’aux plus hauts sujets.</l>
					</lg>
					<lg n="12">
						<l n="56" num="12.1">Oui, Dieu prodigue alors ses grâces plus entières,</l>
						<l n="57" num="12.2">Et portant notre idée au-dessus de nos sens,</l>
						<l n="58" num="12.3">Il nous donne d’en haut d’autant plus de lumières,</l>
						<l n="59" num="12.4"><space unit="char" quantity="12"></space>Qui percent les matières</l>
						<l n="60" num="12.5"><space unit="char" quantity="12"></space>Par des traits plus puissants.</l>
					</lg>
					<lg n="13">
						<l n="61" num="13.1">Cet esprit simple, uni, stable, pur, pacifique,</l>
						<l n="62" num="13.2">En mille soins divers n’est jamais dissipé,</l>
						<l n="63" num="13.3">Et l’honneur de son Dieu, dans tout ce qu’il pratique,</l>
						<l n="64" num="13.4"><space unit="char" quantity="12"></space>Est le projet unique</l>
						<l n="65" num="13.5"><space unit="char" quantity="12"></space>Qui le tient occupé.</l>
					</lg>
					<lg n="14">
						<l n="66" num="14.1">Il est toujours en soi détaché de soi-même ;</l>
						<l n="67" num="14.2">Il ne sait point agir quand il se faut chercher,</l>
						<l n="68" num="14.3">Et fût-il dans l’éclat de la grandeur suprême,</l>
						<l n="69" num="14.4"><space unit="char" quantity="12"></space>Son propre diadème</l>
						<l n="70" num="14.5"><space unit="char" quantity="12"></space>Ne l’y peut attacher.</l>
					</lg>
					<lg n="15">
						<l n="71" num="15.1">Il ne croit trouble égal à celui que se cause</l>
						<l n="72" num="15.2">Un cœur qui s’abandonne à ses propres transports,</l>
						<l n="73" num="15.3">Et maître de soi-même, en soi-même il dispose</l>
						<l n="74" num="15.4"><space unit="char" quantity="12"></space>Tout ce qu’il se propose</l>
						<l n="75" num="15.5"><space unit="char" quantity="12"></space>De produire au dehors.</l>
					</lg>
					<lg n="16">
						<l n="76" num="16.1">Bien loin d’être emporté par le courant rapide</l>
						<l n="77" num="16.2">Des flots impétueux de ses bouillants desirs,</l>
						<l n="78" num="16.3">Il les dompte, il les rompt, il les tourne, il les guide,</l>
						<l n="79" num="16.4"><space unit="char" quantity="12"></space>Et donne ainsi pour bride</l>
						<l n="80" num="16.5"><space unit="char" quantity="12"></space>La raison aux plaisirs.</l>
					</lg>
					<lg n="17">
						<l n="81" num="17.1">Mais pour se vaincre ainsi qu’il faut d’art et de force !</l>
						<l n="82" num="17.2">Qu’il faut pour ce combat préparer de vigueur !</l>
						<l n="83" num="17.3">Et qu’il est malaisé de faire un plein divorce</l>
						<l n="84" num="17.4"><space unit="char" quantity="12"></space>Avec la douce amorce</l>
						<l n="85" num="17.5"><space unit="char" quantity="12"></space>Que chacun porte au cœur !</l>
					</lg>
					<lg n="18">
						<l n="86" num="18.1">Ce devroit être aussi notre unique pensée</l>
						<l n="87" num="18.2">De nous fortifier chaque jour contre nous,</l>
						<l n="88" num="18.3">Pour en déraciner cette amour empressée</l>
						<l n="89" num="18.4"><space unit="char" quantity="12"></space>Où l’âme intéressée</l>
						<l n="90" num="18.5"><space unit="char" quantity="12"></space>Trouve un poison si doux.</l>
					</lg>
					<lg n="19">
						<l n="91" num="19.1">Les soins que cette amour nous donne en cette vie</l>
						<l n="92" num="19.2">Ne peuvent aussi bien nous élever si haut,</l>
						<l n="93" num="19.3">Que la perfection la plus digne d’envie</l>
						<l n="94" num="19.4"><space unit="char" quantity="12"></space>N’y soit toujours suivie</l>
						<l n="95" num="19.5"><space unit="char" quantity="12"></space>Des hontes d’un défaut.</l>
					</lg>
					<lg n="20">
						<l n="96" num="20.1">Nos spéculations ne sont jamais si pures</l>
						<l n="97" num="20.2">Qu’on ne sente un peu d’ombre y régner à son tour ;</l>
						<l n="98" num="20.3">Nos plus vives clartés ont des couleurs obscures,</l>
						<l n="99" num="20.4"><space unit="char" quantity="12"></space>Et cent fausses peintures</l>
						<l n="100" num="20.5"><space unit="char" quantity="12"></space>Naissent d’un seul faux jour.</l>
					</lg>
					<lg n="21">
						<l n="101" num="21.1">Mais n’avoir que mépris pour soi-même et que haine</l>
						<l n="102" num="21.2">Ouvre et fait vers le ciel un chemin plus certain,</l>
						<l n="103" num="21.3">Que le plus haut effort de la science humaine,</l>
						<l n="104" num="21.4"><space unit="char" quantity="12"></space>Qui rend l’âme plus vaine</l>
						<l n="105" num="21.5"><space unit="char" quantity="12"></space>Et l’égare soudain.</l>
					</lg>
					<lg n="22">
						<l n="106" num="22.1">Ce n’est pas que de Dieu ne vienne la science :</l>
						<l n="107" num="22.2">D’elle-même elle est bonne, et n’a rien à blâmer ;</l>
						<l n="108" num="22.3">Mais il faut préférer la bonne conscience</l>
						<l n="109" num="22.4"><space unit="char" quantity="12"></space>À cette impatience</l>
						<l n="110" num="22.5"><space unit="char" quantity="12"></space>De se faire estimer.</l>
					</lg>
					<lg n="23">
						<l n="111" num="23.1">Cependant, sans souci de régler sa conduite,</l>
						<l n="112" num="23.2">On veut être savant, on en cherche le bruit ;</l>
						<l n="113" num="23.3">Et cette ambition par qui l’âme est séduite</l>
						<l n="114" num="23.4"><space unit="char" quantity="12"></space>Souvent traîne à sa suite</l>
						<l n="115" num="23.5"><space unit="char" quantity="12"></space>Mille erreurs pour tout fruit.</l>
					</lg>
					<lg n="24">
						<l n="116" num="24.1">Ah ! Si l’on se donnoit la même diligence,</l>
						<l n="117" num="24.2">Pour extirper le vice et planter la vertu,</l>
						<l n="118" num="24.3">Que pour subtiliser sa propre intelligence</l>
						<l n="119" num="24.4"><space unit="char" quantity="12"></space>Et tirer la science</l>
						<l n="120" num="24.5"><space unit="char" quantity="12"></space>Hors du chemin battu !</l>
					</lg>
					<lg n="25">
						<l n="121" num="25.1">De tant de questions les dangereux mystères</l>
						<l n="122" num="25.2">Produiroient moins de trouble et de renversement,</l>
						<l n="123" num="25.3">Et ne couleroient pas dans les règles austères</l>
						<l n="124" num="25.4"><space unit="char" quantity="12"></space>Des plus saints monastères</l>
						<l n="125" num="25.5"><space unit="char" quantity="12"></space>Tant de relâchement.</l>
					</lg>
					<lg n="26">
						<l n="126" num="26.1">Un jour, un jour viendra qu’il faudra rendre conte,</l>
						<l n="127" num="26.2">Non de ce qu’on a lu, mais de ce qu’on a fait ;</l>
						<l n="128" num="26.3">Et l’orgueilleux savoir, à quelque point qu’il monte,</l>
						<l n="129" num="26.4"><space unit="char" quantity="12"></space>N’aura lors que la honte</l>
						<l n="130" num="26.5"><space unit="char" quantity="12"></space>De son mauvais effet.</l>
					</lg>
					<lg n="27">
						<l n="131" num="27.1">Où sont tous ces docteurs qu’une foule si grande</l>
						<l n="132" num="27.2">Rendoit à tes yeux même autrefois si fameux ?</l>
						<l n="133" num="27.3">Un autre tient leur place, un autre a leur prébende,</l>
						<l n="134" num="27.4"><space unit="char" quantity="12"></space>Sans qu’aucun te demande</l>
						<l n="135" num="27.5"><space unit="char" quantity="12"></space>Un souvenir pour eux.</l>
					</lg>
					<lg n="28">
						<l n="136" num="28.1">Tant qu’a duré leur vie, ils sembloient quelque chose ;</l>
						<l n="137" num="28.2">Il semble après leur mort qu’ils n’ont jamais été :</l>
						<l n="138" num="28.3">Leur mémoire avec eux sous leur tombe est enclose ;</l>
						<l n="139" num="28.4"><space unit="char" quantity="12"></space>Avec eux y repose</l>
						<l n="140" num="28.5"><space unit="char" quantity="12"></space>Toute leur vanité.</l>
					</lg>
					<lg n="29">
						<l n="141" num="29.1">Ainsi passe la gloire où le savant aspire,</l>
						<l n="142" num="29.2">S’il n’a mis son étude à se justifier :</l>
						<l n="143" num="29.3">C’est là le seul emploi qui laisse lieu d’en dire</l>
						<l n="144" num="29.4"><space unit="char" quantity="12"></space>Qu’il avoit su bien lire</l>
						<l n="145" num="29.5"><space unit="char" quantity="12"></space>Et bien étudier.</l>
					</lg>
					<lg n="30">
						<l n="146" num="30.1">Mais au lieu d’aimer Dieu, d’agir pour son service,</l>
						<l n="147" num="30.2">L’éclat d’un vain savoir à toute heure éblouit,</l>
						<l n="148" num="30.3">Et fait suivre à toute heure un brillant artifice</l>
						<l n="149" num="30.4"><space unit="char" quantity="12"></space>Qui mène au précipice,</l>
						<l n="150" num="30.5"><space unit="char" quantity="12"></space>Et là s’évanouit.</l>
					</lg>
					<lg n="31">
						<l n="151" num="31.1">Du seul desir d’honneur notre âme est enflammée :</l>
						<l n="152" num="31.2">Nous voulons être grands plutôt qu’humbles de cœur ;</l>
						<l n="153" num="31.3">Et tout ce bruit flatteur de notre renommée,</l>
						<l n="154" num="31.4"><space unit="char" quantity="12"></space>Comme il n’est que fumée,</l>
						<l n="155" num="31.5"><space unit="char" quantity="12"></space>Se dissipe en vapeur.</l>
					</lg>
					<lg n="32">
						<l n="156" num="32.1">La grandeur véritable est d’une autre nature :</l>
						<l n="157" num="32.2">C’est en vain qu’on la cherche avec la vanité ;</l>
						<l n="158" num="32.3">Celle d’un vrai chrétien, d’une âme toute pure,</l>
						<l n="159" num="32.4"><space unit="char" quantity="12"></space>Jamais ne se mesure</l>
						<l n="160" num="32.5"><space unit="char" quantity="12"></space>Que sur sa charité.</l>
					</lg>
					<lg n="33">
						<l n="161" num="33.1">Vraiment grand est celui qui dans soi se ravale,</l>
						<l n="162" num="33.2">Qui rentre en son néant pour s’y connoître bien,</l>
						<l n="163" num="33.3">Qui de tous les honneurs que l’univers étale</l>
						<l n="164" num="33.4"><space unit="char" quantity="12"></space>Craint la pompe fatale,</l>
						<l n="165" num="33.5"><space unit="char" quantity="12"></space>Et ne l’estime à rien.</l>
					</lg>
					<lg n="34">
						<l n="166" num="34.1">Vraiment sage est celui dont la vertu resserre</l>
						<l n="167" num="34.2">Autour du vrai bonheur l’essor de son esprit,</l>
						<l n="168" num="34.3">Qui prend pour du fumier les choses de la terre,</l>
						<l n="169" num="34.4"><space unit="char" quantity="12"></space>Et qui se fait la guerre</l>
						<l n="170" num="34.5"><space unit="char" quantity="12"></space>Pour gagner Jésus-Christ.</l>
					</lg>
					<lg n="35">
						<l n="171" num="35.1">Et vraiment docte enfin est celui qui préfère</l>
						<l n="172" num="35.2">À son propre vouloir le vouloir de son Dieu,</l>
						<l n="173" num="35.3">Qui cherche en tout, partout, à l’apprendre, à le faire,</l>
						<l n="174" num="35.4"><space unit="char" quantity="12"></space>Et jamais ne diffère</l>
						<l n="175" num="35.5"><space unit="char" quantity="12"></space>Ni pour temps ni pour lieu.</l>
					</lg>
				</div></body></text></TEI>