<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Imitation De Jésus-Christ</title>
				<title type="medium">Édition électronique</title>
				<author key="COR">
					<name>
						<forename>Pierre</forename>
						<surname>CORNEILLE</surname>
					</name>
					<date from="1606" to="1684">1606-1684</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>13205 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2016</date>
				<idno type="local">COR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Imitation De Jésus-Christ</title>
						<author>Pierre Corneille</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/pierrecorneilleimmitadiondejesuschrist.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Imitation De Jésus-Christ</title>
						<author>Pierre Corneille</author>
						<editor>M. Ch. Marty-Laveaux</editor>
						<edition>nouvelle édition</edition>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k51199.r=pierre%20corneille?rk=85837;2</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie de L. Hachette et Cie</publisher>
							<date when="1862">1862</date>
						</imprint>
						<biblScope unit="tome">Tome 8</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1656">1656</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les chiffres arabes des livres et chapitres ont été remplacés par des chiffres romains</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">LIVRE DEUXIÈME</head><div type="poem" key="COR68">
					<head type="main">CHAPITRE XI</head>
					<head type="sub">Du petit nombre de ceux qui aiment la croix <lb></lb>e Jésus-Christ.</head>
					<lg n="1">
						<l n="1" num="1.1">Que d’hommes amoureux de la gloire céleste</l>
						<l n="2" num="1.2">Envisagent la croix comme un fardeau funeste,</l>
						<l n="3" num="1.3">Et cherchent à goûter les consolations,</l>
						<l n="4" num="1.4">Sans vouloir faire essai des tribulations !</l>
						<l n="5" num="1.5">Jésus-Christ voit partout cette humeur variable :</l>
						<l n="6" num="1.6">Il n’a que trop d’amis pour se seoir à sa table,</l>
						<l n="7" num="1.7">Aucun dans le banquet ne veut l’abandonner ;</l>
						<l n="8" num="1.8">Mais au fond du désert il est seul à jeûner.</l>
						<l n="9" num="1.9">Tous lui demandent part à sa pleine allégresse,</l>
						<l n="10" num="1.10">Mais aucun n’en veut prendre à sa pleine tristesse ;</l>
						<l n="11" num="1.11">Et ceux que l’on a vus les plus prompts à s’offrir</l>
						<l n="12" num="1.12">Le quittent les premiers quand il lui faut souffrir.</l>
					</lg>
					<lg n="2">
						<l n="13" num="2.1">Jusqu’à la fraction de ce pain qu’il nous donne,</l>
						<l n="14" num="2.2">Assez de monde ici le suit et l’environne ;</l>
						<l n="15" num="2.3">Mais peu de son amour s’y laissent enflammer</l>
						<l n="16" num="2.4">Jusqu’à boire avec lui dans le calice amer.</l>
						<l n="17" num="2.5">Les miracles brillants dont il sème sa vie</l>
						<l n="18" num="2.6">Par leur éclat à peine échauffent notre envie,</l>
						<l n="19" num="2.7">Que sa honteuse mort refroidit nos esprits</l>
						<l n="20" num="2.8">Jusqu’à ne vouloir plus de ce don à ce prix.</l>
					</lg>
					<lg n="3">
						<l n="21" num="3.1">Beaucoup avec chaleur l’aiment et le bénissent</l>
						<l n="22" num="3.2">Dont, au premier revers, les louanges tarissent.</l>
						<l n="23" num="3.3">Tant qu’ils n’ont à gémir d’aucune adversité,</l>
						<l n="24" num="3.4">Qu’il n’épanche sur eux que sa bénignité,</l>
						<l n="25" num="3.5">Cette faveur sensible aisément sert d’amorce</l>
						<l n="26" num="3.6">À soutenir leur zèle et conserver leur force ;</l>
						<l n="27" num="3.7">Mais lorsque sa bonté se cache tant soit peu,</l>
						<l n="28" num="3.8">Une soudaine glace amortit tout ce feu,</l>
						<l n="29" num="3.9">Et les restes fumants de leur ferveur éteinte</l>
						<l n="30" num="3.10">Ne font partir du cœur que murmure et que plainte,</l>
						<l n="31" num="3.11">Tandis qu’au fond de l’âme un lâche étonnement</l>
						<l n="32" num="3.12">Va de la fermeté jusqu’à l’abattement.</l>
					</lg>
					<lg n="4">
						<l n="33" num="4.1">En usez-vous ainsi, vous dont l’amour extrême</l>
						<l n="34" num="4.2">N’embrasse Jésus-Christ qu’à cause de lui-même,</l>
						<l n="35" num="4.3">Et qui sans regarder votre propre intérêt,</l>
						<l n="36" num="4.4">N’avez de passion que pour ce qui lui plaît ?</l>
						<l n="37" num="4.5">Vous voyez d’un même œil tout ce qu’il vous envoie :</l>
						<l n="38" num="4.6">Vous l’aimez dans l’angoisse ainsi que dans la joie ;</l>
						<l n="39" num="4.7">Vous le savez bénir dans la prospérité,</l>
						<l n="40" num="4.8">Vous le savez louer dans la calamité ;</l>
						<l n="41" num="4.9">Une égale constance attachée à ses traces</l>
						<l n="42" num="4.10">Dans l’un et l’autre sort trouve à lui rendre grâces ;</l>
						<l n="43" num="4.11">Et quand jamais pour vous il n’auroit que rigueurs,</l>
						<l n="44" num="4.12">Mêmes remercîments partiroient de vos cœurs.</l>
					</lg>
					<lg n="5">
						<l n="45" num="5.1">Pur amour de Jésus, que ta force est étrange,</l>
						<l n="46" num="5.2">Quand l’amour-propre en toi ne fait aucun mélange,</l>
						<l n="47" num="5.3">Et que de l’intérêt pleinement dépouillé</l>
						<l n="48" num="5.4">D’aucun regard vers nous tu ne te vois souillé !</l>
						<l n="49" num="5.5">N’ont-ils pas un amour servile et mercenaire,</l>
						<l n="50" num="5.6">Ces cœurs qui n’aiment Dieu que pour se satisfaire,</l>
						<l n="51" num="5.7">Et ne le font l’objet de leurs affections</l>
						<l n="52" num="5.8">Que pour en recevoir des consolations ?</l>
					</lg>
					<lg n="6">
						<l n="53" num="6.1">Aimer Dieu de la sorte et pour nos avantages,</l>
						<l n="54" num="6.2">C’est mettre indignement ses bontés à nos gages,</l>
						<l n="55" num="6.3">Croire d’un peu de vœux payer tout son appui,</l>
						<l n="56" num="6.4">Et nous-mêmes enfin nous aimer plus que lui ;</l>
						<l n="57" num="6.5">Mais où trouvera-t-on une âme si purgée,</l>
						<l n="58" num="6.6">D’espoir de tout salaire à ce point dégagée,</l>
						<l n="59" num="6.7">Qu’elle aime à servir Dieu sans se considérer,</l>
						<l n="60" num="6.8">Et ne cherche en l’aimant que l’heur de l’adorer ?</l>
					</lg>
					<lg n="7">
						<l n="61" num="7.1">Certes il s’en voit peu de qui l’amour soit pure</l>
						<l n="62" num="7.2">Jusqu’à se dépouiller de toute créature ;</l>
						<l n="63" num="7.3">Et s’il est sur la terre un vrai pauvre d’esprit,</l>
						<l n="64" num="7.4">Qui détaché de tout, soit tout à Jésus-Christ,</l>
						<l n="65" num="7.5">C’est un trésor si grand, que ces mines fécondes</l>
						<l n="66" num="7.6">Que la nature écarte au bout des nouveaux mondes,</l>
						<l n="67" num="7.7">Ces mers où se durcit la perle et le coral,</l>
						<l n="68" num="7.8">N’en ont jamais conçu qui fût d’un prix égal.</l>
					</lg>
					<lg n="8">
						<l n="69" num="8.1">Mais aussi ce n’est pas une conquête aisée</l>
						<l n="70" num="8.2">Qu’à ses premiers desirs l’homme trouve exposée :</l>
						<l n="71" num="8.3">Quand pour y parvenir il donne tout son bien,</l>
						<l n="72" num="8.4">Avec ce grand effort il ne fait encor rien ;</l>
						<l n="73" num="8.5">Quelque âpre pénitence ici-bas qu’il s’impose,</l>
						<l n="74" num="8.6">Ses plus longues rigueurs sont encor peu de chose ;</l>
						<l n="75" num="8.7">Que sur chaque science il applique son soin,</l>
						<l n="76" num="8.8">Qu’il la possède entière, il est encor bien loin ;</l>
						<l n="77" num="8.9">Qu’il ait mille vertus dont l’heureux assemblage</l>
						<l n="78" num="8.10">De tous leurs ornements pare son grand courage ;</l>
						<l n="79" num="8.11">Que sa dévotion, que ses hautes ferveurs</l>
						<l n="80" num="8.12">Attirent chaque jour de nouvelles faveurs :</l>
						<l n="81" num="8.13">Sache qu’il lui demeure encor beaucoup à faire,</l>
						<l n="82" num="8.14">S’il manque à ce point seul, qui seul est nécessaire.</l>
						<l n="83" num="8.15">Tu sais quel est ce point, je l’ai trop répété :</l>
						<l n="84" num="8.16">C’est qu’il se quitte encor, quand il a tout quitté,</l>
						<l n="85" num="8.17">Que de tout l’amour-propre il fasse un sacrifice,</l>
						<l n="86" num="8.18">Que de lui-même enfin lui-même il se bannisse,</l>
						<l n="87" num="8.19">Et qu’élevé par là dans un état parfait,</l>
						<l n="88" num="8.20">Il croie, ayant fait tout, n’avoir encor rien fait.</l>
					</lg>
					<lg n="9">
						<l n="89" num="9.1">Qu’il estime fort peu, suivant cette maxime,</l>
						<l n="90" num="9.2">Tout ce qui peut en lui mériter quelque estime ;</l>
						<l n="91" num="9.3">Que lui-même il se die, et du fond de son cœur,</l>
						<l n="92" num="9.4">Serviteur inutile aux emplois du seigneur.</l>
						<l n="93" num="9.5">La vérité l’ordonne : « après avoir, dit-elle,</l>
						<l n="94" num="9.6">Rempli tous les devoirs où ma voix vous appelle,</l>
						<l n="95" num="9.7">Après avoir fait tout ce que je vous prescris,</l>
						<l n="96" num="9.8">Gardez encor pour vous un sincère mépris,</l>
						<l n="97" num="9.9">Et nommez-vous encor disciples indociles,</l>
						<l n="98" num="9.10">Serviteurs fainéants, esclaves inutiles. »</l>
						<l n="99" num="9.11">Ainsi vraiment tout nu, vraiment pauvre d’esprit,</l>
						<l n="100" num="9.12">Tout détaché de tout, et tout à Jésus-Christ,</l>
						<l n="101" num="9.13">Avec le roi prophète il aura lieu de dire :</l>
						<l n="102" num="9.14">« Je n’ai plus rien en moi que ce que Dieu m’inspire ;</l>
						<l n="103" num="9.15">J’y suis seul, j’y suis pauvre. « aucun n’est toutefois</l>
						<l n="104" num="9.16">Ni plus riche en vrais biens, ni plus libre en son choix,</l>
						<l n="105" num="9.17">Ni plus puissant enfin que ce chétif esclave</l>
						<l n="106" num="9.18">Qui foulant tout aux pieds, lui-même encor se brave,</l>
						<l n="107" num="9.19">Et rompant avec soi pour s’unir à son Dieu,</l>
						<l n="108" num="9.20">Sait en tout et partout se mettre au plus bas lieu.</l>
					</lg>
				</div></body></text></TEI>