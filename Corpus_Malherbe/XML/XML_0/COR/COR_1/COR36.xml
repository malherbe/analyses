<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Imitation De Jésus-Christ</title>
				<title type="medium">Édition électronique</title>
				<author key="COR">
					<name>
						<forename>Pierre</forename>
						<surname>CORNEILLE</surname>
					</name>
					<date from="1606" to="1684">1606-1684</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>13205 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2016</date>
				<idno type="local">COR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Imitation De Jésus-Christ</title>
						<author>Pierre Corneille</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/pierrecorneilleimmitadiondejesuschrist.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Imitation De Jésus-Christ</title>
						<author>Pierre Corneille</author>
						<editor>M. Ch. Marty-Laveaux</editor>
						<edition>nouvelle édition</edition>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k51199.r=pierre%20corneille?rk=85837;2</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie de L. Hachette et Cie</publisher>
							<date when="1862">1862</date>
						</imprint>
						<biblScope unit="tome">Tome 8</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1656">1656</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les chiffres arabes des livres et chapitres ont été remplacés par des chiffres romains</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">LIVRE PREMIER</head><div type="poem" key="COR36">
					<head type="main">CHAPITRE IV</head>
					<head type="sub">De la prudence en sa conduite.</head>
					<lg n="1">
						<l n="1" num="1.1"><space unit="char" quantity="8"></space>N’écoute pas tout ce qu’on dit,</l>
						<l n="2" num="1.2"><space unit="char" quantity="8"></space>Et souviens-toi qu’une âme forte</l>
						<l n="3" num="1.3"><space unit="char" quantity="8"></space>Donne malaisément crédit</l>
						<l n="4" num="1.4">À ces bruits indiscrets où la foule s’emporte.</l>
						<l n="5" num="1.5">Il faut examiner avec sincérité,</l>
						<l n="6" num="1.6">Selon l’esprit de Dieu, qui n’est que charité,</l>
						<l n="7" num="1.7"><space unit="char" quantity="8"></space>Tout ce que d’un autre on publie :</l>
						<l n="8" num="1.8">Cependant, ô foiblesse indigne d’un chrétien !</l>
						<l n="9" num="1.9"><space unit="char" quantity="8"></space>Jusque-là souvent on s’oublie</l>
						<l n="10" num="1.10">Qu’on croit beaucoup de mal plutôt qu’un peu de bien.</l>
					</lg>
					<lg n="2">
						<l n="11" num="2.1"><space unit="char" quantity="8"></space>Qui cherche la perfection,</l>
						<l n="12" num="2.2"><space unit="char" quantity="8"></space>Loin de tout croire en téméraire,</l>
						<l n="13" num="2.3"><space unit="char" quantity="8"></space>Pèse avec mûre attention</l>
						<l n="14" num="2.4">Tout ce qu’il entend dire et tout ce qu’il voit faire.</l>
						<l n="15" num="2.5">La plus claire apparence a peine à l’engager :</l>
						<l n="16" num="2.6">Il sait que notre esprit est prompt à mal juger,</l>
						<l n="17" num="2.7"><space unit="char" quantity="8"></space>Notre langue prompte à médire ;</l>
						<l n="18" num="2.8">Et bien qu’il ait sa part en cette infirmité,</l>
						<l n="19" num="2.9"><space unit="char" quantity="8"></space>Sur lui-même il garde un empire</l>
						<l n="20" num="2.10">Qui le fait triompher de sa fragilité.</l>
					</lg>
					<lg n="3">
						<l n="21" num="3.1"><space unit="char" quantity="8"></space>C’est ainsi que son jugement,</l>
						<l n="22" num="3.2"><space unit="char" quantity="8"></space>Quoi qu’il apprenne, quoi qu’il sache,</l>
						<l n="23" num="3.3"><space unit="char" quantity="8"></space>Se porte sans empressement,</l>
						<l n="24" num="3.4">Sans qu’en opiniâtre à son sens il s’attache.</l>
						<l n="25" num="3.5">Il se défend longtemps du mal qu’on dit d’autrui,</l>
						<l n="26" num="3.6">Ou s’il en est enfin convaincu malgré lui,</l>
						<l n="27" num="3.7"><space unit="char" quantity="8"></space>Il ne s’en fait point le trompette ;</l>
						<l n="28" num="3.8">Et cette impression qu’il en prend à regret,</l>
						<l n="29" num="3.9"><space unit="char" quantity="8"></space>Qu’il désavoue et qu’il rejette,</l>
						<l n="30" num="3.10">Demeure dans son âme un éternel secret.</l>
					</lg>
					<lg n="4">
						<l n="31" num="4.1"><space unit="char" quantity="8"></space>Pour conseil en tes actions</l>
						<l n="32" num="4.2"><space unit="char" quantity="8"></space>Prends un homme de conscience ;</l>
						<l n="33" num="4.3"><space unit="char" quantity="8"></space>Préfère ses instructions</l>
						<l n="34" num="4.4">À ce qu’ose inventer l’effort de ta science.</l>
						<l n="35" num="4.5">La bonne et sainte vie, à chaque événement</l>
						<l n="36" num="4.6">Forme l’expérience, ouvre l’entendement,</l>
						<l n="37" num="4.7"><space unit="char" quantity="8"></space>Éclaire l’esprit qui l’embrasse ;</l>
						<l n="38" num="4.8">Et plus on a pour soi des sentiments abjets,</l>
						<l n="39" num="4.9"><space unit="char" quantity="8"></space>Plus Dieu, prodigue de sa grâce,</l>
						<l n="40" num="4.10">Répand à pleines mains la sagesse et la paix.</l>
					</lg>
				</div></body></text></TEI>