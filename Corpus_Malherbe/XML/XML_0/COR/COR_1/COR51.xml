<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Imitation De Jésus-Christ</title>
				<title type="medium">Édition électronique</title>
				<author key="COR">
					<name>
						<forename>Pierre</forename>
						<surname>CORNEILLE</surname>
					</name>
					<date from="1606" to="1684">1606-1684</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>13205 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2016</date>
				<idno type="local">COR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Imitation De Jésus-Christ</title>
						<author>Pierre Corneille</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/pierrecorneilleimmitadiondejesuschrist.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Imitation De Jésus-Christ</title>
						<author>Pierre Corneille</author>
						<editor>M. Ch. Marty-Laveaux</editor>
						<edition>nouvelle édition</edition>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k51199.r=pierre%20corneille?rk=85837;2</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie de L. Hachette et Cie</publisher>
							<date when="1862">1862</date>
						</imprint>
						<biblScope unit="tome">Tome 8</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1656">1656</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les chiffres arabes des livres et chapitres ont été remplacés par des chiffres romains</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">LIVRE PREMIER</head><div type="poem" key="COR51">
					<head type="main">CHAPITRE XIX</head>
					<head type="sub">Des exercices du bon religieux.</head>
					<lg n="1">
						<l n="1" num="1.1">Toi qui dedans un cloître as renfermé ta vie,</l>
						<l n="2" num="1.2">De toutes les vertus tâche de l’enrichir :</l>
						<l n="3" num="1.3">C’est sous ce digne effort que tu dois y blanchir ;</l>
						<l n="4" num="1.4">Ta règle te l’apprend, ton habit t’en convie.</l>
						<l n="5" num="1.5">Fais par un saint amas de ces vivants trésors</l>
						<l n="6" num="1.6">Que le dedans réponde à l’éclat du dehors,</l>
						<l n="7" num="1.7">Que tu sois devant Dieu tel que devant les hommes ;</l>
						<l n="8" num="1.8">Et de l’intérieur prends d’autant plus de soin,</l>
						<l n="9" num="1.9">Que Dieu sans se tromper connoît ce que nous sommes,</l>
						<l n="10" num="1.10">Et que du fond du cœur il se fait le témoin.</l>
					</lg>
					<lg n="2">
						<l n="11" num="2.1">Nos respects en tous lieux lui doivent des louanges,</l>
						<l n="12" num="2.2">En tous lieux il nous voit, il nous juge en tous lieux ;</l>
						<l n="13" num="2.3">Et comme nous marchons partout devant ses yeux,</l>
						<l n="14" num="2.4">Partout il faut porter la pureté des anges.</l>
						<l n="15" num="2.5">Chaque jour recommence à lui donner ton cœur,</l>
						<l n="16" num="2.6">Renouvelle tes vœux, rallume ta ferveur,</l>
						<l n="17" num="2.7">Et t’obstine à lui dire, en demandant sa grâce :</l>
						<l n="18" num="2.8">« Secourez-moi, seigneur, et servez de soutien</l>
						<l n="19" num="2.9">Aux bons commencements que sous vos lois j’embrasse ;</l>
						<l n="20" num="2.10">Car jusques à présent ce que j’ai fait n’est rien. »</l>
					</lg>
					<lg n="3">
						<l n="21" num="3.1">Dans le chemin du ciel l’âme du juste avance,</l>
						<l n="22" num="3.2">Autant que ce propos augmente en fermeté ;</l>
						<l n="23" num="3.3">Son progrès, qui dépend de l’assiduité,</l>
						<l n="24" num="3.4">Veut pour beaucoup de fruit beaucoup de diligence.</l>
						<l n="25" num="3.5">Que si le plus constant et le mieux affermi</l>
						<l n="26" num="3.6">Se relâche souvent, souvent tombe à demi,</l>
						<l n="27" num="3.7">Et n’est jamais si fort qu’il soit inébranlable,</l>
						<l n="28" num="3.8">Que sera-ce de ceux dont le cœur languissant,</l>
						<l n="29" num="3.9">Ou rarement en soi forme un projet semblable,</l>
						<l n="30" num="3.10">Ou le laisse flotter et s’éteindre en naissant ?</l>
					</lg>
					<lg n="4">
						<l n="31" num="4.1">C’est un chemin qui monte entre des précipices :</l>
						<l n="32" num="4.2">Il n’est rien plus aisé que de l’abandonner ;</l>
						<l n="33" num="4.3">Et souvent c’est assez pour nous en détourner</l>
						<l n="34" num="4.4">Que le relâchement des moindres exercices.</l>
						<l n="35" num="4.5">Le bon propos du juste a plus de fondement</l>
						<l n="36" num="4.6">En la grâce de Dieu qu’au propre sentiment.</l>
						<l n="37" num="4.7">Quelque dessein qu’il fasse, en elle il se repose :</l>
						<l n="38" num="4.8">À moins d’un tel secours nous travaillons en vain ;</l>
						<l n="39" num="4.9">Quoi que nous proposions, c’est Dieu seul qui dispose,</l>
						<l n="40" num="4.10">Et pour trouver sa voie, homme, il te faut sa main.</l>
					</lg>
					<lg n="5">
						<l n="41" num="5.1">Laisse là quelquefois l’exercice ordinaire</l>
						<l n="42" num="5.2">Pour faire une action pleine de piété ;</l>
						<l n="43" num="5.3">Tu pourras y rentrer avec facilité</l>
						<l n="44" num="5.4">Si tu n’en es sorti que pour servir ton frère ;</l>
						<l n="45" num="5.5">Mais si par nonchalance, ou par un lâche ennui</l>
						<l n="46" num="5.6">De prendre encor demain le même qu’aujourd’hui,</l>
						<l n="47" num="5.7">Ton âme appesantie une fois s’en détache,</l>
						<l n="48" num="5.8">Cet exercice alors négligé sans sujet</l>
						<l n="49" num="5.9">Imprimera sur elle une honteuse tache,</l>
						<l n="50" num="5.10">Et lui fera sentir le mal qu’elle s’est fait.</l>
					</lg>
					<lg n="6">
						<l n="51" num="6.1">Quelque effort qu’ici-bas l’homme fasse à bien vivre,</l>
						<l n="52" num="6.2">Il est souvent trahi par sa fragilité ;</l>
						<l n="53" num="6.3">Et le meilleur remède à son infirmité,</l>
						<l n="54" num="6.4">C’est de choisir toujours un but certain à suivre.</l>
						<l n="55" num="6.5">Qu’il regarde surtout quel est l’empêchement</l>
						<l n="56" num="6.6">Qui met le plus d’obstacle à son avancement,</l>
						<l n="57" num="6.7">Et que tout son pouvoir s’attache à l’en défaire ;</l>
						<l n="58" num="6.8">Qu’il donne ordre au dedans, qu’il donne ordre au dehors ;</l>
						<l n="59" num="6.9">À cet heureux progrès l’un et l’autre confère,</l>
						<l n="60" num="6.10">Et l’âme a plus de force ayant l’aide du corps.</l>
					</lg>
					<lg n="7">
						<l n="61" num="7.1">Si ta retraite en toi ne peut être assidue,</l>
						<l n="62" num="7.2">Recueille-toi du moins une fois chaque jour,</l>
						<l n="63" num="7.3">Soit lorsque le soleil recommence son tour,</l>
						<l n="64" num="7.4">Soit lorsque sous les eaux sa lumière est fondue.</l>
						<l n="65" num="7.5">Propose le matin et règle tes projets,</l>
						<l n="66" num="7.6">Examine le soir quels en sont les effets ;</l>
						<l n="67" num="7.7">Revois tes actions, tes discours, tes pensées :</l>
						<l n="68" num="7.8">Peut-être y verras-tu, malgré ton bon dessein,</l>
						<l n="69" num="7.9">À chaque occasion mille offenses glissées</l>
						<l n="70" num="7.10">Contre le grand monarque, ou contre le prochain.</l>
					</lg>
					<lg n="8">
						<l n="71" num="8.1">Montre-toi vraiment homme à l’attaque funeste</l>
						<l n="72" num="8.2">Que l’ange ténébreux te porte à tout moment ;</l>
						<l n="73" num="8.3">Dompte la gourmandise, et plus facilement</l>
						<l n="74" num="8.4">Des sentiments charnels tu dompteras le reste.</l>
						<l n="75" num="8.5">Dedans l’oisiveté jamais enseveli,</l>
						<l n="76" num="8.6">Toujours confère, prie, écris, médite, li,</l>
						<l n="77" num="8.7">Ou fais pour le commun quelque chose d’utile :</l>
						<l n="78" num="8.8">L’exercice du corps a quelques fruits bien doux ;</l>
						<l n="79" num="8.9">Mais sans discrétion c’est un travail stérile,</l>
						<l n="80" num="8.10">Et même il n’est pas propre également à tous.</l>
					</lg>
					<lg n="9">
						<l n="81" num="9.1">Ces emplois singuliers qu’on se choisit soi-même</l>
						<l n="82" num="9.2">Doivent fuir avec soin de paroître au dehors ;</l>
						<l n="83" num="9.3">L’étalage les perd, et ce sont des trésors</l>
						<l n="84" num="9.4">Dont la possession veut un secret extrême.</l>
						<l n="85" num="9.5">Surtout n’aime jamais ces choix de ton esprit</l>
						<l n="86" num="9.6">Jusqu’à les préférer à ce qui t’est prescrit ;</l>
						<l n="87" num="9.7">Tout le surabondant doit place au nécessaire.</l>
						<l n="88" num="9.8">Remplis tous tes devoirs avec fidélité ;</l>
						<l n="89" num="9.9">Puis, s’il reste du temps pour l’emploi volontaire,</l>
						<l n="90" num="9.10">Applique tout ce reste où ton zèle est porté.</l>
					</lg>
					<lg n="10">
						<l n="91" num="10.1">Tout esprit n’est pas propre aux mêmes exercices :</l>
						<l n="92" num="10.2">L’un est meilleur pour l’un, l’autre à l’autre sert mieux ;</l>
						<l n="93" num="10.3">Et la diversité, soit des temps, soit des lieux,</l>
						<l n="94" num="10.4">Demande à notre ardeur de différents offices :</l>
						<l n="95" num="10.5">L’un est bon à la fête, et l’autre aux simples jours ;</l>
						<l n="96" num="10.6">De la tentation l’un peut rompre le cours,</l>
						<l n="97" num="10.7">À la tranquillité l’autre est plus convenable ;</l>
						<l n="98" num="10.8">L’homme n’a pas sur soi toujours même pouvoir :</l>
						<l n="99" num="10.9">Autres sont les pensers que la tristesse accable,</l>
						<l n="100" num="10.10">Autres ceux que la joie en Dieu fait concevoir.</l>
					</lg>
					<lg n="11">
						<l n="101" num="11.1">À chaque grande fête augmente et renouvelle</l>
						<l n="102" num="11.2">Et ce bon exercice et ta prière aux saints ;</l>
						<l n="103" num="11.3">Et tiens en l’attendant ton âme entre tes mains,</l>
						<l n="104" num="11.4">Comme prête à passer à la fête éternelle.</l>
						<l n="105" num="11.5">En ces jours consacrés à la dévotion,</l>
						<l n="106" num="11.6">Il faut mieux épurer l’œuvre et l’intention,</l>
						<l n="107" num="11.7">Suivre une plus étroite et plus ferme observance,</l>
						<l n="108" num="11.8">Nous recueillir sans cesse, et nous imaginer</l>
						<l n="109" num="11.9">Que de tous nos travaux la pleine récompense</l>
						<l n="110" num="11.10">Doit par les mains de Dieu bientôt nous couronner.</l>
					</lg>
					<lg n="12">
						<l n="111" num="12.1">Souvent il la recule, et lors il nous faut croire</l>
						<l n="112" num="12.2">Que nous n’y sommes pas dignement préparés,</l>
						<l n="113" num="12.3">Et que ces doux moments ne nous sont différés</l>
						<l n="114" num="12.4">Qu’afin que nous puissions mériter plus de gloire.</l>
						<l n="115" num="12.5">Il nous en comblera dans le temps ordonné :</l>
						<l n="116" num="12.6">Préparons-nous donc mieux à ce jour fortuné.</l>
						<l n="117" num="12.7">« Heureux le serviteur, dit la vérité même,</l>
						<l n="118" num="12.8">Que trouvera son maître en état de veiller !</l>
						<l n="119" num="12.9">Il lui partagera son propre diadème,</l>
						<l n="120" num="12.10">Et de toute sa gloire il le fera briller. »</l>
					</lg>
				</div></body></text></TEI>