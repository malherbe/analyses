<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Imitation De Jésus-Christ</title>
				<title type="medium">Édition électronique</title>
				<author key="COR">
					<name>
						<forename>Pierre</forename>
						<surname>CORNEILLE</surname>
					</name>
					<date from="1606" to="1684">1606-1684</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>13205 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2016</date>
				<idno type="local">COR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Imitation De Jésus-Christ</title>
						<author>Pierre Corneille</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/pierrecorneilleimmitadiondejesuschrist.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Imitation De Jésus-Christ</title>
						<author>Pierre Corneille</author>
						<editor>M. Ch. Marty-Laveaux</editor>
						<edition>nouvelle édition</edition>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k51199.r=pierre%20corneille?rk=85837;2</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie de L. Hachette et Cie</publisher>
							<date when="1862">1862</date>
						</imprint>
						<biblScope unit="tome">Tome 8</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1656">1656</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les chiffres arabes des livres et chapitres ont été remplacés par des chiffres romains</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">LIVRE TROISIÈME</head><div type="poem" key="COR109">
					<head type="main">CHAPITRE XL</head>
					<head type="sub">Que l’homme n’a rien de bon de soi-même, et ne <lb></lb>se peut glorifier d’aucune chose.</head>
					<lg n="1">
						<l n="1" num="1.1">Seigneur, qu’est-ce que l’homme, et dans ton souvenir</l>
						<l n="2" num="1.2">Qui lui donne le rang que tu l’y fais tenir ?</l>
						<l n="3" num="1.3">Que sont les fils d’Adam, que sont tous leurs mérites,</l>
						<l n="4" num="1.4">Pour attirer chez eux l’honneur de tes visites ?</l>
						<l n="5" num="1.5">Que t’a fait l’homme enfin, que ta grâce pour lui</l>
						<l n="6" num="1.6">Aime à se prodiguer, et lui servir d’appui ?</l>
						<l n="7" num="1.7">Ai-je lieu de m’en plaindre avec quelque justice,</l>
						<l n="8" num="1.8">Quand elle m’abandonne à mon propre caprice ?</l>
						<l n="9" num="1.9">Et puis-je à ta rigueur reprocher quelque excès,</l>
						<l n="10" num="1.10">Quand toute ma prière obtient peu de succès ?</l>
					</lg>
					<lg n="2">
						<l n="11" num="2.1">C’est bien alors à moi d’avouer ma foiblesse ;</l>
						<l n="12" num="2.2">C’est à moi de penser et de dire sans cesse :</l>
						<l n="13" num="2.3">« Seigneur, je ne suis rien, je ne puis rien de moi,</l>
						<l n="14" num="2.4">Et je n’ai rien de bon, s’il ne me vient de toi. »</l>
					</lg>
					<lg n="3">
						<l n="15" num="3.1">Mes défauts sont si grands, mon impuissance est telle,</l>
						<l n="16" num="3.2">Qu’elle a vers le néant une pente éternelle.</l>
						<l n="17" num="3.3">À moins que ton secours me relève le cœur,</l>
						<l n="18" num="3.4">À moins que ta bonté ranime ma langueur,</l>
						<l n="19" num="3.5">Qu’elle daigne au dedans me former et m’instruire,</l>
						<l n="20" num="3.6">Mes plus ardents efforts ne peuvent rien produire,</l>
						<l n="21" num="3.7">Et mon infirmité retrouve en un moment</l>
						<l n="22" num="3.8">La tiédeur, le désordre et le relâchement.</l>
					</lg>
					<lg n="4">
						<l n="23" num="4.1">Toi seul, toujours le même et toujours immuable,</l>
						<l n="24" num="4.2">Te soutiens dans un être à jamais perdurable,</l>
						<l n="25" num="4.3">Toujours bon, toujours saint, toujours juste, et toujours</l>
						<l n="26" num="4.4">Dispensant saintement ton bienheureux secours.</l>
						<l n="27" num="4.5">Ta bonté, ta justice agit en toutes choses,</l>
						<l n="28" num="4.6">Et de tout et partout sagement tu disposes ;</l>
						<l n="29" num="4.7">Mais pour moi, qui toujours penche plus fortement</l>
						<l n="30" num="4.8">Vers l’imperfection que vers l’avancement,</l>
						<l n="31" num="4.9">Je n’ai pas un esprit toujours en même assiette :</l>
						<l n="32" num="4.10">Il cherche, il craint, il fuit, il embrasse, il rejette,</l>
						<l n="33" num="4.11">Et son meilleur état, par un triste retour,</l>
						<l n="34" num="4.12">Est sujet à changer plus de sept fois le jour.</l>
					</lg>
					<lg n="5">
						<l n="35" num="5.1">Tous mes maux toutefois rencontrent leur remède,</l>
						<l n="36" num="5.2">Aussitôt qu’il t’a plu d’accourir à mon aide ;</l>
						<l n="37" num="5.3">Et pour faire à mon âme un bonheur souverain,</l>
						<l n="38" num="5.4">Tu n’as qu’à lui prêter, qu’à lui tendre la main.</l>
						<l n="39" num="5.5">Tu le peux, ô mon Dieu, de ta volonté pure,</l>
						<l n="40" num="5.6">Sans emprunter le bras d’aucune créature :</l>
						<l n="41" num="5.7">Tu me peux de toi seul si bien fortifier,</l>
						<l n="42" num="5.8">Que mon âme n’ait plus de quoi se défier,</l>
						<l n="43" num="5.9">Que ma constante ardeur ne tourne plus en glace,</l>
						<l n="44" num="5.10">Que mon sort affermi ne change plus de face,</l>
						<l n="45" num="5.11">Et que mon cœur enfin, plein de zèle et de foi,</l>
						<l n="46" num="5.12">Ainsi que dans son centre ait son repos en toi.</l>
					</lg>
					<lg n="6">
						<l n="47" num="6.1">Ah ! Si jamais ce cœur pouvoit bien se défaire</l>
						<l n="48" num="6.2">Des consolations que la terre suggère,</l>
						<l n="49" num="6.3">Soit pour mieux faire place aux célestes faveurs</l>
						<l n="50" num="6.4">Qui font naître ici-bas et croître les ferveurs,</l>
						<l n="51" num="6.5">Soit par ce grand besoin qui réduit ma foiblesse</l>
						<l n="52" num="6.6">À la nécessité d’implorer ta tendresse,</l>
						<l n="53" num="6.7">Puisque dans les malheurs où je me sens couler</l>
						<l n="54" num="6.8">Il n’est aucun mortel qui puisse consoler,</l>
						<l n="55" num="6.9">Alors certes, alors j’aurois pleine matière</l>
						<l n="56" num="6.10">D’espérer de ta grâce une abondance entière,</l>
						<l n="57" num="6.11">Et de m’épanouir à ces charmes nouveaux</l>
						<l n="58" num="6.12">Dont je verrois ta main adoucir mes travaux.</l>
					</lg>
					<lg n="7">
						<l n="59" num="7.1">C’est de toi, mon sauveur, c’est de toi, source vive,</l>
						<l n="60" num="7.2">Que se répand sur moi tout le bien qui m’arrive.</l>
						<l n="61" num="7.3">Je ne suis qu’un néant bouffi de vanité,</l>
						<l n="62" num="7.4">Je ne suis qu’inconstance et qu’imbécillité ;</l>
						<l n="63" num="7.5">Et quand je me demande un titre légitime</l>
						<l n="64" num="7.6">D’où prendre quelque gloire, et chercher quelque estime,</l>
						<l n="65" num="7.7">Je vois, pour tout appui de mes plus hauts efforts,</l>
						<l n="66" num="7.8">Le néant que je suis, et le rien d’où je sors,</l>
						<l n="67" num="7.9">Et que fonder sa gloire ainsi sur le rien même,</l>
						<l n="68" num="7.10">C’est une vanité qui va jusqu’à l’extrême.</l>
					</lg>
					<lg n="8">
						<l n="69" num="8.1">Ô vent pernicieux ! ô poison des esprits !</l>
						<l n="70" num="8.2">Que le monde sait peu ton véritable prix !</l>
						<l n="71" num="8.3">Ô fausse et vaine gloire ! ô dangereuse peste,</l>
						<l n="72" num="8.4">Qui n’es rien qu’un néant, mais un néant funeste !</l>
						<l n="73" num="8.5">Tes décevants attraits retirent tous nos pas</l>
						<l n="74" num="8.6">Du chemin où la vraie étale ses appas,</l>
						<l n="75" num="8.7">Et l’âme, de ton souffle indignement souillée,</l>
						<l n="76" num="8.8">Des grâces de son maître est par toi dépouillée.</l>
						<l n="77" num="8.9">Oui, notre âme, seigneur, tout ton portrait qu’elle est,</l>
						<l n="78" num="8.10">Commence à te déplaire alors qu’elle se plaît,</l>
						<l n="79" num="8.11">Et son avidité pour de vaines louanges</l>
						<l n="80" num="8.12">La prive des vertus qui l’égaloient aux anges.</l>
					</lg>
					<lg n="9">
						<l n="81" num="9.1">On peut se réjouir et se glorifier,</l>
						<l n="82" num="9.2">Mais ce n’est qu’en toi seul qu’il faut tout appuyer ;</l>
						<l n="83" num="9.3">En toi seul, non en soi, qu’il faut prendre sans cesse</l>
						<l n="84" num="9.4">La véritable gloire et la sainte allégresse,</l>
						<l n="85" num="9.5">Rapporter à toi seul, et non à sa vertu,</l>
						<l n="86" num="9.6">Le plus solide éclat dont on soit revêtu,</l>
						<l n="87" num="9.7">Louer en tous ses dons l’auteur de la nature,</l>
						<l n="88" num="9.8">Et ne voir que lui seul en toute créature.</l>
					</lg>
					<lg n="10">
						<l n="89" num="10.1">Je le veux, ô mon dieu, si je fais quelque bien,</l>
						<l n="90" num="10.2">Pour en louer ton nom qu’on supprime le mien,</l>
						<l n="91" num="10.3">Que l’univers entier par de communs suffrages</l>
						<l n="92" num="10.4">Sur le mépris des miens élève tes ouvrages,</l>
						<l n="93" num="10.5">Que même en celui-ci mon nom soit ignoré</l>
						<l n="94" num="10.6">Afin que le tien seul en soit mieux adoré,</l>
						<l n="95" num="10.7">Que ton Saint-Esprit seul en ait toute la gloire,</l>
						<l n="96" num="10.8">Sans que louange aucune honore ma mémoire,</l>
						<l n="97" num="10.9">Et que puisse à mes yeux s’emparer qui voudra</l>
						<l n="98" num="10.10">De la plus douce odeur que mon vers répandra.</l>
					</lg>
					<lg n="11">
						<l n="99" num="11.1">En toi seul est ma gloire, en toi seul est ma joie,</l>
						<l n="100" num="11.2">Et quoi que l’avenir en ma faveur déploie,</l>
						<l n="101" num="11.3">Je les veux prendre en toi, sans faire vanité</l>
						<l n="102" num="11.4">Que du sincère aveu de mon infirmité.</l>
					</lg>
					<lg n="12">
						<l n="103" num="12.1">C’est aux juifs, c’est aux cœurs que ta grâce abandonne,</l>
						<l n="104" num="12.2">À chercher cet honneur qu’ici l’on s’entre-donne :</l>
						<l n="105" num="12.3">Ils peuvent y courir avec empressement,</l>
						<l n="106" num="12.4">Sans que je porte envie à leur aveuglement.</l>
						<l n="107" num="12.5">La gloire que je cherche, et l’honneur où j’aspire,</l>
						<l n="108" num="12.6">C’est celle, c’est celui que fait ton saint empire,</l>
						<l n="109" num="12.7">Qu’à tes vrais serviteurs départ ta seule main,</l>
						<l n="110" num="12.8">Et qui ne peut souffrir aucun mélange humain.</l>
						<l n="111" num="12.9">Ces honneurs temporels qui rendent l’âme vaine,</l>
						<l n="112" num="12.10">Ces orgueilleux dehors de la grandeur mondaine,</l>
						<l n="113" num="12.11">À ta gloire éternelle une fois comparés,</l>
						<l n="114" num="12.12">Ne sont qu’amusements de cerveaux égarés.</l>
					</lg>
					<lg n="13">
						<l n="115" num="13.1">Ô vérité suprême et toujours adorable,</l>
						<l n="116" num="13.2">Miséricorde immense et toujours ineffable,</l>
						<l n="117" num="13.3">Je ne réclame point dans ma fragilité</l>
						<l n="118" num="13.4">D’autre miséricorde ou d’autre vérité.</l>
						<l n="119" num="13.5">À toi, trinité sainte, espoir du vrai fidèle,</l>
						<l n="120" num="13.6">À toi pleine louange, à toi gloire immortelle !</l>
						<l n="121" num="13.7">Puisse tout l’univers, puisse tout l’avenir,</l>
						<l n="122" num="13.8">Toute l’éternité te louer et bénir !</l>
						<l n="123" num="13.9">Ce sont là tous mes vœux, c’est là tout l’avantage</l>
						<l n="124" num="13.10">Que mes foibles travaux demandent en partage,</l>
						<l n="125" num="13.11">Trop heureux si l’éclat de mon plus digne emploi</l>
						<l n="126" num="13.12">Laisse mon nom obscur pour rejaillir sur toi !</l>
					</lg>
				</div></body></text></TEI>