<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Imitation De Jésus-Christ</title>
				<title type="medium">Édition électronique</title>
				<author key="COR">
					<name>
						<forename>Pierre</forename>
						<surname>CORNEILLE</surname>
					</name>
					<date from="1606" to="1684">1606-1684</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>13205 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2016</date>
				<idno type="local">COR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Imitation De Jésus-Christ</title>
						<author>Pierre Corneille</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/pierrecorneilleimmitadiondejesuschrist.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Imitation De Jésus-Christ</title>
						<author>Pierre Corneille</author>
						<editor>M. Ch. Marty-Laveaux</editor>
						<edition>nouvelle édition</edition>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k51199.r=pierre%20corneille?rk=85837;2</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie de L. Hachette et Cie</publisher>
							<date when="1862">1862</date>
						</imprint>
						<biblScope unit="tome">Tome 8</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1656">1656</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les chiffres arabes des livres et chapitres ont été remplacés par des chiffres romains</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">LIVRE TROISIÈME</head><div type="poem" key="COR100">
					<head type="main">CHAPITRE XXXI</head>
					<head type="sub">Du mépris de toutes les créatures pour s’élever <lb></lb>au créateur.</head>
					<lg n="1">
						<l n="1" num="1.1">Seigneur, si jusqu’ici tu m’as fait mille grâces,</l>
						<l n="2" num="1.2"><space unit="char" quantity="8"></space>Il n’est pas temps que tu t’en lasses :</l>
						<l n="3" num="1.3">J’ai besoin d’un secours encor bien plus puissant,</l>
						<l n="4" num="1.4">Puisqu’il faut m’élever par-dessus la nature,</l>
						<l n="5" num="1.5">Et prendre un vol si haut, qu’aucune créature</l>
						<l n="6" num="1.6"><space unit="char" quantity="8"></space>N’ait pour moi rien d’embarrassant.</l>
					</lg>
					<lg n="2">
						<l n="7" num="2.1">À cet heureux effort en vain je me dispose :</l>
						<l n="8" num="2.2"><space unit="char" quantity="8"></space>Tant qu’ici-bas la moindre chose</l>
						<l n="9" num="2.3">Vers ses foibles attraits saura me ravaler,</l>
						<l n="10" num="2.4">L’imperceptible joug d’une indigne contrainte</l>
						<l n="11" num="2.5">Ne me permettra point cette liberté sainte</l>
						<l n="12" num="2.6"><space unit="char" quantity="8"></space>Qui jusqu’à toi nous fait voler.</l>
					</lg>
					<lg n="3">
						<l n="13" num="3.1">Ton David à ce vol ne vouloit point d’obstacle,</l>
						<l n="14" num="3.2"><space unit="char" quantity="8"></space>Et te demandoit ce miracle,</l>
						<l n="15" num="3.3">Lorsque dans ses ennuis il tenoit ce propos :</l>
						<l n="16" num="3.4">« Qui pourra me donner des ailes de colombe,</l>
						<l n="17" num="3.5">Et du milieu des maux sous qui mon cœur succombe</l>
						<l n="18" num="3.6"><space unit="char" quantity="8"></space>Je volerai jusqu’au repos ? »</l>
					</lg>
					<lg n="4">
						<l n="19" num="4.1">Cet oiseau du vrai calme est le portrait visible ;</l>
						<l n="20" num="4.2"><space unit="char" quantity="8"></space>On ne voit rien de si paisible</l>
						<l n="21" num="4.3">Que la simplicité que nous peignent ses yeux :</l>
						<l n="22" num="4.4">On ne voit rien de libre à l’égal d’un vrai zèle,</l>
						<l n="23" num="4.5">Qui sans rien desirer s’élève à tire-d’aile</l>
						<l n="24" num="4.6"><space unit="char" quantity="8"></space>Au-dessus de tous ces bas lieux.</l>
					</lg>
					<lg n="5">
						<l n="25" num="5.1">Il faut donc pleinement s’abandonner soi-même,</l>
						<l n="26" num="5.2"><space unit="char" quantity="8"></space>S’arracher à tout ce qu’on aime,</l>
						<l n="27" num="5.3">Pousser jusques au ciel des transports plus qu’humains,</l>
						<l n="28" num="5.4">Et bien considérer quels sont les avantages</l>
						<l n="29" num="5.5">Que l’auteur souverain a sur tous les ouvrages</l>
						<l n="30" num="5.6"><space unit="char" quantity="8"></space>Qu’ont daigné façonner ses mains.</l>
					</lg>
					<lg n="6">
						<l n="31" num="6.1">Sans ce détachement, sans cette haute extase,</l>
						<l n="32" num="6.2"><space unit="char" quantity="8"></space>L’âme que ton amour embrase</l>
						<l n="33" num="6.3">Ne peut en liberté goûter tes entretiens :</l>
						<l n="34" num="6.4">Peu savent en effet contempler tes mystères,</l>
						<l n="35" num="6.5">Mais peu forment aussi ces mépris salutaires</l>
						<l n="36" num="6.6"><space unit="char" quantity="8"></space>De toutes sortes de faux biens.</l>
					</lg>
					<lg n="7">
						<l n="37" num="7.1">Ainsi l’homme a besoin que ta bonté suprême,</l>
						<l n="38" num="7.2"><space unit="char" quantity="8"></space>L’élevant par-dessus lui-même,</l>
						<l n="39" num="7.3">Prodigue en sa faveur son trésor infini ;</l>
						<l n="40" num="7.4">Qu’un excès de ta grâce en esprit le ravisse,</l>
						<l n="41" num="7.5">Et de tout autre objet tellement l’affranchisse,</l>
						<l n="42" num="7.6"><space unit="char" quantity="8"></space>Qu’à toi seul il demeure uni.</l>
					</lg>
					<lg n="8">
						<l n="43" num="8.1">À moins que jusque-là l’enlève ainsi ton aide,</l>
						<l n="44" num="8.2"><space unit="char" quantity="8"></space>Quoi qu’il sache, quoi qu’il possède,</l>
						<l n="45" num="8.3">Tout n’est pas de grand poids, tout ne lui sert de rien :</l>
						<l n="46" num="8.4">Il rampe et rampera toujours foible et débile,</l>
						<l n="47" num="8.5">S’il peut s’imaginer rien de grand ou d’utile</l>
						<l n="48" num="8.6"><space unit="char" quantity="8"></space>Que l’immense et souverain bien.</l>
					</lg>
					<lg n="9">
						<l n="49" num="9.1">Tout ce qui n’est point Dieu n’est point digne d’estime,</l>
						<l n="50" num="9.2"><space unit="char" quantity="8"></space>Et son prix le plus légitime,</l>
						<l n="51" num="9.3">Comme enfin ce n’est rien, c’est d’être à rien compté :</l>
						<l n="52" num="9.4">Vous le savez, dévots que la grâce illumine ;</l>
						<l n="53" num="9.5">Votre doctrine aussi de toute autre doctrine</l>
						<l n="54" num="9.6"><space unit="char" quantity="8"></space>Diffère bien en dignité.</l>
					</lg>
					<lg n="10">
						<l n="55" num="10.1">Sa noblesse est bien autre ; et comme l’influence</l>
						<l n="56" num="10.2"><space unit="char" quantity="8"></space>De la suprême intelligence</l>
						<l n="57" num="10.3">Par un sacré canal d’en haut la fait couler,</l>
						<l n="58" num="10.4">Ce qu’à l’esprit humain en peut donner l’étude,</l>
						<l n="59" num="10.5">Ce qu’en peut acquérir la longue inquiétude,</l>
						<l n="60" num="10.6"><space unit="char" quantity="8"></space>Ne la peut jamais égaler.</l>
					</lg>
					<lg n="11">
						<l n="61" num="11.1">Le bien de contempler ce que les cieux admirent</l>
						<l n="62" num="11.2"><space unit="char" quantity="8"></space>Est un bien où plusieurs aspirent,</l>
						<l n="63" num="11.3">Et que de tout leur cœur ils voudroient obtenir ;</l>
						<l n="64" num="11.4">Mais ils suivent si mal la route nécessaire,</l>
						<l n="65" num="11.5">Que souvent ils ne font que ce qu’il faudroit faire</l>
						<l n="66" num="11.6"><space unit="char" quantity="8"></space>Pour éviter d’y parvenir.</l>
					</lg>
					<lg n="12">
						<l n="67" num="12.1">Le trop d’abaissement vers les objets sensibles</l>
						<l n="68" num="12.2"><space unit="char" quantity="8"></space>Fait des obstacles invincibles,</l>
						<l n="69" num="12.3">Comme le trop de soin des marques du dehors ;</l>
						<l n="70" num="12.4">Et la sévérité la mieux étudiée,</l>
						<l n="71" num="12.5">Si l’âme n’est en soi la plus mortifiée,</l>
						<l n="72" num="12.6"><space unit="char" quantity="8"></space>Ne sert qu’au supplice du corps.</l>
					</lg>
					<lg n="13">
						<l n="73" num="13.1">J’ignore, à dire vrai, de quel esprit nous sommes,</l>
						<l n="74" num="13.2"><space unit="char" quantity="8"></space>Nous autres qui parmi les hommes</l>
						<l n="75" num="13.3">Passons pour éclairés et pour spirituels,</l>
						<l n="76" num="13.4">Et nous plongeons ainsi pour des choses légères,</l>
						<l n="77" num="13.5">De vils amusements, des douceurs passagères,</l>
						<l n="78" num="13.6"><space unit="char" quantity="8"></space>En des travaux continuels.</l>
					</lg>
					<lg n="14">
						<l n="79" num="14.1">Parmi de tels soucis que pouvons-nous prétendre,</l>
						<l n="80" num="14.2"><space unit="char" quantity="8"></space>Nous qui savons si peu descendre</l>
						<l n="81" num="14.3">Dans le fond de nos cœurs indignement remplis,</l>
						<l n="82" num="14.4">Et qui si rarement de toutes nos pensées</l>
						<l n="83" num="14.5">Appliquons au dedans les forces ramassées</l>
						<l n="84" num="14.6"><space unit="char" quantity="8"></space>Pour en voir les secrets replis ?</l>
					</lg>
					<lg n="15">
						<l n="85" num="15.1">Notre âme en elle-même à peine est recueillie,</l>
						<l n="86" num="15.2"><space unit="char" quantity="8"></space>Qu’une extravagante saillie</l>
						<l n="87" num="15.3">Nous emporte au dehors, et fait tout avorter,</l>
						<l n="88" num="15.4">Sans repasser jamais sous l’examen sévère</l>
						<l n="89" num="15.5">Ce que nous avons fait, ce que nous voulions faire,</l>
						<l n="90" num="15.6"><space unit="char" quantity="8"></space>Ni ce qu’il nous faut projeter.</l>
					</lg>
					<lg n="16">
						<l n="91" num="16.1">Nous suivons nos desirs sans même y prendre garde,</l>
						<l n="92" num="16.2"><space unit="char" quantity="8"></space>Et rarement notre œil regarde</l>
						<l n="93" num="16.3">Combien à leurs effets d’impureté se joint.</l>
						<l n="94" num="16.4">Lorsque toute la chair eut corrompu sa voie,</l>
						<l n="95" num="16.5">Nous savons que des eaux elle devint la proie,</l>
						<l n="96" num="16.6"><space unit="char" quantity="8"></space>Cependant nous ne tremblons point.</l>
					</lg>
					<lg n="17">
						<l n="97" num="17.1">L’affection interne étant toute gâtée,</l>
						<l n="98" num="17.2"><space unit="char" quantity="8"></space>Les objets dont l’âme est flattée</l>
						<l n="99" num="17.3">N’y faisant qu’une impure et folle impression,</l>
						<l n="100" num="17.4">Il faut bien que l’effet, pareil à son principe,</l>
						<l n="101" num="17.5">Pour marque qu’au dedans la vigueur se dissipe,</l>
						<l n="102" num="17.6"><space unit="char" quantity="8"></space>Porte même corruption.</l>
					</lg>
					<lg n="18">
						<l n="103" num="18.1">Quand un cœur est bien pur, une vertu solide</l>
						<l n="104" num="18.2"><space unit="char" quantity="8"></space>À tous ses mouvements préside ;</l>
						<l n="105" num="18.3">La bonne et sainte vie en est le digne fruit ;</l>
						<l n="106" num="18.4">Mais ce dedans n’est pas ce que l’on considère,</l>
						<l n="107" num="18.5">Et depuis qu’une fois l’effet a de quoi plaire,</l>
						<l n="108" num="18.6"><space unit="char" quantity="8"></space>N’importe comme il est produit.</l>
					</lg>
					<lg n="19">
						<l n="109" num="19.1">La beauté, le savoir, les forces, la richesse,</l>
						<l n="110" num="19.2"><space unit="char" quantity="8"></space>L’heureux travail, la haute adresse,</l>
						<l n="111" num="19.3">C’est ce qu’on examine, et qui fait estimer :</l>
						<l n="112" num="19.4">Qu’un homme soit dévot, patient, humble, affable,</l>
						<l n="113" num="19.5">Qu’il soit pauvre d’esprit, recueilli, charitable,</l>
						<l n="114" num="19.6"><space unit="char" quantity="8"></space>On ne daigne s’en informer.</l>
					</lg>
					<lg n="20">
						<l n="115" num="20.1">Ce n’est qu’à ces dehors que se prend la nature</l>
						<l n="116" num="20.2"><space unit="char" quantity="8"></space>Pour s’en former une peinture ;</l>
						<l n="117" num="20.3">Mais c’est l’intérieur que la grâce veut voir :</l>
						<l n="118" num="20.4">L’une est souvent déçue à suivre l’apparence ;</l>
						<l n="119" num="20.5">Mais l’autre met toujours toute son espérance</l>
						<l n="120" num="20.6"><space unit="char" quantity="8"></space>En Dieu, qui ne peut décevoir.</l>
					</lg>
				</div></body></text></TEI>