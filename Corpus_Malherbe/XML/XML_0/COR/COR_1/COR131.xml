<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Imitation De Jésus-Christ</title>
				<title type="medium">Édition électronique</title>
				<author key="COR">
					<name>
						<forename>Pierre</forename>
						<surname>CORNEILLE</surname>
					</name>
					<date from="1606" to="1684">1606-1684</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>13205 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2016</date>
				<idno type="local">COR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Imitation De Jésus-Christ</title>
						<author>Pierre Corneille</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/pierrecorneilleimmitadiondejesuschrist.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Imitation De Jésus-Christ</title>
						<author>Pierre Corneille</author>
						<editor>M. Ch. Marty-Laveaux</editor>
						<edition>nouvelle édition</edition>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k51199.r=pierre%20corneille?rk=85837;2</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie de L. Hachette et Cie</publisher>
							<date when="1862">1862</date>
						</imprint>
						<biblScope unit="tome">Tome 8</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1656">1656</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les chiffres arabes des livres et chapitres ont été remplacés par des chiffres romains</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">LIVRE QUATRIÈME</head><div type="poem" key="COR131">
					<head type="main">CHAPITRE II</head>
					<head type="sub">Que le sacrement de l’autel nous découvre une grande <lb></lb>bonté et un grand amour de Dieu.</head>
					<lg n="1">
						<l n="1" num="1.1">Je m’approche, seigneur, plein de la confiance</l>
						<l n="2" num="1.2">Que tu veux que je prenne en ta haute bonté :</l>
						<l n="3" num="1.3">Je m’approche en malade, avec impatience</l>
						<l n="4" num="1.4">De recevoir de toi la parfaite santé.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">Je cherche en altéré la fontaine de vie ;</l>
						<l n="6" num="2.2">Je cherche en affamé le pain vivifiant ;</l>
						<l n="7" num="2.3">Et c’est sur cet espoir que mon âme ravie</l>
						<l n="8" num="2.4">Au monarque du ciel présente un mendiant.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">Aux faveurs de son maître ainsi l’esclave espère,</l>
						<l n="10" num="3.2">Ainsi la créature aux dons du créateur ;</l>
						<l n="11" num="3.3">Ainsi le désolé cherche dans sa misère</l>
						<l n="12" num="3.4">Un doux refuge au sein de son consolateur.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">Qui peut m’avoir rendu ta bonté si propice,</l>
						<l n="14" num="4.2">Que jusqu’à moi, seigneur, il te plaise venir ?</l>
						<l n="15" num="4.3">Et qui suis-je après tout, que ton corps me nourrisse,</l>
						<l n="16" num="4.4">Qu’au mien en ce banquet tu le daignes unir ?</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">De quel front un pécheur devant toi comparoître ?</l>
						<l n="18" num="5.2">De quel front jusqu’à toi s’ose-t-il avancer ?</l>
						<l n="19" num="5.3">Comment le souffres-tu, toi, son juge et son maître ?</l>
						<l n="20" num="5.4">Et comment jusqu’à lui daignes-tu t’abaisser ?</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1">Ce n’est point avec toi qu’il faut que je raisonne :</l>
						<l n="22" num="6.2">Tu connois ma foiblesse et mon peu de ferveur,</l>
						<l n="23" num="6.3">Et tu sais que de moi je n’ai rien qui me donne</l>
						<l n="24" num="6.4">Aucun droit de prétendre une telle faveur.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1">Plus je contemple aussi l’excès de ma bassesse,</l>
						<l n="26" num="7.2">Plus j’admire aussitôt celui de ton amour :</l>
						<l n="27" num="7.3">J’adore ta pitié, je bénis ta largesse,</l>
						<l n="28" num="7.4">Je t’en veux rendre gloire et grâces nuit et jour.</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1">C’est par cette clémence, et non pour mes mérites,</l>
						<l n="30" num="8.2">Que tu fais à mes yeux luire ainsi ta bonté,</l>
						<l n="31" num="8.3">Pour faire croître en moi l’amour où tu m’invites,</l>
						<l n="32" num="8.4">Et mieux enraciner la vraie humilité.</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1">Puis donc que tu le veux, puisque tu le commandes,</l>
						<l n="34" num="9.2">J’ose me présenter au don que tu me fais ;</l>
						<l n="35" num="9.3">Et puissé-je ne mettre à des bontés si grandes</l>
						<l n="36" num="9.4">Aucun empêchement par mes lâches forfaits !</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1">Débonnaire Jésus, quelles sont les louanges,</l>
						<l n="38" num="10.2">Quels sont et les respects et les remercîments</l>
						<l n="39" num="10.3">Que te doivent nos cœurs pour ce vrai pain des anges</l>
						<l n="40" num="10.4">Que ta main nous prodigue en ces festins charmants ?</l>
					</lg>
					<lg n="11">
						<l n="41" num="11.1">Telle est la dignité de ce pain angélique,</l>
						<l n="42" num="11.2">Que son expression passe notre pouvoir,</l>
						<l n="43" num="11.3">Et nous voulons en vain que la bouche l’explique,</l>
						<l n="44" num="11.4">Lorsque l’entendement ne la peut concevoir.</l>
					</lg>
					<lg n="12">
						<l n="45" num="12.1">Mais que dois-je penser à cette table sainte ?</l>
						<l n="46" num="12.2">M’approchant de mon dieu, de quoi m’entretenir ?</l>
						<l n="47" num="12.3">J’y porte du respect, du zèle et de la crainte,</l>
						<l n="48" num="12.4">Et ne le puis assez respecter ni bénir.</l>
					</lg>
					<lg n="13">
						<l n="49" num="13.1">Je n’ai rien de meilleur ni de plus salutaire,</l>
						<l n="50" num="13.2">Que de m’humilier devant ta majesté,</l>
						<l n="51" num="13.3">Et de tenir l’œil bas sur toute ma misère,</l>
						<l n="52" num="13.4">Pour élever d’autant l’excès de ta bonté.</l>
					</lg>
					<lg n="14">
						<l n="53" num="14.1">Je te loue, ô mon Dieu, je t’exalte sans cesse ;</l>
						<l n="54" num="14.2">De mon propre mépris je me fais une loi,</l>
						<l n="55" num="14.3">Et je m’abîme au fond de toute ma bassesse,</l>
						<l n="56" num="14.4">Pour de tout mon pouvoir me ravaler sous toi.</l>
					</lg>
					<lg n="15">
						<l n="57" num="15.1">Toi, la pureté même, et moi, la même ordure,</l>
						<l n="58" num="15.2">Toi, le grand saint des saints, toi, leur unique roi,</l>
						<l n="59" num="15.3">Tu viens à cette indigne et vile créature</l>
						<l n="60" num="15.4">Qui ne mérite pas de porter l’œil sur toi !</l>
					</lg>
					<lg n="16">
						<l n="61" num="16.1">Tu viens jusques à moi pour loger en moi-même,</l>
						<l n="62" num="16.2">Tu m’invites toi-même à ces divins banquets,</l>
						<l n="63" num="16.3">Où la profusion de ton amour extrême</l>
						<l n="64" num="16.4">Sert un pain angélique et de célestes mets !</l>
					</lg>
					<lg n="17">
						<l n="65" num="17.1">Ce pain, ce mets sacré que tu nous y fais prendre,</l>
						<l n="66" num="17.2">C’est toi, c’est ton vrai corps, arbitre de mon sort,</l>
						<l n="67" num="17.3">Pain vivant, qui du ciel as bien voulu descendre</l>
						<l n="68" num="17.4">Pour redonner la vie aux enfants de la mort.</l>
					</lg>
					<lg n="18">
						<l n="69" num="18.1">Quels tendres soins pour nous ton amour fait paroître !</l>
						<l n="70" num="18.2">Que grande est la bonté dont part ce grand amour !</l>
						<l n="71" num="18.3">Que ta louange, ô Dieu ! Chaque jour en doit croître !</l>
						<l n="72" num="18.4">Que de remercîments on t’en doit chaque jour !</l>
					</lg>
					<lg n="19">
						<l n="73" num="19.1">Que tu pris un dessein utile et salutaire,</l>
						<l n="74" num="19.2">Quand tu te fis auteur de ce grand sacrement !</l>
						<l n="75" num="19.3">Et l’aimable festin qu’il te plut de nous faire,</l>
						<l n="76" num="19.4">Quand tu nous y donnas ton corps pour aliment !</l>
					</lg>
					<lg n="20">
						<l n="77" num="20.1">Qu’en cet effort d’amour tes œuvres admirables</l>
						<l n="78" num="20.2">Montrent de ta vertu le pouvoir éclatant !</l>
						<l n="79" num="20.3">Et que ces vérités sont pour nous ineffables,</l>
						<l n="80" num="20.4">Que ta voix exécute aussitôt qu’on l’entend !</l>
					</lg>
					<lg n="21">
						<l n="81" num="21.1">Ta parole jadis fit si tôt toutes choses,</l>
						<l n="82" num="21.2">Que rien n’en sépara le son d’avec l’effet ;</l>
						<l n="83" num="21.3">Et ta vertu passant dans les secondes causes,</l>
						<l n="84" num="21.4">À peine l’homme parle, et ton vouloir est fait.</l>
					</lg>
					<lg n="22">
						<l n="85" num="22.1">Chose étrange, et bien digne enfin que la foi vienne</l>
						<l n="86" num="22.2">Au secours de nos sens et de l’esprit humain,</l>
						<l n="87" num="22.3">Que l’espèce du vin tout entier te contienne,</l>
						<l n="88" num="22.4">Que tu sois tout entier sous l’espèce du pain !</l>
					</lg>
					<lg n="23">
						<l n="89" num="23.1">Tu fais de leur substance en toi-même un échange,</l>
						<l n="90" num="23.2">Tu les anéantis, et revêts leurs dehors ;</l>
						<l n="91" num="23.3">Et bien qu’à tous moments on te boive et te mange,</l>
						<l n="92" num="23.4">On ne consume point ni ton sang ni ton corps.</l>
					</lg>
					<lg n="24">
						<l n="93" num="24.1">Grand monarque du ciel, qui dans ce haut étage</l>
						<l n="94" num="24.2">N’as besoin de personne, et ne manques de rien,</l>
						<l n="95" num="24.3">Tu veux loger en nous, et faire un alliage,</l>
						<l n="96" num="24.4">Par ce grand sacrement, de notre sang au tien !</l>
					</lg>
					<lg n="25">
						<l n="97" num="25.1">Conserve donc mon cœur et tout mon corps sans tache,</l>
						<l n="98" num="25.2">Afin qu’un plein repos dans mon âme épandu,</l>
						<l n="99" num="25.3">À ce mystère saint un saint amour m’attache,</l>
						<l n="100" num="25.4">Et qu’à le célébrer je me rende assidu ;</l>
					</lg>
					<lg n="26">
						<l n="101" num="26.1">Que souvent je le puisse offrir en ta mémoire,</l>
						<l n="102" num="26.2">Comme de ta voix propre il t’a plu commander,</l>
						<l n="103" num="26.3">Et qu’après l’avoir pris pour ta plus grande gloire,</l>
						<l n="104" num="26.4">Au salut éternel il me puisse guider.</l>
					</lg>
					<lg n="27">
						<l n="105" num="27.1">Par des transports de joie et de reconnoissance,</l>
						<l n="106" num="27.2">Bénis ton Dieu, mon âme, en ce val de malheurs,</l>
						<l n="107" num="27.3">Où tu reçois ainsi de sa toute-puissance</l>
						<l n="108" num="27.4">Un don si favorable à consoler tes pleurs.</l>
					</lg>
					<lg n="28">
						<l n="109" num="28.1">Sais-tu qu’autant de fois que ton zèle s’élève</l>
						<l n="110" num="28.2">À prendre du sauveur le véritable corps,</l>
						<l n="111" num="28.3">L’œuvre de ton salut autant de fois s’achève,</l>
						<l n="112" num="28.4">Et de tous ses tourments t’applique les trésors ?</l>
					</lg>
					<lg n="29">
						<l n="113" num="29.1">Il n’a rien mérité qu’il ne t’y communique ;</l>
						<l n="114" num="29.2">Et comme son amour ne peut rien refuser,</l>
						<l n="115" num="29.3">Sa bonté toujours pleine et toujours magnifique</l>
						<l n="116" num="29.4">Est un vaste océan qu’on ne peut épuiser.</l>
					</lg>
					<lg n="30">
						<l n="117" num="30.1">Portes-y de ta part l’attention sévère</l>
						<l n="118" num="30.2">D’un cœur renouvelé pour s’y mieux préparer,</l>
						<l n="119" num="30.3">Et pèse mûrement la grandeur d’un mystère</l>
						<l n="120" num="30.4">Dont dépend ton salut que tu vas opérer.</l>
					</lg>
					<lg n="31">
						<l n="121" num="31.1">Lorsque ta propre main offre cette victime,</l>
						<l n="122" num="31.2">Quand tu la vois offrir par un autre à l’autel,</l>
						<l n="123" num="31.3">Tout doit être pour toi surprenant, doux, sublime,</l>
						<l n="124" num="31.4">Comme si de nouveau Dieu se faisoit mortel.</l>
					</lg>
					<lg n="32">
						<l n="125" num="32.1">Oui, tout t’y doit sembler aussi grand, aussi rare</l>
						<l n="126" num="32.2">Que si ce jour-là même il naissoit ici-bas,</l>
						<l n="127" num="32.3">Ou que la cruauté d’une troupe barbare</l>
						<l n="128" num="32.4">Pour le salut de tous le livrât au trépas.</l>
					</lg>
				</div></body></text></TEI>