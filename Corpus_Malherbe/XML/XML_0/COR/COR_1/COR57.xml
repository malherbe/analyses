<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Imitation De Jésus-Christ</title>
				<title type="medium">Édition électronique</title>
				<author key="COR">
					<name>
						<forename>Pierre</forename>
						<surname>CORNEILLE</surname>
					</name>
					<date from="1606" to="1684">1606-1684</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>13205 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2016</date>
				<idno type="local">COR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Imitation De Jésus-Christ</title>
						<author>Pierre Corneille</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/pierrecorneilleimmitadiondejesuschrist.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Imitation De Jésus-Christ</title>
						<author>Pierre Corneille</author>
						<editor>M. Ch. Marty-Laveaux</editor>
						<edition>nouvelle édition</edition>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k51199.r=pierre%20corneille?rk=85837;2</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie de L. Hachette et Cie</publisher>
							<date when="1862">1862</date>
						</imprint>
						<biblScope unit="tome">Tome 8</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1656">1656</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les chiffres arabes des livres et chapitres ont été remplacés par des chiffres romains</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">LIVRE PREMIER</head><div type="poem" key="COR57">
					<head type="main">CHAPITRE XXV</head>
					<head type="sub">Du fervent amendement de toute la vie.</head>
					<lg n="1">
						<l n="1" num="1.1">De ton zèle envers Dieu bannis la nonchalance ;</l>
						<l n="2" num="1.2">Porte un amour actif dans un cœur enflammé ;</l>
						<l n="3" num="1.3">Souviens-toi que le cloître où tu t’es enfermé</l>
						<l n="4" num="1.4">Veut de l’intérieur et de la vigilance ;</l>
						<l n="5" num="1.5">Demande souvent compte au secret de ton cœur</l>
						<l n="6" num="1.6">Du dessein qui t’en fit épouser la rigueur,</l>
						<l n="7" num="1.7">Et renoncer au siècle, à sa pompe, à ses charmes ;</l>
						<l n="8" num="1.8">N’étoit-ce pas pour vivre à Dieu seul attaché,</l>
						<l n="9" num="1.9">Pour embrasser la croix, pour la baigner de larmes,</l>
						<l n="10" num="1.10">Et t’épurer l’esprit dans l’horreur du péché ?</l>
					</lg>
					<lg n="2">
						<l n="11" num="2.1">Montre en ce grand dessein une ferveur constante,</l>
						<l n="12" num="2.2">Et pour un saint progrès rends ce cœur tout de feu ;</l>
						<l n="13" num="2.3">Ta récompense est proche, elle est grande, et dans peu</l>
						<l n="14" num="2.4">Son excès surprenant passera ton attente.</l>
						<l n="15" num="2.5">À tes moindres souhaits tu verras lors s’offrir,</l>
						<l n="16" num="2.6">Non plus de quoi trembler, non plus de quoi souffrir,</l>
						<l n="17" num="2.7">Mais du solide bien l’heureuse plénitude :</l>
						<l n="18" num="2.8">Tes yeux admireront son immense valeur ;</l>
						<l n="19" num="2.9">Tu l’obtiendras sans peine et sans inquiétude,</l>
						<l n="20" num="2.10">Et la posséderas sans crainte et sans douleur.</l>
					</lg>
					<lg n="3">
						<l n="21" num="3.1">Ne dors pas cependant, prends courage, et l’emploie</l>
						<l n="22" num="3.2">Aux précieux effets d’un vertueux propos :</l>
						<l n="23" num="3.3">D’une heure de travail doit naître un long repos,</l>
						<l n="24" num="3.4">D’un moment de souffrance une éternelle joie.</l>
						<l n="25" num="3.5">C’est Dieu qui te promet cette félicité :</l>
						<l n="26" num="3.6">Si tu sais le servir avec fidélité,</l>
						<l n="27" num="3.7">Il sera comme toi fidèle en ses promesses ;</l>
						<l n="28" num="3.8">Sa main quand tu combats cherche à te couronner,</l>
						<l n="29" num="3.9">Et sa profusion, égale à ses richesses,</l>
						<l n="30" num="3.10">Ne voit tous ses trésors que pour te les donner.</l>
					</lg>
					<lg n="4">
						<l n="31" num="4.1">Conçois, il t’en avoue, une haute espérance</l>
						<l n="32" num="4.2">De remporter la palme en combattant sous lui ;</l>
						<l n="33" num="4.3">Espère un plein triomphe avec un tel appui ;</l>
						<l n="34" num="4.4">Mais garde-toi d’en prendre une entière assurance.</l>
						<l n="35" num="4.5">Les philtres dangereux de cette illusion</l>
						<l n="36" num="4.6">Charment si puissamment, que dans l’occasion</l>
						<l n="37" num="4.7">Nous laissons de nos mains échapper la victoire ;</l>
						<l n="38" num="4.8">Et quand le souvenir d’avoir le mieux vécu</l>
						<l n="39" num="4.9">Relâche la ferveur à quelque vaine gloire,</l>
						<l n="40" num="4.10">Qui s’assure de vaincre est aisément vaincu.</l>
					</lg>
					<lg n="5">
						<l n="41" num="5.1">Un jour un grand dévot, dont l’âme, encor que sainte,</l>
						<l n="42" num="5.2">Flottoit dans une longue et triste anxiété,</l>
						<l n="43" num="5.3">Et tournoit sans repos son instabilité</l>
						<l n="44" num="5.4">Tantôt vers l’espérance, et tantôt vers la crainte,</l>
						<l n="45" num="5.5">Accablé sous le poids de cet ennui mortel,</l>
						<l n="46" num="5.6">Prosterné dans l’église au devant d’un autel,</l>
						<l n="47" num="5.7">Rouloit cette inquiète et timide pensée :</l>
						<l n="48" num="5.8">« Ô dieu ! Si je savois, disoit-il en son cœur,</l>
						<l n="49" num="5.9">Qu’enfin ma lâcheté, par mes pleurs effacée,</l>
						<l n="50" num="5.10">De bien persévérer me laissât la vigueur ! »</l>
					</lg>
					<lg n="6">
						<l n="51" num="6.1">Une céleste voix de lui seul entendue</l>
						<l n="52" num="6.2">À sa douleur secrète aussitôt répondit,</l>
						<l n="53" num="6.3">Et par un doux oracle à l’instant lui rendit</l>
						<l n="54" num="6.4">Le calme qui manquoit à son âme éperdue :</l>
						<l n="55" num="6.5">« Eh bien ! Que ferois-tu ? Dit cette aimable voix.</l>
						<l n="56" num="6.6">Montre la même ardeur que si tu le savois,</l>
						<l n="57" num="6.7">Et fais dès maintenant ce que tu voudrois faire ;</l>
						<l n="58" num="6.8">Commence, continue, et ne perds point de temps ;</l>
						<l n="59" num="6.9">Applique tous tes soins à m’aimer, à me plaire,</l>
						<l n="60" num="6.10">Et demeure assuré de ce que tu prétends. »</l>
					</lg>
					<lg n="7">
						<l n="61" num="7.1">Ainsi Dieu conforta cette âme désolée ;</l>
						<l n="62" num="7.2">Cette âme en crut ainsi la divine bonté,</l>
						<l n="63" num="7.3">Et soudain vit céder à la tranquillité</l>
						<l n="64" num="7.4">Les agitations qui l’avoient ébranlée ;</l>
						<l n="65" num="7.5">Un parfait abandon au souverain vouloir</l>
						<l n="66" num="7.6">Dans l’avenir obscur ne chercha plus à voir</l>
						<l n="67" num="7.7">Que les moyens de plaire à l’auteur de sa joie ;</l>
						<l n="68" num="7.8">Un bon commencement fit son ambition,</l>
						<l n="69" num="7.9">Et son unique soin fut de prendre la voie</l>
						<l n="70" num="7.10">Qui pût conduire l’œuvre à sa perfection.</l>
					</lg>
					<lg n="8">
						<l n="71" num="8.1">Espère, espère en Dieu, fais du bien sur la terre,</l>
						<l n="72" num="8.2">Tu recevras du ciel l’abondance des biens :</l>
						<l n="73" num="8.3">C’est par là que David t’enseigne les moyens</l>
						<l n="74" num="8.4">De te rendre vainqueur en cette rude guerre.</l>
						<l n="75" num="8.5">Une chose, il est vrai, fait souvent balancer,</l>
						<l n="76" num="8.6">Attiédit en plusieurs l’ardeur de s’avancer,</l>
						<l n="77" num="8.7">Et dès le premier pas les retire en arrière :</l>
						<l n="78" num="8.8">C’est que leur cœur, sensible encore aux voluptés,</l>
						<l n="79" num="8.9">Ne s’ouvre qu’en tremblant cette rude carrière,</l>
						<l n="80" num="8.10">Tant il conçoit d’horreur de ses difficultés.</l>
					</lg>
					<lg n="9">
						<l n="81" num="9.1">L’objet de cette horreur te doit servir d’amorce,</l>
						<l n="82" num="9.2">La grandeur des travaux ennoblit le combat,</l>
						<l n="83" num="9.3">Et la gloire de vaincre a d’autant plus d’éclat</l>
						<l n="84" num="9.4">Que pour y parvenir on fait voir plus de force.</l>
						<l n="85" num="9.5">L’homme qui porte en soi son plus grand ennemi,</l>
						<l n="86" num="9.6">Plus, à se bien haïr saintement affermi,</l>
						<l n="87" num="9.7">Il trouve en l’amour-propre une âpre résistance,</l>
						<l n="88" num="9.8">Plus il a de mérite à se dompter partout ;</l>
						<l n="89" num="9.9">Et la grâce, que Dieu mesure à sa constance,</l>
						<l n="90" num="9.10">D’autant plus dignement l’en fait venir à bout.</l>
					</lg>
					<lg n="10">
						<l n="91" num="10.1">Tous n’ont pas toutefois mêmes efforts à faire,</l>
						<l n="92" num="10.2">Comme ils n’ont pas en eux à vaincre également,</l>
						<l n="93" num="10.3">Et la diversité de leur tempérament</l>
						<l n="94" num="10.4">Leur donne un plus puissant ou plus foible adversaire ;</l>
						<l n="95" num="10.5">Mais un esprit ardent aux saintes fonctions,</l>
						<l n="96" num="10.6">Quoiqu’il ait à forcer beaucoup de passions,</l>
						<l n="97" num="10.7">Tout chargé d’ennemis, fera plus de miracles</l>
						<l n="98" num="10.8">Qu’un naturel bénin, doux, facile, arrêté,</l>
						<l n="99" num="10.9">Qui ne ressentant point en soi de grands obstacles,</l>
						<l n="100" num="10.10">S’enveloppe et s’endort dans sa tranquillité.</l>
					</lg>
					<lg n="11">
						<l n="101" num="11.1">Agis donc fortement, et fais-toi violence</l>
						<l n="102" num="11.2">Pour te soustraire au mal où tu te vois pencher ;</l>
						<l n="103" num="11.3">Examine quel bien tu dois le plus chercher,</l>
						<l n="104" num="11.4">Et portes-y soudain toute ta vigilance ;</l>
						<l n="105" num="11.5">Mais ne crois pas en toi le voir jamais assez :</l>
						<l n="106" num="11.6">Tes sens à te flatter toujours intéressés</l>
						<l n="107" num="11.7">T’en pourroient souvent faire une fausse peinture</l>
						<l n="108" num="11.8">Porte les yeux plus loin, et regarde en autrui</l>
						<l n="109" num="11.9">Tout ce qui t’y déplaît, tout ce qu’on y censure,</l>
						<l n="110" num="11.10">Et déracine en toi ce qui te choque en lui.</l>
					</lg>
					<lg n="12">
						<l n="111" num="12.1">Dans ce miroir fidèle exactement contemple</l>
						<l n="112" num="12.2">Ce que sont en effet et ce mal et ce bien ;</l>
						<l n="113" num="12.3">Et les considérant d’un œil vraiment chrétien,</l>
						<l n="114" num="12.4">Fais ton profit du bon et du mauvais exemple :</l>
						<l n="115" num="12.5">Que l’un allume en toi l’ardeur de l’imiter,</l>
						<l n="116" num="12.6">Que l’autre excite en toi les soins de l’éviter,</l>
						<l n="117" num="12.7">Ou, si tu l’as suivi, d’en effacer la tache ;</l>
						<l n="118" num="12.8">Sers toi-même d’exemple, et t’en fais une loi,</l>
						<l n="119" num="12.9">Puisque ainsi que ton œil sur les autres s’attache,</l>
						<l n="120" num="12.10">Les autres à leur tour attachent l’œil sur toi.</l>
					</lg>
					<lg n="13">
						<l n="121" num="13.1">Oh ! Qu’il est doux de voir une ferveur divine</l>
						<l n="122" num="13.2">Dans les religieux nourrir la sainteté !</l>
						<l n="123" num="13.3">Qu’on admire avec joie en eux la fermeté</l>
						<l n="124" num="13.4">Et de l’obéissance et de la discipline !</l>
						<l n="125" num="13.5">Qu’il est dur au contraire et scandaleux d’en voir</l>
						<l n="126" num="13.6">S’égarer chaque jour du cloître et du devoir,</l>
						<l n="127" num="13.7">Divaguer en désordre, et s’empresser d’affaires,</l>
						<l n="128" num="13.8">Désavouer l’habit par l’inclination,</l>
						<l n="129" num="13.9">Et pour des embarras un peu trop volontaires</l>
						<l n="130" num="13.10">Négliger les emplois de leur vocation !</l>
					</lg>
					<lg n="14">
						<l n="131" num="14.1">Souviens-toi de tes vœux, et pense à quoi t’engage</l>
						<l n="132" num="14.2">Ce vertueux projet dont ton âme a fait choix ;</l>
						<l n="133" num="14.3">Mets-toi devant les yeux un Jésus-Christ en croix,</l>
						<l n="134" num="14.4">Et jusques en ton cœur fais-en passer l’image :</l>
						<l n="135" num="14.5">À l’aspect amoureux de ce mourant sauveur</l>
						<l n="136" num="14.6">Combien dois-tu rougir de ton peu de ferveur,</l>
						<l n="137" num="14.7">Et du peu de rapport de ta vie à sa vie !</l>
						<l n="138" num="14.8">Et quand il te dira : « je t’appelois aux cieux,</l>
						<l n="139" num="14.9">Je t’ai mis en la voie, et tu l’as mal suivie, »</l>
						<l n="140" num="14.10">Combien doivent couler de larmes de tes yeux !</l>
					</lg>
					<lg n="15">
						<l n="141" num="15.1">Oh ! Qu’un religieux heureusement s’exerce</l>
						<l n="142" num="15.2">Sur cette illustre vie et cette indigne mort !</l>
						<l n="143" num="15.3">Que tout ce qui peut faire ici-bas un doux sort</l>
						<l n="144" num="15.4">Se trouve abondamment dans ce divin commerce !</l>
						<l n="145" num="15.5">Qu’avec peu de raison il chercheroit ailleurs</l>
						<l n="146" num="15.6">Des secours plus puissants, ou des emplois meilleurs !</l>
						<l n="147" num="15.7">Qu’avec pleine clarté la grâce l’illumine !</l>
						<l n="148" num="15.8">Que son intérieur en est fortifié,</l>
						<l n="149" num="15.9">Et se fait promptement une haute doctrine</l>
						<l n="150" num="15.10">Quand il grave en son cœur un dieu crucifié !</l>
					</lg>
					<lg n="16">
						<l n="151" num="16.1">Sa paix est toujours ferme, et quoi qu’on lui commande,</l>
						<l n="152" num="16.2">Il s’y porte avec joie et court avec chaleur ;</l>
						<l n="153" num="16.3">Mais le tiède au contraire a douleur sur douleur,</l>
						<l n="154" num="16.4">Et voit fondre sur lui tout ce qu’il appréhende :</l>
						<l n="155" num="16.5">L’angoisse, le chagrin, les contrariétés,</l>
						<l n="156" num="16.6">Dans son cœur inquiet tombant de tous côtés,</l>
						<l n="157" num="16.7">Lui donnent les ennuis et le trouble en partage ;</l>
						<l n="158" num="16.8">Il demeure accablé sous leurs moindres efforts,</l>
						<l n="159" num="16.9">Parce que le dedans n’a rien qui le soulage,</l>
						<l n="160" num="16.10">Et qu’il n’ose ou ne peut en chercher au dehors.</l>
					</lg>
					<lg n="17">
						<l n="161" num="17.1">Oui, le religieux qui hait la discipline,</l>
						<l n="162" num="17.2">Qu’importune la règle, à qui pèse l’habit,</l>
						<l n="163" num="17.3">Qui par ses actions chaque jour les dédit,</l>
						<l n="164" num="17.4">Se jette en grand péril d’une prompte ruine.</l>
						<l n="165" num="17.5">Qui cherche à vivre au large est toujours à l’étroit :</l>
						<l n="166" num="17.6">Dans ce honteux dessein son esprit maladroit</l>
						<l n="167" num="17.7">Se gêne d’autant plus qu’il se croit satisfaire ;</l>
						<l n="168" num="17.8">Et quoi que de sa règle il ose relâcher,</l>
						<l n="169" num="17.9">Le reste n’a jamais si bien de quoi lui plaire</l>
						<l n="170" num="17.10">Que ses nouveaux dégoûts n’en veuillent retrancher.</l>
					</lg>
					<lg n="18">
						<l n="171" num="18.1">Si ton cœur pour le cloître a de la répugnance</l>
						<l n="172" num="18.2">Jusqu’à grossir l’orgueil de tes sens révoltés,</l>
						<l n="173" num="18.3">Regarde ce que font tant d’autres mieux domptés,</l>
						<l n="174" num="18.4">Jusqu’où va leur étroite et fidèle observance :</l>
						<l n="175" num="18.5">Ils vivent retirés et sortent rarement,</l>
						<l n="176" num="18.6">Grossièrement vêtus et nourris pauvrement,</l>
						<l n="177" num="18.7">Travaillent sans relâche ainsi que sans murmure,</l>
						<l n="178" num="18.8">Parlent peu, dorment peu, se lèvent du matin,</l>
						<l n="179" num="18.9">Prolongent l’oraison, prolongent la lecture,</l>
						<l n="180" num="18.10">Et sous ces dures lois font une douce fin.</l>
					</lg>
					<lg n="19">
						<l n="181" num="19.1">Vois ces grands escadrons d’âmes laborieuses,</l>
						<l n="182" num="19.2">Vois l’ordre des chartreux, vois celui de Cîteaux,</l>
						<l n="183" num="19.3">Vois tout autour de toi mille sacrés troupeaux</l>
						<l n="184" num="19.4">Et de religieux et de religieuses ;</l>
						<l n="185" num="19.5">Vois comme chaque nuit ils rompent le sommeil,</l>
						<l n="186" num="19.6">Et n’attendent jamais le retour du soleil</l>
						<l n="187" num="19.7">Pour envoyer à Dieu l’encens de ses louanges :</l>
						<l n="188" num="19.8">Il te seroit honteux d’avoir quelque lenteur,</l>
						<l n="189" num="19.9">Alors que sur la terre un si grand nombre d’anges</l>
						<l n="190" num="19.10">S’unit à ceux du ciel pour bénir leur auteur.</l>
					</lg>
					<lg n="20">
						<l n="191" num="20.1">Oh ! Si nous pouvions vivre et n’avoir rien à faire</l>
						<l n="192" num="20.2">Qu’à dissiper en nous cette infâme langueur,</l>
						<l n="193" num="20.3">Qu’à louer ce grand maître et de bouche et de cœur,</l>
						<l n="194" num="20.4">Sans que rien de plus bas nous devînt nécessaire !</l>
						<l n="195" num="20.5">Oh ! Si l’âme chrétienne et ses plus saints transports</l>
						<l n="196" num="20.6">N’étoient point asservis aux foiblesses du corps,</l>
						<l n="197" num="20.7">Aux besoins de dormir, de manger et de boire !</l>
						<l n="198" num="20.8">Si rien n’interrompoit un soin continuel</l>
						<l n="199" num="20.9">De publier de Dieu les bontés et la gloire,</l>
						<l n="200" num="20.10">Et d’avancer l’esprit dans le spirituel !</l>
					</lg>
					<lg n="21">
						<l n="201" num="21.1">Que nous serions heureux ! Qu’un an, un jour, une heure,</l>
						<l n="202" num="21.2">Nous feroit bien goûter plus de félicité</l>
						<l n="203" num="21.3">Que les siècles entiers de la captivité</l>
						<l n="204" num="21.4">Où nous réduit la chair dans sa triste demeure !</l>
						<l n="205" num="21.5">Ô dieu, pourquoi faut-il que ces infirmités,</l>
						<l n="206" num="21.6">Ces journaliers tributs, soient des nécessités</l>
						<l n="207" num="21.7">Pour tes vivants portraits qu’illumine ta flamme ?</l>
						<l n="208" num="21.8">Pourquoi pour subsister sur ce lourd élément</l>
						<l n="209" num="21.9">Faut-il d’autres repas que les repas de l’âme ?</l>
						<l n="210" num="21.10">Pourquoi les goûtons-nous, ô dieu, si rarement ?</l>
					</lg>
					<lg n="22">
						<l n="211" num="22.1">Quand l’homme se possède, et que les créatures</l>
						<l n="212" num="22.2">N’ont aucunes douceurs qui puissent l’arrêter,</l>
						<l n="213" num="22.3">C’est alors que sans peine il commence à goûter</l>
						<l n="214" num="22.4">Combien le créateur est doux aux âmes pures.</l>
						<l n="215" num="22.5">Alors, quoi qu’il arrive ou de bien ou de mal,</l>
						<l n="216" num="22.6">Il vit toujours content, et d’un visage égal</l>
						<l n="217" num="22.7">Il reçoit la mauvaise et la bonne fortune :</l>
						<l n="218" num="22.8">L’abondance sur lui tombe sans l’émouvoir,</l>
						<l n="219" num="22.9">La pauvreté pour lui n’est jamais importune,</l>
						<l n="220" num="22.10">La gloire et le mépris n’ont qu’un même pouvoir.</l>
					</lg>
					<lg n="23">
						<l n="221" num="23.1">C’est lors entièrement en Dieu qu’il se repose,</l>
						<l n="222" num="23.2">En Dieu, sa confiance et son unique appui,</l>
						<l n="223" num="23.3">En Dieu, qu’il voit partout, en soi-même, en autrui,</l>
						<l n="224" num="23.4">En Dieu, qui pour son âme est tout en toute chose.</l>
						<l n="225" num="23.5">Où qu’il soit, quoi qu’il fasse, il redoute, il chérit</l>
						<l n="226" num="23.6">Cet être universel à qui rien ne périt,</l>
						<l n="227" num="23.7">Et dans qui tout conserve une immortelle vie,</l>
						<l n="228" num="23.8">Qui ne connoît jamais diversité de temps,</l>
						<l n="229" num="23.9">Et dont la voix sitôt de l’effet est suivie</l>
						<l n="230" num="23.10">Que dire et faire en lui ne sont point deux instants.</l>
					</lg>
					<lg n="24">
						<l n="231" num="24.1">Toi qui, bien que mortel, inconstant, misérable,</l>
						<l n="232" num="24.2">Peux avec son secours aisément te sauver,</l>
						<l n="233" num="24.3">Souviens-toi de la fin où tu dois arriver,</l>
						<l n="234" num="24.4">Et que le temps perdu n’est jamais réparable.</l>
						<l n="235" num="24.5">Va, cours, vole sans cesse aux emplois fructueux :</l>
						<l n="236" num="24.6">Cette sainte chaleur qui fait les vertueux</l>
						<l n="237" num="24.7">Veut des soins assidus et de la diligence ;</l>
						<l n="238" num="24.8">Et du moment fatal que ton manque d’ardeur</l>
						<l n="239" num="24.9">T’osera relâcher à quelque négligence,</l>
						<l n="240" num="24.10">Mille peines suivront ce moment de tiédeur.</l>
					</lg>
					<lg n="25">
						<l n="241" num="25.1">Que si dans un beau feu ton âme persévère,</l>
						<l n="242" num="25.2">Tu n’auras plus à craindre aucun funeste assaut,</l>
						<l n="243" num="25.3">Et l’amour des vertus joint aux grâces d’en haut</l>
						<l n="244" num="25.4">Rendra de jour en jour ta peine plus légère.</l>
						<l n="245" num="25.5">Le zèle et la ferveur peuvent nous préparer</l>
						<l n="246" num="25.6">À quoi qu’en cette vie il nous faille endurer :</l>
						<l n="247" num="25.7">Ils sèment des douceurs au milieu des supplices ;</l>
						<l n="248" num="25.8">Mais ne t’y trompe pas, il faut d’autres efforts,</l>
						<l n="249" num="25.9">Il en faut de plus grands à résister aux vices,</l>
						<l n="250" num="25.10">À se dompter l’esprit, qu’à se gêner le corps.</l>
					</lg>
					<lg n="26">
						<l n="251" num="26.1">L’âme aux petits défauts souvent abandonnée</l>
						<l n="252" num="26.2">En de plus dangereux se laisse bientôt choir,</l>
						<l n="253" num="26.3">Et la parfaite joie arrive avec le soir</l>
						<l n="254" num="26.4">Chez qui sait avec fruit employer la journée.</l>
						<l n="255" num="26.5">Veille donc sur toi-même et sur tes appétits,</l>
						<l n="256" num="26.6">Excite, échauffe-toi toi-même, et t’avertis ;</l>
						<l n="257" num="26.7">Quoi qu’il en soit d’autrui, jamais ne te néglige ;</l>
						<l n="258" num="26.8">Gêne-toi, force-toi, change de bien en mieux ;</l>
						<l n="259" num="26.9">Plus se fait violence un cœur qui se corrige,</l>
						<l n="260" num="26.10">Plus son progrès va haut dans la route des cieux.</l>
					</lg>
				</div></body></text></TEI>