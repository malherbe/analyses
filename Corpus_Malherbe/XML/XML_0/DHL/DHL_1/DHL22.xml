<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Poésies de Madame Deshoulières</title>
				<title type="medium">Édition électronique</title>
				<author key="DHL">
					<name>
						<forename>Antoinette</forename>
						<surname>DESHOULIÈRES</surname>
					</name>
					<date from="1638" to="1694">1638-1694</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4026 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">DHL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Madame Deshoulières</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">https://fr.wikisource.org/wiki/Po%C3%A9sies_de_Madame_Deshouli%C3%A8res</idno>
						<p>Exporté de Wikisource le 24 juillet 2022</p>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies</title>
								<author>Antoinette Deshoulières</author>
								<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k886829s</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>THÉOPHILE BERQUET, LIBRAIRE</publisher>
									<date when="1824">1824</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres de madame et de mademoiselle Deshoulieres</title>
						<author>Antoinette Deshoulières</author>
						<idno type="URL">https://archive.org/details/oeuvresdemadamee01deshuoft/page/n5/mode/2up</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher> Libraires associés</publisher>
							<date when="1768">1768</date>
						</imprint>
						<biblScope unit="tome">I</biblScope>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres de madame et de mademoiselle Deshoulieres</title>
						<author>Antoinette Deshoulières</author>
						<idno type="URL">https://archive.org/details/oeuvresdemadamee02deshuoft/page/n5/mode/2up</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher> Libraires associés</publisher>
							<date when="1768">1768</date>
						</imprint>
						<biblScope unit="tome">II</biblScope>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1688">1688</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>

			<samplingDecl>
				<p>La présente édition électronique est issue d’une exportation à partir de Wikisource.org (Exporté de Wikisource le 26 juillet 2022).</p>
				<p>La partie "Autobiographie" n’est pas incluse dans la présente édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les notes de bas de page ont été reportées en fin de poème.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">ODE</head><div type="poem" key="DHL22">
					<p>
						le soin que le Roi prend de l’éducation de sa noblesse dans ses
						places et dans Saint-Cyr, laquelle remporta le prix à l’Académie
						française.1687.
					</p>
					<lg n="1">
						<l n="1" num="1.1">Toi par qui les mortels rendent leurs noms célèbres,</l>
						<l n="2" num="1.2">Toi que j’invoque ici pour la première fois,</l>
						<l n="3" num="1.3">De mon esprit confus dissipe les ténèbres,</l>
						<l n="4" num="1.4"><space unit="char" quantity="8"/>Et soutiens ma timide voix.</l>
						<l n="5" num="1.5">Le projet que je fais est hardi, je l’avoue ;</l>
						<l n="6" num="1.6">Il aurait effrayé le pasteur de Mantoue,</l>
						<l n="7" num="1.7"><space unit="char" quantity="8"/>Et j’en connais tout le danger.</l>
						<l n="8" num="1.8">Mais, Apollon, par toi si je suis inspirée,</l>
						<l n="9" num="1.9">Mes vers pourront des siens égaler la durée :</l>
						<l n="10" num="1.10"><space unit="char" quantity="8"/>Hâte-toi, viens m’encourager.</l>
					</lg>
					<lg n="2">
						<l n="11" num="2.1">Dieu du jour, tu me dois le secours que j’implore :</l>
						<l n="12" num="2.2">C’est ce héros si grand, si craint dans l’univers,</l>
						<l n="13" num="2.3">Le protecteur des arts, LOUIS, que l’on adore,</l>
						<l n="14" num="2.4"><space unit="char" quantity="8"/>Que je veux chanter dans mes vers.</l>
						<l n="15" num="2.5">Depuis que chaque jour tu sors du sein de l’onde,</l>
						<l n="16" num="2.6">Tu n’as rien vu d’égal dans l’un et l’autre monde,</l>
						<l n="17" num="2.7"><space unit="char" quantity="8"/>Ni si digne du soin des dieux,</l>
						<l n="18" num="2.8">C’est peu pour en parler qu’un langage ordinaire ;</l>
						<l n="19" num="2.9">Et pour le bien louer ce n’est point assez faire,</l>
						<l n="20" num="2.10"><space unit="char" quantity="8"/>Dès que l’on pourra faire mieux.</l>
					</lg>
					<lg n="3">
						<l n="21" num="3.1">Il sait que triompher des erreurs et des vices,</l>
						<l n="22" num="3.2">Répandre la terreur du Gange aux flots glacés,</l>
						<l n="23" num="3.3">Élever en tous lieux de pompeux édifices,</l>
						<l n="24" num="3.4"><space unit="char" quantity="8"/>Pour un grand Roi n’est pas assez :</l>
						<l n="25" num="3.5">Qu’il faut, pour bien remplir ce sacré caractère,</l>
						<l n="26" num="3.6">Qu’au dessein d’arracher son peuple à la misère</l>
						<l n="27" num="3.7"><space unit="char" quantity="8"/>Cèdent tous ses autres projets ;</l>
						<l n="28" num="3.8">Et que, quelque fierté que le trône demande,</l>
						<l n="29" num="3.9">Il faut à tous momens que sa bonté le rende</l>
						<l n="30" num="3.10"><space unit="char" quantity="8"/>Le père de tous ses sujets.</l>
					</lg>
					<lg n="4">
						<l n="31" num="4.1">À peine a-t-il calmé les troubles de la terre,</l>
						<l n="32" num="4.2">Que ce sage héros consulte avec la paix</l>
						<l n="33" num="4.3">Les moyens d’effacer les troubles de la guerre</l>
						<l n="34" num="4.4"><space unit="char" quantity="8"/>Par de mémorables bienfaits.</l>
						<l n="35" num="4.5">Il dérobe les cœurs de sa jeune noblesse</l>
						<l n="36" num="4.6">Aux funestes appas d’une indigne mollesse,</l>
						<l n="37" num="4.7"><space unit="char" quantity="8"/>Compagne d’un trop long repos.</l>
						<l n="38" num="4.8">France, quels soins pour toi prend ton auguste maître</l>
						<l n="39" num="4.9">Ils s’en vont pour jamais dans ton sein faire croître</l>
						<l n="40" num="4.10"><space unit="char" quantity="8"/>Un nombre infini de héros.</l>
					</lg>
					<lg n="5">
						<l n="41" num="5.1">Il établit pour eux des écoles savantes</l>
						<l n="42" num="5.2">Où l’on règle à la fois le courage et les mœurs,</l>
						<l n="43" num="5.3">D’où l’on les fait entrer dans ces routes brillantes</l>
						<l n="44" num="5.4"><space unit="char" quantity="8"/>Qui mènent aux plus grands honneurs.</l>
						<l n="45" num="5.5">On leur enseigne l’art de forcer les murailles,</l>
						<l n="46" num="5.6">De bien asseoir un camp, de gagner des batailles,</l>
						<l n="47" num="5.7"><space unit="char" quantity="8"/>Et de défendre des remparts.</l>
						<l n="48" num="5.8">Dignes de commander au sortir de l’enfance,</l>
						<l n="49" num="5.9">Ils verront la Victoire, attachée à la France,</l>
						<l n="50" num="5.10"><space unit="char" quantity="8"/>Ne suivre que ses étendards.</l>
					</lg>
					<lg n="6">
						<l n="51" num="6.1">Tel cet Être infini dont LOUIS est l’image,</l>
						<l n="52" num="6.2">Par les secrets ressorts d’un pouvoir absolu,</l>
						<l n="53" num="6.3">Des différens périls où la misère engage</l>
						<l n="54" num="6.4"><space unit="char" quantity="8"/>Sut délivrer son peuple élu.</l>
						<l n="55" num="6.5">Long-temps dans un désert, sous de fidèles guides,</l>
						<l n="56" num="6.6">Il conduisit ses pas vers les vertus solides,</l>
						<l n="57" num="6.7"><space unit="char" quantity="8"/>Source des grandes actions,</l>
						<l n="58" num="6.8">Et, quand il eut acquis de parfaites lumières,</l>
						<l n="59" num="6.9">Il lui fit subjuguer des nations entières,</l>
						<l n="60" num="6.10"><space unit="char" quantity="8"/>Terreur des autres nations.</l>
					</lg>
					<lg n="7">
						<l n="61" num="7.1">Mais c’est peu pour LOUIS d’élever dans ses places</l>
						<l n="62" num="7.2">Les fils de tant de vieux et fidèles guerriers</l>
						<l n="63" num="7.3">Qui, dans les champs de Mars, en marchant sur ses traces</l>
						<l n="64" num="7.4"><space unit="char" quantity="8"/>Ont fait des moissons de lauriers.</l>
						<l n="65" num="7.5">Pour leurs filles il montre autant de prévoyance</l>
						<l n="66" num="7.6">Dans l’asile sacré qu’il donne à l’innocence</l>
						<l n="67" num="7.7"><space unit="char" quantity="8"/>Contre tout ce qui la détruit :</l>
						<l n="68" num="7.8">Et par les soins pieux d’une illustre personne</l>
						<l n="69" num="7.9">Que le sort outragea, que la vertu couronne,</l>
						<l n="70" num="7.10"><space unit="char" quantity="8"/>Un si beau dessein fut conduit.</l>
					</lg>
					<lg n="8">
						<l n="71" num="8.1">Dans un superbe enclos où la sagesse habite,</l>
						<l n="72" num="8.2">Où l’on suit des vertus le sentier épineux,</l>
						<l n="73" num="8.3">D’un âge plein d’erreurs mon faible sexe évite</l>
						<l n="74" num="8.4"><space unit="char" quantity="8"/>Les égaremens dangereux.</l>
						<l n="75" num="8.5">D’enfans infortunés cent familles chargées</l>
						<l n="76" num="8.6">Du soin de les pourvoir se trouvent soulagées :</l>
						<l n="77" num="8.7"><space unit="char" quantity="8"/>Quels secours contre un sort ingrat !</l>
						<l n="78" num="8.8">Par lui ce héros paie, en couronnant leurs peines,</l>
						<l n="79" num="8.9">Le sang dont leurs aïeux ont épuisé leurs veines</l>
						<l n="80" num="8.10"><space unit="char" quantity="8"/>Pour la défense de l’état.</l>
					</lg>
					<lg n="9">
						<l n="81" num="9.1">Ainsi dans les jardins l’on voit de jeunes plantes,</l>
						<l n="82" num="9.2">Qu’on ne peut conserver que par des soins divers,</l>
						<l n="83" num="9.3">Vivre et croître à l’abri des ardeurs violentes,</l>
						<l n="84" num="9.4"><space unit="char" quantity="8"/>Et de la rigueur des hivers :</l>
						<l n="85" num="9.5">Par une habile main sans cesse cultivées,</l>
						<l n="86" num="9.6">Et d’une eau vive et pure au besoin abreuvées,</l>
						<l n="87" num="9.7"><space unit="char" quantity="8"/>Elles fleurissent dans leur temps ;</l>
						<l n="88" num="9.8">Tandis qu’à la merci des saisons orageuses,</l>
						<l n="89" num="9.9">Les autres, au milieu des campagnes pierreuses,</l>
						<l n="90" num="9.10"><space unit="char" quantity="8"/>Se flétrissent dès leur printemps.</l>
					</lg>
					<lg n="10">
						<l n="91" num="10.1">Mais quel brillant éclair vient de frapper ma vue ?</l>
						<l n="92" num="10.2">Qui m’appelle ? qu’entends-je ? et qu’est-ce que je vois ?</l>
						<l n="93" num="10.3">Mon cœur est transporté d’une joie inconnue :</l>
						<l n="94" num="10.4"><space unit="char" quantity="8"/>Quels sont ces présages pour moi ?</l>
						<l n="95" num="10.5">Ne m’annoncent-ils point que je verrai la chute</l>
						<l n="96" num="10.6">Des célèbres rivaux avec qui je dispute</l>
						<l n="97" num="10.7"><space unit="char" quantity="8"/>L’honneur de la lice où je cours ?</l>
						<l n="98" num="10.8">Que de gloire ! et quel prix ! si le ciel me l’envoie</l>
						<l n="99" num="10.9">Le portrait de LOUIS à mes regards en proie</l>
						<l n="100" num="10.10"><space unit="char" quantity="8"/>Les occupera tous les jours.</l>
					</lg>
				</div></body></text></TEI>