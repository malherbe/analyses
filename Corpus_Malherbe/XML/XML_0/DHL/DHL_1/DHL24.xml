<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Poésies de Madame Deshoulières</title>
				<title type="medium">Édition électronique</title>
				<author key="DHL">
					<name>
						<forename>Antoinette</forename>
						<surname>DESHOULIÈRES</surname>
					</name>
					<date from="1638" to="1694">1638-1694</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4026 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">DHL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Madame Deshoulières</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">https://fr.wikisource.org/wiki/Po%C3%A9sies_de_Madame_Deshouli%C3%A8res</idno>
						<p>Exporté de Wikisource le 24 juillet 2022</p>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies</title>
								<author>Antoinette Deshoulières</author>
								<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k886829s</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>THÉOPHILE BERQUET, LIBRAIRE</publisher>
									<date when="1824">1824</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres de madame et de mademoiselle Deshoulieres</title>
						<author>Antoinette Deshoulières</author>
						<idno type="URL">https://archive.org/details/oeuvresdemadamee01deshuoft/page/n5/mode/2up</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher> Libraires associés</publisher>
							<date when="1768">1768</date>
						</imprint>
						<biblScope unit="tome">I</biblScope>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres de madame et de mademoiselle Deshoulieres</title>
						<author>Antoinette Deshoulières</author>
						<idno type="URL">https://archive.org/details/oeuvresdemadamee02deshuoft/page/n5/mode/2up</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher> Libraires associés</publisher>
							<date when="1768">1768</date>
						</imprint>
						<biblScope unit="tome">II</biblScope>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1688">1688</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>

			<samplingDecl>
				<p>La présente édition électronique est issue d’une exportation à partir de Wikisource.org (Exporté de Wikisource le 26 juillet 2022).</p>
				<p>La partie "Autobiographie" n’est pas incluse dans la présente édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les notes de bas de page ont été reportées en fin de poème.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">ÉPITRES</head><div type="poem" key="DHL24">
					<head type="main">À Mademoiselle De la Charce</head>
					<lg n="1">
						<l n="1" num="1.1">EH bien ! quel noir chagrin vous occupe aujourd’hui ?</l>
						<l n="2" num="1.2">M’est venu demander avec un fier sourire</l>
						<l n="3" num="1.3"><space unit="char" quantity="8"/>Un jeune seigneur qu’on peut dire</l>
						<l n="4" num="1.4">Aussi beau que l’Amour, aussi traître que lui.</l>
						<l n="5" num="1.5"><space unit="char" quantity="8"/>Vous gardez un profond silence !</l>
						<l n="6" num="1.6"><space unit="char" quantity="4"/>A-t-il repris, jurant à demi-bas :</l>
						<l n="7" num="1.7"><space unit="char" quantity="8"/>Est-ce que vous ne daignez pas</l>
						<l n="8" num="1.8">De ce que vous pensez me faire confidence ?</l>
						<l n="9" num="1.9">Je n’en suis pas peut-être assez digne. À ces mots,</l>
						<l n="10" num="1.10">Pour joindre un autre fat, il m’a tourné le dos.</l>
					</lg>
					<lg n="2">
						<l n="11" num="2.1"><space unit="char" quantity="8"/>Quel discours pouvais-je lui faire,</l>
						<l n="12" num="2.2"><space unit="char" quantity="8"/>Moi qui dans ce même moment</l>
						<l n="13" num="2.3">Repassais dans ma tête avec étonnement</l>
						<l n="14" num="2.4">De la nouvelle cour la conduite ordinaire ?</l>
						<l n="15" num="2.5"><space unit="char" quantity="8"/>M’aurait-il jamais pardonné</l>
						<l n="16" num="2.6"><space unit="char" quantity="8"/>La peinture vive et sincère</l>
						<l n="17" num="2.7">De cent vices auxquels il s’est abandonné ?</l>
						<l n="18" num="2.8"><space unit="char" quantity="4"/>Non ; contre moi le dépit, la colère,</l>
						<l n="19" num="2.9"><space unit="char" quantity="8"/>Le chagrin, tout aurait agi.</l>
					</lg>
					<lg n="3">
						<l n="20" num="3.1">Mais, quoique mes discours eussent pu lui déplaire,</l>
						<l n="21" num="3.2"><space unit="char" quantity="8"/>Son front n’en aurait pas rougi.</l>
						<l n="22" num="3.3">Je sais de ses pareils jusqu’où l’audace monte :</l>
						<l n="23" num="3.4">À tout ce qu’il leur plait osent-ils s’emporter,</l>
						<l n="24" num="3.5"><space unit="char" quantity="8"/>Loin d’en avoir la moindre honte,</l>
						<l n="25" num="3.6"><space unit="char" quantity="8"/>Eux-mêmes vont en plaisanter.</l>
						<l n="26" num="3.7">De leurs déréglemens historiens fidèles,</l>
						<l n="27" num="3.8">Avec un front d’airain ils feront mille fois</l>
						<l n="28" num="3.9">Un odieux détail des plus affreux endroits.</l>
						<l n="29" num="3.10">On dirait, à les voir traiter de bagatelles</l>
						<l n="30" num="3.11"><space unit="char" quantity="8"/>Les horreurs les plus criminelles,</l>
						<l n="31" num="3.12">Que ce n’est point pour eux que sont faites les lois,</l>
						<l n="32" num="3.13"><space unit="char" quantity="8"/>Tant ils ont de mépris pour elles !</l>
					</lg>
					<lg n="4">
						<l n="33" num="4.1">Avec gens sans mérite et du rang le plus bas</l>
						<l n="34" num="4.2"><space unit="char" quantity="8"/>Ils font volontiers connaissance :</l>
						<l n="35" num="4.3">Mais aussi quels égards et quelle déférence</l>
						<l n="36" num="4.4"><space unit="char" quantity="8"/>Voit-on qu’on ait pour eux ? Hélas !</l>
						<l n="37" num="4.5"><space unit="char" quantity="8"/>Ils font oublier leur naissance,</l>
						<l n="38" num="4.6"><space unit="char" quantity="8"/>Quand ils ne s’en souviennent pas.</l>
						<l n="39" num="4.7"><space unit="char" quantity="8"/>Daignent-ils nous rendre visite,</l>
						<l n="40" num="4.8"><space unit="char" quantity="8"/>Le plus ombrageux des époux</l>
						<l n="41" num="4.9"><space unit="char" quantity="8"/>N’en saurait devenir jaloux.</l>
						<l n="42" num="4.10"><space unit="char" quantity="8"/>Ce n’est point pour notre mérite :</l>
						<l n="43" num="4.11"><space unit="char" quantity="8"/>Leurs yeux n’en trouvent point en nous.</l>
						<l n="44" num="4.12">Ce n’est que pour parler de leur gain, de leur perte,</l>
						<l n="45" num="4.13">Se dire que d’un vin qui les charmera tous</l>
						<l n="46" num="4.14">On a fait une heureuse et sûre découverte ;</l>
						<l n="47" num="4.15"><space unit="char" quantity="8"/>Se montrer quelques billets doux ;</l>
						<l n="48" num="4.16"><space unit="char" quantity="8"/>Se dandiner dans une chaise ;</l>
						<l n="49" num="4.17"><space unit="char" quantity="8"/>Faire tous leurs trocs à leur aise,</l>
						<l n="50" num="4.18"><space unit="char" quantity="8"/>Et se donner des rendez-vous.</l>
					</lg>
					<lg n="5">
						<l n="51" num="5.1">Si, par un pur hasard, quelqu’un d’entre eux s’avise</l>
						<l n="52" num="5.2">D’avoir des sentimens tendres, respectueux,</l>
						<l n="53" num="5.3"><space unit="char" quantity="8"/>Tout le reste s’en formalise.</l>
						<l n="54" num="5.4">Il n’est, pour l’arracher à ce penchant heureux,</l>
						<l n="55" num="5.5">Affront qu’on ne lui fasse, horreurs qu’on ne lui dise ;</l>
						<l n="56" num="5.6">Et l’on fait tant, qu’enfin il n’ose être amoureux.</l>
					</lg>
					<lg n="6">
						<l n="57" num="6.1"><space unit="char" quantity="8"/>Causer une heure avec des femmes,</l>
						<l n="58" num="6.2">Leur présenter la main, parler de leurs attraits,</l>
						<l n="59" num="6.3">Entre les jeunes gens, sont des crimes infâmes</l>
						<l n="60" num="6.4"><space unit="char" quantity="8"/>Qu’ils ne se pardonnent jamais.</l>
						<l n="61" num="6.5">Où sont ces cœurs galans ? où sont ces âmes fières ?</l>
						<l n="62" num="6.6"><space unit="char" quantity="8"/>Les Nemours, les Montmorencis,</l>
						<l n="63" num="6.7"><space unit="char" quantity="8"/>Les Bellegardes, les Bussis,</l>
						<l n="64" num="6.8"><space unit="char" quantity="8"/>Les Guises et les Bassompierres ?</l>
						<l n="65" num="6.9"><space unit="char" quantity="8"/>S’il reste encor quelques soucis</l>
						<l n="66" num="6.10">Lorsque de l’Achéron on a traversé l’onde,</l>
						<l n="67" num="6.11">Quelle indignation leur donnent les récits</l>
						<l n="68" num="6.12"><space unit="char" quantity="8"/>De ce qui se passe en ce monde !</l>
						<l n="69" num="6.13"><space unit="char" quantity="8"/>Que n’y peuvent-ils revenir !</l>
						<l n="70" num="6.14"><space unit="char" quantity="8"/>Par leurs bons exemples, peut-être,</l>
						<l n="71" num="6.15">On verrait la tendresse et le respect renaître,</l>
						<l n="72" num="6.16"><space unit="char" quantity="8"/>Que la débauche a su bannir.</l>
						<l n="73" num="6.17"><space unit="char" quantity="8"/>Mais des destins impitoyables</l>
						<l n="74" num="6.18"><space unit="char" quantity="8"/>Les arrêts sont irrévocables :</l>
						<l n="75" num="6.19">Qui passe l’Achéron ne le repasse plus.</l>
						<l n="76" num="6.20"><space unit="char" quantity="8"/>Rien ne ramènera l’usage</l>
						<l n="77" num="6.21"><space unit="char" quantity="8"/>D’être galant, fidèle, sage.</l>
						<l n="78" num="6.22"><space unit="char" quantity="4"/>Les jeunes gens pour jamais sont perdus.</l>
					</lg>
					<lg n="7">
						<l n="79" num="7.1"><space unit="char" quantity="8"/>À bien considérer les choses,</l>
						<l n="80" num="7.2"><space unit="char" quantity="8"/>On a tort de se plaindre d’eux :</l>
						<l n="81" num="7.3"><space unit="char" quantity="8"/>De leurs déréglemens honteux</l>
						<l n="82" num="7.4"><space unit="char" quantity="8"/>Nous sommes les uniques causes.</l>
					</lg>
					<lg n="8">
						<l n="83" num="8.1"><space unit="char" quantity="8"/>Pourquoi leur permettre d’avoir</l>
						<l n="84" num="8.2"><space unit="char" quantity="8"/>Ces impertinens caractères ?</l>
						<l n="85" num="8.3">Que ne les tenons-nous, comme faisaient nos mères,</l>
						<l n="86" num="8.4"><space unit="char" quantity="8"/>Dans le respect, dans le devoir ?</l>
						<l n="87" num="8.5"><space unit="char" quantity="8"/>Avaient-elles plus de pouvoir,</l>
						<l n="88" num="8.6">Plus de beauté que nous, plus d’esprit, plus d’adresse ?</l>
						<l n="89" num="8.7">Ah ! pouvons-nous penser au temps de leur jeunesse</l>
						<l n="90" num="8.8"><space unit="char" quantity="8"/>Et sans honte et sans désespoir ?</l>
						<l n="91" num="8.9"><space unit="char" quantity="8"/>Dans plus d’un réduit agréable</l>
						<l n="92" num="8.10"><space unit="char" quantity="8"/>On voyait venir tour à tour</l>
						<l n="93" num="8.11"><space unit="char" quantity="8"/>Tout ce qu’une superbe cour</l>
						<l n="94" num="8.12"><space unit="char" quantity="8"/>Avait de galant et d’aimable :</l>
						<l n="95" num="8.13"><space unit="char" quantity="8"/>L’esprit, le respect et l’amour</l>
						<l n="96" num="8.14">Y répandaient sur tout un charme inexplicable.</l>
						<l n="97" num="8.15">Les innocens plaisirs, par qui le plus long jour</l>
						<l n="98" num="8.16"><space unit="char" quantity="8"/>Plus vite qu’un moment s’écoule,</l>
						<l n="99" num="8.17"><space unit="char" quantity="8"/>Tous les soirs s’y trouvaient en foule ;</l>
						<l n="100" num="8.18"><space unit="char" quantity="8"/>Et les transports et les désirs,</l>
						<l n="101" num="8.19"><space unit="char" quantity="8"/>Sans le secours de l’espérance,</l>
						<l n="102" num="8.20"><space unit="char" quantity="8"/>À ce qu’on dit, prenaient naissance</l>
						<l n="103" num="8.21"><space unit="char" quantity="8"/>Au milieu de tous ces plaisirs.</l>
					</lg>
					<lg n="9">
						<l n="104" num="9.1">Cet heureux temps n’est plus, un autre a pris sa place.</l>
						<l n="105" num="9.2"><space unit="char" quantity="8"/>Les jeunes gens portent l’audace</l>
						<l n="106" num="9.3"><space unit="char" quantity="8"/>Jusques à la brutalité.</l>
						<l n="107" num="9.4">Quand ils ne nous font pas une incivilité,</l>
						<l n="108" num="9.5"><space unit="char" quantity="8"/>Il semble qu’ils nous fassent grâce.</l>
						<l n="109" num="9.6">Mais, me répondra-t-on, que voulez-vous qu’on fasse ?</l>
						<l n="110" num="9.7"><space unit="char" quantity="8"/>Si ce désordre n’est souffert,</l>
						<l n="111" num="9.8"><space unit="char" quantity="8"/>Regardez quel sort nous menace ;</l>
						<l n="112" num="9.9"><space unit="char" quantity="8"/>Nos maisons seront un désert :</l>
						<l n="113" num="9.10">Il est vrai. Mais sachez que, lorsqu’on les en chasse,</l>
						<l n="114" num="9.11"><space unit="char" quantity="8"/>Ce n’est que du bruit que l’on perd.</l>
						<l n="115" num="9.12">Est-ce un si grand malheur de voir sa chambre vide</l>
						<l n="116" num="9.13"><space unit="char" quantity="8"/>De médisans, de jeunes fous,</l>
						<l n="117" num="9.14">D’insipides railleurs qui n’ont rien de solide</l>
						<l n="118" num="9.15"><space unit="char" quantity="8"/>Que le mépris qu’ils ont pour nous ?</l>
						<l n="119" num="9.16"><space unit="char" quantity="8"/>Oui, par nos indignes manières,</l>
						<l n="120" num="9.17"><space unit="char" quantity="8"/>Ils ont droit de nous mépriser.</l>
						<l n="121" num="9.18"><space unit="char" quantity="4"/>Si nous étions plus sages et plus fières,</l>
						<l n="122" num="9.19"><space unit="char" quantity="8"/>On les verrait en mieux user.</l>
						<l n="123" num="9.20">Mais inutilement on traite ces matières ;</l>
						<l n="124" num="9.21"><space unit="char" quantity="8"/>On y perd sa peine et son temps :</l>
						<l n="125" num="9.22">Aux dépens de sa gloire on cherche des amans.</l>
					</lg>
					<lg n="10">
						<l n="126" num="10.1">Qu’importe que leurs cœurs soient sans délicatesse,</l>
						<l n="127" num="10.2"><space unit="char" quantity="8"/>Sans ardeur, sans sincérité ?</l>
						<l n="128" num="10.3">On les quitte de soins et de fidélité,</l>
						<l n="129" num="10.4"><space unit="char" quantity="8"/>De respect et de politesse ;</l>
						<l n="130" num="10.5">On ne leur donne pas le temps de souhaiter</l>
						<l n="131" num="10.6">Ce qu’au moins par des pleurs, des soins, des complaisances,</l>
						<l n="132" num="10.7"><space unit="char" quantity="8"/>On devrait leur faire acheter.</l>
						<l n="133" num="10.8">On les gâte ; on leur fait de honteuses avances</l>
						<l n="134" num="10.9"><space unit="char" quantity="8"/>Qui ne font que les dégoûter.</l>
					</lg>
					<lg n="11">
						<l n="135" num="11.1">Vous, aimable Daphné, que l’aveugle fortune</l>
						<l n="136" num="11.2"><space unit="char" quantity="8"/>Condamne à vivre dans des lieux</l>
						<l n="137" num="11.3">Où l’on ne connaît point cette foule importune</l>
						<l n="138" num="11.4"><space unit="char" quantity="8"/>Qui suit ici nos demi-dieux,</l>
						<l n="139" num="11.5">Ne vous plaignez jamais de votre destinée.</l>
						<l n="140" num="11.6"><space unit="char" quantity="8"/>Il vaut mieux mille et mille fois</l>
						<l n="141" num="11.7"><space unit="char" quantity="8"/>Avec vos rochers et vos bois</l>
						<l n="142" num="11.8"><space unit="char" quantity="8"/>S’entretenir toute l’année</l>
						<l n="143" num="11.9"><space unit="char" quantity="8"/>Que de passer une heure ou deux</l>
						<l n="144" num="11.10"><space unit="char" quantity="4"/>Avec un tas d’étourdis, de coquettes.</l>
						<l n="145" num="11.11">Des ours et des serpens de vos sombres retraites</l>
						<l n="146" num="11.12"><space unit="char" quantity="8"/>Le commerce est moins dangereux.</l>
					</lg>
				</div></body></text></TEI>