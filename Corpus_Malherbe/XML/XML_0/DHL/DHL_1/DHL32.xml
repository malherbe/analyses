<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Poésies de Madame Deshoulières</title>
				<title type="medium">Édition électronique</title>
				<author key="DHL">
					<name>
						<forename>Antoinette</forename>
						<surname>DESHOULIÈRES</surname>
					</name>
					<date from="1638" to="1694">1638-1694</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4026 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">DHL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Madame Deshoulières</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">https://fr.wikisource.org/wiki/Po%C3%A9sies_de_Madame_Deshouli%C3%A8res</idno>
						<p>Exporté de Wikisource le 24 juillet 2022</p>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies</title>
								<author>Antoinette Deshoulières</author>
								<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k886829s</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>THÉOPHILE BERQUET, LIBRAIRE</publisher>
									<date when="1824">1824</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres de madame et de mademoiselle Deshoulieres</title>
						<author>Antoinette Deshoulières</author>
						<idno type="URL">https://archive.org/details/oeuvresdemadamee01deshuoft/page/n5/mode/2up</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher> Libraires associés</publisher>
							<date when="1768">1768</date>
						</imprint>
						<biblScope unit="tome">I</biblScope>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres de madame et de mademoiselle Deshoulieres</title>
						<author>Antoinette Deshoulières</author>
						<idno type="URL">https://archive.org/details/oeuvresdemadamee02deshuoft/page/n5/mode/2up</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher> Libraires associés</publisher>
							<date when="1768">1768</date>
						</imprint>
						<biblScope unit="tome">II</biblScope>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1688">1688</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>

			<samplingDecl>
				<p>La présente édition électronique est issue d’une exportation à partir de Wikisource.org (Exporté de Wikisource le 26 juillet 2022).</p>
				<p>La partie "Autobiographie" n’est pas incluse dans la présente édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les notes de bas de page ont été reportées en fin de poème.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">ÉPITRES</head><div type="poem" key="DHL32">
					<head type="main">À Monsieur de Benserade</head>
					<lg n="1">
						<l n="1" num="1.1"><space unit="char" quantity="8"/>ILLUSTRE Damon, votre absence</l>
						<l n="2" num="1.2"><space unit="char" quantity="8"/>Commence enfin à m’alarmer ;</l>
						<l n="3" num="1.3"><space unit="char" quantity="8"/>Hé quoi ! cesseriez-vous d’aimer</l>
						<l n="4" num="1.4"><space unit="char" quantity="8"/>Aussitôt que l’hiver commence ?</l>
						<l n="5" num="1.5">Revenez dans ces lieux ; tout y parle de vous ;</l>
						<l n="6" num="1.6"><space unit="char" quantity="8"/>L’amour vous invite à paraître ;</l>
						<l n="7" num="1.7"><space unit="char" quantity="8"/>Suivez ses ordres, mon cher maître :</l>
						<l n="8" num="1.8"><space unit="char" quantity="8"/>De ses droits l’amour est jaloux ;</l>
						<l n="9" num="1.9"><space unit="char" quantity="8"/>Redoutez son juste courroux.</l>
						<l n="10" num="1.10"><space unit="char" quantity="8"/>Que faites-vous à la campagne</l>
						<l n="11" num="1.11"><space unit="char" quantity="8"/>Lorsque les fougueux aquilons</l>
						<l n="12" num="1.12"><space unit="char" quantity="8"/>Désolent les bois, les vallons ?</l>
						<l n="13" num="1.13"><space unit="char" quantity="8"/>N’auriez-vous pas quelque compagne ?</l>
						<l n="14" num="1.14"><space unit="char" quantity="8"/>Ce soupçon fait frémir mon cœur ;</l>
						<l n="15" num="1.15">De mon cruel destin je connais la rigueur.</l>
						<l n="16" num="1.16"><space unit="char" quantity="8"/>Vous ne m’aimez plus, et je gage</l>
						<l n="17" num="1.17"><space unit="char" quantity="8"/>Que vous suivrez le bel usage</l>
						<l n="18" num="1.18"><space unit="char" quantity="8"/>Qui rend sans crime un cœur volage.</l>
						<l n="19" num="1.19"><space unit="char" quantity="8"/>Mais ne serait-ce point aussi</l>
						<l n="20" num="1.20"><space unit="char" quantity="8"/>Que, pour entrer dans la querelle</l>
						<l n="21" num="1.21"><space unit="char" quantity="8"/>De ce malherbien fidèle</l>
						<l n="22" num="1.22">Dont un précieux prix fit en vain le souci,</l>
						<l n="23" num="1.23">Vous osez, faible amant, m’abandonner ainsi ?</l>
						<l n="24" num="1.24">Pour vous voir condamner, Damon, je vous appelle</l>
						<l n="25" num="1.25"><space unit="char" quantity="8"/>Devant les juges que voici.</l>
						<l n="26" num="1.26"><space unit="char" quantity="8"/>Ce sont tous gens dont la prudence</l>
						<l n="27" num="1.27">Sur celle de Nestor emporte la balance.</l>
						<l n="28" num="1.28"><space unit="char" quantity="8"/>L’amoureux Boyer par avance</l>
						<l n="29" num="1.29"><space unit="char" quantity="8"/>S’est déclaré mon protecteur.</l>
						<l n="30" num="1.30"><space unit="char" quantity="8"/>Perrault, des anciens la terreur,</l>
						<l n="31" num="1.31">S’armera de raisons contre votre inconstance ;</l>
						<l n="32" num="1.32"><space unit="char" quantity="8"/>Charpentier, au teint vif et frais,</l>
						<l n="33" num="1.33"><space unit="char" quantity="8"/>Et dont la divine éloquence</l>
						<l n="34" num="1.34">À l’immortalité passera sans relais,</l>
						<l n="35" num="1.35">Soutiendra, j’en suis sûre, avec que violence,</l>
						<l n="36" num="1.36">Qu’heureux ou malheureux, un cœur ne doit jamais</l>
						<l n="37" num="1.37"><space unit="char" quantity="8"/>Sortir de mon obéissance.</l>
						<l n="38" num="1.38"><space unit="char" quantity="8"/>Quinault, des plaisirs le soutien,</l>
						<l n="39" num="1.39"><space unit="char" quantity="8"/>Et les délices de la France,</l>
						<l n="40" num="1.40"><space unit="char" quantity="8"/>Vous donnera pour pénitence</l>
						<l n="41" num="1.41"><space unit="char" quantity="8"/>D’aimer long-temps sans espérance.</l>
						<l n="42" num="1.42"><space unit="char" quantity="8"/>Le bon abbé du Val-Chrétien</l>
						<l n="43" num="1.43">Prendra, s’il s’en souvient, avec soin ma défense.</l>
						<l n="44" num="1.44"><space unit="char" quantity="8"/>Mais pour le Clerc, je n’en sais rien.</l>
						<l n="45" num="1.45">Lavaux, dont la vertu mérite qu’on le nomme</l>
						<l n="46" num="1.46"><space unit="char" quantity="8"/>Un jour à l’évêché de Rome,</l>
						<l n="47" num="1.47">Et dont l’esprit est juste et rempli d’équité,</l>
						<l n="48" num="1.48"><space unit="char" quantity="8"/>Sera, Damon, de mon côté.</l>
						<l n="49" num="1.49"><space unit="char" quantity="8"/>Si vous en voulez davantage</l>
						<l n="50" num="1.50"><space unit="char" quantity="8"/>Pour vous ramener sous mes lois,</l>
						<l n="51" num="1.51"><space unit="char" quantity="8"/>J’y pourrai joindre le suffrage</l>
						<l n="52" num="1.52"><space unit="char" quantity="8"/>Du galant et docte Ménage,</l>
						<l n="53" num="1.53">Qui de l’académie a refusé le choix.</l>
						<l n="54" num="1.54">Cependant n’allez pas trop craindre ma colère ;</l>
						<l n="55" num="1.55">La prudence permet de suivre les saisons ;</l>
						<l n="56" num="1.56">Aujourd’hui l’on rirait si, d’un air trop sévère,</l>
						<l n="57" num="1.57">Je refusais, Damon, d’écouter vos raisons…</l>
					</lg>
				</div></body></text></TEI>