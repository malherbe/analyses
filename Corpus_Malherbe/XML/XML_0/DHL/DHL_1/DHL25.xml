<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Poésies de Madame Deshoulières</title>
				<title type="medium">Édition électronique</title>
				<author key="DHL">
					<name>
						<forename>Antoinette</forename>
						<surname>DESHOULIÈRES</surname>
					</name>
					<date from="1638" to="1694">1638-1694</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4026 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">DHL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Madame Deshoulières</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">https://fr.wikisource.org/wiki/Po%C3%A9sies_de_Madame_Deshouli%C3%A8res</idno>
						<p>Exporté de Wikisource le 24 juillet 2022</p>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies</title>
								<author>Antoinette Deshoulières</author>
								<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k886829s</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>THÉOPHILE BERQUET, LIBRAIRE</publisher>
									<date when="1824">1824</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres de madame et de mademoiselle Deshoulieres</title>
						<author>Antoinette Deshoulières</author>
						<idno type="URL">https://archive.org/details/oeuvresdemadamee01deshuoft/page/n5/mode/2up</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher> Libraires associés</publisher>
							<date when="1768">1768</date>
						</imprint>
						<biblScope unit="tome">I</biblScope>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres de madame et de mademoiselle Deshoulieres</title>
						<author>Antoinette Deshoulières</author>
						<idno type="URL">https://archive.org/details/oeuvresdemadamee02deshuoft/page/n5/mode/2up</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher> Libraires associés</publisher>
							<date when="1768">1768</date>
						</imprint>
						<biblScope unit="tome">II</biblScope>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1688">1688</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>

			<samplingDecl>
				<p>La présente édition électronique est issue d’une exportation à partir de Wikisource.org (Exporté de Wikisource le 26 juillet 2022).</p>
				<p>La partie "Autobiographie" n’est pas incluse dans la présente édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les notes de bas de page ont été reportées en fin de poème.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">ÉPITRES</head><div type="poem" key="DHL25">
					<head type="main">À Monsieur Caze,</head>
					<head type="sub_1">POUR LE JOUR DE SA FÊTE</head>
					<lg n="1">
						<l n="1" num="1.1"><space unit="char" quantity="8"/>ON dit que je ne suis pas bête :</l>
						<l n="2" num="1.2">Cependant, n’en déplaise aux donneurs de renom,</l>
						<l n="3" num="1.3"><space unit="char" quantity="8"/>Quand il faut chanter votre fête</l>
						<l n="4" num="1.4">Je ne saurais tirer un seul vers de ma tête.</l>
						<l n="5" num="1.5">Jean ! Que dire sur Jean ? C’est un terrible nom,</l>
						<l n="6" num="1.6">Que jamais n’accompagne une épithète honnête.</l>
						<l n="7" num="1.7">Jean de Vignes, Jean Logue… Où vais-je ? Trouvez bon</l>
						<l n="8" num="1.8"><space unit="char" quantity="8"/>Qu’en si beau chemin je m’arrête,</l>
						<l n="9" num="1.9">Et que, pour comparer vous et votre patron,</l>
						<l n="10" num="1.10"><space unit="char" quantity="8"/>Je prenne sur un autre ton</l>
						<l n="11" num="1.11"><space unit="char" quantity="8"/>Ce que la légende me prête.</l>
						<l n="12" num="1.12">M’y voilà. Commençons par le saint qu’aujourd’hui</l>
						<l n="13" num="1.13"><space unit="char" quantity="8"/>Notre mère la sainte Église</l>
						<l n="14" num="1.14"><space unit="char" quantity="8"/>Ordonne que l’on solennise,</l>
						<l n="15" num="1.15">Et voyons quel rapport vous avez avec lui.</l>
						<l n="16" num="1.16">Ou je m’y connais mal, ou vous n’en avez guère ;</l>
						<l n="17" num="1.17"><space unit="char" quantity="8"/>Point du tout même, à parler franc.</l>
						<l n="18" num="1.18">L’évangéliste et vous, plus je vous considère,</l>
						<l n="19" num="1.19"><space unit="char" quantity="8"/>Et plus je vais du noir au blanc.</l>
						<l n="20" num="1.20">Avoir pu de Satan éviter tous les pièges ;</l>
						<l n="21" num="1.21">Avoir été d’un Dieu le disciple chéri ;</l>
						<l n="22" num="1.22">Jusqu’à la fin des temps voir les glaçons, les neiges</l>
						<l n="23" num="1.23"><space unit="char" quantity="8"/>Faire place au printemps fleuri,</l>
						<l n="24" num="1.24">Privilége qui seul vaut tous les privilèges,</l>
						<l n="25" num="1.25"><space unit="char" quantity="8"/>N’<choice hand="RR" type="false verse" reason="analysis"><sic>est-ce</sic><corr source="édition de 1768">est</corr></choice> pas, selon moi, ce qui fait</l>
						<l n="26" num="1.26">De l’apôtre et de vous toute la différence ?</l>
						<l n="27" num="1.27"><space unit="char" quantity="8"/>Et l’Apocalypse est un trait</l>
						<l n="28" num="1.28"><space unit="char" quantity="8"/>Qui, fussiez-vous un saint parfait,</l>
						<l n="29" num="1.29"><space unit="char" quantity="8"/>Gâterait trop la ressemblance.</l>
						<l n="30" num="1.30"><space unit="char" quantity="8"/>Oh ! qu’heureuses auraient été</l>
						<l n="31" num="1.31"><space unit="char" quantity="8"/>Quantité de doctes cervelles</l>
						<l n="32" num="1.32">Si saint Jean eût écrit avec la netteté</l>
						<l n="33" num="1.33">Qui, joint au tour charmant, aux grâces naturelles,</l>
						<l n="34" num="1.34"><space unit="char" quantity="8"/>Rend vos tendres chansons si belles !</l>
						<l n="35" num="1.35">Mais que fais-je ! où m’emporte un enjoûment outré ?</l>
						<l n="36" num="1.36"><space unit="char" quantity="8"/>Comparer un livre sacré</l>
						<l n="37" num="1.37"><space unit="char" quantity="8"/>À de profanes bagatelles !</l>
						<l n="38" num="1.38">De telles libertés trouvent plus d’un censeur,</l>
						<l n="39" num="1.39">Qui charitablement en fait un mauvais conte.</l>
						<l n="40" num="1.40">Évitons un danger qui n’est jamais sans honte.</l>
						<l n="41" num="1.41"><space unit="char" quantity="8"/>Peut-être chez le précurseur</l>
						<l n="42" num="1.42"><space unit="char" quantity="8"/>Trouverions-nous mieux notre compte.</l>
						<l n="43" num="1.43"><space unit="char" quantity="8"/>Essayons. Ah ! c’est encor pis.</l>
						<l n="44" num="1.44"><space unit="char" quantity="8"/>Vous n’êtes en rien parallèles.</l>
						<l n="45" num="1.45">Il prêchait au désert, et vous dans les ruelles.</l>
						<l n="46" num="1.46">Une peau de chameau faisait tous ses habits,</l>
						<l n="47" num="1.47">Vous donnez volontiers dans les modes nouvelles.</l>
						<l n="48" num="1.48">Il se désaltérait dans un coulant ruisseau,</l>
						<l n="49" num="1.49"><space unit="char" quantity="8"/>Se nourrissait de sauterelles :</l>
						<l n="50" num="1.50">Vous ne quitteriez pas les ortolans pour elles ;</l>
						<l n="51" num="1.51">Et je me trompe fort, ou vous n’aimez que l’eau</l>
						<l n="52" num="1.52">Que boivent à longs traits les neuf doctes pucelles.</l>
						<l n="53" num="1.53"><space unit="char" quantity="8"/>Vous le voyez, j’ai beau chercher,</l>
						<l n="54" num="1.54">Tourner, approfondir, passer d’un saint à l’autre,</l>
						<l n="55" num="1.55">Vous n’avez rien du tout, soit dit sans vous fâcher,</l>
						<l n="56" num="1.56"><space unit="char" quantity="8"/>Du précurseur ni de l’apôtre.</l>
						<l n="57" num="1.57">J’enrage cependant avec mon bel esprit.</l>
						<l n="58" num="1.58">Aussi pourquoi faut-il, tourné comme vous êtes,</l>
						<l n="59" num="1.59"><space unit="char" quantity="8"/>Porter un nom qui ne fournit</l>
						<l n="60" num="1.60">Rien d’agréable à dire aux plus savans poëtes ;</l>
						<l n="61" num="1.61">Et sur qui, si j’osais en croire mon dépit,</l>
						<l n="62" num="1.62"><space unit="char" quantity="8"/>Je reviendrais aux épithètes ?</l>
						<l n="63" num="1.63">Demeurez-en d’accord ; ce n’est pas sans raison</l>
						<l n="64" num="1.64"><space unit="char" quantity="8"/>Que, de votre nom effrayée,</l>
						<l n="65" num="1.65"><space unit="char" quantity="8"/>Je me suis d’abord écriée :</l>
						<l n="66" num="1.66"><space unit="char" quantity="8"/>Que dirai-je sur un tel nom ?</l>
						<l n="67" num="1.67">J’ai prévu l’embarras. Quand je fais quelque ouvrage,</l>
						<l n="68" num="1.68"><space unit="char" quantity="8"/>Je tâte toujours le terrain.</l>
						<l n="69" num="1.69"><space unit="char" quantity="8"/>Ah ! que maudit soit le parrain</l>
						<l n="70" num="1.70">Qui vous alla donner ce beau nom en partage !</l>
						<l n="71" num="1.71"><space unit="char" quantity="8"/>Il était sans doute en courroux,</l>
						<l n="72" num="1.72"><space unit="char" quantity="8"/>Et voulait vous faire une injure ;</l>
						<l n="73" num="1.73">Fut-il jamais un nom d’un plus mauvais augure ?</l>
						<l n="74" num="1.74"><space unit="char" quantity="8"/>Croyez-moi, débaptisez-vous.</l>
					</lg>
				</div></body></text></TEI>