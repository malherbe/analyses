<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Poésies de Madame Deshoulières</title>
				<title type="medium">Édition électronique</title>
				<author key="DHL">
					<name>
						<forename>Antoinette</forename>
						<surname>DESHOULIÈRES</surname>
					</name>
					<date from="1638" to="1694">1638-1694</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4026 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">DHL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Madame Deshoulières</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">https://fr.wikisource.org/wiki/Po%C3%A9sies_de_Madame_Deshouli%C3%A8res</idno>
						<p>Exporté de Wikisource le 24 juillet 2022</p>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies</title>
								<author>Antoinette Deshoulières</author>
								<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k886829s</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>THÉOPHILE BERQUET, LIBRAIRE</publisher>
									<date when="1824">1824</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres de madame et de mademoiselle Deshoulieres</title>
						<author>Antoinette Deshoulières</author>
						<idno type="URL">https://archive.org/details/oeuvresdemadamee01deshuoft/page/n5/mode/2up</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher> Libraires associés</publisher>
							<date when="1768">1768</date>
						</imprint>
						<biblScope unit="tome">I</biblScope>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres de madame et de mademoiselle Deshoulieres</title>
						<author>Antoinette Deshoulières</author>
						<idno type="URL">https://archive.org/details/oeuvresdemadamee02deshuoft/page/n5/mode/2up</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher> Libraires associés</publisher>
							<date when="1768">1768</date>
						</imprint>
						<biblScope unit="tome">II</biblScope>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1688">1688</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>

			<samplingDecl>
				<p>La présente édition électronique est issue d’une exportation à partir de Wikisource.org (Exporté de Wikisource le 26 juillet 2022).</p>
				<p>La partie "Autobiographie" n’est pas incluse dans la présente édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les notes de bas de page ont été reportées en fin de poème.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">IDYLLES</head><div type="poem" key="DHL1">
					<head type="main">À Mademoiselle de la Charce</head>
					<head type="sub_1">POUR LA FONTAINE DE VAUCLUSE</head>
					<lg n="1">
						<l n="1" num="1.1"><space unit="char" quantity="8"/>QUAND vous me pressez de chanter</l>
						<l n="2" num="1.2"><space unit="char" quantity="8"/>Pour une fontaine fameuse,</l>
						<l n="3" num="1.3">Vous avez oublié que je suis paresseuse ;</l>
						<l n="4" num="1.4">Qu’un simple madrigal pourrait m’épouvanter ;</l>
						<l n="5" num="1.5"><space unit="char" quantity="8"/>Qu’entre une santé languissante</l>
						<l n="6" num="1.6">Et d’illustres amis par le sort outragés</l>
						<l n="7" num="1.7"><space unit="char" quantity="8"/>Mes soins sont toujours partagés.</l>
						<l n="8" num="1.8">Par plus d’une raison devenez moins pressante.</l>
						<l n="9" num="1.9">Daphné, vous ne savez à quoi vous m’engagez.</l>
						<l n="10" num="1.10">Peut-être croyez-vous que, toujours insensible,</l>
						<l n="11" num="1.11"><space unit="char" quantity="8"/>Je vous décrirai dans mes vers,</l>
						<l n="12" num="1.12">Entre de hauts rochers dont l’aspect est terrible,</l>
						<l n="13" num="1.13">Des prés toujours fleuris, des arbres toujours verts</l>
						<l n="14" num="1.14"><space unit="char" quantity="8"/>Une source orgueilleuse et pure,</l>
						<l n="15" num="1.15"><space unit="char" quantity="8"/>Dont l’eau, sur cent rochers divers,</l>
						<l n="16" num="1.16"><space unit="char" quantity="8"/>D’une mousse verte couverts,</l>
						<l n="17" num="1.17"><space unit="char" quantity="8"/>S’épanche, bouillonne, murmure ;</l>
						<l n="18" num="1.18">Des agneaux bondissans sur la tendre verdure,</l>
						<l n="19" num="1.19">Et de leurs conducteurs les rustiques concerts.</l>
						<l n="20" num="1.20">De ce fameux désert la beauté surprenante,</l>
						<l n="21" num="1.21">Que la nature seule a pris soin de former,</l>
						<l n="22" num="1.22">Amusait autrefois mon âme indifférente.</l>
						<l n="23" num="1.23">Combien de fois, hélas ! m’a-t-elle su charmer !</l>
						<l n="24" num="1.24">Cet heureux temps n’est plus : languissante, attendrie,</l>
						<l n="25" num="1.25"><space unit="char" quantity="8"/>Je regarde indifféremment</l>
						<l n="26" num="1.26">Les plus brillantes eaux, la plus verte prairie ;</l>
						<l n="27" num="1.27"><space unit="char" quantity="8"/>Et du soin de ma bergerie</l>
						<l n="28" num="1.28">Je ne fais même plus mon divertissement.</l>
						<l n="29" num="1.29">Je passe tout le jour dans une rêverie</l>
						<l n="30" num="1.30"><space unit="char" quantity="8"/>Qu’on dit qui m’empoisonnera.</l>
						<l n="31" num="1.31">À tout autre plaisir mon esprit se refuse ;</l>
						<l n="32" num="1.32">Et si vous me forcez à parler de Vaucluse,</l>
						<l n="33" num="1.33"><space unit="char" quantity="8"/>Mon cœur tout seul en parlera.</l>
						<l n="34" num="1.34">Je laisserai conter de sa source inconnue</l>
						<l n="35" num="1.35"><space unit="char" quantity="8"/>Ce qu’elle a de prodigieux,</l>
						<l n="36" num="1.36">Sa fuite, son retour, et la vaste étendue</l>
						<l n="37" num="1.37"><space unit="char" quantity="8"/>Qu’arrose son cours furieux.</l>
						<l n="38" num="1.38">Je suivrai le penchant de mon âme enflammée :</l>
						<l n="39" num="1.39">Je ne vous ferai voir dans ces aimables lieux</l>
						<l n="40" num="1.40"><space unit="char" quantity="8"/>Que Laure tendrement aimée,</l>
						<l n="41" num="1.41"><space unit="char" quantity="8"/>Et Pétrarque victorieux.</l>
					</lg>
					<lg n="2">
						<l n="42" num="2.1">Aussi bien de Vaucluse ils font encor la gloire :</l>
						<l n="43" num="2.2">Le temps qui détruit tout respecte leurs plaisirs :</l>
						<l n="44" num="2.3">Les ruisseaux, les rochers, les oiseaux, les zéphyrs</l>
						<l n="45" num="2.4"><space unit="char" quantity="8"/>Font tous les jours leur tendre histoire.</l>
						<l n="46" num="2.5">Oui, cette vive source, en roulant sur ces bords,</l>
						<l n="47" num="2.6">Semble nous raconter les tourmens, les transports</l>
						<l n="48" num="2.7">Que Pétrarque sentait pour la divine Laure.</l>
						<l n="49" num="2.8">Il exprima si bien sa peine, son ardeur,</l>
						<l n="50" num="2.9"><space unit="char" quantity="8"/>Que Laure, malgré sa rigueur,</l>
						<l n="51" num="2.10"><space unit="char" quantity="8"/>L’écouta, plaignit sa langueur,</l>
						<l n="52" num="2.11"><space unit="char" quantity="8"/>Et fit peut-être plus encore.</l>
					</lg>
					<lg n="3">
						<l n="53" num="3.1">Dans cet antre profond, où, sans autres témoins</l>
						<l n="54" num="3.2"><space unit="char" quantity="8"/>Que la naïade et le zéphyre,</l>
						<l n="55" num="3.3"><space unit="char" quantity="8"/>Laure sut, par de tendres soins,</l>
						<l n="56" num="3.4">De l’amoureux Pétrarque adoucir le martyre :</l>
						<l n="57" num="3.5">Dans cet antre, où l’Amour tant de fois fut vainqueur,</l>
						<l n="58" num="3.6"><space unit="char" quantity="8"/>Quelque fierté dont on se pique,</l>
						<l n="59" num="3.7"><space unit="char" quantity="8"/>On sent élever dans son cœur</l>
						<l n="60" num="3.8">Ce trouble dangereux par qui l’amour s’explique,</l>
						<l n="61" num="3.9"><space unit="char" quantity="8"/>Quand il alarme la pudeur.</l>
					</lg>
					<lg n="4">
						<l n="62" num="4.1">Ce n’est pas seulement dans cet antre écarté</l>
						<l n="63" num="4.2">Qu’il reste de leurs feux une marque immortelle :</l>
						<l n="64" num="4.3">Ce fertile vallon dont on a tant vanté</l>
						<l n="65" num="4.4"><space unit="char" quantity="8"/>La solitude et la beauté,</l>
						<l n="66" num="4.5">Voit mille fois le jour, dans la saison nouvelle,</l>
						<l n="67" num="4.6"><space unit="char" quantity="4"/>Les rossignols, les serins, les pinçons</l>
						<l n="68" num="4.7"><space unit="char" quantity="8"/>Répéter sous son vert ombrage</l>
						<l n="69" num="4.8"><space unit="char" quantity="8"/>Je ne sais quel doux badinage</l>
						<l n="70" num="4.9">Dont ces heureux amans leur donnaient des leçons.</l>
					</lg>
					<lg n="5">
						<l n="71" num="5.1">Leurs noms sur ces rochers peuvent encor se lire ;</l>
						<l n="72" num="5.2"><space unit="char" quantity="8"/>L’un avec l’autre est confondu ;</l>
						<l n="73" num="5.3"><space unit="char" quantity="8"/>Et l’âme à peine peut suffire</l>
						<l n="74" num="5.4">Aux tendres mouvemens que leur mélange inspire.</l>
						<l n="75" num="5.5"><space unit="char" quantity="8"/>Quel charme est ici répandu !</l>
						<l n="76" num="5.6">À nous faire imiter ces amans tout conspire ;</l>
						<l n="77" num="5.7">Par les soins de l’Amour leurs soupirs conservés</l>
						<l n="78" num="5.8"><space unit="char" quantity="8"/>Enflamment l’air qu’on y respire ;</l>
						<l n="79" num="5.9"><space unit="char" quantity="8"/>Et les cœurs qui se sont sauvés</l>
						<l n="80" num="5.10"><space unit="char" quantity="8"/>De son impitoyable empire</l>
						<l n="81" num="5.11"><space unit="char" quantity="8"/>À ces déserts sont réservés.</l>
					</lg>
					<lg n="6">
						<l n="82" num="6.1">Tout ce qu’a de charmant leur beauté naturelle</l>
						<l n="83" num="6.2"><space unit="char" quantity="8"/>Ne peut m’occuper un moment.</l>
						<l n="84" num="6.3">Les restes précieux d’une flamme si belle</l>
						<l n="85" num="6.4">Font de mon jeune cœur le seul amusement.</l>
						<l n="86" num="6.5"><space unit="char" quantity="8"/>Ah ! qu’il m’entretient tendrement</l>
						<l n="87" num="6.6"><space unit="char" quantity="8"/>Du bonheur de la belle Laure !</l>
						<l n="88" num="6.7"><space unit="char" quantity="8"/>Et qu’à parler sincèrement,</l>
						<l n="89" num="6.8">Il serait doux d’aimer, si l’on trouvait encore</l>
						<l n="90" num="6.9">Un cœur comme le cœur de son illustre amant !</l>
					</lg>
				</div></body></text></TEI>