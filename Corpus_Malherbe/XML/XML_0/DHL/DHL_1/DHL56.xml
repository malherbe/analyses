<?xml version="1.0" encoding="UTF-8"?><TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Poésies de Madame Deshoulières</title>
				<title type="medium">Édition électronique</title>
				<author key="DHL">
					<name>
						<forename>Antoinette</forename>
						<surname>DESHOULIÈRES</surname>
					</name>
					<date from="1638" to="1694">1638-1694</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4026 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">DHL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Madame Deshoulières</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">https://fr.wikisource.org/wiki/Po%C3%A9sies_de_Madame_Deshouli%C3%A8res</idno>
						<p>Exporté de Wikisource le 24 juillet 2022</p>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies</title>
								<author>Antoinette Deshoulières</author>
								<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k886829s</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>THÉOPHILE BERQUET, LIBRAIRE</publisher>
									<date when="1824">1824</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres de madame et de mademoiselle Deshoulieres</title>
						<author>Antoinette Deshoulières</author>
						<idno type="URL">https://archive.org/details/oeuvresdemadamee01deshuoft/page/n5/mode/2up</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher> Libraires associés</publisher>
							<date when="1768">1768</date>
						</imprint>
						<biblScope unit="tome">I</biblScope>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres de madame et de mademoiselle Deshoulieres</title>
						<author>Antoinette Deshoulières</author>
						<idno type="URL">https://archive.org/details/oeuvresdemadamee02deshuoft/page/n5/mode/2up</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher> Libraires associés</publisher>
							<date when="1768">1768</date>
						</imprint>
						<biblScope unit="tome">II</biblScope>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1688">1688</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>

			<samplingDecl>
				<p>La présente édition électronique est issue d’une exportation à partir de Wikisource.org (Exporté de Wikisource le 26 juillet 2022).</p>
				<p>La partie "Autobiographie" n’est pas incluse dans la présente édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les notes de bas de page ont été reportées en fin de poème.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">BOUQUETS</head><div type="poem" key="DHL56">
					<head type="main">À Madame ***</head>
					<lg n="1">
						<l n="1" num="1.1"><space unit="char" quantity="8"/>SANS me plaindre de la nature,</l>
						<l n="2" num="1.2"><space unit="char" quantity="8"/>Je voyais les premières fleurs</l>
						<l n="3" num="1.3">Répandre dans les airs d’agréables odeurs,</l>
						<l n="4" num="1.4"><space unit="char" quantity="8"/>Et mêler leurs vives couleurs</l>
						<l n="5" num="1.5"><space unit="char" quantity="8"/>Avec la naissante verdure,</l>
						<l n="6" num="1.6"><space unit="char" quantity="8"/>Quand un plus important souci</l>
						<l n="7" num="1.7"><space unit="char" quantity="8"/>Que celui d’embellir la terre,</l>
						<l n="8" num="1.8">À la charmante Flore, au milieu du parterre,</l>
						<l n="9" num="1.9"><space unit="char" quantity="8"/>Me força de parler ainsi :</l>
					</lg>
					<lg n="2">
						<l n="10" num="2.1">Jeune divinité, pour qui le doux Zéphyre</l>
						<l n="11" num="2.2"><space unit="char" quantity="8"/>Pousse tant d’amoureux soupirs ;</l>
						<l n="12" num="2.3"><space unit="char" quantity="8"/>Vous qui ramenez les plaisirs,</l>
						<l n="13" num="2.4">Vous dont toutes les fleurs reconnaissent l’empire,</l>
						<l n="14" num="2.5">De celles du printemps que n’ai-je le destin !</l>
						<l n="15" num="2.6">Je sais que leur beauté ne dure qu’un matin,</l>
						<l n="16" num="2.7">Et que d’un sort plus doux ma naissance et suivie ;</l>
						<l n="17" num="2.8"><space unit="char" quantity="8"/>Mais elles naissent dans le temps</l>
						<l n="18" num="2.9">Qu’on célèbre en ces lieux la fête de Sylvie.</l>
						<l n="19" num="2.10"><space unit="char" quantity="8"/>Hélas ! que je leur porte envie,</l>
						<l n="20" num="2.11">Et que je voudrais bien fleurir dans le printemps !</l>
					</lg>
					<lg n="3">
						<l n="21" num="3.1">Un si juste souhait toucha le cœur de Flore ;</l>
						<l n="22" num="3.2"><space unit="char" quantity="8"/>Et, malgré l’ordre des saisons,</l>
						<l n="23" num="3.3">À peine le soleil eut-il vu deux maisons,</l>
						<l n="24" num="3.4"><space unit="char" quantity="8"/>Que ma fleur commença d’éclore.</l>
						<l n="25" num="3.5">Je perds avec plaisir dans cet heureux état</l>
						<l n="26" num="3.6"><space unit="char" quantity="8"/>Les honneurs que l’été m’apprête ;</l>
						<l n="27" num="3.7"><space unit="char" quantity="8"/>Et, pour couronner votre tête,</l>
						<l n="28" num="3.8">Je parais ce matin avec tout mon éclat.</l>
						<l n="29" num="3.9">Si par mon doux parfum j’obtiens cet avantage,</l>
						<l n="30" num="3.10">Fière d’un tel emploi, je verrai sans ennui</l>
						<l n="31" num="3.11">Mes sœurs dans quelques mois rendre un pareil hommage</l>
						<l n="32" num="3.12"><space unit="char" quantity="8"/>Au plus grand prince d’aujourd’hui.</l>
					</lg>
				</div></body></text></TEI>