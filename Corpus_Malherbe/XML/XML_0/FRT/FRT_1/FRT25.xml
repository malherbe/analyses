<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE BOURDEAU DES NEUF PUCELLES</title>
				<title type="medium">Édition électronique</title>
				<author key="FRT">
					<name>
						<forename>Charles-Théophile</forename>
						<surname>FERET</surname>
					</name>
					<date from="1858" to="1928">1858-1928</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1111 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">FRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le Bourdeau des neuf pucelles</title>
						<author>Charles-Théophile Feret</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">https://www.gutenberg.org/ebooks/66781</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le Bourdeau des neuf pucelles</title>
								<author>Charles-Théophile Feret</author>
								<imprint>
									<pubPlace>CAUDÉRAN-BORDEAUX</pubPlace>
									<publisher>ÉDITIONS DES CAHIERS LITTÉRAIRES</publisher>
									<date when="1923">1923</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1912">1912</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et les notes de l’éditeur n’ont pas été reprises.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>
					Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique
					après application du programme de calcul de la longueur métrique des vers).
				</p>
				<p>Les notes de bas de page (numérotation conforme à l’édition imprimée) ont été reportées en fin de poème.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension (…).</p>
					<p>Les tirets simples ont été remplacés par des tirets demi-cadratin.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-12-09" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.4.xsd)</change>
				<change when="2021-12-10" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">Vers pour <lb></lb>LES SERVANTES</head><div type="poem" key="FRT25">
					<head type="main">Servante d’Hôtel</head>
					<lg n="1">
						<l n="1" num="1.1">Tu ne sers pas Vénus, mais tu sers ses prêtresses,</l>
						<l n="2" num="1.2">Tu regardes monter les sacrificateurs…</l>
						<l n="3" num="1.3">Fais le lit du plaisir, mais crains que la froideur</l>
						<l n="4" num="1.4">De tes mains de Vestale offense la Déesse.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">De ton sang nuptial tu lui dois les prémisses,</l>
						<l n="6" num="2.2">O corps nouveau. Veux-tu ? j’affranchirai tes flancs.</l>
						<l n="7" num="2.3">Pour ma tempe fanée et pour mes cheveux blancs</l>
						<l n="8" num="2.4">Prends-moi, car un vieux maître est plus doux aux novices.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">Irrite par le feu les nymphes. Dans ce vase</l>
						<l n="10" num="3.2">— Tant le jour fut brûlant — lave ton corps laineux,</l>
						<l n="11" num="3.3">Et fais l’ampoule éclore en un <subst reason="analysis" hand="RR" type="phonemization"><del>8</del><add rend="hidden">neuf</add></subst> lumineux,</l>
						<l n="12" num="3.4">Qu’on te voie à cheval sur ce petit Pégase.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">Ah ! que de jougs avec ta chemise tu ôtes !…</l>
						<l n="14" num="4.2">Je t’offre des plaisirs sans amour, goûte-les.</l>
						<l n="15" num="4.3">La Passion veut des serments, fait des valets :</l>
						<l n="16" num="4.4">La riche Volupté, elle, n’a que des hôtes.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">A ceux qui te jetaient une obole il faut prendre</l>
						<l n="18" num="5.2">Un tribut, n’épargnant que moi qui t’enseignai.</l>
						<l n="19" num="5.3">De ta vertu jamais tu n’auras un denier,</l>
						<l n="20" num="5.4">Tu peux tirer bon prix de tes péchés à vendre.</l>
					</lg>
				</div></body></text></TEI>