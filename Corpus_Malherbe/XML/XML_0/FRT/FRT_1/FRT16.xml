<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE BOURDEAU DES NEUF PUCELLES</title>
				<title type="medium">Édition électronique</title>
				<author key="FRT">
					<name>
						<forename>Charles-Théophile</forename>
						<surname>FERET</surname>
					</name>
					<date from="1858" to="1928">1858-1928</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1111 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">FRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le Bourdeau des neuf pucelles</title>
						<author>Charles-Théophile Feret</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">https://www.gutenberg.org/ebooks/66781</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le Bourdeau des neuf pucelles</title>
								<author>Charles-Théophile Feret</author>
								<imprint>
									<pubPlace>CAUDÉRAN-BORDEAUX</pubPlace>
									<publisher>ÉDITIONS DES CAHIERS LITTÉRAIRES</publisher>
									<date when="1923">1923</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1912">1912</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et les notes de l’éditeur n’ont pas été reprises.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>
					Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique
					après application du programme de calcul de la longueur métrique des vers).
				</p>
				<p>Les notes de bas de page (numérotation conforme à l’édition imprimée) ont été reportées en fin de poème.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension (…).</p>
					<p>Les tirets simples ont été remplacés par des tirets demi-cadratin.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-12-09" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.4.xsd)</change>
				<change when="2021-12-10" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">POLYMNIE</head><head type="sub_part">Muse des Hymnes et des Chantsen l’honneur des dieux,des héros et des nymphes.</head><div type="poem" key="FRT16" rhyme="none">
					<head type="main">Ondine</head>
					<lg n="1">
						<l n="1" num="1.1">Entre mes bras fond la mollesse de ton torse. —</l>
						<l n="2" num="1.2">Quand une peine les métamorphose en source,</l>
						<l n="3" num="1.3">Je bois ta jeune vie à tes paupières douces.</l>
					</lg>
					<lg n="2">
						<l n="4" num="2.1">Sur ta langue, serpent qui se darde, se love,</l>
						<l n="5" num="2.2">Et se rebelle entre tes lèvres, mes esclaves,</l>
						<l n="6" num="2.3">Je lape avidement les sucs de ta salive.</l>
					</lg>
					<lg n="3">
						<l n="7" num="3.1">Ta féminité, sous tes cils d’aristocrate</l>
						<l n="8" num="3.2">Qui battent, mais non pas de pudeur hypocrite,</l>
						<l n="9" num="3.3">Me verse ton sang rose en sa coupe secrète.</l>
					</lg>
					<lg n="4">
						<l n="10" num="4.1">Et l’ardente sueur dont le plaisir t’embrase,</l>
						<l n="11" num="4.2">M’imprègne dans ton lit, pleurs d’aube sur la rose,</l>
						<l n="12" num="4.3">Perles chaudes aux seins d’une belle coureuse.</l>
					</lg>
					<lg n="5">
						<l n="13" num="5.1">Tes jambes dans le bain luisent comme la faille.</l>
						<l n="14" num="5.2">Et tu sembles par tes yeux glauques une fille</l>
						<l n="15" num="5.3">Des Eaux, qu’on entrevoit un instant sous les feuilles.</l>
					</lg>
					<lg n="6">
						<l n="16" num="6.1">Sans doute tu naquis du flot qui frise et mousse,</l>
						<l n="17" num="6.2">Et fus Nymphe chanteuse aux roseaux du Permesse ;</l>
						<l n="18" num="6.3">Oublieux d’Aréthuse Alphée eut tes prémices.</l>
					</lg>
					<lg n="7">
						<l n="19" num="7.1">C’est pourquoi sur un buis de flûte dolosive</l>
						<l n="20" num="7.2">Je fausse ces trois clefs, afin qu’elles déçoivent</l>
						<l n="21" num="7.3">Mais charment ton oreille, émue aux jeux suaves.</l>
					</lg>
					<lg n="8">
						<l n="22" num="8.1">Ma rime, Ondine dans le vent qui vire et valse,</l>
						<l n="23" num="8.2">Fluteau parmi les joncs, clairon sur la mer vaste,</l>
						<l n="24" num="8.3">Chuchote en la feuillée, et pleure dans la vasque.</l>
					</lg>
					<lg n="9">
						<l n="25" num="9.1">Puis, aux justes accords à son tour contribue</l>
						<l n="26" num="9.2">Ta sœur, la Nymphe Écho, dans tes grottes herbues ;</l>
						<l n="27" num="9.3">Et telle je te chante après que je t’ai bue.</l>
					</lg>
				</div></body></text></TEI>