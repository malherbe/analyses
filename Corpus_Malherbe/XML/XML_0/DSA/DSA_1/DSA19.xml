<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA COMÉDIE DU MONDE</title>
				<title type="medium">Édition électronique</title>
				<author key="DSA">
					<name>
						<forename>Alfred</forename>
						<nameLink>de</nameLink>
						<surname>ESSARTS</surname>
					</name>
					<date from="1811" to="1893">1811-1893</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>5551 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">DSA_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA COMÉDIE DU MONDE</title>
						<author>Alfred des Essarts</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k5446283w?rk=429186</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title type="main">LA COMÉDIE DU MONDE</title>
								<author>Alfred des Essarts</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher> Comptoir des imprimeurs-unis</publisher>
									<date when="1851">1851</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1851">1851</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension(…).</p>
					<p>Les tirets simples précédés et/ou suivis d’une espace ont été remplacés par des tirets demi-cadratin</p>
					<p>
						Le signe "_" (_) est utilisé à la place du trait d’union pour séparer les syllabes
						du mot "so-ci-a-lis-tes" afin de préserver l’unité du mot dans le traitement des rimes.
					</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-04-25" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-04-25" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DSA19">
				<head type="number">XVIII</head>
				<head type="main">L’ ADIEU DU POÈTE</head>
				<lg n="1">
					<l n="1" num="1.1"><space unit="char" quantity="2"></space>Monsieur de Cercourt mort, la comtesse perdue,</l>
					<l n="2" num="1.2">— Comme si dans la tombe elle fût descendue,</l>
					<l n="3" num="1.3">— Amélie hier belle et parée, aujourd’hui</l>
					<l n="4" num="1.4">Donnant à des souffrants ses soins et son appui ;</l>
					<l n="5" num="1.5">De ces êtres aimés l’un dans le ciel, et l’autre</l>
					<l n="6" num="1.6">Suivant péniblement le sentier de l’Apôtre ;</l>
					<l n="7" num="1.7">L’un goûtant le repos de l’éternel sommeil,</l>
					<l n="8" num="1.8">L’autre de ses remords éprouvant le réveil…</l>
					<l n="9" num="1.9">Plus d’amis… — Tel était le présent déplorable.</l>
					<l n="10" num="1.10">Paul Firmin ressentit l’angoisse d’un coupable</l>
					<l n="11" num="1.11">Qui cherche vainement — en horreur à ses yeux —</l>
					<l n="12" num="1.12">Une âme sympathique, un confident pieux.</l>
					<l n="13" num="1.13">Rien, plus rien !… Il se dit : « — A quoi bon dans ce monde</l>
					<l n="14" num="1.14">Promener le tableau de ma douleur profonde ?</l>
					<l n="15" num="1.15">A quoi bon soutenir des regards indiscrets ?</l>
					<l n="16" num="1.16">Avec de faux amis épancher mes secrets ?</l>
					<l n="17" num="1.17">A la société pour toujours je renonce.</l>
					<l n="18" num="1.18">Qu’entre elle et moi là-haut le Tout-Puissant prononce ;</l>
					<l n="19" num="1.19">Dans mon cœur ulcéré tout n’est plus que débris. »</l>
					<l n="20" num="1.20"><space unit="char" quantity="2"></space>Et sa colère ainsi tourna contre Paris :</l>
					<l n="21" num="1.21">« Adieu, séjour du vice ; adieu, cloaque infâme,</l>
					<l n="22" num="1.22">Où l’on cache si bien les stigmates de l’âme ;</l>
					<l n="23" num="1.23">Mais dont l’impureté, déguisée avec art,</l>
					<l n="24" num="1.24">N’a pu se dérober au feu de mon regard.</l>
					<l n="25" num="1.25">Va, je t’ai devinée et je te lis entière,</l>
					<l n="26" num="1.26">O nouvelle Astarté, vouée à la matière.</l>
					<l n="27" num="1.27">Je te hais, — ou plutôt je n’ai que du mépris</l>
					<l n="28" num="1.28">Pour les vices couverts du grand nom de Paris,</l>
					<l n="29" num="1.29">Pour la société frivole et dissolue</l>
					<l n="30" num="1.30">Que d’un absurde hommage on poursuit, on salue.</l>
					<l n="31" num="1.31">Qu’as-tu fait de ce nom, apanage d’honneur ?</l>
					<l n="32" num="1.32">Ta gloire a disparu ; que devient ta splendeur ?</l>
					<l n="33" num="1.33">Un jour peut-être, un jour on cherchera la trace</l>
					<l n="34" num="1.34">Du sol où maintenant ta multitude passe.</l>
					<l n="35" num="1.35">Puisse s’éteindre aussi jusqu’à ton souvenir !</l>
					<l n="36" num="1.36">Si tu l’avais voulu, tout devrait te bénir ;</l>
					<l n="37" num="1.37">Ton rôle était bien beau, cité grande entre toutes ;</l>
					<l n="38" num="1.38">Vers ton axe central se dirigeaient les routes ;</l>
					<l n="39" num="1.39">Tous les peuples, soumis à ta suprême loi,</l>
					<l n="40" num="1.40">Pour agir ou penser fixaient leurs yeux sur toi.</l>
					<l n="41" num="1.41">Mais lasse de remplir ce magnifique rôle,</l>
					<l n="42" num="1.42">Tu brisas ton épée et souillas ta parole.</l>
					<l n="43" num="1.43">A présent tu n’es plus qu’un vaste lupanar,</l>
					<l n="44" num="1.44">Où la corruption étale son bazar.</l>
					<l n="45" num="1.45">L’austérité te blesse et le talent t’offense.</l>
					<l n="46" num="1.46">Comme un vieux débauché qui penche vers l’enfance,</l>
					<l n="47" num="1.47">Tu ne demandes plus, au terme des plaisirs,</l>
					<l n="48" num="1.48">Qu’à raviver en toi quelques derniers désirs !</l>
					<l n="49" num="1.49">Paris, c’est un marché : tout s’y vend, la science,</l>
					<l n="50" num="1.50">Le travail, la pudeur, — jusqu’à la conscience.</l>
					<l n="51" num="1.51">L’amitié n’est qu’un mot qui résonne et qui ment ;</l>
					<l n="52" num="1.52">L’amour, absent du cœur, promène un faux serment ;</l>
					<l n="53" num="1.53">L’impureté se met un fard d’hypocrisie ;</l>
					<l n="54" num="1.54">Tout conspire à tromper la foi, la poésie.</l>
					<l n="55" num="1.55">L’homme emploie à mal faire un semblant de raison.</l>
					<l n="56" num="1.56">S’il vous ouvre les bras, traduisez : trahison.</l>
					<l n="57" num="1.57">Il nuit pour nuire ; il est dévoré par l’envie ;</l>
					<l n="58" num="1.58">Au culte du Veau-d’Or il a voué sa vie ;</l>
					<l n="59" num="1.59">Et plus souvent le fer brillerait dans sa main,</l>
					<l n="60" num="1.60">S’il ne trouvait la loi qui l’arrête en chemin.</l>
					<l n="61" num="1.61">Depuis dix-huit cents ans, à ce démon d’argile</l>
					<l n="62" num="1.62">Dieu daigne présenter le pain de l’Évangile :</l>
					<l n="63" num="1.63">Mais loin d’en être ému, loin d’en être meilleur,</l>
					<l n="64" num="1.64">L’homme est plus que jamais froid, sceptique et railleur,</l>
					<l n="65" num="1.65">Nous ressembler à Dieu ! quel ridicule songe !</l>
					<l n="66" num="1.66">Dieu, c’est la vérité ; — l’homme, c’est le mensonge.</l>
					<l n="67" num="1.67">Oh ! oui, la décadence et la destruction</l>
					<l n="68" num="1.68">Te menacent enfin, ô vieille nation ;</l>
					<l n="69" num="1.69">A la décrépitude arrive ton génie ;</l>
					<l n="70" num="1.70">Car l’avenir n’est plus au peuple qui renie</l>
					<l n="71" num="1.71">Son culte, son passé, ses lois et ses aïeux.</l>
					<l n="72" num="1.72">Pays dégénéré, je te fais mes adieux.</l>
					<l n="73" num="1.73">Salut, suprême vœu, rêve de solitude !</l>
					<l n="74" num="1.74">Fuir les hommes sera désormais mon étude.</l>
					<l n="75" num="1.75">Autant j’avais pour eux d’ardente charité,</l>
					<l n="76" num="1.76">Autant j’ai de mépris pour leur iniquité.</l>
					<l n="77" num="1.77">Je ne les verrai plus. Je la hais, cette race</l>
					<l n="78" num="1.78">Où toute affection n’est plus qu’une grimace ;</l>
					<l n="79" num="1.79">Car sur ses traits flétris on chercherait en vain</l>
					<l n="80" num="1.80">Le sceau que leur donna le Créateur divin.</l>
					<l n="81" num="1.81">C’en est fait : ma douleur a comblé la mesure.</l>
					<l n="82" num="1.82">Adieu, séjour du vice ; adieu donc, ville impure ;</l>
					<l n="83" num="1.83">Adieu, toi que Satan tient sous son pied vainqueur.</l>
					<l n="84" num="1.84">Tu n’auras point laissé de regrets dans mon cœur ! »</l>
				</lg>
				<lg n="2">
					<l n="85" num="2.1"><space unit="char" quantity="2"></space>C’est ainsi que Firmin exhala sa colère.</l>
					<l n="86" num="2.2">Je conviens qu’il était par trop atrabilaire…</l>
					<l n="87" num="2.3">Mais au fond de son cœur il puisait le mépris ;</l>
					<l n="88" num="2.4">A travers ses douleurs il contemplait Paris…</l>
					<l n="89" num="2.5">De la société s’il se faisait le juge,</l>
					<l n="90" num="2.6">C’est qu’il ne pouvait plus y trouver un refuge.</l>
				</lg>
				<lg n="3">
					<l n="91" num="3.1"><space unit="char" quantity="2"></space>Le poëte, qui vit loin de notre milieu,</l>
					<l n="92" num="3.2">Doit à son insu même exagérer un peu.</l>
					<l n="93" num="3.3">Vous qui passez, avec un sourire à la bouche,</l>
					<l n="94" num="3.4">Auprès de ce rêveur au visage farouche ;</l>
					<l n="95" num="3.5">Vous qui ne le voyez qu’au hasard, un moment,</l>
					<l n="96" num="3.6">Sans vous associer à son muet tourment ;</l>
					<l n="97" num="3.7">Ou lui jetez avec un regard d’ironie</l>
					<l n="98" num="3.8">Cette fausse pitié, seul paîment du génie ;</l>
					<l n="99" num="3.9">Allez, suivez la route où vous marchez heureux,</l>
					<l n="100" num="3.10">Et ne soupçonnez pas les destins ténébreux.</l>
					<l n="101" num="3.11">Ne vous dites jamais qu’il est, sur cette terre,</l>
					<l n="102" num="3.12">Des êtres dont la vie est morne et solitaire ;</l>
					<l n="103" num="3.13">Que l’aspect du plaisir de vos cœurs enivrés</l>
					<l n="104" num="3.14">Fait plus cruellement sentir les jours pleurés.</l>
					<l n="105" num="3.15">Passez, le front couvert de brillantes couronnes ;</l>
					<l n="106" num="3.16">Comptez de beaux printemps et n’ayez pas d’automnes ;</l>
					<l n="107" num="3.17">Que la joie et l’amour accompagnent vos pas ;</l>
					<l n="108" num="3.18">De vous il n’attend rien… <hi rend="ital">Car vous ne savez pas</hi> !</l>
				</lg>
			</div></body></text></TEI>