<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA COMÉDIE DU MONDE</title>
				<title type="medium">Édition électronique</title>
				<author key="DSA">
					<name>
						<forename>Alfred</forename>
						<nameLink>de</nameLink>
						<surname>ESSARTS</surname>
					</name>
					<date from="1811" to="1893">1811-1893</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>5551 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">DSA_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA COMÉDIE DU MONDE</title>
						<author>Alfred des Essarts</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k5446283w?rk=429186</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title type="main">LA COMÉDIE DU MONDE</title>
								<author>Alfred des Essarts</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher> Comptoir des imprimeurs-unis</publisher>
									<date when="1851">1851</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1851">1851</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension(…).</p>
					<p>Les tirets simples précédés et/ou suivis d’une espace ont été remplacés par des tirets demi-cadratin</p>
					<p>
						Le signe "_" (_) est utilisé à la place du trait d’union pour séparer les syllabes
						du mot "so-ci-a-lis-tes" afin de préserver l’unité du mot dans le traitement des rimes.
					</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-04-25" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-04-25" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DSA21">
				<head type="main">CONCLUSION</head>
				<head type="sub">HUIT ANS APRÈS</head>
				<lg n="1">
					<l n="1" num="1.1">A la fin du récit — ou bien de mon histoire, —</l>
					<l n="2" num="1.2">Car elle est vraisemblable et vous pouvez y croire, —</l>
					<l n="3" num="1.3">Me voici parvenu. Reposons-nous, lecteur,</l>
					<l n="4" num="1.4">Et daignez excuser les fautes de l’auteur.</l>
					<l n="5" num="1.5">Rimant tant bien que mal, j’ai traversé l’épreuve ;</l>
					<l n="6" num="1.6">D’autres feront ainsi : la route n’est pas neuve ;</l>
					<l n="7" num="1.7">J’y vois briller Boileau, La Fontaine, Gresset.</l>
					<l n="8" num="1.8">Du roman de la <hi rend="ital">Rose</hi> aux Contes de Musset,</l>
					<l n="9" num="1.9">Le récit familier eut plus d’une épopée.</l>
					<l n="10" num="1.10">La Muse ne vit pas que de grands coups d’épée.</l>
					<l n="11" num="1.11">Si je vous ai distrait, j’ai ma grâce à vos yeux :</l>
					<l n="12" num="1.12"><hi rend="ital">Tous les genres sont bons, hors le genre ennuyeux.</hi></l>
				</lg>
				<lg n="2">
					<l n="13" num="2.1">Quelques détails encore, avant que je termine.</l>
				</lg>
				<lg n="3">
					<l n="14" num="3.1"><space unit="char" quantity="2"></space>La baronne Wolgande-Ulrique-Wilhelmine,</l>
					<l n="15" num="3.2">Très-riche Hollandaise, a pris pour son époux</l>
					<l n="16" num="3.3">Un monsieur <hi rend="ital">de</hi> Caron, assez connu de vous.</l>
					<l n="17" num="3.4">De son charmant Français cette veuve raffole ;</l>
					<l n="18" num="3.5">Amour dont quatre enfants sont le vivant symbole.</l>
					<l n="19" num="3.6">Caron, content du sort, ne demande plus rien ;</l>
					<l n="20" num="3.7">Il trouve qu’ici-bas tout est réglé très-bien,</l>
					<l n="21" num="3.8">Et que les mécontents, qui n’ont pas de quoi vivre,</l>
					<l n="22" num="3.9">Sont des gueux dont il faut que l’État se délivre.</l>
					<l n="23" num="3.10">Fort tranquille à La Haye, il est très-gros, très-gras.</l>
					<l n="24" num="3.11"><space unit="char" quantity="2"></space>Le duc s’est retiré dans son château… là-bas…</l>
					<l n="25" num="3.12">Vous savez ? — Non. — La chose est fort indifférente :</l>
					<l n="26" num="3.13">Le bonheur suit partout cent mille écus de rente,</l>
					<l n="27" num="3.14"><space unit="char" quantity="2"></space>Enguerrand joue au whist ; on peut vivre du jeu.</l>
					<l n="28" num="3.15"><space unit="char" quantity="2"></space>Fortunat, journaliste, est rouge — blanc — ou bleu.</l>
					<l n="29" num="3.16"><space unit="char" quantity="2"></space>Florida ? — Tous les soirs la belle se promène</l>
					<l n="30" num="3.17">Du passage Jouffroy jusqu’à la Madeleine.</l>
					<l n="31" num="3.18"><space unit="char" quantity="2"></space>Forbain ? — Il est préfet. Mais jetant le poignard,</l>
					<l n="32" num="3.19">Il a voué sa haine au parti montagnard.</l>
					<l n="33" num="3.20"><space unit="char" quantity="2"></space>Et Gospur ? — Le profond Gospur fut aux Finances,</l>
					<l n="34" num="3.21">Et nous reconnaissions ses belles ordonnances.</l>
					<l n="35" num="3.22">Pour mieux nous dégrever il quadruplait l’impôt…</l>
					<l n="36" num="3.23">Nous n’eûmes pas encor par lui la <hi rend="ital">poule au pot</hi>.</l>
					<l n="37" num="3.24"><space unit="char" quantity="2"></space>Rétho ? — De ses <hi rend="ital">canards</hi> nous sommes tous victimes ;</l>
					<l n="38" num="3.25">Il les sème partout, au prix de cinq centimes.</l>
					<l n="39" num="3.26">Pour toi, Peuple, il se fait un Brutus, un Caton :</l>
					<l n="40" num="3.27">S’il était au pouvoir, il changerait dé ton.</l>
					<l n="41" num="3.28"><space unit="char" quantity="2"></space>Et Bennotti ? — Dans Rome à son aise il pérore ;</l>
					<l n="42" num="3.29">Il cite Régulus et vingt héros encore ;</l>
					<l n="43" num="3.30">A propos d’un bon Pape, il crie aux Borgias,</l>
					<l n="44" num="3.31">Et sur les <hi rend="ital">Raphaël</hi> il fait des <hi rend="ital">razzias</hi> <ref target="1" type="noteAnchor">(1)</ref>.</l>
					<l n="45" num="3.32"><space unit="char" quantity="2"></space>Crampon ? — Il combattit pour une triste cause.</l>
					<l n="46" num="3.33">Du moins il était brave… A présent, il repose…</l>
					<l n="47" num="3.34">Sur les pavés de Juin il reçut le trépas</l>
					<l n="48" num="3.35">Pour ceux qui le poussaient et ne se battaient pas.</l>
					<l n="49" num="3.36"><space unit="char" quantity="2"></space>Rosencrantz est en Suisse avec notre Montagne.</l>
					<l n="50" num="3.37">Contre le Sonderbund il s’est mis en campagne.</l>
					<l n="51" num="3.38">Monsieur Druey l’estime… Ils s’honorent tous deux…</l>
					<l n="52" num="3.39">L’amitié d’un grand homme est un bienfait des dieux.</l>
					<l n="53" num="3.40"><space unit="char" quantity="2"></space>Revenu des honneurs, Jacob, dans sa boutique,</l>
					<l n="54" num="3.41">Songe au temps où sa main tenait la République.</l>
					<l n="55" num="3.42">A Denys le tyran s’assimilant parfois,</l>
					<l n="56" num="3.43">Jacob s’est consolé par l’exemple des rois.</l>
					<l n="57" num="3.44"><space unit="char" quantity="2"></space>Vendant la <hi rend="ital">Voix du Peuple</hi>, Aristide s’exerce</l>
					<l n="58" num="3.45">A hurler dans la rue… — Un utile commerce !</l>
					<l n="59" num="3.46"><space unit="char" quantity="2"></space>Rasant civiquement les mentons trop garnis,</l>
					<l n="60" num="3.47">Boudet tient magasin <hi rend="ital">aux Coiffeurs réunis</hi>.</l>
					<l n="61" num="3.48"><space unit="char" quantity="2"></space>Attendant que la France encore s’émancipe,</l>
					<l n="62" num="3.49">Flacon boit de la bière et culotte sa pipe.</l>
					<l n="63" num="3.50"><space unit="char" quantity="2"></space>Pierrefitte s’est fait courtier d’élections,</l>
					<l n="64" num="3.51">Et de tous les drapeaux a des échantillons.</l>
					<l n="65" num="3.52"><space unit="char" quantity="2"></space>Et le reste du club ? — Hélas ! les pauvres diables</l>
					<l n="66" num="3.53">Ont eu, pour la plupart, des destins misérables.</l>
					<l n="67" num="3.54">C’est un triste métier que de passer le temps.</l>
					<l n="68" num="3.55">A nourrir des complots ; clubistes mécontents,</l>
					<l n="69" num="3.56">Comprenez donc enfin que de la barricade</l>
					<l n="70" num="3.57">Il ne peut s’élever qu’un pouvoir bien malade.</l>
					<l n="71" num="3.58"><space unit="char" quantity="2"></space>Et que fait à présent notre ami Duriveau</l>
					<l n="72" num="3.59">A qui, n’en fût-il plus, il fallait du nouveau</l>
					<l n="73" num="3.60">Ce statuaire <hi rend="ital">oscur</hi>, qui taillait des colosses,</l>
					<l n="74" num="3.61">A quelques écoliers fait copier des bosses,</l>
					<l n="75" num="3.62">S’estimant fort heureux lorsque Susse et Giroux</l>
					<l n="76" num="3.63">Lui donnent à gagner deux pièces de cent sous.</l>
					<l n="77" num="3.64"><space unit="char" quantity="2"></space>Et Bardoche ? — On l’a pris pour arranger le Louvre.</l>
					<l n="78" num="3.65">De chefs-d’œuvre inconnus Dieu sait ce qu’il découvre ;</l>
					<l n="79" num="3.66">Mais, en revanche,il met souvent à contre-jour</l>
					<l n="80" num="3.67">Les chefs-d’œuvre connus, et c’est un mauvais tour</l>
					<l n="81" num="3.68">Pour les Maîtres : ils ont, trop brillants ou trop sombres,</l>
					<l n="82" num="3.69">De l’ombre sur leurs clairs et du clair sur leurs ombres.</l>
					<l n="83" num="3.70"><space unit="char" quantity="2"></space>Vouzin ? — Il est pédant comme par le passé ;</l>
					<l n="84" num="3.71">Il restera pédant, même étant trépassé.</l>
					<l n="85" num="3.72"><space unit="char" quantity="2"></space>Zacharie à l’État prépare un héritage,</l>
					<l n="86" num="3.73">Et sa Californie est au sixième étage.</l>
					<l n="87" num="3.74"><space unit="char" quantity="2"></space>Saint-Makaire ? — On le dit un très-fort <hi rend="ital">coupletier</hi> ;</l>
					<l n="88" num="3.75">Il mettrait en chansons le Jugement dernier.</l>
					<l n="89" num="3.76">Du reste, avec des tiers, des quarts de vaudeville</l>
					<l n="90" num="3.77">Notre homme a maintenant une maison en ville ;</l>
					<l n="91" num="3.78">Ce qu’il fait ne vaut rien, il en est convaincu :</l>
					<l n="92" num="3.79">Mais il voulait bien vivre… Il aura bien vécu.</l>
				</lg>
				<lg n="4">
					<l part="I" n="93" num="4.1"><space unit="char" quantity="2"></space>Et Firmin ? </l>
					<l part="F" n="93" num="4.1">Dans un coin de l’Archipel, une île</l>
					<l n="94" num="4.2">Pour le triste exilé fut un dernier asile.</l>
					<l n="95" num="4.3">Là, devant le flot calme et le ciel azuré,</l>
					<l n="96" num="4.4">Paul évoque un passé pénible, mais pleuré ;</l>
					<l n="97" num="4.5">Il n’a plus de patrie, il dédaigne la gloire…</l>
					<l n="98" num="4.6">Sa vie est tout entière au fond de sa mémoire.</l>
				</lg>
				<lg n="5">
					<l n="99" num="5.1"><space unit="char" quantity="2"></space>Aux pauvres, Amélie a consacré ses soins.</l>
					<l n="100" num="5.2">Sans espoir de bonheur elle est calme du moins.</l>
					<l n="101" num="5.3">Dieu lui pardonnera sans doute. Chaque année,</l>
					<l n="102" num="5.4">Dans le champ de la mort on la voit prosternée</l>
					<l n="103" num="5.5">Et, des fleurs dans la main, contemplant à genoux</l>
					<l n="104" num="5.6">La tombe où la douleur a plongé son époux.</l>
				</lg>
				<closer>
					<note id="(1)" type="footnote">Ces vers datent de 1848.</note>
				</closer>
			</div></body></text></TEI>