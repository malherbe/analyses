<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA COMÉDIE DU MONDE</title>
				<title type="medium">Édition électronique</title>
				<author key="DSA">
					<name>
						<forename>Alfred</forename>
						<nameLink>de</nameLink>
						<surname>ESSARTS</surname>
					</name>
					<date from="1811" to="1893">1811-1893</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>5551 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">DSA_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA COMÉDIE DU MONDE</title>
						<author>Alfred des Essarts</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k5446283w?rk=429186</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title type="main">LA COMÉDIE DU MONDE</title>
								<author>Alfred des Essarts</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher> Comptoir des imprimeurs-unis</publisher>
									<date when="1851">1851</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1851">1851</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension(…).</p>
					<p>Les tirets simples précédés et/ou suivis d’une espace ont été remplacés par des tirets demi-cadratin</p>
					<p>
						Le signe "_" (_) est utilisé à la place du trait d’union pour séparer les syllabes
						du mot "so-ci-a-lis-tes" afin de préserver l’unité du mot dans le traitement des rimes.
					</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-04-25" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-04-25" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DSA17">
				<head type="number">XVI</head>
				<head type="main">LE DUEL</head>
				<lg n="1">
					<l n="1" num="1.1"><space unit="char" quantity="2"></space>Avoir pour un combat engagé sa parole ;</l>
					<l n="2" num="1.2">Un combat dont la cause est souvent bien frivole ;</l>
					<l n="3" num="1.3">Se dire que l’on est jeune, heureux, fort et beau,</l>
					<l n="4" num="1.4">Et que demain peut-être en un sombre tombeau</l>
					<l n="5" num="1.5">L’on descendra, passant d’une vie enchantée</l>
					<l n="6" num="1.6">A la cité de deuil par la mort habitée !</l>
					<l n="7" num="1.7">La vengeance, déjà savourant son plaisir,</l>
					<l n="8" num="1.8">Ouvre ses mains d’acier afin de vous saisir.</l>
					<l n="9" num="1.9"><space unit="char" quantity="2"></space>Demain… que de pensers se croisent dans la tête !</l>
					<l n="10" num="1.10">Pour l’un des combattants l’éternité s’apprête</l>
					<l n="11" num="1.11">C’est peu qu’il tombe mort sous le fer ou le feu,</l>
					<l n="12" num="1.12">Là-haut il subira le jugement de Dieu.</l>
					<l n="13" num="1.13">Qui pourrait sans frémir accepter cette épreuve</l>
					<l n="14" num="1.14">Et voir loin de son corps errer son âme veuve</l>
					<l n="15" num="1.15">Et dire : « J’étais jeune, heureux, brillant et beau,</l>
					<l n="16" num="1.16">« Tout cela pour descendre, avant l’âge, au tombeau ! »</l>
					<l n="17" num="1.17"><space unit="char" quantity="2"></space>Fatale loi d’honneur ! préjugé de ce monde</l>
					<l n="18" num="1.18">Où, quand la vérité s’enfuit, l’erreur abonde,</l>
					<l n="19" num="1.19">Feras-tu donc toujours sur les autels humains</l>
					<l n="20" num="1.20">Couler des flots de sang répandu par nos mains !</l>
					<l n="21" num="1.21">Il le faut cependant lorsqu’avec artifice</l>
					<l n="22" num="1.22">Au sein de nos foyers l’impureté se glisse,</l>
					<l n="23" num="1.23">Lorsqu’un nom est taché, lorsque l’excès du mal</l>
					<l n="24" num="1.24">Ne peut se réparer devant un tribunal.</l>
					<l n="25" num="1.25">Alors on cherche en soi la force et le refuge ;</l>
					<l n="26" num="1.26">D’offensé qu’on était on se transforme en juge ;</l>
					<l n="27" num="1.27">A défaut de la loi qui, publique, pourrait</l>
					<l n="28" num="1.28">N’être point pour l’honneur un remède discret,</l>
					<l n="29" num="1.29">On ne doit plus entendre, au sortir de l’outrage,</l>
					<l n="30" num="1.30">Que cette conscience, arbitre du courage ;</l>
					<l n="31" num="1.31">Et malheur à celui qui s’arrête au débat,</l>
					<l n="32" num="1.32">Quand un devoir sacré lui montre le combat !</l>
					<l n="33" num="1.33"><space unit="char" quantity="2"></space>Midi sonnait. — Au Bois deux voitures entrèrent</l>
					<l n="34" num="1.34">Et non loin de Longchamps à l’écart s’arrêtèrent.</l>
					<l n="35" num="1.35">Nul promeneur encor ne troublait le repos</l>
					<l n="36" num="1.36">Du lieu que le duel a choisi pour champ-clos.</l>
					<l n="37" num="1.37">D’ailleurs, un voile gris, favorable au mystère,</l>
					<l n="38" num="1.38">Étendait son linceul du ciel jusqu’à la terre.</l>
					<l n="39" num="1.39">Il avait plu la veille et, malgré la saison,</l>
					<l n="40" num="1.40">Une froide rosée humectait le gazon.</l>
					<l n="41" num="1.41">Les oiseaux frissonnants suspendaient leur ramage ;</l>
					<l n="42" num="1.42">Tout semblait annoncer quelque sombre présage ;</l>
					<l n="43" num="1.43">Mais certains du succès, les deux témoins d’Arthur,</l>
					<l n="44" num="1.44">Sachant que nul n’avait le bras ni l’œil plus sûr</l>
					<l n="45" num="1.45">Et qu’il eût défié jusqu’au fameux Saint-George,</l>
					<l n="46" num="1.46">Aux dépens de Firmin riaient à pleine gorge.</l>
					<l n="47" num="1.47">« — Ah ! disait Enguerrand, ce brave paladin</l>
					<l n="48" num="1.48">Sur nos joyeux soupers déverse son dédain,</l>
					<l n="49" num="1.49">Et s’en vient, d’une voix austère et magistrale,</l>
					<l n="50" num="1.50">Au moment du dessert, nous prêcher sa morale !</l>
					<l n="51" num="1.51">Il apprendra bientôt ( j’en suis fâché pour lui)</l>
					<l n="52" num="1.52">Qu’il faut moins se mêler des affaires d’autrui.</l>
					<l n="53" num="1.53">Peste ! si des censeurs on n’écartait l’espèce,</l>
					<l n="54" num="1.54">On devrait dans le noir enterrer sa jeunesse.</l>
					<l part="I" n="55" num="1.55">Qu’en dis-tu ? </l>
					<l part="F" n="55" num="1.55">— Sur ma foi, dit Arthur en bâillant,</l>
					<l n="56" num="1.56">C’était pour en finir le seul expédient.</l>
					<l n="57" num="1.57">Semblable au Commandeur, cet homme sans relâche</l>
					<l n="58" num="1.58">De m’obséder partout s’était donné la tâche.</l>
					<l n="59" num="1.59">Il voulait obtenir une leçon de moi…</l>
					<l n="60" num="1.60">Il l’aura, je ne fais qu’obéir à sa loi.</l>
					<l n="61" num="1.61">Mais se lever si tôt, c’est une chose rude :</l>
					<l n="62" num="1.62">Car de compter midi je n’ai pas l’habitude. »</l>
					<l n="63" num="1.63"><space unit="char" quantity="2"></space>Paul était descendu, sans trouble ni lenteur,</l>
					<l n="64" num="1.64">Comme s’il n’eût été que simple spectateur ;</l>
					<l n="65" num="1.65">Le sang-froid se lisait sur son mâle visage :</l>
					<l n="66" num="1.66">Rozemon en conçut une secrète rage ;</l>
					<l n="67" num="1.67">Et bouillonnant d’ardeur devant son ennemi,</l>
					<l n="68" num="1.68">Par son talent d’escrime, il était raffermi.</l>
					<l n="69" num="1.69"><space unit="char" quantity="2"></space>Cependant Paul Firmin, élevant sa voix grave,</l>
					<l part="I" n="70" num="1.70">Lui dit : </l>
					<l part="F" n="70" num="1.70">« — D’un préjugé je ne suis pas esclave.</l>
					<l n="71" num="1.71">Le courage, à mes yeux, n’est pas dans un duel</l>
					<l n="72" num="1.72">Où souvent le bon droit reçoit le coup mortel.</l>
					<l n="73" num="1.73">Je suis chrétien ; le sang me répugne à répandre.</l>
					<l n="74" num="1.74">Pour la dernière fois, Monsieur, veuillez m’entendre.</l>
					<l n="75" num="1.75">Je ne vous ai tenu qu’un langage d’honneur :</l>
					<l n="76" num="1.76">Si la nuit a calmé votre accès de fureur,</l>
					<l n="77" num="1.77">Et si, pour réparer votre conduite infâme,</l>
					<l n="78" num="1.78">Vous rendez le dépôt que de vous on réclame,</l>
					<l n="79" num="1.79">Je jugerai pour moi l’outrage réparé,</l>
					<l n="80" num="1.80">Et sans rien exiger de plus je partirai.</l>
					<l n="81" num="1.81">— La proposition me paraît assez forte,</l>
					<l n="82" num="1.82">Répondit Rozemon ; en finir de la sorte</l>
					<l n="83" num="1.83">Ce serait très-commode : ainsi, sans risquer rien</l>
					<l n="84" num="1.84">Dans le monde on pourrait faire l’homme de tien !</l>
					<l n="85" num="1.85">On pourrait des vertus promener l’étalage !</l>
					<l n="86" num="1.86">Vous êtes insolent… ayez donc du courage. »</l>
					<l n="87" num="1.87"><space unit="char" quantity="2"></space>Et le baron avait un sourire moqueur</l>
					<l n="88" num="1.88">Comme s’il tenait Paul sous son genou vainqueur.</l>
					<l n="89" num="1.89"><space unit="char" quantity="2"></space>Enguerrand, du fourreau tira les deux épées.</l>
					<l n="90" num="1.90">« — Quand par l’aveuglement les âmes sont frappées,</l>
					<l n="91" num="1.91">Dit Paul sans s’émouvoir, en vain la charité</l>
					<l n="92" num="1.92">Donne un avis prudent… il n’est pas écouté.</l>
					<l n="93" num="1.93">J’ignore quel sera le sort de cette lutte :</l>
					<l n="94" num="1.94">Ferme dans le projet qu’il faut que j’exécute,</l>
					<l n="95" num="1.95">Je voulais vous offrir un suprême moyen</l>
					<l n="96" num="1.96">De montrer quelque honneur… Mais vous ne sentez rien !</l>
					<l n="97" num="1.97">Le cœur est mort en vous. D’après votre réponse</l>
					<l n="98" num="1.98">Combattons maintenant, et que le ciel prononce !</l>
					<l n="99" num="1.99">Je le répète ici : vous seul m’avez forcé</l>
					<l n="100" num="1.100">D’en venir au défi que je vous ai lancé. »</l>
					<l n="101" num="1.101"><space unit="char" quantity="2"></space>Ils engagent la lutte. Également habiles,</l>
					<l n="102" num="1.102">Pour se toucher ils font des efforts inutiles ;</l>
					<l n="103" num="1.103">Le fer heurte le fer, et dans ce cliquetis</l>
					<l n="104" num="1.104">Par mille chocs soudains les coups sont amortis.</l>
					<l n="105" num="1.105">Dans un espace étroit le duel se resserre ;</l>
					<l n="106" num="1.106">Chacun des combattants juge son adversaire ;</l>
					<l n="107" num="1.107">Tous deux, sans avancer, sans reculer non plus,</l>
					<l n="108" num="1.108">A poursuivre leur œuvre ils sont bien résolus.</l>
					<l n="109" num="1.109">Une sueur glacée inonde leur visage…</l>
					<l n="110" num="1.110">Leur force diminue, et non pas leur courage.</l>
					<l n="111" num="1.111"><space unit="char" quantity="2"></space>Arthur, impatient, s’irrite des lenteurs</l>
					<l n="112" num="1.112">Qui tout bas font trembler les muets spectateurs.</l>
					<l n="113" num="1.113">On n’entend que le bruit sinistre de l’épée ;</l>
					<l n="114" num="1.114">Et les témoins d’Arthur, dont l’attente est trompée,</l>
					<l n="115" num="1.115">Sans oser se parler s’interrogent des yeux.</l>
					<l n="116" num="1.116"><space unit="char" quantity="2"></space>Quelqu’un s’est écrié : « — Reposez-vous, Messieurs. »</l>
					<l n="117" num="1.117">Par un commun accord il se fait une trêve.</l>
					<l n="118" num="1.118">Arthur baisse le front lorsque Paul le relève :</l>
					<l n="119" num="1.119">Le premier, fatigué, pâle, tout abattu ;</l>
					<l n="120" num="1.120">Le second, calme et fort du droit de sa vertu.</l>
					<l n="121" num="1.121"><space unit="char" quantity="2"></space>Pendant cet intervalle, oh ! de quelle pensée</l>
					<l n="122" num="1.122">L’âme du libertin dut être traversée !</l>
					<l n="123" num="1.123">C’était l’éternité que ce cruel moment</l>
					<l n="124" num="1.124">Où s’évanouissait tout son aveuglement.</l>
					<l n="125" num="1.125">Dans l’art des spadassins il était passé maître :</l>
					<l n="126" num="1.126">Il y trouve un égal… et son vainqueur peut-être ;</l>
					<l n="127" num="1.127">Il mesure déjà les horreurs du néant…</l>
					<l n="128" num="1.128">Il croit voir sous ses pieds un abîme béant</l>
					<l n="129" num="1.129">Qui s’ouvre, s’ouvre encore et sans pitié l’attire…</l>
					<l n="130" num="1.130">Un indicible effroi lui donne le délire…</l>
					<l n="131" num="1.131">Car il le sent : la mort apparaît aujourd’hui !</l>
					<l n="132" num="1.132">La mort, à vingt-cinq ans !… Elle est là, devant lui !</l>
					<l n="133" num="1.133">Et ce n’est pas au ciel que monte sa prière.</l>
					<l n="134" num="1.134">Lui prier ! Non,pas même à son heure dernière !</l>
					<l n="135" num="1.135">Demander son salut à Firmin ! Non, jamais !</l>
					<l n="136" num="1.136">« — L’enfer le veut, dit-il ; eh bien ! je me soumets. »</l>
					<l n="137" num="1.137"><space unit="char" quantity="2"></space>Il cherche du regard son épée impuissante,</l>
					<l n="138" num="1.138">Rappelle son audace et sa vigueur absente.</l>
					<l n="139" num="1.139">L’émotion lui met un voile sur les yeux.</l>
					<l n="140" num="1.140"><space unit="char" quantity="2"></space>« — Recommençons, » dit Paul, encor plus sérieux.</l>
					<l n="141" num="1.141"><space unit="char" quantity="2"></space>Un éclair de fureur sillonna le visage</l>
					<l n="142" num="1.142">D’Arthur, qui rougissait de son peu de courage.</l>
					<l n="143" num="1.143">Il saisit son épée, et d’un bond se plaça</l>
					<l n="144" num="1.144">Devant Paul… Un moment terrible se passa.</l>
					<l n="145" num="1.145">Arthur multipliait ses coups avec furie,</l>
					<l n="146" num="1.146">Au hasard, sans chercher à protéger sa vie.</l>
					<l n="147" num="1.147">La lutte devenait inégale : Firmin</l>
					<l n="148" num="1.148">Vingt fois du cœur d’Arthur eût trouvé le chemin.</l>
					<l n="149" num="1.149">Mais frapper sans pitié répugnait à son âme :</l>
					<l n="150" num="1.150">Car il ne voyait plus qu’un homme dans l’infâme ;</l>
					<l n="151" num="1.151">Et pour lui, quel que fût l’excès de son mépris,</l>
					<l n="152" num="1.152">Au sang d’un criminel il attachait du prix.</l>
					<l n="153" num="1.153">Trompé sur le motif d’une bonté si grande,</l>
					<l n="154" num="1.154">Arthur redouble ; il faut que Firmin se défende…</l>
					<l n="155" num="1.155">Son ennemi s’élance, et par son propre effort</l>
					<l n="156" num="1.156">Atteint profondément, chancelle et tombe mort.</l>
					<l n="157" num="1.157">Un seul cri de douleur est sorti de sa bouche</l>
					<l n="158" num="1.158">Où l’on peut lire encor la menace farouche.</l>
					<l n="159" num="1.159"><space unit="char" quantity="2"></space>Firmin resta muet, triste, réfléchissant.</l>
					<l n="160" num="1.160">L’âme d’Arthur était partie avec son sang.</l>
					<l n="161" num="1.161">Une égale stupeur régnait de part et d’autre.</l>
					<l n="162" num="1.162"><space unit="char" quantity="2"></space>« — Votre œuvre est achevée, et je remplis la nôtre,</l>
					<l n="163" num="1.163">Dit Enguerrand ; voici les papiers réclamés.</l>
					<l n="164" num="1.164">Tenez. Dans cette boîte ils sont tous renfermés.</l>
					<l n="165" num="1.165"><space unit="char" quantity="2"></space>— Hélas ! murmura Paul, ce dénoûment m’afflige :</l>
					<l n="166" num="1.166">Mais l’orgueil est mortel lorsqu’il mène au vertige. »</l>
					<l n="167" num="1.167"><space unit="char" quantity="2"></space>Cela dit, il s’éloigne après avoir remis</l>
					<l n="168" num="1.168">Le cadavre d’Arthur aux mains de ses amis.</l>
				</lg>
				<lg n="2">
					<l n="169" num="2.1"><space unit="char" quantity="2"></space>Le lendemain, partout se lisait la nouvelle.</l>
					<l n="170" num="2.2">Mais comme on n’avait point divulgué la querelle,</l>
					<l n="171" num="2.3">La même version courut dans les journaux,</l>
					<l n="172" num="2.4">Qui se font des emprunts, tout en étant rivaux.</l>
					<l n="173" num="2.5">Nos graves <hi rend="ital">Moniteur</hi> donnaient pour authentique,</l>
					<l n="174" num="2.6">D’après renseignements, qu’un <hi rend="ital">motif politique</hi></l>
					<l n="175" num="2.7"><hi rend="ital">N’était pas étranger à cet événement</hi>.</l>
					<l n="176" num="2.8">Or des amis d’Arthur quel fut l’étonnement</l>
					<l n="177" num="2.9">De voir que l’on avait transformé la dispute</l>
					<l n="178" num="2.10">Et changé sans façon le terrain de la lutte !</l>
					<l n="179" num="2.11">« — Ce pauvre Rozemon, dit Enguerrand, je croi</l>
					<l n="180" num="2.12">Qu’il n’en savait pas plus, Messieurs, que vous et moi</l>
					<l n="181" num="2.13">En semblable matière ; il eût été capable</l>
					<l n="182" num="2.14">De prendre un député pour un saint véritable.</l>
					<l n="183" num="2.15">Sa politique à lui c’était le jeu, l’amour.</l>
					<l n="184" num="2.16">Pauvre garçon ! sa vie eut l’espace d’un jour.</l>
					<l n="185" num="2.17"><space unit="char" quantity="2"></space>— Eh bien ! dit un <hi rend="ital">lion</hi>, il eut ce qu’on célèbre :</l>
					<l n="186" num="2.18">Le plaisir, le bonheur ; son oraison funèbre</l>
					<l n="187" num="2.19">Est dans ces mots : « <hi rend="ital">L’amour l’avait favorisé</hi> ;</l>
					<l n="188" num="2.20">« <hi rend="ital">S’il est mort jeune, au moins il s’était amusé.</hi> »</l>
					<l n="189" num="2.21">Un autre dit : « — Sa vie à lui fut courte et bonne.</l>
					<l n="190" num="2.22">Qu’est-ce que le vieillard que la force abandonne ?</l>
					<l n="191" num="2.23">Un tronçon impuissant, un affligeant débris</l>
					<l n="192" num="2.24">Auquel le rhumatisme arrache de longs cris.</l>
					<l n="193" num="2.25">Ne plaignons pas celui qui tombe avec sa grâce</l>
					<l n="194" num="2.26">Et ne sent pas venir l’hiver chargé de glace.</l>
					<l n="195" num="2.27"><space unit="char" quantity="2"></space>— A propos, mes amis, dit Enguerrand, sait-on</l>
					<l part="I" n="196" num="2.28">Ce que fait Florida ? </l>
					<l part="F" n="196" num="2.28">— Mais oui. D’abord par ton</l>
					<l n="197" num="2.29">Dans son appartement elle s’est confinée,</l>
					<l n="198" num="2.30">En mémoire d’Arthur… au moins une journée.</l>
					<l n="199" num="2.31">Mais on ne peut gâter ses yeux à trop pleurer :</l>
					<l n="200" num="2.32">C’est ce que lord Asthton a su lui démontrer.</l>
					<l n="201" num="2.33">Aussi, tout en gardant quelque mélancolie,</l>
					<l n="202" num="2.34">Elle va se laisser conduire en Italie. »</l>
					<l n="203" num="2.35"><space unit="char" quantity="2"></space>Cette tendre façon d’oublier un amant</l>
					<l n="204" num="2.36">Parut de ces Messieurs avoir l’assentiment ;</l>
					<l n="205" num="2.37">On s’entretint encor d’Arthur ; et puis la glose</l>
					<l n="206" num="2.38">Étant bien terminée, on parla d’autre chose.</l>
					<l n="207" num="2.39">Ce sont là les regrets du monde d’aujourd’hui,</l>
					<l n="208" num="2.40">Qui craint par-dessus tout les larmes et l’ennui</l>
				</lg>
			</div></body></text></TEI>