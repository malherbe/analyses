<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA COMÉDIE DU MONDE</title>
				<title type="medium">Édition électronique</title>
				<author key="DSA">
					<name>
						<forename>Alfred</forename>
						<nameLink>de</nameLink>
						<surname>ESSARTS</surname>
					</name>
					<date from="1811" to="1893">1811-1893</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>5551 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">DSA_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA COMÉDIE DU MONDE</title>
						<author>Alfred des Essarts</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k5446283w?rk=429186</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title type="main">LA COMÉDIE DU MONDE</title>
								<author>Alfred des Essarts</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher> Comptoir des imprimeurs-unis</publisher>
									<date when="1851">1851</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1851">1851</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension(…).</p>
					<p>Les tirets simples précédés et/ou suivis d’une espace ont été remplacés par des tirets demi-cadratin</p>
					<p>
						Le signe "_" (_) est utilisé à la place du trait d’union pour séparer les syllabes
						du mot "so-ci-a-lis-tes" afin de préserver l’unité du mot dans le traitement des rimes.
					</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-04-25" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-04-25" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DSA18">
				<head type="number">XVII</head>
				<head type="main">LE SECRET DE ZACHARIE</head>
				<lg n="1">
					<l n="1" num="1.1"><space quantity="2" unit="char"></space>Quand Firmin reparut au château, la tristesse</l>
					<l n="2" num="1.2">En avait éloigné les hôtes ; — la comtesse</l>
					<l n="3" num="1.3">Pâle, les yeux en pleurs, les traits bouleversés,</l>
					<l n="4" num="1.4">Dans son cœur ulcéré gardait de noirs pensers.</l>
					<l n="5" num="1.5">Sans cesse interrogeant la pendule trop lente</l>
					<l n="6" num="1.6">Et toute consumée en une affreuse attente,</l>
					<l n="7" num="1.7">Elle était par l’esprit bien loin de ce séjour,</l>
					<l n="8" num="1.8">Et de son défenseur épiait le retour.</l>
					<l n="9" num="1.9"><space quantity="2" unit="char"></space>Le comte, non moins sombre et gardant le silence,</l>
					<l n="10" num="1.10">Ou bien jetant sa voix pleine de violence</l>
					<l n="11" num="1.11">Et brusquant ses valets, se tenait enfermé.</l>
					<l n="12" num="1.12">De sinistres éclairs son regard animé</l>
					<l n="13" num="1.13">Interrogeait les traits de la comtesse, et vite</l>
					<l n="14" num="1.14">Se détournait. — Ainsi le voyageur évite,</l>
					<l n="15" num="1.15">Après l’avoir sondé, le gouffre menaçant.</l>
					<l n="16" num="1.16">Sous un pareil regard se glace tout le sang.</l>
					<l n="17" num="1.17">Quelque chose disait à l’épouse coupable :</l>
					<l n="18" num="1.18">« Le secret est connu ; la justice implacable</l>
					<l n="19" num="1.19">Apprête un châtiment terrible et mérité.</l>
					<l n="20" num="1.20">Le mal, même ici-bas, n’a point l’impunité. »</l>
					<l n="21" num="1.21"><space quantity="2" unit="char"></space>Voilà comme à la joie, au bonheur sans nuage</l>
					<l n="22" num="1.22">Venait de succéder le règne des orages.</l>
					<l n="23" num="1.23">Le bruit d’une voiture a retenti. Firmin</l>
					<l n="24" num="1.24">Paraît ; une cravate enveloppait sa main.</l>
					<l n="25" num="1.25">Le jeune homme était pâle ainsi qu’un jour d’automne.</l>
					<l n="26" num="1.26">La comtesse à sa vue et chancelle, et frissonne…</l>
					<l n="27" num="1.27">Le général, non moins surpris, s’est écrié :</l>
					<l n="28" num="1.28">« — Qu’as-tu donc, et pourquoi ton bras est-il plié ?</l>
					<l n="29" num="1.29">Est-ce d’un accident que provient ta blessure ?</l>
					<l n="30" num="1.30">— Oh ! tranquillisez-vous, c’est une meurtrissure,</l>
					<l part="I" n="31" num="1.31">Dit Firmin. </l>
					<l part="M" n="31" num="1.31">— Tu parais souffrir. </l>
					<l part="F" n="31" num="1.31">— Moi, pas du tout.</l>
					<l n="32" num="1.32">— Tu sembles avoir peine à te tenir debout.</l>
					<l n="33" num="1.33">— Vous croyez ?… Ce n’est rien… L’émotion, la joie…</l>
					<l n="34" num="1.34">Mais vous, cher comte, au deuil êtes-vous donc en proie ?</l>
					<l n="35" num="1.35">Quand je vous ai quitté, votre unique souci</l>
					<l n="36" num="1.36">C’était de voir régner tous les plaisirs ici.</l>
					<l part="I" n="37" num="1.37">Vous voilà tout pensif. </l>
					<l part="F" n="37" num="1.37">— Tu te trompes peut-être.</l>
					<l n="38" num="1.38">— Non, je vous aime trop pour ne pas m’y connaître.</l>
					<l n="39" num="1.39">Mais insister serait sans doute m’exposer</l>
					<l n="40" num="1.40">A vous être importun… Je vais me reposer</l>
					<l n="41" num="1.41">Dans ma chambre ; à dîner nous serons tous ensemble. »</l>
					<l n="42" num="1.42"><space quantity="2" unit="char"></space>Il s’éloigne, laissant la comtesse qui tremble ;</l>
					<l n="43" num="1.43">Bientôt il reviendra la rejoindre au salon ;</l>
					<l n="44" num="1.44">Elle l’a deviné. — Mais que le temps est long</l>
					<l n="45" num="1.45">Lorsque de son maintien il faut faire l’étude !</l>
					<l n="46" num="1.46">Enfin le général, suivant son habitude,</l>
					<l n="47" num="1.47">Monte à cheval et va se promener au loin.</l>
					<l n="48" num="1.48">Paul descend aussitôt. Ils étaient sans témoin</l>
					<l n="49" num="1.49">Et pouvaient échanger leur triste,confidence ;</l>
					<l n="50" num="1.50">Car le chagrin profond veut son indépendance.</l>
					<l n="51" num="1.51">Devant l’indifférent qui pourrait les troubler,</l>
					<l n="52" num="1.52">Les pleurs, signe de deuil, n’aiment pas à couler.</l>
					<l part="I" n="53" num="1.53"><space quantity="2" unit="char"></space>« — Eh bien ! dit Amélie. </l>
					<l part="F" n="53" num="1.53">— Eh bien !… ayez, Madame,</l>
					<l n="54" num="1.54">L’unique souvenir de vos devoirs de femme.</l>
					<l n="55" num="1.55">Rejetez à jamais du fond de votre cœur</l>
					<l n="56" num="1.56">L’indigne sentiment qui le tenait vainqueur ;</l>
					<l n="57" num="1.57">Soyez toute à celui qui, sur votre promesse,</l>
					<l n="58" num="1.58">Avait de ses vieux ans ranimé l’allégresse,</l>
					<l n="59" num="1.59">Et qui de son honneur, sur ce serment pieux,</l>
					<l n="60" num="1.60">Avait mis en vos mains le dépôt précieux.</l>
					<l n="61" num="1.61">Vivez pour lui, par lui ; que chaque jour s’attache</l>
					<l n="62" num="1.62">A réparer la faute, à laver cette tache.</l>
					<l n="63" num="1.63">— Oui, oui, je le ferai. Mais enfin dites donc</l>
					<l n="64" num="1.64">— Oh ! vous aimez encor, même après l’abandon,</l>
					<l n="65" num="1.65">Après la perfidie !… et vous voulez connaître</l>
					<l n="66" num="1.66">Comment le ciel vengeur fait justice d’un traître.</l>
					<l part="I" n="67" num="1.67">— Il est mort ! ! </l>
					<l part="M" n="67" num="1.67">— Oui, Madame. </l>
					<l part="F" n="67" num="1.67">— Il est mort !… Il est mort ! !</l>
					<l n="68" num="1.68">— Le bon droit, cette fois, fut le droit du plus fort.</l>
					<l n="69" num="1.69">Faut-il le répéter : que pas un seul vestige</l>
					<l n="70" num="1.70">Ne reste de ce temps de trouble et de vertige.</l>
					<l n="71" num="1.71">Ainsi ne laissez plus dans votre souvenir</l>
					<l n="72" num="1.72">Place à des sentiments qui purent vous ternir ;</l>
					<l n="73" num="1.73">Rejetez à jamais la fatale pensée</l>
					<l n="74" num="1.74">De cet enivrement où vous fûtes bercée.</l>
					<l n="75" num="1.75">Qu’en voyant votre front pur et rasséréné</l>
					<l n="76" num="1.76">De l’ancienne auréole encore environné,</l>
					<l n="77" num="1.77">Je me dise : La paix à son âme est rendue</l>
					<l n="78" num="1.78">Et la grâce de Dieu sur elle est descendue.</l>
					<l n="79" num="1.79">Redevenez vous-même, et les anges du ciel</l>
					<l n="80" num="1.80">Obtiendront le pardon près du juge éternel.</l>
					<l n="81" num="1.81">Oh ! que le repentir, en vous rendant sublime,</l>
					<l n="82" num="1.82">Vous fasse, s’il se peut, plus grande après le crime.</l>
					<l n="83" num="1.83">Le seul regret permis n’est que dans le remord ;</l>
					<l n="84" num="1.84">Votre époux est vivant si votre amant est mort.</l>
					<l n="85" num="1.85">Vous n’avez pas le droit de verser une larme :</l>
					<l n="86" num="1.86">Car il est des douleurs dont la pudeur s’alarme.</l>
					<l n="87" num="1.87">Je n’ajoute qu’un mot : vous pouvez réparer</l>
					<l n="88" num="1.88">Le délire fatal qui vint vous enivrer.</l>
					<l n="89" num="1.89">Mais pour que vous puissiez tenir la tête haute,</l>
					<l n="90" num="1.90">A force de vertus oubliez votre faute.</l>
					<l n="91" num="1.91">— Oui, oui, mon bon Firmin… Oui, vous avez raison …</l>
					<l part="I" n="92" num="1.92">Je… ne… veux… Que je souffre ! » </l>
					<l part="F" n="92" num="1.92">En effet, le frisson</l>
					<l n="93" num="1.93">Parcourait tout son corps ; une pâleur mortelle</l>
					<l n="94" num="1.94">Avait couvert ses traits. Firmin s’approchant d’elle</l>
					<l n="95" num="1.95">La reçut chancelante, et sur un canapé</l>
					<l n="96" num="1.96">Déposa son beau corps par la douleur frappé,</l>
					<l n="97" num="1.97">Puis sonna vivement. Une femme de chambre</l>
					<l n="98" num="1.98">Accourut ; on porta la comtesse en sa chambre ;</l>
					<l n="99" num="1.99">Elle avait le délire, une fièvre de feu.</l>
					<l part="I" n="100" num="1.100">Comment se procurer un médecin ?… </l>
					<l part="F" n="100" num="1.100">« — Mon Dieu !</l>
					<l n="101" num="1.101">Dit Firmin, des valets la lenteur est extrême.</l>
					<l n="102" num="1.102">Qu’on me selle un cheval !… Je vais courir moi-même</l>
					<l part="I" n="103" num="1.103">A la ville voisine. </l>
					<l part="F" n="103" num="1.103">« Il part comme le vent.</l>
					<l n="104" num="1.104">Sa pensée inquiète est encore en avant</l>
					<l n="105" num="1.105">Et lui fait entrevoir, dans un avenir sombre,</l>
					<l n="106" num="1.106">Un cortége de maux sans mesure et sans nombre.</l>
					<l n="107" num="1.107">Il s’accuse d’avoir été cruel et dur :</l>
					<l n="108" num="1.108">« Pourtant j’ai combattu pour un principe pur :</l>
					<l n="109" num="1.109">J’ai rempli mon devoir ; et, craignant sa folie,</l>
					<l n="110" num="1.110">Moi-même j’ai brûlé les lettres d’Amélie.</l>
					<l n="111" num="1.111">Se peut-il que chez elle il subsiste un regret ?</l>
					<l n="112" num="1.112">S’il était vrai, mon cœur, je crois, la haïrait.</l>
					<l n="113" num="1.113">Non, c’est l’émotion, ce flambeau qui dévore…</l>
					<l n="114" num="1.114">Si ce n’est pas pour moi qui la chéris encore</l>
					<l n="115" num="1.115">Et qui dois à jamais la quitter tôt ou tard,</l>
					<l n="116" num="1.116">Mon Dieu ! conservez-la pour ce pauvre vieillard ;</l>
					<l n="117" num="1.117">Car de la pâle mort, qui lui prendrait sa joie,</l>
					<l n="118" num="1.118">Lui le pauvre vieillard il deviendrait la proie ; »</l>
					<l n="119" num="1.119"><space quantity="2" unit="char"></space>Et pressant son cheval du fouet, de l’éperon,</l>
					<l n="120" num="1.120">Il dévorait l’espace ainsi qu’un tourbillon.</l>
					<l n="121" num="1.121"><space quantity="2" unit="char"></space>Pendant ce temps le comte, au chevet de sa femme,</l>
					<l n="122" num="1.122">Suivait, morne et pensif, les ravages de l’âme.</l>
					<l n="123" num="1.123">Le vieillard se taisait ; mais un œil attentif</l>
					<l n="124" num="1.124">Parfois l’eût vu froisser, d’un geste convulsif,</l>
					<l n="125" num="1.125">Un papier qu’il tenait caché sur sa poitrine.</l>
					<l n="126" num="1.126">D’un feu sombre parfois son regard s’illumine ;</l>
					<l n="127" num="1.127">Il jette quelques mots sans suite ; l’on croirait</l>
					<l n="128" num="1.128">Que ces vagues discours renferment un secret.</l>
					<l n="129" num="1.129">Parfois même il sourit ; mais le froid de la tombe</l>
					<l n="130" num="1.130">Sur son front qui se plisse et sur sa lèvre tombe…</l>
					<l n="131" num="1.131">A la triste malade il semble s’attacher,</l>
					<l n="132" num="1.132">Comme un ange de mort venu pour la chercher.</l>
					<l n="133" num="1.133"><space quantity="2" unit="char"></space>Cependant Amélie était dans le délire.</l>
					<l n="134" num="1.134">Sur ses traits le vieillard continuait de lire ;</l>
					<l n="135" num="1.135">Et plus elle jetait de soupirs et de cris,</l>
					<l n="136" num="1.136">Plus il avait de deuil en ses yeux assombris.</l>
					<l n="137" num="1.137">Les serviteurs allaient, venaient, couraient sans ordre ;</l>
					<l n="138" num="1.138">Le château n’offrait plus que l’aspect du. désordre ;</l>
					<l n="139" num="1.139">On pleurait, on parlait à voix basse, et surtout</l>
					<l part="I" n="140" num="1.140">On appelait Firmin… </l>
					<l part="F" n="140" num="1.140">Paul apparut au bout</l>
					<l n="141" num="1.141">De la grande avenue, épuisé, sans haleine ;</l>
					<l n="142" num="1.142">Son cheval qu’il forçait se soutenait à peine…</l>
					<l n="143" num="1.143">Le docteur du pays venait à quelques pas.</l>
					<l n="144" num="1.144">Ils entrèrent tous deux, mornes et parlant bas.</l>
					<l n="145" num="1.145"><space quantity="2" unit="char"></space>La comtesse, tantôt abattue et tremblante,</l>
					<l n="146" num="1.146">Tantôt dans le transport de la fièvre brûlante,</l>
					<l n="147" num="1.147">Murmurait quelques mots dont le sens était clair.</l>
					<l n="148" num="1.148">Firmin sentit passer une sorte d’éclair</l>
					<l part="I" n="149" num="1.149">Entre l’esprit du comte et le sien. </l>
					<l part="F" n="149" num="1.149">« — Pauvre femme,</l>
					<l n="150" num="1.150">La douleur a vaincu la raison dans ton âme,</l>
					<l n="151" num="1.151">Se dit-il, tu te perds… et tu perds avec toi</l>
					<l n="152" num="1.152">Celui qui ne peut plus se fier à ta foi.</l>
					<l part="I" n="153" num="1.153">Pauvre enfant ! » </l>
					<l part="F" n="153" num="1.153">Le docteur écrivit l’ordonnance.</l>
					<l n="154" num="1.154">« Des soins intelligents, du calme, du silence</l>
					<l n="155" num="1.155">Abattront cette fièvre et vaincront le danger ;</l>
					<l n="156" num="1.156">Car, tout grave qu’il est, ce mal est passager :</l>
					<l n="157" num="1.157">Quelque altération l’aura produit sans doute. »</l>
					<l n="158" num="1.158">« — Arthur… Dieu nous a vus… Oh ! prends bien garde… écoute…</l>
					<l n="159" num="1.159">Le monde est sans pitié pour un coupable amour ;</l>
					<l n="160" num="1.160">Cachons-nous dans la nuit… Je rougis dans le jour…</l>
					<l n="161" num="1.161">Trahison !… trahison !… chassez la courtisane !…</l>
					<l n="162" num="1.162">Tu peux m’abandonner, car le ciel me condamne. »</l>
					<l n="163" num="1.163">La comtesse jeta ces mots, puis s’affaissa ;</l>
					<l n="164" num="1.164">Sur ses yeux par degrés le sommeil se glissa.</l>
					<l part="I" n="165" num="1.165">L’on sortit doucement. </l>
					<l part="F" n="165" num="1.165">« — Le sommeil c’est la vie, »</l>
					<l part="I" n="166" num="1.166">Dit le docteur. </l>
					<l part="F" n="166" num="1.166">Firmin aurait eu grande envie</l>
					<l n="167" num="1.167">De fuir le général ; celui-ci le retint</l>
					<l n="168" num="1.168">Par le bras, le mena dans le parc, et lui tint</l>
					<l n="169" num="1.169">Ce pénible discours, écho de sa souffrance :</l>
					<l n="170" num="1.170">« — Mon Paul, j’ai mis en toi toute ma confiance.</l>
					<l n="171" num="1.171">Enfant, tu n’avais pas cette frivolité</l>
					<l n="172" num="1.172">Dont on fait un beau masque à la perversité.</l>
					<l n="173" num="1.173">Ton âme généreuse, ouverte, sans nuage,</l>
					<l n="174" num="1.174">De ton excellent père offrait l’exacte image.</l>
					<l n="175" num="1.175">Je t’ai toujours aimé : c’est donc à ton honneur</l>
					<l n="176" num="1.176">Que je vais confier ma honte, mon malheur.</l>
					<l n="177" num="1.177">Il me faut devant toi rougir dans ma vieillesse</l>
					<l n="178" num="1.178">Et montrer à la fois ma fureur, ma faiblesse.</l>
					<l part="I" n="179" num="1.179">Je suis déshonoré, Paul. </l>
					<l part="M" n="179" num="1.179">— Vous ! </l>
					<l part="F" n="179" num="1.179">— En doutes-tu ?…</l>
					<l n="180" num="1.180">Un démon est venu corrompre la vertu.</l>
					<l n="181" num="1.181">L’infâme m’a ravi, sous des dehors modestes,</l>
					<l n="182" num="1.182">Un bien que je mettais parmi les biens célestes.</l>
					<l n="183" num="1.183">Amélie a rompu notre lien sacré…</l>
					<l n="184" num="1.184">Je suis déshonoré ! je suis déshonoré !</l>
					<l n="185" num="1.185">— Mon digne ami, mon père, oh ! gardez-vous d’admettre…</l>
					<l n="186" num="1.186">— Tiens, si tu ne veux pas me croire, cette lettre</l>
					<l n="187" num="1.187">Prouvera qu’Amélie, en son fiévreux transport,</l>
					<l n="188" num="1.188">À trahi le secret qui brise notre sort.</l>
					<l n="189" num="1.189">Oh ! je voulais douter… J’appelais calomnie</l>
					<l n="190" num="1.190">Le poison distillé par un vil Zacharie ;</l>
					<l n="191" num="1.191">Un juif, dont autrefois le père m’a volé…</l>
					<l n="192" num="1.192">Mais le secret fatal s’est pour moi dévoilé.</l>
					<l n="193" num="1.193">Là fièvre disait vrai… Lis, Firmin, lis toi-même,</l>
					<l n="194" num="1.194">Et vois comme le juif me jette l’anathème ! »</l>
					<l n="195" num="1.195"><space quantity="2" unit="char"></space>Le jeune homme en tremblant lut ces mots outrageants :</l>
					<l n="196" num="1.196"><space quantity="2" unit="char"></space>«Comte, vous avez en jadis, parmi vos gens</l>
					<l n="197" num="1.197">« L’homme de qui je tiens et mon nom et ma vie,</l>
					<l n="198" num="1.198">« Zacharie Alfernès. — Par une basse envie</l>
					<l n="199" num="1.199">« On l’accusa d’avoir dilapidé vos biens ;</l>
					<l n="200" num="1.200">« Car toujours sur le juif frapperont les chrétiens.</l>
					<l n="201" num="1.201">« C’était, quoi qu’on ait dit, un intendant honnête.</l>
					<l n="202" num="1.202">« Vous vîntes furieux… il demandait l’enquête,</l>
					<l n="203" num="1.203">« Mais vous jeune, bouillant, vous officier sabreur,</l>
					<l n="204" num="1.204">« Sans daigner écouter le pauvre serviteur,</l>
					<l n="205" num="1.205">« Vous le fîtes jeter, par une nuit d’orage,</l>
					<l n="206" num="1.206">« Hors de votre château. Non contents de l’outrage,</l>
					<l n="207" num="1.207">« A grands coups de bâton vos gens sur le chemin</l>
					<l n="208" num="1.208">« Le chassèrent… Mon père en est mort de chagrin !…</l>
					<l n="209" num="1.209">« Avec son souvenir mon cœur, d’intelligence,</l>
					<l n="210" num="1.210">« Garda, comme un trésor, l’espoir de la vengeance.</l>
					<l n="211" num="1.211">« J’ai nourri, caressé, réchauffé cet espoir.</l>
					<l n="212" num="1.212">« Je vous voyais de loin… Vous ne pouviez me voir…</l>
					<l n="213" num="1.213">« Car je suis un insecte inaperçu dans l’herbe…</l>
					<l n="214" num="1.214">« Mais l’insecte parfois mord le lion superbe.</l>
					<l n="215" num="1.215">« J’attendais le hasard a secondé mes vœux</l>
					<l n="216" num="1.216">« En me vengeant de vous autant que je le veux.</l>
					<l n="217" num="1.217">« Un homme s’est glissé, comme <choice type="false_verse" reason="analysis" hand="RR"><sic>un</sic><corr source="none">une</corr></choice> bête fauve,</l>
					<l n="218" num="1.218">« Pour prendre votre femme, au sein de votre alcôve.</l>
					<l n="219" num="1.219">« Cet homme est jeune, beau, séduisant, plein d’ardeur,</l>
					<l n="220" num="1.220">« Intrépide, sautant à pieds joints sur l’honneur.</l>
					<l n="221" num="1.221">« Cet homme s’est joué de votre humeur crédule ;</l>
					<l n="222" num="1.222">« Cet homme a fait de vous un époux ridicule…</l>
					<l n="223" num="1.223">« Pour qu’à son aise il pût se pavaner chez vous,</l>
					<l n="224" num="1.224">« Il a reçu de moi trois mille francs. — L’époux</l>
					<l n="225" num="1.225">« Payait les intérêts, et je crois que la femme</l>
					<l n="226" num="1.226">« A remboursé les frais de cette belle flamme.</l>
					<l n="227" num="1.227">« Mon père est bien vengé ; je puis mourir content.</l>
					<l n="228" num="1.228">« Et vous, notre oppresseur, en diriez-vous autant ?</l>
					<l n="229" num="1.229">« Adieu, comte, je pars. Le monde est la patrie</l>
					<l part="I" n="230" num="1.230">« Des Juifs ; n’essayez pas d’y trouver </l>
					<l part="F" n="230" num="1.230">«ZACHARIE. »</l>
					<l n="231" num="1.231"><space quantity="2" unit="char"></space>A peine Paul Firmin avait-il achevé</l>
					<l n="232" num="1.232">De lire cet écrit que, le cœur soulevé</l>
					<l n="233" num="1.233">De dégoût, il broya le papier qu’un reptile</l>
					<l n="234" num="1.234">Avait,comme à plaisir,maculé de sa bile ;</l>
					<l n="235" num="1.235">Il reprit les morceaux pour les briser encor.</l>
					<l n="236" num="1.236"><space quantity="2" unit="char"></space>Alors à son courroux donnant un libre essor :</l>
					<l n="237" num="1.237">« — Eh bien ! lui dit le comte, il est sans doute infâme ;</l>
					<l n="238" num="1.238">Mais les aveux sortis des lèvres de ma femme</l>
					<l n="239" num="1.239">Ne me permettent pas de douter… Cet Arthur</l>
					<l n="240" num="1.240">A détruit mon bonheur, et l’opprobre est bien sûr …</l>
					<l part="I" n="241" num="1.241">Reste au château… je veux partir, je veux… </l>
					<l part="F" n="241" num="1.241">— Qu’entends-je !</l>
					<l part="I" n="242" num="1.242">Vous partir ! </l>
					<l part="F" n="242" num="1.242">— Songe donc qu’il faut que je me venge.</l>
					<l n="243" num="1.243">— A votre âge ! courir les hasards d’un combat !</l>
					<l n="244" num="1.244">— Va, si près du tombeau sans crainte l’on se bat.</l>
					<l n="245" num="1.245">D’ailleurs, j’éprouve là tant de douleur, de rage,</l>
					<l n="246" num="1.246">Que je ne ressens plus les glaces de mon âge.</l>
					<l n="247" num="1.247">Du sang ! je veux du sang, pour mon honneur flétri !</l>
					<l n="248" num="1.248">Tout le sang de l’amant pour, les pleurs du mari !…</l>
					<l n="249" num="1.249">Peut-être chacun rit comme ce juif immonde :</l>
					<l n="250" num="1.250">L’éclat de ma fureur étonnera le monde.</l>
					<l n="251" num="1.251">Malheur à ce baron au crime habitué !</l>
					<l part="I" n="252" num="1.252">Il faut que je le tue… Adieu. </l>
					<l part="F" n="252" num="1.252">— Je l’ai tué.</l>
					<l n="253" num="1.253">— Se peut-il ! tu m’as pris cette suprême joie</l>
					<l n="254" num="1.254">Cet homme était à moi, cet homme était ma proie.</l>
					<l n="255" num="1.255">Oh ! je ne puis bénir ton pieux dévoûment. »</l>
					<l n="256" num="1.256"><space quantity="2" unit="char"></space>Et le comte s’enfuit, dans un égarement,</l>
					<l n="257" num="1.257">Dans un trouble si grand, si profond, si terrible,</l>
					<l n="258" num="1.258">Qu’il devint pour ses gens une ombre inaccessible,</l>
					<l n="259" num="1.259">Excepté pour Firmin que rien ne rebutait,</l>
					<l n="260" num="1.260">Et dont l’attachement chaque jour s’augmentait.</l>
					<l n="261" num="1.261">Depuis que de la honte il supportait l’étreinte,</l>
					<l n="262" num="1.262">Le comte avait senti son existence éteinte ;</l>
					<l n="263" num="1.263">Cet homme ne vivait que dans un sentiment,</l>
					<l n="264" num="1.264">Et son dernier amour était un doux roman…</l>
					<l n="265" num="1.265">Lui, fatigué des chocs d’une rude carrière,</l>
					<l n="266" num="1.266">Il avait retrouvé dans son âme guerrière</l>
					<l n="267" num="1.267">Ce feu de la jeunesse et cette bonne foi</l>
					<l n="268" num="1.268">Qui font qu’en donnant tout on voudrait tout pour soi.</l>
					<l n="269" num="1.269">Être si confiant et, se créant un culte,</l>
					<l n="270" num="1.270">Semer la loyauté pour recueillir l’insulte ;</l>
					<l n="271" num="1.271">Avoir livré son nom sans tache pour le voir</l>
					<l n="272" num="1.272">Souillé pour un dandy qui se rit du devoir.</l>
					<l n="273" num="1.273">C’est un coup trop affreux, un coup mortel !… Le comte</l>
					<l n="274" num="1.274">Eût grandi sous le deuil, il pliait sous la honte,</l>
					<l n="275" num="1.275">Et dans son déshonneur, pourtant immérité,</l>
					<l n="276" num="1.276">Sentait s’anéantir et vie et dignité.</l>
					<l n="277" num="1.277">L’excès de ce chagrin qui lentement nous mine</l>
					<l n="278" num="1.278"><space quantity="2" unit="char"></space>A ce noble vieillard préparait la ruine.</l>
					<l n="279" num="1.279">Le comte se soutint longtemps par un effort,</l>
					<l n="280" num="1.280">Jusqu’au dernier moment voulant paraître fort ;</l>
					<l n="281" num="1.281">Comme on voyait jadis dans les jeux de l’arène</l>
					<l n="282" num="1.282">L’athlète terrassé, se traînant avec peine,</l>
					<l n="283" num="1.283">Par un dernier salut qu’il faisait de la main</l>
					<l n="284" num="1.284">Embellir son trépas pour le peuple romain.</l>
					<l n="285" num="1.285"><space quantity="2" unit="char"></space>Le jour vint cependant, la coupe étant vidée,</l>
					<l n="286" num="1.286">Où le vieillard sentit qu’on mourait d’une idée.</l>
					<l n="287" num="1.287">Il prit le lit. La fièvre à son tour le minait,</l>
					<l n="288" num="1.288">Tandis que la comtesse au monde revenait :</l>
					<l n="289" num="1.289">Car la jeunesse était le sauveur d’Amélie,</l>
					<l n="290" num="1.290">Et la raison avait surmonté la folie.</l>
					<l n="291" num="1.291">Elle voulut revoir — ne fût-ce qu’une fois</l>
					<l n="292" num="1.292">— Son époux… Il frémit à l’accent de sa voix,</l>
					<l part="I" n="293" num="1.293">Et se dressant terrible : </l>
					<l part="F" n="293" num="1.293">« — Osez-vous bien, Madame,</l>
					<l n="294" num="1.294">Montrer sur votre front les souillures de l’âme ?</l>
					<l n="295" num="1.295">Sortez… Vous me tuez deux fois… Sortez d’ici !</l>
					<l n="296" num="1.296">Pour qui fut sans pitié je serai sans merci. »</l>
					<l n="297" num="1.297"><space quantity="2" unit="char"></space>La comtesse sortit en pleurs et consternée.</l>
					<l n="298" num="1.298">Paul Firmin était là ; car toute sa journée</l>
					<l n="299" num="1.299">Se passait près du comte, et jamais infirmier</l>
					<l n="300" num="1.300">Ne sut aux moribonds se vouer plus entier.</l>
					<l n="301" num="1.301">Le jeune homme approcha sa chaise, et d’un ton grave :</l>
					<l n="302" num="1.302">« — Quoi ! du ressentiment êtes-vous donc l’esclave,</l>
					<l n="303" num="1.303">Dit-il, et gardez-vous au fond de votre cœur</l>
					<l n="304" num="1.304">A travers la souffrance une implacable ardeur ?</l>
					<l n="305" num="1.305">Un moment vient où l’homme avec calme mesure</l>
					<l n="306" num="1.306">Ses biens et ses malheurs, sa joie et sa blessure,</l>
					<l n="307" num="1.307">Et voit comme il faut peu se fier ici-bas</l>
					<l n="308" num="1.308">Au fantôme trompeur qui nous tendait ses bras.</l>
					<l n="309" num="1.309">Quand s’entr’ouvre le ciel, séjour où rien n’altère</l>
					<l n="310" num="1.310">Notre félicité, que petite est la terre !</l>
					<l n="311" num="1.311">Les intérêts du jour, pleins de fragilité,</l>
					<l n="312" num="1.312">S’effacent tout à coup devant l’éternité.</l>
					<l n="313" num="1.313">Je ne vous cache pas, à vous dont le courage</l>
					<l n="314" num="1.314">Trompa souvent la mort au milieu du carnage,</l>
					<l n="315" num="1.315">Je ne vous cache pas que vous serez bientôt</l>
					<l n="316" num="1.316">Aux pieds du Souverain qui nous juge là-haut :</l>
					<l n="317" num="1.317">Il vous demandera si vous avez fait grâce.</l>
					<l n="318" num="1.318">Et comment pourriez-vous désarmer sa menace,</l>
					<l n="319" num="1.319">Si vous n’aviez pas eu dans le cœur ce pardon</l>
					<l n="320" num="1.320">Qui reste aux opprimés comme un céleste don ?</l>
					<l n="321" num="1.321">Vous, juge sur la terre, imitez sa clémence ;</l>
					<l n="322" num="1.322">Pour le pécheur contrit sa douceur est immense ;</l>
					<l n="323" num="1.323">Il envoya son Christ, il répandit son sang</l>
					<l n="324" num="1.324">A sa loi de pardon soyez obéissant,</l>
					<l n="325" num="1.325">Afin qu’il vous accorde, en sa grâce éternelle,</l>
					<l n="326" num="1.326">Le prix de vos bienfaits pour l’âme criminelle.</l>
					<l n="327" num="1.327">On est grand par l’oubli plus que par le courroux,</l>
					<l n="328" num="1.328">Et le ressentiment doit s’éteindre avec vous. »</l>
					<l n="329" num="1.329"><space quantity="2" unit="char"></space>Le comte réfléchit ; — puis rouvrant sa paupière</l>
					<l n="330" num="1.330">Qui semblait s’affaisser sous le poids de la pierre,</l>
					<l part="I" n="331" num="1.331">Il murmura ces mots : </l>
					<l part="F" n="331" num="1.331">« — Tu dictes mon devoir.</l>
					<l n="332" num="1.332">Qu’<hi rend="ital">elle</hi> vienne, Firmin ; je consens à <hi rend="ital">la</hi> voir. »</l>
					<l n="333" num="1.333">Firmin l’alla chercher dans la pièce voisine</l>
					<l n="334" num="1.334">Elle entra… Sa pâleur, son trouble se devine…</l>
					<l n="335" num="1.335">Courbée aux pieds du lit, sans parler, en pleurant,</l>
					<l n="336" num="1.336">Elle attendait l’arrêt des lèvres du mourant.</l>
					<l n="337" num="1.337">Et celui-ci sentait sa force ranimée</l>
					<l n="338" num="1.338">Devant son Amélie… Il l’avait tant aimée !</l>
					<l part="I" n="339" num="1.339"><space quantity="2" unit="char"></space>Voici ce qu’il lui dit : </l>
					<l part="F" n="339" num="1.339">« — Ne crains rien près de moi</l>
					<l n="340" num="1.340">Qui veux te pardonner ; car j’ai compris la loi</l>
					<l n="341" num="1.341">De douceur, par Dieu même écrite en l’Évangile.</l>
					<l n="342" num="1.342">Et nous ne devons point, pour un bonheur fragile,</l>
					<l n="343" num="1.343">Pour des biens passagers conserver dans nos cœurs,</l>
					<l n="344" num="1.344">Créatures d’un jour, d’éternelles rigueurs.</l>
					<l n="345" num="1.345">Tu faisais mon orgueil, mais j’avais trop de joie ;</l>
					<l n="346" num="1.346">Et comme un lac trompeur notre bonheur nous noie,</l>
					<l n="347" num="1.347">Quand nous nous livrons trop au charme décevant</l>
					<l n="348" num="1.348">De notre illusion qui fuit comme le vent.</l>
					<l n="349" num="1.349">Je te pardonne, ô toi qui gémis sur ta faute ;</l>
					<l n="350" num="1.350">Et tu pourras encor tenir la tête haute :</l>
					<l n="351" num="1.351">Car nul ne devra plus te demander raison</l>
					<l n="352" num="1.352">De ton égarement et de ta trahison.</l>
					<l n="353" num="1.353">Ma bénédiction épurera ton âme.</l>
					<l n="354" num="1.354">Va donc, toi que j’aimais, Amélie, ô ma femme,</l>
					<l n="355" num="1.355">Dernier rêve, dernier enchantement pour moi !</l>
					<l n="356" num="1.356">J’eus tort : aurais-je dû te sacrifier toi,</l>
					<l n="357" num="1.357">T’attacher, toi brillante et belle de jeunesse,</l>
					<l n="358" num="1.358">A l’amour d’un époux glacé par la vieillesse ?</l>
					<l n="359" num="1.359">L’harmonie est la loi du bonheur ici-bas.</l>
					<l n="360" num="1.360">La prudence parlait, je ne l’ écoutai pas ;</l>
					<l n="361" num="1.361">A mes désirs livré, j’en savourai les charmes.</l>
					<l n="362" num="1.362">Où manque l’harmonie, un jour coulent les larmes.</l>
					<l n="363" num="1.363">Je m’accuse, je suis coupable du malheur</l>
					<l n="364" num="1.364">Qui termine ma vie et ternit mon honneur.</l>
					<l n="365" num="1.365">Adieu !… Donne parfois une bonne pensée</l>
					<l n="366" num="1.366">A l’époux qui n’est plus, à l’image effacée.</l>
					<l n="367" num="1.367">Garde mon souvenir, il te protégera,</l>
					<l n="368" num="1.368">Et, comme je l’ai fait, Dieu te pardonnera. »</l>
					<l n="369" num="1.369"><space quantity="2" unit="char"></space>Il étendit les mains et bénit la comtesse.</l>
					<l n="370" num="1.370">Chaque moment venait redoubler sa faiblesse.</l>
					<l n="371" num="1.371">Un regard d’Amélie alla remercier</l>
					<l part="I" n="372" num="1.372">Firmin ; puis elle dit : </l>
					<l part="F" n="372" num="1.372">« — Pour me purifier</l>
					<l n="373" num="1.373">Il faut plus qu’un pardon, il faut la pénitence.</l>
					<l n="374" num="1.374">Votre bonté m’a fait entendre ma sentence ;</l>
					<l n="375" num="1.375">Et le ciel me prescrit un devoir tout nouveau :</l>
					<l n="376" num="1.376">Je veux, je veux vivante entrer en mon tombeau ;</l>
					<l n="377" num="1.377">Je veux, loin des plaisirs, loin des fêtes du monde,</l>
					<l n="378" num="1.378">Au sein d’une retraite inconnue et profonde</l>
					<l n="379" num="1.379">Consacrer tous mes jours à soigner les souffrants,</l>
					<l n="380" num="1.380">Soutenir les vieillards, élever les enfants,</l>
					<l n="381" num="1.381">Sous la robe de bure accomplir cette tâche</l>
					<l n="382" num="1.382">Et, Sœur de Charité, travailler sans relâche,</l>
					<l n="383" num="1.383">Pour que Firmin sur terre et vous auprès de Dieu</l>
					<l part="I" n="384" num="1.384">Vous puissiez m’estimer. </l>
					<l part="F" n="384" num="1.384">— Mon Amélie, adieu !</l>
					<l n="385" num="1.385">Viens, Paul, ô mon ami, viens toi qui fus ma femme,</l>
					<l n="386" num="1.386">Je vous bénis… Mon Dieu, mon Dieu, reçois mon âme ! »</l>
					<l n="387" num="1.387"><space quantity="2" unit="char"></space>Le lendemain, la cloche aux tristes tintements</l>
					<l n="388" num="1.388">Portait au loin l’écho de ses gémissements.</l>
				</lg>
			</div></body></text></TEI>