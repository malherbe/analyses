<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA COMÉDIE DU MONDE</title>
				<title type="medium">Édition électronique</title>
				<author key="DSA">
					<name>
						<forename>Alfred</forename>
						<nameLink>de</nameLink>
						<surname>ESSARTS</surname>
					</name>
					<date from="1811" to="1893">1811-1893</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>5551 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">DSA_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA COMÉDIE DU MONDE</title>
						<author>Alfred des Essarts</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k5446283w?rk=429186</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title type="main">LA COMÉDIE DU MONDE</title>
								<author>Alfred des Essarts</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher> Comptoir des imprimeurs-unis</publisher>
									<date when="1851">1851</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1851">1851</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension(…).</p>
					<p>Les tirets simples précédés et/ou suivis d’une espace ont été remplacés par des tirets demi-cadratin</p>
					<p>
						Le signe "_" (_) est utilisé à la place du trait d’union pour séparer les syllabes
						du mot "so-ci-a-lis-tes" afin de préserver l’unité du mot dans le traitement des rimes.
					</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-04-25" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-04-25" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DSA1">
				<head type="main">INTRODUCTION</head>
				<head type="sub">PARIS</head>
				<div n="1" type="section">
					<head type="number">I</head>
					<lg n="1">
						<l n="1" num="1.1"><space unit="char" quantity="2"></space>Je n’ai jamais été de ces fâcheux esprits</l>
						<l n="2" num="1.2">Qui font profession de détester Paris,</l>
						<l n="3" num="1.3">Et dont l’humeur morose à tout propos étale</l>
						<l n="4" num="1.4">Un suprême dédain pour notre capitale.</l>
						<l n="5" num="1.5">Copistes de Rousseau, le sombre genevois,</l>
						<l n="6" num="1.6">A l’envi contre nous ils élèvent la voix,</l>
						<l n="7" num="1.7">Et nous ont fait partout la triste renommée</l>
						<l n="8" num="1.8">De marcher dans la boue, à travers la fumée ;</l>
						<l n="9" num="1.9">D’être légers, bavards, curieux, inconstants ;</l>
						<l n="10" num="1.10">D’agir ou de parler toujours à contre-temps ;</l>
						<l n="11" num="1.11">Et de n’avoir pour règle, en vertu comme en vice,</l>
						<l n="12" num="1.12">Que l’éternelle loi du mobile caprice.</l>
						<l n="13" num="1.13"><space unit="char" quantity="2"></space>Tel est notre portrait : s’il dit la vérité,</l>
						<l n="14" num="1.14">Vous conviendrez du moins qu’on ne l’a point flatté.</l>
						<l n="15" num="1.15"><space unit="char" quantity="2"></space>De critiquer ainsi quelle manie étrange !</l>
						<l n="16" num="1.16">Non, nous n’habitons pas une ville de fange ;</l>
						<l n="17" num="1.17">Le soleil a pour nous des rayons bienfaisants ;</l>
						<l n="18" num="1.18">Paris a rajeuni sous le travail des ans,</l>
						<l n="19" num="1.19">Et nous ne sommes pas tous atteints de folie.</l>
						<l n="20" num="1.20">Nous aimons le plaisir… Au travail il s’allie.</l>
						<l n="21" num="1.21">Nous avons nos défauts ; mais qui n’a pas les siens ?</l>
						<l n="22" num="1.22">Pour moi, j’aime Paris et mes concitoyens.</l>
						<l n="23" num="1.23">Paris est pour l’esprit une mine féconde :</l>
						<l n="24" num="1.24">C’est un grand réservoir où vient puiser le monde.</l>
						<l n="25" num="1.25">L’Idée, en s’échappant de ce centre puissant,</l>
						<l n="26" num="1.26">Échauffe tous les cœurs qu’elle touche en passant.</l>
						<l n="27" num="1.27">Rien ne se fait chez nous, qui dans toute l’Europe</l>
						<l n="28" num="1.28">Soudain ne retentisse et ne se développe.</l>
						<l n="29" num="1.29">Encelades nouveaux, nos moindres mouvements</l>
						<l n="30" num="1.30">Font tressaillir le sol jusqu’en ses fondements.</l>
						<l n="31" num="1.31">Les peuples sur nos lois ont façonné leur code,</l>
						<l n="32" num="1.32">Et de nos vêtements ils reçoivent la mode ;</l>
						<l n="33" num="1.33">Ainsi que nos tableaux, nos livres vont partout ;</l>
						<l n="34" num="1.34">On parle notre langue, on calque notre goût ;</l>
						<l n="35" num="1.35">L’Art qui végète ailleurs, nous donne des merveilles ;</l>
						<l n="36" num="1.36">Rossini, Meyerbeer nous ont voué leurs veilles</l>
						<l n="37" num="1.37">En recevant de nous, par l’hospitalité,</l>
						<l n="38" num="1.38">Leur baptême d’honneurs et de célébrité.</l>
						<l n="39" num="1.39">Les peuples sont le bras, — nous la tête qui pense.</l>
						<l n="40" num="1.40">Que de fièvre chez nous, que d’ardeur se dépense !</l>
						<l n="41" num="1.41">Comme nous répandions notre sang et notre or,</l>
						<l n="42" num="1.42">Tandis qu’à l’Étranger on sommeillait encor !</l>
						<l n="43" num="1.43">Si la France a d’abord reflété la lumière,</l>
						<l n="44" num="1.44">C’est que des nations elle est l’avant-courrière ;</l>
						<l n="45" num="1.45">Qu’elle doit à la fois, en étendant la main,</l>
						<l n="46" num="1.46">Soutenir le courage et montrer le chemin ;</l>
						<l n="47" num="1.47">Chanter l’hymne d’amour ou l’altière fanfare ;</l>
						<l n="48" num="1.48">En guerre être un drapeau, dans la paix être un phare.</l>
						<l n="49" num="1.49"><space unit="char" quantity="2"></space>Cessez donc, ô frondeurs, de blâmer sans raison,</l>
						<l n="50" num="1.50">Et d’ébranler ainsi votre propre maison ;</l>
						<l n="51" num="1.51">Cessez de vous répandre en ironie amère :</l>
						<l n="52" num="1.52">Enfants, ne frappez pas le sein de votre mère.</l>
						<l n="53" num="1.53">Je conviens avec vous qu’il fut de mauvais jours</l>
						<l n="54" num="1.54">Où chez nous la discorde et la haine eurent cours ;</l>
						<l n="55" num="1.55">Où les partis, armés pour la guerre civile,</l>
						<l n="56" num="1.56">Promenèrent la mort dans les murs de la ville…</l>
						<l n="57" num="1.57">Tristes jours qu’il faudrait rayer du souvenir,</l>
						<l n="58" num="1.58">Taches que le passé transmet à l’avenir.</l>
						<l n="59" num="1.59">Mais voit-on l’Océan, sous d’éternels orages,</l>
						<l n="60" num="1.60">Constamment déchaîné, bouleverser ses plages ?</l>
						<l n="61" num="1.61">La fièvre des combats enflamma notre sein ;</l>
						<l n="62" num="1.62">Mais, on l’a dit, le temps est un grand médecin.</l>
						<l n="63" num="1.63">Les siècles à venir nous cachent leurs oracles ;</l>
						<l n="64" num="1.64">L’humanité n’a pas achevé ses miracles.</l>
						<l n="65" num="1.65">Tant que l’arbre de Dieu sur l’homme s’étendra,</l>
						<l n="66" num="1.66">C’est la France surtout que son ombre aimera.</l>
					</lg>
				</div>
				<div n="2" type="section">
					<head type="number">II</head>
					<lg n="1">
						<l n="67" num="1.1"><space unit="char" quantity="2"></space>Souvent je me suis dit,— lorsque dans une fête</l>
						<l n="68" num="1.2">Le plaisir enivrant montait à chaque tête ;</l>
						<l n="69" num="1.3">Lorsqu’aux sons de l’orchestre, amour et volupté</l>
						<l n="70" num="1.4">Faisaient battre le cœur et briller la beauté, —</l>
						<l n="71" num="1.5">Quel roman ce serait, charmantes créatures,</l>
						<l n="72" num="1.6">Si l’on traçait de vous autant de miniatures ;</l>
						<l n="73" num="1.7">Quels traits. quel coloris vous pourriez inspirer !</l>
						<l n="74" num="1.8">Vous chanter, ce serait presque vous adorer.</l>
						<l n="75" num="1.9">Et pourquoi le poëte, ermite solitaire,</l>
						<l n="76" num="1.10">Se tient-il dédaigneux loin des bruits de la terre ;</l>
						<l n="77" num="1.11">Ou, se laissant aller au vain rêve du <hi rend="ital">moi</hi>,</l>
						<l n="78" num="1.12">Remplit-il l’univers de son mystique émoi ?</l>
						<l n="79" num="1.13">Il trouverait ici bien plus de poésie,</l>
						<l n="80" num="1.14">Et la réalité serait sa fantaisie.</l>
						<l n="81" num="1.15"><space unit="char" quantity="2"></space>Faisons donc aujourd’hui, sur des types divers,</l>
						<l n="82" num="1.16">Sur le monde vivant, — notre ROMAN EN VERS.</l>
					</lg>
				</div>
				<div n="3" type="section">
					<head type="number">III</head>
					<lg n="1">
						<l n="83" num="1.1"><space unit="char" quantity="2"></space>Nous n’irons pas chercher, dans les temps héroïques,</l>
						<l n="84" num="1.2">Les modèles connus des figures épiques.</l>
						<l n="85" num="1.3">Ce genre ne sied plus : Ulysse, Ajax, Hector,</l>
						<l n="86" num="1.4">Auprès de nos lecteurs risqueraient d’avoir tort ;</l>
						<l n="87" num="1.5">Les malheurs d’Herminie ou les transports d’Armide</l>
						<l n="88" num="1.6">Ne trouveraient plus même une paupière humide ;</l>
						<l n="89" num="1.7">Énée irait tout seul combattre au Latium,</l>
						<l n="90" num="1.8">Et la <hi rend="ital">Pharsale</hi> aurait un effet d’opium.</l>
						<l n="91" num="1.9">Récits des paladins, des géants, des batailles,</l>
						<l n="92" num="1.10">Le temps vous réservait aussi vos funérailles :</l>
						<l n="93" num="1.11">Vous charmiez nos aïeux… Peut-être un jour nos fils</l>
						<l n="94" num="1.12">Dans le même dédain tiendront-ils nos écrits.</l>
						<l n="95" num="1.13"><space unit="char" quantity="2"></space>N’importe, et quel que soit le destin de ma muse,</l>
						<l n="96" num="1.14">Si de mes fictions quelque lecteur s’amuse,</l>
						<l n="97" num="1.15">J’aurai bien employé mes heures de loisir.</l>
						<l n="98" num="1.16">Puissiez-vous en mes vers goûter quelque plaisir,</l>
						<l n="99" num="1.17">Revenir vers un monde ; aux sentiments d’artistes ;</l>
						<l n="100" num="1.18">Oublier le pathos de nos <hi rend="ital">so_ci_a_lis_tes</hi>,</l>
						<l n="101" num="1.19">Et pour tromper l’ennui vous plonger au besoin</l>
						<l n="102" num="1.20">Dans ce monde d’hier qui déjà semble loin !</l>
						<l n="103" num="1.21">Bien qu’en dise Burger, la tombe au calme invite ;</l>
						<l n="104" num="1.22">Mais ce sont maintenant les vivants qui vont vite ;</l>
						<l n="105" num="1.23">Et l’on n’a pas plus tôt ouvert les yeux au jour,</l>
						<l n="106" num="1.24">Que l’on voit tous les rois renversés tour à tour ;</l>
						<l n="107" num="1.25">Qu’on entend retentir, dans les villes en larmes,</l>
						<l n="108" num="1.26">Le tambour, le canon et la cloche d’alarmes…</l>
						<l n="109" num="1.27">Quand l’émeute sans frein s’élance en rugissant</l>
						<l n="110" num="1.28">Comme un tigre altéré de carnage et de sang,</l>
						<l n="111" num="1.29">Et que nos monuments sont des champs de bataille</l>
						<l n="112" num="1.30">Où l’Art sacrifié périt sous la mitraille.</l>
					</lg>
				</div>
				<div n="4" type="section">
					<head type="number">IV</head>
					<lg n="1">
						<l n="113" num="1.1"><space unit="char" quantity="2"></space>Tâchons, fût-ce un instant, de fixer nos regards</l>
						<l n="114" num="1.2">Sur des temps plus heureux pour l’esprit et les arts.</l>
						<l n="115" num="1.3">Voyons passer la vie ainsi qu’une eau qui change</l>
						<l n="116" num="1.4">Et s’éclaire parfois d’une lueur étrange,</l>
						<l n="117" num="1.5">Limpide aussi parfois quand l’azur reflété</l>
						<l n="118" num="1.6">N’a pas sous le brouillard perdu sa pureté.</l>
						<l n="119" num="1.7">Faisons poser le monde, ainsi que sur la scène</l>
						<l n="120" num="1.8">Se meut devant nos yeux la Comédie humaine.</l>
						<l n="121" num="1.9">Avec vos oripeaux, vos masques, vos galons,</l>
						<l n="122" num="1.10">Défilez tour à tour, acteurs de nos salons.</l>
						<l n="123" num="1.11"><space unit="char" quantity="2"></space>— Mais, dira-t-on, pourquoi ne pas conter en prose ? —</l>
						<l n="124" num="1.12">Si la chose est nouvelle, eh bien ! tentons la chose.</l>
						<l n="125" num="1.13">Lafontaine écrivait : « Si je n’obtiens le prix,</l>
						<l n="126" num="1.14">« <hi rend="ital">J’aurai du moins l’honneur de l’avoir entrepris.</hi> »</l>
						<l n="127" num="1.15">Accueillez un essai ; dans le siècle où nous sommes,</l>
						<l n="128" num="1.16">Lorsqu’on réforme tout, la nature, les hommes,</l>
						<l n="129" num="1.17">Et les gouvernements, et les lois et les mœurs,</l>
						<l n="130" num="1.18">Sera-t-on sans pitié pour les pauvres rimeurs ?</l>
						<l n="131" num="1.19">Et devront-ils garder un silence stoïque,</l>
						<l n="132" num="1.20">Ou bien, de par Platon, quitter la république ?</l>
						<l n="133" num="1.21">Vous qui nous refusez un grain pour subsister,</l>
						<l n="134" num="1.22">Prosaïques fourmis, laissez-nous donc chanter !</l>
					</lg>
				</div>
			</div></body></text></TEI>