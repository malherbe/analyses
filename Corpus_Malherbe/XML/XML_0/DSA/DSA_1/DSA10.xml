<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA COMÉDIE DU MONDE</title>
				<title type="medium">Édition électronique</title>
				<author key="DSA">
					<name>
						<forename>Alfred</forename>
						<nameLink>de</nameLink>
						<surname>ESSARTS</surname>
					</name>
					<date from="1811" to="1893">1811-1893</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>5551 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">DSA_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA COMÉDIE DU MONDE</title>
						<author>Alfred des Essarts</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k5446283w?rk=429186</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title type="main">LA COMÉDIE DU MONDE</title>
								<author>Alfred des Essarts</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher> Comptoir des imprimeurs-unis</publisher>
									<date when="1851">1851</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1851">1851</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension(…).</p>
					<p>Les tirets simples précédés et/ou suivis d’une espace ont été remplacés par des tirets demi-cadratin</p>
					<p>
						Le signe "_" (_) est utilisé à la place du trait d’union pour séparer les syllabes
						du mot "so-ci-a-lis-tes" afin de préserver l’unité du mot dans le traitement des rimes.
					</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-04-25" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-04-25" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DSA10">
				<head type="number">IX</head>
				<head type="main">CLICHY</head>
				<lg n="1">
					<l n="1" num="1.1"><space unit="char" quantity="2"></space>Entre le déshonneur et l’ennui, quel abîme !</l>
					<l n="2" num="1.2">Ne pouvoir échapper au dernier sans un crime ;</l>
					<l n="3" num="1.3">Avoir lutté, sentir qu’au bout de tout effort</l>
					<l n="4" num="1.4">En voulant rester pur on reste le moins fort</l>
					<l n="5" num="1.5">Que de l’oppression il faut subir l’outrage,</l>
					<l n="6" num="1.6">Ou se faire à son tour oppresseur ; quel courage</l>
					<l n="7" num="1.7">Tiendrait contre le sort qui ne nous rend vainqueur</l>
					<l n="8" num="1.8">Après de longs combats, qu’en abaissant le cœur ?</l>
					<l n="9" num="1.9"><space unit="char" quantity="2"></space>Voilà deux mois déjà qu’une étroite cellule</l>
					<l n="10" num="1.10">Emprisonne Caron, victime d’un scrupule ;</l>
					<l n="11" num="1.11">Deux mois que jour par jour l’infortuné se dit :</l>
					<l n="12" num="1.12">« — Quand pourrai-je quitter ce cloaque maudit ?</l>
					<l n="13" num="1.13">Rien ne m’en tirera, non rien, quoi qu’il arrive.</l>
					<l n="14" num="1.14">De cinq ans de langueur j’ai donc la perspective !</l>
					<l n="15" num="1.15">Et pas un seul, pas un de ceux qui m’ont connu</l>
					<l n="16" num="1.16">Par un reste d’égards près de moi n’est venu ;</l>
					<l n="17" num="1.17">Pas un seul n’a daigné, généreux et sincère,</l>
					<l n="18" num="1.18">M’apporter le tribut de la pitié d’un frère.</l>
					<l n="19" num="1.19">Oh ! c’est à détester l’infâme genre humain !</l>
					<l n="20" num="1.20">Contre soi, pour le fuir, on armerait sa main…</l>
					<l n="21" num="1.21">J’ai méconnu Timon dans sa misanthropie :</l>
					<l n="22" num="1.22">Funeste illusion que maintenant j’expie.</l>
					<l n="23" num="1.23">Je donnais volontiers, me contentant de peu</l>
					<l n="24" num="1.24">Et tâchant seulement de soutenir mon jeu ;</l>
					<l n="25" num="1.25">J’oubliais mes ennuis lorsque mes camarades</l>
					<l n="26" num="1.26">Dans ma chambre avec moi partageaient des rasades ;</l>
					<l n="27" num="1.27">Ils m’ont abandonné, les lâches, les ingrats !…</l>
					<l n="28" num="1.28">Des amis ? Je le vois, le malheur n’en a pas. »</l>
					<l n="29" num="1.29"><space unit="char" quantity="2"></space>Un seul était venu ; vous le nommez d’avance :</l>
					<l n="30" num="1.30">Paul Firmin, noble cœur, vivante providence.</l>
					<l n="31" num="1.31">Sans estimer Caron il le plaignait du moins</l>
					<l n="32" num="1.32">Et délicatement pourvut à ses besoins. —</l>
					<l n="33" num="1.33">« Cet argent, lui dit-il, est le fruit d’une quête. »</l>
					<l n="34" num="1.34">Tout en remerciant Caron hocha la tête.</l>
					<l n="35" num="1.35">Il savait maintenant, à n’en pouvoir douter,</l>
					<l n="36" num="1.36">Qu’il faut sur ses amis bien rarement compter.</l>
					<l n="37" num="1.37"><space unit="char" quantity="2"></space>Plus tard, un sentiment de farouche colère</l>
					<l n="38" num="1.38">Le rendit méfiant, sauvage, atrabilaire.</l>
					<l n="39" num="1.39">Enfermé dans sa chambre, il y passait son temps</l>
					<l n="40" num="1.40">Et n’allait au préau qu’à de rares instants.</l>
					<l n="41" num="1.41">On approchait de mai, ce mois où la nature</l>
					<l n="42" num="1.42">Se couvre d’un manteau de fleurs et de verdure ;</l>
					<l n="43" num="1.43">Ce mois du <hi rend="ital">renouveau</hi>, poétique saison.</l>
					<l n="44" num="1.44">Le soleil ne luit pas au sein d’une prison.</l>
					<l n="45" num="1.45">Le pauvre détenu — tel que dans une cage</l>
					<l n="46" num="1.46">L’oiseau qui voit s’étendre un lointain paysage</l>
					<l n="47" num="1.47">Bat de l’aile et gémit, — le pauvre détenu</l>
					<l n="48" num="1.48">Se sentit une ardeur, un besoin inconnu,</l>
					<l n="49" num="1.49">Une soif d’être libre. Il se dit : — « C’est folie</l>
					<l n="50" num="1.50">Que de m’éteindre ici dans la mélancolie,</l>
					<l n="51" num="1.51">Quand un mot peut me rendre au monde des vivants.</l>
					<l n="52" num="1.52">Grandes phrases d’honneur, allez à tous les vents !</l>
					<l n="53" num="1.53">Préjugés que l’on sert, pour lesquels on s’immole,</l>
					<l n="54" num="1.54">Comme si, de tout temps, il fallait quelque idole,</l>
					<l n="55" num="1.55">A votre joug pesant si je me suis soumis,</l>
					<l n="56" num="1.56">Loin de moi !… Mes amis ne sont plus des amis.</l>
					<l n="57" num="1.57">La prison est d’ailleurs un bon apprentissage :</l>
					<l n="58" num="1.58">Ainsi que moi, plus d’un y peut devenir sage. »</l>
					<l n="59" num="1.59"><space unit="char" quantity="2"></space>Son plan sitôt conçu, Caron fit prévenir</l>
					<l n="60" num="1.60">L’usurier ; celui-ci s’empressa de venir.</l>
					<l n="61" num="1.61"><space unit="char" quantity="2"></space>Caron en souriant lui dit : « — Homme intraitable,</l>
					<l n="62" num="1.62">Qui saisissez le corps, ainsi que fait le diable</l>
					<l n="63" num="1.63">Pour l’âme pécheresse, avant peu, je le croi,</l>
					<l n="64" num="1.64">Vous lèverez l’écrou que vous mîtes sur moi.</l>
					<l n="65" num="1.65">— Bah ! quels sont vos moyens pour payer cette dette ?</l>
					<l part="I" n="66" num="1.66">— Ma bourse est vide. </l>
					<l part="M" n="66" num="1.66">— Alors ?… </l>
					<l part="F" n="66" num="1.66">— Ma ruine est complète.</l>
					<l n="67" num="1.67">— Eh bien ! fallait-il donc me déranger ainsi ?</l>
					<l n="68" num="1.68">L’on use ses souliers à grimper jusqu’ici.</l>
					<l n="69" num="1.69">— Monsieur, vous toucherez tout votre argent en ville.</l>
					<l n="70" num="1.70">Vous n’avez qu’à passer chez le duc de Surville. »</l>
					<l part="I" n="71" num="1.71"><space unit="char" quantity="2"></space>L’usurier se troubla. </l>
					<l part="F" n="71" num="1.71">« — J’avais tout deviné,</l>
					<l n="72" num="1.72">Dit Caron, et le tour fut bien imaginé.</l>
					<l n="73" num="1.73">Vous n’étiez qu’un agent, un moyen… Mais qu’importe !</l>
					<l n="74" num="1.74">L’essentiel pour moi c’est qu’on m’ouvre la porte.</l>
					<l n="75" num="1.75">Monsieur, vous remettrez au duc avec grand soin</l>
					<l n="76" num="1.76">Le paquet que voici : surtout pas de témoin !</l>
					<l n="77" num="1.77">A beaux deniers comptants se paie un tel message.</l>
					<l n="78" num="1.78">J’espère m’envoler en oiseau de passage ;</l>
					<l n="79" num="1.79">Et dès demain, morbleu ! loin de ces murs maudits,</l>
					<l n="80" num="1.80">Réchauffer au soleil mes membres engourdis. »</l>
					<l n="81" num="1.81"><space unit="char" quantity="2"></space>Le juif prit le paquet, et sous sa houppelande</l>
					<l n="82" num="1.82">Le cacha prudemment ; le soir, sur sa demande,</l>
					<l n="83" num="1.83">On appelait Caron, afin de l’avertir</l>
					<l n="84" num="1.84">Qu’il n’avait plus de dette et qu’il pouvait partir.</l>
					<l n="85" num="1.85"><space unit="char" quantity="2"></space>Huit jours sont écoulés ; tel qu’un chasseur en plaine</l>
					<l n="86" num="1.86">Caron vit au grand air, il fume, il se promène.</l>
					<l n="87" num="1.87">Et voici qu’il apprend, d’une part, qu’on a mis</l>
					<l n="88" num="1.88">A l’ <hi rend="ital">ombre</hi>, c’est-à-dire en prison ses amis,</l>
					<l n="89" num="1.89">Tous accusés d’avoir, dans un club anarchique,</l>
					<l n="90" num="1.90">Tenté de renverser la forme monarchique.</l>
					<l n="91" num="1.91">Notre homme ne craint rien, grâce au duc qui pour lui</l>
					<l n="92" num="1.92">A daigné devenir un patron, un appui ;</l>
					<l n="93" num="1.93">De ses anciens amis à son aise il se raille,</l>
					<l n="94" num="1.94">Et rit comme la Mort dans un jour de bataille.</l>
					<l part="I" n="95" num="1.95"><space unit="char" quantity="2"></space>D’autre part, chez le duc il est mandé. </l>
					<l part="F" n="95" num="1.95">« — Mon cher,</l>
					<l n="96" num="1.96">Je suis content de vous ; il m’en coûte un peu cher,</l>
					<l n="97" num="1.97">Le moyen était bon selon ma prophétie ;</l>
					<l n="98" num="1.98">Il me fera rentrer dans la diplomatie ;</l>
					<l n="99" num="1.99">Je reprends mon crédit et suis ambassadeur. »</l>
					<l n="100" num="1.100"><space unit="char" quantity="2"></space>Caron se prosterna devant tant de grandeur.</l>
					<l n="101" num="1.101">Le duc en souriant lui dit avec finesse</l>
					<l n="102" num="1.102">« — Je n’ai pas oublié mon ancienne promesse.</l>
					<l n="103" num="1.103">Les papiers que voici valent dix mille francs. »</l>
					<l part="I" n="104" num="1.104"><space unit="char" quantity="2"></space>Caron rougit. </l>
					<l part="F" n="104" num="1.104">« — Monsieur le duc, j e ne les prends,</l>
					<l n="105" num="1.105">Dit-il, qu’avec dégoût, en m’accusant moi-même.</l>
					<l n="106" num="1.106">— C’est là, dit le vieillard, un langage que j’aime.</l>
					<l n="107" num="1.107">Vous avez des défauts ; mais on peut, sur ma foi,</l>
					<l n="108" num="1.108">Vous amener à bien, et je l’essaîrai, moi.</l>
					<l n="109" num="1.109">Très-attentivement écoutez-moi, jeune homme :</l>
					<l n="110" num="1.110">S’il vous convenait mieux d’emporter cette somme,</l>
					<l n="111" num="1.111">Elle est à vous… prenez, vous ne reviendrez plus.</l>
					<l n="112" num="1.112">Mais si vous préférez être, avec mille écus,</l>
					<l n="113" num="1.113">Mon secrétaire, alors auprès de ma personne</l>
					<l n="114" num="1.114">Vous resterez ; pour vous la chance est assez bonne. »</l>
					<l n="115" num="1.115"><space unit="char" quantity="2"></space>Caron, ivre de joie, alla tomber aux pieds</l>
					<l part="I" n="116" num="1.116">Du duc. </l>
					<l part="F" n="116" num="1.116">« — Remettez-vous, dit Surville, et soyez</l>
					<l n="117" num="1.117">Bien certain qu’envers vous je tiendrai ma parole.</l>
					<l part="I" n="118" num="1.118">— A vous mon dévoûment ! </l>
					<l part="F" n="118" num="1.118">— Maintenant votre rôle</l>
					<l n="119" num="1.119">Change complètement ; il vous faudra, Caron,</l>
					<l n="120" num="1.120">Vous pénétrer de l’air qu’exhale ce salon,</l>
					<l n="121" num="1.121">Être grave, profond, cacher votre pensée,</l>
					<l n="122" num="1.122">Rire peu, parler peu, marcher tête baissée,</l>
					<l n="123" num="1.123">Paraître sérieux, pour qu’on dise, à vous voir ;</l>
					<l n="124" num="1.124">« C’est un homme <hi rend="ital">posé</hi>, tout entier au devoir. »</l>
					<l n="125" num="1.125">Allez ; votre logis sera prêt dans une heure ;</l>
					<l n="126" num="1.126">Vous aurez dans l’hôtel travaux, table et demeure. »</l>
					<l n="127" num="1.127"><space unit="char" quantity="2"></space>Le nouveau secrétaire en deux bonds s’élança</l>
					<l n="128" num="1.128">Vers son ancien réduit ; dans l’âtre il entassa</l>
					<l n="129" num="1.129">Ses lettres, ses papiers, ses manuscrits : la flamme</l>
					<l n="130" num="1.130">Eut bientôt consumé ces débris que son âme</l>
					<l n="131" num="1.131">Reniait à jamais, ainsi qu’un pèlerin</l>
					<l n="132" num="1.132">D’importuns compagnons s’éloigne sans chagrin.</l>
					<l n="133" num="1.133">Comme il avait touché son premier mois d’avance,</l>
					<l n="134" num="1.134">Dans le Palais-Royal il fit en diligence</l>
					<l n="135" num="1.135">Choix d’un habillement qui fût <hi rend="ital">grave</hi> ; en effet</l>
					<l n="136" num="1.136">Le duc, quand il rentra, parut très-satisfait.</l>
					<l n="137" num="1.137">Depuis ce jour, Caron adopta les lunettes.</l>
					<l n="138" num="1.138">Humble et flatteur, le duc disait-il des sornettes,</l>
					<l n="139" num="1.139">Caron applaudissait, Garon trouvait tout bien ;</l>
					<l n="140" num="1.140">Il vantait dans son maître et discours et maintien ;</l>
					<l n="141" num="1.141">Il lui donnait des soins comme un valet de chambre,</l>
					<l n="142" num="1.142">Et se courbait toujours, sans être un <hi rend="ital">fier Sicambre</hi>.</l>
					<l n="143" num="1.143">Firmin le grondait bien sur cette humilité ;</l>
					<l n="144" num="1.144">Mais Caron persistait dans sa docilité :</l>
					<l n="145" num="1.145">Tellement, qu’il était devenu, pour Surville,</l>
					<l n="146" num="1.146">Indispensable… sans avoir l’air d’être utile.</l>
					<l n="147" num="1.147"><space unit="char" quantity="2"></space>Le duc au Luxembourg s’était rendu. Caron</l>
					<l n="148" num="1.148">Ayant battu Paris rentrait chez son patron ;</l>
					<l part="I" n="149" num="1.149">Une voix l’appela. </l>
					<l part="M" n="149" num="1.149">« — Tiens, Forbain ! </l>
					<l part="F" n="149" num="1.149">— Quelle chance !</l>
					<l part="I" n="150" num="1.150">Où vas-tu ? d’où viens-tu, Caron ? </l>
					<l part="F" n="150" num="1.150">— Mais… je me lance.</l>
					<l part="I" n="151" num="1.151">Et toi ? </l>
					<l part="F" n="151" num="1.151">— J’ai du bonheur ; on nous a dénoncés ;</l>
					<l n="152" num="1.152">Dans toutes les prisons nos amis sont placés ;</l>
					<l n="153" num="1.153">Seul j’ai pu me soustraire à la crise commune…</l>
					<l n="154" num="1.154">Et mon oncle en mourant me laisse sa fortune. »</l>
					<l n="155" num="1.155"><space unit="char" quantity="2"></space>Nous devons expliquer un fait encor voilé.</l>
					<l n="156" num="1.156">Caron, aimant Forbain, n’avait pas signalé</l>
					<l n="157" num="1.157">Son ancien camarade aux coups de la justice.</l>
					<l n="158" num="1.158"><space unit="char" quantity="2"></space>Forbain le contemplait avec quelque malice.</l>
					<l part="I" n="159" num="1.159">« — Qu’ai-je donc d’étonnant ? </l>
					<l part="F" n="159" num="1.159">— C’est merveilleux à voir,</l>
					<l n="160" num="1.160">Lui dit Forbain, Caron tout habillé de noir !</l>
					<l n="161" num="1.161">Gants paille, chapeau large et la cravate blanche,</l>
					<l n="162" num="1.162">Ainsi qu’un marguillier sur son banc le dimanche.</l>
					<l part="I" n="163" num="1.163">— Ai-je l’air <hi rend="ital">sérieux</hi> ? </l>
					<l part="F" n="163" num="1.163">— Oui, même un peu pédant.</l>
					<l part="I" n="164" num="1.164">— Bravo ! c’est ma tenue aujourd’hui. </l>
					<l part="F" n="164" num="1.164">— Cependant</l>
					<l n="165" num="1.165">Tu me plaisais bien mieux en nos temps de folie,</l>
					<l part="I" n="166" num="1.166">Avec ton paletot de pauvre artiste. </l>
					<l part="F" n="166" num="1.166">— Oublie</l>
					<l n="167" num="1.167">Ces jours que pour ma part je voudrais renier.</l>
					<l n="168" num="1.168">A de nouveaux devoirs je saurai me plier.</l>
					<l n="169" num="1.169">Un noble ambassadeur m’attache à sa personne</l>
					<l n="170" num="1.170">Secrétaire d’un duc ! c’est un titre qui sonne !</l>
					<l part="I" n="171" num="1.171">— Dis son premier laquais. </l>
					<l part="F" n="171" num="1.171">— Va, tu ne comprends pas.</l>
					<l n="172" num="1.172">— Quoi ! tu t’assocîrais à tous ses mauvais pas ?</l>
					<l n="173" num="1.173">— Je vais mettre le pied sur la mobile roue</l>
					<l part="I" n="174" num="1.174">Qui porte la fortune. </l>
					<l part="F" n="174" num="1.174">— Et souvent nous secoue !</l>
					<l n="175" num="1.175">— Mon costume à présent doit s’expliquer pour toi.</l>
					<l n="176" num="1.176">Il faut avoir d’abord l’habit de son emploi.</l>
					<l n="177" num="1.177">Oui, mon cher, trop longtemps ignoré de moi-même,</l>
					<l n="178" num="1.178">Avec vous j’ai mené l’existence bohème ;</l>
					<l n="179" num="1.179">Je me range, et deviens un homme sérieux.</l>
					<l part="I" n="180" num="1.180">— Toi, Caron, se peut-il ? </l>
					<l part="F" n="180" num="1.180">— Daigne en croire tes yeux.</l>
					<l n="181" num="1.181">En rude champion soutenant la morale,</l>
					<l n="182" num="1.182">Je parlerai toujours d’une voix magistrale,</l>
					<l n="183" num="1.183">Comme Monsieur Guizot notre digne patron.</l>
					<l n="184" num="1.184">Je prendrai volontiers le peuple pour plastron.</l>
					<l n="185" num="1.185">A la démocratie, à ses sourdes intrigues</l>
					<l n="186" num="1.186">De mes raisonnements j’opposerai les digues.</l>
					<l n="187" num="1.187">Un jour probablement je serai député :</l>
					<l n="188" num="1.188">Personne mieux que moi n’aura jamais voté.</l>
					<l n="189" num="1.189">Mais avant tout je suis <hi rend="ital">sérieux</hi>. C’est la mode.</l>
					<l n="190" num="1.190">Les plus brillants succès sont écrits dans ce code.</l>
					<l n="191" num="1.191">Si l’on a de l’esprit, de l’ <hi rend="ital">humour</hi>, du savoir,</l>
					<l n="192" num="1.192">Néant ! sans un front grave et sans un habit noir.</l>
					<l n="193" num="1.193">Voulez-vous des emplois ? traitez l’Économie.</l>
					<l n="194" num="1.194">Pour obtenir l’honneur d’être à l’Académie</l>
					<l n="195" num="1.195">N’écrivez pas, ou bien à des traductions</l>
					<l n="196" num="1.196">Bornez, homme prudent, vos méditations.</l>
					<l n="197" num="1.197">N’essayez rien de neuf, étouffez votre muse ;</l>
					<l n="198" num="1.198">Blâmez, blâmez beaucoup, et soyez sans excuse</l>
					<l n="199" num="1.199">Pour tous les travailleurs de ce siècle d’airain</l>
					<l n="200" num="1.200">Qui n’ont pas leur Capoue au Palais-Mazarin.</l>
					<l n="201" num="1.201">Inventez une étoile… un système… une langue…</l>
					<l n="202" num="1.202">Cultivez la grammaire, et surtout la harangue :</l>
					<l n="203" num="1.203">Les hommes moutonniers seront à vos genoux,</l>
					<l n="204" num="1.204">Et le budget aura des largesses pour vous.</l>
					<l n="205" num="1.205">— Vraiment, je suis confus… A la démocratie</l>
					<l part="I" n="206" num="1.206">Tu renonces ! </l>
					<l part="F" n="206" num="1.206">— Ce n’est qu’une atroce ineptie.</l>
					<l n="207" num="1.207">On peut, à dix-huit ans, rêver l’égalité,</l>
					<l n="208" num="1.208">Faire en son cœur un temple à la fraternité.</l>
					<l n="209" num="1.209">Mais plus tard, quand on juge et l’effet et la cause,</l>
					<l n="210" num="1.210">Que les républicains paraissent peu de chose !</l>
					<l n="211" num="1.211">Au fils il faut un père, au peuple il faut un roi.</l>
					<l n="212" num="1.212">Toute société se base sur la loi.</l>
					<l n="213" num="1.213">— Allons, te voilà grave et peut-être trop sage.</l>
					<l part="I" n="214" num="1.214">A quand la croix d’honneur ? </l>
					<l part="F" n="214" num="1.214">— Après l’apprentissage.</l>
					<l part="I" n="215" num="1.215">— A quand l’Académie ? </l>
					<l part="F" n="215" num="1.215">— Attends encor, mon vieux,</l>
					<l n="216" num="1.216">Quand j’aurai publié quelque livre ennuyeux,</l>
					<l n="217" num="1.217">Et qu’il sera prouvé par le poids de l’ouvrage</l>
					<l n="218" num="1.218">Qu’aux Trente-Neuf mon nom ne porte pas ombrage.</l>
					<l n="219" num="1.219">— Décidément, mon cher, ton destin sera beau.</l>
					<l n="220" num="1.220">Mais crains que ton esprit n’y trouve son tombeau.</l>
					<l n="221" num="1.221">— L’esprit !… Lorsqu’on en vit, la famine est certaine.</l>
					<l n="222" num="1.222">— Il est vrai… Cependant écoute Lafontaine :</l>
					<l n="223" num="1.223">« Attaché, dit le loup, vous ne courez-donc pas</l>
					<l n="224" num="1.224"><space unit="char" quantity="4"></space>« Où vous voulez ? — Pas toujours ; mais qu’importe ?</l>
					<l n="225" num="1.225">« — Il importe si bien que de tous vos repas</l>
					<l n="226" num="1.226"><space unit="char" quantity="8"></space>« Je ne veux en aucune sorte</l>
					<l n="227" num="1.227">« Et ne voudrais pas même à ce prix un trésor. » »</l>
					<l n="228" num="1.228">Cela dit, maître loup s’enfuit et court encor. »</l>
					<l n="229" num="1.229">Je suis le loup farouche ; adieu donc, chien docile.</l>
					<l n="230" num="1.230">— Forbain, sans compliment tu n’es qu’un imbécile. »</l>
					<l n="231" num="1.231"><space unit="char" quantity="2"></space>Que de gens aujourd’hui seraient prêts à bénir</l>
					<l n="232" num="1.232">Le collier, s’ils pouvaient à ce prix parvenir !</l>
				</lg>
			</div></body></text></TEI>