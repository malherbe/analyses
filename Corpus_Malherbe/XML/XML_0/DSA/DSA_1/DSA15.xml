<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA COMÉDIE DU MONDE</title>
				<title type="medium">Édition électronique</title>
				<author key="DSA">
					<name>
						<forename>Alfred</forename>
						<nameLink>de</nameLink>
						<surname>ESSARTS</surname>
					</name>
					<date from="1811" to="1893">1811-1893</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>5551 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">DSA_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA COMÉDIE DU MONDE</title>
						<author>Alfred des Essarts</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k5446283w?rk=429186</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title type="main">LA COMÉDIE DU MONDE</title>
								<author>Alfred des Essarts</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher> Comptoir des imprimeurs-unis</publisher>
									<date when="1851">1851</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1851">1851</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension(…).</p>
					<p>Les tirets simples précédés et/ou suivis d’une espace ont été remplacés par des tirets demi-cadratin</p>
					<p>
						Le signe "_" (_) est utilisé à la place du trait d’union pour séparer les syllabes
						du mot "so-ci-a-lis-tes" afin de préserver l’unité du mot dans le traitement des rimes.
					</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-04-25" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-04-25" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DSA15">
				<head type="number">XIV</head>
				<head type="main">UN SOUPER-RÉGENCE</head>
				<lg n="1">
					<l n="1" num="1.1"><space unit="char" quantity="2"></space>Florida, Rozemon, Enguerrand et cinq drôles,</l>
					<l n="2" num="1.2">Débiteurs de l’habit qu’ils ont sur leurs épaules,</l>
					<l n="3" num="1.3">Catilinas du whist, aigrefins élégants,</l>
					<l n="4" num="1.4">Qu’un préjugé bourgeois admire sur leurs gants,</l>
					<l n="5" num="1.5">Pour un souper joyeux, pour une ample curée,</l>
					<l n="6" num="1.6">Ont pris un rendez-vous à la Maison dorée :</l>
					<l n="7" num="1.7">Restaurant où le Diable, aux nuits de carnaval,</l>
					<l n="8" num="1.8">La serviette à la main, tient son grand festival.</l>
					<l n="9" num="1.9">Oh ! dans ces cabinets où saute le champagne,</l>
					<l n="10" num="1.10">Que d’esprits insensés ont battu la campagne !</l>
					<l n="11" num="1.11">Que d’étourdis, charmés par un air fraternel,</l>
					<l n="12" num="1.12">Ont mangé l’avenir et le bien paternel</l>
					<l n="13" num="1.13">Avec ces faux amis dont la troupe s’envole</l>
					<l n="14" num="1.14">Quand au fond de la poche il n’est plus une obole ;</l>
					<l n="15" num="1.15">Après à la curée et prompts à délaisser</l>
					<l n="16" num="1.16">Le sot qui sur leurs pas a voulu s’élancer :</l>
					<l n="17" num="1.17">Saluant son réveil, lorsqu’il sort du délire,</l>
					<l n="18" num="1.18">Par l’infernal adieu de leur éclat de rire.</l>
					<l n="19" num="1.19"><space unit="char" quantity="2"></space>Jamais un œil humain, je crois, ne regarda</l>
					<l n="20" num="1.20">Rien de plus séduisant que cette Florida.</l>
					<l n="21" num="1.21">Si je la décrivis lorsqu’elle était Cécile,</l>
					<l n="22" num="1.22">La bien peindre aujourd’hui serait plus difficile.</l>
					<l n="23" num="1.23">Sous l’étreinte et l’ardeur des passions du mal</l>
					<l n="24" num="1.24">Elle a pris dans sa grâce un air original.</l>
					<l n="25" num="1.25">Le vice offre à l’esprit une pente terrible</l>
					<l n="26" num="1.26">La vertu n’a jamais ce charme irrésistible ;</l>
					<l n="27" num="1.27">Tandis que la pudeur voile ses agréments,</l>
					<l n="28" num="1.28">La luxure s’en fait d’utiles instruments.</l>
					<l n="29" num="1.29">La pauvre Cendrillon s’étiole dans l’âtre ;</l>
					<l n="30" num="1.30">Mais dès qu’avec fracas, sur un autre théâtre</l>
					<l n="31" num="1.31">Elle paraît, portant un costume de cour,</l>
					<l n="32" num="1.32">Chacun de ses regards fait éclore l’amour.</l>
					<l n="33" num="1.33"><space unit="char" quantity="2"></space>A ces tentations de parures, de fêtes,</l>
					<l n="34" num="1.34">Vont se perdre aujourd’hui les cœurs les plus honnêtes.</l>
					<l n="35" num="1.35">« Quoi ! je consumerais dans un obscur métier</l>
					<l n="36" num="1.36">« Et dans la pauvreté mon printemps tout entier ;</l>
					<l n="37" num="1.37">« Certaine qu’à mes vœux l’avenir se dérobe,</l>
					<l n="38" num="1.38">« J’aurais pour me couvrir une mesquine robe,</l>
					<l n="39" num="1.39">« La même dans l’hiver comme au temps le plus chaud…</l>
					<l n="40" num="1.40">« Vivant dans ma mansarde ainsi qu’en un cachot !</l>
					<l n="41" num="1.41">« Pourquoi donc au bonheur resterais-je rebelle ?</l>
					<l n="42" num="1.42">« Pourquoi la pauvreté quand je suis jeune et belle ? »</l>
					<l n="43" num="1.43"><space unit="char" quantity="2"></space>C’est ainsi que Satan, dans le Quartier latin,</l>
					<l n="44" num="1.44">Récolte chaque jour un fort joli butin.</l>
					<l n="45" num="1.45">Pour atteindre aisément le but qu’il se propose,</l>
					<l n="46" num="1.46">Lui-même en cent façons il se métamorphose :</l>
					<l n="47" num="1.47">La soie aux doux reflets, le bouquet odorant,</l>
					<l n="48" num="1.48">L’orchestre dans le bal, la carte au restaurant,</l>
					<l n="49" num="1.49">Le gentil brodequin qui rend le pied si leste,</l>
					<l n="50" num="1.50">L’ombrelle, la capote, élégance modeste,</l>
					<l n="51" num="1.51">Tout cela c’est Satan, docteur ès-passions.</l>
					<l n="52" num="1.52">Filles qu’il veut tenter, gare aux séductions !</l>
					<l n="53" num="1.53">Satan a pour convaincre un air si bon apôtre…</l>
					<l n="54" num="1.54">Le plaisir qu’on accepte est le chemin d’un autre.</l>
					<l n="55" num="1.55">A peine avec le mal a-t-on capitulé,</l>
					<l n="56" num="1.56">Que le but va fuyant, sans cesse reculé.</l>
					<l n="57" num="1.57">Hier on ne voulait que se distraire une heure ;</l>
					<l n="58" num="1.58">Aujourd’hui dans le bal jusqu’à l’aube on demeure.</l>
					<l n="59" num="1.59">Mieux logée, on se trouve encor trop simplement :</l>
					<l n="60" num="1.60">Il faut, Cité Trévise, un bel appartement.</l>
					<l n="61" num="1.61">Eh quoi ! se fatiguer à marcher dans la boue ?</l>
					<l n="62" num="1.62">Vite un leste <hi rend="ital">brougham</hi> qui sur sa double roue</l>
					<l n="63" num="1.63">Emportera Madame aux endroits fréquentés</l>
					<l n="64" num="1.64">Où vont cavalcader les <hi rend="ital">lions</hi> bien rentés.</l>
					<l n="65" num="1.65"><space unit="char" quantity="2"></space>Et Satan rit alors ; cet ennemi de l’âme</l>
					<l n="66" num="1.66">Dans ses piéges toujours fera tomber la femme.</l>
					<l n="67" num="1.67"><space unit="char" quantity="2"></space>La parenthèse est longue… Excusez, mais il faut</l>
					<l n="68" num="1.68">Parfois être un peu long, et c’est un bon défaut.</l>
					<l n="69" num="1.69">Si l’on voit aujourd’hui tant de prostituées,</l>
					<l n="70" num="1.70">C’est que pour la vertu l’on n’a que des huées</l>
					<l n="71" num="1.71">Et qu’on tient en dédain le travail, le talent,</l>
					<l n="72" num="1.72">Si la loi du hasard ne l’a fait opulent.</l>
					<l n="73" num="1.73">Telle qu’un passereau que le miroir captive,*</l>
					<l n="74" num="1.74">A l’appel du plaisir devenue attentive,</l>
					<l n="75" num="1.75">Cécile avait compris le charme souverain</l>
					<l n="76" num="1.76">De briller, de tenir les hommes sous son frein ;</l>
					<l n="77" num="1.77">Et bientôt surpassant nos Phrynés à la mode,</l>
					<l n="78" num="1.78">Elle avait excellé dans ce métier commode.</l>
					<l n="79" num="1.79">Plus elle avait souffert de dégoûts et d’ennui,</l>
					<l n="80" num="1.80">Plus elle s’en vengeait maintenant sur celui</l>
					<l n="81" num="1.81">Qui subissait son joug ; avec quelque volume</l>
					<l n="82" num="1.82">Que l’on entrât chez elle, on y laissait sa plume.</l>
					<l n="83" num="1.83"><space unit="char" quantity="2"></space>Elle était magnifique à voir auprès d’Arthur,</l>
					<l n="84" num="1.84">Levant son verre plein d’un alicante pur,</l>
					<l n="85" num="1.85">Les cheveux déroulés, la gorge bondissante,</l>
					<l n="86" num="1.86">Et d’un appel hardi la lèvre frémissante.</l>
					<l n="87" num="1.87">« — Je crois, mes chers enfants, que le plaisir languit,</l>
					<l n="88" num="1.88">Dit-elle ; employons mieux le reste de la nuit.</l>
					<l n="89" num="1.89">Nous n’avons pas encor salué comme un frère</l>
					<l n="90" num="1.90">Le punch, ce feu qu’on boit dans le cristal du verre.</l>
					<l n="91" num="1.91">Ne sait-on plus souper ? Allons donc, Rozemon,</l>
					<l n="92" num="1.92">Tu prends un air de saint, toi le plus franc démon.</l>
					<l n="93" num="1.93">— Comme elle me connaît ! s’écria le jeune homme.</l>
					<l n="94" num="1.94">Mais toi, comment aussi faut-il que l’on te nomme ;</l>
					<l n="95" num="1.95">Toi qui, grisette hier, as si promptement pris</l>
					<l n="96" num="1.96">Le genre et les façons qui plaisent à Paris ?</l>
					<l n="97" num="1.97">Ce changement subit augmente encor tes charmes,</l>
					<l n="98" num="1.98">Et pour nous subjuguer il te prête des armes.</l>
					<l part="I" n="99" num="1.99">Révèle ton secret. </l>
					<l part="F" n="99" num="1.99">— Je n’en ai pas. L’ennui</l>
					<l n="100" num="1.100">M’a faite tout d’un coup ce qu’on voit aujourd’hui.</l>
					<l n="101" num="1.101">— Mais ce ton élégant… — Beau secret ! je suis femme,</l>
					<l n="102" num="1.102">— Dis donc magicienne habile à prendre une âme.</l>
					<l n="103" num="1.103">— Mon cher Arthur, assez de fades compliments.</l>
					<l n="104" num="1.104">Qu’en faveur des amis se taisent les amants,</l>
					<l n="105" num="1.105">— Je suis trop pastoral : mais il est difficile</l>
					<l n="106" num="1.106">D’aimer ma Florida sans exalter Cécile.</l>
					<l n="107" num="1.107">Ah ! si le Fortunat se présentait ici,</l>
					<l n="108" num="1.108">Je crois que de nous voir il aurait du souci.</l>
					<l n="109" num="1.109">— A propos, dit alors en riant un convive,</l>
					<l n="110" num="1.110">Ce <hi rend="ital">Môsieur</hi> eut pour vous une passion vive.</l>
					<l n="111" num="1.111">Comment se termina votre églogue d’amour ?</l>
					<l n="112" num="1.112">— Comme tout se termine : on se convient un jour ;</l>
					<l n="113" num="1.113">Et puis, le lendemain, on se trouve insipides,</l>
					<l n="114" num="1.114">Les meilleurs dénoûments doivent être rapides.</l>
					<l n="115" num="1.115">— Très-bien ! très-bien ! Buvons, dit le cercle moqueur.</l>
					<l n="116" num="1.116">— Respect à Fortunat ; c’est mon premier vainqueur,</l>
					<l n="117" num="1.117">Dit encor Florida ; mais il n’était pas sage,</l>
					<l n="118" num="1.118">Et le pauvre garçon a fait vite naufrage.</l>
					<l n="119" num="1.119">— Comment, dit Enguerrand, serait-il <hi rend="ital">raffalé</hi> ?</l>
					<l n="120" num="1.120">— Si bien, que vers Bruxelle un jour il a filé.</l>
					<l n="121" num="1.121">Adieu sa commandite ; il s’afflige et soupire…</l>
					<l n="122" num="1.122">Par compensation le hanneton respire. »</l>
					<l n="123" num="1.123"><space unit="char" quantity="2"></space>Ce fut par un fou rire, à remplir la maison,</l>
					<l n="124" num="1.124">Que la troupe accueillit la funèbre oraison.</l>
					<l n="125" num="1.125">Tels étaient les adieux de l’ingrate maîtresse</l>
					<l n="126" num="1.126">Pour l’amant qu’elle avait plongé dans la détresse.</l>
					<l n="127" num="1.127">Lorsque le plaisir seul a formé le lien,</l>
					<l n="128" num="1.128">Aux jours où vous souffrez ne comptez plus sur rien ;</l>
					<l n="129" num="1.129">Et n’attendez jamais qu’une tendresse louche</l>
					<l n="130" num="1.130">D’une femme qui vend les baisers de sa bouche.</l>
					<l n="131" num="1.131"><space unit="char" quantity="2"></space>Florida cependant paraissait réfléchir.</l>
					<l n="132" num="1.132">Elle laissait ses bras et sa tête fléchir,</l>
					<l n="133" num="1.133">Et semblait éprouver le poids de la fatigue.</l>
					<l n="134" num="1.134"><space unit="char" quantity="2"></space>On s’étonne. Enguerrand que ce nuage intrigue,</l>
					<l n="135" num="1.135">S’écrie, en se servant un morceau délicat :</l>
					<l n="136" num="1.136">« — Je serais satisfait qu’un ami m’expliquât</l>
					<l n="137" num="1.137">Le souci que j’ai lu dans les yeux de Madame,</l>
					<l n="138" num="1.138">Ces yeux qui versent moins de larmes que de flamme.</l>
					<l part="I" n="139" num="1.139">— Ah ! je pensais, dit-elle. </l>
					<l part="F" n="139" num="1.139">— Admirable ! charmant !</l>
					<l n="140" num="1.140">Vous avez pour penser bien choisi le moment !</l>
					<l n="141" num="1.141">— Non, je me reportais, par un pas en arrière,</l>
					<l n="142" num="1.142">Vers le temps où j’allais à pied, simple ouvrière,</l>
					<l n="143" num="1.143">Gagnant le pain du jour, heureuse de l’argent</l>
					<l n="144" num="1.144">Qui se trouvait au bout d’un travail diligent.</l>
					<l n="145" num="1.145">Il me fallait alors, adonnée à ma tâche,</l>
					<l n="146" num="1.146">Pour un bien faible prix m’appliquer sans relâche :</l>
					<l n="147" num="1.147">Mais aussi, mes besoins étaient si modérés !</l>
					<l n="148" num="1.148">Et quand de l’escalier je montais les degrés,</l>
					<l n="149" num="1.149">Lorsque je revoyais mon étroite chambrette,</l>
					<l n="150" num="1.150">Simple et sans ornement, mais tranquille et proprette ;</l>
					<l n="151" num="1.151">Quand j’arrangeais mes fleurs, puis me mettais au lit,</l>
					<l n="152" num="1.152">Je n’avais nul désir, nul regret dans l’esprit.</l>
					<l n="153" num="1.153">Sous mon petit chapeau, sous une robe blanche,</l>
					<l n="154" num="1.154">Vraiment je me trouvais fort belle le dimanche.</l>
					<l n="155" num="1.155">Je ne soupçonnais point qu’on pût faire un faux pas,</l>
					<l n="156" num="1.156">Ni surtout souhaiter ce que l’on n’avait pas.</l>
					<l n="157" num="1.157">D’ailleurs, dans le passé je trouvais ma défense.</l>
					<l n="158" num="1.158">J’avais eu la leçon du malheur dans l’enfance ;</l>
					<l n="159" num="1.159">J’avais appris à fuir le scandale, le bruit.</l>
					<l n="160" num="1.160">Mon père— un ouvrier— revenait chaque nuit</l>
					<l n="161" num="1.161">Sombre, les yeux éteints par son ivresse immonde ;</l>
					<l n="162" num="1.162">Il s’irritait, jurait et battait tout le monde.</l>
					<l n="163" num="1.163">Il donnait son argent aux filles du quartier.</l>
					<l n="164" num="1.164">Si ma mère pleurait, armé d’un tire-pied,</l>
					<l n="165" num="1.165">Il la frappait… Pourtant, c’était la pauvre femme</l>
					<l n="166" num="1.166">Qui de notre mansarde était l’espoir et l’âme,</l>
					<l n="167" num="1.167">Elle qui nourrissait d’un travail assidu</l>
					<l n="168" num="1.168">Les enfants, le mari dans ses vices perdu.</l>
					<l n="169" num="1.169">Et si quelque prière arrivait à sa bouche,</l>
					<l n="170" num="1.170">Il la raillait, jetant un blasphème farouche !…</l>
					<l n="171" num="1.171">J’ai vu tant de malheur, que j’en avais conçu</l>
					<l n="172" num="1.172">Le besoin d’un état modeste, inaperçu.</l>
					<l n="173" num="1.173">Ma mère succomba, victime de la vie…</l>
					<l n="174" num="1.174">De la dispersion cette mort fut suivie ;</l>
					<l n="175" num="1.175">Mon père nous quitta, pour <hi rend="ital">nocer</hi> à son gré ;</l>
					<l n="176" num="1.176">Au vice comme lui, mon frère s’est livré…</l>
					<l n="177" num="1.177">Je ne les revis plus. — Vous m’avez entendue,</l>
					<l n="178" num="1.178">Et je vous ai montré la vérité bien nue.</l>
					<l n="179" num="1.179"><space unit="char" quantity="2"></space>— A présent, palsembleu ! dit un jeune <hi rend="ital">lion</hi>,</l>
					<l n="180" num="1.180">Vous avez rejeté cette condition ;</l>
					<l n="181" num="1.181">Vous régnez et surtout vous excitez l’envie !</l>
					<l n="182" num="1.182">Le travail, c’est la peine infligée à la vie.</l>
					<l n="183" num="1.183">Dans l’ombre sied-il bien d’aller s’ensevelir</l>
					<l n="184" num="1.184">Quand on a ce qu’il faut pour briller et jouir ?</l>
					<l n="185" num="1.185">Ne vous souvenez plus qu’il fut une Cécile,</l>
					<l n="186" num="1.186">Et menez bruyamment l’existence facile.</l>
					<l n="187" num="1.187">— Oui, je veux oublier… ; oui, je suis Florida. »</l>
					<l n="188" num="1.188">Elle reprit son verre et d’un trait le vida ;</l>
					<l n="189" num="1.189">Puis fit retentir l’air de ses éclats de rire.</l>
					<l n="190" num="1.190">« — Bravo ! dit Rozemon ; j’aime mieux ce délire.</l>
					<l part="I" n="191" num="1.191">Qu’est devenu Brécourt ? </l>
					<l part="F" n="191" num="1.191">— Ma foi, je l’ai purgé</l>
					<l part="I" n="192" num="1.192">De ses grands revenus. </l>
					<l part="M" n="192" num="1.192">— Puis ?… </l>
					<l part="F" n="192" num="1.192">— Il a son congé.</l>
					<l part="I" n="193" num="1.193">— Et le comte Strogoff ? </l>
					<l part="F" n="193" num="1.193">— Il retourne en Russie</l>
					<l n="194" num="1.194">Pour réparer les trous de sa bourse amincie.</l>
					<l part="I" n="195" num="1.195">— Et le banquier Crushmann ? </l>
					<l part="F" n="195" num="1.195">— Oh ! qu’il est ennuyeux !</l>
					<l n="196" num="1.196">Comme un poisson pâmé dilatant ses gros yeux,</l>
					<l n="197" num="1.197">Couvrant de chaînes d’or sa panse rebondie,</l>
					<l n="198" num="1.198">Il s’en vient de l’amour faire la parodie.</l>
					<l n="199" num="1.199">J’ai pour adorateurs des céladons flétris,</l>
					<l n="200" num="1.200">En quête de plaisirs que le temps a taris ;</l>
					<l n="201" num="1.201">Une rose toujours brille à leur boutonnière ;</l>
					<l n="202" num="1.202">Ils ont, grâce à Guerlain, une odeur printanière ;</l>
					<l n="203" num="1.203">Leurs femmes, leurs enfants, oubliant tout pour moi ;</l>
					<l n="204" num="1.204">Ils sont à mes genoux, je leur dicte ma loi.</l>
					<l n="205" num="1.205">Je fais ce que je veux de ces vieilles poupées</l>
					<l n="206" num="1.206">Qui rampent à mes pieds, de moi seule occupées…</l>
					<l n="207" num="1.207">Ce sont les grands du jour… Mais comme ils sont petits !</l>
					<l n="208" num="1.208">Je me rince la bouche après qu’ils sont partis !</l>
					<l n="209" num="1.209">— Cette digression me paraît fort piquante,</l>
					<l n="210" num="1.210">Dit Rozemon ; chantons là-dessus, ma bacchante :</l>
				</lg>
				<lg n="2">
					<l n="211" num="2.1"><space unit="char" quantity="16"></space>Au choc du verre</l>
					<l n="212" num="2.2"><space unit="char" quantity="16"></space>On est joyeux ;</l>
					<l n="213" num="2.3"><space unit="char" quantity="16"></space>Moi je révère</l>
					<l n="214" num="2.4"><space unit="char" quantity="8"></space>Le jeune amour et le vin vieux.</l>
				</lg>
				<lg n="3">
					<l n="215" num="3.1"><space unit="char" quantity="12"></space>Triste philosophie</l>
					<l n="216" num="3.2"><space unit="char" quantity="12"></space>Qui voudrais m’ ennuyer,</l>
					<l n="217" num="3.3"><space unit="char" quantity="12"></space>Va-t’en, je te défie</l>
					<l n="218" num="3.4"><space unit="char" quantity="12"></space>De me faire bâiller.</l>
				</lg>
				<lg n="4">
					<l n="219" num="4.1"><space unit="char" quantity="12"></space>Quand la mousse pétille,</l>
					<l n="220" num="4.2"><space unit="char" quantity="12"></space>A mes regards tout brille ;</l>
					<l n="221" num="4.3"><space unit="char" quantity="12"></space>A tout le genre humain</l>
					<l n="222" num="4.4"><space unit="char" quantity="16"></space>Je tends la main.</l>
				</lg>
				<lg n="5">
					<l n="223" num="5.1"><space unit="char" quantity="16"></space>Au choc du verre</l>
					<l n="224" num="5.2"><space unit="char" quantity="16"></space>On est joyeux ;</l>
					<l n="225" num="5.3"><space unit="char" quantity="16"></space>Moi je révère</l>
					<l n="226" num="5.4"><space unit="char" quantity="8"></space>Le jeune amour et le vin vieux</l>
				</lg>
				<lg n="6">
					<l n="227" num="6.1"><space unit="char" quantity="12"></space>Grands faiseurs de morale,</l>
					<l n="228" num="6.2"><space unit="char" quantity="12"></space>Venez auprès de nous</l>
					<l n="229" num="6.3"><space unit="char" quantity="12"></space>Sans craindre le scandale ;</l>
					<l n="230" num="6.4"><space unit="char" quantity="12"></space>On rit avec les fous.</l>
					<l n="231" num="6.5"><space unit="char" quantity="12"></space>Trop tôt la gaîté passe ;</l>
					<l n="232" num="6.6"><space unit="char" quantity="12"></space>Prenez, prenez la place</l>
					<l n="233" num="6.7"><space unit="char" quantity="12"></space>Que vous garde Satan…</l>
					<l n="234" num="6.8"><space unit="char" quantity="16"></space>Il vous attend !</l>
				</lg>
				<lg n="7">
					<l n="235" num="7.1"><space unit="char" quantity="16"></space>Au choc du verre</l>
					<l n="236" num="7.2"><space unit="char" quantity="16"></space>On est joyeux ;</l>
					<l n="237" num="7.3"><space unit="char" quantity="16"></space>Moi je révère</l>
					<l n="238" num="7.4"><space unit="char" quantity="8"></space>Le jeune amour et le vin vieux.</l>
				</lg>
				<lg n="8">
					<l n="239" num="8.1"><space unit="char" quantity="12"></space>L’hiver viendra morose</l>
					<l n="240" num="8.2"><space unit="char" quantity="12"></space>Enlever, quelque jour,</l>
					<l n="241" num="8.3"><space unit="char" quantity="12"></space>Son éclat à la rose,</l>
					<l n="242" num="8.4"><space unit="char" quantity="12"></space>Son sourire à l’amour.</l>
					<l n="243" num="8.5"><space unit="char" quantity="12"></space>Mais avant sa visite</l>
					<l n="244" num="8.6"><space unit="char" quantity="12"></space>Amusons-nous bien vite,</l>
					<l n="245" num="8.7"><space unit="char" quantity="12"></space>Sans toucher au poison</l>
					<l n="246" num="8.8"><space unit="char" quantity="16"></space>De la raison.</l>
				</lg>
				<lg n="9">
					<l n="247" num="9.1"><space unit="char" quantity="16"></space>Au choc du verre</l>
					<l n="248" num="9.2"><space unit="char" quantity="16"></space>On est joyeux ;</l>
					<l n="249" num="9.3"><space unit="char" quantity="16"></space>Moi je révère</l>
					<l n="250" num="9.4"><space unit="char" quantity="8"></space>Le jeune amour et le vin vieux. »</l>
				</lg>
				<lg n="10">
					<l n="251" num="10.1">Nous ne décrirons pas le tumulte, le bruit</l>
					<l n="252" num="10.2">Que l’orgie en marchant répandait. Vers minuit</l>
					<l n="253" num="10.3">Un garçon avertit le baron de se rendre</l>
					<l n="254" num="10.4">Dans un salon voisin : tout exprès pour l’attendre</l>
					<l n="255" num="10.5">Un Monsieur, <hi rend="ital">fort bien mis du reste</hi>, était venu.</l>
					<l n="256" num="10.6">« — Eh bien ! il attendra, cet illustre inconnu,</l>
					<l n="257" num="10.7">Dit Rozemon ; il est des gens d’étrange sorte !</l>
					<l n="258" num="10.8">Quand je suis occupé demander que je sorte…</l>
					<l n="259" num="10.9">La licence est hardie ; allez au visiteur</l>
					<l n="260" num="10.10">Donner son compte ; allez, fût-il le Commandeur,</l>
					<l n="261" num="10.11">Vînt-il du plus lointain de tous les hémisphères,</l>
					<l n="262" num="10.12">Je ne le verrais pas : à demain les affaires ! »</l>
					<l n="263" num="10.13"><space unit="char" quantity="2"></space>Quelques instants après, reparut le garçon.</l>
					<l n="264" num="10.14">Il tenait une carte à la main. Le frisson</l>
					<l n="265" num="10.15">Saisit soudain Arthur, qui tordit sa moustache.</l>
					<l n="266" num="10.16">« <hi rend="ital">Monsieur, je vous attends si vous n’êtes un lâche</hi>.</l>
					<l part="I" n="267" num="10.17">« Paul FIRMIN. » </l>
					<l part="F" n="267" num="10.17">Tels étaient les mots durs et pressants</l>
					<l n="268" num="10.18">Qui du fougueux Arthur avaient glacé les sens.</l>
					<l n="269" num="10.19">L’oracle écrit aux murs du maître de Ninive,</l>
					<l n="270" num="10.20">Disant : « La mort est proche, et le vainqueur arrive, »</l>
					<l n="271" num="10.21">Émut peut-être moins l’orgueilleux Balthazar,</l>
					<l n="272" num="10.22">Que cette simple carte envoyée au hasard.</l>
					<l n="273" num="10.23">« <hi rend="ital">Monsieur, je vous attends si vous n’êtes un lâche !</hi> »</l>
					<l n="274" num="10.24">« — Il m’attend, lui, Firmin… et croit que je me cache !</l>
					<l part="I" n="275" num="10.25">Non, non ! </l>
					<l part="F" n="275" num="10.25">— Où courez-vous, lui dirent ses amis.</l>
					<l n="276" num="10.26">— Ne vous étonnez pas… Un rendez-vous remis</l>
					<l n="277" num="10.27">Qu’on vient me rappeler… Une affaire importante</l>
					<l n="278" num="10.28">Remplissez, verre en main, les moments de l’attente. »</l>
					<l n="279" num="10.29"><space unit="char" quantity="2"></space>Arthur sortit. Moitié sérieux et plaisant,</l>
					<l n="280" num="10.30">Il salua Firmin qui, d’un geste imposant,</l>
					<l n="281" num="10.31">Fit comprendre à ce fat qu’après la parodie</l>
					<l n="282" num="10.32">Il fallait aborder la sombre tragédie.</l>
					<l n="283" num="10.33"><space unit="char" quantity="2"></space>« — Je suis venu, dit-il, mais ce n’est pas pour moi,</l>
					<l part="I" n="284" num="10.34">Monsieur, veuillez le croire. </l>
					<l part="F" n="284" num="10.34">— Oui certes, je le croi.</l>
					<l n="285" num="10.35">— Si de vos procédés j’eus parfois à me plaindre,</l>
					<l n="286" num="10.36">N’ayant jamais eu l’art de plier ni de feindre,</l>
					<l n="287" num="10.37">Je vous avais assez marqué mon sentiment</l>
					<l n="288" num="10.38">Et n’eusse pas daigné me venger autrement.</l>
					<l n="289" num="10.39">— Monsieur, vous débutez par l’injure et l’outrage !</l>
					<l n="290" num="10.40">— Écoutez ; il m’en reste à dire davantage.</l>
					<l n="291" num="10.41">Mais encore une fois qu’il soit bien constaté</l>
					<l n="292" num="10.42">Que je suis l’instrument d’une autre volonté.</l>
					<l n="293" num="10.43">— Ah ! l’on vous a choisi… la chose se devine,</l>
					<l n="294" num="10.44">Comme un représentant de haine féminine !</l>
					<l part="I" n="295" num="10.45">C’est un emploi brillant. </l>
					<l part="F" n="295" num="10.45">— Monsieur, ne raillez pas.</l>
					<l n="296" num="10.46">On verrait qui de nous descendrait le plus bas</l>
					<l n="297" num="10.47">Si jamais l’on pouvait, remontant les années,</l>
					<l n="298" num="10.48">Sur un même tableau tracer nos destinées.</l>
					<l part="I" n="299" num="10.49">— Vous êtes un Caton ! </l>
					<l part="F" n="299" num="10.49">— Et vous, Monsieur, et vous.</l>
					<l n="300" num="10.50">Je me tais. Il n’est rien de commun entre nous.</l>
					<l n="301" num="10.51">— Alors pourquoi m’avoir dérangé ? Je vous quitte.</l>
					<l n="302" num="10.52">— A m’entendre, Monsieur, d’abord je vous invite,</l>
					<l n="303" num="10.53">Dit Firmin, qui debout, dans son droit affermi,</l>
					<l n="304" num="10.54">Foudroyait du regard son frivole ennemi.</l>
					<l n="305" num="10.55">De la séduction vous avez fait étude ;</l>
					<l n="306" num="10.56">Puis vous avez payé de noire ingratitude</l>
					<l n="307" num="10.57">Ce sacrifice immense et coûteux à l’honneur</l>
					<l n="308" num="10.58">D’une femme qui donne et sa vie et son cœur.</l>
					<l n="309" num="10.59">Vous avez dans le cœur de cette jeune femme,</l>
					<l n="310" num="10.60">Vous, séducteur habile, entretenu la flamme ;</l>
					<l n="311" num="10.61">Bien certain d’un triomphe à l’aise préparé,</l>
					<l n="312" num="10.62">Vous avez dit : « Je suis le maître, qu’à mon gué</l>
					<l n="313" num="10.63">« Désormais elle pense, elle agisse. :. Je règne !</l>
					<l n="314" num="10.64">« Ne m’aimât-elle plus, qu’un jour elle me craigne. »</l>
					<l n="315" num="10.65">Voilà votre calcul, j’ai su le deviner.</l>
					<l n="316" num="10.66">Vos séduisants dehors n’ont pu me fasciner ;</l>
					<l n="317" num="10.67">Car le moment arrive où, tombant de lui-même,</l>
					<l n="318" num="10.68">Le masque laisse voir la bouche qui blasphème.</l>
					<l n="319" num="10.69">Ah ! vous croyez, Monsieur, qu’on peut impunément,</l>
					<l n="320" num="10.70">Tartufe de salons, se jouer du serment,</l>
					<l n="321" num="10.71">Et par de beaux semblants cacher avec adresse</l>
					<l n="322" num="10.72">Le fond tout gangrené de la scélératesse !</l>
					<l n="323" num="10.73">Et vous avez pensé qu’il est de ces excès</l>
					<l n="324" num="10.74">Qui passent, chaque jour, à l’abri du succès !</l>
					<l n="325" num="10.75">Semer le déshonneur en héros à la mode,</l>
					<l n="326" num="10.76">Puis rire de son crime, oh ! c’était bien commode.</l>
					<l n="327" num="10.77">Mais le mal n’est jamais si prudemment caché</l>
					<l n="328" num="10.78">Qu’on ne sache la voie où vous avez marché ;</l>
					<l n="329" num="10.79">Et Dieu charge toujours quelqu’un de sa justice.</l>
					<l n="330" num="10.80">Il m’a donné ce soin ; afin qu’il s’accomplisse,</l>
					<l n="331" num="10.81">Je suis venu vous prendre au milieu du plaisir.</l>
					<l n="332" num="10.82">Comme le débauché que la mort vient saisir.</l>
					<l n="333" num="10.83">Ne vous détournez pas… Je vous regarde en face…</l>
					<l n="334" num="10.84">Voyez si sur mes traits une émotion passe…</l>
					<l n="335" num="10.85">Car j’ai ma conscience et je fais mon devoir.</l>
					<l n="336" num="10.86">Ce que de vous j’attends, vous devez le savoir.</l>
					<l n="337" num="10.87">Vainement vous avez avec un art habile</l>
					<l n="338" num="10.88">Déguisé le poison que le vice distille ;</l>
					<l n="339" num="10.89">La ruse vous servit, et le hasard vous perd.</l>
					<l n="340" num="10.90">— A juger le prochain vous êtes très-expert,</l>
					<l n="341" num="10.91">Monsieur, dit Rozemon. Vous m’arguez de ruse :</l>
					<l n="342" num="10.92">Mais ne peut-on savoir quel indice m’accuse ?</l>
					<l n="343" num="10.93">— Ce débat avec vous est au-dessous de moi.</l>
					<l n="344" num="10.94">— Cependant vous prenez un singulier emploi.</l>
					<l n="345" num="10.95">— Je défends une femme : elle est votre victime</l>
					<l n="346" num="10.96">Et je la sauverai, sans excuser son crime.</l>
					<l n="347" num="10.97">— Que voulez-vous enfin, émissaire fatal ?</l>
					<l n="348" num="10.98">— Des lettres qui seraient la mort du général.</l>
					<l part="I" n="349" num="10.99">— Vous ne les aurez pas ! </l>
					<l part="F" n="349" num="10.99">— Je les veux. C’est ma tâche,</l>
					<l n="350" num="10.100">Et vous me les rendrez si vous n’êtes un lâche.</l>
					<l part="I" n="351" num="10.101">— Encor ! </l>
					<l part="F" n="351" num="10.101">— Si vous n’avez, pour garder ce trésor,</l>
					<l n="352" num="10.102">Fait de secrets calculs qu’on paie avec de l’or.</l>
					<l part="I" n="353" num="10.103">— C’en est trop ! </l>
					<l part="F" n="353" num="10.103">— Ah ! chez vous parle quelque courage ?</l>
					<l n="354" num="10.104">— Implacable censeur, si j’écoutais ma rage,</l>
					<l part="I" n="355" num="10.105">Je… Mais nous nous verrons ailleurs. </l>
					<l part="F" n="355" num="10.105">— Quand ? — Dès demain.</l>
					<l part="I" n="356" num="10.106">A la Porte-Maillot. </l>
					<l part="F" n="356" num="10.106">— J’y serai, dit Firmin.</l>
					<l part="I" n="357" num="10.107">À six heures. </l>
					<l part="F" n="357" num="10.107">— Non, non, jamais je ne me lève</l>
					<l n="358" num="10.108">Sitôt. C’est pour midi. Jusque-là faisons trêve.</l>
					<l part="I" n="359" num="10.109">Laissez-moi retourner vers mes amis. </l>
					<l part="F" n="359" num="10.109">— Ces fous !</l>
					<l part="I" n="360" num="10.110">Si vous êtes vaincu, les lettres </l>
					<l part="F" n="360" num="10.110">— Sont à vous.</l>
					<l n="361" num="10.111">Mes témoins les auront ; mais il faut vous attendre,</l>
					<l n="362" num="10.112">Vous qui les réclamez, à me voir les défendre</l>
					<l n="363" num="10.113">Avec acharnement. Puisque le sort le veut,</l>
					<l n="364" num="10.114">Puisqu’il faut en finir, oubliant qu’il se peut</l>
					<l n="365" num="10.115">Que demain je succombe, oh ! j’accueille avec joie</l>
					<l n="366" num="10.116">L’espoir de me venger que le hasard m’envoie.</l>
					<l n="367" num="10.117">Je vous tiendrai, mon fer tout près de votre cœur,</l>
					<l n="368" num="10.118">Vous qui m’avez tant nui, soit grave, soit moqueur ;</l>
					<l n="369" num="10.119">Vous qui n’avez cessé de fronder ma conduite ;</l>
					<l n="370" num="10.120">Vous dont je dois subir l’éternelle poursuite !</l>
					<l n="371" num="10.121">Comme une ombre importune attachée à mes pas,</l>
					<l n="372" num="10.122">Vous étiez un ennui que je n’avouais pas,</l>
					<l n="373" num="10.123">Mais qui rongeait mon cœur et dont je me délivre</l>
					<l n="374" num="10.124">Au jour où l’un de nous aura cessé de vivre,</l>
					<l n="375" num="10.125">Jusqu’à votre vertu, tout en vous me blessait.</l>
					<l n="376" num="10.126">Ah ! vous venez enfin !… La haine m’oppressait ;</l>
					<l n="377" num="10.127">Vous venez vous offrir vous-même à ma vengeance !</l>
					<l n="378" num="10.128">Mes désirs avec vous étaient d’intelligence.</l>
					<l n="379" num="10.129">A demain donc, Monsieur ; j’accepte le cartel.</l>
					<l n="380" num="10.130">Que ce dernier combat soit un combat mortel ! »</l>
					<l n="381" num="10.131"><space unit="char" quantity="2"></space>Paul, s’inclinant, sortit calme. Son adversaire,</l>
					<l n="382" num="10.132">Tout troublé qu’il était, prit — chose nécessaire</l>
					<l n="383" num="10.133">— Pour rentrer au souper, un sourire, un maintien.</l>
					<l n="384" num="10.134">« — Ah ! que vous êtes long ! vraiment ce n’est pas bien,</l>
					<l part="I" n="385" num="10.135">Dit Florida. </l>
					<l part="M" n="385" num="10.135">— Pardon. </l>
					<l part="F" n="385" num="10.135">— Un créancier peut-être</l>
					<l part="I" n="386" num="10.136">Vous ennuyait, mon cher ? </l>
					<l part="F" n="386" num="10.136">— Oh ! non, par la fenêtre</l>
					<l part="I" n="387" num="10.137">Je l’eusse fait partir. C’était </l>
					<l part="M" n="387" num="10.137">— Qui ? </l>
					<l part="F" n="387" num="10.137">— Paul Firmin.</l>
					<l part="I" n="388" num="10.138">— Ce pédant ! dit chacun. </l>
					<l part="F" n="388" num="10.138">— Nous nous battons demain. »</l>
				</lg>
			</div></body></text></TEI>