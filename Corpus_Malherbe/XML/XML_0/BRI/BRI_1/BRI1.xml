<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Marie</title>
				<title type="medium">Une édition électronique</title>
				<author key="BRI">
					<name>
						<forename>Auguste</forename>
						<surname>BRIZEUX</surname>
					</name>
					<date from="1803" to="1858">1803-1858</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2412 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BRI_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Marie</title>
						<author>Auguste Brizeux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>FRANTEXT</publisher>
						<idno type="FRANTEXT">M547</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Marie</title>
								<author>Auguste Brizeux</author>
								<imprint>
									<publisher>Garnier,Paris</publisher>
									<date when="1910">1910</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Auguste Brizeux</author>
						<imprint>
							<publisher>Alphonse Lemerre,Éditeur</publisher>
							<date when="1884">1879-1884</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1831">1831</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les balises de pagination ont été supprimées.</p>
				<p>Les astérisques placées devant les noms propres ont été supprimés</p>
				<p>Les vers imcomplets (avec renvoi de la fin de vers) ont été restitués.</p>
				<p>Les citations et dédicaces ont été restituées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BRI1">
				<head type="main">MARIE (1)</head>
				<lg n="1">
					<l n="1" num="1.1">Rien ne trouble ta paix, ô doux Léta ! Le monde</l>
					<l n="2" num="1.2">En vain s’agite et pousse une plainte profonde,</l>
					<l n="3" num="1.3">Tu n’as pas entendu ce long gémissement,</l>
					<l n="4" num="1.4">Et ton eau vers la mer coule aussi mollement ;</l>
					<l n="5" num="1.5">Sur l’herbe de tes prés les joyeuses cavales</l>
					<l n="6" num="1.6">Luttent chaque matin, et ces belles rivales</l>
					<l n="7" num="1.7">Toujours d’un bord à l’autre appellent leurs époux,</l>
					<l n="8" num="1.8">Qui plongent dans tes flots, hennissants et jaloux :</l>
					<l n="9" num="1.9">Il m’en souvient ici, comme en cette soirée</l>
					<l n="10" num="1.10">Où de bœufs, de chevaux notre barque entourée</l>
					<l n="11" num="1.11">Sous leurs pieds s’abîmait, quand nous, hardis marins,</l>
					<l n="12" num="1.12">Nous gagnâmes le bord, suspendus à leurs crins,</l>
					<l n="13" num="1.13">Excitant par nos voix et suivant à la nage</l>
					<l n="14" num="1.14">Ce troupeau qui montait pêle-mêle au rivage.</l>
					<l n="15" num="1.15">J’irai, j’irai revoir les saules du Létâ,</l>
					<l n="16" num="1.16">Et toi qu’en ses beaux jours mon enfance habita,</l>
					<l n="17" num="1.17">Paroisse bien-aimée, humble coin de la terre</l>
					<l n="18" num="1.18">Où l’on peut vivre encore et mourir solitaire !</l>
					<l n="19" num="1.19">Aujourd’hui que tout cœur est triste et que chacun</l>
					<l n="20" num="1.20">Doit gémir sur lui-même et sur le mal commun ;</l>
					<l n="21" num="1.21">Que le monde, épuisé par une ardente fièvre,</l>
					<l n="22" num="1.22">N’a plus un souffle pur pour rafraîchir sa lèvre ;</l>
					<l n="23" num="1.23">Qu’après un si long temps de périls et d’efforts,</l>
					<l n="24" num="1.24">Dans l’ardeur du combat succombent les plus forts ;</l>
					<l n="25" num="1.25">Que d’autres, haletants, rendus de lassitude,</l>
					<l n="26" num="1.26">Sont près de défaillir, alors la solitude</l>
					<l n="27" num="1.27">Vers son riant lointain nous attire, et nos voix</l>
					<l n="28" num="1.28">Se prennent à chanter l’eau, les fleurs et les bois ;</l>
					<l n="29" num="1.29">Alors c’est un bonheur, quand tout meurt ou chancelle,</l>
					<l n="30" num="1.30">De se mêler à l’âme immense, universelle,</l>
					<l n="31" num="1.31">D’oublier ce qui fuit, les peuples et les jours,</l>
					<l n="32" num="1.32">Pour vivre avec Dieu seul, et partout et toujours.</l>
					<l n="33" num="1.33">Ainsi, lorsque la flamme au milieu d’une ville</l>
					<l n="34" num="1.34">Éclate, et qu’il n’est plus contre elle un sûr asile,</l>
					<l n="35" num="1.35">Hommes, femmes, chargés de leurs petits enfants,</l>
					<l n="36" num="1.36">Se sauvent demi-nus, et, couchés dans les champs,</l>
					<l n="37" num="1.37">Ils regardent de loin, dans un morne silence,</l>
					<l n="38" num="1.38">L’incendie en fureur qui mugit et s’élance ;</l>
					<l n="39" num="1.39">Cependant la nature est calme, dans les cieux</l>
					<l n="40" num="1.40">Chaque étoile poursuit son cours mystérieux,</l>
					<l n="41" num="1.41">Nul anneau n’est brisé dans la chaîne infinie,</l>
					<l n="42" num="1.42">Et l’univers entier roule avec harmonie.</l>
					<l n="43" num="1.43">Immuable nature, apparais aujourd’hui !</l>
					<l n="44" num="1.44">Que chacun dans ton sein dépose son ennui !</l>
					<l n="45" num="1.45">Tâche de nous séduire à tes beautés suprêmes,</l>
					<l n="46" num="1.46">Car nous sommes bien las du monde et de nous-mêmes :</l>
					<l n="47" num="1.47">Si tu veux dévoiler ton front jeune et divin,</l>
					<l n="48" num="1.48">Peut-être, heureux vieillards, nous sourirons enfin !</l>
					<l n="49" num="1.49">Celle pour qui j’écris avec amour ce livre</l>
					<l n="50" num="1.50">Ne le lira jamais ; quand le soir la délivre</l>
					<l n="51" num="1.51">Des longs travaux du jour, des soins de la maison,</l>
					<l n="52" num="1.52">C’est assez à son fils de dire une chanson ;</l>
					<l n="53" num="1.53">D’ailleurs, en parcourant chaque feuille légère,</l>
					<l n="54" num="1.54">Ses yeux n’y trouveraient qu’une langue étrangère,</l>
					<l n="55" num="1.55">Elle qui n’a rien vu que ses champs, ses taillis,</l>
					<l n="56" num="1.56">Et parle seulement la langue du pays.</l>
					<l n="57" num="1.57">Pourtant je veux poursuivre ; et quelque ami peut-être,</l>
					<l n="58" num="1.58">Resté dans nos forêts et venant à connaître</l>
					<l n="59" num="1.59">Ce livre où son beau temps tout joyeux renaîtra,</l>
					<l n="60" num="1.60">Dans une fête, un jour, en dansant lui dira</l>
					<l n="61" num="1.61">Cette histoire qu’ici j’ai commencé d’écrire,</l>
					<l n="62" num="1.62">Et qu’en son ignorance elle ne doit pas lire ;</l>
					<l n="63" num="1.63">Un sourire incrédule, un regard curieux,</l>
					<l n="64" num="1.64">À ce récit naïf, passeront dans ses yeux ;</l>
					<l n="65" num="1.65">Puis, de nouveau mêlée à la foule qui gronde,</l>
					<l n="66" num="1.66">Tout entière au plaisir elle suivra la ronde.</l>
				</lg>
			</div></body></text></TEI>