<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Marie</title>
				<title type="medium">Une édition électronique</title>
				<author key="BRI">
					<name>
						<forename>Auguste</forename>
						<surname>BRIZEUX</surname>
					</name>
					<date from="1803" to="1858">1803-1858</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2412 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BRI_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Marie</title>
						<author>Auguste Brizeux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>FRANTEXT</publisher>
						<idno type="FRANTEXT">M547</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Marie</title>
								<author>Auguste Brizeux</author>
								<imprint>
									<publisher>Garnier,Paris</publisher>
									<date when="1910">1910</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Auguste Brizeux</author>
						<imprint>
							<publisher>Alphonse Lemerre,Éditeur</publisher>
							<date when="1884">1879-1884</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1831">1831</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les balises de pagination ont été supprimées.</p>
				<p>Les astérisques placées devant les noms propres ont été supprimés</p>
				<p>Les vers imcomplets (avec renvoi de la fin de vers) ont été restitués.</p>
				<p>Les citations et dédicaces ont été restituées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BRI43">
				<head type="main">À MA MÈRE</head>
				<lg n="1">
					<l n="1" num="1.1">Si je ne t’aimais pas, qui donc pourrais-je aimer ?</l>
					<l n="2" num="1.2">Quand ton cœur au mien seul semble se ranimer,</l>
					<l n="3" num="1.3">Lorsque dans tout le jour peut-être il n’est point d’heure</l>
					<l n="4" num="1.4">Que ta pensée aimante autour de ma demeure</l>
					<l n="5" num="1.5">Ne vienne, redoutant mille lointains périls</l>
					<l n="6" num="1.6">Et des chagrins sans nombre et dont souffre ton fils !</l>
					<l n="7" num="1.7">Et quel est ton bonheur, sinon avec ta mère,</l>
					<l n="8" num="1.8">Mon autre mère aussi (car le destin sévère,</l>
					<l n="9" num="1.9">Sous lequel je me traîne et m’agite aujourd’hui,</l>
					<l n="10" num="1.10">Du moins me réservait en vous un double appui),</l>
					<l n="11" num="1.11">Toutes deux en secret quel bonheur est le vôtre,</l>
					<l n="12" num="1.12">Sinon de me pleurer, et toujours l’une à l’autre</l>
					<l n="13" num="1.13">De parler de celui que vous ne pouvez voir,</l>
					<l n="14" num="1.14">D’une lettre en retard qu’on eût dû recevoir,</l>
					<l n="15" num="1.15">Qui vous arrive enfin, mais rouvre vos alarmes,</l>
					<l n="16" num="1.16">Et que vous arrosez, comme moi, de vos larmes ?</l>
					<l n="17" num="1.17">Et vous vous consultez ; et tu m’écris alors</l>
					<l n="18" num="1.18">Pour forcer ma paresse à de nouveaux efforts :</l>
					<l n="19" num="1.19">C’est mon sort, c’est le tien ; au besoin tu m’en pries ;</l>
					<l n="20" num="1.20">Et qu’il faut triompher de ces sauvageries,</l>
					<l n="21" num="1.21">De ces fières humeurs, de ces hauteurs de ton</l>
					<l n="22" num="1.22">Que me transmit mon père avec le sang breton ;</l>
					<l n="23" num="1.23">Puis viennent de ces riens, de ces mots, de ces choses,</l>
					<l n="24" num="1.24">Que toute femme trouve, en écrivant, écloses,</l>
					<l n="25" num="1.25">Qu’on baise avec transport, et qu’on relit tout bas !</l>
					<l n="26" num="1.26">Oh ! Qui pourrais-je aimer, si je ne t’aimais pas ?</l>
					<l n="27" num="1.27">Et malgré tes avis, mes soins de toute sorte,</l>
					<l n="28" num="1.28">Si ma mauvaise étoile, enfin, est la plus forte,</l>
					<l n="29" num="1.29">Si je sens par degrés mon âme se flétrir</l>
					<l n="30" num="1.30">Et se miner mon corps, vers qui donc recourir ?</l>
					<l n="31" num="1.31">Vers toi, qui toujours douce, et bienveillante et bonne,</l>
					<l n="32" num="1.32">D’un reproche tardif n’affligerais personne,</l>
					<l n="33" num="1.33">Dont l’esprit indulgent n’a pas encor vieilli,</l>
					<l n="34" num="1.34">Dont le front, jeune encore, est demeuré sans pli !</l>
					<l n="35" num="1.35">Lorsque seule, en hiver, assidue à l’ouvrage,</l>
					<l n="36" num="1.36">Le soir, tu sentiras défaillir ton courage,</l>
					<l n="37" num="1.37">Songeant que, sans profit pour mon bien à venir,</l>
					<l n="38" num="1.38">J’ai quitté la maison pour n’y plus revenir ;</l>
					<l n="39" num="1.39">Quand ton cœur abîmé dans cette idée amère</l>
					<l n="40" num="1.40">Sera près de se rompre, alors prends, ô ma mère !</l>
					<l n="41" num="1.41">Prends ce livre qu’ici j’écrivis plein de toi,</l>
					<l n="42" num="1.42">Et tu croiras me voir et causer avec moi !</l>
					<l n="43" num="1.43">Tes conseils, mes regrets, nos communes pensées</l>
					<l n="44" num="1.44">Y sont avec amour et jour par jour tracées.</l>
					<l n="45" num="1.45">Ce livre est plein de toi ; dans la longueur des nuits,</l>
					<l n="46" num="1.46">Qu’il vienne, comme un baume, assoupir tes ennuis !</l>
					<l n="47" num="1.47">Si ton doigt y souligne un mot frais, un mot tendre,</l>
					<l n="48" num="1.48">De ta bouche riante, enfant j’ai dû l’entendre ;</l>
					<l n="49" num="1.49">Son miel avec ton lait dans mon âme a coulé ;</l>
					<l n="50" num="1.50">Ta bouche, à mon berceau, me l’avait révélé.</l>
				</lg>
			</div></body></text></TEI>