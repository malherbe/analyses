<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Marie</title>
				<title type="medium">Une édition électronique</title>
				<author key="BRI">
					<name>
						<forename>Auguste</forename>
						<surname>BRIZEUX</surname>
					</name>
					<date from="1803" to="1858">1803-1858</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2412 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BRI_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Marie</title>
						<author>Auguste Brizeux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>FRANTEXT</publisher>
						<idno type="FRANTEXT">M547</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Marie</title>
								<author>Auguste Brizeux</author>
								<imprint>
									<publisher>Garnier,Paris</publisher>
									<date when="1910">1910</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Auguste Brizeux</author>
						<imprint>
							<publisher>Alphonse Lemerre,Éditeur</publisher>
							<date when="1884">1879-1884</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1831">1831</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les balises de pagination ont été supprimées.</p>
				<p>Les astérisques placées devant les noms propres ont été supprimés</p>
				<p>Les vers imcomplets (avec renvoi de la fin de vers) ont été restitués.</p>
				<p>Les citations et dédicaces ont été restituées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BRI25">
				<head type="main">MARIE (6)</head>
				<lg n="1">
					<l n="1" num="1.1">Du bois de Ker-Mélô jusqu’au moulin de Teir,</l>
					<l n="2" num="1.2">J’ai passé tout le jour sur le bord de la mer,</l>
					<l n="3" num="1.3">Respirant sous les pins leur odeur de résine,</l>
					<l n="4" num="1.4">Poussant devant mes pieds leur feuille lisse et fine,</l>
					<l n="5" num="1.5">Et d’instants en instants, par-dessus Saint-Michel,</l>
					<l n="6" num="1.6">Lorsque éclatait le bruit de la barre d’Enn-Tell,</l>
					<l n="7" num="1.7">M’arrêtant pour entendre ; au milieu des bruyères,</l>
					<l n="8" num="1.8">Carnac m’apparaissait avec toutes ses pierres,</l>
					<l n="9" num="1.9">Et parmi les men-hîr erraient comme autrefois</l>
					<l n="10" num="1.10">Les vieux guerriers des clans, leurs prêtres et leurs rois.</l>
					<l n="11" num="1.11">Puis, je marchais encore au hasard et sans règle.</l>
					<l n="12" num="1.12">C’est ainsi que, faisant le tour d’un champ de seigle,</l>
					<l n="13" num="1.13">Je trouvai deux enfants couchés au pied d’un houx,</l>
					<l n="14" num="1.14">Deux enfants qui jouaient, sur le sable, aux cailloux ;</l>
					<l n="15" num="1.15">Et soudain, dans mon cœur cette vie innocente,</l>
					<l n="16" num="1.16">Qu’une image bien chère à mes yeux représente,</l>
					<l n="17" num="1.17">Ô Maï ! Si fortement s’est mise à revenir,</l>
					<l n="18" num="1.18">Qu’il ma fallu chanter encor ce souvenir.</l>
					<l n="19" num="1.19">Dans ce sombre Paris, toi que j’ai tant rêvée,</l>
					<l n="20" num="1.20">Vois ! Comme en nos vallons mon cœur t’a retrouvée.</l>
					<l n="21" num="1.21">À l’âge qui pour moi fut si plein de douceurs,</l>
					<l n="22" num="1.22">J’avais pour être aimé trois cousines (trois sœurs) :</l>
					<l n="23" num="1.23">Elles venaient souvent me voir au presbytère :</l>
					<l n="24" num="1.24">Le nom qu’elles portaient alors, je dois le taire,</l>
					<l n="25" num="1.25">Toutes trois aujourd’hui marchent le front voilé,</l>
					<l n="26" num="1.26">Une près de Morlaix et deux à Kemperlé ;</l>
					<l n="27" num="1.27">Mais je sais qu’en leur cloître elles me sont fidèles,</l>
					<l n="28" num="1.28">Elles ont prié Dieu pour moi qui parle d’elles.</l>
				</lg>
				<lg n="2">
					<l n="29" num="2.1">Chez mon ancien curé, l’été, d’un lieu voisin,</l>
					<l n="30" num="2.2">Elles venaient donc voir l’écolier leur cousin,</l>
					<l n="31" num="2.3">Prenaient, en me parlant, un langage de mères ;</l>
					<l n="32" num="2.4">Ou bien, selon leur âge et le mien, moins sévères,</l>
					<l n="33" num="2.5">S’informaient de Marie, objet de mes amours,</l>
					<l n="34" num="2.6">Et si, pour l’embrasser, je la suivais toujours ;</l>
					<l n="35" num="2.7">Et comme ma rougeur montrait assez ma flamme,</l>
					<l n="36" num="2.8">Ces sœurs, qui sans pitié jouaient avec mon âme,</l>
					<l n="37" num="2.9">Curieuses aussi, résolurent de voir</l>
					<l n="38" num="2.10">Celle qui me tenait si jeune en son pouvoir.</l>
				</lg>
				<lg n="3">
					<l n="39" num="3.1">À l’heure de midi, lorsque de leur village</l>
					<l n="40" num="3.2">Les enfants accouraient au bourg, selon l’usage,</l>
					<l n="41" num="3.3">Les voilà de s’asseoir, en riant, toutes trois,</l>
					<l n="42" num="3.4">Devant le cimetière, au-dessous de la croix ;</l>
					<l n="43" num="3.5">Et quand au catéchisme arrivait une fille,</l>
					<l n="44" num="3.6">Rouge sous la chaleur et qui semblait gentille,</l>
					<l n="45" num="3.7">Comme il en venait tant de Ker-Barz, Ker-Halvé,</l>
					<l n="46" num="3.8">Et par tous les sentiers qui vont à Ti-Névé,</l>
					<l n="47" num="3.9">Elles barraient sa route, et par plaisanterie</l>
					<l n="48" num="3.10">Disaient en soulevant sa coiffe : « es-tu Marie ? »</l>
					<l n="49" num="3.11">Or celle-ci passait avec Joseph Daniel ;</l>
					<l n="50" num="3.12">Elle entendit son nom, et vite, grâce au ciel !</l>
					<l n="51" num="3.13">Se sauvait, quand Daniel, comme une biche fauve,</l>
					<l n="52" num="3.14">La poursuivit, criant : « voici Maï qui se sauve ! »</l>
					<l n="53" num="3.15">Et, sautant par-dessus les tombes et leurs morts,</l>
					<l n="54" num="3.16">Au détour du clocher la prit à bras le-corps :</l>
					<l n="55" num="3.17">Elle se débattait, se cachait la figure ;</l>
					<l n="56" num="3.18">Mais chacun écarta ses mains et sa coiffure ;</l>
					<l n="57" num="3.19">Et les yeux des trois sœurs s’ouvrirent pour bien voir</l>
					<l n="58" num="3.20">Cette grappe du Scorf, cette fleur de blé noir.</l>
				</lg>
			</div></body></text></TEI>