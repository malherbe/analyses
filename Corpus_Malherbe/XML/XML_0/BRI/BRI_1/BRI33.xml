<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Marie</title>
				<title type="medium">Une édition électronique</title>
				<author key="BRI">
					<name>
						<forename>Auguste</forename>
						<surname>BRIZEUX</surname>
					</name>
					<date from="1803" to="1858">1803-1858</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2412 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BRI_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Marie</title>
						<author>Auguste Brizeux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>FRANTEXT</publisher>
						<idno type="FRANTEXT">M547</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Marie</title>
								<author>Auguste Brizeux</author>
								<imprint>
									<publisher>Garnier,Paris</publisher>
									<date when="1910">1910</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Auguste Brizeux</author>
						<imprint>
							<publisher>Alphonse Lemerre,Éditeur</publisher>
							<date when="1884">1879-1884</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1831">1831</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les balises de pagination ont été supprimées.</p>
				<p>Les astérisques placées devant les noms propres ont été supprimés</p>
				<p>Les vers imcomplets (avec renvoi de la fin de vers) ont été restitués.</p>
				<p>Les citations et dédicaces ont été restituées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BRI33">
				<head type="main">L’ÉLÉGIE DE LE BRAZ</head>
				<lg n="1">
					<l n="1" num="1.1">Si vous laissez encor les beaux genêts fleuris</l>
					<l n="2" num="1.2">Et les champs de blé noir pour aller à Paris,</l>
					<l n="3" num="1.3">Quand vous aurez tout vu dans cette grande ville,</l>
					<l n="4" num="1.4">Combien elle est superbe et combien elle est vile,</l>
					<l n="5" num="1.5">Regrettant le pays, informez-vous alors</l>
					<l n="6" num="1.6">Où du pauvre Le Brâz on a jeté le corps.</l>
					<l n="7" num="1.7">(son nom serait Ar-Brâz, mais nous, lâches et traîtres,</l>
					<l n="8" num="1.8">Nous avons oublié les noms de nos ancêtres.)</l>
					<l n="9" num="1.9">Et puis devant ce corps brûlé par le charbon</l>
					<l n="10" num="1.10">Songez comme il mourut, lui simple, honnête et bon.</l>
					<l n="11" num="1.11">C’est qu’il avait aussi quitté son coin de terre,</l>
					<l n="12" num="1.12">Sur le bord du chemin sa maison solitaire,</l>
					<l n="13" num="1.13">Le pré de Ker-Végan, Ar-Ros, sombres coteaux :</l>
					<l n="14" num="1.14">Là, rencontrant la mer, le Scorf brise ses flots ;</l>
					<l n="15" num="1.15">Dans le fond, le moulin fait mugir son écluse,</l>
					<l n="16" num="1.16">Et dès que le meunier enfle sa cornemuse,</l>
					<l n="17" num="1.17">Au tomber de la nuit, les esprits des talus,</l>
					<l n="18" num="1.18">Les noirs corriganed dansent sur le palus.</l>
				</lg>
				<lg n="2">
					<l n="19" num="2.1">— Je dirai : si la mort, dans la ville muette</l>
					<l n="20" num="2.2">Et les tristes faubourgs, passe sur sa charrette,</l>
					<l n="21" num="2.3">Prenez entre vos mains un des pans du linceul,</l>
					<l n="22" num="2.4">Car le malheur de tous est le malheur d’un seul ;</l>
					<l n="23" num="2.5">Mais, ô bardes pieux ! Vous qui parmi la mousse</l>
					<l n="24" num="2.6">Retrouverez un jour la harpe antique et douce,</l>
					<l n="25" num="2.7">Et dont le lai savant répétera dans l’air</l>
					<l n="26" num="2.8">Les soupirs de la lande et les cris de la mer,</l>
					<l n="27" num="2.9">Quand avec ses faubourgs la ville est ivre et folle,</l>
					<l n="28" num="2.10">Criez qu’un malheureux en secret se désole ;</l>
					<l n="29" num="2.11">Si vos cœurs sont souffrants, vous-mêmes plaignez-vous,</l>
					<l n="30" num="2.12">Car le malheur d’un seul est le malheur de tous.</l>
					<l n="31" num="2.13">Chantres de mon pays, plaignez celui qui souffre !</l>
					<l n="32" num="2.14">Paris roula Le Brâz bien longtemps dans son gouffre ;</l>
					<l n="33" num="2.15">Un ami le suivait durant ces jours hideux :</l>
					<l n="34" num="2.16">Tous deux, pour en finir, s’étouffèrent tous deux. —</l>
				</lg>
				<lg n="3">
					<l n="35" num="3.1">Non, ce n’est pas ainsi que l’on meurt en Bretagne !</l>
					<l n="36" num="3.2">La vie a tout son cours ; ou, si le froid vous gagne,</l>
					<l n="37" num="3.3">Comme une jeune plante encor loin de juillet,</l>
					<l n="38" num="3.4">Celle qui vous nourrit autrefois de son lait</l>
					<l n="39" num="3.5">S’assied à votre lit ; pleurant sur son ouvrage,</l>
					<l n="40" num="3.6">De la voix cependant elle vous encourage ;</l>
					<l n="41" num="3.7">Et lorsqu’enfin le corps reste seul sur le lit,</l>
					<l n="42" num="3.8">De ses tremblantes mains elle l’ensevelit ;</l>
					<l n="43" num="3.9">La foule, vers le soir, l’emporte et l’accompagne</l>
					<l n="44" num="3.10">Jusques au cimetière ouvert dans la campagne. —</l>
					<l n="45" num="3.11">Si Le Brâz eût aimé le pré de Ker-Végan,</l>
					<l n="46" num="3.12">Les taillis d’alentour, le Scorf et son étang,</l>
					<l n="47" num="3.13">Il chanterait encor sur le Ros ; ou sa mère,</l>
					<l n="48" num="3.14">Mourant, l’aurait soigné comme, depuis, son frère.</l>
					<l n="49" num="3.15">Son corps reposerait dans le bourg de Kéven,</l>
					<l n="50" num="3.16">Près du mur de l’église et sous un tertre fin ;</l>
					<l n="51" num="3.17">Ses parents y viendraient prier avant la messe,</l>
					<l n="52" num="3.18">Tous les petits enfants y lutteraient sans cesse.</l>
					<l n="53" num="3.19">Étendu dans sa fosse, il entendrait leur bruit,</l>
					<l n="54" num="3.20">Et les corriganed y danseraient la nuit.</l>
				</lg>
				<lg n="4">
					<l n="55" num="4.1">Oh ! Ne quittez jamais le seuil de votre porte !</l>
					<l n="56" num="4.2">Mourez dans la maison où votre mère est morte !</l>
					<l n="57" num="4.3">Voilà ce qu’à Paris avait déjà chanté</l>
					<l n="58" num="4.4">Un poète inconnu qu’on n’a pas écouté.</l>
				</lg>
			</div></body></text></TEI>