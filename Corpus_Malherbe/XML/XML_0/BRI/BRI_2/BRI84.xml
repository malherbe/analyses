<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FLEUR D’OR</title>
				<title type="medium">Une édition électronique</title>
				<author key="BRI">
					<name>
						<forename>Auguste</forename>
						<surname>BRIZEUX</surname>
					</name>
					<date from="1803" to="1858">1803-1858</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2992 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BRI_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA FLEUR D’OR</title>
						<author>Auguste Brizeux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">https://fr.wikisource.org/wiki/La_Fleur_d%E2%80%99Or</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Œuvres de Auguste Brizeux — LA FLEUR D’OR</title>
								<author>Auguste Brizeux</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Alphonse Lemerre, éditeur</publisher>
									<date when="1981">1981</date>
								</imprint>
								<biblScope unit="tome">3</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1874">1874</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les deux préfaces ne sont pas reprises.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-02-05" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-02-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LIVRE QUATRIÈME</head><head type="sub_part">A FLORENCE</head><div type="poem" key="BRI84">
					<head type="main">La Fleur qui m’est douce</head>
					<lg n="1">
						<l n="1" num="1.1">L’ACCORD des vers et des lyres</l>
						<l n="2" num="1.2">Murmure dans son sommeil :</l>
						<l n="3" num="1.3">Il a de nobles délires,</l>
						<l n="4" num="1.4">Il rêve marbres, porphyres,</l>
						<l n="5" num="1.5">Temples au fronton vermeil.</l>
					</lg>
					<lg n="2">
						<l n="6" num="2.1">S’il s’éveille, tout enchante</l>
						<l n="7" num="2.2">Sa pensée et son regard ;</l>
						<l n="8" num="2.3">Et, lyre lui-même, il chante</l>
						<l n="9" num="2.4">Et la nature vivante</l>
						<l n="10" num="2.5">Et les symboles de l’art.</l>
					</lg>
					<lg n="3">
						<l n="11" num="3.1">Il dit le jeune Persée</l>
						<l n="12" num="3.2">Debout, le glaive à la main,</l>
						<l n="13" num="3.3">Et, prompt comme la pensée,</l>
						<l n="14" num="3.4">Hermès, dieu du caducée.</l>
						<l n="15" num="3.5">Au ciel prenant son chemin.</l>
					</lg>
					<lg n="4">
						<l n="16" num="4.1">Tous les dieux de l’Étrurie</l>
						<l n="17" num="4.2">Dans leurs vêtements soyeux</l>
						<l n="18" num="4.3">Passent ; et la théorie</l>
						<l n="19" num="4.4">Déroule avec symétrie</l>
						<l n="20" num="4.5">Ses anneaux mystérieux.</l>
					</lg>
					<lg n="5">
						<l n="21" num="5.1">Puis Cimabué, grave et calme,</l>
						<l n="22" num="5.2">Erre autour de la cité :</l>
						<l n="23" num="5.3">Armé de sa docte palme,</l>
						<l n="24" num="5.4">Il reflète d’un front calme</l>
						<l n="25" num="5.5">La primitive beauté.</l>
					</lg>
					<lg n="6">
						<l n="26" num="6.1">Fleur, d’où le savoir émane</l>
						<l n="27" num="6.2">Comme un parfum épuré,</l>
						<l n="28" num="6.3">Par un invisible arcane,</l>
						<l n="29" num="6.4">De toi, beau lis de Toscane,</l>
						<l n="30" num="6.5">Tout esprit s’est enivré :</l>
					</lg>
					<lg n="7">
						<l n="31" num="7.1">Pourtant la fleur qui m’est douce</l>
						<l n="32" num="7.2">Croît sur les caps de la mer ;</l>
						<l n="33" num="7.3">Sauvage comme la mousse,</l>
						<l n="34" num="7.4">Sans l’art de l’homme elle pousse,</l>
						<l n="35" num="7.5">Libre au bord du gouffre amer.</l>
					</lg>
				</div></body></text></TEI>