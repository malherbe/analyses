<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FLEUR D’OR</title>
				<title type="medium">Une édition électronique</title>
				<author key="BRI">
					<name>
						<forename>Auguste</forename>
						<surname>BRIZEUX</surname>
					</name>
					<date from="1803" to="1858">1803-1858</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2992 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BRI_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA FLEUR D’OR</title>
						<author>Auguste Brizeux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">https://fr.wikisource.org/wiki/La_Fleur_d%E2%80%99Or</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Œuvres de Auguste Brizeux — LA FLEUR D’OR</title>
								<author>Auguste Brizeux</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Alphonse Lemerre, éditeur</publisher>
									<date when="1981">1981</date>
								</imprint>
								<biblScope unit="tome">3</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1874">1874</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les deux préfaces ne sont pas reprises.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-02-05" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-02-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LIVRE SEPTIÈME</head><head type="sub_part">A VENISE</head><div type="poem" key="BRI124">
					<head type="main">Væ Victis</head>
					<opener>
						<epigraph>
							<cit>
								<quote>
									Pour moi, je regarde ces Vénètes <lb></lb>
									(de la Gaule) comme les fondateurs <lb></lb>
									de Venise dans le golfe Adriatique.
								</quote>
								<bibl>
									<name>ST RAB., LIV. IV.</name>
								</bibl>
							</cit>
						</epigraph>
					</opener>
					<lg n="1">
						<l n="1" num="1.1">L’ÉCHO des temps passés n’est-il pas mort en vous,</l>
						<l n="2" num="1.2">Gaulois-Italiens ? Savez-vous qui vous êtes ?</l>
						<l n="3" num="1.3">De graves érudits vont répétant chez nous :</l>
						<l n="4" num="1.4">« Oui, les Vénitiens sont enfants des Vénètes. »</l>
						<l n="5" num="1.5">Et moi, de votre gloire amoureux et jaloux,</l>
						<l n="6" num="1.6">Comme un frère je pleure ici sur vos défaites.</l>
					</lg>
					<lg n="2">
						<l n="7" num="2.1">Tous ces hommes du Nord au visage épaté</l>
						<l n="8" num="2.2">Ce soir nous observaient, et lui, brave jeune homme,</l>
						<l n="9" num="2.3">Élevé dans l’orgueil de sa belle cité :</l>
						<l n="10" num="2.4">« Oh ! Venise avilie, et vous, Florence et Rome ! »</l>
						<l n="11" num="2.5">Vinrent d’autres soldats leur baguette à la main,</l>
						<l n="12" num="2.6">Lui, pâle, m’entraîna par un autre chemin :</l>
					</lg>
					<lg n="3">
						<l n="13" num="3.1">« Oui, fuyons, taisons-nous, car nous n’avons plus d’armes.</l>
						<l n="14" num="3.2">Ils ont pris nos couteaux, car nos couteaux tuaient.</l>
						<l n="15" num="3.3">Le dirai-je (et ses yeux se gonflèrent de larmes) ?</l>
						<l n="16" num="3.4">Nous hommes d’un sang noble, ô dieux ! ils nous frappaient.</l>
					</lg>
					<lg n="4">
						<l n="17" num="4.1"><hi rend="ital">Væ victis !</hi> mot cruel qui durement s’expie !</l>
						<l n="18" num="4.2">Le sais-tu,<hi rend="ital">Brenn</hi>* féroce, ô sauvage insensé ?</l>
						<l n="19" num="4.3">Ainsi tu t’écriais, le fer sur l’Italie ;</l>
						<l n="20" num="4.4">Hélas ! sur tes enfants l’anathème a passé.</l>
						<l n="21" num="4.5">Vous donc, vainqueurs nouveaux, plus de parole impie :</l>
						<l n="22" num="4.6">Ce dard revient frapper le bras qui l’a lancé.</l>
					</lg>
					<lg n="5">
						<l n="23" num="5.1">Oui, malheur aux vaincus, car le plus fort abuse,</l>
						<l n="24" num="5.2">Il aime sous ses pieds à fouler tous les cœurs :</l>
						<l n="25" num="5.3">Mais le joug le plus dur pourtant faiblit et s’use,</l>
						<l n="26" num="5.4">L’esclave s’affranchit ou par force ou par ruse.</l>
						<l n="27" num="5.5"><space unit="char" quantity="8"></space>Tôt ou tard malheur aux vainqueurs !…</l>
						<l n="28" num="5.6">O changement du sort ! ô justice confuse !</l>
						<l n="29" num="5.7">Flux, reflux éternels et de sang et de pleurs !</l>
					</lg>
					<closer>
						<note id="*" type="footnote">Brenn, chef, d’où Brennus.</note>
					</closer>
				</div></body></text></TEI>