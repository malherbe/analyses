<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FLEUR D’OR</title>
				<title type="medium">Une édition électronique</title>
				<author key="BRI">
					<name>
						<forename>Auguste</forename>
						<surname>BRIZEUX</surname>
					</name>
					<date from="1803" to="1858">1803-1858</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2992 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BRI_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA FLEUR D’OR</title>
						<author>Auguste Brizeux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">https://fr.wikisource.org/wiki/La_Fleur_d%E2%80%99Or</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Œuvres de Auguste Brizeux — LA FLEUR D’OR</title>
								<author>Auguste Brizeux</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Alphonse Lemerre, éditeur</publisher>
									<date when="1981">1981</date>
								</imprint>
								<biblScope unit="tome">3</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1874">1874</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les deux préfaces ne sont pas reprises.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-02-05" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-02-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LIVRE SIXIÈME</head><head type="sub_part">A NAPLE</head><div type="poem" key="BRI111">
					<head type="main">Les trois Frères</head>
					<div type="section" n="1">
						<head type="number">I</head>
						<lg n="1">
							<l n="1" num="1.1">TU reçus en naissant le don de la beauté,</l>
							<l n="2" num="1.2">Un front pur, un regard plein de sérénité</l>
							<l n="3" num="1.3">D’où sortait par éclairs, comme une chaste flamme,</l>
							<l n="4" num="1.4">L’idéale beauté que renfermait ton âme.</l>
							<l n="5" num="1.5">Les vierges, les enfants et les anges de Dieu</l>
							<l n="6" num="1.6">(Ce qu’on voit de plus doux en tout temps, en tout lieu),</l>
							<l n="7" num="1.7">Morts à jamais sans toi, retrouvèrent la vie,</l>
							<l n="8" num="1.8">Et ta main amoureuse en sema l’Italie :</l>
							<l n="9" num="1.9">Salut et gloire à toi, peintre envoyé du ciel,</l>
							<l n="10" num="1.10">Jeune ange au long profil appelé Raphaël !</l>
						</lg>
					</div>
					<div type="section" n="2">
						<head type="number">II</head>
						<lg n="1">
							<l n="11" num="1.1">À celui qui dormit sur l’épaule du Maître</l>
							<l n="12" num="1.2">Salut ! L’ami loyal fait oublier le traître.</l>
							<l n="13" num="1.3">Sous ses longs cheveux bruns, salut au bien-aimé,</l>
							<l n="14" num="1.4">Par qui, tout étant fait, le corps fut embaumé,</l>
							<l n="15" num="1.5">Et conservée aussi la plus tendre parole</l>
							<l n="16" num="1.6">De la nouvelle loi qui rapproche et console.</l>
							<l n="17" num="1.7">Tous ces mots de géhenne et de peuple maudit,</l>
							<l n="18" num="1.8">Sur ses lèvres de miel nul ne les entendit,</l>
							<l n="19" num="1.9">Mais ces mots : « Aimez-vous, enfants, les uns les autres, »</l>
							<l n="20" num="1.10">Voilà ce que disait le plus doux des apôtres.</l>
						</lg>
					</div>
					<div type="section" n="3">
						<head type="number">III</head>
						<lg n="1">
							<l n="21" num="1.1">L’évangéliste Jean, le peintre Raphaël,</l>
							<l n="22" num="1.2">Ces deux beaux envoyés de l’amour éternel,</l>
							<l n="23" num="1.3">Ont un frère en Jésus, digne que Jésus l’aime,</l>
							<l n="24" num="1.4">Bien qu’il soit né païen et soit mort sans baptême ;</l>
							<l n="25" num="1.5">Virgile est celui-là : tant l’aimable douceur</l>
							<l n="26" num="1.6">Au vrai Dieu nous élève et fait toute âme sœur.</l>
							<l n="27" num="1.7">Donc, comme une couronne autour de l’Évangile,</l>
							<l n="28" num="1.8">Inscrivez ces trois noms : Jean, Raphaël, Virgile,</l>
							<l n="29" num="1.9">Le disciple fervent, le peintre au pur contour,</l>
							<l n="30" num="1.10">Le poète inspiré qui devina l’amour.</l>
						</lg>
					</div>
				</div></body></text></TEI>