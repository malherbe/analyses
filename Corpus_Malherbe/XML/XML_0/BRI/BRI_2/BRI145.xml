<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FLEUR D’OR</title>
				<title type="medium">Une édition électronique</title>
				<author key="BRI">
					<name>
						<forename>Auguste</forename>
						<surname>BRIZEUX</surname>
					</name>
					<date from="1803" to="1858">1803-1858</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2992 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BRI_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA FLEUR D’OR</title>
						<author>Auguste Brizeux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">https://fr.wikisource.org/wiki/La_Fleur_d%E2%80%99Or</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Œuvres de Auguste Brizeux — LA FLEUR D’OR</title>
								<author>Auguste Brizeux</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Alphonse Lemerre, éditeur</publisher>
									<date when="1981">1981</date>
								</imprint>
								<biblScope unit="tome">3</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1874">1874</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les deux préfaces ne sont pas reprises.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-02-05" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-02-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LIVRE NEUVIÈME</head><head type="sub_part">EN BRETAGNE</head><div type="poem" key="BRI145">
					<head type="main">Les trois Poètes</head>
					<lg n="1">
						<l n="1" num="1.1">O penseurs inspirés que l’on nomme poètes,</l>
						<l n="2" num="1.2">Chercheurs de tous les temps, que de routes secrètes</l>
						<l n="3" num="1.3"><space unit="char" quantity="8"></space>Pour venir à la vérité !</l>
						<l n="4" num="1.4">Nature, Esprit, Raison, que n’avez-vous tenté,</l>
						<l n="5" num="1.5"><space unit="char" quantity="8"></space>O belles âmes inquiètes ?</l>
					</lg>
					<div n="1" type="section">
						<head type="number">I</head>
						<lg n="1">
							<l n="6" num="1.1">Absorbé dans le Tout il l’appelait son dieu.</l>
							<l n="7" num="1.2"><space unit="char" quantity="8"></space>Force invisible, éther ou feu,</l>
							<l n="8" num="1.3">Ce qui donne son âme à la nature entière</l>
							<l n="9" num="1.4">L’animait ; sur les monts, à l’ombre des grands bois,</l>
							<l n="10" num="1.5">Les choses l’attiraient par leurs intimes lois ;</l>
							<l n="11" num="1.6">Il parlait au torrent, il comprenait la pierre,</l>
							<l n="12" num="1.7">Et son art composait de ces milliers de voix</l>
							<l n="13" num="1.8">Un hymne où se mêlaient l’esprit et la matière.</l>
							<l n="14" num="1.9"><space unit="char" quantity="8"></space>Masse sans cercle et sans milieu,</l>
							<l n="15" num="1.10">Le grand Tout l’absorbait, lui l’appelait son dieu.</l>
						</lg>
					</div>
					<div n="2" type="section">
						<head type="number">II</head>
						<lg n="1">
							<l n="16" num="1.1">Les yeux levés au ciel où sont les belles choses,</l>
							<l n="17" num="1.2">Le poète attendait qu’enfin son astre eût lui,</l>
							<l n="18" num="1.3">Lorsque les trois Vertus descendirent vers lui ;</l>
							<l n="19" num="1.4">Et leurs longs vêtements étaient blancs, verts et roses.</l>
							<l n="20" num="1.5">Elles avaient les bras l’un à l’autre enlacés,</l>
							<l n="21" num="1.6">Mais leur front était chaste et leurs regards baissés ;</l>
							<l n="22" num="1.7">D’en haut elles disaient : « Je crois ! — J’espère ! — J’aime ! »</l>
							<l n="23" num="1.8">Le poète écouta les trois mots à genoux :</l>
							<l n="24" num="1.9">De là viennent ses chants et mystiques et doux :</l>
							<l n="25" num="1.10">Dans ce monde terrestre il chante un divin thème.</l>
						</lg>
					</div>
					<div n="3" type="section">
						<head type="number">III</head>
						<lg n="1">
							<l n="26" num="1.1">Il l’a voulu, le barde, et, par un libre effort,</l>
							<l n="27" num="1.2">Son cœur et son esprit, ses sens, tout est d’accord.</l>
							<l n="28" num="1.3"><space unit="char" quantity="8"></space>Extase libre, extase pure !</l>
							<l n="29" num="1.4">Dans la triple unité du poète penseur.</l>
							<l n="30" num="1.5">Tout ce qui lui répond : Dieu, l’homme et la nature.</l>
							<l n="31" num="1.6">Harmonieusement retentit et murmure ;</l>
							<l n="32" num="1.7">Chaque voix est distincte et se fond dans un chœur.</l>
							<l n="33" num="1.8"><space unit="char" quantity="8"></space>O barde sage ! extase pure !</l>
							<l n="34" num="1.9">Replié sur lui-même, il écoute enchanté</l>
							<l n="35" num="1.10">Les modulations de cette trinité. —</l>
						</lg>
					</div>
					<lg n="2">
						<l n="36" num="2.1"><space unit="char" quantity="8"></space>O belles âmes inquiètes,</l>
						<l n="37" num="2.2">Nature, Esprit, Raison, que n’avez-vous tenté ?</l>
						<l n="38" num="2.3"><space unit="char" quantity="8"></space>Pour venir à la vérité,</l>
						<l n="39" num="2.4">Chercheurs de tous les temps, que de routes secrètes,</l>
						<l n="40" num="2.5">O penseurs inspirés que l’on nomme poètes !</l>
					</lg>
				</div></body></text></TEI>