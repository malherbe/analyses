<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FLEUR D’OR</title>
				<title type="medium">Une édition électronique</title>
				<author key="BRI">
					<name>
						<forename>Auguste</forename>
						<surname>BRIZEUX</surname>
					</name>
					<date from="1803" to="1858">1803-1858</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2992 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BRI_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA FLEUR D’OR</title>
						<author>Auguste Brizeux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">https://fr.wikisource.org/wiki/La_Fleur_d%E2%80%99Or</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Œuvres de Auguste Brizeux — LA FLEUR D’OR</title>
								<author>Auguste Brizeux</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Alphonse Lemerre, éditeur</publisher>
									<date when="1981">1981</date>
								</imprint>
								<biblScope unit="tome">3</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1874">1874</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les deux préfaces ne sont pas reprises.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-02-05" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-02-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LIVRE QUATRIÈME</head><head type="sub_part">A FLORENCE</head><div type="poem" key="BRI81">
					<head type="main">Giannina</head>
					<opener>
						<placeName>Près de Fiésole.</placeName>
					</opener>
					<lg n="1">
						<l n="1" num="1.1">PLUS de sombres pensers ! Poètes radieux,</l>
						<l n="2" num="1.2">Ouvrons à la beauté notre esprit et nos yeux ;</l>
					</lg>
					<lg n="2">
						<l n="3" num="2.1">Partout, dans les palais, sous le chaume paisible,</l>
						<l n="4" num="2.2">Suivons-la ; dans les cœurs voyons cette invisible ;</l>
					</lg>
					<lg n="3">
						<l n="5" num="3.1">Et comme aux jours de foi couronnés par les arts,</l>
						<l n="6" num="3.2">Que la consolatrice enchante les regards !</l>
					</lg>
					<lg n="4">
						<l n="7" num="4.1">La grâce vit encor, la grâce florentine,</l>
						<l n="8" num="4.2">Elle qu’aimaient le marbre et la toile divine.</l>
					</lg>
					<lg n="5">
						<l n="9" num="5.1">Hier, j’ai vu s’unir dans un groupe charmant</l>
						<l n="10" num="5.2">La bouche d’une mère et celle d’une enfant.</l>
					</lg>
					<lg n="6">
						<l n="11" num="6.1">Légère, elle passait, sur le front sa corbeille.</l>
						<l n="12" num="6.2">Où dormait dans les fleurs sa fille, fleur vermeille :</l>
					</lg>
					<lg n="7">
						<l n="13" num="7.1">De cette blonde enfant elle a fait son bijou,</l>
						<l n="14" num="7.2">Blanche agrafe à son cœur, collier d’or à son cou ;</l>
					</lg>
					<lg n="8">
						<l n="15" num="8.1">Sur le bord du chemin parfois elle la pose,</l>
						<l n="16" num="8.2">Et donne son sein blanc à cette bouche rose ;</l>
					</lg>
					<lg n="9">
						<l n="17" num="9.1">Puis, telle que l’oiseau s’élançant du buisson,</l>
						<l n="18" num="9.2">Joyeuse, elle reprend sa route et sa chanson…</l>
					</lg>
					<lg n="10">
						<l n="19" num="10.1">Et moi, qui m’en venais morne et baissant la tête,</l>
						<l n="20" num="10.2">Devant ce frais tableau, réjoui, je m’arrête.</l>
					</lg>
					<lg n="11">
						<l n="21" num="11.1">Influence charmante ! Amant de la beauté !</l>
						<l n="22" num="11.2">En me laissant aller à mon cœur, j’ai chanté.</l>
					</lg>
					<lg n="12">
						<l n="23" num="12.1">O vivante madone et si pleine de grâce !</l>
						<l n="24" num="12.2">Les fleurs de poésie ont germé sur sa trace.</l>
					</lg>
					<lg n="13">
						<head type="main">À JULES SANDEAU</head>
						<l n="25" num="13.1">Voulez-vous ce bluet de mon humble chemin.</l>
						<l n="26" num="13.2">Poète qui semez des lis à pleine main ?</l>
					</lg>
				</div></body></text></TEI>