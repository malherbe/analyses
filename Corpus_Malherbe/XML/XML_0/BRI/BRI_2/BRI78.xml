<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FLEUR D’OR</title>
				<title type="medium">Une édition électronique</title>
				<author key="BRI">
					<name>
						<forename>Auguste</forename>
						<surname>BRIZEUX</surname>
					</name>
					<date from="1803" to="1858">1803-1858</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2992 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BRI_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA FLEUR D’OR</title>
						<author>Auguste Brizeux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">https://fr.wikisource.org/wiki/La_Fleur_d%E2%80%99Or</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Œuvres de Auguste Brizeux — LA FLEUR D’OR</title>
								<author>Auguste Brizeux</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Alphonse Lemerre, éditeur</publisher>
									<date when="1981">1981</date>
								</imprint>
								<biblScope unit="tome">3</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1874">1874</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les deux préfaces ne sont pas reprises.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-02-05" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-02-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LIVRE TROISIÈME</head><head type="sub_part">AU BORD DE LA MÉDITERRANÉE</head><div type="poem" key="BRI78">
					<head type="main">Lettre</head>
					<opener>
						<salute>à un Chanteur de Tréguier</salute>
					</opener>
					<lg n="1">
						<l n="1" num="1.1">COMME je voyageais sur le chemin de Rome,</l>
						<l n="2" num="1.2">Iannic Coz, une lettre arrivait jusqu’à moi ;</l>
						<l n="3" num="1.3"><space unit="char" quantity="8"></space>On y parle de vous, brave homme,</l>
						<l n="4" num="1.4">Des chanteurs de Tréguier vous le chef et le roi.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">« Grâce à Jean, disait-on, sans tes vers point de fête.</l>
						<l n="6" num="2.2">Aux luttes il les chante, il les chante aux Pardons ;</l>
						<l n="7" num="2.3"><space unit="char" quantity="8"></space>Et le tisserand les répète</l>
						<l n="8" num="2.4">En poussant sa navette entre tous ses cordons.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">« Mon sonneur les sait mieux que matines et laudes ;</l>
						<l n="10" num="3.2">Pour Iannic le chanteur, ce malin Trégorrois,</l>
						<l n="11" num="3.3"><space unit="char" quantity="8"></space>Il t’a dû bien des crêpes chaudes.</l>
						<l n="12" num="3.4">Bien du cidre nouveau pour rafraîchir sa voix. »</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">Voila ce qu’on m’écrit et j’ai tressailli d’aise :</l>
						<l n="14" num="4.2">À moi le bruit, à vous le cidre jusqu’au bord ;</l>
						<l n="15" num="4.3"><space unit="char" quantity="8"></space>Sur un seul point, ne vous déplaise,</l>
						<l n="16" num="4.4">Beau chanteur, mon ami, nous serons peu d’accord.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">Certain libraire intrus sous sa presse maudite</l>
						<l n="18" num="5.2">A repétri pour vous et travaillé mon grain ;</l>
						<l n="19" num="5.3"><space unit="char" quantity="8"></space>Mon cœur de barde s’en irrite ;</l>
						<l n="20" num="5.4">Moi-même dans le four j’aime à mettre mon pain.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1">Mangez-le. De grand cœur, ami, je vous le donne ;</l>
						<l n="22" num="6.2">Mais gardez, en l’offrant, d’y jeter votre sel ;</l>
						<l n="23" num="6.3"><space unit="char" quantity="8"></space>Assez pour la table bretonne</l>
						<l n="24" num="6.4">Mêlent au pur froment un levain criminel.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1">Si quelque nain méchant fendait votre bombarde,</l>
						<l n="26" num="7.2">Faussait l’anche, ou mettait du sable dans les trous,</l>
						<l n="27" num="7.3"><space unit="char" quantity="8"></space>Vous crîriez !… Ainsi fait le barde.</l>
						<l n="28" num="7.4">Le juge peut m’entendre : Ami, le savez-vous ?</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1">Pourtant je veux la paix. — Pour les jours qui vont suivre</l>
						<l n="30" num="8.2">Ce triste hiver, voici ma nouvelle chanson ;</l>
						<l n="31" num="8.3"><space unit="char" quantity="8"></space>Que vos sacs se gonflent de cuivre ;</l>
						<l n="32" num="8.4">Bien repu, chaque soir, rentrez à la maison.</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1">Des forêts à la mer poursuivez votre quête ;</l>
						<l n="34" num="9.2">Qu’on redise après vous <hi rend="ital">Les Conscrits de Plô-Meûr</hi> ;</l>
						<l n="35" num="9.3"><space unit="char" quantity="8"></space>Ne chantez pas à pleine tête :</l>
						<l n="36" num="9.4">Faites pleurer les yeux et soupirer le cœur.</l>
					</lg>
				</div></body></text></TEI>