<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FLEUR D’OR</title>
				<title type="medium">Une édition électronique</title>
				<author key="BRI">
					<name>
						<forename>Auguste</forename>
						<surname>BRIZEUX</surname>
					</name>
					<date from="1803" to="1858">1803-1858</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2992 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BRI_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA FLEUR D’OR</title>
						<author>Auguste Brizeux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">https://fr.wikisource.org/wiki/La_Fleur_d%E2%80%99Or</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Œuvres de Auguste Brizeux — LA FLEUR D’OR</title>
								<author>Auguste Brizeux</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Alphonse Lemerre, éditeur</publisher>
									<date when="1981">1981</date>
								</imprint>
								<biblScope unit="tome">3</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1874">1874</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les deux préfaces ne sont pas reprises.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-02-05" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-02-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LIVRE CINQUIÈME</head><head type="sub_part">A ROME</head><div type="poem" key="BRI105">
					<head type="main">Talismans</head>
					<lg n="1">
						<l n="1" num="1.1">TALISMANS d’amitié, triple et mystique envoi,</l>
						<l n="2" num="1.2">Protégez ceux que j’aime et parlez-leur de moi.</l>
					</lg>
					<ab type="dash">────────</ab>
					<div n="1" type="section">
						<head type="number">I</head>
						<lg n="1">
							<l n="3" num="1.1">Les vers comme les chants ont un pouvoir féerique ;</l>
							<l n="4" num="1.2">Ainsi le croit Eir-Inn ou la verte Armorique.</l>
							<l n="5" num="1.3">Dans ton berceau d’osier, un matin, cher enfant,</l>
							<l n="6" num="1.4">On cachera ces vers : leur pouvoir te défend ;</l>
							<l n="7" num="1.5">Mais vers Clone-Menà, froid pays de ta mère,</l>
							<l n="8" num="1.6">Un jour, quand tu suivras le daim sur la bruyère,</l>
							<l n="9" num="1.7">Pense au barde breton, alors muet vieillard,</l>
							<l n="10" num="1.8">Et chante au son du cor ses vers dans le brouillard.</l>
							<l n="11" num="1.9">Les chants comme les vers ont un pouvoir féerique ;</l>
							<l n="12" num="1.10">Ainsi le croit Eir-Inn ou la verte Armorique.</l>
						</lg>
					</div>
					<div n="2" type="section">
						<head type="number">II</head>
						<lg n="1">
							<l n="13" num="1.1">Bonne grand’mère, à toi les célestes soutiens !</l>
							<l n="14" num="1.2">Ce chapelet bénit par le chef des chrétiens,</l>
							<l n="15" num="1.3">Tout près de ton fauteuil suspends-le dans ta chambre.</l>
							<l n="16" num="1.4">Vois, ses cinquante grains sont d’aloès et d’ambre :</l>
							<l n="17" num="1.5">Qu’ils parfument entre eux ton modeste réduit</l>
							<l n="18" num="1.6">Et, t’occupant le jour, te consolent la nuit !</l>
							<l n="19" num="1.7">L’hiver de notre vie est souvent morne et sombre,</l>
							<l n="20" num="1.8">Et de tes pleurs secrets seule tu sais le nombre ;</l>
							<l n="21" num="1.9">Prends donc ce chapelet, et puisse chaque grain</l>
							<l n="22" num="1.10">Défilé sous tes doigts entraîner un chagrin !</l>
						</lg>
					</div>
					<div n="3" type="section">
						<head type="number">III</head>
						<lg n="1">
							<l n="23" num="1.1">Celui qui recevra cette feuille séchée</l>
							<l n="24" num="1.2">De mon envoi pieux aura l’âme touchée.</l>
							<l n="25" num="1.3">À Saint-Onofrio je la cueillis un soir</l>
							<l n="26" num="1.4">Sous le chêne du Tasse, ombrage calme et noir</l>
							<l n="27" num="1.5">Où jadis entouré de moines au front blême,</l>
							<l n="28" num="1.6">Lui, plus triste qu’eux tous, leur lisait son poème :</l>
							<l n="29" num="1.7">Là vint pour s’abriter le grand infortuné</l>
							<l n="30" num="1.8">Attendant que le jour du triomphe eût sonné.</l>
							<l n="31" num="1.9">Et c’est là qu’il mourut ; car nous autres poètes,</l>
							<l n="32" num="1.10">Toujours nous demandons des couvents ou des fêtes.</l>
						</lg>
					</div>
					<ab type="dash">────────</ab>
					<lg n="2">
						<l n="33" num="2.1">Talismans d’amitié, triple et mystique envoi,</l>
						<l n="34" num="2.2">Protégez ceux que j’aime et parlez-leur de moi.</l>
					</lg>
				</div></body></text></TEI>