<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FLEUR D’OR</title>
				<title type="medium">Une édition électronique</title>
				<author key="BRI">
					<name>
						<forename>Auguste</forename>
						<surname>BRIZEUX</surname>
					</name>
					<date from="1803" to="1858">1803-1858</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2992 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BRI_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA FLEUR D’OR</title>
						<author>Auguste Brizeux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">https://fr.wikisource.org/wiki/La_Fleur_d%E2%80%99Or</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Œuvres de Auguste Brizeux — LA FLEUR D’OR</title>
								<author>Auguste Brizeux</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Alphonse Lemerre, éditeur</publisher>
									<date when="1981">1981</date>
								</imprint>
								<biblScope unit="tome">3</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1874">1874</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les deux préfaces ne sont pas reprises.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-02-05" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-02-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LIVRE DEUXIÈME</head><head type="sub_part">A PARIS</head><div type="poem" key="BRI66">
					<head type="main">À E.</head>
					<div n="1" type="section">
						<head type="number">I</head>
						<lg n="1">
							<l n="1" num="1.1">LE jour naît : dans les prés et sous les taillis verts</l>
							<l n="2" num="1.2">Allons, allons cueillir et des fleurs et des vers,</l>
							<l n="3" num="1.3"><space unit="char" quantity="8"></space>Tandis que la ville repose ;</l>
							<l n="4" num="1.4">La fleur ouvre au matin plus de pourpre et d’azur,</l>
							<l n="5" num="1.5">Et le vers, autre fleur, s’épanouit plus pur</l>
							<l n="6" num="1.6"><space unit="char" quantity="8"></space>À l’aube humide qui l’arrose.</l>
						</lg>
						<lg n="2">
							<l n="7" num="2.1">Que de fleurs ont passé qu’on n’a point su cueillir !</l>
							<l n="8" num="2.2">Sur sa tige oubliée, ah ! ne laissons vieillir</l>
							<l n="9" num="2.3"><space unit="char" quantity="8"></space>Aucune des fleurs de ce monde.</l>
							<l n="10" num="2.4">Allons cueillir des fleurs ! par un charme idéal.</l>
							<l n="11" num="2.5">Qu’avec l’encens des vers leur parfum matinal</l>
							<l n="12" num="2.6"><space unit="char" quantity="8"></space>Amoureusement se confonde.</l>
						</lg>
						<lg n="3">
							<l n="13" num="3.1">Allons cueillir des vers ! sous la fleur du buisson</l>
							<l n="14" num="3.2">Entendez-vous l’oiseau qui chante sa chanson ?</l>
							<l n="15" num="3.3"><space unit="char" quantity="8"></space>Tout chante et fleurit, c’est l’aurore !</l>
							<l n="16" num="3.4">Je veux chanter aussi : blonde fille du ciel.</l>
							<l n="17" num="3.5">Ainsi de fleur en fleur va butinant son miel</l>
							<l n="18" num="3.6"><space unit="char" quantity="8"></space>L’abeille joyeuse et sonore.</l>
						</lg>
						<lg n="4">
							<l n="19" num="4.1">Cueillons des fleurs ! Et puis, heureux de mon fardeau,</l>
							<l n="20" num="4.2">Je reviendrai m’asseoir prés du léger rideau</l>
							<l n="21" num="4.3"><space unit="char" quantity="8"></space>Qui voile encor ma bien-aimée,</l>
							<l n="22" num="4.4">Et, du bruit de mes vers dissipant son sommeil,</l>
							<l n="23" num="4.5">Je ferai sur ses yeux et sur son front vermeil</l>
							<l n="24" num="4.6"><space unit="char" quantity="8"></space>Tomber une pluie embaumée.</l>
						</lg>
						<lg n="5">
							<l n="25" num="5.1">Riante et mollement soulevée à demi,</l>
							<l n="26" num="5.2">Je veux que de mes fleurs sur son front endormi</l>
							<l n="27" num="5.3"><space unit="char" quantity="8"></space>Sa blanche main suive la trace ;</l>
							<l n="28" num="5.4">Et qu’en un doux silence admirant leurs couleurs,</l>
							<l n="29" num="5.5">Elle doute longtemps qui, des vers ou des fleurs,</l>
							<l n="30" num="5.6"><space unit="char" quantity="8"></space>Ont plus de fraîcheur et de grâce.</l>
						</lg>
					</div>
					<div n="2" type="section">
						<head type="number">II</head>
						<lg n="1">
							<l n="31" num="1.1">Mes rustiques habits étaient là dans la chambre,</l>
							<l n="32" num="1.2"><space unit="char" quantity="8"></space>Costume sauvage et brillant :</l>
							<l n="33" num="1.3"><space unit="char" quantity="8"></space>Je songeais en les déployant</l>
							<l n="34" num="1.4">Aux lieux qui m’ont vu jeune, aux retours en septembre.</l>
						</lg>
						<lg n="2">
							<l n="35" num="2.1">Elle, toute au présent, riait de mes soucis ;</l>
							<l n="36" num="2.2"><space unit="char" quantity="8"></space>Ou sur mon passé, chose éteinte,</l>
							<l n="37" num="2.3"><space unit="char" quantity="8"></space>Revenant légère et sans crainte</l>
							<l n="38" num="2.4">(Mais s’abusant peut-être), écoutait mes récits.</l>
						</lg>
						<lg n="3"> 
							<l n="39" num="3.1">Souvent les fruits lointains sont plus doux, bien qu’étrange</l>
							<l n="40" num="3.2"><space unit="char" quantity="8"></space>Au cœur d’un autre on aime à voir,</l>
							<l n="41" num="3.3"><space unit="char" quantity="8"></space>À doubler par lui son savoir :</l>
							<l n="42" num="3.4">Notre esprit curieux se plaît à ces échanges.</l>
						</lg>
						<lg n="4">
							<l n="43" num="4.1">J’écoute, disait-elle ; allons, barde, chantez ! »</l>
							<l n="44" num="4.2"><space unit="char" quantity="8"></space>Et, le front penché sur la glace,</l>
							<l n="45" num="4.3"><space unit="char" quantity="8"></space>Elle rattachait avec grâce</l>
							<l n="46" num="4.4">Ses cheveux, noirs bandeaux sur ses tempes jetés.</l>
						</lg>
					</div>
					<div n="3" type="section">
						<head type="number">III</head>
						<lg n="1">
							<l n="47" num="1.1">En elle je n’aimai d’abord que la beauté,</l>
							<l n="48" num="1.2">La bouche humide et fraîche ouverte à la gaîté,</l>
							<l n="49" num="1.3"><space unit="char" quantity="8"></space>Et l’or bruni de ses épaules,</l>
							<l n="50" num="1.4">Et les frêles contours de ce corps souple et fin</l>
							<l n="51" num="1.5">Qui plie à chaque pas, comme à l’air du matin</l>
							<l n="52" num="1.6"><space unit="char" quantity="8"></space>Le long des eaux tremblent les saules.</l>
						</lg>
						<lg n="2">
							<l n="53" num="2.1">J’ai connu la beauté ! que m’importait alors</l>
							<l n="54" num="2.2">Si nulle âme, en parlant, n’animait ce beau corps,</l>
							<l n="55" num="2.3"><space unit="char" quantity="8"></space>Ces longues paupières d’Arabe ?</l>
							<l n="56" num="2.4">Heureux de respirer ce souffle virginal,</l>
							<l n="57" num="2.5">Ou d’écouter, rêveur, de sa voix de cristal</l>
							<l n="58" num="2.6"><space unit="char" quantity="8"></space>Tomber quelque molle syllabe.</l>
						</lg>
						<lg n="3">
							<l n="59" num="3.1">Pardon, si tu le peux ! à tes genoux, pardon !</l>
							<l n="60" num="3.2">Lorsque, le cœur brisé, pâle et dans l’abandon,</l>
							<l n="61" num="3.3"><space unit="char" quantity="8"></space>Plus faible que toi, faible femme.</l>
							<l n="62" num="3.4">Je vins tout éploré te dire mes douleurs,</l>
							<l n="63" num="3.5">Ta secrète beauté s’éveilla sous mes pleurs.</l>
							<l n="64" num="3.6"><space unit="char" quantity="8"></space>Et tu me révélais ton âme.</l>
						</lg>
						<lg n="4">
							<l n="65" num="4.1">O larmes ! ô soupirs ! ô mystères d’amour !</l>
							<l n="66" num="4.2">Femmes, pour nous charmer, vous avez tour à tour</l>
							<l n="67" num="4.3"><space unit="char" quantity="8"></space>La beauté visible et cachée ;</l>
							<l n="68" num="4.4">Êtres deux fois doués ! Êtres puissants et doux !</l>
							<l n="69" num="4.5">Vous domptez notre force ; elle marche après vous</l>
							<l n="70" num="4.6"><space unit="char" quantity="8"></space>D’un double lien attachée.</l>
						</lg>
					</div>
					<div n="4" type="section">
						<head type="number">IV</head>
						<lg n="1">
							<l n="71" num="1.1">Ah ! dis-moi, jeune femme, autour de ta demeure</l>
							<l n="72" num="1.2"><space unit="char" quantity="8"></space>N’entends-tu pas de voix qui pleure ?</l>
							<l n="73" num="1.3">Comme moi tu perdis le rire aux ailes d’or ;</l>
							<l n="74" num="1.4">Mais ton crédule espoir l’appelle-t-il encor ?</l>
							<l n="75" num="1.5">Heureuse d’espérer ! — Après un long silence,</l>
							<l n="76" num="1.6">Lorsqu’un hymne en secret de mon âme s’élance,</l>
							<l n="77" num="1.7">Ce n’est plus vers mes jours de printemps et de fleurs</l>
							<l n="78" num="1.8">C’est assez d’écarter de moi l’ange des pleurs,</l>
							<l n="79" num="1.9">Cet ange toujours pâle et toujours lamentable</l>
							<l n="80" num="1.10">Qui pleure à mon chevet et qui pleure à ma table.</l>
							<l n="81" num="1.11">Mais si le rire ailé rentre dans ma maison.</l>
							<l n="82" num="1.12">Si l’été qui fleurit sèche sous un rayon</l>
							<l n="83" num="1.13">Mes larmes, tu verras la chanteuse alouette</l>
							<l n="84" num="1.14">Envier dans le ciel ma voix qu’on dit muette,</l>
							<l n="85" num="1.15">Les bardes, s’éveillant, diront : « C’est lui ! c’est lui ! »</l>
							<l n="86" num="1.16">Et les tranquilles eaux du Leff… Mais aujourd’hui !</l>
							<l n="87" num="1.17"><space unit="char" quantity="8"></space>Ah ! dis-moi, jeune et douce femme,</l>
							<l n="88" num="1.18">N’entends-tu pas des voix qui pleurent dans ton âme ?</l>
						</lg>
					</div>
					<div n="5" type="section">
						<head type="number">V</head>
						<lg n="1">
							<l n="89" num="1.1">Si je viens à passer, sur ton front, en tremblant.</l>
							<l n="90" num="1.2">Hélas ! n’abaisse pas ainsi ton voile blanc,</l>
							<l n="91" num="1.3"><space unit="char" quantity="8"></space>Toute pâle et toute troublée ;</l>
							<l n="92" num="1.4">Au bras qui te conduit n’attache plus ton bras ;</l>
							<l n="93" num="1.5">Comme pour m’éviter, ne presse plus tes pas</l>
							<l n="94" num="1.6"><space unit="char" quantity="8"></space>Vers quelque solitaire allée.</l>
						</lg>
						<lg n="2">
							<l n="95" num="2.1">Eh bien ! si ma rencontre importune tes yeux,</l>
							<l n="96" num="2.2">Parle avec confiance et décide en quels lieux</l>
							<l n="97" num="2.3"><space unit="char" quantity="8"></space>Il faut pour toi que je m’exile ;</l>
							<l n="98" num="2.4">Ton amour fut ma paix, mon bonheur, mon soutien,</l>
							<l n="99" num="2.5">Qu’aujourd’hui mon repos ne trouble plus le tien ;</l>
							<l n="100" num="2.6"><space unit="char" quantity="8"></space>Commande, je serai docile.</l>
						</lg>
						<lg n="3">
							<l n="101" num="3.1">Alors tes yeux ternis reprendront leur azur,</l>
							<l n="102" num="3.2">Le jour comme autrefois naîtra limpide et pur,</l>
							<l n="103" num="3.3"><space unit="char" quantity="8"></space>La nuit s’écoulera sans fièvre ;</l>
							<l n="104" num="3.4">Tu t’abandonneras à ta sécurité.</l>
							<l n="105" num="3.5">Et l’innocence aimable et la douce gaîté</l>
							<l n="106" num="3.6"><space unit="char" quantity="8"></space>Souriront encor sur ta lèvre.</l>
						</lg>
						<lg n="4">
							<l n="107" num="4.1">Dis un mot et je pars. — Sans trop d’ennuis pour toi,</l>
							<l n="108" num="4.2">Si je puis cependant demeurer, souffre-moi ;</l>
							<l n="109" num="4.3"><space unit="char" quantity="8"></space>Et, lorsque au détour d’une rue</l>
							<l n="110" num="4.4">Tout à coup devant toi m’offrira le hasard,</l>
							<l n="111" num="4.5">Passe libre et sans peur, ne crains pas mon regard ;</l>
							<l n="112" num="4.6"><space unit="char" quantity="8"></space>Je ne t’aurai pas reconnue.</l>
						</lg>
						<lg n="5">
							<l n="113" num="5.1">Seulement (je t’en prie !), oh ! quand tu seras loin,</l>
							<l n="114" num="5.2">Quand je pourrai braver et soupçons et témoin,</l>
							<l n="115" num="5.3"><space unit="char" quantity="8"></space>Vers toi que je tourne la tête.</l>
							<l n="116" num="5.4">Pour observer encor ton pas modeste et lent,</l>
							<l n="117" num="5.5">El tout ce qu’à mon cœur ce marcher indolent</l>
							<l n="118" num="5.6"><space unit="char" quantity="8"></space>Rappelle de grâce secrète.</l>
						</lg>
						<lg n="6">
							<l n="119" num="6.1">Alors, alors mon cœur bondira ! mille accords,</l>
							<l n="120" num="6.2">Mille vœux dans mou cœur retentiront alors.</l>
							<l n="121" num="6.3"><space unit="char" quantity="8"></space>Et se répandront sur ta route ;</l>
							<l n="122" num="6.4">Et mille illusions, mille prospérités,</l>
							<l n="123" num="6.5">Comme des anges purs iront à tes côtés.</l>
							<l n="124" num="6.6"><space unit="char" quantity="8"></space>Ce jour-là, si le ciel m’écoute !</l>
						</lg>
					</div>
				</div></body></text></TEI>