<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FLEUR D’OR</title>
				<title type="medium">Une édition électronique</title>
				<author key="BRI">
					<name>
						<forename>Auguste</forename>
						<surname>BRIZEUX</surname>
					</name>
					<date from="1803" to="1858">1803-1858</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2992 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BRI_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA FLEUR D’OR</title>
						<author>Auguste Brizeux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">https://fr.wikisource.org/wiki/La_Fleur_d%E2%80%99Or</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Œuvres de Auguste Brizeux — LA FLEUR D’OR</title>
								<author>Auguste Brizeux</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Alphonse Lemerre, éditeur</publisher>
									<date when="1981">1981</date>
								</imprint>
								<biblScope unit="tome">3</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1874">1874</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les deux préfaces ne sont pas reprises.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-02-05" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-02-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LIVRE HUITIÈME</head><head type="sub_part">A PARIS</head><div type="poem" key="BRI132">
					<head type="main">Portraits</head>
					<div type="section" n="1">
						<head type="number">I</head>
						<head type="main">LE POÈTE</head>
						<lg n="1">
							<l n="1" num="1.1">D’UNE larme du Christ celle qui fut formée</l>
							<l n="2" num="1.2">Choisit sur terre un barde enclin à tous les pleurs,</l>
							<l n="3" num="1.3">Et, pleurant, lui montra la chaîne de douleurs</l>
							<l n="4" num="1.4">Qui tient depuis Adam notre race enfermée ;</l>
							<l n="5" num="1.5">Chez les anges la vierge avait nom Éloa,</l>
							<l n="6" num="1.6">Nom sacré que plus tard le barde révéla ;</l>
							<l n="7" num="1.7">Il parcourut les temps à l’ombre de ses ailes,</l>
							<l n="8" num="1.8">Recherchant le malheur et chantant la pitié ;</l>
							<l n="9" num="1.9">Puis, quand l’ange tomba, sa mystique amitié</l>
							<l n="10" num="1.10">Eut pour des maux sans fin des plaintes immortelles.</l>
						</lg>
					</div>
					<div type="section" n="2">
						<head type="number">II</head>
						<head type="main">LE PRÊTRE</head>
						<lg n="1">
							<l n="11" num="1.1">Tu mérites aussi de tout pieux chanteur</l>
							<l n="12" num="1.2">Un hymne d’amitié, cœur tendre et toujours jeune,</l>
							<l n="13" num="1.3">Toi qui sus opposer aux souffrances du jeûne</l>
							<l n="14" num="1.4">L’âme et le corps du Christ, froment générateur<ref type="noteAnchor">*</ref></l>
							<l n="15" num="1.5">Tu t’es bien pénétré de sa vertu secrète :</l>
							<l n="16" num="1.6">C’est la douceur du prêtre et celle du poète ;</l>
							<l n="17" num="1.7">Mais la réflexion au langage savant</l>
							<l n="18" num="1.8">Gouverne avec bonheur ton zèle et le tempère ;</l>
							<l n="19" num="1.9">On t’appellera Maître, et, cortège fervent,</l>
							<l n="20" num="1.10">Des fils de ton esprit te suivront comme un père.</l>
						</lg>
					</div>
					<div type="section" n="3">
						<head type="number">III</head>
						<head type="main">LE PHILOSOPHE</head>
						<lg n="1">
							<l n="21" num="1.1">Les jeunes gens rêveurs tournaient vers lui les yeux ;</l>
							<l n="22" num="1.2">Lui, Sage au front candide issu des anciens Sages,</l>
							<l n="23" num="1.3">Attentif au présent, mais planant sur les âges,</l>
							<l n="24" num="1.4">Lisait nos changements dans une loi des cieux.</l>
							<l n="25" num="1.5">Comme un platonicien dans sa tunique blanche,</l>
							<l n="26" num="1.6">Replié sur lui-même, ainsi vivait Ballanche,</l>
							<l n="27" num="1.7">Mystérieux penseur, calme et triste à la fois :</l>
							<l n="28" num="1.8">S’il enseigne à quel prix le bien germe et s’enfante,</l>
							<l n="29" num="1.9">Ses chants révélateurs semblent d’un hiérophante,</l>
							<l n="30" num="1.10">Ou la plainte d’Orphée expirant dans les bois.</l>
						</lg>
					</div>
					<closer>
						<note type="footnote" id="*">Du Dogme générateur de l’Eucharistie, par l’abbé Gerbet.</note>
					</closer>
				</div></body></text></TEI>