<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FLEUR D’OR</title>
				<title type="medium">Une édition électronique</title>
				<author key="BRI">
					<name>
						<forename>Auguste</forename>
						<surname>BRIZEUX</surname>
					</name>
					<date from="1803" to="1858">1803-1858</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2992 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BRI_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA FLEUR D’OR</title>
						<author>Auguste Brizeux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">https://fr.wikisource.org/wiki/La_Fleur_d%E2%80%99Or</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Œuvres de Auguste Brizeux — LA FLEUR D’OR</title>
								<author>Auguste Brizeux</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Alphonse Lemerre, éditeur</publisher>
									<date when="1981">1981</date>
								</imprint>
								<biblScope unit="tome">3</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1874">1874</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les deux préfaces ne sont pas reprises.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-02-05" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-02-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LIVRE SEPTIÈME</head><head type="sub_part">A VENISE</head><div type="poem" key="BRI126">
					<head type="main">L’Asile</head>
					<lg n="1">
						<l n="1" num="1.1">REPOSONS-NOUS ailleurs, le doute a hérissé</l>
						<l n="2" num="1.2">De trop de dards aigus la couche du passé.</l>
						<l n="3" num="1.3">Mais croire, mais aimer quand toute âme s’envole,</l>
						<l n="4" num="1.4">Et quand chaque matin voit tomber chaque idole !</l>
						<l n="5" num="1.5">Cependant il le faut, croyons, aimons encor,</l>
						<l n="6" num="1.6">Croyons bien aux plaisirs et pour eux aimons l’or,</l>
						<l n="7" num="1.7">Croyons à cela seul qu’on ne doit plus rien croire,</l>
						<l n="8" num="1.8">Hors aux baisers cueillis sur un beau front d’ivoire ;</l>
						<l n="9" num="1.9">Dieu mort, ils ont tué l’amour et l’amitié :</l>
						<l n="10" num="1.10">Croyons tous au malheur sans croire à la pitié,</l>
						<l n="11" num="1.11">Et cherchons loin, bien loin, un asile suprême</l>
						<l n="12" num="1.12">Pour oublier enfin les autres et nous-même.</l>
						<l n="13" num="1.13">O vous, frères amis, qui d’un monde hideux,</l>
						<l n="14" num="1.14">Voyageurs éplorés, êtes sortis tous deux,</l>
						<l n="15" num="1.15">L’un éteignant sa vie au creux de la vallée,</l>
						<l n="16" num="1.16">L’autre emportant au cloître une âme désolée,</l>
						<l n="17" num="1.17">Mais tous deux expirant d’une si douce voix</l>
						<l n="18" num="1.18">Que votre sol natal en agita ses bois,</l>
						<l n="19" num="1.19">Ah ! s’il est loin du monde un lieu sûr où l’on dorme,</l>
						<l n="20" num="1.20">Répondez, Amaury, dites, Joseph Delorme,</l>
						<l n="21" num="1.21">Où le lit est meilleur et le sommeil plus long :</l>
						<l n="22" num="1.22">Est-ce à l’ombre du cloître ? Est-ce au creux du vallon ?</l>
						<l n="23" num="1.23">En nous-même peut-être il est un sûr refuge</l>
						<l n="24" num="1.24">Où l’âme en descendant sait juger qui la juge,</l>
						<l n="25" num="1.25">Un sanctuaire calme où le doute acéré</l>
						<l n="26" num="1.26">Malgré tous ses replis n’a jamais pénétré :</l>
						<l n="27" num="1.27">Beau temple intérieur tout rempli d’eaux lustrales,</l>
						<l n="28" num="1.28">De mets fortifiants et d’essences vitales.</l>
						<l n="29" num="1.29">Si les corps sont régis par l’éternelle loi,</l>
						<l n="30" num="1.30">Sonde ta destinée, âme, et rassure-toi !</l>
						<l n="31" num="1.31">Quel Titan espéra dans ses deux mains géantes</l>
						<l n="32" num="1.32">Détruire une de vous, molécules vivantes ;</l>
						<l n="33" num="1.33">Ou de l’âme déserte exiler sans retour</l>
						<l n="34" num="1.34">La divine espérance et le divin amour ?</l>
					</lg>
				</div></body></text></TEI>