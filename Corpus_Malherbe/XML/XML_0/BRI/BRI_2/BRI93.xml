<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FLEUR D’OR</title>
				<title type="medium">Une édition électronique</title>
				<author key="BRI">
					<name>
						<forename>Auguste</forename>
						<surname>BRIZEUX</surname>
					</name>
					<date from="1803" to="1858">1803-1858</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2992 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BRI_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA FLEUR D’OR</title>
						<author>Auguste Brizeux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">https://fr.wikisource.org/wiki/La_Fleur_d%E2%80%99Or</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Œuvres de Auguste Brizeux — LA FLEUR D’OR</title>
								<author>Auguste Brizeux</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Alphonse Lemerre, éditeur</publisher>
									<date when="1981">1981</date>
								</imprint>
								<biblScope unit="tome">3</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1874">1874</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les deux préfaces ne sont pas reprises.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-02-05" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-02-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LIVRE CINQUIÈME</head><head type="sub_part">A ROME</head><div type="poem" key="BRI93">
					<head type="main">À S. Mauto</head>
					<head type="sub_1">(Nom italien de saint Malô)</head>
					<lg n="1">
						<l n="1" num="1.1">COMMENT, bon saint Malô, pauvre évêque breton,</l>
						<l n="2" num="1.2">Une église de Rome a-t-elle pris ton nom ?</l>
						<l n="3" num="1.3">Ah ! dans cette cité païenne et catholique,</l>
						<l n="4" num="1.4">Quand, fatigué de voir et d’admirer toujours,</l>
						<l n="5" num="1.5">Enfin je découvris ton humble basilique,</l>
						<l n="6" num="1.6">Ah ! cirques et forums, colonnades et tours,</l>
						<l n="7" num="1.7">Comme tout disparut ! et, durant quelques jours,</l>
						<l n="8" num="1.8">Mon pays me revint frais et mélancolique.</l>
						<l n="9" num="1.9">Malô, l’illusion fidèle me poursuit :</l>
						<l n="10" num="1.10">Ton bâton pastoral dans Rome me conduit.</l>
					</lg>
					<lg n="2">
						<l n="11" num="2.1">Hier encor j’errais, et maisons, monastères,</l>
						<l n="12" num="2.2">Théâtres, tout dormait ; le Tibre coulait noir,</l>
						<l n="13" num="2.3">Et je suivais ses bords, lorsque, par ce beau soir,</l>
						<l n="14" num="2.4">Saint-Pierre m’apparut inondé de lumières :</l>
						<l n="15" num="2.5">Avait-on allumé pour mon saint inconnu</l>
						<l n="16" num="2.6">Cette fête magique où seul j’étais venu ?</l>
						<l n="17" num="2.7">Des milliers de flambeaux (grandeurs toutes romaines !)</l>
						<l n="18" num="2.8">Éclairaient sans témoins et le dôme et la nuit,</l>
						<l n="19" num="2.9">Et sous la colonnade on entendait le bruit</l>
						<l n="20" num="2.10"><space unit="char" quantity="12"></space>Des immenses fontaines.</l>
						<l n="21" num="2.11">Éclat du Vatican, luxe pontifical,</l>
						<l n="22" num="2.12">M’écriai-je, ici-bas vous n’avez point d’égal !</l>
						<l n="23" num="2.13">Le ciel allume seul une pareille fête,</l>
						<l n="24" num="2.14">Délices de l’Arabe errant dans les déserts ;</l>
						<l n="25" num="2.15">Immobile et serein, seul, après la tempête,</l>
						<l n="26" num="2.16">Sur l’Océan plaintif il tient ses yeux ouverts,</l>
						<l n="27" num="2.17">Pour apaiser la vague et les grands monstres verts ;</l>
						<l n="28" num="2.18">Malô, de tels flambeaux scintillaient sur ta tête,</l>
						<l n="29" num="2.19">Quand, guidant ton esquif, un ange aux ailes d’or</l>
						<l n="30" num="2.20">T’envoyait convertir les païens de l’Arvor !</l>
					</lg>
					<lg n="3">
						<l n="31" num="3.1">Patron des voyageurs, les fils de ton rivage,</l>
						<l n="32" num="3.2">Venus à ce milieu de l’univers chrétien,</l>
						<l n="33" num="3.3">Connaîtront désormais ton nom italien,</l>
						<l n="34" num="3.4">Et tu seras un but dans leur pèlerinage.</l>
						<l n="35" num="3.5">Les plus tendres de cœur à Rome apporteront</l>
						<l n="36" num="3.6">Quelques fleurs des landiers pour réjouir ton front :</l>
						<l n="37" num="3.7">Mais là-bas, près des mers, sous ta sombre chapelle,</l>
						<l n="38" num="3.8">Fête-les au retour, bon saint, et souris-leur</l>
						<l n="39" num="3.9">Quand sur ton humble autel ils mettront une fleur</l>
						<l n="40" num="3.10"><space unit="char" quantity="12"></space>De la ville éternelle.</l>
					</lg>
				</div></body></text></TEI>