<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FLEUR D’OR</title>
				<title type="medium">Une édition électronique</title>
				<author key="BRI">
					<name>
						<forename>Auguste</forename>
						<surname>BRIZEUX</surname>
					</name>
					<date from="1803" to="1858">1803-1858</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2992 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BRI_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA FLEUR D’OR</title>
						<author>Auguste Brizeux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">https://fr.wikisource.org/wiki/La_Fleur_d%E2%80%99Or</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Œuvres de Auguste Brizeux — LA FLEUR D’OR</title>
								<author>Auguste Brizeux</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Alphonse Lemerre, éditeur</publisher>
									<date when="1981">1981</date>
								</imprint>
								<biblScope unit="tome">3</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1874">1874</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les deux préfaces ne sont pas reprises.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-02-05" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-02-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LIVRE SIXIÈME</head><head type="sub_part">A NAPLE</head><div type="poem" key="BRI114">
					<head type="main">Morgana*</head>
					<opener>
						<salute>À Hersart de la Villemarqué</salute>
					</opener>
					<div type="section" n="1">
						<head type="main">UN PÂTRE.</head>
						<lg n="1">
							<l n="1" num="1.1">DEBOUT, mes bons seigneurs ! c’est assez pour Morphée.</l>
							<l n="2" num="1.2"><space unit="char" quantity="8"></space>Allons voir Morgana la fée,</l>
							<l n="3" num="1.3">Sur un char de vapeurs avec l’aube arrivée.</l>
						</lg>
						<lg n="2">
							<l n="4" num="2.1"><space unit="char" quantity="8"></space>Chaque été, prenant son essor.</l>
							<l n="5" num="2.2">Légère, elle s’en vient des brumes de l’Arvor</l>
							<l n="6" num="2.3"><space unit="char" quantity="8"></space>Bâtir ici ses palais d’or.</l>
						</lg>
						<lg n="3">
							<l n="7" num="3.1">Au pâtre de Reggio si vous tardez à croire,</l>
							<l n="8" num="3.2"><space unit="char" quantity="8"></space>Gravissons le haut promontoire :</l>
							<l n="9" num="3.3">Là nous verrons la fée et dans toute sa gloire.</l>
						</lg>
						<lg n="4">
							<l n="10" num="4.1"><space unit="char" quantity="8"></space>Que de monde ! ouvrez bien les yeux :</l>
							<l n="11" num="4.2">Le prodige veut naître, et déjà des flots bleus</l>
							<l n="12" num="4.3"><space unit="char" quantity="8"></space>S’étend le miroir onduleux.</l>
						</lg>
						<lg n="5">
							<l n="13" num="5.1">Place au pâle étranger ! Car peut-être Morgane</l>
							<l n="14" num="5.2"><space unit="char" quantity="8"></space>(Comme au pasteur notre Diane)</l>
							<l n="15" num="5.3">Un soir lui dévoila sa beauté diaphane.</l>
						</lg>
					</div>
					<div type="section" n="2">
						<head type="number">UN VOYAGEUR.</head>
						<lg n="1">
							<l n="16" num="1.1"><space unit="char" quantity="8"></space>Non ! — Pourtant d’aïeul en aïeul,</l>
							<l n="17" num="1.2">Comme un saint talisman que l’aîné portait seul,</l>
							<l n="18" num="1.3"><space unit="char" quantity="8"></space>Mon nom me faisait son filleul.</l>
						</lg>
						<lg n="2">
							<l n="19" num="2.1">Enfant, j’errai longtemps aux féeriques royaumes,</l>
							<l n="20" num="2.2"><space unit="char" quantity="8"></space>M’enivrant de couleurs, d’arômes :</l>
							<l n="21" num="2.3">Hélas ! je suis encore un chasseur de fantômes !</l>
						</lg>
						<lg n="3">
							<l n="22" num="3.1"><space unit="char" quantity="8"></space>Oh ! le caprice est mon vainqueur.</l>
							<l n="23" num="3.2">Sujet d’un bon Génie ou d’un Esprit moqueur,</l>
							<l n="24" num="3.3"><space unit="char" quantity="8"></space>Je cède aux rêves de mon cœur.</l>
						</lg>
					</div>
					<div type="section" n="3">
						<head type="number">LE PÂTRE.</head>
						<lg n="1">
							<l n="25" num="1.1">Regardez ! regardez ! docte magicienne,</l>
							<l n="26" num="1.2"><space unit="char" quantity="8"></space>Sur la vague sicilienne,</l>
							<l n="27" num="1.3">La fée a commencé son œuvre aérienne.</l>
						</lg>
						<lg n="2">
							<l n="28" num="2.1"><space unit="char" quantity="8"></space>Ah ! voyez sous les doigts divins</l>
							<l n="29" num="2.2">S’entasser les coteaux sillonnés de ravins…</l>
							<l n="30" num="2.3"><space unit="char" quantity="8"></space>J’entends frissonner les sapins !</l>
						</lg>
					</div>
					<div type="section" n="4">
						<head type="number">UN ARTISTE.</head>
						<lg n="1">
							<l n="31" num="1.1">L’amour grossier des champs, ô pâtre, te fascine !</l>
							<l n="32" num="1.2"><space unit="char" quantity="8"></space>Œuvre de Morgane ou d’Alcine,</l>
							<l n="33" num="1.3">Cet amas de châteaux splendides, c’est Messine.</l>
						</lg>
					</div>
					<div type="section" n="5">
						<head type="number">UN VOYAGEUR.</head>
						<lg n="1">
							<l n="34" num="1.1"><space unit="char" quantity="8"></space>Moi, je vous dis : c’est Bod-cador !</l>
							<l n="35" num="1.2">Val qu’Arthur emplissait des appels de son cor,</l>
							<l n="36" num="1.3"><space unit="char" quantity="8"></space>Où dans la nuit il chasse encor.</l>
						</lg>
						<lg n="2">
							<l n="37" num="2.1">C’est la tour de Léon, c’est un pic de Cornouailles,</l>
							<l n="38" num="2.2"><space unit="char" quantity="8"></space>Elven couronné de broussailles :</l>
							<l n="39" num="2.3">Mon cœur, voici Carnac, le champ des funérailles !</l>
						</lg>
						<lg n="3">
							<l n="40" num="3.1"><space unit="char" quantity="8"></space>O bonne fée, à mon retour.</l>
							<l n="41" num="3.2">Sur nos grèves à toi, dès le réveil du jour,</l>
							<l n="42" num="3.3"><space unit="char" quantity="8"></space>Une belle chanson d’amour !</l>
						</lg>
						<lg n="4">
							<l n="43" num="4.1">Pour tes fils d’Occident, ô toi qui recomposes</l>
							<l n="44" num="4.2"><space unit="char" quantity="8"></space>Un pays dans les vapeurs roses.</l>
							<l n="45" num="4.3">Et sous l’ardent Midi charmes leurs cœurs moroses !</l>
						</lg>
						<ab type="dash">────────</ab>
						<lg n="5">
							<l n="46" num="5.1"><space unit="char" quantity="8"></space>Courbé par ses réflexions.</l>
							<l n="47" num="5.2">Un savant écoutait : « Ah ! dit-il, épargnons</l>
							<l n="48" num="5.3"><space unit="char" quantity="8"></space>Leur beau miroir d’illusions ! »</l>
						</lg>
					</div>
					<closer>
						<note type="footnote" id="*">
							Mor-gana, <hi rend="ital">Fille de la mer</hi> — C’est à cette fée armoricaine que le peuple
							attribue, en Calabre, le curieux phénomène de réfraction qui se voit souvent dans
							le détroit de Messine. Les côtes de la Sicile viennent se réfléchir dans la mer
							comme un miroir, mais un miroir féerique où l’imagination sait trouver ce qu’elle
							désire.
						</note>
					</closer>
				</div></body></text></TEI>