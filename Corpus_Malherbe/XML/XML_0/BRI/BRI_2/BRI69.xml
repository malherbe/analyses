<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FLEUR D’OR</title>
				<title type="medium">Une édition électronique</title>
				<author key="BRI">
					<name>
						<forename>Auguste</forename>
						<surname>BRIZEUX</surname>
					</name>
					<date from="1803" to="1858">1803-1858</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2992 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BRI_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA FLEUR D’OR</title>
						<author>Auguste Brizeux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">https://fr.wikisource.org/wiki/La_Fleur_d%E2%80%99Or</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Œuvres de Auguste Brizeux — LA FLEUR D’OR</title>
								<author>Auguste Brizeux</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Alphonse Lemerre, éditeur</publisher>
									<date when="1981">1981</date>
								</imprint>
								<biblScope unit="tome">3</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1874">1874</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les deux préfaces ne sont pas reprises.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-02-05" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-02-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LIVRE DEUXIÈME</head><head type="sub_part">A PARIS</head><div type="poem" key="BRI69">
					<head type="main">À Lucy</head>
					<lg n="1">
						<l n="1" num="1.1">LUCY, depuis un temps, lors même qu’on te loue,</l>
						<l n="2" num="1.2">Une rougeur soudaine éclate sur ta joue,</l>
						<l n="3" num="1.3">Ta voix hésite et tremble, et tes regards troublés</l>
						<l n="4" num="1.4">S’éteignent à travers tes cils longs et mouillés :</l>
						<l n="5" num="1.5">Quand ton âme est sans tache, oh ! pourquoi cette honte,</l>
						<l n="6" num="1.6">Et sur ton front si blanc cette rougeur qui monte ?</l>
						<l n="7" num="1.7">Enfant, ah ! pauvre enfant en proie au ver rongeur !</l>
						<l n="8" num="1.8">Cette hydre dévorante et qu’on nomme rougeur.</l>
						<l n="9" num="1.9">Je la connais ! Deux ans, jeune et l’âme encor pure.</l>
						<l n="10" num="1.10">Grandissant comme toi, j’ai senti sa morsure,</l>
						<l n="11" num="1.11">Et son souffle de feu, vif et subtil poison,</l>
						<l n="12" num="1.12">Courir par tous mes sens et troubler ma raison.</l>
						<l n="13" num="1.13">N’est-ce pas ? Dans le cœur c’est comme une hydre affreuse,</l>
						<l n="14" num="1.14">Qui sans cesse y retord ses anneaux et le creuse,</l>
						<l n="15" num="1.15">Et jamais ne sommeille, et cherche à s’élancer</l>
						<l n="16" num="1.16">Dès qu’un œil attentif sur vous vient se fixer ;</l>
						<l n="17" num="1.17">La flamme de son corps vous consume au passage,</l>
						<l n="18" num="1.18">Elle sort par vos yeux, luit sur votre visage,</l>
						<l n="19" num="1.19">L’air manque, tous vos sens sont domptés à la fois,</l>
						<l n="20" num="1.20">Et vous restez sans pouls, sans regards et sans voix !…</l>
						<l n="21" num="1.21">O tourment de l’enfer, honte, éternel supplice</l>
						<l n="22" num="1.22">Qui marque la vertu de la couleur du vice,</l>
						<l n="23" num="1.23">À la tendre innocence ôte son doux repos,</l>
						<l n="24" num="1.24">Et son rire si frais, et tous ses gais propos !…</l>
						<l n="25" num="1.25">Prends courage pourtant, ô blonde enfant qu’on aime.</l>
						<l n="26" num="1.26">Lasse de sa victime, un jour et d’elle-même</l>
						<l n="27" num="1.27">La rougeur s’en ira ; mais alors dans ton cœur</l>
						<l n="28" num="1.28">Avec son trouble aimable entrera la pudeur.</l>
					</lg>
				</div></body></text></TEI>