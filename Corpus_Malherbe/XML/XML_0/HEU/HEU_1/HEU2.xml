<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">L’INITIATION DOULOUREUSE</title>
				<title type="sub">1er cahier</title>
				<title type="medium">Édition électronique</title>
				<author key="HEU">
					<name>
						<forename>Gaston</forename>
						<surname>HEUX</surname>
					</name>
					<date from="1879" to="1950">1879-1950</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2512 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2016</date>
				<idno type="local">HEU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">L’Initiation douloureuse — 1er cahier</title>
						<author>Gaston Heux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k141655w/f1n269.texteBrut</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L’Initiation douloureuse — 1er cahier</title>
								<author>Gaston Heux</author>
								<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k10575703.r=%22max%20elskamp%22 ?rk=42918 ;4</idno>
								<imprint>
									<pubPlace>Paris — Bruxelles</pubPlace>
									<publisher>Éditions Gauloises</publisher>
									<date when="1924">1924</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1924">1924</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les tirets simples ont été remplacés par des tirets demi-cadratins.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Il reste probablement de nombreuses erreurs de numérisation indétectables par le correcteur orthographique ; exemple : lu pour tu.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">Le livre puéril</head><head type="main_subpart">I</head><head type="sub_1">… et le plus ingénu est le plus près des dieux…</head><div type="poem" key="HEU2">
						<opener>
							<salute>Au maître du <hi rend="ital">Chant dans l’Ombre</hi>, <lb></lb>à Fernand Séverin.</salute>
						</opener>
						<head type="main">JEUNE DIEU</head>
						<lg n="1">
							<l n="1" num="1.1">Cet enfant qui s’en vient rêve seul son beau rêve.</l>
							<l n="2" num="1.2">Un Dieu n’a pas encor, doux Créateur d’une Ève,</l>
							<l n="3" num="1.3">Visité son sommeil ni dédoublé sa chair.</l>
							<l n="4" num="1.4">Il dispense entre tous son trésor le plus cher,</l>
							<l n="5" num="1.5">Et son amour pensif d’un parfum baigne l’ombre !…</l>
							<l n="6" num="1.6">Pas un de ses bienfaits qui réduise leur nombre…</l>
							<l n="7" num="1.7">Quels dons auraient leur prix tant qu’il ne s’est donné</l>
							<l n="8" num="1.8">Et tout à ses candeurs d’éternel nouveau-né,</l>
							<l n="9" num="1.9">Il fait sa volupté, grande parmi les grandes,</l>
							<l n="10" num="1.10">D’un geste qui s’ignore et s’épanche en offrandes !</l>
						</lg>
						<lg n="2">
							<l n="11" num="2.1">Le voici dans le jour adorable, tressant</l>
							<l n="12" num="2.2">A ses cheveux légers de l’églantine en sang…</l>
						</lg>
						<lg n="3">
							<l n="13" num="3.1">D’autres ne sont torrents que pour subir leur digue !…</l>
							<l n="14" num="3.2">L’Univers veut combler l’être qui se prodigue…</l>
							<l n="15" num="3.3">Tant d’échos pleins de lui qui se sont cru sa voix !</l>
							<l n="16" num="3.4"> ?Envahissant des sens exaspérés cent fois,</l>
							<l n="17" num="3.5">Lueurs, hymnes, odeurs, versant leur riche essence,</l>
							<l n="18" num="3.6">Fondent la terre jeune à son adolescence.</l>
							<l n="19" num="3.7">Pas de gouffre où l’éclair de sa force n’ait lui…</l>
							<l n="20" num="3.8">Les cieux en longs sillons s’illuminent de lui,</l>
							<l n="21" num="3.9">Et, sûr enfin que chaque aspect le renouvelle,</l>
							<l n="22" num="3.10">Il savoure en son cœur sa vie universelle !…</l>
						</lg>
						<lg n="4">
							<l n="23" num="4.1">Penchée aux coupes d’or, que l’ivresse a de choix !…</l>
						</lg>
						<lg n="5">
							<l n="24" num="5.1">Il vient ! d’amples transports ondulent par les bois,</l>
							<l n="25" num="5.2">Dont l’épaisse fraîcheur caresse d’ombres vertes</l>
							<l n="26" num="5.3">Son doux regard crédule et ses lèvres offertes…</l>
							<l n="27" num="5.4">Les troncs où s’égosille un nid suave et sûr</l>
							<l n="28" num="5.5">En jets mélodieux se hâtent vers l’azur…</l>
							<l n="29" num="5.6">Sur leur cime, d’or pâle et de vert nuancée,</l>
							<l n="30" num="5.7">La nue, au zénith bleu, sommeille balancée…</l>
							<l n="31" num="5.8">Mais l’enfant aperçoit parmi son souvenir</l>
							<l n="32" num="5.9">D’autres hôtes encore y descendre frémir.</l>
							<l n="33" num="5.10">Car il vous suit des yeux au ciel des nuits sublimes,</l>
							<l n="34" num="5.11">Essaims vertigineux jaillis de quels abîmes,</l>
							<l n="35" num="5.12">Étoiles que le rythme épars des belles nuits</l>
							<l n="36" num="5.13">Berce, au faîte des bois, dans la houle des bruits !</l>
							<l n="37" num="5.14">Il partage avec vous son âme fraternelle…</l>
							<l n="38" num="5.15">Sa fougue vous entraîne, autre forme de l’aile,</l>
							<l n="39" num="5.16">Mais la vôtre est de flamme, astres, et vous volez !</l>
							<l n="40" num="5.17">De branche en branche, en tourbillons, étincelez !</l>
							<l n="41" num="5.18">A peine appuyez-vous votre élan qui rayonne,</l>
							<l n="42" num="5.19">Qu’un neuf essor déjà dans votre être frissonne…</l>
							<l n="43" num="5.20">Vous laissez aux rameaux vos palpitations,</l>
							<l n="44" num="5.21">O divins oiseaux d’or des constellations !…</l>
						</lg>
						<lg n="6">
							<l n="45" num="6.1">Tel, au-delà de soi, son instinct le prolonge…</l>
							<l n="46" num="6.2">Il crée un monde entier dans l’instant qu’il le songe,</l>
							<l n="47" num="6.3">Et c’est un Dieu ! et son vouloir torrentiel</l>
							<l n="48" num="6.4">Jailli d’un cœur divin, roule encore du ciel…</l>
							<l n="49" num="6.5">Il est onde et bondit, flamme, crépite et brille…</l>
							<l n="50" num="6.6">Inépuisablement sa vigueur s’éparpille,</l>
							<l n="51" num="6.7">Et tel, des vasques d’or de son illusion,</l>
							<l n="52" num="6.8">Descend le long ruisseau de la Création…</l>
						</lg>
					</div></body></text></TEI>