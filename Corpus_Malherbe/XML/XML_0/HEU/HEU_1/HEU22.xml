<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">L’INITIATION DOULOUREUSE</title>
				<title type="sub">1er cahier</title>
				<title type="medium">Édition électronique</title>
				<author key="HEU">
					<name>
						<forename>Gaston</forename>
						<surname>HEUX</surname>
					</name>
					<date from="1879" to="1950">1879-1950</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2512 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2016</date>
				<idno type="local">HEU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">L’Initiation douloureuse — 1er cahier</title>
						<author>Gaston Heux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k141655w/f1n269.texteBrut</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L’Initiation douloureuse — 1er cahier</title>
								<author>Gaston Heux</author>
								<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k10575703.r=%22max%20elskamp%22 ?rk=42918 ;4</idno>
								<imprint>
									<pubPlace>Paris — Bruxelles</pubPlace>
									<publisher>Éditions Gauloises</publisher>
									<date when="1924">1924</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1924">1924</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les tirets simples ont été remplacés par des tirets demi-cadratins.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Il reste probablement de nombreuses erreurs de numérisation indétectables par le correcteur orthographique ; exemple : lu pour tu.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">Le livre viril</head><head type="main_subpart">II</head><head type="main_subpart">Le doute croissant</head><head type="sub_1">… de la chair périssable à l’effeuillement de l’âme</head><div type="poem" key="HEU22">
						<head type="main">FÊTE NOSTALGIQUE</head>
						<div type="section" n="1">
							<head type="number">I</head>
							<head type="main">Jet d’eau</head>
							<lg n="1">
								<l n="1" num="1.1">Au cœur stérile de la vasque,</l>
								<l n="2" num="1.2">Merveille d’un clos écarté,</l>
								<l n="3" num="1.3">Un grand lys d’onde et de clarté</l>
								<l n="4" num="1.4">Improvise son jet fantasque</l>
							</lg>
							<lg n="2">
								<l n="5" num="2.1">Tandis qu’il hausse par les airs</l>
								<l n="6" num="2.2">Sa transparence essentielle,</l>
								<l n="7" num="2.3">Tant de ciel s’éparpille en elle</l>
								<l n="8" num="2.4">Qu’on s’en explique les éclairs !</l>
							</lg>
							<lg n="3">
								<l n="9" num="3.1">Le vent mouillé qui l’évapore</l>
								<l n="10" num="3.2">Répand son bruit comme un parfum</l>
								<l n="11" num="3.3">Et dispersant un vague embrun,</l>
								<l n="12" num="3.4">Prodigue ce pollen sonore.</l>
							</lg>
							<lg n="4">
								<l n="13" num="4.1">La grâce aride du jardin</l>
								<l n="14" num="4.2">S’humecte à sa poussière osée ;</l>
								<l n="15" num="4.3">Cœurs secrets sevrés de rosée,</l>
								<l n="16" num="4.4">Les fleurs s’y fécondent soudain…</l>
							</lg>
							<lg n="5">
								<l n="17" num="5.1">Toute sveltesse s’en inspire…</l>
								<l n="18" num="5.2">Tout élan refait son essor.</l>
								<l n="19" num="5.3">Et l’enchantement du décor</l>
								<l n="20" num="5.4">Se glisse en riant dans son rire.</l>
							</lg>
							<lg n="6">
								<l n="21" num="6.1">—Ah ! jet d’eau de trouble conseil</l>
								<l n="22" num="6.2">Qui me veux épars dans ta joie,</l>
								<l n="23" num="6.3">Quelqu’un déjà danse et tournoie</l>
								<l n="24" num="6.4">Dans ton mirage de soleil…</l>
							</lg>
							<lg n="7">
								<l n="25" num="7.1">Prompte et nerveuse court la sphère</l>
								<l n="26" num="7.2">Tendue à ta cime ou plongeant,</l>
								<l n="27" num="7.3">Court la cible de faux argent</l>
								<l n="28" num="7.4">Qu’un plomb fatal prouve de verre.</l>
							</lg>
							<lg n="8">
								<l n="29" num="8.1">Et l’esprit qu’un charme abusa,</l>
								<l n="30" num="8.2">Rentré dans sa mélancolie,</l>
								<l n="31" num="8.3">Entoure de fous sa folie</l>
								<l n="32" num="8.4">Et fait signe à Gastibelza !</l>
							</lg>
							<lg n="9">
								<l n="33" num="9.1">Je revis ton sens nostalgique</l>
								<l n="34" num="9.2">Au faîte d’un rêve trop beau,</l>
								<l n="35" num="9.3">Où mon être, — pour quel jet d’eau ?</l>
								<l n="36" num="9.4">Figure la cible tragique…</l>
							</lg>
							<lg n="10">
								<l n="37" num="10.1">Ainsi dans l’oubli de son sort,</l>
								<l n="38" num="10.2">(Des yeux vides béant sur elle)</l>
								<l n="39" num="10.3">Ma chair dans mon âme immortelle</l>
								<l n="40" num="10.4">Tente l’adresse de la Mort !</l>
							</lg>
						</div>
						<div type="section" n="2">
							<head type="number">II</head>
							<head type="main">Effeuillements</head>
							<lg n="1">
								<l n="41" num="1.1">Au parc intime dont les roses</l>
								<l n="42" num="1.2">Effeuillent, autour d’un bassin,</l>
								<l n="43" num="1.3">Leurs grâces de fleurs sur les poses</l>
								<l n="44" num="1.4">Qu’exaspère un Triton d’airain,</l>
							</lg>
							<lg n="2">
								<l n="45" num="2.1">Mon rêve attardé se parfume,</l>
								<l n="46" num="2.2">Au bord d’un océan de fleurs</l>
								<l n="47" num="2.3">Qui, comme un gouffre son écume,</l>
								<l n="48" num="2.4">Lui jette l’embrun des senteurs…</l>
							</lg>
							<lg n="3">
								<l n="49" num="3.1">C’est un parc autour de mes doutes,</l>
								<l n="50" num="3.2">Un clos d’amour et de soleil</l>
								<l n="51" num="3.3">Où mon cœur, pourtant aux écoutes,</l>
								<l n="52" num="3.4">Se refuse au tendre conseil…</l>
							</lg>
							<lg n="4">
								<l n="53" num="4.1">Oui ! feuilles de l’arbre sonore,</l>
								<l n="54" num="4.2">Dont tant de sens charge le bruit,</l>
								<l n="55" num="4.3">Vous qui redites dans l’aurore</l>
								<l n="56" num="4.4">Tant d’aveux appris de la nuit,</l>
							</lg>
							<lg n="5">
								<l n="57" num="5.1">Une secrète et chaste idylle</l>
								<l n="58" num="5.2">Entre des fantômes d’amants,</l>
								<l n="59" num="5.3">Échange, grave et puérile,</l>
								<l n="60" num="5.4">L’éternité dans leurs serments.</l>
							</lg>
							<lg n="6">
								<l n="61" num="6.1">Et vos baisers, je le soupçonne,</l>
								<l n="62" num="6.2">Lèvres d’or prêtes à pâmer,</l>
								<l n="63" num="6.3">O feuilles au seuil de l’automne,</l>
								<l n="64" num="6.4">M’intiment des ordres d’aimer !</l>
							</lg>
							<lg n="7">
								<l n="65" num="7.1">Mais, trahi des roses mortelles,</l>
								<l n="66" num="7.2">J’en ai retrouvé dans la Chair…</l>
								<l n="67" num="7.3">L’Esprit seul, au fond des prunelles</l>
								<l n="68" num="7.4">A mes sens, depuis, restait cher</l>
							</lg>
							<lg n="8">
								<l n="69" num="8.1">Déjà la terre nostalgique</l>
								<l n="70" num="8.2">Ne fêtait plus que d’autres yeux…</l>
								<l n="71" num="8.3">Le soir, à l’horizon tragique,</l>
								<l n="72" num="8.4">Me désabuse encor des cieux :…</l>
							</lg>
							<lg n="9">
								<l n="73" num="9.1">Un long effeuillement de flammes,</l>
								<l n="74" num="9.2">Dans le jardin du pur Été,</l>
								<l n="75" num="9.3">Prédit l’effeuillement des âmes</l>
								<l n="76" num="9.4">A qui leur crut l’Éternité !</l>
							</lg>
						</div>
					</div></body></text></TEI>