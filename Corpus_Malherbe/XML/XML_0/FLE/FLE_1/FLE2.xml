<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Friperies</title>
				<title type="medium">Une édition électronique</title>
				<author key="FLE">
					<name>
						<forename>Fernand</forename>
						<surname>FLEURET</surname>
					</name>
					<date from="1883" to="1945">1883-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>455 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">FLE_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Friperies</title>
						<author>Fernand Fleuret</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/friperies00fleu</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Friperies</title>
								<author>Fernand Fleuret</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>CHEZ EUGÈNE REY, LIBRAIRE</publisher>
									<date when="1907">1907</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			</fileDesc>
		<profileDesc>
			<creation>
				<date when="1907">1907</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Cette édition électronique ne comprend pas la préface de l’auteur.</p>
				<p>L’édition qui est à l’origine de la version électronique n’est pas indiquée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-08-30" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2019-08-30" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="FLE2">
				<head type="main">UN SOIR</head>
				<opener>
					<epigraph>
						<cit>
							<quote>« …la bonté qui s’en allait de ces choses… » </quote>
							<bibl>
								<name>P. V.</name>
							</bibl>
						</cit>
					</epigraph>
				</opener>
				<lg n="1">
					<l n="1" num="1.1">La fanfare des cors rend son âme légère</l>
					<l n="2" num="1.2">Et le soir se recueille en l’Église des bois.</l>
					<l n="3" num="1.3">Car au ciel qui se fane, à d’invisibles doigts,</l>
					<l n="4" num="1.4"><space unit="char" quantity="12"></space>Tremble l’hostie lunaire.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">— Seigneur, Dieu du Silence auguste et de la Nuit,</l>
					<l n="6" num="2.2">Sanctifiez les fleurs qui meurent embaumées.</l>
					<l n="7" num="2.3">Et les vieilles maisons, expirantes aussi.</l>
					<l n="8" num="2.4"><space unit="char" quantity="12"></space>Qui râlent leurs fumées !</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Seigneur, acceptez l’âme humide de sanglots</l>
					<l n="10" num="3.2">Des grands parcs éplorés et des forêts d’automne ;</l>
					<l n="11" num="3.3">Seigneur, bénissez la louange monotone</l>
					<l n="12" num="3.4"><space unit="char" quantity="12"></space>Qui monte des jets d’eau !</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">S’il est, par ce beau soir, une humble destinée</l>
					<l n="14" num="4.2">Qu’il vous faille choisir pour en orner les cieux.</l>
					<l n="15" num="4.3">Au moins qu’indolemment la mort lui soit donnée</l>
					<l n="16" num="4.4"><space unit="char" quantity="12"></space>En souffle sur les yeux ;</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1">Que son cœur ait la paix de l’abside fermée</l>
					<l n="18" num="5.2">Qui vous priait encor tantôt, depuis mille ans ;</l>
					<l n="19" num="5.3">Et que le sol lui soit moins lourd que les buées</l>
					<l n="20" num="5.4"><space unit="char" quantity="12"></space>Que voici sur les champs !</l>
				</lg>
			</div></body></text></TEI>