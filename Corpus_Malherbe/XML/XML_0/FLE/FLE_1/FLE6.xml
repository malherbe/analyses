<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Friperies</title>
				<title type="medium">Une édition électronique</title>
				<author key="FLE">
					<name>
						<forename>Fernand</forename>
						<surname>FLEURET</surname>
					</name>
					<date from="1883" to="1945">1883-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>455 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">FLE_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Friperies</title>
						<author>Fernand Fleuret</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/friperies00fleu</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Friperies</title>
								<author>Fernand Fleuret</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>CHEZ EUGÈNE REY, LIBRAIRE</publisher>
									<date when="1907">1907</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			</fileDesc>
		<profileDesc>
			<creation>
				<date when="1907">1907</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Cette édition électronique ne comprend pas la préface de l’auteur.</p>
				<p>L’édition qui est à l’origine de la version électronique n’est pas indiquée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-08-30" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2019-08-30" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="FLE6">
				<head type="main">AMOUR DU PASSÉ LÉGENDAIRE</head>
				<opener>
					<epigraph>
						<cit>
							<quote>
								« …N’as-lu pas désiré, ma sœur au regard de<lb></lb>
								jadis, qu’en un de mes poèmes apparussent ces<lb></lb>
								mots « la grâce des choses fanées ?… »				
							</quote>
							<bibl>
								<name>Stéphane Mallarmé.</name>
							</bibl>
						</cit>
						<cit>
							<quote>
								« Je devine à travers un murmure,<lb></lb>
								Le conteur subtil des voix anciennes… » 	
							</quote>
							<bibl>
								<name>Paul Verlaine.</name>
							</bibl>
						</cit>
					</epigraph>
				</opener>
				<lg n="1">
					<l n="1" num="1.1">Les images qu’on haïrait pour maladroites</l>
					<l n="2" num="1.2">Avec leur gaucherie populaire et précise.</l>
					<l n="3" num="1.3">Si l’on n’avait la foi, du temps des almanachs,</l>
					<l n="4" num="1.4">Au bon Dieu bleu de Prusse en robe de prélat ;</l>
					<l n="5" num="1.5">Puis les vieilles photos et leurs modes massives !</l>
					<l n="6" num="1.6">— Les images qu’on haïrait pour maladroites…</l>
				</lg>
				<lg n="2">
					<l n="7" num="2.1">Les objets, où revit l’esprit qui les conçut.</l>
					<l n="8" num="2.2">Qu’embue encor l’ombre des mains qui les palpèrent.</l>
					<l n="9" num="2.3">Où se pose un tépide et jaloux faix de doigts</l>
					<l n="10" num="2.4">Pour parer des baisers insidieux et froids</l>
					<l n="11" num="2.5">Qu’aime à plaquer tant méchamment la nuit lunaire…</l>
					<l n="12" num="2.6">— Les objets, où revit l’esprit qui les conçut…</l>
				</lg>
				<lg n="3">
					<l n="13" num="3.1">Ce monde, dont la cendre est du fard ! et les cloches</l>
					<l n="14" num="3.2">Nostalgiques des crinolines, qui m’appellent</l>
					<l n="15" num="3.3">Avec d’amples rumeurs de soie et de satin,</l>
					<l n="16" num="3.4">En des pays d’hortensias et boulingrins</l>
					<l n="17" num="3.5">Et de roucoulements lascifs de tourterelles !</l>
					<l n="18" num="3.6">— Ce monde, dont la cendre est du fard ! et ces cloches.</l>
				</lg>
				<lg n="4">
					<l n="19" num="4.1">Amour de tout cela. Passé ! et des maisons</l>
					<l n="20" num="4.2">Où la mort vient flairer avec bruit sous les portes</l>
					<l n="21" num="4.3">Si rien ne fut trahi de son dû ancien.</l>
					<l n="22" num="4.4">Et grogne à la chanson des gonds musiciens</l>
					<l n="23" num="4.5">Qui saluent les cent ans des remugles qui sortent !</l>
					<l n="24" num="4.6">Amour du Passé légendaire et des maisons…</l>
				</lg>
			</div></body></text></TEI>