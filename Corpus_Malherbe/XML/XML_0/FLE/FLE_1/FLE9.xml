<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Friperies</title>
				<title type="medium">Une édition électronique</title>
				<author key="FLE">
					<name>
						<forename>Fernand</forename>
						<surname>FLEURET</surname>
					</name>
					<date from="1883" to="1945">1883-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>455 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">FLE_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Friperies</title>
						<author>Fernand Fleuret</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/friperies00fleu</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Friperies</title>
								<author>Fernand Fleuret</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>CHEZ EUGÈNE REY, LIBRAIRE</publisher>
									<date when="1907">1907</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			</fileDesc>
		<profileDesc>
			<creation>
				<date when="1907">1907</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Cette édition électronique ne comprend pas la préface de l’auteur.</p>
				<p>L’édition qui est à l’origine de la version électronique n’est pas indiquée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-08-30" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2019-08-30" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="FLE9">
				<head type="main">FIANÇAILLES</head>
				<opener>
					<epigraph>
						<cit>
							<quote>
								« J’aimerais que la rose<lb></lb>
								Tût encore au rosier !… » 
							</quote>
							<bibl>
								(Vieille chanson.)
							</bibl>
						</cit>
					</epigraph>
				</opener>
				<lg n="1">
					<l n="1" num="1.1">J’attends tes Pas, J’attends ta Voix… Tu serais telle</l>
					<l n="2" num="1.2">Qu’en mes songes quelque belle Image effacée ;</l>
					<l n="3" num="1.3">Tu viendrais d’un Pays qu’on nommerait <hi rend="ital">Passé</hi>,</l>
					<l n="4" num="1.4">Et tu m’apporterais un peu de ses nouvelles</l>
					<l n="5" num="1.5">Entre tes vieux cartons et tes vieux almanachs.</l>
				</lg>
				<lg n="2">
					<l n="6" num="2.1">Tu comblerais ma chambre avec ta crinoline…</l>
				</lg>
				<lg n="3">
					<l n="7" num="3.1">Comme tes pâles sœurs, tu lirais Lamartine</l>
					<l n="8" num="3.2">Dans un keepsake avec ton nom dessus : Anna,</l>
					<l n="9" num="3.3">Anaïs, ou quelque autre, usé par les années…</l>
					<l n="10" num="3.4">Mais, qu’importe ton nom ? Je ne le saurais pas !</l>
					<l n="11" num="3.5">Tu serais l’Inconnue Étrange, de Là-Bas,</l>
					<l n="12" num="3.6">Mon beau Chagrin, vêtu de robes surannées…</l>
					<l n="13" num="3.7">Douce et chère à mes chers bibelots d’autrefois,</l>
					<l n="14" num="3.8">Dont l’éclair défaillant à luire s’exténue.</l>
					<l n="15" num="3.9">Tu voudrais, pour guérir leurs grâces morfondues.</l>
					<l n="16" num="3.10">Passer l’ennui doré des bagues à tes doigts !</l>
					<l n="17" num="3.11">Et tu saurais ces airs qui filent des quenouilles.</l>
					<l n="18" num="3.12">Ces vieux airs en sabots qui savaient tant bercer</l>
					<l n="19" num="3.13">Et que l’on retrouvait, quand on était blessé,</l>
					<l n="20" num="3.14">A son chevet, comme un ami qui s’agenouille…</l>
					<l n="21" num="3.15">Quand Janvier te verrait regretter les glaïeuls</l>
					<l n="22" num="3.16">Que dresse aux carreaux bleus une tige d’osier.</l>
					<l n="23" num="3.17">Tu me dirais ces airs que chantait ton aïeule :</l>
					<l n="24" num="3.18"><hi rend="ital">J’aimerais que la rose encor fût au rosier…</hi></l>
				</lg>
				<lg n="4">
					<l n="25" num="4.1">J’attends tes Pas, ta Voix, qui viendront en berline</l>
					<l n="26" num="4.2">J’attends tes Yeux pâlis comme un ruban passé ;</l>
					<l n="27" num="4.3">J’attends ce goût de cendre au fard de ton baiser :</l>
				</lg>
				<lg n="5">
					<l n="28" num="5.1">J’attends tes Pas, tes Yeux, ta Voix, ta Crinoline..</l>
				</lg>
			</div></body></text></TEI>