<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">BOUQUETS ET PRIÈRES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3255 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Bouquets et Prières</title>
						<author>Marceline Desbordes-Valmore</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k5510985p/f1n316.texteBrut</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Bouquets et Prières</title>
								<author>Marceline Desbordes-Valmore</author>
								<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k5510985p.r=bouquets+et+pri%C3%A8res.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Dumont</publisher>
									<date when="1843">1843</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>3255 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1843">1843</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface de l’auteur n’a pas été intégrée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
			</listChange>
		</revisionDesc>




	</teiHeader><text><body><div type="poem" key="DES294">
				<head type="main">MOI JE LE SAIS</head>
				<opener>
					<salute>À mademoiselle Louise Grombach.</salute>
				</opener>
				<lg n="1">
					<l n="1" num="1.1">Vous le saurez, la vie a des abîmes</l>
					<l n="2" num="1.2">Cachés au loin sous d’innombrables fleurs :</l>
					<l n="3" num="1.3">Les rossignols qui chantent à leurs cimes,</l>
					<l n="4" num="1.4">Où chantent-ils dans la saison des pleurs ?</l>
					<l n="5" num="1.5">Vous le saurez, la vie a des abîmes</l>
					<l n="6" num="1.6">Cachés au loin sous d’innombrables fleurs.</l>
				</lg>
				<lg n="2">
					<l n="7" num="2.1">Oui, la jeunesse est le pays des larmes ;</l>
					<l n="8" num="2.2">Moi je le sais : j’en viens. Je pleure encor,</l>
					<l n="9" num="2.3">Le front vibrant de ses feux, de ses charmes ;</l>
					<l n="10" num="2.4">Le cœur brisé de son dernier accord !</l>
					<l n="11" num="2.5">Oui, la jeunesse est le pays des larmes.</l>
					<l n="12" num="2.6">Moi je le sais : j’en viens. Je pleure encor !</l>
				</lg>
				<lg n="3">
					<l n="13" num="3.1">Lorsqu’on finit d’être jeune, on s’arrête ;</l>
					<l n="14" num="3.2">À tant de jours on veut reprendre un jour :</l>
					<l n="15" num="3.3">Ils sont partis et l’on penche sa tête.</l>
					<l n="16" num="3.4">D’un tel voyage à quand donc le retour ?</l>
					<l n="17" num="3.5">Lorsqu’on finit d’être jeune, on s’arrête ;</l>
					<l n="18" num="3.6">À tant de jours on veut reprendre un jour !</l>
				</lg>
				<lg n="4">
					<l n="19" num="4.1">Souffrant tout bas de ses mille blessures,</l>
					<l n="20" num="4.2">On croit mourir : voyez, on ne meurt pas.</l>
					<l n="21" num="4.3">De tous serpens Dieu guérit les morsures,</l>
					<l n="22" num="4.4">Et le dictame est semé sous nos pas.</l>
					<l n="23" num="4.5">Souffrant tout bas de ses mille blessures,</l>
					<l n="24" num="4.6">On croit mourir : on plie, on ne meurt pas !</l>
				</lg>
				<lg n="5">
					<l n="25" num="5.1">Rappelez-vous ce chant d’une glaneuse,</l>
					<l n="26" num="5.2">Qui s’arrêta pour serrer votre main ;</l>
					<l n="27" num="5.3">Et si du sort l’étoile lumineuse,</l>
					<l n="28" num="5.4">Vous mûrit mieux les épis du chemin,</l>
					<l n="29" num="5.5">Rappelez-vous ce chant d’une glaneuse,</l>
					<l n="30" num="5.6">Qui s’arrêta pour serrer votre main !</l>
				</lg>
			</div></body></text></TEI>