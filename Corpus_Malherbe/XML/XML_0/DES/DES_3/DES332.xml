<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">BOUQUETS ET PRIÈRES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3255 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Bouquets et Prières</title>
						<author>Marceline Desbordes-Valmore</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k5510985p/f1n316.texteBrut</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Bouquets et Prières</title>
								<author>Marceline Desbordes-Valmore</author>
								<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k5510985p.r=bouquets+et+pri%C3%A8res.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Dumont</publisher>
									<date when="1843">1843</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>3255 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1843">1843</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface de l’auteur n’a pas été intégrée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
			</listChange>
		</revisionDesc>




	</teiHeader><text><body><div type="poem" key="DES332">
				<head type="main">FÊTE D’UNE VILLE DE FLANDRE</head>
				<head type="sub_1">POUR PHILIPPE-LE-BON</head>
				<lg n="1">
					<l n="1" num="1.1">C’est demain qu’une ville aimée,</l>
					<l n="2" num="1.2">Aimante et joyeuse, et charmée,</l>
					<l n="3" num="1.3">Tout en fête s’éveillera ;</l>
					<l n="4" num="1.4">C’est demain que fifres et danse,</l>
					<l n="5" num="1.5">Parcourant le sol en cadence,</l>
					<l n="6" num="1.6">Riront au peuple, qui rira !</l>
				</lg>
				<lg n="2">
					<l n="7" num="2.1">De fleurs et de chants couronnée,</l>
					<l n="8" num="2.2">Levez-vous donc, belle journée,</l>
					<l n="9" num="2.3">Pour bénir mon premier séjour :</l>
					<l n="10" num="2.4">Que dans vos heures sans alarme,</l>
					<l n="11" num="2.5">Il ne tombe pas une larme</l>
					<l n="12" num="2.6">Sur la Flandre, ma sainte amour !</l>
				</lg>
				<lg n="3">
					<l n="13" num="3.1">Que nul serpent n’y souille l’herbe ;</l>
					<l n="14" num="3.2">Que l’humble épi s’y lève en gerbe ;</l>
					<l n="15" num="3.3">Comme on le voyait au vieux temps ;</l>
					<l n="16" num="3.4">Que les chaînes en soient bannies,</l>
					<l n="17" num="3.5">Que les mères y soient bénies ;</l>
					<l n="18" num="3.6">Que les pauvres y soient contens !</l>
				</lg>
				<lg n="4">
					<l n="19" num="4.1">Répondez, cloches bondissantes,</l>
					<l n="20" num="4.2">Aux fanfares retentissantes,</l>
					<l n="21" num="4.3">Chantant gloire et patrie en chœur</l>
					<l n="22" num="4.4">Promenez vos belles volées,</l>
					<l n="23" num="4.5">Et de vos hymnes redoublées,</l>
					<l n="24" num="4.6">De ma Flandre égayez le cœur !</l>
				</lg>
				<lg n="5">
					<l n="25" num="5.1">Ainsi que les ondes accrues ;</l>
					<l n="26" num="5.2">Enfans qui remplissez les rues,</l>
					<l n="27" num="5.3">N’est-ce pas que c’est doux à voir ?</l>
					<l n="28" num="5.4">Ouvrez vos yeux et vos oreilles,</l>
					<l n="29" num="5.5">Du jour contemplez les merveilles,</l>
					<l n="30" num="5.6">Pour nous les raconter le soir.</l>
				</lg>
				<lg n="6">
					<l n="31" num="6.1">Entrez, hameau, bourg et village :</l>
					<l n="32" num="6.2">Par ces grands tableaux du vieil âge,</l>
					<l n="33" num="6.3">Vous le voyez, ô bonnes gens,</l>
					<l n="34" num="6.4">Si notre Philippe est encore</l>
					<l n="35" num="6.5">Couronné d’un nom qu’on adore,</l>
					<l n="36" num="6.6">C’est qu’il aimait les indigens !</l>
				</lg>
				<lg n="7">
					<l n="37" num="7.1">Mais d’où vient que mon âme pleure</l>
					<l n="38" num="7.2">Sur le clocher où chante l’heure,</l>
					<l n="39" num="7.3">Et sonne aux autres un beau jour ?</l>
					<l n="40" num="7.4">Non, dans ses fêtes sans alarme,</l>
					<l n="41" num="7.5">Qu’il ne tombe pas une larme</l>
					<l n="42" num="7.6">Sur la Flandre, ma sainte amour !</l>
				</lg>
				<lg n="8">
					<l n="43" num="8.1">Mon père a chanté dans l’espace ;</l>
					<l n="44" num="8.2">Où son ombre a passé, je passe,</l>
					<l n="45" num="8.3">Comme lui priant et chantant :</l>
					<l n="46" num="8.4">L’orgue ainsi lève sa prière,</l>
					<l n="47" num="8.5">Attendrissant la nef entière ;</l>
					<l n="48" num="8.6">L’orgue est triste ; il chante pourtant !</l>
				</lg>
			</div></body></text></TEI>