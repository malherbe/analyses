<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">BOUQUETS ET PRIÈRES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3255 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Bouquets et Prières</title>
						<author>Marceline Desbordes-Valmore</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k5510985p/f1n316.texteBrut</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Bouquets et Prières</title>
								<author>Marceline Desbordes-Valmore</author>
								<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k5510985p.r=bouquets+et+pri%C3%A8res.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Dumont</publisher>
									<date when="1843">1843</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>3255 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1843">1843</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface de l’auteur n’a pas été intégrée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
			</listChange>
		</revisionDesc>




	</teiHeader><text><body><div type="poem" key="DES341">
				<head type="main">LA PREMIÈRE COMMUNION</head>
				<head type="sub">d’INÈS</head>
				<lg n="1">
					<l n="1" num="1.1">Tes yeux noirs, ma fille,</l>
					<l n="2" num="1.2">Sont plus doux ce soir,</l>
					<l n="3" num="1.3">Que l’encens qui brille</l>
					<l n="4" num="1.4">Au saint encensoir !</l>
					<l n="5" num="1.5">Tu sembles un ange,</l>
					<l n="6" num="1.6">Sous son voile encor,</l>
					<l n="7" num="1.7">Qui rêve, et s’arrange</l>
					<l n="8" num="1.8">Pour prendre l’essor.</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1">Jeune âme sauvage,</l>
					<l n="10" num="2.2">Tremblante en mes bras,</l>
					<l n="11" num="2.3">Confie au plus sage</l>
					<l n="12" num="2.4">Tes doux embarras :</l>
					<l n="13" num="2.5">Dans cette belle heure,</l>
					<l n="14" num="2.6">On cause avec Dieu ;</l>
					<l n="15" num="2.7">Va, pour ce qui pleure,</l>
					<l n="16" num="2.8">Lui parler un peu !</l>
				</lg>
				<lg n="3">
					<l n="17" num="3.1">Si l’enfant lui porte</l>
					<l n="18" num="3.2">Trois souhaits en fleurs,</l>
					<l n="19" num="3.3">Il ouvre sa porte</l>
					<l n="20" num="3.4">À ces vœux sans pleurs :</l>
					<l n="21" num="3.5">Pour rêver ces choses,</l>
					<l n="22" num="3.6">Baisse bien les yeux,</l>
					<l n="23" num="3.7">Et laisse tes roses</l>
					<l n="24" num="3.8">S’exhaler aux cieux !</l>
				</lg>
				<lg n="4">
					<l n="25" num="4.1">Pour l’hymne éphémère</l>
					<l n="26" num="4.2">De ta voix d’oiseau,</l>
					<l n="27" num="4.3">Demande à sa mère</l>
					<l n="28" num="4.4">L’appui d’un roseau ;</l>
					<l n="29" num="4.5">Pour tes jeunes ailes</l>
					<l n="30" num="4.6">Un vol sans effroi ;</l>
					<l n="31" num="4.7">Ton soleil pour elles,</l>
					<l n="32" num="4.8">Ton bonheur pour moi !</l>
				</lg>
			</div></body></text></TEI>