<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">BOUQUETS ET PRIÈRES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3255 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Bouquets et Prières</title>
						<author>Marceline Desbordes-Valmore</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k5510985p/f1n316.texteBrut</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Bouquets et Prières</title>
								<author>Marceline Desbordes-Valmore</author>
								<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k5510985p.r=bouquets+et+pri%C3%A8res.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Dumont</publisher>
									<date when="1843">1843</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>3255 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1843">1843</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface de l’auteur n’a pas été intégrée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
			</listChange>
		</revisionDesc>




	</teiHeader><text><body><div type="poem" key="DES320">
				<head type="main">LOUISE DE LA VALLIÈRE</head>
				<head type="sub_1">QUITTANT SA MÈRE</head>
				<head type="form">FRAGMENT DE BULWER</head>
				<lg n="1">
					<l part="I" n="1" num="1.1">Votre dernier bonsoir, ma mère ! </l>
				</lg>
				<lg n="2">
					<head type="main">SA MÈRE</head>
					<l part="F" n="1">Tu chancelles,</l>
					<l n="2" num="2.1">Et je vois sous ton front les pleurs que tu me cèles.</l>
					<l n="3" num="2.2">Pourtant le monde s’ouvre indulgent devant toi,</l>
					<l n="4" num="2.3">Ma fille ! Une humble reine, unie au plus grand roi,</l>
					<l n="5" num="2.4">T’appelle dans sa cour où, disent les poètes,</l>
					<l n="6" num="2.5">La gloire a surpassé, par ses pompeuses fêtes,</l>
					<l part="I" n="7" num="2.6">Tout ce que l’Orient dit à l’histoire. </l>
				</lg>
				<lg n="3">
					<head type="main">LOUISE</head>
					<l part="F" n="7">Hélas !</l>
				</lg>
				<lg n="4">
					<head type="main">SA MÈRE</head>
					<l part="I" n="8" num="4.1">Une cour !… une cour !… </l>
				</lg>
				<lg n="5">
					<head type="main">LOUISE</head>
					<l part="F" n="8">Où vous ne serez pas,</l>
					<l n="9" num="5.1">Ma mère ! Ni ces murs où j’entendais vos pas ;</l>
					<l n="10" num="5.2">Ces murs, vieux raconteurs de mes jeunes années,</l>
					<l n="11" num="5.3">Qui recouvraient en paix deux humbles destinées.</l>
					<l n="12" num="5.4">Ni les champs, ni les bois, où je voyais le jour</l>
					<l n="13" num="5.5">Descendre et se lever, doux comme votre amour.</l>
					<l n="14" num="5.6">Écoutez, c’est l’<hi rend="ital">Ave Maria</hi>… que je l’aime !</l>
					<l n="15" num="5.7">Oh ! puissé-je partout redescendre en moi-même,</l>
					<l n="16" num="5.8">À cette heure du ciel, et retrouver mon cœur</l>
					<l n="17" num="5.9">Aussi plein de ma mère et de mon Créateur !</l>
					<l part="I" n="18" num="5.10">L’Éden d’un pauvre enfant, c’est le berceau. </l>
				</lg>
				<lg n="6">
					<head type="main">SA MÈRE</head>
					<l part="F" n="18">Louise !</l>
					<l n="19" num="6.1">L’Éden, c’est la vertu ; c’est ton âme, promise</l>
					<l n="20" num="6.2">Au désert de mes ans. En traversant les cours,</l>
					<l n="21" num="6.3">Dieu sur ton aile blanche étendra son secours.</l>
					<l n="22" num="6.4">Enfant ! si le respect tient lieu d’expérience,</l>
					<l n="23" num="6.5">Ta mère, n’est-ce pas, sera ta conscience ?</l>
				</lg>
				<lg n="7">
					<head type="main">LOUISE</head>
					<l n="24" num="7.1">Oui, je vous dirai tout, et vous me répondrez</l>
					<l n="25" num="7.2">Chaque soir une ligne, où vous me bénirez ;</l>
					<l n="26" num="7.3">Et vous irez pour moi dans ces pauvres chaumières,</l>
					<l n="27" num="7.4">D’où nous voyons là-bas scintiller les lumières :</l>
					<l n="28" num="7.5">De l’hiver qui s’approche il faut les consoler,</l>
					<l n="29" num="7.6">Et nourrir mes oiseaux, trop faibles pour voler.</l>
				</lg>
				<lg n="8">
					<head type="main">SA MÈRE</head>
					<l n="30" num="8.1">Tu ne me parles pas d’un jeune aigle, ma fille,</l>
					<l n="31" num="8.2">Dès le berceau promis pour chef à ma famille.</l>
					<l n="32" num="8.3">Si j’apprends qu’à la guerre il ne pense qu’à toi,</l>
					<l part="I" n="33" num="8.4">Ne t’en dirai-je rien ? </l>
				</lg>
				<lg n="9">
					<head type="main">LOUISE</head>
					<l part="F" n="33">Il aime tant le roi !</l>
				</lg>
				<lg n="10">
					<head type="main">SA MÈRE</head>
					<l n="34" num="10.1">Et nous sommes déjà si fières de sa gloire,</l>
					<l n="35" num="10.2">Que ses droits sont écrits dans ta jeune mémoire.</l>
				</lg>
				<lg n="11">
					<head type="main">LOUISE</head>
					<l part="I" n="36" num="11.1">Il aime tant le roi ! </l>
				</lg>
				<lg n="12">
					<head type="main">SA MÈRE</head>
					<l part="F" n="36">C’est qu’il l’a déjà vu,</l>
				</lg>
				<lg n="13">
					<head type="main">LOUISE</head>
					<l n="37" num="13.1">Et moi, je vais le voir ! Toujours je l’ai prévu,</l>
					<l part="I" n="38" num="13.2">Oui, c’est un bien grand roi ! </l>
				</lg>
				<lg n="14">
					<head type="main">SA MÈRE</head>
					<l part="F" n="38">Le plus grand de la terre.</l>
					<l part="I" n="39" num="14.1">Splendide dans la paix… </l>
				</lg>
				<lg n="15">
					<head type="main">LOUISE</head>
					<l part="F" n="39">Incroyable mystère !</l>
					<l n="40" num="15.1">Tous mes rêves d’enfant ont été pleins de lui,</l>
					<l n="41" num="15.2">Ma mère, et de le voir je n’ai peur qu’aujourd’hui.</l>
					<l n="42" num="15.3">J’ai toujours deviné ses yeux et son sourire,</l>
					<l n="43" num="15.4">Son front, où la nature avait écrit : Empire !</l>
					<l n="44" num="15.5">Oui, j’ai toujours rêvé que le roi me parlait,</l>
					<l n="45" num="15.6">Et que sa grande voix de bien loin m’appelait :</l>
					<l rhyme="none" n="46" num="15.7"><space quantity="10" unit="char"></space>N’est-ce pas étonnant, ma mère !…</l>
					<ab type="dot">. . . . . . . . . . . . . . . . . . . . . . . . . . . .</ab>
					<ab type="dot">. . . . . . . . . . . . . . . . . . . . . . . . . . . .</ab>
				</lg>
			</div></body></text></TEI>