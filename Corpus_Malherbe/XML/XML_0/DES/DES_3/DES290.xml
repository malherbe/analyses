<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">BOUQUETS ET PRIÈRES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3255 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Bouquets et Prières</title>
						<author>Marceline Desbordes-Valmore</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k5510985p/f1n316.texteBrut</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Bouquets et Prières</title>
								<author>Marceline Desbordes-Valmore</author>
								<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k5510985p.r=bouquets+et+pri%C3%A8res.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Dumont</publisher>
									<date when="1843">1843</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>3255 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1843">1843</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface de l’auteur n’a pas été intégrée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
			</listChange>
		</revisionDesc>




	</teiHeader><text><body><div type="poem" key="DES290">
				<head type="main">LES ENFANS À LA COMMUNION</head>
				<lg n="1">
					<head type="main">UNE VOIX</head>
					<l n="1" num="1.1">Laissez venir à Dieu la grâce et l’innocence :</l>
					<l n="2" num="1.2">Laissez remonter l’âme à sa divine essence !</l>
				</lg>
				<lg n="2">
					<head type="main">LES ENFANS</head>
					<l n="3" num="2.1">Nous venons ! nous venons, Maître doux et divin,</l>
					<l n="4" num="2.2">Comme l’agneau sans fiel et le pain sans levain,</l>
					<l n="5" num="2.3">Nous venons, l’âme en fleur, vous chercher à l’église :</l>
					<l n="6" num="2.4">Sous votre long manteau sauvez-nous de la bise.</l>
					<l n="7" num="2.5">On nous a dit, Seigneur, que vous étiez ici,</l>
					<l n="8" num="2.6">Et que vous demandez les enfans : nous voici.</l>
				</lg>
				<lg n="3">
					<head type="main">UNE VOIX</head>
					<l n="9" num="3.1">Laissez, laissez passer la grâce et l’innocence,</l>
					<l n="10" num="3.2">Laissez remonter l’âme à sa divine essence.</l>
				</lg>
				<lg n="4">
					<head type="main">UNE FEMME</head>
					<l n="11" num="4.1">Oh ! que ces voix d’enfans font de mal et de bien !</l>
					<l n="12" num="4.2">De leur Dieu sans colère ils ne redoutent rien.</l>
					<l n="13" num="4.3">Le chemin est ouvert aux ailes de leurs âmes ;</l>
					<l n="14" num="4.4">Rien de ces purs flambeaux ne fait trembler les flammes.</l>
					<l n="15" num="4.5">Hélas ! en les voyant rayonner au saint lieu,</l>
					<l n="16" num="4.6">Quelle femme oserait se confesser à Dieu ?</l>
				</lg>
				<lg n="5">
					<head type="main">UNE VOIX</head>
					<l n="17" num="5.1">Laissez, laissez passer la grâce et l’innocence :</l>
					<l n="18" num="5.2">Laissez remonter l’âme à sa divine essence !</l>
				</lg>
				<lg n="6">
					<head type="main">LES ENFANS</head>
					<l n="19" num="6.1">Doux Maître ! nous venons, sans passé, sans remords,</l>
					<l n="20" num="6.2">Vous prier tendrement pour nos frères, les morts.</l>
					<l n="21" num="6.3">Qu’ils sortent du tombeau comme nous de nos langes ;</l>
					<l n="22" num="6.4">Doux Père ! accordez-leur encor des ailes d’anges.</l>
					<l n="23" num="6.5">Si pour les l’acheter nous n’avons pas de pleurs,</l>
					<l n="24" num="6.6">Dieu des petits enfans, prenez toutes nos fleurs !</l>
				</lg>
				<lg n="7">
					<head type="main">UNE VOIX</head>
					<l n="25" num="7.1">Laissez venir à Dieu la grâce et l’innocence :</l>
					<l n="26" num="7.2">Laissez remonter l’âme à sa divine essence !</l>
				</lg>
				<lg n="8">
					<head type="main">UNE FEMME</head>
					<l n="27" num="8.1">Béni soit le coin sombre où s’isole mon cœur !</l>
					<l n="28" num="8.2">Je ne l’entrerai plus vivante dans le chœur.</l>
					<l n="29" num="8.3">Dieu remet les pardons aux enfans qui l’enchantent ;</l>
					<l n="30" num="8.4">Mais ce n’est pas pour moi, c’est pour les morts qu’ils chantent.</l>
					<l n="31" num="8.5">Quand nous avons choisi notre amer abandon,</l>
					<l n="32" num="8.6">Nul ange pour nos pleurs ne demande pardon.</l>
				</lg>
				<lg n="9">
					<head type="main">UNE VOIX</head>
					<l n="33" num="9.1">Laissez, laissez passer la grâce et l’innocence :</l>
					<l n="34" num="9.2">Laissez remonter l’âme à sa divine essence !</l>
				</lg>
				<lg n="10">
					<head type="main">LES ENFANS</head>
					<l n="35" num="10.1">Nos mères ont appris qu’en ce jour solennel,</l>
					<l n="36" num="10.2">Tout vœu d’enfant s’élève aux pieds de l’Éternel.</l>
					<l n="37" num="10.3">Jésus ! prenez ce vœu sur nos bouches sans feinte ;</l>
					<l n="38" num="10.4">Du coupable qui pleure encouragez la plainte ;</l>
					<l n="39" num="10.5">Tendez vos bras ouverts au pécheur prosterné,</l>
					<l n="40" num="10.6">Et qu’il soit, comme nous, votre enfant pardonné !</l>
				</lg>
				<lg n="11">
					<head type="main">UNE VOIX</head>
					<l n="41" num="11.1">Laissez, laissez passer le vœu de l’innocence :</l>
					<l n="42" num="11.2">Laissez remonter l’âme à sa divine essence !</l>
				</lg>
				<lg n="12">
					<head type="main">UNE FEMME</head>
					<l n="43" num="12.1">Je me confesse à Dieu qui descend dans mes pleurs !</l>
					<l n="44" num="12.2">Dieu, qui peut d’un regard changer la ronce en fleurs !</l>
					<l n="45" num="12.3">Voix du monde, cessez : je rapprends qu’on espère !</l>
					<l n="46" num="12.4">Voix des anges, chantez : je retourne à mon Père !</l>
					<l n="47" num="12.5">Je me relève à Dieu dans l’élan de ma foi ;</l>
					<l n="48" num="12.6">L’enfance a pardonné : mon Dieu, pardonnez-moi !</l>
				</lg>
				<lg n="13">
					<head type="main">UNE VOIX</head>
					<l n="49" num="13.1">Laissez passer la foi, la grâce et l’innocence :</l>
					<l n="50" num="13.2">Laissez remonter l’âme a sa divine essence !</l>
				</lg>
			</div></body></text></TEI>