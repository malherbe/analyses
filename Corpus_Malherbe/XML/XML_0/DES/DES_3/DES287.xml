<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">BOUQUETS ET PRIÈRES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3255 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Bouquets et Prières</title>
						<author>Marceline Desbordes-Valmore</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k5510985p/f1n316.texteBrut</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Bouquets et Prières</title>
								<author>Marceline Desbordes-Valmore</author>
								<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k5510985p.r=bouquets+et+pri%C3%A8res.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Dumont</publisher>
									<date when="1843">1843</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>3255 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1843">1843</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface de l’auteur n’a pas été intégrée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
			</listChange>
		</revisionDesc>




	</teiHeader><text><body><div type="poem" key="DES287">
				<head type="main">CROYANCE POPULAIRE</head>
				<head type="sub_1">PRIÈRE AUX INNOCENS</head>
				<lg n="1">
					<l n="1" num="1.1">Beaux innocens, morts à minuit,</l>
					<l n="2" num="1.2">Réveillés quand la lune luit :</l>
				</lg>
				<lg n="2">
					<l n="3" num="2.1">Descendez sur mon front qui pleure</l>
					<l n="4" num="2.2">Et sauvez-moi d’entendre l’heure.</l>
					<l n="5" num="2.3">L’heure qui sonne fait souffrir</l>
					<l n="6" num="2.4">Quand la vie est triste à mourir ;</l>
					<l n="7" num="2.5">C’est l’espérance qui nous quitte ;</l>
					<l n="8" num="2.6">C’est le pouls du temps qui bat vite !</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Petits trépassés de minuit,</l>
					<l n="10" num="3.2">Endormez mon cœur qui me nuit.</l>
				</lg>
				<lg n="4">
					<l n="11" num="4.1">Pudiques sanglots de vos mères,</l>
					<l n="12" num="4.2">Doux fruits des voluptés amères,</l>
					<l n="13" num="4.3">Soufflez dans mon sort pâlissant,</l>
					<l n="14" num="4.4">De la foi le feu tout-puissant :</l>
					<l n="15" num="4.5">La foi ! c’est l’haleine des anges ;</l>
					<l n="16" num="4.6">C’est l’amour, sans flammes étranges !</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1">Beaux petits anges de minuit,</l>
					<l n="18" num="5.2">Épurez mon cœur qui me nuit.</l>
				</lg>
				<lg n="6">
					<l n="19" num="6.1">Fleurs entre le ciel et la tombe !</l>
					<l n="20" num="6.2">Portez à Dieu l’âme qui tombe.</l>
					<l n="21" num="6.3">Parlez à la Reine des cieux,</l>
					<l n="22" num="6.4">, Des pleurs qui rougissent mes yeux ;</l>
					<l n="23" num="6.5">Ramassez la fleur de la terre,</l>
					<l n="24" num="6.6">Qui meurt foulée et solitaire.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1">Beaux petits enfans de minuit,</l>
					<l n="26" num="7.2">Relevez mon cœur qui me nuit.</l>
				</lg>
				<lg n="8">
					<l n="27" num="8.1">La terre a séché mon haleine ;</l>
					<l n="28" num="8.2">Je parle et je m’entends à peine.</l>
					<l n="29" num="8.3">Écoutez : j’ai perdu l’accent</l>
					<l n="30" num="8.4">Du ciel, d’où votre vol descend.</l>
					<l n="31" num="8.5">Chantez mon nom seul à ma mère,</l>
					<l n="32" num="8.6">Pour qu’il rentre dans sa prière !</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1">Beaux innocens, morts à minuit,</l>
					<l n="34" num="9.2">Desserrez mon cœur qui me nuit.</l>
				</lg>
				<lg n="10">
					<l n="35" num="10.1">Avant d’être ainsi consternée,</l>
					<l n="36" num="10.2">Pâle devant ma destinée,</l>
					<l n="37" num="10.3">Je fus un enfant comme vous ;</l>
					<l n="38" num="10.4">J’avais le ciel sous mon front doux.</l>
					<l n="39" num="10.5">Oh ! changez ma robe flétrie,</l>
					<l n="40" num="10.6">Et menez-moi dans ma patrie !</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1">Enfans ! réveillés à minuit,</l>
					<l n="42" num="11.2">Déliez mon cœur qui me nuit.</l>
				</lg>
				<lg n="12">
					<l n="43" num="12.1">Sur votre jeune aile qui vole,</l>
					<l n="44" num="12.2">Élevez ma faible parole :</l>
					<l n="45" num="12.3">Il faut que je pleure trop bas.</l>
					<l n="46" num="12.4">Puisque le ciel né m’entend pas.</l>
					<l n="47" num="12.5">Mais quoi ? n’entend-il pas là feuille</l>
					<l n="48" num="12.6">Gémir, quand l’ouragan la cueille !</l>
				</lg>
				<lg n="13">
					<l n="49" num="13.1">Enfans réveillés à minuit,</l>
					<l n="50" num="13.2">Apaisez mon cœur qui me nuit.</l>
				</lg>
				<lg n="14">
					<l n="51" num="14.1">Dites-moi si dans votre monde,</l>
					<l n="52" num="14.2">La mémoire est calme et profonde ?</l>
					<l n="53" num="14.3">Déchirez mon obscurité,</l>
					<l n="54" num="14.4">Rayons blancs de l’éternité :</l>
					<l n="55" num="14.5">Vous tous qui m’avez entendue,</l>
					<l n="56" num="14.6">Répondez-moi ; suis-je perdue !…</l>
				</lg>
				<lg n="15">
					<l n="57" num="15.1">Beaux petits enfans de minuit ;</l>
					<l n="58" num="15.2">Éclairez mon cœur qui me nuit.</l>
				</lg>
				<lg n="16">
					<l n="59" num="16.1">Planez sur les maisons fermées</l>
					<l n="60" num="16.2">De nos jeunes sœurs bien-aimées ;</l>
					<l n="61" num="16.3">Que les vierges n’entendent pas</l>
					<l n="62" num="16.4">Le démon soupirer tout bas :</l>
					<l n="63" num="16.5">À minuit, les maisons ouvertes,</l>
					<l n="64" num="16.6">Présagent tant de tombes vertes !</l>
				</lg>
				<lg n="17">
					<l n="65" num="17.1">Heureux enfans morts à minuit</l>
					<l n="66" num="17.2">Éteignez mon cœur qui me nuit !</l>
				</lg>
			</div></body></text></TEI>