<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">BOUQUETS ET PRIÈRES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3255 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Bouquets et Prières</title>
						<author>Marceline Desbordes-Valmore</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k5510985p/f1n316.texteBrut</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Bouquets et Prières</title>
								<author>Marceline Desbordes-Valmore</author>
								<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k5510985p.r=bouquets+et+pri%C3%A8res.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Dumont</publisher>
									<date when="1843">1843</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>3255 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1843">1843</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface de l’auteur n’a pas été intégrée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
			</listChange>
		</revisionDesc>




	</teiHeader><text><body><div type="poem" key="DES289">
				<head type="main">DIEU PLEURE AVEC LES INNOCENTS</head>
				<lg n="1">
					<l n="1" num="1.1">Il fallait la laisser, solitaire et pieuse,</l>
					<l n="2" num="1.2">S’abreuver de <choice reason="analysis" type="false_verse" hand="CA"><sic>prieres</sic><corr source="édition_1973">prière</corr></choice> et d’indigentes fleurs :</l>
					<l n="3" num="1.3">Si peu lui semblait tout ; misère harmonieuse,</l>
					<l n="4" num="1.4">Sédentaire à l’église et bornée à ses pleurs.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Il fallait la laisser au long travail penchée,</l>
					<l n="6" num="2.2">Du rideau d’un vieux mur bornant son horizon</l>
					<l n="7" num="2.3">Le ciel la regardait sous ses cheveux penchée ;</l>
					<l n="8" num="2.4">Et quelque doux cantique apaisait sa raison.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Ce qu’elle avait perdu, qui pouvait le lui rendre ?</l>
					<l n="10" num="3.2">Aux enfans orphelins on ne rend pas les morts :</l>
					<l n="11" num="3.3">Mais seule, jour par jour, elle venait d’apprendre</l>
					<l n="12" num="3.4">Qu’un goût divin se mêle aux douleurs sans remords.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">SI fallait lui laisser Dieu pleurant avec elle ;</l>
					<l n="14" num="4.2">N’en doutez pas, «<hi rend="ital">Dieu pleure avec les innocens</hi>. »</l>
					<l n="15" num="4.3">Et vous l’avez volée à cet ami fidèle ;</l>
					<l n="16" num="4.4">Et vous avez versé la terre sur ses sens.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1">Vous avez dévasté la belle âme ingénue ;</l>
					<l n="18" num="5.2">Elle sait aujourd’hui la chute de l’orgueil.</l>
					<l n="19" num="5.3">Dieu vous demandera ce qu’elle est devenue :</l>
					<l n="20" num="5.4">Pour un ange tombé tout le ciel est en deuil.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1">Ah ! pour l’avoir tuée en mourrez-vous moins vite ?</l>
					<l n="22" num="6.2">Le tombeau, qui prend tout, vous fait-il moins d’effroi :</l>
					<l n="23" num="6.3">Il prend tout. Comme une ombre affligée ou maudite,</l>
					<l n="24" num="6.4">Vous quitterez la terre, en fussiez-vous le roi !</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1">Cherchez : elle est peut-être un peu vivante encore ;</l>
					<l n="26" num="7.2">Épousez dans la mort son amer abandon ;</l>
					<l n="27" num="7.3">Sanctifiez à deux votre nom qu’elle adore,</l>
					<l n="28" num="7.4">Et montez l’un par l’autre au céleste pardon !</l>
				</lg>
			</div></body></text></TEI>