<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">BOUQUETS ET PRIÈRES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3255 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Bouquets et Prières</title>
						<author>Marceline Desbordes-Valmore</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k5510985p/f1n316.texteBrut</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Bouquets et Prières</title>
								<author>Marceline Desbordes-Valmore</author>
								<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k5510985p.r=bouquets+et+pri%C3%A8res.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Dumont</publisher>
									<date when="1843">1843</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>3255 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1843">1843</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface de l’auteur n’a pas été intégrée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
			</listChange>
		</revisionDesc>




	</teiHeader><text><body><div type="poem" key="DES342">
				<head type="main">LES AMITIÉS DE LA JEUNESSE</head>
				<lg n="1">
					<l n="1" num="1.1">Des nœuds dont sa vie est liée,</l>
					<l n="2" num="1.2">Soulevant un moment le poids,</l>
					<l n="3" num="1.3">Et d’un long orage essuyée,</l>
					<l n="4" num="1.4">Mon âme se cherche une voix.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Comme sur le bord de sa cage</l>
					<l n="6" num="2.2">L’oiseau contraint de s’arrêter,</l>
					<l n="7" num="2.3">Sur ma bouche ainsi qu’au jeune âge,</l>
					<l n="8" num="2.4">L’âme est assise et veut chanter,</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Mon jeune âge a fait deux amies,</l>
					<l n="10" num="3.2">Dont l’une est partie avant moi,</l>
					<l n="11" num="3.3">Parfum de mes fleurs endormies :</l>
					<l n="12" num="3.4">L’autre fleur vivante, c’est toi !</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">Celle qui dort, je l’ai rêvée</l>
					<l n="14" num="4.2">Son bras enlacé dans le mien,</l>
					<l n="15" num="4.3">Tandis que toi, ma retrouvée,</l>
					<l n="16" num="4.4">Tu la retenais sous le tien.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1">Nous allions, comme trois colombes,</l>
					<l n="18" num="5.2">Effleurant à peine le blé ;</l>
					<l n="19" num="5.3">Et vers lé doux sentier des tombes</l>
					<l n="20" num="5.4">Le triple essor s’est envolé.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1">Pour panser un peu nos blessures,</l>
					<l n="22" num="6.2">Nous nous abattions dans les fleurs ;</l>
					<l n="23" num="6.3">Et ses angéliques censures</l>
					<l n="24" num="6.4">Ne s’aigrissaient pas de nos pleurs.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1">Son ombre, qui battait des ailes,</l>
					<l n="26" num="7.2">Charmante, nous disait tout bas :</l>
					<l n="27" num="7.3">« Allons voir des choses nouvelles ;</l>
					<l n="28" num="7.4">Allons vers Dieu, qui ne meurt pas ! »</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1">Elle marchait, pâle et contente,</l>
					<l n="30" num="8.2">Sans sourire, mais sans pleurer ;</l>
					<l n="31" num="8.3">Son âme, couchée à l’attente,</l>
					<l n="32" num="8.4">Avait fini de soupirer.</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1">La foule glissait devant elle,</l>
					<l n="34" num="9.2">Comme dans le monde on faisait,</l>
					<l n="35" num="9.3">Pour s’assurer qu’elle était belle</l>
					<l n="36" num="9.4">Comme le monde le disait.</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1">Des ombres lui criaient : « Madame !</l>
					<l n="38" num="10.2">Pour nous répondre arrêtez-vous :</l>
					<l n="39" num="10.3">Vous qui prenez âme par âme,</l>
					<l n="40" num="10.4">Où vous allez emmenez-nous !</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1">Car nous sommes bien accablées</l>
					<l n="42" num="11.2">D’attendre où l’on attend toujours :</l>
					<l n="43" num="11.3">Hélas ! nous serions moins troublées</l>
					<l n="44" num="11.4">D’entrer où finissent les jours ! »</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1">Alors ses pitiés envahies</l>
					<l n="46" num="12.2">Dans sou cœur semblaient se presser,</l>
					<l n="47" num="12.3">Devant ces âmes éblouies</l>
					<l n="48" num="12.4">Qui se heurtaient pour l’embrasser.</l>
				</lg>
				<lg n="13">
					<l n="49" num="13.1">Nous entrâmes dans une église,</l>
					<l n="50" num="13.2">Pour nous reposer à genoux ;</l>
					<l n="51" num="13.3">La Vierge seule était assise,</l>
					<l n="52" num="13.4">Posant son doux regard sur nous.</l>
				</lg>
				<lg n="14">
					<l n="53" num="14.1">Aux fenêtres de ses demeures</l>
					<l n="54" num="14.2">Les lumières ne tremblaient pas,</l>
					<l n="55" num="14.3">Et l’on n’entendait plus les heures</l>
					<l n="56" num="14.4">S’entre-détruire comme en bas.</l>
				</lg>
				<lg n="15">
					<l n="57" num="15.1">Notre corps ne faisait plus d’ombre</l>
					<l n="58" num="15.2">Comme dans ce triste univers,</l>
					<l n="59" num="15.3">Et notre âme n’était plus sombre :</l>
					<l n="60" num="15.4">Le soleil passait à travers !</l>
				</lg>
				<lg n="16">
					<l n="61" num="16.1">Voilà comment je l’ai rêvée,</l>
					<l n="62" num="16.2">Son bras enlacé dans le mien ;</l>
					<l n="63" num="16.3">Tandis que toi, ma retrouvée,</l>
					<l n="64" num="16.4">Tu la retenais sous le tien.</l>
				</lg>
			</div></body></text></TEI>