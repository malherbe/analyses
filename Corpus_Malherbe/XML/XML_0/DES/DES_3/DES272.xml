<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">BOUQUETS ET PRIÈRES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3255 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Bouquets et Prières</title>
						<author>Marceline Desbordes-Valmore</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k5510985p/f1n316.texteBrut</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Bouquets et Prières</title>
								<author>Marceline Desbordes-Valmore</author>
								<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k5510985p.r=bouquets+et+pri%C3%A8res.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Dumont</publisher>
									<date when="1843">1843</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>3255 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1843">1843</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface de l’auteur n’a pas été intégrée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
			</listChange>
		</revisionDesc>




	</teiHeader><text><body><div type="poem" key="DES272">
				<head type="main">JOURS D’ÉTÉ</head>
				<lg n="1">
					<l n="1" num="1.1">Ma sœur m’aimait en mère : elle m’apprit à lire.</l>
					<l n="2" num="1.2">Ce qu’elle y mit d’ardeur ne saurait se décrire :</l>
					<l n="3" num="1.3">Mais l’enfant ne sait pas qu’apprendre, c’est courir,</l>
					<l n="4" num="1.4">Et qu’on lui donne, assis, le monde à parcourir.</l>
					<l n="5" num="1.5">Voir ! voir ! l’enfant veut voir. Les doux bruits de la rue</l>
					<l n="6" num="1.6">Albertine charmante à la vitre apparue,</l>
					<l n="7" num="1.7">Élevant ses bouquets, ses volans, et là-bas,</l>
					<l n="8" num="1.8">Les jeux qui m’attendaient et ne commençaient pas ;</l>
					<l n="9" num="1.9">Oh ! le livre avait tort ! Tous les livres du monde,</l>
					<l n="10" num="1.10">Ne valaient pas un chant de la lointaine ronde,</l>
					<l n="11" num="1.11">Où mon âme sans moi tournait de main en main,</l>
					<l n="12" num="1.12">Quand ma sœur avait dit : — Tu danseras demain.</l>
				</lg>
				<lg n="2">
					<l n="13" num="2.1">Demain, c’était jamais ! Ma jeune providence,</l>
					<l n="14" num="2.2">Nouant d’un fil prudent les ailes de la danse,</l>
					<l n="15" num="2.3">Me répétait en vain toute grave et tout bas :</l>
					<l n="16" num="2.4">« Vois donc : je suis heureuse, et je ne danse pas. »</l>
				</lg>
				<lg n="3">
					<l n="17" num="3.1"><space quantity="12" unit="char"></space>J’aimais tant les anges</l>
					<l n="18" num="3.2"><space quantity="12" unit="char"></space>Glissant au soleil !</l>
					<l n="19" num="3.3"><space quantity="12" unit="char"></space>Ce flot sans mélanges,</l>
					<l n="20" num="3.4"><space quantity="12" unit="char"></space>D’amour sans pareil !</l>
					<l n="21" num="3.5"><space quantity="12" unit="char"></space>Étude vivante</l>
					<l n="22" num="3.6"><space quantity="12" unit="char"></space>D’avenirs en fleur ;</l>
					<l n="23" num="3.7"><space quantity="12" unit="char"></space>École savante,</l>
					<l n="24" num="3.8"><space quantity="12" unit="char"></space>Savante au bonheur !</l>
				</lg>
				<lg n="4">
					<l n="25" num="4.1">Pour regarder de près ces aurores nouvelles,</l>
					<l n="26" num="4.2">Mes six ans curieux battaient toutes leurs ailes ;</l>
					<l n="27" num="4.3">Marchant sur l’alphabet rangé sur mes genoux,</l>
					<l n="28" num="4.4">La mouche en bourdonnant me disait : Venez-vous ?…</l>
					<l n="29" num="4.5">Et mon nom qui tintait dans l’air ardent de joie,</l>
					<l n="30" num="4.6">Les pigeons sans liens sous leur robe de soie,</l>
					<l n="31" num="4.7">Mollement envolés de maison en maison,</l>
					<l n="32" num="4.8">Dont le fluide essor entraînait ma raison ;</l>
					<l n="33" num="4.9">Les arbres, hors des murs poussant leurs têtes vertes ;</l>
					<l n="34" num="4.10">Jusqu’au fond des jardins les demeures ouvertes ;</l>
					<l n="35" num="4.11">Le rire de l’été sonnant de toutes parts,</l>
					<l n="36" num="4.12">Et le congé, sans livre ! errant aux vieux remparts :</l>
					<l n="37" num="4.13">Tout combattait ma sœur à l’aiguille attachée ;</l>
					<l n="38" num="4.14">Tout passait en chantant sous ma tête penchée ;</l>
					<l n="39" num="4.15">Tout m’enlevait, boudeuse et riante à la fois ;</l>
					<l n="40" num="4.16">Et l’alphabet toujours s’endormait dans ma voix.</l>
				</lg>
				<lg n="5">
					<l n="41" num="5.1">Oh ! l’enfance est poète. Assise ou turbulente,</l>
					<l n="42" num="5.2">Elle reconnaît tout empreint de plus haut lieu :</l>
					<l n="43" num="5.3">L’oiseau qui jette au loin sa musique volante,</l>
					<l n="44" num="5.4"><space quantity="6" unit="char"></space>Lui chante une lettre de Dieu !</l>
				</lg>
				<lg n="6">
					<l n="45" num="6.1">Moi, j’y reviens toujours à l’enfance chérie,</l>
					<l n="46" num="6.2">Comme un pâle exilé cherche au loin sa patrie »</l>
					<l n="47" num="6.3">Bel âge qui demande :<hi rend="ital">en quoi sont faits les morts</hi> ?</l>
					<l n="48" num="6.4">_Et dit avec Malcolm ; « Qu’est-ce que le Remords ? »</l>
					<l n="49" num="6.5">Esprit qui passe, ouvrant son aile souple et forte,</l>
					<l n="50" num="6.6">Au souffle impérieux qui l’enivre et l’emporte,</l>
					<l n="51" num="6.7">D’où vient qu’à ton beau rêve où se miraient les cieux,</l>
					<l n="52" num="6.8">Je sens fondre une larme en un coin dé mes yeux ?</l>
					<l n="53" num="6.9">C’est qu’aux flots de lait pur que me versait ma mère,</l>
					<l n="54" num="6.10">Ne se mêlait alors pas une goutte amère ;</l>
					<l n="55" num="6.11">C’est qu’on baisait l’enfant qui criait : Tout pour moi !</l>
					<l n="56" num="6.12">C’est qu’on lui répondait encor : « Oui tout pour toi ;</l>
					<l n="57" num="6.13">« Veux-tu le monde aussi ? tu l’auras, ma jeune âme. »</l>
					<l n="58" num="6.14">Hélas ! qu’avons-nous eu ? belle espérance ! ô femme !</l>
					<l n="59" num="6.15">O toi qui m’as trompée avec tes blonds cheveux,</l>
					<l n="60" num="6.16">Tes chants de rossignol et tes placides jeux !</l>
				</lg>
				<lg n="7">
					<l n="61" num="7.1">Ma sœur : ces jours d’été nous les courions ensemble ;</l>
					<l n="62" num="7.2">Je reprends sous leurs flots ta douce main qui tremble ;</l>
					<l n="63" num="7.3">Je t’aime du bonheur que tu tenais de moi ;</l>
					<l n="64" num="7.4">Et mes soleils d’alors se rallument sur toi !</l>
					<l n="65" num="7.5">Mais j’épelais enfin : l’esprit et la lumière,</l>
					<l n="66" num="7.6">Éclairaient par degrés la page, la première</l>
					<l n="67" num="7.7">D’un beau livre, terni sous mes doigts, sous mes pleurs,</l>
					<l n="68" num="7.8">Où la Bible aux enfans ouvre toutes ses fleurs :</l>
					<l n="69" num="7.9">Pourtant c’est par le cœur, cette bible vivante,</l>
					<l n="70" num="7.10">Que je compris bientôt qu’on me faisait savante :</l>
					<l n="71" num="7.11">Dieu ! le jour n’entre-t-il dans notre entendement,</l>
					<l n="72" num="7.12">Que trempé pour jamais d’un triste sentiment !</l>
				</lg>
				<lg n="8">
					<l n="73" num="8.1">Un frêle enfant manquait aux genoux de ma mère :</l>
					<l n="74" num="8.2">Il s’était comme enfui par une bise amère,</l>
					<l n="75" num="8.3">Et, disparu du rang de ses petits amis,</l>
					<l n="76" num="8.4">Au berceau blanc, le soir, il ne fut pas remis.</l>
					<l n="77" num="8.5">Ce vague souvenir, sur ma jeune pensée</l>
					<l n="78" num="8.6">Avait pesé deux ans, et puis, m’avait laissée.</l>
					<l n="79" num="8.7">Je ne comprenais plus pourquoi, pâle de pleurs,</l>
					<l n="80" num="8.8">Ma mère, vers l’église allait avec ses fleurs.</l>
					<l n="81" num="8.9">L’église, en ce temps là, des vertes sépultures,</l>
					<l n="82" num="8.10">Se composait encor de sévères ceintures ;</l>
					<l n="83" num="8.11">Et versant sur les morts ses longs hymnes fervens,</l>
					<l n="84" num="8.12">Au rendez-vous de tous appelait les vivans.</l>
					<l n="85" num="8.13">C’était beau d’enfermer dans une même enceinte,</l>
					<l n="86" num="8.14">La poussière animée et la poussière éteinte ;</l>
					<l n="87" num="8.15">C’était doux, dans les fleurs éparses au saint lieu,</l>
					<l n="88" num="8.16">De respirer son père en visitant son Dieu !</l>
				</lg>
				<lg n="9">
					<l n="89" num="9.1"><space quantity="2" unit="char"></space>J’y pense ; un jour de tiède et pâle automne,</l>
					<l n="90" num="9.2"><space quantity="2" unit="char"></space>Après le mois qui consume et qui tonne,</l>
					<l n="91" num="9.3"><space quantity="2" unit="char"></space>Près de ma sœur et ma main dans sa main,</l>
					<l n="92" num="9.4"><space quantity="2" unit="char"></space>De Notre-Dame ayant pris le chemin</l>
					<l n="93" num="9.5"><space quantity="2" unit="char"></space>Tout sinueux, planté de croix fleuries,</l>
					<l n="94" num="9.6"><space quantity="2" unit="char"></space>Où se mouraient des couronnes flétries,</l>
					<l n="95" num="9.7"><space quantity="2" unit="char"></space>Je regardais avec saisissement</l>
					<l n="96" num="9.8"><space quantity="2" unit="char"></space>Ce que ma sœur saluait tristement.</l>
					<l n="97" num="9.9"><space quantity="2" unit="char"></space>La lune large avant la nuit levée,</l>
					<l n="98" num="9.10"><space quantity="2" unit="char"></space>Comme une lampe avant l’heure éprouvée,</l>
					<l n="99" num="9.11"><space quantity="2" unit="char"></space>D’un reflet rouge enluminait les croix,</l>
					<l n="100" num="9.12"><space quantity="2" unit="char"></space>L’église blanche et tous ces lits étroits ;</l>
					<l n="101" num="9.13"><space quantity="2" unit="char"></space>Puis, dans les coins le chardon solitaire,</l>
					<l n="102" num="9.14"><space quantity="2" unit="char"></space>Éparpillait ses flocons sur la terre.</l>
				</lg>
				<lg n="10">
					<l n="103" num="10.1"><space quantity="2" unit="char"></space>Sans deviner ce que c’est que mourir,</l>
					<l n="104" num="10.2"><space quantity="2" unit="char"></space>Devant la mort je n’osai plus courir.</l>
					<l n="105" num="10.3"><space quantity="2" unit="char"></space>Un ruban gris qui serpentait dans l’herbe,</l>
					<l n="106" num="10.4"><space quantity="2" unit="char"></space>De résédas nouant l’humide gerbe,</l>
					<l n="107" num="10.5"><space quantity="2" unit="char"></space>Tira mon âme au tertre le plus vert,</l>
					<l n="108" num="10.6"><space quantity="2" unit="char"></space>Sous la madone, au flanc sept fois ouvert :</l>
					<l n="109" num="10.7"><space quantity="2" unit="char"></space>Là, j’épelai notre nom de famille,</l>
					<l n="110" num="10.8"><space quantity="2" unit="char"></space>Et je pâlis, faible petite fille ;</l>
					<l n="111" num="10.9"><space quantity="2" unit="char"></space>Puis mot à mot : « Notre dernier venu</l>
					<l n="112" num="10.10"><space quantity="2" unit="char"></space>Est passé là vers le monde inconnu ! »</l>
					<l n="113" num="10.11"><space quantity="2" unit="char"></space>Cette leçon, aux pieds de Notre-Dame,</l>
					<l n="114" num="10.12"><space quantity="2" unit="char"></space>Mouilla mes yeux et dessilla mon âme :</l>
					<l n="115" num="10.13"><space quantity="2" unit="char"></space>Je savais lire ! et j’appris sous des fleurs,</l>
					<l n="116" num="10.14"><space quantity="2" unit="char"></space>Ce qu’une mère aime avec tant de pleurs.</l>
					<l n="117" num="10.15"><space quantity="2" unit="char"></space>Je savais lire… et je pleurai moi-même.</l>
					<l n="118" num="10.16"><space quantity="2" unit="char"></space>Merci, ma sœur : on pleure dès qu’on aime.</l>
					<l n="119" num="10.17"><space quantity="2" unit="char"></space>Si jeune donc que soit le souvenir ;</l>
					<l n="120" num="10.18"><space quantity="2" unit="char"></space>C’est par un deuil qu’il faut y revenir ?</l>
				</lg>
				<lg n="11">
					<l n="121" num="11.1">Mais, que j’aime à t’aimer, sœur charmante et sévère,</l>
					<l n="122" num="11.2">Qui reçus pour nous deux l’instinct qui persévère ;</l>
					<l n="123" num="11.3">Rayon droit du devoir, humble, ardent et caché,</l>
					<l n="124" num="11.4">Sur mon aveugle vie à toute heure épanché !</l>
					<l n="125" num="11.5">Oh ! si Dieu m’aime encore ; oh ! si Dieu me remporte,</l>
					<l n="126" num="11.6">Comme un rêve flottant, sur le seuil de ta porte,</l>
					<l n="127" num="11.7">Devant mes traits changés si tu fermes tes bras,</l>
					<l n="128" num="11.8">Je saisirai ta main…, tu me reconnaîtras !</l>
				</lg>
			</div></body></text></TEI>