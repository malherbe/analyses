<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">BOUQUETS ET PRIÈRES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3255 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Bouquets et Prières</title>
						<author>Marceline Desbordes-Valmore</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k5510985p/f1n316.texteBrut</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Bouquets et Prières</title>
								<author>Marceline Desbordes-Valmore</author>
								<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k5510985p.r=bouquets+et+pri%C3%A8res.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Dumont</publisher>
									<date when="1843">1843</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>3255 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1843">1843</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface de l’auteur n’a pas été intégrée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
			</listChange>
		</revisionDesc>




	</teiHeader><text><body><div type="poem" key="DES309">
				<head type="main">À L’AUTEUR DE MARIE</head>
				<head type="sub_1">M. BRISEUC</head>
				<lg n="1">
					<l n="1" num="1.1">Vos vers c’est le printemps : pluie et soleil ensemble ;</l>
					<l n="2" num="1.2">C’est l’orage et l’oiseau dans le chêne qui tremble.</l>
					<l n="3" num="1.3">Moi, quand je me souviens, le front sur mes genoux,</l>
					<l n="4" num="1.4">J’écoute un de vos chants, jeune et vrai comme vous.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Vous ! que j’ai vu monter à la haute Italie,</l>
					<l n="6" num="2.2">Enfant, plein de musique et de mélancolie ;</l>
					<l n="7" num="2.3">Poète ! qu’une hysope arrêtait en chemin ;</l>
					<l n="8" num="2.4">Frère, attardant son pas pour rencontrer ma main.</l>
					<l n="9" num="2.5">Quand vous alliez fervent vers le peuple qui prie,</l>
					<l n="10" num="2.6">Vous portiez dans le cœur le livre de Marie ;</l>
					<l n="11" num="2.7">Vous aviez des parfums plein l’âme, et dans les yeux,</l>
					<l n="12" num="2.8">Comme au temps où l’on croit, de longs reflets des cieux.</l>
					<l n="13" num="2.9">Tout est dans ce beau livre écrit avec des flammes,</l>
					<l n="14" num="2.10">Reliquaire d’amour qui fait rêver les femmes ;</l>
					<l n="15" num="2.11">Dont chaque page pure exhale une âme en fleur,</l>
					<l n="16" num="2.12">Qui se répand dans l’ombre et coule pleur par pleur !</l>
					<l n="17" num="2.13">Chaste et vivante école, où ma vague pensée</l>
					<l n="18" num="2.14">Apprit à soulever son aile embarrassée ;</l>
					<l n="19" num="2.15">Seuil du toit paternel où s’élève un berceau ;</l>
					<l n="20" num="2.16">Foi vive, écoutant Dieu dans la voix du ruisseau ;</l>
					<l n="21" num="2.17">Instinct sublime et doux, qui touche une grande âme,</l>
					<l n="22" num="2.18">De pitié pour l’enfant, de respect pour la femme :</l>
					<l n="23" num="2.19">Tout est dans ce beau livre où l’on vous voit passer,</l>
					<l n="24" num="2.20">Marcher seul au soleil, et sourire et penser,</l>
					<l n="25" num="2.21">Et regarder de loin l’idole reconnue,</l>
					<l n="26" num="2.22">Comme aux nuits du pasteur l’étoile revenue,</l>
					<l n="27" num="2.23">Ou comme l’églantine au front du printemps vert,</l>
					<l n="28" num="2.24">Qui s’étonne et sourit d’avoir vaincu l’hiver :</l>
					<l n="29" num="2.25">Vos mains si sagement ont touché sa couronne,</l>
					<l n="30" num="2.26">Qu’elle ne rougit pas dans l’air qui l’environne,</l>
					<l n="31" num="2.27">Non, la vierge allaitante et ruminant le ciel,</l>
					<l n="32" num="2.28">N’a pas souri plus vierge aux mains de Raphaël !</l>
				</lg>
				<lg n="3">
					<l n="33" num="3.1">Eh bien ! qu’avez-vous fait des vertes espérances,</l>
					<l n="34" num="3.2">Frais dictame attendu par d’amères souffrances ?</l>
					<l n="35" num="3.3">En avez-vous cueilli sur les grands Apennins ?</l>
					<l n="36" num="3.4">Rome s’est-elle émue à vos ennuis divins ?</l>
					<l n="37" num="3.5">Vos cris ont-ils troublé cette reine indolente,</l>
					<l n="38" num="3.6">De son sommeil d’îlote a s’éveiller si lente ?</l>
					<l n="39" num="3.7">Avez-vous fait bondir dans les échos dormans</l>
					<l n="40" num="3.8">Vos colères d’amour et vos espoirs charmans ?</l>
					<l n="41" num="3.9">Ah ! vous me regardez et vous murmurez : DANTE !</l>
					<l n="42" num="3.10">Avez-vous dans l’enfer plongé votre âme ardente ?</l>
					<l n="43" num="3.11">Savez-vous Béatrix ? et vos traits pâlissans,</l>
					<l n="44" num="3.12">Disent-ils le secret de vos nouveaux accens !</l>
				</lg>
				<lg n="4">
					<l n="45" num="4.1"><space quantity="6" unit="char"></space>Si vous savez ce que fait l’âme sombre,</l>
					<l n="46" num="4.2"><space quantity="6" unit="char"></space>Bien que passant à travers beaucoup d’ombre,</l>
					<l n="47" num="4.3"><space quantity="6" unit="char"></space>Tant qu’au chemin pend un rayon vermeil,</l>
					<l n="48" num="4.4"><space quantity="6" unit="char"></space>Prenez, prenez le côté du soleil !</l>
					<l n="49" num="4.5"><space quantity="6" unit="char"></space>Aimez ce roi, le plus grand roi du monde,</l>
					<l n="50" num="4.6"><space quantity="6" unit="char"></space>Illuminant la terre qu’il féconde,</l>
					<l n="51" num="4.7"><space quantity="6" unit="char"></space>Ne gardant rien au bout de ses rayons,</l>
					<l n="52" num="4.8"><space quantity="6" unit="char"></space>Des flots d’or pur qu’il répand aux sillons !</l>
				</lg>
				<lg n="5">
					<l n="53" num="5.1"><space quantity="6" unit="char"></space>Allez grandir, jeune homme, à sa lumière,</l>
					<l n="54" num="5.2"><space quantity="6" unit="char"></space>Et chantez Dieu, source unique et première,</l>
					<l n="55" num="5.3"><space quantity="6" unit="char"></space>Du chaud trésor également versé</l>
					<l n="56" num="5.4"><space quantity="6" unit="char"></space>Sur l’humble chaume et le temple élancé !</l>
				</lg>
				<lg n="6">
					<l n="57" num="6.1"><space quantity="6" unit="char"></space>Tournez à lui, tournez lyre vivante,</l>
					<l n="58" num="6.2"><space quantity="6" unit="char"></space>Comme Daniel, sans le savoir, savante :</l>
					<l n="59" num="6.3"><space quantity="6" unit="char"></space>Baignez dans l’air tous vos rhytmes brûlans,</l>
					<l n="60" num="6.4"><space quantity="6" unit="char"></space>Qui, loin du jour mûriraient froids et lents.</l>
				</lg>
				<lg n="7">
					<l n="61" num="7.1"><space quantity="6" unit="char"></space>Buvez, buvez, à la source cachée,</l>
					<l n="62" num="7.2"><space quantity="6" unit="char"></space>Dieu vous la doit pour l’avoir bien cherchée :</l>
					<l n="63" num="7.3"><space quantity="6" unit="char"></space>Dieu le découvre à si peu d’entre nous</l>
					<l n="64" num="7.4"><space quantity="6" unit="char"></space>Ce filet d’eau que l’on boit à genoux !</l>
				</lg>
				<lg n="8">
					<l n="65" num="8.1"><space quantity="6" unit="char"></space>Moi, je suis ceux que la gelée offense ;</l>
					<l n="66" num="8.2"><space quantity="6" unit="char"></space>Que l’âpre hiver insulte sans défense ;</l>
					<l n="67" num="8.3"><space quantity="6" unit="char"></space>Qui, pour foyer n’ont qu’un vieux mur vermeil,</l>
					<l n="68" num="8.4"><space quantity="6" unit="char"></space>Chauffé par jour d’un rayon de soleil !</l>
				</lg>
			</div></body></text></TEI>