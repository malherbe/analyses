<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES INÉDITES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3950 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies inédites</title>
						<author>Madame Desbordes-Valmore</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Inédites</title>
								<author>Madame Desbordes-Valmore</author>
								<editor>Gustave Revilliod</editor>
								<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
								<imprint>
									<pubPlace>Genève</pubPlace>
									<publisher>Impr. de J. Fick</publisher>
									<date when="1860">1860</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>3950 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1860">1860</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les corrections mentionnées en errata ont été intégrées.</p>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">AMOUR</head><div type="poem" key="DES371">
					<head type="main">AU LIVRE DE LEOPARDI</head>
					<lg n="1">
						<l n="1" num="1.1">Il est de longs soupirs qui traversent les âges</l>
						<l n="2" num="1.2">Pour apprendre l’amour aux âmes les plus sages.</l>
						<l n="3" num="1.3">Ô sages ! de si loin que ces soupirs viendront,</l>
						<l n="4" num="1.4">Leurs brûlantes douceurs un jour vous troubleront.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">Et s’il vous faut garder parmi vos solitudes</l>
						<l n="6" num="2.2">Le calme qui préside aux sévères études,</l>
						<l n="7" num="2.3">Ne risquez pas vos yeux sur les tendres éclairs</l>
						<l n="8" num="2.4">De l’orage éternel enfermé dans ces vers,</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">Dans ces chants, dans ces cris, dans ces plaintes voilées,</l>
						<l n="10" num="3.2">Tocsins toujours vibrant de douleurs envolées.</l>
						<l n="11" num="3.3">Oh ! n’allez pas tenter, d’un courage hardi,</l>
						<l n="12" num="3.4">Tout cet amour qui pleure avec Léopardi !</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">Léopardi ! Doux christ oublié de son père.</l>
						<l n="14" num="4.2">Altéré de la mort sans le ciel qu’elle espère,</l>
						<l n="15" num="4.3">Qu’elle ouvre d’une clé pendue à tout berceau,</l>
						<l n="16" num="4.4">Levant de l’avenir l’insoulevable sceau.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">Ennemi de lui seul ! Aimer, et ne pas croire !</l>
						<l n="18" num="5.2">Sentir l’eau sur sa lèvre et ne pas l’oser boire !</l>
						<l n="19" num="5.3">Ne pas respirer Dieu dans l’âme d’une fleur !</l>
						<l n="20" num="5.4">Ne pas consoler l’ange attristé dans son cœur.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1">Ce que l’ange a souffert chez l’homme aveugle et tendre,</l>
						<l n="22" num="6.2">Ce qu’ils ont dit entre eux sans venir à s’entendre,</l>
						<l n="23" num="6.3">Ce qu’ils ont l’un par l’autre enduré de combats,</l>
						<l n="24" num="6.4">Sages qui voulez vivre, oh ! ne l’apprenez pas !</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1">Oh la mort ! ce sera le vrai réveil du songe !</l>
						<l n="26" num="7.2">Liberté ! ce sera ton règne sans mensonge !</l>
						<l n="27" num="7.3">Le grand dévoilement des âmes et du jour ;</l>
						<l n="28" num="7.4">Ce sera Dieu lui-même… oh, ce sera l’amour !</l>
					</lg>
				</div></body></text></TEI>