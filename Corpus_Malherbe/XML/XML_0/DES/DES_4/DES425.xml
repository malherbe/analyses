<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES INÉDITES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3950 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies inédites</title>
						<author>Madame Desbordes-Valmore</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Inédites</title>
								<author>Madame Desbordes-Valmore</author>
								<editor>Gustave Revilliod</editor>
								<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
								<imprint>
									<pubPlace>Genève</pubPlace>
									<publisher>Impr. de J. Fick</publisher>
									<date when="1860">1860</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>3950 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1860">1860</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les corrections mentionnées en errata ont été intégrées.</p>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ENFANTS ET JEUNES FILLES</head><div type="poem" key="DES425">
					<head type="main">LA JEUNE PENSIONNAIRE</head>
					<lg n="1">
						<l n="1" num="1.1">Ah ! je suis inconsolable</l>
						<l n="2" num="1.2">D’avoir perdu mon ruban !</l>
						<l n="3" num="1.3">Ma chère, il était semblable</l>
						<l n="4" num="1.4">Aux rouleaux de mon volant.</l>
						<l n="5" num="1.5">Celui-ci, bien qu’adorable.</l>
						<l n="6" num="1.6">Regarde, est d’un autre blanc !…</l>
					</lg>
					<lg n="2">
						<l n="7" num="2.1">On a bien raison de dire :</l>
						<l n="8" num="2.2">« Les chagrins sont près de nous.</l>
						<l n="9" num="2.3">Pas un cœur qui ne soupire</l>
						<l n="10" num="2.4">Du sort méchant et jaloux.</l>
						<l n="11" num="2.5">Tu ris… Ne me fais pas rire !</l>
						<l n="12" num="2.6">Pourtant, ce serait bien doux !</l>
					</lg>
					<lg n="3">
						<l n="13" num="3.1">Mais je suis inconsolable</l>
						<l n="14" num="3.2">D’avoir perdu mon ruban !</l>
						<l n="15" num="3.3">Ma chère, il était semblable</l>
						<l n="16" num="3.4">Aux rouleaux de mon volant.</l>
						<l n="17" num="3.5">Celui-ci, bien qu’adorable,</l>
						<l n="18" num="3.6">Regarde, est d’un autre blanc.</l>
					</lg>
					<lg n="4">
						<l n="19" num="4.1">Mise hier comme une fée,</l>
						<l n="20" num="4.2">Au bras de mon frère Henri,</l>
						<l n="21" num="4.3">D’un coup de vent décoiffée.</l>
						<l n="22" num="4.4">J’entre, et chacun pousse un cri</l>
						<l n="23" num="4.5">J’étais toute ébouriffée :</l>
						<l n="24" num="4.6">Juge si nous avons ri !</l>
					</lg>
					<lg n="5">
						<l n="25" num="5.1">Mais je suis inconsolable</l>
						<l n="26" num="5.2">D’avoir perdu mon ruban !</l>
						<l n="27" num="5.3">Ma chère, il était semblable</l>
						<l n="28" num="5.4">Aux rouleaux de mon volant.</l>
						<l n="29" num="5.5">Celui-ci, bien qu’adorable.</l>
						<l n="30" num="5.6">Regarde, est d’un autre blanc !</l>
					</lg>
					<lg n="6">
						<l n="31" num="6.1">La joie est dans notre école.</l>
						<l n="32" num="6.2">Mais toujours le bonheur ment !</l>
						<l n="33" num="6.3">Tiens, c’est un oiseau qui vole !</l>
						<l n="34" num="6.4">Moi, j’irai Dieu sait comment…</l>
						<l n="35" num="6.5">Que ne suis-je un peu frivole</l>
						<l n="36" num="6.6">Au moins pour danser gaiement.</l>
					</lg>
					<lg n="7">
						<l n="37" num="7.1">Mais je suis inconsolable</l>
						<l n="38" num="7.2">D’avoir perdu mon ruban ;</l>
						<l n="39" num="7.3">Ma chère, il était semblable</l>
						<l n="40" num="7.4">Aux rouleaux de mon volant.</l>
						<l n="41" num="7.5">Celui-ci, bien qu’adorable,</l>
						<l n="42" num="7.6">Regarde, est d’un autre blanc.</l>
					</lg>
					<lg n="8">
						<l n="43" num="8.1">Si j’étais moins désolée</l>
						<l n="44" num="8.2">Nous redirions notre pas…</l>
						<l n="45" num="8.3">Pourtant, avant l’assemblée.</l>
						<l n="46" num="8.4">Chantons et valsons tout bas.</l>
						<l n="47" num="8.5">Suis-moi ! je suis envolée !</l>
						<l n="48" num="8.6">C’est enchanteur, n’est ce pas !…</l>
					</lg>
					<lg n="9">
						<l n="49" num="9.1">Mais je suis inconsolable</l>
						<l n="50" num="9.2">D’avoir perdu mon ruban ;</l>
						<l n="51" num="9.3">Ma chère, il était semblable</l>
						<l n="52" num="9.4">Aux rouleaux de mon volant.</l>
						<l n="53" num="9.5">Celui-ci, bien qu’adorable,</l>
						<l n="54" num="9.6">Regarde, est d’un autre blanc !</l>
					</lg>
				</div></body></text></TEI>