<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES INÉDITES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3950 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies inédites</title>
						<author>Madame Desbordes-Valmore</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Inédites</title>
								<author>Madame Desbordes-Valmore</author>
								<editor>Gustave Revilliod</editor>
								<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
								<imprint>
									<pubPlace>Genève</pubPlace>
									<publisher>Impr. de J. Fick</publisher>
									<date when="1860">1860</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>3950 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1860">1860</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les corrections mentionnées en errata ont été intégrées.</p>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">POÉSIES DIVERSES</head><div type="poem" key="DES457">
					<head type="main">LA PAUVRE FILLE</head>
					<lg n="1">
						<l n="1" num="1.1"><space quantity="4" unit="char"></space>À toi le monde ! à toi la vie !</l>
						<l n="2" num="1.2"><space quantity="4" unit="char"></space>À toi tout ce que l’homme envie !</l>
						<l n="3" num="1.3"><space quantity="4" unit="char"></space>Mais dans l’ombre et sans me nommer,</l>
						<l n="4" num="1.4">À moi le ciel ! à moi le bonheur de t’aimer !</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><space quantity="4" unit="char"></space>Tu n’en sauras rien sur la terre.</l>
						<l n="6" num="2.2"><space quantity="4" unit="char"></space>Flamme invisible, en ton chemin</l>
						<l n="7" num="2.3"><space quantity="4" unit="char"></space>Je vivrai d’un ardent mystère</l>
						<l n="8" num="2.4"><space quantity="4" unit="char"></space>Sans avoir rencontré ta main.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><space quantity="4" unit="char"></space>À toi le monde ! à toi la vie !</l>
						<l n="10" num="3.2"><space quantity="4" unit="char"></space>À toi tout ce que l’homme envie !</l>
						<l n="11" num="3.3"><space quantity="4" unit="char"></space>Mais dans l’ombre et sans me nommer,</l>
						<l n="12" num="3.4">À moi le ciel ! à moi le bonheur de t’aimer !</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><space quantity="4" unit="char"></space>Jeune aigle, amour d’une hirondelle,</l>
						<l n="14" num="4.2"><space quantity="4" unit="char"></space>Qui te cache ses humbles jours,</l>
						<l n="15" num="4.3"><space quantity="4" unit="char"></space>Va planer loin d’un cœur fidèle</l>
						<l n="16" num="4.4"><space quantity="4" unit="char"></space>Dont le cri te suivra toujours.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><space quantity="4" unit="char"></space>À toi le monde ! à toi la vie !</l>
						<l n="18" num="5.2"><space quantity="4" unit="char"></space>À toi tout ce que l’homme envie !</l>
						<l n="19" num="5.3"><space quantity="4" unit="char"></space>Mais dans l’ombre et sans me nommer,</l>
						<l n="20" num="5.4">À moi le ciel ! à moi le bonheur de t’aimer !</l>
					</lg>
				</div></body></text></TEI>