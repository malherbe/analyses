<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES INÉDITES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3950 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies inédites</title>
						<author>Madame Desbordes-Valmore</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Inédites</title>
								<author>Madame Desbordes-Valmore</author>
								<editor>Gustave Revilliod</editor>
								<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
								<imprint>
									<pubPlace>Genève</pubPlace>
									<publisher>Impr. de J. Fick</publisher>
									<date when="1860">1860</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>3950 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1860">1860</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les corrections mentionnées en errata ont été intégrées.</p>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">FAMILLE</head><div type="poem" key="DES390">
					<head type="main">À MON FILS</head>
					<head type="sub">AVANT LE COLLÈGE</head>
					<lg n="1">
						<l n="1" num="1.1">Un soir, l’âtre éclairait notre maison fermée,</l>
						<l n="2" num="1.2">Par le travail et toi doucement animée.</l>
						<l n="3" num="1.3">Ton aïeul tout rêveur te prit sur ses genoux,</l>
						<l n="4" num="1.4">(Il n’a jamais sommeil pour veiller avec nous)</l>
						<l n="5" num="1.5">Il parla le premier de départ, de collège,</l>
						<l n="6" num="1.6">De travaux, de la gloire aussi qui les allège,</l>
						<l n="7" num="1.7">Content d’avoir été, jeune un jour comme toi,</l>
						<l n="8" num="1.8">Emmené par sa mère… il le disait pour moi…</l>
						<l n="9" num="1.9">Puis traçant des tableaux pour étendre ta vue,</l>
						<l n="10" num="1.10">De nouveaux horizons découvrant l’étendue,</l>
						<l n="11" num="1.11">Il dit que, si petit qu’il fût, par le chemin.</l>
						<l n="12" num="1.12">Il soutenait sa mère et lui tenait la main.</l>
						<l n="13" num="1.13">Il raconta comment cette femme prudente</l>
						<l n="14" num="1.14">L’avait porté loin d’elle en sa tendresse ardente.</l>
						<l n="15" num="1.15">Ses yeux étaient mouillés me fixant en dessous…</l>
						<l n="16" num="1.16">De ce poignant effort je l’aime et je l’absous !</l>
						<l n="17" num="1.17">Sur quoi, me voyant coudre un manteau de voyage,</l>
						<l n="18" num="1.18">Il m’embrassa deux fois pour louer mon courage,</l>
						<l n="19" num="1.19">Et toi, voyant qu’à tout je n’opposais plus rien,</l>
						<l n="20" num="1.20">Tu répondis : « Allons, mère, je le veux bien ! »</l>
					</lg>
					<lg n="2">
						<l n="21" num="2.1">Oui, l’enfant veut toujours aller, perçant l’espace,</l>
						<l n="22" num="2.2">Tourner autour du monde et voir ce qui s’y passe.</l>
						<l n="23" num="2.3">Oui, son âme est l’oiseau qui n’a point de séjour.</l>
						<l n="24" num="2.4">Et qui vole partout où Dieu répand le jour.</l>
						<l n="25" num="2.5">Dès ce moment j’appris que j’avais fait un rêve,</l>
						<l n="26" num="2.6">Que tout nous dit adieu, que tout bonheur s’achève,</l>
						<l n="27" num="2.7">Et je devins confuse en pesant mon devoir.</l>
						<l n="28" num="2.8">L’ai-je rempli ? Mon père était là pour le voir.</l>
						<l n="29" num="2.9">Le lendemain déjà dépassant la charmille</l>
						<l n="30" num="2.10">Et dérobant une âme au nid de la famille.</l>
						<l n="31" num="2.11">Quand nos pigeons rangés nous regardaient partir,</l>
						<l n="32" num="2.12">Trois fois prompte à rentrer, trois fois lente à sortir,</l>
						<l n="33" num="2.13">Comme celle qui croit oublier quelque chose.</l>
						<l n="34" num="2.14">Je ne pouvais sur toi tirer la porte close ;</l>
						<l n="35" num="2.15">Et le guide appelait : ah ! je l’entendais bien.</l>
						<l n="36" num="2.16">Mais j’oubliais toujours qu’il ne manquait plus rien.</l>
					</lg>
					<lg n="3">
						<l n="37" num="3.1">Et toi, dont toute l’âme éclatait sans culture.</l>
						<l n="38" num="3.2">Partout où s’arrêtait notre lourde voiture,</l>
						<l n="39" num="3.3">Cher petit protecteur de mon rude chemin,</l>
						<l n="40" num="3.4">Tu descendais devant pour me donner la main.</l>
						<l n="41" num="3.5">On souriait de voir, empressé comme un page,</l>
						<l n="42" num="3.6">Un enfant si soumis, si diligent, si sage ;</l>
						<l n="43" num="3.7">Et je disais en moi, triste comme aujourd’hui :</l>
						<l n="44" num="3.8">« Jamais je ne pourrai m’en revenir sans lui ! »</l>
					</lg>
					<lg n="4">
						<l n="45" num="4.1">Nous qui portons les fruits sur la terre où nous sommes,</l>
						<l n="46" num="4.2">Si fortes pour aimer, nous, faibles sœurs des hommes,</l>
						<l n="47" num="4.3">Ô mères, pourquoi donc les mettons-nous au jour.</l>
						<l n="48" num="4.4">Ces tendres fruits volés à notre ardent amour ?</l>
						<l n="49" num="4.5">À peine ils sont à nous qu’on veut nous les reprendre.</l>
						<l n="50" num="4.6">Ô mères, savez-vous ce qu’on va leur apprendre ?</l>
						<l n="51" num="4.7">À trembler sous un maître, à n’oser, par devoir,</l>
						<l n="52" num="4.8">Qu’une fois tous les ans demander à nous voir ;</l>
						<l n="53" num="4.9">À détourner de nous leurs mémoires légères.</l>
						<l n="54" num="4.10">Alors que sauront-ils ? Les langues étrangères,</l>
						<l n="55" num="4.11">Les vains soulèvements des peuples malheureux.</l>
						<l n="56" num="4.12">Et les fléaux humains toujours armés contre eux.</l>
						<l n="57" num="4.13">C’est donc beau ? Mais le temps saurait les en instruire.</l>
						<l n="58" num="4.14">Candeur de mon enfant, on va bien vous détruire !</l>
						<l n="59" num="4.15">Quand je le reverrai, mon fils sera savant ;</l>
						<l n="60" num="4.16">Il parlera latin ! Hélas, mon pauvre enfant.</l>
						<l n="61" num="4.17">Moi, je n’oserai plus peigner ta tête blonde.</l>
						<l n="62" num="4.18">Tu parleras latin ! Ta science profonde</l>
						<l n="63" num="4.19">Ne pouvant avec moi suivre un long entretien,</l>
						<l n="64" num="4.20">Tu diras tout surpris : « Ma mère ne sait rien ! »</l>
						<l n="65" num="4.21">Eh ! que veux-tu : l’amour n’en sait pas davantage ;</l>
						<l n="66" num="4.22">Ce maître conduit tout sans faire un grand tapage.</l>
						<l n="67" num="4.23">Il va ! Tant que mes pieds pouvaient porter mes jours,</l>
						<l n="68" num="4.24">J’allais chercher partout, pour t’en combler toujours,</l>
						<l n="69" num="4.25">Les fruits qui font bondir ta jeune fantaisie,</l>
						<l n="70" num="4.26">C’est notre étude à nous, c’est notre poésie.</l>
						<l n="71" num="4.27">Et je versais aussi quelques graves leçons</l>
						<l n="72" num="4.28">À ton doux cœur bercé par mes douces chansons.</l>
						<l n="73" num="4.29">N’était-ce pas assez pour nourrir ton jeune âge ?</l>
						<l n="74" num="4.30">Car tu n’as pas huit ans, chère âme ! Et c’est dommage,</l>
						<l n="75" num="4.31">Oui, je le dis dommage, et frayeur, et danger.</l>
						<l n="76" num="4.32">D’ouvrir tant de secrets à ton âge léger.</l>
					</lg>
				</div></body></text></TEI>