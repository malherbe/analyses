<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES INÉDITES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3950 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies inédites</title>
						<author>Madame Desbordes-Valmore</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Inédites</title>
								<author>Madame Desbordes-Valmore</author>
								<editor>Gustave Revilliod</editor>
								<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
								<imprint>
									<pubPlace>Genève</pubPlace>
									<publisher>Impr. de J. Fick</publisher>
									<date when="1860">1860</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>3950 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1860">1860</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les corrections mentionnées en errata ont été intégrées.</p>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">FAMILLE</head><div type="poem" key="DES398">
					<head type="main">DEUX MÈRES</head>
					<opener>
						<salute>À Caroline Branchu.</salute>
					</opener>
					<lg n="1">
						<l n="1" num="1.1">Une femme pleurait des pleurs d’une autre femme ;</l>
						<l n="2" num="1.2">Elles ont leurs secrets qu’elles plaignent toujours.</l>
						<l n="3" num="1.3">Celle qui regardait reconnaissait son âme :</l>
						<l n="4" num="1.4">Aux plus tendres, dit-on, les plus tristes amours !</l>
						<l n="5" num="1.5">L’enfant s’était enfui du toit de la plus pâle ;</l>
						<l n="6" num="1.6">Le père avait crié : « Qu’il ne revienne pas ! »</l>
						<l n="7" num="1.7">Et la mère, essayant ce ton sévère et mâle,</l>
						<l n="8" num="1.8">S’efforçait de crier : « Qu’il ne revienne… » hélas !</l>
					</lg>
					<lg n="2">
						<l n="9" num="2.1">L’autre saisit ses mains, commandant le silence,</l>
						<l n="10" num="2.2">Comme on fait au malade aigri qui veut mourir ;</l>
						<l n="11" num="2.3">Puis, soulageant ce cœur frappé d’un coup de lance.</l>
						<l n="12" num="2.4">Lui dit ces mots sans art pour l’aider à guérir :</l>
					</lg>
					<lg n="3">
						<l n="13" num="3.1"><space quantity="8" unit="char"></space>Lorsque Dieu descend sur la terre,</l>
						<l n="14" num="3.2"><space quantity="8" unit="char"></space>Il se cache au cœur d’une mère.</l>
					</lg>
					<lg n="4">
						<l n="15" num="4.1"><space quantity="8" unit="char"></space>En regardant rouler nos flots,</l>
						<l n="16" num="4.2"><space quantity="8" unit="char"></space>Penché sur ce monde qu’il aime,</l>
						<l n="17" num="4.3"><space quantity="8" unit="char"></space>Jésus, triste au fond du ciel même,</l>
						<l n="18" num="4.4"><space quantity="8" unit="char"></space>Retrouve ses divins sanglots.</l>
					</lg>
					<lg n="5">
						<l n="19" num="5.1"><space quantity="8" unit="char"></space>Alors, s’il revient sur la terre,</l>
						<l n="20" num="5.2"><space quantity="8" unit="char"></space>Il se cache au cœur d’une mère.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><space quantity="8" unit="char"></space>Lorsque par un volage enfant</l>
						<l n="22" num="6.2"><space quantity="8" unit="char"></space>Une tendre femme offensée</l>
						<l n="23" num="6.3"><space quantity="8" unit="char"></space>N’ose dire qui l’a blessée.</l>
						<l n="24" num="6.4"><space quantity="8" unit="char"></space>C’est que Jésus le lui défend :</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><space quantity="8" unit="char"></space>Car il est toujours sur la terre</l>
						<l n="26" num="7.2"><space quantity="8" unit="char"></space>Caché dans le cœur d’une mère.</l>
					</lg>
					<lg n="8">
						<l n="27" num="8.1"><space quantity="8" unit="char"></space>L’enfant par le monde égaré</l>
						<l n="28" num="8.2"><space quantity="8" unit="char"></space>Revient-il, tout las de ses charmes.</l>
						<l n="29" num="8.3"><space quantity="8" unit="char"></space>Un cœur plein d’amour et de larmes</l>
						<l n="30" num="8.4"><space quantity="8" unit="char"></space>Se rouvre au transfuge adoré.</l>
					</lg>
					<lg n="9">
						<l n="31" num="9.1"><space quantity="8" unit="char"></space>Car Jésus l’attend sur la terre,</l>
						<l n="32" num="9.2"><space quantity="8" unit="char"></space>Caché dans le cœur d’une mère.</l>
					</lg>
					<lg n="10">
						<l n="33" num="10.1">Durant ce doux conseil que buvait sa douleur,</l>
						<l n="34" num="10.2">L’écouteuse essuyait deux larmes incessantes ;</l>
						<l n="35" num="10.3">Elle voyait l’espoir passer dans son malheur ;</l>
						<l n="36" num="10.4">Elle voyait la mer aux vagues blanchissantes ;</l>
						<l n="37" num="10.5">Elle voyait l’enfant emporté sur les flots,</l>
						<l n="38" num="10.6">Et la foi dans son sein refoulait ses sanglots.</l>
						<l n="39" num="10.7">Au bord de son oreille elle entendait : « Courage ! »</l>
						<l n="40" num="10.8">Alors elle ceignit son manteau de voyage,</l>
						<l n="41" num="10.9">Et ses longs yeux de mère interrogeant les cieux,</l>
						<l n="42" num="10.10">Demandèrent sa route aux vents silencieux.</l>
					</lg>
					<lg n="11">
						<l n="43" num="11.1">Il se fit un grand calme au fond de sa blessure ;</l>
						<l n="44" num="11.2">On eût dit qu’on l’aidait tant sa marche était sûre ;</l>
						<l n="45" num="11.3">Et, se laissant glisser sous la pluie et le vent.</l>
						<l n="46" num="11.4">Elle jeta son âme au Dieu de son enfant :</l>
					</lg>
					<lg n="12">
						<l n="47" num="12.1"><space quantity="8" unit="char"></space>— « Quand les autres m’ont accablée,</l>
						<l n="48" num="12.2"><space quantity="8" unit="char"></space>Seigneur, vous m’avez consolée !</l>
						<l n="49" num="12.3"><space quantity="8" unit="char"></space>Je marcherai donc devant moi.</l>
						<l n="50" num="12.4"><space quantity="8" unit="char"></space>Pleine d’amour, pleine de foi ;</l>
						<l n="51" num="12.5"><space quantity="8" unit="char"></space>L’orage est en vain sur ma tête,</l>
						<l n="52" num="12.6"><space quantity="8" unit="char"></space>Vous me parlez dans la tempête ;</l>
						<l n="53" num="12.7"><space quantity="8" unit="char"></space>Elle menace et Dieu défend :</l>
						<l n="54" num="12.8"><space quantity="8" unit="char"></space>Dieu ! guidez-moi vers mon enfant.</l>
					</lg>
					<lg n="13">
						<l n="55" num="13.1"><space quantity="8" unit="char"></space>« Vous êtes le soutien des mères.</l>
						<l n="56" num="13.2"><space quantity="8" unit="char"></space>Le vengeur des larmes amères ;</l>
						<l n="57" num="13.3"><space quantity="8" unit="char"></space>On m’a dérobé mon trésor.</l>
						<l n="58" num="13.4"><space quantity="8" unit="char"></space>Mais vous me le gardez encor.</l>
						<l n="59" num="13.5"><space quantity="8" unit="char"></space>Dieu ! vous en êtes le seul maître,</l>
						<l n="60" num="13.6"><space quantity="8" unit="char"></space>Et vous le ferez bien connaître :</l>
						<l n="61" num="13.7"><space quantity="8" unit="char"></space>Par votre foi qui me défend,</l>
						<l n="62" num="13.8"><space quantity="8" unit="char"></space>Dieu ! guidez-moi vers mon enfant ! »</l>
					</lg>
					<lg n="14">
						<l n="63" num="14.1">Et plus tard l’autre mère à sa fenêtre assise</l>
						<l n="64" num="14.2">Tressaillit tout à coup d’une sainte surprise :</l>
						<l n="65" num="14.3">Elle voyait venir en lui tendant la main</l>
						<l n="66" num="14.4">Une humble voyageuse empressée au chemin.</l>
					</lg>
					<lg n="15">
						<l n="67" num="15.1">Sous une tiède lune aux errants favorable,</l>
						<l n="68" num="15.2">Lui montrant de ses pleurs le salaire adorable ;</l>
						<l n="69" num="15.3">Car un manteau de bure entr’ouvert par le vent</l>
						<l n="70" num="15.4">Abritait embrassés la mère avec l’enfant !</l>
					</lg>
					<closer>
						<placeName>De Boulogne, au bord de la mer.</placeName>
					</closer>
				</div></body></text></TEI>