<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES INÉDITES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3950 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies inédites</title>
						<author>Madame Desbordes-Valmore</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Inédites</title>
								<author>Madame Desbordes-Valmore</author>
								<editor>Gustave Revilliod</editor>
								<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
								<imprint>
									<pubPlace>Genève</pubPlace>
									<publisher>Impr. de J. Fick</publisher>
									<date when="1860">1860</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>3950 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1860">1860</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les corrections mentionnées en errata ont été intégrées.</p>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">FAMILLE</head><div type="poem" key="DES382">
					<head type="main">UN RUISSEAU DE LA SCARPE</head>
					<lg n="1">
						<l n="1" num="1.1">Oui, j’avais des trésors… j’en ai plein ma mémoire.</l>
						<l n="2" num="1.2">J’ai des banquets rêvés où l’orphelin va boire.</l>
						<l n="3" num="1.3">Oh ! quel enfant des bleds, le long des chemins verts,</l>
						<l n="4" num="1.4">N’a, dans ses jeux errants, possédé l’univers ?</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">Emmenez-moi, chemins !…Mais non, ce n’est plus l’heure,</l>
						<l n="6" num="2.2">Il faudrait revenir en courant où l’on pleure,</l>
						<l n="7" num="2.3">Sans avoir regardé jusqu’au fond le ruisseau</l>
						<l n="8" num="2.4">Dont la vague mouilla l’osier de mon berceau.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">Il courait vers la Scarpe en traversant nos rues</l>
						<l n="10" num="3.2">Qu’épurait la fraîcheur de ses ondes accrues ;</l>
						<l n="11" num="3.3">Et l’enfance aux longs cris saluait son retour</l>
						<l n="12" num="3.4">Qui faisait déborder tous les puits d’alentour.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">Écoliers de ce temps, troupe alerte et bruyante,</l>
						<l n="14" num="4.2">Où sont-ils vos présents jetés à l’eau fuyante ?</l>
						<l n="15" num="4.3">Le livre ouvert, parfois vos souliers pour vaisseaux,</l>
						<l n="16" num="4.4">Et vos petits jardins de mousse et d’arbrisseaux ?</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">Air natal ! aliment de saveur sans seconde,</l>
						<l n="18" num="5.2">Qui nourris tes enfants et les baise à la ronde ;</l>
						<l n="19" num="5.3">Air natal imprégné des souffles de nos champs,</l>
						<l n="20" num="5.4">Qui fais les cœurs pareils et pareils les penchants !</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1">Et la longue innocence, et le joyeux sourire</l>
						<l n="22" num="6.2">Des nôtres, qui n’ont pas de plus beau livre à lire</l>
						<l n="23" num="6.3">Que leur visage ouvert et leurs grands yeux d’azur,</l>
						<l n="24" num="6.4">Et leur timbre profond d’où sort l’entretien sûr !</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1">Depuis que j’ai quitté tes haleines bénies.</l>
						<l n="26" num="7.2">Tes familles aux mains facilement unies.</l>
						<l n="27" num="7.3">Je ne sais quoi d’amer à mon pain s’est mêlé.</l>
						<l n="28" num="7.4">Et partout sur mon jour une larme a tremblé.</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1">Et je n’ai plus osé vivre à poitrine pleine</l>
						<l n="30" num="8.2">Ni respirer tout l’air qu’il faut à mon haleine.</l>
						<l n="31" num="8.3">On eût dit qu’un témoin s’y serait opposé…</l>
						<l n="32" num="8.4">Vivre pour vivre, oh non ! je ne l’ai plus osé !</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1">Non, le cher souvenir n’est qu’un cri de souffrance !</l>
						<l n="34" num="9.2">Viens donc, toi, dont le cours peut traverser la France ;</l>
						<l n="35" num="9.3">À ta molle clarté je livrerai mon front,</l>
						<l n="36" num="9.4">Et dans tes flots du moins mes larmes se perdront.</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1">Viens ranimer le cœur séché de nostalgie,</l>
						<l n="38" num="10.2">Le prendre, et l’inonder d’une fraîche énergie.</l>
						<l n="39" num="10.3">En sortant d’abreuver l’herbe de nos guérets,</l>
						<l n="40" num="10.4">Viens, ne fût-ce qu’une heure, abreuver mes regrets !</l>
					</lg>
					<lg n="11">
						<l n="41" num="11.1">Amène avec ton bruit une de nos abeilles</l>
						<l n="42" num="11.2">Dont l’essaim, quoique absent, bourdonne en mes oreilles,</l>
						<l n="43" num="11.3">Elle en parle toujours ! diront-ils… Mais, mon Dieu,</l>
						<l n="44" num="11.4">Jeune, on a tant aimé ces parcelles de feu !</l>
					</lg>
					<lg n="12">
						<l n="45" num="12.1">Ces gouttes de soleil dans notre azur qui brille.</l>
						<l n="46" num="12.2">Dansant sur le tableau lointain de la famille,</l>
						<l n="47" num="12.3">Visiteuses des bleds où logent tant de fleurs.</l>
						<l n="48" num="12.4">Miel qui vole émané des célestes chaleurs !</l>
					</lg>
					<lg n="13">
						<l n="49" num="13.1">J’en ai tant vu passer dans l’enclos de mon père</l>
						<l n="50" num="13.2">Qu’il en fourmille au fond de tout ce que j’espère ;</l>
						<l n="51" num="13.3">Sur toi dont l’eau rapide a délecté mes jours.</l>
						<l n="52" num="13.4">Et m’a fait cette voix qui soupire toujours.</l>
					</lg>
					<lg n="14">
						<l n="53" num="14.1">Dans ce poignant amour que je m’efforce à rendre,</l>
						<l n="54" num="14.2">Dont j’ai souffert longtemps avant de le comprendre,</l>
						<l n="55" num="14.3">Comme d’un pâle enfant on berce le souci,</l>
						<l n="56" num="14.4">Ruisseau, tu me rendrais ce qui me manque ici.</l>
					</lg>
					<lg n="15">
						<l n="57" num="15.1">Ton bruit sourd, se mêlant au rouet de ma mère,</l>
						<l n="58" num="15.2">Enlevant à son cœur quelque pensée amère,</l>
						<l n="59" num="15.3">Quand pour nous le donner elle cherchait là-bas</l>
						<l n="60" num="15.4">Un bonheur attardé qui ne revenait pas.</l>
					</lg>
					<lg n="16">
						<l n="61" num="16.1">Cette mère, à ta rive elle est assise encore ;</l>
						<l n="62" num="16.2">La voilà qui me parle, ô mémoire sonore !</l>
						<l n="63" num="16.3">Ô mes palais natals qu’on m’a fermés souvent !</l>
						<l n="64" num="16.4">La voilà qui les rouvre à son heureux enfant !</l>
					</lg>
					<lg n="17">
						<l n="65" num="17.1">Je ressaisis sa robe, et ses mains, et son âme !</l>
						<l n="66" num="17.2">Sur ma lèvre entr’ouverte elle répand sa flamme !</l>
						<l n="67" num="17.3">Non ! par tout l’or du monde on ne me paîrait pas</l>
						<l n="68" num="17.4">Ce souffle, ce ruisseau qui font trembler mes pas !</l>
					</lg>
				</div></body></text></TEI>