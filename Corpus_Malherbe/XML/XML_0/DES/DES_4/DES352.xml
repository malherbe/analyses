<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES INÉDITES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3950 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies inédites</title>
						<author>Madame Desbordes-Valmore</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Inédites</title>
								<author>Madame Desbordes-Valmore</author>
								<editor>Gustave Revilliod</editor>
								<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
								<imprint>
									<pubPlace>Genève</pubPlace>
									<publisher>Impr. de J. Fick</publisher>
									<date when="1860">1860</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>3950 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1860">1860</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les corrections mentionnées en errata ont été intégrées.</p>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">AMOUR</head><div type="poem" key="DES352">
					<head type="main">LES CLOCHES ET LES LARMES</head>
					<lg n="1">
						<l n="1" num="1.1">Sur la terre où sonne l’heure,</l>
						<l n="2" num="1.2">Tout pleure, ah ! mon Dieu, tout pleure.</l>
					</lg>
					<lg n="2">
						<l n="3" num="2.1">L’orgue sous le sombre arceau,</l>
						<l n="4" num="2.2">Le pauvre offrant sa neuvaine,</l>
						<l n="5" num="2.3">Le prisonnier dans sa chaîne</l>
						<l n="6" num="2.4">Et l’enfant dans son berceau ;</l>
					</lg>
					<lg n="3">
						<l n="7" num="3.1">Sur la terre où sonne l’heure.</l>
						<l n="8" num="3.2">Tout pleure, ah ! mon Dieu, tout pleure.</l>
					</lg>
					<lg n="4">
						<l n="9" num="4.1">La cloche pleure le jour</l>
						<l n="10" num="4.2">Qui va mourir sur l’église,</l>
						<l n="11" num="4.3">Et cette pleureuse assise,</l>
						<l n="12" num="4.4">Qu’a-t-elle à pleurer ?… L’amour.</l>
					</lg>
					<lg n="5">
						<l n="13" num="5.1">Sur la terre où sonne l’heure,</l>
						<l n="14" num="5.2">Tout pleure, ah ! mon Dieu, tout pleure.</l>
					</lg>
					<lg n="6">
						<l n="15" num="6.1">Priant les anges cachés</l>
						<l n="16" num="6.2">D’assoupir ses nuits funestes</l>
						<l n="17" num="6.3">Voyez, aux sphères célestes,</l>
						<l n="18" num="6.4">Ses longs regards attachés.</l>
					</lg>
					<lg n="7">
						<l n="19" num="7.1">Sur la terre où sonne l’heure,</l>
						<l n="20" num="7.2">Tout pleure, ah ! mon Dieu, tout pleure.</l>
					</lg>
					<lg n="8">
						<l n="21" num="8.1">Et le ciel a répondu :</l>
						<l n="22" num="8.2">« Terre, ô terre, attendez l’heure !</l>
						<l n="23" num="8.3">J’ai dit à tout ce qui pleure.</l>
						<l n="24" num="8.4">Que tout lui sera rendu. »</l>
					</lg>
					<lg n="9">
						<l n="25" num="9.1">Sonnez, cloches ruisselantes !</l>
						<l n="26" num="9.2">Ruisselez, larmes brûlantes !</l>
						<l n="27" num="9.3">Cloches qui pleurez le jour !</l>
						<l n="28" num="9.4">Beaux yeux qui pleurez l’amour !</l>
					</lg>
				</div></body></text></TEI>