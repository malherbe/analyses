<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES INÉDITES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3950 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies inédites</title>
						<author>Madame Desbordes-Valmore</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Inédites</title>
								<author>Madame Desbordes-Valmore</author>
								<editor>Gustave Revilliod</editor>
								<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
								<imprint>
									<pubPlace>Genève</pubPlace>
									<publisher>Impr. de J. Fick</publisher>
									<date when="1860">1860</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>3950 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1860">1860</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les corrections mentionnées en errata ont été intégrées.</p>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">POÉSIES DIVERSES</head><div type="poem" key="DES454">
					<head type="main">LES DEUX MARINIÈRES</head>
					<lg n="1">
						<head>Marina</head>
						<l n="1" num="1.1">Entends-tu le canon du fort</l>
						<l n="2" num="1.2">Pour le vaisseau qui rentre au port ?</l>
						<l n="3" num="1.3">Mais, cousine, le capitaine</l>
						<l n="4" num="1.4">Tient l’équipage en quarantaine !</l>
						<l n="5" num="1.5">Viens voir de loin le bâtiment</l>
						<l n="6" num="1.6">Qui te ramène ton amant.</l>
					</lg>
					<lg n="2">
						<head>Laly Galine</head>
						<l n="7" num="2.1">Laisse-moi reprendre mon cœur</l>
						<l n="8" num="2.2">Qui s’en va de joie et de peur.</l>
						<l n="9" num="2.3">J’avais rêvé cette nouvelle,</l>
						<l n="10" num="2.4">Mais vois ! je suis moins forte qu’elle…</l>
						<l n="11" num="2.5">C’est ma neuvaine au roi des cieux</l>
						<l n="12" num="2.6">Qui met de tels pleurs dans mes yeux.</l>
					</lg>
					<lg n="3">
						<head>Marina</head>
						<l n="13" num="3.1">Tu me fais rire avec tes pleurs :</l>
						<l n="14" num="3.2">Prends plutôt dentelles et fleurs !</l>
						<l n="15" num="3.3">Prends et puisque Dieu te l’envoie,</l>
						<l n="16" num="3.4">Folle ! ne pleure pas de joie,</l>
						<l n="17" num="3.5">Car je sais que les amoureux</l>
						<l n="18" num="3.6">N’aiment pas qu’on pleure pour eux.</l>
					</lg>
					<lg n="4">
						<head>Laly Galine</head>
						<l n="19" num="4.1">Que veux-tu ? Je suis faite ainsi,</l>
						<l n="20" num="4.2">Et parfois l’homme pleure aussi.</l>
						<l n="21" num="4.3">Il n’est pas plus fier que moi-même,</l>
						<l n="22" num="4.4">Cousine, et c’est pourquoi je l’aime.</l>
						<l n="23" num="4.5">Une larme sauve : autrement</l>
						<l n="24" num="4.6">On mourrait de saisissement.</l>
					</lg>
					<lg n="5">
						<head>Marina</head>
						<l n="25" num="5.1">Allons ! viens ! tu n’en finis pas !</l>
						<l n="26" num="5.2">Viens ! Tout le monde court là-bas</l>
						<l n="27" num="5.3">Au salut du canon qui roule.</l>
						<l n="28" num="5.4">Ton marin te croit dans la foule ;</l>
						<l n="29" num="5.5">C’est la lenteur qui fait mourir,</l>
						<l n="30" num="5.6">Moi, mes pieds brûlent de courir.</l>
					</lg>
					<lg n="6">
						<head>Laly Galine</head>
						<l n="31" num="6.1">Marina, laisse-moi m’asseoir…</l>
						<l n="32" num="6.2">Je serai plus forte ce soir.</l>
						<l n="33" num="6.3">Il est là, j’ai le temps d’attendre ;</l>
						<l n="34" num="6.4">S’il parlait on pourrait l’entendre !</l>
						<l n="35" num="6.5">Comme l’oiseau qui suit le vent,</l>
						<l n="36" num="6.6">Mon âme est allée en avant !</l>
					</lg>
					<lg n="7">
						<head>Marina</head>
						<l n="37" num="7.1">Mon âme est partout où je cours,</l>
						<l n="38" num="7.2">Et je m’endors aux longs discours.</l>
						<l n="39" num="7.3">Ta vie est comme une prière</l>
						<l n="40" num="7.4">Qui craint le bruit et la lumière.</l>
						<l n="41" num="7.5">Pour moi, sans bruit et sans soleil,</l>
						<l n="42" num="7.6">Le temps serait un long sommeil.</l>
					</lg>
					<lg n="8">
						<head>Laly Galine</head>
						<l n="43" num="8.1">Le soir sera beau, Marina,</l>
						<l n="44" num="8.2">Dans la barque qu’il dessina.</l>
						<l n="45" num="8.3">La nuit n’y sera plus amère…</l>
						<l n="46" num="8.4">Mais je veux embrasser ma mère !</l>
						<l n="47" num="8.5">Va chercher du bruit pour ton cœur</l>
						<l n="48" num="8.6">Dieu fait à chacun son bonheur !</l>
					</lg>
					<closer>
						<placeName>Rochefort.</placeName>.
					</closer>
				</div></body></text></TEI>