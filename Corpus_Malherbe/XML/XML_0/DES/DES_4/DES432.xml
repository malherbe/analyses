<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES INÉDITES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3950 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies inédites</title>
						<author>Madame Desbordes-Valmore</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Inédites</title>
								<author>Madame Desbordes-Valmore</author>
								<editor>Gustave Revilliod</editor>
								<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
								<imprint>
									<pubPlace>Genève</pubPlace>
									<publisher>Impr. de J. Fick</publisher>
									<date when="1860">1860</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>3950 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1860">1860</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les corrections mentionnées en errata ont été intégrées.</p>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ENFANTS ET JEUNES FILLES</head><div type="poem" key="DES432">
					<head type="main">RENCONTRE D’UNE CHÈVRE ET D’UNE BREBIS</head>
					<lg n="1">
						<l n="1" num="1.1">« Pardon ! n’est-ce pas vous que j’ai vue une fois ? »</l>
						<l n="2" num="1.2"><space quantity="4" unit="char"></space>Dit, en faisant la révérence,</l>
						<l n="3" num="1.3">La chèvre à la brebis de chétive apparence,</l>
						<l n="4" num="1.4"><space quantity="4" unit="char"></space>Liée et seule au bord d’un bois.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">« Vous étiez, si c’est vous, si charmante et si folle</l>
						<l n="6" num="2.2"><space quantity="4" unit="char"></space>Qu’en vous voyant ainsi je n’osais vous parler.</l>
						<l n="7" num="2.3">J’accusais ma mémoire, et j’allais m’en aller</l>
						<l n="8" num="2.4"><space quantity="4" unit="char"></space>Sans vous adresser la parole. »</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">Et la brebis, levant sa tête avec effort,</l>
						<l n="10" num="3.2"><space quantity="4" unit="char"></space>Bêle ce sanglot de son âme :</l>
						<l n="11" num="3.3">— «Vous ne vous trompez pas ; c’est… c’était moi, madame ;</l>
						<l n="12" num="3.4"><space quantity="4" unit="char"></space>Et me voilà !… voilà le sort.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">« Quand j’étais blanche et rose, on m’a beaucoup parée.</l>
						<l n="14" num="4.2">Aux fêtes du printemps on m’habillait de fleurs ;</l>
						<l n="15" num="4.3">On me laissait brouter sur de tendres couleurs.</l>
						<l n="16" num="4.4"><space quantity="4" unit="char"></space>Et je me croyais adorée.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">« L’eau filtrant du rocher pour laver ma toison</l>
						<l n="18" num="5.2"><space quantity="4" unit="char"></space>Ne semblait jamais assez claire ;</l>
						<l n="19" num="5.3">Oh ! madame, c’est doux ! oui, c’est si doux de plaire</l>
						<l n="20" num="5.4"><space quantity="4" unit="char"></space>Qu’on n’en cherche pas la raison.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1">« Je dansais à la flûte une couronne en tête ;</l>
						<l n="22" num="6.2">J’en faisais mon devoir et ma cour au pasteur.</l>
						<l n="23" num="6.3">Je buvais dans sa tasse, intrépide, sans peur,</l>
						<l n="24" num="6.4"><space quantity="4" unit="char"></space>Et ses festins étaient ma fête.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1">« Tout changea. Le pasteur, las de m’être indulgent,</l>
						<l n="26" num="7.2"><space quantity="4" unit="char"></space>Me fit traîner au sacrifice.</l>
						<l n="27" num="7.3">Toutefois un enfant me sauva du supplice</l>
						<l n="28" num="7.4"><space quantity="4" unit="char"></space>Alors qu’on allait m’égorgeant.</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1">« La pitié… Je le crois, mais on m’ôta ma laine,</l>
						<l n="30" num="8.2">Ma sonnette d’argent, mes flots de rubans verts.</l>
						<l n="31" num="8.3">Ma liberté, ma part dans ce bel univers.</l>
						<l n="32" num="8.4"><space quantity="4" unit="char"></space>Et le doux lait dont j’étais pleine.</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1">« Je fus liée… » — « Horreur ! Ah ! j’aurais tant mordu,</l>
						<l n="34" num="9.2"><space quantity="4" unit="char"></space>Tant bondi pour casser ma corde,</l>
						<l n="35" num="9.3">Tant bramé vers le ciel : « À moi ! Miséricorde !</l>
						<l n="36" num="9.4"><space quantity="4" unit="char"></space>Que mon droit m’eût été rendu.</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1">« Aux cris de l’innocence il faut que Dieu réponde !</l>
						<l n="38" num="10.2">Oui, madame, on m’égorge : il doit me secourir.</l>
						<l n="39" num="10.3">Il doit me délier, moi, faite pour courir</l>
						<l n="40" num="10.4"><space quantity="4" unit="char"></space>Toutes les montagnes du monde ! »</l>
					</lg>
					<lg n="11">
						<l n="41" num="11.1">Le nez de la brebis se baissa consterné.</l>
						<l n="42" num="11.2"><space quantity="4" unit="char"></space>Humble aux bonheurs, douce au martyre,</l>
						<l n="43" num="11.3">Son cœur saigne et pourtant sa plainte se retire</l>
						<l n="44" num="11.4"><space quantity="4" unit="char"></space>De la chèvre au front étonné.</l>
					</lg>
					<lg n="12">
						<l n="45" num="12.1">— « Quoi ! vous ne sautez pas contre un sort si funeste ?</l>
						<l n="46" num="12.2">Que votre haine est molle et lente à s’enflammer ! »</l>
						<l n="47" num="12.3">— « La haine corromprait le bonheur qui me reste. »</l>
						<l n="48" num="12.4">— « Hé, mon Dieu ! quel est donc votre bonheur ? »— « D’aimer.»</l>
					</lg>
				</div></body></text></TEI>