<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES INÉDITES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3950 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies inédites</title>
						<author>Madame Desbordes-Valmore</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Inédites</title>
								<author>Madame Desbordes-Valmore</author>
								<editor>Gustave Revilliod</editor>
								<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
								<imprint>
									<pubPlace>Genève</pubPlace>
									<publisher>Impr. de J. Fick</publisher>
									<date when="1860">1860</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>3950 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1860">1860</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les corrections mentionnées en errata ont été intégrées.</p>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">FAMILLE</head><div type="poem" key="DES393">
					<head type="main">ONDINE À L’ÉCOLE</head>
					<lg n="1">
						<l n="1" num="1.1">Vous entriez, Ondine, à cette porte étroite,</l>
						<l n="2" num="1.2">Quand vous étiez petite, et vous vous teniez droite ;</l>
						<l n="3" num="1.3">Et quelque long carton sous votre bras passé</l>
						<l n="4" num="1.4">Vous donnait on ne sait quel air grave et sensé</l>
						<l n="5" num="1.5">Qui vous rendait charmante. Aussi, votre maîtresse</l>
						<l n="6" num="1.6">Vous regardait venir, et fière avec tendresse.</l>
						<l n="7" num="1.7">Opposant votre calme aux rires triomphants,</l>
						<l n="8" num="1.8">Vous montrait pour exemple à son peuple d’enfants ;</l>
						<l n="9" num="1.9">Et du nid studieux l’harmonie argentine</l>
						<l n="10" num="1.10">Poussait à votre vue : « Ondine ! Ondine ! Ondine ! »</l>
						<l n="11" num="1.11">Car vous teniez déjà votre palme à la main,</l>
						<l n="12" num="1.12">Et l’ange du savoir hantait votre chemin.</l>
					</lg>
					<lg n="2">
						<l n="13" num="2.1">Moi, penchée au balcon qui surmontait la rue,</l>
						<l n="14" num="2.2">Comme une sentinelle à son heure accourue.</l>
						<l n="15" num="2.3">Je poursuivais des yeux mon mobile trésor,</l>
						<l n="16" num="2.4">Et disparue enfin je vous voyais encor.</l>
						<l n="17" num="2.5">Vous entraîniez mon âme avec vous, fille aimée,</l>
						<l n="18" num="2.6">Et je vous embrassais par la porte fermée.</l>
					</lg>
					<lg n="3">
						<l n="19" num="3.1">Quel temps ! De tous ces jours d’école et de soleil</l>
						<l n="20" num="3.2">Qui hâtaient la pensée à votre front vermeil,</l>
						<l n="21" num="3.3">De ces flots de peinture et de grâce inspirée,</l>
						<l n="22" num="3.4">L’âme sort-elle heureuse, ô ma douce lettrée ?</l>
						<l n="23" num="3.5">Dites, si quelque femme avec votre candeur</l>
						<l n="24" num="3.6">En passant par la gloire est allée au bonheur ?</l>
					</lg>
					<lg n="4">
						<l n="25" num="4.1">Oh ! que vous me manquiez, jeune âme de mon âme !</l>
						<l n="26" num="4.2">Quel effroi de sentir s’éloigner une flamme</l>
						<l n="27" num="4.3">Que j’avais mise au monde, et qui venait de moi,</l>
						<l n="28" num="4.4">Et qui s’en allait seule : Ondine ! quel effroi !</l>
					</lg>
					<lg n="5">
						<l n="29" num="5.1">Oui, proclamé vainqueur parmi les jeunes filles,</l>
						<l n="30" num="5.2">Quand votre nom montait dans toutes les familles,</l>
						<l n="31" num="5.3">Vos lauriers m’alarmaient à l’ardeur des flambeaux :</l>
						<l n="32" num="5.4">Ils cachaient vos cheveux que j’avais faits si beaux !</l>
						<l n="33" num="5.5">Non, voile plus divin, non, plus riche parure</l>
						<l n="34" num="5.6">N’a jamais d’un enfant ombragé la figure.</l>
						<l n="35" num="5.7">Sur ce flot ruisselant qui vous gardait du jour</l>
						<l n="36" num="5.8">Le poids d’une couronne oppressait mon amour.</l>
						<l n="37" num="5.9">Vos maîtres étaient fiers et moi j’étais tremblante ;</l>
						<l n="38" num="5.10">J’avais peur d’attiser l’auréole brûlante.</l>
						<l n="39" num="5.11">Et, troublée aux parfums de si précoces fleurs,</l>
						<l n="40" num="5.12">Vois-tu, j’en ai payé l’éclat par bien des pleurs.</l>
						<l n="41" num="5.13">Comprends tout… J’avais vu tant de fleurs consumées !</l>
						<l n="42" num="5.14">Tant de mères mourir, de leur amour blâmées !</l>
						<l n="43" num="5.15">Ne sachant bien qu’aimer je priais Dieu pour vous,</l>
						<l n="44" num="5.16">Pour qu’il te gardât simple et tendre comme nous ;</l>
						<l n="45" num="5.17">Et toi tu souriais intrépide à m’apprendre</l>
						<l n="46" num="5.18">Ce que Dieu t’ordonnait, ce qu’il fallait comprendre.</l>
						<l n="47" num="5.19">Muse, aujourd’hui, dis-nous dans ta pure candeur</l>
						<l n="48" num="5.20">Si Dieu te l’ordonnait du moins pour ton bonheur ?</l>
					</lg>
				</div></body></text></TEI>