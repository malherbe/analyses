<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES INÉDITES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3950 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies inédites</title>
						<author>Madame Desbordes-Valmore</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Inédites</title>
								<author>Madame Desbordes-Valmore</author>
								<editor>Gustave Revilliod</editor>
								<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
								<imprint>
									<pubPlace>Genève</pubPlace>
									<publisher>Impr. de J. Fick</publisher>
									<date when="1860">1860</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>3950 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1860">1860</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les corrections mentionnées en errata ont été intégrées.</p>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ENFANTS ET JEUNES FILLES</head><div type="poem" key="DES419">
					<head type="main">LE NUAGE ET L’ENFANT</head>
					<lg n="1">
						<l n="1" num="1.1">L’enfant disait au nuage</l>
						<l n="2" num="1.2">« Attends-moi jusqu’à demain,</l>
						<l n="3" num="1.3">Et par le même chemin</l>
						<l n="4" num="1.4">Nous nous mettrons en voyage.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">« Toi, sous tes belles lueurs ;</l>
						<l n="6" num="2.2">Moi, dans les champs pleins de fleurs,</l>
						<l n="7" num="2.3">Sur le cheval de mon père :</l>
						<l n="8" num="2.4">Nous irons vite, j’espère !</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">« Je m’y tiens bien, tu verras !</l>
						<l n="10" num="3.2">J’y monte seul à la porte ;</l>
						<l n="11" num="3.3">Et quand mon père m’emporte,</l>
						<l n="12" num="3.4">Je n’ai pas peur dans ses bras.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">« Quand il fait beau, comme un guide,</l>
						<l n="14" num="4.2">En tête il me fait asseoir ;</l>
						<l n="15" num="4.3">Toi, d’en haut tu pourrais voir</l>
						<l n="16" num="4.4">Comme je tiens bien la bride !</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">« Ah ! je voudrais d’ici là</l>
						<l n="18" num="5.2">Ne faire qu’une enjambée</l>
						<l n="19" num="5.3">Sur la nuit toute tombée,</l>
						<l n="20" num="5.4">Pour te dire : Me voilà !</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1">« Mais je vais faire un beau rêve</l>
						<l n="22" num="6.2">Où je rêverai de toi ;</l>
						<l n="23" num="6.3">Jusqu’à ce que Dieu l’achève,</l>
						<l n="24" num="6.4">Ami nuage, attends-moi !</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1">Comme il jetait les paroles</l>
						<l n="26" num="7.2">De ses espérances folles,</l>
						<l n="27" num="7.3">Le nuage décevant</l>
						<l n="28" num="7.4">Glissait, poussé par le vent.</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1">Pourtant le bambin sautille,</l>
						<l n="30" num="8.2">L’oiseau chauffe, l’eau scintille,</l>
						<l n="31" num="8.3">Et l’écho lui sonne au cœur :</l>
						<l n="32" num="8.4">« Demain ! demain ! quel bonheur ! »</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1">Enfin le soleil se couche</l>
						<l n="34" num="9.2">Et son baiser qui le touche</l>
						<l n="35" num="9.3">D’un voile ardent clôt ses yeux</l>
						<l n="36" num="9.4">Qu’il tenait ouverts aux cieux.</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1">Près de rentrer chez sa mère,</l>
						<l n="38" num="10.2">Au voyageur éphémère</l>
						<l n="39" num="10.3">L’enfant veut parler encor,</l>
						<l n="40" num="10.4">Mais le beau fantôme d’or</l>
					</lg>
					<lg n="11">
						<l n="41" num="11.1">N’est plus qu’une vapeur grise</l>
						<l n="42" num="11.2">Qu’avec un cri de surprise,</l>
						<l n="43" num="11.3">L’enfant qu’il vient d’éblouir</l>
						<l n="44" num="11.4">Voit fondre et s’évanouir.</l>
					</lg>
					<lg n="12">
						<l n="45" num="12.1">Au cri de la petite âme,</l>
						<l n="46" num="12.2">S’est élancée une femme</l>
						<l n="47" num="12.3">Qui, le voyant sauf et sain,</l>
						<l n="48" num="12.4">Boudeur l’emporte à son sein.</l>
					</lg>
					<lg n="13">
						<l n="49" num="13.1">Plaintif, le mignon s’y cache,</l>
						<l n="50" num="13.2">Déclarant ce qui le fâche,</l>
						<l n="51" num="13.3">Que, sans son bel étranger,</l>
						<l n="52" num="13.4">Il ne veut plus voyager !</l>
					</lg>
					<lg n="14">
						<l n="53" num="14.1">« Si tu chéris les nuages,</l>
						<l n="54" num="14.2">Mon amour, pour tes voyages</l>
						<l n="55" num="14.3">Le temps en aura toujours ;</l>
						<l n="56" num="14.4">Il en passe tous les jours.</l>
					</lg>
					<lg n="15">
						<l n="57" num="15.1">— Ce ne sera plus le même,</l>
						<l n="58" num="15.2">Celui-là, mère, je l’aime ! »</l>
						<l n="59" num="15.3">Dit l’enfant, puis il pleura…</l>
						<l n="60" num="15.4">Et la femme soupira.</l>
					</lg>
					<closer>
						<dateline>
							<date when="1848">Juin 1848</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>