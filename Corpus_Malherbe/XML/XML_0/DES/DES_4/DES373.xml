<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES INÉDITES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3950 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies inédites</title>
						<author>Madame Desbordes-Valmore</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Inédites</title>
								<author>Madame Desbordes-Valmore</author>
								<editor>Gustave Revilliod</editor>
								<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
								<imprint>
									<pubPlace>Genève</pubPlace>
									<publisher>Impr. de J. Fick</publisher>
									<date when="1860">1860</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>3950 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1860">1860</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les corrections mentionnées en errata ont été intégrées.</p>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">AMOUR</head><div type="poem" key="DES373">
					<head type="main">LA JEUNE COMÉDIENNE</head>
					<head type="sub">À FONTENAY-LES-ROSES</head>
					<lg n="1">
						<l n="1" num="1.1">Légère, on la portait ! C’était comme une fête ;</l>
						<l n="2" num="1.2">Chaque fleur, pour la voir, semblait lever la tête ;</l>
						<l n="3" num="1.3">Le soleil, à pleins feux, ruisselait dans les champs ;</l>
						<l n="4" num="1.4">Une église allumait ses flambeaux et ses chants ;</l>
						<l n="5" num="1.5">Les cieux resplendissaient sans nuage, sans blâme ;</l>
						<l n="6" num="1.6">De la morte charmante ils laissaient passer l’âme.</l>
						<l n="7" num="1.7">Et les hommes en bas marchaient silencieux,</l>
						<l n="8" num="1.8">La rêverie au cœur et l’espérance aux yeux.</l>
						<l n="9" num="1.9">Plus loin, des moissonneurs penchés sur leur faucille,</l>
						<l n="10" num="1.10">Devinaient et plaignaient ce poids de jeune fille</l>
						<l n="11" num="1.11">Au deuil blanc ; car, pressé de vivre et de souffrir,</l>
						<l n="12" num="1.12">L’homme partout s’attarde à regarder mourir.</l>
					</lg>
					<lg n="2">
						<l n="13" num="2.1">Jamais le mois brûlant n’avait vu tant de roses !</l>
						<l n="14" num="2.2">Pour de plus doux emplois elles semblaient écloses.</l>
						<l n="15" num="2.3">Le chemin les jetait sous les pieds de l’enfant</l>
						<l n="16" num="2.4">Couché, qu’on enlevait de ce sol triomphant.</l>
						<l n="17" num="2.5">Cet immobile enfant venait d’être Laurence,</l>
						<l n="18" num="2.6">Que sa crédule mère appelait <hi rend="ital">Espérance</hi>.</l>
						<l n="19" num="2.7">Oui, la mère est crédule en regardant le jour</l>
						<l n="20" num="2.8">Flotter au fond des yeux de l’enfant, son amour !</l>
						<l n="21" num="2.9">C’est trop peu d’une vie à cette âme qui s’ouvre :</l>
						<l n="22" num="2.10">C’est une éternité que la mère y découvre.</l>
						<l n="23" num="2.11">L’éternité fuyait pour ne plus revenir ;</l>
						<l n="24" num="2.12">Laurence avait changé de route et d’avenir.</l>
					</lg>
					<lg n="3">
						<l n="25" num="3.1">La veille elle avait dit : « Six vierges couronnées,</l>
						<l n="26" num="3.2">« Dont les âmes au mal ne se sont pas données,</l>
						<l n="27" num="3.3">« Demain, le long des bleds, mèneront le convoi,</l>
						<l n="28" num="3.4">« Tendront mon dernier voile et prieront Dieu pour moi.</l>
						<l n="29" num="3.5">« Pour moi, s’il est un coin, parmi les hautes herbes,</l>
						<l n="30" num="3.6">« Que ne visitent pas les charités superbes,</l>
						<l n="31" num="3.7">« Un coin vert, où jamais on n’entend rien gémir,</l>
						<l n="32" num="3.8">« J’y voudrais bien aller ! j’y voudrais bien dormir !</l>
						<l n="33" num="3.9">« S’il vous plaît, qu’on m’y porte ! Il me faut du silence ;</l>
						<l n="34" num="3.10">« Un saule au doux frisson, que l’air baigne et balance.</l>
						<l n="35" num="3.11">« Sur nous, si Dieu le veut, l’aurore passera,</l>
						<l n="36" num="3.12">« Et parmi le vent frais l’oiseau seul chantera.</l>
						<l n="37" num="3.13">« Tant de bruits sur la terre ont étourdi mon âme !</l>
						<l n="38" num="3.14">« Oui, c’est une pitié d’y naître pauvre et femme.</l>
						<l n="39" num="3.15">« Ne me démentez pas, corrupteurs… ah ! pardon !</l>
						<l n="40" num="3.16">« Vivez ! j’ai pris sur moi la faute et l’abandon.</l>
						<l n="41" num="3.17">« J’ai bien assez souffert pour que Dieu vous pardonne !</l>
						<l n="42" num="3.18">« Vivez : tous mes pardons à moi, je vous les donne.</l>
						<l n="43" num="3.19">« Mais si quelque autre enfant la voix pleine de pleurs</l>
						<l n="44" num="3.20">« Vient chanter devant vous, ne souillez plus ses fleurs.</l>
						<l n="45" num="3.21">« Paix ! Éloignez d’ici cette musique affreuse…</l>
						<l n="46" num="3.22">« Fermez tout… là, c’est bien. Ô Vierge généreuse,</l>
						<l n="47" num="3.23">« Je ne veux plus entendre et regarder que vous :</l>
						<l n="48" num="3.24">« Oh ! que vous êtes calme !… Oh ! que vous suivre est doux !… »</l>
					</lg>
					<lg n="4">
						<l n="49" num="4.1">Puis elle regarda fixe et droit devant elle,</l>
						<l n="50" num="4.2">Tandis que de ses yeux la mémoire infidèle</l>
						<l n="51" num="4.3">S’effaçait, comme on voit, aux approches du soir,</l>
						<l n="52" num="4.4">Par degrés se ternir les clartés d’un miroir.</l>
						<l n="53" num="4.5">Un sourire y passa, mais un sourire étrange :</l>
						<l n="54" num="4.6">On eût dit qu’auprès d’elle invisible, un autre ange</l>
						<l n="55" num="4.7">Détournait de sa bouche, où la vie hésitait,</l>
						<l n="56" num="4.8">Une coupe inutile à l’espoir qui mentait.</l>
						<l n="57" num="4.9">— « Non, je ne veux plus boire ; assez, cria Laurence,</l>
						<l n="58" num="4.10">« Assez, je n’ai plus soif. » Et tout devint silence.</l>
					</lg>
					<lg n="5">
						<l n="59" num="5.1">Les pauvres, sur leurs doigts, comptaient ses jeunes jours</l>
						<l n="60" num="5.2">Disant qu’elle était sainte, ayant donné toujours.</l>
						<l n="61" num="5.3">Toujours elle donnait, cette belle indigente,</l>
						<l n="62" num="5.4">Madeleine insultée et comme elle indulgente.</l>
						<l n="63" num="5.5">Dans son rêve fuyant sillonné d’un peu d’or,</l>
						<l n="64" num="5.6">Elle étendait les mains croyant donner encor.</l>
					</lg>
					<lg n="6">
						<l n="65" num="6.1">Mais quoi, le rossignol soulevé dans la brise</l>
						<l n="66" num="6.2">S’en retournait à Dieu par l’arceau d’une église,</l>
						<l n="67" num="6.3">Et sous tant de bouquets jetés sur son départ,</l>
						<l n="68" num="6.4">Seul, de tout ce printemps, ne prenait plus sa part.</l>
						<l n="69" num="6.5">Et comme s’en allait ce lumineux cortège,</l>
						<l n="70" num="6.6">En chantant : « Que le Dieu qui mourut la protège ! ».</l>
						<l n="71" num="6.7">Prise d’un souvenir qui me serrait la voix,</l>
						<l n="72" num="6.8">Je criai, sans parler : « Qu’est-ce donc que je vois ! »</l>
					</lg>
					<lg n="7">
						<l n="73" num="7.1">Alors, posant ma main où la douleur s’élance,</l>
						<l n="74" num="7.2">Je ressentis au cœur comme un grand coup de lance,</l>
						<l n="75" num="7.3">Tel que le recevra tout pauvre cœur humain</l>
						<l n="76" num="7.4">Devant ces corps d’enfant tombés par le chemin.</l>
						<l n="77" num="7.5">Appelant par son nom la douce pardonnée.</l>
						<l n="78" num="7.6">Presque sans le vouloir je marchais consternée,</l>
						<l n="79" num="7.7">Puis, rêvant son front pâle et naguère adoré,</l>
						<l n="80" num="7.8">La force abandonna mon corps,… et je pleurai.</l>
					</lg>
					<lg n="8">
						<l n="81" num="8.1">Pourtant, l’atome ailé dont le vol se déploie</l>
						<l n="82" num="8.2">Traçait au fond de l’air mille cercles de joie.</l>
						<l n="83" num="8.3">L’hirondelle au bec noir acclamait son retour ;</l>
						<l n="84" num="8.4">Le cri des coqs lointains sonnait l’heure et l’amour ;</l>
						<l n="85" num="8.5">Là bas, des ramiers blancs flottaient à longues voiles</l>
						<l n="86" num="8.6">Et semblaient, en plein jour, de filantes étoiles ;</l>
						<l n="87" num="8.7">L’arrêt n’avait frappé que sur un jeune sort</l>
						<l n="88" num="8.8">Qui, soumis, s’éteignait sous les doigts de la mort.</l>
					</lg>
					<lg n="9">
						<l n="89" num="9.1">Dans ce grand requiem formé parla nature.</l>
						<l n="90" num="9.2">Six voix d’enfants poussaient leurs élans sans culture</l>
						<l n="91" num="9.3">Au fond des bois ombreux mille oiseaux s’ébattaient,</l>
						<l n="92" num="9.4">Et l’on eût dit au loin que les arbres chantaient.</l>
					</lg>
					<lg n="10">
						<l n="93" num="10.1">Quand la nuit s’étendit sur l’ardent paysage,</l>
						<l n="94" num="10.2">Quand tout bruit s’effaça, l’astre au tendre visage</l>
						<l n="95" num="10.3">Vers une croix nouvelle allongea ses fils d’or</l>
						<l n="96" num="10.4">Comme un baiser de mère à son enfant qui dort.</l>
					</lg>
					<lg n="11">
						<l n="97" num="11.1">Dormez, dormez, jeunesse, apaisez vos orages !</l>
						<l n="98" num="11.2">Que tout vous soit repos sous ces chastes ombrages !</l>
						<l n="99" num="11.3">Nuls vices ne viendront vous tenter en ce lieu ;</l>
						<l n="100" num="11.4">Germez dans l’espérance, et laissez faire à Dieu !</l>
					</lg>
				</div></body></text></TEI>