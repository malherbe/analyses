<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES INÉDITES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3950 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies inédites</title>
						<author>Madame Desbordes-Valmore</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Inédites</title>
								<author>Madame Desbordes-Valmore</author>
								<editor>Gustave Revilliod</editor>
								<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
								<imprint>
									<pubPlace>Genève</pubPlace>
									<publisher>Impr. de J. Fick</publisher>
									<date when="1860">1860</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>3950 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1860">1860</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les corrections mentionnées en errata ont été intégrées.</p>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ENFANTS ET JEUNES FILLES</head><div type="poem" key="DES417">
					<head type="main">LE PETIT MÉCONTENT</head>
					<lg n="1">
						<l n="1" num="1.1">Mère, je veux crier et faire un grand tapage.</l>
						<l n="2" num="1.2">Comment, je ne peux pas tous les jours être sage !</l>
						<l n="3" num="1.3">Non, mère, c’est trop long tous les jours, tous les jours !</l>
						<l n="4" num="1.4">Le monsieur l’a bien dit : « Rien ne dure toujours. »</l>
						<l n="5" num="1.5">Tant mieux ! je vais m’enfuir et crier comme George.</l>
						<l part="I" n="6" num="1.6">Qui m’en empêchera ? </l>
						<l part="F" n="6" num="1.6">— Personne. À pleine gorge,</l>
						<l n="7" num="1.7">Vous pouvez, cher ami, vous donner ce régal.</l>
						<l part="I" n="8" num="1.8">Mais vous serez malade… </l>
						<l part="F" n="8" num="1.8">— Oh ! cela m’est égal :</l>
						<l part="I" n="9" num="1.9">George ne meurt jamais. </l>
						<l part="F" n="9" num="1.9">— George afflige sa mère.</l>
						<l n="10" num="1.10">Un enfant mal appris est une joie amère.</l>
						<l part="I" n="11" num="1.11">— Je reviendrai t’aimer. </l>
						<l part="F" n="11" num="1.11">— M’aimer sans m’obéir ?</l>
						<l n="12" num="1.12">Déserter ton devoir, enfant, c’est me trahir.</l>
						<l n="13" num="1.13">Je crains, moi, qu’avant peu personne ne vous aime,</l>
						<l n="14" num="1.14">Et vous vous ferez peur tout seul avec vous-même.</l>
						<l n="15" num="1.15">— Non ! George n’a pas peur dans le cabinet noir.</l>
						<l n="16" num="1.16">Il dit que c’est tout brun comme quand c’est le soir ;</l>
						<l n="17" num="1.17">Pas plus. Et puis il chante à travers la serrure ;</l>
						<l n="18" num="1.18">Il se moque des grands, il fait le coq, il jure.</l>
						<l n="19" num="1.19">C’est brave de chanter sans jour et sans flambeau !</l>
						<l part="I" n="20" num="1.20">Je veux être méchant pour voir. </l>
						<l part="F" n="20" num="1.20">— Ce sera beau !</l>
						<l part="I" n="21" num="1.21">— Je veux être grondé : gronde donc. </l>
						<l part="F" n="21" num="1.21">— Pourquoi faire ?</l>
						<l part="I" n="22" num="1.22">Vous me faites pitié. </l>
						<l part="F" n="22" num="1.22">— Je suis las de me taire !</l>
						<l n="23" num="1.23">J’ai cassé mon cheval ; j’ai mis de l’encre à tout ;</l>
						<l part="I" n="24" num="1.24">Regarde ma figure ! </l>
						<l part="F" n="24" num="1.24">— Oui, c’est laid jusqu’au bout.</l>
						<l n="25" num="1.25">Mais qui vous a donné ce faux air de courage ?</l>
						<l n="26" num="1.26">Hier encor, priant Dieu qu’il vous rendît bien sage,</l>
						<l n="27" num="1.27">Vous vouliez ressembler à notre vieux cousin.</l>
						<l n="28" num="1.28">— Je n’avais pas été chez le petit voisin.</l>
						<l n="29" num="1.29">Il bat des pieds très-bien quand on le contrarie ;</l>
						<l n="30" num="1.30">Il ne dit pas bonjour, même quand on l’en prie !…</l>
						<l n="31" num="1.31">Ah ! ah ! c’est qu’on est fier d’être mis en prison !</l>
						<l n="32" num="1.32">— Beaucoup de grands enfants y perdent la raison.</l>
						<l n="33" num="1.33">Pour leurs mères surtout c’est une triste gloire !</l>
						<l n="34" num="1.34">Restez libre et soumis, si vous voulez m’en croire.</l>
						<l n="35" num="1.35">Moi, je n’ai point de cage où mettre mon enfant ;</l>
						<l n="36" num="1.36">Pas même les oiseaux, mon cœur me le défend.</l>
						<l n="37" num="1.37">Vous n’obtiendrez de moi ni prison, ni colère,</l>
						<l n="38" num="1.38">Et j’attendrai, de loin, que le temps vous éclaire.</l>
						<l part="I" n="39" num="1.39">— De loin ? </l>
						<l part="F" n="39" num="1.39">— Battez des pieds, poussez des cris affreux,</l>
						<l n="40" num="1.40">Devenez comme George un petit malheureux.</l>
						<l part="I" n="41" num="1.41">Vous en aurez la honte au grand jour. </l>
						<l part="F" n="41" num="1.41">— Quelle honte ?</l>
						<l part="I" n="42" num="1.42">George rit ; je rirai… </l>
						<l part="F" n="42" num="1.42">— Nous voici loin de compte.</l>
						<l n="43" num="1.43">Si vous ne craignez pas de rougir devant Dieu,</l>
						<l n="44" num="1.44">Il faudra, mon enfant, bientôt nous dire adieu.</l>
						<l n="45" num="1.45">À vivre sans honneur, moi, je ne puis prétendre,</l>
						<l n="46" num="1.46">Et si vous n’étiez pas ma gloire la plus tendre,</l>
						<l n="47" num="1.47">À la mère de George il faudrait ressembler.</l>
						<l part="I" n="48" num="1.48">— Oh ! non, ressemble-toi ! </l>
						<l part="F" n="48" num="1.48">— Son sort me fait trembler.</l>
						<l n="49" num="1.49">Loin de la saluer, quand cette femme passe.</l>
						<l n="50" num="1.50">On se détourne d’elle, on lui fait de l’espace,</l>
						<l n="51" num="1.51">On va de porte en porte en chuchotant tout bas :</l>
						<l n="52" num="1.52">«Elle a gâté son fruit, ne la saluons pas ! »</l>
						<l n="53" num="1.53">Le fruit accuse l’arbre, et l’on juge, et le blâme</l>
						<l n="54" num="1.54">Tombera sur la mère et non sur la jeune âme</l>
						<l n="55" num="1.55">Qu’elle a laissé corrompre. On est plein de rigueur.</l>
						<l part="I" n="56" num="1.56">— Que dit-on de la dame ? </l>
						<l part="F" n="56" num="1.56">— On dit qu’elle est sans cœur</l>
						<l n="57" num="1.57">Voyez comme elle est triste au fond de sa faiblesse !</l>
						<l n="58" num="1.58">Le monde la méprise et son enfant la blesse !</l>
						<l n="59" num="1.59">Ô mère humiliée en votre unique amour,</l>
						<l n="60" num="1.60">Je vous plaignis souvent : me plaindrez-vous un jour ?</l>
						<l n="61" num="1.61">— Pardon !… je ne veux pas te voir humiliée…</l>
						<l n="62" num="1.62">Pardon ! pardon ! Je veux que tu sois saluée !</l>
						<l n="63" num="1.63">Mère, je serai bon comme le vieux cousin !</l>
						<l n="64" num="1.64">Mère, je n’irai plus chez le petit voisin ! »</l>
						<l n="65" num="1.65">La mère tressaillit dans une vive étreinte ;</l>
						<l n="66" num="1.66">L’enfant ne cria plus ; il fut bon sans contrainte.</l>
						<l n="67" num="1.67">Et quand on saluait cette mère en chemin,</l>
						<l n="68" num="1.68">Il rougissait de joie et lui serrait la main !</l>
					</lg>
				</div></body></text></TEI>