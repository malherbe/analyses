<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES INÉDITES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3950 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies inédites</title>
						<author>Madame Desbordes-Valmore</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Inédites</title>
								<author>Madame Desbordes-Valmore</author>
								<editor>Gustave Revilliod</editor>
								<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
								<imprint>
									<pubPlace>Genève</pubPlace>
									<publisher>Impr. de J. Fick</publisher>
									<date when="1860">1860</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>3950 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1860">1860</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les corrections mentionnées en errata ont été intégrées.</p>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">FAMILLE</head><div type="poem" key="DES401">
					<head type="main">LA VOIX PERDUE</head>
					<head type="sub_2">(Ma fille Inès.)</head>
					<lg n="1">
						<head>La jeune fille</head>
						<l n="1" num="1.1">Ma mère, entendez-vous, quand la lune est levée,</l>
						<l n="2" num="1.2">L’oiseau qui la salue en veillant sa couvée ?</l>
						<l n="3" num="1.3">Ne fait-il pas rêver les arbres endormis ?</l>
						<l n="4" num="1.4">Pourquoi chante-t-il seul ! Il n’a donc pas d’amis ?</l>
					</lg>
					<lg n="2">
						<head>La mère</head>
						<l n="5" num="2.1">Il en a ! Des bannis il soulage la route ;</l>
						<l n="6" num="2.2">Dans tous ces nids couchés on le bénit sans doute.</l>
						<l n="7" num="2.3">Il parle à quelque mère humble et pareille à moi,</l>
						<l n="8" num="2.4">À quelque enfant sauvage et charmant comme toi.</l>
					</lg>
					<lg n="3">
						<head>La jeune fille</head>
						<l n="9" num="3.1">Que je l’aime ! Avec nous que je voudrais le prendre !</l>
						<l n="10" num="3.2">Tout ce qu’il chante à Dieu que je voudrais l’apprendre !</l>
						<l n="11" num="3.3">Lui, s’il voulait venir, heureux dans notre amour,</l>
						<l n="12" num="3.4">Nous lui ferions aimer le monde et le grand jour,</l>
					</lg>
					<lg n="4">
						<head>La mère</head>
						<l n="13" num="4.1">Il mourrait. Son destin est d’être solitaire ;</l>
						<l n="14" num="4.2">De jeter ses sanglots, libre, entre ciel et terre ;</l>
						<l n="15" num="4.3">D’attacher sa compagne, humble et pareille à moi,</l>
						<l n="16" num="4.4">À son doux nid sauvage et charmant comme toi !</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">On a dit qu’autrefois, au sein d’une famille,</l>
						<l n="18" num="5.2">Il vécut sous un front brûlant de jeune fille.</l>
						<l n="19" num="5.3">Cet être harmonieux aimait l’ombre et les fleurs ;</l>
						<l n="20" num="5.4">Nul ne pouvait l’entendre et retenir ses pleurs.</l>
						<l n="21" num="5.5">Rossignol, il chantait aux errantes étoiles ;</l>
						<l n="22" num="5.6">Jeune fille, il pleurait, dérobé sous ses voiles.</l>
					</lg>
					<lg n="6">
						<head>La jeune fille</head>
						<l part="I" n="23" num="6.1">Et la mère ? </l>
					</lg>
					<lg n="7">
						<head>La mère</head>
						<l part="F" n="23">Était tendre et fière autant que moi</l>
						<l n="24" num="7.1">De son enfant sauvage et charmant comme toi !</l>
					</lg>
					<lg n="8">
						<head>La jeune fille</head>
						<l part="I" n="25" num="8.1">Après ? </l>
					</lg>
					<lg n="9">
						<head>La mère</head>
						<l part="F" n="25">De ce front pâle où frissonnaient ses ailes</l>
						<l n="26" num="9.1">L’oiseau voulait sortir et s’envoler par elles.</l>
						<l n="27" num="9.2">Un jour, forçant le voile où gémissait sa voix,</l>
						<l n="28" num="9.3">Il emporta le timbre et s’enfuit dans les bois.</l>
					</lg>
					<lg n="10">
						<head>La jeune fille</head>
						<l part="I" n="29" num="10.1">Après ?… </l>
					</lg>
					<lg n="11">
						<head>La mère</head>
						<l part="F" n="29">L’enfant rêveur n’aima plus qu’en silence,</l>
						<l n="30" num="11.1">Cherchant toujours le saule où l’oiseau se balance.</l>
					</lg>
					<lg n="12">
						<head>La jeune fille</head>
						<l part="I" n="31" num="12.1">Et la mère ? </l>
					</lg>
					<lg n="13">
						<head>La mère</head>
						<l part="F" n="31">Suivit, tendre et pareille à moi,</l>
						<l n="32" num="13.1">Son doux enfant muet et charmant comme toi !</l>
					</lg>
				</div></body></text></TEI>