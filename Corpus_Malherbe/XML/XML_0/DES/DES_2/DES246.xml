<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES PLEURS</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2802 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>2802 vers9617 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>L’Aurore en fuite (poèmes choisis)</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition établie par Christine Planté</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions Points</publisher>
						<date when="2010">2010</date>
					</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Ajout de l’attribut rhyme="none" dans le poème MINUIT (lacune confirmée par l’éditon Bertrand 1973).</p>
				<p>Ajout de l’attribut rhyme="none" pour un vers dans le poème AGAR.</p>
				<p>Ajout de l’attribut rhyme="none" pour un vers dans le poème LE PETIT RIEUR.</p>
				<p>Ajout d’un vers dans le poème LOUISE LABÉ (voir l’édition  Bertrand 1973).</p>
				<p>Ajout d’un vers dans le poème Révélation (voir l’édition de 2010).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
				<change when="2018-09-03" who="RR">Ajout de l’attribut rhyme="none" pour les vers sans rime.</change>
				<change when="2018-09-03" who="RR">Ajout des vers manquants.</change>
				<change when="2018-11-02" who="RR">Ajout des vers manquants.</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DES246">
					<head type="main">LOUISE LABÉ</head>
					<opener>
						<epigraph>
							<cit>
								<quote>
									Tant que mes yeux pourront larmes espandre <lb></lb>
									À l’heur passé avec toy regretter ; <lb></lb>
									Et qu’aux sanglots et soupirs résister, <lb></lb>
									Pourra ma voix, et un peu faire entendre ; <lb></lb>
									Tant que ma main pourra les cordes tendre <lb></lb>
									Du mignard lut, pour tes grâces chanter ; <lb></lb>
									Tant que l’esprit voudra se contenter <lb></lb>
									De ne vouloir rien fors que toi comprendre ; <lb></lb>
									Je ne souhaitte encore point mourir. <lb></lb>
									Mais quand mes yeux je sentiray tarir, <lb></lb>
									Ma voix cassée, et ma main impuissante, <lb></lb>
									Et mon esprit en ce mortel séjour <lb></lb>
									Ne pouvant plus montrer sygne d’amante, <lb></lb>
									Priray la mort noircir mon plus cher jour.
								</quote>
								 <bibl><hi rend="smallcap">Louise Labé</hi></bibl>
							</cit>
							<cit>
								<quote>
									Quand vous lirez, ô dames lionnoises ! <lb></lb>
									Les miens écrits pleins d’amoureuses noises ; <lb></lb>
									Quand mes regrets, ennuis, dépits et larmes, <lb></lb>
									M’orrez chanter en pitoyables carmes, <lb></lb>
									Ne veuillez point condamner ma simplesse, <lb></lb>
									Et jeune erreur de ma folle jeunesse, <lb></lb>
									Si c’est erreur !
								</quote>
							</cit>
						</epigraph>
					</opener>
					<lg n="1">
						<l n="1" num="1.1">Quoi ! c’est là ton berceau, poétique Louise !</l>
						<l n="2" num="1.2">Mélodieux enfant, fait d’amour et d’amour,</l>
						<l n="3" num="1.3">Et d’âme, et d’âme encore, et de mollesse exquise ;</l>
						<l n="4" num="1.4">Quoi ! c’est là que ta vie a pris l’air et le jour !</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">Quoi ! les murs étouffants de cette étroite rue</l>
						<l n="6" num="2.2">Ont laissé, sans l’éteindre, éclore ta raison ?</l>
						<l n="7" num="2.3">Quoi ! c’est là qu’a brillé ta lampe disparue ?</l>
						<l n="8" num="2.4">La jeune perle ainsi colore sa prison !</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">Où posais-tu tes pieds délicats et sensibles,</l>
						<l n="10" num="3.2">Sur le sol irrité que j’effleure en tremblant ?</l>
						<l n="11" num="3.3">Quel ange, aplanissant ces sentiers impossibles,</l>
						<l n="12" num="3.4">A soutenu ton vol sur leur pavé brûlant ?</l>
						<l n="13" num="3.5">Oh ! les cailloux aigus font chanceler la grâce ;</l>
						<l n="14" num="3.6">Ici l’enfance, lente et craintive à souffrir,</l>
						<l n="15" num="3.7">Pour s’élancer aux fleurs, pour en chercher la trace,</l>
						<l n="16" num="3.8">En sortant du berceau, n’apprend pas à courir.</l>
						<l n="17" num="3.9">Paresseuse, elle marche ; et sa timide joie</l>
						<l n="18" num="3.10">Ressemble au papillon sur l’épine arrêté,</l>
						<l n="19" num="3.11"><choice reason="analysis" type="missing" hand="RR"><sic> </sic><corr source="édition_1973">Son aile s’y déchire avant qu’il ne la voie,</corr></choice></l>
						<l n="20" num="3.12">À son instinct rôdeur il boude tout l’été.</l>
						<l n="21" num="3.13">As-tu vu ce radeau, longue et mouvante rue,</l>
						<l n="22" num="3.14">Qui s’enfuit sur le dos du fleuve voyageur ?</l>
						<l n="23" num="3.15">Osais-tu regarder, de mille ondes accrue,</l>
						<l n="24" num="3.16">Cette onde qui surgit comme un fléau vengeur !</l>
					</lg>
					<lg n="4">
						<l n="25" num="4.1">Non, ce n’est pas ainsi que je rêvais ta cage,</l>
						<l n="26" num="4.2">Fauvette à tête blonde, au chant libre et joyeux ;</l>
						<l n="27" num="4.3">Je suspendais ton aile à quelque frais bocage,</l>
						<l n="28" num="4.4">Plein d’encens et de jour aussi doux que tes yeux !</l>
						<l n="29" num="4.5">Et le Rhône en colère, et la Saône dormante,</l>
						<l n="30" num="4.6">N’avaient point baptisé tes beaux jours tramés d’or ;</l>
						<l n="31" num="4.7">Dans un cercle de feu tourmentée et charmante,</l>
						<l n="32" num="4.8">J’ai cru qu’avec des fleurs tu décrivais ton sort,</l>
						<l n="33" num="4.9">Et que ton aile au vent n’était point arrêtée</l>
						<l n="34" num="4.10">Sous ces réseaux de fer aux rigides couleurs ;</l>
						<l n="35" num="4.11">Et que tu respirais la tristesse enchantée</l>
						<l n="36" num="4.12">Que la paix du désert imprime aux jeunes fleurs ;</l>
						<l n="37" num="4.13">Que tu livrais aux flots tes amoureuses larmes,</l>
						<l n="38" num="4.14">Miroir pur et profond qu’interrogeaient tes charmes ;</l>
						<l n="39" num="4.15">Et que tes vers émus, nés d’un frais souvenir,</l>
						<l n="40" num="4.16">S’en allaient sans efforts chanter dans l’avenir !</l>
					</lg>
					<lg n="5">
						<l n="41" num="5.1"><space quantity="6" unit="char"></space>Mais tu vivais d’une flamme</l>
						<l n="42" num="5.2"><space quantity="6" unit="char"></space>Raillée en ce froid séjour ;</l>
						<l n="43" num="5.3"><space quantity="6" unit="char"></space>Et tu pleurais de ton âme,</l>
						<l n="44" num="5.4"><space quantity="6" unit="char"></space>Ô Salamandre d’amour !</l>
					</lg>
					<lg n="6">
						<l n="45" num="6.1"><space quantity="6" unit="char"></space>Quand sur les feuilles parlantes</l>
						<l n="46" num="6.2"><space quantity="6" unit="char"></space>Que ton cœur sut embraser,</l>
						<l n="47" num="6.3"><space quantity="6" unit="char"></space>Tu laisses dans un baiser</l>
						<l n="48" num="6.4"><space quantity="6" unit="char"></space>Courir tes larmes brûlantes,</l>
					</lg>
					<lg n="7">
						<l n="49" num="7.1">Ô Louise ! on croit voir l’éphémère éternel</l>
						<l n="50" num="7.2">Filer dans les parfums sa soyeuse industrie ;</l>
						<l n="51" num="7.3">Lorsque, tombé du ciel, son ardente patrie,</l>
						<l n="52" num="7.4">Il en retient dans l’ombre un rayon paternel ;</l>
						<l n="53" num="7.5">Fiévreux, loin du soleil, l’insecte se consume ;</l>
						<l n="54" num="7.6">D’un fil d’or sur lui-même ourdissant la beauté,</l>
						<l n="55" num="7.7">Inaperçu dans l’arbre où le vent l’a jeté,</l>
						<l n="56" num="7.8">Sous un linceul de feu son âme se rallume !</l>
					</lg>
					<lg n="8">
						<l n="57" num="8.1">Oui ! ce sublime atome est le rêve des arts ;</l>
						<l n="58" num="8.2">Oui ! les arts dédaignés meurent en chrysalides,</l>
						<l n="59" num="8.3">Quand la douce chaleur de caressants regards</l>
						<l n="60" num="8.4">Fait pousser par degrés leurs ailes invalides.</l>
						<l n="61" num="8.5">Telle, étonnée et triste au bord de son réveil,</l>
						<l n="62" num="8.6">Quelque jeune Louise, ignorant sa couronne,</l>
						<l n="63" num="8.7">N’ose encor révéler à l’air qui l’environne</l>
						<l n="64" num="8.8">Qu’une âme chante et pleure autour de son sommeil.</l>
						<l n="65" num="8.9">Car tu l’as dit : longtemps un silence invincible,</l>
						<l n="66" num="8.10">Étendu sur ta voix qui s’éveillait sensible,</l>
						<l n="67" num="8.11">Fit mourir dans ton sein des accents tout amour,</l>
						<l n="68" num="8.12">Que tu tremblais d’entendre et de livrer au jour.</l>
					</lg>
					<lg n="9">
						<l n="69" num="9.1">Mais l’amour ! oh ! l’amour se venge d’être esclave.</l>
						<l n="70" num="9.2">Fièvre des jeunes cœurs, orage des beaux jours,</l>
						<l n="71" num="9.3">Qui consume la vie et la promet toujours,</l>
						<l n="72" num="9.4">Indompté sous les nœuds qui lui servent d’entrave,</l>
						<l n="73" num="9.5">Oh ! l’invisible amour circule dans les airs,</l>
						<l n="74" num="9.6">Dans les flots, dans les fleurs, dans les songes de l’âme,</l>
						<l n="75" num="9.7">Dans le jour qui languit trop chargé de sa flamme,</l>
						<l n="76" num="9.8"><space quantity="6" unit="char"></space>Et dans les nocturnes concerts !</l>
					</lg>
					<lg n="10">
						<l n="77" num="10.1">Et tu chantas l’amour ! ce fut ta destinée ;</l>
						<l n="78" num="10.2">Belle ! et femme ! et naïve, et du monde étonnée,</l>
						<l n="79" num="10.3">De la foule qui passe évitant la faveur,</l>
						<l n="80" num="10.4">Inclinant sur ton fleuve un front tendre et rêveur,</l>
						<l n="81" num="10.5">Louise ! tu chantas. À peine de l’enfance</l>
						<l n="82" num="10.6">Ta jeunesse hâtive eut perdu les liens,</l>
						<l n="83" num="10.7">L’amour te prit sans peur, sans débats, sans défense ;</l>
						<l n="84" num="10.8">Il fit tes jours, tes nuits, tes tourments et tes biens !</l>
					</lg>
					<lg n="11">
						<l n="85" num="11.1">Et toujours par ta chaîne au rivage attachée,</l>
						<l n="86" num="11.2">Comme une nymphe triste au milieu des roseaux,</l>
						<l n="87" num="11.3"><space quantity="6" unit="char"></space>Des roseaux à demi cachée,</l>
						<l n="88" num="11.4">Louise ! tu chantas dans les fleurs et les eaux.</l>
					</lg>
					<lg n="12">
						<l n="89" num="12.1">De cette cité sourde, oh ! que l’âme est changée !</l>
						<l n="90" num="12.2">Autrefois tu charmais l’oreille des pasteurs ;</l>
						<l n="91" num="12.3">Autrefois, en passant, d’humbles navigateurs</l>
						<l n="92" num="12.4">Suspendaient à ta voix la rame négligée,</l>
						<l n="93" num="12.5">Et recueillant dans l’air ton rire harmonieux,</l>
						<l n="94" num="12.6">Comme un écho fuyant on les entendait rire ;</l>
						<l n="95" num="12.7"><space quantity="6" unit="char"></space>Car sous tes doigts ingénieux,</l>
						<l n="96" num="12.8">Le luth ému disait tout ce qu’il voulait dire !</l>
					</lg>
					<lg n="13">
						<l n="97" num="13.1">Tout ce que tu voyais de beau dans l’univers,</l>
						<l n="98" num="13.2">N’est-ce pas comme au fond de quelque glace pure,</l>
						<l n="99" num="13.3">Coulait dans ta mémoire et s’y gravait en vers ?</l>
						<l n="100" num="13.4">Oui ! l’âme poétique est une chambre obscure</l>
						<l n="101" num="13.5">Où s’enferme le monde et ses aspects divers !</l>
					</lg>
				</div></body></text></TEI>