<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES PLEURS</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2802 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>2802 vers9617 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>L’Aurore en fuite (poèmes choisis)</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition établie par Christine Planté</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions Points</publisher>
						<date when="2010">2010</date>
					</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Ajout de l’attribut rhyme="none" dans le poème MINUIT (lacune confirmée par l’éditon Bertrand 1973).</p>
				<p>Ajout de l’attribut rhyme="none" pour un vers dans le poème AGAR.</p>
				<p>Ajout de l’attribut rhyme="none" pour un vers dans le poème LE PETIT RIEUR.</p>
				<p>Ajout d’un vers dans le poème LOUISE LABÉ (voir l’édition  Bertrand 1973).</p>
				<p>Ajout d’un vers dans le poème Révélation (voir l’édition de 2010).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
				<change when="2018-09-03" who="RR">Ajout de l’attribut rhyme="none" pour les vers sans rime.</change>
				<change when="2018-09-03" who="RR">Ajout des vers manquants.</change>
				<change when="2018-11-02" who="RR">Ajout des vers manquants.</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DES245">
					<head type="main">LA MÉMOIRE</head>
					<opener>
						<epigraph>
							<cit>
								<quote>
									Hélas ! qu’est-ce que l’amour, si ce n’est une douleur ?
								</quote>
								 <bibl><hi rend="smallcap">Byron.</hi></bibl>
							</cit>
						</epigraph>
					</opener>
					<lg n="1">
						<l n="1" num="1.1">Tais-toi, ma sœur ! le passé brûle.</l>
						<l n="2" num="1.2">Son nom, c’est lui ; ne le dis plus :</l>
						<l n="3" num="1.3">Se reprendre à des biens perdus,</l>
						<l n="4" num="1.4">C’est marcher au flot qui recule.</l>
						<l n="5" num="1.5">Empreint d’une ardente douceur,</l>
						<l n="6" num="1.6">À peine effleure-t-il ma bouche,</l>
						<l n="7" num="1.7">Comme une flamme qui me touche,</l>
						<l n="8" num="1.8">Ce nom brûle… Tais-toi, ma sœur.</l>
					</lg>
					<lg n="2">
						<l n="9" num="2.1">Femme, tu vois un cœur de femme</l>
						<l n="10" num="2.2">Au fond de nos yeux consternés,</l>
						<l n="11" num="2.3">Lorsqu’à s’éteindre condamnés,</l>
						<l n="12" num="2.4">Trop de fièvre en usa la flamme.</l>
						<l n="13" num="2.5">Au mal qui fait longtemps souffrir,</l>
						<l n="14" num="2.6">Crois-moi, l’homme est plus inflexible ;</l>
						<l n="15" num="2.7">Il nous défend d’être sensible :</l>
						<l n="16" num="2.8">Il ne défend pas d’en mourir !</l>
					</lg>
					<lg n="3">
						<l n="17" num="3.1">Ce qu’il sait de science amère</l>
						<l n="18" num="3.2">Pour mentir à son propre amour ;</l>
						<l n="19" num="3.3">Ce qu’il peut inventer un jour</l>
						<l n="20" num="3.4">Contre son idole éphémère ;</l>
						<l n="21" num="3.5">Ce que j’ai ressenti tout bas</l>
						<l n="22" num="3.6">De sa haine… ou de son délire,</l>
						<l n="23" num="3.7">Tout haut je ne veux pas le dire,</l>
						<l n="24" num="3.8">Pour que Dieu ne me venge pas !</l>
					</lg>
					<lg n="4">
						<l n="25" num="4.1">Car j’ai là comme une prière</l>
						<l n="26" num="4.2">Qui pleure pour lui nuit et jour ;</l>
						<l n="27" num="4.3">C’est la charité dans l’amour,</l>
						<l n="28" num="4.4">Ou c’est sa parole première.</l>
						<l n="29" num="4.5">Qu’elle enfermait d’âme et de foi,</l>
						<l n="30" num="4.6">Sa voix jeune et si tôt parjure !</l>
						<l n="31" num="4.7">J’en parle à Dieu sans son injure,</l>
						<l n="32" num="4.8">Pour que Dieu l’aime autant que moi.</l>
					</lg>
					<lg n="5">
						<l n="33" num="5.1">Je garde au cœur la fraîche empreinte</l>
						<l n="34" num="5.2">De ce qu’il fut dans sa candeur ;</l>
						<l n="35" num="5.3">Et, quand Dieu pèsera mon cœur,</l>
						<l n="36" num="5.4">Crois-tu qu’il en brise l’étreinte ?</l>
						<l n="37" num="5.5">Lui n’est plus lui, même à ses yeux ;</l>
						<l n="38" num="5.6">D’autres n’ont que son faux hommage :</l>
						<l n="39" num="5.7">Je le plains ; mais sa belle image,</l>
						<l n="40" num="5.8">Je ne la lui rendrai qu’aux cieux !</l>
					</lg>
				</div></body></text></TEI>