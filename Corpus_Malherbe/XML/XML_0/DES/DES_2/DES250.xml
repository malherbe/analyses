<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES PLEURS</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2802 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>2802 vers9617 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>L’Aurore en fuite (poèmes choisis)</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition établie par Christine Planté</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions Points</publisher>
						<date when="2010">2010</date>
					</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Ajout de l’attribut rhyme="none" dans le poème MINUIT (lacune confirmée par l’éditon Bertrand 1973).</p>
				<p>Ajout de l’attribut rhyme="none" pour un vers dans le poème AGAR.</p>
				<p>Ajout de l’attribut rhyme="none" pour un vers dans le poème LE PETIT RIEUR.</p>
				<p>Ajout d’un vers dans le poème LOUISE LABÉ (voir l’édition  Bertrand 1973).</p>
				<p>Ajout d’un vers dans le poème Révélation (voir l’édition de 2010).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
				<change when="2018-09-03" who="RR">Ajout de l’attribut rhyme="none" pour les vers sans rime.</change>
				<change when="2018-09-03" who="RR">Ajout des vers manquants.</change>
				<change when="2018-11-02" who="RR">Ajout des vers manquants.</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DES250">
					<head type="main">AUX MANES D’EDMOND GÉRAUD</head>
					<opener>
						<epigraph>
						<cit>
							<quote>
								« Mon fils ! lui répond l’ermite, <lb></lb>
								De Notre-Dame-des-Bois <lb></lb>
								Le pouvoir est sans limite, <lb></lb>
								Et le ciel s’ouvre à sa voix : <lb></lb>
								Mais, hélas ! sur cette terre <lb></lb>
								Où l’homme ne vit qu’un jour, <lb></lb>
								Il n’est ni croix ni rosaire <lb></lb>
								Qui guérisse de l’amour ! »
							</quote>
							<bibl>
								<name><hi rend="smallcap">Edmond Géraud</hi></name>, <lb></lb>
								<hi rend="ital">L’Ermite de Sainte-Avelle.</hi>
							</bibl>
						</cit>
					</epigraph>
					</opener>
					<lg n="1">
						<l n="1" num="1.1">Comme tout change vite ! Arbres de Belle-Allée,</l>
						<l n="2" num="1.2">Quoi ! vos ombres déjà couvrent un mausolée !</l>
						<l n="3" num="1.3">Une ceinture noire endeuille un jeune enfant ;</l>
						<l n="4" num="1.4">Son âge y veut chanter ; la mort le lui défend.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">Le rossignol ému, l’hirondelle hardie,</l>
						<l n="6" num="2.2">Revenus au printemps sous l’ardoise ou les fleurs,</l>
						<l n="7" num="2.3">Ont demandé peut-être à la frêle Élodie</l>
						<l n="8" num="2.4">Pourquoi son doux visage est tout pâle de pleurs.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">Elle a dit : « Taisez-vous ; laissez dormir mon père.</l>
						<l n="10" num="3.2"><space quantity="6" unit="char"></space>Il ne chante plus avec nous.</l>
						<l n="11" num="3.3">Ne couvrez point ma voix ; car tout ce que j’espère,</l>
						<l n="12" num="3.4">C’est qu’il la reconnaît quand je prie à genoux.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">« Mais ne vous sauvez pas : c’est encor sa demeure ;</l>
						<l n="14" num="4.2">Il aimait à nourrir vos nids et vos chansons ;</l>
						<l n="15" num="4.3">Ma mère sait par cœur ses pieuses leçons,</l>
						<l n="16" num="4.4"><space quantity="6" unit="char"></space>Et Dieu ne veut pas qu’elle meure !</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">« Taisez-vous ; elle est veuve, et tout la fait pleurer.</l>
						<l n="18" num="5.2">Ne lui rappelez pas votre chant le plus tendre ;</l>
						<l n="19" num="5.3">Une lyre est brisée ; elle croirait l’entendre ;</l>
						<l n="20" num="5.4">Laissez-lui du silence et le temps d’espérer !</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1">« Écoutez : car l’enfant du barde et du poète</l>
						<l n="22" num="6.2">Sait épeler la vie en : mots harmonieux,</l>
						<l n="23" num="6.3">Et mon père a versé sur ma bouche muette</l>
						<l n="24" num="6.4">Des paroles d’amour qu’il allait prendre aux cieux.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1">« Et je les retiendrai : Je veux avec ma mère</l>
						<l n="26" num="7.2">Parler comme il parlait aux pèlerins troublés ;</l>
						<l n="27" num="7.3">Je sais comme il rendait leur route moins amère,</l>
						<l n="28" num="7.4"><space quantity="6" unit="char"></space>Quand ils s’éloignaient consolés !</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1">« C’est lui qui me portait, pour enhardir mon âge,</l>
						<l n="30" num="8.2">Où germent les oiseaux dans leurs œufs renfermés,</l>
						<l n="31" num="8.3">Quand je plongeais mon cœur dans votre frais ménage,</l>
						<l n="32" num="8.4">Pour compter des petits, comme moi tant aimés !</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1">« Regarde ! disait-il ; oh ! regarde, ma fille !</l>
						<l n="34" num="9.2">C’est ainsi que ta mère a couvé notre enfant ;</l>
						<l n="35" num="9.3">L’âme du rossignol s’use pour sa famille !… »</l>
						<l n="36" num="9.4">Et puis, il me berçait sur son cœur triomphant.</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1">« Puis, un soir dans ses yeux tremblait une lumière,</l>
						<l n="38" num="10.2">Pareille à cette étoile. — Hélas ! je l’aime bien ! —</l>
						<l n="39" num="10.3">Et de sa bouche encor sortit une prière</l>
						<l n="40" num="10.4">Mélodieuse… et puis je n’entendis plus rien.</l>
					</lg>
					<lg n="11">
						<l n="41" num="11.1">« Le lendemain, ma mère était seule et couchée ;</l>
						<l n="42" num="11.2">Une parure affreuse enveloppait ses pleurs ;</l>
						<l n="43" num="11.3">Et sous la noire étreinte à mon corps attachée,</l>
						<l n="44" num="11.4">Moi ! je passe un printemps sans baisers et sans fleurs. »</l>
					</lg>
					<lg n="12">
						<l n="45" num="12.1">Mais l’enfant n’a pas dit, barde de <hi rend="ital">Sainte-Avelle</hi>,</l>
						<l n="46" num="12.2">Ton cortège de gloire au dernier de tes jours ;</l>
						<l n="47" num="12.3">Et nos bouquets lointains vers une ombre nouvelle,</l>
						<l n="48" num="12.4">Qui s’en retourne jeune où l’on aime toujours !</l>
					</lg>
					<lg n="13">
						<l n="49" num="13.1">Oui ! le dernier adieu d’une lyre expirée</l>
						<l n="50" num="13.2">Sonne le rendez-vous pour un autre avenir ;</l>
						<l n="51" num="13.3">Il tinte une prière ! une plainte sacrée,</l>
						<l n="52" num="13.4">Qui roule avec tristesse au fond du souvenir !</l>
					</lg>
				</div></body></text></TEI>