<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES PLEURS</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2802 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>2802 vers9617 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>L’Aurore en fuite (poèmes choisis)</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition établie par Christine Planté</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions Points</publisher>
						<date when="2010">2010</date>
					</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Ajout de l’attribut rhyme="none" dans le poème MINUIT (lacune confirmée par l’éditon Bertrand 1973).</p>
				<p>Ajout de l’attribut rhyme="none" pour un vers dans le poème AGAR.</p>
				<p>Ajout de l’attribut rhyme="none" pour un vers dans le poème LE PETIT RIEUR.</p>
				<p>Ajout d’un vers dans le poème LOUISE LABÉ (voir l’édition  Bertrand 1973).</p>
				<p>Ajout d’un vers dans le poème Révélation (voir l’édition de 2010).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
				<change when="2018-09-03" who="RR">Ajout de l’attribut rhyme="none" pour les vers sans rime.</change>
				<change when="2018-09-03" who="RR">Ajout des vers manquants.</change>
				<change when="2018-11-02" who="RR">Ajout des vers manquants.</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DES233">
					<head type="main">LE MAL DU PAYS</head>
					<opener>
						<epigraph>
							<cit>
								<quote>
									Ce front facile à se rider, ces joues légèrement <lb></lb>creusées, gardaient, l’empreinte du sceau dont le <lb></lb>malheur marque ses sujets, comme pour leur <lb></lb>laisser la consolation de se reconnaître d’un regard <lb></lb>fraternel, et de s’unir pour lui résister.
								</quote>
								<bibl><hi rend="smallcap">Madame de Balzac.</hi></bibl>
							</cit>
							<cit>
								<quote>
									Clémentine adorée, âme céleste et pure, <lb></lb>
									Qui, parmi les rigueurs d’une injuste maison, <lb></lb>
									Ne perd point l’innocence en perdant la raison.
								</quote>
							</cit>
						</epigraph>
					</opener>
					<lg n="1">
						<l n="1" num="1.1">Je veux aller mourir aux lieux où je suis née :</l>
						<l n="2" num="1.2">Le tombeau d’Albertine est près de mon berceau ;</l>
						<l n="3" num="1.3">Je veux aller trouver son ombre abandonnée ;</l>
						<l n="4" num="1.4">Je veux un même lit près du même ruisseau.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">Je veux dormir. J’ai soif de sommeil, d’innocence,</l>
						<l n="6" num="2.2">D’amour ! d’un long silence écouté sans effroi,</l>
						<l n="7" num="2.3">De l’air pur qui soufflait au jour de ma naissance,</l>
						<l n="8" num="2.4">Doux pour l’enfant du pauvre et pour l’enfant du roi.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">J’ai soif d’un frais oubli, d’une voix qui pardonne.</l>
						<l n="10" num="3.2">Qu’on me rende Albertine ! elle avait cette voix</l>
						<l n="11" num="3.3">Qu’un souvenir du ciel à quelques femmes donne ;</l>
						<l n="12" num="3.4">Elle a béni mon nom… autre part… autrefois !</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">Autrefois !… qu’il est loin le jour de son baptême !</l>
						<l n="14" num="4.2">Nous entrâmes au monde un jour qu’il était beau :</l>
						<l n="15" num="4.3">Le sel qui l’ondoya fut dissous sur moi-même,</l>
						<l n="16" num="4.4">Et le prêtre pour nous n’alluma qu’un flambeau.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">D’où vient-on quand on frappe aux portes de la terre ?</l>
						<l n="18" num="5.2">Sans clarté dans la vie, où s’adressent nos pas ?</l>
						<l n="19" num="5.3">Inconnus aux mortels qui nous tendent les bras,</l>
						<l n="20" num="5.4">Pleurants, comme effrayés d’un sort involontaire.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1">Où va-t-on quand, lassé d’un chemin sans bonheur,</l>
						<l n="22" num="6.2">On tourne vers le ciel un regard chargé d’ombre ?</l>
						<l n="23" num="6.3">Quand on ferme sur nous l’autre porte, si sombre !</l>
						<l n="24" num="6.4">Et qu’un ami n’a plus que nos traits dans son cœur ?</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1">Ah ! quand je descendrai rapide, palpitante,</l>
						<l n="26" num="7.2">L’invisible sentier qu’on ne remonte pas,</l>
						<l n="27" num="7.3">Reconnaîtrai-je enfin la seule âme constante</l>
						<l n="28" num="7.4">Qui m’aimait imparfaite et me grondait si bas ?</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1">Te verrai-je, Albertine ! Ombre jeune et craintive ;</l>
						<l n="30" num="8.2">Jeune, tu t’envolas peureuse des autans :</l>
						<l n="31" num="8.3">Dénouant pour mourir ta robe de printemps,</l>
						<l n="32" num="8.4">Tu dis : « Semez ces fleurs sur ma cendre captive. »</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1">Oui ! je reconnaîtrai tes traits pâles, charmants,</l>
						<l n="34" num="9.2">Miroir de la pitié qui marchait sur tes traces,</l>
						<l n="35" num="9.3">Qui pleurait dans ta voix, angélisait tes grâces,</l>
						<l n="36" num="9.4">Et qui s’enveloppait dans tes doux vêtements !</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1">Oui, tu ne m’es qu’absente, et la mort n’est qu’un voile,</l>
						<l n="38" num="10.2">Albertine ! et tu sais l’autre vie avant moi.</l>
						<l n="39" num="10.3">Un jour, j’ai vu ton âme aux feux blancs d’une étoile ;</l>
						<l n="40" num="10.4">Elle a baisé mon front, et j’ai dit : « C’est donc toi ! »</l>
					</lg>
					<lg n="11">
						<l n="41" num="11.1">Viens encor, viens ! j’ai tant de choses à te dire !</l>
						<l n="42" num="11.2">Ce qu’on t’a fait souffrir, je le sais ! j’ai souffert.</l>
						<l n="43" num="11.3">Ô ma plus que sœur, viens ! ce que je n’ose écrire,</l>
						<l n="44" num="11.4">Viens le voir palpiter dans mon cœur entr’ouvert !</l>
					</lg>
				</div></body></text></TEI>