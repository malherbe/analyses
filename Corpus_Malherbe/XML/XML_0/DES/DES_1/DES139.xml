<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>9618 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>9618 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies</title>
					<author>Marceline Desbordes-Valmore</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>A. Boulland, libraire-éditeur</publisher>
						<date when="1830">1830</date>
					</imprint>
					<extent>9618 verstome second</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-12-20" who="RR">Insertion des corrections métriques dans le corps du texte.</change>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
				<change when="2018-11-02" who="RR">Correction dans le poème "Dors, ma Mère".</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">POÉSIES DIVERSES</head><div type="poem" key="DES139">
						<head type="main">LA MOUCHE BLEUE</head>
						<lg n="1">
							<l n="1" num="1.1">Humble fille de l’air, mouche bleue et gentille,</l>
							<l n="2" num="1.2">Qui rafraîchis ton vol sur d’humides roseaux,</l>
							<l n="3" num="1.3"><space quantity="6" unit="char"></space>N’es-tu pas le nain des oiseaux ?</l>
							<l n="4" num="1.4">Non ! tu ne chantes pas, légère volatile,</l>
							<l n="5" num="1.5">Tu n’as point de plumage, et ton rapide essor</l>
							<l n="6" num="1.6">M’en fait mieux admirer l’invisible ressort.</l>
							<l n="7" num="1.7">Tu ris de l’oiseleur, tu fais sauver sa joie,</l>
							<l n="8" num="1.8">Ton piquant aiguillon le distrait de sa proie,</l>
							<l n="9" num="1.9"><space quantity="6" unit="char"></space>Et ton bourdonnement moqueur</l>
							<l n="10" num="1.10">Lui nomme impunément son agile vainqueur.</l>
							<l n="11" num="1.11">Tu montes jusqu’aux cieux, les ailes étendues ;</l>
							<l n="12" num="1.12">Un rayon de soleil te guide et te soutient ;</l>
							<l n="13" num="1.13">Ta famille dansante et s’y joue et s’y tient,</l>
							<l n="14" num="1.14">Comme un essaim de fleurs dans les airs répandues.</l>
							<l n="15" num="1.15">Qu’il est gai de te voir t’y balancer longtemps,</l>
							<l n="16" num="1.16">Descendre vers la terre, et remonter encore,</l>
							<l n="17" num="1.17">Y chercher, renaissante au souffle du printemps,</l>
							<l n="18" num="1.18">Sur la robe de gaze un reflet de l’aurore !</l>
							<l n="19" num="1.19">Violette vivante ! à ce peu qu’il t’a fait,</l>
							<l n="20" num="1.20">Le Ciel donna le monde, imprima la pensée,</l>
							<l n="21" num="1.21">Le sentiment, l’amour ! et, sans remords blessée,</l>
							<l n="22" num="1.22"><space quantity="2" unit="char"></space>Pour toi, du moins, l’amour n’est qu’un bienfait !</l>
						</lg>
						<lg n="2">
							<l n="23" num="2.1">Je m’amuse à rêver sur ton frêle édifice</l>
							<l n="24" num="2.2"><space quantity="6" unit="char"></space>Soutenu de frêles piliers,</l>
							<l n="25" num="2.3"><space quantity="6" unit="char"></space>Si polis et si réguliers,</l>
							<l n="26" num="2.4"><space quantity="2" unit="char"></space>Qu’on les croirait mouvants par artifice.</l>
							<l n="27" num="2.5">Hélas ! dans l’âge le plus fort,</l>
							<l n="28" num="2.6">Comme toi l’homme tombe ; et ce maître du monde</l>
							<l n="29" num="2.7"><space quantity="6" unit="char"></space>N’a plus d’ami qui le seconde</l>
							<l n="30" num="2.8"><space quantity="6" unit="char"></space>Dans son duel avec la Mort</l>
						</lg>
						<lg n="3">
							<l n="31" num="3.1">Ô mouche ! que ton être occupa mon enfance !</l>
							<l n="32" num="3.2">Combien, lorsqu’attristant mon paisible loisir</l>
							<l n="33" num="3.3">Quelque enfant sous mes yeux accourait te saisir,</l>
							<l n="34" num="3.4"><space quantity="6" unit="char"></space>Mes larmes prenaient ta défense !</l>
						</lg>
						<lg n="4">
							<l n="35" num="4.1">Petite philosophe, on a médit de toi :</l>
							<l n="36" num="4.2">J’en veux à la fourmi qui t’a cherché querelle.</l>
							<l n="37" num="4.3">Un printemps fait ta vie, en jouir est ta loi ;</l>
							<l n="38" num="4.4">Es-tu moins prévoyante, es-tu moins riche qu’elle ?</l>
							<l n="39" num="4.5">Esclave de la terre, elle y rampe toujours ;</l>
							<l n="40" num="4.6">Ses trésors souterrains sont clos à l’indigence ;</l>
							<l n="41" num="4.7">Et, quand il a rempli son avare exigence,</l>
							<l n="42" num="4.8">Du ciron malheureux elle abrège les jours.</l>
							<l n="43" num="4.9">Pour toi, souvent rêveuse et souvent endormie,</l>
							<l n="44" num="4.10">Je t’observe partout avec des yeux d’amie :</l>
							<l n="45" num="4.11">Quand la nature est triste, il ne te faut plus rien,</l>
							<l n="46" num="4.12">Et tu romps avec elle un fragile lien.</l>
						</lg>
						<lg n="5">
							<l n="47" num="5.1">Oh ! puisse l’âpre hiver épargner ta faiblesse !</l>
							<l n="48" num="5.2">Que l’aquilon jamais ne te soit rigoureux !</l>
							<l n="49" num="5.3">Que ton corps délicat, qu’un rien détruit ou blesse,</l>
							<l n="50" num="5.4">Trouve contre la brume un foyer généreux !</l>
							<l n="51" num="5.5">Atome voyageur ! en passant les montagnes,</l>
							<l n="52" num="5.6">Les ruisseaux, les chemins, les cités, les campagnes,</l>
							<l n="53" num="5.7">Que Dieu te sauve, hélas ! et du bec d’un oiseau,</l>
							<l n="54" num="5.8"><space quantity="6" unit="char"></space>Et de l’insecte au fin réseau !</l>
						</lg>
					</div></body></text></TEI>