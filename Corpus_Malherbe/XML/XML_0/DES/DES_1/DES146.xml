<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>9618 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>9618 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies</title>
					<author>Marceline Desbordes-Valmore</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>A. Boulland, libraire-éditeur</publisher>
						<date when="1830">1830</date>
					</imprint>
					<extent>9618 verstome second</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-12-20" who="RR">Insertion des corrections métriques dans le corps du texte.</change>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
				<change when="2018-11-02" who="RR">Correction dans le poème "Dors, ma Mère".</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">POÉSIES DIVERSES</head><div type="poem" key="DES146">
						<head type="main">LA SOURIS CHEZ UN JUGE</head>
						<lg n="1">
							<l n="1" num="1.1">Tremblante, prise au piège et respirant à peine,</l>
							<l n="2" num="1.2">Sortie imprudemment du maternel séjour,</l>
							<l n="3" num="1.3">Rêvant sa dernière heure au seul bruit de sa chaîne,</l>
							<l n="4" num="1.4">Une jeune souris voyait tomber le jour.</l>
							<l n="5" num="1.5">Dans le grillage étroit qui la tient prisonnière,</l>
							<l n="6" num="1.6">A passé d’un flambeau l’éclatante lumière ;</l>
							<l n="7" num="1.7">Elle tressaille, écoute : un silence de paix</l>
							<l n="8" num="1.8">Succède au mouvement qui la glaçait de crainte ;</l>
							<l n="9" num="1.9">Et d’un vieux mur caché sous des lambris épais</l>
							<l n="10" num="1.10">On entend murmurer cette humble et douce plainte :</l>
							<l n="11" num="1.11">« Dans ta belle maison, toi qui rentres content,</l>
							<l n="12" num="1.12">Quand je me sens mourir de la mort qui m’attend,</l>
							<l n="13" num="1.13">Redoutable ennemi de tout ce qui respire,</l>
							<l n="14" num="1.14">Oh ! n’étends pas sur moi ton oppressif empire !</l>
							<l n="15" num="1.15">Laisse ton cœur s’ouvrir au cri du malheureux :</l>
							<l n="16" num="1.16">Hélas ! est-on moins grand pour être généreux ?</l>
							<l n="17" num="1.17">Laisse-moi boire encor l’air, la douce rosée,</l>
							<l n="18" num="1.18">Ce bienfait de la nuit, ce céleste présent,</l>
							<l n="19" num="1.19"><space quantity="2" unit="char"></space>Dont, par un souffle humide et bienfaisant,</l>
							<l n="20" num="1.20"><space quantity="2" unit="char"></space>Chaque matin la terre est arrosée.</l>
							<l n="21" num="1.21"><space quantity="2" unit="char"></space>Juge, sois juste et rends-moi mes trésors,</l>
							<l n="22" num="1.22">Un ciel à contempler, ma liberté native :</l>
							<l n="23" num="1.23">Dieu me fit de la vie un plaisir sans remords,</l>
							<l n="24" num="1.24"><space quantity="6" unit="char"></space>Toi, tu la rends sombre et captive.</l>
						</lg>
						<lg n="2">
							<l n="25" num="2.1">« Je suis une souris née au dernier printemps ;</l>
							<l n="26" num="2.2">L’été commence. Hélas ! c’est vivre peu de temps !</l>
							<l n="27" num="2.3">Viens voir, je porte encor la robe de l’enfance.</l>
							<l n="28" num="2.4"><space quantity="2" unit="char"></space>Le blé nouveau, le riz friand, les noix,</l>
							<l n="29" num="2.5"><space quantity="2" unit="char"></space>Disait ma mère, allaient avant deux mois</l>
							<l n="30" num="2.6"><space quantity="6" unit="char"></space>Enrichir mon adolescence.</l>
							<l n="31" num="2.7">Peu m’est assez pourtant ; facile à me nourrir,</l>
							<l n="32" num="2.8">Je ne suis pas gourmande et tout sert au ménage ;</l>
							<l n="33" num="2.9">Un grain d’orge suffit aux souris de mon âge,</l>
							<l n="34" num="2.10"><space quantity="6" unit="char"></space>Pour les empêcher de mourir.</l>
						</lg>
						<lg n="3">
							<l n="35" num="3.1">« Ne me fais pas mourir ! Suis l’exemple d’un sage :</l>
							<l n="36" num="3.2">Les souris sans danger visitaient son séjour ;</l>
							<l n="37" num="3.3">Car ce sage disait : « De nos âmes un jour</l>
							<l n="38" num="3.4">« Le sein des animaux peut être le passage.</l>
							<l n="39" num="3.5">« Tout est possible à Dieu, l’impossible est son bien ;</l>
							<l n="40" num="3.6">« Si par lui l’homme est tout, par lui l’homme n’est rien.</l>
							<l n="41" num="3.7">« Grâce donc ! criait-il aux hommes en colère,</l>
							<l n="42" num="3.8">« Muets pour la clémence et sourds à la prière ;</l>
							<l n="43" num="3.9">« Grâce ! oubliez un peu les mots : glaive, trépas ;</l>
							<l n="44" num="3.10">« Régnez sur le plus faible et ne le tuez pas !</l>
							<l n="45" num="3.11">« La colombe au cœur tendre, à la plume argentée,</l>
							<l n="46" num="3.12">« Peut-être est une amante aux forêts arrêtée</l>
							<l n="47" num="3.13">« Par le doux souvenir d’un amour malheureux ;</l>
							<l n="48" num="3.14">« On croit le deviner à son chant douloureux.</l>
							<l n="49" num="3.15">« Qui sait si la souris n’est pas la jeune fille</l>
							<l n="50" num="3.16">« Frappée en folâtrant au sein de sa famille,</l>
							<l n="51" num="3.17">« Et qui tombe immobile en courant dans les fleurs :</l>
							<l n="52" num="3.18">« Car, pour un peu de miel, que d’absinthe et de pleurs ! »</l>
						</lg>
						<lg n="4">
							<l n="53" num="4.1">« Si le sage a dit vrai, tremble d’être inflexible,</l>
							<l n="54" num="4.2">Tremble de tourmenter l’âme errante et sensible</l>
							<l n="55" num="4.3">D’une sœur qui t’aima, d’une jeune beauté</l>
							<l n="56" num="4.4">Qui se plaisait, enfant, sur ton sein agité.</l>
						</lg>
						<lg n="5">
							<l n="57" num="5.1"><space quantity="6" unit="char"></space>« Enfin, si ma part de la vie</l>
							<l n="58" num="5.2"><space quantity="6" unit="char"></space>N’est que le rayon passager</l>
							<l n="59" num="5.3">Du jour que mon cachot me dérobe et m’envie,</l>
							<l n="60" num="5.4">Ce don si fugitif, daigne le ménager !</l>
							<l n="61" num="5.5">Vivre, c’est vivre enfin, et le néant m’alarme ;</l>
							<l n="62" num="5.6">Cette crainte au méchant coûte au moins une larme ;</l>
							<l n="63" num="5.7">Juge de son horreur pour un cœur tout amour,</l>
							<l n="64" num="5.8">Et si loin de la nuit ne m’éteins pas le jour !</l>
							<l n="65" num="5.9">Faut-il te dire tout ? je veux devenir mère.</l>
							<l n="66" num="5.10">Laisse-moi donc revoir, dans ma douleur amère,</l>
							<l n="67" num="5.11">Un ami de mon âge, imprudent comme moi,</l>
							<l n="68" num="5.12">Qui pour me délivrer s’élancerait vers toi.</l>
							<l n="69" num="5.13">S’il avait de mon sort la triste confidence,</l>
							<l n="70" num="5.14">Je lui dirais en vain : Sauvez-vous ! il viendrait :</l>
							<l n="71" num="5.15">L’amour au désespoir connaît-il la prudence ?</l>
							<l n="72" num="5.16">Il rongerait mes fers, ou bien il me suivrait.</l>
						</lg>
						<lg n="6">
							<l n="73" num="6.1"><space quantity="2" unit="char"></space>« J’ai dit l’amour : tu le connais peut-être ?</l>
							<l n="74" num="6.2"><space quantity="2" unit="char"></space>Béni soit Dieu ! car l’amour est humain.</l>
							<l n="75" num="6.3">Oui, je retrouverai la moitié de mon être,</l>
							<l n="76" num="6.4"><space quantity="6" unit="char"></space>Et je serai libre demain !</l>
							<l n="77" num="6.5">Oui, tu sais que l’amour console la nature,</l>
							<l n="78" num="6.6">Qu’il jette au prisonnier des rêves gracieux,</l>
							<l n="79" num="6.7">Qu’il souffle à son oreille un chant délicieux,</l>
							<l n="80" num="6.8">Et que même au coupable il sauve la torture.</l>
							<l n="81" num="6.9">Et je suis à genoux… et je tremble… et j’attends…</l>
							<l n="82" num="6.10">Homme, pour te fléchir qu’il faut parler longtemps !</l>
						</lg>
						<lg n="7">
							<l n="83" num="7.1">« Un jour, que cet aveu m’en obtienne la grâce,</l>
							<l n="84" num="7.2">J’avais salué l’aube et ton premier repas,</l>
							<l n="85" num="7.3">Lorsqu’un bruit, plus léger que le bruit de mes pas,</l>
							<l n="86" num="7.4">M’avertit qu’en secret quelqu’un cherchait ta trace.</l>
							<l n="87" num="7.5">Ta voix devint alors plus douce de moitié.</l>
							<l n="88" num="7.6">Celle qui répondait me parut suppliante,</l>
							<l n="89" num="7.7">Et, si je ne m’abuse, à la tendre pitié</l>
							<l n="90" num="7.8">Tu donnas plus d’une heure, ou l’heure était bien lente !</l>
							<l n="91" num="7.9">Le bruit cessa, j’entrai ; les débris d’un festin</l>
							<l n="92" num="7.10">M’invitaient à la table enfin abandonnée ;</l>
							<l n="93" num="7.11"><space quantity="2" unit="char"></space>Et sur ma vie un moment <choice reason="analysis" type="false_verse" hand="CA"><sic>infortunée</sic><corr source="édition_1973">fortunée</corr></choice></l>
							<l n="94" num="7.12"><space quantity="2" unit="char"></space>Je vis pleuvoir les bienfaits du destin.</l>
							<l n="95" num="7.13">Dans ces lieux trop aimés qu’à présent je déteste,</l>
							<l n="96" num="7.14">J’ai vu, j’ai respecté la boucle de cheveux,</l>
							<l n="97" num="7.15">Tombés d’un front charmant pour enchaîner tes vœux ;</l>
							<l n="98" num="7.16">Ils ne sont pas les tiens, leur couleur me l’atteste.</l>
							<l n="99" num="7.17"><space quantity="6" unit="char"></space>Ces liens souples et dorés,</l>
							<l n="100" num="7.18"><space quantity="6" unit="char"></space>Ces doux aveux, ces feuillets roses,</l>
							<l n="101" num="7.19">Les rubans embaumés dont ces lettres sont closes,</l>
							<l n="102" num="7.20">N’ont pas séduit mes sens de langueur enivrés.</l>
							<l n="103" num="7.21">J’ai respiré de loin la cire parfumée</l>
							<l n="104" num="7.22">Qui scella, j’en suis sûre, un secret qui t’est cher :</l>
							<l n="105" num="7.23">Le hasard me l’apprit sans m’en être informée ;</l>
							<l n="106" num="7.24">Je courais, j’étais libre… hélas ! c’était hier !</l>
						</lg>
						<lg n="8">
							<l n="107" num="8.1">« Tu sommeillais peut-être, et plus vive que sage,</l>
							<l n="108" num="8.2">Au pied de ces rideaux, que je baigne de pleurs,</l>
							<l n="109" num="8.3">J’aperçus, ne crains pas que je le dise ailleurs,</l>
							<l n="110" num="8.4">Un soulier trop petit pour être à ton usage :</l>
							<l n="111" num="8.5">Je m’y blottis joyeuse et je le fis courir ;</l>
							<l n="112" num="8.6">Je traînais en riant cette maison mobile,</l>
							<l n="113" num="8.7">Dont les dehors, ornés par quelque main habile,</l>
							<l n="114" num="8.8">M’enflaient d’un peu d’orgueil, et l’orgueil fait mourir :</l>
							<l n="115" num="8.9">Car, depuis ce moment, éveillé par la haine,</l>
							<l n="116" num="8.10">Tu m’élevas dans l’ombre une affreuse prison.</l>
							<l n="117" num="8.11">Innocente souris, pour m’écraser sans peine,</l>
							<l n="118" num="8.12">Un homme est descendu jusqu’à la trahison !</l>
							<l n="119" num="8.13">Non ! ne m’écrase pas ! et si ma peur te touche,</l>
							<l n="120" num="8.14">Que l’accent du pardon s’échappe de ta bouche !</l>
							<l n="121" num="8.15">Il est dieu, leur dirai-je, il m’a donné des jours !</l>
							<l n="122" num="8.16">Ton toit sera béni, ton nom vivra toujours,</l>
							<l n="123" num="8.17">Et toujours de beaux yeux aimeront à le lire.</l>
						</lg>
						<lg n="9">
							<l n="124" num="9.1">« Et si jamais ton cœur, brûlé d’un saint délire,</l>
							<l n="125" num="9.2"><space quantity="6" unit="char"></space>A langui pour la liberté,</l>
							<l n="126" num="9.3">Qu’elle se donne à toi dans toute sa beauté !</l>
							<l n="127" num="9.4"><space quantity="6" unit="char"></space>Que sur ta sereine carrière</l>
							<l n="128" num="9.5">Elle épanche à flots purs sa tranquille lumière :</l>
							<l n="129" num="9.6">Qu’elle trace à ta vie un facile sentier</l>
							<l n="130" num="9.7">Et te sème de fleurs un siècle tout entier ! »</l>
						</lg>
						<lg n="10">
							<l n="131" num="10.1"><space quantity="2" unit="char"></space>Elle se tut. Le juge alors : « Hé vite !</l>
							<l n="132" num="10.2"><space quantity="2" unit="char"></space>« Elle est au piège, hâtez-vous d’accourir :</l>
							<l n="133" num="10.3"><space quantity="2" unit="char"></space>« Étouffez-la, cette pauvre petite ;</l>
							<l n="134" num="10.4"><space quantity="6" unit="char"></space>« Je n’aime pas à voir souffrir. »</l>
						</lg>
					</div></body></text></TEI>