<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>9618 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>9618 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies</title>
					<author>Marceline Desbordes-Valmore</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>A. Boulland, libraire-éditeur</publisher>
						<date when="1830">1830</date>
					</imprint>
					<extent>9618 verstome second</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-12-20" who="RR">Insertion des corrections métriques dans le corps du texte.</change>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
				<change when="2018-11-02" who="RR">Correction dans le poème "Dors, ma Mère".</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">IDYLLES</head><div type="poem" key="DES13">
						<head type="main">PHILIS</head>
						<lg n="1">
							<l n="1" num="1.1">Presse-toi, vieux berger, tout annonce l’orage.</l>
							<l n="2" num="1.2">Le vent courbe les blés, détruit la fleur sauvage ;</l>
							<l n="3" num="1.3">Un murmure plaintif circule au fond des bois,</l>
							<l n="4" num="1.4">Et l’écho me répond en attristant ma voix.</l>
							<l n="5" num="1.5">De ton chien prévoyant la garde est plus austère,</l>
							<l n="6" num="1.6">Il rôde, en haletant, d’un air triste et sévère ;</l>
							<l n="7" num="1.7">Du fond de la vallée il ramène un agneau</l>
							<l n="8" num="1.8">Et le chasse en grondant jusqu’au sein du troupeau.</l>
						</lg>
						<lg n="2">
							<l n="9" num="2.1">L’ouragan tourbillonne et ravage la plaine.</l>
							<l n="10" num="2.2">L’éclair poursuit l’éclair, il tonne, il va pleuvoir,</l>
							<l n="11" num="2.3">Tout s’efface ; il fait nuit longtemps avant le soir ;</l>
							<l n="12" num="2.4">Et le toit de Philis ne se voit plus qu’à peine.</l>
							<l n="13" num="2.5">Laisse-moi te guider. Si tu ne peux courir,</l>
							<l n="14" num="2.6">Je soutiendrai tes pas. Ne crains point ma jeunesse ;</l>
							<l n="15" num="2.7">J’ai déjà quatorze ans ; j’honore la vieillesse,</l>
							<l n="16" num="2.8">Et je suis assez grand, du moins, pour la chérir.</l>
							<l n="17" num="2.9">La petite Philis t’ouvrira sa chaumière ;</l>
							<l n="18" num="2.10">Son père m’a vu naître, il m’appelle son fils.</l>
							<l n="19" num="2.11">Peut-être qu’autrefois tu connaissais sa mère ;</l>
							<l n="20" num="2.12">Elle n’est plus… mais viens, tu connaîtras Philis !</l>
							<l n="21" num="2.13">Oui, berger, c’est Philis qui m’a dit tout à l’heure :</l>
							<l n="22" num="2.14">« Olivier, le ciel gronde ; on s’enferme au hameau.</l>
							<l n="23" num="2.15">Nous sommes à l’abri, mais au pied du coteau</l>
							<l n="24" num="2.16">Je vois un vieux berger ; qu’il vienne en ma demeure !</l>
							<l n="25" num="2.17">Regarde sur son front voler ses cheveux blancs ;</l>
							<l n="26" num="2.18">Comme il lève les yeux vers le ciel en colère !</l>
							<l n="27" num="2.19">Il se met à genoux… C’est qu’il a des enfants,</l>
							<l n="28" num="2.20">Et qu’il demande au ciel de leur garder un père ! »</l>
							<l n="29" num="2.21">Et Philis de mes mains a retiré sa main,</l>
							<l n="30" num="2.22">Et jusqu’au fond du cœur j’ai cru sentir ses larmes,</l>
							<l n="31" num="2.23">Et j’ai couru vers toi. Mais, au bout du chemin,</l>
							<l n="32" num="2.24">Tu verras s’il est doux de calmer ses alarmes.</l>
							<l n="33" num="2.25">Berger, voilà Philis ! — Elle nous tend les bras :</l>
							<l n="34" num="2.26">Vois comme son sourire est mêlé de tristesse !</l>
							<l n="35" num="2.27">Elle songe à sa mère et pleure de tendresse ;</l>
							<l n="36" num="2.28">Sa mère lui sourit… mais ne lui répond pas !</l>
							<l n="37" num="2.29">Entrons. — Le vieux berger rêve à ton doux langage,</l>
							<l n="38" num="2.30">Philis ; il te regarde, il est moins abattu.</l>
							<l n="39" num="2.31">On est calme avec toi, même au bruit de l’orage.</l>
							<l n="40" num="2.32">Ô Philis ! on est bien auprès de la vertu !</l>
							<l n="41" num="2.33">Tandis que ses moutons, sous la feuillée obscure,</l>
							<l n="42" num="2.34">Arrachent à la terre une humide verdure,</l>
							<l n="43" num="2.35">Je lui raconterai, pour charmer ta frayeur,</l>
							<l n="44" num="2.36">Le plus beau de mes jours, le jour où je t’ai vue.</l>
							<l n="45" num="2.37">Si tu crains d’un éclair la lueur imprévue,</l>
							<l n="46" num="2.38">Tant que je parlerai, cache-toi sur mon cœur.</l>
						</lg>
						<lg n="3">
							<l n="47" num="3.1">Cache-toi… Ma Philis n’avait pas dix années,</l>
							<l n="48" num="3.2">Quand le hasard lia nos âmes étonnées.</l>
							<l n="49" num="3.3">Je l’aimai plus que moi, plus que mon cher agneau</l>
							<l n="50" num="3.4">Que j’offris à Philis et qu’elle trouvait beau.</l>
							<l n="51" num="3.5">C’était un jour de fête, et cet agneau volage</l>
							<l n="52" num="3.6">S’enfuit, malgré mes cris, loin de notre village.</l>
							<l n="53" num="3.7">Sous ce bouquet de houx, qui cache une maison,</l>
							<l n="54" num="3.8">L’agneau vint se jeter… Hélas ! qu’il eut raison !</l>
							<l n="55" num="3.9">J’y rencontrai Philis ; je crus la reconnaître ;</l>
							<l n="56" num="3.10">Je crus l’avoir aimée avant même de naître ;</l>
							<l n="57" num="3.11">Je sentis que mon cœur s’enfuyait vers le sien,</l>
							<l n="58" num="3.12">Et je vis dans ses yeux qu’elle attendait le mien.</l>
							<l n="59" num="3.13">Elle avait à ses pieds sa guirlande effeuillée ;</l>
							<l n="60" num="3.14">Elle pleurait… C’était une rose mouillée.</l>
							<l n="61" num="3.15">Saisi de sa douleur, je ne pouvais parler ;</l>
							<l n="62" num="3.16">Je ne pouvais la joindre, hélas ! ni m’en aller.</l>
							<l n="63" num="3.17">Son œil noir dans les pleurs brillait comme une étoile,</l>
							<l n="64" num="3.18">Ou comme un doux rayon quand il pleut au soleil.</l>
							<l n="65" num="3.19">On eût dit que mes yeux se dégageaient d’un voile</l>
							<l n="66" num="3.20">Et que ce doux regard enchantait mon réveil !</l>
							<l n="67" num="3.21">J’oubliai mon hameau, mes parents, ma chaumière ;</l>
							<l n="68" num="3.22">Mon âme pour la voir venait sous ma paupière.</l>
							<l n="69" num="3.23">J’oubliai de punir l’agneau capricieux,</l>
							<l n="70" num="3.24">Je regardais Philis, et je voyais les cieux.</l>
						</lg>
						<lg n="4">
							<l n="71" num="4.1">« Qui t’alarme, lui dis-je, ô petite bergère ?</l>
							<l n="72" num="4.2">As-tu peur d’un bélier caché dans la bruyère ?</l>
							<l n="73" num="4.3">Ou quelque méchant pâtre, en grossissant sa voix,</l>
							<l n="74" num="4.4">Ose-t-il t’empêcher de courir dans les bois ?</l>
							<l n="75" num="4.5">Je voudrais… Je voudrais savoir comme on t’appelle.</l>
							<l n="76" num="4.6">Moi, je suis Olivier. — Je suis Philis, dit-elle.</l>
							<l n="77" num="4.7">Je n’ai vu qu’un agneau qu’appelait un enfant,</l>
							<l n="78" num="4.8">Et je n’ai pas eu peur à la voix d’un méchant.</l>
							<l n="79" num="4.9">Mais, en cueillant des fleurs pour couronner ma tête,</l>
							<l n="80" num="4.10">Je disais : Ce fut donc encore un jour de fête,</l>
							<l n="81" num="4.11">Puisqu’on m’avait parée avec de blancs atours,</l>
							<l n="82" num="4.12">Que ma mère en priant s’endormit pour toujours.</l>
							<l n="83" num="4.13">Elle avait demandé le pasteur du village.</l>
							<l n="84" num="4.14">Le pasteur avait dit : « Espérance et courage ! »</l>
							<l n="85" num="4.15">Il bénit son sommeil, et, pleurant avec nous,</l>
							<l n="86" num="4.16">Parlait bas à mon père immobile à genoux.</l>
							<l n="87" num="4.17">Les bergers pour la voir entouraient la chaumière.</l>
							<l n="88" num="4.18">Son nom, qu’ils aimaient tous, unissait leur prière.</l>
							<l n="89" num="4.19">Sous le même rideau je voulus me cacher ;</l>
							<l n="90" num="4.20">Mon père, en gémissant, put seul m’en détacher.</l>
							<l n="91" num="4.21">Vers le soir, dans son lit un ange vint la prendre ;</l>
							<l n="92" num="4.22">Il emporta ma mère, et je la vis descendre</l>
							<l n="93" num="4.23">À travers le sentier qu’éclairaient deux flambeaux.</l>
							<l n="94" num="4.24">On chantait, mais ce chant m’arrachait des sanglots.</l>
							<l n="95" num="4.25">Je lui tendais les bras du haut de la montagne,</l>
							<l n="96" num="4.26">Quand je vis des hiboux voler dans la campagne.</l>
							<l n="97" num="4.27">Je n’osai plus crier : ma voix me faisait peur.</l>
							<l n="98" num="4.28">Son nom, qui m’étouffait, s’enferma dans mon cœur.</l>
							<l n="99" num="4.29">L’ombre m’enveloppa : le reste, je l’ignore.</l>
							<l n="100" num="4.30">On me trouva plongée en un profond sommeil.</l>
							<l n="101" num="4.31">Hélas ! dans ce sommeil on pleure, on aime encore.</l>
							<l n="102" num="4.32">Il en est un, dit-on, sans amour, sans réveil !</l>
							<l n="103" num="4.33">Depuis ce jour de fête, on n’a pas vu ma mère ;</l>
							<l n="104" num="4.34">Au sentier, chaque soir, elle appelle mon père ;</l>
							<l n="105" num="4.35">Mais, quand je veux savoir s’il l’a vue en chemin,</l>
							<l n="106" num="4.36">Il soupire et me dit : « Je la verrai demain ! »</l>
							<l n="107" num="4.37">Voilà, petit berger, la cause de mes larmes.</l>
							<l n="108" num="4.38">À mon père attristé je cache mes alarmes ;</l>
							<l n="109" num="4.39">Pour lui plaire, souvent je me pare de fleurs,</l>
							<l n="110" num="4.40">Et j’apprends à sourire en retenant mes pleurs. »</l>
						</lg>
						<lg n="5">
							<l n="111" num="5.1">Son père l’écoutait à travers la fenêtre ;</l>
							<l n="112" num="5.2">Je le pris pour le mien, en le voyant paraître.</l>
							<l n="113" num="5.3">D’un air triste et content il sourit à Philis,</l>
							<l n="114" num="5.4">Et depuis ce moment il m’appela son fils.</l>
							<l n="115" num="5.5">L’agneau sautait près d’elle et broutait sa couronne.</l>
							<l n="116" num="5.6">Hors de moi, je saisis ce précieux larcin ;</l>
							<l n="117" num="5.7">En tremblant de plaisir, je le mis dans mon sein.</l>
							<l n="118" num="5.8">« Si mon agneau te plaît, prends-le, je te le donne,</l>
							<l n="119" num="5.9">Dis-je alors à Philis. Chaque jour, chaque soir,</l>
							<l n="120" num="5.10">Si ton père y consent, je reviendrai le voir.</l>
							<l n="121" num="5.11">Il semble qu’il demande et choisit sa maîtresse ;</l>
							<l n="122" num="5.12">Comme il me caressait, je vois qu’il te caresse.</l>
							<l n="123" num="5.13">Les nœuds pour l’arrêter sont déjà superflus.</l>
							<l n="124" num="5.14">Tu lui parles, Philis, il ne m’écoute plus ! »</l>
						</lg>
						<lg n="6">
							<l n="125" num="6.1">Son père, en l’embrassant, nous permit cet échange.</l>
							<l n="126" num="6.2">Il fallut m’en aller. Je courus, sous la grange,</l>
							<l n="127" num="6.3">À mes tendres parents raconter mon bonheur.</l>
							<l n="128" num="6.4">Je montrai la guirlande encore sur mon cœur ;</l>
							<l n="129" num="6.5">Je parlais de Philis et j’embrassais ma mère ;</l>
							<l n="130" num="6.6">Je brûlais que le jour nous rendît sa lumière.</l>
							<l n="131" num="6.7">En respirant des fleurs enfin je m’endormis,</l>
							<l n="132" num="6.8">Et mon rêve disait : — Philis ! Philis ! Philis !</l>
							<l n="133" num="6.9">Ce nom charme en tous lieux mon oreille ravie ;</l>
							<l n="134" num="6.10">Il a frappé mon âme et commencé ma vie ;</l>
							<l n="135" num="6.11">Mes lèvres, en dormant, savent le prononcer,</l>
							<l n="136" num="6.12">Et, dans l’ombre, ma main essaie à le tracer.</l>
							<l n="137" num="6.13">C’est pour l’unir au mien que j’apprends à l’écrire…</l>
							<l n="138" num="6.14">Éveille-toi, Philis ! je n’ai plus rien à dire.</l>
							<l n="139" num="6.15">Tu peux ouvrir les yeux, le calme est de retour ;</l>
							<l n="140" num="6.16">Le soleil épuré recommence un beau jour ;</l>
							<l n="141" num="6.17">Avant de les quitter, il sèche nos campagnes,</l>
							<l n="142" num="6.18">Et de ses derniers feux redore les montagnes.</l>
						</lg>
						<lg n="7">
							<l n="143" num="7.1">Ô berger ! si le ciel ici t’a fait venir,</l>
							<l n="144" num="7.2">C’est que le ciel nous aime, et qu’il va nous bénir !</l>
							<l n="145" num="7.3">Mais tes moutons joyeux se jettent dans la plaine ;</l>
							<l n="146" num="7.4">La pluie et la poussière ont pénétré leur laine ;</l>
							<l n="147" num="7.5">Demain, dans le ruisseau qui baigne le vallon,</l>
							<l n="148" num="7.6">J’irai t’aider moi-même à blanchir leur toison ;</l>
							<l n="149" num="7.7">J’irai… de ma Philis tu vois venir le père ;</l>
							<l n="150" num="7.8">Elle court dans ses bras, et l’atteint la première.</l>
							<l n="151" num="7.9">Ô berger, si jamais, seul et loin de ton fils,</l>
							<l n="152" num="7.10">L’orage te surprend, souviens-toi de Philis !</l>
						</lg>
					</div></body></text></TEI>