<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>9618 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>9618 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies</title>
					<author>Marceline Desbordes-Valmore</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>A. Boulland, libraire-éditeur</publisher>
						<date when="1830">1830</date>
					</imprint>
					<extent>9618 verstome second</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-12-20" who="RR">Insertion des corrections métriques dans le corps du texte.</change>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
				<change when="2018-11-02" who="RR">Correction dans le poème "Dors, ma Mère".</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part"> POÉSIES INÉDITES <lb></lb>MÉLANGES</head><div type="poem" key="DES174">
						<head type="main">LE MENDIANT</head>
						<head type="sub_1">IMITÉ DE L’ANGLAIS</head>
						<lg n="1">
							<l n="1" num="1.1">Un ministre du ciel courbé sous les offrandes</l>
							<l n="2" num="1.2">Que la piété riche aux pauvres destinait,</l>
							<l n="3" num="1.3">Fier de son lourd fardeau, lentement cheminait,</l>
							<l n="4" num="1.4">Pesant les fruits sacrés de ses saintes demandes.</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1">« Mon père, ayez pitié d’un homme malheureux,</l>
							<l n="6" num="2.2">Lui crie un indigent qui traînait sa misère ;</l>
							<l n="7" num="2.3">Vous avez recueilli des bienfaits si nombreux !</l>
							<l n="8" num="2.4">Vous avez attendri tant de cœurs généreux !</l>
							<l n="9" num="2.5">Donnez-moi ; votre marche en sera plus légère.</l>
						</lg>
						<lg n="3">
							<l n="10" num="3.1">— Au loin, dit le saint homme, au loin ! Quels sont vos droits</l>
							<l n="11" num="3.2">Pour oser aspirer aux aumônes sacrées ?</l>
							<l n="12" num="3.3">Ce n’est point aux passants qu’elles sont consacrées ;</l>
							<l n="13" num="3.4">Au loin ! je suis en eau : chacun porte sa croix !</l>
						</lg>
						<lg n="4">
							<l n="14" num="4.1">— Mais, mon père, je meurs ! — Eh bien ! est-ce ma faute ?</l>
							<l n="15" num="4.2">Je vous trouve plaisant de vous en plaindre à moi :</l>
							<l n="16" num="4.3">Les gueux ont aujourd’hui la prière bien haute !</l>
							<l n="17" num="4.4">J’ai mes pauvres, passez ! Allez servir le roi.</l>
							<l n="18" num="4.5">— Mon père, je suis vieux. — Je vous en félicite ;</l>
							<l n="19" num="4.6">Vous aurez moins longtemps à souffrir ici-bas.</l>
							<l n="20" num="4.7">— Au nom de Dieu ! du pain, mon père ! — Passez vite,</l>
							<l n="21" num="4.8">Importun vagabond ! — Je me retire, hélas !</l>
							<l n="22" num="4.9">Laissez tomber au moins une céleste aumône</l>
							<l n="23" num="4.10">Sur ma faim qu’en passant vous pouviez soulager ;</l>
							<l n="24" num="4.11">Vos bénédictions… — Oui, mon fils, Dieu l’ordonne :</l>
							<l n="25" num="4.12">Puisque tu vas mourir, tu fais bien d’y songer.</l>
							<l n="26" num="4.13">Mets-toi donc à genoux. — Et moi je les refuse,</l>
							<l n="27" num="4.14"><space quantity="6" unit="char"></space>Dit le pauvre d’un ton moqueur ;</l>
							<l n="28" num="4.15"><space quantity="6" unit="char"></space>Passez, père, je vous récuse,</l>
							<l n="29" num="4.16"><space quantity="6" unit="char"></space>N’épuisez pas votre bon cœur.</l>
							<l n="30" num="4.17">J’ai trop faim pour courir <choice reason="analysis" type="false_verse" hand="CA"><sic>derrière</sic><corr source="édition_1973">après</corr></choice> l’oiseau qui vole ;</l>
							<l n="31" num="4.18">Vos bénédictions ne sauraient me nourrir :</l>
							<l n="32" num="4.19"><space quantity="6" unit="char"></space>Le don ne vaut pas une obole,</l>
							<l n="33" num="4.20"><space quantity="6" unit="char"></space>Puisque vous daignez me l’offrir. »</l>
						</lg>
					</div></body></text></TEI>