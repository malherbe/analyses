<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>9618 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>9618 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies</title>
					<author>Marceline Desbordes-Valmore</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>A. Boulland, libraire-éditeur</publisher>
						<date when="1830">1830</date>
					</imprint>
					<extent>9618 verstome second</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-12-20" who="RR">Insertion des corrections métriques dans le corps du texte.</change>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
				<change when="2018-11-02" who="RR">Correction dans le poème "Dors, ma Mère".</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part"> POÉSIES INÉDITES <lb></lb>MÉLANGES</head><div type="poem" key="DES150">
						<head type="main">DEUX RAMIERS</head>
						<lg n="1">
							<l n="1" num="1.1">D’où venez-vous, couple triste et charmant ?</l>
							<l n="2" num="1.2">Rien parmi nous ne vous appelle encore ;</l>
							<l n="3" num="1.3">Les jours d’avril n’ont qu’une pâle aurore,</l>
							<l n="4" num="1.4">Et nul abri pour l’amoureux tourment ;</l>
							<l n="5" num="1.5">Les blés frileux cachant leurs fronts timides,</l>
							<l n="6" num="1.6">Comme les fleurs, tremblent au vent du nord ;</l>
							<l n="7" num="1.7">Le lierre seul couvre les murs humides ;</l>
							<l n="8" num="1.8">Et l’hirondelle est toujours loin du port.</l>
							<l n="9" num="1.9">Vous deux chassés par le malheur, sans doute,</l>
							<l n="10" num="1.10">Et consolés du malheur par l’amour,</l>
							<l n="11" num="1.11">Pour échapper à quelque noir vautour,</l>
							<l n="12" num="1.12">De l’Orient vous avez fui la route,</l>
							<l n="13" num="1.13">Au toit prochain, je vous entends gémir ;</l>
							<l n="14" num="1.14">Ah ! vous souffrez… je ne sais plus dormir !</l>
							<l n="15" num="1.15">Des vrais amants doux et discrets modèles,</l>
							<l n="16" num="1.16">J’ai vos douleurs ; que n’ai-je aussi vos ailes !</l>
							<l n="17" num="1.17">Je volerais sur votre humble rempart ;</l>
							<l n="18" num="1.18">Tristes ramiers, j’irais, triste moi-même,</l>
							<l n="19" num="1.19">En souvenir d’un malheureux que j’aime,</l>
							<l n="20" num="1.20">Du peu que j’ai vous offrir une part.</l>
							<l n="21" num="1.21">Il erre seul… et vous errez ensemble !</l>
							<l n="22" num="1.22">Dans vos baisers que votre exil est doux !</l>
							<l n="23" num="1.23">Le même sort vous frappe et vous rassemble ;</l>
							<l n="24" num="1.24">Oh ! que d’amants sont moins heureux que vous !</l>
							<l n="25" num="1.25">Venez tous deux, venez sur ma fenêtre</l>
							<l n="26" num="1.26">De votre soif étancher les ardeurs ;</l>
							<l n="27" num="1.27">Des cieux dorés, où l’amour vous fit naître,</l>
							<l n="28" num="1.28">Au toit du pauvre oubliez les splendeurs.</l>
							<l n="29" num="1.29">Que l’un de vous se hasarde à descendre ;</l>
							<l n="30" num="1.30">Le plus hardi doit guider le plus tendre ;</l>
							<l n="31" num="1.31">D’un cœur qui bat d’amour et de frayeur</l>
							<l n="32" num="1.32">Pour un moment qu’il détache son cœur.</l>
							<l n="33" num="1.33">Voici du grain, voici de l’eau limpide,</l>
							<l n="34" num="1.34">Humble secours par mes mains répandu ;</l>
							<l n="35" num="1.35">Il soutiendra votre destin timide,</l>
							<l n="36" num="1.36">Si tout un jour vous l’avez attendu !</l>
							<l n="37" num="1.37">Ainsi, mon Dieu, sur la route lointaine,</l>
							<l n="38" num="1.38">Semez vos dons à mon cher voyageur !</l>
							<l n="39" num="1.39">Ne souffrez pas que quelque voix hautaine</l>
							<l n="40" num="1.40">Sur son front pur appelle la rougeur.</l>
							<l n="41" num="1.41">Que ma prière en tout lieu le devance ;</l>
							<l n="42" num="1.42">Dieu ! que pas un ne le nomme étranger !</l>
							<l n="43" num="1.43">Aidez son cœur à porter notre absence,</l>
							<l n="44" num="1.44">Et que parfois le temps lui soit léger !</l>
						</lg>
					</div></body></text></TEI>