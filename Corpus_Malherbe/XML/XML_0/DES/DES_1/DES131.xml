<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>9618 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>9618 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies</title>
					<author>Marceline Desbordes-Valmore</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>A. Boulland, libraire-éditeur</publisher>
						<date when="1830">1830</date>
					</imprint>
					<extent>9618 verstome second</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-12-20" who="RR">Insertion des corrections métriques dans le corps du texte.</change>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
				<change when="2018-11-02" who="RR">Correction dans le poème "Dors, ma Mère".</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">POÉSIES DIVERSES</head><div type="poem" key="DES131">
						<head type="main">L’ORPHELINE</head>
						<lg n="1">
							<l n="1" num="1.1"><space quantity="6" unit="char"></space>Un seigneur, d’aimable figure,</l>
							<l n="2" num="1.2"><space quantity="2" unit="char"></space>Brillant d’esprit et brillant de parure,</l>
							<l n="3" num="1.3">Prestiges tout-puissants sur la simplicité,</l>
							<l n="4" num="1.4"><space quantity="2" unit="char"></space>Voulut séduire une jeune beauté.</l>
							<l n="5" num="1.5">Sans appui dans le monde, elle était orpheline,</l>
							<l n="6" num="1.6"><space quantity="10" unit="char"></space>Et se nommait Pauline.</l>
							<l n="7" num="1.7"><space quantity="2" unit="char"></space>Pauline, hélas ! a perdu le repos,</l>
							<l n="8" num="1.8"><space quantity="2" unit="char"></space>De vifs regards, de séduisants propos</l>
							<l n="9" num="1.9"><space quantity="2" unit="char"></space>Troublent la paix de cette âme ingénue,</l>
							<l n="10" num="1.10"><space quantity="2" unit="char"></space>Elle aime enfin, et son heure est venue.</l>
							<l n="11" num="1.11"><space quantity="2" unit="char"></space>Pour un ingrat devait-elle sonner ?</l>
							<l n="12" num="1.12">Mais pour craindre cette heure, il faut la deviner ;</l>
							<l n="13" num="1.13"><space quantity="2" unit="char"></space>Et l’Orpheline, en sa première flamme,</l>
							<l n="14" num="1.14"><space quantity="2" unit="char"></space>Rêve l’amour aussi pur que son âme.</l>
							<l n="15" num="1.15"><space quantity="2" unit="char"></space>Six mois ainsi coulent rapidement.</l>
							<l n="16" num="1.16"><space quantity="2" unit="char"></space>Tout est bonheur, ivresse, enchantement.</l>
							<l n="17" num="1.17"><space quantity="2" unit="char"></space>Un villageois qui soupirait pour elle,</l>
							<l n="18" num="1.18"><space quantity="2" unit="char"></space>Renferme alors sa tendresse fidèle ;</l>
							<l n="19" num="1.19">Edmond ne la suit plus, et cache à tous les yeux</l>
							<l n="20" num="1.20"><space quantity="2" unit="char"></space>Son humble hommage et ses timides vœux.</l>
							<l n="21" num="1.21"><space quantity="2" unit="char"></space>Sans le vouloir, Pauline a su lui plaire :</l>
							<l n="22" num="1.22"><space quantity="2" unit="char"></space>Edmond n’a plus qu’à l’aimer et se taire.</l>
							<l n="23" num="1.23"><space quantity="2" unit="char"></space>L’amour modeste est souvent méconnu ;</l>
							<l n="24" num="1.24"><space quantity="2" unit="char"></space>Pour éblouir il est trop ingénu.</l>
							<l n="25" num="1.25"><space quantity="2" unit="char"></space>Sans s’occuper d’un amant qu’elle ignore,</l>
							<l n="26" num="1.26"><space quantity="2" unit="char"></space>Pauline est tout à celui qu’elle adore ;</l>
							<l n="27" num="1.27"><space quantity="2" unit="char"></space>Elle ne voit encor dans l’avenir</l>
							<l n="28" num="1.28"><space quantity="2" unit="char"></space>Que le moment où l’ingrat doit venir ;</l>
							<l n="29" num="1.29"><space quantity="2" unit="char"></space>Et respectant le séducteur qu’elle aime,</l>
							<l n="30" num="1.30"><space quantity="2" unit="char"></space>Croit n’adorer que la sagesse même.</l>
							<l n="31" num="1.31"><space quantity="2" unit="char"></space>Pensive et seule, elle y rêvait un soir ;</l>
							<l n="32" num="1.32"><space quantity="2" unit="char"></space>Dans sa cabane il entre avec l’espoir.</l>
							<l n="33" num="1.33"><space quantity="2" unit="char"></space>L’amour, la nuit, la crainte, le silence,</l>
							<l n="34" num="1.34"><space quantity="2" unit="char"></space>Tout est d’accord pour perdre l’innocence.</l>
							<l n="35" num="1.35"><space quantity="2" unit="char"></space>Les yeux baissés, d’un air naïf et doux.</l>
							<l n="36" num="1.36">Elle pleure en voyant son seigneur à genoux.</l>
							<l n="37" num="1.37"><space quantity="2" unit="char"></space>Riant tout bas de ses tendres alarmes,</l>
							<l n="38" num="1.38"><space quantity="2" unit="char"></space>À peine il voit ses peines et ses larmes.</l>
							<l n="39" num="1.39"><space quantity="2" unit="char"></space>Sans deviner qu’on lui vole un plaisir,</l>
							<l n="40" num="1.40"><space quantity="2" unit="char"></space>Pauline, hélas ! en eut le repentir.</l>
							<l n="41" num="1.41"><space quantity="2" unit="char"></space>Le lendemain, dans sa simple demeure,</l>
							<l n="42" num="1.42"><space quantity="2" unit="char"></space>Avec l’Amour elle attendit en vain ;</l>
							<l n="43" num="1.43"><space quantity="2" unit="char"></space>Elle attendit encor le lendemain,</l>
							<l n="44" num="1.44">Le mois entier, chaque jour, à toute heure !</l>
							<l n="45" num="1.45"><space quantity="2" unit="char"></space>Par le remords lentement déchiré,</l>
							<l n="46" num="1.46"><space quantity="2" unit="char"></space>D’un sombre ennui son cœur est dévoré.</l>
							<l n="47" num="1.47"><space quantity="2" unit="char"></space>Elle offre à Dieu cet amour qui l’opprime ;</l>
							<l n="48" num="1.48">Puisqu’il fait tant de mal, il faut qu’il soit un crime.</l>
							<l n="49" num="1.49"><space quantity="2" unit="char"></space>Mais ne vivant que par le souvenir,</l>
							<l n="50" num="1.50">Le passé la poursuit jusque dans l’avenir.</l>
							<l n="51" num="1.51"><space quantity="2" unit="char"></space>Plus de sommeil ; Pauline en vain l’appelle ;</l>
							<l n="52" num="1.52"><space quantity="2" unit="char"></space>Pour le malheur il est sourd et rebelle.</l>
							<l n="53" num="1.53"><space quantity="2" unit="char"></space>Plus de vertu, plus d’amis, plus d’amant ;</l>
							<l n="54" num="1.54"><space quantity="2" unit="char"></space>Tout est perdu par l’erreur d’un moment.</l>
							<l n="55" num="1.55">C’est la fleur du vallon sur sa tige abattue</l>
							<l n="56" num="1.56"><space quantity="2" unit="char"></space>Par le frimas qui l’effeuille et la tue.</l>
						</lg>
						<lg n="2">
							<l n="57" num="2.1"><space quantity="2" unit="char"></space>C’était l’hiver : la saison de l’Amour</l>
							<l n="58" num="2.2"><space quantity="2" unit="char"></space>Semblait avoir disparu sans retour.</l>
							<l n="59" num="2.3"><space quantity="2" unit="char"></space>Assise, un soir, au bord de sa chaumière,</l>
							<l n="60" num="2.4"><space quantity="2" unit="char"></space>Pleurant sa honte et fuyant la lumière,</l>
							<l n="61" num="2.5"><space quantity="2" unit="char"></space>Un bruit soudain fait tressaillir son cœur ;</l>
							<l n="62" num="2.6"><space quantity="2" unit="char"></space>Un char léger ramène son vainqueur…</l>
							<l n="63" num="2.7"><space quantity="2" unit="char"></space>Il a parlé… c’est la voix qu’elle adore :</l>
							<l n="64" num="2.8"><space quantity="2" unit="char"></space>« C’est lui, dit-elle, il vient, il m’aime encore ! »</l>
							<l n="65" num="2.9"><space quantity="2" unit="char"></space>Mais un regard fait tout évanouir ;</l>
							<l n="66" num="2.10"><space quantity="2" unit="char"></space>L’espoir s’enfuit… Pauline va mourir.</l>
							<l n="67" num="2.11"><space quantity="2" unit="char"></space>Oui, c’est l’ingrat qu’elle attend et qu’elle aime.</l>
							<l n="68" num="2.12"><space quantity="2" unit="char"></space>Mais peignez-vous son désespoir extrême !</l>
							<l n="69" num="2.13"><space quantity="2" unit="char"></space>Il n’est pas seul. Il entraîne, à son tour,</l>
							<l n="70" num="2.14"><space quantity="2" unit="char"></space>L’objet nouveau de son volage amour.</l>
							<l n="71" num="2.15"><space quantity="2" unit="char"></space>À cette vue, immobile et glacée,</l>
							<l n="72" num="2.16"><space quantity="2" unit="char"></space>Le cœur saisi d’une affreuse pensée,</l>
							<l n="73" num="2.17"><space quantity="2" unit="char"></space>Pauline au ciel jette un cri douloureux,</l>
							<l n="74" num="2.18"><space quantity="2" unit="char"></space>Tombe à genoux et détourne les yeux.</l>
							<l n="75" num="2.19"><space quantity="2" unit="char"></space>Le froid du soir circule dans ses veines ;</l>
							<l n="76" num="2.20">Son âme s’engourdit dans l’oubli de ses peines ;</l>
							<l n="77" num="2.21">Et, prenant par degrés le sommeil pour la mort,</l>
							<l n="78" num="2.22">En embrassant la terre, elle pleure et s’endort.</l>
						</lg>
						<lg n="3">
							<l n="79" num="3.1">Dieu qui la plaint l’enveloppe d’un songe ;</l>
							<l n="80" num="3.2">Et la pitié descend sur l’aile du mensonge !</l>
							<l n="81" num="3.3">Elle croit voir un Ange protecteur</l>
							<l n="82" num="3.4">La ranimer doucement sur son cœur,</l>
							<l n="83" num="3.5">Presser sa main, l’observer en silence,</l>
							<l n="84" num="3.6">Les yeux mouillés des pleurs de l’indulgence.</l>
							<l n="85" num="3.7"><space quantity="2" unit="char"></space>« Dieu vous a donc envoyé près de moi,</l>
							<l n="86" num="3.8"><space quantity="2" unit="char"></space>Lui dit Pauline, et vous suivez sa loi ?</l>
							<l n="87" num="3.9"><space quantity="2" unit="char"></space>Si la vertu vient essuyer mes larmes,</l>
							<l n="88" num="3.10"><space quantity="2" unit="char"></space>Parlez ! sa voix aura pour moi des charmes.</l>
							<l n="89" num="3.11"><space quantity="2" unit="char"></space>Voyez mon sort ! voyez mon repentir ! »</l>
							<l n="90" num="3.12"><space quantity="2" unit="char"></space>On lui répond par un profond soupir.</l>
							<l n="91" num="3.13"><space quantity="2" unit="char"></space>Son œil mourant s’entr’ouvre à la lumière.</l>
							<l n="92" num="3.14"><space quantity="2" unit="char"></space>L’Ange est Edmond à genoux, sur la pierre,</l>
							<l n="93" num="3.15"><space quantity="2" unit="char"></space>Qui plein d’effroi, soutient d’un bras tremblant,</l>
							<l n="94" num="3.16"><space quantity="2" unit="char"></space>Ce corps glacé qu’il réchauffe en pleurant.</l>
							<l n="95" num="3.17"><space quantity="2" unit="char"></space>« Ne craignez rien, dit l’amant jeune et sage ;</l>
							<l n="96" num="3.18"><space quantity="2" unit="char"></space>Sans défiance appuyez-vous sur moi ;</l>
							<l n="97" num="3.19"><space quantity="2" unit="char"></space>Notre cabane est au bout du village ;</l>
							<l n="98" num="3.20"><space quantity="2" unit="char"></space>Un cri plaintif vient d’y porter l’effroi.</l>
							<l n="99" num="3.21"><space quantity="2" unit="char"></space>Ma mère attend, venez près de ma mère ;</l>
							<l n="100" num="3.22"><space quantity="2" unit="char"></space>Vous lui direz le sujet de vos pleurs ;</l>
							<l n="101" num="3.23"><space quantity="2" unit="char"></space>Ma mère est bonne, elle plaint vos douleurs ;</l>
							<l n="102" num="3.24">Soyez sa fille, et moi… je serai votre frère.</l>
							<l n="103" num="3.25"><space quantity="2" unit="char"></space>— Hélas ! dit-elle, avec même douceur,</l>
							<l n="104" num="3.26"><space quantity="2" unit="char"></space>Soyez mon frère, et sauvez votre sœur. »</l>
						</lg>
					</div></body></text></TEI>