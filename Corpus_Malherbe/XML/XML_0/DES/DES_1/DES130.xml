<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>9618 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>9618 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies</title>
					<author>Marceline Desbordes-Valmore</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>A. Boulland, libraire-éditeur</publisher>
						<date when="1830">1830</date>
					</imprint>
					<extent>9618 verstome second</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-12-20" who="RR">Insertion des corrections métriques dans le corps du texte.</change>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
				<change when="2018-11-02" who="RR">Correction dans le poème "Dors, ma Mère".</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">POÉSIES DIVERSES</head><div type="poem" key="DES130">
						<head type="main">L’HIRONDELLE ET LE ROSSIGNOL</head>
						<head type="sub_1">À M. ARNAUD</head>
						<lg n="1">
							<l n="1" num="1.1">Prête à s’élancer, joyeuse,</l>
							<l n="2" num="1.2">Aux libres plaines des cieux,</l>
							<l n="3" num="1.3">L’Hirondelle voyageuse</l>
							<l n="4" num="1.4">À la saison pluvieuse</l>
							<l n="5" num="1.5">Jetait un long cri d’adieu.</l>
						</lg>
						<lg n="2">
							<l n="6" num="2.1">Sous un chêne solitaire</l>
							<l n="7" num="2.2">Elle entend le rossignol ;</l>
							<l n="8" num="2.3">Sa voix lui fut toujours chère ;</l>
							<l n="9" num="2.4">Et la jeune passagère</l>
							<l n="10" num="2.5">Écoute, et suspend son vol.</l>
						</lg>
						<lg n="3">
							<l n="11" num="3.1">Elle recueille, attentive,</l>
							<l n="12" num="3.2">L’accent qui cherche le cœur ;</l>
							<l n="13" num="3.3">Mais ce chant qui la captive,</l>
							<l n="14" num="3.4">Dans sa mesure moins vive,</l>
							<l n="15" num="3.5">N’exprime plus le bonheur !</l>
						</lg>
						<lg n="4">
							<l n="16" num="4.1">« À quoi rêvez-vous, dit-elle ?</l>
							<l n="17" num="4.2">Les zéphyrs sont au beau temps ;</l>
							<l n="18" num="4.3">Sur la rive maternelle</l>
							<l n="19" num="4.4">Le doux printemps vous appelle ;</l>
							<l n="20" num="4.5">N’aimez-vous plus le printemps ?</l>
						</lg>
						<lg n="5">
							<l n="21" num="5.1">— Sauvez-vous, pauvre petite,</l>
							<l n="22" num="5.2">Sans me demander pourquoi</l>
							<l n="23" num="5.3">J’ai choisi ce sombre gîte :</l>
							<l n="24" num="5.4">L’oiseleur, qu’en vain j’évite,</l>
							<l n="25" num="5.5">Vous l’apprendrait mieux que moi. »</l>
						</lg>
						<lg n="6">
							<l n="26" num="6.1">Alors autour du grand chêne</l>
							<l n="27" num="6.2">Elle entrevoit des réseaux ;</l>
							<l n="28" num="6.3">Gémissante et hors d’haleine,</l>
							<l n="29" num="6.4">Elle veut briser la chaîne</l>
							<l n="30" num="6.5">Du roi des petits oiseaux.</l>
						</lg>
						<lg n="7">
							<l n="31" num="7.1">« Vous n’êtes pas assez forte,</l>
							<l n="32" num="7.2">Dit-il ; mais consolez-vous.</l>
							<l n="33" num="7.3">Du monde il faut que tout sorte ;</l>
							<l n="34" num="7.4">Dieu n’y plaça qu’une porte,</l>
							<l n="35" num="7.5">Et la Mort l’ouvre pour tous.</l>
						</lg>
						<lg n="8">
							<l n="36" num="8.1">Sur cette plage étrangère,</l>
							<l n="37" num="8.2">Égales à leur réveil,</l>
							<l n="38" num="8.3">Et la reine et la bergère,</l>
							<l n="39" num="8.4">Sous le marbre et la fougère,</l>
							<l n="40" num="8.5">Dorment du même sommeil.</l>
						</lg>
						<lg n="9">
							<l n="41" num="9.1">Sous cette loi simple et juste</l>
							<l n="42" num="9.2">On voit passer tour à tour</l>
							<l n="43" num="9.3">L’oiseleur, l’oiseau, l’arbuste,</l>
							<l n="44" num="9.4">Les rois et leur race auguste :</l>
							<l n="45" num="9.5">J’y passerai donc un jour.</l>
						</lg>
						<lg n="10">
							<l n="46" num="10.1">Mais des rois l’ombre incertaine</l>
							<l n="47" num="10.2">Demande grâce souvent</l>
							<l n="48" num="10.3">Au destin qui les entraîne :</l>
							<l n="49" num="10.4">L’oiseau blessé qui s’y traîne</l>
							<l n="50" num="10.5">Se repose en arrivant.</l>
						</lg>
						<lg n="11">
							<l n="51" num="11.1">Là, de la flèche empennée</l>
							<l n="52" num="11.2">Tous les traits sont amortis ;</l>
							<l n="53" num="11.3">Et la mère infortunée,</l>
							<l n="54" num="11.4">Libre, et désemprisonnée,</l>
							<l n="55" num="11.5">Chante auprès de ses petits !</l>
						</lg>
						<lg n="12">
							<l n="56" num="12.1">Si votre pitié naïve</l>
							<l n="57" num="12.2">Ne craint pas de nouveaux pleurs,</l>
							<l n="58" num="12.3">Cherchez, au bord de la rive,</l>
							<l n="59" num="12.4">Une feuille fugitive</l>
							<l n="60" num="12.5">Où sont gravés mes malheurs . »</l>
						</lg>
						<lg n="13">
							<l n="61" num="13.1">Sous l’ombre mystérieuse</l>
							<l n="62" num="13.2">La feuille alors murmura ;</l>
							<l n="63" num="13.3">Et, longtemps silencieuse,</l>
							<l n="64" num="13.4">Plus triste que curieuse,</l>
							<l n="65" num="13.5">L’Hirondelle soupira.</l>
						</lg>
						<lg n="14">
							<l n="66" num="14.1">« Adieu donc, s’écria-t-elle,</l>
							<l n="67" num="14.2">Puisqu’il faut partir sans vous !</l>
							<l n="68" num="14.3">Puisse une feuille nouvelle,</l>
							<l n="69" num="14.4">Quelque jour, à l’Hirondelle</l>
							<l n="70" num="14.5">Révéler un sort plus doux ! »</l>
						</lg>
					</div></body></text></TEI>