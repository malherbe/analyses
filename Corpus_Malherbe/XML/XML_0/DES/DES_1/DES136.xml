<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>9618 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>9618 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies</title>
					<author>Marceline Desbordes-Valmore</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>A. Boulland, libraire-éditeur</publisher>
						<date when="1830">1830</date>
					</imprint>
					<extent>9618 verstome second</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-12-20" who="RR">Insertion des corrections métriques dans le corps du texte.</change>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
				<change when="2018-11-02" who="RR">Correction dans le poème "Dors, ma Mère".</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">POÉSIES DIVERSES</head><div type="poem" key="DES136">
						<head type="main">LE PETIT ARTHUR DE BRETAGNE</head>
						<head type="sub_1">À LA TOUR DE ROUEN</head>
						<lg n="1">
							<l n="1" num="1.1">Par mon baptême, ô ma mère,</l>
							<l n="2" num="1.2">Je voudrais être l’enfant</l>
							<l n="3" num="1.3">Qui bondit sur la bruyère</l>
							<l n="4" num="1.4">Avec l’agneau qu’il défend.</l>
							<l n="5" num="1.5">J’ai soif de l’eau qui murmure</l>
							<l n="6" num="1.6">Et fuit là-bas dans les fleurs :</l>
							<l n="7" num="1.7">L’eau de la tour est moins pure,</l>
							<l n="8" num="1.8">Je la trouble avec mes pleurs.</l>
						</lg>
						<lg n="2">
							<l n="9" num="2.1">Si le rayon d’une étoile</l>
							<l n="10" num="2.2">Glisse au fond de ma prison,</l>
							<l n="11" num="2.3">Les barreaux forment un voile</l>
							<l n="12" num="2.4">Qui tourmente ma raison.</l>
							<l n="13" num="2.5">Quand le fer qui se colore</l>
							<l n="14" num="2.6">M’annonce que le jour luit,</l>
							<l n="15" num="2.7">Le petit Arthur encore</l>
							<l n="16" num="2.8">Est triste comme la nuit.</l>
						</lg>
						<lg n="3">
							<l n="17" num="3.1">Pour bercer ma jeune enfance</l>
							<l n="18" num="3.2">Vous saviez des airs touchants ;</l>
							<l n="19" num="3.3">Et j’ai reçu la défense</l>
							<l n="20" num="3.4">De me rappeler vos chants !</l>
							<l n="21" num="3.5">Mais que la flûte lointaine</l>
							<l n="22" num="3.6">M’apporte un réveil plus doux,</l>
							<l n="23" num="3.7">Je tressaille dans ma chaîne ;</l>
							<l n="24" num="3.8">Ma mère, je pense à vous.</l>
						</lg>
						<lg n="4">
							<l n="25" num="4.1">Ce vieux gardien dont l’œil sombre</l>
							<l n="26" num="4.2">Un soir me remplit d’effroi,</l>
							<l n="27" num="4.3">Qui, sur mes pas, comme une ombre,</l>
							<l n="28" num="4.4">Fit peur au pauvre enfant-roi,</l>
							<l n="29" num="4.5">J’ai vu son front, moins austère,</l>
							<l n="30" num="4.6">Vers ses enfants se baisser :</l>
							<l n="31" num="4.7">Hélas ! que n’est-il mon père !</l>
							<l n="32" num="4.8">Il daignerait m’embrasser.</l>
						</lg>
						<lg n="5">
							<l n="33" num="5.1">Lorsque la fièvre brûlante</l>
							<l n="34" num="5.2">Sur lui fit planer la mort,</l>
							<l n="35" num="5.3">Sa bouche, pâle et tremblante,</l>
							<l n="36" num="5.4">Dit qu’il avait un remord.</l>
							<l n="37" num="5.5">De cette affreuse démence</l>
							<l n="38" num="5.6">Cherchant à le secourir,</l>
							<l n="39" num="5.7">J’ai chanté votre romance</l>
							<l n="40" num="5.8">Pour l’empêcher de souffrir.</l>
						</lg>
						<lg n="6">
							<l n="41" num="6.1">Aux sons de la vieille harpe</l>
							<l n="42" num="6.2">Il s’endormit sur mon sein,</l>
							<l n="43" num="6.3">Enveloppé de l’écharpe</l>
							<l n="44" num="6.4">Dont me para votre main.</l>
							<l n="45" num="6.5">Une reine l’a brodée :</l>
							<l n="46" num="6.6">Mon geôlier la garde encor…</l>
							<l n="47" num="6.7">Je ne l’ai plus demandée ;</l>
							<l n="48" num="6.8">Et c’était mon seul trésor.</l>
						</lg>
						<lg n="7">
							<l n="49" num="7.1">Peut-être ce sacrifice</l>
							<l n="50" num="7.2">En secret l’attendrira</l>
							<l n="51" num="7.3">Et qu’à vos larmes, propice,</l>
							<l n="52" num="7.4">Un moment il me rendra.</l>
							<l n="53" num="7.5">Mes biens, mes jours, ma couronne,</l>
							<l n="54" num="7.6">Tout ce qu’ils brûlent d’avoir,</l>
							<l n="55" num="7.7">Oh ! ma mère, je le donne ;</l>
							<l n="56" num="7.8">Mais avant je veux vous voir.</l>
						</lg>
						<lg n="8">
							<l n="57" num="8.1">Malgré leur veille farouche,</l>
							<l n="58" num="8.2">J’appris seul à retracer</l>
							<l n="59" num="8.3">Le premier nom que ma bouche</l>
							<l n="60" num="8.4">Essaya de prononcer.</l>
							<l n="61" num="8.5">Ne pouvant briser la pierre</l>
							<l n="62" num="8.6">Où j’ai nommé leur vainqueur,</l>
							<l n="63" num="8.7">Ils ont brûlé ma paupière ;</l>
							<l n="64" num="8.8">Mais la mémoire est au cœur.</l>
						</lg>
						<lg n="9">
							<l n="65" num="9.1">En vain leurs bandeaux funèbres</l>
							<l n="66" num="9.2">Ont puni mes faibles yeux ;</l>
							<l n="67" num="9.3">À genoux, dans les ténèbres,</l>
							<l n="68" num="9.4">Ma prière monte aux cieux ;</l>
							<l n="69" num="9.5">L’épée y dort suspendue ;</l>
							<l n="70" num="9.6">Comme vous en ce séjour,</l>
							<l n="71" num="9.7">Mon père, on la croit perdue :</l>
							<l n="72" num="9.8">Mais si je l’atteins un jour !…</l>
						</lg>
					</div></body></text></TEI>