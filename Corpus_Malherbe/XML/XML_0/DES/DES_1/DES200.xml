<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>9618 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>9618 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies</title>
					<author>Marceline Desbordes-Valmore</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>A. Boulland, libraire-éditeur</publisher>
						<date when="1830">1830</date>
					</imprint>
					<extent>9618 verstome second</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-12-20" who="RR">Insertion des corrections métriques dans le corps du texte.</change>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
				<change when="2018-11-02" who="RR">Correction dans le poème "Dors, ma Mère".</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part"> POÉSIES INÉDITES <lb></lb>ROMANCES</head><div type="poem" key="DES200">
						<head type="main">LA NOVICE</head>
						<head type="sub_1">IMITÉ DE MOORE</head>
						<lg n="1">
							<l n="1" num="1.1">Une jeune et blanche novice,</l>
							<l n="2" num="1.2">À l’ombre des bosquets cloîtrés</l>
							<l n="3" num="1.3">Rêvant à son pur sacrifice,</l>
							<l n="4" num="1.4">Promenait ses vœux timorés ;</l>
							<l n="5" num="1.5">Et sur des agnus consacrés</l>
							<l n="6" num="1.6">Chantait des cantiques sacrés.</l>
						</lg>
						<lg n="2">
							<l n="7" num="2.1">« Ici nous vivons, disait-elle,</l>
							<l n="8" num="2.2">Mortes aux terrestres douleurs,</l>
							<l n="9" num="2.3">Et les Anges sous leur tutèle,</l>
							<l n="10" num="2.4">Nous gardent des tendres malheurs ;</l>
							<l n="11" num="2.5">Nos soupirs, sur l’encens des fleurs,</l>
							<l n="12" num="2.6">S’en vont aux cieux avec nos pleurs.</l>
						</lg>
						<lg n="3">
							<l n="13" num="3.1">Amour ! laisse en paix ma cellule !</l>
							<l n="14" num="3.2">Sœur Isaure dit qu’autrefois</l>
							<l n="15" num="3.3">Une sainte jeune et crédule</l>
							<l n="16" num="3.4">Te prit pour un Ange, à ta voix ;</l>
							<l n="17" num="3.5">Et que l’ange, au pied de la croix</l>
							<l n="18" num="3.6">Te ressemble, sans ton carquois. »</l>
						</lg>
						<lg n="4">
							<l n="19" num="4.1">L’Amour alors prêta l’oreille ;</l>
							<l n="20" num="4.2">Il dormait sur l’aile du vent.</l>
							<l n="21" num="4.3">Un soupir l’offense et l’éveille ;</l>
							<l n="22" num="4.4">Hélas ! qu’il s’éveille souvent !</l>
							<l n="23" num="4.5">Comme un ange ami du couvent,</l>
							<l n="24" num="4.6">Il apparut tendre et fervent.</l>
						</lg>
						<lg n="5">
							<l n="25" num="5.1">Ses yeux bleus, riants et perfides,</l>
							<l n="26" num="5.2">Amortis par la piété,</l>
							<l n="27" num="5.3">Lancèrent des flammes timides</l>
							<l n="28" num="5.4">Au cœur de la jeune beauté.</l>
							<l n="29" num="5.5">« Dieu ! dit-elle, à votre clarté,</l>
							<l n="30" num="5.6">Je vois un ange en vérité ! »</l>
						</lg>
						<lg n="6">
							<l n="31" num="6.1">Cet ange aux mystiques paupières</l>
							<l n="32" num="6.2">Est un Dieu cruel et moqueur ;</l>
							<l n="33" num="6.3">Tes pleurs, ton encens, tes prières,</l>
							<l n="34" num="6.4">Ne guériront pas ta langueur :</l>
							<l n="35" num="6.5">Tu ne fuiras plus ton vainqueur,</l>
							<l n="36" num="6.6">Jeune sainte, il est dans ton cœur.</l>
						</lg>
						<lg n="7">
							<l n="37" num="7.1">Ses yeux illuminent ton âme,</l>
							<l n="38" num="7.2">Ses soupirs répondent aux tiens ;</l>
							<l n="39" num="7.3">Les autels brûlent de sa flamme,</l>
							<l n="40" num="7.4">Et tes feux ne sont plus chrétiens ;</l>
							<l n="41" num="7.5">Grand Dieu ! ses trompeurs entretiens,</l>
							<l n="42" num="7.6">Séduiraient les anges gardiens !</l>
						</lg>
					</div></body></text></TEI>