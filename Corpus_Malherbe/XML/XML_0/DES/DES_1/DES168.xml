<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>9618 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>9618 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies</title>
					<author>Marceline Desbordes-Valmore</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>A. Boulland, libraire-éditeur</publisher>
						<date when="1830">1830</date>
					</imprint>
					<extent>9618 verstome second</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-12-20" who="RR">Insertion des corrections métriques dans le corps du texte.</change>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
				<change when="2018-11-02" who="RR">Correction dans le poème "Dors, ma Mère".</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part"> POÉSIES INÉDITES <lb></lb>MÉLANGES</head><div type="poem" key="DES168">
						<head type="main">LA VALLÉE DE LA SCARPE</head>
						<lg n="1">
							<l n="1" num="1.1"><space quantity="6" unit="char"></space>Mon beau pays, mon frais berceau,</l>
							<l n="2" num="1.2"><space quantity="6" unit="char"></space>Air pur de ma verte contrée,</l>
							<l n="3" num="1.3"><space quantity="6" unit="char"></space>Lieux où mon enfance ignorée</l>
							<l n="4" num="1.4"><space quantity="6" unit="char"></space>Coulait comme un humble ruisseau ;</l>
							<l n="5" num="1.5">S’il me reste des jours, m’en irai-je attendrie</l>
							<l n="6" num="1.6">Errer sur vos chemins qui jettent tant de fleurs ,</l>
							<l n="7" num="1.7">Replonger tous mes ans dans une rêverie</l>
							<l n="8" num="1.8">Où l’âme n’entend plus que ce seul mot : « Patrie ! »</l>
							<l n="9" num="1.9"><space quantity="6" unit="char"></space>Et ne répond que par des pleurs ?</l>
							<l n="10" num="1.10">Ciel !… un peu de ma vie ira-t-elle, paisible,</l>
							<l n="11" num="1.11">Se perdre sur la Scarpe au cristal argenté ?</l>
							<l n="12" num="1.12">Cette eau qui m’a portée, innocente et sensible,</l>
							<l n="13" num="1.13">Frémira-t-elle un jour sous mon sort agité ?</l>
							<l n="14" num="1.14">Entendrai-je au rivage, encor cette harmonie,</l>
							<l n="15" num="1.15">Ce bruit de l’univers, cette voie infinie,</l>
							<l n="16" num="1.16">Qui parlait sur ma tête et chantait à la fois</l>
							<l n="17" num="1.17">Comme un peuple lointain répondant à ma voix ?</l>
						</lg>
						<lg n="2">
							<l n="18" num="2.1">Quand le dernier rayon d’un jour qui va s’éteindre</l>
							<l n="19" num="2.2">Colore l’eau qui tremble et qui porte au sommeil,</l>
							<l n="20" num="2.3">Ô mon premier miroir ! ô mon plus doux soleil !</l>
							<l n="21" num="2.4">Je vous vois… et jamais je ne peux vous atteindre !</l>
							<l n="22" num="2.5">Mais cette heure était belle, et belle sa couleur :</l>
							<l n="23" num="2.6">Dans son doux souvenir un moment reposée,</l>
							<l n="24" num="2.7">Elle passe à mon âme ainsi que la rosée</l>
							<l n="25" num="2.8"><space quantity="10" unit="char"></space>Passe au fond d’une fleur.</l>
							<l n="26" num="2.9">D ’un repentir qui dort elle suspend la chaîne ;</l>
							<l n="27" num="2.10">Pour la goûter en paix le temps se meut à peine ;</l>
							<l n="28" num="2.11">Non, ce n’est pas la nuit, non, ce n’est pas le jour :</l>
							<l n="29" num="2.12">C’est une douce fée, et je la nomme : Amour !</l>
							<l n="30" num="2.13">C’est l’heure où l’âme en vain détrompée et flétrie</l>
							<l n="31" num="2.14">Rappelle en gémissant l’âme qu’elle a chérie.</l>
							<l n="32" num="2.15">Oh ! qui n’a souhaité redevenir enfant !</l>
							<l n="33" num="2.16">Dans le fond de mon cœur que je le suis souvent !</l>
							<l n="34" num="2.17">Mais comme un jeune oiseau, né sous un beau feuillage,</l>
							<l n="35" num="2.18">Fraîchement balancé dans l’arbre paternel,</l>
							<l n="36" num="2.19">Supposait à sa vie un printemps éternel,</l>
							<l n="37" num="2.20">Et qui voit accourir l’hiver dans un orage,</l>
							<l n="38" num="2.21">J’ai vu tomber la feuille, au vert pur et joyeux,</l>
							<l n="39" num="2.22">Dont le frémissement plaisait à mon oreille ;</l>
							<l n="40" num="2.23">Du même arbre aujourd’hui la fleur n’est plus pareille.</l>
							<l n="41" num="2.24">Le temps, déjà le temps a-t-il touché mes yeux ?</l>
						</lg>
						<lg n="3">
							<l n="42" num="3.1">Du moins, là-bas, dans l’ombre, où par lui tout arrive</l>
							<l n="43" num="3.2">Si mes pas chancelants tombent avant le soir,</l>
							<l n="44" num="3.3">Il est doux en fuyant de regarder la rive</l>
							<l n="45" num="3.4">Où naguères l’on vint jouer avec l’espoir.</l>
							<l n="46" num="3.5">Là, de la vague enfance un regret qui sommeille</l>
							<l n="47" num="3.6">Dans les fleurs du passé tout-à-coup se réveille ;</l>
							<l n="48" num="3.7">Il reparaît vivant à nos yeux d’aujourd’hui ;</l>
							<l n="49" num="3.8">On tend les bras, on pleure en passant devant lui !</l>
						</lg>
						<lg n="4">
							<l n="50" num="4.1">Ce tendre abattement vous saisit-il, mon frère,</l>
							<l n="51" num="4.2">Le soir, quand vous passez près du seuil de mon père ?</l>
							<l n="52" num="4.3">Croyez-vous voir mon père assis, calme, rêveur ?</l>
							<l n="53" num="4.4">Dites-vous à quelqu’un : « Elle était là, ma sœur ! »</l>
							<l n="54" num="4.5">Eh bien ! racontez-moi ce qu’on fait dans nos plaines ;</l>
							<l n="55" num="4.6">Peignez-moi vos plaisirs, vos jeux, surtout vos peines.</l>
							<l n="56" num="4.7">Dans l’église isolée… où tu m’as dit adieu,</l>
							<l n="57" num="4.8">Mon frère, donne encore à l’aveugle qui prie :</l>
							<l n="58" num="4.9">Dis que c’est pour ta sœur ; dis, pour ta sœur chérie ;</l>
							<l n="59" num="4.10">Dis que ta sœur est triste, et qu’il en parle à Dieu !</l>
						</lg>
						<lg n="5">
							<l n="60" num="5.1">Et le vieux prisonnier de la haute tourelle</l>
							<l n="61" num="5.2">Respire-t-il encore à travers les barreaux ?</l>
							<l n="62" num="5.3">Partage-t-il toujours avec la tourterelle</l>
							<l n="63" num="5.4">Son pain, qu’avaient déjà partagé ses bourreaux ?</l>
							<l n="64" num="5.5">Cette fille de l’air, à la prison vouée,</l>
							<l n="65" num="5.6">Dont l’aile palpitante appelait le captif,</l>
							<l n="66" num="5.7">Était-ce une âme aimante au malheur envoyée ?</l>
							<l n="67" num="5.8">Était-ce l’espérance au vol tendre et furtif ?</l>
							<l n="68" num="5.9">Oui ; si les vents du nord chassaient l’oiseau débile,</l>
							<l n="69" num="5.10">L’œil perçant du captif le cherchait jusqu’au soir :</l>
							<l n="70" num="5.11">De l’espace désert voyageur immobile,</l>
							<l n="71" num="5.12">Il oubliait de vivre ; il attendait l’espoir.</l>
							<l n="72" num="5.13">Car toujours jusqu’au terme où nous devons atteindre,</l>
							<l n="73" num="5.14">Jusqu’au jour qui n’a plus pour nous de lendemain,</l>
							<l n="74" num="5.15">Le flambeau de l’espoir vacille sans s’éteindre,</l>
							<l n="75" num="5.16">Comme un rayon qui part d’une immortelle main.</l>
						</lg>
						<lg n="6">
							<l n="76" num="6.1">Et lui, voit-il encor la froide sentinelle</l>
							<l n="77" num="6.2">Attachée en silence au cercle de ses jours ?</l>
							<l n="78" num="6.3">D’une faute expiée est-ce l’ombre éternelle ?</l>
							<l n="79" num="6.4">Sur ses rêves troublés veille-t-elle toujours ?</l>
							<l n="80" num="6.5">Regarde-t-il encor sous sa demeure sombre</l>
							<l n="81" num="6.6">Les fleurs ?… Libre du moins, toi, tu les cueilleras !</l>
							<l n="82" num="6.7">Oh ! que j’ai vu souvent ses yeux luire dans l’ombre,</l>
							<l n="83" num="6.8">Étonnés qu’un enfant vînt lui tendre les bras !</l>
							<l n="84" num="6.9">Il me montrait ses mains l’une à l’autre enchaînées ;</l>
							<l n="85" num="6.10">Je les voyais trembler, pâles et décharnées.</l>
							<l n="86" num="6.11">Au poids de tant de fer joignait-il un remord ?</l>
							<l n="87" num="6.12">Est-il heureux enfin ? est-il libre ? est-il mort ?</l>
							<l n="88" num="6.13">Que j’ai pleuré sa vie ! Ô Liberté céleste,</l>
							<l n="89" num="6.14">Sans toi, mon jeune cœur étouffait dans mon sein ;</l>
							<l n="90" num="6.15">Je t’implorais au pied de ce donjon funeste.</l>
							<l n="91" num="6.16">Un jour… as-tu, mon frère, oublié ce dessein ?</l>
							<l n="92" num="6.17">De la déesse un jour tu me montras l’image.</l>
							<l n="93" num="6.18">Ô Dieu ! qu’elle était belle ! Arrivais-tu des cieux,</l>
							<l n="94" num="6.19">Liberté, pour ouvrir et pour charmer les yeux ?</l>
							<l n="95" num="6.20">Dans nos temples d’alors on te rendait hommage ;</l>
							<l n="96" num="6.21">Partout l’encens, les fleurs, l’or mûri des moissons,</l>
							<l n="97" num="6.22">Les danses du jeune âge et les jeunes chansons,</l>
							<l n="98" num="6.23">Partout l’étonnement, le doux rire des Grâces,</l>
							<l n="99" num="6.24">Partout la foule émue à genoux sur tes traces !</l>
						</lg>
						<lg n="7">
							<l n="100" num="7.1">Et je voulais courir, pour le vieux prisonnier,</l>
							<l n="101" num="7.2">Te chercher par le monde où l’on t’avait revue ;</l>
							<l n="102" num="7.3">Te demander pourquoi, dans nos champs revenue,</l>
							<l n="103" num="7.4">À bénir ton retour il était le dernier.</l>
							<l n="104" num="7.5">Doux crime d’un enfant ! clémence aventureuse !</l>
							<l n="105" num="7.6">Je t’aime, un jour entier tu m’as rendue heureuse !</l>
							<l n="106" num="7.7">Toi dont le cœur naïf y prêta du secours,</l>
							<l n="107" num="7.8">Mon frère, dans mes vœux reconnais-moi toujours.</l>
							<l n="108" num="7.9">Que jamais sur ta vie une grille inflexible</l>
							<l n="109" num="7.10"><space quantity="6" unit="char"></space>N’étende son voile de fer !</l>
							<l n="110" num="7.11">Sois libre ! et que le sort content, s’il est possible,</l>
							<l n="111" num="7.12">N’ajoute plus tes maux à ce que j’ai souffert !</l>
						</lg>
						<lg n="8">
							<l n="112" num="8.1">On m’arrêta fuyante ; et, craintive, à ma mère</l>
							<l n="113" num="8.2">Je fus à jointes mains conduite vers le soir.</l>
							<l n="114" num="8.3">Ô mère ! trop heureuse encor de me revoir,</l>
							<l n="115" num="8.4">Sa tremblante leçon ne me fut point amère ;</l>
							<l n="116" num="8.5">Car, de mon front coupable en détachant les fleurs,</l>
							<l n="117" num="8.6">Pour cacher son sourire elle baisa mes pleurs.</l>
						</lg>
						<lg n="9">
							<l n="118" num="9.1">J’oubliai mon voyage, et jamais ta souffrance,</l>
							<l n="119" num="9.2">Vieux captif ! et jamais ton doux nom, Liberté !</l>
							<l n="120" num="9.3">Et jamais ton pardon de mon cœur regretté,</l>
							<l n="121" num="9.4">Ma mère ! et ton beau rêve envolé, belle France !</l>
							<l n="122" num="9.5">Et la leçon : « Ma fille, où voulez-vous courir ?</l>
							<l n="123" num="9.6">Votre idole n’est pas où vous pensez l’atteindre.</l>
							<l n="124" num="9.7">Un flambeau vous éclaire, et vous alliez l’éteindre :</l>
							<l n="125" num="9.8">Ce flambeau, c’est ma vie, et je n’ai qu’à mourir,</l>
							<l n="126" num="9.9">Si vous m’abandonnez. Pour vous, chère ingénue,</l>
							<l n="127" num="9.10">Livrée à des regrets que vous ne savez pas,</l>
							<l n="128" num="9.11">Sous le toit déserté, faible et traînant vos pas,</l>
							<l n="129" num="9.12"><space quantity="6" unit="char"></space>Trop tard vous seriez revenue.</l>
							<l n="130" num="9.13">Vos yeux à peine ouverts égareront vos jours,</l>
							<l n="131" num="9.14">Enfant, si près de moi vous ne marchez toujours.</l>
						</lg>
						<lg n="10">
							<l n="132" num="10.1">« La Liberté, ma fille, est un ange qui vole.</l>
							<l n="133" num="10.2">Pour l’arrêter longtemps là terre est trop frivole.</l>
							<l n="134" num="10.3">Trop d’encens lui déplaît, trop de cris lui font peur ;</l>
							<l n="135" num="10.4">Elle étouffe en un temple, et sa puissante haleine,</l>
							<l n="136" num="10.5">Qui cherche les parfums et l’air pur de la plaine,</l>
							<l n="137" num="10.6">Rafraîchit en passant le front du laboureur.</l>
							<l n="138" num="10.7">On dit qu’elle descend rapide, inattendue ;</l>
							<l n="139" num="10.8">Que son aile sur nous repose détendue…</l>
							<l n="140" num="10.9">Hélas ! où donc est-elle ? En vain j’ouvre les yeux ;</l>
							<l n="141" num="10.10">En vain, dit-on : « Voyez !» Je ne la vois qu’aux cieux.</l>
							<l n="142" num="10.11">Loin, bien loin des palais, au toit du pauvre même,</l>
							<l n="143" num="10.12">Où l’on travaille en paix, où l’on prie, où l’on aime,</l>
							<l n="144" num="10.13">Où l’indigence obtient une obole et des pleurs,</l>
							<l n="145" num="10.14">La déesse en silence aime à jeter ses fleurs.</l>
							<l n="146" num="10.15">Les fleurs tombent sans bruit, et de peur de l’envie,</l>
							<l n="147" num="10.16">On les effeuille à Dieu, qui dit : « Cache ta vie. »</l>
							<l n="148" num="10.17">Ainsi priez, ma fille, et marchez près de moi :</l>
							<l n="149" num="10.18">Un jour tout sera libre, et Dieu seul sera roi. »</l>
						</lg>
					</div></body></text></TEI>