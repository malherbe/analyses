<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>9618 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>9618 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies</title>
					<author>Marceline Desbordes-Valmore</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>A. Boulland, libraire-éditeur</publisher>
						<date when="1830">1830</date>
					</imprint>
					<extent>9618 verstome second</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-12-20" who="RR">Insertion des corrections métriques dans le corps du texte.</change>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
				<change when="2018-11-02" who="RR">Correction dans le poème "Dors, ma Mère".</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part"> POÉSIES INÉDITES <lb></lb>MÉLANGES</head><div type="poem" key="DES177">
						<head type="main">LE PAPILLON MALADE</head>
						<head type="sub_1">APOLOGUE</head>
						<lg n="1">
							<l n="1" num="1.1">Las des fleurs, épuisé de ses longues amours,</l>
							<l n="2" num="1.2"><space quantity="6" unit="char"></space>Un papillon, dans sa vieillesse,</l>
							<l n="3" num="1.3">(Il avait du printemps goûté les plus beaux jours),</l>
							<l n="4" num="1.4">Voyait d’un œil chagrin la tendre hardiesse</l>
							<l n="5" num="1.5">Des amants nouveau-nés, dont le rapide essor</l>
							<l n="6" num="1.6">Effleurait les boutons qu’humectait la rosée.</l>
						</lg>
						<lg n="2">
							<l n="7" num="2.1">Soulevant un matin le débile ressort</l>
							<l n="8" num="2.2"><space quantity="6" unit="char"></space>De son aile à demi-brisée :</l>
							<l n="9" num="2.3">« Tout a changé, dit-il, tout se fane. Autrefois</l>
							<l n="10" num="2.4">L’univers n’avait point cet aspect qui m’afflige.</l>
							<l n="11" num="2.5"><space quantity="6" unit="char"></space>Oui, la nature se néglige ;</l>
							<l n="12" num="2.6">Aussi pour la chanter l’oiseau n’a plus de voix.</l>
							<l n="13" num="2.7">Les papillons passés avaient bien plus de charmes !</l>
							<l n="14" num="2.8">Toutes les fleurs tombaient sous nos brûlantes armes</l>
							<l n="15" num="2.9">Touchés par le soleil, nos légers vêtements</l>
							<l n="16" num="2.10"><space quantity="6" unit="char"></space>Semblaient brodés de diamants !</l>
							<l n="17" num="2.11"><space quantity="6" unit="char"></space>Je ne vois plus rien sur la terre</l>
							<l n="18" num="2.12"><space quantity="6" unit="char"></space>Qui ressemble à mon beau matin !</l>
							<l n="19" num="2.13">J’ai froid. Tout, jusqu’aux fleurs, prend une teinte austère,</l>
							<l n="20" num="2.14">Et je n’ai plus de goût aux restes du festin !</l>
							<l n="21" num="2.15">Ce gazon si charmant, ce duvet des prairies,</l>
							<l n="22" num="2.16">Où mon vol fatigué descendait vers le soir,</l>
							<l n="23" num="2.17">Où Chloé, qui n’est plus, vint chanter et s’asseoir,</l>
							<l n="24" num="2.18">N’offre plus qu’un vert pâle et des couleurs flétries !</l>
							<l n="25" num="2.19">L’air me soutient à peine à travers les brouillards</l>
							<l n="26" num="2.20">Qui voilent le soleil de mes longues journées ;</l>
							<l n="27" num="2.21">Mes heures, sans amour, se changent en années :</l>
							<l n="28" num="2.22"><space quantity="6" unit="char"></space>Hélas ! que je plains les vieillards !</l>
						</lg>
						<lg n="3">
							<l n="29" num="3.1">Je voudrais, cependant, que mon expérience</l>
							<l n="30" num="3.2"><space quantity="6" unit="char"></space>Servît à tous ces fils de l’air.</l>
							<l n="31" num="3.3">Sous des bouquets flétris j’ai puisé ma science,</l>
							<l n="32" num="3.4">J’ai défini la vie, enfants : c’est un éclair !</l>
							<l n="33" num="3.5">Frêles triomphateurs ! vos ailes intrépides</l>
							<l n="34" num="3.6">S’arrêteront un jour avec étonnement :</l>
							<l n="35" num="3.7">Plus de larcins alors, plus de baisers avides ;</l>
							<l n="36" num="3.8">Les roses subiront un affreux changement.</l>
						</lg>
						<lg n="4">
							<l n="37" num="4.1">Je croyais comme vous qu’une flamme immortelle</l>
							<l n="38" num="4.2">Coulait dans les parfums créés pour me nourrir,</l>
							<l n="39" num="4.3"><space quantity="6" unit="char"></space>Qu’une fleur était toujours belle,</l>
							<l n="40" num="4.4"><space quantity="6" unit="char"></space>Et que rien ne devait mourir.</l>
							<l n="41" num="4.5">Mais le temps m’a parlé ; sa sévère éloquence</l>
							<l n="42" num="4.6">A détendu mon vol et glacé mes penchants :</l>
							<l n="43" num="4.7">Le coteau me fatigue et je me traîne aux champs ;</l>
							<l n="44" num="4.8">Enfin, je vois la mort où votre inconséquence</l>
							<l n="45" num="4.9">Poursuit la volupté. Je n’ai plus de désir,</l>
							<l n="46" num="4.10">Car on dit que l’amour est un bonheur coupable :</l>
							<l n="47" num="4.11">Hélas ! d’y succomber je ne suis plus capable,</l>
							<l n="48" num="4.12">Et je suis tout honteux d’avoir eu du plaisir. »</l>
						</lg>
						<lg n="5">
							<l n="49" num="5.1"><space quantity="6" unit="char"></space>Près du sybarite, invalide,</l>
							<l n="50" num="5.2">Un papillon naissait dans toute sa beauté :</l>
							<l n="51" num="5.3">Cette plainte l’étonne ; il rêve, il est tenté</l>
							<l n="52" num="5.4"><space quantity="6" unit="char"></space>De rentrer dans sa chrysalide.</l>
							<l n="53" num="5.5">« Quoi ! dit-il, ce ciel pur, ce soleil généreux,</l>
							<l n="54" num="5.6"><space quantity="2" unit="char"></space>Qui me transforme et qui me fait éclore,</l>
							<l n="55" num="5.7">Mon berceau transparent qu’il chauffe et qu’il colore.</l>
							<l n="56" num="5.8">Tous ces biens me rendront coupable et malheureux !</l>
							<l n="57" num="5.9">Mais un instinct si doux m’attire dans la vie !</l>
							<l n="58" num="5.10">Un souffle si puissant m’appelle autour des fleurs !</l>
							<l n="59" num="5.11">Là-bas, ces coteaux verts, ces brillantes couleurs</l>
							<l n="60" num="5.12">Font naître tant d’espoir, tant d’amour, tant d’envie !</l>
							<l n="61" num="5.13">Oh ! tais-toi, pauvre sage, ou pauvre ingrat, tais-toi !</l>
							<l n="62" num="5.14">Tu nous défends les fleurs encor penché sur elles.</l>
							<l n="63" num="5.15">Dors, si tu n’aimes plus ; mais les cieux sont à moi :</l>
							<l n="64" num="5.16">J’éclos pour m’envoler, et je risque mes ailes ! »</l>
						</lg>
					</div></body></text></TEI>