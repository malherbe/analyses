<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>9618 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>9618 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies</title>
					<author>Marceline Desbordes-Valmore</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>A. Boulland, libraire-éditeur</publisher>
						<date when="1830">1830</date>
					</imprint>
					<extent>9618 verstome second</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-12-20" who="RR">Insertion des corrections métriques dans le corps du texte.</change>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
				<change when="2018-11-02" who="RR">Correction dans le poème "Dors, ma Mère".</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ÉLÉGIES</head><div type="poem" key="DES45">
						<head type="main">ÉLÉGIE</head>
						<lg n="1">
							<l n="1" num="1.1">J’étais à toi peut-être avant de t’avoir vu.</l>
							<l n="2" num="1.2">Ma vie, en se formant, fut promise à la tienne ;</l>
							<l n="3" num="1.3">Ton nom m’en avertit par un trouble imprévu ;</l>
							<l n="4" num="1.4">Ton âme s’y cachait pour éveiller la mienne.</l>
							<l n="5" num="1.5">Je l’entendis un jour et je perdis la voix ;</l>
							<l n="6" num="1.6">Je l’écoutai longtemps, j’oubliai de répondre.</l>
							<l n="7" num="1.7">Mon être avec le tien venait de se confondre ;</l>
							<l n="8" num="1.8">Je crus qu’on m’appelait pour la première fois.</l>
							<l n="9" num="1.9">Savais-tu ce prodige ? Eh bien, sans te connaître,</l>
							<l n="10" num="1.10">J’ai deviné par lui mon amant et mon maître,</l>
							<l n="11" num="1.11">Et je le reconnus dans tes premiers accents,</l>
							<l n="12" num="1.12">Quand tu vins éclairer mes beaux jours languissants.</l>
							<l n="13" num="1.13">Ta voix me fit pâlir, et mes yeux se baissèrent.</l>
							<l n="14" num="1.14">Dans un regard muet nos âmes s’embrassèrent ;</l>
							<l n="15" num="1.15">Au fond de ce regard ton nom se révéla,</l>
							<l n="16" num="1.16">Et sans le demander j’avais dit : Le voilà !</l>
							<l n="17" num="1.17">Dès lors il ressaisit mon oreille étonnée ;</l>
							<l n="18" num="1.18">Elle y devint soumise, elle y fut enchaînée.</l>
							<l n="19" num="1.19">J’exprimais par lui seul mes plus doux sentiments ;</l>
							<l n="20" num="1.20">Je l’unissais au mien pour signer mes serments.</l>
							<l n="21" num="1.21">Je le lisais partout, ce nom rempli de charmes,</l>
							<l n="22" num="1.22"><space quantity="10" unit="char"></space>Et je versais des larmes.</l>
							<l n="23" num="1.23">D’un éloge enchanteur toujours environné,</l>
							<l n="24" num="1.24">À mes yeux éblouis il s’offrait couronné.</l>
							<l n="25" num="1.25">Je l’écrivais… bientôt je n’osai plus l’écrire,</l>
							<l n="26" num="1.26">Et mon timide amour le changeait en sourire.</l>
							<l n="27" num="1.27">Il me cherchait la nuit, il berçait mon sommeil,</l>
							<l n="28" num="1.28">Il résonnait encore autour de mon réveil ;</l>
							<l n="29" num="1.29">Il errait dans mon souffle, et, lorsque je soupire,</l>
							<l n="30" num="1.30">C’est lui qui me caresse et que mon cœur respire.</l>
							<l n="31" num="1.31">Nom chéri ! nom charmant ! oracle de mon sort !</l>
							<l n="32" num="1.32">Hélas ! que tu me plais, que ta grâce me touche !</l>
							<l n="33" num="1.33">Tu m’annonças la vie, et, mêlé dans la mort,</l>
							<l n="34" num="1.34">Comme un dernier baiser tu fermeras ma bouche.</l>
						</lg>
					</div></body></text></TEI>