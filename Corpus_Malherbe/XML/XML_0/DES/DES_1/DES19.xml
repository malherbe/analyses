<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>9618 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>9618 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies</title>
					<author>Marceline Desbordes-Valmore</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>A. Boulland, libraire-éditeur</publisher>
						<date when="1830">1830</date>
					</imprint>
					<extent>9618 verstome second</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-12-20" who="RR">Insertion des corrections métriques dans le corps du texte.</change>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
				<change when="2018-11-02" who="RR">Correction dans le poème "Dors, ma Mère".</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ÉLÉGIES</head><div type="poem" key="DES19">
						<head type="main">LE CONCERT</head>
						<lg n="1">
							<l n="1" num="1.1"><space quantity="2" unit="char"></space>Quelle soirée ! ô Dieu ! que j’ai souffert !</l>
							<l n="2" num="1.2">Dans un trouble charmant je suivais l’Espérance ;</l>
							<l n="3" num="1.3">Elle enchantait pour moi les apprêts du concert,</l>
							<l n="4" num="1.4"><space quantity="2" unit="char"></space>Et je devais y pleurer ton absence.</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1">Dans la foule cent fois j’ai cru t’apercevoir ;</l>
							<l n="6" num="2.2">Mes vœux toujours trahis n’embrassaient que ton ombre ;</l>
							<l n="7" num="2.3">L’Amour me la laissait tout à coup entrevoir.</l>
							<l n="8" num="2.4">Pour l’entraîner bientôt vers le lieu le plus sombre.</l>
							<l n="9" num="2.5">Séduite par mon cœur toujours plus agité,</l>
							<l n="10" num="2.6">Je voyais dans le vague errer ta douce image,</l>
							<l n="11" num="2.7">Comme un astre chéri, qu’enveloppe un nuage,</l>
							<l n="12" num="2.8">Par des rayons douteux perce l’obscurité.</l>
						</lg>
						<lg n="3">
							<l n="13" num="3.1">Pour la première fois insensible à tes charmes,</l>
							<l n="14" num="3.2">Art d’Orphée, art du cœur, j’ai méconnu ta loi ;</l>
							<l n="15" num="3.3">J’étais toute à l’Amour, lui seul régnait sur moi,</l>
							<l n="16" num="3.4"><space quantity="2" unit="char"></space>Et le cruel faisait couler mes larmes !</l>
							<l n="17" num="3.5"><space quantity="2" unit="char"></space>D’un chant divin goûte-t-on la douceur</l>
							<l n="18" num="3.6">Lorsqu’on attend la voix de celui que l’on aime ?</l>
							<l n="19" num="3.7"><space quantity="6" unit="char"></space>Je craignais ton charme suprême,</l>
							<l n="20" num="3.8"><space quantity="6" unit="char"></space>Il nourrissait trop ma langueur,</l>
							<l n="21" num="3.9"><space quantity="6" unit="char"></space>Les sons d’une harpe plaintive,</l>
							<l n="22" num="3.10">En frappant sur mon sein, le faisaient tressaillir :</l>
							<l n="23" num="3.11"><space quantity="2" unit="char"></space>Ils fatiguaient mon oreille attentive,</l>
							<l n="24" num="3.12"><space quantity="6" unit="char"></space>Et je me sentais défaillir.</l>
						</lg>
						<lg n="4">
							<l n="25" num="4.1">Et toi ! que faisais-tu, mon idole chérie,</l>
							<l n="26" num="4.2"><space quantity="2" unit="char"></space>Quand ton absence éternisait le jour ?</l>
							<l n="27" num="4.3"><space quantity="2" unit="char"></space>Quand je donnais tout mon être à l’amour,</l>
							<l n="28" num="4.4"><space quantity="6" unit="char"></space>M’as-tu donné ta rêverie ?</l>
							<l n="29" num="4.5"><space quantity="2" unit="char"></space>As-tu gémi de la longueur du temps ?</l>
							<l n="30" num="4.6"><space quantity="2" unit="char"></space>D’un soir… d’un siècle écoulé pour attendre ?</l>
							<l n="31" num="4.7">Non ! son poids douloureux accable le plus tendre ;</l>
							<l n="32" num="4.8">Seule, j’en ai compté les heures, les instants :</l>
							<l n="33" num="4.9">J’ai langui sans bonheur, de moi-même arrachée ;</l>
							<l n="34" num="4.10"><space quantity="6" unit="char"></space>Et toi, tu ne m’as point cherchée !</l>
						</lg>
						<lg n="5">
							<l n="35" num="5.1">Mais quoi ! l’impatience a soulevé mon sein,</l>
							<l n="36" num="5.2">Et, lasse de rougir de ma tendre infortune,</l>
							<l n="37" num="5.3"><space quantity="2" unit="char"></space>Je me dérobe à ce bruyant essaim</l>
							<l n="38" num="5.4">Des papillons du soir, dont l’hommage importune.</l>
							<l n="39" num="5.5">L’heure, aujourd’hui si lente à s’écouler pour moi,</l>
							<l n="40" num="5.6">Ne marche pas encore avec plus de vitesse ;</l>
							<l n="41" num="5.7">Mais je suis seule au moins, seule avec ma tristesse,</l>
							<l n="42" num="5.8">Et je trace, en rêvant, cette lettre pour toi,</l>
							<l n="43" num="5.9">Pour toi, que j’espérais, que j’accuse, que j’aime !</l>
							<l n="44" num="5.10">Pour toi, mon seul désir, mon tourment, mon bonheur !</l>
							<l n="45" num="5.11"><space quantity="2" unit="char"></space>Mais je ne veux la livrer qu’à toi-même,</l>
							<l n="46" num="5.12"><space quantity="6" unit="char"></space>Et tu la liras sur mon cœur.</l>
						</lg>
					</div></body></text></TEI>