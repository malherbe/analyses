<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>9618 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>9618 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies</title>
					<author>Marceline Desbordes-Valmore</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>A. Boulland, libraire-éditeur</publisher>
						<date when="1830">1830</date>
					</imprint>
					<extent>9618 verstome second</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-12-20" who="RR">Insertion des corrections métriques dans le corps du texte.</change>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
				<change when="2018-11-02" who="RR">Correction dans le poème "Dors, ma Mère".</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ÉLÉGIES</head><div type="poem" key="DES68">
						<head type="main">LE RÊVE DE MON ENFANT</head>
						<head type="sub_1">À MADAME PAULINE DUCHAMBGE</head>
						<lg n="1">
							<l n="1" num="1.1">« Mère ! petite mère ! » Il m’appelait ainsi ;</l>
							<l n="2" num="1.2">Et moi, je tressaillais à cet accent si tendre ;</l>
							<l n="3" num="1.3">Tout mon être agité s’éveillait pour l’entendre ;</l>
							<l n="4" num="1.4">Je ne l’entendrai plus : il ne dort plus ici.</l>
							<l n="5" num="1.5">Où retentit sa voix, qui calmait ma souffrance,</l>
							<l n="6" num="1.6"><space quantity="6" unit="char"></space>Comme la voix de l’espérance,</l>
							<l n="7" num="1.7">Formée (on l’aurait dit) de rosée et de miel ?</l>
							<l n="8" num="1.8">Le ciel en fut jaloux, elle doit être au ciel.</l>
							<l n="9" num="1.9">Non ! elle est dans mon cœur : je l’y tiens enfermée ;</l>
							<l n="10" num="1.10">Elle soupire encore, elle parle avec moi.</l>
							<l n="11" num="1.11">Durant mes longues nuits, cette voix tant aimée</l>
							<l n="12" num="1.12">Me dit : « Ne pleure plus ! je ne dors pas pour toi. »</l>
							<l n="13" num="1.13">Oh ! moitié de ma vie, à ma vie arrachée !</l>
							<l n="14" num="1.14">Viens ! redis-moi ton rêve ; il m’a prédit ton sort.</l>
							<l n="15" num="1.15">Que ta plainte, une fois de mon cœur épanchée,</l>
							<l n="16" num="1.16">Rappelle un jeune cygne et son doux chant de mort !</l>
							<l n="17" num="1.17">« Écoute, m’as-tu dit, écoute mon beau songe ! »</l>
							<l n="18" num="1.18">Le premier… le dernier qui berça ton sommeil !</l>
							<l n="19" num="1.19">De ce récit confus, prophétique mensonge,</l>
							<l n="20" num="1.20">Cher innocent, tu vins saluer mon réveil.</l>
							<l n="21" num="1.21">« Écoute ! je dormais ; j’avais dit ma prière.</l>
							<l n="22" num="1.22">J’ai vu venir vers moi deux anges : qu’ils sont beaux !</l>
							<l n="23" num="1.23">Je voudrais être un ange. Ils portent des flambeaux</l>
							<l n="24" num="1.24">Que le vent n’éteint pas. L’un d’eux a dit : « Mon frère,</l>
							<l n="25" num="1.25">Nous venons te chercher ; veux-tu nous suivre ? — Oh ! oui,</l>
							<l n="26" num="1.26">Je veux vous suivre… On chante ; est-ce fête aujourd’hui ?</l>
							<l n="27" num="1.27">— C’est fête. Viens chercher des parures nouvelles. »</l>
							<l n="28" num="1.28">Et mes bras s’étendaient pour imiter leurs ailes ;</l>
							<l n="29" num="1.29">Je m’envolais comme eux, je riais… j’avais peur !</l>
							<l n="30" num="1.30">Dieu parlait ! Dieu pour moi montrait une couronne :</l>
							<l n="31" num="1.31">C’est aux enfants chéris que sa bonté la donne,</l>
							<l n="32" num="1.32">Et Dieu me l’a promise, et Dieu n’est pas trompeur.</l>
							<l n="33" num="1.33">J’irai bientôt le voir ; j’irai bientôt… — Ma vie !</l>
							<l n="34" num="1.34">Où donc étais-je alors ?… — Attends… je ne sais pas…</l>
							<l n="35" num="1.35">Tu pleurais sur la terre, où je t’avais suivie.</l>
							<l n="36" num="1.36">— Tu me laissais pleurer ? —Je t’appelais tout bas.</l>
							<l n="37" num="1.37">— Tu voulais me revoir ? — Je ne pouvais, ma mère,</l>
							<l n="38" num="1.38">Dieu ne t’appelait pas. » Un froid saisissement</l>
							<l n="39" num="1.39">Passa jusqu’à mon cœur, et cet être charmant,</l>
							<l n="40" num="1.40">Calme, rêvait encor sa céleste chimère.</l>
							<l n="41" num="1.41">Dès lors un mal secret répandit sa pâleur</l>
							<l n="42" num="1.42">Sur ce front incliné, qui brûlait sous mes larmes.</l>
							<l n="43" num="1.43">Je voyais se détruire avant moi tant de charmes,</l>
							<l n="44" num="1.44">Comme un frêle bouton s’effeuille avant la fleur.</l>
							<l n="45" num="1.45">Je le voyais ! et moi, rebelle… suppliante,</l>
							<l n="46" num="1.46">Je disputais un ange à l’immortel séjour.</l>
							<l n="47" num="1.47">Après soixante jours de deuil et d’épouvante,</l>
							<l n="48" num="1.48">Je criais vers le ciel : « Encore, encore un jour ! »</l>
							<l n="49" num="1.49">Vainement. J’épuisai mon âme tout entière ;</l>
							<l n="50" num="1.50">À ce berceau plaintif j’enchaînai mes douleurs ;</l>
							<l n="51" num="1.51">Repoussant le sommeil et m’abreuvant de pleurs,</l>
							<l n="52" num="1.52">Je criais à la mort : « Frappe-moi la première ! »</l>
							<l n="53" num="1.53">Vainement. Et la mort, froide dans son courroux,</l>
							<l n="54" num="1.54">Irritée à l’espoir qu’elle accourait éteindre</l>
							<l n="55" num="1.55">Et moissonnant l’enfant, ne daigna pas atteindre</l>
							<l n="56" num="1.56"><space quantity="6" unit="char"></space>La mère expirante à genoux.</l>
							<l n="57" num="1.57">Et quand je reparus morne et découronnée,</l>
							<l n="58" num="1.58">Après avoir longtemps craint jusqu’à l’amitié,</l>
							<l n="59" num="1.59">Cette troupe légère, un moment consternée,</l>
							<l n="60" num="1.60">Suspendit ses plaisirs, et sentit la pitié.</l>
							<l n="61" num="1.61">« D’où viens-tu ? m’a-t-on dit, et quels nuages sombres</l>
							<l n="62" num="1.62"><space quantity="10" unit="char"></space>Ont environné d’ombres</l>
							<l n="63" num="1.63"><space quantity="10" unit="char"></space>Tes yeux noyés de pleurs ?</l>
							<l n="64" num="1.64"><space quantity="10" unit="char"></space>Ton soir est loin encore,</l>
							<l n="65" num="1.65"><space quantity="10" unit="char"></space>Et ta paisible aurore</l>
							<l n="66" num="1.66"><space quantity="10" unit="char"></space>T’avait promis des fleurs. »</l>
							<l n="67" num="1.67">Oui, la rose a brillé sur mon riant voyage ;</l>
							<l n="68" num="1.68">Tous les yeux l’admiraient dans son jeune feuillage ;</l>
							<l n="69" num="1.69">L’étoile du matin l’aidait à s’entr’ouvrir,</l>
							<l n="70" num="1.70">Et l’étoile du soir la regardait mourir.</l>
							<l n="71" num="1.71">Vers la terre déjà sa tête était penchée ;</l>
							<l n="72" num="1.72">L’insecte inaperçu s’y creusait un tombeau ;</l>
							<l n="73" num="1.73">Sa feuille murmurait, en tombant desséchée :</l>
							<l n="74" num="1.74">« Déjà la nuit ! déjà… Le jour était si beau ! »</l>
						</lg>
					</div></body></text></TEI>