<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>9618 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>9618 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies</title>
					<author>Marceline Desbordes-Valmore</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>A. Boulland, libraire-éditeur</publisher>
						<date when="1830">1830</date>
					</imprint>
					<extent>9618 verstome second</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-12-20" who="RR">Insertion des corrections métriques dans le corps du texte.</change>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
				<change when="2018-11-02" who="RR">Correction dans le poème "Dors, ma Mère".</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part"> POÉSIES INÉDITES <lb></lb>MÉLANGES</head><div type="poem" key="DES166">
						<head type="main">ÉLÉGIE</head>
						<lg n="1">
							<l n="1" num="1.1">Toi que l’on plaint, toi que j’envie,</l>
							<l n="2" num="1.2">Indigente de nos hameaux,</l>
							<l n="3" num="1.3">Toi dont ce chêne aux vieux rameaux</l>
							<l n="4" num="1.4">N’a pas vu commencer la vie ;</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1">Toi qui n’attends plus des mortels</l>
							<l n="6" num="2.2">Ni ton bonheur, ni ta souffrance ;</l>
							<l n="7" num="2.3">Toi dont la dernière espérance</l>
							<l n="8" num="2.4">S’incline aux rustiques autels ;</l>
						</lg>
						<lg n="3">
							<l n="9" num="3.1">Toi que dans le fond des chaumières</l>
							<l n="10" num="3.2">On appelle, avant de mourir,</l>
							<l n="11" num="3.3">Pour aider une âme à souffrir</l>
							<l n="12" num="3.4">Par ton exemple et tes prières ;</l>
						</lg>
						<lg n="4">
							<l n="13" num="4.1">Oh ! donne-moi tes cheveux blancs,</l>
							<l n="14" num="4.2">Ta marche pesante et courbée,</l>
							<l n="15" num="4.3">Ta mémoire enfin absorbée,</l>
							<l n="16" num="4.4">Tes vieux jours, tes pas chancelants,</l>
							<l n="17" num="4.5">Tes yeux sans lumière, sans larmes,</l>
							<l n="18" num="4.6">Assoupis sous les doigts du temps,</l>
							<l n="19" num="4.7">Miroirs ternis pour tous les charmes</l>
							<l n="20" num="4.8">Et pour tous les feux du printemps !</l>
							<l n="21" num="4.9">Ce souffle qui t’anime à peine,</l>
							<l n="22" num="4.10">Ce reste incertain de chaleur,</l>
							<l n="23" num="4.11">Et qui s’éteint de veine en veine,</l>
							<l n="24" num="4.12">Comme il est éteint dans ton cœur.</l>
						</lg>
						<lg n="5">
							<l n="25" num="5.1">Prends ma jeunesse et ses orages,</l>
							<l n="26" num="5.2">Mes cheveux libres et flottants ;</l>
							<l n="27" num="5.3">Prends mes vœux que l’on croit contents ;</l>
							<l n="28" num="5.4">Prends ces doux et trompeurs suffrages</l>
							<l n="29" num="5.5">Que ne goûtent plus mes douleurs,</l>
							<l n="30" num="5.6">Ce triste éclat qui m’environne,</l>
							<l n="31" num="5.7">Et cette fragile couronne</l>
							<l n="32" num="5.8">Qu’on attache en vain sur mes pleurs !</l>
						</lg>
						<lg n="6">
							<l n="33" num="6.1">Changeons d’âme et de destinée ;</l>
							<l n="34" num="6.2">Prends, pour ton avenir d’un jour,</l>
							<l n="35" num="6.3">Ma jeune saison condamnée</l>
							<l n="36" num="6.4">Au désespoir d’un long amour !</l>
						</lg>
						<lg n="7">
							<l n="37" num="7.1">Ah ! si cet échange est possible,</l>
							<l n="38" num="7.2">Que toi seule, à mes vœux sensible,</l>
							<l n="39" num="7.3">Au Temps me présente pour toi ;</l>
							<l n="40" num="7.4">Qu’il éteigne alors sous son aile</l>
							<l n="41" num="7.5">Une image ardente et cruelle</l>
							<l n="42" num="7.6">Qui brûle et s’attache sur moi !</l>
						</lg>
						<lg n="8">
							<l n="43" num="8.1">Que ces flots, ces molles verdures,</l>
							<l n="44" num="8.2">Ces frais bruissements des bois</l>
							<l n="45" num="8.3">N’imitent plus, dans leur murmure,</l>
							<l n="46" num="8.4">Les accents d’une seule voix !</l>
							<l n="47" num="8.5">Que pour moi, comme à ton oreille</l>
							<l n="48" num="8.6">Que rien n’émeut, que rien n’éveille,</l>
							<l n="49" num="8.7">Le souvenir n’ait point d’échos,</l>
							<l n="50" num="8.8">L’ombre du soir point de féerie ;</l>
							<l n="51" num="8.9">Que les ruisseaux de la prairie</l>
							<l n="52" num="8.10">Ne me soient plus que des ruisseaux !</l>
						</lg>
						<lg n="9">
							<l n="53" num="9.1">Que, semblable à la chrysalide,</l>
							<l n="54" num="9.2">Qui sous sa froide et sombre égide</l>
							<l n="55" num="9.3">Couve son destin radieux,</l>
							<l n="56" num="9.4">Demain, sur des ailes de flamme,</l>
							<l n="57" num="9.5">Comme l’insecte qui peint l’âme,</l>
							<l n="58" num="9.6">J’étende mon vol vers les cieux !…</l>
						</lg>
						<lg n="10">
							<l n="59" num="10.1">Mais tu regagnes sans m’entendre</l>
							<l n="60" num="10.2">Le sentier qui mène au vallon ;</l>
							<l n="61" num="10.3">Insensible aux cris d’un cœur tendre,</l>
							<l n="62" num="10.4">Comme aux soupirs de l’Aquilon,</l>
							<l n="63" num="10.5">Tu n’écoutes plus de la terre</l>
							<l n="64" num="10.6">Le bruit, les plaintes, ni les chants ;</l>
							<l n="65" num="10.7">Et, sur ton chemin solitaire,</l>
							<l n="66" num="10.8">Inutile même aux méchants</l>
							<l n="67" num="10.9">Qui me suivent d’un pas agile,</l>
							<l n="68" num="10.10">Toi, dans ces incultes séjours,</l>
							<l n="69" num="10.11">Tu dérobes ton pied d’argile</l>
							<l n="70" num="10.12">Aux pièges où tombent mes jours !</l>
						</lg>
						<lg n="11">
							<l n="71" num="11.1">Suis ta route, vieille bergère ;</l>
							<l n="72" num="11.2">En glanant l’aride fougère,</l>
							<l n="73" num="11.3">Debout encor sous ton fardeau,</l>
							<l n="74" num="11.4">Sans craindre une voix importune,</l>
							<l n="75" num="11.5">Bientôt ta paisible infortune</l>
							<l n="76" num="11.6">Cheminera sur mon tombeau.</l>
						</lg>
					</div></body></text></TEI>