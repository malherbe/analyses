<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>9618 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>9618 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies</title>
					<author>Marceline Desbordes-Valmore</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>A. Boulland, libraire-éditeur</publisher>
						<date when="1830">1830</date>
					</imprint>
					<extent>9618 verstome second</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-12-20" who="RR">Insertion des corrections métriques dans le corps du texte.</change>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
				<change when="2018-11-02" who="RR">Correction dans le poème "Dors, ma Mère".</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">POÉSIES DIVERSES</head><div type="poem" key="DES141">
						<head type="main">CONTE D’ENFANT</head>
						<lg n="1">
							<l n="1" num="1.1">Il ne faut plus courir à travers les bruyères,</l>
							<l n="2" num="1.2">Enfant, ni sans congé vous hasarder au loin.</l>
							<l n="3" num="1.3">Vous êtes très petit, et vous avez besoin</l>
							<l n="4" num="1.4">Que l’on vous aide encore à dire vos prières.</l>
							<l n="5" num="1.5">Que feriez-vous aux champs, si vous étiez perdu ?</l>
							<l n="6" num="1.6">Si vous ne trouviez plus le sentier du village ?</l>
							<l n="7" num="1.7">On dirait : « Quoi ! si jeune, il est mort ? c’est dommage ! »</l>
							<l n="8" num="1.8">Vous crieriez… De si loin seriez-vous entendu ?</l>
							<l n="9" num="1.9">Vos petits compagnons, à l’heure accoutumée,</l>
							<l n="10" num="1.10">Danseraient à la porte et chanteraient tout bas ;</l>
							<l n="11" num="1.11">Il faudrait leur répondre, en la tenant fermée :</l>
							<l n="12" num="1.12">« Une mère est malade, enfants, ne chantez pas ! »</l>
							<l n="13" num="1.13">Et vos cris rediraient : « Ô ma mère ! ô ma mère ! »</l>
							<l n="14" num="1.14">L’écho vous répondrait, l’écho vous ferait peur.</l>
							<l n="15" num="1.15">L’herbe humide et la nuit vous transiraient le cœur.</l>
							<l n="16" num="1.16">Vous n’auriez à manger que quelque plante amère ;</l>
							<l n="17" num="1.17">Point de lait, point de lit !… Il faudrait donc mourir ?</l>
							<l n="18" num="1.18">J’en frissonne ! et vraiment ce tableau fait frémir.</l>
							<l n="19" num="1.19">Embrassons-nous, je vais vous conter une histoire ;</l>
							<l n="20" num="1.20">Ma tendresse pour vous éveille ma mémoire.</l>
						</lg>
						<lg n="2">
							<l n="21" num="2.1">« Il était un berger, veillant avec amour</l>
							<l n="22" num="2.2">Sur des agneaux chéris, qui l’aimaient à leur tour.</l>
							<l n="23" num="2.3">Il les désaltérait dans une eau claire et saine,</l>
							<l n="24" num="2.4">Les baignait à la source, et blanchissait leur laine ;</l>
							<l n="25" num="2.5">De serpolet, de thym, parfumait leurs repas ;</l>
							<l n="26" num="2.6">Des plus faibles encor guidait les premiers pas ;</l>
							<l n="27" num="2.7">D’un ruisseau quelquefois permettait l’escalade.</l>
							<l n="28" num="2.8">Si l’un d’eux, au retour, traînait un pied malade,</l>
							<l n="29" num="2.9">Il était dans ses bras tout doucement porté,</l>
							<l n="30" num="2.10">Et, la nuit, sur son lit, dormait à son côté ;</l>
							<l n="31" num="2.11">Réveillés le matin par l’aurore vermeille,</l>
							<l n="32" num="2.12">Il leur jouait des airs à captiver l’oreille ;</l>
							<l n="33" num="2.13">Plus tard, quand ils broutaient leur souper sous ses yeux,</l>
							<l n="34" num="2.14">Aux sons de sa musette il les rendait joyeux.</l>
							<l n="35" num="2.15">Enfin il renfermait sa famille chérie</l>
							<l n="36" num="2.16"><space quantity="10" unit="char"></space>Dedans la bergerie.</l>
							<l n="37" num="2.17">Quand l’ombre sur les champs jetait son manteau noir,</l>
							<l n="38" num="2.18"><space quantity="10" unit="char"></space>Il leur disait : « Bonsoir,</l>
							<l n="39" num="2.19">Chers agneaux ! sans danger reposez tous ensemble ;</l>
							<l n="40" num="2.20">L’un par l’autre pressés, demeurez chaudement ;</l>
							<l n="41" num="2.21">Jusqu’à ce qu’un beau jour se lève et nous rassemble,</l>
							<l n="42" num="2.22">Sous la garde des chiens dormez tranquillement. »</l>
						</lg>
						<lg n="3">
							<l n="43" num="3.1">Les chiens rôdaient alors, et le pasteur sensible</l>
							<l n="44" num="3.2">Les revoyait heureux dans un rêve paisible.</l>
							<l n="45" num="3.3">Eh ! ne l’étaient-ils pas ? Tous bénissaient leur sort,</l>
							<l n="46" num="3.4">Excepté le plus jeune ; hardi, malin, folâtre,</l>
							<l n="47" num="3.5">Des fleurs, du miel, des blés et des bois idolâtre,</l>
							<l n="48" num="3.6">Seul il jugeait tout bas que son maître avait tort.</l>
						</lg>
						<lg n="4">
							<l n="49" num="4.1">Un jour, riant d’avance, et roulant sa chimère,</l>
							<l n="50" num="4.2">Ce petit fou d’agneau s’en vint droit à sa mère.</l>
							<l n="51" num="4.3">Sage et vieille brebis, soumise à son pasteur.</l>
							<l n="52" num="4.4">« Mère ! écoutez, dit-il ; d’où vient qu’on nous enferme ?</l>
							<l n="53" num="4.5">Les chiens ne le sont pas, et j’en prends de l’humeur.</l>
							<l n="54" num="4.6">Cette loi m’est trop dure, et j’y veux mettre un terme.</l>
							<l n="55" num="4.7">Je vais courir partout, j’y suis très résolu.</l>
							<l n="56" num="4.8">Le bois doit être beau pendant le clair de lune.</l>
							<l n="57" num="4.9">Oui, mère, dès ce soir je veux tenter fortune :</l>
							<l n="58" num="4.10">Tant pis pour le pasteur, c’est lui qui l’a voulu.</l>
						</lg>
						<lg n="5">
							<l n="59" num="5.1">— Demeurez, mon agneau, dit la mère attendrie ;</l>
							<l n="60" num="5.2">Vous n’êtes qu’un enfant, bon pour la bergerie ;</l>
							<l n="61" num="5.3">Restez-y près de moi ! Si vous voulez partir,</l>
							<l n="62" num="5.4">Hélas ! j ! ose pour vous prévoir un repentir.</l>
						</lg>
						<lg n="6">
							<l n="63" num="6.1">— J’ose vous dire non, cria le volontaire… »</l>
							<l n="64" num="6.2">Un chien les obligea tous les deux à se taire.</l>
						</lg>
						<lg n="7">
							<l n="65" num="7.1">Quand le soleil couchant au parc les rappela</l>
							<l n="66" num="7.2">Et que par flots joyeux le troupeau s’écoula,</l>
							<l n="67" num="7.3">L’agneau sous une haie établit sa cachette ;</l>
							<l n="68" num="7.4">Il avait finement détaché sa clochette.</l>
							<l n="69" num="7.5">Dès que le parc fut clos, il courut à l’entour.</l>
							<l n="70" num="7.6">Il jouait, gambadait, sautait à perdre haleine.</l>
							<l n="71" num="7.7">« Je voyage, dit-il, je suis libre à mon tour !</l>
							<l n="72" num="7.8">Je ris, je n’ai pas peur ; la lune est claire et pleine :</l>
							<l n="73" num="7.9">Allons au bois, dansons, broutons ! » Mais, par malheur,</l>
							<l n="74" num="7.10">Des loups pour leurs enfants cherchaient alors curée :</l>
							<l n="75" num="7.11">Un peu de laine, hélas ! sanglante et déchirée,</l>
							<l n="76" num="7.12">Fut tout ce que le vent daigna rendre au pasteur.</l>
							<l n="77" num="7.13">Jugez comme il fut triste, à l’aube renaissante !</l>
							<l n="78" num="7.14">Jugez comme on plaignit la mère gémissante !</l>
							<l n="79" num="7.15">« Quoi ! ce soir, cria-t-elle, on nous appellera,</l>
							<l n="80" num="7.16">Et ce soir… et jamais l’agneau ne répondra ! »</l>
							<l n="81" num="7.17">En l’appelant en vain elle affligea l’Aurore ;</l>
							<l n="82" num="7.18">Le soir elle mourut en l’appelant encore.</l>
						</lg>
					</div></body></text></TEI>