<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>9618 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>9618 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies</title>
					<author>Marceline Desbordes-Valmore</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>A. Boulland, libraire-éditeur</publisher>
						<date when="1830">1830</date>
					</imprint>
					<extent>9618 verstome second</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-12-20" who="RR">Insertion des corrections métriques dans le corps du texte.</change>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
				<change when="2018-11-02" who="RR">Correction dans le poème "Dors, ma Mère".</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part"> POÉSIES INÉDITES <lb></lb>MÉLANGES</head><div type="poem" key="DES158">
						<head type="main">ÉLÉGIE</head>
						<lg n="1">
							<l n="1" num="1.1">Un jour, écoute… un jour, j’étais bien malheureuse !</l>
							<l n="2" num="1.2">Je marchais, je traînais une tristesse affreuse,</l>
							<l n="3" num="1.3">À travers la distance, et les monts et les bois,</l>
							<l n="4" num="1.4">Et l’air, qui m’empêchait de ressaisir ta voix,</l>
							<l n="5" num="1.5">Je te reconnaissais. Obstinée à t’attendre,</l>
							<l n="6" num="1.6">Mon âme me disait : « Parle ! il va nous entendre ;</l>
							<l n="7" num="1.7">Parle ! ou, sans toi, vers lui laisse-moi m’échapper.</l>
							<l n="8" num="1.8">De silence et de pleurs pourquoi m’envelopper ?</l>
							<l n="9" num="1.9">Ah ! je veux mes amours ! Le feu cherche la flamme ;</l>
							<l n="10" num="1.10"><space quantity="10" unit="char"></space>L’âme demande l’âme ;</l>
							<l n="11" num="1.11">Et toi, tu veux mourir ! La cendre de l’orgueil</l>
							<l n="12" num="1.12">Se répand sur tes jours et m’éteint dans le deuil.</l>
							<l n="13" num="1.13">De ton timide cœur brûlante prisonnière,</l>
							<l n="14" num="1.14">Je consume ta vie, et j’appelle les cieux :</l>
							<l n="15" num="1.15">Regarde ! Ils sont là-bas, dans ses traits, dans ses yeux.</l>
							<l n="16" num="1.16">Rends-les moi ! Cette grâce, au moins, c’est la première. »</l>
						</lg>
						<lg n="2">
							<l n="17" num="2.1">« — Oh ! taisez-vous, mon âme, il n’y faut plus songer.</l>
							<l n="18" num="2.2">Qu’il ignore à jamais ce délire funeste !</l>
							<l n="19" num="2.3">Dans de folles amours, qui ? moi le replonger ?</l>
							<l n="20" num="2.4">Moi, troubler son bonheur ? C’est celui qui me reste ! »</l>
						</lg>
						<lg n="3">
							<l n="21" num="3.1">Et je ne donnai plus de voix à mes douleurs ;</l>
							<l n="22" num="3.2">De ton séjour heureux je détournai la vue ;</l>
							<l n="23" num="3.3">La prière m’offrit sa douceur imprévue ;</l>
							<l n="24" num="3.4">Je respirai d’attendre, et je fondis en pleurs.</l>
						</lg>
						<lg n="4">
							<l n="25" num="4.1">Dieu m’écouta peut-être : une larme le touche ;</l>
							<l n="26" num="4.2">Il savait bien le nom que retenait ma bouche ;</l>
							<l n="27" num="4.3">Et c’est lui qui permet que, sans nous rencontrer,</l>
							<l n="28" num="4.4">Ton image partout vienne à moi se montrer ;</l>
							<l n="29" num="4.5">Partout !… Tu m’apparais jusque dans ton enfance ;</l>
							<l n="30" num="4.6"><space quantity="2" unit="char"></space>Je te vois rire, à la vie, à tes jeux ;</l>
							<l n="31" num="4.7"><space quantity="2" unit="char"></space>Si quelque objet blesse tes jeunes yeux,</l>
							<l n="32" num="4.8"><space quantity="2" unit="char"></space>Je suis ton guide, et je prends ta défense ;</l>
							<l n="33" num="4.9"><space quantity="2" unit="char"></space>Je m’agenouille au pied de ton berceau ;</l>
							<l n="34" num="4.10"><space quantity="2" unit="char"></space>Adolescent, je te suis dans ta course.</l>
							<l n="35" num="4.11"><space quantity="2" unit="char"></space>Ainsi, le pâtre aime à trouver la source</l>
							<l n="36" num="4.12"><space quantity="2" unit="char"></space>D’où échappa son ami, le ruisseau !</l>
						</lg>
						<lg n="5">
							<l n="37" num="5.1"><space quantity="2" unit="char"></space>Dans les vallons où vivait ma famille,</l>
							<l n="38" num="5.2"><space quantity="2" unit="char"></space>Je sens tes jours couler près de mes jours ;</l>
							<l n="39" num="5.3"><space quantity="2" unit="char"></space>Tu n’y descends que pour une humble fille,</l>
							<l n="40" num="5.4"><space quantity="2" unit="char"></space>Et nos deux noms se répondent toujours !</l>
							<l n="41" num="5.5"><space quantity="2" unit="char"></space>Au vieux calvaire où mouraient mes guirlandes,</l>
							<l n="42" num="5.6"><space quantity="2" unit="char"></space>Nos vœux unis vont se réfugier ;</l>
							<l n="43" num="5.7"><space quantity="2" unit="char"></space>Je t’associe à mes pures offrandes ;</l>
							<l n="44" num="5.8"><space quantity="2" unit="char"></space>Ton bras m’enlace, et je t’entends prier.</l>
						</lg>
						<lg n="6">
							<l n="45" num="6.1"><space quantity="2" unit="char"></space>Parfois l’Amour, d’un flambeau plus austère,</l>
							<l n="46" num="6.2"><space quantity="2" unit="char"></space>De l’avenir dissipe le brouillard.</l>
							<l n="47" num="6.3"><space quantity="2" unit="char"></space>Tu m’es rendu sous les traits d’un vieillard ;</l>
							<l n="48" num="6.4"><space quantity="2" unit="char"></space>Pour l’amour vrai, le temps est sans mystère.</l>
							<l n="49" num="6.5"><space quantity="2" unit="char"></space>Vieillard, je t’aime ! Un charme déchirant</l>
							<l n="50" num="6.6"><space quantity="2" unit="char"></space>Me fait chercher la main qui m’a blessée ;</l>
							<l n="51" num="6.7"><space quantity="2" unit="char"></space>Elle me touche… elle n’est point glacée.</l>
							<l n="52" num="6.8"><space quantity="2" unit="char"></space>Et sur mon sein je la presse en pleurant.</l>
						</lg>
						<lg n="7">
							<l n="53" num="7.1">Qui voudrait m’arracher ces tendres rêveries,</l>
							<l n="54" num="7.2">Où tes regards émus, sur les miens attachés,</l>
							<l n="55" num="7.3">Relisent nos secrets dans mon âme cachés !</l>
							<l n="56" num="7.4">Où ma main dans tes mains brûlantes et chéries</l>
							<l n="57" num="7.5">Tombe, et reste longtemps, comme si le bonheur</l>
							<l n="58" num="7.6">Les unissait encore et remplissait mon cœur !</l>
						</lg>
					</div></body></text></TEI>