<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>9618 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>9618 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies</title>
					<author>Marceline Desbordes-Valmore</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>A. Boulland, libraire-éditeur</publisher>
						<date when="1830">1830</date>
					</imprint>
					<extent>9618 verstome second</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-12-20" who="RR">Insertion des corrections métriques dans le corps du texte.</change>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
				<change when="2018-11-02" who="RR">Correction dans le poème "Dors, ma Mère".</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ÉLÉGIES</head><div type="poem" key="DES62">
						<head type="main">À MADEMOISELLE <lb></lb>GEORGINA NAIRAC</head>
						<lg n="1">
							<l n="1" num="1.1">Ah ! prends garde à l’amour, il menace ta vie ;</l>
							<l n="2" num="1.2">Je l’ai vu dans les pleurs que tu verses pour moi.</l>
							<l n="3" num="1.3">Prends garde, s’il est temps ! il erre autour de toi,</l>
							<l n="4" num="1.4">Et c’est avec des pleurs aussi qu’il m’a suivie.</l>
							<l n="5" num="1.5">Retourne vers ta mère et ne la quitte pas.</l>
							<l n="6" num="1.6">Va, comme un faible oiseau que menace l’orage,</l>
							<l n="7" num="1.7">Contre son sein paisible appuyer ton courage ;</l>
							<l n="8" num="1.8">Portes-y ta jeunesse, enchaînes-y tes pas.</l>
							<l n="9" num="1.9">Plus heureuse que nous, de son printemps calmée,</l>
							<l n="10" num="1.10">Laisse-la te soustraire à de vaines douleurs.</l>
							<l n="11" num="1.11">Va ! tu me béniras de t’avoir alarmée ;</l>
							<l n="12" num="1.12"><space quantity="6" unit="char"></space>Je fus confiante, et je meurs.</l>
						</lg>
						<lg n="2">
							<l n="13" num="2.1">Folle sécurité d’une âme qui s’ignore,</l>
							<l n="14" num="2.2">C’est donc ainsi toujours que vous devez finir ?</l>
							<l n="15" num="2.3">Quand on n’a pas souffert on ne sait rien encore,</l>
							<l n="16" num="2.4">On ne veut confier son cœur qu’à l’avenir.</l>
							<l n="17" num="2.5">Dans l’âge du danger, je n’avais plus de mère :</l>
							<l n="18" num="2.6">Déjà mon tendre guide, arrêté par la mort,</l>
							<l n="19" num="2.7"><space quantity="6" unit="char"></space>N’entendait plus ma plainte amère ;</l>
							<l n="20" num="2.8">Déjà ses yeux fermés n’éclairaient plus mon sort.</l>
						</lg>
						<lg n="3">
							<l n="21" num="3.1">Retourne vers ta mère, et que ton innocence,</l>
							<l n="22" num="3.2">Prudemment effrayée au tableau de mes jours,</l>
							<l n="23" num="3.3">Joigne à mon souvenir, qu’il faut plaindre toujours,</l>
							<l n="24" num="3.4"><space quantity="6" unit="char"></space>Une longue reconnaissance.</l>
						</lg>
						<lg n="4">
							<l n="25" num="4.1">Mais tu n’as pas souffert ? ta tranquille pitié,</l>
							<l n="26" num="4.2">Dis-le moi, n’a donné ses pleurs qu’à l’amitié ?</l>
							<l n="27" num="4.3">Non, tu n’as pas senti cette fièvre de l’âme,</l>
							<l n="28" num="4.4">Ce frisson douloureux qui passe au fond du cœur.</l>
							<l n="29" num="4.5">L’air ne t’a pas semblé comme une molle flamme,</l>
							<l n="30" num="4.6">Qui verse dans les sens la soif et la langueur ?</l>
							<l n="31" num="4.7">Ce triste isolement, ce tendre ennui, ces larmes,</l>
							<l n="32" num="4.8">Ce besoin de presser un cœur semblable au tien,</l>
							<l n="33" num="4.9">D’une voix qui poursuit le fidèle entretien,</l>
							<l n="34" num="4.10">Rien n’a comblé ta vie et de crainte et de charmes ?</l>
							<l n="35" num="4.11">Cet objet souhaité, dans un jour imprévu,</l>
							<l n="36" num="4.12">Ne t’a pas sur son sein réunie à toi-même ;</l>
							<l n="37" num="4.13">Ce tendre objet qui trompe, et qu’il faut que l’on aime,</l>
							<l n="38" num="4.14"><space quantity="10" unit="char"></space>Tu ne l’as jamais vu !…</l>
							<l n="39" num="4.15">Je l’ai vu plein d’amour, et l’amour m’a trompée ;</l>
							<l n="40" num="4.16">Je ne croyais que lui ; de lui seul occupée,</l>
							<l n="41" num="4.17">J’ai perdu mon repos dans sa félicité ;</l>
							<l n="42" num="4.18">Je l’ai voulu. Mon Dieu ! c’était sa volonté.</l>
						</lg>
						<lg n="5">
							<l n="43" num="5.1">Il savait tant de mots pour me rendre sensible,</l>
							<l n="44" num="5.2">Pour instruire mon âme ardente à la douleur !</l>
							<l n="45" num="5.3">Lui seul a ce pouvoir, cet art, ce don flexible,</l>
							<l n="46" num="5.4">Lui seul donne la vie ensemble et le malheur.</l>
							<l n="47" num="5.5">Mais le malheur enfin détache de la vie.</l>
							<l n="48" num="5.6"><space quantity="6" unit="char"></space>Non, je ne veux plus de mon sort,</l>
							<l n="49" num="5.7">Je ne veux plus souffrir. Sais-tu ce que j’envie ?</l>
							<l n="50" num="5.8">Sais-tu ce qu’après lui j’ai souhaité ? La mort.</l>
							<l n="51" num="5.9">Son pied ne presse plus le seuil de ma demeure,</l>
							<l n="52" num="5.10">Et pour ne la plus voir il invente un chemin.</l>
							<l n="53" num="5.11">Sans lui rien demander, j’écoute passer l’heure ;</l>
							<l n="54" num="5.12">L’heure dit comme lui : « Ni ce soir, ni demain ! »</l>
							<l n="55" num="5.13">Mais je compte, j’attends que moins inexorable</l>
							<l n="56" num="5.14">Une heure, la dernière à mes maux secourable,</l>
							<l n="57" num="5.15">Éteigne sur ma cendre un importun flambeau,</l>
							<l n="58" num="5.16">Et défende à l’amour de troubler mon tombeau.</l>
						</lg>
						<lg n="6">
							<l n="59" num="6.1">Quand celui qui me fuit ne songeait qu’à me suivre,</l>
							<l n="60" num="6.2">Le cours de mes beaux ans fut près de se tarir :</l>
							<l n="61" num="6.3"><space quantity="2" unit="char"></space>Qu’il m’eût alors été doux de mourir</l>
							<l n="62" num="6.4">Pour l’amant dont les pleurs me suppliaient de vivre !</l>
							<l n="63" num="6.5">« Ne meurs pas, disait-il, ou je meurs avec toi ! »</l>
							<l n="64" num="6.6">Et mon âme, enchaînée à cette âme amoureuse,</l>
							<l n="65" num="6.7">N’osa… quitter la terre et combler son effroi.</l>
							<l n="66" num="6.8">L’imprudent ! sous ses pleurs j’allais m’éteindre heureuse.</l>
							<l n="67" num="6.9">J’allais mourir aimée. Il m’a rendu des jours,</l>
							<l n="68" num="6.10">Pour m’apprendre, ô douleur ! qu’on n’aime pas toujours.</l>
						</lg>
						<lg n="7">
							<l n="69" num="7.1">Une nouvelle voix à son oreille est douce ;</l>
							<l n="70" num="7.2">D’autres yeux qu’il entend désarment son courroux ;</l>
							<l n="71" num="7.3">Et ce n’est plus ma main qu’il presse ou qu’il repousse,</l>
							<l n="72" num="7.4"><space quantity="6" unit="char"></space>Alors qu’il est tendre ou jaloux.</l>
							<l n="73" num="7.5">Quoi ! ce n’est plus vers moi qu’il apporte sans crainte</l>
							<l n="74" num="7.6">Son espoir, son désir, son plus secret dessein :</l>
							<l n="75" num="7.7">Et s’il est malheureux, s’il exhale une plainte,</l>
							<l n="76" num="7.8"><space quantity="10" unit="char"></space>Ce n’est plus dans mon sein !</l>
						</lg>
						<lg n="8">
							<l n="77" num="8.1">L’ai-je trahi ? Jamais. Il eut mon âme entière.</l>
							<l n="78" num="8.2">Hélas ! j’étais étreinte à lui comme le lierre.</l>
							<l n="79" num="8.3">Que pour m’en arracher il m’a fallu souffrir !</l>
							<l n="80" num="8.4">Dans cet effort cruel je me sentis mourir.</l>
							<l n="81" num="8.5">Il détourna les yeux, il n’a pas vu mes larmes ;</l>
							<l n="82" num="8.6">Mon reproche jamais n’éveilla ses alarmes ;</l>
							<l n="83" num="8.7">Jamais de ses beaux jours je ne ternis un jour ;</l>
							<l n="84" num="8.8">Il garda le bonheur ; moi, j’ai gardé l’amour.</l>
						</lg>
					</div></body></text></TEI>