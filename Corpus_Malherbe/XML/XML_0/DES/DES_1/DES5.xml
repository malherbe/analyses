<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>9618 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>9618 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies</title>
					<author>Marceline Desbordes-Valmore</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>A. Boulland, libraire-éditeur</publisher>
						<date when="1830">1830</date>
					</imprint>
					<extent>9618 verstome second</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-12-20" who="RR">Insertion des corrections métriques dans le corps du texte.</change>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
				<change when="2018-11-02" who="RR">Correction dans le poème "Dors, ma Mère".</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">IDYLLES</head><div type="poem" key="DES5">
						<head type="main">L’ORAGE</head>
						<lg n="1">
							<l n="1" num="1.1"><space quantity="6" unit="char"></space>Ô quelle accablante chaleur !</l>
							<l n="2" num="1.2">On dirait que le ciel va toucher la montagne.</l>
							<l n="3" num="1.3">Vois ce nuage en feu qui rougit la campagne :</l>
							<l n="4" num="1.4">Quels éclairs ! quel bruit sourd ! ne t’en va pas ; j’ai peur !</l>
							<l n="5" num="1.5"><space quantity="6" unit="char"></space>Les cris aigus de l’hirondelle</l>
							<l n="6" num="1.6">Annoncent le danger qui règne autour de nous ;</l>
							<l n="7" num="1.7">Son amant effrayé la poursuit et l’appelle,</l>
							<l n="8" num="1.8">Pauvres petits oiseaux, vous retrouverez-vous ?</l>
						</lg>
						<lg n="2">
							<l n="9" num="2.1">Reste, mon bien-aimé ! reste, je t’en conjure ;</l>
							<l n="10" num="2.2"><space quantity="10" unit="char"></space>Le ciel va s’entr’ouvrir.</l>
							<l n="11" num="2.3">De l’orage sans moi tu veux braver l’injure ;</l>
							<l n="12" num="2.4">Cruel ! en me quittant, tu me verrais mourir.</l>
							<l n="13" num="2.5">Ce nuage embrasé qui promène la foudre,</l>
							<l n="14" num="2.6">Vois-tu bien, s’il éclate, on est réduit en poudre !</l>
							<l n="15" num="2.7">Encourage mon cœur, il palpite pour toi…</l>
							<l n="16" num="2.8">Ta main tremble, Olivier. As-tu peur comme moi ?</l>
							<l n="17" num="2.9">Tu t’éloignes ; tu crains un danger que j’ignore :</l>
							<l n="18" num="2.10">En est-il un plus grand que d’exposer tes jours ?</l>
							<l n="19" num="2.11">Je donnerais pour toi ma vie et nos amours ;</l>
							<l n="20" num="2.12">Si j’avais d’autres biens, tu les aurais encore.</l>
							<l n="21" num="2.13">En cédant à tes vœux, j’ai trahi mon devoir ;</l>
							<l n="22" num="2.14">Mais ne m’en punis pas. Elle est loin, ta chaumière.</l>
							<l n="23" num="2.15">Pour nous parler d’amour, tu demandais le soir ;</l>
							<l n="24" num="2.16">Eh bien ! pour te sauver, prends la nuit tout entière ;</l>
							<l n="25" num="2.17">Mais ne me parle plus de ce cruel amour ;</l>
							<l n="26" num="2.18">Je vais l’offrir à Dieu, dans ma tristesse extrême :</l>
							<l n="27" num="2.19"><space quantity="6" unit="char"></space>C’est en priant pour ce que j’aime</l>
							<l n="28" num="2.20"><space quantity="10" unit="char"></space>Que j’attendrai le jour.</l>
						</lg>
						<lg n="3">
							<l n="29" num="3.1">Sur nos champs inondés tourne un moment la vue.</l>
							<l n="30" num="3.2">Réponds ! Malgré mes pleurs veux-tu partir encor ?</l>
							<l n="31" num="3.3">Méchant, ne souris plus de me voir trop émue ;</l>
							<l n="32" num="3.4">Peut-on ne pas trembler en quittant son trésor ?</l>
							<l n="33" num="3.5">Je vais me réunir à ma sœur endormie.</l>
							<l n="34" num="3.6">Adieu ! laisse gronder et gémir l’aquilon ;</l>
							<l n="35" num="3.7">Quand il aura cessé d’attrister le vallon,</l>
							<l n="36" num="3.8">Tu pourras t’éloigner du toit de ton amie.</l>
						</lg>
						<lg n="4">
							<l n="37" num="4.1">Mais quel nouveau malheur ! Qu’allons-nous devenir ?</l>
							<l n="38" num="4.2"><space quantity="2" unit="char"></space>N’entends-tu pas la voix de mon vieux père ?</l>
							<l n="39" num="4.3"><space quantity="2" unit="char"></space>Ne vois-tu pas une faible lumière ?</l>
							<l n="40" num="4.4"><space quantity="2" unit="char"></space>De ce côté, Dieu ! s’il allait venir !</l>
							<l n="41" num="4.5"><space quantity="2" unit="char"></space>Pour une faute, Olivier, que d’alarmes !</l>
							<l n="42" num="4.6">Laisse-moi seule au moins supporter son courroux ;</l>
							<l n="43" num="4.7"><space quantity="2" unit="char"></space>Puis tu viendras embrasser ses genoux,</l>
							<l n="44" num="4.8"><space quantity="2" unit="char"></space>Quand je l’aurai désarmé par mes larmes.</l>
							<l n="45" num="4.9">Non ! la porte entr’ouverte a causé ma frayeur :</l>
							<l n="46" num="4.10">On tremble au moindre bruit lorsque l’on est coupable.</l>
							<l n="47" num="4.11">Laisse-moi respirer du trouble qui m’accable,</l>
							<l n="48" num="4.12"><space quantity="6" unit="char"></space>Laisse-moi retrouver mon cœur.</l>
							<l n="49" num="4.13"><space quantity="2" unit="char"></space>Séparons-nous, je suis trop attendrie ;</l>
							<l n="50" num="4.14">Sur ce cœur agité ne pose plus ta main ;</l>
							<l n="51" num="4.15">Va ! si le ciel entend ma prière chérie,</l>
							<l n="52" num="4.16"><space quantity="6" unit="char"></space>Il sera plus calme demain :</l>
							<l n="53" num="4.17">Demain, au point du jour, j’irai trouver mon père ;</l>
							<l n="54" num="4.18">Sa bonté préviendra mes timides aveux ;</l>
							<l n="55" num="4.19">De nos tendres amours pardonnant le mystère,</l>
							<l n="56" num="4.20">Il ne t’appellera que pour combler tes vœux.</l>
						</lg>
						<lg n="5">
							<l n="57" num="5.1">Déjà le vent rapide emporte le nuage,</l>
							<l n="58" num="5.2">La lune nous ramène un doux rayon d’espoir ;</l>
							<l n="59" num="5.3">Adieu ! je ne crains plus d’oublier mon devoir,</l>
							<l n="60" num="5.4">Ô mon cher Olivier ! j’ai trop peur de l’orage !</l>
						</lg>
					</div></body></text></TEI>