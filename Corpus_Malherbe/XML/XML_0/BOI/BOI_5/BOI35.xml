<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE LUTRIN</title>
				<title type="sub">POÈME HÉROÏ-COMIQUE</title>
				<title type="medium">Une édition électronique</title>
				<author key="BOI">
					<name>
						<forename>Nicolas</forename>
						<surname>BOILEAU-DESPRÉAUX</surname>
					</name>
					<date from="1636" to="1711">1636-1711</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1228 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BOI_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Œuvres poétiques de Boileau-Despréaux avec une introduction et des notes, par F. Brunetière</title>
						<author>Nicolas Boileau-Despréaux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k97348967</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Œuvres poétiques</title>
								<author>Nicolas Boileau-Despréaux</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Hachette</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres poétiques</title>
						<author>Nicolas Boileau-Despréaux</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>chez TH. DESOER, LIBRAIRE,</publisher>
							<date when="1825">1825</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1674" to="1683">1674-1683</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La présente édition correspond à une partie du texte d’origine.</p>
				<p>L’ensemble des textes versifiés est réparti en 7 fichiers correspondant chacun à un chapitre.</p>
				<p>La préface ainsi que les autres parties liminaires ne sont pas encodées.</p>
				<p>Les gravures et références des gravures sont pas incluses.</p>
				<p>Les notes de l’éditeur ne sont pas incluses.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (séquence de 3 ou 4 points) ont été remplacés par le signe typographique approprié.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BOI35">
				<head type="main">CHANT VI</head>
				<lg n="1">
					<l n="1" num="1.1"><space quantity="2" unit="char"></space>Tandis que tout conspire à la guerre sacrée,</l>
					<l n="2" num="1.2">La Piété sincère, aux Alpes retirée,</l>
					<l n="3" num="1.3">Du fond de son désert entend les tristes cris</l>
					<l n="4" num="1.4">De ses sujets cachés dans les murs de Paris.</l>
					<l n="5" num="1.5">Elle quitte à l’instant sa retraite divine :</l>
					<l n="6" num="1.6">La Foi, d’un pas certain, devant elle chemine ;</l>
					<l n="7" num="1.7">L’Espérance au front gai l’appuie et la conduit ;</l>
					<l n="8" num="1.8">Et, la bourse à la main, la Charité la suit.</l>
					<l n="9" num="1.9">Vers Paris elle vole, et, d’une audace sainte,</l>
					<l n="10" num="1.10">Vient aux pieds de Thémis proférer cette plainte :</l>
					<l n="11" num="1.11">« Vierge, effroi des méchants, appui de mes autels,</l>
					<l n="12" num="1.12">Qui, la balance en main, règles tous les mortels,</l>
					<l n="13" num="1.13">Ne viendrai-je jamais en tes bras salutaires</l>
					<l n="14" num="1.14">Que pousser des soupirs, et pleurer mes misères ?</l>
					<l n="15" num="1.15">Ce n’est donc pas assez, qu’au mépris de tes lois,</l>
					<l n="16" num="1.16">L’Hypocrisie ait pris et mon nom et ma voix ;</l>
					<l n="17" num="1.17">Que, sous ce nom sacré, partout ses mains avares</l>
					<l n="18" num="1.18">Cherchent à me ravir crosses, mitres, tiares !</l>
					<l n="19" num="1.19">Faudra-t-il voir encor cent monstres furieux</l>
					<l n="20" num="1.20">Ravager mes États usurpés à tes yeux ?</l>
					<l n="21" num="1.21">Dans les temps orageux de mon naissant empire,</l>
					<l n="22" num="1.22">Au sortir du baptême on courait au martyre ;</l>
					<l n="23" num="1.23">Chacun, plein de mon nom, ne respirait que moi ;</l>
					<l n="24" num="1.24">Le fidèle, attentif aux règles de sa loi,</l>
					<l n="25" num="1.25">Fuyant des vanités la dangereuse amorce,</l>
					<l n="26" num="1.26">Aux honneurs appelé, n’y montait que par force ;</l>
					<l n="27" num="1.27">Ces cœurs, que les bourreaux ne faisaient point frémir,</l>
					<l n="28" num="1.28">A l’offre d’une mitre <choice resp="RR" hand="RR" reason="analysis" type="orthography"><sic>étaients</sic><corr source="edition_1925">étaient</corr></choice> prêts à gémir ;</l>
					<l n="29" num="1.29">Et, sans peur des travaux, sur mes traces divines</l>
					<l n="30" num="1.30">Couraient chercher le ciel au travers des épines.</l>
					<l n="31" num="1.31">Mais, depuis que l’Église eut, aux yeux des mortels,</l>
					<l n="32" num="1.32">De son sang en tous lieux cimenté ses autels,</l>
					<l n="33" num="1.33">Le calme dangereux succédant aux orages,</l>
					<l n="34" num="1.34">Une lâche tiédeur s’empara des courages ;</l>
					<l n="35" num="1.35">De leur zèle brûlant l’ardeur se ralentit ;</l>
					<l n="36" num="1.36">Sous le joug des péchés leur foi s’appesantit ;</l>
					<l n="37" num="1.37">Le moine secoua le cilice et la haire ;</l>
					<l n="38" num="1.38">Le chanoine indolent apprit à ne rien faire ;</l>
					<l n="39" num="1.39">Le prélat, par la brigue aux honneurs parvenu,</l>
					<l n="40" num="1.40">Ne sut plus qu’abuser d’un ample revenu,</l>
					<l n="41" num="1.41">Et, pour toutes vertus, fit, au dos d’un carrosse,</l>
					<l n="42" num="1.42">A côté d’une mitre armorier sa crosse.</l>
					<l n="43" num="1.43">L’Ambition partout chassa l’Humilité,</l>
					<l n="44" num="1.44">Dans la crasse du froc logea la Vanité.</l>
					<l n="45" num="1.45">Alors, de tous les cœurs l’union fut détruite ;</l>
					<l n="46" num="1.46">Dans mes cloîtres sacrés la Discorde introduite</l>
					<l n="47" num="1.47">Y bâtit de mon bien ses plus sûrs arsenaux ;</l>
					<l n="48" num="1.48">Traîna tous mes sujets au pied des tribunaux.</l>
					<l n="49" num="1.49">En vain, à ses fureurs j’opposai mes prières,</l>
					<l n="50" num="1.50">L’insolente, à mes yeux, marcha sous mes bannières.</l>
					<l n="51" num="1.51">Pour comble de misère, un tas de faux docteurs</l>
					<l n="52" num="1.52">Vint flatter les péchés de discours imposteurs ;</l>
					<l n="53" num="1.53">Infectant les esprits d’exécrables maximes,</l>
					<l n="54" num="1.54">Voulut faire à Dieu même approuver tous les crimes ;</l>
					<l n="55" num="1.55">Une servile peur tint lieu de charité ;</l>
					<l n="56" num="1.56">Le besoin d’aimer Dieu passa pour nouveauté ;</l>
					<l n="57" num="1.57">Et chacun à mes pieds, conservant sa malice,</l>
					<l n="58" num="1.58">N’apporta de vertu que l’aveu de son vice.</l>
					<l n="59" num="1.59"><space quantity="2" unit="char"></space>« Pour éviter l’affront de ces noirs attentats,</l>
					<l n="60" num="1.60">J’allai chercher le calme au séjour des frimas,</l>
					<l n="61" num="1.61">Sur ces monts entourés d’une éternelle glace,</l>
					<l n="62" num="1.62">Où jamais au printemps les hivers n’ont fait place.</l>
					<l n="63" num="1.63">Mais, jusque dans la nuit de mes sacrés déserts,</l>
					<l n="64" num="1.64">Le bruit de mes malheurs fait retentir les airs.</l>
					<l n="65" num="1.65">Aujourd’hui même encore, une voix trop fidèle</l>
					<l n="66" num="1.66">M’a d’un triste désastre apporté la nouvelle :</l>
					<l n="67" num="1.67">J’apprends, que dans ce temple où le plus saint des rois</l>
					<l n="68" num="1.68">Consacra tout le fruit de ses pieux exploits,</l>
					<l n="69" num="1.69">Et signala pour moi sa pompeuse largesse,</l>
					<l n="70" num="1.70">L’implacable Discorde et l’infâme Mollesse,</l>
					<l n="71" num="1.71">Foulant aux pieds les lois, l’honneur, et le devoir,</l>
					<l n="72" num="1.72">Usurpent en mon nom le souverain pouvoir.</l>
					<l n="73" num="1.73">Souffriras-tu, ma sœur, une action si noire ?</l>
					<l n="74" num="1.74">Quoi ! ce temple, à ta porte élevé pour ma gloire,</l>
					<l n="75" num="1.75">Où jadis des humains j’attirais tous les vœux,</l>
					<l n="76" num="1.76">Sera de leurs combats le théâtre honteux !</l>
					<l n="77" num="1.77">Non, non ; il faut enfin que ma vengeance éclate ;</l>
					<l n="78" num="1.78">Assez et trop longtemps l’impunité les flatte ;</l>
					<l n="79" num="1.79">Prends ton glaive, et, fondant sur ces audacieux,</l>
					<l n="80" num="1.80">Viens aux yeux des mortels justifier les cieux. »</l>
					<l n="81" num="1.81"><space quantity="2" unit="char"></space>Ainsi parle à sa sœur cette vierge enflammée :</l>
					<l n="82" num="1.82">La grâce est dans ses yeux d’un feu pur allumée.</l>
					<l n="83" num="1.83">Thémis, sans différer, lui promet son secours,</l>
					<l n="84" num="1.84">La flatte, la rassure, et lui tient ce discours :</l>
					<l n="85" num="1.85">« Chère et divine sœur, dont les mains secourables</l>
					<l n="86" num="1.86">Ont tant de fois séché les pleurs des misérables,</l>
					<l n="87" num="1.87">Pourquoi toi-même, en proie à tes vives douleurs,</l>
					<l n="88" num="1.88">Cherches-tu sans raison à grossir tes malheurs ?</l>
					<l n="89" num="1.89">En vain, de tes sujets l’ardeur est ralentie ;</l>
					<l n="90" num="1.90">D’un ciment éternel ton Église est bâtie,</l>
					<l n="91" num="1.91">Et jamais de l’enfer les noirs frémissements</l>
					<l n="92" num="1.92">N’en sauraient ébranler les fermes fondements.</l>
					<l n="93" num="1.93">Au milieu des combats, des troubles, des querelles,</l>
					<l n="94" num="1.94">Ton nom encor chéri vit au sein des fidèles.</l>
					<l n="95" num="1.95">Crois-moi, dans ce lieu même où l’on veut t’opprimer,</l>
					<l n="96" num="1.96">Le trouble qui t’étonne est facile à calmer ;</l>
					<l n="97" num="1.97">Et, pour y rappeler la paix tant désirée,</l>
					<l n="98" num="1.98">Je vais t’ouvrir, ma sœur, une route assurée.</l>
					<l n="99" num="1.99">Prête-moi donc l’oreille, et retiens tes soupirs.</l>
					<l n="100" num="1.100">Vers ce temple fameux, si cher à tes désirs,</l>
					<l n="101" num="1.101">Où le ciel fut pour toi si prodigue en miracles,</l>
					<l n="102" num="1.102">Non loin de ce palais où je rends mes oracles,</l>
					<l n="103" num="1.103">Est un vaste séjour des mortels révéré,</l>
					<l n="104" num="1.104">Et de clients soumis à toute heure entouré.</l>
					<l n="105" num="1.105">Là, sous le faix pompeux de ma pourpre honorable,</l>
					<l n="106" num="1.106">Veille au soin de ma gloire un homme incomparable :</l>
					<l n="107" num="1.107">Ariste, dont le ciel et Louis ont fait choix</l>
					<l n="108" num="1.108">Pour régler ma balance et dispenser mes lois.</l>
					<l n="109" num="1.109">Par lui dans le barreau sur mon trône affermie,</l>
					<l n="110" num="1.110">Je vois hurler en vain la chicane ennemie ;</l>
					<l n="111" num="1.111">Par lui, la vérité ne craint plus l’imposteur ;</l>
					<l n="112" num="1.112">Et l’orphelin n’est plus dévoré du tuteur.</l>
					<l n="113" num="1.113">Mais, pourquoi vainement t’en retracer l’image ?</l>
					<l n="114" num="1.114">Tu le connais assez : Ariste est ton ouvrage ;</l>
					<l n="115" num="1.115">C’est toi, qui le formas dès ses plus jeunes ans ;</l>
					<l n="116" num="1.116">Son mérite sans tache est un de tes présents ;</l>
					<l n="117" num="1.117">Tes divines leçons, avec le lait sucées,</l>
					<l n="118" num="1.118">Allumèrent l’ardeur de ses nobles pensées.</l>
					<l n="119" num="1.119">Aussi son cœur, pour toi brûlant d’un si beau feu,</l>
					<l n="120" num="1.120">N’en fit point dans le monde un lâche désaveu ;</l>
					<l n="121" num="1.121">Et son zèle hardi, toujours prêt à paroître,</l>
					<l n="122" num="1.122">N’alla point se cacher dans les ombres d’un cloître.</l>
					<l n="123" num="1.123">Va le trouver, ma sœur : à ton auguste nom,</l>
					<l n="124" num="1.124">Tout s’ouvrira d’abord en sa sainte maison ;</l>
					<l n="125" num="1.125">Ton visage est connu de sa noble famille ;</l>
					<l n="126" num="1.126">Tout y garde tes lois : enfants, sœur, femme, fille ;</l>
					<l n="127" num="1.127">Tes yeux d’un seul regard sauront le pénétrer ;</l>
					<l n="128" num="1.128">Et, pour obtenir tout, tu n’as qu’à te montrer. »</l>
					<l n="129" num="1.129"><space quantity="2" unit="char"></space>Là s’arrête Thémis. La Piété charmée</l>
					<l n="130" num="1.130">Sent renaître la joie en son âme calmée ;</l>
					<l n="131" num="1.131">Elle court chez Ariste ; et s’offrant à ses yeux :</l>
					<l n="132" num="1.132">« Que me sert, lui dit-elle, Ariste, qu’en tous lieux</l>
					<l n="133" num="1.133">Tu signales pour moi ton zèle et ton courage,</l>
					<l n="134" num="1.134">Si la Discorde impie à ta porte m’outrage ?</l>
					<l n="135" num="1.135">Deux puissants ennemis, par elle envenimés,</l>
					<l n="136" num="1.136">Dans ces murs, autrefois si saints, si renommés,</l>
					<l n="137" num="1.137">A mes sacrés autels font un profane insulte,</l>
					<l n="138" num="1.138">Remplissent tout d’effroi, de trouble, et de tumulte.</l>
					<l n="139" num="1.139">De leur crime à leurs yeux va-t’en peindre l’horreur :</l>
					<l n="140" num="1.140">Sauve-moi, sauve-les de leur propre fureur. »</l>
					<l n="141" num="1.141"><space quantity="2" unit="char"></space>Elle sort à ces mots. Le héros en prière</l>
					<l n="142" num="1.142">Demeure tout couvert de feux et de lumière ;</l>
					<l n="143" num="1.143">De la céleste fille il reconnaît l’éclat,</l>
					<l n="144" num="1.144">Et mande au même instant le chantre et le prélat.</l>
					<l n="145" num="1.145"><space quantity="2" unit="char"></space>Muse, c’est à ce coup que mon esprit timide</l>
					<l n="146" num="1.146">Dans sa course élevée a besoin qu’on le guide,</l>
					<l n="147" num="1.147">Pour chanter par quels soins, par quels nobles travaux,</l>
					<l n="148" num="1.148">Un mortel sut fléchir ces superbes rivaux.</l>
					<l n="149" num="1.149"><space quantity="2" unit="char"></space>Mais plutôt, toi qui fis ce merveilleux ouvrage,</l>
					<l n="150" num="1.150">Ariste, c’est à toi d’en instruire notre âge.</l>
					<l n="151" num="1.151">Seul tu peux révéler par quel art tout-puissant</l>
					<l n="152" num="1.152">Tu rendis tout à coup le chantre obéissant.</l>
					<l n="153" num="1.153">Tu sais, par quel conseil, rassemblant le chapitre,</l>
					<l n="154" num="1.154">Lui-même, de sa main, reporta le pupitre ;</l>
					<l n="155" num="1.155">Et comment le prélat, de ses respects content,</l>
					<l n="156" num="1.156">Le fit du banc fatal enlever à l’instant.</l>
					<l n="157" num="1.157">Parle donc : c’est à toi d’éclaircir ces merveilles.</l>
					<l n="158" num="1.158">Il me suffit, pour moi, d’avoir su, par mes veilles,</l>
					<l n="159" num="1.159">Jusqu’au sixième chant pousser ma fiction,</l>
					<l n="160" num="1.160">Et fait d’un vain pupitre un second Ilion.</l>
					<l n="161" num="1.161">Finissons. Aussi bien, quelque ardeur qui m’inspire,</l>
					<l n="162" num="1.162">Quand je songe au héros qui me reste à décrire,</l>
					<l n="163" num="1.163">Qu’il faut parler de toi, mon esprit éperdu</l>
					<l n="164" num="1.164">Demeure sans parole, interdit, confondu.</l>
					<l n="165" num="1.165"><space quantity="2" unit="char"></space>Ariste, c’est ainsi qu’en ce sénat illustre</l>
					<l n="166" num="1.166">Où Thémis, par tes soins, reprend son premier lustre,</l>
					<l n="167" num="1.167">Quand, la première fois, un athlète nouveau</l>
					<l n="168" num="1.168">Vient combattre en champ clos aux joutes du barreau,</l>
					<l n="169" num="1.169">Souvent, sans y penser, ton auguste présence</l>
					<l n="170" num="1.170">Troublant par trop d’éclat sa timide éloquence,</l>
					<l n="171" num="1.171">Le nouveau Cicéron, tremblant, décoloré,</l>
					<l n="172" num="1.172">Cherche en vain son discours sur sa langue égaré ;</l>
					<l n="173" num="1.173">En vain, pour gagner temps, dans ses transes affreuses,</l>
					<l n="174" num="1.174">Traîne d’un dernier mot les syllabes honteuses ;</l>
					<l n="175" num="1.175">Il hésite, il bégaye ; — et le triste orateur</l>
					<l n="176" num="1.176">Demeure enfin muet aux yeux du spectateur.</l>
				</lg>
			</div></body></text></TEI>