<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE LUTRIN</title>
				<title type="sub">POÈME HÉROÏ-COMIQUE</title>
				<title type="medium">Une édition électronique</title>
				<author key="BOI">
					<name>
						<forename>Nicolas</forename>
						<surname>BOILEAU-DESPRÉAUX</surname>
					</name>
					<date from="1636" to="1711">1636-1711</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1228 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BOI_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Œuvres poétiques de Boileau-Despréaux avec une introduction et des notes, par F. Brunetière</title>
						<author>Nicolas Boileau-Despréaux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k97348967</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Œuvres poétiques</title>
								<author>Nicolas Boileau-Despréaux</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Hachette</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres poétiques</title>
						<author>Nicolas Boileau-Despréaux</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>chez TH. DESOER, LIBRAIRE,</publisher>
							<date when="1825">1825</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1674" to="1683">1674-1683</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La présente édition correspond à une partie du texte d’origine.</p>
				<p>L’ensemble des textes versifiés est réparti en 7 fichiers correspondant chacun à un chapitre.</p>
				<p>La préface ainsi que les autres parties liminaires ne sont pas encodées.</p>
				<p>Les gravures et références des gravures sont pas incluses.</p>
				<p>Les notes de l’éditeur ne sont pas incluses.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (séquence de 3 ou 4 points) ont été remplacés par le signe typographique approprié.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BOI34">
				<head type="main">CHANT V</head>
				<lg n="1">
					<l n="1" num="1.1">L’Aurore cependant, d’un juste effroi troublée,</l>
					<l n="2" num="1.2">Des chanoines levés voit la troupe assemblée,</l>
					<l n="3" num="1.3">Et contemple longtemps, avec des yeux confus,</l>
					<l n="4" num="1.4">Ces visages fleuris qu’elle n’a jamais vus.</l>
					<l n="5" num="1.5">Chez Sidrac aussitôt Brontin d’un pied fidèle</l>
					<l n="6" num="1.6">Du pupitre abattu va porter la nouvelle.</l>
					<l n="7" num="1.7">Le vieillard, de ses soins bénit l’heureux succès,</l>
					<l n="8" num="1.8">Et sur un bois détruit bâtit mille procès ;</l>
					<l n="9" num="1.9">L’espoir d’un doux tumulte échauffant son courage,</l>
					<l n="10" num="1.10">Il ne sent plus le poids ni les glaces de l’âge ;</l>
					<l n="11" num="1.11">Et chez le trésorier, de ce pas, à grand bruit,</l>
					<l n="12" num="1.12">Vient étaler au jour les crimes de la nuit.</l>
					<l n="13" num="1.13">Au récit imprévu de l’horrible insolence,</l>
					<l n="14" num="1.14">Le prélat, hors du lit, impétueux, s’élance.</l>
					<l n="15" num="1.15">Vainement, d’un breuvage à deux mains apporté,</l>
					<l n="16" num="1.16">Gilotin avant tout le veut voir humecté ;</l>
					<l n="17" num="1.17">Il veut partir à jeun ; il se peigne, il s’apprête ;</l>
					<l n="18" num="1.18">L’ivoire trop hâté deux fois rompt sur sa tête,</l>
					<l n="19" num="1.19">Et deux fois de sa main le buis tombe en morceaux.</l>
					<l n="20" num="1.20">Tel, Hercule filant rompait tous les fuseaux.</l>
					<l n="21" num="1.21">Il sort demi-paré ; mais déjà, sur sa porte,</l>
					<l n="22" num="1.22">Il voit de saints guerriers une ardente cohorte,</l>
					<l n="23" num="1.23">Qui tous, remplis pour lui d’une égale vigueur,</l>
					<l n="24" num="1.24">Sont prêts, pour le servir, à déserter le chœur.</l>
					<l n="25" num="1.25">Mais le vieillard condamne un projet inutile.</l>
					<l n="26" num="1.26">« Nos destins sont, dit-il, écrits chez la Sibylle :</l>
					<l n="27" num="1.27">Son antre n’est pas loin ; allons la consulter,</l>
					<l n="28" num="1.28">Et subissons la loi qu’elle nous va dicter. »</l>
					<l n="29" num="1.29">Il dit : à ce conseil, où la raison domine,</l>
					<l n="30" num="1.30">Sur ses pas au barreau la troupe s’achemine,</l>
					<l n="31" num="1.31">Et bientôt, dans le temple, entend, non sans frémir,</l>
					<l n="32" num="1.32">De l’antre redouté les soupiraux gémir.</l>
					<l n="33" num="1.33"><space quantity="2" unit="char"></space>Entre ces vieux appuis, dont l’affreuse Grand’salle</l>
					<l n="34" num="1.34">Soutient l’énorme poids de sa voûte infernale,</l>
					<l n="35" num="1.35">Est un pilier fameux, des plaideurs respecté,</l>
					<l n="36" num="1.36">Et toujours de Normands à midi fréquenté.</l>
					<l n="37" num="1.37">Là, sur des tas poudreux de sacs et de pratique,</l>
					<l n="38" num="1.38">Hurle tous les matins une Sibylle étique :</l>
					<l n="39" num="1.39">On l’appelle Chicane ; et ce monstre odieux</l>
					<l n="40" num="1.40">Jamais pour l’équité n’eut d’oreilles ni d’yeux.</l>
					<l n="41" num="1.41">La Disette au teint blême, et la triste Famine,</l>
					<l n="42" num="1.42">Les Chagrins dévorants, et l’infâme Ruine,</l>
					<l n="43" num="1.43">Enfants infortunés de ses raffinements,</l>
					<l n="44" num="1.44">Troublent l’air d’alentour de longs gémissements.</l>
					<l n="45" num="1.45">Sans cesse, feuilletant les lois et la coutume,</l>
					<l n="46" num="1.46">Pour consumer autrui, le monstre se consume ;</l>
					<l n="47" num="1.47">Et, dévorant maisons, palais, châteaux entiers,</l>
					<l n="48" num="1.48">Rend pour des monceaux d’or de vains tas de papiers.</l>
					<l n="49" num="1.49">Sous le coupable effort de sa noire insolence,</l>
					<l n="50" num="1.50">Thémis a vu cent fois chanceler sa balance.</l>
					<l n="51" num="1.51">Incessamment il va de détour en détour.</l>
					<l n="52" num="1.52">Comme un hibou, souvent il se dérobe au jour.</l>
					<l n="53" num="1.53">Tantôt, les yeux en feu, c’est un lion superbe ;</l>
					<l n="54" num="1.54">Tantôt, humble serpent, il se glisse sous l’herbe.</l>
					<l n="55" num="1.55">En vain, pour le dompter, le plus juste des rois</l>
					<l n="56" num="1.56">Fit régler le chaos des ténébreuses lois :</l>
					<l n="57" num="1.57">Ses griffes, vainement par Pussort accourcies,</l>
					<l n="58" num="1.58">Se rallongent déjà, toujours d’encre noircies ;</l>
					<l n="59" num="1.59">Et ses ruses, perçant et digues et remparts,</l>
					<l n="60" num="1.60">Par cent brèches déjà rentrent de toutes parts.</l>
					<l n="61" num="1.61"><space quantity="2" unit="char"></space>Le vieillard humblement l’aborde et le salue ;</l>
					<l n="62" num="1.62">Et faisant, avant tout, briller l’or à sa vue :</l>
					<l n="63" num="1.63">« Reine des longs procès, dit-il, dont le savoir</l>
					<l n="64" num="1.64">Rend la force inutile et les lois sans pouvoir ;</l>
					<l n="65" num="1.65">Toi, pour qui dans le Mans le laboureur moissonne ;</l>
					<l n="66" num="1.66">Pour qui naissent à Caen tous les fruits de l’automne ;</l>
					<l n="67" num="1.67">Si, dès mes premiers ans, heurtant tous les mortels,</l>
					<l n="68" num="1.68">L’encre a toujours pour moi coulé sur tes autels,</l>
					<l n="69" num="1.69">Daigne encor me connaître en ma saison dernière.</l>
					<l n="70" num="1.70">D’un prélat, qui t’implore, exauce la prière.</l>
					<l n="71" num="1.71">Un rival orgueilleux, de sa gloire offensé,</l>
					<l n="72" num="1.72">A détruit le lutrin par nos mains redressé :</l>
					<l n="73" num="1.73">Épuise en sa faveur ta science fatale ;</l>
					<l n="74" num="1.74">Du Digeste et du Code ouvre-nous le dédale ;</l>
					<l n="75" num="1.75">Et montre-nous cet art, connu de tes amis,</l>
					<l n="76" num="1.76">Qui, dans ses propres lois, embarrasse Thémis. »</l>
					<l n="77" num="1.77"><space quantity="2" unit="char"></space>La Sibylle, à ces mots, déjà hors d’elle-même,</l>
					<l n="78" num="1.78">Fait lire sa fureur sur son visage blême ;</l>
					<l n="79" num="1.79">Et, pleine du démon qui la vient oppresser,</l>
					<l n="80" num="1.80">Par ces mots étonnants tâche à le repousser :</l>
					<l n="81" num="1.81">« Chantres, ne craignez plus une audace insensée :</l>
					<l n="82" num="1.82">Je vois, je vois au chœur la masse replacée ;</l>
					<l n="83" num="1.83">Mais il faut des combats. Tel est l’arrêt du sort ;</l>
					<l n="84" num="1.84">Et surtout, évitez un dangereux accord. »</l>
					<l n="85" num="1.85"><space quantity="2" unit="char"></space>Là, bornant son discours, encor tout écumante,</l>
					<l n="86" num="1.86">Elle souffle aux guerriers l’esprit qui la tourmente ;</l>
					<l n="87" num="1.87">Et dans leurs cœurs, brûlants de la soif de plaider,</l>
					<l n="88" num="1.88">Verse l’amour de nuire, et la peur de céder.</l>
					<l n="89" num="1.89"><space quantity="2" unit="char"></space>Pour tracer à loisir une longue requête,</l>
					<l n="90" num="1.90">A retourner chez soi leur brigade s’apprête.</l>
					<l n="91" num="1.91">Sous leurs pas diligents le chemin disparoît,</l>
					<l n="92" num="1.92">Et le pilier, loin d’eux, déjà baisse et décroît.</l>
					<l n="93" num="1.93"><space quantity="2" unit="char"></space>Loin du bruit cependant, les chanoines à table</l>
					<l n="94" num="1.94">Immolent trente mets à leur faim indomptable ;</l>
					<l n="95" num="1.95">Leur appétit fougueux, par l’objet excité,</l>
					<l n="96" num="1.96">Parcourt tous les recoins d’un monstrueux pâté ;</l>
					<l n="97" num="1.97">Par le sel irritant la soif est allumée ;</l>
					<l n="98" num="1.98">Lorsque, d’un pied léger, la prompte Renommée,</l>
					<l n="99" num="1.99">Semant partout l’effroi, vient au chantre éperdu</l>
					<l n="100" num="1.100">Conter l’affreux détail de l’oracle rendu.</l>
					<l n="101" num="1.101">Il se lève, enflammé de muscat et de bile,</l>
					<l n="102" num="1.102">Et prétend à son tour consulter la Sibylle.</l>
					<l n="103" num="1.103">Évrard a beau gémir du repas déserté :</l>
					<l n="104" num="1.104">Lui-même est au barreau par le nombre emporté.</l>
					<l n="105" num="1.105">Par les détours étroits d’une barrière oblique,</l>
					<l n="106" num="1.106">Ils gagnent les degrés et le perron antique,</l>
					<l n="107" num="1.107">Où sans cesse, étalant bons et méchants écrits,</l>
					<l n="108" num="1.108">Barbin vend aux passants des auteurs à tout prix.</l>
					<l n="109" num="1.109"><space quantity="2" unit="char"></space>Là, le chantre à grand bruit arrive et se fait place,</l>
					<l n="110" num="1.110">Dans le fatal instant que, d’une égale audace,</l>
					<l n="111" num="1.111">Le prélat et sa troupe, à pas tumultueux,</l>
					<l n="112" num="1.112">Descendaient du Palais l’escalier tortueux.</l>
					<l n="113" num="1.113">L’un et l’autre rival, s’arrêtant au passage,</l>
					<l n="114" num="1.114">Se mesure des yeux, s’observe, s’envisage ;</l>
					<l n="115" num="1.115">Une égale fureur anime leurs esprits.</l>
					<l n="116" num="1.116">Tels, deux fougueux taureaux, de jalousie épris,</l>
					<l n="117" num="1.117">Auprès d’une génisse au front large et superbe,</l>
					<l n="118" num="1.118">Oubliant tous les jours le pâturage et l’herbe,</l>
					<l n="119" num="1.119">A l’aspect l’un de l’autre embrasés, furieux,</l>
					<l n="120" num="1.120">Déjà le front baissé, se menacent des yeux.</l>
					<l n="121" num="1.121">Mais Évrard, en passant coudoyé par Boirude,</l>
					<l n="122" num="1.122">Ne sait point contenir son aigre inquiétude ;</l>
					<l n="123" num="1.123">Il entre chez Barbin, et, d’un bras irrité,</l>
					<l n="124" num="1.124">Saisissant du Cyrus un volume écarté,</l>
					<l n="125" num="1.125">Il lance au sacristain le tome épouvantable.</l>
					<l n="126" num="1.126">Boirude fuit le coup : le volume effroyable</l>
					<l n="127" num="1.127">Lui rase le visage, et, droit dans l’estomac,</l>
					<l n="128" num="1.128">Va frapper en sifflant l’infortuné Sidrac.</l>
					<l n="129" num="1.129">Le vieillard, accablé de l’horrible Artamène,</l>
					<l n="130" num="1.130">Tombe aux pieds du prélat, sans pouls et sans haleine ;</l>
					<l n="131" num="1.131">Sa troupe le croit mort ; et chacun, empressé,</l>
					<l n="132" num="1.132">Se croit frappé du coup dont il le voit blessé.</l>
					<l n="133" num="1.133">Aussitôt, contre Évrard vingt champions s’élancent ;</l>
					<l n="134" num="1.134">Pour soutenir leur choc les chanoines s’avancent ;</l>
					<l n="135" num="1.135">La Discorde triomphe ; et du combat fatal</l>
					<l n="136" num="1.136">Par un cri donne en l’air l’effroyable signal.</l>
					<l n="137" num="1.137"><space quantity="2" unit="char"></space>Chez le libraire absent tout entre, tout se mêle ;</l>
					<l n="138" num="1.138">Les livres sur Évrard fondent comme la grêle,</l>
					<l n="139" num="1.139">Qui dans un grand jardin, à coups impétueux,</l>
					<l n="140" num="1.140">Abat l’honneur naissant des rameaux fructueux.</l>
					<l n="141" num="1.141">Chacun s’arme au hasard du livre qu’il rencontre :</l>
					<l n="142" num="1.142">L’un tient le <hi rend="ital">Nœud d’amour</hi>, l’autre en saisit la <hi rend="ital">Montre</hi> ;</l>
					<l n="143" num="1.143">L’un prend le seul <hi rend="ital">Jonas</hi> qu’on ait vu relié ;</l>
					<l n="144" num="1.144">L’autre, un Tasse français, en naissant oublié.</l>
					<l n="145" num="1.145">L’élève de Barbin, commis à la boutique,</l>
					<l n="146" num="1.146">Veut en vain s’opposer à leur fureur gothique :</l>
					<l n="147" num="1.147">Les volumes, sans choix à la tête jetés,</l>
					<l n="148" num="1.148">Sur le perron poudreux volent de tous côtés.</l>
					<l n="149" num="1.149">Là, près d’un Guarini, Térence tombe à terre ;</l>
					<l n="150" num="1.150">Là, Xénophon dans l’air heurte contre un La Serre.</l>
					<l n="151" num="1.151">Oh ! que d’écrits obscurs, de livres ignorés,</l>
					<l n="152" num="1.152">Furent en ce grand jour de la poudre tirés !</l>
					<l n="153" num="1.153">Vous en fûtes tirés, <hi rend="ital">Almerinde</hi> et <hi rend="ital">Simandre</hi> ;</l>
					<l n="154" num="1.154">Et toi, rebut du peuple, inconnu <hi rend="ital">Caloandre</hi>,</l>
					<l n="155" num="1.155">Dans ton repos, dit-on, saisi par Gaillerbois,</l>
					<l n="156" num="1.156">Tu vis le jour alors pour la première fois !</l>
					<l n="157" num="1.157">Chaque coup sur la chair laisse une meurtrissure ;</l>
					<l n="158" num="1.158">Déjà plus d’un guerrier se plaint d’une blessure.</l>
					<l n="159" num="1.159">D’un Le Vayer épais Giraut est renversé ;</l>
					<l n="160" num="1.160">Marineau, d’un Brébeuf à l’épaule blessé,</l>
					<l n="161" num="1.161">En sent par tout le bras une douleur amère,</l>
					<l n="162" num="1.162">Et maudit la <hi rend="ital">Pharsale</hi> aux provinces si chère.</l>
					<l n="163" num="1.163">D’un Pinchêne in-quarto Dodillon étourdi</l>
					<l n="164" num="1.164">A longtemps le teint pâle et le cœur affadi.</l>
					<l n="165" num="1.165">Au plus fort du combat, le chapelain Garagne,</l>
					<l n="166" num="1.166">Vers le sommet du front atteint d’un <hi rend="ital">Charlemagne</hi>,</l>
					<l n="167" num="1.167">— Des vers de ce poème effet prodigieux !</l>
					<l n="168" num="1.168">— Tout prêt à s’endormir, bâille et ferme les yeux.</l>
					<l n="169" num="1.169">A plus d’un combattant la <hi rend="ital">Clélie</hi> est fatale :</l>
					<l n="170" num="1.170">Girou dix fois par elle éclate et se signale.</l>
					<l n="171" num="1.171">Mais, tout cède aux efforts du chanoine Fabri :</l>
					<l n="172" num="1.172">Ce guerrier, dans l’Église aux querelles nourri,</l>
					<l n="173" num="1.173">Est robuste de corps, terrible de visage,</l>
					<l n="174" num="1.174">Et de l’eau dans son vin n’a jamais su l’usage.</l>
					<l n="175" num="1.175">Il terrasse lui seul et Guibert et Grasset,</l>
					<l n="176" num="1.176">Et Gorillon la basse, et Grandin le fausset,</l>
					<l n="177" num="1.177">Et Gerbais l’agréable, et Guérin l’insipide.</l>
					<l n="178" num="1.178"><space quantity="2" unit="char"></space>Des chantres désormais la brigade timide</l>
					<l n="179" num="1.179">S’écarte, et du Palais regagne les chemins :</l>
					<l n="180" num="1.180">Telle, à l’aspect d’un loup, terreur des champs voisins,</l>
					<l n="181" num="1.181">Fuit d’agneaux effrayés une troupe bêlante ;</l>
					<l n="182" num="1.182">Ou tels, devant Achille, aux campagnes du Xanthe,</l>
					<l n="183" num="1.183">Les Troyens se sauvaient à l’abri de leurs tours ;</l>
					<l n="184" num="1.184">Quand Brontin à Boirude adresse ce discours :</l>
					<l n="185" num="1.185">« Illustre porte-croix, par qui notre bannière</l>
					<l n="186" num="1.186">N’a jamais en marchant fait un pas en arrière,</l>
					<l n="187" num="1.187">Un chanoine lui seul, triomphant du prélat,</l>
					<l n="188" num="1.188">Du rochet à nos yeux ternira-t-il l’éclat ?</l>
					<l n="189" num="1.189">Non, non : pour te couvrir de sa main redoutable,</l>
					<l n="190" num="1.190">Accepte de mon corps l’épaisseur favorable ;</l>
					<l n="191" num="1.191">Viens, et, sous ce rempart, à ce guerrier hautain</l>
					<l n="192" num="1.192">Fais voler ce Quinault qui me reste à la main. »</l>
					<l n="193" num="1.193">A ces mots, il lui tend le doux et tendre ouvrage :</l>
					<l n="194" num="1.194">Le sacristain, bouillant de zèle et de courage,</l>
					<l n="195" num="1.195">Le prend, se cache, approche, et, droit entre les yeux,</l>
					<l n="196" num="1.196">Frappe du noble écrit l’athlète audacieux.</l>
					<l n="197" num="1.197">Mais, c’est pour l’ébranler une faible tempête,</l>
					<l n="198" num="1.198">Le livre sans vigueur mollit contre sa tête.</l>
					<l n="199" num="1.199">Le chanoine les voit ; de colère embrasé :</l>
					<l n="200" num="1.200">« Attendez, leur dit-il, couple lâche et rusé,</l>
					<l n="201" num="1.201">Et jugez si ma main, aux grands exploits novice,</l>
					<l n="202" num="1.202">Lance à mes ennemis un livre qui mollisse. »</l>
					<l n="203" num="1.203">A ces mots, il saisit un vieil <hi rend="ital">Infortiat</hi>,</l>
					<l n="204" num="1.204">Grossi des visions d’Accurse et d’Alciat,</l>
					<l n="205" num="1.205">Inutile ramas de gothique écriture,</l>
					<l n="206" num="1.206">Dont quatre ais mal unis formaient la couverture,</l>
					<l n="207" num="1.207">Entourée à demi d’un vieux parchemin noir,</l>
					<l n="208" num="1.208">Où pendait à trois clous un reste de fermoir.</l>
					<l n="209" num="1.209">Sur l’ais qui le soutient auprès d’un Avicenne,</l>
					<l n="210" num="1.210">Deux des plus forts mortels l’ébranleraient à peine :</l>
					<l n="211" num="1.211">Le chanoine, pourtant, l’enlève sans effort ;</l>
					<l n="212" num="1.212">Et, sur le couple pâle et déjà demi-mort,</l>
					<l n="213" num="1.213">Fait tomber à deux mains l’effroyable tonnerre.</l>
					<l n="214" num="1.214">Les guerriers, de ce coup, vont mesurer la terre,</l>
					<l n="215" num="1.215">Et, du bois et des clous meurtris et déchirés,</l>
					<l n="216" num="1.216">Longtemps, loin du perron, roulent sur les degrés.</l>
					<l n="217" num="1.217">Au spectacle étonnant de leur chute imprévue,</l>
					<l n="218" num="1.218">Le prélat pousse un cri qui pénètre la nue.</l>
					<l n="219" num="1.219">Il maudit dans son cœur le démon des combats,</l>
					<l n="220" num="1.220">Et de l’horreur du coup il recule six pas.</l>
					<l n="221" num="1.221">Mais, bientôt, rappelant son antique prouesse,</l>
					<l n="222" num="1.222">Il tire du manteau sa dextre vengeresse ;</l>
					<l n="223" num="1.223">Il part, et, de ses doigts saintement allongés,</l>
					<l n="224" num="1.224">Bénit tous les passants, en deux files rangés.</l>
					<l n="225" num="1.225">Il sait que l’ennemi, que ce coup va surprendre,</l>
					<l n="226" num="1.226">Désormais sur ses pieds ne l’oserait attendre,</l>
					<l n="227" num="1.227">Et déjà voit pour lui tout le peuple en courroux</l>
					<l n="228" num="1.228">Crier aux combattants : « Profanes, à genoux ! »</l>
					<l n="229" num="1.229">Le chantre, qui de loin voit approcher l’orage,</l>
					<l n="230" num="1.230">Dans son cœur éperdu cherche en vain du courage ;</l>
					<l n="231" num="1.231">Sa fierté l’abandonne : il tremble, il cède, il fuit ;</l>
					<l n="232" num="1.232">Le long des sacrés murs sa brigade le suit ;</l>
					<l n="233" num="1.233">Tout s’écarte à l’instant ; mais aucun n’en réchappe ;</l>
					<l n="234" num="1.234">Partout, le doigt vainqueur les suit et les rattrape.</l>
					<l n="235" num="1.235">Évrard seul, en un coin prudemment retiré,</l>
					<l n="236" num="1.236">Se croyait à couvert de l’insulte sacré ;</l>
					<l n="237" num="1.237">Mais le prélat vers lui fait une marche adroite ;</l>
					<l n="238" num="1.238">Il l’observe de l’œil, et tirant vers la droite,</l>
					<l n="239" num="1.239">Tout d’un coup tourne à gauche, et d’un bras fortuné</l>
					<l n="240" num="1.240">Bénit subitement le guerrier consterné.</l>
					<l n="241" num="1.241">Le chanoine, surpris de la foudre mortelle,</l>
					<l n="242" num="1.242">Se dresse, et lève en vain une tête rebelle ;</l>
					<l n="243" num="1.243">Sur ses genoux tremblants il tombe à cet aspect,</l>
					<l n="244" num="1.244">Et donne à la frayeur ce qu’il doit au respect.</l>
					<l n="245" num="1.245">Dans le temple aussitôt, le prélat plein de gloire</l>
					<l n="246" num="1.246">Va goûter les doux fruits de sa sainte victoire ;</l>
					<l n="247" num="1.247">Et de leur vain projet les chanoines punis,</l>
					<l n="248" num="1.248">S’en retournent chez eux éperdus et bénis.</l>
				</lg>
			</div></body></text></TEI>