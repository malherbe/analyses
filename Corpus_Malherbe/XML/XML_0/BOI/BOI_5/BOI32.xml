<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE LUTRIN</title>
				<title type="sub">POÈME HÉROÏ-COMIQUE</title>
				<title type="medium">Une édition électronique</title>
				<author key="BOI">
					<name>
						<forename>Nicolas</forename>
						<surname>BOILEAU-DESPRÉAUX</surname>
					</name>
					<date from="1636" to="1711">1636-1711</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1228 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BOI_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Œuvres poétiques de Boileau-Despréaux avec une introduction et des notes, par F. Brunetière</title>
						<author>Nicolas Boileau-Despréaux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k97348967</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Œuvres poétiques</title>
								<author>Nicolas Boileau-Despréaux</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Hachette</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres poétiques</title>
						<author>Nicolas Boileau-Despréaux</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>chez TH. DESOER, LIBRAIRE,</publisher>
							<date when="1825">1825</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1674" to="1683">1674-1683</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La présente édition correspond à une partie du texte d’origine.</p>
				<p>L’ensemble des textes versifiés est réparti en 7 fichiers correspondant chacun à un chapitre.</p>
				<p>La préface ainsi que les autres parties liminaires ne sont pas encodées.</p>
				<p>Les gravures et références des gravures sont pas incluses.</p>
				<p>Les notes de l’éditeur ne sont pas incluses.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (séquence de 3 ou 4 points) ont été remplacés par le signe typographique approprié.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BOI32">
				<head type="main">CHANT III</head>
				<lg n="1">
					<l n="1" num="1.1"><space quantity="2" unit="char"></space>Mais la Nuit aussitôt, de ses ailes affreuses,</l>
					<l n="2" num="1.2">Couvre des Bourguignons les campagnes vineuses,</l>
					<l n="3" num="1.3">Revole vers Paris, et, hâtant son retour,</l>
					<l n="4" num="1.4">Déjà de Montlhéry voit la fameuse tour.</l>
					<l n="5" num="1.5">Ses murs, dont le sommet se dérobe à la vue,</l>
					<l n="6" num="1.6">Sur la cime d’un roc s’allongent dans la nue,</l>
					<l n="7" num="1.7">Et, présentant de loin leur objet ennuyeux,</l>
					<l n="8" num="1.8">Du passant qui le fuit, semblent suivre les yeux.</l>
					<l n="9" num="1.9">Mille oiseaux effrayants, mille corbeaux funèbres</l>
					<l n="10" num="1.10">De ces murs désertés habitent les ténèbres :</l>
					<l n="11" num="1.11">Là, depuis trente hivers, un hibou retiré</l>
					<l n="12" num="1.12">Trouvait contre le jour un refuge assuré.</l>
					<l n="13" num="1.13">Des désastres fameux ce messager fidèle</l>
					<l n="14" num="1.14">Sait toujours des malheurs la première nouvelle ;</l>
					<l n="15" num="1.15">Et, tout prêt d’en semer le présage odieux,</l>
					<l n="16" num="1.16">Il attendait la Nuit dans ces sauvages lieux.</l>
					<l n="17" num="1.17">Aux cris qu’à son abord vers le ciel il envoie,</l>
					<l n="18" num="1.18">Il rend tous ses voisins attristés de sa joie.</l>
					<l n="19" num="1.19">La plaintive Progné de douleur en frémit,</l>
					<l n="20" num="1.20">Et, dans les bois prochains, Philomèle en gémit.</l>
					<l n="21" num="1.21">« Suis-moi, » lui dit la Nuit. L’oiseau plein d’allégresse</l>
					<l n="22" num="1.22">Reconnaît à ce ton la voix de sa maîtresse ;</l>
					<l n="23" num="1.23">Il la suit ; et tous deux, d’un cours précipité,</l>
					<l n="24" num="1.24">De Paris, à l’instant, abordent la Cité.</l>
					<l n="25" num="1.25">Là, s’élançant d’un vol que le vent favorise,</l>
					<l n="26" num="1.26">Ils montent au sommet de la fatale église.</l>
					<l n="27" num="1.27">La Nuit baisse la vue, et, du haut du clocher,</l>
					<l n="28" num="1.28">Observe les guerriers, les regarde marcher :</l>
					<l n="29" num="1.29">Elle voit le barbier, qui d’une main légère,</l>
					<l n="30" num="1.30">Tient un verre de vin qui rit dans la fougère ;</l>
					<l n="31" num="1.31">Et chacun, tour à tour s’inondant de ce jus,</l>
					<l n="32" num="1.32">Célébrer, en buvant, Gilotin et Bacchus.</l>
					<l n="33" num="1.33">« Ils triomphent, dit-elle, et leur âme abusée</l>
					<l n="34" num="1.34">Se promet dans mon ombre une victoire aisée ;</l>
					<l n="35" num="1.35">Mais, allons ; il est temps qu’ils connaissent la Nuit. »</l>
					<l n="36" num="1.36">A ces mots, regardant le hibou qui la suit,</l>
					<l n="37" num="1.37">Elle perce les murs de la voûte sacrée ;</l>
					<l n="38" num="1.38">Jusqu’en la sacristie elle s’ouvre une entrée ;</l>
					<l n="39" num="1.39">Et, dans le ventre creux du pupitre fatal,</l>
					<l n="40" num="1.40">Va placer de ce pas le sinistre animal.</l>
					<l n="41" num="1.41"><space quantity="2" unit="char"></space>Mais les trois champions, pleins de vin et d’audace,</l>
					<l n="42" num="1.42">Du Palais cependant passent la grande place ;</l>
					<l n="43" num="1.43">Et, suivant de Bacchus les auspices sacrés,</l>
					<l n="44" num="1.44">De l’auguste Chapelle ils montent les degrés.</l>
					<l n="45" num="1.45">Ils atteignaient déjà le superbe portique</l>
					<l n="46" num="1.46">Où Ribou le libraire, au fond de sa boutique,</l>
					<l n="47" num="1.47">Sous vingt fidèles clefs garde et tient en dépôt</l>
					<l n="48" num="1.48">L’amas toujours entier des écrits de Haynaut. :</l>
					<l n="49" num="1.49">Quand Boirude, qui voit que le péril approche,</l>
					<l n="50" num="1.50">Les arrête ; et, tirant un fusil de sa poche,</l>
					<l n="51" num="1.51">Des veines d’un caillou, qu’il frappe au même instant,</l>
					<l n="52" num="1.52">Il fait jaillir un feu qui pétille en sortant ;</l>
					<l n="53" num="1.53">Et bientôt, au brasier d’une mèche enflammée</l>
					<l n="54" num="1.54">Montre, à l’aide du soufre une cire allumée.</l>
					<l n="55" num="1.55">Cet astre tremblotant, dont le jour les conduit,</l>
					<l n="56" num="1.56">Est pour eux un soleil au milieu de la nuit.</l>
					<l n="57" num="1.57">Le temple, à sa faveur, est ouvert par Boirude ;</l>
					<l n="58" num="1.58">Ils passent de la nef la vaste solitude ;</l>
					<l n="59" num="1.59">Et dans la sacristie entrant, non sans terreur,</l>
					<l n="60" num="1.60">En percent jusqu’au fond la ténébreuse horreur.</l>
					<l n="61" num="1.61"><space quantity="2" unit="char"></space>C’est là, que du lutrin gît la machine énorme.</l>
					<l n="62" num="1.62">La troupe quelque temps en admire la forme.</l>
					<l n="63" num="1.63">Mais le barbier, qui tient les moments précieux :</l>
					<l n="64" num="1.64">« Ce spectacle n’est pas pour amuser nos yeux,</l>
					<l n="65" num="1.65">Dit-il ; le temps est cher ; portons-le dans le temple ;</l>
					<l n="66" num="1.66">C’est là qu’il faut demain qu’un prélat le contemple. »</l>
					<l n="67" num="1.67">Et d’un bras, à ces mots, qui peut tout ébranler,</l>
					<l n="68" num="1.68">Lui-même, se courbant, s’apprête à le rouler.</l>
					<l n="69" num="1.69">Mais à peine il y touche, ô prodige incroyable !</l>
					<l n="70" num="1.70">Que du pupitre sort une voix effroyable.</l>
					<l n="71" num="1.71">Brontin en est ému, le sacristain pâlit,</l>
					<l n="72" num="1.72">Le perruquier commence à regretter son lit.</l>
					<l n="73" num="1.73">Dans son hardi projet toutefois il s’obstine,</l>
					<l n="74" num="1.74">Lorsque, des flancs poudreux de la vaste machine,</l>
					<l n="75" num="1.75">L’oiseau sort en courroux, et, d’un cri menaçant,</l>
					<l n="76" num="1.76">Achève d’étonner le barbier frémissant.</l>
					<l n="77" num="1.77">De ses ailes dans l’air secouant la poussière,</l>
					<l n="78" num="1.78">Dans la main de Boirude il éteint la lumière.</l>
					<l n="79" num="1.79">Les guerriers à ce coup demeurent confondus ;</l>
					<l n="80" num="1.80">Ils regagnent la nef, de frayeur éperdus ;</l>
					<l n="81" num="1.81">Sous leurs corps tremblotants leurs genoux s’affaiblissent ;</l>
					<l n="82" num="1.82">D’une subite horreur leurs cheveux se hérissent ;</l>
					<l n="83" num="1.83">Et bientôt, au travers des ombres de la nuit,</l>
					<l n="84" num="1.84">Le timide escadron se dissipe et s’enfuit.</l>
					<l n="85" num="1.85"><space quantity="2" unit="char"></space>Ainsi lorsqu’en un coin, qui leur tient lieu d’asile,</l>
					<l n="86" num="1.86">D’écoliers libertins une troupe indocile,</l>
					<l n="87" num="1.87">Loin des yeux d’un préfet au travail assidu,</l>
					<l n="88" num="1.88">Va tenir quelquefois un brelan défendu,</l>
					<l n="89" num="1.89">Si du veillant Argus la figure effrayante</l>
					<l n="90" num="1.90">Dans l’ardeur du plaisir à leurs yeux se présente,</l>
					<l n="91" num="1.91">Le jeu cesse à l’instant, l’asile est déserté,</l>
					<l n="92" num="1.92">Et tout fuit à grands pas le tyran redouté.</l>
					<l n="93" num="1.93"><space quantity="2" unit="char"></space>La Discorde, qui voit leur honteuse disgrâce,</l>
					<l n="94" num="1.94">Dans les airs cependant tonne, éclate, menace,</l>
					<l n="95" num="1.95">Et, malgré la frayeur dont leurs cœurs sont glacés,</l>
					<l n="96" num="1.96">S’apprête à réunir ses soldats dispersés.</l>
					<l n="97" num="1.97">Aussitôt, de Sidrac elle emprunte l’image :</l>
					<l n="98" num="1.98">Elle ride son front, allonge son visage ;</l>
					<l n="99" num="1.99">Sur un bâton noueux laisse courber son corps,</l>
					<l n="100" num="1.100">Dont la chicane semble animer les ressorts ;</l>
					<l n="101" num="1.101">Prend un cierge en sa main ; et, d’une voix cassée,</l>
					<l n="102" num="1.102">Vient ainsi gourmander la troupe terrassée :</l>
					<l n="103" num="1.103">« Lâches, où fuyez-vous ? Quelle peur vous abat ?</l>
					<l n="104" num="1.104">Aux cris d’un vil oiseau vous cédez sans combat !</l>
					<l n="105" num="1.105">Où sont ces beaux discours jadis si pleins d’audace ?</l>
					<l n="106" num="1.106">Craignez-vous d’un hibou l’impuissante grimace ?</l>
					<l n="107" num="1.107">Que feriez-vous, hélas ! si quelque exploit nouveau</l>
					<l n="108" num="1.108">Chaque jour, comme moi, vous traînait au barreau ;</l>
					<l n="109" num="1.109">S’il fallait, sans amis, briguant une audience,</l>
					<l n="110" num="1.110">D’un magistrat glacé soutenir la présence ;</l>
					<l n="111" num="1.111">Ou, d’un nouveau procès hardi solliciteur,</l>
					<l n="112" num="1.112">Aborder sans argent un clerc de rapporteur ?</l>
					<l n="113" num="1.113">Croyez-moi, mes enfants, je vous parle à bon titre :</l>
					<l n="114" num="1.114">J’ai moi seul autrefois plaidé tout un Chapitre ;</l>
					<l n="115" num="1.115">Et le barreau n’a point de monstres si hagards,</l>
					<l n="116" num="1.116">Dont mon œil n’ait cent fois soutenu les regards.</l>
					<l n="117" num="1.117">Tous les jours sans trembler j’assiégeais leurs passages.</l>
					<l n="118" num="1.118">— L’Église était alors fertile en grands courages !</l>
					<l n="119" num="1.119">— Le moindre d’entre nous, sans argent, sans appui,</l>
					<l n="120" num="1.120">Eût plaidé le prélat et le chantre avec lui.</l>
					<l n="121" num="1.121">Le monde, de qui l’âge avance les ruines,</l>
					<l n="122" num="1.122">Ne peut plus enfanter de ces âmes divines ;</l>
					<l n="123" num="1.123">Mais, que vos cœurs, du moins, imitant leurs vertus,</l>
					<l n="124" num="1.124">De l’aspect d’un hibou ne soient pas abattus.</l>
					<l n="125" num="1.125">Songez, quel déshonneur va souiller votre gloire,</l>
					<l n="126" num="1.126">Quand le chantre demain entendra sa victoire.</l>
					<l n="127" num="1.127">Vous verrez, tous les jours, le chanoine insolent,</l>
					<l n="128" num="1.128">Au seul mot de hibou vous sourire en parlant.</l>
					<l n="129" num="1.129">Votre âme, à ce penser, de colère murmure…</l>
					<l n="130" num="1.130">Allez donc, de ce pas, en prévenir l’injure ;</l>
					<l n="131" num="1.131">Méritez les lauriers qui vous sont réservés ;</l>
					<l n="132" num="1.132">Et ressouvenez-vous quel prélat vous servez.</l>
					<l n="133" num="1.133">Mais déjà la fureur dans vos yeux étincelle :</l>
					<l n="134" num="1.134">Marchez, courez, volez où l’honneur vous appelle ;</l>
					<l n="135" num="1.135">Que le prélat, surpris d’un changement si prompt,</l>
					<l n="136" num="1.136">Apprenne la vengeance aussitôt que l’affront. »</l>
					<l n="137" num="1.137"><space quantity="2" unit="char"></space>En achevant ces mots, la Déesse guerrière</l>
					<l n="138" num="1.138">De son pied trace en l’air un sillon de lumière,</l>
					<l n="139" num="1.139">Rend aux trois champions leur intrépidité,</l>
					<l n="140" num="1.140">Et les laisse tout pleins de sa divinité.</l>
					<l n="141" num="1.141">C’est ainsi, grand Condé, qu’en ce combat célèbre,</l>
					<l n="142" num="1.142">Où ton bras fit trembler le Rhin, l’Escaut, et l’Èbre,</l>
					<l n="143" num="1.143">Lorsqu’aux plaines de Lens nos bataillons poussés</l>
					<l n="144" num="1.144">Furent presque à tes yeux ouverts et renversés,</l>
					<l n="145" num="1.145">Ta valeur, arrêtant les troupes fugitives,</l>
					<l n="146" num="1.146">Rallia d’un regard leurs cohortes craintives,</l>
					<l n="147" num="1.147">Répandit dans leurs rangs ton esprit belliqueux,</l>
					<l n="148" num="1.148">Et força la victoire à te suivre avec eux.</l>
					<l n="149" num="1.149"><space quantity="2" unit="char"></space>La colère à l’instant succédant à la crainte,</l>
					<l n="150" num="1.150">Ils rallument le feu de leur bougie éteinte ;</l>
					<l n="151" num="1.151">Ils rentrent ; l’oiseau sort ; l’escadron raffermi</l>
					<l n="152" num="1.152">Rit du honteux départ d’un si faible ennemi.</l>
					<l n="153" num="1.153">Aussitôt, dans le chœur la machine emportée</l>
					<l n="154" num="1.154">Est sur le banc du chantre à grand bruit remontée.</l>
					<l n="155" num="1.155">Ses ais demi-pourris, que l’âge a relâchés,</l>
					<l n="156" num="1.156">Sont à coups de maillet unis et rapprochés.</l>
					<l n="157" num="1.157">Sous les coups redoublés tous les bancs retentissent ;</l>
					<l n="158" num="1.158">Les murs en sont émus ; les voûtes en mugissent,</l>
					<l n="159" num="1.159">Et l’orgue même en pousse un long gémissement.</l>
					<l n="160" num="1.160"><space quantity="2" unit="char"></space>Que fais-tu, chantre, hélas ! dans ce triste moment ?</l>
					<l n="161" num="1.161">Tu dors d’un profond somme, et ton cœur sans alarmes</l>
					<l n="162" num="1.162">Ne sait pas qu’on bâtit l’instrument de tes larmes !</l>
					<l n="163" num="1.163">Oh ! que si quelque bruit, par un heureux réveil,</l>
					<l n="164" num="1.164">T’annonçait du lutrin le funeste appareil !</l>
					<l n="165" num="1.165">Avant que de souffrir qu’on en posât la masse,</l>
					<l n="166" num="1.166">Tu viendrais en apôtre expirer dans ta place,</l>
					<l n="167" num="1.167">Et, martyr glorieux d’un point d’honneur nouveau,</l>
					<l n="168" num="1.168">Offrir ton corps aux clous et ta tête au marteau.</l>
					<l n="169" num="1.169">Mais déjà sur ton banc la machine enclavée</l>
					<l n="170" num="1.170">Est, durant ton sommeil, à ta honte élevée :</l>
					<l n="171" num="1.171">Le sacristain achève en deux coups de rabot ;</l>
					<l n="172" num="1.172">Et le pupitre enfin tourne sur son pivot.</l>
				</lg>
			</div></body></text></TEI>