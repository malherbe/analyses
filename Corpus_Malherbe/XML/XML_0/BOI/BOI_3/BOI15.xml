<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ÉPITRES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BOI">
					<name>
						<forename>Nicolas</forename>
						<surname>BOILEAU-DESPRÉAUX</surname>
					</name>
					<date from="1636" to="1711">1636-1711</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1708 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BOI_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Œuvres poétiques de Boileau-Despréaux avec une introduction et des notes, par F. Brunetière</title>
						<author>Nicolas Boileau-Despréaux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k97348967</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Œuvres poétiques</title>
								<author>Nicolas Boileau-Despréaux</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Hachette</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1670" to="1698">1670-1698</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La présente édition correspond à une partie du texte d’origine.</p>
				<p>L’ensemble des textes versifiés est réparti en 7 fichiers correspondant chacun à un chapitre.</p>
				<p>La préface ainsi que les autres parties liminaires ne sont pas encodées.</p>
				<p>Les gravures et références des gravures sont pas incluses.</p>
				<p>Les notes de l’éditeur ne sont pas incluses.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (séquence de 3 ou 4 points) ont été remplacés par le signe typographique approprié.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BOI15">
				<head type="main">ÉPÎTRE II</head>
				<head type="sub">A M. L’ABBÉ DES ROCHES</head>
				<opener>
					<dateline>
						<date when="1669">(1669)</date>
					</dateline>
				</opener>
				<lg n="1">
					<l n="1" num="1.1">A quoi bon réveiller mes Muses endormies,</l>
					<l n="2" num="1.2">Pour tracer aux auteurs des règles ennemies ?</l>
					<l n="3" num="1.3">Penses-tu qu’aucun d’eux veuille subir mes lois,</l>
					<l n="4" num="1.4">Ni suivre une raison qui parle par ma voix ?</l>
					<l n="5" num="1.5"><space quantity="2" unit="char"></space>« O le plaisant docteur, qui, sur les pas d’Horace,</l>
					<l n="6" num="1.6">Vient prêcher, diront-ils, la réforme au Parnasse !</l>
					<l n="7" num="1.7">Nos écrits sont mauvais ; les siens valent-ils mieux ? »</l>
					<l n="8" num="1.8">J’entends déjà d’ici Linière furieux</l>
					<l n="9" num="1.9">Qui m’appelle au combat sans prendre un plus long terme.</l>
					<l n="10" num="1.10"><space quantity="2" unit="char"></space>« De l’encre, du papier ! dit-il ; qu’on nous enferme !</l>
					<l n="11" num="1.11">Voyons qui de nous deux, plus aisé dans ses vers,</l>
					<l n="12" num="1.12">Aura plus tôt rempli la page et le revers. »</l>
					<l n="13" num="1.13">Moi donc, qui suis peu fait à ce genre d’escrime,</l>
					<l n="14" num="1.14">Je le laisse tout seul verser rime sur rime,</l>
					<l n="15" num="1.15">Et, souvent, de dépit contre moi s’exerçant,</l>
					<l n="16" num="1.16">Punir de mes défauts le papier innocent.</l>
					<l n="17" num="1.17">Mais toi, qui ne crains point qu’un rimeur te noircisse,</l>
					<l n="18" num="1.18">Que fais-tu cependant seul en ton bénéfice ?</l>
					<l n="19" num="1.19">Attends-tu qu’un fermier, payant, quoiqu’un peu tard,</l>
					<l n="20" num="1.20">De ton bien pour le moins daigne te faire part ?</l>
					<l n="21" num="1.21">Vas-tu, grand défenseur des droits de ton église,</l>
					<l n="22" num="1.22">De tes moines mutins réprimer l’entreprise ?</l>
					<l n="23" num="1.23">Crois-moi, dût Auzanet t’assurer du succès,</l>
					<l n="24" num="1.24">Abbé, n’entreprends point même un juste procès ;</l>
					<l n="25" num="1.25">N’imite point ces fous dont la sotte avarice</l>
					<l n="26" num="1.26">Va de ses revenus engraisser la justice ;</l>
					<l n="27" num="1.27">Qui, toujours assignant, et toujours assignés,</l>
					<l n="28" num="1.28">Souvent demeurent gueux de vingt procès gagnés.</l>
					<l n="29" num="1.29"><space quantity="2" unit="char"></space>« Soutenons bien nos droits : sot est celui qui donne. »</l>
					<l n="30" num="1.30">C’est ainsi devers Caen que tout Normand raisonne ;</l>
					<l n="31" num="1.31">Ce sont là les leçons dont un père manceau</l>
					<l n="32" num="1.32">Instruit son fils novice au sortir du berceau ;</l>
					<l n="33" num="1.33">Mais, pour toi, qui nourri bien en deçà de l’Oise,</l>
					<l n="34" num="1.34">As sucé la vertu picarde et champenoise,</l>
					<l n="35" num="1.35">Non, non, tu n’iras point, ardent bénéficier,</l>
					<l n="36" num="1.36">Faire enrouer pour toi Corbin ni Le Mazier.</l>
					<l n="37" num="1.37">Toutefois, si jamais quelque ardeur bilieuse</l>
					<l n="38" num="1.38">Allumait dans ton cœur l’humeur litigieuse,</l>
					<l n="39" num="1.39">Consulte-moi d’abord, et, pour la réprimer,</l>
					<l n="40" num="1.40">Retiens bien la leçon que je te vais rimer.</l>
					<l n="41" num="1.41"><space quantity="2" unit="char"></space>Un jour, dit un auteur, n’importe en quel chapitre,</l>
					<l n="42" num="1.42">Deux voyageurs à jeun rencontrèrent une huître.</l>
					<l n="43" num="1.43">Tous deux la contestaient, lorsque, dans leur chemin,</l>
					<l n="44" num="1.44">La Justice passa, la balance à la main.</l>
					<l n="45" num="1.45">Devant elle, à grand bruit, ils expliquent la chose ;</l>
					<l n="46" num="1.46">Tous deux, avec dépens, veulent gagner leur cause.</l>
					<l n="47" num="1.47">La Justice, pesant ce droit litigieux,</l>
					<l n="48" num="1.48">Demande l’huître, l’ouvre, et l’avale à leurs yeux,</l>
					<l n="49" num="1.49">Et, par ce bel arrêt terminant la bataille :</l>
					<l n="50" num="1.50">« Tenez ; voilà, dit-elle, à chacun une écaille ;</l>
					<l n="51" num="1.51">Des sottises d’autrui nous vivons au Palais,</l>
					<l n="52" num="1.52">Messieurs ; l’huître était bonne. Adieu. Vivez en paix. »</l>
				</lg>
			</div></body></text></TEI>