<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ÉPITRES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BOI">
					<name>
						<forename>Nicolas</forename>
						<surname>BOILEAU-DESPRÉAUX</surname>
					</name>
					<date from="1636" to="1711">1636-1711</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1708 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BOI_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Œuvres poétiques de Boileau-Despréaux avec une introduction et des notes, par F. Brunetière</title>
						<author>Nicolas Boileau-Despréaux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k97348967</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Œuvres poétiques</title>
								<author>Nicolas Boileau-Despréaux</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Hachette</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1670" to="1698">1670-1698</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La présente édition correspond à une partie du texte d’origine.</p>
				<p>L’ensemble des textes versifiés est réparti en 7 fichiers correspondant chacun à un chapitre.</p>
				<p>La préface ainsi que les autres parties liminaires ne sont pas encodées.</p>
				<p>Les gravures et références des gravures sont pas incluses.</p>
				<p>Les notes de l’éditeur ne sont pas incluses.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (séquence de 3 ou 4 points) ont été remplacés par le signe typographique approprié.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BOI22">
				<head type="main">ÉPÎTRE IX</head>
				<head type="sub">AU MARQUIS DE SEIGNELAY</head>
					<opener>
					<dateline>
						<date when="1675">(1675)</date>
					</dateline>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><space quantity="2" unit="char"></space>Dangereux ennemi de tout mauvais flatteur,</l>
					<l n="2" num="1.2">Seignelay, c’est en vain qu’un ridicule auteur,</l>
					<l n="3" num="1.3">Prêt à porter ton nom de l’Èbre jusqu’au Gange,</l>
					<l n="4" num="1.4">Croit te prendre aux filets d’une sotte louange ;</l>
					<l n="5" num="1.5">Aussitôt ton esprit, prompt à se révolter,</l>
					<l n="6" num="1.6">S’échappe, et rompt le piège où l’on veut l’arrêter.</l>
					<l n="7" num="1.7">Il n’en est pas ainsi de ces esprits frivoles</l>
					<l n="8" num="1.8">Que tout flatteur endort au son de ses paroles ;</l>
					<l n="9" num="1.9">Qui, dans un vain sonnet, placés au rang des dieux,</l>
					<l n="10" num="1.10">Se plaisent à fouler l’Olympe radieux ;</l>
					<l n="11" num="1.11">Et, fiers du haut étage où La Serre les loge,</l>
					<l n="12" num="1.12">Avalent sans dégoût le plus grossier éloge.</l>
					<l n="13" num="1.13">Tu ne te repais point d’encens à si bas prix.</l>
					<l n="14" num="1.14">Non que tu sois pourtant de ces rudes esprits</l>
					<l n="15" num="1.15">Qui regimbent toujours, quelque main qui les flatte :</l>
					<l n="16" num="1.16">Tu souffres la louange adroite et délicate,</l>
					<l n="17" num="1.17">Dont la trop forte odeur n’ébranle point les sens ;</l>
					<l n="18" num="1.18">Mais un auteur novice à répandre l’encens,</l>
					<l n="19" num="1.19">Souvent, à son héros, dans un bizarre ouvrage,</l>
					<l n="20" num="1.20">Donne de l’encensoir au travers du visage,</l>
					<l n="21" num="1.21">Va louer Monterey d’Oudenarde forcé,</l>
					<l n="22" num="1.22">Ou vante aux Électeurs Turenne repoussé.</l>
					<l n="23" num="1.23">Tout éloge imposteur blesse une âme sincère.</l>
					<l n="24" num="1.24">Si, pour faire sa cour à ton illustre père,</l>
					<l n="25" num="1.25">Seignelay, quelque auteur, d’un faux zèle emporté,</l>
					<l n="26" num="1.26">Au lieu de peindre en lui la noble activité,</l>
					<l n="27" num="1.27">La solide vertu, la vaste intelligence,</l>
					<l n="28" num="1.28">Le zèle pour son roi, l’ardeur, la vigilance,</l>
					<l n="29" num="1.29">La constante équité, l’amour pour les beaux-arts,</l>
					<l n="30" num="1.30">Lui donnait les vertus d’Alexandre ou de Mars,.</l>
					<l n="31" num="1.31">Et, pouvant justement l’égaler à Mécène,</l>
					<l n="32" num="1.32">Le comparait au fils de Pélée ou d’Alcmène,</l>
					<l n="33" num="1.33">Ses yeux, d’un tel discours faiblement éblouis,</l>
					<l n="34" num="1.34">Bientôt dans ce tableau reconnaîtraient Louis ;</l>
					<l n="35" num="1.35">Et glaçant d’un regard la Muse et le poète,</l>
					<l n="36" num="1.36">Imposeraient silence à sa verve indiscrète.</l>
					<l n="37" num="1.37"><space quantity="2" unit="char"></space>Un cœur noble est content de ce qu’il trouve en lui,</l>
					<l n="38" num="1.38">Et ne s’applaudit pas des qualités d’autrui.</l>
					<l n="39" num="1.39">Que me sert en effet qu’un admirateur fade</l>
					<l n="40" num="1.40">Vante mon embonpoint, si je me sens malade ?</l>
					<l n="41" num="1.41">Si, dans cet instant même, un feu séditieux</l>
					<l n="42" num="1.42">Fait bouillonner mon sang et pétiller mes yeux ?</l>
					<l n="43" num="1.43">Rien n’est beau que le vrai, le vrai seul est aimable ;</l>
					<l n="44" num="1.44">Il doit régner partout, et même dans la fable ;</l>
					<l n="45" num="1.45">De toute fiction l’adroite fausseté</l>
					<l n="46" num="1.46">Ne tend qu’à faire aux yeux briller la vérité.</l>
					<l n="47" num="1.47"><space quantity="2" unit="char"></space>Sais-tu pourquoi mes vers sont lus dans les provinces,</l>
					<l n="48" num="1.48">Sont recherchés du peuple, et reçus chez les princes ?</l>
					<l n="49" num="1.49">Ce n’est pas que leurs sons, agréables, nombreux,</l>
					<l n="50" num="1.50">Soient toujours à l’oreille également heureux ;</l>
					<l n="51" num="1.51">Qu’en plus d’un lieu le sens n’y gêne la mesure ;</l>
					<l n="52" num="1.52">Et qu’un mot quelquefois n’y brave la césure ;</l>
					<l n="53" num="1.53">Mais c’est qu’en eux le vrai, du mensonge vainqueur,</l>
					<l n="54" num="1.54">Partout se montre aux yeux, et va saisir le cœur ;</l>
					<l n="55" num="1.55">Que le bien et le mal y sont prisés au juste ;</l>
					<l n="56" num="1.56">Que jamais un faquin n’y tint un rang auguste ;</l>
					<l n="57" num="1.57">Et que mon cœur, toujours conduisant mon esprit,</l>
					<l n="58" num="1.58">Ne dit rien aux lecteurs, qu’à soi-même il n’ait dit ;</l>
					<l n="59" num="1.59">Ma pensée au grand jour partout s’offre et s’expose,</l>
					<l n="60" num="1.60">Et mon vers, bien ou mal, dit toujours quelque chose.</l>
					<l n="61" num="1.61">C’est par là quelquefois que ma rime surprend ;</l>
					<l n="62" num="1.62">C’est là ce que n’ont point <hi rend="ital">Jonas</hi>, ni <hi rend="ital">Childebrand</hi>,</l>
					<l n="63" num="1.63">Ni tous ces vains amas de frivoles sornettes,</l>
					<l n="64" num="1.64"><hi rend="ital">Montre, Miroir d’amour, Amitiés, Amourettes</hi>,</l>
					<l n="65" num="1.65">Dont le titre souvent est l’unique soutien,</l>
					<l n="66" num="1.66">Et qui, parlant beaucoup, ne disent jamais rien.</l>
					<l n="67" num="1.67"><space quantity="2" unit="char"></space>Mais, peut-être, enivré des vapeurs de ma Muse,</l>
					<l n="68" num="1.68">Moi-même en ma faveur, Seignelay, je m’abuse…</l>
					<l n="69" num="1.69">Cessons de nous flatter : il n’est esprit si droit</l>
					<l n="70" num="1.70">Qui ne soit imposteur et faux par quelque endroit ;</l>
					<l n="71" num="1.71">Sans cesse on prend le masque, et, quittant la nature,</l>
					<l n="72" num="1.72">On craint de se montrer sous sa propre figure.</l>
					<l n="73" num="1.73">Par là, le plus sincère assez souvent déplaît.</l>
					<l n="74" num="1.74">Rarement, un esprit ose être ce qu’il est.</l>
					<l n="75" num="1.75">Vois-tu cet importun que tout le monde évite ;</l>
					<l n="76" num="1.76">Cet homme à toujours fuir, qui jamais ne vous quitte ?</l>
					<l n="77" num="1.77">Il n’est pas sans esprit ; mais, né triste et pesant,</l>
					<l n="78" num="1.78">Il veut être folâtre, évaporé, plaisant ;</l>
					<l n="79" num="1.79">Il s’est fait de sa joie une loi nécessaire ;</l>
					<l n="80" num="1.80">Et ne déplaît enfin que pour vouloir trop plaire.</l>
					<l n="81" num="1.81">La simplicité plaît sans étude et sans art.</l>
					<l n="82" num="1.82">Tout charme en un enfant, dont la langue sans fard,</l>
					<l n="83" num="1.83">A peine du filet encor débarrassée,</l>
					<l n="84" num="1.84">Sait d’un air innocent bégayer sa pensée.</l>
					<l n="85" num="1.85">Le faux est toujours fade, ennuyeux, languissant.</l>
					<l n="86" num="1.86">Mais la nature est vraie, et d’abord on la sent ;</l>
					<l n="87" num="1.87">C’est elle seule en tout qu’on admire et qu’on aime ;</l>
					<l n="88" num="1.88">Un esprit né chagrin plaît par son chagrin même ;</l>
					<l n="89" num="1.89">Chacun pris dans son air est agréable en soi ;</l>
					<l n="90" num="1.90">Ce n’est que l’air d’autrui qui peut déplaire en moi.</l>
					<l n="91" num="1.91"><space quantity="2" unit="char"></space>Ce marquis était né doux, commode, agréable ;</l>
					<l n="92" num="1.92">On vantait en tous lieux son ignorance aimable ;</l>
					<l n="93" num="1.93">Mais, depuis quelques mois devenu grand docteur,</l>
					<l n="94" num="1.94">Il a pris un faux air, une sotte hauteur ;</l>
					<l n="95" num="1.95">Il ne veut plus parler que de rime et de prose ;</l>
					<l n="96" num="1.96">Des auteurs décriés il prend en main la cause ;</l>
					<l n="97" num="1.97">Il rit du mauvais goût de tant d’hommes divers,</l>
					<l n="98" num="1.98">Et va voir l’opéra, seulement pour les vers.</l>
					<l n="99" num="1.99">Voulant se redresser, soi-même on s’estropie,</l>
					<l n="100" num="1.100">Et d’un original on fait une copie.</l>
					<l n="101" num="1.101">L’ignorance vaut mieux qu’un savoir affecté.</l>
					<l n="102" num="1.102">Rien n’est beau, je reviens, que par la vérité :</l>
					<l n="103" num="1.103">C’est par elle qu’on plaît, et qu’on peut longtemps plaire.</l>
					<l n="104" num="1.104">L’esprit lasse aisément, si le cœur n’est sincère.</l>
					<l n="105" num="1.105">En vain, par sa grimace, un bouffon odieux</l>
					<l n="106" num="1.106">A table nous fait rire, et divertit nos yeux :</l>
					<l n="107" num="1.107">Ses bons mots ont besoin de farine et de plâtre.</l>
					<l n="108" num="1.108">Prenez-le tête à tête, ôtez-lui son théâtre,</l>
					<l n="109" num="1.109">Ce n’est plus qu’un cœur bas, un coquin ténébreux :</l>
					<l n="110" num="1.110">Son visage essuyé n’a plus rien que d’affreux.</l>
					<l n="111" num="1.111">J’aime un esprit aisé qui se montre, qui s’ouvre,</l>
					<l n="112" num="1.112">Et qui plaît d’autant plus, que plus il se découvre.</l>
					<l n="113" num="1.113">Mais, la seule vertu peut souffrir la clarté ;</l>
					<l n="114" num="1.114">Le vice, toujours sombre, aime l’obscurité ;</l>
					<l n="115" num="1.115">Pour paraître au grand jour il faut qu’il se déguise ;</l>
					<l n="116" num="1.116">C’est lui, qui de nos mœurs a banni la franchise.</l>
					<l n="117" num="1.117"><space quantity="2" unit="char"></space>Jadis, l’homme vivait au travail occupé,</l>
					<l n="118" num="1.118">Et, ne trompant jamais, n’était jamais trompé ;</l>
					<l n="119" num="1.119">On ne connaissait point la ruse et l’imposture ;</l>
					<l n="120" num="1.120">Le Normand même alors ignorait le parjure ;</l>
					<l n="121" num="1.121">Aucun rhéteur encore, arrangeant le discours,</l>
					<l n="122" num="1.122">N’avait d’un art menteur enseigné les détours ;</l>
					<l n="123" num="1.123">Mais, sitôt qu’aux humains, faciles à séduire,</l>
					<l n="124" num="1.124">L’abondance eut donné le loisir de se nuire,</l>
					<l n="125" num="1.125">La mollesse amena la fausse vanité ;</l>
					<l n="126" num="1.126">Chacun chercha pour plaire un visage emprunté ;</l>
					<l n="127" num="1.127">Pour éblouir les yeux, la fortune arrogante</l>
					<l n="128" num="1.128">Affecta d’étaler une pompe insolente ;</l>
					<l n="129" num="1.129">L’or éclata partout sur les riches habits ;</l>
					<l n="130" num="1.130">On polit l’émeraude, on tailla le rubis ;</l>
					<l n="131" num="1.131">Et la laine et la soie, en cent façons nouvelles,</l>
					<l n="132" num="1.132">Apprirent à quitter leurs couleurs naturelles.</l>
					<l n="133" num="1.133">La trop courte beauté monta sur des patins ;</l>
					<l n="134" num="1.134">La coquette tendit ses lacs tous les matins ;</l>
					<l n="135" num="1.135">Et, mettant la céruse et le plâtre en usage,</l>
					<l n="136" num="1.136">Composa de sa main les fleurs de son visage.</l>
					<l n="137" num="1.137">L’ardeur de s’enrichir chassa la bonne foi ;</l>
					<l n="138" num="1.138">Le courtisan n’eut plus de sentiments à soi ;</l>
					<l n="139" num="1.139">Tout ne fut plus que fard, qu’erreur, que tromperie ;</l>
					<l n="140" num="1.140">On vit partout régner la basse flatterie.</l>
					<l n="141" num="1.141">Le Parnasse surtout, fécond en imposteurs,</l>
					<l n="142" num="1.142">Diffama le papier par ses propos menteurs :</l>
					<l n="143" num="1.143">De là vint cet amas d’ouvrages mercenaires,</l>
					<l n="144" num="1.144">Stances, odes, sonnets, épîtres liminaires,</l>
					<l n="145" num="1.145">Où toujours le héros passe pour sans pareil,</l>
					<l n="146" num="1.146">Et, fût-il louche ou borgne, est réputé soleil.</l>
					<l n="147" num="1.147"><space quantity="2" unit="char"></space>Ne crois pas toutefois, sur ce discours bizarre,</l>
					<l n="148" num="1.148">Que, d’un frivole encens malignement avare,</l>
					<l n="149" num="1.149">J’en veuille sans raison frustrer tout l’univers.</l>
					<l n="150" num="1.150">La louange agréable est l’âme des beaux vers.</l>
					<l n="151" num="1.151">Mais je tiens, comme toi, qu’il faut qu’elle soit vraie</l>
					<l n="152" num="1.152">Et que son tour adroit n’ait rien qui nous effraie.</l>
					<l n="153" num="1.153">Alors, comme j’ai dit, tu la sais écouter,</l>
					<l n="154" num="1.154">Et sans crainte à tes yeux on pourrait t’exalter.</l>
					<l n="155" num="1.155">Mais, sans t’aller chercher des vertus dans les nues,</l>
					<l n="156" num="1.156">Il faudrait peindre en toi des vérités connues ;</l>
					<l n="157" num="1.157">Décrire ton esprit ami de la raison ;</l>
					<l n="158" num="1.158">Ton ardeur pour ton Roi, puisée en ta maison ;</l>
					<l n="159" num="1.159">A servir ses desseins ta vigilance heureuse ;</l>
					<l n="160" num="1.160">Ta probité sincère, utile, officieuse.</l>
					<l n="161" num="1.161">Tel, qui hait à se voir peint en de faux portraits,</l>
					<l n="162" num="1.162">Sans chagrin voit tracer ses véritables traits :</l>
					<l n="163" num="1.163">Condé même, Condé, ce héros formidable,</l>
					<l n="164" num="1.164">Et, non moins qu’aux Flamands, aux flatteurs redoutable,</l>
					<l n="165" num="1.165">Ne s’offenserait pas si quelque adroit pinceau</l>
					<l n="166" num="1.166">Traçait de ses exploits le fidèle tableau ;</l>
					<l n="167" num="1.167">Et, dans Senef en feu contemplant sa peinture,</l>
					<l n="168" num="1.168">Ne désavouerait pas Malherbe ni Voiture.</l>
					<l n="169" num="1.169">Mais, malheur au poète insipide, odieux,</l>
					<l n="170" num="1.170">Qui viendrait le glacer d’un éloge ennuyeux !</l>
					<l n="171" num="1.171">Il aurait beau crier : « Premier prince du monde !</l>
					<l n="172" num="1.172">« Courage sans pareil ! lumière sans seconde ! »</l>
					<l n="173" num="1.173">Ses vers, jetés d’abord sans tourner le feuillet,</l>
					<l n="174" num="1.174">Iraient dans l’antichambre amuser Pacolet.</l>
				</lg>
			</div></body></text></TEI>