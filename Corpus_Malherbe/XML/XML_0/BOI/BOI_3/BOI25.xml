<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ÉPITRES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BOI">
					<name>
						<forename>Nicolas</forename>
						<surname>BOILEAU-DESPRÉAUX</surname>
					</name>
					<date from="1636" to="1711">1636-1711</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1708 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BOI_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Œuvres poétiques de Boileau-Despréaux avec une introduction et des notes, par F. Brunetière</title>
						<author>Nicolas Boileau-Despréaux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k97348967</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Œuvres poétiques</title>
								<author>Nicolas Boileau-Despréaux</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Hachette</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1670" to="1698">1670-1698</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La présente édition correspond à une partie du texte d’origine.</p>
				<p>L’ensemble des textes versifiés est réparti en 7 fichiers correspondant chacun à un chapitre.</p>
				<p>La préface ainsi que les autres parties liminaires ne sont pas encodées.</p>
				<p>Les gravures et références des gravures sont pas incluses.</p>
				<p>Les notes de l’éditeur ne sont pas incluses.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (séquence de 3 ou 4 points) ont été remplacés par le signe typographique approprié.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BOI25">
				<head type="main">ÉPÎTRE XII</head>
				<head type="sub">SUR L’AMOUR DE DIEU</head>
				<head type="sub_1">A M. L’ABBÉ RENAUDOT</head>
				<opener>
					<dateline>
						<date when="1695">(1695)</date>
					</dateline>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><space quantity="2" unit="char"></space>Docte abbé, tu dis vrai : l’homme au crime attaché,</l>
					<l n="2" num="1.2">En vain, sans aimer Dieu, croit sortir du péché.</l>
					<l n="3" num="1.3">Toutefois, n’en déplaise aux transports frénétiques</l>
					<l n="4" num="1.4">Du fougueux moine auteur des troubles germaniques,</l>
					<l n="5" num="1.5">Des tourments de l’enfer la salutaire peur</l>
					<l n="6" num="1.6">N’est pas toujours l’effet d’une noire vapeur,</l>
					<l n="7" num="1.7">Qui, de remords sans fruit agitant le coupable,</l>
					<l n="8" num="1.8">Aux yeux de Dieu le rende encor plus haïssable.</l>
					<l n="9" num="1.9">Cette utile frayeur, propre à nous pénétrer,</l>
					<l n="10" num="1.10">Vient souvent de la grâce en nous prête d’entrer,</l>
					<l n="11" num="1.11">Qui veut dans notre cœur se rendre la plus forte,</l>
					<l n="12" num="1.12">Et, pour se faire ouvrir, déjà frappe à la porte.</l>
					<l n="13" num="1.13"><space quantity="2" unit="char"></space>Si le pécheur, poussé de ce saint mouvement,</l>
					<l n="14" num="1.14">Reconnaissant son crime, aspire au sacrement,</l>
					<l n="15" num="1.15">Souvent Dieu tout à coup d’un vrai zèle l’enflamme ;</l>
					<l n="16" num="1.16">Le Saint-Esprit revient habiter dans son âme,</l>
					<l n="17" num="1.17">Y convertit enfin les ténèbres en jour,</l>
					<l n="18" num="1.18">Et la crainte servile en filial amour.</l>
					<l n="19" num="1.19">C’est ainsi que souvent la sagesse suprême,</l>
					<l n="20" num="1.20">Pour chasser le démon, se sert du démon même.</l>
					<l n="21" num="1.21"><space quantity="2" unit="char"></space>Mais, lorsqu’en sa malice un pécheur obstiné,</l>
					<l n="22" num="1.22">Des horreurs de l’enfer vainement étonné,</l>
					<l n="23" num="1.23">Loin d’aimer, humble fils, son véritable père,</l>
					<l n="24" num="1.24">Craint et regarde Dieu comme un tyran sévère ;</l>
					<l n="25" num="1.25">Au bien qu’il nous promet ne trouve aucun appas,</l>
					<l n="26" num="1.26">Et souhaite en son cœur que ce Dieu ne soit pas ;</l>
					<l n="27" num="1.27">En vain, la peur sur lui remportant la victoire,</l>
					<l n="28" num="1.28">Aux pieds d’un prêtre il court décharger sa mémoire ;</l>
					<l n="29" num="1.29">Vil esclave, toujours sous le joug du péché,</l>
					<l n="30" num="1.30">Au démon qu’il redoute il demeure attaché.</l>
					<l n="31" num="1.31">L’amour, essentiel à notre pénitence,</l>
					<l n="32" num="1.32">Doit être l’heureux fruit de notre repentance.</l>
					<l n="33" num="1.33">Non ! quoi que l’ignorance enseigne sur ce point,</l>
					<l n="34" num="1.34">Dieu ne fait jamais grâce à qui ne l’aime point.</l>
					<l n="35" num="1.35">A le chercher la peur nous dispose et nous aide,</l>
					<l n="36" num="1.36">Mais il ne vient jamais, que l’amour ne succède.</l>
					<l n="37" num="1.37">Cessez de m’opposer vos discours imposteurs,</l>
					<l n="38" num="1.38">Confesseurs insensés, ignorants séducteurs,</l>
					<l n="39" num="1.39">Qui, pleins des vains propos que l’erreur vous débite,</l>
					<l n="40" num="1.40">Vous figurez qu’en vous un pouvoir sans limite</l>
					<l n="41" num="1.41">Justifie à coup sûr tout pécheur alarmé ;</l>
					<l n="42" num="1.42">Et que, sans aimer Dieu l’on peut en être aimé.</l>
					<l n="43" num="1.43"><space quantity="2" unit="char"></space>Quoi donc ! cher Renaudot, un chrétien effroyable,</l>
					<l n="44" num="1.44">Qui jamais, servant Dieu, n’eut d’objet que le diable,</l>
					<l n="45" num="1.45">Pourra, marchant toujours dans des sentiers maudits,</l>
					<l n="46" num="1.46">Par des formalités gagner le paradis !</l>
					<l n="47" num="1.47">Et, parmi les élus, dans la gloire éternelle,</l>
					<l n="48" num="1.48">Pour quelques sacrements reçus sans aucun zèle,</l>
					<l n="49" num="1.49">Dieu fera voir aux yeux des saints épouvantés</l>
					<l n="50" num="1.50">Son ennemi mortel assis à ses côtés !</l>
					<l n="51" num="1.51">Peut-on se figurer de si folles chimères ?</l>
					<l n="52" num="1.52">On voit pourtant, on voit des docteurs, même austères,</l>
					<l n="53" num="1.53">Qui, les semant partout, s’en vont pieusement</l>
					<l n="54" num="1.54">De toute piété saper le fondement ;</l>
					<l n="55" num="1.55">Qui, le cœur infecté d’erreurs si criminelles,</l>
					<l n="56" num="1.56">Se disent hautement les purs, les vrais fidèles ;</l>
					<l n="57" num="1.57">Traitant d’abord d’impie et d’hérétique affreux</l>
					<l n="58" num="1.58">Quiconque ose pour Dieu se déclarer contre eux.</l>
					<l n="59" num="1.59">De leur audace en vain les vrais chrétiens gémissent :</l>
					<l n="60" num="1.60">Prêts à la repousser, les plus hardis mollissent,</l>
					<l n="61" num="1.61">Et, voyant contre Dieu le diable accrédité,</l>
					<l n="62" num="1.62">N’osent qu’en bégayant prêcher la vérité.</l>
					<l n="63" num="1.63">Mollirons-nous aussi ? Non ; sans peur, sur ta trace,</l>
					<l n="64" num="1.64">Docte abbé, de ce pas j’irai leur dire en face :</l>
					<l n="65" num="1.65">« Ouvrez les yeux enfin, aveugles dangereux.</l>
					<l n="66" num="1.66">Oui, je vous le soutiens, il serait moins affreux</l>
					<l n="67" num="1.67">De ne point reconnaître un Dieu maître du monde,</l>
					<l n="68" num="1.68">Et qui règle à son gré le ciel, la terre et l’onde,</l>
					<l n="69" num="1.69">Qu’en avouant qu’il est, et qu’il sut tout former,</l>
					<l n="70" num="1.70">D’oser dire, qu’on peut lui plaire sans l’aimer.</l>
					<l n="71" num="1.71">Un si bas, si honteux, si faux christianisme</l>
					<l n="72" num="1.72">Ne vaut pas des Platons l’éclairé paganisme,</l>
					<l n="73" num="1.73">Et chérir les vrais biens, sans en savoir l’auteur,</l>
					<l n="74" num="1.74">Vaut mieux que, sans l’aimer, connaître un créateur. »</l>
					<l n="75" num="1.75">Expliquons-nous pourtant. Par cette ardeur si sainte,</l>
					<l n="76" num="1.76">Que je veux qu’en un cœur amène enfin la crainte,</l>
					<l n="77" num="1.77">Je n’entends pas ici ce doux saisissement,</l>
					<l n="78" num="1.78">Ces transports pleins de joie et de ravissement,</l>
					<l n="79" num="1.79">Qui font des bienheureux la juste récompense ;</l>
					<l n="80" num="1.80">Et qu’un cœur rarement goûte ici par avance.</l>
					<l n="81" num="1.81">Dans nous l’amour de Dieu, fécond en saints désirs,</l>
					<l n="82" num="1.82">N’y produit pas toujours de sensibles plaisirs ;</l>
					<l n="83" num="1.83">Souvent, le cœur qui l’a ne le sait pas lui-même ;</l>
					<l n="84" num="1.84">Tel craint de n’aimer pas, qui sincèrement aime ;</l>
					<l n="85" num="1.85">Et, tel croit au contraire être brûlant d’ardeur,</l>
					<l n="86" num="1.86">Qui n’eut jamais pour Dieu que glace et que froideur.</l>
					<l n="87" num="1.87">C’est ainsi quelquefois qu’un indolent mystique,</l>
					<l n="88" num="1.88">Au milieu des péchés tranquille fanatique,</l>
					<l n="89" num="1.89">Du plus parfait amour pense avoir l’heureux don,</l>
					<l n="90" num="1.90">Et croit posséder Dieu dans les bras du démon.</l>
					<l n="91" num="1.91"><space quantity="2" unit="char"></space>Voulez-vous donc savoir si la foi dans votre âme</l>
					<l n="92" num="1.92">Allume les ardeurs d’une sincère flamme ?</l>
					<l n="93" num="1.93">Consultez-vous vous-même : à ses règles soumis,</l>
					<l n="94" num="1.94">Pardonnez-vous sans peine à tous vos ennemis ?</l>
					<l n="95" num="1.95">Combattez-vous vos sens ? domptez-vous vos faiblesses ?</l>
					<l n="96" num="1.96">Dieu dans le pauvre est-il l’objet de vos largesses ?</l>
					<l n="97" num="1.97">Enfin dans tous ses points pratiquez-vous sa loi ?</l>
					<l n="98" num="1.98">Oui, dites-vous. Allez, vous l’aimez, croyez-moi.</l>
					<l n="99" num="1.99">« Qui fait exactement ce que ma loi commande,</l>
					<l n="100" num="1.100">A pour moi, dit ce Dieu, l’amour que je demande. »</l>
					<l n="101" num="1.101">Faites-le donc ; et, sûr qu’il nous veut sauver tous,</l>
					<l n="102" num="1.102">Ne vous alarmez point pour quelques vains dégoûts</l>
					<l n="103" num="1.103">Qu’en sa ferveur souvent la plus sainte âme éprouve ;</l>
					<l n="104" num="1.104">« Marchez, courez à lui : qui le cherche le trouve ; »</l>
					<l n="105" num="1.105">Et, plus de votre cœur il paraît s’écarter,</l>
					<l n="106" num="1.106">Plus par vos actions songez à l’arrêter.</l>
					<l n="107" num="1.107">Mais, ne soutenez point cet horrible blasphème,</l>
					<l n="108" num="1.108">Qu’un sacrement reçu, qu’un prêtre, que Dieu même,</l>
					<l n="109" num="1.109">Quoi que vos faux docteurs osent vous avancer,</l>
					<l n="110" num="1.110">De l’amour qu’on lui doit puissent vous dispenser.</l>
					<l n="111" num="1.111"><space quantity="2" unit="char"></space>« Mais s’il faut qu’avant tout, dans une âme chrétienne,</l>
					<l n="112" num="1.112">Diront ces grands docteurs, l’amour de Dieu survienne,</l>
					<l n="113" num="1.113">Puisque ce seul amour suffit pour nous sauver,</l>
					<l n="114" num="1.114">De quoi le sacrement viendra-t-il nous laver ?</l>
					<l n="115" num="1.115">Sa vertu n’est donc plus qu’une vertu frivole. »</l>
					<l n="116" num="1.116">Oh ! le bel argument, digne de leur école !</l>
					<l n="117" num="1.117">Quoi ! dans l’amour divin en nos cœurs allumé,</l>
					<l n="118" num="1.118">Le vœu du sacrement n’est-il pas renfermé ?</l>
					<l n="119" num="1.119">Un païen converti, qui croit un Dieu suprême,</l>
					<l n="120" num="1.120">Peut-il être chrétien qu’il n’aspire au baptême ?</l>
					<l n="121" num="1.121">Ni le chrétien en pleurs être vraiment touché</l>
					<l n="122" num="1.122">Qu’il ne veuille à l’église avouer son péché ?</l>
					<l n="123" num="1.123">Du funeste esclavage où le démon nous traîne</l>
					<l n="124" num="1.124">C’est le sacrement seul qui peut rompre la chaîne :</l>
					<l n="125" num="1.125">Aussi l’amour d’abord y court avidement ;</l>
					<l n="126" num="1.126">Mais lui-même il en est l’âme et le fondement.</l>
					<l n="127" num="1.127">Lorsqu’un pécheur, ému d’une humble repentance,</l>
					<l n="128" num="1.128">Par les degrés prescrits court à la pénitence,</l>
					<l n="129" num="1.129">S’il n’y peut parvenir, Dieu sait les supposer.</l>
					<l n="130" num="1.130">Le seul amour manquant ne peut point s’excuser.</l>
					<l n="131" num="1.131">C’est par lui que dans nous la grâce fructifie ;</l>
					<l n="132" num="1.132">C’est lui, qui nous ranime et qui nous vivifie ;</l>
					<l n="133" num="1.133">Pour nous rejoindre à Dieu, lui seul est le lien ;</l>
					<l n="134" num="1.134">Et, sans lui, foi, vertus, sacrements, tout n’est rien.</l>
					<l n="135" num="1.135"><space quantity="2" unit="char"></space>A ces discours pressants que saurait-on répondre ?</l>
					<l n="136" num="1.136">Mais, approchez ; je veux encor mieux vous confondre,</l>
					<l n="137" num="1.137">Docteurs. Dites-moi donc : quand nous sommes absous,</l>
					<l n="138" num="1.138">Le Saint-Esprit est-il, ou n’est-il pas en nous ?</l>
					<l n="139" num="1.139">S’il est en nous, peut-il, n’étant qu’amour lui-même,</l>
					<l n="140" num="1.140">Ne nous échauffer point de son amour suprême ?</l>
					<l n="141" num="1.141">Et s’il n’est pas en nous, Satan, toujours vainqueur,</l>
					<l n="142" num="1.142">Ne demeure-t-il pas maître de notre cœur ?</l>
					<l n="143" num="1.143">Avouez donc qu’il faut qu’en nous l’amour renaisse ;</l>
					<l n="144" num="1.144">Et n’allez point, pour fuir la raison qui vous presse,</l>
					<l n="145" num="1.145">Donner le nom d’amour au trouble inanimé</l>
					<l n="146" num="1.146">Qu’au cœur d’un criminel la peur seule a formé.</l>
					<l n="147" num="1.147">L’ardeur qui justifie, et que Dieu nous envoie,</l>
					<l n="148" num="1.148">Quoiqu’ici bas souvent inquiète et sans joie,</l>
					<l n="149" num="1.149">Est pourtant cette ardeur, ce même feu d’amour,</l>
					<l n="150" num="1.150">Dont brûle un bienheureux en l’éternel séjour.</l>
					<l n="151" num="1.151">Dans le fatal instant qui borne notre vie,</l>
					<l n="152" num="1.152">Il faut que de ce feu notre âme soit remplie ;</l>
					<l n="153" num="1.153">Et Dieu, sourd à nos cris s’il ne l’y trouve pas,</l>
					<l n="154" num="1.154">Ne l’y rallume plus après notre trépas.</l>
					<l n="155" num="1.155">Rendez-vous donc enfin à ces clairs syllogismes ;</l>
					<l n="156" num="1.156">Et ne prétendez plus, par vos confus sophismes,</l>
					<l n="157" num="1.157">Pouvoir encore aux veux du fidèle éclairé</l>
					<l n="158" num="1.158">Cacher l’amour de Dieu dans l’école égaré.</l>
					<l n="159" num="1.159">Apprenez que la gloire où le ciel nous appelle</l>
					<l n="160" num="1.160">Un jour des vrais enfants doit couronner le zèle,</l>
					<l n="161" num="1.161">Et non les froids remords d’un esclave craintif,</l>
					<l n="162" num="1.162">Où crut voir Abéli quelque amour négatif.</l>
					<l n="163" num="1.163"><space quantity="2" unit="char"></space>Mais quoi ! j’entends déjà plus d’un fier scolastique</l>
					<l n="164" num="1.164">Qui, me voyant ici sur ce ton dogmatique</l>
					<l n="165" num="1.165">En vers audacieux traiter ces points sacrés,</l>
					<l n="166" num="1.166">Curieux, me demande où j’ai pris mes degrés ;</l>
					<l n="167" num="1.167">Et si, pour m’éclairer sur ces sombres matières,</l>
					<l n="168" num="1.168">Deux cents auteurs extraits m’ont prêté leurs lumières.</l>
					<l n="169" num="1.169">Non. Mais, pour décider que l’homme, qu’un chrétien</l>
					<l n="170" num="1.170">Est obligé d’aimer l’unique auteur du bien,</l>
					<l n="171" num="1.171">Le Dieu qui le nourrit, le Dieu qui le fit naître,</l>
					<l n="172" num="1.172">Qui nous vint par sa mort donner un second être,</l>
					<l n="173" num="1.173">Faut-il avoir reçu le bonnet doctoral,</l>
					<l n="174" num="1.174">Avoir extrait Gamache, Isambert et du Val ?</l>
					<l n="175" num="1.175">Dieu, dans son Livre saint, sans chercher d’autre ouvrage,</l>
					<l n="176" num="1.176">Ne l’a-t-il pas écrit lui-même à chaque page ?</l>
					<l n="177" num="1.177">De vains docteurs encore, ô prodige honteux,</l>
					<l n="178" num="1.178">Oseront nous en faire un problème douteux !</l>
					<l n="179" num="1.179">Viendront traiter d’erreur digne de l’anathème</l>
					<l n="180" num="1.180">L’indispensable loi d’aimer Dieu pour lui-même,</l>
					<l n="181" num="1.181">Et, par un dogme faux dans nos jours enfanté,</l>
					<l n="182" num="1.182">Des devoirs du chrétien rayer la charité !</l>
					<l n="183" num="1.183"><space quantity="2" unit="char"></space>Si j’allais consulter chez eux le moins sévère,</l>
					<l n="184" num="1.184">Et lui disais : « Un fils doit-il aimer son père ?</l>
					<l n="185" num="1.185">— Ah ! peut-on en douter ? » dirait-il brusquement ;</l>
					<l n="186" num="1.186">Et quand je leur demande en ce même moment :</l>
					<l n="187" num="1.187">« L’homme, ouvrage d’un Dieu seul bon et seul aimable,</l>
					<l n="188" num="1.188">Doit-il aimer ce Dieu, son père véritable ? »</l>
					<l n="189" num="1.189">Leur plus rigide auteur n’ose le décider,</l>
					<l n="190" num="1.190">Et craint, en l’affirmant, de se trop hasarder !</l>
					<l n="191" num="1.191"><space quantity="2" unit="char"></space>Je ne m’en puis défendre ; il faut que je t’écrive</l>
					<l n="192" num="1.192">La figure bizarre, et pourtant assez vive,</l>
					<l n="193" num="1.193">Que je sus l’autre jour employer dans son lieu,</l>
					<l n="194" num="1.194">Et qui déconcerta ces ennemis de Dieu.</l>
					<l n="195" num="1.195">Au sujet d’un écrit qu’on nous venait de lire,</l>
					<l n="196" num="1.196">Un d’entre eux m’insulta, sur ce que j’osai dire</l>
					<l n="197" num="1.197">Qu’il faut, pour être absous d’un crime confessé,</l>
					<l n="198" num="1.198">Avoir pour Dieu du moins un amour commencé.</l>
					<l n="199" num="1.199">« Ce dogme, me dit-il, est un pur calvinisme. »</l>
					<l n="200" num="1.200">O ciel ! me voilà donc dans l’erreur, dans le schisme,</l>
					<l n="201" num="1.201">Et partant réprouvé ! « Mais, poursuivis-je alors,</l>
					<l n="202" num="1.202">Quand Dieu viendra juger les vivants et les morts,</l>
					<l n="203" num="1.203">Et, des humbles agneaux, objets de sa tendresse,</l>
					<l n="204" num="1.204">Séparera des boucs la troupe pécheresse,</l>
					<l n="205" num="1.205">A tous il nous dira, sévère ou gracieux,</l>
					<l n="206" num="1.206">Ce qui nous fit impurs ou justes à ses yeux.</l>
					<l n="207" num="1.207">Selon vous donc, à moi réprouvé, bouc infâme,</l>
					<l n="208" num="1.208">« Va brûler, dira-t-il en l’éternelle flamme,</l>
					<l n="209" num="1.209">« Malheureux qui soutins que l’homme dût m’aimer ;</l>
					<l n="210" num="1.210">« Et qui, sur ce sujet trop prompt à déclamer,</l>
					<l n="211" num="1.211">« Prétendis qu’il fallait, pour fléchir ma justice,</l>
					<l n="212" num="1.212">« Que le pécheur, touché de l’horreur de son vice,</l>
					<l n="213" num="1.213">« De quelque ardeur pour moi sentît les mouvements, «</l>
					<l n="214" num="1.214">Et gardât le premier de mes commandements ! »</l>
					<l n="215" num="1.215">Dieu, si je vous en crois, me tiendra ce langage.</l>
					<l n="216" num="1.216">Mais, à vous, tendre agneau, son plus cher héritage,</l>
					<l n="217" num="1.217">Orthodoxe ennemi d’un dogme si blâmé,</l>
					<l n="218" num="1.218">« Venez, vous dira-t-il, venez, mon bien-aimé ;</l>
					<l n="219" num="1.219">« Vous qui, dans les détours de vos raisons subtiles «</l>
					<l n="220" num="1.220">Embarrassant les mots d’un des plus saints conciles,</l>
					<l n="221" num="1.221">« Avez délivré l’homme, ô l’utile docteur !</l>
					<l n="222" num="1.222">« De l’importun fardeau d’aimer son Créateur ;</l>
					<l n="223" num="1.223">« Entrez au ciel, venez, comblé de mes louanges,</l>
					<l n="224" num="1.224">« Du besoin d’aimer Dieu désabuser les anges. »</l>
					<l n="225" num="1.225">A de tels mots, si Dieu pouvait les prononcer,</l>
					<l n="226" num="1.226">Pour moi je répondrais, je crois, sans l’offenser :</l>
					<l n="227" num="1.227">« Oh ! que pour vous mon cœur moins dur et moins farouche,</l>
					<l n="228" num="1.228">Seigneur, n’a-t-il, hélas ! parlé comme ma bouche ! »</l>
					<l n="229" num="1.229">Ce serait ma réponse à ce Dieu fulminant.</l>
					<l n="230" num="1.230">Mais, vous, de ses douceurs objet fort surprenant,</l>
					<l n="231" num="1.231">Je ne sais pas comment, ferme en votre doctrine,</l>
					<l n="232" num="1.232">Des ironiques mots de sa bouche divine</l>
					<l n="233" num="1.233">Vous pourriez, sans rougeur et sans confusion,</l>
					<l n="234" num="1.234">Soutenir l’amertume et la dérision. »</l>
					<l n="235" num="1.235"><space quantity="2" unit="char"></space>L’audace du docteur, par ce discours frappée,</l>
					<l n="236" num="1.236">Demeura sans réplique à ma prosopopée.</l>
					<l n="237" num="1.237">Il sortit tout à coup, et, murmurant tout bas</l>
					<l n="238" num="1.238">Quelques termes d’aigreur que je n’entendis pas,</l>
					<l n="239" num="1.239">S’en alla chez Binsfeld, ou chez Basile Ponce,</l>
					<l n="240" num="1.240">Sur l’heure à mes raisons chercher une réponse.</l>
				</lg>
			</div></body></text></TEI>