<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">SATIRES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BOI">
					<name>
						<forename>Nicolas</forename>
						<surname>BOILEAU-DESPRÉAUX</surname>
					</name>
					<date from="1636" to="1711">1636-1711</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2918 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BOI_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Œuvres poétiques de Boileau-Despréaux avec une introduction et des notes, par F. Brunetière</title>
						<author>Nicolas Boileau-Despréaux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k97348967</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Œuvres poétiques</title>
								<author>Nicolas Boileau-Despréaux</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Hachette</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1666" to="1716">1666-1716</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La présente édition correspond à une partie du texte d’origine.</p>
				<p>L’ensemble des textes versifiés est réparti en 7 fichiers correspondant chacun à un chapitre.</p>
				<p>La préface ainsi que les autres parties liminaires ne sont pas encodées.</p>
				<p>Les gravures et références des gravures sont pas incluses.</p>
				<p>Les notes de l’éditeur ne sont pas incluses.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (séquence de 3 ou 4 points) ont été remplacés par le signe typographique approprié.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BOI8">
				<head type="main">SATIRE VII</head>
				<opener>
					<dateline>
						<date when="1663">(1663)</date>
					</dateline>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><space quantity="2" unit="char"></space>Muse, changeons de style, et quittons la satire :</l>
					<l n="2" num="1.2">C’est un méchant métier que celui de médire,</l>
					<l n="3" num="1.3">A l’auteur qui l’embrasse il est toujours fatal,</l>
					<l n="4" num="1.4">Le mal qu’on dit d’autrui ne produit que du mal ;</l>
					<l n="5" num="1.5">Maint poète, aveuglé d’une telle manie,</l>
					<l n="6" num="1.6">En courant à l’honneur, trouve l’ignominie ;</l>
					<l n="7" num="1.7">Et tel mot, pour avoir réjoui le lecteur,</l>
					<l n="8" num="1.8">A coûté bien souvent des larmes à l’auteur.</l>
					<l n="9" num="1.9"><space quantity="2" unit="char"></space>Un éloge ennuyeux, un froid panégyrique,</l>
					<l n="10" num="1.10">Peut pourrir à son aise au fond d’une boutique,</l>
					<l n="11" num="1.11">Ne craint point du public les jugements divers,</l>
					<l n="12" num="1.12">Et n’a pour ennemis que la poudre et les vers :</l>
					<l n="13" num="1.13">Mais, un auteur malin, qui rit et qui fait rire,</l>
					<l n="14" num="1.14">Qu’on blâme en le lisant, et pourtant qu’on veut lire,</l>
					<l n="15" num="1.15">Dans ses plaisants accès qui se croit tout permis,</l>
					<l n="16" num="1.16">De ses propres rieurs se fait des ennemis.</l>
					<l n="17" num="1.17">Un discours trop sincère aisément nous outrage :</l>
					<l n="18" num="1.18">Chacun dans ce miroir pense voir son visage,</l>
					<l n="19" num="1.19">Et tel, en vous lisant, admire chaque trait,</l>
					<l n="20" num="1.20">Qui, dans le fond de l’âme, et vous craint et vous hait.</l>
					<l n="21" num="1.21"><space quantity="2" unit="char"></space>Muse, c’est donc en vain que la main vous démange,</l>
					<l n="22" num="1.22">S’il faut rimer ici, rimons quelque louange,</l>
					<l n="23" num="1.23">Et cherchons un héros, parmi cet univers,</l>
					<l n="24" num="1.24">Digne de notre encens et digne de nos vers.</l>
					<l n="25" num="1.25">Mais, à ce grand effort en vain je vous anime,</l>
					<l n="26" num="1.26">Je ne puis pour louer rencontrer une rime.</l>
					<l n="27" num="1.27">Dès que j’y veux rêver, ma veine est aux abois.</l>
					<l n="28" num="1.28">J’ai beau frotter mon front, j’ai beau mordre mes doigts,</l>
					<l n="29" num="1.29">Je ne puis arracher du creux de ma cervelle</l>
					<l n="30" num="1.30">Que des vers plus forcés que ceux de <hi rend="ital">la Pucelle</hi> ;</l>
					<l n="31" num="1.31">Je pense être à la gêne ; et, pour un tel dessein,</l>
					<l n="32" num="1.32">La plume et le papier résistent à ma main.</l>
					<l n="33" num="1.33">Mais, quand il faut railler, j’ai ce que je souhaite.</l>
					<l n="34" num="1.34">Alors, certes, alors je me connais poète,</l>
					<l n="35" num="1.35">Phébus, dès que je parle, est prêt à m’exaucer,</l>
					<l n="36" num="1.36">Mes mots viennent sans peine, et courent se placer.</l>
					<l n="37" num="1.37">Faut-il peindre un fripon fameux dans cette ville ?</l>
					<l n="38" num="1.38">Ma main, sans que j’y rêve, écrira Raumaville.</l>
					<l n="39" num="1.39">Faut-il d’un sot parfait montrer l’original ?</l>
					<l n="40" num="1.40">Ma plume au bout du vers d’abord trouve Sofal.</l>
					<l n="41" num="1.41">Je sens que mon esprit travaille de génie.</l>
					<l n="42" num="1.42">Faut-il d’un froid rimeur dépeindre la manie ?</l>
					<l n="43" num="1.43">Mes vers, comme un torrent, coulent sur le papier :</l>
					<l n="44" num="1.44">Je rencontre à la fois Perrin et Pelletier,</l>
					<l n="45" num="1.45">Bonnecorse, Pradon, Colletet, Titreville ;</l>
					<l n="46" num="1.46">Et, pour un que je veux, j’en trouve plus de mille.</l>
					<l n="47" num="1.47">Aussitôt je triomphe ; et ma Muse en secret</l>
					<l n="48" num="1.48">S’estime et s’applaudit du beau coup qu’elle a fait.</l>
					<l n="49" num="1.49">C’est en vain qu’au milieu de ma fureur extrême</l>
					<l n="50" num="1.50">Je me fais quelquefois des leçons à moi-même ;</l>
					<l n="51" num="1.51">En vain je veux au moins faire grâce à quelqu’un ;</l>
					<l n="52" num="1.52">Ma plume aurait regret d’en épargner aucun,</l>
					<l n="53" num="1.53">Et, sitôt qu’une fois la verve me domine,</l>
					<l n="54" num="1.54">Tout ce qui s’offre à moi passe par l’étamine.</l>
					<l n="55" num="1.55">Le mérite pourtant m’est toujours précieux.</l>
					<l n="56" num="1.56">Mais, tout fat me déplaît, et me blesse les yeux,</l>
					<l n="57" num="1.57">Je le poursuis partout, comme un chien fait sa proie,</l>
					<l n="58" num="1.58">Et ne le sens jamais qu’aussitôt je n’aboie.</l>
					<l n="59" num="1.59">Enfin, sans perdre temps en de si vains propos,</l>
					<l n="60" num="1.60">Je sais coudre une rime au bout de quelques mots.</l>
					<l n="61" num="1.61">Souvent j’habille en vers une maligne prose :</l>
					<l n="62" num="1.62">C’est par là que je vaux, si je vaux quelque chose.</l>
					<l n="63" num="1.63">Ainsi, soit que bientôt, par une dure loi,</l>
					<l n="64" num="1.64">La mort, d’un vol affreux, vienne fondre sur moi,</l>
					<l n="65" num="1.65">Soit que le ciel me garde un cours long et tranquille,</l>
					<l n="66" num="1.66">A Rome ou dans Paris, aux champs ou dans la ville,</l>
					<l n="67" num="1.67">Dût ma Muse par là choquer tout l’univers,</l>
					<l n="68" num="1.68">Riche, gueux, triste, ou gai, je veux faire des vers.</l>
					<l n="69" num="1.69"><space quantity="2" unit="char"></space>« Pauvre esprit, dira-t-on, que je plains ta folie !</l>
					<l n="70" num="1.70">Modère ces bouillons de ta mélancolie,</l>
					<l n="71" num="1.71">Et garde, qu’un de ceux que tu penses blâmer</l>
					<l n="72" num="1.72">N’éteigne dans ton sang cette ardeur de rimer. »</l>
					<l n="73" num="1.73">Eh quoi ! lorsqu’autrefois Horace, après Lucile,</l>
					<l n="74" num="1.74">Exhalait en bons mots les vapeurs de sa bile,</l>
					<l n="75" num="1.75">Et, vengeant la vertu par des traits éclatants,</l>
					<l n="76" num="1.76">Allait ôter le masque aux vices de son temps ;</l>
					<l n="77" num="1.77">Ou bien, quand Juvénal, de sa mordante plume</l>
					<l n="78" num="1.78">Faisant couler des flots de fiel et d’amertume,</l>
					<l n="79" num="1.79">Gourmandait en courroux tout le peuple latin,</l>
					<l n="80" num="1.80">L’un ou l’autre fit-il une tragique fin ?</l>
					<l n="81" num="1.81">Et que craindre, après tout, d’une fureur si vaine ?</l>
					<l n="82" num="1.82">Personne ne connaît ni mon nom ni ma veine ;</l>
					<l n="83" num="1.83">On ne voit point mes vers, à l’envi de Montreuil,</l>
					<l n="84" num="1.84">Grossir impunément les feuillets d’un recueil ;</l>
					<l n="85" num="1.85">A peine, quelquefois, je me force à les lire,</l>
					<l n="86" num="1.86">Pour plaire à quelque ami que charme la satire,</l>
					<l n="87" num="1.87">Qui me flatte peut-être, et, d’un air imposteur,</l>
					<l n="88" num="1.88">Rit tout haut de l’ouvrage, et tout bas de l’auteur.</l>
					<l n="89" num="1.89">Enfin c’est mon plaisir, je veux me satisfaire.</l>
					<l n="90" num="1.90">Je ne puis bien parler, et ne saurais me taire,</l>
					<l n="91" num="1.91">Et, dès qu’un mot plaisant vient luire à mon esprit,</l>
					<l n="92" num="1.92">Je n’ai point de repos qu’il ne soit en écrit.</l>
					<l n="93" num="1.93">Je ne résiste point au torrent qui m’entraîne</l>
					<l n="94" num="1.94"><space quantity="2" unit="char"></space>Mais, c’est assez parlé, prenons un peu d’haleine ;</l>
					<l n="95" num="1.95">Ma main, pour cette fois, commence à se lasser ;</l>
					<l n="96" num="1.96">Finissons. Mais, demain, Muse, à recommencer.</l>
				</lg>
			</div></body></text></TEI>