<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">SATIRES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BOI">
					<name>
						<forename>Nicolas</forename>
						<surname>BOILEAU-DESPRÉAUX</surname>
					</name>
					<date from="1636" to="1711">1636-1711</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2918 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BOI_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Œuvres poétiques de Boileau-Despréaux avec une introduction et des notes, par F. Brunetière</title>
						<author>Nicolas Boileau-Despréaux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k97348967</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Œuvres poétiques</title>
								<author>Nicolas Boileau-Despréaux</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Hachette</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1666" to="1716">1666-1716</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La présente édition correspond à une partie du texte d’origine.</p>
				<p>L’ensemble des textes versifiés est réparti en 7 fichiers correspondant chacun à un chapitre.</p>
				<p>La préface ainsi que les autres parties liminaires ne sont pas encodées.</p>
				<p>Les gravures et références des gravures sont pas incluses.</p>
				<p>Les notes de l’éditeur ne sont pas incluses.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (séquence de 3 ou 4 points) ont été remplacés par le signe typographique approprié.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BOI4">
				<head type="main">SATIRE III</head>
				<opener>
					<dateline>
						<date when="1665">(1665)</date>
					</dateline>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><space quantity="2" unit="char"></space><del reason="analysis" type="irrelevant" hand="RR">A</del>. — Quel sujet inconnu vous trouble et vous altère,</l>
					<l n="2" num="1.2">D’où vous vient aujourd’hui cet air sombre et sévère,</l>
					<l n="3" num="1.3">Et ce visage enfin plus pâle qu’un rentier</l>
					<l n="4" num="1.4">A l’aspect d’un arrêt qui retranche un quartier ?</l>
					<l n="5" num="1.5">Qu’est devenu ce teint, dont la couleur fleurie</l>
					<l n="6" num="1.6">Semblait d’ortolans seuls et de bisques nourrie,</l>
					<l n="7" num="1.7">Où la joie en son lustre attirait les regards,</l>
					<l n="8" num="1.8">Et le vin en rubis brillait de toutes parts ?</l>
					<l n="9" num="1.9">Qui vous a pu plonger dans cette humeur chagrine ?</l>
					<l n="10" num="1.10">A-t-on par quelque édit réformé la cuisine ?</l>
					<l n="11" num="1.11">Ou quelque longue pluie, inondant vos vallons,</l>
					<l n="12" num="1.12">A-t-elle fait couler vos vins et vos melons ?</l>
					<l n="13" num="1.13">Répondez donc, enfin, ou bien je me retire.</l>
					<l n="14" num="1.14"><space quantity="2" unit="char"></space><del reason="analysis" type="irrelevant" hand="RR">P</del>. — Ah ! de grâce, un moment, souffrez que je respire.</l>
					<l n="15" num="1.15">Je sors de chez un fat, qui, pour m’empoisonner,</l>
					<l n="16" num="1.16">Je pense, exprès chez lui m’a forcé de dîner.</l>
					<l n="17" num="1.17">Je l’avais bien prévu ! Depuis près d’une année</l>
					<l n="18" num="1.18">J’éludais tous les jours sa poursuite obstinée.</l>
					<l n="19" num="1.19">Mais, hier, il m’aborde, et me serrant la main :</l>
					<l n="20" num="1.20"><space quantity="2" unit="char"></space>« Ah ! Monsieur, m’a-t-il dit, je vous attends demain.</l>
					<l n="21" num="1.21">N’y manquez pas, au moins ! J’ai quatorze bouteilles</l>
					<l n="22" num="1.22">D’un vin vieux… Boucingo n’en a point de pareilles,</l>
					<l n="23" num="1.23">Et je gagerais bien que, chez le Commandeur,</l>
					<l n="24" num="1.24">Villandri priserait sa sève et sa verdeur.</l>
					<l n="25" num="1.25">Molière avec <hi rend="ital">Tartuffe</hi> y doit jouer son rôle,</l>
					<l n="26" num="1.26">Et Lambert, qui plus est, m’a donné sa parole ;</l>
					<l n="27" num="1.27">C’est tout dire en un mot, et vous le connaissez.</l>
					<l n="28" num="1.28">— Quoi ! Lambert ? — Oui, Lambert. — A demain. C’est assez. »</l>
					<l n="29" num="1.29"><space quantity="2" unit="char"></space>Ce matin donc, séduit par sa vaine promesse,</l>
					<l n="30" num="1.30">J’y cours, midi sonnant, au sortir de la messe.</l>
					<l n="31" num="1.31">A peine étais-je entré, que, ravi de me voir,</l>
					<l n="32" num="1.32">Mon homme, en m’embrassant, m’est venu recevoir,</l>
					<l n="33" num="1.33">Et, montrant à mes yeux une allégresse entière :</l>
					<l n="34" num="1.34">« Nous n’avons, m’a-t-il dit, ni Lambert ni Molière ;</l>
					<l n="35" num="1.35">Mais, puisque je vous vois, je me tiens trop content.</l>
					<l n="36" num="1.36">Vous êtes un brave homme. Entrez. On vous attend. »</l>
					<l n="37" num="1.37"><space quantity="2" unit="char"></space>A ces mots, mais trop tard, reconnaissant ma faute,</l>
					<l n="38" num="1.38">Je le suis en tremblant dans une chambre haute,</l>
					<l n="39" num="1.39">Où, malgré les volets, le soleil irrité</l>
					<l n="40" num="1.40">Formait un poêle ardent au milieu de l’été.</l>
					<l n="41" num="1.41">Le couvert était mis dans ce lieu de plaisance,</l>
					<l n="42" num="1.42">Où j’ai trouvé d’abord, pour toute connaissance,</l>
					<l n="43" num="1.43">Deux nobles campagnards, grands lecteurs de romans,</l>
					<l n="44" num="1.44">Qui m’ont dit tout <hi rend="ital">Cyrus</hi> dans leurs longs compliments.</l>
					<l n="45" num="1.45">J’enrageais. Cependant, on apporte un potage :</l>
					<l n="46" num="1.46">Un coq y paraissait en pompeux équipage,</l>
					<l n="47" num="1.47">Qui, changeant sur ce plat et d’état et de nom,</l>
					<l n="48" num="1.48">Par tous les conviés s’est appelé chapon.</l>
					<l n="49" num="1.49">Deux assiettes suivaient, dont l’une était ornée</l>
					<l n="50" num="1.50">D’une langue en ragoût, de persil couronnée ;</l>
					<l n="51" num="1.51">L’autre, d’un godiveau tout brûlé par dehors,</l>
					<l n="52" num="1.52">Dont un beurre gluant inondait tous les bords.</l>
					<l n="53" num="1.53">On s’assied : mais d’abord notre troupe serrée</l>
					<l n="54" num="1.54">Tenait à peine autour d’une table carrée,</l>
					<l n="55" num="1.55">Où chacun malgré soi l’un sur l’autre porté,</l>
					<l n="56" num="1.56">Faisait un tour à gauche, et mangeait de côté.</l>
					<l n="57" num="1.57">Jugez en cet état si je pouvais me plaire !</l>
					<l n="58" num="1.58">Moi ! qui ne compte rien ni le vin ni la chère,</l>
					<l n="59" num="1.59">Si l’on n’est plus au large assis en un festin</l>
					<l n="60" num="1.60">Qu’aux sermons de Cassagne, ou de l’abbé Cotin.</l>
					<l n="61" num="1.61"><space quantity="2" unit="char"></space>Notre hôte, cependant, s’adressant à la troupe :</l>
					<l n="62" num="1.62">« Que vous semble, a-t-il dit, du goût de cette soupe ?</l>
					<l n="63" num="1.63">Sentez-vous le citron, dont on a mis le jus,</l>
					<l n="64" num="1.64">Avec des jaunes d’œuf mêlés dans du verjus ?</l>
					<l n="65" num="1.65">Ma foi, vive Mignot et tout ce qu’il apprête ! »</l>
					<l n="66" num="1.66">Les cheveux cependant me dressaient à la tête :</l>
					<l n="67" num="1.67">Car Mignot, c’est tout dire, et dans le monde entier</l>
					<l n="68" num="1.68">Jamais empoisonneur ne sut mieux son métier.</l>
					<l n="69" num="1.69">J’approuvais tout pourtant de la mine et du geste,</l>
					<l n="70" num="1.70">Pensant qu’au moins le vin dût réparer le reste.</l>
					<l n="71" num="1.71">Pour m’en éclaircir donc, j’en demande ; et d’abord</l>
					<l n="72" num="1.72">Un laquais effronté m’apporte un rouge-bord</l>
					<l n="73" num="1.73">D’un Auvernat fumeux, qui, mêlé de Lignage,</l>
					<l n="74" num="1.74">Se vendait chez Crenet pour vin de l’Hermitage,</l>
					<l n="75" num="1.75">Et qui, rouge et vermeil, mais fade et doucereux,</l>
					<l n="76" num="1.76">N’avait rien qu’un goût plat et qu’un déboire affreux.</l>
					<l n="77" num="1.77">A peine ai-je senti cette liqueur traîtresse,</l>
					<l n="78" num="1.78">Que de ces vins mêlés j’ai reconnu l’adresse.</l>
					<l n="79" num="1.79">Toutefois, avec eau que j’y mets à foison,</l>
					<l n="80" num="1.80">J’espérais adoucir la force du poison.</l>
					<l n="81" num="1.81">Mais, qui l’aurait pensé ? pour comble de disgrâce,</l>
					<l n="82" num="1.82">Par le chaud qu’il faisait, nous n’avions point de glace !</l>
					<l n="83" num="1.83">Point de glace, bon Dieu ! dans le fort de l’été !</l>
					<l n="84" num="1.84">Au mois de juin ! Pour moi, j’étais si transporté,</l>
					<l n="85" num="1.85">Que, donnant de fureur tout le festin au diable,</l>
					<l n="86" num="1.86">Je me suis vu vingt fois prêt à quitter la table,</l>
					<l n="87" num="1.87">Et, dût-on m’appeler et fantasque et bourru,</l>
					<l n="88" num="1.88">J’allais sortir enfin, quand le rôt a paru.</l>
					<l n="89" num="1.89"><space quantity="2" unit="char"></space>Sur un lièvre, flanqué de six poulets étiques,</l>
					<l n="90" num="1.90">S’élevaient trois lapins, animaux domestiques,</l>
					<l n="91" num="1.91">Qui, dès leur tendre enfance élevés dans Paris,</l>
					<l n="92" num="1.92">Sentaient encor le chou dont ils furent nourris :</l>
					<l n="93" num="1.93">Autour de cet amas de viandes entassées,</l>
					<l n="94" num="1.94">Régnait un long cordon d’alouettes pressées ;</l>
					<l n="95" num="1.95">Et, sur les bords du plat, six pigeons étalés</l>
					<l n="96" num="1.96">Présentaient pour renfort leurs squelettes brûlés.</l>
					<l n="97" num="1.97">A côté de ce plat, paraissaient deux salades,</l>
					<l n="98" num="1.98">L’une, de pourpier jaune, et l’autre, d’herbes fades,</l>
					<l n="99" num="1.99">Dont l’huile de fort loin saisissait l’odorat,</l>
					<l n="100" num="1.100">Et nageait dans des flots de vinaigre rosat.</l>
					<l n="101" num="1.101">Tous mes sots, à l’instant, changeant de contenance,</l>
					<l n="102" num="1.102">Ont loué du festin la superbe ordonnance ;</l>
					<l n="103" num="1.103">Tandis que mon faquin, qui se voyait priser,</l>
					<l n="104" num="1.104">Avec un ris moqueur les priait d’excuser.</l>
					<l n="105" num="1.105">Surtout certain hâbleur, à la gueule affamée,</l>
					<l n="106" num="1.106">Qui vint à ce festin conduit par la fumée,</l>
					<l n="107" num="1.107">Et qui s’est dit profès dans l’ordre des Coteaux,</l>
					<l n="108" num="1.108">A fait, en bien mangeant, l’éloge des morceaux.</l>
					<l n="109" num="1.109">Je riais de le voir, avec sa mine étique,</l>
					<l n="110" num="1.110">Son rabat jadis blanc, et sa perruque antique,</l>
					<l n="111" num="1.111">En lapins de garenne ériger nos clapiers,</l>
					<l n="112" num="1.112">Et nos pigeons cauchois en superbes ramiers,</l>
					<l n="113" num="1.113">Et, pour flatter notre hôte, observant son visage,</l>
					<l n="114" num="1.114">Composer sur ses yeux son geste et son langage,</l>
					<l n="115" num="1.115">Quand notre hôte charmé, m’avisant sur ce point :</l>
					<l n="116" num="1.116">« Qu’avez-vous donc, dit-il, que vous ne mangez point ?</l>
					<l n="117" num="1.117">Je vous trouve aujourd’hui l’âme tout inquiète,</l>
					<l n="118" num="1.118">Et les morceaux entiers restent sur votre assiette…</l>
					<l n="119" num="1.119">Aimez-vous la muscade ? On en a mis partout.</l>
					<l n="120" num="1.120">Ah ! monsieur, ces poulets sont d’un merveilleux goût ;</l>
					<l n="121" num="1.121">Ces pigeons sont dodus ; mangez, sur ma parole.</l>
					<l n="122" num="1.122">J’aime à voir aux lapins cette chair blanche et molle.</l>
					<l n="123" num="1.123">Ma foi ! tout est passable, il faut le confesser,</l>
					<l n="124" num="1.124">Et Mignot aujourd’hui s’est voulu surpasser.</l>
					<l n="125" num="1.125">Quand on parle de sauce, il faut qu’on y raffine ;</l>
					<l n="126" num="1.126">Pour moi, j’aime surtout que le poivre y domine :</l>
					<l n="127" num="1.127">J’en suis fourni, Dieu sait ! et j’ai tout Pelletier</l>
					<l n="128" num="1.128">Roulé dans mon office en cornets de papier. »</l>
					<l n="129" num="1.129">A tous ces beaux discours, j’étais comme une pierre,</l>
					<l n="130" num="1.130">Ou comme la statue est au <hi rend="ital">Festin de Pierre</hi> ;</l>
					<l n="131" num="1.131">Et, sans dire un seul mot, j’avalais au hasard</l>
					<l n="132" num="1.132">Quelque aile de poulet dont j’arrachais le lard.</l>
					<l n="133" num="1.133">Cependant, mon hâbleur, avec une voix haute,</l>
					<l n="134" num="1.134">Porte à mes campagnards la santé de notre hôte,</l>
					<l n="135" num="1.135">Qui tous deux pleins de joie, en jetant un grand cri,</l>
					<l n="136" num="1.136">Avec un rouge-bord acceptent son défi.</l>
					<l n="137" num="1.137">Un si galant exploit réveillant tout le monde,</l>
					<l n="138" num="1.138">On a porté partout des verres à la ronde,</l>
					<l n="139" num="1.139">Où les doigts des laquais, dans la crasse tracés,</l>
					<l n="140" num="1.140">Témoignaient par écrit qu’on les avait rincés.</l>
					<l n="141" num="1.141">Quand un des conviés, d’un ton mélancolique,</l>
					<l n="142" num="1.142">Lamentant tristement une chanson bachique,</l>
					<l n="143" num="1.143">Tous mes sots à la fois, ravis de l’écouter,</l>
					<l n="144" num="1.144">Détonnant de concert, se mettent à chanter.</l>
					<l n="145" num="1.145">La musique sans doute était rare et charmante !</l>
					<l n="146" num="1.146">L’un, traîne en longs fredons une voix glapissante,</l>
					<l n="147" num="1.147">Et l’autre, l’appuyant de son aigre fausset,</l>
					<l n="148" num="1.148">Semble un violon faux qui jure sous l’archet.</l>
					<l n="149" num="1.149"><space quantity="2" unit="char"></space>Sur ce point, un jambon d’assez maigre apparence</l>
					<l n="150" num="1.150">Arrive sous le nom de jambon de Mayence.</l>
					<l n="151" num="1.151">Un valet le portait, marchant à pas comptés,</l>
					<l n="152" num="1.152">Comme un recteur suivi des quatre facultés.</l>
					<l n="153" num="1.153">Deux marmitons crasseux, revêtus de serviettes,</l>
					<l n="154" num="1.154">Lui servaient de massiers, et portaient deux assiettes,</l>
					<l n="155" num="1.155">L’une, de champignons avec des ris de veau,</l>
					<l n="156" num="1.156">Et l’autre de pois verts, qui se noyaient dans l’eau.</l>
					<l n="157" num="1.157">Un spectacle si beau surprenant l’assemblée,</l>
					<l n="158" num="1.158">Chez tous les conviés la joie est redoublée,</l>
					<l n="159" num="1.159">Et la troupe, à l’instant, cessant de fredonner,</l>
					<l n="160" num="1.160">D’un ton gravement fou s’est mise à raisonner.</l>
					<l n="161" num="1.161">Le vin au plus muet fournissant des paroles,</l>
					<l n="162" num="1.162">Chacun a débité ses maximes frivoles,</l>
					<l n="163" num="1.163">Réglé les intérêts de chaque potentat,</l>
					<l n="164" num="1.164">Corrigé la police, et réformé l’État ;</l>
					<l n="165" num="1.165">Puis, de là, s’embarquant dans la nouvelle guerre,</l>
					<l n="166" num="1.166">A vaincu la Hollande ou battu l’Angleterre.</l>
					<l n="167" num="1.167"><space quantity="2" unit="char"></space>Enfin, laissant en paix tous ces peuples divers,</l>
					<l n="168" num="1.168">De propos en propos on a parlé de vers.</l>
					<l n="169" num="1.169">Là, tous mes sots, enflés d’une nouvelle audace,</l>
					<l n="170" num="1.170">Ont jugé des auteurs en maîtres du Parnasse,</l>
					<l n="171" num="1.171">Mais notre hôte surtout, pour la justesse et l’art,</l>
					<l n="172" num="1.172">Élevait jusqu’au ciel Théophile et Ronsard ;</l>
					<l n="173" num="1.173">Quand un des campagnards, relevant sa moustache,</l>
					<l n="174" num="1.174">Et son feutre à grands poils ombragé d’un panache,</l>
					<l n="175" num="1.175">Impose à tous silence, et d’un ton de docteur :</l>
					<l n="176" num="1.176">« Morbleu ! dit-il, La Serre est un charmant auteur !</l>
					<l n="177" num="1.177">Ses vers sont d’un beau style, et sa prose est coulante.</l>
					<l n="178" num="1.178"><hi rend="ital">La Pucelle</hi> est encore une œuvre bien galante,</l>
					<l n="179" num="1.179">Et je ne sais pourquoi je bâille en la lisant.</l>
					<l n="180" num="1.180">Le Pays, sans mentir, est un bouffon plaisant,</l>
					<l n="181" num="1.181">Mais je ne trouve rien de beau dans ce Voiture</l>
					<l n="182" num="1.182">Ma foi, le jugement sert bien dans la lecture.</l>
					<l n="183" num="1.183">A mon gré, le Corneille est joli quelquefois.</l>
					<l n="184" num="1.184">En vérité, pour moi j’aime le beau françois.</l>
					<l n="185" num="1.185">Je ne sais pas pourquoi l’on vante l’<hi rend="ital">Alexandre</hi>,</l>
					<l n="186" num="1.186">Ce n’est qu’un glorieux qui ne dit rien de tendre.</l>
					<l n="187" num="1.187">Les héros chez Quinault parlent bien autrement,</l>
					<l n="188" num="1.188">Et, jusqu’à « Je vous hais », tout s’y dit tendrement.</l>
					<l n="189" num="1.189">On dit qu’on l’a drapé dans certaine satire,</l>
					<l n="190" num="1.190">Qu’un jeune homme… — Ah ! je sais ce que vous voulez dire,</l>
					<l n="191" num="1.191">A répondu notre hôte : « Un auteur sans défaut,</l>
					<l n="192" num="1.192">« La raison dit Virgile, et la rime Quinault. »</l>
					<l n="193" num="1.193">— Justement. A mon gré la pièce est assez plate.</l>
					<l n="194" num="1.194">Et puis, blâmer Quinault !… avez-vous vu l’<hi rend="ital">Astrate</hi> ?</l>
					<l n="195" num="1.195">C’est là ce qu’on appelle un ouvrage achevé !</l>
					<l n="196" num="1.196">Surtout, l’anneau royal me semble bien trouvé.</l>
					<l n="197" num="1.197">Son sujet est conduit d’une belle manière,</l>
					<l n="198" num="1.198">Et chaque acte, en sa pièce, est une pièce entière.</l>
					<l n="199" num="1.199">Je ne puis plus souffrir ce que les autres font.</l>
					<l n="200" num="1.200"><space quantity="2" unit="char"></space>— Il est vrai que Quinault est un esprit profond,</l>
					<l n="201" num="1.201">A repris certain fat, qu’à sa mine discrète</l>
					<l n="202" num="1.202">Et son maintien jaloux, j’ai reconnu poète,</l>
					<l n="203" num="1.203">Mais, il en est pourtant qui le pourraient valoir.</l>
					<l n="204" num="1.204">— Ma foi, ce n’est pas vous qui nous le ferez voir,</l>
					<l n="205" num="1.205">A dit mon campagnard avec une voix claire,</l>
					<l n="206" num="1.206">Et déjà tout bouillant de vin et de colère.</l>
					<l n="207" num="1.207">— Peut-être, a dit l’auteur, pâlissant de courroux ;</l>
					<l n="208" num="1.208">Mais, vous, pour en parler, vous y connaissez-vous ?</l>
					<l n="209" num="1.209">— Mieux que vous mille fois, dit le noble en furie.</l>
					<l n="210" num="1.210">— Vous ? mon Dieu ! mêlez-vous de boire, je vous prie,</l>
					<l n="211" num="1.211">A l’auteur sur-le-champ aigrement reparti.</l>
					<l n="212" num="1.212">— Je suis donc un sot, moi ? vous en avez menti, »</l>
					<l n="213" num="1.213">Reprend le campagnard, et, sans plus de langage,</l>
					<l n="214" num="1.214">Lui jette pour défi son assiette au visage.</l>
					<l n="215" num="1.215">L’autre esquive le coup, et l’assiette, volant,</l>
					<l n="216" num="1.216">S’en va frapper le mur, et revient en roulant.</l>
					<l n="217" num="1.217">A cet affront, l’auteur, se levant de la table,</l>
					<l n="218" num="1.218">Lance à mon campagnard un regard effroyable,</l>
					<l n="219" num="1.219">Et, chacun vainement se ruant entre deux,</l>
					<l n="220" num="1.220">Nos braves, s’accrochant, se prennent aux cheveux.</l>
					<l n="221" num="1.221">Aussitôt, sous leurs pieds, les tables renversées</l>
					<l n="222" num="1.222">Font voir un long débris de bouteilles cassées :</l>
					<l n="223" num="1.223">En vain à lever tout les valets sont fort prompts,</l>
					<l n="224" num="1.224">Et les ruisseaux de vin coulent aux environs.</l>
					<l n="225" num="1.225"><space quantity="2" unit="char"></space>Enfin, pour arrêter cette lutte barbare,</l>
					<l n="226" num="1.226">De nouveau l’on s’efforce, on crie, on les sépare,</l>
					<l n="227" num="1.227">Et, leur première ardeur passant en un moment,</l>
					<l n="228" num="1.228">On a parlé de paix et d’accommodement.</l>
					<l n="229" num="1.229">Mais, tandis qu’à l’envi tout le monde y conspire,</l>
					<l n="230" num="1.230">J’ai gagné doucement la porte sans rien dire,</l>
					<l n="231" num="1.231">Avec un bon serment que, si pour l’avenir,</l>
					<l n="232" num="1.232">En pareille cohue on me peut retenir,</l>
					<l n="233" num="1.233">Je consens de bon cœur, pour punir ma folie,</l>
					<l n="234" num="1.234">Que tous les vins pour moi deviennent vins de Brie ;</l>
					<l n="235" num="1.235">Qu’à Paris le gibier manque tous les hivers ;</l>
					<l n="236" num="1.236">Et qu’à peine au mois d’août l’on mange des pois verts.</l>
				</lg>
			</div></body></text></TEI>