<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">SATIRES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BOI">
					<name>
						<forename>Nicolas</forename>
						<surname>BOILEAU-DESPRÉAUX</surname>
					</name>
					<date from="1636" to="1711">1636-1711</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2918 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BOI_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Œuvres poétiques de Boileau-Despréaux avec une introduction et des notes, par F. Brunetière</title>
						<author>Nicolas Boileau-Despréaux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k97348967</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Œuvres poétiques</title>
								<author>Nicolas Boileau-Despréaux</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Hachette</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1666" to="1716">1666-1716</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La présente édition correspond à une partie du texte d’origine.</p>
				<p>L’ensemble des textes versifiés est réparti en 7 fichiers correspondant chacun à un chapitre.</p>
				<p>La préface ainsi que les autres parties liminaires ne sont pas encodées.</p>
				<p>Les gravures et références des gravures sont pas incluses.</p>
				<p>Les notes de l’éditeur ne sont pas incluses.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (séquence de 3 ou 4 points) ont été remplacés par le signe typographique approprié.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BOI3">
				<head type="main">SATIRE II</head>
				<head type="sub_1">A M. DE MOLIÈRE</head>
				<opener>
					<dateline>
						<date when="1664">(1664)</date>
					</dateline>
				</opener>
				<lg n="1">
					<l n="1" num="1.1">Rare et fameux esprit, dont la fertile veine</l>
					<l n="2" num="1.2">Ignore en écrivant le travail et la peine,</l>
					<l n="3" num="1.3">Pour qui tient Apollon tous ses trésors ouverts,</l>
					<l n="4" num="1.4">Et qui sais à quel coin se marquent les bons vers,</l>
					<l n="5" num="1.5">Dans les combats d’esprit savant maître d’escrime,</l>
					<l n="6" num="1.6">Enseigne-moi, Molière, où tu trouves la rime.</l>
					<l n="7" num="1.7">On dirait, quand tu veux, qu’elle te vient chercher :</l>
					<l n="8" num="1.8">Jamais au bout du vers on ne te voit broncher,</l>
					<l n="9" num="1.9">Et, sans qu’un long détour t’arrête ou t’embarrasse,</l>
					<l n="10" num="1.10">A peine as-tu parlé qu’elle-même s’y place.</l>
					<l n="11" num="1.11">Mais, moi, qu’un vain caprice, une bizarre humeur,</l>
					<l n="12" num="1.12">Pour mes péchés, je crois, fit devenir rimeur,</l>
					<l n="13" num="1.13">Dans ce rude métier où mon esprit se tue,</l>
					<l n="14" num="1.14">En vain, pour la trouver, je travaille et je sue,</l>
					<l n="15" num="1.15">Souvent j’ai beau rêver du matin jusqu’au soir :</l>
					<l n="16" num="1.16">Quand je veux dire blanc, la quinteuse dit noir.</l>
					<l n="17" num="1.17">Si je veux d’un galant dépeindre la figure,</l>
					<l n="18" num="1.18">Ma plume pour rimer trouve l’abbé de Pure ;</l>
					<l n="19" num="1.19">Si je pense exprimer un auteur sans défaut,</l>
					<l n="20" num="1.20">La raison dit Virgile, et la rime Quinault ;</l>
					<l n="21" num="1.21">Enfin, quoi que je fasse, ou que je veuille faire,</l>
					<l n="22" num="1.22">La bizarre toujours vient m’offrir le contraire.</l>
					<l n="23" num="1.23">De rage, quelquefois, ne pouvant la trouver,</l>
					<l n="24" num="1.24">Triste, las, et confus, je cesse d’y rêver,</l>
					<l n="25" num="1.25">Et, maudissant vingt fois le démon qui m’inspire,</l>
					<l n="26" num="1.26">Je fais mille serments de ne jamais écrire.</l>
					<l n="27" num="1.27">Mais, quand j’ai bien maudit et Muses et Phébus,</l>
					<l n="28" num="1.28">Je la vois qui paraît quand je n’y pense plus.</l>
					<l n="29" num="1.29">Aussitôt, malgré moi, tout mon feu se rallume,</l>
					<l n="30" num="1.30">Je reprends sur-le-champ le papier et la plume,</l>
					<l n="31" num="1.31">Et, de mes vains serments perdant le souvenir,</l>
					<l n="32" num="1.32">J’attends de vers en vers qu’elle daigne venir.</l>
					<l n="33" num="1.33">Encor si, pour rimer, dans sa verve indiscrète,</l>
					<l n="34" num="1.34">Ma Muse, au moins, souffrait une froide épithète !</l>
					<l n="35" num="1.35">Je ferais comme un autre, et, sans chercher si loin,</l>
					<l n="36" num="1.36">J’aurais toujours des mots pour les coudre au besoin.</l>
					<l n="37" num="1.37">Si je louais Philis <hi rend="ital">en miracles féconde</hi>,</l>
					<l n="38" num="1.38">Je trouverais bientôt : <hi rend="ital">à nulle autre seconde</hi> ;</l>
					<l n="39" num="1.39">Si je voulais vanter un objet <hi rend="ital">nonpareil</hi>,</l>
					<l n="40" num="1.40">Je mettrais à l’instant : <hi rend="ital">plus beau que le soleil</hi> ;</l>
					<l n="41" num="1.41">Enfin, parlant toujours d’<hi rend="ital">astres<hi rend="ital"> et de </hi>merveilles</hi>,</l>
					<l n="42" num="1.42">De <hi rend="ital">chefs-d’œuvre des cieux</hi>, de <hi rend="ital">beautés sans pareilles</hi>,</l>
					<l n="43" num="1.43">Avec tous ces beaux mots, souvent mis au hasard,</l>
					<l n="44" num="1.44">Je pourrais aisément, sans génie et sans art,</l>
					<l n="45" num="1.45">Et transposant cent fois et le nom et le verbe,</l>
					<l n="46" num="1.46">Dans mes vers recousus mettre en pièces Malherbe.</l>
					<l n="47" num="1.47">Mais, mon esprit, tremblant sur le choix de ses mots,</l>
					<l n="48" num="1.48">N’en dira jamais un s’il ne tombe à propos,</l>
					<l n="49" num="1.49">Et ne saurait souffrir qu’une phrase insipide</l>
					<l n="50" num="1.50">Vienne à la fin d’un vers remplir la place vide ;</l>
					<l n="51" num="1.51">Ainsi, recommençant un ouvrage vingt fois,</l>
					<l n="52" num="1.52">Si j’écris quatre mots, j’en effacerai trois.</l>
					<l n="53" num="1.53"><space quantity="2" unit="char"></space>Maudit soit le premier, dont la verve insensée</l>
					<l n="54" num="1.54">Dans les bornes d’un vers renferma sa pensée,</l>
					<l n="55" num="1.55">Et, donnant à ses mots une étroite prison,</l>
					<l n="56" num="1.56">Voulut avec la rime enchaîner la raison !</l>
					<l n="57" num="1.57">Sans ce métier, fatal au repos de ma vie,</l>
					<l n="58" num="1.58">Mes jours, pleins de loisir, couleraient sans envie :</l>
					<l n="59" num="1.59">Je n’aurais qu’à chanter, rire, boire d’autant,</l>
					<l n="60" num="1.60">Et, comme un gras chanoine, à mon aise, et content,</l>
					<l n="61" num="1.61">Passer tranquillement, sans souci, sans affaire,</l>
					<l n="62" num="1.62">La nuit à bien dormir, et le jour à rien faire.</l>
					<l n="63" num="1.63">Mon cœur, exempt de soins, libre de passion,</l>
					<l n="64" num="1.64">Sait donner une borne à son ambition,</l>
					<l n="65" num="1.65">Et, fuyant des grandeurs la présence importune,</l>
					<l n="66" num="1.66">Je ne vais point au Louvre adorer la fortune,</l>
					<l n="67" num="1.67">Et je serais heureux, si, pour me consumer,</l>
					<l n="68" num="1.68">Un destin envieux ne m’avait fait rimer.</l>
					<l n="69" num="1.69"><space quantity="2" unit="char"></space>Mais, depuis le moment que cette frénésie</l>
					<l n="70" num="1.70">De ses noires vapeurs troubla ma fantaisie,</l>
					<l n="71" num="1.71">Et qu’un Démon, jaloux de mon contentement,</l>
					<l n="72" num="1.72">M’inspira le dessein d’écrire poliment,</l>
					<l n="73" num="1.73">Tous les jours, malgré moi, cloué sur un ouvrage,</l>
					<l n="74" num="1.74">Retouchant un endroit, effaçant une page,</l>
					<l n="75" num="1.75">Enfin, passant ma vie en ce triste métier,</l>
					<l n="76" num="1.76">J’envie, en écrivant, le sort de Pelletier.</l>
					<l n="77" num="1.77"><space quantity="2" unit="char"></space>Bienheureux Scudéri, dont la fertile plume</l>
					<l n="78" num="1.78">Peut tous les mois sans peine enfanter un volume !</l>
					<l n="79" num="1.79">Tes écrits, il est vrai, sans art et languissants,</l>
					<l n="80" num="1.80">Semblent être formés en dépit du bon sens !</l>
					<l n="81" num="1.81">Mais ils trouvent pourtant, quoi qu’on en puisse dire,</l>
					<l n="82" num="1.82">Un marchand pour les vendre, et des sots pour les lire ;</l>
					<l n="83" num="1.83">Et quand la rime enfin se trouve au bout des vers,</l>
					<l n="84" num="1.84">Qu’importe que le reste y soit mis de travers ?</l>
					<l n="85" num="1.85">Malheureux mille fois celui dont la manie</l>
					<l n="86" num="1.86">Veut aux règles de l’art asservir son génie !</l>
					<l n="87" num="1.87">Un sot, en écrivant, fait tout avec plaisir ;</l>
					<l n="88" num="1.88">Il n’a point en ses vers l’embarras de choisir ;</l>
					<l n="89" num="1.89">Et, toujours amoureux de ce qu’il vient d’écrire,</l>
					<l n="90" num="1.90">Ravi d’étonnement, en soi-même il s’admire.</l>
					<l n="91" num="1.91">Mais, un esprit sublime en vain veut s’élever</l>
					<l n="92" num="1.92">A ce degré parfait qu’il tâche de trouver ;</l>
					<l n="93" num="1.93">Et, toujours mécontent de ce qu’il vient de faire,</l>
					<l n="94" num="1.94">Il plaît à tout le monde, et ne saurait se plaire ;</l>
					<l n="95" num="1.95">Et tel dont en tous lieux chacun vante l’esprit</l>
					<l n="96" num="1.96">Voudrait pour son repos n’avoir jamais écrit.</l>
					<l n="97" num="1.97"><space quantity="2" unit="char"></space>Toi donc, qui vois les maux où ma Muse s’abîme,</l>
					<l n="98" num="1.98">De grâce, enseigne-moi l’art de trouver la rime,</l>
					<l n="99" num="1.99">Ou, puisque enfin tes soins y seraient superflus,</l>
					<l n="100" num="1.100">Molière, enseigne-moi l’art de ne rimer plus.</l>
				</lg>
			</div></body></text></TEI>