<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ODES, ÉPIGRAMMES ET AUTRES POÉSIES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BOI">
					<name>
						<forename>Nicolas</forename>
						<surname>BOILEAU-DESPRÉAUX</surname>
					</name>
					<date from="1636" to="1711">1636-1711</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>633 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BOI_6</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Œuvres poétiques de Boileau-Despréaux avec une introduction et des notes, par F. Brunetière</title>
						<author>Nicolas Boileau-Despréaux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k97348967</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Œuvres poétiques</title>
								<author>Nicolas Boileau-Despréaux</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Hachette</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1664" to="1704">1664-1704</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La présente édition correspond à une partie du texte d’origine.</p>
				<p>L’ensemble des textes versifiés est réparti en 7 fichiers correspondant chacun à un chapitre.</p>
				<p>La préface ainsi que les autres parties liminaires ne sont pas encodées.</p>
				<p>Les gravures et références des gravures sont pas incluses.</p>
				<p>Les notes de l’éditeur ne sont pas incluses.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (séquence de 3 ou 4 points) ont été remplacés par le signe typographique approprié.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-06-29" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-06-29" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BOI36">
				<head type="main">ODE</head>
				<head type="sub">sur la prise de Namur</head>
				<lg n="1">
					<l n="1" num="1.1">Quelle docte et sainte ivresse</l>
					<l n="2" num="1.2">Aujourd’hui me fait la loi ?</l>
					<l n="3" num="1.3">Chastes Nymphes du Permesse,</l>
					<l n="4" num="1.4">N’est-ce pas vous que je voi ?</l>
					<l n="5" num="1.5">Accourez, Troupe savante ;</l>
					<l n="6" num="1.6">Des sons que ma lyre enfante</l>
					<l n="7" num="1.7">Ces arbres sont réjouis ;</l>
					<l n="8" num="1.8">Marquez-en bien la cadence ;</l>
					<l n="9" num="1.9">Et vous, Vents, faites silence :</l>
					<l n="10" num="1.10">Je vais parler de LOUIS.</l>
				</lg>
				<lg n="2">
					<l n="11" num="2.1">Dans ses chansons immortelles,</l>
					<l n="12" num="2.2">Comme un aigle audacieux,</l>
					<l n="13" num="2.3">Pindare, étendant ses ailes,</l>
					<l n="14" num="2.4">Fuit loin des vulgaires yeux ;</l>
					<l n="15" num="2.5">Mais, ô ma fidèle lyre,</l>
					<l n="16" num="2.6">Si, dans l’ardeur qui m’inspire,</l>
					<l n="17" num="2.7">Tu peux suivre mes transports ;</l>
					<l n="18" num="2.8">Les chênes des monts de Thrace</l>
					<l n="19" num="2.9">N’ont rien ouï, que n’efface</l>
					<l n="20" num="2.10">La douceur de tes accords.</l>
				</lg>
				<lg n="3">
					<l n="21" num="3.1">Est-ce Apollon, et Neptune,</l>
					<l n="22" num="3.2">Qui sur ces rocs sourcilleux,</l>
					<l n="23" num="3.3">Ont, compagnons de fortune,</l>
					<l n="24" num="3.4">Bâti ces murs orgueilleux ?</l>
					<l n="25" num="3.5">De leur enceinte fameuse</l>
					<l n="26" num="3.6">La Sambre, unie à la Meuse,</l>
					<l n="27" num="3.7">Défend le fatal abord ;</l>
					<l n="28" num="3.8">Et par cent bouches horribles,</l>
					<l n="29" num="3.9">L’airain sur ces monts terribles</l>
					<l n="30" num="3.10">Vomit le fer et la mort ;</l>
				</lg>
				<lg n="4">
					<l n="31" num="4.1">Dix mille vaillants Alcides,</l>
					<l n="32" num="4.2">Les bordant de toutes parts,</l>
					<l n="33" num="4.3">D’éclairs, au loin homicides,</l>
					<l n="34" num="4.4">Font pétiller leurs remparts ;</l>
					<l n="35" num="4.5">Et dans son sein infidèle</l>
					<l n="36" num="4.6">Partout la terre y recèle</l>
					<l n="37" num="4.7">Un feu prêt à s’élancer,</l>
					<l n="38" num="4.8">Qui soudain perçant son gouffre,</l>
					<l n="39" num="4.9">Ouvre un sépulcre de soufre</l>
					<l n="40" num="4.10">A quiconque ose avancer.</l>
				</lg>
				<lg n="5">
					<l n="41" num="5.1">Namur, devant tes murailles,</l>
					<l n="42" num="5.2">Jadis la Grèce eût, vingt ans,</l>
					<l n="43" num="5.3">Sans fruit, vu les funérailles</l>
					<l n="44" num="5.4">De ses plus fiers combattants.</l>
					<l n="45" num="5.5">Quelle effroyable puissance</l>
					<l n="46" num="5.6">Aujourd’hui pourtant s’avance,</l>
					<l n="47" num="5.7">Prête à foudroyer tes monts !</l>
					<l n="48" num="5.8">Quel bruit, quel feu l’environne !</l>
					<l n="49" num="5.9">C’est Jupiter en personne,</l>
					<l n="50" num="5.10">Ou c’est le vainqueur de Mons.</l>
				</lg>
				<lg n="6">
					<l n="51" num="6.1">N’en doute point, c’est lui-même :</l>
					<l n="52" num="6.2">Tout brille en lui, tout est roi.</l>
					<l n="53" num="6.3">Dans Bruxelles, Nassau blême</l>
					<l n="54" num="6.4">Commence à trembler pour toi.</l>
					<l n="55" num="6.5">En vain, il voit le Batave,</l>
					<l n="56" num="6.6">Désormais docile esclave,</l>
					<l n="57" num="6.7">Rangé sous ses étendards ;</l>
					<l n="58" num="6.8">En vain, au Lion belgique,</l>
					<l n="59" num="6.9">Il voit l’Aigle germanique</l>
					<l n="60" num="6.10">Uni sous les Léopards ;</l>
				</lg>
				<lg n="7">
					<l n="61" num="7.1">Plein de la frayeur nouvelle</l>
					<l n="62" num="7.2">Dont ses sens sont agités,</l>
					<l n="63" num="7.3">A son secours il appelle</l>
					<l n="64" num="7.4">Les peuples les plus vantés :</l>
					<l n="65" num="7.5">Ceux-là viennent du rivage</l>
					<l n="66" num="7.6">Où s’enorgueillit le Tage</l>
					<l n="67" num="7.7">De l’or qui roule en ses eaux ;</l>
					<l n="68" num="7.8">Ceux-ci, des champs où la neige</l>
					<l n="69" num="7.9">Des marais de la Norvège</l>
					<l n="70" num="7.10">Neuf mois couvre les roseaux…</l>
				</lg>
				<lg n="8">
					<l n="71" num="8.1">Mais qui fait enfler la Sambre ?</l>
					<l n="72" num="8.2">Sous les Jumeaux effrayés,</l>
					<l n="73" num="8.3">Des froids torrents de décembre</l>
					<l n="74" num="8.4">Les champs partout sont noyés ;</l>
					<l n="75" num="8.5">Cérès s’enfuit, éplorée</l>
					<l n="76" num="8.6">De voir en proie à Borée</l>
					<l n="77" num="8.7">Ses guérets d’épis chargés ;</l>
					<l n="78" num="8.8">Et sous les urnes fangeuses</l>
					<l n="79" num="8.9">Des Hyades orageuses</l>
					<l n="80" num="8.10">Tous ses trésors submergés.</l>
				</lg>
				<lg n="9">
					<l n="81" num="9.1">Déployez toutes vos rages,</l>
					<l n="82" num="9.2">Princes, vents, peuples, frimas ;</l>
					<l n="83" num="9.3">Ramassez tous vos nuages,</l>
					<l n="84" num="9.4">Rassemblez tous vos soldats ;</l>
					<l n="85" num="9.5">Malgré vous Namur en poudre</l>
					<l n="86" num="9.6">S’en va tomber, sous la foudre</l>
					<l n="87" num="9.7">Qui dompta Lille, Courtrai,</l>
					<l n="88" num="9.8">Gand, la superbe espagnole,</l>
					<l n="89" num="9.9">Saint-Omer, Besançon, Dôle,</l>
					<l n="90" num="9.10">Ypres, Mastricht, et Cambrai.</l>
				</lg>
				<lg n="10">
					<l n="91" num="10.1">Mes présages s’accomplissent :</l>
					<l n="92" num="10.2">Il commence à chanceler ;</l>
					<l n="93" num="10.3">Sous les coups qui retentissent</l>
					<l n="94" num="10.4">Ses murs s’en vont s’écrouler ;</l>
					<l n="95" num="10.5">Mars en feu, qui les domine,</l>
					<l n="96" num="10.6">Souffle à grand bruit leur ruine ;</l>
					<l n="97" num="10.7">Et les bombes, dans les airs</l>
					<l n="98" num="10.8">Allant chercher le tonnerre,</l>
					<l n="99" num="10.9">Semblent, tombant sur la terre,</l>
					<l n="100" num="10.10">Vouloir s’ouvrir les enfers.</l>
				</lg>
				<lg n="11">
					<l n="101" num="11.1">Accourez, Nassau, Bavière,</l>
					<l n="102" num="11.2">De ces murs l’unique espoir.</l>
					<l n="103" num="11.3">A couvert d’une rivière,</l>
					<l n="104" num="11.4">Venez, vous pouvez tout voir.</l>
					<l n="105" num="11.5">Considérez ces approches ;</l>
					<l n="106" num="11.6">Voyez grimper sur ces roches</l>
					<l n="107" num="11.7">Ces athlètes belliqueux ;</l>
					<l n="108" num="11.8">Et dans les eaux, dans la flamme,</l>
					<l n="109" num="11.9">LOUIS, à tout donnant l’âme,</l>
					<l n="110" num="11.10">Marcher, courir avec eux.</l>
				</lg>
				<lg n="12">
					<l n="111" num="12.1">Contemplez dans la tempête</l>
					<l n="112" num="12.2">Qui sort de ces boulevards,</l>
					<l n="113" num="12.3">La plume qui sur sa tête</l>
					<l n="114" num="12.4">Attire tous les regards :</l>
					<l n="115" num="12.5">A cet astre redoutable,</l>
					<l n="116" num="12.6">Toujours un sort favorable</l>
					<l n="117" num="12.7">S’attache dans les combats :</l>
					<l n="118" num="12.8">Et toujours, avec la Gloire,</l>
					<l n="119" num="12.9">Mars, amenant la Victoire,</l>
					<l n="120" num="12.10">Vole, et le suit à grands pas.</l>
				</lg>
				<lg n="13">
					<l n="121" num="13.1">Grands défenseurs de l’Espagne ;</l>
					<l n="122" num="13.2">Montrez-vous, il en est temps.</l>
					<l n="123" num="13.3">Courage ! vers la Mehagne</l>
					<l n="124" num="13.4">Voilà vos drapeaux flottants.</l>
					<l n="125" num="13.5">Jamais ses ondes craintives</l>
					<l n="126" num="13.6">N’ont vu sur leurs faibles rives</l>
					<l n="127" num="13.7">Tant de guerriers s’amasser.</l>
					<l n="128" num="13.8">Courez donc… Qui vous retarde ?</l>
					<l n="129" num="13.9">Tout l’univers vous regarde ;</l>
					<l n="130" num="13.10">N’osez-vous la traverser ?</l>
				</lg>
				<lg n="14">
					<l n="131" num="14.1">Loin de fermer le passage</l>
					<l n="132" num="14.2">A vos nombreux bataillons,</l>
					<l n="133" num="14.3">Luxembourg a du rivage</l>
					<l n="134" num="14.4">Reculé ses pavillons.</l>
					<l n="135" num="14.5">Quoi ! leur seul aspect vous glace !</l>
					<l n="136" num="14.6">Où sont ces chefs pleins d’audace,</l>
					<l n="137" num="14.7">Jadis si prompts à marcher,</l>
					<l n="138" num="14.8">Qui devaient, de la Tamise,</l>
					<l n="139" num="14.9">Et de la Drave soumise,</l>
					<l n="140" num="14.10">Jusqu’à Paris nous chercher ?</l>
				</lg>
				<lg n="15">
					<l n="141" num="15.1">Cependant, l’effroi redouble</l>
					<l n="142" num="15.2">Sur les remparts de Namur ;</l>
					<l n="143" num="15.3">Son gouverneur, qui se trouble,</l>
					<l n="144" num="15.4">S’enfuit sous son dernier mur ;</l>
					<l n="145" num="15.5">Déjà, jusques à ses portes</l>
					<l n="146" num="15.6">Je vois monter nos cohortes,</l>
					<l n="147" num="15.7">La flamme et le fer en main ;</l>
					<l n="148" num="15.8">Et sur les monceaux de piques,</l>
					<l n="149" num="15.9">De corps morts, de rocs, de briques,</l>
					<l n="150" num="15.10">S’ouvrir un large chemin.</l>
				</lg>
				<lg n="16">
					<l n="151" num="16.1">C’en est fait. Je viens d’entendre</l>
					<l n="152" num="16.2">Sur ces rochers éperdus</l>
					<l n="153" num="16.3">Battre un signal pour se rendre.</l>
					<l n="154" num="16.4">Le feu cesse Ils sont rendus !</l>
					<l n="155" num="16.5">Dépouillez votre arrogance,</l>
					<l n="156" num="16.6">Fiers ennemis de la France ;</l>
					<l n="157" num="16.7">Et, désormais gracieux,</l>
					<l n="158" num="16.8">Allez, à Liège, à Bruxelles,</l>
					<l n="159" num="16.9">Porter les humbles nouvelles</l>
					<l n="160" num="16.10">De Namur pris à vos yeux.</l>
				</lg>
				<lg n="17">
					<l n="161" num="17.1">Pour moi, que Phébus anime</l>
					<l n="162" num="17.2">De ses transports les plus doux,</l>
					<l n="163" num="17.3">Rempli de ce Dieu sublime,</l>
					<l n="164" num="17.4">Je vais, plus hardi que vous,</l>
					<l n="165" num="17.5">Montrer que sur le Parnasse,</l>
					<l n="166" num="17.6">Des bois fréquentés d’Horace,</l>
					<l n="167" num="17.7">Ma Muse dans son déclin,</l>
					<l n="168" num="17.8">Sait encor les avenues,</l>
					<l n="169" num="17.9">Et des sources inconnues</l>
					<l n="170" num="17.10">A l’auteur du Saint-Paulin.</l>
				</lg>
			</div></body></text></TEI>