<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES LÈVRES CLOSES</title>
				<title type="medium">Édition électronique</title>
				<author key="DRX">
					<name>
						<forename>Léon</forename>
						<surname>DIERX</surname>
					</name>
					<date from="1838" to="1912">1838-1912</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement métrique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1986 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DRX_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les lèvres closes</title>
						<author>Léon Dierx</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/leondierxleslevrescloses.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres complètes, tome I</title>
						<author>Léon Dierx</author>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k2722190.r=l%C3%A9on+dierx.langFR</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ALPHONSE LEMERRE ÉDITEUR</publisher>
							<date when="1925">1925</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1867">1867</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>Les majuscules accentuées ont été vérifiées.</p>
				<p>Les majuscules en début de vers ont été restituées</p>
				<p>Le formatage strophique a été rétabli</p>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
				<p>Le poème "la Beauté" a été ajouté à partir d’une saisie manuelle</p>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DRX45">
				<head type="main">LA CHANSON DE MAHALL</head>
				<lg n="1">
					<l n="1" num="1.1">C’est un soir calme ; un souffle aux aromes subtils</l>
					<l n="2" num="1.2">Vanne de fleurs en fleurs, et du parc aux collines,</l>
					<l n="3" num="1.3">Le pollen qu’il dépose aux pointes des pistils ;</l>
					<l n="4" num="1.4">Un soir d’été serein, aux étoiles câlines.</l>
					<l n="5" num="1.5">La lune magnétique arrose les halliers ;</l>
					<l n="6" num="1.6">Et dans l’herbe, pareils à deux grands boucliers</l>
					<l n="7" num="1.7">Chus d’un duel gigantesque en preuve pour l’histoire,</l>
					<l n="8" num="1.8">Dorment deux lacs jaloux, d’acier blanc criblé d’or.</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1">À la tour du château s’éclaire l’oratoire</l>
					<l n="10" num="2.2">De Gemma. — par accès, le long du corridor,</l>
					<l n="11" num="2.3">Comme l’appel lointain d’un blessé qu’on emporte,</l>
					<l n="12" num="2.4">Se répète un soupir traînant de porte en porte.</l>
					<l n="13" num="2.5">Hors la fenêtre rouge aux deux barres en croix,</l>
					<l n="14" num="2.6">Tout reste abandonné dans l’antique demeure ;</l>
					<l n="15" num="2.7">Hors la plainte du vent, rien n’élève la voix.</l>
					<l n="16" num="2.8">C’est qu’une femme est là, qui souffre, prie, et pleure !</l>
				</lg>
				<lg n="3">
					<l n="17" num="3.1">Sur d’étroites cloisons pèse le dôme obscur ;</l>
					<l n="18" num="3.2">Mais un haut lampadaire est dressé près du mur,</l>
					<l n="19" num="3.3">Et vers un portrait d’homme au noir sourcil projette</l>
					<l n="20" num="3.4">Les tremblantes lueurs d’une lampe d’argent.</l>
					<l n="21" num="3.5">L’âme du mort revit sur l’image inquiète,</l>
					<l n="22" num="3.6">Sans cesse du front blême aux lèvres voltigeant.</l>
					<l n="23" num="3.7">Au dossier blasonné de sa chaise ducale,</l>
					<l n="24" num="3.8">Croisant les doigts, se tient Gemma, muette et pâle,</l>
					<l n="25" num="3.9">Immobile, debout, jeune et belle, en grand deuil.</l>
					<l n="26" num="3.10">Son bras luit à travers le crêpe qui le voile ;</l>
					<l n="27" num="3.11">Et l’on voit un foyer de tristesse et d’orgueil</l>
					<l n="28" num="3.12">En ses yeux maintenus fixement vers la toile.</l>
					<l n="29" num="3.13">Dans son cadre d’ébène un très large miroir</l>
					<l n="30" num="3.14">Réfléchit le portrait de l’homme au sourcil noir,</l>
					<l n="31" num="3.15">La veuve comme un spectre, et les sombres tentures</l>
					<l n="32" num="3.16">Qui viennent s’écraser partout sur le tapis ;</l>
					<l n="33" num="3.17">Des filets de lumière alternent aux sculptures.</l>
					<l n="34" num="3.18">Assise à la fenêtre et les sens assoupis,</l>
					<l n="35" num="3.19">Une vieille marmonne entre ses dents branlantes</l>
					<l n="36" num="3.20">Des mots qui troublent seuls le vol des heures lentes.</l>
					<l n="37" num="3.21">Tout au fond saigne un christ d’ivoire, et devant lui</l>
					<l n="38" num="3.22">Repose un beau missel incrusté d’armoiries,</l>
					<l n="39" num="3.23">Sur le prie-Dieu de chêne, auprès de son étui.</l>
					<l n="40" num="3.24">Un mystère s’amasse au bas des draperies.</l>
				</lg>
				<lg n="4">
					<l n="41" num="4.1">Et, tout à coup, crispant ses deux mains sur son cœur</l>
					<l n="42" num="4.2">Où bouillonnait le flot grossi de sa douleur,</l>
					<l n="43" num="4.3">Gemma se tord, la tête et le buste en arrière.</l>
					<l n="44" num="4.4">Elle arrache ses yeux, à la longue taris,</l>
					<l n="45" num="4.5">De ce regard jamais éteint sous la paupière,</l>
					<l n="46" num="4.6">Et, la gorge entr’ouverte à d’impossibles cris,</l>
					<l n="47" num="4.7">Marche en se roidissant dans la chambre, suivie</l>
					<l n="48" num="4.8">Par ce regard dardé du fond d’une autre vie.</l>
					<l n="49" num="4.9">Elle s’arrête enfin, sans geste, à l’angle clair</l>
					<l n="50" num="4.10">De la creuse embrasure où, dans l’ombre baignée,</l>
					<l n="51" num="4.11">La vieille à l’autre coin chante sur un vieux air,</l>
					<l n="52" num="4.12">Et près de son rouet s’endort, lasse araignée.</l>
					<l n="53" num="4.13">Tout le passé renaît en Gemma, jours par jours ;</l>
					<l n="54" num="4.14">Et flottant sur le parc au hasard des détours,</l>
					<l n="55" num="4.15">La transporte et la roule ainsi dans son supplice :</l>
				</lg>
				<lg n="5">
					<l n="56" num="5.1">« Ciel tranquille ! Ciel vaste et profond ! Dont la paix</l>
					<l n="57" num="5.2">Semble s’éterniser sous les nappes d’eau lisse,</l>
					<l n="58" num="5.3">Et lointaine descend dans les taillis épais !</l>
					<l n="59" num="5.4">Regard multiplié des nuits, qui nous surveilles !</l>
					<l n="60" num="5.5">Où sont-ils, ces matins aux si fraîches merveilles,</l>
					<l n="61" num="5.6">Que, comme vous limpide et pure, j’ai vécus !</l>
					<l n="62" num="5.7">Où le métal uni de mes jeunes prunelles</l>
					<l n="63" num="5.8">À sa clarté brisait tous les désirs aigus !</l>
					<l n="64" num="5.9">Où j’allais promenant mes candeurs fraternelles</l>
					<l n="65" num="5.10">Dans le vert paradis des bois pleins de soleil ;</l>
					<l n="66" num="5.11">Où nul visage encor ne hantait mon sommeil !</l>
					<l n="67" num="5.12">Ah ! Tu gisais inerte en mon sein, comme un lâche,</l>
					<l n="68" num="5.13">Mon cœur ! Rien ne pouvait t’émouvoir ! Un vautour,</l>
					<l n="69" num="5.14">De son bec implacable, aujourd’hui, sans relâche,</l>
					<l n="70" num="5.15">En te criant : « trop tard ! « te déchire à ton tour ! »</l>
				</lg>
				<lg n="6">
					<l n="71" num="6.1">Et tandis que Gemma, d’une étreinte qui broie,</l>
					<l n="72" num="6.2">Tourmente sa poitrine au repentir en proie,</l>
					<l n="73" num="6.3">La vieille chante, ainsi qu’en un rêve, tout bas :</l>
				</lg>
				<lg n="7">
					<l rhyme="none" n="74" num="7.1"><space quantity="4" unit="char"></space>« La pluie aux grains froids là-haut tombe à verse.</l>
					<l rhyme="none" n="75" num="7.2"><space quantity="4" unit="char"></space>Mon cher enfant dort, et moi je le berce,</l>
					<l rhyme="none" n="76" num="7.3"><space quantity="4" unit="char"></space>Dans son berceau fait de chêne et de plomb.</l>
					<l rhyme="none" n="77" num="7.4"><space quantity="4" unit="char"></space>J’entends un bruit sec qui gratte et qui perce.</l>
					<l rhyme="none" n="78" num="7.5"><space quantity="4" unit="char"></space>Tu dors, mon enfant, d’un sommeil bien long !</l>
					<l rhyme="none" n="79" num="7.6"><space quantity="4" unit="char"></space>— Mon enfant s’agite en ses draps de plomb.</l>
				</lg>
				<lg n="8">
					<l rhyme="none" n="80" num="8.1"><space quantity="4" unit="char"></space>« Un lourd cauchemar, mon enfant, t’agite.</l>
					<l rhyme="none" n="81" num="8.2"><space quantity="4" unit="char"></space>Ton berceau de chêne est un mauvais gîte.</l>
					<l rhyme="none" n="82" num="8.3"><space quantity="4" unit="char"></space>— Mon âme est partie, et vide est mon corps ! »</l>
				</lg>
				<lg n="9">
					<l n="83" num="9.1">Gemma sait que Mâhall est une pauvre folle</l>
					<l n="84" num="9.2">Qui l’aime, voilà tout, mais qu’on ne comprend pas.</l>
					<l n="85" num="9.3">Le malheur, dont blêmit sur son front l’auréole</l>
					<l n="86" num="9.4">Sinistre, la rend sourde aux vains mots. — elle entend</l>
					<l n="87" num="9.5">Son remords qui plus haut gronde, lui répétant :</l>
					<l n="88" num="9.6">« Trop tard ! Il est trop tard ! Rappelle-toi ! Déroule</l>
					<l n="89" num="9.7">Ce chapelet maudit de tes loisirs ingrats,</l>
					<l n="90" num="9.8">Quand les appels vers toi se succédaient en foule,</l>
					<l n="91" num="9.9">Quand sous tes seins, figés alors entre tes bras,</l>
					<l n="92" num="9.10">S’élargissait un vide aux voûtes taciturnes ;</l>
					<l n="93" num="9.11">Quand plaintes et parfums, débordant de leurs urnes,</l>
					<l n="94" num="9.12">Ne faisaient rien vibrer en toi, n’embaumaient rien !</l>
					<l n="95" num="9.13">À jamais à présent dans la nuit vengeresse,</l>
					<l n="96" num="9.14">Dans l’oubli de ta forme et du martyre ancien,</l>
					<l n="97" num="9.15">Il dort. Nul souvenir assidu ne l’oppresse.</l>
					<l n="98" num="9.16">Il a tout rejeté de la vie ; il est mort !</l>
					<l n="99" num="9.17">Eh bien ! Apprends l’amour ! Sous la dent qui te mord,</l>
					<l n="100" num="9.18">Regarde ruisseler tes pleurs expiatoires !</l>
					<l n="101" num="9.19">Vierge, tu souriais aux fièvres de l’amant ;</l>
					<l n="102" num="9.20">Fière de ta beauté, n’ayant pas d’autres gloires,</l>
					<l n="103" num="9.21">Tu ne savais répondre à l’ardeur d’un serment.</l>
					<l n="104" num="9.22">Mais femme, ta beauté de marbre encor s’est tue ;</l>
					<l n="105" num="9.23">Et tu ne sentais pas à tes pieds de statue</l>
					<l n="106" num="9.24">Retomber la prière et se fendre le cœur</l>
					<l n="107" num="9.25">De l’époux dont tu fus la cruelle pensée ;</l>
					<l n="108" num="9.26">Voilà que son image a vaincu ta torpeur,</l>
					<l n="109" num="9.27">Et qu’à son souvenir tu l’aimas, insensée ! »</l>
				</lg>
				<lg n="10">
					<l n="110" num="10.1">Elle songe. En dormant Mâhall chante tout bas :</l>
				</lg>
				<lg n="11">
					<l rhyme="none" n="111" num="11.1"><space quantity="4" unit="char"></space>« Un lourd cauchemar, mon enfant, t’agite.</l>
					<l rhyme="none" n="112" num="11.2"><space quantity="4" unit="char"></space>Ton berceau de chêne est un mauvais gîte.</l>
					<l rhyme="none" n="113" num="11.3"><space quantity="4" unit="char"></space>— Depuis que mon âme a laissé mon corps,</l>
					<l rhyme="none" n="114" num="11.4"><space quantity="4" unit="char"></space>Comme un vieux logis que le vent visite,</l>
					<l rhyme="none" n="115" num="11.5"><space quantity="4" unit="char"></space>J’appartiens entière aux âmes des morts ;</l>
					<l rhyme="none" n="116" num="11.6"><space quantity="4" unit="char"></space>Mon enfant, ton âme agite mon corps.</l>
				</lg>
				<lg n="12">
					<l rhyme="none" n="117" num="12.1"><space quantity="4" unit="char"></space>« Dans l’œil des enfants lisent leurs nourrices.</l>
					<l rhyme="none" n="118" num="12.2"><space quantity="4" unit="char"></space>Les morts ont aussi parfois leurs caprices. »</l>
				</lg>
				<lg n="13">
					<l n="119" num="13.1">— Lorsque chante Mâhall on ne l’écoute pas.</l>
					<l n="120" num="13.2">Gemma songe. « bonheur, plaisir, joie, espérance !</l>
					<l n="121" num="13.3">Quand l’angoisse nous tient et nous courbe impuissants,</l>
					<l n="122" num="13.4">Ces mots qu’on récusait sous leur vague apparence</l>
					<l n="123" num="13.5">Dans leur immensité sont tous éblouissants !</l>
					<l n="124" num="13.6">Oui, le regret, bien plus que l’espoir, aux musiques</l>
					<l n="125" num="13.7">Divines sait mêler des visions magiques !</l>
					<l n="126" num="13.8">Certe, il m’aimait jadis d’un amour effréné,</l>
					<l n="127" num="13.9">Usant sur moi l’effort des facultés mortelles,</l>
					<l n="128" num="13.10">L’homme qui vers l’espace aveugle s’est tourné,</l>
					<l n="129" num="13.11">Consumé par l’attente au froid de mes prunelles.</l>
					<l n="130" num="13.12">Si je n’ai rien compris alors, ni cet amour,</l>
					<l n="131" num="13.13">Ni ce vivace espoir de m’animer un jour,</l>
					<l n="132" num="13.14">Ni cette volonté, ni sa morne agonie,</l>
					<l n="133" num="13.15">D’où vient qu’à peine seul, mon cœur s’est éveillé,</l>
					<l n="134" num="13.16">Lentement, par degrés, de sa longue atonie ?</l>
					<l n="135" num="13.17">D’où vient qu’en mon désert un calice a brillé ?</l>
					<l n="136" num="13.18">Que l’idole aussitôt s’est changée en victime,</l>
					<l n="137" num="13.19">Et lit profondément dans l’infini sublime</l>
					<l n="138" num="13.20">De ce culte perdu qui l’embrase aujourd’hui ? »</l>
				</lg>
				<lg n="14">
					<l n="139" num="14.1">Et Gemma vers la chambre où le portrait l’attire</l>
					<l n="140" num="14.2">Se retourne, et revient s’arrêter devant lui.</l>
					<l n="141" num="14.3">Sur ses noirs vêtements pendent ses bras de cire.</l>
					<l n="142" num="14.4">— Mâhall reprend son rêve et sa chanson tout bas :</l>
				</lg>
				<lg n="15">
					<l rhyme="none" n="143" num="15.1"><space quantity="4" unit="char"></space>« Dans l’œil des enfants lisent leurs nourrices.</l>
					<l rhyme="none" n="144" num="15.2"><space quantity="4" unit="char"></space>Les morts ont aussi parfois leurs caprices.</l>
					<l rhyme="none" n="145" num="15.3"><space quantity="4" unit="char"></space>Lorsque tu souffrais, je sais une fleur</l>
					<l rhyme="none" n="146" num="15.4"><space quantity="4" unit="char"></space>Que je te donnais pour que tu guérisses ;</l>
					<l rhyme="none" n="147" num="15.5"><space quantity="4" unit="char"></space>Son baiser rendait ton sommeil meilleur.</l>
					<l rhyme="none" n="148" num="15.6"><space quantity="4" unit="char"></space>— Mon enfant demande une étrange fleur !</l>
					<l rhyme="none" n="149" num="15.7"><space quantity="4" unit="char"></space>« Il sait des secrets plus vieux que la tombe !</l>
					<l rhyme="none" n="150" num="15.8"><space quantity="4" unit="char"></space>— La pluie aux grains froids sur mes membres tombe… »</l>
				</lg>
				<lg n="16">
					<l n="151" num="16.1">Les yeux sur le portrait, Gemma ne l’entend pas ;</l>
					<l n="152" num="16.2">Son corps est immobile et sa lèvre est muette,</l>
					<l n="153" num="16.3">Mais sa détresse ainsi toujours gonfle son sein :</l>
				</lg>
				<lg n="17">
					<l n="154" num="17.1">— « ah ! Dans ces yeux ouverts une âme se reflète !</l>
					<l n="155" num="17.2">Et j’y vois clairement tourbillonner l’essaim</l>
					<l n="156" num="17.3">Des vœux et des mépris qui maintenant me rongent !</l>
					<l n="157" num="17.4">Tyranniques regards ! Comme en les miens ils plongent !</l>
					<l n="158" num="17.5">Beaucoup plus haut en moi que les yeux d’un vivant,</l>
					<l n="159" num="17.6">Ils parlent nuit et jour et m’ont enfin soumise ;</l>
					<l n="160" num="17.7">Et j’y revois au jeu d’un reflet décevant</l>
					<l n="161" num="17.8">Tous les édens murés de la terre promise !</l>
					<l n="162" num="17.9">Mais les inassouvis s’endorment-ils jamais ?</l>
					<l n="163" num="17.10">Leur donnes-tu l’oubli, toi qui nous le promets,</l>
					<l n="164" num="17.11">Ô mort ? — lui, voudra-t-il m’oublier dans ta fosse ?</l>
					<l n="165" num="17.12">Il n’aimait point alors ! Seule, je sais aimer,</l>
					<l n="166" num="17.13">Moi qui sens que ta voix comme toute autre est fausse,</l>
					<l n="167" num="17.14">Et qu’à l’heure où sur moi le plomb va se fermer,</l>
					<l n="168" num="17.15">Mon amour éternel, martyrisant délice,</l>
					<l n="169" num="17.16">M’écrasera les seins de son royal cilice !</l>
					<l n="170" num="17.17">Mais non ! S’il était vrai que pour l’éternité</l>
					<l n="171" num="17.18">Rien ne survît, ô mort ! De l’humaine amertume ;</l>
					<l n="172" num="17.19">Si malgré toi là-bas il n’a rien emporté,</l>
					<l n="173" num="17.20">Qui donc met dans ses yeux comme un appel posthume ? »</l>
				</lg>
				<lg n="18">
					<l n="174" num="18.1">Et Gemma se rapproche et touche le portrait,</l>
					<l n="175" num="18.2">Dont une clarté douce anime chaque trait</l>
					<l n="176" num="18.3">Et la bouche qui luit plus pourpre et semble humide.</l>
					<l n="177" num="18.4">— Mâhall sur l’escabeau recommence tout bas :</l>
				</lg>
				<lg n="19">
					<l rhyme="none" n="178" num="19.1"><space quantity="4" unit="char"></space>« Il sait des secrets plus vieux que la tombe !</l>
					<l rhyme="none" n="179" num="19.2"><space quantity="4" unit="char"></space>— La pluie aux grains froids sur mes membres tombe.</l>
					<l rhyme="none" n="180" num="19.3"><space quantity="4" unit="char"></space>Oh ! Rouge est la fleur ! Mortel son poison !</l>
					<l rhyme="none" n="181" num="19.4"><space quantity="4" unit="char"></space>Pourquoi la veut-il ? Pour quelle hécatombe ?</l>
					<l rhyme="none" n="182" num="19.5"><space quantity="4" unit="char"></space>Moi, dans la forêt, je cours sans raison ! …</l>
					<l rhyme="none" n="183" num="19.6"><space quantity="4" unit="char"></space>Un mort veut baiser, ô fleur ! Ton poison !</l>
				</lg>
				<lg n="20">
					<l rhyme="none" n="184" num="20.1"><space quantity="4" unit="char"></space>« Hier, j’ai frotté de poison sa bouche.</l>
					<l rhyme="none" n="185" num="20.2"><space quantity="4" unit="char"></space>Dans son cadre il dort : que nul ne le touche !</l>
					<l rhyme="none" n="186" num="20.3"><space quantity="4" unit="char"></space>— Le désir des morts dompte les vivants… »</l>
				</lg>
				<lg n="21">
					<l n="187" num="21.1">— « non, non ! — pense Gemma, — quelque obstiné fluide</l>
					<l n="188" num="21.2">Jaillit de ces yeux noirs qui ne me quittent pas.</l>
					<l n="189" num="21.3">La mort a des secrets plus anciens que la tombe !</l>
					<l n="190" num="21.4">L’éclat qui m’enveloppe et sous qui je succombe,</l>
					<l n="191" num="21.5">Quel peintre aurait donc su le fixer dans ces yeux ?</l>
					<l n="192" num="21.6">Non ! N’est-ce pas plutôt qu’un être toujours triste</l>
					<l n="193" num="21.7">Me poursuit par delà son exil soucieux ?</l>
					<l n="194" num="21.8">Qu’un amour idéal auquel rien ne résiste</l>
					<l n="195" num="21.9">Triomphe enfin après que les sens sont glacés ?</l>
					<l n="196" num="21.10">Ah ! S’il en est ainsi, chère ombre ! C’est assez !</l>
					<l n="197" num="21.11">Cesse de t’agiter ! Ou vengeance ou victoire,</l>
					<l n="198" num="21.12">Vois, je t’aime aujourd’hui plus que tu ne m’aimais !</l>
					<l n="199" num="21.13">Apaise-toi ! Tu peux me sourire et me croire !</l>
					<l n="200" num="21.14">Plus que ne fit le tien, mon cœur saigne à jamais ;</l>
					<l n="201" num="21.15">Et j’expie ! Et j’attends l’heure du dernier râle,</l>
					<l n="202" num="21.16">Où je m’envolerai vers ta poitrine pâle,</l>
					<l n="203" num="21.17">Plus riche de baisers et de larmes de sang,</l>
					<l n="204" num="21.18">Que toi du désespoir de tes élans stériles ! »</l>
				</lg>
				<lg n="22">
					<l n="205" num="22.1">— Une flamme qui tremble et qui va faiblissant</l>
					<l n="206" num="22.2">Fait courir sur les murs les ombres plus fébriles ;</l>
					<l n="207" num="22.3">Et la vieille Mâhall chante encore tout bas :</l>
				</lg>
				<lg n="23">
					<l rhyme="none" n="208" num="23.1"><space quantity="4" unit="char"></space>« À travers un cadre il tendait la bouche.</l>
					<l rhyme="none" n="209" num="23.2"><space quantity="4" unit="char"></space>J’ai frotté la fleur. Que nul ne le touche !</l>
					<l rhyme="none" n="210" num="23.3"><space quantity="4" unit="char"></space>— Le désir des morts dompte les vivants.</l>
					<l rhyme="none" n="211" num="23.4"><space quantity="4" unit="char"></space>Dans mon vieux corps vide et qui branle aux vents,</l>
					<l rhyme="none" n="212" num="23.5"><space quantity="4" unit="char"></space>Les âmes des morts veillent les vivants !</l>
					<l rhyme="none" n="213" num="23.6"><space quantity="4" unit="char"></space>— Ainsi qu’un portrait, dans un cadre il couche ! »</l>
				</lg>
				<lg n="24">
					<l n="214" num="24.1">Gemma vers le tableau n’a plus à faire un pas :</l>
					<l n="215" num="24.2">Elle se penche et joint sa lèvre chaude à celle</l>
					<l n="216" num="24.3">Du portrait, qui lui semble avoir alors souri ;</l>
					<l n="217" num="24.4">Puis recule, frissonne un court moment, chancelle,</l>
					<l n="218" num="24.5">Et tombe empoisonnée, et morte, sans un cri !</l>
				</lg>
			</div></body></text></TEI>