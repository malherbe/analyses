<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES LÈVRES CLOSES</title>
				<title type="medium">Édition électronique</title>
				<author key="DRX">
					<name>
						<forename>Léon</forename>
						<surname>DIERX</surname>
					</name>
					<date from="1838" to="1912">1838-1912</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement métrique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1986 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DRX_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les lèvres closes</title>
						<author>Léon Dierx</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/leondierxleslevrescloses.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres complètes, tome I</title>
						<author>Léon Dierx</author>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k2722190.r=l%C3%A9on+dierx.langFR</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ALPHONSE LEMERRE ÉDITEUR</publisher>
							<date when="1925">1925</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1867">1867</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>Les majuscules accentuées ont été vérifiées.</p>
				<p>Les majuscules en début de vers ont été restituées</p>
				<p>Le formatage strophique a été rétabli</p>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
				<p>Le poème "la Beauté" a été ajouté à partir d’une saisie manuelle</p>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DRX23">
				<head type="main">PROLOGUE</head>
				<lg n="1">
					<l n="1" num="1.1">J’ai détourné mes yeux de l’homme et de la vie,</l>
					<l n="2" num="1.2">Et mon âme a rôdé sous l’herbe des tombeaux.</l>
					<l n="3" num="1.3">J’ai détrompé mon cœur de toute humaine envie,</l>
					<l n="4" num="1.4">Et je l’ai dispersé dans les bois par lambeaux.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">J’ai voulu vivre sourd aux voix des multitudes,</l>
					<l n="6" num="2.2">Comme un aïeul couvert de silence et de nuit,</l>
					<l n="7" num="2.3">Et pareil aux sentiers qui vont aux solitudes,</l>
					<l n="8" num="2.4">Avoir des songes frais que nul désir ne suit.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Mais le sépulcre en moi laissa filtrer ses rêves,</l>
					<l n="10" num="3.2">Et d’ici j’ai tenté d’impossibles efforts.</l>
					<l n="11" num="3.3">Les forêts ? Leur angoisse a traversé les grèves,</l>
					<l n="12" num="3.4">Et j’ai senti passer leurs souffles dans mon corps.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">Le soupir qui s’amasse au bord des lèvres closes</l>
					<l n="14" num="4.2">A fait l’obsession du calme où j’aspirais ;</l>
					<l n="15" num="4.3">Comme un manoir hanté de visions moroses,</l>
					<l n="16" num="4.4">J’ai recélé l’effroi des rendez-vous secrets.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1">Et depuis, au milieu des douleurs et des fêtes,</l>
					<l n="18" num="5.2">Morts qui voulez parler, taciturnes vivants,</l>
					<l n="19" num="5.3">Bois solennels ! J’entends vos âmes inquiètes</l>
					<l n="20" num="5.4">Sans cesse autour de moi frissonner dans les vents.</l>
				</lg>
			</div></body></text></TEI>