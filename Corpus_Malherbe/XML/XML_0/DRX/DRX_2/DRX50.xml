<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES LÈVRES CLOSES</title>
				<title type="medium">Édition électronique</title>
				<author key="DRX">
					<name>
						<forename>Léon</forename>
						<surname>DIERX</surname>
					</name>
					<date from="1838" to="1912">1838-1912</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement métrique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1986 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DRX_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les lèvres closes</title>
						<author>Léon Dierx</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/leondierxleslevrescloses.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres complètes, tome I</title>
						<author>Léon Dierx</author>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k2722190.r=l%C3%A9on+dierx.langFR</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ALPHONSE LEMERRE ÉDITEUR</publisher>
							<date when="1925">1925</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1867">1867</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>Les majuscules accentuées ont été vérifiées.</p>
				<p>Les majuscules en début de vers ont été restituées</p>
				<p>Le formatage strophique a été rétabli</p>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
				<p>Le poème "la Beauté" a été ajouté à partir d’une saisie manuelle</p>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DRX50">
				<head type="main">JAMAIS</head>
				<opener>
					<salute>A Frédéric Plessis.</salute>
				</opener>
				<lg n="1">
					<l n="1" num="1.1">Amour ! Dans tous les temps des hommes t’ont chanté !</l>
					<l n="2" num="1.2">Inventeurs d’un mensonge, ils auront tous porté</l>
					<l n="3" num="1.3">Le cercle ardent qui reste aux martyrs, et la gloire</l>
					<l n="4" num="1.4">D’avoir su faire un dieu de toi, forme illusoire ! »</l>
					<l n="5" num="1.5">Comme en son souterrain, tel, encor ce jour-là,</l>
					<l n="6" num="1.6">Le démon qui l’habite en mon esprit parla.</l>
					<l n="7" num="1.7">Et depuis bien des mois il désolait ma vie ;</l>
					<l n="8" num="1.8">Et les anges joyeux que chaque amant convie</l>
					<l n="9" num="1.9">À rallumer le temple et l’autel, tout confus</l>
					<l n="10" num="1.10">S’arrêtaient devant l’hôte aux méprisants refus.</l>
				</lg>
				<lg n="2">
					<l n="11" num="2.1">Et lorsque vint le soir, ce fossoyeur fidèle</l>
					<l n="12" num="2.2">De nos virilités qu’il abat d’un coup d’aile,</l>
					<l n="13" num="2.3">Suivant la passion qu’insulta le dédain,</l>
					<l n="14" num="2.4">Comme un voleur j’ouvris la grille du jardin ;</l>
					<l n="15" num="2.5">Et tremblant à mes pas sur le sable qui crie,</l>
					<l n="16" num="2.6">L’oreille au moindre choc dans la branche flétrie,</l>
					<l n="17" num="2.7">Plus lourd encor, plus lâche encor, plus lentement</l>
					<l n="18" num="2.8">Encor, je m’avançai près des murs, comprimant</l>
					<l n="19" num="2.9">Avec force à la fois la révolte et la honte</l>
					<l n="20" num="2.10">Du souvenir navré qui dans le fiel remonte.</l>
					<l n="21" num="2.11">— Ah ! Ce jour-là, plutôt qu’un autre, quel espoir</l>
					<l n="22" num="2.12">Avait comme un parfum embaumé l’air du soir ?</l>
					<l n="23" num="2.13">Quand le soleil fondit dans sa vapeur cuivrée,</l>
					<l n="24" num="2.14">Quel écho, m’imposant l’illusion qu’il crée,</l>
					<l n="25" num="2.15">M’avait dit : c’est l’aurore ! On t’appelle ! Suis-moi !</l>
					<l n="26" num="2.16">Quel nuage avait pris, pour raffermir ma foi,</l>
					<l n="27" num="2.17">L’incarnat féminin qu’un sourire illumine ?</l>
					<l n="28" num="2.18">Quelle heure de jadis aux fleuraisons d’hermine</l>
					<l n="29" num="2.19">Résonna plus vibrante en mon amer passé ?</l>
					<l n="30" num="2.20">Quelle ivresse m’avait jusque là-bas poussé ?</l>
					<l n="31" num="2.21">Et quand je fus au bout de la trop chère allée</l>
					<l n="32" num="2.22">Pleine encor des senteurs de ses cheveux, peuplée</l>
					<l n="33" num="2.23">De blancs spectres de robe aux détours des chemins ;</l>
					<l n="34" num="2.24">Quand, appuyant ma face à la vitre et mes mains,</l>
					<l n="35" num="2.25">Je regardai la salle où mon âme était née</l>
					<l n="36" num="2.26">Sous les yeux violets qui l’avaient condamnée,</l>
					<l n="37" num="2.27">Qu’espérais-je y revoir, sinon le dur éclair</l>
					<l n="38" num="2.28">D’un implacable arrêt qu’on regrave en ma chair ;</l>
					<l n="39" num="2.29">Sinon la joie unique et toujours bien formelle</l>
					<l n="40" num="2.30">De vivre et d’être jeune, et de se savoir belle,</l>
					<l n="41" num="2.31">Et de rire en pensant au mal qu’ont fait ses yeux ?</l>
				</lg>
				<lg n="3">
					<l n="42" num="3.1">Certes, les nefs n’ont pas l’aspect religieux</l>
					<l n="43" num="3.2">Que me montrait la chambre aux lueurs amorties ;</l>
					<l n="44" num="3.3">Et sans doute, entr’ouvrant ses griffes pressenties,</l>
					<l n="45" num="3.4">L’ange des maux subits, tout proche, et sans pitié,</l>
					<l n="46" num="3.5">Attentif, épiait l’œuvre faite à moitié.</l>
					<l n="47" num="3.6">Au milieu des coussins elle était là, couchée ;</l>
					<l n="48" num="3.7">Et par instants sa main, de l’ombre détachée,</l>
					<l n="49" num="3.8">Chassait on ne sait quel péril d’un geste prompt ;</l>
					<l n="50" num="3.9">Mais sous un autre vol se retournait son front ;</l>
					<l n="51" num="3.10">Et des bouches que rien n’arrête ou ne déjoue</l>
					<l n="52" num="3.11">Marquaient un baiser rouge au milieu de sa joue.</l>
					<l n="53" num="3.12">Sa main gauche dormait dans celles du vieillard,</l>
					<l n="54" num="3.13">Qui tout auprès, debout, la couvrant d’un regard</l>
					<l n="55" num="3.14">Sec et morne, semblait chercher dans sa mémoire</l>
					<l n="56" num="3.15">Les couleurs d’un visage auquel il ne peut croire.</l>
					<l n="57" num="3.16">Mais le sang de la vie avait seul déserté</l>
					<l n="58" num="3.17">Ce visage. Jamais l’éclat de la beauté</l>
					<l n="59" num="3.18">N’auréola plus fière et plus pâle figure.</l>
					<l n="60" num="3.19">Elle était là, les cils levés, sans un murmure,</l>
					<l n="61" num="3.20">Et paraissait attendre et provoquer sans peur</l>
					<l n="62" num="3.21">Les doigts de l’invisible et lugubre sculpteur</l>
					<l n="63" num="3.22">Qui sur les corps quittés se délecte et s’obstine.</l>
					<l n="64" num="3.23">Celle qui, m’opposant l’allégresse enfantine,</l>
					<l n="65" num="3.24">Par ses yeux où mourait mon plus charmé désir</l>
					<l n="66" num="3.25">M’apprit l’horreur de voir les étoiles s’enfuir ;</l>
					<l n="67" num="3.26">Celle-là dont l’empreinte au fond de ma pensée,</l>
					<l n="68" num="3.27">Le jour où je jurai de l’avoir effacée,</l>
					<l n="69" num="3.28">S’installa plus riante et défiant l’oubli ;</l>
					<l n="70" num="3.29">Celle-là n’était rien que le songe aboli</l>
					<l n="71" num="3.30">Dans l’éparse vapeur de larmes bien taries.</l>
					<l n="72" num="3.31">Mais le fleuve est plus large, amour, où tu charries</l>
					<l n="73" num="3.32">Aujourd’hui mon trésor plus splendide au néant !</l>
					<l n="74" num="3.33">Et des cyprès sans fin au feuillage géant</l>
					<l n="75" num="3.34">Bordent tous les sentiers dont je parcours la trace.</l>
					<l n="76" num="3.35">Ce n’est plus son sourire adorable ou sa grâce</l>
					<l n="77" num="3.36">Qui de loin me traverse en creusant mon regret ;</l>
					<l n="78" num="3.37">Ma raison, aujourd’hui, sans trouble évoquerait</l>
					<l n="79" num="3.38">Les boucles, les regards et la bouche ravie</l>
					<l n="80" num="3.39">Où j’avais cru noués tous les fils de ma vie.</l>
					<l n="81" num="3.40">Fantôme d’autrefois, à jamais détrôné,</l>
					<l n="82" num="3.41">Je souris à mon tour, et je t’ai pardonné.</l>
					<l n="83" num="3.42">Cheveux que les parfums choisissaient pour image,</l>
					<l n="84" num="3.43">Prunelles, dont jadis je m’étais cru le mage,</l>
					<l n="85" num="3.44">Lèvres qui m’emplissiez de chants intérieurs,</l>
					<l n="86" num="3.45">Anciennes visions qui revivez ailleurs !</l>
					<l n="87" num="3.46">Non, je n’ai jamais vu ni pleuré vos reliques ;</l>
					<l n="88" num="3.47">Mon destin n’avait pas, ô contours chimériques !</l>
					<l n="89" num="3.48">Sondé les profondeurs blêmes du désespoir,</l>
					<l n="90" num="3.49">Et, corbeau funéraire au fond d’un vieux manoir,</l>
					<l n="91" num="3.50">Sinistre suzerain des demeures désertes,</l>
					<l n="92" num="3.51">Dans les cendres traîné ses ailerons inertes.</l>
					<l n="93" num="3.52">Vous m’aviez abusé, mes pleurs avaient menti ;</l>
					<l n="94" num="3.53">Je n’avais pas souffert ; je n’avais pas senti</l>
					<l n="95" num="3.54">Tes ongles sous ma peau, tes flammes dans mes veines,</l>
					<l n="96" num="3.55">Amour, dieu languissant, couronné de verveines !</l>
					<l n="97" num="3.56">Seulement ce soir-là j’ai compris, et j’ai bu</l>
					<l n="98" num="3.57">Les philtres abhorrés d’un hanap inconnu.</l>
					<l n="99" num="3.58">En un instant, ce soir, des siècles d’amertume</l>
					<l n="100" num="3.59">Ont en moi refoulé leur dévorante écume ;</l>
					<l n="101" num="3.60">Et je sais à présent, et pour l’éternité,</l>
					<l n="102" num="3.61">Ce que c’est que le poids d’un cœur épouvanté</l>
					<l n="103" num="3.62">Où tu trônes, muet, tendant tes sombres ailes,</l>
					<l n="104" num="3.63">Amour, dieu frémissant, couronné d’immortelles !</l>
				</lg>
				<lg n="4">
					<l n="105" num="4.1">Oui, devant ce visage au teint de marbre, aux yeux</l>
					<l n="106" num="4.2">Sublimes, obscurcis de secrets orgueilleux ;</l>
					<l n="107" num="4.3">Devant le solennel silence de ces lèvres</l>
					<l n="108" num="4.4">Qu’agitait le travail accéléré des fièvres ;</l>
					<l n="109" num="4.5">Devant cette victime offerte sans combats</l>
					<l n="110" num="4.6">Au messager divin dont elle entend les pas,</l>
					<l n="111" num="4.7">Un sanglot me remplit pour l’existence entière ;</l>
					<l n="112" num="4.8">Et sur mon passé mort, c’est la mourante altière</l>
					<l n="113" num="4.9">Et sans rivale en moi qui régna, dans sa paix,</l>
					<l n="114" num="4.10">Et dans sa mer d’ébène, immuable à jamais.</l>
					<l n="115" num="4.11">— Ah ! Dans des yeux profonds si nos yeux savent lire,</l>
					<l n="116" num="4.12">En ce moment, les siens révélaient le martyre</l>
					<l n="117" num="4.13">De la vierge que brûle un indicible amour,</l>
					<l n="118" num="4.14">Que l’angoisse a déjà consumée à son tour,</l>
					<l n="119" num="4.15">Et qui dans sa noblesse et sa pudeur s’exile,</l>
					<l n="120" num="4.16">Tandis qu’en sa fierté périt son corps tranquille.</l>
					<l n="121" num="4.17">Et si, pendant le cours d’un dernier entretien,</l>
					<l n="122" num="4.18">Ce soir-là son regard eût plongé dans le mien,</l>
					<l n="123" num="4.19">Certe, elle eût tressailli d’y voir jaillir vers elle</l>
					<l n="124" num="4.20">Un feu lui renvoyant par la même étincelle</l>
					<l n="125" num="4.21">Ma douleur infinie en son mal infini.</l>
					<l n="126" num="4.22">Et si la mort qui plane autour d’un front terni</l>
					<l n="127" num="4.23">Laisse parfois le sang y refluer peut-être,</l>
					<l n="128" num="4.24">Comme au sommet brumeux la rougeur vient renaître,</l>
					<l n="129" num="4.25">Qui donc pourrait la faire obéir à sa loi ?</l>
					<l n="130" num="4.26">Qui donc peut commander aux dieux, si ce n’est toi,</l>
					<l n="131" num="4.27">Amour, dieu tout puissant, roi des métamorphoses ?</l>
					<l n="132" num="4.28">Dans la bise du moins tu m’as dicté ces choses.</l>
					<l n="133" num="4.29">L’impossible, c’était d’être là. Je t’ai cru.</l>
				</lg>
				<lg n="5">
					<l n="134" num="5.1">Sous les arbres, alors, sans penser j’ai couru.</l>
					<l n="135" num="5.2">Il m’en souvient, quelqu’un avait ouvert la grille ;</l>
					<l n="136" num="5.3">Des voix avaient parlé du père et de la fille ;</l>
					<l n="137" num="5.4">Deux hommes noirs venaient ; sur leurs pas ténébreux</l>
					<l n="138" num="5.5">Je m’élançai sans bruit, et j’entrai derrière eux.</l>
					<l n="139" num="5.6">Le père à ses côtés les laissa prendre place ;</l>
					<l n="140" num="5.7">Ils chuchotaient, tenant la pauvre main si lasse,</l>
					<l n="141" num="5.8">Secouèrent la tête, et leur art fut à bout.</l>
					<l n="142" num="5.9">Lui, toujours, regardait sa fille, voilà tout.</l>
					<l n="143" num="5.10">Puis j’entendis rouvrir derrière moi la porte ;</l>
					<l n="144" num="5.11">L’un d’eux disait : « demain cette enfant sera morte. »</l>
					<l n="145" num="5.12">Le corridor avait glissé des souffles froids,</l>
					<l n="146" num="5.13">Et nous restâmes seuls dans la chambre, tous trois.</l>
				</lg>
				<lg n="6">
					<l n="147" num="6.1">Qu’ai-je dit au vieillard, alors ? Quelle croyance</l>
					<l n="148" num="6.2">Eut-il en moi, celui dont la vaste science</l>
					<l n="149" num="6.3">Se reniait, vaincue, et qui ne priait pas ?</l>
					<l n="150" num="6.4">Sur quoi me jugea-t-il enchanteur du trépas ?</l>
					<l n="151" num="6.5">Je l’ignore. Insensé ! Savais-je aussi moi-même</l>
					<l n="152" num="6.6">Ce que je murmurais, dans cette nuit suprême,</l>
					<l n="153" num="6.7">Sur la tempe où posait le bout d’un doigt mortel ?</l>
					<l n="154" num="6.8">Je sais que je parlais ; qu’un sacrilège appel,</l>
					<l n="155" num="6.9">S’exaltant à mesure au remords qui l’enivre,</l>
					<l n="156" num="6.10">La suppliait de croire à l’amour, et de vivre ;</l>
					<l n="157" num="6.11">De se reprendre au seuil de ce ciel qui nous ment ;</l>
					<l n="158" num="6.12">De ressaisir enfin la force à mon serment,</l>
					<l n="159" num="6.13">Et de ressusciter d’un bond, dans la fanfare</l>
					<l n="160" num="6.14">Qu’un bonheur triomphal ici-bas lui prépare !</l>
					<l n="161" num="6.15">— Mourir ! Non, si des yeux pareils se sont fermés</l>
					<l n="162" num="6.16">Jamais, c’est que des yeux ne les ont point aimés !</l>
					<l n="163" num="6.17">Si pareille beauté s’est pour toujours éteinte,</l>
					<l n="164" num="6.18">C’est que deux bras plus forts ne l’avaient pas étreinte !</l>
					<l n="165" num="6.19">C’est qu’un amour fervent, aux longues volontés,</l>
					<l n="166" num="6.20">N’avait pas repoli ces yeux désenchantés,</l>
					<l n="167" num="6.21">Ni rappelé l’instinct dans la fibre dissoute !</l>
					<l n="168" num="6.22">Ou bien, c’est qu’ils voulaient mourir, ces yeux, sans doute,</l>
					<l n="169" num="6.23">C’est qu’il voulait dormir sous l’herbe, ce beau corps !</l>
					<l n="170" num="6.24">Éloquence et prière, impérieux efforts,</l>
					<l n="171" num="6.25">Tout se brisa devant son entêté silence.</l>
					<l n="172" num="6.26">Rien un instant n’a pu troubler la somnolence</l>
					<l n="173" num="6.27">Du funeste brouillard qui submergeait déjà</l>
					<l n="174" num="6.28">Ces grands lacs dilatés où mon malheur plongea.</l>
					<l n="175" num="6.29">Elle entendait pourtant. De ses lèvres hautaines,</l>
					<l n="176" num="6.30">Par trois fois, à la fin, deux syllabes lointaines</l>
					<l n="177" num="6.31">Vinrent frapper en moi, tranchantes comme un fer.</l>
					<l n="178" num="6.32">Le mot que vont hurlant les damnés dans l’enfer :</l>
					<l n="179" num="6.33">Jamais ! Jamais ! Jamais ! Par trois fois dans mon âme</l>
					<l n="180" num="6.34">J’en ai senti le coup qui glaçait toute flamme.</l>
					<l n="181" num="6.35">Et la nuit, d’heure en heure, opprimait son beau sein ;</l>
					<l n="182" num="6.36">Et plus terrifié qu’un nocturne assassin,</l>
					<l n="183" num="6.37">Plus muet que son père au désespoir stérile,</l>
					<l n="184" num="6.38">Jusqu’au jour, avec lui, sur son sommeil fébrile</l>
					<l n="185" num="6.39">Je veillai, dans mes poings pressant ses doigts roidis.</l>
					<l n="186" num="6.40">Et la lampe trembla sous l’aube ; et j’entendis</l>
					<l n="187" num="6.41">Dans le jardin chanter les oiseaux sur les branches.</l>
					<l n="188" num="6.42">La croisée allongea vers nous ses lignes blanches ;</l>
					<l n="189" num="6.43">Alors un long soupir nous prévint d’un réveil ;</l>
					<l n="190" num="6.44">Et, comme en saluant l’approche du soleil,</l>
					<l n="191" num="6.45">Elle sourit, tournée un peu vers la fenêtre.</l>
					<l n="192" num="6.46">Un frisson de plaisir courut dans tout son être ;</l>
					<l n="193" num="6.47">Et, se dressant debout dans ses vêtements blancs,</l>
					<l n="194" num="6.48">Aux rayons du matin elle ouvrit ses bras lents.</l>
					<l n="195" num="6.49">Un flot d’or ruissela sur elle, et la lumière</l>
					<l n="196" num="6.50">Qui l’éblouit, fermant pour toujours sa paupière,</l>
					<l n="197" num="6.51">La renversa rigide et morte sur les draps.</l>
				</lg>
				<lg n="7">
					<l n="198" num="7.1">Et vous nous entouriez, funèbres apparats !</l>
					<l n="199" num="7.2">Et l’âcre odeur flottait de l’encens et des cierges ;</l>
					<l n="200" num="7.3">Et sur son lit couvert des symboles des vierges,</l>
					<l n="201" num="7.4">Ses traits inanimés s’ennoblissaient plus purs ;</l>
					<l n="202" num="7.5">Et le jour s’embrunit ; et rapide, à pas sûrs,</l>
					<l n="203" num="7.6">La nuit montait partout, poussant par intervalles</l>
					<l n="204" num="7.7">Des adieux prolongés sous les portes des salles ;</l>
					<l n="205" num="7.8">Et le vieillard, sans voix, sans pleurs, sans mouvement,</l>
					<l n="206" num="7.9">Vers la morte toujours regardait fixement ;</l>
					<l n="207" num="7.10">Et moi, je m’enfonçais dans l’affreuse inertie</l>
					<l n="208" num="7.11">D’un corps vide sur qui pèse une ombre épaissie.</l>
					<l n="209" num="7.12">Et tout à coup, voilà qu’au fond de la noirceur</l>
					<l n="210" num="7.13">Où je sombrais, surgit une étrange lueur,</l>
					<l n="211" num="7.14">Qui s’accrut, m’inondant de sa clarté divine,</l>
					<l n="212" num="7.15">Et qu’un frais hosanna chanta dans ma poitrine.</l>
					<l n="213" num="7.16">Dans un vertigineux élan qui m’enlevait</l>
					<l n="214" num="7.17">Je bondis, et penché sur le fatal chevet,</l>
					<l n="215" num="7.18">Je criai comme un fou ces paroles avides :</l>
					<l n="216" num="7.19">— « l’aurore vient nous prendre au bas des cieux livides !</l>
					<l n="217" num="7.20">Toi qui fus inflexible alors que tu vivais,</l>
					<l n="218" num="7.21">Qui mourus en vouant ma vie aux dieux mauvais,</l>
					<l n="219" num="7.22">Métella ! N’est-ce pas, tu ne m’es plus rebelle ?</l>
					<l n="220" num="7.23">Tu vois tout, et ton âme en liberté m’appelle.</l>
					<l n="221" num="7.24">Elle m’aime à la fin ! Je le sais. Je la sens</l>
					<l n="222" num="7.25">Qui vante en moi le ciel des amours renaissants.</l>
					<l n="223" num="7.26">Eh bien ! Du seuil certain de la patrie ouverte</l>
					<l n="224" num="7.27">Pour toi ! Sous mon pardon de l’injure soufferte</l>
					<l n="225" num="7.28">Jadis ; au nom sacré de cet amour promis ;</l>
					<l n="226" num="7.29">Si cette âme erre encore en tes nerfs endormis,</l>
					<l n="227" num="7.30">Enfreins l’ordre odieux ! Revis une seconde !</l>
					<l n="228" num="7.31">Je t’adjure ! Qu’un mot, qu’un signe au moins réponde !</l>
					<l n="229" num="7.32">Est-ce toi qui passas dans mon rêve éperdu ?</l>
					<l n="230" num="7.33">Métella ! Métella ! Cette fois, m’aimes-tu ? »</l>
				</lg>
				<lg n="8">
					<l n="231" num="8.1">Et j’achevais à peine un geste qui l’implore,</l>
					<l n="232" num="8.2">Que je vis remuer cette bouche incolore ;</l>
					<l n="233" num="8.3">Et dans le monde atroce où je me rabîmais,</l>
					<l n="234" num="8.4">Une voix sans nom dit : jamais ! Jamais ! Jamais !</l>
				</lg>
			</div></body></text></TEI>