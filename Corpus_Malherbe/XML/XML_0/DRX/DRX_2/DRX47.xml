<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES LÈVRES CLOSES</title>
				<title type="medium">Édition électronique</title>
				<author key="DRX">
					<name>
						<forename>Léon</forename>
						<surname>DIERX</surname>
					</name>
					<date from="1838" to="1912">1838-1912</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement métrique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1986 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DRX_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les lèvres closes</title>
						<author>Léon Dierx</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/leondierxleslevrescloses.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres complètes, tome I</title>
						<author>Léon Dierx</author>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k2722190.r=l%C3%A9on+dierx.langFR</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ALPHONSE LEMERRE ÉDITEUR</publisher>
							<date when="1925">1925</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1867">1867</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>Les majuscules accentuées ont été vérifiées.</p>
				<p>Les majuscules en début de vers ont été restituées</p>
				<p>Le formatage strophique a été rétabli</p>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
				<p>Le poème "la Beauté" a été ajouté à partir d’une saisie manuelle</p>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DRX47">
				<head type="main">FLOTS DES MERS</head>
				<opener>
					<salute>À Émile Bergerat.</salute>
				</opener>
				<lg n="1">
					<l n="1" num="1.1">Flots qui portiez la vie au seuil obscur des temps,</l>
					<l n="2" num="1.2">Qui la roulez toujours en embryons flottants</l>
					<l n="3" num="1.3">Dans le flux et reflux du primitif servage,</l>
					<l n="4" num="1.4">Éternels escadrons cabrés sur un rivage</l>
					<l n="5" num="1.5">Ou contre un roc, l’écume au poitrail, flots des mers,</l>
					<l n="6" num="1.6">Que vos bruits et leur rythme immortel me sont chers !</l>
					<l n="7" num="1.7">Partout où recouvrant récifs, galets de sables,</l>
					<l n="8" num="1.8">Escaladant en vain les bords infranchissables,</l>
					<l n="9" num="1.9">Vous brisez votre élan tout aussitôt repris,</l>
					<l n="10" num="1.10">Vous aurez subjugué les cœurs et les esprits.</l>
					<l n="11" num="1.11">L’ordre immémorial au même assaut vous lance,</l>
					<l n="12" num="1.12">Et vous n’aurez connu ni repos ni silence</l>
					<l n="13" num="1.13">Sur ce globe où chaque être, après un court effort,</l>
					<l n="14" num="1.14">Pour l’oublier se fait immobile et s’endort.</l>
					<l n="15" num="1.15">Enfanteurs de la nue éclatante ou qui gronde,</l>
					<l n="16" num="1.16">Flots des mers, ennemis de tous les caps du monde,</l>
					<l n="17" num="1.17">Vous leur jetez avec vos limons coutumiers</l>
					<l n="18" num="1.18">Son rêve et son histoire épars en des fumiers.</l>
					<l n="19" num="1.19">Dans vos sillons mouvants submergés par vos cimes</l>
					<l n="20" num="1.20">Vous ensevelissez et bercez vos victimes,</l>
					<l n="21" num="1.21">Ainsi qu’en le berçant vous poussez devant vous</l>
					<l n="22" num="1.22">L’animalcule aveugle éclos dans vos remous.</l>
					<l n="23" num="1.23">A tous les sols marins votre appel se répète.</l>
					<l n="24" num="1.24">Mais sous l’azur limpide ou pendant la tempête,</l>
					<l n="25" num="1.25">Doux murmure expirant sur la grève, ou fureur</l>
					<l n="26" num="1.26">Retentissante au fond des vieux gouffres d’horreur,</l>
					<l n="27" num="1.27">C’est à jamais un chant de détresse et de plainte.</l>
					<l n="28" num="1.28">Perpétuels martyrs refoulés dans l’étreinte,</l>
					<l n="29" num="1.29">Armée aux rangs serrés qui monte et qui descend,</l>
					<l n="30" num="1.30">Un désir est en vous qui se sait impuissant.</l>
					<l n="31" num="1.31">Que la nuit s’épaississe ou bien que le jour croisse,</l>
					<l n="32" num="1.32">Vous accourez de loin, vous rapportez l’angoisse,</l>
					<l n="33" num="1.33">Aux pieds de vos remparts certains vous revenez,</l>
					<l n="34" num="1.34">Et mêlez aux rumeurs des ans disséminés</l>
					<l n="35" num="1.35">Les soupirs inconnus, les voix de ceux qu’on pleure.</l>
					<l n="36" num="1.36">La vôtre est toujours jeune et seule ici demeure.</l>
					<l n="37" num="1.37">Messagers du chaos, damnés de l’action,</l>
					<l n="38" num="1.38">Serviteurs du secret de la création,</l>
					<l n="39" num="1.39">Votre spectacle auguste et sa vaste harmonie</l>
					<l n="40" num="1.40">Émouvront plus que tout la pensée infinie.</l>
					<l n="41" num="1.41">Nous n’aurons combattu qu’une heure ; incessamment,</l>
					<l n="42" num="1.42">Vous clamez dans l’espace un plus ancien tourment !</l>
					<l n="43" num="1.43">Ah ! n’est-il pas celui d’une âme emprisonnée</l>
					<l n="44" num="1.44">Qui, ne sachant pourquoi ni comment elle est née,</l>
					<l n="45" num="1.45">Le demande en battant les murs de l’horizon ?</l>
					<l n="46" num="1.46">Flots sacrés ! L’univers est encor la prison !</l>
					<l n="47" num="1.47">Nous avons beau fouiller et le ciel et la terre,</l>
					<l n="48" num="1.48">Tout n’est que doute, énigme, illusion, mystère.</l>
				</lg>
			</div></body></text></TEI>