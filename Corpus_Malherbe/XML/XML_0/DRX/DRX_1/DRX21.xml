<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÈMES ET POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="DRX">
					<name>
						<forename>Léon</forename>
						<surname>DIERX</surname>
					</name>
					<date from="1838" to="1912">1838-1912</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement métrique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2449 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DRX_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poèmes Et Poésies</title>
						<author>Léon Dierx</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/leondierxpoemesetpoesies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres complètes, tome I</title>
						<author>Léon Dierx</author>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k2722190.r=l%C3%A9on+dierx.langFR</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ALPHONSE LEMERRE ÉDITEUR</publisher>
							<date when="1925">1925</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1864">1864</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>Les majuscules accentuées ont été vérifiées.</p>
				<p>Les majuscules en début de vers ont été restituées</p>
				<p>Le formatage strophique a été rétabli</p>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DRX21">
				<head type="main">L’Épreuve</head>
				<lg n="1">
					<l n="1" num="1.1">L’Invisible, celui qui règne dans les cieux,</l>
					<l n="2" num="1.2">Assembla ses enfants pour lui chanter sa gloire ;</l>
					<l n="3" num="1.3">Et Satan était là, qui se dressait près d’eux.</l>
				</lg>
				<lg n="2">
					<l n="4" num="2.1">Et le Très-haut lui dit : « D’où viens-tu ? — mon histoire</l>
					<l n="5" num="2.2">Est vieille, répondit l’adversaire : j’ai fait</l>
					<l n="6" num="2.3">Tout le tour de ton œuvre avec mon aile noire.</l>
				</lg>
				<lg n="3">
					<l n="7" num="3.1">« J’ai délié l’esprit que ta règle étouffait ;</l>
					<l n="8" num="3.2">J’ai pourri le bon grain, j’ai récolté l’ivraie ;</l>
					<l n="9" num="3.3">Tes anges ont raison de chanter, en effet !</l>
				</lg>
				<lg n="4">
					<l n="10" num="4.1">« Leur louange est mensonge et ma parole est vraie :</l>
					<l n="11" num="4.2">L’esprit de l’homme est plein d’aversion pour toi.</l>
					<l n="12" num="4.3">Nu ne t’aime, hors ceux que ta rancune effraie.</l>
				</lg>
				<lg n="5">
					<l n="13" num="5.1">— « Tu n’as considéré que l’incomplète foi,</l>
					<l n="14" num="5.2">Dit l’éternel, de ceux que l’épreuve terrasse.</l>
					<l n="15" num="5.3">Les cœurs simples et purs sont heureux sous ma loi.</l>
				</lg>
				<lg n="6">
					<l n="16" num="6.1">— « Sur un fumier, couvert d’une lèpre vorace,</l>
					<l n="17" num="6.2">Un être, dit Satan, sans amis, sans espoir,</l>
					<l n="18" num="6.3">Survivait, en opprobre à tous ceux de sa race.</l>
				</lg>
				<lg n="7">
					<l n="19" num="7.1">« C’était un homme. Nu, gisant, horrible à voir,</l>
					<l n="20" num="7.2">Avec un caillou plat il grattait ses ulcères,</l>
					<l n="21" num="7.3">Le jour durant sans pain, et sans sommeil le soir.</l>
				</lg>
				<lg n="8">
					<l n="22" num="8.1">« Si pour te réjouir les maux sont nécessaires,</l>
					<l n="23" num="8.2">Il avait en cela cent fois bien mérité ;</l>
					<l n="24" num="8.3">Car ce juste n’avait point d’égal en misères.</l>
				</lg>
				<lg n="9">
					<l n="25" num="9.1">« Loin de tous, en dehors des murs d’une cité,</l>
					<l n="26" num="9.2">Dans le pays de Hus où le péché domine,</l>
					<l n="27" num="9.3">Il maudissait la vie et ton iniquité.</l>
				</lg>
				<lg n="10">
					<l n="28" num="10.1">« Oui, tordu par son mal, mangé par la vermine,</l>
					<l n="29" num="10.2">Vile forme sans nom parmi les animaux,</l>
					<l n="30" num="10.3">Il ouvrait ce regard que la haine illumine. »</l>
				</lg>
				<lg n="11">
					<l n="31" num="11.1">Le Très-fort dit : « Qu’importe une chair en lambeaux ?</l>
					<l n="32" num="11.2">Le juste est celui seul qui lui-même s’oublie,</l>
					<l n="33" num="11.3">Et ne contemple pas uniquement ses maux.</l>
				</lg>
				<lg n="12">
					<l n="34" num="12.1">— « Celui-ci n’avait point une âme ensevelie</l>
					<l n="35" num="12.2">Dans son propre tourment, si monstrueux qu’il fût :</l>
					<l n="36" num="12.3">Les pleurs universels l’avaient toute remplie.</l>
				</lg>
				<lg n="13">
					<l n="37" num="13.1">« Moi, le rôdeur sournois et qui veille à l’affût,</l>
					<l n="38" num="13.2">Le fomenteur subtil des mauvaises pensées,</l>
					<l n="39" num="13.3">Je pris ce malheureux effroyable pour but.</l>
				</lg>
				<lg n="14">
					<l n="40" num="14.1">« Et ses chairs tout d’abord furent cicatrisées ;</l>
					<l n="41" num="14.2">Je le guéris sur l’heure, et le soutins debout</l>
					<l n="42" num="14.3">N’ayant plus souvenir de ses hontes passées.</l>
				</lg>
				<lg n="15">
					<l n="43" num="15.1">« Il regarda la cuve où s’amoncelle et bout</l>
					<l n="44" num="15.2">L’épais fourmillement des hommes, et qui fume ;</l>
					<l n="45" num="15.3">Puis l’horizon qui n’a commencement ni bout ;</l>
				</lg>
				<lg n="16">
					<l n="46" num="16.1">« Et je vis qu’il restait dévoré d’amertume</l>
					<l n="47" num="16.2">En songeant à l’angoisse où ton peuple croupit</l>
					<l n="48" num="16.3">Sous ton œil clos au fond d’une insondable brume.</l>
				</lg>
				<lg n="17">
					<l n="49" num="17.1">« Je rendis la jeunesse à son corps décrépit ;</l>
					<l n="50" num="17.2">Je dressai l’arc noueux et brisé de son torse ;</l>
					<l n="51" num="17.3">Après, j’enveloppai ses membres d’un habit.</l>
				</lg>
				<lg n="18">
					<l n="52" num="18.1">« La ville flamboyait comme une immense amorce.</l>
					<l n="53" num="18.2">Je lui dis : « Va ! La vie est bonne ; sois heureux ! »</l>
					<l n="54" num="18.3">Et je fis resplendir la beauté sur sa force.</l>
				</lg>
				<lg n="19">
					<l n="55" num="19.1">« Il y marcha, parmi des mendiants poudreux ;</l>
					<l n="56" num="19.2">Et je vis, le suivant pas à pas à la piste,</l>
					<l n="57" num="19.3">Qu’il se sentait imbu du fiel de leurs yeux creux.</l>
				</lg>
				<lg n="20">
					<l n="58" num="20.1">— « Eh bien ! Dit l’être unique à Satan : qu’il assiste</l>
					<l n="59" num="20.2">Son frère, celui-là qui voit l’appel d’autrui !</l>
					<l n="60" num="20.3">Cet homme s’en ira joyeux, s’il était triste.</l>
				</lg>
				<lg n="21">
					<l n="61" num="21.1">— « L’aumône, il se peut bien, fait sourire celui</l>
					<l n="62" num="21.2">Qui donnant un denier se dit qu’il te le prête,</l>
					<l n="63" num="21.3">Et ne place un secours qu’au taux de ton appui.</l>
				</lg>
				<lg n="22">
					<l n="64" num="22.1">« Je connais la prudence entre toutes secrète !</l>
					<l n="65" num="22.2">Lui, supputait, au fond de lui-même, combien</l>
					<l n="66" num="22.3">Sont là, pour qui jamais table ou moisson n’est prête.</l>
				</lg>
				<lg n="23">
					<l n="67" num="23.1">« Morne, il allait, disant : « Je ne possède rien ! »</l>
					<l n="68" num="23.2">Je l’avais rendu jeune et fort ; je le fis riche</l>
					<l n="69" num="23.3">A ne pouvoir compter ses troupeaux ni son bien.</l>
				</lg>
				<lg n="24">
					<l n="70" num="24.1">« Quiconque errait, sordide, et tel qu’un chien sans niche,</l>
					<l n="71" num="24.2">Vendangea dans sa vigne et glana dans son champ.</l>
					<l n="72" num="24.3">Mais l’ortie est tenace au cœur que l’on défriche !</l>
				</lg>
				<lg n="25">
					<l n="73" num="25.1">« Si prodige fût-il, l’avare et le méchant</l>
					<l n="74" num="25.2">Pullulent sur la terre ; et lui, voyait sans cesse</l>
					<l n="75" num="25.3">De maigres doigts nouveaux à ses mains s’accrochant.</l>
				</lg>
				<lg n="26">
					<l n="76" num="26.1">« Comprenant que pour un à qui l’on fait largesse</l>
					<l n="77" num="26.2">Mille crieront, vers toi les bras en vain dressés,</l>
					<l n="78" num="26.3">Généreux, il faisait l’aumône avec tristesse.</l>
				</lg>
				<lg n="27">
					<l n="79" num="27.1">— « Ils ont l’amour, les fils de ceux que j’ai chassés !</l>
					<l n="80" num="27.2">Et la femme a des yeux où j’ai mis ma lumière.</l>
					<l n="81" num="27.3">Pour aimer le très-bon, qu’ils s’aiment ! C’est assez !</l>
				</lg>
				<lg n="28">
					<l n="82" num="28.1">— « Parfois un astre brille au fond d’une paupière ;</l>
					<l n="83" num="28.2">Et l’amour est vraiment le reflet de l’Éden !</l>
					<l n="84" num="28.3">A qui veut l’entrevoir, un ange crie : « Arrière ! »</l>
				</lg>
				<lg n="29">
					<l n="85" num="29.1">« Comme un ressouvenir du souriant jardin,</l>
					<l n="86" num="29.2">Il la chercha, l’ivresse ineffablement pure.</l>
					<l n="87" num="29.3">Mais la beauté qui charme a le cruel dédain.</l>
				</lg>
				<lg n="30">
					<l n="88" num="30.1">« Il était beau. Toujours il vivait la torture</l>
					<l n="89" num="30.2">De ceux que la laideur a marqués en naissant</l>
					<l n="90" num="30.3">Pour servir à l’amour d’éternelle pâture.</l>
				</lg>
				<lg n="31">
					<l n="91" num="31.1">« Il aima. Sa révolte encore allait croissant ;</l>
					<l n="92" num="31.2">Car, doué d’un esprit que la justice affame,</l>
					<l n="93" num="31.3">Les fureurs des jaloux le tenaient frémissant.</l>
				</lg>
				<lg n="32">
					<l n="94" num="32.1">« C’est le suprême don que l’amour d’une femme.</l>
					<l n="95" num="32.2">Mais tout cœur qui se donne est pour d’autres perdu,</l>
					<l n="96" num="32.3">Et seul en est joyeux l’égoïste ou l’infâme.</l>
				</lg>
				<lg n="33">
					<l n="97" num="33.1">« Il fut aimé. Mais lui, s’assombrissait, mordu</l>
					<l n="98" num="33.2">Par tous les désespoirs que la beauté méprise,</l>
					<l n="99" num="33.3">Par le cri furieux de l’amour entendu.</l>
				</lg>
				<lg n="34">
					<l n="100" num="34.1">« Si grand qu’un bonheur soit, pour l’homme sans traîtrise,</l>
					<l n="101" num="34.2">S’il est fait du malheur d’un autre, n’est-ce pas</l>
					<l n="102" num="34.3">La coupe de poison que la main ivre a prise ?</l>
				</lg>
				<lg n="35">
					<l n="103" num="35.1">« Et je riais de voir que tout fruit mûr, là-bas,</l>
					<l n="104" num="35.2">Est sûrement percé par un ver invisible ;</l>
					<l n="105" num="35.3">Et qu’il revomissait les plus puissants appâts.</l>
				</lg>
				<lg n="36">
					<l n="106" num="36.1">« Et je prenais toujours ce cœur simple pour cible.</l>
					<l n="107" num="36.2">J’élargissais encor la part de son bonheur,</l>
					<l n="108" num="36.3">Sans qu’un remercîment pour toi lui fût possible.</l>
				</lg>
				<lg n="37">
					<l n="109" num="37.1">— « Mon œuvre est bon ainsi qu’il est ! dit le seigneur.</l>
					<l n="110" num="37.2">— Et les routes du ciel aux hommes sont fermées !</l>
					<l n="111" num="37.3">Je sais cela, reprit le parfait raisonneur.</l>
				</lg>
				<lg n="38">
					<l n="112" num="38.1">« Les rêves les plus chers aux foules affamées,</l>
					<l n="113" num="38.2">Lui, les réalisait. Il fut roi sur les rois</l>
					<l n="114" num="38.3">Qui se disent choisis par le dieu des armées.</l>
				</lg>
				<lg n="39">
					<l n="115" num="39.1">« Le meurtre est le plaisir où tes fils sont adroits,</l>
					<l n="116" num="39.2">C’est la gloire de ceux qui portent la couronne ;</l>
					<l n="117" num="39.3">Mais la sienne chargeait son front, si tu m’en crois.</l>
				</lg>
				<lg n="40">
					<l n="118" num="40.1">« O créateur d’Adam ! Quel concert t’environne !</l>
					<l n="119" num="40.2">De tous les avortons du couple rejeté,</l>
					<l n="120" num="40.3">Qui donc plus que ce roi se lamenta ? Personne !</l>
				</lg>
				<lg n="41">
					<l n="121" num="41.1">« Léguant l’arrêt divin à leur postérité,</l>
					<l n="122" num="41.2">Tous ont gémi, les forts, les lâches, les victimes.</l>
					<l n="123" num="41.3">Nul n’a vécu plus pâle et plus épouvanté,</l>
				</lg>
				<lg n="42">
					<l n="124" num="42.1">« Que ce puissant, par moi sorti des noirs abîmes</l>
					<l n="125" num="42.2">Pour être sur la terre, et plus qu’eux, revêtu</l>
					<l n="126" num="42.3">Du glacial frisson pris à toutes les cimes !</l>
				</lg>
				<lg n="43">
					<l n="127" num="43.1">« Le plus affreux supplice est l’extrême vertu.</l>
					<l n="128" num="43.2">Son grand sanglot déborde, et monte dans les âges</l>
					<l n="129" num="43.3">Vers celui qui toujours dans son ombre s’est tu.</l>
				</lg>
				<lg n="44">
					<l n="130" num="44.1">« Écoute ce qu’il dit, le sage entre les sages :</l>
					<l n="131" num="44.2">« Tout n’est que vanité, cendre, fumée ou vent !</l>
					<l n="132" num="44.3">« Et rien ne sert, travaux, fortune, apprentissages !</l>
				</lg>
				<lg n="45">
					<l n="133" num="45.1">« Tout passe et meurt, le fou, l’inepte et le savant !</l>
					<l n="134" num="45.2">« Il n’est rien de nouveau ; tout vient par aventure !</l>
					<l n="135" num="45.3">« L’état d’un mort vaut mieux que l’état d’un vivant !</l>
				</lg>
				<lg n="46">
					<l n="136" num="46.1">« Toutes sortes de maux rongent la créature,</l>
					<l n="137" num="46.2">« Et de tous la pensée est le pire tourment ;</l>
					<l n="138" num="46.3">« Et l’amour est amer plus que la sépulture ! »</l>
				</lg>
				<lg n="47">
					<l n="139" num="47.1">« Voilà ton œuvre ! Il est risible assurément</l>
					<l n="140" num="47.2">De te voir pour cela convoquer tes phalanges</l>
					<l n="141" num="47.3">A t’appeler Très-haut, Très-fort et Très-clément !</l>
				</lg>
				<lg n="48">
					<l n="142" num="48.1">« Dis-leur donc devant moi de chanter tes louanges ! »</l>
					<l n="143" num="48.2">— Mais celui dont le trône est au fond des sept cieux,</l>
					<l n="144" num="48.3">Ne répondit plus rien au corrupteur des anges ;</l>
				</lg>
				<lg n="49">
					<l n="145" num="49.1">L’invisible resta là-haut silencieux !</l>
				</lg>
			</div></body></text></TEI>