<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÈMES ET POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="DRX">
					<name>
						<forename>Léon</forename>
						<surname>DIERX</surname>
					</name>
					<date from="1838" to="1912">1838-1912</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement métrique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2449 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DRX_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poèmes Et Poésies</title>
						<author>Léon Dierx</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/leondierxpoemesetpoesies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres complètes, tome I</title>
						<author>Léon Dierx</author>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k2722190.r=l%C3%A9on+dierx.langFR</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ALPHONSE LEMERRE ÉDITEUR</publisher>
							<date when="1925">1925</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1864">1864</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>Les majuscules accentuées ont été vérifiées.</p>
				<p>Les majuscules en début de vers ont été restituées</p>
				<p>Le formatage strophique a été rétabli</p>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DRX10">
				<head type="main">Soleil couchant</head>
				<opener>
					<salute>À Monsieur Édouard Hervé.</salute>
				</opener>
				<lg n="1">
					<l n="1" num="1.1">Aux bords retentissants des plages écumeuses</l>
					<l n="2" num="1.2">Pleines de longs soupirs mêlés de lourds sanglots,</l>
					<l n="3" num="1.3">Sous le déroulement monotone des flots ;</l>
					<l n="4" num="1.4">Près des gouffres remplis des falaises brumeuses ;</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">A l’heure où le soleil, ainsi qu’un roi cruel</l>
					<l n="6" num="2.2">Qui veut parer de draps sanglants ses funérailles,</l>
					<l n="7" num="2.3">Se déchire et secoue au dehors ses entrailles ;</l>
					<l n="8" num="2.4">A l’heure où lentement l’ombre envahit le ciel ;</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Un homme se tenait silencieux. La côte</l>
					<l n="10" num="3.2">Était déserte. Lui, debout, d’un œil amer</l>
					<l n="11" num="3.3">Il regardait tomber l’astre rouge à la mer ;</l>
					<l n="12" num="3.4">Et sa pensée aussi déferlait, sombre et haute.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">Ah ! Ce n’était pas l’homme au sortir de l’Éden,</l>
					<l n="14" num="4.2">Fils encore innocent d’une race nouvelle ;</l>
					<l n="15" num="4.3">En qui la vie afflue, à qui Dieu se révèle,</l>
					<l n="16" num="4.4">Et qui pour tous les maux n’a qu’un mâle dédain ;</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1">L’homme essayant sa force au seuil des premiers âges,</l>
					<l n="18" num="5.2">Libre dans l’univers libre et grand comme lui ;</l>
					<l n="19" num="5.3">Défiant l’avenir, et dont l’œil ébloui</l>
					<l n="20" num="5.4">Reflète l’horizon des vierges paysages ;</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1">Plein d’un orgueil sans peur et d’un espoir sans fin ;</l>
					<l n="22" num="6.2">Et dans sa beauté fière à qui tout se confie,</l>
					<l n="23" num="6.3">Sur la création odorante et ravie</l>
					<l n="24" num="6.4">Passant majestueux sous un signe divin ;</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1">C’était l’homme vieilli des races séculaires,</l>
					<l n="26" num="7.2">Fils de la lassitude et des labeurs déçus,</l>
					<l n="27" num="7.3">Et qui, désabusé des dons qu’il a reçus,</l>
					<l n="28" num="7.4">A des printemps plus froids que les hivers polaires ;</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1">Qui, remuant la cendre immense du passé,</l>
					<l n="30" num="8.2">Initié tout jeune au mensonge des rêves,</l>
					<l n="31" num="8.3">A vu la vanité de ses luttes sans trêves,</l>
					<l n="32" num="8.4">Et sans but désormais s’en va le front baissé ;</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1">Qui, ployant sous le poids d’insupportables chaînes,</l>
					<l n="34" num="9.2">Se connaît tout entier dans la joie ou les pleurs,</l>
					<l n="35" num="9.3">Rassasié du rire autant que des douleurs ;</l>
					<l n="36" num="9.4">Sans élans pour le bien, et pour le mal sans haines ;</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1">C’était l’homme rongé par l’angoisse ; vaincu</l>
					<l n="38" num="10.2">Sous l’énervant dégoût de sa propre impuissance ;</l>
					<l n="39" num="10.3">Et fatal héritier d’une aride science,</l>
					<l n="40" num="10.4">Contempteur de la vie avant d’avoir vécu.</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1">En vain il proclamait son génie et sa gloire !</l>
					<l n="42" num="11.2">L’ennui met sur ses bras le plomb du châtiment ;</l>
					<l n="43" num="11.3">Et son âme qu’il raille, hélas ! Plus tristement</l>
					<l n="44" num="11.4">Se rendort à ces bruits de pompe dérisoire.</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1">Stupide et vil, trempé d’inutiles sueurs,</l>
					<l n="46" num="12.2">En vain il rit des dieu qu’ont adorés ses pères,</l>
					<l n="47" num="12.3">Et s’élance aux profits du fond de ses repaires,</l>
					<l n="48" num="12.4">Les doigts crispés, les yeux pleins d’obliques lueurs.</l>
				</lg>
				<lg n="13">
					<l n="49" num="13.1">Car le veau d’or, ce dieu comme un autre implacable,</l>
					<l n="50" num="13.2">A l’enfer de Midas le regarde marcher ;</l>
					<l n="51" num="13.3">Honneur, amour, vertu, tout ce qu’il veut toucher,</l>
					<l n="52" num="13.4">Se change sous ses mains en cet or qui l’accable.</l>
				</lg>
				<lg n="14">
					<l n="53" num="14.1">Oui, ce dieu, son premier délire, et son dernier,</l>
					<l n="54" num="14.2">Le plus riche en autels, le plus riche en apôtres,</l>
					<l n="55" num="14.3">Le plus vieux, qui vit naître et mourir tous les autres,</l>
					<l n="56" num="14.4">Avant le chant du coq il va le renier.</l>
				</lg>
				<lg n="15">
					<l n="57" num="15.1">Il va le renier comme eux tous. Dans les nues</l>
					<l n="58" num="15.2">Il l’enverra siéger, livide, avec les dieux</l>
					<l n="59" num="15.3">Morts maintenant, jadis beaux, fiers et radieux,</l>
					<l n="60" num="15.4">Qui sur les monts sacrés vivaient en troupes nues ;</l>
				</lg>
				<lg n="16">
					<l n="61" num="16.1">Près des spectres blafards abandonnés du jour,</l>
					<l n="62" num="16.2">Qui planent en lambeaux sur les glaces du pôle,</l>
					<l n="63" num="16.3">Et qu’un souffle inconnu, les poussant par l’épaule,</l>
					<l n="64" num="16.4">Promène dans l’horreur des exils sans retour.</l>
				</lg>
				<lg n="17">
					<l n="65" num="17.1">Pas un ne reviendra ! Le vent de l’ironie</l>
					<l n="66" num="17.2">A balayé partout l’ambition du beau.</l>
					<l n="67" num="17.3">Sur le dernier autel plus désert qu’un tombeau</l>
					<l n="68" num="17.4">L’herbe croît. Il n’est plus de divine agonie !</l>
				</lg>
				<lg n="18">
					<l n="69" num="18.1">Plus d’esprits enivrés ! Plus d’hymnes, plus d’encens !</l>
					<l n="70" num="18.2">Plus de convives ceints de verveine et de roses !</l>
					<l n="71" num="18.3">Plus d’apôtre en extase, et plus d’apothéoses !</l>
					<l n="72" num="18.4">Plus de soupirs poussés hors du monde des sens !</l>
				</lg>
				<lg n="19">
					<l n="73" num="19.1">Sur la montagne en feu nul ne se transfigure,</l>
					<l n="74" num="19.2">Et pour quelque dépouille aux fétides odeurs,</l>
					<l n="75" num="19.3">L’homme consumera ses dernières ardeurs</l>
					<l n="76" num="19.4">Sous un ciel qui n’a plus la sublime envergure.</l>
				</lg>
				<lg n="20">
					<l n="77" num="20.1">Dans un air sans échos sa voix s’éteint. Voilà</l>
					<l n="78" num="20.2">Qu’il méprise à la fin sa chair comme son âme,</l>
					<l n="79" num="20.3">Et que, toujours brûlé d’une invisible flamme,</l>
					<l n="80" num="20.4">Il retourne aux abris chantants qu’il dépeupla.</l>
				</lg>
				<lg n="21">
					<l n="81" num="21.1">Mais les transports qui font la jeunesse si belle</l>
					<l n="82" num="21.2">Reviennent-ils jamais gonfler les cœurs flétris !</l>
					<l n="83" num="21.3">Les pleurs, les repentirs, les plaintes et les cris</l>
					<l n="84" num="21.4">Ont-ils jamais ému l’impassible Cybèle !</l>
				</lg>
				<lg n="22">
					<l n="85" num="22.1">Nature indifférente, au secret douloureux,</l>
					<l n="86" num="22.2">Prés aux vertes senteurs, forêts aux noirs mystères,</l>
					<l n="87" num="22.3">Monts couronnés de pins ou de neiges austères,</l>
					<l n="88" num="22.4">Vous êtes sans pitié, comme tous les heureux !</l>
				</lg>
				<lg n="23">
					<l n="89" num="23.1">L’homme a levé sur vous sa hache sacrilège ;</l>
					<l n="90" num="23.2">Sur vous il s’est rué follement, et sa voix</l>
					<l n="91" num="23.3">A maudit le silence injurieux des bois</l>
					<l n="92" num="23.4">Où meurt le vain appel du désir qui l’assiège :</l>
				</lg>
				<lg n="24">
					<l n="93" num="24.1">A jamais il a fui tout ce monde enchanté</l>
					<l n="94" num="24.2">Qu’aux rayons de la lune, au fond des solitudes,</l>
					<l n="95" num="24.3">On voyait s’essayer aux molles attitudes</l>
					<l n="96" num="24.4">Sous l’œil ardent d’un faune ivre de volupté.</l>
				</lg>
				<lg n="25">
					<l n="97" num="25.1">Quand Pan mourut, un cri monta de rive en rive ;</l>
					<l n="98" num="25.2">Dans la foi du poète il retentit encor.</l>
					<l n="99" num="25.3">Comme un chasseur perdu qui sonne en vain du cor,</l>
					<l n="100" num="25.4">L’homme court sans qu’un son en réponse n’arrive.</l>
				</lg>
				<lg n="26">
					<l n="101" num="26.1">Las de lui-même aussi, voilà que haletant,</l>
					<l n="102" num="26.2">Comme Sisyphe sous le rocher qui l’écrase,</l>
					<l n="103" num="26.3">Il s’arrête, et qu’à l’heure où l’occident s’embrase,</l>
					<l n="104" num="26.4">Il sent les maux soufferts revivre en un instant.</l>
				</lg>
				<lg n="27">
					<l n="105" num="27.1">C’est une heure sinistre et pleine de vertiges.</l>
					<l n="106" num="27.2">Depuis les premiers jours, sa magique splendeur</l>
					<l n="107" num="27.3">Nous étreint, et nous fait sonder la profondeur</l>
					<l n="108" num="27.4">D’un passé qui tressaille en fulgurants vestiges.</l>
				</lg>
				<lg n="28">
					<l n="109" num="28.1">Comme l’astre qui fond en longs fleuves pourprés</l>
					<l n="110" num="28.2">Dont les reflets au loin baignent les nobles cimes,</l>
					<l n="111" num="28.3">Le cœur de l’homme saigne en plongeant aux abîmes</l>
					<l n="112" num="28.4">Où ses regrets encor hurlent désespérés.</l>
				</lg>
				<lg n="29">
					<l n="113" num="29.1">Mais aujourd’hui, devant la chute glorieuse</l>
					<l n="114" num="29.2">Du globe dont l’éclat brillait sur son berceau,</l>
					<l n="115" num="29.3">Ce n’est plus vers l’éden dont il gardait le sceau</l>
					<l n="116" num="29.4">Qu’il se reporte au bout d’une ardeur furieuse.</l>
				</lg>
				<lg n="30">
					<l n="117" num="30.1">Ce n’est plus son enfance au cantique lointain</l>
					<l n="118" num="30.2">Dont le ressouvenir en ses fêtes s’exhale ;</l>
					<l n="119" num="30.3">Ni la branche arborée en palme triomphale</l>
					<l n="120" num="30.4">Qu’il pleure, en gémissant sur sa part du destin.</l>
				</lg>
				<lg n="31">
					<l n="121" num="31.1">Ce n’est plus un saint nom qu’il invoque ou qu’il prie,</l>
					<l n="122" num="31.2">Hélas ! Et ce n’est plus, même quand vient le soir,</l>
					<l n="123" num="31.3">La mort, son épouvante et son dernier espoir,</l>
					<l n="124" num="31.4">Qu’il appelle, sentant toute source tarie.</l>
				</lg>
				<lg n="32">
					<l n="125" num="32.1">Sous la dent sans pitié du démon qui le mord</l>
					<l n="126" num="32.2">Rien ne ranime plus sa force ou son courage ;</l>
					<l n="127" num="32.3">Et voilà qu’il se tait sans un reste de rage,</l>
					<l n="128" num="32.4">Car il ne peut plus croire à ta promesse, ô mort !</l>
				</lg>
				<lg n="33">
					<l n="129" num="33.1">Tu ne peux rien sur l’âme ; et l’impossible envie</l>
					<l n="130" num="33.2">Toujours l’assoiffera de bonheur, n’importe où ;</l>
					<l n="131" num="33.3">Tu ne peux l’engloutir aussi dans quelque trou ;</l>
					<l n="132" num="33.4">Ce n’est pas le repos qui par toi nous convie !</l>
				</lg>
				<lg n="34">
					<l n="133" num="34.1">— Et le soleil, jetant sa suprême clarté,</l>
					<l n="134" num="34.2">Laissa l’homme, le front plus bas, les yeux plus mornes ;</l>
					<l n="135" num="34.3">Et l’esprit descendu dans une nuit sans bornes</l>
					<l n="136" num="34.4">Sous l’effrayant fardeau de son éternité.</l>
				</lg>
			</div></body></text></TEI>