<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÈMES ET POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="DRX">
					<name>
						<forename>Léon</forename>
						<surname>DIERX</surname>
					</name>
					<date from="1838" to="1912">1838-1912</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement métrique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2449 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DRX_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poèmes Et Poésies</title>
						<author>Léon Dierx</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/leondierxpoemesetpoesies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres complètes, tome I</title>
						<author>Léon Dierx</author>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k2722190.r=l%C3%A9on+dierx.langFR</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ALPHONSE LEMERRE ÉDITEUR</publisher>
							<date when="1925">1925</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1864">1864</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>Les majuscules accentuées ont été vérifiées.</p>
				<p>Les majuscules en début de vers ont été restituées</p>
				<p>Le formatage strophique a été rétabli</p>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DRX11">
				<head type="main">L’œil</head>
				<lg n="1">
					<l n="1" num="1.1">Sous l’épais treillis des feuilles tremblantes,</l>
					<l n="2" num="1.2">Au plus noir du bois la lune descend ;</l>
					<l n="3" num="1.3">Et des troncs moussus aux cimes des plantes,</l>
					<l n="4" num="1.4">Son regard fluide et phosphorescent</l>
					<l n="5" num="1.5">Fait trembler aux bords des corolles closes</l>
					<l n="6" num="1.6"><space quantity="12" unit="char"></space>Les larmes des choses.</l>
				</lg>
				<lg n="2">
					<l n="7" num="2.1">Lorsque l’homme oublie au fond du sommeil,</l>
					<l n="8" num="2.2">La vie éternelle est dans les bois sombres ;</l>
					<l n="9" num="2.3">Dans les taillis veufs du brûlant soleil</l>
					<l n="10" num="2.4">Sous la lune encor palpitent leurs ombres,</l>
					<l n="11" num="2.5">Et jamais leur âme, au bout d’un effort,</l>
					<l n="12" num="2.6"><space quantity="12" unit="char"></space>Jamais ne s’endort !</l>
				</lg>
				<lg n="3">
					<l n="13" num="3.1">Le clair de la lune en vivantes gerbes</l>
					<l n="14" num="3.2">Sur les hauts gazons filtre des massifs.</l>
					<l n="15" num="3.3">Et les fronts penchés, les pieds dans les herbes,</l>
					<l n="16" num="3.4">Les filles des eaux, en essaims pensifs,</l>
					<l n="17" num="3.5">Sous les saules blancs en rond sont assises,</l>
					<l n="18" num="3.6"><space quantity="12" unit="char"></space>Formes indécises.</l>
				</lg>
				<lg n="4">
					<l n="19" num="4.1">La lune arrondit son disque lointain</l>
					<l n="20" num="4.2">Sur le bois vêtu d’un brouillard magique</l>
					<l n="21" num="4.3">Et dans une eau blême aux reflets d’étain ;</l>
					<l n="22" num="4.4">Et ce vieil étang, miroir nostalgique,</l>
					<l n="23" num="4.5">Semble ton grand œil, ô nature ! Hélas !</l>
					<l n="24" num="4.6"><space quantity="12" unit="char"></space>Semble un grand œil las.</l>
				</lg>
			</div></body></text></TEI>