<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÈMES ET POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="DRX">
					<name>
						<forename>Léon</forename>
						<surname>DIERX</surname>
					</name>
					<date from="1838" to="1912">1838-1912</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement métrique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2449 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DRX_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poèmes Et Poésies</title>
						<author>Léon Dierx</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/leondierxpoemesetpoesies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres complètes, tome I</title>
						<author>Léon Dierx</author>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k2722190.r=l%C3%A9on+dierx.langFR</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ALPHONSE LEMERRE ÉDITEUR</publisher>
							<date when="1925">1925</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1864">1864</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>Les majuscules accentuées ont été vérifiées.</p>
				<p>Les majuscules en début de vers ont été restituées</p>
				<p>Le formatage strophique a été rétabli</p>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DRX14">
				<head type="main">Stella Vespera</head>
				<div type="section" n="1">
					<head type="number">I</head>
					<lg n="1">
						<l n="1" num="1.1">L’image de Florence en moi s’était dressée</l>
						<l n="2" num="1.2">Ce soir-là. De nouveau, j’y suivais en pensée</l>
						<l n="3" num="1.3">Les pas silencieux de Stella Vespera.</l>
						<l n="4" num="1.4">Sœur des merveilles d’art qu’un beau siècle inspira,</l>
						<l n="5" num="1.5">Elle m’avait charmé comme un pur marbre antique,</l>
						<l n="6" num="1.6">Et me hantait depuis, fantôme énigmatique.</l>
					</lg>
					<lg n="2">
						<l n="7" num="2.1">On disait sa famille oubliée. Un secret</l>
						<l n="8" num="2.2">Cachait sa vie à tous. On ne la rencontrait</l>
						<l n="9" num="2.3">Que dans quelque musée illustre. Sur sa trace,</l>
						<l n="10" num="2.4">Comme un témoin souffert dont l’amour embarrasse,</l>
						<l n="11" num="2.5">Une vieille toujours traînait à quelques pas,</l>
						<l n="12" num="2.6">Les yeux fixés sur elle, et ne lui parlant pas,</l>
						<l n="13" num="2.7">Duègne ou mère, à la fois gardienne et protectrice,</l>
						<l n="14" num="2.8">Et tout en murmurant, soumise à son caprice.</l>
						<l n="15" num="2.9">Tous les jours, environ une heure avant le soir,</l>
						<l n="16" num="2.10">On la voyait venir du plus désert couloir</l>
						<l n="17" num="2.11">Faire choix d’un portrait de madone ou de dame</l>
						<l n="18" num="2.12">En lequel un vieux maître avait mis sa grande âme.</l>
						<l n="19" num="2.13">Elle restait alors, les bras croisés, couvrant</l>
						<l n="20" num="2.14">Le tableau d’un regard de défi, pénétrant</l>
						<l n="21" num="2.15">Et large, d’où partait vers la tête sans vie</l>
						<l n="22" num="2.16">Je ne sais quel éclair de dédain et d’envie.</l>
						<l n="23" num="2.17">Certe, avec ces chefs-d’œuvre au renom magistral</l>
						<l n="24" num="2.18">Elle aurait, sans pâlir, pu lutter d’idéal ;</l>
						<l n="25" num="2.19">Et moi-même, j’avais, au fond des galeries,</l>
						<l n="26" num="2.20">Dans quelque coin, derrière un pan des draperies,</l>
						<l n="27" num="2.21">Maintes fois contemplé cet entretien muet,</l>
						<l n="28" num="2.22">Antagonisme étrange où nul ne remuait</l>
						<l n="29" num="2.23">Du type impérissable et du type éphémère.</l>
						<l n="30" num="2.24">Chacun s’écartait d’elle ainsi que de sa mère.</l>
						<l n="31" num="2.25">On lui donnait vingt ans à peine. Une clarté</l>
						<l n="32" num="2.26">Comme un rayonnement entourait sa beauté</l>
						<l n="33" num="2.27">Qui, splendide, éclatait en floraison entière,</l>
						<l n="34" num="2.28">Mais se sculptait aussi, comme en un bloc de pierre,</l>
						<l n="35" num="2.29">Dans une incomparable et mortelle froideur.</l>
						<l n="36" num="2.30">Ceux que vers elle avait attirés trop d’ardeur</l>
						<l n="37" num="2.31">S’étaient sentis vaincus et terrassés sur place</l>
						<l n="38" num="2.32">Par une pesanteur de mépris et de glace</l>
						<l n="39" num="2.33">Qui tombait de ses yeux sans pareils. Son vrai nom,</l>
						<l n="40" num="2.34">Nul n’avait jamais pu l’apprendre, disait-on.</l>
						<l n="41" num="2.35">Comme elle apparaissait vers une heure tardive</l>
						<l n="42" num="2.36">Dans les palais, sans bruit, solennelle et pensive,</l>
						<l n="43" num="2.37">On lui trouva bientôt ce nom mystérieux</l>
						<l n="44" num="2.38">De Stella Vespera. Personne, jeune ou vieux,</l>
						<l n="45" num="2.39">Par prière ou présent, n’avait obtenu d’elle</l>
						<l n="46" num="2.40">Qu’elle posât jamais devant lui pour modèle.</l>
						<l n="47" num="2.41">Elle n’aimait que l’art d’autrefois, et semblait</l>
						<l n="48" num="2.42">Fuir le peintre au travail devant un chevalet.</l>
					</lg>
					<lg n="3">
						<l n="49" num="3.1">Les curieux, lassés d’un effort inutile,</l>
						<l n="50" num="3.2">La laissaient disparaître au bas d’un péristyle</l>
						<l n="51" num="3.3">Dans l’ombre et dans la foule. On s’était contenté</l>
						<l n="52" num="3.4">D’une légende autour de sa sévérité.</l>
						<l n="53" num="3.5">On disait qu’autrefois, Stella, sans aucun voile,</l>
						<l n="54" num="3.6">Avait brillé, bijou d’un palais, sur la toile,</l>
						<l n="55" num="3.7">Conception d’un prince inconnu du pinceau,</l>
						<l n="56" num="3.8">Sans rivale, parmi les plus dignes du sceau</l>
						<l n="57" num="3.9">Des maîtres plus heureux dont la gloire se nomme.</l>
						<l n="58" num="3.10">Pour ce corps insensible, on disait qu’un jeune homme,</l>
						<l n="59" num="3.11">Un peintre florentin, plus tard s’était épris</l>
						<l n="60" num="3.12">D’un amour insensé mais fervent, et pour prix</l>
						<l n="61" num="3.13">Sut animer aussi cette autre Galatée.</l>
						<l n="62" num="3.14">Un soir qu’il l’appelait dans la salle écartée,</l>
						<l n="63" num="3.15">Il la sentit tomber dans ses bras doucement.</l>
						<l n="64" num="3.16">Quand il mourut, Stella, fidèle à son amant,</l>
						<l n="65" num="3.17">Fut prise du dégoût de sa métamorphose ;</l>
						<l n="66" num="3.18">Et pour se rendormir dans sa première pose</l>
						<l n="67" num="3.19">Comme autrefois, au ciel d’un art patricien,</l>
						<l n="68" num="3.20">Voulut chercher son cadre et son palais ancien ;</l>
						<l n="69" num="3.21">Mais soit qu’elle eût perdu la mémoire à cette heure,</l>
						<l n="70" num="3.22">Soit que le feu peut-être eût détruit la demeure,</l>
						<l n="71" num="3.23">Elle ne put jamais les trouver. C’est ainsi</l>
						<l n="72" num="3.24">Que Stella, sous l’élan d’un unique souci,</l>
						<l n="73" num="3.25">Errait désespérée, et jalouse de celles</l>
						<l n="74" num="3.26">Qui dans l’orgueil serein des formes immortelles</l>
						<l n="75" num="3.27">De musée en musée insultaient son destin.</l>
					</lg>
					<lg n="4">
						<l n="76" num="4.1">D’autres disaient encore et tenaient pour certain</l>
						<l n="77" num="4.2">Que l’art avait en elle un malfaisant génie,</l>
						<l n="78" num="4.3">Dont le regard, tombé sur une œuvre finie,</l>
						<l n="79" num="4.4">Changeait la toile exquise en rebut d’atelier.</l>
					</lg>
					<lg n="5">
						<l n="80" num="5.1">Tel était à Paris le conte familier</l>
						<l n="81" num="5.2">Qui depuis mon retour m’obsédait, plus encore</l>
						<l n="82" num="5.3">Ce soir-là ; car octobre, agitateur sonore,</l>
						<l n="83" num="5.4">Semait dans l’air les voix des souvenirs perdus.</l>
						<l n="84" num="5.5">Et ceux-là revenaient en moi plus assidus,</l>
						<l n="85" num="5.6">Tandis qu’avec Centi, sur la berge isolée,</l>
						<l n="86" num="5.7">Je suivais pas à pas quelque lointaine allée.</l>
						<l n="87" num="5.8">Je l’avoue, en tout temps je me suis abreuvé</l>
						<l n="88" num="5.9">Des choses d’outre-vie, et n’ai que trop rêvé.</l>
						<l n="89" num="5.10">Mais Centi, le grand peintre, avait poussé mon âme</l>
						<l n="90" num="5.11">Vers les mondes obscurs dont il trouait la trame ;</l>
						<l n="91" num="5.12">Et dans ses mots, parfois, filtrait subtilement</l>
						<l n="92" num="5.13">Le dangereux levain d’un bizarre aliment</l>
						<l n="93" num="5.14">Qui, bien loin du réel, comme un corps qu’on délie,</l>
						<l n="94" num="5.15">Me roulait aux confins troublants de la folie.</l>
						<l n="95" num="5.16">Ce soir, en regardant sous la fraîcheur des eaux,</l>
						<l n="96" num="5.17">Où les arbres en feu renversaient leurs arceaux,</l>
						<l n="97" num="5.18">Le brouillard s’épaissir dans ce autres portiques,</l>
						<l n="98" num="5.19">Je sentais que l’esprit des songes fantastiques</l>
						<l n="99" num="5.20">Dormait autour de nous. Par instinct, j’arrêtai</l>
						<l n="100" num="5.21">Le récit sur les bords de mes lèvres monté,</l>
						<l n="101" num="5.22">Pour ne pas réveiller ce tentateur tranquille.</l>
						<l n="102" num="5.23">Nous nous taisions, laissant derrière nous la ville.</l>
					</lg>
					<lg n="6">
						<l n="103" num="6.1">Le peintre s’arrêtait ; il murmura vers moi :</l>
						<l n="104" num="6.2">« Qu’est-ce que le génie, après tout ? C’est ma foi</l>
						<l n="105" num="6.3">Qu’il est évocateur, aussi bien que prophète ;</l>
						<l n="106" num="6.4">Que ce qu’il croit créer est l’image parfaite</l>
						<l n="107" num="6.5">D’un être que retient l’avenir ou la mort,</l>
						<l n="108" num="6.6">Ou qui, peut-être aussi, se cache à son effort,</l>
						<l n="109" num="6.7">Bien loin ou près de lui, mais dans son heure même,</l>
						<l n="110" num="6.8">Réalité vivante égale à l’art suprême,</l>
						<l n="111" num="6.9">Mais qu’un cercle défend, redoutable au désir,</l>
						<l n="112" num="6.10">Fatal à qui la cherche, et la voudrait saisir !</l>
					</lg>
					<lg n="7">
						<l n="113" num="7.1">— Et selon vous, lui dis-je, il faudrait ainsi croire</l>
						<l n="114" num="7.2">La réalité fille ou sœur de l’illusoire ? »</l>
					</lg>
					<lg n="8">
						<l n="115" num="8.1">Il se tut quelque temps, et, plus calme, reprit :</l>
						<l n="116" num="8.2">« L’art est un miroir clair pour un puissant esprit !</l>
						<l n="117" num="8.3">L’ancêtre, dont le nom m’est un âpre héritage,</l>
						<l n="118" num="8.4">Eut, dit-on, la folie et la gloire en partage.</l>
						<l n="119" num="8.5">Mais c’est un fait, célèbre à Florence, jadis,</l>
						<l n="120" num="8.6">Que cinquante ans après sa mort, sous Léon Dix,</l>
						<l n="121" num="8.7">Dans cette ville même, on ne sait d’où venue,</l>
						<l n="122" num="8.8">Vivait aux yeux de tous une femme inconnue,</l>
						<l n="123" num="8.9">Laquelle était l’exact et merveilleux portrait</l>
						<l n="124" num="8.10">De son chef-d’œuvre à lui, qu’un grand prince montrait,</l>
						<l n="125" num="8.11">Et que tous renommaient à l’égal d’un prodige.</l>
					</lg>
					<lg n="9">
						<l n="126" num="9.1">— Et qui donc le possède aujourd’hui ? Répondis-je.</l>
						<l n="127" num="9.2">— Quelque vingt ans après son palais s’écroula</l>
						<l n="128" num="9.3">Dans la flamme avec lui. Mais laissons tout cela ;</l>
						<l n="129" num="9.4">Venez bientôt me voir et parler de Florence.</l>
						<l n="130" num="9.5">Je sens pour cette ville une étrange attirance ;</l>
						<l n="131" num="9.6">Et pour m’en délivrer il faudra bien qu’un jour</l>
						<l n="132" num="9.7">Dans la noble cité je m’éveille à mon tour. »</l>
					</lg>
				</div>
				<div type="section" n="2">
					<head type="number">II</head>
					<lg n="1">
						<l n="133" num="1.1">En entrant, j’admirais à loisir, d’habitude,</l>
						<l n="134" num="1.2">Le riche encombrement du cabinet d’étude ;</l>
						<l n="135" num="1.3">Comme de vieux amis, je les connaissais bien,</l>
						<l n="136" num="1.4">Tous ces dressoirs à jours de style italien ;</l>
						<l n="137" num="1.5">Ces ivoires jaunis, ces coupes, ces épées</l>
						<l n="138" num="1.6">Aux médailles d’acier par Cellini frappées ;</l>
						<l n="139" num="1.7">Ces bronzes florentins ; dans leurs cadres toscans</l>
						<l n="140" num="1.8">Ces bustes de seigneurs aux grands airs provocants,</l>
						<l n="141" num="1.9">Qui tous à leurs pourpoints portaient la même date.</l>
						<l n="142" num="1.10">Cette fois, je passai devant eux à la hâte,</l>
						<l n="143" num="1.11">Mais non sans me sentir brusquement traversé</l>
						<l n="144" num="1.12">Par la sensation d’un glorieux passé ;</l>
						<l n="145" num="1.13">Et les mots de Centi sur Florence, la veille,</l>
						<l n="146" num="1.14">Me semblèrent encor tinter à mon oreille.</l>
						<l n="147" num="1.15">L’atelier m’attirait ; et du premier coup d’œil</l>
						<l n="148" num="1.16">Je demeurai cloué de stupeur sur le seuil,</l>
						<l n="149" num="1.17">Comme un halluciné devant l’esprit qui passe.</l>
						<l n="150" num="1.18">Sur cinq grands chevalets qui tous me faisaient face,</l>
						<l n="151" num="1.19">Dans leurs cadres égaux, j’avais vu cinq portraits</l>
						<l n="152" num="1.20">Éternisant cinq fois d’un coup les mêmes traits.</l>
					</lg>
					<lg n="2">
						<l n="153" num="2.1">Du plafond, tout autour, tombait en masses lourdes</l>
						<l n="154" num="2.2">La tenture au sujet païen, aux couleurs sourdes ;</l>
						<l n="155" num="2.3">Et magnétiquement je reportai les yeux</l>
						<l n="156" num="2.4">Vers les tableaux, travail d’un art prestigieux,</l>
						<l n="157" num="2.5">Sur lesquels un jour vif affluant dans la salle</l>
						<l n="158" num="2.6">Versait à pleins carreaux sa nappe triomphale.</l>
					</lg>
					<lg n="3">
						<l n="159" num="3.1">Chacun semblait le but d’un vouloir différent.</l>
						<l n="160" num="3.2">L’on eût dit du premier quelque tout neuf Rembrandt.</l>
						<l n="161" num="3.3">C’étaient les mêmes fonds d’épaisses atmosphères</l>
						<l n="162" num="3.4">Et d’obscurité chaude aux attrayants mystères ;</l>
						<l n="163" num="3.5">Mais jamais le pinceau du maître hollandais</l>
						<l n="164" num="3.6">N’avait si loin poussé les ténèbres ; jamais</l>
						<l n="165" num="3.7">Si merveilleusement il n’en creusa les ondes</l>
						<l n="166" num="3.8">Sous une transparence aux caresses profondes.</l>
						<l n="167" num="3.9">Quant au visage même, à peine il paraissait</l>
						<l n="168" num="3.10">Sur les bords de la nuit qui l’ensevelissait.</l>
						<l n="169" num="3.11">Mais en me rapprochant, contemplateur avide,</l>
						<l n="170" num="3.12">Quelque baigné qu’il fût par une ombre fluide</l>
						<l n="171" num="3.13">Avare des blancheurs qu’elle dérobe au jour ;</l>
						<l n="172" num="3.14">Quelque indécis que fût l’harmonieux contour</l>
						<l n="173" num="3.15">Du col à la poitrine où le sein vient de naître ;</l>
						<l n="174" num="3.16">Il me fallait aussi sur-le-champ reconnaître</l>
						<l n="175" num="3.17">Une noblesse éparse au sommet de ce front,</l>
						<l n="176" num="3.18">Dans les vagues lueurs qui plus bas se fondront ;</l>
						<l n="177" num="3.19">Une suavité dans cette chevelure</l>
						<l n="178" num="3.20">Onduleuse ; une grâce enfantine et si pure</l>
						<l n="179" num="3.21">Sur ces lèvres ; partout, pour chaque ligne enfin,</l>
						<l n="180" num="3.22">Une virginité de calme séraphin,</l>
						<l n="181" num="3.23">Une fleur de jeunesse, une aristocratie</l>
						<l n="182" num="3.24">De rêve, s’unissant dans sa gloire adoucie</l>
						<l n="183" num="3.25">A la solennité d’une apparition</l>
						<l n="184" num="3.26">Dont Rembrandt n’a jamais cherché l’impression.</l>
					</lg>
					<lg n="4">
						<l n="185" num="4.1">Concevez à présent cette confuse image</l>
						<l n="186" num="4.2">S’avançant de degrés en degrés, d’âge en âge,</l>
						<l n="187" num="4.3">De toile en toile, vers la lumière et vers vous ;</l>
						<l n="188" num="4.4">Du fond de ces vapeurs au rayonnement roux,</l>
						<l n="189" num="4.5">Voyez-la s’imprégner chaque fois d’une vie</l>
						<l n="190" num="4.6">Plus intense, toujours à l’ombre plus ravie,</l>
						<l n="191" num="4.7">Virginale toujours, mais femme cependant</l>
						<l n="192" num="4.8">De plus en plus, plus fière aussi vous regardant,</l>
						<l n="193" num="4.9">Et des limbes premiers de son adolescence</l>
						<l n="194" num="4.10">Arrivant, sous l’essor de sa jeune puissance,</l>
						<l n="195" num="4.11">Jusqu’à l’éclosion enfin d’une beauté</l>
						<l n="196" num="4.12">Sûre d’avoir conquis son immortalité.</l>
						<l n="197" num="4.13">Tels j’admirais, plongé dans de longues extases,</l>
						<l n="198" num="4.14">Ces portraits successifs, insaisissables phases</l>
						<l n="199" num="4.15">De la forme endormie encor dans sa candeur</l>
						<l n="200" num="4.16">A la forme éveillée en sa riche splendeur,</l>
						<l n="201" num="4.17">Qui se connaît et qui s’impose, de la vierge</l>
						<l n="202" num="4.18">Qu’un songe inconscient et sans amour submerge</l>
						<l n="203" num="4.19">A celle qui se sent aimée, et dont les yeux</l>
						<l n="204" num="4.20">Ne réfléchissent rien d’un cœur silencieux.</l>
						<l n="205" num="4.21">Et maintenant, tout près de moi, la pâle tête</l>
						<l n="206" num="4.22">Qui dans le dernier cadre, illusion complète,</l>
						<l n="207" num="4.23">Respirait, échappée aux baisers de la nuit ;</l>
						<l n="208" num="4.24">Dardait vers moi l’éclair d’un regard qui poursuit ;</l>
						<l n="209" num="4.25">S’enveloppait de vie et d’éclat, palpitante</l>
						<l n="210" num="4.26">Des vivaces espoirs d’une héroïque attente,</l>
						<l n="211" num="4.27">Et magnifiquement, comme un matin d’été,</l>
						<l n="212" num="4.28">Épanouie au sein de sa propre clarté ;</l>
						<l n="213" num="4.29">Ainsi qu’en un miroir un reflet qui s’obstine,</l>
						<l n="214" num="4.30">C’était bien cette fois la tête florentine</l>
						<l n="215" num="4.31">De Stella Vespera, telle que bien souvent</l>
						<l n="216" num="4.32">Naguère je l’avais contemplée en rêvant.</l>
					</lg>
					<lg n="5">
						<l n="217" num="5.1">Jamais l’art ne fixa d’une main plus fidèle</l>
						<l n="218" num="5.2">Dans son panthéon chaste un glorieux modèle ;</l>
						<l n="219" num="5.3">Jamais aussi, devant le génie et l’amour,</l>
						<l n="220" num="5.4">Plus belle vérité ne se fit voir au jour.</l>
					</lg>
					<lg n="6">
						<l n="221" num="6.1">Ainsi, mon souvenir, dans sa forme absolue,</l>
						<l n="222" num="6.2">Triomphant, tout à coup se dressait à ma vue,</l>
						<l n="223" num="6.3">M’enchaînait de nouveau, si loin ! Et se parait</l>
						<l n="224" num="6.4">D’un charme plus profond fait d’un nouveau secret,</l>
						<l n="225" num="6.5">Sacrant tout l’atelier du silence des temples !</l>
						<l n="226" num="6.6">Et moi, je m’abîmais dans ses prunelles amples.</l>
						<l n="227" num="6.7">Bien des heures, j’avais jusqu’ici médité,</l>
						<l n="228" num="6.8">En pensant à ses yeux, sur leur étrangeté ;</l>
						<l n="229" num="6.9">Ce jour-là, tout à coup, sur l’image imprévue</l>
						<l n="230" num="6.10">J’en surpris la raison restée inaperçue.</l>
						<l n="231" num="6.11">« Oui, me dis-je, en effet, l’un de ses yeux est noir</l>
						<l n="232" num="6.12">Et luisant comme l’encre, et l’autre, comme un soir</l>
						<l n="233" num="6.13">Sans lune, est d’un bleu sombre étoilé de lumières ;</l>
						<l n="234" num="6.14">Et leurs disques rivaux emplissent les paupières ! »</l>
					</lg>
					<lg n="7">
						<l n="235" num="7.1">Enfin, un dernier cadre, isolé dans un coin</l>
						<l n="236" num="7.2">De l’atelier, forçait ma vue un peu plus loin.</l>
						<l n="237" num="7.3">Ce n’était qu’une ébauche, une esquisse légère,</l>
						<l n="238" num="7.4">Mais toujours de Stella, l’obsédante étrangère.</l>
						<l n="239" num="7.5">Quel nimbe reluirait sur ce front renaissant ?</l>
						<l n="240" num="7.6">Centi voulait-il donc, d’un désir tout récent,</l>
						<l n="241" num="7.7">Artiste inassouvi, surpasser la nature,</l>
						<l n="242" num="7.8">Et jusqu’au surhumain tenter une aventure ?</l>
						<l n="243" num="7.9">Ou bien, comme il avait, magicien de l’art,</l>
						<l n="244" num="7.10">Suivi cette beauté d’un scrupuleux regard</l>
						<l n="245" num="7.11">Dans son progrès, depuis l’aube crépusculaire</l>
						<l n="246" num="7.12">Jusqu’à l’heure qu’un ciel d’apothéose éclaire,</l>
						<l n="247" num="7.13">Allait-il la poursuivre, artiste sans pitié,</l>
						<l n="248" num="7.14">Dans son déclin aussi chaque jour épié ?</l>
					</lg>
					<lg n="8">
						<l n="249" num="8.1">Et le temps s’écoulait. Mes yeux enthousiastes</l>
						<l n="250" num="8.2">Toujours interrogeaient ce visage en ses fastes ;</l>
						<l n="251" num="8.3">Et, comme sur les bords d’un puits vertigineux,</l>
						<l n="252" num="8.4">Je me sentais sans fin pris dans les mille nœuds</l>
						<l n="253" num="8.5">D’une énigme enlacée à l’énigme contraire ;</l>
						<l n="254" num="8.6">Et nul raisonnement ne pouvait m’y soustraire ;</l>
						<l n="255" num="8.7">Et, dans la vaste salle où je demeurais seul,</l>
						<l n="256" num="8.8">Il me semblait parfois que l’esprit de l’aïeul</l>
						<l n="257" num="8.9">Derrière moi veillait au fond des angles sombres ;</l>
						<l n="258" num="8.10">Car vers les murs déjà s’amoncelaient les ombres.</l>
						<l n="259" num="8.11">Le soir vint. Éperdu d’extase, stupéfait,</l>
						<l n="260" num="8.12">Je regardais toujours. Le génie, en effet,</l>
						<l n="261" num="8.13">Ne laisse pas en vain sur ses œuvres l’empreinte</l>
						<l n="262" num="8.14">D’une forte pensée. Une énergique étreinte</l>
						<l n="263" num="8.15">Sort toujours de la toile abandonnée, et tient</l>
						<l n="264" num="8.16">Dans son réseau subtil le profane qui vient</l>
						<l n="265" num="8.17">Troubler impudemment l’atelier solitaire.</l>
					</lg>
					<lg n="9">
						<l n="266" num="9.1">La nuit s’épaississait au fond du sanctuaire,</l>
						<l n="267" num="9.2">Noyant tout, chevalets, cadres et cheveux blonds.</l>
						<l n="268" num="9.3">Alors, et malgré moi, furtif, à reculons,</l>
						<l n="269" num="9.4">Je partis lentement, chassé par ces fronts pâles</l>
						<l n="270" num="9.5">Qui, lumineux, pareils à de larges opales,</l>
						<l n="271" num="9.6">Paraissaient, sous le flux des ténèbres montant,</l>
						<l n="272" num="9.7">M’enfoncer un regard de foule inquiétant.</l>
					</lg>
					<lg n="10">
						<l n="273" num="10.1">Le malheur s’abattit sur moi cette nuit même,</l>
						<l n="274" num="10.2">Et pour longtemps crispa sur mon cœur sa main blême.</l>
						<l n="275" num="10.3">Au fond d’une retraite, au loin, et dans l’oubli</l>
						<l n="276" num="10.4">De Stella, je vécus un temps enseveli.</l>
					</lg>
				</div>
				<div type="section" n="3">
					<head type="number">III</head>
					<lg n="1">
						<l n="277" num="1.1">Je revins. Quelques jours plus tard, dans un musée,</l>
						<l n="278" num="1.2">Je promenais sans but ma tristesse apaisée,</l>
						<l n="279" num="1.3">Quand je vis disparaître, au bas d’un escalier,</l>
						<l n="280" num="1.4">Une vieille en costume au style singulier,</l>
						<l n="281" num="1.5">Qui me remémora la vierge d’Italie</l>
						<l n="282" num="1.6">Qu’à ses portraits lointains une énigme relie.</l>
						<l n="283" num="1.7">Je voulus pénétrer ce secret jusqu’au bout,</l>
						<l n="284" num="1.8">Et courus chez Centi. Je le trouvai debout</l>
						<l n="285" num="1.9">Devant sa dernière œuvre ; et ses yeux, dans l’ivresse</l>
						<l n="286" num="1.10">Du triomphe, élevaient leur brûlante caresse</l>
						<l n="287" num="1.11">Sur la toile achevée, et seule cette fois.</l>
						<l n="288" num="1.12">Lui-même s’agitai, parlant à haute voix,</l>
						<l n="289" num="1.13">Artiste émerveillé devant son propre ouvrage.</l>
						<l n="290" num="1.14">Dès l’abord, une joie éclaira son visage ;</l>
						<l n="291" num="1.15">Il s’élança, me prit le bras, et, m’entraînant</l>
						<l n="292" num="1.16">En face du tableau, s’écria : « Maintenant,</l>
						<l n="293" num="1.17">Regardez ! … répondez ! N’est-ce pas, qu’elle est belle ?</l>
						<l n="294" num="1.18">N’est-ce pas, qu’elle arrive à l’amour qui l’appelle ? »</l>
					</lg>
					<lg n="2">
						<l n="295" num="2.1">Et moi, je regardais déjà, me demandant</l>
						<l n="296" num="2.2">Comment il avait pu, d’un effort ascendant,</l>
						<l n="297" num="2.3">Faire plus resplendir la tête sans rivale,</l>
						<l n="298" num="2.4">Et, par plus de magie, en un plus pur ovale</l>
						<l n="299" num="2.5">Vivifier ces traits sous un ciel ébloui.</l>
						<l n="300" num="2.6">Comme autrefois, toujours, c’était bien aujourd’hui</l>
						<l n="301" num="2.7">Le beau front lumineux et chargé de pensées ;</l>
						<l n="302" num="2.8">Mais son éclat, vainqueur des ombres dispersées,</l>
						<l n="303" num="2.9">Brillait plus éloquent encore ; il se gonflait,</l>
						<l n="304" num="2.10">Flamboyant, agrandi sous le double reflet</l>
						<l n="305" num="2.11">D’un éternel bonheur et d’une paix conquise.</l>
						<l n="306" num="2.12">C’était, sous la lueur changeante qui l’irise,</l>
						<l n="307" num="2.13">La même chevelure aux anneaux blonds et bruns,</l>
						<l n="308" num="2.14">Libres et déroulés sans fin, dont quelques-uns,</l>
						<l n="309" num="2.15">Voluptueux flocons qu’un sein grec illumine,</l>
						<l n="310" num="2.16">Flottaient confusément aux bords de la poitrine.</l>
						<l n="311" num="2.17">Mais, plus souple auréole et plus suave encor,</l>
						<l n="312" num="2.18">S’épandait sur le cou leur opulent trésor.</l>
						<l n="313" num="2.19">Les yeux étaient toujours aussi pleins, aussi chastes,</l>
						<l n="314" num="2.20">Aussi profonds, l’un bleu comme les nuits néfastes</l>
						<l n="315" num="2.21">Sans lune, l’autre, noir comme l’encre, et tous deux</l>
						<l n="316" num="2.22">Limpides ; mais le large éclair qui sortait d’eux</l>
						<l n="317" num="2.23">N’était plus la clarté de l’orgueil ni du rêve ;</l>
						<l n="318" num="2.24">C’était l’ardent rayon de l’amour qui se lève ;</l>
						<l n="319" num="2.25">Et la lèvre, plus rouge encor, plus finement</l>
						<l n="320" num="2.26">Découpée aujourd’hui, comme pour le serment</l>
						<l n="321" num="2.27">Et pour l’aveu, s’ouvrait au baiser qui l’attire.</l>
						<l n="322" num="2.28">On entait à travers ce superbe sourire</l>
						<l n="323" num="2.29">La victoire éclater dans la soumission,</l>
						<l n="324" num="2.30">Comme aussi dans ces yeux, avec la passion,</l>
						<l n="325" num="2.31">Passer l’enivrement d’une beauté céleste.</l>
						<l n="326" num="2.32">Et comme refoulant derrière elle, d’un geste,</l>
						<l n="327" num="2.33">Et pour jamais, bien loin, les brumes d’autrefois,</l>
						<l n="328" num="2.34">Par un miracle d’art qui renverse les lois,</l>
						<l n="329" num="2.35">Dans la pleine lumière où chaque trait s’anime</l>
						<l n="330" num="2.36">Elle avançait vers nous son visage sublime.</l>
					</lg>
					<lg n="3">
						<l n="331" num="3.1">Et c’était l’idéal, pensais-je, que là-bas,</l>
						<l n="332" num="3.2">Malgré tout, l’autre encor ne réalisait pas.</l>
					</lg>
					<lg n="4">
						<l n="333" num="4.1">« Enfin ! S’écria-t-il, cette fois, c’est bien elle !</l>
						<l n="334" num="4.2">N’est-ce pas, qu’elle vit ? N’est-ce pas, qu’elle est belle ?</l>
						<l n="335" num="4.3">Une âme plane aussi sur ma création,</l>
						<l n="336" num="4.4">Et ton cœur bat en moi, divin Pygmalion !</l>
						<l n="337" num="4.5">Qui donc a pu railler ton amour ineffable ?</l>
						<l n="338" num="4.6">Ta Galatée, ô grec ! N’était point une fable !</l>
						<l n="339" num="4.7">Ce n’est pas ta statue au marbre radieux</l>
						<l n="340" num="4.8">Qui s’anima pour toi sous le souffle des dieux.</l>
						<l n="341" num="4.9">Non. Mais ils t’ont permis, ton œuvre terminée,</l>
						<l n="342" num="4.10">De rencontrer alors la femme devinée !</l>
					</lg>
					<lg n="5">
						<l n="343" num="5.1">— Celle-là, quant à moi, j’en reste convaincu,</l>
						<l n="344" num="5.2">Lui dis-je, n’est qu’un songe, et n’a jamais vécu.</l>
						<l n="345" num="5.3">Mais les autres, Centi ! Vous avez, je le jure,</l>
						<l n="346" num="5.4">Sous le soleil de tous vu passer leur figure !</l>
					</lg>
					<lg n="6">
						<l n="347" num="6.1">— Où donc l’aurais-je pu ? dit-il. Mais que me font</l>
						<l n="348" num="6.2">Ces ébauches, d’ailleurs ! Dans leur néant profond</l>
						<l n="349" num="6.3">Qu’elles rentrent ! Voici la seule qui soit faite</l>
						<l n="350" num="6.4">Pour moi, l’évocateur, ou pour moi, le prophète !</l>
						<l n="351" num="6.5">Et maudits soient-ils tous, les pinceaux ! Je suis né</l>
						<l n="352" num="6.6">Trop tard, ou bien trop tôt. L’amour est condamné !</l>
						<l n="353" num="6.7">Car l’amour est au fond du royaume des rêves,</l>
						<l n="354" num="6.8">Dans les bosquets perdus qu’on remplacés les grèves,</l>
						<l n="355" num="6.9">Dans les mondes encor sans voix et sans écho,</l>
						<l n="356" num="6.10">Dans le silencieux amas des vieux chaos,</l>
						<l n="357" num="6.11">Dans la poussière d’or des mirages splendides,</l>
						<l n="358" num="6.12">Ou dans les paradis noyés des Atlantides !</l>
						<l n="359" num="6.13">Oui, je vous dis qu’un jour elle vivra, sinon</l>
						<l n="360" num="6.14">Qu’elle est morte à jamais sans avoir su mon nom ! »</l>
					</lg>
					<lg n="7">
						<l n="361" num="7.1">Et pendant qu’il parlait, je voyais sur sa lèvre</l>
						<l n="362" num="7.2">Trembler le désespoir furieux et la fièvre.</l>
						<l n="363" num="7.3">« Regardez, reprit-il, elle a chassé la nuit</l>
						<l n="364" num="7.4">Qui jadis l’entourait, jalouse, et qui s’enfuit !</l>
						<l n="365" num="7.5">Elle apparaît, semblable à l’étoile dernière,</l>
						<l n="366" num="7.6">Sur mon cœur épanchant tout un ciel de lumière !</l>
						<l n="367" num="7.7">Et je l’aime ! Et jamais l’éclair d’un œil vivant,</l>
						<l n="368" num="7.8">Je le sais, ici-bas n’a frappé plus avant,</l>
						<l n="369" num="7.9">Ni fait plus tressaillir les profondeurs d’une âme !</l>
						<l n="370" num="7.10">Dans l’amour infini d’un amant, jamais femme,</l>
						<l n="371" num="7.11">Comme une reine au fond d’un palais, n’a marché,</l>
						<l n="372" num="7.12">De salle en salle, aux chants d’un orchestre caché,</l>
						<l n="373" num="7.13">Vers un trône plus beau, d’un pas plus sûr ! Je l’aime,</l>
						<l n="374" num="7.14">Celle-ci dont ma main a retracé l’emblème,</l>
						<l n="375" num="7.15">La morte, ou l’invisible encor, l’être innomé</l>
						<l n="376" num="7.16">Qui, si j’avais vécu plus tôt, m’aurait aimé,</l>
						<l n="377" num="7.17">Qui m’aimerait plus tard, si je pouvais revivre !</l>
						<l n="378" num="7.18">La femme qui peut-être à l’heure même enivre</l>
						<l n="379" num="7.19">Quelque part d’autres yeux, ô rage ! Que mes yeux,</l>
						<l n="380" num="7.20">Et qui doit, loin de moi, mourir sous d’autres cieux !</l>
						<l n="381" num="7.21">Ah ! Si vraiment tu vis, si je pouvais le croire,</l>
						<l n="382" num="7.22">Périssent d’un seul coup mon génie et ma gloire !</l>
						<l n="383" num="7.23">Et vienne aussi la mort ! Je l’accepte, content,</l>
						<l n="384" num="7.24">Pourvu que je te voie une heure, un seul instant,</l>
						<l n="385" num="7.25">Et te parle, et t’entende, et t’admire, et t’adore,</l>
						<l n="386" num="7.26">O toi qui m’aimeras ! ô femme dont j’ignore</l>
						<l n="387" num="7.27">La patrie et le nom ! Toi qui prends mon destin,</l>
						<l n="388" num="7.28">Et souris comme au ciel l’étoile du matin ! »</l>
					</lg>
					<lg n="8">
						<l n="389" num="8.1">Je frémissais ainsi qu’un blessé que l’on touche,</l>
						<l n="390" num="8.2">Et mon secret déjà s’échappait de ma bouche ;</l>
						<l n="391" num="8.3">Derrière nous un bruit de pas, en ce moment,</l>
						<l n="392" num="8.4">Nous fit nous retourner tous les deux brusquement</l>
						<l n="393" num="8.5">Vers le vaste rideau qui recouvrait l’entrée.</l>
						<l n="394" num="8.6">Dans un angle une main, vive lueur montrée,</l>
						<l n="395" num="8.7">Avec un geste prompt l’écarta tout entier,</l>
						<l n="396" num="8.8">Repliant les anneaux sur la tringle d’acier.</l>
						<l n="397" num="8.9">Et debout sur le seuil, grande et noble statue,</l>
						<l n="398" num="8.10">Une femme était là, royalement vêtue,</l>
						<l n="399" num="8.11">Comme en un autre cadre, immobile, ses traits</l>
						<l n="400" num="8.12">Recouverts d’un long voile aux attirants secrets,</l>
						<l n="401" num="8.13">Pareille aux visions des nuits surnaturelles,</l>
						<l n="402" num="8.14">Qui, dilatant d’effroi les yeux fixés sur elles,</l>
						<l n="403" num="8.15">Fascinent les vivants par leur solennité.</l>
						<l n="404" num="8.16">Une femme était là, sûre de sa beauté,</l>
						<l n="405" num="8.17">Au maintien qu’aussitôt j’avais cru reconnaître,</l>
						<l n="406" num="8.18">Et vers qui, jaillissant de la haute fenêtre,</l>
						<l n="407" num="8.19">Comme pour un salut, ruisselèrent d’un bond</l>
						<l n="408" num="8.20">Les feux enorgueillis du soleil moribond.</l>
						<l n="409" num="8.21">A peine elle aperçut la peinture immortelle,</l>
						<l n="410" num="8.22">Que l’ombre étincela sous la riche dentelle ;</l>
						<l n="411" num="8.23">Alors, d’une voix lente, au timbre musical</l>
						<l n="412" num="8.24">Comme le clair écho d’un sonore métal,</l>
						<l n="413" num="8.25">Elle laissa tomber ces mots dans le silence :</l>
					</lg>
					<lg n="9">
						<l n="414" num="9.1">« Au beau siècle de l’art, autrefois, dans Florence,</l>
						<l n="415" num="9.2">Grand parmi les plus grands fut l’un de vos aïeux,</l>
						<l n="416" num="9.3">Dont le chef-d’œuvre était le portrait merveilleux</l>
						<l n="417" num="9.4">De mon aïeule à moi, qu’on nomma par la ville</l>
						<l n="418" num="9.5">L’étoile du matin. Dans un siècle infertile</l>
						<l n="419" num="9.6">Votre nom seul rayonne. En vous je reconnais</l>
						<l n="420" num="9.7">Le plus digne héritier des anciens ; je venais</l>
						<l n="421" num="9.8">Demander au Centi revivant de renaître</l>
						<l n="422" num="9.9">Sous le divin pinceau qu’il tient de son ancêtre,</l>
						<l n="423" num="9.10">Moi, dont le nom, là-bas, est l’étoile du soir ! »</l>
					</lg>
					<lg n="10">
						<l n="424" num="10.1">Et moi, je frissonnais plus fort, car je pus voir,</l>
						<l n="425" num="10.2">Son voile ôté, Stella vers l’œuvre prophétique</l>
						<l n="426" num="10.3">Marcher, reflet palpable et modèle identique ;</l>
						<l n="427" num="10.4">Je sentais mes cheveux se hérisser d’effroi,</l>
						<l n="428" num="10.5">Car Centi tout à coup s’était rué sur moi,</l>
						<l n="429" num="10.6">Car ses ongles m’entraient dans la chair leurs tenailles,</l>
						<l n="430" num="10.7">Et j’entendais courir, en rayant les murailles,</l>
						<l n="431" num="10.8">Le rire aigu qui glace et qui pénètre en nous,</l>
						<l n="432" num="10.9">Le rire intarissable où se tordent les fous !</l>
					</lg>
				</div>
			</div></body></text></TEI>