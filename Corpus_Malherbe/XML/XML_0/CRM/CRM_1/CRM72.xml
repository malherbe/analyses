<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRABANT</title>
				<title type="medium">Une édition électronique</title>
				<author key="CRM">
					<name>
						<forename>Maurice</forename>
						<surname>CARÊME</surname>
					</name>
					<date from="1899" to="1978">1899-1978</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3326 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CRM_1</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Brabant</title>
						<author>Maurice Carême</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LES ÉDITIONS OUVRIÈRES</publisher>
							<date when="1976">1976</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1967">1967</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-02-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-02-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CRM72">
				<head type="main">L’ANGE</head>
				<opener>
					<salute>A Armand Bernier</salute>
				</opener>
				<lg n="1">
					<l n="1" num="1.1">Mais oui, regarde ! c’est un ange</l>
					<l n="2" num="1.2">Qui vient vers toi par le sentier.</l>
					<l n="3" num="1.3">Il a sa robe à longues manches</l>
					<l n="4" num="1.4">Et sa figure est rose et blanche</l>
					<l n="5" num="1.5">Comme la fleur de l’églantier.</l>
				</lg>
				<lg n="2">
					<l n="6" num="2.1">Le vois-tu sourire au pinson</l>
					<l n="7" num="2.2">Qui fait plier la basse branche</l>
					<l n="8" num="2.3">Au plus vif de son oraison ?</l>
					<l n="9" num="2.4">Le vois-tu sourire au pinson</l>
					<l n="10" num="2.5">Qui n’oublie pas que c’est dimanche ?</l>
				</lg>
				<lg n="3">
					<l n="11" num="3.1">Et l’écureuil ? Pourquoi vient-il</l>
					<l n="12" num="3.2">Faire le beau au pied du chêne ?</l>
					<l n="13" num="3.3">C’est que l’ange tient une faine</l>
					<l n="14" num="3.4">Et qu’il la tend — ainsi soit-il —</l>
					<l n="15" num="3.5">A l’écureuil au pied du chêne.</l>
				</lg>
				<lg n="4">
					<l n="16" num="4.1">Il approche. Mais c’est le ciel</l>
					<l n="17" num="4.2">Que l’on voit luire dans ses yeux.</l>
					<l n="18" num="4.3">C’est la même douceur de miel,</l>
					<l n="19" num="4.4">Le même éclat, le même bleu,</l>
					<l n="20" num="4.5">Le même nuage réel !</l>
				</lg>
				<lg n="5">
					<l n="21" num="5.1">Et le voilà si transparent</l>
					<l n="22" num="5.2">Qu’au travers de sa robe à manches,</l>
					<l n="23" num="5.3">On voit le pinson doux parlant</l>
					<l n="24" num="5.4">Et l’écureuil, la basse branche</l>
					<l n="25" num="5.5">Et le dimanche rose et blanc.</l>
				</lg>
				<lg n="6">
					<l n="26" num="6.1">Là ! il passe… il t’a traversé !</l>
					<l n="27" num="6.2">‒ O cette odeur de lis et d’ache ! ‒</l>
					<l n="28" num="6.3">Quelle est cette étonnante grâce</l>
					<l n="29" num="6.4">Qui fait courir de la clarté</l>
					<l n="30" num="6.5">Dans ton corps soudain très léger ?</l>
				</lg>
				<lg n="7">
					<l n="31" num="7.1">Mais oui, regarde ! c’est un ange</l>
					<l n="32" num="7.2">Qui s’éloigne sur le sentier</l>
					<l n="33" num="7.3">Avec sa robe à longues manches.</l>
					<l n="34" num="7.4">Sous son pas, on voit les lotiers</l>
					<l n="35" num="7.5">Ployer, soudain lourds de rosée.</l>
				</lg>
			</div></body></text></TEI>