<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRABANT</title>
				<title type="medium">Une édition électronique</title>
				<author key="CRM">
					<name>
						<forename>Maurice</forename>
						<surname>CARÊME</surname>
					</name>
					<date from="1899" to="1978">1899-1978</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3326 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CRM_1</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Brabant</title>
						<author>Maurice Carême</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LES ÉDITIONS OUVRIÈRES</publisher>
							<date when="1976">1976</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1967">1967</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-02-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-02-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CRM198">
				<head type="main">TU VOIS, DIT LE SOLEIL…</head>
				<lg n="1">
					<l n="1" num="1.1">Tu vois, dit le soleil, toujours je recommence</l>
					<l n="2" num="1.2"><space unit="char" quantity="12"></space>A marcher sur les toits.</l>
					<l n="3" num="1.3">Aujourd’hui, le brouillard a une transparence</l>
					<l n="4" num="1.4"><space unit="char" quantity="12"></space>Qui me dore les doigts.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Je n’ai d’autre secret que ma grande patience,</l>
					<l n="6" num="2.2"><space unit="char" quantity="12"></space>Que mon travail sans fin.</l>
					<l n="7" num="2.3">Il me faut des années pour qu’un chêne balance</l>
					<l n="8" num="2.4"><space unit="char" quantity="12"></space>Son ombre sur le lin.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Je connais bien la mort. C’est une vieille amie.</l>
					<l n="10" num="3.2"><space unit="char" quantity="12"></space>Elle ne prend jamais</l>
					<l n="11" num="3.3">Que les gens qui s’ennuient, que les gens qui oublient</l>
					<l n="12" num="3.4"><space unit="char" quantity="12"></space>De rouvrir leurs volets.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">Tu vois, dit le soleil. Le monde recommence</l>
					<l n="14" num="4.2"><space unit="char" quantity="12"></space>Comme au tout premier jour.</l>
					<l n="15" num="4.3">Le brouillard, aujourd’hui, a cette transparence</l>
					<l n="16" num="4.4"><space unit="char" quantity="12"></space>Que je donne à l’amour.</l>
				</lg>
			</div></body></text></TEI>