<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRABANT</title>
				<title type="medium">Une édition électronique</title>
				<author key="CRM">
					<name>
						<forename>Maurice</forename>
						<surname>CARÊME</surname>
					</name>
					<date from="1899" to="1978">1899-1978</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3326 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CRM_1</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Brabant</title>
						<author>Maurice Carême</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LES ÉDITIONS OUVRIÈRES</publisher>
							<date when="1976">1976</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1967">1967</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-02-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-02-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CRM45">
				<head type="main">MÈRE, SAIS-JE POURQUOI…</head>
				<lg n="1">
					<l n="1" num="1.1">Mère, sais-je pourquoi, sur ce chemin montant</l>
					<l n="2" num="1.2">Qui va de Basse-Wavre au bois de Dion-le-Val,</l>
					<l n="3" num="1.3">Je t’aperçois, enfant trottant près d’un cheval</l>
					<l n="4" num="1.4">Qui tire une roulotte peinte en vert et blanc ?</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Peut-être es-tu sortie du morne cimetière</l>
					<l n="6" num="2.2">Qu’on voit là-bas hisser péniblement ses croix</l>
					<l n="7" num="2.3">Pour revenir ici comme un peu de lumière</l>
					<l n="8" num="2.4">Qui marcherait dans l’ombre bleue tout près de moi.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Ton père, le forain, un juron à la bouche,</l>
					<l n="10" num="3.2">Fouette son cheval maigre et se plaint des pavés</l>
					<l n="11" num="3.3">Qui secouent la roulotte ainsi qu’un grand panier</l>
					<l n="12" num="3.4">Plein de poupées joufflues et de sucreries rouges.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">Toi, tu souris, les mains encore glacées d’aube.</l>
					<l n="14" num="4.2">Accrochés aux sapins, de grands pans de ciel volent.</l>
					<l n="15" num="4.3">Une églantine danse, attachée à ta robe.</l>
					<l n="16" num="4.4">Tu songes, en marchant, à tes amies d’école.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1">« Voilà encore Henriette absente ! diront-elles,</l>
					<l n="18" num="5.2">Elle ne saura pas sa leçon de calcul. »</l>
					<l n="19" num="5.3">Toi, tu apprends à faire des nœuds d’hirondelles,</l>
					<l n="20" num="5.4">A parler, dans le vent, avec les libellules.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1">Comme des passereaux dans une immense cage,</l>
					<l n="22" num="6.2">Tu écoutes chanter les bois et les villages</l>
					<l n="23" num="6.3">Et devines déjà, au clin d’œil des nuages,</l>
					<l n="24" num="6.4">Tout ce que l’heure cache en son grand tablier.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1">Et tout à l’heure, au seuil de la pauvre baraque</l>
					<l n="26" num="7.2">Où tu vendras à d’autres les belles poupées</l>
					<l n="27" num="7.3">Avec lesquelles tu n’as jamais pu jouer,</l>
					<l n="28" num="7.4">Tu souriras, comme je te vois, sous les arbres,</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1">Sourire ici, trottant sur ce chemin d’abeilles</l>
					<l n="30" num="8.2">Qui va de Basse-Wavre au bois de Dion-le-Val,</l>
					<l n="31" num="8.3">Sourire en caressant, résignée, le cheval</l>
					<l n="32" num="8.4">Dont le poitrail tendu fume dans le soleil.</l>
				</lg>
			</div></body></text></TEI>