<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRABANT</title>
				<title type="medium">Une édition électronique</title>
				<author key="CRM">
					<name>
						<forename>Maurice</forename>
						<surname>CARÊME</surname>
					</name>
					<date from="1899" to="1978">1899-1978</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3326 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CRM_1</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Brabant</title>
						<author>Maurice Carême</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LES ÉDITIONS OUVRIÈRES</publisher>
							<date when="1976">1976</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1967">1967</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-02-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-02-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CRM71">
				<head type="main">BRUXELLES</head>
				<lg n="1">
					<l n="1" num="1.1">Pourquoi ne laisserais-tu pas monter de toi</l>
					<l n="2" num="1.2"><space unit="char" quantity="20"></space>La ville</l>
					<l n="3" num="1.3">Avec ses tours, ses volières d’oiseaux, ses toits</l>
					<l n="4" num="1.4"><space unit="char" quantity="20"></space>De tuiles ?</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">C’est tout le long des cœurs qu’ici les douces biches</l>
					<l n="6" num="2.2"><space unit="char" quantity="20"></space>Repassent</l>
					<l n="7" num="2.3">Préférant quelquefois à l’ombre des rues riches,</l>
					<l n="8" num="2.4"><space unit="char" quantity="20"></space>L’impasse.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Des arbres égarés regardent par-dessus</l>
					<l n="10" num="3.2"><space unit="char" quantity="20"></space>Les murs</l>
					<l n="11" num="3.3">Courir les écoliers dont les cris ingénus</l>
					<l n="12" num="3.4"><space unit="char" quantity="20"></space>Rassurent.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">C’est là que l’amour tient si candidement sa</l>
					<l n="14" num="4.2"><space unit="char" quantity="20"></space>Balance</l>
					<l n="15" num="4.3">Qu’on ne devine plus, dans le va-et-vient, sa</l>
					<l n="16" num="4.4"><space unit="char" quantity="20"></space>Présence</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1">Que dans l’apparition soudaine et secrète à</l>
					<l n="18" num="5.2"><space unit="char" quantity="20"></space>Nos yeux,</l>
					<l n="19" num="5.3">Loin de toute verdure drue, d’une bête à</l>
					<l n="20" num="5.4"><space unit="char" quantity="20"></space>Bon Dieu.</l>
				</lg>
			</div></body></text></TEI>