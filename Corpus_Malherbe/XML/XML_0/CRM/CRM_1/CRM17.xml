<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRABANT</title>
				<title type="medium">Une édition électronique</title>
				<author key="CRM">
					<name>
						<forename>Maurice</forename>
						<surname>CARÊME</surname>
					</name>
					<date from="1899" to="1978">1899-1978</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3326 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CRM_1</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Brabant</title>
						<author>Maurice Carême</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LES ÉDITIONS OUVRIÈRES</publisher>
							<date when="1976">1976</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1967">1967</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-02-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-02-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CRM17">
				<head type="main">QU’AS-TU A DORMIR…</head>
				<lg n="1">
					<l n="1" num="1.1">Qu’as-tu à dormir au milieu des gerbes ?</l>
					<l n="2" num="1.2">Déjà le Brabant est tout en rumeur.</l>
					<l n="3" num="1.3">On entend la voix du petit brin d’herbe</l>
					<l n="4" num="1.4">Autant que la voix chaude du faucheur.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Le ruisseau lui-même au milieu des vaches</l>
					<l n="6" num="2.2">Claque sans répit ainsi qu’un long fouet.</l>
					<l n="7" num="2.3">Le vent cingle loirs, martres et furets.</l>
					<l n="8" num="2.4">Faut-il qu’on te montre aussi la cravache ?</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Tes menus soucis ? Mais on les connaît.</l>
					<l n="10" num="3.2">Quel églantier n’a son réseau d’épines ?</l>
					<l n="11" num="3.3">Que d’amères pluies pour que se devine,</l>
					<l n="12" num="3.4">Sur un chaume étroit, un épi parfait !</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">Plus l’été e£t clair, plus sombre e£t l’orage.</l>
					<l n="14" num="4.2">Un coteau frappé par un coup de grêle</l>
					<l n="15" num="4.3">Rouvre ses yeux las, lève son visage</l>
					<l n="16" num="4.4">Et sourit quand même au jeune arc-en-ciel</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1">Et demain encore, au cœur du Brabant,</l>
					<l n="18" num="5.2">Ne verras-tu pas, de la cruche blonde</l>
					<l n="19" num="5.3">De l’aube inclinée sur ton toit dormant,</l>
					<l n="20" num="5.4">La clarté emplir peu à peu le monde ?</l>
				</lg>
			</div></body></text></TEI>