<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES AMOURS JAUNES</title>
				<title type="medium">Édition électronique</title>
				<author key="CRB">
					<name>
						<forename>Tristan</forename>
						<surname>CORBIÈRE</surname>
					</name>
					<date from="1845" to="1875">1845-1875</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement métrique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4094 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">CRB_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Amours jaunes</title>
						<author>Tristan Corbière</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/ebooks/16883</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Amours jaunes</title>
								<author>Tristan Corbière</author>
								<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k70668p.r=tristan+corbi%C3%A8re.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE DU XIXe SIÈCLE ‒ GLADY FRÈRES, ÉDITEURS</publisher>
									<date when="1873">1873</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1873">1873</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">RACCROCS</head><div type="poem" key="CRB62">
					<head type="main">DÉJEUNER DE SOLEIL</head>
					<opener>
						<dateline>
							<placeName>Bois de Boulogne</placeName> I<hi rend="sup">er</hi> mai.
						</dateline>
					</opener>
					<lg n="1">
						<l n="1" num="1.1">Au Bois, les lauriers sont coupés,</l>
						<l n="2" num="1.2">Mais le <hi rend="ital">Persil</hi> verdit encore ;</l>
						<l n="3" num="1.3">Au <hi rend="ital">Serpolet</hi>, petits coupés</l>
						<l n="4" num="1.4">Vertueux vont lever l’Aurore…</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">L’Aurore brossant sa palette :</l>
						<l n="6" num="2.2">Kh’ol, carmin et poudre de riz ;</l>
						<l n="7" num="2.3">Pour faire dire — la coquette —</l>
						<l n="8" num="2.4">Qu’on fait bien les ciels à Paris.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">Par ce petit-lever de Mai,</l>
						<l n="10" num="3.2">Le Bois se croit à la campagne :</l>
						<l n="11" num="3.3">Et, fraîchement trait, le Champagne</l>
						<l n="12" num="3.4">Semble de la mousse de lait.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">Là, j’ai vu les <hi rend="ital">Chère Madame</hi></l>
						<l n="14" num="4.2">S’encanailler avec le frais…</l>
						<l n="15" num="4.3">Malgré tout prendre un vrai bain d’âme !</l>
						<l n="16" num="4.4">— Vous vous engommerez après. —</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">… La voix à la note expansive :</l>
						<l n="18" num="5.2">— Vous comprenez ; voici mon truc :</l>
						<l n="19" num="5.3">Je vends mes Memphis, et j’arrive…</l>
						<l n="20" num="5.4">— Cent louis !… — Eh, Eh ! Bibi… — Mon duc ?…</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1">On presse de petites mains :</l>
						<l n="22" num="6.2">— Tiens … assez pur cet attelage. —</l>
						<l n="23" num="6.3">Même les cochers, au dressage,</l>
						<l n="24" num="6.4">Redeviennent simples humains.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1">— Encor toi ! vieille <hi rend="ital">Belle-Impure !</hi></l>
						<l n="26" num="7.2">Toujours, les pieds au plat, tu sors,</l>
						<l n="27" num="7.3">Dans ce déjeuner de nature,</l>
						<l n="28" num="7.4">Fondre un souper à huit ressorts… —</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1">Voici l’école buissonnière :</l>
						<l n="30" num="8.2">Quelques maris jaunes de teint,</l>
						<l n="31" num="8.3">Et qui <hi rend="ital">rentrent dans ta carrière</hi></l>
						<l n="32" num="8.4">D’assez bonne heure … le matin.</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1">Le lapin inquiet s’arrête,</l>
						<l n="34" num="9.2">Un sergent-de-ville s’assied,</l>
						<l n="35" num="9.3">Le sportsman promène sa bête,</l>
						<l n="36" num="9.4">Et le rêveur la sienne — à pied. —</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1">Arthur même a presque une tête,</l>
						<l n="38" num="10.2">Son faux-col s’ouvre matinal…</l>
						<l n="39" num="10.3">Peut-être se sent-il poète,</l>
						<l n="40" num="10.4">Tout comme <hi rend="ital">Byron</hi> — son cheval.</l>
					</lg>
					<lg n="11">
						<l n="41" num="11.1">Diane au petit galop de chasse</l>
						<l n="42" num="11.2">Fait galoper les papillons</l>
						<l n="43" num="11.3">Et caracoler sur sa trace,</l>
						<l n="44" num="11.4">Son Tigre et les vieux beaux Lions.</l>
					</lg>
					<lg n="12">
						<l n="45" num="12.1">Naseaux fumants, grand œil en flamme,</l>
						<l n="46" num="12.2">Crins d’étalon : cheval et femme</l>
						<l part="I" n="47" num="12.3"><hi rend="ital">Saillent de l’avant !</hi>… </l>
						<l part="F" n="47" num="12.3"> — Peu poli.</l>
						<l n="48" num="12.4">— Pardon : <hi rend="ital">maritime</hi> … et joli.</l>
					</lg>
				</div></body></text></TEI>