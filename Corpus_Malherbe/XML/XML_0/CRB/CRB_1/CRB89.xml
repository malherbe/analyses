<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES AMOURS JAUNES</title>
				<title type="medium">Édition électronique</title>
				<author key="CRB">
					<name>
						<forename>Tristan</forename>
						<surname>CORBIÈRE</surname>
					</name>
					<date from="1845" to="1875">1845-1875</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement métrique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4094 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">CRB_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Amours jaunes</title>
						<author>Tristan Corbière</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/ebooks/16883</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Amours jaunes</title>
								<author>Tristan Corbière</author>
								<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k70668p.r=tristan+corbi%C3%A8re.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE DU XIXe SIÈCLE ‒ GLADY FRÈRES, ÉDITEURS</publisher>
									<date when="1873">1873</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1873">1873</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">GENS DE MER</head><div type="poem" key="CRB89">
					<head type="main">AU VIEUX ROSCOFF</head>
					<head type="sub_1">Berceuse en Nord-Ouest mineur</head>
					<lg n="1">
						<l n="1" num="1.1">Trou de flibustiers, vieux nid</l>
						<l n="2" num="1.2">A corsaires ! — dans la tourmente,</l>
						<l n="3" num="1.3">Dors ton bon somme de granit</l>
						<l n="4" num="1.4">Sur tes caves que le flot hante…</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">Ronfle à la mer, ronfle à la brise ;</l>
						<l n="6" num="2.2">Ta corne dans la brume grise,</l>
						<l n="7" num="2.3">Ton pied marin dans les brisans…</l>
						<l n="8" num="2.4">— Dors : tu peux fermer ton œil borgne</l>
						<l n="9" num="2.5">Ouvert sur le large, et qui lorgne</l>
						<l n="10" num="2.6">Les Anglais, depuis trois cents ans.</l>
					</lg>
					<lg n="3">
						<l n="11" num="3.1">— Dors, vieille coque bien ancrée ;</l>
						<l n="12" num="3.2">Les margats et les cormorans</l>
						<l n="13" num="3.3">Tes grands poètes d’ouragans</l>
						<l n="14" num="3.4">Viendront chanter à la marée…</l>
					</lg>
					<lg n="4">
						<l n="15" num="4.1">— Dors, vieille fille-à-matelots ;</l>
						<l n="16" num="4.2">Plus ne te soûleront ces flots</l>
						<l n="17" num="4.3">Qui te faisaient une ceinture</l>
						<l n="18" num="4.4">Dorée, aux nuits rouges de vin,</l>
						<l n="19" num="4.5">De sang, de feu ! — Dors… Sur ton sein</l>
						<l n="20" num="4.6">L’or ne tondra plus en friture.</l>
					</lg>
					<lg n="5">
						<l n="21" num="5.1">— Où sont les noms de tes amants…</l>
						<l n="22" num="5.2">— La mer et la gloire étaient folles ! —</l>
						<l n="23" num="5.3">Noms de lascars ! noms de géants !</l>
						<l n="24" num="5.4">Crachés des gueules d’espingoles…</l>
					</lg>
					<lg n="6">
						<l n="25" num="6.1">Où battaient-ils, ces pavillons,</l>
						<l n="26" num="6.2">Écharpant ton ciel en haillons !…</l>
						<l n="27" num="6.3">— Dors au ciel de plomb sur tes dunes…</l>
						<l n="28" num="6.4">Dors : plus ne viendront ricocher</l>
						<l n="29" num="6.5">Les boulets morts, sur ton clocher</l>
						<l n="30" num="6.6">Criblé — comme un prunier — de prunes…</l>
					</lg>
					<lg n="7">
						<l n="31" num="7.1">— Dors : sous les noires cheminées,</l>
						<l n="32" num="7.2">Écoute rêver tes enfants,</l>
						<l n="33" num="7.3">Mousses du quatre-vingt-dix ans,</l>
						<l n="34" num="7.4">Épaves des belles années…</l>
					</lg>
					<ab type="dot">. . . . . . . . . . . . . . . . . . . . . . .</ab>
					<lg n="8">
						<l n="35" num="8.1">Il dort ton bon canon de fer,</l>
						<l n="36" num="8.2">A plat-ventre aussi dans sa souille,</l>
						<l n="37" num="8.3">Grêlé par les lunes d’hyver…</l>
						<l n="38" num="8.4">Il dort son lourd sommeil de rouille.</l>
					</lg>
					<lg n="9">
						<l n="39" num="9.1">— Va : ronfle au vent, vieux ronfleur,</l>
						<l n="40" num="9.2">Tiens toujours ta gueule enragée</l>
						<l n="41" num="9.3">Braquée à l’Anglais !… et chargée</l>
						<l n="42" num="9.4">De maigre jonc-marin en fleur.</l>
					</lg>
					<closer>
						<dateline>
							<placeName>Roscoff</placeName> — Décembre.
						</dateline>
					</closer>
				</div></body></text></TEI>