<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES AMOURS JAUNES</title>
				<title type="medium">Édition électronique</title>
				<author key="CRB">
					<name>
						<forename>Tristan</forename>
						<surname>CORBIÈRE</surname>
					</name>
					<date from="1845" to="1875">1845-1875</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement métrique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4094 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">CRB_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Amours jaunes</title>
						<author>Tristan Corbière</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/ebooks/16883</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Amours jaunes</title>
								<author>Tristan Corbière</author>
								<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k70668p.r=tristan+corbi%C3%A8re.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE DU XIXe SIÈCLE ‒ GLADY FRÈRES, ÉDITEURS</publisher>
									<date when="1873">1873</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1873">1873</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">GENS DE MER</head><div type="poem" key="CRB87">
					<head type="main">LETTRE DU MEXIQUE</head>
					<opener>
						<dateline>
							<placeName>La Vera-Cruz</placeName> 10 février.
						</dateline>
					</opener>
					<lg n="1">
						<l n="1" num="1.1">Vous m’avez confié le petit. — Il est mort.</l>
						<l n="2" num="1.2">« Et plus d’un camarade avec, pauvre cher être.</l>
						<l n="3" num="1.3">L’équipage … <del reason="analysis" type="syneresis" hand="RR">y</del> en a plus. Il reviendra peut-être</l>
						<l n="4" num="1.4"><space quantity="8" unit="char"></space>Quelques-uns de nous. — C’est le sort » —</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">« Rien n’est beau comme ça — Matelot — pour un homme</l>
						<l n="6" num="2.2">Tout le monde en voudrait à terre — C’est bien sur.</l>
						<l n="7" num="2.3">Sans le désagrément. Rien que ça : Voyez comme</l>
						<l n="8" num="2.4"><space quantity="8" unit="char"></space>Déjà l’apprentissage est dur. »</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">« Je pleure en marquant ça, moi, vieux <hi rend="ital">Frère-la-côte.</hi></l>
						<l n="10" num="3.2">J’aurais donné ma peau joliment sans façon</l>
						<l n="11" num="3.3">Pour vous le renvoyer… Moi, ce n’est pas ma faute :</l>
						<l n="12" num="3.4"><space quantity="8" unit="char"></space>Ce mal-là n’a pas de raison. »</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">« La fièvre est ici comme Mars en carême,</l>
						<l n="14" num="4.2">Au cimetière on va toucher sa ration.</l>
						<l n="15" num="4.3">Le zouave a nommé ça — Parisien quand-même —</l>
						<l n="16" num="4.4"><space quantity="8" unit="char"></space>« <hi rend="ital">Le Jardin d’acclimatation</hi>. »</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">« Consolez-vous. Le monde y crève comme mouches.</l>
						<l n="18" num="5.2">… J’ai trouvé dans son sac des souvenir de cœur :</l>
						<l n="19" num="5.3">Un portrait de fille, et deux petites babouches,</l>
						<l n="20" num="5.4"><space quantity="8" unit="char"></space>Et : marqué — <hi rend="ital">Cadeau pour ma sœur</hi>. » —</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1">« Il fait dire à <hi rend="ital">maman</hi> : qu’il a fait sa prière.</l>
						<l n="22" num="6.2">Au père : qu’il serait mieux mort dans un combat.</l>
						<l n="23" num="6.3">Deux anges étaient là sur son heure dernière :</l>
						<l n="24" num="6.4"><space quantity="8" unit="char"></space>Un matelot. Un vieux soldat. »</l>
					</lg>
					<closer>
						<dateline>
							<placeName>Toulon</placeName> 24 mai.
						</dateline>
					</closer>
				</div></body></text></TEI>