<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES AMOURS JAUNES</title>
				<title type="medium">Édition électronique</title>
				<author key="CRB">
					<name>
						<forename>Tristan</forename>
						<surname>CORBIÈRE</surname>
					</name>
					<date from="1845" to="1875">1845-1875</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement métrique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4094 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">CRB_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Amours jaunes</title>
						<author>Tristan Corbière</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/ebooks/16883</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Amours jaunes</title>
								<author>Tristan Corbière</author>
								<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k70668p.r=tristan+corbi%C3%A8re.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE DU XIXe SIÈCLE ‒ GLADY FRÈRES, ÉDITEURS</publisher>
									<date when="1873">1873</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1873">1873</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">RACCROCS</head><div type="poem" key="CRB69">
					<head type="main">HIDALDO !</head>
					<lg n="1">
						<l n="1" num="1.1">Ils sont fiers ceux-là !… comme poux sur la gale !</l>
						<l n="2" num="1.2">C’est à la don-Juan qu’ils vous <hi rend="ital">font</hi> votre malle.</l>
						<l n="3" num="1.3">Ils ne sentent pas bon, mais ils fleurent le preux :</l>
						<l n="4" num="1.4">Valeureux vauriens, crétins chevalereux !</l>
						<l n="5" num="1.5">Prenant sans demander — toujours suant la race, —</l>
						<l n="6" num="1.6">Et demandant un sol, — mais toujours pleins de grâce.</l>
					</lg>
					<lg n="2">
						<l n="7" num="2.1">Là, j’ai fait le croquis d’un mendiant à cheval :</l>
						<l n="8" num="2.2">— Le Cid … un cid par un <hi rend="ital">été</hi> de carnaval :</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">— Je cheminais — à pieds — traînant une compagne ;</l>
						<l n="10" num="3.2">Le soleil craquelait la route en blanc-d’Espagne ;</l>
						<l n="11" num="3.3">Et <hi rend="ital">le cid</hi> fut sur nous en un temps de galop…</l>
						<l n="12" num="3.4">Là, me pressant entre le mur et le garrot :</l>
						<l n="13" num="3.5">— Ah ! seigneur <hi rend="ital">Cavalier</hi>, d’honneur ! sur ma parole !</l>
						<l n="14" num="3.6">Je mendie à genoux : un oignon … une obole ?… —</l>
						<l n="15" num="3.7">(Et son cheval paissait mon col.) — Pauvre animal,</l>
						<l n="16" num="3.8">Il vous aime déjà ! Ne prenez pas à mal…</l>
						<l n="17" num="3.9">— Au large ! — Oh ! mais : au moins votre bout de cigare ?…</l>
						<l n="18" num="3.10">La Vierge vous le rende. — Allons : au large ! ou : gare !</l>
						<l n="19" num="3.11">(Son pied nu prenait ma poche en étrier.)</l>
						<l n="20" num="3.12">— Pitié pour un infirme, o seigneur-cavalier…</l>
						<l n="21" num="3.13">— Tiens donc un sou… — Senor, que jamais je n’oublie</l>
						<l n="22" num="3.14">Votre Grâce ! Pardon, je vous ai retardé…</l>
						<l n="23" num="3.15">Senora : Merci, toi ! pour être si jolie…</l>
						<l n="24" num="3.16">Ma Jolie, et : Merci pour m’avoir regardé !</l>
					</lg>
					<closer>
						<placeName>(<hi rend="ital">Cosas de Espana.</hi>)</placeName>
					</closer>
				</div></body></text></TEI>