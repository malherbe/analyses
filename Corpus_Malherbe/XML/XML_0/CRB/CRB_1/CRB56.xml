<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES AMOURS JAUNES</title>
				<title type="medium">Édition électronique</title>
				<author key="CRB">
					<name>
						<forename>Tristan</forename>
						<surname>CORBIÈRE</surname>
					</name>
					<date from="1845" to="1875">1845-1875</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement métrique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4094 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">CRB_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Amours jaunes</title>
						<author>Tristan Corbière</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/ebooks/16883</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Amours jaunes</title>
								<author>Tristan Corbière</author>
								<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k70668p.r=tristan+corbi%C3%A8re.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE DU XIXe SIÈCLE ‒ GLADY FRÈRES, ÉDITEURS</publisher>
									<date when="1873">1873</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1873">1873</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">RACCROCS</head><div type="poem" key="CRB56">
					<head type="main">DÉCOURAGEUX</head>
					<lg n="1">
						<l n="1" num="1.1">Ce fut un vrai poète : Il n’avait pas de chant.</l>
						<l n="2" num="1.2">Mort, il aimait le jour et dédaigna de geindre.</l>
						<l n="3" num="1.3">Peintre : il aimait son art — Il oublia de peindre…</l>
						<l n="4" num="1.4">Il voyait trop — Et voir est un aveuglement.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">— Songe-creux : bien profond il resta dans son rêve ;</l>
						<l n="6" num="2.2">Sans lui donner la forme en baudruche qui crève,</l>
						<l n="7" num="2.3">Sans <hi rend="ital">ouvrir le bonhomme</hi>, et se chercher dedans.</l>
					</lg>
					<lg n="3">
						<l n="8" num="3.1">— Par héros de roman : il adorait la brune,</l>
						<l n="9" num="3.2">Sans voir s’elle était blonde… Il adorait la lune ;</l>
						<l n="10" num="3.3">Mais il n’aima jamais — Il n’avait pas le temps.</l>
					</lg>
					<lg n="4">
						<l n="11" num="4.1">— Chercheur infatigable : Ici-bas où l’on rame,</l>
						<l n="12" num="4.2">Il regardait ramer, du haut de sa grande âme.</l>
						<l n="13" num="4.3">Fatigué de pitié pour ceux qui ramaient bien…</l>
					</lg>
					<lg n="5">
						<l n="14" num="5.1">Mineur de la pensée : il touchait son front blême,</l>
						<l n="15" num="5.2">Pour gratter un bouton ou gratter le problème</l>
						<l n="16" num="5.3"><space quantity="8" unit="char"></space>Qui travaillait là — Faire rien. —</l>
					</lg>
					<lg n="6">
						<l n="17" num="6.1">— Il parlait : « Oui, la Muse est stérile ! elle est fille</l>
						<l n="18" num="6.2">D’amour, d’oisiveté, de prostitution ;</l>
						<l n="19" num="6.3">Ne la déformez pas en ventre de famille</l>
						<l n="20" num="6.4">Que couvre un étalon pour la production ! »</l>
					</lg>
					<lg n="7">
						<l n="21" num="7.1">« O vous tous qui gâchez, maçons de la pensée !</l>
						<l n="22" num="7.2">Vous tous que son caprice a touchés en amants,</l>
						<l n="23" num="7.3">— Vanité, vanité — La folle nuit passée,</l>
						<l n="24" num="7.4">Vous l’affichez <hi rend="ital">en charge</hi> aux yeux ronds des manants ! »</l>
					</lg>
					<lg n="8">
						<l n="25" num="8.1">« Elle vous effleurait, vous, comme chats qu’on noie,</l>
						<l n="26" num="8.2">Vous avez accroché son aile ou son réseau,</l>
						<l n="27" num="8.3">Fiers d’avoir dans vos mains un bout de plume d’oie,</l>
						<l n="28" num="8.4">Ou des poils à gratter, en façon de pinceau ! »</l>
					</lg>
					<lg n="9">
						<l n="29" num="9.1">— Il disait : « O naïf Océan ! O fleurettes,</l>
						<l n="30" num="9.2">Ne sommes-nous pas là, sans peintres, ni poètes !…</l>
						<l n="31" num="9.3">Quel vitrier a peint ! quel aveugle a chanté !…</l>
						<l n="32" num="9.4">Et quel vitrier chante en raclant sa palette,</l>
						<l n="33" num="9.5">Ou quel aveugle a peint avec sa clarinette !</l>
						<l part="I" n="34" num="9.6">— Est-ce l’art ?… » </l>
					</lg>
					<lg n="10">
						<l part="F" n="34">— Lui resta dans le Sublime Bête</l>
						<l n="35" num="10.1">Noyer son orgueil vide et sa virginité.</l>
					</lg>
					<closer>
						<placeName>Méditerranée</placeName>.
					</closer>
				</div></body></text></TEI>