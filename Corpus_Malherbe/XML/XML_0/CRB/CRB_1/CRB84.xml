<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES AMOURS JAUNES</title>
				<title type="medium">Édition électronique</title>
				<author key="CRB">
					<name>
						<forename>Tristan</forename>
						<surname>CORBIÈRE</surname>
					</name>
					<date from="1845" to="1875">1845-1875</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement métrique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4094 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">CRB_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Amours jaunes</title>
						<author>Tristan Corbière</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/ebooks/16883</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Amours jaunes</title>
								<author>Tristan Corbière</author>
								<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k70668p.r=tristan+corbi%C3%A8re.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE DU XIXe SIÈCLE ‒ GLADY FRÈRES, ÉDITEURS</publisher>
									<date when="1873">1873</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1873">1873</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">GENS DE MER</head><div type="poem" key="CRB84">
					<head type="main">LA GOUTTE</head>
					<lg n="1">
						<l n="1" num="1.1">Sous un seul hunier — le dernier — à la cape,</l>
						<l n="2" num="1.2">Le navire était soûl ; l’eau sur nous faisait nappe.</l>
						<l n="3" num="1.3">— Aux pompes, faillis chiens ! — L’équipage fit — non. —</l>
					</lg>
					<lg n="2">
						<l part="I" n="4" num="2.1">— Le hunier ! le hunier !… </l>
						<l part="F" n="4" num="2.1">C’est un coup de canon.</l>
						<l n="5" num="2.2">Un grand froufrou de soie à travers la tourmente.</l>
					</lg>
					<lg n="3">
						<l n="6" num="3.1">— Le hunier emporté ! — C’est la fin. Quelqu’un chante. —</l>
						<l n="7" num="3.2">— Tais-toi, Lascar ! — Tantôt. — Le hunier emporté !…</l>
						<l n="8" num="3.3">— Pare le foc, quelqu’un de bonne volonté !…</l>
						<l n="9" num="3.4">— Moi. — Toi, lascar ? — Je chantais ça, moi, capitaine.</l>
						<l n="10" num="3.5">— Va. — Non : la goutte avant ? — Non, après. — Pas la peine :</l>
						<l part="I" n="11" num="3.6">La grande tasse est là pour un coup… — </l>
						<l part="F" n="11" num="3.6">Pour braver,</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1">Quoi ! mourir pour mourir et ne rien sauver…</l>
						<l n="13" num="4.2">— Fais comme tu pourras : Coupe. Et gare à ta drisse.</l>
						<l part="I" n="14" num="4.3">— Merci — </l>
						<l part="F" n="14" num="4.3">D’un bond du singe il saute, de la lisse,</l>
						<l n="15" num="4.4">Sur le beaupré noyé, dans les agrès pendants.</l>
						<l part="I" n="16" num="4.5">— Bravo ! — </l>
						<l part="F" n="16" num="4.5">Nous regardions, la mort entre les dents.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">— Garçons, tous à la drisse ! à nous ! pare l’écoute !…</l>
						<l n="18" num="5.2">(Le coup de grâce enfin… ) — Hisse ! barre au vent toute !</l>
						<l part="I" n="19" num="5.3">Hurrah ! nous abattons !… — </l>
						<l part="F" n="19" num="5.3">Et le foc déferlé</l>
						<l n="20" num="5.4">Redresse en un clin d’œil le navire acculé.</l>
						<l n="21" num="5.5">C’est le salut à nous qui bat dans cette loque</l>
						<l n="22" num="5.6">Fuyant devant le temps ! Encor paré la coque !</l>
						<l part="I" n="23" num="5.7">— Hurrah pour le lascar ! — Le lascar ?… </l>
						<l part="F" n="23" num="5.7"> — A la mer !</l>
						<l n="24" num="5.8">— Disparu ? — Disparu — Bon, ce n’est pas trop cher.</l>
			</lg>
			<ab type="dot">. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .</ab>
			<lg n="6">
						<l n="25" num="6.1">— Ouf ! c’est fait — Toi, Lascar ! — moi, Lascar, capitaine,</l>
						<l n="26" num="6.2">La lame m’a rincé de dessus la poulaine,</l>
						<l n="27" num="6.3">Le même coup de mer m’a ramené gratis…</l>
						<l n="28" num="6.4">Allons, mes poux n’auront pas besoin d’onguent-gris.</l>
					</lg>
					<lg n="7">
						<l n="29" num="7.1">— Accoste, tout le monde ! Et toi, Lascar, écoute :</l>
						<l n="30" num="7.2">Nous te devons la vie… — Après ? — Pour ça ?… — La goutte !</l>
						<l n="31" num="7.3">Mais c’était pas pour ça, n’allez pas croire, au moins…</l>
						<l n="32" num="7.4">— Viens m’embrasser ! — Attrape à torcher les grouins.</l>
						<l n="33" num="7.5">J’suis pas beau, capitain’, mais, soit dit en famille,</l>
						<l n="34" num="7.6">Je vous ai fait plaisir plus qu’une belle fille ?…</l>
			</lg>
			<ab type="dot">. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .</ab>
			<lg n="8">
						<l n="35" num="8.1">Le capitaine mit, ce jour, sur son rapport :</l>
						<l n="36" num="8.2">— <hi rend="ital">Gros temps. Laissé porter. Rien de neuf à bord</hi>. —</l>
					</lg>
					<closer>
						<placeName>À bord</placeName>.
					</closer>
				</div></body></text></TEI>