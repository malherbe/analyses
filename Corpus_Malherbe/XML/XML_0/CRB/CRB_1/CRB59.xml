<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES AMOURS JAUNES</title>
				<title type="medium">Édition électronique</title>
				<author key="CRB">
					<name>
						<forename>Tristan</forename>
						<surname>CORBIÈRE</surname>
					</name>
					<date from="1845" to="1875">1845-1875</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement métrique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4094 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">CRB_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Amours jaunes</title>
						<author>Tristan Corbière</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/ebooks/16883</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Amours jaunes</title>
								<author>Tristan Corbière</author>
								<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k70668p.r=tristan+corbi%C3%A8re.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE DU XIXe SIÈCLE ‒ GLADY FRÈRES, ÉDITEURS</publisher>
									<date when="1873">1873</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1873">1873</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">RACCROCS</head><div type="poem" key="CRB59">
					<head type="main">LITANIE DU SOMMEIL</head>
					<opener>
						<epigraph>
							<cit>
								<quote>J’ai scié le sommeil ! </quote>
								<bibl>
									<name>MACBETH</name>.
								</bibl>
							</cit>
						</epigraph>
					</opener>
					<lg n="1">
						<l n="1" num="1.1">Vous qui ronflez au coin d’une épouse endormie,</l>
						<l n="2" num="1.2">RUMINANT ! savez-vous ce soupir : L’INSOMNIE ?</l>
						<l n="3" num="1.3">— Avez-vous vu la Nuit, et le Sommeil ailé,</l>
						<l n="4" num="1.4">Papillon de minuit dans la nuit envolé,</l>
						<l n="5" num="1.5">Sans un coup d’aile ami, vous laissant sur le seuil,</l>
						<l n="6" num="1.6">Seul, dans le pot-au-noir au couvercle sans œil :</l>
						<l n="7" num="1.7">— Avez-vous navigué ?… La pensée est la houle</l>
						<l n="8" num="1.8">Ressassant le galet : ma tête … votre boule.</l>
						<l n="9" num="1.9">— Vous êtes-vous laissé voyager en ballon ?</l>
						<l n="10" num="1.10">— Non ? — bien, c’est l’insomnie. — Un grand coup de talon</l>
						<l n="11" num="1.11">Là ! — Vous voyez cligner des chandelles étranges :</l>
						<l n="12" num="1.12">Une femme, une Gloire en soleil, des archanges…</l>
						<l n="13" num="1.13">Et, la nuit s’éteignant dans le jour à demi,</l>
						<l n="14" num="1.14">Vous vous réveillez coi, sans vous être endormi.</l>
					</lg>
					<ab type="star">*</ab>
					<lg n="2">
						<l n="15" num="2.1">Sommeil ! écoute-moi : je parlerai bien bas :</l>
						<l n="16" num="2.2">Sommeil — Ciel-de-lit de ceux qui n’en ont pas !</l>
					</lg>
					<lg n="3">
						<l n="17" num="3.1">Toi qui planes avec l’Albatros des tempêtes,</l>
						<l n="18" num="3.2">Et qui t’assieds sur les casques-à-mèche honnêtes !</l>
						<l n="19" num="3.3">SOMMEIL ! — Oreiller blanc des vierges assez bêtes !</l>
						<l n="20" num="3.4">Et Soupape à secret des vierges assez faites !</l>
						<l n="21" num="3.5">— Moelleux Matelas de l’échine en arête !</l>
						<l n="22" num="3.6">Sac noir où les chassés s’en vont cacher leur tête !</l>
						<l n="23" num="3.7">Rôdeur de boulevard extérieur ! Proxénète !</l>
						<l n="24" num="3.8">Pays où le muet se réveille prophète !</l>
						<l n="25" num="3.9">Césure du vers long, et Rime du poète !</l>
					</lg>
					<lg n="4">
						<l n="26" num="4.1">SOMMEIL ! — Loup-Garou gris ! Sommeil Noir de fumée !</l>
						<l n="27" num="4.2">SOMMEIL ! — Loup de velours, de dentelle embaumée !</l>
						<l n="28" num="4.3">Baiser de l’Inconnue, et Baiser de l’Aimée !</l>
						<l n="29" num="4.4">— SOMMEIL ! Voleur de nuit ! Folle-brise pâmée !</l>
						<l n="30" num="4.5">Parfum qui monte au ciel des tombes parfumées !</l>
						<l n="31" num="4.6"><hi rend="ital">Carrosse à Cendrillon</hi> ramassant <hi rend="ital">les Traînées !</hi></l>
						<l n="32" num="4.7">Obscène Confesseur des dévotes mort-nées !</l>
					</lg>
					<lg n="5">
						<l n="33" num="5.1">Toi qui viens, comme un chien, lécher la vieille plaie</l>
						<l n="34" num="5.2">Du martyr que la mort tiraille sur sa claie !</l>
						<l n="35" num="5.3">O Sourire forcé de la crise tuée !</l>
						<l n="36" num="5.4">SOMMEIL Brise alizée ! Aurorale buée !</l>
					</lg>
					<lg n="6">
						<l n="37" num="6.1">Trop-plein de l’existence, et Torchon neuf qu’on passe</l>
						<l n="38" num="6.2">Au CAFÉ DE LA VIE, à chaque assiette grasse !</l>
						<l n="39" num="6.3">Grain d’ennui qui nous pleut de l’ennui des espaces !</l>
						<l n="40" num="6.4">Chose qui court encor, sans sillage et sans traces !</l>
						<l n="41" num="6.5">Pont-levis des fossés ! Passage des impasses !</l>
					</lg>
					<lg n="7">
						<l n="42" num="7.1">SOMMEIL ! — Caméléon tout pailleté d’étoiles !</l>
						<l n="43" num="7.2">Vaisseau-fantôme errant tout seul à pleines voiles !</l>
						<l n="44" num="7.3">Femme du rendez-vous, s’enveloppant d’un voile !</l>
						<l n="45" num="7.4">SOMMEIL ! — Triste Araignée, étends sur moi ta toile !</l>
					</lg>
					<lg n="8">
						<l n="46" num="8.1">SOMMEIL auréolé ! féerique Apothéose,</l>
						<l n="47" num="8.2">Exaltant le grabat du déclassé qui pose !</l>
						<l n="48" num="8.3">Patient Auditeur de l’incompris qui cause !</l>
						<l n="49" num="8.4">Refuge du pêcheur, de l’innocent qui n’ose !</l>
						<l n="50" num="8.5">Domino ! Diables-bleus ! Ange-gardien rose !</l>
					</lg>
					<lg n="9">
						<l n="51" num="9.1">Voix mortelle qui vibre aux immortelles ondes !</l>
						<l n="52" num="9.2">Réveil des échos morts et des choses profondes,</l>
						<l n="53" num="9.3">— Journal du soir : TEMPS, SIÈCLE et REVUE DES DEUX MONDES !</l>
					</lg>
					<lg n="10">
						<l n="54" num="10.1">Fontaine de Jouvence et Borne de l’envie !</l>
						<l n="55" num="10.2">— Toi qui viens assouvir la faim inassouvie !</l>
						<l n="56" num="10.3">Toi qui viens délier la pauvre âme ravie,</l>
						<l n="57" num="10.4">Pour la noyer d’air pur au large de la vie !</l>
					</lg>
					<lg n="11">
						<l n="58" num="11.1">Toi qui, le rideau bas, viens lâcher la ficelle</l>
						<l n="59" num="11.2">Du Chat, du Commissaire, et de Polichinelle,</l>
						<l n="60" num="11.3">Du violoncelliste et de son violoncelle,</l>
						<l n="61" num="11.4">Et la lyre de ceux dont la Muse est pucelle !</l>
					</lg>
					<lg n="12">
						<l n="62" num="12.1">Grand Dieu, Maître de tout ! Maître de ma Maîtresse</l>
						<l n="63" num="12.2">Qui me trompe avec toi — l’amoureuse Paresse —</l>
						<l n="64" num="12.3">O bain de voluptés ! éventail de caresse !</l>
					</lg>
					<lg n="13">
						<l n="65" num="13.1">SOMMEIL ! Honnêteté des voleurs ! Clair de lune</l>
						<l n="66" num="13.2">Des yeux crevés ! — SOMMEIL ! Roulette de fortune</l>
						<l n="67" num="13.3">De tout infortuné ! Balayeur de rancune !</l>
					</lg>
					<lg n="14">
						<l n="68" num="14.1">O corde-de-pendu de la Planète lourde !</l>
						<l n="69" num="14.2">Accord éolien hantant l’oreille sourde !</l>
						<l n="70" num="14.3">— Beau Conteur à dormir debout : conte ta bourde ?…</l>
						<l n="71" num="14.4">SOMMEIL ! — Foyer de ceux dont morte est la falourde !</l>
					</lg>
					<lg n="15">
						<l n="72" num="15.1">SOMMEIL — Foyer de ceux dont la falourde est morte !</l>
						<l n="73" num="15.2">Passe-partout de ceux qui sont mis à la porte !</l>
						<l n="74" num="15.3">Face-de-bois pour les créanciers et leur sorte !</l>
						<l n="75" num="15.4">Paravent du mari contre la femme-forte !</l>
					</lg>
					<lg n="16">
						<l n="76" num="16.1">Surface des profonds ! Profondeur des jocrisses !</l>
						<l n="77" num="16.2">Nourrice du soldat et Soldat des nourrices !</l>
						<l n="78" num="16.3">Paix des juges-de-paix ! Police des polices !</l>
						<l n="79" num="16.4">SOMMEIL ! — Belle-de-nuit entrouvrant son calice !</l>
						<l n="80" num="16.5">Larve, Ver-luisant et nocturne Cilice !</l>
						<l n="81" num="16.6">Puits de vérité de monsieur la Palisse !</l>
					</lg>
					<lg n="17">
						<l n="82" num="17.1">Soupirail d’en haut ! Rais de poussière impalpable,</l>
						<l n="83" num="17.2">Qui viens rayer du jour la lanterne implacable !</l>
					</lg>
					<lg n="18">
						<l n="84" num="18.1">Sommeil — écoute-moi, je parlerai bien bas :</l>
						<l n="85" num="18.2">Crépuscule flottant de <hi rend="ital">l’être ou n’être pas !</hi>…</l>
					</lg>
					<lg n="19">
						<l n="86" num="19.1">Sombre lucidité ! Clair-obscur ! Souvenir</l>
						<l n="87" num="19.2">De l’Inouï ! Marée ! Horizon ! Avenir !</l>
						<l n="88" num="19.3">Conte des <hi rend="ital">Mille-et-une-nuits</hi> doux à ouïr !</l>
						<l n="89" num="19.4">Lampiste d’<hi rend="ital">Aladin</hi> qui sais nous éblouir !</l>
						<l n="90" num="19.5">Eunuque noir ! muet blanc ! Derviche ! Djinn ! Fakir !</l>
						<l n="91" num="19.6">Conte de Fée où <hi rend="ital">le Roi</hi> se laisse assoupir !</l>
						<l n="92" num="19.7">Forêt-vierge où <hi rend="ital">Peau-d’Âne</hi> en pleurs va s’accroupir !</l>
						<l n="93" num="19.8">Garde-manger où l’Ogre encor va s’assouvir !</l>
						<l n="94" num="19.9">Tourelle où <hi rend="ital">ma sœur Anne</hi> allait voir rien venir !</l>
						<l n="95" num="19.10">Tour où <hi rend="ital">dame Malbrouck</hi> voyait page courir…</l>
						<l n="96" num="19.11">Où <hi rend="ital">Femme Barbe-Bleue</hi> oyait l’heure mourir !…</l>
						<l n="97" num="19.12">Où <hi rend="ital">Belle au-Bois-Dormant</hi> dormait dans un soupir !</l>
					</lg>
					<lg n="20">
						<l n="98" num="20.1">Cuirasse du petit ! Camisole du fort !</l>
						<l n="99" num="20.2">Lampion des éteints ! éteignoir du remord !</l>
						<l n="100" num="20.3">Conscience du juste, et du pochard qui dort !</l>
						<l n="101" num="20.4">Contre-poids des poids faux de l’épicier de Sort !</l>
						<l n="102" num="20.5">Portrait enluminé de la livide Mort !</l>
					</lg>
					<lg n="21">
						<l n="103" num="21.1">Grand fleuve où Cupidon va retremper ses dards</l>
						<l n="104" num="21.2">SOMMEIL ! — Corne de Diane, et corne du cornard !</l>
						<l n="105" num="21.3">Couveur de magistrats et Couveur de lézards !</l>
						<l n="106" num="21.4">Marmite d’<hi rend="ital">Arlequin</hi> ! — bout de cuir, lard, homard —</l>
						<l n="107" num="21.5">SOMMEIL ! — Noce de ceux qui sont dans les beaux-arts.</l>
					</lg>
					<lg n="22">
						<l n="108" num="22.1">Boulet des forcenés, Liberté des captifs !</l>
						<l n="109" num="22.2">Sabbat du somnambule et Relai des poussifs ! —</l>
						<l n="110" num="22.3">SOMME ! Actif du passif et Passif de l’actif !</l>
						<l n="111" num="22.4">Pavillon de <hi rend="ital">la Folle</hi> et <hi rend="ital">Folle</hi> du poncif !…</l>
						<l n="112" num="22.5">— O viens changer de patte au cormoran pensif !</l>
					</lg>
					<lg n="23">
						<l n="113" num="23.1">O brun Amant de l’Ombre ! Amant honteux du jour !</l>
						<l n="114" num="23.2">Bal de nuit où Psyché veut démasquer l’Amour !</l>
						<l n="115" num="23.3">Grosse Nudité du chanoine en jupon court !</l>
						<l n="116" num="23.4">Panier-à-salade idéal ! Banal four !</l>
						<l n="117" num="23.5">Omnibus où, dans l’Orbe, on fait pour rien un tour</l>
					</lg>
					<lg n="24">
						<l n="118" num="24.1">Sommeil ! Drame hagard ! Sommeil, molle Langueur !</l>
						<l n="119" num="24.2">Bouche d’or du silence et Bâillon du blagueur !</l>
						<l n="120" num="24.3">Berceuse des vaincus ! Perchoir des coqs vainqueurs !</l>
						<l n="121" num="24.4">Alinéa du livre où dorment les longueurs !</l>
					</lg>
					<lg n="25">
						<l n="122" num="25.1">Du jeune homme rêveur Singulier Féminin !</l>
						<l n="123" num="25.2">De la femme rêvant pluriel masculin !</l>
					</lg>
					<lg n="26">
						<l n="124" num="26.1">SOMMEIL ! — Râtelier du Pégase fringant !</l>
						<l n="125" num="26.2">SOMMEIL ! — Petite pluie abattant l’ouragan !</l>
						<l n="126" num="26.3">SOMMEIL ! — Dédale vague où vient le revenant !</l>
						<l n="127" num="26.4">SOMMEIL ! — Long corridor où plangore le vent !</l>
					</lg>
					<lg n="27">
						<l n="128" num="27.1">Néant du fainéant ! Lazzarone infini !</l>
						<l n="129" num="27.2">Aurore boréale au sein du jour terni !</l>
					</lg>
					<lg n="28">
						<l n="130" num="28.1">Sommeil ! — Autant de pris sur notre éternité !</l>
						<l n="131" num="28.2">Tour du cadran <hi rend="ital">à blanc !</hi> Clou du Mont-de-Piété !</l>
						<l n="132" num="28.3">Héritage en Espagne à tout déshérité !</l>
						<l n="133" num="28.4">Coup de rapière dans l’eau du fleuve Léthé !</l>
						<l n="134" num="28.5">Génie au nimbe d’or des grands hallucinés</l>
						<l n="135" num="28.6">Nid des petits hiboux ! Aile des déplumés !</l>
					</lg>
					<lg n="29">
						<l n="136" num="29.1">Immense Vache à lait dont nous sommes les veaux !</l>
						<l n="137" num="29.2">Arche où le hère et le boa changent de peaux !</l>
						<l n="138" num="29.3">Arc-en-ciel miroitant ! Faux du vrai ! Vrai du faux !</l>
						<l n="139" num="29.4">Ivresse que la brute appelle le repos !</l>
						<l n="140" num="29.5">Sorcière de Bohême à sayon d’oripeaux !</l>
						<l n="141" num="29.6">Tityre sous l’ombrage essayant des pipeaux !</l>
						<l n="142" num="29.7">Temps qui porte un chibouck à la place de faux !</l>
						<l n="143" num="29.8">Parque qui met un peu d’huile à ses ciseaux !</l>
						<l n="144" num="29.9">Parque qui met un peu de chanvre à ses fuseaux !</l>
						<l n="145" num="29.10">Chat qui joue avec le peloton d’Atropos !</l>
					</lg>
					<lg n="30">
						<l n="146" num="30.1">SOMMEIL ! — Manne de grâce au cœur disgracié !</l>
						<ab type="dot">. . . . . . . . . . . . . . . . . . . . . . . . . . . .</ab>
					</lg>
					<lg n="31">
						<l n="147" num="31.1">LE SOMMEIL S’ÉVEILLANT ME DIT : TU M’AS SCIÉ.</l>
						<ab type="dot">. . . . . . . . . . . . . . . . . . . . . . . . . . . .</ab>
					</lg>
					<ab type="star">*</ab>
					<lg n="32">
						<l n="148" num="32.1">Toi qui souffles dessus une épouse enrayée,</l>
						<l n="149" num="32.2">RUMINANT ! dilatant ta pupille éraillée ;</l>
						<l n="150" num="32.3">Sais-tu ?… Ne sais-tu pas ce soupir — LE RÉVEIL ! —</l>
						<l n="151" num="32.4">Qui baille au ciel, parmi les crins d’or du soleil</l>
						<l n="152" num="32.5">Et les crins fous de ta Déesse ardente et blonde ?…</l>
						<l n="153" num="32.6">— Non ?… — Sais tu le réveil du philosophe immonde</l>
						<l n="154" num="32.7">— Le Porc — rognonnant sa prière du matin ;</l>
						<l n="155" num="32.8">Ou le réveil, extrait-d’âge de la catin ?…</l>
						<l n="156" num="32.9">As-tu jamais sonné le réveil de la meute ;</l>
						<l n="157" num="32.10">As-tu jamais senti l’éveil sourd de l’émeute,</l>
						<l n="158" num="32.11">Ou le réveil de plomb du malade fini ?…</l>
						<l n="159" num="32.12">As-tu vu s’étirer l’œil des Lazzaroni ?…</l>
						<l n="160" num="32.13">Sais-tu ?… ne sais-tu pas le chant de l’alouette ?</l>
						<l n="161" num="32.14">— Non — Gluants sont tes cils, pâteuse est ta luette.</l>
						<l n="162" num="32.15">Ruminant ! Tu n’as pas L’INSOMNIE, éveillé ;</l>
						<l n="163" num="32.16">Tu n’as pas LE SOMMEIL, ô Sac ensommeillé !</l>
					</lg>
					<closer>
						<placeName>(<hi rend="ital">Lits divers — Une nuit de jour</hi>.)</placeName>
					</closer>
				</div></body></text></TEI>