<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES AMOURS JAUNES</title>
				<title type="medium">Édition électronique</title>
				<author key="CRB">
					<name>
						<forename>Tristan</forename>
						<surname>CORBIÈRE</surname>
					</name>
					<date from="1845" to="1875">1845-1875</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement métrique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4094 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">CRB_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Amours jaunes</title>
						<author>Tristan Corbière</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/ebooks/16883</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Amours jaunes</title>
								<author>Tristan Corbière</author>
								<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k70668p.r=tristan+corbi%C3%A8re.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE DU XIXe SIÈCLE ‒ GLADY FRÈRES, ÉDITEURS</publisher>
									<date when="1873">1873</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1873">1873</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LES AMOURS JAUNES</head><div type="poem" key="CRB16">
					<head type="main">I SONNET</head>
					<head type="sub_1">AVEC LA MANIÈRE DE S’EN SERVIR</head>
					<opener>
						<epigraph>
							<cit>
								<quote>Réglons notre papier et formons bien nos lettres</quote>
							</cit>
						</epigraph>
					</opener>
					<lg n="1">
						<l n="1" num="1.1">Vers filés à la main et d’un pied uniforme,</l>
						<l n="2" num="1.2">Emboîtant bien le pas, par quatre en peloton ;</l>
						<l n="3" num="1.3">Qu’en marquant la césure, un des quatre s’endorme…</l>
						<l n="4" num="1.4">Ça peut dormir debout comme soldats de plomb.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">Sur le <hi rend="ital">railway</hi> du Pinde est la ligne, la forme ;</l>
						<l n="6" num="2.2">Aux fils du télégraphe : — on en suit quatre, en long ;</l>
						<l n="7" num="2.3">A chaque pieu, la rime — exemple : <hi rend="ital">chloroforme</hi>,</l>
						<l n="8" num="2.4">— Chaque vers est un fil, et la rime un jalon.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">— Télégramme sacré — 20<add reason="analysis" type="phonemization" rend="hidden" hand="RR">vingt</add> mots. — Vite à mon aide…</l>
						<l n="10" num="3.2">(Sonnet — c’est un sonnet — ) ô Muse d’Archimède !</l>
						<l n="11" num="3.3">— La preuve d’un sonnet est par l’addition :</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1">— Je pose 4<add reason="analysis" type="phonemization" rend="hidden" hand="RR">quatr</add> et 4<add reason="analysis" type="phonemization" rend="hidden" hand="RR">quatr</add> = 8<add reason="analysis" type="phonemization" rend="hidden" hand="RR">huit</add> ! Alors je procède,</l>
						<l n="13" num="4.2">En posant 3<add reason="analysis" type="phonemization" rend="hidden" hand="RR">trois</add> et 3<add reason="analysis" type="phonemization" rend="hidden" hand="RR">trois</add> ! — Tenons Pégase raide :</l>
						<l n="14" num="4.3">« Ô lyre ! Ô délire ! Ô… » — Sonnet — Attention !</l>
					</lg>
					<closer>
						<dateline>
							<placeName>Pic de la Maladetta</placeName> — Août.
						</dateline>
					</closer>
				</div></body></text></TEI>