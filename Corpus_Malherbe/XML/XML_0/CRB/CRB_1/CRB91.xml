<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES AMOURS JAUNES</title>
				<title type="medium">Édition électronique</title>
				<author key="CRB">
					<name>
						<forename>Tristan</forename>
						<surname>CORBIÈRE</surname>
					</name>
					<date from="1845" to="1875">1845-1875</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement métrique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4094 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">CRB_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Amours jaunes</title>
						<author>Tristan Corbière</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/ebooks/16883</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Amours jaunes</title>
								<author>Tristan Corbière</author>
								<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k70668p.r=tristan+corbi%C3%A8re.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE DU XIXe SIÈCLE ‒ GLADY FRÈRES, ÉDITEURS</publisher>
									<date when="1873">1873</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1873">1873</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">GENS DE MER</head><div type="poem" key="CRB91">
					<head type="main">LE NAUFRAGEUR</head>
					<lg n="1">
						<l n="1" num="1.1">Si ce n’était pas vrai — Que je crève !</l>
						<ab type="dot">. . . . . . . . . . . . . . . . . . . . . . . . . . . .</ab>
						<l n="2" num="1.2"><space quantity="8" unit="char"></space>J’ai vu dans mes yeux, dans mon rêve,</l>
						<l n="3" num="1.3"><space quantity="8" unit="char"></space>La NOTRE-DAME DES BRISANS</l>
						<l n="4" num="1.4"><space quantity="8" unit="char"></space>Qui jetait à ses pauvres gens</l>
						<l n="5" num="1.5"><space quantity="8" unit="char"></space>Un gros navire sur leur grève…</l>
						<l n="6" num="1.6"><space quantity="8" unit="char"></space>Sur la grève des Kerlouans</l>
						<l n="7" num="1.7"><space quantity="8" unit="char"></space>Aussi goélands que les goélands.</l>
					</lg>
					<lg n="2">
						<l n="8" num="2.1">Le sort est dans l’eau : le cormoran nage,</l>
						<l n="9" num="2.2">Le vent bat en côte, et c’est le <hi rend="ital">Mois Noir</hi>…</l>
						<l n="10" num="2.3">Oh ! moi je sens bien de loin le naufrage !</l>
						<l n="11" num="2.4">Moi j’entends là-haut chasser le nuage.</l>
						<l n="12" num="2.5">Moi je vois profond dans la nuit, sans voir !</l>
					</lg>
					<lg n="3">
						<l n="13" num="3.1"><space quantity="8" unit="char"></space>Moi je siffle quand la mer gronde,</l>
						<l n="14" num="3.2"><space quantity="8" unit="char"></space>Oiseau de malheur à poil roux !…</l>
						<l n="15" num="3.3"><space quantity="8" unit="char"></space>J’ai promis aux douaniers de ronde,</l>
						<l n="16" num="3.4"><space quantity="8" unit="char"></space>Leur part, pour rester dans leurs trous…</l>
						<l n="17" num="3.5"><space quantity="8" unit="char"></space>Que je sois seul ! — oiseau d’épave</l>
						<l n="18" num="3.6"><space quantity="8" unit="char"></space>Sur les brisans que la mer lave…</l>
						<ab type="dot">. . . . . . . . . . . . . . . . . . . . . . . . . . . .</ab>
						<l n="19" num="3.7"><space quantity="8" unit="char"></space>Oiseau de malheur à poil roux !</l>
					</lg>
					<lg n="4">
						<l n="20" num="4.1"><space quantity="8" unit="char"></space> — Et qu’il vente la peau du diable !</l>
						<l n="21" num="4.2"><space quantity="8" unit="char"></space>Je sens ça déjà sous ma peau.</l>
						<l n="22" num="4.3"><space quantity="8" unit="char"></space>La mer moutonne !… — Ho, mon troupeau !</l>
						<l n="23" num="4.4"><space quantity="8" unit="char"></space> — C’est moi le berger, sur le sable…</l>
					</lg>
					<lg n="5">
						<l n="24" num="5.1">L’enfer fait l’amour. — Je ris comme un mort —</l>
						<l n="25" num="5.2">Sautez sous le <hi rend="ital">Hû !</hi>… le <hi rend="ital">Hû</hi> des rafales,</l>
						<l n="26" num="5.3">Sur les <hi rend="ital">noirs taureaux sourds, blanches cavales !</hi></l>
						<l n="27" num="5.4">Votre écume à moi, <hi rend="ital">cavales d’Armor !</hi></l>
						<l n="28" num="5.5">Et vos crins au vent !… — Je ris comme un mort —</l>
					</lg>
					<lg n="6">
						<l n="29" num="6.1"><space quantity="8" unit="char"></space>Mon père était un vieux <hi rend="ital">saltin</hi><ref target="1" type="noteAnchor">1</ref>,</l>
						<l n="30" num="6.2"><space quantity="8" unit="char"></space>Ma mère une vieille <hi rend="ital">morgate</hi><ref target="2" type="noteAnchor">2</ref>…</l>
						<l n="31" num="6.3"><space quantity="8" unit="char"></space>Une nuit, sonna le tocsin :</l>
						<l n="32" num="6.4"><space quantity="8" unit="char"></space> — Vite à la côte : une frégate ! —</l>
						<l n="33" num="6.5"><space quantity="8" unit="char"></space>… Et dans la nuit, jusqu’au matin,</l>
						<l n="34" num="6.6"><space quantity="8" unit="char"></space>Ils ont tout rincé la frégate…</l>
						<l n="35" num="6.7"><space quantity="8" unit="char"></space> — Mais il dort mort le vieux <hi rend="ital">saltin</hi>,</l>
						<l n="36" num="6.8"><space quantity="8" unit="char"></space>Et morte la vieille <hi rend="ital">morgate</hi>…</l>
						<l n="37" num="6.9"><space quantity="8" unit="char"></space>Là-haut, dans le paradis saint</l>
						<l n="38" num="6.10"><space quantity="8" unit="char"></space>Ils n’ont plus besoin de frégate.</l>
					</lg>
					<closer>
						<note type="footnote" id="1"><hi rend="ital">Saltin</hi> : pilleur d’épaves.</note>
						<note type="footnote" id="2"><hi rend="ital">Morgate</hi> : pieuvre.</note>
						<dateline>
							<placeName>Ranc de Kerlouan</placeName> — Novembre.
						</dateline>
					</closer>
				</div></body></text></TEI>