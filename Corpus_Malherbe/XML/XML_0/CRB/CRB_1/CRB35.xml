<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES AMOURS JAUNES</title>
				<title type="medium">Édition électronique</title>
				<author key="CRB">
					<name>
						<forename>Tristan</forename>
						<surname>CORBIÈRE</surname>
					</name>
					<date from="1845" to="1875">1845-1875</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement métrique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4094 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">CRB_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Amours jaunes</title>
						<author>Tristan Corbière</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/ebooks/16883</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Amours jaunes</title>
								<author>Tristan Corbière</author>
								<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k70668p.r=tristan+corbi%C3%A8re.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE DU XIXe SIÈCLE ‒ GLADY FRÈRES, ÉDITEURS</publisher>
									<date when="1873">1873</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1873">1873</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LES AMOURS JAUNES</head><div type="poem" key="CRB35">
					<head type="main">LE POÈTE CONTUMACE</head>
					<lg n="1">
						<l n="1" num="1.1">Sur la côte d’ARMOR, — Un ancien vieux couvent,</l>
						<l n="2" num="1.2">Les vents se croyaient là dans un moulin-à-vent,</l>
						<l n="3" num="1.3"><space quantity="8" unit="char"></space>Et les ânes de la contrée,</l>
						<l n="4" num="1.4">Au lierre râpé, venaient râper leurs dents</l>
						<l n="5" num="1.5">Contre un mur si troué que, pour entrer dedans,</l>
						<l n="6" num="1.6"><space quantity="8" unit="char"></space>On n’aurait pu trouver l’entrée.</l>
					</lg>
					<lg n="2">
						<l n="7" num="2.1">— Seul — mais toujours debout avec un rare aplomb,</l>
						<l n="8" num="2.2">Crénelé comme la mâchoire d’une vieille,</l>
						<l n="9" num="2.3">Son toit à coups-de-poing sur le coin de l’oreille,</l>
						<l n="10" num="2.4">Aux corneilles bayant, se tenait le donjon,</l>
					</lg>
					<lg n="3">
						<l n="11" num="3.1">Fier toujours d’avoir eu, dans le temps, sa légende…</l>
						<l n="12" num="3.2">Ce n’était plus qu’un nid à gens de contrebande,</l>
						<l n="13" num="3.3">Vagabonds de nuit, amoureux buissonniers,</l>
						<l n="14" num="3.4">Chiens errants, vieux rats, fraudeurs et douaniers.</l>
					</lg>
					<lg n="4">
						<l n="15" num="4.1">— Aujourd’hui l’hôte était de la borgne tourelle,</l>
						<l n="16" num="4.2">Un Poète sauvage, avec un plomb dans l’aile,</l>
						<l n="17" num="4.3">Et tombé là parmi les antiques hiboux</l>
						<l n="18" num="4.4">Qui l’estimaient d’en haut. — Il respectait leurs trous, —</l>
						<l n="19" num="4.5">Lui, seul hibou payant, comme son <hi rend="ital">bail</hi> le porte :</l>
						<l n="20" num="4.6"><hi rend="ital">Pour vingt-cinq écus l’an, dont : remettre une porte</hi>. —</l>
					</lg>
					<lg n="5">
						<l n="21" num="5.1">Pour les gens du pays, il ne les voyait pas :</l>
						<l n="22" num="5.2">Seulement, en passant, eux regardaient d’en bas,</l>
						<l n="23" num="5.3"><space quantity="8" unit="char"></space>Se montrant du nez sa fenêtre ;</l>
						<l n="24" num="5.4">Le curé se doutait que c’était un lépreux ;</l>
						<l n="25" num="5.5">Et le maire disait : — Moi, qu’est-ce que j’y peux,</l>
						<l n="26" num="5.6"><space quantity="8" unit="char"></space>C’est plutôt un Anglais … un <hi rend="ital">Être</hi>.</l>
					</lg>
					<lg n="6">
						<l n="27" num="6.1">Les femmes avaient su — sans doute par les buses,</l>
						<l n="28" num="6.2">Qu’il <hi rend="ital">vivait en concubinage avec des Muses !</hi>…</l>
						<l n="29" num="6.3">Un hérétique enfin… Quelque <hi rend="ital">Parisien</hi></l>
						<l n="30" num="6.4">De Paris ou d’ailleurs. — Hélas ! on n’en sait rien. —</l>
						<l n="31" num="6.5">Il était invisible ; et, comme <hi rend="ital">ses Donzelles</hi></l>
						<l n="32" num="6.6"><hi rend="ital">Ne s’affichaient pas trop</hi>, on ne parla plus d’elles.</l>
					</lg>
					<lg n="7">
						<l n="33" num="7.1">— Lui, c’était simplement un long flâneur, sec, pâle ;</l>
						<l n="34" num="7.2">Un ermite-amateur, chassé par la rafale…</l>
						<l n="35" num="7.3">Il avait trop aimé les beaux pays malsains</l>
						<l n="36" num="7.4">Condamné des huissiers, comme des médecins,</l>
						<l n="37" num="7.5">Il avait posé là, seul et cherchant sa place</l>
						<l n="38" num="7.6">Pour mourir seul ou pour vivre par contumace…</l>
					</lg>
					<lg n="8">
						<l n="39" num="8.1"><space quantity="8" unit="char"></space>Faisant, d’un à-peu-près d’artiste,</l>
						<l n="40" num="8.2"><space quantity="8" unit="char"></space>Un philosophe d’à peu près,</l>
						<l n="41" num="8.3"><space quantity="8" unit="char"></space>Râleur de soleil ou de frais,</l>
						<l n="42" num="8.4"><space quantity="8" unit="char"></space>En dehors de l’humaine piste.</l>
					</lg>
					<lg n="9">
						<l n="43" num="9.1">Il lui restait encore un hamac, une vielle,</l>
						<l n="44" num="9.2">Un barbet qui dormait sous le nom de <hi rend="ital">Fidèle</hi> ;</l>
						<l n="45" num="9.3">Non moins fidèle était, triste et doux comme lui,</l>
						<l n="46" num="9.4">Un autre compagnon qui s’appelait l’Ennui.</l>
					</lg>
					<lg n="10">
						<l n="47" num="10.1">Se mourant en sommeil, il se vivait en rêve.</l>
						<l n="48" num="10.2">Son rêve était le flot qui montait sur la grève,</l>
						<l n="49" num="10.3"><space quantity="8" unit="char"></space>Le flot qui descendait ;</l>
						<l n="50" num="10.4">Quelquefois, vaguement, il se prenait attendre…</l>
						<l n="51" num="10.5">Attendre quoi … le flot monter — le flot descendre —</l>
						<l n="52" num="10.6"><space quantity="8" unit="char"></space>Ou l’Absente… Qui sait ?</l>
					</lg>
					<lg n="11">
						<l n="53" num="11.1">Le sait-il bien lui-même ?… Au vent de sa guérite,</l>
						<l n="54" num="11.2">A-t-il donc oublié comme les morts vont vite,</l>
						<l n="55" num="11.3">Lui, ce viveur vécu, revenant égaré,</l>
						<l n="56" num="11.4">Cherche-t-il son follet, à lui, mal enterré ?</l>
					</lg>
					<lg n="12">
						<l n="57" num="12.1">— Certe, Elle n’est pas loin, celle après qui tu brames,</l>
						<l n="58" num="12.2">O Cerf de Saint-Hubert ! Mais ton front est sans flammes…</l>
						<l n="59" num="12.3">N’apparais pas, mon vieux, triste et faux déterré…</l>
						<l n="60" num="12.4">Fais le mort si tu peux… Car Elle t’a pleuré !</l>
					</lg>
					<lg n="13">
						<l n="61" num="13.1">— Est-ce qu’il pouvait, Lui !… n’était-il pas poète…</l>
						<l n="62" num="13.2">Immortel comme un autre ?… Et dans sa pauvre tête</l>
						<l n="63" num="13.3">Déménagée, encor il sentait que les vers</l>
						<l n="64" num="13.4">Hexamètres faisaient les cent pas de travers.</l>
					</lg>
					<lg n="14">
						<l n="65" num="14.1">— Manque de savoir-vivre extrême — il survivait —</l>
						<l n="66" num="14.2">Et — manque de savoir-mourir — il écrivait :</l>
					</lg>
					<lg n="15">
						<l n="67" num="15.1">« C’est un être passé de cent lunes, ma Chère,</l>
						<l n="68" num="15.2">En ton cœur poétique, à l’état légendaire.</l>
						<l n="69" num="15.3">Je rime, donc je vis … ne crains pas, c’est <hi rend="ital">à blanc</hi>.</l>
						<l n="70" num="15.4">— Une coquille d’huître en rupture de banc ! —</l>
						<l n="71" num="15.5">Oui, j’ai beau me palper : c’est moi ! — Dernière faute —</l>
						<l n="72" num="15.6">En route pour les cieux — car ma niche est si haute ! —</l>
						<l n="73" num="15.7">Je me suis demandé, prêt à prendre l’essor :</l>
						<l n="74" num="15.8">Tête ou pile … — Et voilà — je me demande encor… »</l>
					</lg>
					<lg n="16">
						<l n="75" num="16.1">« C’est à toi que je fis mes adieux à la vie,</l>
						<l n="76" num="16.2">A toi qui me pleuras, jusqu’à me faire envie</l>
						<l n="77" num="16.3">De rester me pleurer avec toi. Maintenant</l>
						<l n="78" num="16.4">C’est joué, je ne suis qu’un gâteux revenant,</l>
						<l n="79" num="16.5">En os et … (j’allais dire en chair). — La chose est sûre</l>
						<l n="80" num="16.6">C’est bien moi, je suis là — mais comme une rature. »</l>
					</lg>
					<lg n="17">
						<l n="81" num="17.1">« Nous étions amateurs de curiosité :</l>
						<l n="82" num="17.2">Viens voir <hi rend="ital">le Bibelot</hi>. — Moi j’en suis dégoûté. —</l>
						<l n="83" num="17.3">Dans mes dégoûts surtout, j’ai des goûts élégants ;</l>
						<l n="84" num="17.4">Tu sais : j’avais lâché la Vie avec des gants ;</l>
						<l n="85" num="17.5">L’<hi rend="ital">Autre</hi> n’est pas même à prendre avec des pincettes …</l>
						<l n="86" num="17.6">Je cherche au mannequin de nouvelles toilettes. »</l>
					</lg>
					<lg n="18">
						<l n="87" num="18.1">« Reviens m’aider : Tes yeux dans ces yeux-là ! Ta lèvre</l>
						<l n="88" num="18.2">Sur cette lèvre !… Et, là, ne sens-tu pas ma fièvre</l>
						<l n="89" num="18.3">— Ma <hi rend="ital">fièvre de Toi ?</hi>… — Sous l’orbe est-il passé</l>
						<l n="90" num="18.4">L’arc-en-ciel au charbon par nos nuits laissé ?</l>
						<l n="91" num="18.5">Et cette étoile ?… — Oh ! va, ne cherche plus l’étoile</l>
						<l n="92" num="18.6"><space quantity="8" unit="char"></space>Que tu voulais voir à mon front ;</l>
						<l n="93" num="18.7"><space quantity="8" unit="char"></space>Une araignée a fait sa toile,</l>
						<l n="94" num="18.8"><space quantity="8" unit="char"></space>Au même endroit — dans le plafond. »</l>
					</lg>
					<lg n="19">
						<l n="95" num="19.1">« Je suis un étranger. — Cela vaut mieux peut-être…</l>
						<l n="96" num="19.2">— Eh bien ! non, viens encor un peu me reconnaître ;</l>
						<l n="97" num="19.3">Comme au bon saint Thomas, je veux te voir la foi,</l>
						<l n="98" num="19.4">Je veux te voir toucher la plaie et dire : — Toi ! » —</l>
					</lg>
					<lg n="20">
						<l n="99" num="20.1">« Viens encor me finir — c’est très gai : De ta chambre,</l>
						<l n="100" num="20.2">Tu verras mes moissons — Nous sommes en décembre —</l>
						<l n="101" num="20.3">Mes grands bois de sapin, les fleurs d’or des genêts,</l>
						<l n="102" num="20.4">Mes bruyères d’Armor … — en tas sur les chenets.</l>
						<l n="103" num="20.5">Viens te gorger d’air pur — Ici j’ai de la brise</l>
						<l n="104" num="20.6">Si franche !… que le bout de ma toiture en frise.</l>
						<l n="105" num="20.7">Le soleil est si doux … — qu’il gèle tout le temps.</l>
						<l n="106" num="20.8">Le printemps… — Le printemps n’est-ce pas tes vingt ans.</l>
						<l n="107" num="20.9">On n’attend plus que toi, vois : déjà l’hirondelle</l>
						<l n="108" num="20.10">Se pose … en fer rouillé, clouée à ma tourelle. —</l>
						<l n="109" num="20.11">Et bientôt nous pourrons cueillir le champignon…</l>
						<l n="110" num="20.12">Dans mon escalier que dore … un lumignon.</l>
						<l n="111" num="20.13">Dans le mur qui verdoie existe une pervenche</l>
						<l n="112" num="20.14">Sèche. — … Et puis nous irons à l’eau <hi rend="ital">faire</hi> la planche</l>
						<l n="113" num="20.15">— Planches d’épave au sec — comme moi — sur ces plages.</l>
						<l n="114" num="20.16">La Mer roucoule sa <hi rend="ital">Berceuse pour naufrages</hi> ;</l>
						<l n="115" num="20.17">Barcarolle du soir … pour les canards sauvages. »</l>
					</lg>
					<lg n="21">
						<l n="116" num="21.1">« En <hi rend="ital">Paul et Virginie</hi>, et virginaux — veux-tu —</l>
						<l n="117" num="21.2">Nous nous mettrons au vert du paradis perdu…</l>
						<l n="118" num="21.3">Ou <hi rend="ital">Robinson avec Vendredi</hi> — c’est facile —</l>
						<l n="119" num="21.4">La pluie a déjà fait, de mon royaume, une île. »</l>
					</lg>
					<lg n="22">
						<l n="120" num="22.1">« Si pourtant, près de moi, tu crains la solitude,</l>
						<l n="121" num="22.2">Nous avons des amis, sans fard — Un braconnier ;</l>
						<l n="122" num="22.3">Sans compter un caban bleu qui, par habitude,</l>
						<l n="123" num="22.4">Fait toujours les cent-pas et contient un douanier…</l>
						<l n="124" num="22.5">Plus de clercs d’huissier ! J’ai le clair de la lune,</l>
						<l n="125" num="22.6">Et des amis pierrots amoureux sans fortune. »</l>
					</lg>
					<lg n="23">
						<l n="126" num="23.1">— « Et nos nuits !… <hi rend="ital">Belles nuits pour l’orgie à la tour !</hi>…</l>
						<l n="127" num="23.2">Nuits à la Roméo ! — Jamais il ne fait jour. —</l>
						<l n="128" num="23.3">La Nature au réveil — réveil de déchaînée —</l>
						<l n="129" num="23.4">Secouant son drap blanc … éteint ma cheminée.</l>
						<l n="130" num="23.5">Voici mes rossignols … rossignols d’ouragans —</l>
						<l n="131" num="23.6">Gais comme des poinçons — sanglots de chats-huans !</l>
						<l n="132" num="23.7">Ma girouette dérouille en haut sa tyrolienne</l>
						<l n="133" num="23.8">Et l’on entend gémir ma porte éolienne,</l>
						<l n="134" num="23.9">Comme chez saint Antoine en sa tentation…</l>
						<l n="135" num="23.10">Oh viens ! joli Suppôt de la séduction ! »</l>
					</lg>
					<lg n="24">
						<l n="136" num="24.1">— « Hop ! les rats du grenier dansent des farandoles !</l>
						<l n="137" num="24.2">Les ardoises du toit roulent en castagnoles !</l>
						<l part="I" n="138" num="24.3">Les Folles-du-logis… </l>
						<l part="F" n="138" num="24.3">Non, je n’ai plus de Folles ! »</l>
					</lg>
					<lg n="25">
						<l n="139" num="25.1">… « Comme je revendrais ma dépouille à Satan</l>
						<l n="140" num="25.2">S’il me tentait avec un petit Revenant…</l>
						<l n="141" num="25.3">— Toi — Je te vois partout, mais comme un voyant blême,</l>
						<l n="142" num="25.4">Je t’adore… Et c’est pauvre : adorer ce qu’on aime !</l>
						<l n="143" num="25.5">Apparais, un poignard dans le cœur ! — Ce sera,</l>
						<l n="144" num="25.6">Tu sais bien, comme dans <hi rend="ital">Inès de La Sierra</hi>…</l>
						<l part="I" n="145" num="25.7">— On frappe … oh ! c’est quelqu’un… </l>
						<l part="F" n="145" num="25.7">Hélas ! oui, c’est un rat. »</l>
					</lg>
					<lg n="26">
						<l n="146" num="26.1">— « Je rêvasse … et toujours c’est <hi rend="ital">Toi</hi>. Sur toute chose,</l>
						<l n="147" num="26.2">Comme un esprit follet, ton souvenir se pose :</l>
						<l n="148" num="26.3">Ma solitude — <hi rend="ital">Toi !</hi> — Mes hiboux à l’œil d’or :</l>
						<l n="149" num="26.4">— <hi rend="ital">Toi !</hi> — Ma girouette folle : Oh <hi rend="ital">Toi !</hi>… — Que sais-je encor,</l>
						<l n="150" num="26.5">— <hi rend="ital">Toi</hi> : mes volets ouvrant les bras dans la tempête…</l>
						<l n="151" num="26.6">Une lointaine voix : c’est Ta chanson ! — c’est fête !…</l>
						<l n="152" num="26.7">Les rafales fouaillant Ton nom perdu — c’est bête —</l>
						<l n="153" num="26.8">C’est bête, mais c’est <hi rend="ital">Toi</hi> ! Mon cœur au grand ouvert</l>
						<l n="154" num="26.9"><space quantity="8" unit="char"></space>Comme mes volets en pantenne,</l>
						<l n="155" num="26.10"><space quantity="8" unit="char"></space>Bat, tout affolé sous l’haleine</l>
						<l n="156" num="26.11"><space quantity="8" unit="char"></space>Des plus bizarres courants d’air. »</l>
					</lg>
					<lg n="27">
						<l n="157" num="27.1">« Tiens … une ombre portée, un instant, est venue</l>
						<l n="158" num="27.2">Dessiner ton profil sur la muraille nue,</l>
						<l n="159" num="27.3">Et j’ai tourné la tête… — Espoir ou souvenir —</l>
						<l n="160" num="27.4"><hi rend="ital"> Ma Sœur Anne, à la tour, voyez-vous pas venir ?</hi> »…</l>
					</lg>
					<lg n="28">
						<l n="161" num="28.1">— « Rien ! — je vois … je vois, dans ma froide chambrette,</l>
						<l n="162" num="28.2">Mon lit capitonné de <hi rend="ital">satin de brouette</hi> ;</l>
						<l n="163" num="28.3">Et mon chien qui dort dessus — Pauvre animal —</l>
						<l n="164" num="28.4">… Et je ris … parce que ça me fait un peu mal. »</l>
					</lg>
					<lg n="29">
						<l n="165" num="29.1">« J’ai pris, pour t’appeler, ma vielle et ma lyre.</l>
						<l n="166" num="29.2">Mon cœur fait de l’esprit — le sot — pour se leurrer…</l>
						<l n="167" num="29.3">Viens pleurer, si mes vers ont pu te faire rire ;</l>
						<l n="168" num="29.4"><space quantity="8" unit="char"></space>Viens rire, s’ils t’ont fait pleurer… »</l>
					</lg>
					<lg n="30">
						<l n="169" num="30.1">« Ce sera drôle… Viens jouer à la misère,</l>
						<l n="170" num="30.2">D’après nature : — <hi rend="ital">Un cœur avec une chaumière</hi>. —</l>
						<l n="171" num="30.3">… Il pleut dans mon foyer, il pleut dans mon cœur feu.</l>
						<l n="172" num="30.4">Viens ! Ma chandelle est morte et je n’ai plus de feu… »</l>
					</lg>
					<ab type="star">*</ab>
					<lg n="31">
						<l n="173" num="31.1">Sa lampe se mourait. Il ouvrit la fenêtre.</l>
						<l n="174" num="31.2">Le soleil se levait. Il regarda sa lettre,</l>
						<l n="175" num="31.3">Rit et la déchira… Les petits morceaux blancs,</l>
						<l n="176" num="31.4">Dans la brume, semblaient un vol de goélands.</l>
					</lg>
					<closer>
						<dateline>
							<placeName>Penmarcʼh</placeName> — jour de Noël.
						</dateline>
					</closer>
				</div></body></text></TEI>