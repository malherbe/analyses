<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Coffret de Crusoé</title>
				<title type="medium">Édition électronique</title>
				<author key="DAN">
					<name>
						<forename>Louis</forename>
						<surname>DANTIN</surname>
					</name>
					<date from="1865" to="1945">1865-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2468 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2016</date>
				<idno type="local">DAN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Coffret de Crusoé</title>
						<author>Louis Dantin</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/louisdantinlecofretdecrusoee.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Le Coffret de Crusoé</title>
						<author>Louis Dantin</author>
						<imprint>
							<publisher>ÉDITIONS ALBERT LÉVESQUE</publisher>
							<date when="1932">1932</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1932">1932</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">VI</head><head type="main_part">CHANSON INTIME</head><div type="poem" key="DAN25">
						<head type="main">Sympathie astrale</head>
						<lg n="1">
							<l n="1" num="1.1">Comme des astres seuls dans les éthers sans fin,</l>
							<l n="2" num="1.2">Suivant l’orbe cruel dont la loi les captive,</l>
							<l n="3" num="1.3">Rose, nos coeurs erraient par la route pensive</l>
							<l n="4" num="1.4">Où vont les coeurs amis qui se cherchent en vain.</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1">Comme des astres seuls que leur flamme consume,</l>
							<l n="6" num="2.2">Et qui, dans l’infini, mélancoliquement</l>
							<l n="7" num="2.3">Dispersent les rayons où saigne leur tourment</l>
							<l n="8" num="2.4">Sans que pour leur sourire aucun reflet s’allume ;</l>
						</lg>
						<lg n="3">
							<l n="9" num="3.1">Rose, nos coeurs erraient, de rêves lacérés,</l>
							<l n="10" num="3.2">Et parmi la gaieté de la tourbe qui passe,</l>
							<l n="11" num="3.3">Nous mourions de marcher isolés dans l’espace</l>
							<l n="12" num="3.4">Et du regret latent de nous être ignorés.</l>
						</lg>
						<lg n="4">
							<l n="13" num="4.1">Mais un jour, j’ai senti frémir au loin ta plainte ;</l>
							<l n="14" num="4.2">Ta lueur a percé, rapide, mon exil,</l>
							<l n="15" num="4.3">Et dans mon être, ému d’un effluve subtil,</l>
							<l n="16" num="4.4">Chaque atome a vibré sous l’attraction sainte.</l>
						</lg>
						<lg n="5">
							<l n="17" num="5.1">Nous nous sommes aimés sans nous connaître encor,</l>
							<l n="18" num="5.2">Et sans que le baiser eût fiancé nos lèvres</l>
							<l n="19" num="5.3">Nos âmes se donnaient en d’électriques fièvres</l>
							<l n="20" num="5.4">Et nos jeunes désirs chantaient des hymnes d’or.</l>
						</lg>
						<lg n="6">
							<l n="21" num="6.1">Trop loin pour que nos yeux fondissent leurs prunelles,</l>
							<l n="22" num="6.2">Nous mirions nos pensers comme des cristaux purs,</l>
							<l n="23" num="6.3">Extasiés de voir dans nos rêves obscurs</l>
							<l n="24" num="6.4">S’allumer le flambeau des amours éternelles.</l>
						</lg>
						<ab type="dot">… ... … ... … ... … ... … ... … ... … ... … ... … ... …</ab>
						<lg n="7">
							<l n="25" num="7.1">Mais déjà, sans pitié, dans notre âpre chemin</l>
							<l n="26" num="7.2">L’inflexible Destin nous entraînait plus vite :</l>
							<l n="27" num="7.3">Et nous suivions chacun notre fatal orbite,</l>
							<l n="28" num="7.4">Comme des astres seuls dans les éthers sans fin…</l>
						</lg>
					</div></body></text></TEI>