<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Coffret de Crusoé</title>
				<title type="medium">Édition électronique</title>
				<author key="DAN">
					<name>
						<forename>Louis</forename>
						<surname>DANTIN</surname>
					</name>
					<date from="1865" to="1945">1865-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2468 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2016</date>
				<idno type="local">DAN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Coffret de Crusoé</title>
						<author>Louis Dantin</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/louisdantinlecofretdecrusoee.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Le Coffret de Crusoé</title>
						<author>Louis Dantin</author>
						<imprint>
							<publisher>ÉDITIONS ALBERT LÉVESQUE</publisher>
							<date when="1932">1932</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1932">1932</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">VI</head><head type="main_part">CHANSON INTIME</head><div type="poem" key="DAN24">
						<head type="main">Noël intime</head>
						<lg n="1">
							<l n="1" num="1.1">Oh ! qu’ils furent heureux, les pâtres de Judée,</l>
							<l n="2" num="1.2">Éveillés au buccin de l’Ange triomphant,</l>
							<l n="3" num="1.3">Et la troupe des Rois par l’Étoile guidée</l>
							<l n="4" num="1.4">Vers le chaume mystique où s’abritait l’Enfant !</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1">Tous ceux qui, dans la paix de cette nuit agreste,</l>
							<l n="6" num="2.2">Trouvèrent le Promis, le Christ enfin venu,</l>
							<l n="7" num="2.3">Et ceux même, ignorants de l’Envoyé céleste,</l>
							<l n="8" num="2.4">Qui L’avaient repoussé, mais du moins L’avaient vu !</l>
						</lg>
						<lg n="3">
							<l n="9" num="3.1">La Mère, s’enivrant d’extase virginale,</l>
							<l n="10" num="3.2">Joseph, pour qui tout le mystère enfin a lui,</l>
							<l n="11" num="3.3">Et l’étable, et la crèche, et la bise hivernale</l>
							<l n="12" num="3.4">Par les vieux ais disjoints se glissant jusqu’à Lui !</l>
						</lg>
						<lg n="4">
							<l n="13" num="4.1">Tout ce qui Le toucha dans sa chair ou son âme,</l>
							<l n="14" num="4.2">Tout ce que son rayon commença d’éblouir,</l>
							<l n="15" num="4.3">Princes savants, bergers pieux, Hérode infâme,</l>
							<l n="16" num="4.4">Tout ce qui crut en Lui, fût-ce pour Le haïr !</l>
						</lg>
						<lg n="5">
							<l n="17" num="5.1">Oh ! qu’ils furent heureux ! Moi, dans l’ombre muette,</l>
							<l n="18" num="5.2">Je m’asseois, pasteur morne et blême de soucis,</l>
							<l n="19" num="5.3">Et jamais un Archange à ma veille inquiète</l>
							<l n="20" num="5.4">Ne vient jeter le Gloria in excelsis.</l>
						</lg>
						<lg n="6">
							<l n="21" num="6.1">Je scrute le reflet de toutes les étoiles,</l>
							<l n="22" num="6.2">Mage pensif, avec un désir surhumain,</l>
							<l n="23" num="6.3">Mais leur front radieux n’a pour moi que des voiles</l>
							<l n="24" num="6.4">Et pas une du doigt ne me montre un chemin.</l>
						</lg>
						<lg n="7">
							<l n="25" num="7.1">Et mon âme est la Vierge attendant la promesse,</l>
							<l n="26" num="7.2">Mais que ne touche point le souffle de l’Esprit,</l>
							<l n="27" num="7.3">Ou le vieillard en pleurs qu’un sombre doute oppresse</l>
							<l n="28" num="7.4">Et qui n’a jamais su d’où venait Jésus-Christ.</l>
						</lg>
						<lg n="8">
							<l n="29" num="8.1">Je suis l’étable offrant en vain son sol aride</l>
							<l n="30" num="8.2">Au Roi toujours lointain et toujours attendu ;</l>
							<l n="31" num="8.3">Et dans mon coeur voici la crèche, berceau vide,</l>
							<l n="32" num="8.4">Où le vent froid gémit comme un espoir perdu.</l>
						</lg>
					</div></body></text></TEI>