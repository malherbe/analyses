<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Coffret de Crusoé</title>
				<title type="medium">Édition électronique</title>
				<author key="DAN">
					<name>
						<forename>Louis</forename>
						<surname>DANTIN</surname>
					</name>
					<date from="1865" to="1945">1865-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2468 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2016</date>
				<idno type="local">DAN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Coffret de Crusoé</title>
						<author>Louis Dantin</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/louisdantinlecofretdecrusoee.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Le Coffret de Crusoé</title>
						<author>Louis Dantin</author>
						<imprint>
							<publisher>ÉDITIONS ALBERT LÉVESQUE</publisher>
							<date when="1932">1932</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1932">1932</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">VI</head><head type="main_part">CHANSON INTIME</head><div type="poem" key="DAN31">
						<head type="main">Mon coeur</head>
						<lg n="1">
							<l n="1" num="1.1">Ah ! mon coeur est un gouffe insondable et béant</l>
							<l n="2" num="1.2">Où le Désir écume et bout comme une braise,</l>
							<l n="3" num="1.3">Et, pauvres oiseaux fous qu’attirait le néant,</l>
							<l n="4" num="1.4">Tous mes amours sont là tombés dans la fournaise.</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1">Amours naïfs des jours de mes robes d’enfant,</l>
							<l n="6" num="2.2">Amours sacrés, rayons de ma jeunesse austère,</l>
							<l n="7" num="2.3">Amours cruels, qui brisiez l’âme en triomphant,</l>
							<l n="8" num="2.4">Amours maudits, courbés de honte, et qu’il faut taire.</l>
						</lg>
						<lg n="3">
							<l n="9" num="3.1">Les amours nés au choc d’un regard fugitif,</l>
							<l n="10" num="3.2">Au charme d’un sourire, au tulle d’un corsage ;</l>
							<l n="11" num="3.3">Ceux qu’a lancés de loin au coeur inattentif</l>
							<l n="12" num="3.4">L’arc rose d’une lèvre où l’aveu se présage.</l>
						</lg>
						<lg n="4">
							<l n="13" num="4.1">Les amours patients et sûrs, calmes et doux,</l>
							<l n="14" num="4.2">Faisant à l’âme comme un nid sur une cime,</l>
							<l n="15" num="4.3">Et les amours trahis qu’on traîne à deux genoux :</l>
							<l n="16" num="4.4">Tous mes amours sont là dans mon coeur, cet abîme.</l>
						</lg>
						<lg n="5">
							<l n="17" num="5.1">Aucun n’a déserté l’abri vertigineux,</l>
							<l n="18" num="5.2">Nul n’a péri, ployant au souffle qui l’embrase ;</l>
							<l n="19" num="5.3">Tous sont vivants encore et, plaintif ou joyeux,</l>
							<l n="20" num="5.4">Leur choeur chante toujours les larmes ou l’extase.</l>
						</lg>
						<lg n="6">
							<l n="21" num="6.1">Tous mes amis d’hier et des passés lointains,</l>
							<l n="22" num="6.2">Ceux qu’abrite mon toit, les autres dont l’absence</l>
							<l n="23" num="6.3">A fait l’ombre plus vague et les traits incertains,</l>
							<l n="24" num="6.4">Ceux que m’a pris la mort même, ou l’indifférence ;</l>
						</lg>
						<lg n="7">
							<l n="25" num="7.1">Mon coeur les gardes tous, trésor pieux et cher,</l>
							<l n="26" num="7.2">Sources de son ivresse et de ses agonies,</l>
							<l n="27" num="7.3">Et les frissons anciens de l’âme ou de la chair,</l>
							<l n="28" num="7.4">Il les revit dans leurs caresses rajeunies.</l>
						</lg>
						<lg n="8">
							<l n="29" num="8.1">Qu’importe qu’il s’élance à de nouveaux désirs</l>
							<l n="30" num="8.2">En une soif d’aimer tyrannique et suprême ?</l>
							<l n="31" num="8.3">Il reste empreint du sceau des premiers souvenirs,</l>
							<l n="32" num="8.4">Et tout ce qu’il chérit un jour, toujours il l’aime.</l>
						</lg>
						<lg n="9">
							<l n="33" num="9.1">Il est inassouvi parce qu’il est profond,</l>
							<l n="34" num="9.2">Il veut tout consumer parce qu’il est intense,</l>
							<l n="35" num="9.3">Mais ce qui dans sa flamme invisible se fond</l>
							<l n="36" num="9.4">Dure plus beau, paré d’une éternelle essence.</l>
						</lg>
						<lg n="10">
							<l n="37" num="10.1">Laissez donc nos destins intimes se lier,</l>
							<l n="38" num="10.2">Soeur nouvelle, chère âme, hier encore inconnue.</l>
							<l n="39" num="10.3">Mon coeur vous attendait ; l’âbime hospitalier</l>
							<l n="40" num="10.4">Se fait riant et doux pour votre bienvenue.</l>
						</lg>
						<lg n="11">
							<l n="41" num="11.1">Prenez place, ma reine, au cercle radieux ;</l>
							<l n="42" num="11.2">Des amantes d’antan ne soyez point jalouse :</l>
							<l n="43" num="11.3">Comme si l’univers ne portait que nous deux</l>
							<l n="44" num="11.4">Vous m’aurez tout entier, ô ma millième épouse !</l>
						</lg>
						<lg n="12">
							<l n="45" num="12.1">Que votre âme se ferme aux doutes obsesseurs ;</l>
							<l n="46" num="12.2">Qu’elle tende plutôt des lèvres fraternelles</l>
							<l n="47" num="12.3">À celles qu’un destin mystique fit vos soeurs ;</l>
							<l n="48" num="12.4">Je vous aimerai mieux en vous aimant pour elles.</l>
						</lg>
						<lg n="13">
							<l n="49" num="13.1">Notre tendresse ira plus pure se créant</l>
							<l n="50" num="13.2">Pour avoir du Soupçon ignoré le fantôme,</l>
							<l n="51" num="13.3">Et nos deux coeurs grandis feront un coeur géant</l>
							<l n="52" num="13.4">Où la terre et les cieux sembleront un atome…</l>
						</lg>
						<lg n="14">
							<l n="53" num="14.1">Ah ! mon coeur est un gouffre insondable et béant !</l>
						</lg>
					</div></body></text></TEI>