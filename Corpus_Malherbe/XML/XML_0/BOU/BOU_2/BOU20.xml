<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES. FESTONS ET ASTRAGALES</title>
				<title type="sub"></title>
				<title type="medium">Une édition électronique</title>
				<author key="BOU">
					<name>
						<forename>Louis</forename>
						<surname>BOUILHET</surname>
					</name>
					<date from="1822" to="1869">1822-1869</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2963 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BOU_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>POÉSIES. FESTONS ET ASTRAGALES</title>
						<author>Louis Bouilhet</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k9737201m</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES. FESTONS ET ASTRAGALES</title>
								<author>Louis Bouilhet</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE NOUVELLE</publisher>
									<date when="1859">1859</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1859">1859</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-09-20" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.4.xsd)</change>
				<change when="2021-09-20" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BOU20">
				<lg n="1">
					<l n="1" num="1.1">Quand vous m’avez quitté, boudeuse et mutinée</l>
					<l n="2" num="1.2">Secouant mes baisers, comme un arbre ses fleurs,</l>
					<l n="3" num="1.3">Je restai seul, debout, près de la cheminée,</l>
					<l n="4" num="1.4">Me forçant au sourire, et me sentant des pleurs ;</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">C’était le premier doute et le premier nuage</l>
					<l n="6" num="2.2">Dans ce beau ciel d’amour qu’un souffle peut ternir,</l>
					<l n="7" num="2.3">Et me croyant bien fort, et me posant en sage,</l>
					<l n="8" num="2.4">J’avais raillé vos saints que j’aurais dû bénir ;</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">A vos preuves de Dieu, mon oreille était sourde,</l>
					<l n="10" num="3.2">Je heurtais votre foi d’un sarcasme moqueur…</l>
					<l n="11" num="3.3">L homme est lâche et brutal, l’homme a la main trop lourde</l>
					<l n="12" num="3.4">Pour toucher à votre aile, ô croyances du cœur !</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">Pardon, j’en suis puni plus qu’on ne saurait dire !</l>
					<l n="14" num="4.2">J’ai vu jaillir l’éclair de vos grands yeux si doux ;</l>
					<l n="15" num="4.3">Pour garder ma raison, j’ai perdu maint sourire,</l>
					<l n="16" num="4.4">Ah ! montrez-moi l’autel, que j’y tombe à genoux !</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1">Votre loi ? j’y consens ! Votre Dieu ? je l’adore !</l>
					<l n="18" num="5.2">A vos saints préférés j’offre mes encensoirs,</l>
					<l n="19" num="5.3">Même on vous passera, pour deux baisers encore,</l>
					<l n="20" num="5.4">Vos dominicains blancs et vos jésuites noirs !</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1">Dans votre amour profond, je vais creuser ma grotte,</l>
					<l n="22" num="6.2">Et, loin dès bruits du monde, entre vos bras de lait,</l>
					<l n="23" num="6.3">L’ermite, chaque jour, de sa lèvre dévote,</l>
					<l n="24" num="6.4">Sur l’émail de vos dents dira son chapelet !</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1">J’irais, prêtre docile à toute fantaisie,</l>
					<l n="26" num="7.2">Avec le gui du chêne ou la tiare d’or,</l>
					<l n="27" num="7.3">Du Teutatès de Gaule au Bhagavat d’Asie,</l>
					<l n="28" num="7.4">Des cabires persans aux dieux glacés du nord !</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1">Que s’il vous fait plaisir d’être mahométane,</l>
					<l n="30" num="8.2">Allah !… de Mahomet j’espère les sept deux !</l>
					<l n="31" num="8.3">Si vous aimez Brahma, je serai le brahmane !</l>
					<l n="32" num="8.4">Mon culte est ta croyance, et mes dieux sont tes dieux !</l>
				</lg>
			</div></body></text></TEI>