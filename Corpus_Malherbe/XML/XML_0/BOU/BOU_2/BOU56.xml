<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES. FESTONS ET ASTRAGALES</title>
				<title type="sub"></title>
				<title type="medium">Une édition électronique</title>
				<author key="BOU">
					<name>
						<forename>Louis</forename>
						<surname>BOUILHET</surname>
					</name>
					<date from="1822" to="1869">1822-1869</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2963 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BOU_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>POÉSIES. FESTONS ET ASTRAGALES</title>
						<author>Louis Bouilhet</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k9737201m</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES. FESTONS ET ASTRAGALES</title>
								<author>Louis Bouilhet</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE NOUVELLE</publisher>
									<date when="1859">1859</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1859">1859</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-09-20" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.4.xsd)</change>
				<change when="2021-09-20" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BOU56">
				<head type="main">LE LABOUREUR</head>
				<opener>
					<salute>A MON AMI EUGÈNE CRÉPET</salute>
				</opener>
				<lg n="1">
					<l n="1" num="1.1">O laboureur de l’âme, ô semeur éternel,</l>
					<l n="2" num="1.2">Poëte, avant le jour, loin du toit paternel,</l>
					<l n="3" num="1.3"><space unit="char" quantity="8"></space>Sans écouter le chien qui gronde,</l>
					<l n="4" num="1.4">Pars avec ta charrue et ton rude aiguillon :</l>
					<l n="5" num="1.5">Tu sais que le temps presse, et qu’il faut au sillon</l>
					<l n="6" num="1.6"><space unit="char" quantity="8"></space>Jeter tout l’avenir d’un monde.</l>
				</lg>
				<lg n="2">
					<l n="7" num="2.1">Il part ; la plaine immense, au lever du soleil,</l>
					<l n="8" num="2.2">N’a pas même un oiseau qui chante le réveil,</l>
					<l n="9" num="2.3"><space unit="char" quantity="8"></space>Pas mème un arbre qui frissonne.</l>
					<l n="10" num="2.4">C’est un terrain maudit, dans le vaste univers,</l>
					<l n="11" num="2.5">Et, sur les durs cailloux dont les champs sont couverts,</l>
					<l n="12" num="2.6"><space unit="char" quantity="8"></space>On entend le soc dur qui sonne.</l>
				</lg>
				<lg n="3">
					<l n="13" num="3.1">L’air est en feu : midi, sur l’ardent travailleur,</l>
					<l n="14" num="3.2">Comme un manteau de plomb, fait tomber sa chaleur :</l>
					<l n="15" num="3.3"><space unit="char" quantity="8"></space>Mais qu’importe aux tâches divines !</l>
					<l n="16" num="3.4">Il marche dans l’espoir, dans la foi, dans l’azur,</l>
					<l n="17" num="3.5">Et la sainte sueur qui coule à son front pur</l>
					<l n="18" num="3.6"><space unit="char" quantity="8"></space>Semble un bandeau de perles fines.</l>
				</lg>
				<lg n="4">
					<l n="19" num="4.1">Il voit, il voit déjà, sur le sol âpre encor,</l>
					<l n="20" num="4.2">Frémir les bois touffus et rouler les blés d’or,</l>
					<l n="21" num="4.3"><space unit="char" quantity="8"></space>Tout tachetés de fleurs vermeilles ;</l>
					<l n="22" num="4.4">Il ne s’aperçoit pas, le rêveur ingénu,</l>
					<l n="23" num="4.5">Que mille taons jaloux, pour piquer son sein nu,</l>
					<l n="24" num="4.6"><space unit="char" quantity="8"></space>Vont bourdonnant à ses oreilles !</l>
				</lg>
				<lg n="5">
					<l n="25" num="5.1">Puis, quand au foyer sombre il retourne, le soir,</l>
					<l n="26" num="5.2">Tous les petits enfants se pressent pour le voir,</l>
					<l n="27" num="5.3"><space unit="char" quantity="8"></space>Au seuil des fermes souriantes ;</l>
					<l n="28" num="5.4">Car, pareils aux grands bœufs qui rentrent à pas lourds,</l>
					<l n="29" num="5.5">Ses vers au large flanc font tinter, dans les cours,</l>
					<l n="30" num="5.6"><space unit="char" quantity="8"></space>Leurs colliers de rimes bruyantes.</l>
				</lg>
			</div></body></text></TEI>