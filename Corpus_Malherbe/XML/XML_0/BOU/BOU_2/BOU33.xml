<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES. FESTONS ET ASTRAGALES</title>
				<title type="sub"></title>
				<title type="medium">Une édition électronique</title>
				<author key="BOU">
					<name>
						<forename>Louis</forename>
						<surname>BOUILHET</surname>
					</name>
					<date from="1822" to="1869">1822-1869</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2963 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BOU_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>POÉSIES. FESTONS ET ASTRAGALES</title>
						<author>Louis Bouilhet</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k9737201m</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES. FESTONS ET ASTRAGALES</title>
								<author>Louis Bouilhet</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE NOUVELLE</publisher>
									<date when="1859">1859</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1859">1859</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-09-20" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.4.xsd)</change>
				<change when="2021-09-20" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BOU33">
				<head type="main">LE DANSEUR BATHYLLE</head>
				<lg n="1">
					<l n="1" num="1.1">La belle Métella, femme du vieux préteur,</l>
					<l n="2" num="1.2">Est pâle maintenant, et. porte dans son cœur</l>
					<l n="3" num="1.3"><space unit="char" quantity="8"></space>Un mal secret qui la déchire ;</l>
					<l n="4" num="1.4">Par le bois d’orangers qui borde sa villa,</l>
					<l n="5" num="1.5">Elle marche au hasard, la belle Métella,</l>
					<l n="6" num="1.6"><space unit="char" quantity="8"></space>Comme une bacchante en délire.</l>
				</lg>
				<lg n="2">
					<l n="7" num="2.1">Pour sonder jusqu’au fond l’avenir incertain,</l>
					<l n="8" num="2.2">Vingt fois l’urne d’albâtre où roule le destin</l>
					<l n="9" num="2.3"><space unit="char" quantity="8"></space>Sous ses doigts tremblants s’est vidée ;</l>
					<l n="10" num="2.4">Et vingt fois Métella, chez les magiciens,</l>
					<l n="11" num="2.5">A mêlé, dans la nuit, les sorts campaniens</l>
					<l n="12" num="2.6"><space unit="char" quantity="8"></space>Aux enchantements de Chaldée !</l>
				</lg>
				<lg n="3">
					<l n="13" num="3.1">Elle aime, et ce n’est pas le chevalier romain,</l>
					<l n="14" num="3.2">Bien qu’il soit jeune et fier, et qu’il presse,’en chemin,</l>
					<l n="15" num="3.3"><space unit="char" quantity="8"></space>Une cavale au frein sonore,</l>
					<l n="16" num="3.4">Qu’il ait sa place au cirque, auprès des sénateurs,</l>
					<l n="17" num="3.5">Que sa bague étincelle, et qu’au jour des honneurs,</l>
					<l n="18" num="3.6"><space unit="char" quantity="8"></space>D’olivier son front se décore !</l>
				</lg>
				<lg n="4">
					<l n="19" num="4.1">Ce n’est pas le consul, au long manteau rayé</l>
					<l n="20" num="4.2">Si beau qu’à son aspect, du peuple émerveillé</l>
					<l n="21" num="4.3"><space unit="char" quantity="8"></space>Tombe le murmure frivole,</l>
					<l n="22" num="4.4">Alors que précédé du licteur éclatant,</l>
					<l n="23" num="4.5">Avec sa robe blanche, il balaye, en montant,</l>
					<l n="24" num="4.6"><space unit="char" quantity="8"></space>Les blancs degrés du capitole !</l>
				</lg>
				<lg n="5">
					<l n="25" num="5.1">Ce n’est pas le tribun, l’homme au pouvoir hautain,</l>
					<l n="26" num="5.2">Qui d’un mot de sa bouche arrête le destin,</l>
					<l n="27" num="5.3"><space unit="char" quantity="8"></space>Ni l’édile aux dons magnifiques,</l>
					<l n="28" num="5.4">Ni le riche patron, de qui mille clients</l>
					<l n="29" num="5.5">Autour de la sportule humbles et suppliants</l>
					<l n="30" num="5.6"><space unit="char" quantity="8"></space>Sans cesse assiègent les portiques.</l>
				</lg>
				<lg n="6">
					<l n="31" num="6.1">Si Métella soupire et n’a plus de sommeil,</l>
					<l n="32" num="6.2">Ce n’est point le soldat bruni par le soleil</l>
					<l n="33" num="6.3"><space unit="char" quantity="8"></space>Qui trouble sa nuit inquiète,</l>
					<l n="34" num="6.4">Ni le poëte grec aux vers ingénieux,</l>
					<l n="35" num="6.5">Ni l’esclave gaulois, prince par ses aïeux,</l>
					<l n="36" num="6.6"><space unit="char" quantity="8"></space>Qui porte une urne sur sa tête.</l>
				</lg>
				<lg n="7">
					<l n="37" num="7.1">L’image qui bondit sous ses yeux enflammés,</l>
					<l n="38" num="7.2">C’est le danseur Bathylle, aux cheveux parfumés,</l>
					<l n="39" num="7.3"><space unit="char" quantity="8"></space>Bathylle aux poses languissantes,</l>
					<l n="40" num="7.4">Bathylle qui s’envole, et qui glisse, et qui fuit,</l>
					<l n="41" num="7.5">Et fait battre le cœur des matrones, au bruit</l>
					<l n="42" num="7.6"><space unit="char" quantity="8"></space>De ses cymbales frémissantes.</l>
				</lg>
				<lg n="8">
					<l n="43" num="8.1">Bathylle qu’aux Romains la Grèce, un jour, céda,</l>
					<l n="44" num="8.2">Si gracieux, alors qu’il danse la Léda,</l>
					<l n="45" num="8.3"><space unit="char" quantity="8"></space>Sous une tunique de femme !</l>
					<l n="46" num="8.4">Ou quand son corps mobile, en cercle se tordant,</l>
					<l n="47" num="8.5">Tourne comme une roue, et dans son vol ardent</l>
					<l n="48" num="8.6"><space unit="char" quantity="8"></space>De tout un peuple emporte l’âme !</l>
				</lg>
				<lg n="9">
					<l n="49" num="9.1">Mais Bathylle est cruel, et ne se donne pas,</l>
					<l n="50" num="9.2">Il veut un sang illustre et de nobles appas</l>
					<l n="51" num="9.3"><space unit="char" quantity="8"></space>Pour une faveur qu’il accorde ;</l>
					<l n="52" num="9.4">Et plus d’un sénateur aux antiques aïeux</l>
					<l n="53" num="9.5">Triomphant d’être père, élève sous ses yeux,</l>
					<l n="54" num="9.6"><space unit="char" quantity="8"></space>Quelque petit danseur de corde.</l>
				</lg>
			</div></body></text></TEI>