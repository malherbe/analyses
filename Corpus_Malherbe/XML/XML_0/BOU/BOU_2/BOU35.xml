<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES. FESTONS ET ASTRAGALES</title>
				<title type="sub"></title>
				<title type="medium">Une édition électronique</title>
				<author key="BOU">
					<name>
						<forename>Louis</forename>
						<surname>BOUILHET</surname>
					</name>
					<date from="1822" to="1869">1822-1869</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2963 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BOU_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>POÉSIES. FESTONS ET ASTRAGALES</title>
						<author>Louis Bouilhet</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k9737201m</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES. FESTONS ET ASTRAGALES</title>
								<author>Louis Bouilhet</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE NOUVELLE</publisher>
									<date when="1859">1859</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1859">1859</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-09-20" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.4.xsd)</change>
				<change when="2021-09-20" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BOU35">
				<head type="main">CIGOGNES ET TURBOTS</head>
				<opener>
					<salute>A ASINIUS SEMPRONIUS RUFUS</salute>
				</opener>
				<lg n="1">
					<l n="1" num="1.1">Salut, Sempronius, mortel inimitable !</l>
					<l n="2" num="1.2">O toi qui le premier fis servir sur ta table</l>
					<l n="3" num="1.3">La cigogne au. pied rouge et le turbot marin !</l>
					<l n="4" num="1.4">L’artiste, éternisant ta divine effigie,</l>
					<l n="5" num="1.5">Devait tailler pour toi les marbres de Phrygie</l>
					<l n="6" num="1.6"><space unit="char" quantity="8"></space>Et graver tes traits sur l’airain !</l>
				</lg>
				<lg n="2">
					<l n="7" num="2.1">Pour te montrer plus grand aux nations béantes,</l>
					<l n="8" num="2.2">Père des bons festins et des sauces piquantes,</l>
					<l n="9" num="2.3">Ton siècle s’épuisa dans ton enfantement !</l>
					<l n="10" num="2.4">Les destins dès longtemps préparaient ta venue,</l>
					<l n="11" num="2.5">Et quelque astre inconnu dut briller sous la nue</l>
					<l n="12" num="2.6"><space unit="char" quantity="8"></space>A ton premier vagissement !</l>
				</lg>
				<lg n="3">
					<l n="13" num="3.1">Avant toi, les Romains, dans leur instinct vulgaire,</l>
					<l n="14" num="3.2">De la chair des troupeaux et des fruits de la terre</l>
					<l n="15" num="3.3">Rassasiaient leur faim digne de vils pasteurs ;</l>
					<l n="16" num="3.4">Et l’écuelle de bois et la salière antique</l>
					<l n="17" num="3.5">Ornèrent, trois cents ans, cette table rustique</l>
					<l n="18" num="3.6"><space unit="char" quantity="8"></space>Où ruminaient les sénateurs.</l>
				</lg>
				<lg n="4">
					<l n="19" num="4.1">Quand ils se rassemblaient pour sauver la patrie,</l>
					<l n="20" num="4.2">Souvent l’odeur de l’ail emplissait la curie,</l>
					<l n="21" num="4.3">Jusqu’au portique sombre où s’inclinaient les rois,</l>
					<l n="22" num="4.4">Et laissant à moitié quelque brouet immonde,</l>
					<l n="23" num="4.5">Ils s’élançaient, d’un bond, à l’empire du monde,</l>
					<l n="24" num="4.6"><space unit="char" quantity="8"></space>Gorgés de raves et de pois.</l>
				</lg>
				<lg n="5">
					<l n="25" num="5.1">Au retour des combats, après quelque victoire,</l>
					<l n="26" num="5.2">Leur nef jetait au port sa cargaison de gloire,</l>
					<l n="27" num="5.3">Tétrarques, chefs vaincus, étendards en lambeaux…</l>
					<l n="28" num="5.4">Mais ils se trompaient tous, honneur à toi, grand homme !</l>
					<l n="29" num="5.5">Ta voile triomphante a rapporté dans Rome</l>
					<l n="30" num="5.6"><space unit="char" quantity="8"></space>Des cigognes et des turbots !</l>
				</lg>
				<lg n="6">
					<l n="31" num="6.1">Plus fort que ce marin dont le croc d’abordage</l>
					<l n="32" num="6.2">Éventrait à grand bruit les vaisseaux de Carthage,</l>
					<l n="33" num="6.3">Aux hérissons de mer tu lanças tes réseaux,</l>
					<l n="34" num="6.4">Et, conquérant gourmet, ceint de myrte et de lierre,</l>
					<l n="35" num="6.5">Avec tes cuisiniers tu parcourus la terre,</l>
					<l n="36" num="6.6"><space unit="char" quantity="8"></space>Pour assiéger des nids d’oiseaux !</l>
				</lg>
				<lg n="7">
					<l n="37" num="7.1">Rome alors, ô Rufus, méconnut ton génie,</l>
					<l n="38" num="7.2">Et l’on dit que le peuple, avec ignominie,</l>
					<l n="39" num="7.3">Refusa la préture à tes vœux obstinés…</l>
					<l n="40" num="7.4">Mais que t’importe, à toi, le bruit que fait la foule ?</l>
					<l n="41" num="7.5">Sa rumeur éphémère est un flot qui s’écoule,</l>
					<l n="42" num="7.6"><space unit="char" quantity="8"></space>Tes beaux jours ne sont pas sonnés !</l>
				</lg>
				<lg n="8">
					<l n="43" num="8.1">Ils viendront, ils viendront, quand, sur la capitale,</l>
					<l n="44" num="8.2">Soufflera mollement la brise orientale ;</l>
					<l n="45" num="8.3">Quand, sous sa mitre d’or, le pâle citoyen</l>
					<l n="46" num="8.4">Traînant par le forum sa démarche indolente,</l>
					<l n="47" num="8.5">Secoûra les parfums de sa robe volante,</l>
					<l n="48" num="8.6"><space unit="char" quantity="8"></space>Comme un satrape assyrien .</l>
				</lg>
				<lg n="9">
					<l n="49" num="9.1">Ils viendront quand, la nuit, l’impériale orgie</l>
					<l n="50" num="9.2">Jettera sous les cieux sa lueur élargie</l>
					<l n="51" num="9.3">Ou de sa chaude haleine embaumera les mers ;</l>
					<l n="52" num="9.4">Et tu t’éveilleras, et ton ombre sacrée</l>
					<l n="53" num="9.5">Viendra planer parfois sur les rocs de Caprée,</l>
					<l n="54" num="9.6"><space unit="char" quantity="8"></space>Au bruit des nocturnes concerts.</l>
				</lg>
				<lg n="10">
					<l n="55" num="10.1">O martyr des festins ! le luxe d’Italie</l>
					<l n="56" num="10.2">Vengera largement ta mémoire avilie,</l>
					<l n="57" num="10.3">Et tu pourras surgir de la poudre du sol,</l>
					<l n="58" num="10.4">Le jour où fumera, sur la table romaine,</l>
					<l n="59" num="10.5">Un sanglier sauvage, à la sauce troyenne,</l>
					<l n="60" num="10.6"><space unit="char" quantity="8"></space>Plein de langues de rossignol.</l>
				</lg>
			</div></body></text></TEI>