<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES. FESTONS ET ASTRAGALES</title>
				<title type="sub"></title>
				<title type="medium">Une édition électronique</title>
				<author key="BOU">
					<name>
						<forename>Louis</forename>
						<surname>BOUILHET</surname>
					</name>
					<date from="1822" to="1869">1822-1869</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2963 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BOU_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>POÉSIES. FESTONS ET ASTRAGALES</title>
						<author>Louis Bouilhet</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k9737201m</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES. FESTONS ET ASTRAGALES</title>
								<author>Louis Bouilhet</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE NOUVELLE</publisher>
									<date when="1859">1859</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1859">1859</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-09-20" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.4.xsd)</change>
				<change when="2021-09-20" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BOU46">
				<head type="main">LA CHANSON <lb></lb>DU MARCHAND DE MOURON</head>
				<lg n="1">
					<l n="1" num="1.1">Petits serins, petits moineaux,</l>
					<l n="2" num="1.2">Passez la tête à vos barreaux,</l>
					<l n="3" num="1.3">Je viens des bois et de la plaine,</l>
					<l n="4" num="1.4">De mouron frais ma hotte est pleine.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><space unit="char" quantity="8"></space>Mouron ! mouron !</l>
					<l n="6" num="2.2"><space unit="char" quantity="6"></space>Qui veut du mouron ?</l>
				</lg>
				<lg n="3">
					<l n="7" num="3.1">Au long des prés et des ruisseaux,</l>
					<l n="8" num="3.2">Des champs tout blonds aux verts coteaux,</l>
					<l n="9" num="3.3">Parmi la mousse et la bruyère,</l>
					<l n="10" num="3.4">Je vais cherchant la graine amère…</l>
				</lg>
				<lg n="4">
					<l n="11" num="4.1"><space unit="char" quantity="8"></space>Mouron ! mouron !</l>
					<l n="12" num="4.2"><space unit="char" quantity="6"></space>Qui veut du mouron ?</l>
				</lg>
				<lg n="5">
					<l n="13" num="5.1">Pour vous cueillir le picotin,</l>
					<l n="14" num="5.2">Je m’éveille, dès le matin,</l>
					<l n="15" num="5.3">Car, la nuit, mes songes fidèles,</l>
					<l n="16" num="5.4">Sont pleins de chants et de bruits d’ailes.</l>
				</lg>
				<lg n="6">
					<l n="17" num="6.1"><space unit="char" quantity="8"></space>Mouron ! mouron !</l>
					<l n="18" num="6.2"><space unit="char" quantity="6"></space>Qui veut du mouron ?</l>
				</lg>
				<lg n="7">
					<l n="19" num="7.1">Je suis le père des oiseaux,</l>
					<l n="20" num="7.2">Et, dans leur prison de roseaux,</l>
					<l n="21" num="7.3">Tous, quand je chante par la ville,</l>
					<l n="22" num="7.4">Frissonnent au perchoir mobile.</l>
				</lg>
				<lg n="8">
					<l n="23" num="8.1"><space unit="char" quantity="8"></space>Mouron ! mouron !</l>
					<l n="24" num="8.2"><space unit="char" quantity="6"></space>Qui veut du mouron ?</l>
				</lg>
				<lg n="9">
					<l n="25" num="9.1">Amis à l’œil luisant et noir,</l>
					<l n="26" num="9.2">Vous vous croirez libres, ce soir,</l>
					<l n="27" num="9.3">Quand, à la grille de vos cages,</l>
					<l n="28" num="9.4">S’étaleront mes gais feuillages.</l>
				</lg>
				<lg n="10">
					<l n="29" num="10.1"><space unit="char" quantity="8"></space>Mouron ! mouron !</l>
					<l n="30" num="10.2"><space unit="char" quantity="6"></space>Qui veut du mouron ?</l>
				</lg>
				<lg n="11">
					<l n="31" num="11.1">Merles, pinsons, chardonnerets,</l>
					<l n="32" num="11.2">J’ai vu vos frères des forêts,</l>
					<l n="33" num="11.3">Et j’ai des nouvelles certaines</l>
					<l n="34" num="11.4">Des bois, des monts, et des fontaines.</l>
				</lg>
				<lg n="12">
					<l n="35" num="12.1"><space unit="char" quantity="8"></space>Mouron ! mouron !</l>
					<l n="36" num="12.2"><space unit="char" quantity="6"></space>Qui veut du mouron ?</l>
				</lg>
				<lg n="13">
					<l n="37" num="13.1">Je les vois venir, par milliers,</l>
					<l n="38" num="13.2">Quand je passe au fond des halliers,</l>
					<l n="39" num="13.3">Et, pour me jaser dans l’oreille,</l>
					<l n="40" num="13.4">Plus d’un se pose à ma corbeille.</l>
				</lg>
				<lg n="14">
					<l n="41" num="14.1"><space unit="char" quantity="8"></space>Mouron ! mouron !</l>
					<l n="42" num="14.2"><space unit="char" quantity="6"></space>Qui veut du mouron ?</l>
				</lg>
			</div></body></text></TEI>