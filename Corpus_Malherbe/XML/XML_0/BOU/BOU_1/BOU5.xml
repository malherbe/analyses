<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Melænis</title>
				<title type="sub">conte romain</title>
				<title type="medium">Une édition électronique</title>
				<author key="BOU">
					<name>
						<forename>Louis</forename>
						<surname>BOUILHET</surname>
					</name>
					<date from="1822" to="1869">1822-1869</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2956 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2018">2018</date>
				<idno type="local">BOU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Melænis : conte romain</title>
						<author>Louis Bouilhet</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
						<orgname>
							<choice>
								<abbr>CNRTL</abbr>
								<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
							</choice>
						</orgname>
						<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
					</publisher>
					<idno type="FRANTEXT">M269</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Melænis : conte romains</title>
								<author>Louis Bouilhet</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>M. Levy</publisher>
									<date when="1857">1857</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1857">1857</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>Les balises de pagination ont été supprimées.</p>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique
					après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules en début de vers ont été restituées.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2018-12-20" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2018-12-20" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BOU5">
				<head type="main">CHANT CINQUIÈME</head>
				<lg n="1">
					<l n="1" num="1.1">Ô frère de l’amour, hyménée ! Hyménée !</l>
					<l n="2" num="1.2">Dieu couronné de fleurs, jeune homme aux blonds cheveux,</l>
					<l n="3" num="1.3">Toi dont la main secoue un flambeau résineux !</l>
					<l n="4" num="1.4">Toi qui conduis l’amant à la vierge étonnée,</l>
					<l n="5" num="1.5">Quand aux sons du crotale et de la flûte aimée,</l>
					<l n="6" num="1.6">L’étoile de Vénus palpite dans les cieux !</l>
				</lg>
				<lg n="2">
					<l n="7" num="2.1">Pour toi la jeune fille, en respirant à peine,</l>
					<l n="8" num="2.2">Va cueillir dans les champs son chapeau de verveine ;</l>
					<l n="9" num="2.3">Elle tremble, elle hésite, elle écoute en chemin,</l>
					<l n="10" num="2.4">Les bois, les prés, les flots au murmure incertain,</l>
					<l n="11" num="2.5">Et regarde en rêvant, la ceinture de laine</l>
					<l n="12" num="2.6">Que l’époux, sur ses pieds, fera tomber demain.</l>
				</lg>
				<lg n="3">
					<l n="13" num="3.1">Elle ne viendra plus dans les campagnes blondes</l>
					<l n="14" num="3.2">Jouer avec ses sœurs, aux rayons du soleil !</l>
					<l n="15" num="3.3">Car les temps sont passés des courses vagabondes,</l>
					<l n="16" num="3.4">Des plaisirs enfantins ; demain, à son réveil,</l>
					<l n="17" num="3.5">Elle sera l’épouse aux angoisses profondes,</l>
					<l n="18" num="3.6">Par qui vit la famille et le foyer vermeil.</l>
				</lg>
				<lg n="4">
					<l n="19" num="4.1">Elle sera mêlée aux mères sérieuses,</l>
					<l n="20" num="4.2">Chaste, grave, et parfois guidant avec fierté</l>
					<l n="21" num="4.3">Un beau groupe d’enfants qui saute à son côté,</l>
					<l n="22" num="4.4">Tandis que, contemplant leurs têtes gracieuses,</l>
					<l n="23" num="4.5">Le père, à ses cils noirs sent des larmes joyeuses</l>
					<l n="24" num="4.6">Glisser, comme la pluie, après un jour d’été.</l>
				</lg>
				<lg n="5">
					<l n="25" num="5.1">Oh ! Qui dira la paix, et le bonheur tranquille !</l>
					<l n="26" num="5.2">Muse, qui chantera, dans des vers assez doux,</l>
					<l n="27" num="5.3">La maison reluisante, et les baisers d’époux !</l>
					<l n="28" num="5.4">Les pénates, au feu, séchant leur corps d’argile !</l>
					<l n="29" num="5.5">Et l’essaim des valets, et le cercle immobile</l>
					<l n="30" num="5.6">Des aïeux, sur le seuil, rongés du temps jaloux !</l>
				</lg>
				<lg n="6">
					<l n="31" num="6.1">Elles vivaient ainsi, les mères d’Étrurie,</l>
					<l n="32" num="6.2">Celles du Latium et du pays sabin,</l>
					<l n="33" num="6.3">Gardant comme un trésor, loin du tumulte humain,</l>
					<l n="34" num="6.4">Le travail, la pudeur, les dieux et la patrie !</l>
					<l n="35" num="6.5">Elles n’attendaient pas qu’un préteur d’Illyrie</l>
					<l n="36" num="6.6">Vînt tenter leur vertu des colliers à la main !</l>
				</lg>
				<lg n="7">
					<l n="37" num="7.1">Laissons l’amant rôder près des murs de sa belle,</l>
					<l n="38" num="7.2">Et, les cheveux mouillés par les pleurs de la nuit,</l>
					<l n="39" num="7.3">Faire, sous le ciel noir, hurler le chien fidèle,</l>
					<l n="40" num="7.4">Comme un voleur furtif que la crainte poursuit.</l>
					<l n="41" num="7.5">J’aime mieux pour entrer, la porte que l’échelle,</l>
					<l n="42" num="7.6">J’aime mieux pour sortir, le calme que le bruit.</l>
				</lg>
				<lg n="8">
					<l n="43" num="8.1">L’amant, c’est le chasseur qui marche par la plaine,</l>
					<l n="44" num="8.2">Hâve et noir de poussière, avec son lévrier ;</l>
					<l n="45" num="8.3">L’époux majestueux ressemble au cuisinier</l>
					<l n="46" num="8.4">Qui, sans battre les bois, a sa marmite pleine ;</l>
					<l n="47" num="8.5">Tous deux, filet en main, vont cherchant leur aubaine :</l>
					<l n="48" num="8.6">L’amant pêche à la mer et l’époux au vivier.</l>
				</lg>
				<lg n="9">
					<l n="49" num="9.1">Quand les temps sont partis de la jeunesse folle,</l>
					<l n="50" num="9.2">Quand il n’a plus de jour à jeter au destin,</l>
					<l n="51" num="9.3">L’homme, essoufflé, s’assoit sur le bord du chemin,</l>
					<l n="52" num="9.4">Entre l’avenir sombre et le passé frivole,</l>
					<l n="53" num="9.5">Alors l’épouse vient, l’épouse qui console</l>
					<l n="54" num="9.6">Et relève son âme en lui tendant la main !…</l>
				</lg>
				<lg n="10">
					<l n="55" num="10.1">« Des perles ! » dit Bacca, courant par les cuisines,</l>
					<l n="56" num="10.2">Plus ardent que Vulcain au bord de ses fourneaux :</l>
					<l n="57" num="10.3">« J’aime dans les pois gris l’éclat des perles fines !</l>
					<l n="58" num="10.4">« Arrosez la polente avec le vin de Cos ;</l>
					<l n="59" num="10.5">« Ces huîtres seraient mieux sous des herbes marines.</l>
					<l n="60" num="10.6">« Pressez le feu ; Chrysale, a-t-on vu mes turbots ?…</l>
				</lg>
				<lg n="11">
					<l n="61" num="11.1">« ‒ Non, maître ! ‒ par Bacchus ! C’est une raillerie !</l>
					<l n="62" num="11.2">« Fiez-vous pour souper au vaisseau d’un préteur !</l>
					<l n="63" num="11.3">« Jamais gabarre à flot n’eut pareille lenteur !</l>
					<l n="64" num="11.4">« Herclé !… pas de turbots !… le jour qu’on se marie !… »</l>
					<l n="65" num="11.5">Et le bon cuisinier tordait avec furie</l>
					<l n="66" num="11.6">Ses cheveux grisonnants, sur son front en sueur.</l>
				</lg>
				<lg n="12">
					<l n="67" num="12.1">C’était un homme osseux et maigre de figure,</l>
					<l n="68" num="12.2">Bien que tout cuisinier classique soit orné</l>
					<l n="69" num="12.3">D’un ventre respectable et d’un chef bourgeonné ;</l>
					<l n="70" num="12.4">Mais lui ne rôtissait que pour la gloire pure.</l>
					<l n="71" num="12.5">Tel bâtit des palais qui couche sur la dure,</l>
					<l n="72" num="12.6">Tel barde des faisans qui n’a pas déjeuné !…</l>
				</lg>
				<lg n="13">
					<l n="73" num="13.1">Et les broches tournaient, de bécasses chargées,</l>
					<l n="74" num="13.2">Et la cigogne blanche aux ailes allongées</l>
					<l n="75" num="13.3">Couvait des œufs de paon, dans des corbeilles d’or ;</l>
					<l n="76" num="13.4">De grands poissons d’azur qui semblaient vivre encor</l>
					<l n="77" num="13.5">Nageaient dans le safran, tandis que deux rangées</l>
					<l n="78" num="13.6">D’huîtres et d’escargots se pressaient sur le bord.</l>
				</lg>
				<lg n="14">
					<l n="79" num="14.1">Une rumeur montait, incessante et profonde,</l>
					<l n="80" num="14.2">De valets affairés que la sueur inonde,</l>
					<l n="81" num="14.3">De vaisselle sonore et de poêlons d’airain,</l>
					<l n="82" num="14.4">Frémissant sur le feu comme le flot marin :</l>
					<l n="83" num="14.5">« Allons ! Criait Bacca pour exciter son monde,</l>
					<l n="84" num="14.6">« Vous boirez, à la noce, un tonneau de bon vin !</l>
				</lg>
				<lg n="15">
					<l n="85" num="15.1">« A-t-on vu mes turbots ?… ‒ pas encor, dit Chrysale.</l>
					<l n="86" num="15.2">« ‒ Bombax !… » c’était le mot terrible et redouté ;</l>
					<l n="87" num="15.3">Les marmitons tremblaient et Bacca devint pâle :</l>
					<l n="88" num="15.4">« S’ils arrivent trop tard, c’est une indignité !</l>
					<l n="89" num="15.5">« Au Tibre !… il en est temps ! Courez sans intervalle !…</l>
					<l n="90" num="15.6">« J’ai vécu cinquante ans par mon nom respecté ;</l>
				</lg>
				<lg n="16">
					<l n="91" num="16.1">« J’ai fait de grands repas et des fêtes splendides,</l>
					<l n="92" num="16.2">« Où, couronnés de fleurs et la coupe à la main,</l>
					<l n="93" num="16.3">« Cent convives joyeux mangeaient jusqu’au matin !</l>
					<l n="94" num="16.4">« De la Bretagne froide aux régions torrides,</l>
					<l n="95" num="16.5">« Quand nous avions soupé, trois mondes étaient vides !</l>
					<l n="96" num="16.6">« Et les dieux, à Bacca devaient une autre fin !…</l>
				</lg>
				<lg n="17">
					<l n="97" num="17.1">« ‒ Les turbots ! » dit Chrysale en enfonçant la porte</l>
					<l n="98" num="17.2">Plutôt qu’il ne l’ouvrit, tant sa joie était forte ;</l>
					<l n="99" num="17.3">Puis, auprès du vieux maître arrivant en deux bonds :</l>
					<l n="100" num="17.4">« Je les ai vus moi-même ! Ils sont dodus et ronds !</l>
					<l n="101" num="17.5">« Mais le préteur est mort en voyage ! ‒ qu’importe !</l>
					<l n="102" num="17.6">« Dit Bacca radieux, si les turbots sont bons !… »</l>
				</lg>
				<lg n="18">
					<l n="103" num="18.1">La villa de Tibur avait un air de fête,</l>
					<l n="104" num="18.2">Les murs de marbre blanc semblaient frémir d’amour ;</l>
					<l n="105" num="18.3">S’il n’était pas si tard nous en ferions le tour ;</l>
					<l n="106" num="18.4">Mais voici Marcia qui passe sa toilette :</l>
					<l n="107" num="18.5">Dans les miroirs d’argent, sa beauté se reflète,</l>
					<l n="108" num="18.6">Et l’ambre et les parfums voltigent alentour !</l>
				</lg>
				<lg n="19">
					<l n="109" num="19.1">Avez-vous vu parfois, sur une coupe antique,</l>
					<l n="110" num="19.2">Entre deux beaux festons d’acanthe sinueux,</l>
					<l n="111" num="19.3">Diane chasseresse avec ses longs cheveux,</l>
					<l n="112" num="19.4">Quand elle sort de l’onde, et, baigneuse pudique,</l>
					<l n="113" num="19.5">Livre aux nymphes des bois sa gorge magnifique,</l>
					<l n="114" num="19.6">Et ses pieds nus, mouillés par les flots amoureux !</l>
				</lg>
				<lg n="20">
					<l n="115" num="20.1">Telle et plus jeune encor près d’une eau qui murmure,</l>
					<l n="116" num="20.2">Dans un bassin de marbre aux contours ciselés,</l>
					<l n="117" num="20.3">Frémissante, et les yeux par ses grands cils voilés,</l>
					<l n="118" num="20.4">Marcia souriait ; sous sa blanche parure,</l>
					<l n="119" num="20.5">Une esclave, avec art attachait la ceinture,</l>
					<l n="120" num="20.6">L’autre, les brodequins de perles étoilés.</l>
				</lg>
				<lg n="21">
					<l n="121" num="21.1">Ses longs cheveux tombaient comme ceux des vestales,</l>
					<l n="122" num="21.2">Séparés par le fer en six tresses égales,</l>
					<l n="123" num="21.3">L’anneau serrait son doigt, et du coffre odorant,</l>
					<l n="124" num="21.4">Les matrones tiraient le voile de safran,</l>
					<l n="125" num="21.5">Avec la pièce d’or des fêtes nuptiales,</l>
					<l n="126" num="21.6">Et le fuseau qui dit : « travaillez en aimant ! »</l>
				</lg>
				<lg n="22">
					<l n="127" num="22.1">Ainsi qu’un arc tendu, sur son œil qui pétille,</l>
					<l n="128" num="22.2">Son sourcil se courbait par le pinceau tracé ;</l>
					<l n="129" num="22.3">Entre ses dents d’émail un souffle cadencé</l>
					<l n="130" num="22.4">Glissait comme la brise au bord d’une coquille ;</l>
					<l n="131" num="22.5">Un petit serpent vert dont la tête frétille</l>
					<l n="132" num="22.6">Entourait son bras nu, d’un bracelet glacé.</l>
				</lg>
				<lg n="23">
					<l n="133" num="23.1">Des toiles de Milet, des tuniques traînantes,</l>
					<l n="134" num="23.2">Parmi les beaux colliers sur les tables épars,</l>
					<l n="135" num="23.3">Déroulaient à longs plis leurs teintes chatoyantes ;</l>
					<l n="136" num="23.4">Les couronnes de fleurs riaient de toutes parts,</l>
					<l n="137" num="23.5">C’était un bruit confus d’étoffes ondoyantes,</l>
					<l n="138" num="23.6">Et mille reflets d’or à troubler les regards.</l>
				</lg>
				<lg n="24">
					<l n="139" num="24.1">« Salut ! » dit le bouffon en entrant dans la salle,</l>
					<l n="140" num="24.2">Avec une façon de tête triomphale,</l>
					<l n="141" num="24.3">Et portant, comme amour, la torche et le carquois :</l>
					<l n="142" num="24.4">« Je me marie aussi, je viens faire mon choix ;</l>
					<l n="143" num="24.5">« Celle-ci me plairait, mais elle est un peu pâle ;</l>
					<l n="144" num="24.6">« Cette autre serait mieux plus haute de deux doigts ! »</l>
				</lg>
				<lg n="25">
					<l n="145" num="25.1">Et Coracoïdès, comme un patron sans gêne,</l>
					<l n="146" num="25.2">Devant chaque suivante agitait son flambeau ;</l>
					<l n="147" num="25.3">Puis, saisissant le bras d’une esclave africaine :</l>
					<l n="148" num="25.4">« Par Castor ! Cria-t-il, c’est le meilleur morceau !</l>
					<l n="149" num="25.5">« Je préfère aux seins blancs les poitrines d’ébène,</l>
					<l n="150" num="25.6">« C’est le cœur, après tout, qui leur monte à la peau !</l>
				</lg>
				<lg n="26">
					<l n="151" num="26.1">« J’aime ces yeux d’argent, ce nez dont les deux ailes</l>
					<l n="152" num="26.2">« S’étalent, cette bouche, aux bords gonflés et ronds,</l>
					<l n="153" num="26.3">« Qui semble avoir mangé des mûres. Nous verrons</l>
					<l n="154" num="26.4">« Des contrastes charmants et des choses nouvelles ;</l>
					<l n="155" num="26.5">« Ensemble, ce sera superbe : nous ferons</l>
					<l n="156" num="26.6">« Des enfants blancs et noirs, comme les hirondelles ! »</l>
				</lg>
				<lg n="27">
					<l n="157" num="27.1">Et vers sa sombre épouse il tendait, en parlant,</l>
					<l n="158" num="27.2">Une poire de coing, d’après l’antique usage :</l>
					<l n="159" num="27.3">« Dans neuf mois la grenade !… ajouta le galant,</l>
					<l n="160" num="27.4">« Je tente la fortune, et me voue au ménage ;</l>
					<l n="161" num="27.5">« Tous les maris trompés ne font pas le tapage</l>
					<l n="162" num="27.6">« De Ménélas. Pour moi, je suis moins pétulant !… »</l>
				</lg>
				<lg n="28">
					<l n="163" num="28.1">La suivante, immobile, écoutait cette histoire,</l>
					<l n="164" num="28.2">Sans montrer au dehors la moindre émotion ;</l>
					<l n="165" num="28.3">La pudeur libyenne est une fiction,</l>
					<l n="166" num="28.4">On ne saurait rougir, quand on a la peau noire</l>
					<l n="167" num="28.5">« Hélas ! Dit Marcia, joignant ses mains d’ivoire,</l>
					<l n="168" num="28.6">« Quel malheur de quitter un si gentil bouffon !</l>
				</lg>
				<lg n="29">
					<l n="169" num="29.1">« Quand je t’aurai perdu, qui donc me fera rire ?</l>
					<l n="170" num="29.2">« Mon pauvre petit nain, je ne te verrai plus</l>
					<l n="171" num="29.3">« Imiter, en dansant, le faune ou le satyre !…</l>
					<l n="172" num="29.4">« Mais, pourquoi, reprit-elle, abandonner Paulus ?</l>
					<l n="173" num="29.5">« Viens avec nous !… » le nain se tenait, sans mot dire,</l>
					<l n="174" num="29.6">Et roulait au hasard, des yeux irrésolus.</l>
				</lg>
				<lg n="30">
					<l n="175" num="30.1">Enfin, la tête basse, et d’une voix câline,</l>
					<l n="176" num="30.2">« Bacca vient-il aussi ? ‒ non, dit-elle en riant.</l>
					<l n="177" num="30.3">« ‒ C’est que… fit le bouffon, pâle et balbutiant,</l>
					<l n="178" num="30.4">« J’aime le vieux Bacca ! C’est une âme divine !… »</l>
					<l n="179" num="30.5">Et Coracoïdès, qu’un double instinct domine,</l>
					<l n="180" num="30.6">Sentait sourdre en lui-même, un combat effrayant.</l>
				</lg>
				<lg n="31">
					<l n="181" num="31.1">Le cœur et l’estomac luttaient de violence :</l>
					<l n="182" num="31.2">Il adorait Paulus, et la bécasse au vin,</l>
					<l n="183" num="31.3">Et voyait s’échapper, ainsi qu’un songe vain,</l>
					<l n="184" num="31.4">Les ragoûts safranés, l’ivresse et la bombance !…</l>
					<l n="185" num="31.5">On l’eût pris volontiers pour un chien qui balance</l>
					<l n="186" num="31.6">Entre la voix du maître et l’odeur du festin.</l>
				</lg>
				<lg n="32">
					<l n="187" num="32.1">Cependant sur les monts la nuit tendait ses voiles,</l>
					<l n="188" num="32.2">L’astre cher aux époux se levait dans les cieux ;</l>
					<l n="189" num="32.3">On entendait, au loin, les jeunes gens heureux</l>
					<l n="190" num="32.4">Qui jetaient, tous en chœur, leurs chansons aux étoiles.</l>
					<l n="191" num="32.5">« Il vient !… » dit Marcia, baissant les riches toiles</l>
					<l n="192" num="32.6">Dont le mince tissu voltigeait sur ses yeux.</l>
				</lg>
				<lg n="33">
					<l n="193" num="33.1">C’était le chant d’hymen, la flûte, les cymbales,</l>
					<l n="194" num="33.2">Et le pétillement des torches dans la nuit ;</l>
					<l n="195" num="33.3">Le cortège amoureux s’avançait… et le bruit</l>
					<l n="196" num="33.4">Montait, comme la mer, en bruyantes rafales.</l>
					<l n="197" num="33.5">Déjà sonnent les pieds sur le pavé des salles ;</l>
					<l n="198" num="33.6">Hyménée !… hyménée !… on approche… c’est lui !</l>
				</lg>
				<lg n="34">
					<l n="199" num="34.1">C’est lui, dans son manteau de pourpre tyrienne !</l>
					<l n="200" num="34.2">Beau, jeune, ivre d’espoir, et défiant les pleurs.</l>
					<l n="201" num="34.3">Sous leur toge de fête aux riantes couleurs,</l>
					<l n="202" num="34.4">Ses amis, alentour, effeuillaient la verveine,</l>
					<l n="203" num="34.5">Et tout frottés d’onguents, selon la mode ancienne,</l>
					<l n="204" num="34.6">Cinq enfants secouaient des flambeaux et des fleurs.</l>
				</lg>
				<lg n="35">
					<l n="205" num="35.1">« Caia !… Thalassius !… hyménée !… hyménée !… »</l>
					<l n="206" num="35.2">Ainsi chantaient cent voix montant à l’unisson ;</l>
					<l n="207" num="35.3">Des esclaves portaient la quenouille ordonnée,</l>
					<l n="208" num="35.4">Le coffret odorant, l’eau sainte, la toison,</l>
					<l n="209" num="35.5">Et la graisse de loup, dont l’épouse bien née</l>
					<l n="210" num="35.6">Doit frotter, en entrant, le seuil de sa maison.</l>
				</lg>
				<lg n="36">
					<l n="211" num="36.1">Les matrones à part, sous leur voile pudique,</l>
					<l n="212" num="36.2">Pour le dernier conseil se réservaient encor.</l>
					<l n="213" num="36.3">Ce jour-là, jusqu’au bout, l’édile fut très-fort,</l>
					<l n="214" num="36.4">La lèvre souriante et d’un air pacifique,</l>
					<l n="215" num="36.5">Il tenait, pour signer, le sigillum antique,</l>
					<l n="216" num="36.6">Ancus avec Numa, l’aqueduc et le port !</l>
				</lg>
				<lg n="37">
					<l n="217" num="37.1">Ce fut lui qui, prenant le rôle de la mère,</l>
					<l n="218" num="37.2">Étreignit au départ la belle sur son cœur,</l>
					<l n="219" num="37.3">Et laissant échapper la larme de rigueur,</l>
					<l n="220" num="37.4">La retint dans ses bras comme il convient de faire ;</l>
					<l n="221" num="37.5">Car déjà deux enfants, ceints de myrte et de lierre,</l>
					<l n="222" num="37.6">Entraînaient par la main, l’épouse du rhéteur.</l>
				</lg>
				<lg n="38">
					<l n="223" num="38.1">C’est alors qu’au milieu des pompes érotiques</l>
					<l n="224" num="38.2">On entendit des cris dans l’ombre du jardin,</l>
					<l n="225" num="38.3">Et la voix des valets courant sous les portiques :</l>
					<l n="226" num="38.4">« Elle est folle !… arrêtez !… » mais sur ses gonds d’airain</l>
					<l n="227" num="38.5">La porte tressaillit, et, cessant les cantiques,</l>
					<l n="228" num="38.6">La foule, comme un flot, se replia soudain…</l>
				</lg>
				<lg n="39">
					<l n="229" num="39.1">Calme plein de terreur qui couve la tempête !</l>
					<l n="230" num="39.2">Melænis, en silence, apparut sur le seuil ;</l>
					<l n="231" num="39.3">On eût dit une morte échappée au cercueil ;</l>
					<l n="232" num="39.4">Entre les rangs pressés, sans retourner la tête,</l>
					<l n="233" num="39.5">Elle allait à pas lents et tachait de son deuil</l>
					<l n="234" num="39.6">L’or et la pourpre en feu sur les robes de fête :</l>
				</lg>
				<lg n="40">
					<l n="235" num="40.1">« Éteignez ces flambeaux ! Cessez vos chants joyeux !</l>
					<l n="236" num="40.2">« Avant d’unir ici l’inceste à l’adultère,</l>
					<l n="237" num="40.3">« Souvenez-vous des morts, et respectez les dieux ! »</l>
					<l n="238" num="40.4">Sa voix sur tous les fronts roulait comme un tonnerre :</l>
					<l n="239" num="40.5">« Arrêtez ! » et du bras les séparant tous deux :</l>
					<l n="240" num="40.6">« Paulus, voici ta sœur ! Marcia, c’est ton frère !</l>
				</lg>
				<lg n="41">
					<l n="241" num="41.1">« ‒ Son frère !… dit la foule, étrange événement !</l>
					<l n="242" num="41.2">« Qui l’eût cru ? Qui l’eût dit ? Est-ce un fourbe ? Est-ce un traître ?</l>
					<l n="243" num="41.3">« ‒ C’est faux ! Hurla Paulus, cette danseuse ment !</l>
					<l n="244" num="41.4">« J’arracherai son masque et la ferai connaître !… »</l>
					<l n="245" num="41.5">Melænis, de la main, l’écarta gravement :</l>
					<l n="246" num="41.6">« Ici, chez Marcius, je n’écoute qu’un maître !</l>
				</lg>
				<lg n="42">
					<l n="247" num="42.1">« Vois tous tes conviés, ils pâlissent d’effroi !</l>
					<l n="248" num="42.2">« C’est que ta mère est morte ! Et la vieille sibylle</l>
					<l n="249" num="42.3">« N’a pas de tombe encor sur son cadavre froid !…</l>
					<l n="250" num="42.4">« ‒ Staphyla ! Fit Paulus. ‒ ta mère ! » mais l’édile :</l>
					<l n="251" num="42.5">« Ai-je bien entendu ! Qui parle de Staphyle ?…</l>
					<l n="252" num="42.6">« ‒ Vieillard, dit Melænis en s’avançant, c’est moi !</l>
				</lg>
				<lg n="43">
					<l n="253" num="43.1">« Moi, qui seule ai reçu sa parole dernière,</l>
					<l n="254" num="43.2">« Moi, qui seule en mes bras soutins son front glacé,</l>
					<l n="255" num="43.3">« Moi, dont les yeux ont vu comme au bord d’un cratère,</l>
					<l n="256" num="43.4">« Les abîmes d’un cœur où l’amour a passé !…</l>
					<l n="257" num="43.5">« Et je m’étonne encor que le fils et le père</l>
					<l n="258" num="43.6">« N’aient pas frémi, dans l’âme, au cri qu’elle a poussé !</l>
				</lg>
				<lg n="44">
					<l n="259" num="44.1">« Donc, s’il te reste au cœur quelque trace incertaine,</l>
					<l n="260" num="44.2">« Quelque écho du passé qui murmure tout bas,</l>
					<l n="261" num="44.3">« Souviens-toi, Marcius, de cette campanienne,</l>
					<l n="262" num="44.4">« Qui partit en serrant son fils entre ses bras.</l>
					<l n="263" num="44.5">« ‒ Dieux ! Fit l’édile en pleurs, ma jeunesse lointaine</l>
					<l n="264" num="44.6">« Accourt comme un fantôme au-devant de mes pas !</l>
				</lg>
				<lg n="45">
					<l n="265" num="45.1">« Je comprends maintenant ma haine et ma tendresse ;</l>
					<l n="266" num="45.2">« Mon fils, embrasse-moi ! Cette femme a raison :</l>
					<l n="267" num="45.3">« Quel que soit le transport dont l’aiguillon nous presse,</l>
					<l n="268" num="45.4">« Le sang ne connaît pas de modération,</l>
					<l n="269" num="45.5">« Et je fus, tour à tour, pardonne à ma faiblesse,</l>
					<l n="270" num="45.6">« Ton père par l’amour et par l’aversion !… »</l>
				</lg>
				<lg n="46">
					<l n="271" num="46.1">Les gens qui soupent bien ont l’âme épanouie,</l>
					<l n="272" num="46.2">Rien n’est tendre et naïf comme un buveur joyeux ;</l>
					<l n="273" num="46.3">Ce fut, sur ma parole, un tableau curieux</l>
					<l n="274" num="46.4">De voir le gros coupable, à la face élargie,</l>
					<l n="275" num="46.5">Ainsi qu’un jouvenceau tomber dans l’élégie</l>
					<l n="276" num="46.6">Et se meurtrir le sein pour un crime amoureux.</l>
				</lg>
				<lg n="47">
					<l n="277" num="47.1">Il riait, il pleurait, et tournant par la salle,</l>
					<l n="278" num="47.2">De sa fille à son fils courait incessamment ;</l>
					<l n="279" num="47.3">‒ Melænis avait fui pendant cet intervalle, ‒</l>
					<l n="280" num="47.4">Et Marcia, sans voix comme sans mouvement,</l>
					<l n="281" num="47.5">Égalait en pâleur sa robe nuptiale :</l>
					<l n="282" num="47.6">« Ô ma sœur ! Adieu donc !… » dit Paulus tristement.</l>
				</lg>
				<lg n="48">
					<l n="283" num="48.1">Il avait ce visage, à peindre difficile,</l>
					<l n="284" num="48.2">Qu’il est fin de cacher sous un pan du manteau.</l>
					<l n="285" num="48.3">Marcia, Melænis, la sorcière, l’édile,</l>
					<l n="286" num="48.4">Tout s’agitait ensemble au fond de son cerveau,</l>
					<l n="287" num="48.5">Et chaque souvenir, fugitif et mobile,</l>
					<l n="288" num="48.6">Lui passait sur le front comme une ombre sur l’eau.</l>
				</lg>
				<lg n="49">
					<l n="289" num="49.1">Hélas ! Le vide aux mains et le doute dans l’âme,</l>
					<l n="290" num="49.2">Il voyait tout à coup, formidable réveil,</l>
					<l n="291" num="49.3">Pâlir son Eurydice au seuil du jour vermeil !</l>
					<l n="292" num="49.4">Devant ses longs regards, sans larmes et sans flamme,</l>
					<l n="293" num="49.5">La sœur se dessinait gravement, et la femme</l>
					<l n="294" num="49.6">Fondait, comme la neige aux rayons du soleil !</l>
				</lg>
				<lg n="50">
					<l n="295" num="50.1">Il partit, il quitta la salle et les convives ;</l>
					<l n="296" num="50.2">Ces fronts parés de fleurs lui faisaient mal à voir ;</l>
					<l n="297" num="50.3">Il voulait l’air, l’espace, et, comme aux ondes vives,</l>
					<l n="298" num="50.4">Tremper sa tête en feu dans les brises du soir.</l>
					<l n="299" num="50.5">Mais la fête obstinée, en notes fugitives,</l>
					<l n="300" num="50.6">Courait autour de lui ; là-bas, sous le ciel noir,</l>
				</lg>
				<lg n="51">
					<l n="301" num="51.1">Les pâtres, suspendus au versant des collines,</l>
					<l n="302" num="51.2">Chantaient pour son hymen les strophes fescennines ;</l>
					<l n="303" num="51.3">Les villages dansaient, et Paulus aux abois,</l>
					<l n="304" num="51.4">Sous ses pieds, en passant faisait craquer les noix</l>
					<l n="305" num="51.5">Dont on avait semé les routes tiburtines :</l>
					<l n="306" num="51.6">« Ô Vénus !… cria-t-il, d’une tremblante voix,</l>
				</lg>
				<lg n="52">
					<l n="307" num="52.1">« À quoi bon dans les cieux, comme une raillerie,</l>
					<l n="308" num="52.2">« Sur mon front abattu secouer ton fanal ?</l>
					<l n="309" num="52.3">« Ne pouvais-tu du moins, refusant le signal,</l>
					<l n="310" num="52.4">« Cacher pour cette nuit ta lumière chérie ?…</l>
					<l n="311" num="52.5">« Hélas ! J’irai tout seul dans ma chambre fleurie,</l>
					<l n="312" num="52.6">« M’étendre, en sanglotant, sur le lit nuptial !</l>
				</lg>
				<lg n="53">
					<l n="313" num="53.1">« Les gais musiciens, parmi les feux sans nombre,</l>
					<l n="314" num="53.2">« Attendent le cortège et les époux nouveaux.</l>
					<l n="315" num="53.3">« Les flûtes se tairont et la nuit sera sombre.</l>
					<l n="316" num="53.4">« Marcia ! Marcia ! Devant les cinq flambeaux</l>
					<l n="317" num="53.5">« Je ne déferai pas ta ceinture !… et dans l’ombre</l>
					<l n="318" num="53.6">« Je ne sentirai pas, de tes cheveux si beaux,</l>
				</lg>
				<lg n="54">
					<l n="319" num="54.1">« Rouler les flots épars !… mon seul bien sur la terre,</l>
					<l n="320" num="54.2">« Quoi ! Mort ! évanoui ! Disparu sans retour !</l>
					<l n="321" num="54.3">« Que faire maintenant de ce cœur plein d’amour ?</l>
					<l n="322" num="54.4">« De mon sang ? De ma vie ? ô misère ! Misère !</l>
					<l n="323" num="54.5">« Un autre sur son sein l’étreindra quelque jour ;</l>
					<l n="324" num="54.6">« Un autre quelque jour ne sera pas son frère !…</l>
				</lg>
				<lg n="55">
					<l n="325" num="55.1">« ‒ Paulus ! Dit une voix, quelqu’un te reste encor !… »</l>
					<l n="326" num="55.2">Le jeune homme effaré fit un pas en arrière :</l>
					<l n="327" num="55.3">« Oh ! Je le sais trop bien !… dit-il avec effort,</l>
					<l n="328" num="55.4">« C’est mon mauvais génie envoyé sur la terre !</l>
					<l n="329" num="55.5">« Parle ! Que te faut-il à cette heure dernière ?</l>
					<l n="330" num="55.6">« Je suis tombé si bas, que je me ris du sort !…</l>
				</lg>
				<lg n="56">
					<l n="331" num="56.1">« ‒ Paulus, dit Melænis, je t’aime avec démence !</l>
					<l n="332" num="56.2">« Je t’aime avec fureur ! Ma haine et ma vengeance,</l>
					<l n="333" num="56.3">« Tu n’as donc pas compris que c’était de l’amour ?</l>
					<l n="334" num="56.4">« Hélas ! Courbant le front sous mon fardeau trop lourd,</l>
					<l n="335" num="56.5">« J’ai baisé tes pieds nus, et tu fus sans clémence !</l>
					<l n="336" num="56.6">« J’ai frappé ta poitrine, et ton cœur était sourd !…</l>
				</lg>
				<lg n="57">
					<l n="337" num="57.1">« Tu connais maintenant cette longue torture</l>
					<l n="338" num="57.2">« Qui fait le jour sans joie et la nuit sans sommeil ;</l>
					<l n="339" num="57.3">« Tu sais le sang qui bout, à la lave pareil,</l>
					<l n="340" num="57.4">« La bouche qui frémit, la tempe qui murmure ;</l>
					<l n="341" num="57.5">« Oh ! Tu peux mesurer mon mal à ta blessure,</l>
					<l n="342" num="57.6">« Et dire ce qu’on souffre au moment du réveil ! »</l>
				</lg>
				<lg n="58">
					<l n="343" num="58.1">Elle avait dans la voix une musique étrange ;</l>
					<l n="344" num="58.2">Et Paulus l’écoutait, comme les matelots</l>
					<l n="345" num="58.3">La sirène qui chante, assise au bord des flots.</l>
					<l n="346" num="58.4">Depuis quelques instants, dans le ciel sans mélange,</l>
					<l n="347" num="58.5">Des nuages flottaient ainsi que des îlots,</l>
					<l n="348" num="58.6">Et parfois, un éclair glissait comme une frange</l>
				</lg>
				<lg n="59">
					<l n="349" num="59.1">À l’horizon plus noir. Ce que sentait Paulus,</l>
					<l n="350" num="59.2">Ce n’était pas l’amour ni l’ivresse insensée,</l>
					<l n="351" num="59.3">Mais l’engourdissement de toute la pensée,</l>
					<l n="352" num="59.4">Mais la froide sueur sur ses membres perclus.</l>
					<l n="353" num="59.5">Comme en un songe affreux, sa poitrine oppressée</l>
					<l n="354" num="59.6">S’épuisait pour crier en spasmes superflus !</l>
				</lg>
				<lg n="60">
					<l n="355" num="60.1">Une puissance occulte envahissait son âme,</l>
					<l n="356" num="60.2">Des tonnerres lointains roulaient au fond des cieux :</l>
					<l n="357" num="60.3">« Qu’es-tu donc ? Lui dit-il en ouvrant de grands yeux,</l>
					<l n="358" num="60.4">« Où prends-tu cette voix qui charme et cette flamme</l>
					<l n="359" num="60.5">« Qui, dans tes longs regards, brille comme une lame ?</l>
					<l n="360" num="60.6">« Quel effrayant destin nous enchaîna tous deux ?…</l>
				</lg>
				<lg n="61">
					<l n="361" num="61.1">« ‒ Que t’importe, ô Paulus ! S’écria la danseuse,</l>
					<l n="362" num="61.2">« Aimons-nous ! Aimons-nous ! Cela seul est réel !</l>
					<l n="363" num="61.3">« Viens cacher nos baisers dans la nuit orageuse !</l>
					<l n="364" num="61.4">« Notre torche d’hymen, c’est la tempête au ciel !</l>
					<l n="365" num="61.5">« Nous fuirons ; nous aurons quelque retraite ombreuse</l>
					<l n="366" num="61.6">« Pour y faire, à nos cœurs, un exil éternel !…</l>
				</lg>
				<lg n="62">
					<l n="367" num="62.1">« Viens ! Qu’attends-tu ? Partons ! Pour nos désirs immenses,</l>
					<l n="368" num="62.2">« Paulus, la vie est courte, et le monde est étroit.</l>
					<l n="369" num="62.3">« C’est un souffle fatal qui me pousse vers toi.</l>
					<l n="370" num="62.4">« Notre bonheur est fait de pleurs et de vengeances,</l>
					<l n="371" num="62.5">« Et cet amour terrible aura des violences</l>
					<l n="372" num="62.6">« Pleines de volupté, de délire et d’effroi ! »</l>
				</lg>
				<lg n="63">
					<l n="373" num="63.1">Sa voix tomba ; le vent soulevait la poussière,</l>
					<l n="374" num="63.2">La tempête grondait, et je ne sais comment,</l>
					<l n="375" num="63.3">Mais leurs bouches en feu s’unirent lentement ;</l>
					<l n="376" num="63.4">Tour à tour voilés d’ombre, ou baignés de lumière,</l>
					<l n="377" num="63.5">Ils se tenaient debout, sous le ciel écumant,</l>
					<l n="378" num="63.6">Et s’embrassaient tous deux aux éclats du tonnerre !</l>
				</lg>
				<lg n="64">
					<l n="379" num="64.1">Mais tout à coup, le bruit d’un pas retentissant</l>
					<l n="380" num="64.2">Frappa l’ombre, un fer nu brilla près d’un visage.</l>
					<l n="381" num="64.3">« À moi ! Je suis blessé !… » dit Paulus frémissant.</l>
					<l n="382" num="64.4">Puis il tomba d’un bloc, sans parler davantage,</l>
					<l n="383" num="64.5">Et la danseuse vit, aux lueurs de l’orage,</l>
					<l n="384" num="64.6">Un soldat qui fuyait, avec son glaive en sang !</l>
				</lg>
				<lg n="65">
					<l n="385" num="65.1">Ce que fit Melænis, après cette aventure,</l>
					<l n="386" num="65.2">Je l’ignore ! ô lecteur ! Vint-elle au cabaret</l>
					<l n="387" num="65.3">Trouver Pantabolus et payer la blessure ?</l>
					<l n="388" num="65.4">Dansa-t-on cette nuit aux bouges de Suburre ?</l>
					<l n="389" num="65.5">J’ai cherché vainement ; l’hôtelier, fort discret,</l>
					<l n="390" num="65.6">N’a pas, même à prix d’or, dévoilé le secret.</l>
				</lg>
				<lg n="66">
					<l n="391" num="66.1">Commode, l’empereur, eut une fin tragique ;</l>
					<l n="392" num="66.2">Trahi par ses amis, malgré son nom divin,</l>
					<l n="393" num="66.3">Hercule rendit l’âme, étranglé dans son bain.</l>
					<l n="394" num="66.4">Quant à Pantabolus, il partit pour l’Afrique,</l>
					<l n="395" num="66.5">Fut fait centurion, tomba d’un coup de pique,</l>
					<l n="396" num="66.6">Et regretta surtout les filles et le vin.</l>
				</lg>
				<lg n="67">
					<l n="397" num="67.1">Aux festins du patron, s’arrondissant la panse,</l>
					<l n="398" num="67.2">Stellio vécut vieux et devint gras à lard.</l>
					<l n="399" num="67.3">Le muletier prit femme et se pendit plus tard.</l>
					<l n="400" num="67.4">Le bon Polydamas, le maître d’éloquence,</l>
					<l n="401" num="67.5">Comme il se promenait un jour sans méfiance,</l>
					<l n="402" num="67.6">Mourut d’un barbarisme entendu par hasard.</l>
				</lg>
				<lg n="68">
					<l n="403" num="68.1">Bacca fut l’inventeur de la sauce troyenne,</l>
					<l n="404" num="68.2">Ou la poussa, du moins, à sa perfection.</l>
					<l n="405" num="68.3">Le bouffon se noya dans la marmite pleine,</l>
					<l n="406" num="68.4">Un soir qu’à la cuisine il volait un bouillon.</l>
					<l n="407" num="68.5">Marcia ? ‒ le bruit court qu’elle se fit chrétienne.</l>
					<l n="408" num="68.6">Marcius ? ‒ il creva d’une indigestion !</l>
				</lg>
			</div></body></text></TEI>