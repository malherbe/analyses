<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">DERNIÈRES CHANSONS</title>
				<title type="medium">Une édition électronique</title>
				<author key="BOU">
					<name>
						<forename>Louis</forename>
						<surname>BOUILHET</surname>
					</name>
					<date from="1822" to="1869">1822-1869</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2222 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BOU_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>DERNIÈRES CHANSONS</title>
						<title>POÉSIES POSTHUMES</title>
						<author>Louis Bouilhet</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">M253</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>DERNIÈRES CHANSONS</title>
								<title>POÉSIES POSTHUMES</title>
								<author>Louis Bouilhet</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>M. Levy</publisher>
									<date when="1872">1872</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>ŒUVRES DE LOUIS BOUILHET</title>
						<title>FESTONS ET ASTRAGALES, MELÆNIS, DERNIÈRES CHANSONS</title>
						<author>Louis Bouilhet</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ALPHONSE LEMERRE ÉDITEUR</publisher>
							<date when="1891">1891</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1869">1869</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>Les balises de pagination ont été supprimées.</p>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules en début de vers ont été restituées.</p>
					<p>Les chiffres romains de la numérotation des poèmes ont été rétablis.</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-09-20" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.4.xsd)</change>
				<change when="2021-09-20" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BOU97">
				<head type="number">XXX</head>
				<head type="main">LIED NORMAND</head>
				<head type="sub_2">(reconstruit avec les débris d’une vieille chanson normande.)</head>
				<lg n="1">
					<l n="1" num="1.1"><space unit="char" quantity="10"></space>Sous le chèvrefeuil,</l>
					<l n="2" num="1.2"><space unit="char" quantity="10"></space>Je vidais bouteille.</l>
					<l n="3" num="1.3"><space unit="char" quantity="10"></space>Trois amis en deuil</l>
					<l n="4" num="1.4"><space unit="char" quantity="10"></space>M’ont dit à l’oreille :</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><space unit="char" quantity="2"></space> ‒ eh ! Bon ! Bon ! Bon ! ‒ qu’on nous verse encor !</l>
					<l n="6" num="2.2">Le vin, c’est du sang ! ‒ le cidre, de l’or !</l>
				</lg>
				<lg n="3">
					<l n="7" num="3.1"><space unit="char" quantity="10"></space>« Prends bien garde à toi,</l>
					<l n="8" num="3.2"><space unit="char" quantity="10"></space>On te fauche l’herbe. »</l>
					<l n="9" num="3.3"><space unit="char" quantity="10"></space> ‒ « Je n’ai pas d’effroi,</l>
					<l n="10" num="3.4"><space unit="char" quantity="10"></space>J’ai rentré ma gerbe ! »</l>
				</lg>
				<lg n="4">
					<l n="11" num="4.1"><space unit="char" quantity="2"></space> ‒ eh ! Bon ! Bon ! Bon ! ‒ qu’on nous verse encor !</l>
					<l n="12" num="4.2">Le vin, c’est du sang ! ‒ le cidre, de l’or !</l>
				</lg>
				<lg n="5">
					<l n="13" num="5.1"><space unit="char" quantity="10"></space>Il prend son cheval,</l>
					<l n="14" num="5.2"><space unit="char" quantity="10"></space>Sa bride et sa selle,</l>
					<l n="15" num="5.3"><space unit="char" quantity="10"></space>Et court, par le val,</l>
					<l n="16" num="5.4"><space unit="char" quantity="10"></space>Au seuil de sa belle.</l>
				</lg>
				<lg n="6">
					<l n="17" num="6.1"><space unit="char" quantity="2"></space> ‒ eh ! Bon ! Bon ! Bon ! ‒ qu’on nous verse encor !</l>
					<l n="18" num="6.2">Le vin, c’est du sang ! ‒ le cidre, de l’or !</l>
				</lg>
				<lg n="7">
					<l n="19" num="7.1"><space unit="char" quantity="10"></space>Y trouve un garçon,</l>
					<l n="20" num="7.2"><space unit="char" quantity="10"></space>Qui faisait ripaille,</l>
					<l n="21" num="7.3"><space unit="char" quantity="10"></space>Lequel eut frisson,</l>
					<l n="22" num="7.4"><space unit="char" quantity="10"></space>De peur de bataille.</l>
				</lg>
				<lg n="8">
					<l n="23" num="8.1"><space unit="char" quantity="2"></space> ‒ eh ! Bon ! Bon ! Bon ! ‒ qu’on nous verse encor !</l>
					<l n="24" num="8.2">Le vin, c’est du sang ! ‒ le cidre, de l’or !</l>
				</lg>
				<lg n="9">
					<l n="25" num="9.1"><space unit="char" quantity="10"></space> ‒ « Reste désormais</l>
					<l n="26" num="9.2"><space unit="char" quantity="10"></space>Près de cette femme ;</l>
					<l n="27" num="9.3"><space unit="char" quantity="10"></space>Tu n’auras jamais</l>
					<l n="28" num="9.4"><space unit="char" quantity="10"></space>L’orgueil de mon âme ! »</l>
				</lg>
				<lg n="10">
					<l n="29" num="10.1"><space unit="char" quantity="2"></space> ‒ eh ! Bon ! Bon ! Bon ! ‒ qu’on nous verse encor !</l>
					<l n="30" num="10.2">Le vin, c’est du sang ! ‒ le cidre, de l’or !</l>
				</lg>
				<lg n="11">
					<l n="31" num="11.1"><space unit="char" quantity="10"></space> ‒ « Au fond de son coeur,</l>
					<l n="32" num="11.2"><space unit="char" quantity="10"></space>J’ai cueilli naguère</l>
					<l n="33" num="11.3"><space unit="char" quantity="10"></space>Une belle fleur</l>
					<l n="34" num="11.4"><space unit="char" quantity="10"></space>Qu’on ne trouve guère.</l>
				</lg>
				<lg n="12">
					<l n="35" num="12.1"><space unit="char" quantity="2"></space> ‒ eh ! Bon ! Bon ! Bon ! ‒ qu’on nous verse encor !</l>
					<l n="36" num="12.2">Le vin, c’est du sang ! ‒ le cidre, de l’or !</l>
				</lg>
				<lg n="13">
					<l n="37" num="13.1"><space unit="char" quantity="10"></space>« J’ai couché trois ans,</l>
					<l n="38" num="13.2"><space unit="char" quantity="10"></space>La nuit avec elle,</l>
					<l n="39" num="13.3"><space unit="char" quantity="10"></space>Dans de beaux draps blancs,</l>
					<l n="40" num="13.4"><space unit="char" quantity="10"></space>Garnis de dentelle.</l>
				</lg>
				<lg n="14">
					<l n="41" num="14.1"><space unit="char" quantity="2"></space> ‒ eh ! Bon ! Bon ! Bon ! ‒ qu’on nous verse encor !</l>
					<l n="42" num="14.2">Le vin, c’est du sang ! ‒ le cidre, de l’or !</l>
				</lg>
				<lg n="15">
					<l n="43" num="15.1"><space unit="char" quantity="10"></space>« Reste et sois joyeux ! »</l>
					<l n="44" num="15.2"><space unit="char" quantity="10"></space> ‒ j’ai trois enfants d’elle</l>
					<l n="45" num="15.3"><space unit="char" quantity="10"></space>L’un est à Bayeux,</l>
					<l n="46" num="15.4"><space unit="char" quantity="10"></space>L’autre à la Rochelle !</l>
				</lg>
				<lg n="16">
					<l n="47" num="16.1"><space unit="char" quantity="2"></space> ‒ eh ! Bon ! Bon ! Bon ! ‒ qu’on nous verse encor !</l>
					<l n="48" num="16.2">Le vin, c’est du sang ! ‒ le cidre, de l’or !</l>
				</lg>
				<lg n="17">
					<l n="49" num="17.1"><space unit="char" quantity="10"></space>« Le troisième, ici,</l>
					<l n="50" num="17.2"><space unit="char" quantity="10"></space>Dessous les charmilles !</l>
					<l n="51" num="17.3"><space unit="char" quantity="10"></space>Fait tout son souci</l>
					<l n="52" num="17.4"><space unit="char" quantity="10"></space>De courir les filles !…</l>
				</lg>
				<lg n="18">
					<l n="53" num="18.1"><space unit="char" quantity="2"></space> ‒ eh ! Bon ! Bon ! Bon ! ‒ qu’on nous verse encor !</l>
					<l n="54" num="18.2">Le vin, c’est du sang ! ‒ le cidre, de l’or !</l>
				</lg>
				<lg n="19">
					<l n="55" num="19.1"><space unit="char" quantity="10"></space>« ‒ reste, beau vainqueur !</l>
					<l n="56" num="19.2"><space unit="char" quantity="10"></space> ‒ moi qui suis leur père,</l>
					<l n="57" num="19.3"><space unit="char" quantity="10"></space>J’ai noyé mon coeur</l>
					<l n="58" num="19.4"><space unit="char" quantity="10"></space>Au fond d’un grand verre ! »</l>
				</lg>
				<lg n="20">
					<l n="59" num="20.1"><space unit="char" quantity="2"></space> ‒ eh ! Bon ! Bon ! Bon ! ‒ qu’on nous verse encor !</l>
					<l n="60" num="20.2">Le vin, c’est du sang ! ‒ le cidre, de l’or !</l>
				</lg>
			</div></body></text></TEI>