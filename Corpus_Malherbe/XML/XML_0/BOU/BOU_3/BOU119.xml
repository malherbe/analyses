<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">DERNIÈRES CHANSONS</title>
				<title type="medium">Une édition électronique</title>
				<author key="BOU">
					<name>
						<forename>Louis</forename>
						<surname>BOUILHET</surname>
					</name>
					<date from="1822" to="1869">1822-1869</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2222 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BOU_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>DERNIÈRES CHANSONS</title>
						<title>POÉSIES POSTHUMES</title>
						<author>Louis Bouilhet</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">M253</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>DERNIÈRES CHANSONS</title>
								<title>POÉSIES POSTHUMES</title>
								<author>Louis Bouilhet</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>M. Levy</publisher>
									<date when="1872">1872</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>ŒUVRES DE LOUIS BOUILHET</title>
						<title>FESTONS ET ASTRAGALES, MELÆNIS, DERNIÈRES CHANSONS</title>
						<author>Louis Bouilhet</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ALPHONSE LEMERRE ÉDITEUR</publisher>
							<date when="1891">1891</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1869">1869</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>Les balises de pagination ont été supprimées.</p>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules en début de vers ont été restituées.</p>
					<p>Les chiffres romains de la numérotation des poèmes ont été rétablis.</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-09-20" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.4.xsd)</change>
				<change when="2021-09-20" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BOU119">
				<head type="number">LII</head>
				<head type="main">PARJURE</head>
				<head type="sub_2">(Traduit d’Ovide, élégie III ; <hi rend="ital">Amours</hi></head>
				<lg n="1">
					<l n="1" num="1.1">Comment croirais-je aux dieux ? Celle</l>
					<l n="2" num="1.2">Qui m’a trompé si souvent,</l>
					<l n="3" num="1.3">Sur ma vie ! Est aussi belle,</l>
					<l n="4" num="1.4">Aussi belle que devant !</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Elle garde en son allure</l>
					<l n="6" num="2.2">La grâce des premiers jours.</l>
					<l n="7" num="2.3">Longue était sa chevelure,</l>
					<l n="8" num="2.4">Ses cheveux sont longs toujours.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Son pied, près du mien, se pose</l>
					<l n="10" num="3.2">Tout petit, comme autrefois.</l>
					<l n="11" num="3.3">Je la connus blanche et rose,</l>
					<l n="12" num="3.4">Blanche et rose je la vois.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">Ses regards ‒ ma bouche en grince ! ‒</l>
					<l n="14" num="4.2">Ont leurs flammes au complet.</l>
					<l n="15" num="4.3">Elle était mince ; elle est mince.</l>
					<l n="16" num="4.4">Elle plaisait ; elle plaît.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1">Ainsi donc les jeunes filles</l>
					<l n="18" num="5.2">Ont droit de trahir les gens !</l>
					<l n="19" num="5.3">Ainsi donc au plus gentilles</l>
					<l n="20" num="5.4">Les dieux sont plus indulgents !</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1">Naguère, en ses accolades,</l>
					<l n="22" num="6.2">Par nos yeux elle a juré.</l>
					<l n="23" num="6.3">Les siens ne sont pas malades ;</l>
					<l n="24" num="6.4">Les miens ont tout enduré.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1">Ô cieux, dont rit la cruelle,</l>
					<l n="26" num="7.2">Dites-nous par quelle loi,</l>
					<l n="27" num="7.3">Quand tout le crime est pour elle,</l>
					<l n="28" num="7.4">Toute la peine est pour moi ?</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1">Ou le mot dieu qu’on redoute</l>
					<l n="30" num="8.2">N’est qu’un mot sonore et creux,</l>
					<l n="31" num="8.3">Ou les dieux, sans aucun doute,</l>
					<l n="32" num="8.4">Sont là-haut très-amoureux.</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1">Quand nous mentons, nous les hommes,</l>
					<l n="34" num="9.2">Phébus frémit dans les airs,</l>
					<l n="35" num="9.3">Et, sur tous tant que nous sommes,</l>
					<l n="36" num="9.4">Fait pleuvoir ses traits amers ;</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1">Neptune enfle la tempête,</l>
					<l n="38" num="10.2">Mars prend son glaive inhumain,</l>
					<l n="39" num="10.3">Minerve a le casque en tête,</l>
					<l n="40" num="10.4">Jupiter, la foudre en main.</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1">Mais les dieux ont peur des belles,</l>
					<l n="42" num="11.2">Des belles aux fronts vainqueurs.</l>
					<l n="43" num="11.3">Elles savent, les rebelles,</l>
					<l n="44" num="11.4">Qu’ils ont des yeux et des coeurs !</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1">Ah !… si j’étais dieu moi-même,</l>
					<l n="46" num="12.2">Je les laisserais mentir.</l>
					<l n="47" num="12.3">Je serais un dieu qu’on aime</l>
					<l n="48" num="12.4">Sans crainte et sans repentir !</l>
				</lg>
				<lg n="13">
					<l n="49" num="13.1"> ‒ toi, cependant, ma charmante,</l>
					<l n="50" num="13.2">Abuse un peu moins des cieux,</l>
					<l n="51" num="13.3">Ou, s’il faut que ta voix mente,</l>
					<l n="52" num="13.4">Pitié pour mes pauvres yeux !</l>
				</lg>
			</div></body></text></TEI>