<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">DERNIÈRES CHANSONS</title>
				<title type="medium">Une édition électronique</title>
				<author key="BOU">
					<name>
						<forename>Louis</forename>
						<surname>BOUILHET</surname>
					</name>
					<date from="1822" to="1869">1822-1869</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2222 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BOU_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>DERNIÈRES CHANSONS</title>
						<title>POÉSIES POSTHUMES</title>
						<author>Louis Bouilhet</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">M253</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>DERNIÈRES CHANSONS</title>
								<title>POÉSIES POSTHUMES</title>
								<author>Louis Bouilhet</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>M. Levy</publisher>
									<date when="1872">1872</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>ŒUVRES DE LOUIS BOUILHET</title>
						<title>FESTONS ET ASTRAGALES, MELÆNIS, DERNIÈRES CHANSONS</title>
						<author>Louis Bouilhet</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ALPHONSE LEMERRE ÉDITEUR</publisher>
							<date when="1891">1891</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1869">1869</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>Les balises de pagination ont été supprimées.</p>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules en début de vers ont été restituées.</p>
					<p>Les chiffres romains de la numérotation des poèmes ont été rétablis.</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-09-20" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.4.xsd)</change>
				<change when="2021-09-20" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BOU117">
				<head type="number">L</head>
				<head type="main">ÉTUDE ANTIQUE</head>
				<lg n="1">
					<l n="1" num="1.1">Il est jeune, il est pâle ‒ et beau comme une fille.</l>
					<l n="2" num="1.2">Ses longs cheveux flottants d’un noeud d’or sont liés,</l>
					<l n="3" num="1.3">La perle orientale à son cothurne brille,</l>
					<l n="4" num="1.4">Il danse ‒ et, secouant sa torche qui petille,</l>
					<l n="5" num="1.5">À l’entour de son cou fait claquer ses colliers.</l>
				</lg>
				<lg n="2">
					<l n="6" num="2.1">Tout frotté de parfums et la tête luisante,</l>
					<l n="7" num="2.2">Il passe en souriant et montre ses bras nus.</l>
					<l n="8" num="2.3">Un lait pur a lavé sa main éblouissante,</l>
					<l n="9" num="2.4">Et de sa joue en fleur la puberté naissante</l>
					<l n="10" num="2.5">Tombe aux pinces de fer du barbier Licinus.</l>
				</lg>
				<lg n="3">
					<l n="11" num="3.1">Près des musiciens dont la flûte soupire,</l>
					<l n="12" num="3.2">De la scène, en rêvant, il écoute le bruit ;</l>
					<l n="13" num="3.3">Ou, laissant sur ses pas les senteurs de la myrrhe,</l>
					<l n="14" num="3.4">Il se mêle au troupeau des femmes en délire,</l>
					<l n="15" num="3.5">Que le fanal des bains attire dans la nuit.</l>
				</lg>
				<lg n="4">
					<l n="16" num="4.1">S’il a de ses sourcils peint le cercle d’ébène,</l>
					<l n="17" num="4.2">Ce n’est pas pour Néëre ou Lesbie aux bras blancs.</l>
					<l n="18" num="4.3">Jamais, jamais sa main chaude de votre haleine,</l>
					<l n="19" num="4.4">Vierges, n’a dénoué la ceinture de laine</l>
					<l n="20" num="4.5">Que la pudeur timide attachait à vos flancs.</l>
				</lg>
				<lg n="5">
					<l n="21" num="5.1">Pour lui, le proconsul épuisera l’empire ;</l>
					<l n="22" num="5.2">Le prêtre comme aux dieux lui donnerait l’encens ;</l>
					<l n="23" num="5.3">Le poëte l’appelle ou Mopsus ou Tytire,</l>
					<l n="24" num="5.4">Et lui glisse en secret, sur ses tables de cire,</l>
					<l n="25" num="5.5">Le distique amoureux, aux dactyles dansants.</l>
				</lg>
				<lg n="6">
					<l n="26" num="6.1">Par la ville, en tous lieux, autour de lui bourdonne</l>
					<l n="27" num="6.2">L’essaim des jeunes gens aux regards enflammés…</l>
					<l n="28" num="6.3">Et le sage lui-même, en s’arrêtant, frissonne</l>
					<l n="29" num="6.4">Quand son ombre chancelle et que son luth résonne</l>
					<l n="30" num="6.5">Au fauve soupirail des bouges enfumés.</l>
				</lg>
			</div></body></text></TEI>