<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">DERNIÈRES CHANSONS</title>
				<title type="medium">Une édition électronique</title>
				<author key="BOU">
					<name>
						<forename>Louis</forename>
						<surname>BOUILHET</surname>
					</name>
					<date from="1822" to="1869">1822-1869</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2222 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BOU_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>DERNIÈRES CHANSONS</title>
						<title>POÉSIES POSTHUMES</title>
						<author>Louis Bouilhet</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">M253</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>DERNIÈRES CHANSONS</title>
								<title>POÉSIES POSTHUMES</title>
								<author>Louis Bouilhet</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>M. Levy</publisher>
									<date when="1872">1872</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>ŒUVRES DE LOUIS BOUILHET</title>
						<title>FESTONS ET ASTRAGALES, MELÆNIS, DERNIÈRES CHANSONS</title>
						<author>Louis Bouilhet</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ALPHONSE LEMERRE ÉDITEUR</publisher>
							<date when="1891">1891</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1869">1869</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>Les balises de pagination ont été supprimées.</p>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules en début de vers ont été restituées.</p>
					<p>Les chiffres romains de la numérotation des poèmes ont été rétablis.</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-09-20" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.4.xsd)</change>
				<change when="2021-09-20" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BOU106">
				<head type="number">XXXIX</head>
				<head type="main">AU GRAND TONNEAU D’HEIDELBERG</head>
				<lg n="1">
					<l n="1" num="1.1">Monstre des temps homériques,</l>
					<l n="2" num="1.2">Dans les nôtres déclassé ;</l>
					<l n="3" num="1.3">Polyphème des barriques,</l>
					<l n="4" num="1.4">Dont l’oeil au ventre placé</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Provoque avec assurance</l>
					<l n="6" num="2.2"> ‒ avortons d’un siècle obtus ‒</l>
					<l n="7" num="2.3">Nos tonneaux qui sont en France,</l>
					<l n="8" num="2.4">Étroits comme nos vertus !</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Foudre géant, qu’à ta forme</l>
					<l n="10" num="3.2">On prendrait pour un vaisseau,</l>
					<l n="11" num="3.3">Du bon vin cercueil énorme</l>
					<l n="12" num="3.4">Dont je possède un morceau.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">Je veux, plein d’un effroi vague,</l>
					<l n="14" num="4.2">Et m’agenouillant trois fois</l>
					<l n="15" num="4.3"> ‒ comme un dévot dans sa bague</l>
					<l n="16" num="4.4">Met un fragment de la croix ‒</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1">Sur un reposoir gothique,</l>
					<l n="18" num="5.2">Dans un coffret de satin,</l>
					<l n="19" num="5.3">Enchâsser ton bois mystique,</l>
					<l n="20" num="5.4">Tiède aussi d’un sang divin !</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1">Heidelberg !… par tes féeries,</l>
					<l n="22" num="6.2">Par tes gnomes familiers,</l>
					<l n="23" num="6.3">Par tes noires brasseries</l>
					<l n="24" num="6.4">Où chantent tes écoliers,</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1">Par ton château, sous les nues</l>
					<l n="26" num="7.2">Debout comme un souvenir,</l>
					<l n="27" num="7.3">Au nom des splendeurs connues</l>
					<l n="28" num="7.4">Et des gloires à venir,</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1">Puisse au loin, joyeux cratère,</l>
					<l n="30" num="8.2">Ton fût, sur tes monts planté,</l>
					<l n="31" num="8.3">Envahir toute la terre</l>
					<l n="32" num="8.4">Sous un flot de volupté !</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1">Et puisse la paix féconde,</l>
					<l n="34" num="9.2">Comme dans un saint anneau,</l>
					<l n="35" num="9.3">Un jour enfermer le monde</l>
					<l n="36" num="9.4">Au cercle de ton tonneau !…</l>
				</lg>
			</div></body></text></TEI>