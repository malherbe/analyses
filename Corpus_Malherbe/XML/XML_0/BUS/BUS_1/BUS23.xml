<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="BUS">
					<name>
						<forename>Alfred</forename>
						<surname>BUSQUET</surname>
					</name>
					<date from="1819" to="1883">1819-1883</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2356 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BUS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÉSIES</title>
						<author>Alfred Busquet</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URI">https ://archive.org/details/poesiespremieres00alfr/mode/2up</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title type="main">POÉSIES</title>
								<author>Alfred Busquet</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE HACHETTE ET Cie</publisher>
									<date when="1884">1884</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1884">1884</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface (AU LECTEUR) n’a pas été reprise.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension(…).</p>
					<p>Les tirets simples précédés et/ou suivis d’une espace ont été remplacés par des tirets demi-cadratin</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-04-17" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-04-17" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BUS23">
				<head type="main">LES BARBERI</head>
				<lg n="1">
					<l n="1" num="1.1">C’est l’heure où le Corso se remplit de voitures,</l>
					<l n="2" num="1.2">L’heure des Barberi, du bruit, des confitures.</l>
					<l n="3" num="1.3">Des gambades, des cris, des capucins, des fous.</l>
					<l n="4" num="1.4">Des nièces de curés avec leurs yeux si doux.</l>
					<l n="5" num="1.5">Des Pandolphes, des Turcs, des danses enragées.</l>
					<l n="6" num="1.6">Des confetti neigeux, des noix et des dragées,</l>
					<l n="7" num="1.7">De ce qui fait la ville un paradis mondain,</l>
					<l n="8" num="1.8">Jusqu’à l’heure où finit le carnaval romain.</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1">Notre ami, jeune, ardent, fort peu mélancolique.</l>
					<l n="10" num="2.2">Au sommet de l’Ara-Cœli, contre un portique.</l>
					<l n="11" num="2.3">Parmi les spectateurs s’est assis pour mieux voir.</l>
					<l n="12" num="2.4">Sur les degrés voisins une belle à l’œil noir.</l>
					<l n="13" num="2.5">Au corsage opulent, au port de reine, et telle</l>
					<l n="14" num="2.6">Qu’en rêvait Sanzio, qu’en sculpta Praxitèle,</l>
					<l n="15" num="2.7">Plus belle que Junon et plus robuste encor,</l>
					<l n="16" num="2.8">Avec ses cheveux hoirs que tient l’épingle d’or.</l>
					<l n="17" num="2.9">Regardait… Notre ami sourit à la voisine,</l>
					<l n="18" num="2.10">Il risque un mot ou deux… Mais la Transtévérine</l>
					<l n="19" num="2.11">Ne répond pas au jeu… Ses regards sont ailleurs,</l>
					<l n="20" num="2.12">Notre galant emploie un tour et des meilleurs,</l>
					<l n="21" num="2.13">De ceux qu’une Romaine en ces jours de folie</l>
					<l n="22" num="2.14">Permet en souriant et qu’il faut qu’on oublie.</l>
					<l n="23" num="2.15">Son corsage entr’ouvert laissait voir un beau sein,</l>
					<l n="24" num="2.16">Il l’emplit de bonbons égarés à dessein.</l>
					<l n="25" num="2.17">Sans pouvoir arracher un sourire à la belle.</l>
					<l n="26" num="2.18">Ni d’un regard hautain, l’insulte moins cruelle.</l>
					<l n="27" num="2.19">Lorsque de confetti le corset est tout plein.</l>
					<l n="28" num="2.20">Poussant le buse avec un geste souverain.</l>
					<l n="29" num="2.21">Elle les fait rouler de son sein jusqu’à terre…</l>
					<l n="30" num="2.22">Le jeu recommença par trois fois sans colère…</l>
					<l n="31" num="2.23">Pour attraper au vol les bonbons épiés.</l>
					<l n="32" num="2.24">Les enfans querelleurs se roulent à ses piés ;</l>
					<l n="33" num="2.25">Mais elle, indifférente au bruit, non combattue.</l>
					<l n="34" num="2.26">Elle a repris soudain ses grands airs de statue.</l>
					<l n="35" num="2.27">Et les yeux dilatés regarde à l’horizon</l>
					<l n="36" num="2.28">Celui dont elle est hère, un jeune et beau garçon</l>
					<l n="37" num="2.29">Qui, les sourcils arqués et le torse en arrière.</l>
					<l n="38" num="2.30">Retient le Barberi par sa rude crinière.</l>
					<l n="39" num="2.31">Et de ses doigts noueux lui meurtrit les naseaux.</l>
					<l n="40" num="2.32">Tout pareil à Castor, fier dompteur de chevaux.</l>
				</lg>
				<closer>
					<placeName>Rome</placeName>.
				</closer>
			</div></body></text></TEI>