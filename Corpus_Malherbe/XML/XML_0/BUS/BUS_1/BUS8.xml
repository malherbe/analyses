<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="BUS">
					<name>
						<forename>Alfred</forename>
						<surname>BUSQUET</surname>
					</name>
					<date from="1819" to="1883">1819-1883</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2356 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BUS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÉSIES</title>
						<author>Alfred Busquet</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URI">https ://archive.org/details/poesiespremieres00alfr/mode/2up</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title type="main">POÉSIES</title>
								<author>Alfred Busquet</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE HACHETTE ET Cie</publisher>
									<date when="1884">1884</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1884">1884</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface (AU LECTEUR) n’a pas été reprise.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension(…).</p>
					<p>Les tirets simples précédés et/ou suivis d’une espace ont été remplacés par des tirets demi-cadratin</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-04-17" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-04-17" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">SUR LES CHEMINS</head><head type="sub_part">IMPRESSIONS DE VOYAGE</head><head type="sub_part">PORTUGAL-ESPAGNE</head><div type="poem" key="BUS8">
					<head type="number">VII</head>
					<head type="main">LES BERMUDES</head>
					<lg n="1">
						<l n="1" num="1.1">— Toi qui, perché sur les Bermudes,</l>
						<l n="2" num="1.2">Comme un vautour sur un rocher.</l>
						<l n="3" num="1.3">Dans les tempêtes les plus rudes</l>
						<l n="4" num="1.4">Veilles au phare, dur nocher.</l>
						<l n="5" num="1.5">Parmi les voiles vagabondes.</l>
						<l n="6" num="1.6">Alcyons perdus sur les ondes.</l>
						<l n="7" num="1.7">Fuyant ton rescif déserté.</l>
						<l n="8" num="1.8">En est-il une par mégarde</l>
						<l n="9" num="1.9">Qui nous rapporte. Dieu le garde !</l>
						<l n="10" num="1.10">Ton drapeau, sainte Liberté !</l>
					</lg>
					<lg n="2">
						<l n="11" num="2.1">— Hier, durant la nuit la plus noire.</l>
						<l n="12" num="2.2">Sur le roc, aiguille du glacier.</l>
						<l n="13" num="2.3">J’étais debout… Le promontoire</l>
						<l n="14" num="2.4">Flambait aux feux de mon brasier.</l>
						<l n="15" num="2.5">Soudain, voici que dans la brume</l>
						<l n="16" num="2.6">Le canon tonne, le flot fume.</l>
						<l n="17" num="2.7">Et je vois, comme dans l’enfer.</l>
						<l n="18" num="2.8">Des hommes se livrer bataille.</l>
						<l n="19" num="2.9">Et des nègres de haute taille</l>
						<l n="20" num="2.10">Qu’on précipitait dans la mer.</l>
					</lg>
					<lg n="3">
						<l n="21" num="3.1">— Vieillard, ta parole est sinistre,</l>
						<l n="22" num="3.2">Mais je pardonne à ton humeur,</l>
						<l n="23" num="3.3">Car ta mémoire est le registre</l>
						<l n="24" num="3.4">Des naufrages et du malheur ;</l>
						<l n="25" num="3.5">Sur ce roc perdu, seul au monde.</l>
						<l n="26" num="3.6">Dans une obscurité profonde.</l>
						<l n="27" num="3.7">Loin de tous, tu vis irrité.</l>
						<l n="28" num="3.8">Mais tu peux, poursuivant ton rêve.</l>
						<l n="29" num="3.9">Quand ton regard vers Dieu se lève.</l>
						<l n="30" num="3.10">Croire au moins à l’égalité.</l>
					</lg>
					<lg n="4">
						<l n="31" num="4.1">— L’Égalité, c’est la patrie.</l>
						<l n="32" num="4.2">C’est la terre, et j’en suis chassé ;</l>
						<l n="33" num="4.3">Tout mon sang se révolte et crie</l>
						<l n="34" num="4.4">Contre mon sort et le passé.</l>
						<l n="35" num="4.5">Les flots aussi bien que la terre</l>
						<l n="36" num="4.6">N’ont pas éclairci le mystère</l>
						<l n="37" num="4.7">Qu’on n’approfondira jamais :</l>
						<l n="38" num="4.8">Abaissez, du haut des Bermudes,</l>
						<l n="39" num="4.9">Vos regards sur les latitudes,</l>
						<l n="40" num="4.10">Que voyez-vous ? Des mâts anglais !</l>
					</lg>
					<lg n="5">
						<l part="I" n="41" num="5.1">— Je te plains. </l>
						<l part="F" n="41" num="5.1">— Cesse de me plaindre,</l>
						<l n="42" num="5.2">Garde ton ingrate pitié.</l>
						<l n="43" num="5.3">Moi, je vous hais tous, sans vous craindre.</l>
						<l n="44" num="5.4">Car je vous ai tous sous mon pié ;</l>
						<l n="45" num="5.5">Esclave de la destinée.</l>
						<l n="46" num="5.6">Ma vie au roc est enchaînée.</l>
						<l n="47" num="5.7">Je suis libre et captif… Je puis</l>
						<l n="48" num="5.8">Faire un soleil avec mon phare</l>
						<l n="49" num="5.9">Ou de ces flots faire un Tartare</l>
						<l n="50" num="5.10">Plus noir que les plus noires nuits.</l>
					</lg>
					<lg n="6">
						<l n="51" num="6.1">Vous qui hantez les solitudes.</l>
						<l n="52" num="6.2">Marins sur la mer égarés.</l>
						<l n="53" num="6.3">Fuyez la côte et les Bermudes,</l>
						<l n="54" num="6.4">Et ces rocs, tigres effarés.</l>
						<l n="55" num="6.5">De moi n’espérez aucune aide.</l>
						<l n="56" num="6.6">N’attendez pas que j’intercède</l>
						<l n="57" num="6.7">Pour vous disputer au trépas.</l>
						<l n="58" num="6.8">Vos sanglots sont mes chants de fête.</l>
						<l n="59" num="6.9">Je pourrais calmer la tempête</l>
						<l n="60" num="6.10">Que certes, je ne voudrais pas !</l>
				</lg>
					<closer>
						<dateline>
							<placeName>Bermudes</placeName>,
							<date when="1859">1859</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>