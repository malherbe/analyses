<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="BUS">
					<name>
						<forename>Alfred</forename>
						<surname>BUSQUET</surname>
					</name>
					<date from="1819" to="1883">1819-1883</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2356 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BUS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÉSIES</title>
						<author>Alfred Busquet</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URI">https ://archive.org/details/poesiespremieres00alfr/mode/2up</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title type="main">POÉSIES</title>
								<author>Alfred Busquet</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE HACHETTE ET Cie</publisher>
									<date when="1884">1884</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1884">1884</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface (AU LECTEUR) n’a pas été reprise.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension(…).</p>
					<p>Les tirets simples précédés et/ou suivis d’une espace ont été remplacés par des tirets demi-cadratin</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-04-17" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-04-17" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">SUR LES CHEMINS</head><head type="sub_part">IMPRESSIONS DE VOYAGE</head><head type="sub_part">PORTUGAL-ESPAGNE</head><div type="poem" key="BUS15">
					<head type="number">XIV</head>
					<head type="main">LA GADITANE</head>
					<lg n="1">
						<l n="1" num="1.1">Sous tes palmiers, Alaméda,</l>
						<l n="2" num="1.2">Comme un jaloux caché dans l’ombre,</l>
						<l n="3" num="1.3">Comme un voleur dans la nuit sombre,</l>
						<l n="4" num="1.4">J’attendais ; — le sort décida !</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">Je t’aperçus, ô Gaditane,</l>
						<l n="6" num="2.2">Brune Andalouse aux regards bleus,</l>
						<l n="7" num="2.3">Ange ou démon, fée ou sultane.</l>
						<l n="8" num="2.4">Viens-tu de l’enfer ou des cieux ?</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">Viens à la nuit, à la nuit brune.</l>
						<l n="10" num="3.2">Sous ces bosquets où dort la lune.</l>
						<l n="11" num="3.3">Et nous verrons, ô mon amour.</l>
						<l n="12" num="3.4">Sur les flots se lever le jour.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">Voici le chemin de la France</l>
						<l n="14" num="4.2">Par delà Porto, Chiclana.</l>
						<l n="15" num="4.3">Ce noir rocher, c’est l’espérance,</l>
						<l n="16" num="4.4">Jésus sur les flots chemina !</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">Dans le pays qui m’a vu naître</l>
						<l n="18" num="5.2">Nous avons aussi des fruits d’or,</l>
						<l n="19" num="5.3">Des jardins dont je suis le maître.</l>
						<l n="20" num="5.4">Des fleurs, et des ruisseaux encor !</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1">Nous avons tout ! Seule tu manques</l>
						<l n="22" num="6.2">A la couronne du pays.</l>
						<l n="23" num="6.3">Parmi les jeunes filles franques</l>
						<l n="24" num="6.4">Tu brillerais comme un rubis !</l>
					</lg>
				</div></body></text></TEI>