<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">MÉLODIES POÉTIQUES</title>
				<title type="medium">Édition électronique</title>
				<author key="FER">
					<name>
						<forename>Albert</forename>
						<surname>FERLAND</surname>
					</name>
					<date from="1872" to="1943">1872-1943</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1014 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">FER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Mélodies poétiques</title>
						<author>Albert Ferland</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">https://www.poesies.net/albertferlandmelodiespoetiques.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Mélodies poétiques</title>
								<author>Albert Ferland</author>
								<idno type="URI">https://books.google.fr/books?id=TBENAAAAYAAJ</idno>
								<imprint>
									<pubPlace>Montréal</pubPlace>
									<publisher>Pierre J. Bédard, Imprimeur-relieur</publisher>
									<date when="1893">1893</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1893">1893</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-08-01" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-08-01" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">SUR LES FIBRES DU CŒUR</head><div type="poem" key="FER26">
					<head type="main">PARLER D’AMOUR</head>
					<lg n="1">
						<l n="1" num="1.1">Ah ! si tu comprenais combien j’aime t’entendre,</l>
						<l n="2" num="1.2">Toi qui sais t’exprimer avec tant de douceur,</l>
						<l n="3" num="1.3">Tu laisserais souvent ta voix suave et tendre</l>
						<l n="4" num="1.4">Me délecter l’oreille et m’émouvoir le cœur !</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">Quand sur ta bouche rose un mot d’amour expire,</l>
						<l n="6" num="2.2">Plus doux que des accords sur la lèvre des flots,</l>
						<l n="7" num="2.3">Je me sens tressaillir comme sous le zéphyre</l>
						<l n="8" num="2.4">La feuille harmonieuse au front des verts bouleaux.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">Oh ! tandis que la nuit prête une ombre imposante</l>
						<l n="10" num="3.2">À l’homme sur la terre, à l’astre dans les cieux,</l>
						<l n="11" num="3.3">Parle-moi donc, sans bruit, comme parle une amante,</l>
						<l n="12" num="3.4">Le sourire à la bouche et l’âme dans les yeux !</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">Si tu veux m’enivrer par un bonheur suprême</l>
						<l n="14" num="4.2">Et me remplir le cœur d’un ineffable émoi,</l>
						<l n="15" num="4.3">Tu n’as qu’à répéter ce mot divin : « Je t’aime, »</l>
						<l n="16" num="4.4">Que tu me dis tout bas, en t’inclinant sur moi.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">Maintenant que tu sais combien j’aime t’entendre,</l>
						<l n="18" num="5.2">Toi qui parles d’amour avec tant de douceur,</l>
						<l n="19" num="5.3">Oh ! laisse donc encor ta voix suave et tendre</l>
						<l n="20" num="5.4">Me délecter l’oreille et m’émouvoir le cœur !</l>
					</lg>
				</div></body></text></TEI>