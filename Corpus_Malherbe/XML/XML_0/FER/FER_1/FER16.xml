<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">MÉLODIES POÉTIQUES</title>
				<title type="medium">Édition électronique</title>
				<author key="FER">
					<name>
						<forename>Albert</forename>
						<surname>FERLAND</surname>
					</name>
					<date from="1872" to="1943">1872-1943</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1014 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">FER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Mélodies poétiques</title>
						<author>Albert Ferland</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">https://www.poesies.net/albertferlandmelodiespoetiques.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Mélodies poétiques</title>
								<author>Albert Ferland</author>
								<idno type="URI">https://books.google.fr/books?id=TBENAAAAYAAJ</idno>
								<imprint>
									<pubPlace>Montréal</pubPlace>
									<publisher>Pierre J. Bédard, Imprimeur-relieur</publisher>
									<date when="1893">1893</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1893">1893</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-08-01" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-08-01" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">MÉLANCOLIES</head><div type="poem" key="FER16">
					<head type="main">RIMES AUTOMNALES</head>
					<lg n="1">
						<l n="1" num="1.1">Adieu, les frais zéphyrs, les aubes ravissantes</l>
						<l n="2" num="1.2">Qui font pâlir l’azur et sourire les eaux !</l>
						<l n="3" num="1.3">Adieu, source limpide aux ondes jaillissantes</l>
						<l n="4" num="1.4">Et doux pleurs du matin perlant sur les roseaux !</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">Hélas ! les jours sereins que l’aurore charmante</l>
						<l n="6" num="2.2">Enfante au bas des cieux derrière l’horizon</l>
						<l n="7" num="2.3">Font place aux vastes pleurs que roule la tourmente,</l>
						<l n="8" num="2.4">A la mauvaise humeur de la triste saison.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">Bientôt l’oiseau frileux quittera nos rivages</l>
						<l n="10" num="3.2">En voyant sous l’autan les bois se dégarnir,</l>
						<l n="11" num="3.3">Les brumes s’entasser sur les rochers sauvages</l>
						<l n="12" num="3.4">Et l’homme méditer et le ciel se ternir.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">Déjà l’automne plane au fond des cieux moroses,</l>
						<l n="14" num="4.2">Où le soleil est pâle ainsi qu’un œil mourant,</l>
						<l n="15" num="4.3">Et le souffle hiémal qui disperse les roses</l>
						<l n="16" num="4.4">Fait sangloter la feuille au front du bois pleurant.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">Une immense tristesse assombrit la nature,</l>
						<l n="18" num="5.2">Qui gémit sur la terre et râle dans les flots ;</l>
						<l n="19" num="5.3">À l’horrible aquilon, qui gronde et les torture,</l>
						<l n="20" num="5.4">Le roc jette un soupir et l’onde des sanglots.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1">Sous les cruels frimas le flanc des monts frissonne,</l>
						<l n="22" num="6.2">Le fleuve va frémir dans les immensités ;</l>
						<l n="23" num="6.3">Tout se lamente et souffre, et le vent ne moissonne</l>
						<l n="24" num="6.4">Que pleurs dans les déserts et cris dans les cités.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1">Plus d’un regard s’attriste au fond des lointains vagues ;</l>
						<l n="26" num="7.2">L’oiseau dans les brouillards sème un lugubre accent ;</l>
						<l n="27" num="7.3">Un funèbre accord naît sous l’écume des vagues ;</l>
						<l n="28" num="7.4">Nul rayon ne reluit dans le ciel pâlissant.</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1">Durant ces jours de deuil, qui meurent dans l’orage,</l>
						<l n="30" num="8.2">L’homme devient plus grave et se plaît à songer ;</l>
						<l n="31" num="8.3">Il va souvent, pensif, rêver sous quelque ombrage,</l>
						<l n="32" num="8.4">Écouter l’aquilon qui vient tout ravager.</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1">La nature l’émeut par sa douleur immense :</l>
						<l n="34" num="9.2">Il ne peut s’empêcher de pleurer, de frémir,</l>
						<l n="35" num="9.3">Car son cœur est sensible et quand elle commence</l>
						<l n="36" num="9.4">A souffrir sous la bise il commence à gémir.</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1">C’est bon qu’il pleure ainsi sous l’aquilon qui tonne,</l>
						<l n="38" num="10.2">Qu’il pense à son passé, qu’il songe à l’avenir,</l>
						<l n="39" num="10.3">Que pour les morts il prie et que le sombre automne</l>
						<l n="40" num="10.4">Lui dise qu’il verra bientôt la mort venir.</l>
					</lg>
				</div></body></text></TEI>