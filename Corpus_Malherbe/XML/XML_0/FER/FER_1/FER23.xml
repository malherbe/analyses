<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">MÉLODIES POÉTIQUES</title>
				<title type="medium">Édition électronique</title>
				<author key="FER">
					<name>
						<forename>Albert</forename>
						<surname>FERLAND</surname>
					</name>
					<date from="1872" to="1943">1872-1943</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1014 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">FER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Mélodies poétiques</title>
						<author>Albert Ferland</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">https://www.poesies.net/albertferlandmelodiespoetiques.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Mélodies poétiques</title>
								<author>Albert Ferland</author>
								<idno type="URI">https://books.google.fr/books?id=TBENAAAAYAAJ</idno>
								<imprint>
									<pubPlace>Montréal</pubPlace>
									<publisher>Pierre J. Bédard, Imprimeur-relieur</publisher>
									<date when="1893">1893</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1893">1893</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-08-01" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-08-01" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">SUR LES FIBRES DU CŒUR</head><div type="poem" key="FER23">
					<head type="main">PRÉFÉRENCE</head>
					<lg n="1">
						<l n="1" num="1.1">Lorsqu’au bas de l’azur, qui semble lui sourire,</l>
						<l n="2" num="1.2">L’aurore resplendit dans un coin d’horizon,</l>
						<l n="3" num="1.3">J’aime à voir l’onde pure, au souffle d’un zéphyre,</l>
						<l n="4" num="1.4">Bercer le doux reflet de son pâle rayon.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">J’aime entendre les chants formidables, sublimes,</l>
						<l n="6" num="2.2">Dont la foudre remplit l’immensité des cieux,</l>
						<l n="7" num="2.3">Lorsque, faisant d’horreur frissonner ses abîmes,</l>
						<l n="8" num="2.4">Son noir courroux s’attelle à son char lumineux.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">J’aime à voir palpiter durant sa valse folle,</l>
						<l n="10" num="3.2">La gente libellule au corset de saphir,</l>
						<l n="11" num="3.3">Et, sous les bois dormants, le soir, la luciole</l>
						<l n="12" num="3.4">Voltiger dans une ombre et soudain resplendir.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">J’aime des ruisselets les gracieux murmures,</l>
						<l n="14" num="4.2">Le blanc duvet des nids cachés dans les vallons,</l>
						<l n="15" num="4.3">L’arome exubérant qu’exhalent les ramures</l>
						<l n="16" num="4.4">Et le fin gazouillis des joyeux oisillons.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">J’aime la blanche opale et la blonde topaze,</l>
						<l n="18" num="5.2">La féerique améthyste et les feux éclatants</l>
						<l n="19" num="5.3">Que mêle un diamant au rayon qui l’embrase</l>
						<l n="20" num="5.4">Et les rouges rubis que l’on dirait sanglants.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1">J’aime les frais baisers de la brise éplorée,</l>
						<l n="22" num="6.2">Qui redit au désert le soupir de l’amant,</l>
						<l n="23" num="6.3">Et le gai papillon dont l’aile diaprée</l>
						<l n="24" num="6.4">Semble avoir l’arc-en-ciel en son azur charmant.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1">J’aime voir une étoile entre les beaux nuages,</l>
						<l n="26" num="7.2">La lune aux flots d’argent derrière un mont lointain ;</l>
						<l n="27" num="7.3">J’aime entendre les eaux chanter sur les rivages</l>
						<l n="28" num="7.4">Et le gai paysan fredonner le matin.</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1">J’aime le jeune enfant qui, paisible, sommeille</l>
						<l n="30" num="8.2">Dans son gentil berceau de dentelles orné,</l>
						<l n="31" num="8.3">Et le sourire errant sur sa lèvre vermeille,</l>
						<l n="32" num="8.4">Ainsi qu’une rougeur sur le front incliné.</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1">J’aime les résédas, la verveine odorante,</l>
						<l n="34" num="9.2">Le chrysanthème d’or des automnes rêveurs</l>
						<l n="35" num="9.3">Et les pâles lilas dont la neige enivrante</l>
						<l n="36" num="9.4">Blanchit le tapis vert des renouveaux en fleurs.</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1">Mais j’aime plus encore, ô brume enchanteresse,</l>
						<l n="38" num="10.2">Ton regard qui sur moi se pose avec douceur,</l>
						<l n="39" num="10.3">Et ta suave voix qui me remplit d’ivresse</l>
						<l n="40" num="10.4">En me charmant l’oreille et m’émouvant le cœur.</l>
					</lg>
				</div></body></text></TEI>