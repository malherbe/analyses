<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">FEMMES RÊVÉES</title>
				<title type="medium">Édition électronique</title>
				<author key="FER">
					<name>
						<forename>Albert</forename>
						<surname>FERLAND</surname>
					</name>
					<date from="1872" to="1943">1872-1943</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>234 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">FER_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Femmes Rêvées</title>
						<author>Albert Ferland</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URI">https://www.gutenberg.org/ebooks/12365</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Femmes Rêvées</title>
								<author>Albert Ferland</author>
								<idno type="URI">https://archive.org/details/femmesrves00ferl</idno>
								<imprint>
									<pubPlace>Montréal</pubPlace>
									<publisher>Chez l’auteur</publisher>
									<date when="1899">1899</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1899">1899</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface n’est pas reprise dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-08-01" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-08-01" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">FEMMES RÊVÉES</head><div type="poem" key="FER47">
					<head type="main">Rêve</head>
					<lg n="1">
						<l n="1" num="1.1">Les cheveux flottants et la gorge nue,</l>
						<l n="2" num="1.2"><space unit="char" quantity="4"></space>Au sein d’un val où j’étais seul,</l>
						<l n="3" num="1.3"><space unit="char" quantity="8"></space>Une femme est venue</l>
					</lg>
					<lg n="2">
						<l n="4" num="2.1">Calme, en traversant l’ombre d’un tilleul,</l>
						<l n="5" num="2.2"><space unit="char" quantity="4"></space>Elle s’embellit d’un sourire</l>
						<l n="6" num="2.3"><space unit="char" quantity="8"></space>Quand elle me vit seul,</l>
					</lg>
					<lg n="3">
						<l n="7" num="3.1">Et, parfumant l’air d’une odeur de myrrhe,</l>
						<l n="8" num="3.2"><space unit="char" quantity="4"></space>Elle vint s’asseoir près de moi</l>
						<l n="9" num="3.3"><space unit="char" quantity="8"></space>Ne cessant de sourire.</l>
					</lg>
					<lg n="4">
						<l n="10" num="4.1">Puis elle m’offrit, vibrante d’émoi,</l>
						<l n="11" num="4.2"><space unit="char" quantity="4"></space>Le baiser de sa lèvre rose,</l>
						<l n="12" num="4.3"><space unit="char" quantity="8"></space>En s’inclinant sur moi,</l>
					</lg>
					<lg n="5">
						<l n="13" num="5.1">Les cheveux flottants, la bouche mi-close.</l>
					</lg>
				</div></body></text></TEI>