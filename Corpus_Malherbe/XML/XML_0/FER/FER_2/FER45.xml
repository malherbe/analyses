<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">FEMMES RÊVÉES</title>
				<title type="medium">Édition électronique</title>
				<author key="FER">
					<name>
						<forename>Albert</forename>
						<surname>FERLAND</surname>
					</name>
					<date from="1872" to="1943">1872-1943</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>234 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">FER_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Femmes Rêvées</title>
						<author>Albert Ferland</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URI">https://www.gutenberg.org/ebooks/12365</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Femmes Rêvées</title>
								<author>Albert Ferland</author>
								<idno type="URI">https://archive.org/details/femmesrves00ferl</idno>
								<imprint>
									<pubPlace>Montréal</pubPlace>
									<publisher>Chez l’auteur</publisher>
									<date when="1899">1899</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1899">1899</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface n’est pas reprise dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-08-01" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-08-01" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CHANTS D’AMOUR</head><head type="sub_part">Tirés du « Cantique des Cantiques »</head><div type="poem" key="FER45">
					<head type="number">II</head>
					<head type="main">Beauté des Époux</head>
					<lg n="1">
						<head type="main">L’ÉPOUX</head>
						<l n="1" num="1.1">Vois donc, ma sœur, épouse, ô fontaine scellée,</l>
						<l n="2" num="1.2">Comme ton corps est svelte et d’aspect gracieux !</l>
					</lg>
					<lg n="2">
						<head type="main">L’ÉPOUSE</head>
						<l n="3" num="2.1">Vois donc, ô mon époux, ô lis de la vallée,</l>
						<l n="4" num="2.2">Comme en toi toute chose est parfaite à mes yeux !</l>
					</lg>
					<lg n="3">
						<head type="main">L’ÉPOUX</head>
						<l n="5" num="3.1">Tes cheveux sont pareils à des troupeaux de chèvres</l>
						<l n="6" num="3.2">Poursuivant sur les monts leurs chemins coutumiers.</l>
					</lg>
					<lg n="4">
						<head type="main">L’ÉPOUSE</head>
						<l n="7" num="4.1">La myrrhe, ô bien-aimé, distille de tes lèvres,</l>
						<l n="8" num="4.2">Tes cheveux sont pareils aux pousses des palmiers.</l>
					</lg>
					<lg n="5">
						<head type="main">L’ÉPOUX</head>
						<l n="9" num="5.1">Tes mains qui des couleurs de l’aurore sont teintes</l>
						<l n="10" num="5.2">Semblent deux papillons autour de toi volant.</l>
					</lg>
					<lg n="6">
						<head type="main">L’ÉPOUSE</head>
						<l n="11" num="6.1">Tes mains, faites au tour, sont pleines d’hyacinthes,</l>
						<l n="12" num="6.2">Et ta tête superbe est un or excellent.</l>
					</lg>
					<lg n="7">
						<head type="main">L’ÉPOUX</head>
						<l n="13" num="7.1">Tes yeux dont le regard a blessé ma prunelle</l>
						<l n="14" num="7.2">Sont purs comme les flots des vasques d’Hésébon.</l>
					</lg>
					<lg n="8">
						<head type="main">L’ÉPOUSE</head>
						<l n="15" num="8.1">Tes yeux à qui mon corps chastement se révèle</l>
						<l n="16" num="8.2">Sont clairs comme les eaux des puits de Salomon.</l>
					</lg>
				</div></body></text></TEI>