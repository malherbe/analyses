<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">FEMMES RÊVÉES</title>
				<title type="medium">Édition électronique</title>
				<author key="FER">
					<name>
						<forename>Albert</forename>
						<surname>FERLAND</surname>
					</name>
					<date from="1872" to="1943">1872-1943</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>234 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">FER_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Femmes Rêvées</title>
						<author>Albert Ferland</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URI">https://www.gutenberg.org/ebooks/12365</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Femmes Rêvées</title>
								<author>Albert Ferland</author>
								<idno type="URI">https://archive.org/details/femmesrves00ferl</idno>
								<imprint>
									<pubPlace>Montréal</pubPlace>
									<publisher>Chez l’auteur</publisher>
									<date when="1899">1899</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1899">1899</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface n’est pas reprise dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-08-01" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-08-01" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CHANTS D’AMOUR</head><head type="sub_part">Tirés du « Cantique des Cantiques »</head><div type="poem" key="FER44">
					<head type="number">I</head>
					<head type="main">Dis-nous, ô jeune femme</head>
					<lg n="1">
						<head type="main">LES FILLES DE JÉRUSALEM</head>
						<l n="1" num="1.1"><space unit="char" quantity="12"></space>Dis-nous, ô jeune femme</l>
						<l n="2" num="1.2"><space unit="char" quantity="12"></space>Dis-nous ton bien-aimé</l>
						<l n="3" num="1.3"><space unit="char" quantity="10"></space>L’aimé qui, d’un pur cinname,</l>
						<l n="4" num="1.4"><space unit="char" quantity="8"></space>Ton lit doit être parfumé.</l>
					</lg>
					<lg n="2">
						<head type="main">L’ÉPOUSE</head>
						<l n="5" num="2.1">Celui que mon cœur aime est un bouquet de myrrhe ;</l>
						<l n="6" num="2.2">Son baiser dont l’ardeur est celle du midi</l>
						<l n="7" num="2.3">Est non moins odorant que le nard de Palmyre</l>
						<l n="8" num="2.4">Et meilleur que le sang des vignes d’Eugaddi.</l>
					</lg>
					<lg n="3">
						<head type="main">LES FILLES DE JÉRUSALEM</head>
						<l n="9" num="3.1"><space unit="char" quantity="12"></space>Dis-nous, ô jeune femme</l>
						<l n="10" num="3.2"><space unit="char" quantity="12"></space>Dis-nous ton bien-aimé</l>
						<l n="11" num="3.3"><space unit="char" quantity="10"></space>L’aimé qui, d’un pur cinname,</l>
						<l n="12" num="3.4"><space unit="char" quantity="8"></space>Ton lit doit être parfumé.</l>
					</lg>
					<lg n="4">
						<head type="main">L’ÉPOUSE</head>
						<l n="13" num="4.1">Que ne m’est-il donné d’être à son ombre assise !</l>
						<l n="14" num="4.2">Son aspect est pareil à celui de l’Hermon ;</l>
						<l n="15" num="4.3">Des filles de Sion plus d’une en est éprise ;</l>
						<l n="16" num="4.4">C’est une huile épandue et rare que son nom.</l>
					</lg>
					<lg n="5">
						<head type="main">LES FILLES DE JÉRUSALEM</head>
						<l n="17" num="5.1"><space unit="char" quantity="12"></space>Dis-nous, ô jeune femme</l>
						<l n="18" num="5.2"><space unit="char" quantity="12"></space>Dis-nous ton bien-aimé</l>
						<l n="19" num="5.3"><space unit="char" quantity="10"></space>L’aimé qui, d’un pur cinname,</l>
						<l n="20" num="5.4"><space unit="char" quantity="8"></space>Ton lit doit être parfumé.</l>
					</lg>
					<lg n="6">
						<head type="main">L’ÉPOUSE</head>
						<l n="21" num="6.1">Admise en ses celliers, j’inclinerai l’amphore,</l>
						<l n="22" num="6.2">Et, vous distribuant le nectar des festins,</l>
						<l n="23" num="6.3">Je me plairai, joyeuse, à vous redire encore</l>
						<l n="24" num="6.4">Que son baiser vainqueur est meilleur que les vins.</l>
					</lg>
					<lg n="7">
						<head type="main">LES FILLES DE JÉRUSALEM</head>
						<l n="25" num="7.1"><space unit="char" quantity="12"></space>Dis-nous, ô jeune femme</l>
						<l n="26" num="7.2"><space unit="char" quantity="12"></space>Dis-nous ton bien-aimé</l>
						<l n="27" num="7.3"><space unit="char" quantity="10"></space>L’aimé qui, d’un pur cinname,</l>
						<l n="28" num="7.4"><space unit="char" quantity="8"></space>Ton lit doit être parfumé.</l>
					</lg>
					<lg n="8">
						<head type="main">L’ÉPOUSE</head>
						<l n="29" num="8.1">Je suis brune et pourtant mon roi m’a comparée</l>
						<l n="30" num="8.2">A ses coursiers traînant le char de Pharaon ;</l>
						<l n="31" num="8.3">Je suis belle à ses yeux, quoique décolorée,</l>
						<l n="32" num="8.4">Plus que les pavillons du sage Salomon.</l>
					</lg>
					<lg n="9">
						<head type="main">LES FILLES DE JÉRUSALEM</head>
						<l n="33" num="9.1"><space unit="char" quantity="12"></space>Dis-nous, ô jeune femme</l>
						<l n="34" num="9.2"><space unit="char" quantity="12"></space>Dis-nous ton bien-aimé</l>
						<l n="35" num="9.3"><space unit="char" quantity="10"></space>L’aimé qui, d’un pur cinname,</l>
						<l n="36" num="9.4"><space unit="char" quantity="8"></space>Ton lit doit être parfumé.</l>
					</lg>
					<lg n="10">
						<head type="main">L’ÉPOUSE</head>
						<l n="37" num="10.1">Ne considérez plus que je me sais hâlée,</l>
						<l n="38" num="10.2">Dans les flots lumineux qui baignaient les sentiers,</l>
						<l n="39" num="10.3">Lorsqu’en mai je m’en suis septante fois allée</l>
						<l n="40" num="10.4">Garder ma vigne en fleur au jardin des noyers.</l>
					</lg>
					<lg n="11">
						<head type="main">LES FILLES DE JÉRUSALEM</head>
						<l n="41" num="11.1"><space unit="char" quantity="12"></space>Dis-nous, ô jeune femme</l>
						<l n="42" num="11.2"><space unit="char" quantity="12"></space>Dis-nous ton bien-aimé</l>
						<l n="43" num="11.3"><space unit="char" quantity="10"></space>L’aimé qui, d’un pur cinname,</l>
						<l n="44" num="11.4"><space unit="char" quantity="8"></space>Ton lit doit être parfumé.</l>
					</lg>
					<lg n="12">
						<head type="main">L’ÉPOUSE</head>
						<l n="45" num="12.1">Celui que mon cœur aime est un bouquet de myrrhe ;</l>
						<l n="46" num="12.2">Son baiser dont l’ardeur est celle du midi</l>
						<l n="47" num="12.3">Est non moins odorant que le nard de Palmyre</l>
						<l n="48" num="12.4">Et meilleur que le sang des vignes d’Engaddi.</l>
					</lg>
				</div></body></text></TEI>