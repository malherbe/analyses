<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">HOMONYMES</head><div type="poem" key="BRT200">
				<head type="number">XXV</head>
				<lg n="1">
					<l n="1" num="1.1">L’<subst type="completion" hand="ML" reason="analysis"><del>...</del><add rend="hidden">air</add></subst> libre s’imprégnait des senteurs de l’aurore ;</l>
					<l n="2" num="1.2">Dans son <subst type="completion" hand="ML" reason="analysis"><del>....</del><add rend="hidden">aire</add></subst> en lieux hauts l’oiseau dormait encore ;</l>
					<l n="3" num="1.3">Plus que lui matineux, les moissonneurs hâlés</l>
					<l n="4" num="1.4">Préparaient à grand bruit une <subst type="completion" hand="ML" reason="analysis"><del>....</del><add rend="hidden">aire</add></subst> pour les blés.</l>
					<l n="5" num="1.5">L’<subst type="completion" hand="ML" reason="analysis"><del>....</del><add rend="hidden">aire</add></subst> enfin des bons vents favorisait leur tâche ;</l>
					<l n="6" num="1.6">La récolte exigeait un travail sans relâche.</l>
					<l n="7" num="1.7">C’était, pour l’opulent et le déshérité,</l>
					<l n="8" num="1.8">Une <subst type="completion" hand="ML" reason="analysis"><del>...</del><add rend="hidden">ère</add></subst> d’abondance et de prospérité.</l>
					<l n="9" num="1.9">Aussi chacun aux champs arrivait-il grande <subst type="completion" hand="ML" reason="analysis"><del>....</del><add rend="hidden">erre</add></subst></l>
					<l n="10" num="1.10">Le maître et ses valets, le glaneur pauvre <subst type="completion" hand="ML" reason="analysis"><del>....</del><add rend="hidden">hère</add></subst> ;</l>
					<l n="11" num="1.11">Le moine laboureur dans sa <subst type="completion" hand="ML" reason="analysis"><del>.....</del><add rend="hidden">haire</add></subst> de crin ;</l>
					<l n="12" num="1.12">L’adolescent folâtre et le vieillard chagrin.</l>
					<l n="13" num="1.13">Et, durant le repas, à l’ombre d’un gros hêtre,</l>
					<l n="14" num="1.14">Les filles entonnaient en chœur un <subst type="completion" hand="ML" reason="analysis"><del>...</del><add rend="hidden">air</add></subst> champêtre ;</l>
					<l n="15" num="1.15">Tandis que l’on voyait en pieux à <hi rend="ital">parte</hi>,</l>
					<l n="16" num="1.16">L’<subst type="completion" hand="ML" reason="analysis"><del>...</del><add rend="hidden">air</add></subst> grave et récitant son <hi rend="ital">benedicite</hi>,</l>
					<l n="17" num="1.17">Un timide aspirant du prochain séminaire,</l>
					<l n="18" num="1.18">Desservant à venir du diocèse d’<subst type="completion" hand="ML" reason="analysis"><del>....</del><add rend="hidden">aire</add></subst> ;</l>
					<l n="19" num="1.19">Et qu’en roulant les <subst type="completion" hand="ML" reason="analysis"><del>.</del><add rend="hidden">èr</add></subst>, un verbeux érudit,</l>
					<l n="20" num="1.20">Tout seul à s’écouter, n’était point contredit.</l>
				</lg>
					<closer>
						<note type="footnote" id="none">▪ Air, aire, aire, aire, ère, erre, hère, haire, air, air, aire, r.</note>
					</closer>
			</div></body></text></TEI>