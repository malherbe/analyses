<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PROVERBES</head><div type="poem" key="BRT95">
				<head type="number">XCV</head>
				<lg n="1">
					<l n="1" num="1.1"><space unit="char" quantity="8"></space>J’aime les champêtres effluves :</l>
					<l n="2" num="1.2"><space unit="char" quantity="8"></space>L’odeur du raisin dans les cuves,</l>
					<l n="3" num="1.3">Des blés mûrs au sillon, des pommes au pressoir.</l>
					<l n="4" num="1.4"><space unit="char" quantity="8"></space>J’aime, quand bouillonne la sève,</l>
					<l n="5" num="1.5"><space unit="char" quantity="8"></space>Les senteurs que la brise enlève</l>
					<l n="6" num="1.6">À la corolle d’or, végétal encensoir !</l>
					<l n="7" num="1.7"><space unit="char" quantity="8"></space>Des bois, des landes et des chaumes,</l>
					<l n="8" num="1.8"><space unit="char" quantity="8"></space>Il se dégage tant d’arômes</l>
					<l n="9" num="1.9">Que l’on n’y peut songer aux tristes lendemains…</l>
					<l n="10" num="1.10"><space unit="char" quantity="8"></space>Et l’esprit a le vol plus libre,</l>
					<l n="11" num="1.11"><space unit="char" quantity="8"></space>Et le sang bout dans chaque fibre</l>
					<l n="12" num="1.12">Quand de foin, sur les chars, s’embaument les chemins.</l>
					<l n="13" num="1.13"><space unit="char" quantity="8"></space>L’hiver, dans l’étable prochaine,</l>
					<l n="14" num="1.14"><space unit="char" quantity="8"></space>Au râtelier d’aune ou de chêne,</l>
					<l n="15" num="1.15">Ces parfums des prés verts demeurent attachés ;</l>
					<l n="16" num="1.16"><space unit="char" quantity="8"></space>Et les grands bœufs aux fortes hanches,</l>
					<l n="17" num="1.17"><space unit="char" quantity="8"></space>Au rude poil, aux cornes blanches,</l>
					<l n="18" num="1.18">Ruminent indolents, vers la crèche penchés.</l>
					<l n="19" num="1.19"><space unit="char" quantity="8"></space>À côté, le cheval s’ébroue ;</l>
					<l n="20" num="1.20"><space unit="char" quantity="8"></space>Plus loin, le dindon fait la roue ;</l>
					<l n="21" num="1.21">Au bât prêtant leur dos, les ânes vont partir.</l>
					<l n="22" num="1.22"><space unit="char" quantity="8"></space>Dans un coin, les lapins se grattent.</l>
					<l n="23" num="1.23"><space unit="char" quantity="8"></space>Mais au dehors les coqs se battent,</l>
					<l n="24" num="1.24">Et de leurs cris aigus la cour va retentir.</l>
				</lg>
					<closer>
						<note type="footnote" id="none">▪ Quand il n’y a plus de foin au râtelier, les ânes se battent.</note>
					</closer>
			</div></body></text></TEI>