<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">MOTS CARRÉS</head><div type="poem" key="BRT244">
				<head type="number">XIX</head>
				<lg n="1">
					<l n="1" num="1.1">Les dieux s’en vont : Jupin, <subst hand="ML" reason="analysis" type="completion"><del>.....</del><add rend="hidden">Vénus</add></subst>, Junon, Mercure</l>
					<l n="2" num="1.2">Ont achevé leur temps, même en littérature !</l>
					<l n="3" num="1.3"><space unit="char" quantity="12"></space>Mais on lira toujours</l>
					<l n="4" num="1.4">Molière le réel avec ses Cydalise,</l>
					<l n="5" num="1.5">Avec ses Harpagon, ses Agnès, ses <subst hand="ML" reason="analysis" type="completion"><del>.....</del><add rend="hidden">Élise</add></subst></l>
					<l n="6" num="1.6"><space unit="char" quantity="12"></space>Et ses ruses d’amours.</l>
					<l n="7" num="1.7">Aux générations qui vont suivre les nôtres,</l>
					<l n="8" num="1.8"><hi rend="ital">La Guerre du <subst hand="ML" reason="analysis" type="completion"><del>.....</del><add rend="hidden">Nizam</add></subst></hi>, <hi rend="ital">la Floride</hi> et bien d’autres</l>
					<l n="9" num="1.9"><space unit="char" quantity="12"></space>Parleront de Méry ;</l>
					<l n="10" num="1.10">Et longtemps il sera d’habitude et d’<subst hand="ML" reason="analysis" type="completion"><del>.....</del><add rend="hidden">usage</add></subst></l>
					<l n="11" num="1.11">D’avoir sur ses rayons le saisissant ouvrage</l>
					<l n="12" num="1.12"><space unit="char" quantity="12"></space>De Gabriel Ferry.</l>
					<l n="13" num="1.13">Ainsi les bous auteurs, aux champs de la pensée,</l>
					<l n="14" num="1.14">Poursuivent pas à pas leur marche cadencée</l>
					<l n="15" num="1.15"><space unit="char" quantity="12"></space>Pour y <subst hand="ML" reason="analysis" type="completion"><del>.....</del><add rend="hidden">semer</add></subst> le grain.</l>
					<l n="16" num="1.16">Et la postérité dans leurs sillons moissonne,</l>
					<l n="17" num="1.17">Gravant avec honneur les traits de leur personne</l>
					<l n="18" num="1.18"><space unit="char" quantity="12"></space>Sur ses tables d’airain.</l>
				</lg>
					<closer>
						<note id="none" type="footnote">▪ VÉNUS<lb></lb>▪ ÉLISE<lb></lb>▪ NIZAM<lb></lb>▪ USAGE<lb></lb>▪ SEMER</note>
					</closer>
			</div></body></text></TEI>