<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">NOUS TOUS</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4011 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Nous tous</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/banvillenoustous.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Nous Tous.</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>G. Charpentier et Cie, éditeurs</publisher>
							<date when="1884">1884</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1884">1883-1884</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN286">
				<head type="number">XCIV</head>
				<head type="main">L’Été de Paris</head>
				<lg n="1">
					<l n="1" num="1.1">Nous dont il a pris les âmes,</l>
					<l n="2" num="1.2">Adorons encor, l’été,</l>
					<l n="3" num="1.3">Paris plein d’ombre et de flammes,</l>
					<l n="4" num="1.4">Jouvence et charmant Léthé !</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Ah ! dans cette heureuse ville,</l>
					<l n="6" num="2.2">Quand les gêneurs sont partis</l>
					<l n="7" num="2.3">Formant une longue file,</l>
					<l n="8" num="2.4">On trouve de bons partis.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Alors, dans les parcs superbes,</l>
					<l n="10" num="3.2">Un tas de fleurs ardemment</l>
					<l n="11" num="3.3">Jaillissent parmi les herbes,</l>
					<l n="12" num="3.4">Comme un éblouissement.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">C’est comme une immense orgie</l>
					<l n="14" num="4.2">Où brillent sous le ciel pur</l>
					<l n="15" num="4.3">La pourpre de feu rougie,</l>
					<l n="16" num="4.4">L’or, l’écarlate et l’azur ;</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1">Et notre Éden est moins triste</l>
					<l n="18" num="5.2">Que la grève d’Étretat,</l>
					<l n="19" num="5.3">Car Paris est le fleuriste</l>
					<l n="20" num="5.4">Qui sait le mieux notre état.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1">Avec ses beaux équipages</l>
					<l n="22" num="6.2">Et ses reines, dont les cieux</l>
					<l n="23" num="6.3">Admirent les fiers tapages,</l>
					<l n="24" num="6.4">Le Bois est délicieux.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1">Zéphyr ! c’est là que tu bouges,</l>
					<l n="26" num="7.2">Et qu’en tes abris nouveaux</l>
					<l n="27" num="7.3">On voit des rosettes rouges</l>
					<l n="28" num="7.4">Aux oreilles des chevaux.</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1">Et le soir, quand se déploie</l>
					<l n="30" num="8.2">Le peuple doux et bavard</l>
					<l n="31" num="8.3">Sous le gaz fou, quelle joie</l>
					<l n="32" num="8.4">D’être sur le boulevard !</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1">Tandis que, sous des rubriques,</l>
					<l n="34" num="9.2">Les absents mangent, par ton,</l>
					<l n="35" num="9.3">Des tourne-dos chimériques</l>
					<l n="36" num="9.4">Et des truites de carton ;</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1">Tandis qu’en la chaude steppe</l>
					<l n="38" num="10.2">Ils s’égarent, sans appui,</l>
					<l n="39" num="10.3">Dans quelque vulgaire Dieppe</l>
					<l n="40" num="10.4">Ou quelque sinistre Puy ;</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1">Sans que jamais on nous triche,</l>
					<l n="42" num="11.2">Avec un bon compagnon</l>
					<l n="43" num="11.3">Nous dînons au café Riche,</l>
					<l n="44" num="11.4">Ou bien à l’air, chez Bignon ;</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1">Puis, tandis que dans les gares</l>
					<l n="46" num="12.2">Ils suivent un flot confus,</l>
					<l n="47" num="12.3">Nous fumons de bons cigares</l>
					<l n="48" num="12.4">Sous les grands arbres touffus.</l>
				</lg>
				<lg n="13">
					<l n="49" num="13.1">Tous ces gens qui sur l’asphalte</l>
					<l n="50" num="13.2">Passent, et dont l’œil sourit,</l>
					<l n="51" num="13.3">Ont le bonheur qui s’exalte</l>
					<l n="52" num="13.4">Sous le souffle de l’esprit.</l>
				</lg>
				<lg n="14">
					<l n="53" num="14.1">Pratiques, exempts de poses,</l>
					<l n="54" num="14.2">Ayant maint tour dans leur sac,</l>
					<l n="55" num="14.3">Ils savent le prix des choses</l>
					<l n="56" num="14.4">Et la langue de Balzac.</l>
				</lg>
				<lg n="15">
					<l n="57" num="15.1">Sur ce bitume où vous n’êtes</l>
					<l n="58" num="15.2">Plus, ô voyageurs marris,</l>
					<l n="59" num="15.3">De belles dames honnêtes</l>
					<l n="60" num="15.4">Passent avec leurs maris ;</l>
				</lg>
				<lg n="16">
					<l n="61" num="16.1">Et sous nos yeux bénévoles,</l>
					<l n="62" num="16.2">Qui les suivent à loisir,</l>
					<l n="63" num="16.3">D’autres aussi, plus frivoles,</l>
					<l n="64" num="16.4">Que l’on voit avec plaisir.</l>
				</lg>
				<lg n="17">
					<l n="65" num="17.1">Emma, dont la voix est douce</l>
					<l n="66" num="17.2">Comme un soupir de hautbois,</l>
					<l n="67" num="17.3">Avec sa cousine rousse</l>
					<l n="68" num="17.4">Marche, un éventail aux doigts.</l>
				</lg>
				<lg n="18">
					<l n="69" num="18.1">Claire, que la haute gomme</l>
					<l n="70" num="18.2">Chante, suit son hospodar,</l>
					<l n="71" num="18.3">En robe écarlate comme</l>
					<l n="72" num="18.4">La vareuse de Nadar.</l>
				</lg>
				<lg n="19">
					<l n="73" num="19.1">Rosette, qui n’est pas sage,</l>
					<l n="74" num="19.2">(On l’a célé vainement,)</l>
					<l n="75" num="19.3">Erre devant le passage</l>
					<l n="76" num="19.4">Où loge <hi rend="ital">L’Événement</hi>.</l>
				</lg>
				<lg n="20">
					<l n="77" num="20.1">Lucile, que chacun aime,</l>
					<l n="78" num="20.2">Et qui boude à tort Tony,</l>
					<l n="79" num="20.3">Prend avec lui tout de même</l>
					<l n="80" num="20.4">Des glaces chez Tortoni.</l>
				</lg>
				<lg n="21">
					<l n="81" num="21.1">Et Jeanne, qui hait la prose,</l>
					<l n="82" num="21.2">Met, effet qui nous est cher ! —</l>
					<l n="83" num="21.3">Sur sa chair couleur de rose</l>
					<l n="84" num="21.4">Des roses couleur de chair.</l>
				</lg>
				<lg n="22">
					<l n="85" num="22.1">Cependant, sur les falaises,</l>
					<l n="86" num="22.2">Nos fuyards murmurent : Miss !</l>
					<l n="87" num="22.3">A l’oreille des Anglaises</l>
					<l n="88" num="22.4">Bien plus sveltes qu’Artémis,</l>
				</lg>
				<lg n="23">
					<l n="89" num="23.1">Et souffletés par les vagues,</l>
					<l n="90" num="23.2">Ils promènent leurs vestons</l>
					<l n="91" num="23.3">Sur des Himalayas vagues.</l>
					<l n="92" num="23.4">Ne les suivons pas. Restons !</l>
				</lg>
				<lg n="24">
					<l n="93" num="24.1">Car, amis, sur leurs grimaces</l>
					<l n="94" num="24.2">Pour que vous vous réglassiez,</l>
					<l n="95" num="24.3">Il vous faudrait voir des masses</l>
					<l n="96" num="24.4">De torrents et de glaciers,</l>
				</lg>
				<lg n="25">
					<l n="97" num="25.1">Et, moins gais que Cléopâtre</l>
					<l n="98" num="25.2">Se livrant à ses aspics,</l>
					<l n="99" num="25.3">Sous la conduite d’un pâtre</l>
					<l n="100" num="25.4">Escalader d’affreux pics !</l>
				</lg>
				<lg n="26">
					<l n="101" num="26.1">Ah ! parmi les machinistes</l>
					<l n="102" num="26.2">De l’avalanche et du vent,</l>
					<l n="103" num="26.3">Que les excursionnistes</l>
					<l n="104" num="26.4">Aillent toujours en avant !</l>
				</lg>
				<lg n="27">
					<l n="105" num="27.1">Que l’oracle d’Épidaure,</l>
					<l n="106" num="27.2">Transis, mouillés jusqu’aux os,</l>
					<l n="107" num="27.3">Les mène au chaste Mont-Dore</l>
					<l n="108" num="27.4">Boire de cruelles eaux !</l>
				</lg>
				<lg n="28">
					<l n="109" num="28.1">Qu’ils aillent aux bords farouches</l>
					<l n="110" num="28.2">Que mord l’Océan amer,</l>
					<l n="111" num="28.3">Pour ressembler à des mouches</l>
					<l n="112" num="28.4">Au bord de la vaste mer !</l>
				</lg>
				<lg n="29">
					<l n="113" num="29.1">Qu’ils s’égarent sous les brumes</l>
					<l n="114" num="29.2">Et dans les sombres halliers,</l>
					<l n="115" num="29.3">En laissant toutes leurs plumes</l>
					<l n="116" num="29.4">Aux griffes des hôteliers !</l>
				</lg>
				<lg n="30">
					<l n="117" num="30.1">Mais nous, âmes casanières,</l>
					<l n="118" num="30.2">Restons, gagnons nos paris,</l>
					<l n="119" num="30.3">Puisque nous trouvons Asnières</l>
					<l n="120" num="30.4">Encor trop loin de Paris !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1883">12 août 1883.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>