<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">NOUS TOUS</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4011 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Nous tous</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/banvillenoustous.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Nous Tous.</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>G. Charpentier et Cie, éditeurs</publisher>
							<date when="1884">1884</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1884">1883-1884</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN280">
				<head type="number">LXXXVIII</head>
				<head type="main">La Fourmi et la cigale</head>
				<lg n="1">
					<l n="1" num="1.1">Laure, belle entre les grasses,</l>
					<l n="2" num="1.2">Qui porte avec mille grâces</l>
					<l n="3" num="1.3"><space quantity="8" unit="char"></space>Les diamants,</l>
					<l n="4" num="1.4">Sans jamais en être vaine,</l>
					<l n="5" num="1.5">Trouve qu’elle a trop de peine</l>
					<l n="6" num="1.6"><space quantity="8" unit="char"></space>Et trop d’amants.</l>
				</lg>
				<lg n="2">
					<l n="7" num="2.1">Elle dit : Je me fatigue</l>
					<l n="8" num="2.2">De tout ce luxe prodigue,</l>
					<l n="9" num="2.3"><space quantity="8" unit="char"></space>De tous ces ors.</l>
					<l n="10" num="2.4">Tout cela, c’est trop d’affaire,</l>
					<l n="11" num="2.5">Et je ne sais plus que faire</l>
					<l n="12" num="2.6"><space quantity="8" unit="char"></space>De mes trésors.</l>
				</lg>
				<lg n="3">
					<l n="13" num="3.1">Chacun a la fantaisie</l>
					<l n="14" num="3.2">De goûter à l’ambroisie</l>
					<l n="15" num="3.3"><space quantity="8" unit="char"></space>De mes baisers.</l>
					<l n="16" num="3.4">Ils arrivent des deux pôles,</l>
					<l n="17" num="3.5">Et les lys de mes épaules</l>
					<l n="18" num="3.6"><space quantity="8" unit="char"></space>En sont usés.</l>
				</lg>
				<lg n="4">
					<l n="19" num="4.1">Ils me disent trop de phrases.</l>
					<l n="20" num="4.2">D’ailleurs, j’ai trop de topazes</l>
					<l n="21" num="4.3"><space quantity="8" unit="char"></space>Et de rubis.</l>
					<l n="22" num="4.4">Faut-il donc les mettre en poudre,</l>
					<l n="23" num="4.5">Ou, plus simplement, les coudre</l>
					<l n="24" num="4.6"><space quantity="8" unit="char"></space>Sur mes habits ?</l>
				</lg>
				<lg n="5">
					<l n="25" num="5.1">Telle se désole, en prose,</l>
					<l n="26" num="5.2">Laure, pareille à la rose</l>
					<l n="27" num="5.3"><space quantity="8" unit="char"></space>Qui resplendit.</l>
					<l n="28" num="5.4">Elle se moque d’un prince</l>
					<l n="29" num="5.5">Et d’un banquier. Mais la mince</l>
					<l n="30" num="5.6"><space quantity="8" unit="char"></space>Irma lui dit :</l>
				</lg>
				<lg n="6">
					<l n="31" num="6.1">Je n’ai rien dans mon armoire,</l>
					<l n="32" num="6.2">Car les satins et la moire</l>
					<l n="33" num="6.3"><space quantity="8" unit="char"></space>Se vendent cher,</l>
					<l n="34" num="6.4">Et si, l’hiver, je frissonne,</l>
					<l n="35" num="6.5">C’est que j’ai sur ma personne</l>
					<l n="36" num="6.6"><space quantity="8" unit="char"></space>Trop peu de chair.</l>
				</lg>
				<lg n="7">
					<l n="37" num="7.1">Si les faiseurs de tapages</l>
					<l n="38" num="7.2">Ont mis trop d’or sur les pages</l>
					<l n="39" num="7.3"><space quantity="8" unit="char"></space>De ton roman,</l>
					<l n="40" num="7.4">Ne jette pas tout, ma belle,</l>
					<l n="41" num="7.5">Dans les boîtes de Poubelle,</l>
					<l n="42" num="7.6"><space quantity="8" unit="char"></space>Et donne-m’en !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1884">8 mars 1884.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>