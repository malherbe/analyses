<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">NOUS TOUS</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4011 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Nous tous</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/banvillenoustous.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Nous Tous.</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>G. Charpentier et Cie, éditeurs</publisher>
							<date when="1884">1884</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1884">1883-1884</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN231">
				<head type="number">XXXIX</head>
				<head type="main">Édouard Manet</head>
				<lg n="1">
					<l n="1" num="1.1">Ce riant, ce blond Manet,</l>
					<l n="2" num="1.2">De qui la grâce émanait,</l>
					<l n="3" num="1.3">Gai, subtil, charmant en somme,</l>
					<l n="4" num="1.4">Dans sa barbe d’Apollon,</l>
					<l n="5" num="1.5">Eut, de la nuque au talon,</l>
					<l n="6" num="1.6">Un bel air de gentilhomme.</l>
				</lg>
				<lg n="2">
					<l n="7" num="2.1">Son mal fut celui des forts.</l>
					<l n="8" num="2.2">Il voulait s’égarer hors</l>
					<l n="9" num="2.3">De la route coutumière</l>
					<l n="10" num="2.4">Et vivre avec les esprits.</l>
					<l n="11" num="2.5">Il eut le tort d’être épris</l>
					<l n="12" num="2.6">Du jour et de la lumière.</l>
				</lg>
				<lg n="3">
					<l n="13" num="3.1">Belle Ève blonde à l’œil noir,</l>
					<l n="14" num="3.2">Il voulait te faire voir</l>
					<l n="15" num="3.3">Parmi l’air que tu respires,</l>
					<l n="16" num="3.4">Et dégrafer ton collier</l>
					<l n="17" num="3.5">Ailleurs que dans l’atelier.</l>
					<l n="18" num="3.6">On a fait des crimes pires.</l>
				</lg>
				<lg n="4">
					<l n="19" num="4.1">On l’adore, on l’a banni.</l>
					<l n="20" num="4.2">Il n’avait mérité ni</l>
					<l n="21" num="4.3">Cet excès d’honneur, ni cette</l>
					<l n="22" num="4.4">Indignité. Le public</l>
					<l n="23" num="4.5">A sa manie et son tic</l>
					<l n="24" num="4.6">Et ne voit qu’une facette.</l>
				</lg>
				<lg n="5">
					<l n="25" num="5.1">Que l’artiste, esclave et roi,</l>
					<l n="26" num="5.2">Aime la Peinture, ou toi,</l>
					<l n="27" num="5.3">Chaste Muse enchanteresse</l>
					<l n="28" num="5.4">Dont le front m’éblouissait,</l>
					<l n="29" num="5.5">Quand on part, l’important, c’est</l>
					<l n="30" num="5.6">D’avoir chéri sa maîtresse.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1884">11 janvier 1884.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>