<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">NOUS TOUS</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4011 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Nous tous</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/banvillenoustous.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Nous Tous.</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>G. Charpentier et Cie, éditeurs</publisher>
							<date when="1884">1884</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1884">1883-1884</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN273">
				<head type="number">LXXXI</head>
				<head type="main">Cigarettes</head>
				<lg n="1">
					<l n="1" num="1.1">Donc, la reine de Taïti,</l>
					<l n="2" num="1.2"><space quantity="4" unit="char"></space>Si l’on n’a pas menti,</l>
					<l n="3" num="1.3">Nous apporte, en sa chevelure,</l>
					<l n="4" num="1.4"><space quantity="4" unit="char"></space>La fine dentelure</l>
					<l n="5" num="1.5">Et l’ombre et le parfum amer</l>
					<l n="6" num="1.6"><space quantity="4" unit="char"></space>De l’orageuse mer.</l>
					<l n="7" num="1.7">N’ayant plus du tout de royaume,</l>
					<l n="8" num="1.8"><space quantity="4" unit="char"></space>Libre de ce fantôme,</l>
					<l n="9" num="1.9">Elle vient admirer Paris,</l>
					<l n="10" num="1.10">Les houris, les souris,</l>
					<l n="11" num="1.11">Tout ce que notre ville étale</l>
					<l n="12" num="1.12"><space quantity="4" unit="char"></space>De grâce orientale</l>
					<l n="13" num="1.13">Et tous ces lys purs et troublants</l>
					<l n="14" num="1.14"><space quantity="4" unit="char"></space>Qu’on voit dans les bals blancs.</l>
					<l n="15" num="1.15">Sage pourtant comme une Hélène,</l>
					<l n="16" num="1.16"><space quantity="4" unit="char"></space>En sa robe de laine,</l>
					<l n="17" num="1.17">Et levant toujours vers les cieux</l>
					<l n="18" num="1.18"><space quantity="4" unit="char"></space>Ses yeux insoucieux,</l>
					<l n="19" num="1.19">On dit que la belle princesse</l>
					<l n="20" num="1.20">Fume, fume sans cesse,</l>
					<l n="21" num="1.21">Regarde naître et voltiger</l>
					<l n="22" num="1.22"><space quantity="4" unit="char"></space>Le nuage léger</l>
					<l n="23" num="1.23">Et se laisse conter fleurettes</l>
					<l n="24" num="1.24"><space quantity="4" unit="char"></space>Par mille cigarettes.</l>
					<l n="25" num="1.25">Humbles rimeurs, nous qui rêvons,</l>
					<l n="26" num="1.26"><space quantity="4" unit="char"></space>Certes, nous l’approuvons</l>
					<l n="27" num="1.27">Dans sa fumerie éternelle,</l>
					<l n="28" num="1.28"><space quantity="4" unit="char"></space>Et nous faisons comme elle.</l>
					<l n="29" num="1.29">Car bien clos, à l’abri des vents,</l>
					<l n="30" num="1.30">Songer sur les divans,</l>
					<l n="31" num="1.31">Fut toujours une douce chose ;</l>
					<l n="32" num="1.32"><space quantity="4" unit="char"></space>Respirer une rose,</l>
					<l n="33" num="1.33">Nous plaît ; boire un généreux vin,</l>
					<l n="34" num="1.34"><space quantity="4" unit="char"></space>C’est un régal divin ;</l>
					<l n="35" num="1.35">Lire Henri Heine ou Shakspere,</l>
					<l n="36" num="1.36"><space quantity="4" unit="char"></space>Cela vaut un empire ;</l>
					<l n="37" num="1.37">Tout va délicieusement</l>
					<l n="38" num="1.38"><space quantity="4" unit="char"></space>Pour le cœur d’un amant,</l>
					<l n="39" num="1.39">Quand un rayon de soleil dore</l>
					<l n="40" num="1.40">Les cheveux qu’il adore ;</l>
					<l n="41" num="1.41">On se plaît à ne rien prouver ;</l>
					<l n="42" num="1.42"><space quantity="4" unit="char"></space>Il est bon, pour trouver</l>
					<l n="43" num="1.43">L’anéantissement physique,</l>
					<l n="44" num="1.44"><space quantity="4" unit="char"></space>D’écouter la musique ;</l>
					<l n="45" num="1.45">Mais alors que le jour s’enfuit,</l>
					<l n="46" num="1.46"><space quantity="4" unit="char"></space>Dans le calme réduit</l>
					<l n="47" num="1.47">Qu’un tapis effacé décore,</l>
					<l n="48" num="1.48"><space quantity="4" unit="char"></space>Il est plus doux encore</l>
					<l n="49" num="1.49">De fumer, et de voir le feu,</l>
					<l n="50" num="1.50">Dans un nuage bleu,</l>
					<l n="51" num="1.51">Mettre de rouges collerettes</l>
					<l n="52" num="1.52"><space quantity="4" unit="char"></space>Au cou des cigarettes.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1884">1er mars 1884.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>