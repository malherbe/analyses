<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">NOUS TOUS</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4011 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Nous tous</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/banvillenoustous.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Nous Tous.</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>G. Charpentier et Cie, éditeurs</publisher>
							<date when="1884">1884</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1884">1883-1884</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN239">
				<head type="number">XLVII</head>
				<head type="main">Galatéa</head>
				<lg n="1">
					<l n="1" num="1.1">Pailleron, ce vrai sage,</l>
					<l n="2" num="1.2">Est donc, selon l’ancien</l>
					<l n="3" num="1.3"><space quantity="12" unit="char"></space>Usage,</l>
					<l n="4" num="1.4">Académicien !</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Son discours, où tout sonne</l>
					<l n="6" num="2.2">Comme l’or, n’a lésé</l>
					<l n="7" num="2.3"><space quantity="12" unit="char"></space>Personne :</l>
					<l n="8" num="2.4">Prodige malaisé !</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Chez lui l’esprit abonde,</l>
					<l n="10" num="3.2">Et s’il ravit et prit</l>
					<l n="11" num="3.3"><space quantity="12" unit="char"></space>Le monde</l>
					<l n="12" num="3.4">Que charme encor l’esprit,</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">C’est qu’avec sa folie</l>
					<l n="14" num="4.2">Chantant sous le ciel bleu,</l>
					<l n="15" num="4.3"><space quantity="12" unit="char"></space>Thalie</l>
					<l n="16" num="4.4">Est toujours dans son jeu ;</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1">Et tendrement folâtre,</l>
					<l n="18" num="5.2">A l’Institut comme au</l>
					<l n="19" num="5.3"><space quantity="12" unit="char"></space>Théâtre,</l>
					<l n="20" num="5.4">La Nymphe au vert rameau,</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1">Légère sur les planches,</l>
					<l n="22" num="6.2">Lui sourit avec ses</l>
					<l n="23" num="6.3"><space quantity="12" unit="char"></space>Dents blanches,</l>
					<l n="24" num="6.4">Et le mène au succès.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1">C’est bien, Académie,</l>
					<l n="26" num="7.2">D’avoir en ton giron,</l>
					<l n="27" num="7.3"><space quantity="12" unit="char"></space>Ma mie,</l>
					<l n="28" num="7.4">Accueilli Pailleron ;</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1">Mais plus d’un, à cette heure,</l>
					<l n="30" num="8.2">Pour vous brûle d’amour</l>
					<l n="31" num="8.3"><space quantity="12" unit="char"></space>Et pleure.</l>
					<l n="32" num="8.4">Madame, à qui le tour ?</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1">Veuve souvent trompée,</l>
					<l n="34" num="9.2">Ne poussez pas à bout</l>
					<l n="35" num="9.3"><space quantity="12" unit="char"></space>Coppée,</l>
					<l n="36" num="9.4">Ni le subtil About.</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1">L’un célèbre (il est nôtre !)</l>
					<l n="38" num="10.2">Marguerite au rouet,</l>
					<l n="39" num="10.3"><space quantity="12" unit="char"></space>Mais l’autre</l>
					<l n="40" num="10.4">Est un fils d’Arouet.</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1">Sans qu’on vous morigène,</l>
					<l n="42" num="11.2">Si le choix hasardeux</l>
					<l n="43" num="11.3"><space quantity="12" unit="char"></space>Vous gêne,</l>
					<l n="44" num="11.4">Prenez-les tous les deux.</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1">Ah ! cette Académie,</l>
					<l n="46" num="12.2">Dans son rêve indolent</l>
					<l n="47" num="12.3"><space quantity="12" unit="char"></space>Blêmie !</l>
					<l n="48" num="12.4">Si l’homme est un volant,</l>
				</lg>
				<lg n="13">
					<l n="49" num="13.1">Elle tient la raquette !</l>
					<l n="50" num="13.2">Être plus qu’il ne faut</l>
					<l n="51" num="13.3"><space quantity="12" unit="char"></space>Coquette,</l>
					<l n="52" num="13.4">Est son plus cher défaut.</l>
				</lg>
				<lg n="14">
					<l n="53" num="14.1">Tenez ! voyez-la ! comme</l>
					<l n="54" num="14.2">Elle jette, en riant,</l>
					<l n="55" num="14.3"><space quantity="12" unit="char"></space>La pomme</l>
					<l n="56" num="14.4">A qui va la priant !</l>
				</lg>
				<lg n="15">
					<l n="57" num="15.1">Puis, montrant ses épaules,</l>
					<l n="58" num="15.2">Vite, elle s’enfuit vers</l>
					<l n="59" num="15.3"><space quantity="12" unit="char"></space>Les saules,</l>
					<l n="60" num="15.4">Ses cheveux de travers.</l>
				</lg>
				<lg n="16">
					<l n="61" num="16.1">Pourtant elle a beau geindre !</l>
					<l n="62" num="16.2">Si l’adroit amant sait</l>
					<l n="63" num="16.3"><space quantity="12" unit="char"></space>L’atteindre,</l>
					<l n="64" num="16.4">Sans demander qui c’est,</l>
				</lg>
				<lg n="17">
					<l n="65" num="17.1">Et l’a prise et meurtrie,</l>
					<l n="66" num="17.2">Quoiqu’elle entre en courroux</l>
					<l n="67" num="17.3"><space quantity="12" unit="char"></space>Et crie :</l>
					<l n="68" num="17.4">Pour qui me prenez-vous ?</l>
				</lg>
				<lg n="18">
					<l n="69" num="18.1">Elle a beau se défendre</l>
					<l n="70" num="18.2">Et conter son roman</l>
					<l n="71" num="18.3"><space quantity="12" unit="char"></space>Si tendre,</l>
					<l n="72" num="18.4">Et s’écrier : Maman !</l>
				</lg>
				<lg n="19">
					<l n="73" num="19.1">Si l’amant, toujours ferme</l>
					<l n="74" num="19.2">Et sachant tout oser,</l>
					<l n="75" num="19.3"><space quantity="12" unit="char"></space>Lui ferme</l>
					<l n="76" num="19.4">La bouche d’un baiser ;</l>
				</lg>
				<lg n="20">
					<l n="77" num="20.1">La jeteuse de pomme</l>
					<l n="78" num="20.2">Dit, en ouvrant ses bras :</l>
					<l n="79" num="20.3"><space quantity="12" unit="char"></space>Cher homme,</l>
					<l n="80" num="20.4">Fais ce que tu voudras !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1884">25 janvier 1884.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>