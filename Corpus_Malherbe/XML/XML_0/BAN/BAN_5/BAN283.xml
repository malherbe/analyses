<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">NOUS TOUS</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4011 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Nous tous</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/banvillenoustous.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Nous Tous.</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>G. Charpentier et Cie, éditeurs</publisher>
							<date when="1884">1884</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1884">1883-1884</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN283">
				<head type="number">XCI</head>
				<head type="main">Aveu</head>
				<lg n="1">
					<l n="1" num="1.1">On lui disait : Mademoiselle,</l>
					<l n="2" num="1.2">Faites votre confession.</l>
					<l n="3" num="1.3">Rire est joli ; mais être oiselle</l>
					<l n="4" num="1.4">N’est pas une profession.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">On admire votre corsage</l>
					<l n="6" num="2.2">Et ce bel œil intelligent ;</l>
					<l n="7" num="2.3">Mais pour être tout à fait sage,</l>
					<l n="8" num="2.4">Économisez de l’argent.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Si votre beauté fulgurante</l>
					<l n="10" num="3.2">Éblouit toujours le miroir,</l>
					<l n="11" num="3.3">Achetez des coupons de rente</l>
					<l n="12" num="3.4">Et mettez-les dans un tiroir.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">Car un jour viendra, jour de jeûne,</l>
					<l n="14" num="4.2">Où, le doux printemps ayant fui,</l>
					<l n="15" num="4.3">Vous serez jeune, mais moins jeune</l>
					<l n="16" num="4.4">Que vous ne l’êtes aujourd’hui.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1">Lors, pour braver les épigrammes</l>
					<l n="18" num="5.2">Et garder les amants épris,</l>
					<l n="19" num="5.3">Il faudra des <hi rend="ital">cheveux pour dames</hi>;</l>
					<l n="20" num="5.4">Vous savez qu’ils sont hors de prix.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1">On lui parlait ainsi. Mais elle</l>
					<l n="22" num="6.2">Répondit, fugitif éclair :</l>
					<l n="23" num="6.3">Merci, messieurs, pour votre zèle ;</l>
					<l n="24" num="6.4">J’ai la lèvre rouge et l’œil clair.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1">Je m’amuse, et la vie est douce ;</l>
					<l n="26" num="7.2">Regardez ma petite main.</l>
					<l n="27" num="7.3">Je roule et n’amasse pas mousse,</l>
					<l n="28" num="7.4">Comme la pierre du chemin.</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1">Et je ris. Être ou ne pas être</l>
					<l n="30" num="8.2">Gaie, est la seule question.</l>
					<l n="31" num="8.3">Je ne prendrai personne en traître,</l>
					<l n="32" num="8.4">Pas même le prix Montyon.</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1">J’erre, en emplissant ma corbeille</l>
					<l n="34" num="9.2">Des lys où l’aube a mis ses pleurs,</l>
					<l n="35" num="9.3">Et j’aspire, comme l’abeille,</l>
					<l n="36" num="9.4">Le suc des odorantes fleurs.</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1">Thésauriser m’est impossible.</l>
					<l n="38" num="10.2">J’égrène ma folle chanson,</l>
					<l n="39" num="10.3">Et puis, j’ai le tort invincible</l>
					<l n="40" num="10.4">D’être aimante — comme chausson !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1884">ll mars 1884.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>