<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">NOUS TOUS</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4011 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Nous tous</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/banvillenoustous.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Nous Tous.</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>G. Charpentier et Cie, éditeurs</publisher>
							<date when="1884">1884</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1884">1883-1884</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN287">
				<head type="number">XCV</head>
				<head type="main">Le Palais-Royal</head>
				<head type="sub_2">
					Strophes dites par mademoiselle Maria Legault <lb></lb>
					le 14 septembre 1880 <lb></lb>
					pour l’inauguration de la nouvelle salle <lb></lb>
					Direction Briet et Delcroix.
				</head>
				<lg n="1">
					<l n="1" num="1.1">Toi que le caprice emporte,</l>
					<l n="2" num="1.2">Public parisien, tu</l>
					<l n="3" num="1.3">Ne t’es pas trompé de porte :</l>
					<l n="4" num="1.4">Écoute mon impromptu.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Ce palais où tout flamboie,</l>
					<l n="6" num="2.2">Riant comme un prairial</l>
					<l n="7" num="2.3">Plein de lumière et de joie,</l>
					<l n="8" num="2.4">C’est bien le Palais-Royal.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Oui, viens chez toi, foule aimée !</l>
					<l n="10" num="3.2">Après les temps révolus,</l>
					<l n="11" num="3.3">La vieille salle enfumée</l>
					<l n="12" num="3.4">Est morte : n’en parlons plus.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">L’architecte Paul Sédille</l>
					<l n="14" num="4.2">A paré de cent trésors</l>
					<l n="15" num="4.3">Ce gai boudoir où tout brille,</l>
					<l n="16" num="4.4">Les lys, la pourpre et les ors.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1">Notre plafond, comme un astre,</l>
					<l n="18" num="5.2">Rit, par tes yeux savouré ;</l>
					<l n="19" num="5.3">Le savant peintre Lavastre</l>
					<l n="20" num="5.4">Broda son dôme ajouré,</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1">Et dans l’air, qui s’extasie,</l>
					<l n="22" num="6.2">Lança, d’un vol indompté,</l>
					<l n="23" num="6.3">Le Rire, la Fantaisie,</l>
					<l n="24" num="6.4">La Chanson, la Volupté.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1">Partout des apothéoses,</l>
					<l n="26" num="7.2">Des enfants ensorceleurs,</l>
					<l n="27" num="7.3">Des feuillages et des roses,</l>
					<l n="28" num="7.4">Des ruissellements de fleurs,</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1">Et, dans leurs jeux téméraires</l>
					<l n="30" num="8.2">Et leurs fiers ébats, Dalou</l>
					<l n="31" num="8.3">A sculpté partout les frères</l>
					<l n="32" num="8.4">De l’Amour, ce gai filou.</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1">O Comédie ! ô Folie !</l>
					<l n="34" num="9.2">Qui riez sur les néants,</l>
					<l n="35" num="9.3">Sa main, pour charmer Thalie,</l>
					<l n="36" num="9.4">Modela vos fronts géants,</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1">Et, souffletant nos augures,</l>
					<l n="38" num="10.2">Vers un avenir voilé</l>
					<l n="39" num="10.3">Vous volez, saintes figures,</l>
					<l n="40" num="10.4">Dans l’idéal étoilé !</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1">Puis dans un cartel mystique</l>
					<l n="42" num="11.2">S’inscrit, au front du palais,</l>
					<l n="43" num="11.3">Le miraculeux distique</l>
					<l n="44" num="11.4">Du grand aïeul Rabelais.</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1">Car c’est lui que veulent suivre</l>
					<l n="46" num="12.2">Nos auteurs, sans orgueil vain,</l>
					<l n="47" num="12.3">Et c’est lui qui les enivre</l>
					<l n="48" num="12.4">Avec son généreux vin.</l>
				</lg>
				<lg n="13">
					<l n="49" num="13.1">Nos pères, dans leur souffrance,</l>
					<l n="50" num="13.2">Buvaient ce vin écumeux</l>
					<l n="51" num="13.3">Qui désaltéra la France,</l>
					<l n="52" num="13.4">Et nous le boirons comme eux !</l>
				</lg>
				<lg n="14">
					<l n="53" num="14.1">C’est ici qu’en son délire,</l>
					<l n="54" num="14.2">S’ouvrit aux grands histrions</l>
					<l n="55" num="14.3">La chère maison du Rire :</l>
					<l n="56" num="14.4">Donc, ô mes amis ! rions.</l>
				</lg>
				<lg n="15">
					<l n="57" num="15.1">Notre passé fut si riche !</l>
					<l n="58" num="15.2">Et, sans nul doute, on connaît</l>
					<l n="59" num="15.3">Nos maîtres : Sardou, Labiche,</l>
					<l n="60" num="15.4">Et Meilhac, et Gondinet ;</l>
				</lg>
				<lg n="16">
					<l n="61" num="16.1">Halévy, plein de finesse ;</l>
					<l n="62" num="16.2">Siraudin et Delacour,</l>
					<l n="63" num="16.3">Thiboust, sourire et jeunesse</l>
					<l n="64" num="16.4">De la muse de l’amour !</l>
				</lg>
				<lg n="17">
					<l n="65" num="17.1">Puis, sous la clarté des lustres,</l>
					<l n="66" num="17.2">La comédie eut chez nous</l>
					<l n="67" num="17.3">Ses bouffons les plus illustres :</l>
					<l n="68" num="17.4">O souvenir triste et doux !</l>
				</lg>
				<lg n="18">
					<l n="69" num="18.1">Autrefois, jeune et frivole,</l>
					<l n="70" num="18.2">C’est ici que Déjazet</l>
					<l n="71" num="18.3">Égrenait sa chanson folle,</l>
					<l n="72" num="18.4">Et, comme un ruisseau, jasait.</l>
				</lg>
				<lg n="19">
					<l n="73" num="19.1">Achard, qui charma la ville,</l>
					<l n="74" num="19.2">Tousez, qui n’était pas sot,</l>
					<l n="75" num="19.3">Leménil, le bon Sainville,</l>
					<l n="76" num="19.4">Et Levassor, et Grassot ;</l>
				</lg>
				<lg n="20">
					<l n="77" num="20.1">Gil Pérès, hélas ! Thalie</l>
					<l n="78" num="20.2">A chéri ces grands railleurs</l>
					<l n="79" num="20.3">Pleins de verve et de folie ;</l>
					<l n="80" num="20.4">Moi, j’en passe, et des meilleurs,</l>
				</lg>
				<lg n="21">
					<l n="81" num="21.1">Mais Émile Bayard groupe</l>
					<l n="82" num="21.2">Sur un panneau triomphant</l>
					<l n="83" num="21.3">Toute l’immortelle troupe</l>
					<l n="84" num="21.4">Qui commence à Mars enfant,</l>
				</lg>
				<lg n="22">
					<l n="85" num="22.1">Et qui posséda naguère</l>
					<l n="86" num="22.2">Ces rois de notre métier</l>
					<l n="87" num="22.3">Armés pour la grande guerre :</l>
					<l n="88" num="22.4">Samson, Régnier et Potier !</l>
				</lg>
				<lg n="23">
					<l n="89" num="23.1">Puis, de cette époque sainte,</l>
					<l n="90" num="23.2">Ingénieux et malin,</l>
					<l n="91" num="23.3">Reste le bon Hyacinthe</l>
					<l n="92" num="23.4">Avec son nez aquilin ;</l>
				</lg>
				<lg n="24">
					<l n="93" num="24.1">Et celui qui te déride,</l>
					<l n="94" num="24.2">Le grand, le vrai sage, effroi</l>
					<l n="95" num="24.3">De la bêtise candide :</l>
					<l n="96" num="24.4">L’inimitable Geoffroy ;</l>
				</lg>
				<lg n="25">
					<l n="97" num="25.1">Geoffroy, qui jette et secoue</l>
					<l n="98" num="25.2">Sur les types qu’il revêt</l>
					<l n="99" num="25.3">Tant de lumière, et qui joue</l>
					<l n="100" num="25.4">Comme Molière écrivait !</l>
				</lg>
				<lg n="26">
					<l n="101" num="26.1">Et de tant de gloire éparse</l>
					<l n="102" num="26.2">Demeure aussi Lhéritier,</l>
					<l n="103" num="26.3">Qui des princes de la farce</l>
					<l n="104" num="26.4">Est le fidèle héritier !</l>
				</lg>
				<lg n="27">
					<l n="105" num="27.1">Puis, cher public qui m’accueilles,</l>
					<l n="106" num="27.2">Après les glorieux noms</l>
					<l n="107" num="27.3">Envolés comme des feuilles,</l>
					<l n="108" num="27.4">Tremblants d’espoir, nous venons.</l>
				</lg>
				<lg n="28">
					<l n="109" num="28.1">Exempts de toute humeur noire,</l>
					<l n="110" num="28.2">Tu nous verras toujours gais,</l>
					<l n="111" num="28.3">Très sûrs de notre mémoire,</l>
					<l n="112" num="28.4">Contents, jamais fatigués.</l>
				</lg>
				<lg n="29">
					<l n="113" num="29.1">Nous mettrons dans nos programmes</l>
					<l n="114" num="29.2">Tout, hors le genre ennuyeux.</l>
					<l n="115" num="29.3">C’est à toi seul que nos femmes</l>
					<l n="116" num="29.4">Feront ici les doux yeux.</l>
				</lg>
				<lg n="30">
					<l n="117" num="30.1">Oui, nous ferons pour te plaire</l>
					<l n="118" num="30.2">Un effort quotidien ;</l>
					<l n="119" num="30.3">Mais donne-nous pour salaire,</l>
					<l n="120" num="30.4">Ami, ce que tu sais bien,</l>
				</lg>
				<lg n="31">
					<l n="121" num="31.1">Et, par un doux bruit sonore</l>
					<l n="122" num="31.2">Charmant notre essai loyal,</l>
					<l n="123" num="31.3">Dis que nous sommes encore</l>
					<l n="124" num="31.4">Ton bon vieux Palais-Royal !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1880">8 septembre 1880.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>