<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">NOUS TOUS</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4011 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Nous tous</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/banvillenoustous.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Nous Tous.</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>G. Charpentier et Cie, éditeurs</publisher>
							<date when="1884">1884</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1884">1883-1884</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN213">
				<head type="number">XXI</head>
				<head type="main">Ballard</head>
				<lg n="1">
					<l n="1" num="1.1">Il est mort. Destinée obscure.</l>
					<l n="2" num="1.2">Pauvre Yorick ! Pauvre Ballard !</l>
					<l n="3" num="1.3">Jamais ce brave homme n’eut cure</l>
					<l n="4" num="1.4">D’élever le niveau de l’art.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Ce vieil acteur du Vaudeville,</l>
					<l n="6" num="2.2">Impassible à son humble rang,</l>
					<l n="7" num="2.3">Fut jadis par toute la ville</l>
					<l n="8" num="2.4">Aussi connu que le loup blanc.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Éternel comme sainte Thècle,</l>
					<l n="10" num="3.2">Sans que jamais on l’augmentât,</l>
					<l n="11" num="3.3">Pendant au moins trois quarts de siècle</l>
					<l n="12" num="3.4">Il a très bien fait son état.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">Très bien. Correctement. Sans faute.</l>
					<l n="14" num="4.2">Fechter n’était pas son rival.</l>
					<l n="15" num="4.3">Il n’eut pas l’ambition haute</l>
					<l n="16" num="4.4">Qu’on le vît en Armand Duval.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1">Non. Il jouait les domestiques,</l>
					<l n="18" num="5.2">Goûtant, courbé sous l’humble loi,</l>
					<l n="19" num="5.3">Mille voluptés fantastiques</l>
					<l n="20" num="5.4">A tenir ce modeste emploi ;</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1">Et tout comme sur une bûche</l>
					<l n="22" num="6.2">Que dévore le feu charmant,</l>
					<l n="23" num="6.3">Sur ses deux jambes la peluche</l>
					<l n="24" num="6.4">Fleurissait naturellement.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1">Comme Ruy Blas, âme livrée</l>
					<l n="26" num="7.2">Aux coups du destin abusif,</l>
					<l n="27" num="7.3">Lorsqu’il n’avait pas sa livrée</l>
					<l n="28" num="7.4">Il était déguisé tout vif.</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1">Humble et fort peu payé, qu’importe !</l>
					<l n="30" num="8.2">Un manque de soin le navrait.</l>
					<l n="31" num="8.3">On lui disait : Fermez la porte.</l>
					<l n="32" num="8.4">Ouvrez la fenêtre. Il l’ouvrait.</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1">Il supporta la vie amère,</l>
					<l n="34" num="9.2">Pur de toute défection,</l>
					<l n="35" num="9.3">Pour cette idéale chimère :</l>
					<l n="36" num="9.4">L’amour de la perfection.</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1">Figure au devoir asservie</l>
					<l n="38" num="10.2">Comme un esclave nubien,</l>
					<l n="39" num="10.3">Il disait : Madame est servie.</l>
					<l n="40" num="10.4">Seulement, il le disait bien.</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1">Hélas ! par nulle récompense</l>
					<l n="42" num="11.2">Son sort ne fut édulcoré,</l>
					<l n="43" num="11.3">Car ce comédien, je pense,</l>
					<l n="44" num="11.4">Ne fut même pas décoré.</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1">Et maintenant, comme on le narre,</l>
					<l n="46" num="12.2">Ombre éprise encor de son art,</l>
					<l n="47" num="12.3">Il sert là-bas, sur le Ténare,</l>
					<l n="48" num="12.4">Arnal et madame Thénard.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1883">14 décembre 1883.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>