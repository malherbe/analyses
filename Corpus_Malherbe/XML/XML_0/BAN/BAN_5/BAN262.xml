<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">NOUS TOUS</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4011 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Nous tous</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/banvillenoustous.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Nous Tous.</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>G. Charpentier et Cie, éditeurs</publisher>
							<date when="1884">1884</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1884">1883-1884</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN262">
				<head type="number">LXX</head>
				<head type="main">Lex</head>
				<lg n="1">
					<l n="1" num="1.1">Rosette avait un joli signe</l>
					<l n="2" num="1.2">Dans un endroit qui n’est pas laid,</l>
					<l n="3" num="1.3">Amusant sur le cou de cygne,</l>
					<l n="4" num="1.4">Comme une mouche sur du lait.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Elle avait des bouffettes roses</l>
					<l n="6" num="2.2">Sur ses gais souliers de satin,</l>
					<l n="7" num="2.3">Qui vous disaient des tas de choses</l>
					<l n="8" num="2.4">Dans un langage clandestin.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Et parfois aussi, la folâtre,</l>
					<l n="10" num="3.2">Pardonnant aux lys d’être nus,</l>
					<l n="11" num="3.3">Décolletée au coin de l’âtre,</l>
					<l n="12" num="3.4">Laissait voir ses seins ingénus.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">Hier Gontran, lui rendant visite,</l>
					<l n="14" num="4.2">Vit avec un tragique effroi</l>
					<l n="15" num="4.3">Qu’un long vêtement parasite</l>
					<l n="16" num="4.4">Voilait tous ces jouets de roi.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1">Gigantesque feuille de vigne,</l>
					<l n="18" num="5.2">Une robe aux plis trop osés</l>
					<l n="19" num="5.3">Cachait les bouffettes, le signe</l>
					<l n="20" num="5.4">Et les tendres boutons rosés.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1">Alors, d’une âme humiliée,</l>
					<l n="22" num="6.2">Il dit : O prodige nouveau !</l>
					<l n="23" num="6.3">Voilà Rosette reliée</l>
					<l n="24" num="6.4">Comme un volume in-octavo !</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1">Chez vous, on était camarade</l>
					<l n="26" num="7.2">Avec les roses et les lys.</l>
					<l n="27" num="7.3">D’où nous vient cette mascarade ?</l>
					<l n="28" num="7.4">Thècle remplace Amaryllis !</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1">Mais Rosette à la pâleur d’ambre</l>
					<l n="30" num="8.2">Lui dit : Vous n’avez donc pas lu,</l>
					<l n="31" num="8.3">Monsieur, les débats de la Chambre</l>
					<l n="32" num="8.4">Et ce que l’on a résolu ?</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1">J’embellissais les jours moroses</l>
					<l n="34" num="9.2">Par des notes bizarres ; mais</l>
					<l n="35" num="9.3">Le signe et les bouffettes roses,</l>
					<l n="36" num="9.4">Nul ne les verra plus jamais.</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1">Si quelque regard les rencontre,</l>
					<l n="38" num="10.2">Ce sera plus tard, dans les cieux :</l>
					<l n="39" num="10.3">Car il ne faut plus que l’on montre</l>
					<l n="40" num="10.4">Des emblèmes séditieux !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1884">19 février 1884.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>