<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">NOUS TOUS</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4011 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Nous tous</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/banvillenoustous.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Nous Tous.</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>G. Charpentier et Cie, éditeurs</publisher>
							<date when="1884">1884</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1884">1883-1884</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN238">
				<head type="number">XLVI</head>
				<head type="main">Dans le monde</head>
				<lg n="1">
					<l n="1" num="1.1">Amené, jeune et plein d’espoir,</l>
					<l n="2" num="1.2">A la fête que donne Adèle,</l>
					<l n="3" num="1.3">Luc, charmant dans son habit noir,</l>
					<l n="4" num="1.4">Se demande ce qu’on a d’elle.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Ébloui comme l’étourneau,</l>
					<l n="6" num="2.2">Il voit se presser sous les lustres</l>
					<l n="7" num="2.3">En fleurs, venus de Murano,</l>
					<l n="8" num="2.4">Un tas de bonshommes illustres.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Les femmes aux fronts querelleurs</l>
					<l n="10" num="3.2">Ressembleraient aux jeunes mères</l>
					<l n="11" num="3.3">D’un tas de Cupidons voleurs,</l>
					<l n="12" num="3.4">Avec leurs croupes de Chimères.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">On s’amuse, ou l’on faitsemblant.</l>
					<l n="14" num="4.2">Tout, dans cette fête, respire</l>
					<l n="15" num="4.3">Le mystère doux et troublant.</l>
					<l n="16" num="4.4">On dirait que l’on y conspire.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1">Oh ! que d’invités ! Quelques-uns</l>
					<l n="18" num="5.2">Disent des paroles sans queue</l>
					<l n="19" num="5.3">Ni tête. Des flots de parfums</l>
					<l n="20" num="5.4">Montent dans l’atmosphère bleue.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1">Et partout, sous ce voile bleu</l>
					<l n="22" num="6.2">Qui ravirait les coloristes,</l>
					<l n="23" num="6.3">On voit des diamants de feu</l>
					<l n="24" num="6.4">Et des seins nus et des yeux tristes.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1">Une femme au sourcil courbé</l>
					<l n="26" num="7.2">Comme un arc, dont on s’émerveille,</l>
					<l n="27" num="7.3">Appelle un ministre : Bébé,</l>
					<l n="28" num="7.4">Et deux collégiens : Ma vieille.</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1">A tout ce poëme diffus</l>
					<l n="30" num="8.2">Voulant comprendre quelque chose,</l>
					<l n="31" num="8.3">Luc s’adresse d’un air confus</l>
					<l n="32" num="8.4">A sa belle voisine Rose,</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1">Qui met des cœurs dans ses prisons.</l>
					<l n="34" num="9.2">Timide, il s’est penché vers elle</l>
					<l n="35" num="9.3">Au point d’effleurer ses frisons.</l>
					<l n="36" num="9.4">Oh ! lui dit-il, mademoiselle,</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1">Guidez mes esprits, éblouis</l>
					<l n="38" num="10.2">Par votre chevelure blonde.</l>
					<l n="39" num="10.3">Ici, je vois bien que je suis</l>
					<l n="40" num="10.4">Dans le monde. Mais dans quel monde ?</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1">J’ai fait ce rêve étrange et doux :</l>
					<l n="42" num="11.2">Conduire à travers la Bohème</l>
					<l n="43" num="11.3">Un bel être pareil à vous.</l>
					<l n="44" num="11.4">Est-ce ici le monde où l’on aime ?</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1">Sur ma lèvre, un vol de baisers</l>
					<l n="46" num="12.2">Qui voudraient fuir vers votre joue,</l>
					<l n="47" num="12.3">S’enivre de ses tons rosés.</l>
					<l n="48" num="12.4">Est-ce ici le monde où l’on joue ?</l>
				</lg>
				<lg n="13">
					<l n="49" num="13.1">Mais si vous le voulez, je veux</l>
					<l n="50" num="13.2">Trouver la tristesse meilleure.</l>
					<l n="51" num="13.3">Je sens frissonner vos cheveux.</l>
					<l n="52" num="13.4">Est-ce ici le monde où l’on pleure ?</l>
				</lg>
				<lg n="14">
					<l n="53" num="14.1">Ou, si vous le voulez aussi,</l>
					<l n="54" num="14.2">J’aime la joie et son délire.</l>
					<l n="55" num="14.3">Répondez, madame, est-ce ici</l>
					<l n="56" num="14.4">Le monde où l’on se tord de rire ?</l>
				</lg>
				<lg n="15">
					<l n="57" num="15.1">Rose écoute ces mots ardents</l>
					<l n="58" num="15.2">Et regarde, presque touchée,</l>
					<l n="59" num="15.3">Le jeune ingénu, dont ses dents</l>
					<l n="60" num="15.4">Feraient à peine une bouchée.</l>
				</lg>
				<lg n="16">
					<l n="61" num="16.1">Rose qui connaît tout, le suc</l>
					<l n="62" num="16.2">Des poisons, le goût de la lie</l>
					<l n="63" num="16.3">Et tout le reste, dit à Luc,</l>
					<l n="64" num="16.4">En levant ses yeux d’Ophélie,</l>
				</lg>
				<lg n="17">
					<l n="65" num="17.1">Ses pâles yeux diamantés</l>
					<l n="66" num="17.2">Où frissonne un tragique rêve :</l>
					<l n="67" num="17.3">Jeune homme, allez-vous-en. Partez.</l>
					<l n="68" num="17.4">C’est ici le monde où l’on crève !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1884">18 janvier 1884.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>