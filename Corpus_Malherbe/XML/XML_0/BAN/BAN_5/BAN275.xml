<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">NOUS TOUS</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4011 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Nous tous</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/banvillenoustous.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Nous Tous.</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>G. Charpentier et Cie, éditeurs</publisher>
							<date when="1884">1884</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1884">1883-1884</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN275">
				<head type="number">LXXXIII</head>
				<head type="main">Prière</head>
				<lg n="1">
					<l n="1" num="1.1">Ah ! n’allons pas en longue queue,</l>
					<l n="2" num="1.2"><space quantity="8" unit="char"></space>Humiliés,</l>
					<l n="3" num="1.3">Chez ce traiteur de la banlieue</l>
					<l n="4" num="1.4"><space quantity="8" unit="char"></space>Dont vous parliez !</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Fêtons notre ami, sans nul doute,</l>
					<l n="6" num="2.2"><space quantity="8" unit="char"></space>Quand sans ennuis</l>
					<l n="7" num="2.3">Il a bien parcouru sa route.</l>
					<l n="8" num="2.4"><space quantity="8" unit="char"></space>Certes, j’en suis.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Avec le vin de la vendange,</l>
					<l n="10" num="3.2"><space quantity="8" unit="char"></space>Sachons encor</l>
					<l n="11" num="3.3">Lui verser la saine louange,</l>
					<l n="12" num="3.4"><space quantity="8" unit="char"></space>Comme un flot d’or,</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">Et qu’alors le poëte en flamme</l>
					<l n="14" num="4.2"><space quantity="8" unit="char"></space>Reste orateur ;</l>
					<l n="15" num="4.3">Mais n’allons pas chez cet infâme</l>
					<l n="16" num="4.4"><space quantity="8" unit="char"></space>Restaurateur !</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1">Effroi de la race latine,</l>
					<l n="18" num="5.2"><space quantity="8" unit="char"></space>Crime formel,</l>
					<l n="19" num="5.3">Sa soupe est de la gélatine</l>
					<l n="20" num="5.4"><space quantity="8" unit="char"></space>Au caramel.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1">On entend parmi ses hors-d’œuvre</l>
					<l n="22" num="6.2"><space quantity="8" unit="char"></space>Un cri plaintif,</l>
					<l n="23" num="6.3">Et j’aimerais mieux une pieuvre</l>
					<l n="24" num="6.4"><space quantity="8" unit="char"></space>Que son rosbeef.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1">Sa volaille a l’aspect lubrique,</l>
					<l n="26" num="7.2"><space quantity="8" unit="char"></space>Et ses homards</l>
					<l n="27" num="7.3">Sont bons pour des nègres d’Afrique</l>
					<l n="28" num="7.4"><space quantity="8" unit="char"></space>Aux nez camards.</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1">Même on le compare à Procuste</l>
					<l n="30" num="8.2"><space quantity="8" unit="char"></space>Dans les journaux.</l>
					<l n="31" num="8.3">Il collabore avec Locuste</l>
					<l n="32" num="8.4"><space quantity="8" unit="char"></space>Sur des fourneaux.</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1">Fuyons cet homme à l’esprit large,</l>
					<l n="34" num="9.2"><space quantity="8" unit="char"></space>Mais au cœur vain ;</l>
					<l n="35" num="9.3">Car c’est avec de la litharge</l>
					<l n="36" num="9.4"><space quantity="8" unit="char"></space>Qu’il fait son vin.</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1">Craignons ses crèmes éhontées</l>
					<l n="38" num="10.2"><space quantity="8" unit="char"></space>Et les dégâts</l>
					<l n="39" num="10.3">Que feraient ses pièces montées</l>
					<l n="40" num="10.4"><space quantity="8" unit="char"></space>Et ses nougats.</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1">Fauchant les gens, comme des herbes,</l>
					<l n="42" num="11.2"><space quantity="8" unit="char"></space>Au son des cors,</l>
					<l n="43" num="11.3">Il prétend donner de superbes</l>
					<l n="44" num="11.4"><space quantity="8" unit="char"></space>Repas de corps.</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1">Au temps passé, nous y dînâmes</l>
					<l n="46" num="12.2"><space quantity="8" unit="char"></space>En grand gala ;</l>
					<l n="47" num="12.3">Mais il ferait bientôt des âmes</l>
					<l n="48" num="12.4"><space quantity="8" unit="char"></space>De ces corps-là.</l>
				</lg>
				<lg n="13">
					<l n="49" num="13.1">Évitons sa cuisine atroce ;</l>
					<l n="50" num="13.2"><space quantity="8" unit="char"></space>Car, sans honneur,</l>
					<l n="51" num="13.3">On périrait chez ce féroce</l>
					<l n="52" num="13.4"><space quantity="8" unit="char"></space>Empoisonneur !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1884">3 mars 1884.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>