<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">NOUS TOUS</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4011 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Nous tous</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/banvillenoustous.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Nous Tous.</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>G. Charpentier et Cie, éditeurs</publisher>
							<date when="1884">1884</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1884">1883-1884</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN244">
				<head type="number">LII</head>
				<head type="main">Bal masqué</head>
				<lg n="1">
					<l n="1" num="1.1">On peut voir des yeux de phosphore</l>
					<l n="2" num="1.2">Briller au bal de l’Opéra.</l>
					<l n="3" num="1.3">C’est bien moins loin que le Bosphore</l>
					<l n="4" num="1.4">Et que le faubourg de Péra.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Tous les ennuis sont prosaïques,</l>
					<l n="6" num="2.2">Et la vie est un promenoir.</l>
					<l n="7" num="2.3">Pourquoi pas sous les mosaïques</l>
					<l n="8" num="2.4">Se promener en habit noir ?</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Plus d’allures dévergondées.</l>
					<l n="10" num="3.2">Sur le bel escalier géant</l>
					<l n="11" num="3.3">Les gens échangent leurs idées :</l>
					<l n="12" num="3.4">Rien du tout, contre le néant.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">L’âpre musique des Tziganes,</l>
					<l n="14" num="4.2">Pensive comme le Destin,</l>
					<l n="15" num="4.3">Étonne et ravit les organes</l>
					<l n="16" num="4.4">Agacés par son bruit lointain,</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1">Et jette, comme une caresse,</l>
					<l n="18" num="5.2">Dans l’âme de nos Dalilas,</l>
					<l n="19" num="5.3">Un vague désir de paresse,</l>
					<l n="20" num="5.4">Avec la chanson des guzlas.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1">Quant au passé, qui sous les lustres</l>
					<l n="22" num="6.2">Enchanta notre œil ébloui</l>
					<l n="23" num="6.3">Avec ses tordions illustres,</l>
					<l n="24" num="6.4">Tout cela s’est évanoui.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1">Chicard danse dans les étoiles !</l>
					<l n="26" num="7.2">Et son plumet tressaille encor</l>
					<l n="27" num="7.3">Dans l’azur, et parmi les toiles</l>
					<l n="28" num="7.4">De ce vertigineux décor.</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1">Pomaré, chaste en sa démence</l>
					<l n="30" num="8.2">Dont jamais nous ne nous lassions,</l>
					<l n="31" num="8.3">Danse un cavalier seul immense</l>
					<l n="32" num="8.4">Avec les constellations ;</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1">Et raillant la lyre thébaine,</l>
					<l n="34" num="9.2">Musard aux pâleurs de safran</l>
					<l n="35" num="9.3">Agite son bâton d’ébène</l>
					<l n="36" num="9.4">Dans le farouche Aldébaran.</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1">Strauss, poursuivi par les huées</l>
					<l n="38" num="10.2">Des astres au front curieux,</l>
					<l n="39" num="10.3">Emporte au milieu des nuées</l>
					<l n="40" num="10.4">Le sombre galop furieux ;</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1">Et Gavarni, qui rêve encore</l>
					<l n="42" num="11.2">A leurs impudiques ardeurs,</l>
					<l n="43" num="11.3">Voit se confondre avec l’aurore</l>
					<l n="44" num="11.4">Les pourpres de ses débardeurs.</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1">Masques, danseurs, satins, amantes,</l>
					<l n="46" num="12.2">Bacchantes du long corridor,</l>
					<l n="47" num="12.3">Mer, dont les vagues écumantes</l>
					<l n="48" num="12.4">Se roulaient comme un serpent d’or ;</l>
				</lg>
				<lg n="13">
					<l n="49" num="13.1">Avec ta face inanimée,</l>
					<l n="50" num="13.2">Tu nous apparais, Carnaval,</l>
					<l n="51" num="13.3">Comme on revoit dans la fumée</l>
					<l n="52" num="13.4">Le spectre d’un combat naval !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1884">1er février 1884.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>