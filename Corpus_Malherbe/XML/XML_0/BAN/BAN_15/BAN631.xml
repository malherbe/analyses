<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ROSES DE NOËL</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>600 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_15</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">ROSES DE NOËL</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="1999">1999</date>
								</imprint>
								<biblScope unit="tome">VI</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre, éditeur</publisher>
							<date when="1891">1889-1891</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1878">1878</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN631">
				<head type="main">Nous voilà tous</head>
				<lg n="1">
					<l n="1" num="1.1">Mère, nous voilà tous, moi ton fils, qui te fête,</l>
					<l n="2" num="1.2">Et celle qui pour moi Dieu lui-même avait faite,</l>
					<l n="3" num="1.3">Et l’enfant adoré qui porte dans ses yeux</l>
					<l n="4" num="1.4">Un monde qui s’agite, encor mystérieux,</l>
					<l n="5" num="1.5">Et toi, tu nous bénis, o ma chère nourrice !</l>
					<l n="6" num="1.6">O mère, que toujours l’espoir en toi fleurisse !</l>
					<l n="7" num="1.7">Nous ne sommes pas seuls à baiser doucement</l>
					<l n="8" num="1.8">Ta tête calme où luit comme un éclair charmant.</l>
					<l n="9" num="1.9"><space quantity="2" unit="char"></space>Car lorsque dans le ciel grandit l’aube vermeille,</l>
					<l n="10" num="1.10">Le murmure étouffé de tout ce qui s’éveille</l>
					<l n="11" num="1.11">Court sur les arbres nus et sur les claires eaux.</l>
					<l n="12" num="1.12">L’air est plein du frisson des ailes des oiseaux</l>
					<l n="13" num="1.13">Et des âmes des morts et du souffle des Anges ;</l>
					<l n="14" num="1.14">Celui vers qui toujours monte un flot de louanges</l>
					<l n="15" num="1.15">Et qui de nos douleurs a fait des voluptés,</l>
					<l n="16" num="1.16">Nous dit alors tout bas : Voici l’heure. Écoutez.</l>
					<l n="17" num="1.17">Et plus faibles qu’un vol d’abeilles sur les mousses,</l>
					<l n="18" num="1.18">Nous entendons les voix qui nous semblaient si douces</l>
					<l n="19" num="1.19">Jadis ; car rien ne meurt, la tombe n’a rien pris</l>
					<l n="20" num="1.20">De la clarté sereine et pure des esprits,</l>
					<l n="21" num="1.21">Et Dieu, qui les créa dans leur splendeur première,</l>
					<l n="22" num="1.22">N’a pas fait du néant avec de la lumière.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1875"> 19 Novembre 1875.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>