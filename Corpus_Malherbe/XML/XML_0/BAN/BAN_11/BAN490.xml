<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">TRENTE-SIX BALLADES JOYEUSES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1215 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_11</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Trente-six Ballades joyeuses</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/banville36ballades.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Trente-six Ballades joyeuses</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">http://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="1999">1999</date>
								</imprint>
								<biblScope unit="tome">VI</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Genève</pubPlace>
							<publisher>Slatkine Reprints</publisher>
							<date when="1972">1972</date>
						</imprint>
						<biblScope unit="tome">VI</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1873">1873</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN490">
					<head type="number">XVI</head>
					<head type="main">Ballade sur les hôtes mystérieux de la Forêt</head>
					<lg n="1">
						<l n="1" num="1.1">Il chante encor, l’essaim railleur des fées,</l>
						<l n="2" num="1.2">Bien protégé par l’épine et le houx</l>
						<l n="3" num="1.3">Que le zéphyr caresse par bouffées.</l>
						<l n="4" num="1.4">Diane aussi, l’épouvante des loups,</l>
						<l n="5" num="1.5">Au fond des bois cache son cœur jaloux.</l>
						<l n="6" num="1.6">Son culte vit dans plus d’une chaumière.</l>
						<l n="7" num="1.7">Quand les taillis sont baignés de lumière,</l>
						<l n="8" num="1.8">A l’heure calme où la lune paraît,</l>
						<l n="9" num="1.9">Échevelée à travers la clairière,</l>
						<l n="10" num="1.10">Diane court dans la noire forêt.</l>
					</lg>
					<lg n="2">
						<l n="11" num="2.1">De nénufars et de feuilles coiffées,</l>
						<l n="12" num="2.2">La froide nixe et l’ondine aux yeux doux</l>
						<l n="13" num="2.3">Mènent le bal, follement attifées,</l>
						<l n="14" num="2.4">Et près du nain, dont les cheveux sont roux,</l>
						<l n="15" num="2.5">Les sylphes verts dansent et font les fous.</l>
						<l n="16" num="2.6">On voit passer une figure altière,</l>
						<l n="17" num="2.7">Et l’on entend au bord de la rivière</l>
						<l n="18" num="2.8">Un long sanglot, un soupir de regret</l>
						<l n="19" num="2.9">Et des pas sourds qui déchirent du lierre :</l>
						<l n="20" num="2.10">Diane court dans la noire forêt.</l>
					</lg>
					<lg n="3">
						<l n="21" num="3.1">Diane, au bois récoltant ses trophées,</l>
						<l n="22" num="3.2">Entend le cerf gémissant fuir ses coups</l>
						<l n="23" num="3.3">Et se pleurer en plaintes étouffées.</l>
						<l n="24" num="3.4">Un vent de glace a rougi ses genoux ;</l>
						<l n="25" num="3.5">Ses lévriers, ivres de son courroux,</l>
						<l n="26" num="3.6">Sont accourus à sa voix familière.</l>
						<l n="27" num="3.7">La grande Nymphe à la fauve paupière</l>
						<l n="28" num="3.8">Sur son arc d’or assujettit le trait ;</l>
						<l n="29" num="3.9">Puis, secouant sa mouvante crinière,</l>
						<l n="30" num="3.10">Diane court dans la noire forêt.</l>
					</lg>
					<lg n="4">
						<head type="form">Envoi</head>
						<l n="31" num="4.1">Prince, il est temps, fuyons cette poussière</l>
						<l n="32" num="4.2">Du carrefour, et la forêt de pierre.</l>
						<l n="33" num="4.3">Sous le feuillage et sous l’antre secret,</l>
						<l n="34" num="4.4">Nous trouverons la ville hospitalière ;</l>
						<l n="35" num="4.5">Diane court dans la noire forêt.</l>
					</lg>
					<closer>
						<dateline>
							<date when="1861">Novembre 1861.</date>
						</dateline>
					</closer>
				</div></body></text></TEI>