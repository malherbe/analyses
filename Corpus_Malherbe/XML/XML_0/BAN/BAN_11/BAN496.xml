<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">TRENTE-SIX BALLADES JOYEUSES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1215 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_11</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Trente-six Ballades joyeuses</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/banville36ballades.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Trente-six Ballades joyeuses</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">http://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="1999">1999</date>
								</imprint>
								<biblScope unit="tome">VI</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Genève</pubPlace>
							<publisher>Slatkine Reprints</publisher>
							<date when="1972">1972</date>
						</imprint>
						<biblScope unit="tome">VI</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1873">1873</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN496">
					<head type="number">XXII</head>
					<head type="main">Double Ballade des sottises de Paris</head>
					<lg n="1">
						<l n="1" num="1.1">C’est un étrange bacchanal</l>
						<l n="2" num="1.2">Dans ce Paris vraiment baroque</l>
						<l n="3" num="1.3">Où règne le petit journal,</l>
						<l n="4" num="1.4">Et qu’une drôlesse provoque</l>
						<l n="5" num="1.5">En lui laissant voir sous sa toque</l>
						<l n="6" num="1.6">Des cheveux d’un cuivre vermeil</l>
						<l n="7" num="1.7">Comme le bon or qu’elle croque.</l>
						<l n="8" num="1.8">Moi, j’en ris, les jours de soleil.</l>
					</lg>
					<lg n="2">
						<l n="9" num="2.1">Être probe est original</l>
						<l n="10" num="2.2">Dans cette Babel équivoque</l>
						<l n="11" num="2.3">Où, malgré le Code pénal,</l>
						<l n="12" num="2.4">Chacun suit les mœurs de l’époque ;</l>
						<l n="13" num="2.5">Où Scapin remplace Archiloque,</l>
						<l n="14" num="2.6">Mais où Pindare, aux Dieux pareil,</l>
						<l n="15" num="2.7">Souperait d’un œuf à la coque.</l>
						<l n="16" num="2.8">Moi, j’en ris, les jours de soleil.</l>
					</lg>
					<lg n="3">
						<l n="17" num="3.1">Dans ce pêle-mêle vénal,</l>
						<l n="18" num="3.2">Qu’est-ce que l’honneur ? Une loque</l>
						<l n="19" num="3.3">Pour amuser le tribunal,</l>
						<l n="20" num="3.4">Qu’agite, pendant son colloque,</l>
						<l n="21" num="3.5">L’avocat, soufflant comme un phoque.</l>
						<l n="22" num="3.6">Le pauvre juge, en son sommeil,</l>
						<l n="23" num="3.7">Entend ces cris de ventriloque.</l>
						<l n="24" num="3.8">Moi, j’en ris, les jours de soleil.</l>
					</lg>
					<lg n="4">
						<l n="25" num="4.1">La Bête au regard virginal</l>
						<l n="26" num="4.2">Que tout millionnaire invoque,</l>
						<l n="27" num="4.3">Prodigue son amour banal</l>
						<l n="28" num="4.4">Et chacun s’en emberlucoque.</l>
						<l n="29" num="4.5">C’est pour elle qu’on se disloque,</l>
						<l n="30" num="4.6">Et tous les cœurs sont en éveil</l>
						<l n="31" num="4.7">Dès que frémit sa pendeloque.</l>
						<l n="32" num="4.8">Moi, j’en ris, les jours de soleil.</l>
					</lg>
					<lg n="5">
						<l n="33" num="5.1">Au sein d’un tumulte infernal</l>
						<l n="34" num="5.2">Ce sont partout glaives qu’on choque,</l>
						<l n="35" num="5.3">Torches qui servent de fanal,</l>
						<l n="36" num="5.4">Mépris solide et réciproque,</l>
						<l n="37" num="5.5">Mensonges que la Haine évoque,</l>
						<l n="38" num="5.6">Idiots dont on prend conseil,</l>
						<l n="39" num="5.7">Maîtres qu’on flatte et qu’on révoque :</l>
						<l n="40" num="5.8">Moi, j’en ris, les jours de soleil.</l>
					</lg>
					<lg n="6">
						<l n="41" num="6.1">Comme une image d’Épinal,</l>
						<l n="42" num="6.2">Flamboie en sa riche défroque</l>
						<l n="43" num="6.3">Devant le café Cardinal</l>
						<l n="44" num="6.4">Ce cruel Paris, qui se moque</l>
						<l n="45" num="6.5">Des sauvages de l’Orénoque,</l>
						<l n="46" num="6.6">Et dont le superbe appareil</l>
						<l n="47" num="6.7">Indignait Thomas Vireloque :</l>
						<l n="48" num="6.8">Moi, j’en ris, les jours de soleil.</l>
					</lg>
					<closer>
						<dateline>
							<date when="1869">Juin 1869.</date>
						</dateline>
					</closer>
				</div></body></text></TEI>