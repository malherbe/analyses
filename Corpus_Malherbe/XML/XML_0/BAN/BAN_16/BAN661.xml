<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">SONNAILLES ET CLOCHETTES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3290 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_16</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">SONNAILLES ET CLOCHETTES</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="1997">1997</date>
								</imprint>
								<biblScope unit="tome">VII</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
				<monogr>
					<title>Sonnailles et clochettes</title>
					<author>Théodore de Banville</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>G. Charpentier et Cie, Éditeurs</publisher>
						<date when="1890">1890</date>
					</imprint>
				</monogr>
			</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1888">1888</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN661">
				<head type="number">XXVIII</head>
				<head type="main">Titania</head>
				<lg n="1">
					<l n="1" num="1.1">Bottom, être baigné d’azur,</l>
					<l n="2" num="1.2">Baisé dans les apothéoses,</l>
					<l n="3" num="1.3">Toujours la Reine mettra sur</l>
					<l n="4" num="1.4">Tes longues oreilles, des roses.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Au bal, où maint gardénia</l>
					<l n="6" num="2.2">Chantait son amoureuse gamme,</l>
					<l n="7" num="2.3">Je vis hier Titania.</l>
					<l n="8" num="2.4">Elle était déguisée en dame.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Car Worth, qui peinait et rêvait</l>
					<l n="10" num="3.2">Comme un forgeron dans sa forge,</l>
					<l n="11" num="3.3">Dans une robe d’or avait</l>
					<l n="12" num="3.4">Mis en captivité sa gorge.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">Dans ses cheveux doux et charmants,</l>
					<l n="14" num="4.2">Tout pareils à ces bestioles,</l>
					<l n="15" num="4.3">Cent mille et mille diamants</l>
					<l n="16" num="4.4">Brillaient comme des lucioles.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1">Elle avait je ne sais quel feu</l>
					<l n="18" num="5.2">D’espoir, de fureur et de joie</l>
					<l n="19" num="5.3">Dans les clartés de son œil bleu,</l>
					<l n="20" num="5.4">Comme un loup qui cherche sa proie.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1">Et moi, qui ne songeais à rien,</l>
					<l n="22" num="6.2">Je contemplais, en bon lyrique</l>
					<l n="23" num="6.3">Épris du ciel aérien,</l>
					<l n="24" num="6.4">La Reine du pays féerique.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1">Elle et moi, quand j’étais bandit,</l>
					<l n="26" num="7.2">Bien souvent ensemble nous rîmes.</l>
					<l n="27" num="7.3">Elle m’aperçut et me dit :</l>
					<l n="28" num="7.4">Cher Banville, marchand de rimes,</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1">Aujourd’hui le temps est fait pour</l>
					<l n="30" num="8.2">Me donner des rêves d’amante.</l>
					<l n="31" num="8.3">Je ne sais quel frisson d’amour</l>
					<l n="32" num="8.4">Court sur mes lys, et les tourmente.</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1">Je voudrais qu’en chants inégaux</l>
					<l n="34" num="9.2">Un beau mortel, suivant mon ombre,</l>
					<l n="35" num="9.3">Me récitât des madrigaux</l>
					<l n="36" num="9.4">Dans quelque boudoir un peu sombre.</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1">Ah ! lui dis-je, comment serait</l>
					<l n="38" num="10.2">L’être qui, bouffon ridicule,</l>
					<l n="39" num="10.3">Pour vous ne recommencerait</l>
					<l n="40" num="10.4">Les exploits effrayants d’Hercule ?</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1">Ne saurez-vous pas tout ravir,</l>
					<l n="42" num="11.2">Étant la fille de Shakspere ?</l>
					<l n="43" num="11.3">Demandez, faites-vous servir.</l>
					<l n="44" num="11.4">Tout Paris est là, qui soupire.</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1">Voici des seigneurs qu’il faut voir !</l>
					<l n="46" num="12.2">Ils n’ont pas traîné dans les bouges.</l>
					<l n="47" num="12.3">Au contraire, ils semblent avoir</l>
					<l n="48" num="12.4">Été cuits dans leurs habits rouges.</l>
				</lg>
				<lg n="13">
					<l n="49" num="13.1">Voici des financiers d’enfer,</l>
					<l n="50" num="13.2">Ludovic, Edgar, Anatole,</l>
					<l n="51" num="13.3">Qui dans leurs grands coffres de fer</l>
					<l n="52" num="13.4">Ont su détourner le Pactole.</l>
				</lg>
				<lg n="14">
					<l n="53" num="14.1">Puis, voici de subtils esprits</l>
					<l n="54" num="14.2">Dont le groupe heureux se dessine.</l>
					<l n="55" num="14.3">Ils sont malins, ayant appris</l>
					<l n="56" num="14.4">Le cœur des femmes — dans Racine.</l>
				</lg>
				<lg n="15">
					<l n="57" num="15.1">Tel je montrais le Tout-Paris</l>
					<l n="58" num="15.2">Brillant comme une chrysolithe.</l>
					<l n="59" num="15.3">Mais la Fée aux rires fleuris</l>
					<l n="60" num="15.4">Me dit, en voyant cette élite :</l>
				</lg>
				<lg n="16">
					<l n="61" num="16.1">Oui, pour une tête à l’envers,</l>
					<l n="62" num="16.2">Tu fais à souhait ton Plutarque ;</l>
					<l n="63" num="16.3">Tous ces Parisiens divers</l>
					<l n="64" num="16.4">Me semblent d’une bonne marque.</l>
				</lg>
				<lg n="17">
					<l n="65" num="17.1">Tous répondent à mon désir ;</l>
					<l n="66" num="17.2">Il n’en est pas que je condamne.</l>
					<l n="67" num="17.3">Mais je ne sais lequel choisir…</l>
					<l n="68" num="17.4">Car ils ont tous la tête d’âne !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1889"> 5 février 1889.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>