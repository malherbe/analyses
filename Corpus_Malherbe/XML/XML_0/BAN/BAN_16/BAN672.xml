<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">SONNAILLES ET CLOCHETTES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3290 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_16</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">SONNAILLES ET CLOCHETTES</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="1997">1997</date>
								</imprint>
								<biblScope unit="tome">VII</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
				<monogr>
					<title>Sonnailles et clochettes</title>
					<author>Théodore de Banville</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>G. Charpentier et Cie, Éditeurs</publisher>
						<date when="1890">1890</date>
					</imprint>
				</monogr>
			</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1888">1888</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN672">
				<head type="number">XXXIX</head>
				<head type="main">Concurrence</head>
				<lg n="1">
					<l n="1" num="1.1">Là-haut gronde un orage.</l>
					<l n="2" num="1.2">Le soleil plein de rage</l>
					<l n="3" num="1.3">Semble s’extasier</l>
					<l n="4" num="1.4"><space quantity="4" unit="char"></space>Dans un brasier.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Le turbulent tonnerre</l>
					<l n="6" num="2.2">Célèbre un centenaire</l>
					<l n="7" num="2.3">Au milieu des éclairs</l>
					<l n="8" num="2.4"><space quantity="4" unit="char"></space>Ardents et clairs.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">A quelle œuvre inconnue</l>
					<l n="10" num="3.2">Travaillent dans la nue</l>
					<l n="11" num="3.3">Les Chérubins riants</l>
					<l n="12" num="3.4"><space quantity="4" unit="char"></space>Des Orients ?</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">Ces faiseurs de poëmes</l>
					<l n="14" num="4.2">Ouvrent, comme nous-mêmes,</l>
					<l n="15" num="4.3">Une Exposition</l>
					<l n="16" num="4.4"><space quantity="4" unit="char"></space>Dans leur Sion.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1">On y vient du nocturne</l>
					<l n="18" num="5.2">Sirius, de Saturne,</l>
					<l n="19" num="5.3">De Vénus tout en feu</l>
					<l n="20" num="5.4"><space quantity="4" unit="char"></space>Dans l’azur bleu,</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1">Et de l’ombre où fulgure,</l>
					<l n="22" num="6.2">Détachant sa figure</l>
					<l n="23" num="6.3">Dans l’éther de safran,</l>
					<l n="24" num="6.4"><space quantity="4" unit="char"></space>Aldébaran.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1">Le Berger des étoiles</l>
					<l n="26" num="7.2">A dans ses larges toiles</l>
					<l n="27" num="7.3">Emprisonné divers</l>
					<l n="28" num="7.4"><space quantity="4" unit="char"></space>Grands univers.</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1">Là, tendant leurs échines,</l>
					<l n="30" num="8.2">D’invincibles machines</l>
					<l n="31" num="8.3">Font mouvoir les vermeils</l>
					<l n="32" num="8.4"><space quantity="4" unit="char"></space>Cœurs des Soleils.</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1">Puis, des fontaines vives</l>
					<l n="34" num="9.2">Dans leurs eaux convulsives</l>
					<l n="35" num="9.3">Roulent des firmaments</l>
					<l n="36" num="9.4"><space quantity="4" unit="char"></space>De diamants.</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1">La chevelure d’Ève</l>
					<l n="38" num="10.2">Et sa bouche de rêve</l>
					<l n="39" num="10.3">Les ont teintes de leurs</l>
					<l n="40" num="10.4"><space quantity="4" unit="char"></space>Tendres couleurs,</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1">Et des jardins étranges</l>
					<l n="42" num="11.2">Fleurissent, dont les Anges</l>
					<l n="43" num="11.3">Ailés et triomphants</l>
					<l n="44" num="11.4"><space quantity="4" unit="char"></space>Sont les Alphands.</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1">Là naissent, blancs et lisses,</l>
					<l n="46" num="12.2">Ouvrant leurs purs calices</l>
					<l n="47" num="12.3">Près des amaryllis,</l>
					<l n="48" num="12.4"><space quantity="4" unit="char"></space>D’immenses lys,</l>
				</lg>
				<lg n="13">
					<l n="49" num="13.1">Et des roses farouches,</l>
					<l n="50" num="13.2">Pareilles à des bouches</l>
					<l n="51" num="13.3">Que tout baise à l’entour,</l>
					<l n="52" num="13.4"><space quantity="4" unit="char"></space>Disent : Amour !</l>
				</lg>
				<lg n="14">
					<l n="53" num="14.1">Parmi l’or des fournaises</l>
					<l n="54" num="14.2">Dansent des Javanaises</l>
					<l n="55" num="14.3">Venant d’une Java</l>
					<l n="56" num="14.4"><space quantity="4" unit="char"></space>Où nul ne va.</l>
				</lg>
				<lg n="15">
					<l n="57" num="15.1">Mille milliers de rimes</l>
					<l n="58" num="15.2">S’éparpillent, sublimes,</l>
					<l n="59" num="15.3">En un glorieux chant ;</l>
					<l n="60" num="15.4"><space quantity="4" unit="char"></space>Et se penchant</l>
				</lg>
				<lg n="16">
					<l n="61" num="16.1">Vers les grands téléphones,</l>
					<l n="62" num="16.2">Hurlent des Tisiphones</l>
					<l n="63" num="16.3">Et parlent en mots fins</l>
					<l n="64" num="16.4"><space quantity="4" unit="char"></space>Les Séraphins.</l>
				</lg>
				<lg n="17">
					<l n="65" num="17.1">Dans la nue électrique,</l>
					<l n="66" num="17.2">Dieu, puissamment lyrique,</l>
					<l n="67" num="17.3">Lutte avec Edison</l>
					<l n="68" num="17.4"><space quantity="4" unit="char"></space>A sa façon ;</l>
				</lg>
				<lg n="18">
					<l n="69" num="18.1">Et de ses mains profondes,</l>
					<l n="70" num="18.2">L’ingénieur des mondes</l>
					<l n="71" num="18.3">Construit dans le plein ciel</l>
					<l n="72" num="18.4"><space quantity="4" unit="char"></space>Sa Tour Eiffel !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1889"> 9 juillet 1889.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>