<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">SONNAILLES ET CLOCHETTES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3290 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_16</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">SONNAILLES ET CLOCHETTES</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="1997">1997</date>
								</imprint>
								<biblScope unit="tome">VII</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
				<monogr>
					<title>Sonnailles et clochettes</title>
					<author>Théodore de Banville</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>G. Charpentier et Cie, Éditeurs</publisher>
						<date when="1890">1890</date>
					</imprint>
				</monogr>
			</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1888">1888</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN662">
				<head type="number">XXIX</head>
				<head type="main">Neige</head>
				<lg n="1">
					<l n="1" num="1.1">La neige tombe en flocons</l>
					<l n="2" num="1.2">Sur les toits, sur les balcons.</l>
					<l n="3" num="1.3">C’est à se croire en Norvège.</l>
					<l n="4" num="1.4">Les gazons gèlent, tapis</l>
					<l n="5" num="1.5">Sous un merveilleux tapis.</l>
					<l n="6" num="1.6">Car il neige, il neige, il neige.</l>
				</lg>
				<lg n="2">
					<l n="7" num="2.1">Pour combiner, en ses jeux,</l>
					<l n="8" num="2.2">Un effet de blanc neigeux,</l>
					<l n="9" num="2.3">Le ciel a jeté ses perles.</l>
					<l n="10" num="2.4">Mais ces parures de cour</l>
					<l n="11" num="2.5">Sont un mince régal, pour</l>
					<l n="12" num="2.6">Les moineaux et pour les merles.</l>
				</lg>
				<lg n="3">
					<l n="13" num="3.1">Par ce temps, ils n’errent pas.</l>
					<l n="14" num="3.2">Mais enfin, pour quel repas</l>
					<l n="15" num="3.3">Cette nappe est-elle mise ?</l>
					<l n="16" num="3.4">La Terre, montrant son flanc,</l>
					<l n="17" num="3.5">Est dans un vêtement blanc,</l>
					<l n="18" num="3.6">Comme une dame en chemise.</l>
				</lg>
				<lg n="4">
					<l n="19" num="4.1">Or, mesdames, le rimeur</l>
					<l n="20" num="4.2">Se livre à sa belle humeur,</l>
					<l n="21" num="4.3">Et sur les routes divines</l>
					<l n="22" num="4.4">Aux harmonieux dessins,</l>
					<l n="23" num="4.5">Voit les blancheurs de vos seins</l>
					<l n="24" num="4.6">Et celles de vos poitrines.</l>
				</lg>
				<lg n="5">
					<l n="25" num="5.1">Sous la neige ensevelis,</l>
					<l n="26" num="5.2">Mais levant leurs fronts pâlis</l>
					<l n="27" num="5.3">Que le vent ne peut abattre,</l>
					<l n="28" num="5.4">Les arbres un peu tremblants,</l>
					<l n="29" num="5.5">Ont tous des panaches blancs,</l>
					<l n="30" num="5.6">Comme le roi Henri Quatre.</l>
				</lg>
				<lg n="6">
					<l n="31" num="6.1">Les petits dos féminins</l>
					<l n="32" num="6.2">Sont comme des Apennins ;</l>
					<l n="33" num="6.3">Et Flavie, Emma, Nadège,</l>
					<l n="34" num="6.4">Pour qui j’enfle mes pipeaux,</l>
					<l n="35" num="6.5">Sur leurs élégants chapeaux</l>
					<l n="36" num="6.6">Emportent des fleurs de neige.</l>
				</lg>
				<lg n="7">
					<l n="37" num="7.1">Des loups, terreur des marmots,</l>
					<l n="38" num="7.2">Pénètrent dans les hameaux,</l>
					<l n="39" num="7.3">Plus sérieux que des mages,</l>
					<l n="40" num="7.4">Et si j’en crois le journal,</l>
					<l n="41" num="7.5">On en voit dans Épinal,</l>
					<l n="42" num="7.6">Où se vendent les images.</l>
				</lg>
				<lg n="8">
					<l n="43" num="8.1">Ces loups, fuyant nos paris,</l>
					<l n="44" num="8.2">Ne viennent pas à Paris.</l>
					<l n="45" num="8.3">Mais dans ce Paris, qui m’aime</l>
					<l n="46" num="8.4">Et qui, malgré les méchants,</l>
					<l n="47" num="8.5">Écoute parfois nos chants,</l>
					<l n="48" num="8.6">Nous en avons tout de même.</l>
				</lg>
				<lg n="9">
					<l n="49" num="9.1">Ces chasseurs qui passent dans</l>
					<l n="50" num="9.2">La ville, ont du sang aux dents.</l>
					<l n="51" num="9.3">O Balzac ! c’est toi qui trouves</l>
					<l n="52" num="9.4">Ces meurtriers, ces filous ;</l>
					<l n="53" num="9.5">Nous avons beaucoup de loups,</l>
					<l n="54" num="9.6">Et même aussi, quelques louves.</l>
				</lg>
				<lg n="10">
					<l n="55" num="10.1">Sveltes comme des fuseaux,</l>
					<l n="56" num="10.2">Elles tendent leurs museaux</l>
					<l n="57" num="10.3">De bêtes aventurières,</l>
					<l n="58" num="10.4">Et plus d’un sage barbon</l>
					<l n="59" num="10.5">Estime qu’il serait bon</l>
					<l n="60" num="10.6">D’exterminer ces guerrières.</l>
				</lg>
				<lg n="11">
					<l n="61" num="11.1">Mais le prudent louvetier</l>
					<l n="62" num="11.2">Veut bien les amnistier,</l>
					<l n="63" num="11.3">Si leur candeur les protège ;</l>
					<l n="64" num="11.4">Et ne soyez pas surpris</l>
					<l n="65" num="11.5">Que ces louves de Paris</l>
					<l n="66" num="11.6">Aient la blancheur de la neige !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1889"> 19 février 1889.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>