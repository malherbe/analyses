<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">SONNAILLES ET CLOCHETTES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3290 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_16</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">SONNAILLES ET CLOCHETTES</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="1997">1997</date>
								</imprint>
								<biblScope unit="tome">VII</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
				<monogr>
					<title>Sonnailles et clochettes</title>
					<author>Théodore de Banville</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>G. Charpentier et Cie, Éditeurs</publisher>
						<date when="1890">1890</date>
					</imprint>
				</monogr>
			</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1888">1888</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN655">
				<head type="number">XXII</head>
				<head type="main">Peinture</head>
				<lg n="1">
					<l n="1" num="1.1">Elle était blanche comme un cygne</l>
					<l n="2" num="1.2">Et parfaitement nue, et puis</l>
					<l n="3" num="1.3">Sans aucune feuille de vigne,</l>
					<l n="4" num="1.4">Car elle sortait de son puits.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Moi, je pris le blanc et le rouge</l>
					<l n="6" num="2.2">Et sur son visage et son flanc,</l>
					<l n="7" num="2.3">Sans souci du zéphyr qui bouge,</l>
					<l n="8" num="2.4">Fier, je mis du rouge et du blanc.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Avec ces couleurs de théâtre,</l>
					<l n="10" num="3.2">Que sans remords nous étalons,</l>
					<l n="11" num="3.3">Je maquillai son corps folâtre</l>
					<l n="12" num="3.4">De la nuque jusqu’aux talons.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">Et je disais : Le Beau respire</l>
					<l n="14" num="4.2">Chez l’auteur suave et mesquin ;</l>
					<l n="15" num="4.3">Aussi doit-on, fuyant Shakspere,</l>
					<l n="16" num="4.4">Aimer éperdûment Berquin !</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1">Épris de la gloire et du lucre,</l>
					<l n="18" num="5.2">Il serait bon qu’on les briguât</l>
					<l n="19" num="5.3">Avec une Revue en sucre</l>
					<l n="20" num="5.4">Et des romanciers en nougat !</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1">Pas de choses éblouissantes !</l>
					<l n="22" num="6.2">Foin de la Rose au cœur vermeil !</l>
					<l n="23" num="6.3">Surtout craignez les indécentes</l>
					<l n="24" num="6.4">Éclaboussures de soleil !</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1">On peut célébrer le Hanôvre,</l>
					<l n="26" num="7.2">Ou Londres, avec Tom et Bob ;</l>
					<l n="27" num="7.3">Mais que la rime soit très pauvre !</l>
					<l n="28" num="7.4">Oh ! beaucoup plus pauvre que Job !</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1">Certes, le vrai morceau de prince</l>
					<l n="30" num="8.2">Qu’il faut louanger en un lai,</l>
					<l n="31" num="8.3">C’est une demoiselle, mince</l>
					<l n="32" num="8.4">Comme un svelte manche à balai.</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1">Le style très sobre, sans honte</l>
					<l n="34" num="9.2">Avec la vertu correspond :</l>
					<l n="35" num="9.3">Ce sont les vrais lions de fonte</l>
					<l n="36" num="9.4">Qui rugissent au bout du pont !</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1">Ainsi je parlais, magnanime,</l>
					<l n="38" num="10.2">Tâchant, dans ma péroraison,</l>
					<l n="39" num="10.3">D’agenouiller la nymphe Rime</l>
					<l n="40" num="10.4">Sous le dur fouet de la Raison.</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1">Et toujours, avec politesse,</l>
					<l n="42" num="11.2">Éteignant la pourpre du sang,</l>
					<l n="43" num="11.3">Parmi les lys de la déesse</l>
					<l n="44" num="11.4">Je mettais du rouge et du blanc.</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1">Et comme, en cette ardente fièvre,</l>
					<l n="46" num="12.2">Sur le rouge, sans l’effacer,</l>
					<l n="47" num="12.3">Je passais la patte de lièvre,</l>
					<l n="48" num="12.4">Un critique vint à passer.</l>
				</lg>
				<lg n="13">
					<l n="49" num="13.1">Alors, tout à coup faisant halte,</l>
					<l n="50" num="13.2">Oubliant sa rédaction,</l>
					<l n="51" num="13.3">Il admira, droit sur l’asphalte,</l>
					<l n="52" num="13.4">Mes discours et mon action.</l>
				</lg>
				<lg n="14">
					<l n="53" num="14.1">Que vois-je ? Quel est ce prodige ?</l>
					<l n="54" num="14.2">Dit-il avec sévérité.</l>
					<l n="55" num="14.3">Que faites-vous là ? — Moi ? lui dis-je ;</l>
					<l n="56" num="14.4">Mais — je farde la Vérité !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1888"> 13 novembre 1888.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>