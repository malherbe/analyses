<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">IDYLLES PRUSSIENNES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2876 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_10</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Idylles prussiennes</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/banvilleidyllesprussienes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Idylles prussiennes</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="1999">1999</date>
								</imprint>
								<biblScope unit="tome">VI</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre, éditeur</publisher>
							<date when="1891">1889-1891</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Genève</pubPlace>
							<publisher>Slatkine Reprints</publisher>
							<date when="1972">1972</date>
						</imprint>
						<biblScope unit="tome">IV</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN418">
				<head type="main">La Soirée</head>
				<lg n="1">
					<l n="1" num="1.1">Lorsqu’en revenant du rempart</l>
					<l n="2" num="1.2">Où, plein d’une foi chaleureuse,</l>
					<l n="3" num="1.3">Il a bien veillé pour sa part,</l>
					<l n="4" num="1.4">Le père quitte sa vareuse,</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">En voilà jusqu’au lendemain !</l>
					<l n="6" num="2.2">Il t’oublie, aigre vent qui souffles</l>
					<l n="7" num="2.3">Sur les talus, et, d’une main</l>
					<l n="8" num="2.4">Réjouie, il met ses pantoufles.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Après avoir dîné sans bruit,</l>
					<l n="10" num="3.2">Il regardera quelque estampe</l>
					<l n="11" num="3.3">Ou bien lira jusqu’à minuit</l>
					<l n="12" num="3.4">Aux douces clartés de la lampe,</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">Avec sa femme et ses enfants,</l>
					<l n="14" num="4.2">Amusant l’un d’eux sur sa jambe</l>
					<l n="15" num="4.3">Et voyant leurs fronts triomphants</l>
					<l n="16" num="4.4">Luire aux clartés du feu qui flambe.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1">Il caresse complaisamment</l>
					<l n="18" num="5.2">Cette jeune et chère couvée</l>
					<l n="19" num="5.3">Et suit avec un œil d’amant</l>
					<l n="20" num="5.4">Sa compagne enfin retrouvée,</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1">Qui, charmante en sa floraison,</l>
					<l n="22" num="6.2">Sous le clair regard qui l’admire</l>
					<l n="23" num="6.3">Se promène dans la maison</l>
					<l n="24" num="6.4">Qu’elle éclaire de son sourire.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1">Alors le père tout heureux</l>
					<l n="26" num="7.2">Ne regrette ni les théâtres,</l>
					<l n="27" num="7.3">Où des cailloux aventureux</l>
					<l n="28" num="7.4">Ornaient de fausses Cléopâtres,</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1">Ni les cafés, plus laids encor,</l>
					<l n="30" num="8.2">Où des Phrynés aux blancheurs mates</l>
					<l n="31" num="8.3">Flamboyaient sous leurs cheveux d’or,</l>
					<l n="32" num="8.4">Comme des bêtes écarlates.</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1">Plus de cercles, où par monceau</l>
					<l n="34" num="9.2">L’or tombait, et ruisselait comme</l>
					<l n="35" num="9.3">L’eau méprisable du ruisseau !</l>
					<l n="36" num="9.4">La femme a retrouvé son homme,</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1">Et chacun reste avec les siens,</l>
					<l n="38" num="10.2">Riant à l’enfant qui babille,</l>
					<l n="39" num="10.3">Grâce à messieurs les Prussiens,</l>
					<l n="40" num="10.4">Qui nous ont rendu la famille !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1870">Octobre 1870.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>