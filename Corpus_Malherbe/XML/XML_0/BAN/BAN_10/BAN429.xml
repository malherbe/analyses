<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">IDYLLES PRUSSIENNES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2876 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_10</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Idylles prussiennes</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/banvilleidyllesprussienes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Idylles prussiennes</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="1999">1999</date>
								</imprint>
								<biblScope unit="tome">VI</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre, éditeur</publisher>
							<date when="1891">1889-1891</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Genève</pubPlace>
							<publisher>Slatkine Reprints</publisher>
							<date when="1972">1972</date>
						</imprint>
						<biblScope unit="tome">IV</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN429">
				<head type="main">L’Histoire</head>
				<lg n="1">
					<l n="1" num="1.1">Bismarck en soldat qu’on redoute</l>
					<l n="2" num="1.2">Parle, et, sans le contrarier,</l>
					<l n="3" num="1.3">L’austère Déesse l’écoute,</l>
					<l n="4" num="1.4">Pensive sous son vert laurier.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Oui, dit le chancelier, en somme,</l>
					<l n="6" num="2.2">Berger ou comte palatin,</l>
					<l n="7" num="2.3">Monarque ou mendiant, tout homme</l>
					<l n="8" num="2.4">Est l’artisan de son destin.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Qu’il porte la pourpre ou la bure,</l>
					<l n="10" num="3.2">Pauvre ou détenteur d’un trésor,</l>
					<l n="11" num="3.3">Qu’il soit né dans la foule obscure</l>
					<l n="12" num="3.4">Ou sur le trône aux franges d’or,</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">Ses œuvres, dont il est le maître</l>
					<l n="14" num="4.2">Et dont il n’a pas hérité,</l>
					<l n="15" num="4.3">Décideront ce qu’il doit être,</l>
					<l n="16" num="4.4">Même pour la postérité !</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1">Cet assassin à tête blonde</l>
					<l n="18" num="5.2">Qui prend la lyre d’Arion,</l>
					<l n="19" num="5.3">Néron, quoique maître du monde,</l>
					<l n="20" num="5.4">N’est qu’un insipide histrion.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1">Alexandre suit sa chimère</l>
					<l n="22" num="6.2">Comme un soldat sans feu ni lieu,</l>
					<l n="23" num="6.3">Et cependant l’aveugle Homère</l>
					<l n="24" num="6.4">De mendiant devient un dieu.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1">On ne saurait tromper la gloire</l>
					<l n="26" num="7.2">Devant l’avenir indigné.</l>
					<l n="27" num="7.3">Que devient un titre illusoire</l>
					<l n="28" num="7.4">Si nous ne l’avons pas gagné ?</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1">Murat, qui, d’un geste bravache</l>
					<l n="30" num="8.2">Voulant fendre en deux les cieux clairs,</l>
					<l n="31" num="8.3">Va, faisant siffler sa cravache</l>
					<l n="32" num="8.4">Parmi la foudre et les éclairs,</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1">Qu’est-il pour la France hautaine,</l>
					<l n="34" num="9.2">Pour cette guerrière aux yeux bleus ?</l>
					<l n="35" num="9.3">Un roi ? non ; mais un capitaine,</l>
					<l n="36" num="9.4">Un vague Roland fabuleux,</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1">Un courtisan de l’aventure.</l>
					<l n="38" num="10.2">Et Marceau, tenant dans sa main</l>
					<l n="39" num="10.3">Son épée invincible et pure,</l>
					<l n="40" num="10.4">Est plus grand qu’un César romain !</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1">C’est pourquoi, Déesse, si j’ose</l>
					<l n="42" num="11.2">Agir comme un roi, je suis roi,</l>
					<l n="43" num="11.3">Créant ma propre apothéose !</l>
					<l n="44" num="11.4">Bismarck, par ces mots qui font loi,</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1">D’une manière péremptoire</l>
					<l n="46" num="12.2">Achève sa péroraison.</l>
					<l n="47" num="12.3">Brigadier, lui répond l’Histoire,</l>
					<l n="48" num="12.4">Brigadier, vous avez raison !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1870">Octobre 1870.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>