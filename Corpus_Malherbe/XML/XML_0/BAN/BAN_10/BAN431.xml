<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">IDYLLES PRUSSIENNES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2876 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_10</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Idylles prussiennes</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/banvilleidyllesprussienes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Idylles prussiennes</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="1999">1999</date>
								</imprint>
								<biblScope unit="tome">VI</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre, éditeur</publisher>
							<date when="1891">1889-1891</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Genève</pubPlace>
							<publisher>Slatkine Reprints</publisher>
							<date when="1972">1972</date>
						</imprint>
						<biblScope unit="tome">IV</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN431">
				<head type="main">Le Jour des Morts</head>
				<lg n="1">
					<l n="1" num="1.1">Je prends ces fleurs, dont les corolles</l>
					<l n="2" num="1.2">Ont encor des souffles vivants,</l>
					<l n="3" num="1.3">Et sur l’aile des brises folles</l>
					<l n="4" num="1.4">Je les disperse aux quatre vents.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Dans l’ombre où, tombés avec joie,</l>
					<l n="6" num="2.2">Vous frissonnez pâles et nus,</l>
					<l n="7" num="2.3">C’est à vous que je les envoie,</l>
					<l n="8" num="2.4">O soldats ! ô morts inconnus !</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">O soldats morts pour la patrie !</l>
					<l n="10" num="3.2">Qui, déjà glacés et mourants,</l>
					<l n="11" num="3.3">L’avez acclamée et chérie,</l>
					<l n="12" num="3.4">O mes frères ! ô mes parents !</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">O ma généreuse famille !</l>
					<l n="14" num="4.2">O parure de nos malheurs !</l>
					<l n="15" num="4.3">Ces fleurs dont la corolle brille,</l>
					<l n="16" num="4.4">Je vous les offre avec mes pleurs.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1">O mobiles, gais et superbes,</l>
					<l n="18" num="5.2">Si voisins de l’enfance encor,</l>
					<l n="19" num="5.3">Avec vos visages imberbes</l>
					<l n="20" num="5.4">Et vos cheveux aux reflets d’or !</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1">Cavaliers, soldats de la ligne,</l>
					<l n="22" num="6.2">Turcos, par le soleil brûlés,</l>
					<l n="23" num="6.3">Vétérans au courage insigne,</l>
					<l n="24" num="6.4">Chasseurs d’Afrique aux fronts hâlés !</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1">Où dormez-vous ? Pour vous sourire,</l>
					<l n="26" num="7.2">Où peut-on se mettre à genoux,</l>
					<l n="27" num="7.3">Héros qui voliez au martyre</l>
					<l n="28" num="7.4">Et qui l’avez souffert pour nous ?</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1">Nous l’ignorons. C’est là peut-être.</l>
					<l n="30" num="8.2">Qui peut le dire ? Et c’est pourquoi,</l>
					<l n="31" num="8.3">Lorsque enfin nous allons renaître,</l>
					<l n="32" num="8.4">Pleins de bravoure et pleins de foi,</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1">Après ces longs jours de souffrance,</l>
					<l n="34" num="9.2">De haine et de meurtre exécré,</l>
					<l n="35" num="9.3">Le sol tout entier de la France</l>
					<l n="36" num="9.4">Nous sera désormais sacré.</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1">Foule par la guerre immolée,</l>
					<l n="38" num="10.2">Nous adorerons en tout temps</l>
					<l n="39" num="10.3">Cette terre partout mêlée</l>
					<l n="40" num="10.4">A votre cendre, ô combattants !</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1">Et quand la Paix aux mains fleuries</l>
					<l n="42" num="11.2">Aura, nourrice des chansons,</l>
					<l n="43" num="11.3">Ravivé l’herbe des prairies</l>
					<l n="44" num="11.4">Et les fleurettes des buissons,</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1">Vos sœurs, vos mères, vos amantes</l>
					<l n="46" num="12.2">Viendront dans les champs embaumés,</l>
					<l n="47" num="12.3">Parmi les campagnes charmantes,</l>
					<l n="48" num="12.4">Chercher la place où vous dormez,</l>
				</lg>
				<lg n="13">
					<l n="49" num="13.1">Pâles d’une espérance folle,</l>
					<l n="50" num="13.2">Et, rêveuses, suivant des yeux</l>
					<l n="51" num="13.3">Le ruisseau pourpré qui s’envole</l>
					<l n="52" num="13.4">Avec un bruit mystérieux, —</l>
				</lg>
				<lg n="14">
					<l n="53" num="14.1">La colline où frémit le tremble,</l>
					<l n="54" num="14.2">Le nid d’où l’oiseau s’envola</l>
					<l n="55" num="14.3">Et la place où le rosier tremble,</l>
					<l n="56" num="14.4">Se diront : C’est peut-être là !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1870">Novembre 1870.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>