<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">IDYLLES PRUSSIENNES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2876 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_10</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Idylles prussiennes</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/banvilleidyllesprussienes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Idylles prussiennes</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="1999">1999</date>
								</imprint>
								<biblScope unit="tome">VI</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre, éditeur</publisher>
							<date when="1891">1889-1891</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Genève</pubPlace>
							<publisher>Slatkine Reprints</publisher>
							<date when="1972">1972</date>
						</imprint>
						<biblScope unit="tome">IV</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN447">
				<head type="main">Scapin tout seul</head>
				<lg n="1">
					<l n="1" num="1.1">Or un nouvel acteur bouffon</l>
					<l n="2" num="1.2">Vient, jouant le tortionnaire,</l>
					<l n="3" num="1.3">Prendre son haleine au typhon</l>
					<l n="4" num="1.4">Et ses hurlements au tonnerre.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Sans tache, comme un aubépin,</l>
					<l n="6" num="2.2">Il porte, dans sa gloire insigne,</l>
					<l n="7" num="2.3">L’habit blanc qui sied à Scapin,</l>
					<l n="8" num="2.4">Couleur de la neige et du cygne.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Mais il perce l’azur du ciel</l>
					<l n="10" num="3.2">Avec sa moustache effroyable</l>
					<l n="11" num="3.3">Qui n’a rien d’artificiel,</l>
					<l n="12" num="3.4">Et, sacrant toujours comme un diable,</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">Il fait rage avec son manteau,</l>
					<l n="14" num="4.2">Comme pour éteindre Gomorrhe,</l>
					<l n="15" num="4.3">Car il fait les don Spavento,</l>
					<l n="16" num="4.4">Les Fracasse et les Matamore.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1">Ah ! tête ! Ah ! ventre ! Ah ! Belphégor !</l>
					<l n="18" num="5.2">Dit-il, qui faut-il que je perce</l>
					<l n="19" num="5.3">Tout d’abord, ou le grand mogor</l>
					<l n="20" num="5.4">Ou bien le grand sophi de Perse ?</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1">Donnons. Ferme. Poussons. Tenez.</l>
					<l n="22" num="6.2">Ah ! morbleu ! si je m’évertue !…</l>
					<l n="23" num="6.3">Soutenez, marauds, soutenez.</l>
					<l n="24" num="6.4">Ah ! coquins ! Ah ! canaille ! Tue !</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1">Il reprend : J’ai mis aujourd’hui</l>
					<l n="26" num="7.2">Mars et Jupiter dans les bagnes.</l>
					<l n="27" num="7.3">Ah ! veillaques ! je suis celui</l>
					<l n="28" num="7.4">Dont le fer tranche les montagnes !</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1">Surtout, s’il a peur de l’éclair,</l>
					<l n="30" num="8.2">Que nul, quelle que soit sa taille,</l>
					<l n="31" num="8.3">N’aille, ni dans l’eau ni dans l’air,</l>
					<l n="32" num="8.4">Franchir mes lignes de bataille !</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1">Fût-ce un pigeon qui suit le vent,</l>
					<l n="34" num="9.2">Je ne m’en inquiète guère ;</l>
					<l n="35" num="9.3">Le pigeon passera devant</l>
					<l n="36" num="9.4">Les juges du conseil de guerre.</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1">Pour les dépêches, qu’en ballon</l>
					<l n="38" num="10.2">La brise emporte par surprise,</l>
					<l n="39" num="10.3">Elles me trahissent, et l’on</l>
					<l n="40" num="10.4">Jugera, s’il le faut, la brise.</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1">Et si, tremblantes à demi,</l>
					<l n="42" num="11.2">Les étoiles, ouvrant leurs voiles,</l>
					<l n="43" num="11.3">Renseignent sur moi l’ennemi,</l>
					<l n="44" num="11.4">Je fusillerai les étoiles !</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1">Parlant ainsi, lorsqu’il s’émeut,</l>
					<l n="46" num="12.2">De massacres et de désastres,</l>
					<l n="47" num="12.3">Matamore fait ce qu’il peut</l>
					<l n="48" num="12.4">Pour ferrailler contre les astres.</l>
				</lg>
				<lg n="13">
					<l n="49" num="13.1">Et lorsque, non sans un soupir,</l>
					<l n="50" num="13.2">Planté devant un mur d’auberge,</l>
					<l n="51" num="13.3">Il a tailladé le zéphyr,</l>
					<l n="52" num="13.4">Il essuie encor sa flamberge.</l>
				</lg>
				<lg n="14">
					<l n="53" num="14.1">C’est ainsi que, cherchant le trait,</l>
					<l n="54" num="14.2">Par ces époques insalubres,</l>
					<l n="55" num="14.3">Monsieur de Bismarck se distrait</l>
					<l n="56" num="14.4">En jouant les Scapins lugubres.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1870">Décembre 1870.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>