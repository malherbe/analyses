<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES STALACTITES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1359 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_9</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LES STALACTITES</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/banvillelesstalactictes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
				<monogr>
					<title>Œuvres de Théodore de Banville. Les stalactites, Odelettes, Améthystes, Le Forgeron.</title>
					<author>Théodore de Banville</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Alphonse Lemerre, éditeur</publisher>
						<date when="1889">1889</date>
					</imprint>
				</monogr>
				<note>Édition de référence pour les corrections métriques</note>
			</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1846">1843-1846</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN391">
				<lg n="1">
					<l n="1" num="1.1">Camille, quand la Nuit t’endort sous ses grands voiles ;</l>
					<l n="2" num="1.2">Quand un rêve céleste emplit tes yeux d’étoiles ;</l>
					<l n="3" num="1.3">Quand tes regards, lassés des fatigues du jour,</l>
					<l n="4" num="1.4">Se reposent partout sur des routes fleuries</l>
					<l n="5" num="1.5">Dans le pays charmant des molles rêveries,</l>
					<l n="6" num="1.6">Camille, que vois-tu dans tes songes d’amour ?</l>
				</lg>
				<lg n="2">
					<l n="7" num="2.1">Nous vois-tu, revenant par les noires allées,</l>
					<l n="8" num="2.2">Tous deux, donner des pleurs aux choses envolées</l>
					<l n="9" num="2.3">Que l’oubli dédaigneux couvre de flots dormants,</l>
					<l n="10" num="2.4">Ou dans le vieux manoir, au fond des parcs superbes,</l>
					<l n="11" num="2.5">Pousser de l’éperon parmi les hautes herbes</l>
					<l n="12" num="2.6">Les pas précipités de nos chevaux fumants ?</l>
				</lg>
				<lg n="3">
					<l n="13" num="3.1">Dans les moires de l’eau dont l’azur étincelle,</l>
					<l n="14" num="3.2">Nous vois-tu laissant fuir une frêle nacelle</l>
					<l n="15" num="3.3">Sur le grand lac paisible et frémissant d’accords,</l>
					<l n="16" num="3.4">Où devant les grands bois et les coteaux de vignes,</l>
					<l n="17" num="3.5">Glisse amoureusement la blancheur des beaux cygnes,</l>
					<l n="18" num="3.6">Aux accents mariés des harpes et des cors ?</l>
				</lg>
				<lg n="4">
					<l n="19" num="4.1">Moi, je vois rayonner tes yeux dans la nuit sombre,</l>
					<l n="20" num="4.2">Et je songe à ce jour où je sentis dans l’ombre,</l>
					<l n="21" num="4.3">Pour la première fois, de ton col renversé</l>
					<l n="22" num="4.4">Tombant à larges flots avec leur splendeur fière,</l>
					<l n="23" num="4.5">Tes cheveux d’or emplir mes deux mains de lumière,</l>
					<l n="24" num="4.6">Et ta lèvre de feu baiser mon front glacé.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1844">Août 1844.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>