<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES STALACTITES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1359 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_9</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LES STALACTITES</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/banvillelesstalactictes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
				<monogr>
					<title>Œuvres de Théodore de Banville. Les stalactites, Odelettes, Améthystes, Le Forgeron.</title>
					<author>Théodore de Banville</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Alphonse Lemerre, éditeur</publisher>
						<date when="1889">1889</date>
					</imprint>
				</monogr>
				<note>Édition de référence pour les corrections métriques</note>
			</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1846">1843-1846</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN398">
				<head type="main">Élégie</head>
				<opener>
					<epigraph>
						<cit>
							<quote>
								Gallus et Hesperiis, et Gallus notus Eoïs <lb></lb>
								Et sua cum Gallo nota Lycoris erit.
							</quote>
							<bibl>
								<name>Ovide</name>.
							</bibl>
						</cit>
					</epigraph>
				</opener>
				<lg n="1">
					<l n="1" num="1.1">Tombez dans mon cœur, souvenirs confus,</l>
					<l n="2" num="1.2"><space quantity="8" unit="char"></space>Du haut des branches touffues !</l>
				</lg>
				<lg n="2">
					<l n="3" num="2.1">Oh ! parlez-moi d’elle, antres et rochers,</l>
					<l n="4" num="2.2"><space quantity="8" unit="char"></space>Retraites à tous cachées !</l>
				</lg>
				<lg n="3">
					<l n="5" num="3.1">Parlez, parlez d’elle, ô sentiers fleuris !</l>
					<l n="6" num="3.2"><space quantity="8" unit="char"></space>Bois, ruisseaux, vertes prairies !</l>
				</lg>
				<lg n="4">
					<l n="7" num="4.1">O charmes amers ! dans ce frais décor</l>
					<l n="8" num="4.2"><space quantity="8" unit="char"></space>Elle m’apparaît encore.</l>
				</lg>
				<lg n="5">
					<l n="9" num="5.1">C’est elle, ô mon cœur ! sur ces gazons verts,</l>
					<l n="10" num="5.2"><space quantity="8" unit="char"></space>Au milieu des primevères !</l>
				</lg>
				<lg n="6">
					<l n="11" num="6.1">Je vois s’envoler ses fins cheveux d’or</l>
					<l n="12" num="6.2"><space quantity="8" unit="char"></space>Au zéphyr qui les adore,</l>
				</lg>
				<lg n="7">
					<l n="13" num="7.1">Et notre amandier couvre son beau cou</l>
					<l n="14" num="7.2"><space quantity="8" unit="char"></space>Des blanches fleurs qu’il secoue !</l>
				</lg>
				<lg n="8">
					<l n="15" num="8.1">Sur mon bras frémit son bras ingénu,</l>
					<l n="16" num="8.2"><space quantity="8" unit="char"></space>Et frissonne sa main nue.</l>
				</lg>
				<lg n="9">
					<l n="17" num="9.1">Le feuillage est noir, le ciel étoilé,</l>
					<l n="18" num="9.2"><space quantity="8" unit="char"></space>Viens, suivons la noire allée !</l>
				</lg>
				<lg n="10">
					<l n="19" num="10.1">La belle-de-nuit s’ouvre toute en feu,</l>
					<l n="20" num="10.2"><space quantity="8" unit="char"></space>La voûte du ciel est bleue.</l>
				</lg>
				<lg n="11">
					<l n="21" num="11.1">Écoutez, ma mie, au coin du vieux mur,</l>
					<l n="22" num="11.2"><space quantity="8" unit="char"></space>Le rossignol qui murmure.</l>
				</lg>
				<lg n="12">
					<l n="23" num="12.1">Chante ta chanson, ô doux rossignol !</l>
					<l n="24" num="12.2"><space quantity="8" unit="char"></space>Ta chanson qui nous console,</l>
				</lg>
				<lg n="13">
					<l n="25" num="13.1">Et que pour toi seul, à côté du lys,</l>
					<l n="26" num="13.2"><space quantity="8" unit="char"></space>La rose ouvre son calice !</l>
				</lg>
				<lg n="14">
					<l n="27" num="14.1">Des yeux tant aimés tombe un divin pleur</l>
					<l n="28" num="14.2"><space quantity="8" unit="char"></space>Sur ma tempe qu’il effleure.</l>
				</lg>
				<lg n="15">
					<l n="29" num="15.1">O larme d’amour, trésor sans pareil !</l>
					<l n="30" num="15.2"><space quantity="8" unit="char"></space>Dites-moi si je sommeille ?</l>
				</lg>
				<lg n="16">
					<l n="31" num="16.1">Qui t’envoie, hélas ! charmant souvenir,</l>
					<l n="32" num="16.2"><space quantity="8" unit="char"></space>Briser mon cœur qui soupire ?</l>
				</lg>
				<lg n="17">
					<l n="33" num="17.1">Hélas ! je suis seul dans ces bois épars</l>
					<l n="34" num="17.2"><space quantity="8" unit="char"></space>Où résonnaient les guitares.</l>
				</lg>
				<lg n="18">
					<l n="35" num="18.1">Une illusion, songe évanoui,</l>
					<l n="36" num="18.2"><space quantity="8" unit="char"></space>Charmait mon âme éblouie.</l>
				</lg>
				<lg n="19">
					<l n="37" num="19.1">Je fatigue seul le flot de cristal,</l>
					<l n="38" num="19.2"><space quantity="8" unit="char"></space>L’herbe où la fleur d’or s’étale,</l>
				</lg>
				<lg n="20">
					<l n="39" num="20.1">L’antre et la fontaine où croît le glaïeul,</l>
					<l n="40" num="20.2"><space quantity="8" unit="char"></space>Et ma voix fatigue seule</l>
				</lg>
				<lg n="21">
					<l n="41" num="21.1">La forêt tremblante et l’azur du lac</l>
					<l n="42" num="21.2"><space quantity="8" unit="char"></space>De ma plainte élégiaque !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1844">Août 1844.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>