<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES STALACTITES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1359 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_9</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LES STALACTITES</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/banvillelesstalactictes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
				<monogr>
					<title>Œuvres de Théodore de Banville. Les stalactites, Odelettes, Améthystes, Le Forgeron.</title>
					<author>Théodore de Banville</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Alphonse Lemerre, éditeur</publisher>
						<date when="1889">1889</date>
					</imprint>
				</monogr>
				<note>Édition de référence pour les corrections métriques</note>
			</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1846">1843-1846</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN386">
				<lg n="1">
					<l n="1" num="1.1">Chère, voici le mois de mai,</l>
					<l n="2" num="1.2">Le mois du printemps parfumé</l>
					<l n="3" num="1.3"><space quantity="8" unit="char"></space>Qui, sous les branches,</l>
					<l n="4" num="1.4">Fait vibrer des sons inconnus,</l>
					<l n="5" num="1.5">Et couvre les seins demi-nus</l>
					<l n="6" num="1.6"><space quantity="8" unit="char"></space>De robes blanches.</l>
				</lg>
				<lg n="2">
					<l n="7" num="2.1">Voici la saison des doux nids,</l>
					<l n="8" num="2.2">Le temps où les cieux rajeunis</l>
					<l n="9" num="2.3"><space quantity="8" unit="char"></space>Sont tout en flamme,</l>
					<l n="10" num="2.4">Où déjà, tout le long du jour,</l>
					<l n="11" num="2.5">Le doux rossignol de l’amour</l>
					<l n="12" num="2.6"><space quantity="8" unit="char"></space>Chante dans l’âme.</l>
				</lg>
				<lg n="3">
					<l n="13" num="3.1">Ah ! de quels suaves rayons</l>
					<l n="14" num="3.2">Se dorent nos illusions</l>
					<l n="15" num="3.3"><space quantity="12" unit="char"></space>Les plus chéries,</l>
					<l n="16" num="3.4">Et combien de charmants espoirs</l>
					<l n="17" num="3.5">Nous jettent dans l’ombre des soirs</l>
					<l n="18" num="3.6"><space quantity="8" unit="char"></space>Leurs rêveries !</l>
				</lg>
				<lg n="4">
					<l n="19" num="4.1">Parmi nos rêves à tous deux,</l>
					<l n="20" num="4.2">Beaux projets souvent hasardeux</l>
					<l n="21" num="4.3"><space quantity="8" unit="char"></space>Qui sont les mêmes,</l>
					<l n="22" num="4.4">Songes pleins d’amour et de foi</l>
					<l n="23" num="4.5">Que tu dois avoir comme moi,</l>
					<l n="24" num="4.6"><space quantity="8" unit="char"></space>Puisque tu m’aimes ;</l>
				</lg>
				<lg n="5">
					<l n="25" num="5.1">Il en est un seul plus aimé.</l>
					<l n="26" num="5.2">Tel meurt un zéphyr embaumé</l>
					<l n="27" num="5.3"><space quantity="8" unit="char"></space>Sur votre bouche,</l>
					<l n="28" num="5.4">Telle, par une ardente nuit,</l>
					<l n="29" num="5.5">De quelque Séraphin, sans bruit,</l>
					<l n="30" num="5.6"><space quantity="12" unit="char"></space>L’aile vous touche.</l>
				</lg>
				<lg n="6">
					<l n="31" num="6.1">Camille, as-tu rêvé parfois</l>
					<l n="32" num="6.2">Qu’à l’heure où s’éveillent les bois</l>
					<l n="33" num="6.3"><space quantity="8" unit="char"></space>Et l’alouette,</l>
					<l n="34" num="6.4">Où Roméo, vingt fois baisé,</l>
					<l n="35" num="6.5">Enjambe le balcon brisé</l>
					<l n="36" num="6.6"><space quantity="8" unit="char"></space>De Juliette,</l>
				</lg>
				<lg n="7">
					<l n="37" num="7.1">Nous partons tous les deux, tout seuls ?</l>
					<l n="38" num="7.2">Hors Paris, dans les grands tilleuls</l>
					<l n="39" num="7.3"><space quantity="8" unit="char"></space>Un rayon joue ;</l>
					<l n="40" num="7.4">L’air sent les lilas et le thym,</l>
					<l n="41" num="7.5">La fraîche brise du matin</l>
					<l n="42" num="7.6"><space quantity="8" unit="char"></space>Baise ta joue.</l>
				</lg>
				<lg n="8">
					<l n="43" num="8.1">Après avoir passé tout près</l>
					<l n="44" num="8.2">De vastes ombrages, plus frais</l>
					<l n="45" num="8.3"><space quantity="12" unit="char"></space>Qu’une glacière</l>
					<l n="46" num="8.4">Et tout pleins de charmants abords,</l>
					<l n="47" num="8.5">Nous allons nous asseoir aux bords</l>
					<l n="48" num="8.6"><space quantity="8" unit="char"></space>De la rivière.</l>
				</lg>
				<lg n="9">
					<l n="49" num="9.1">L’eau frémit, le poisson changeant</l>
					<l n="50" num="9.2">Émaille la vague d’argent</l>
					<l n="51" num="9.3"><space quantity="8" unit="char"></space>D’écailles blondes ;</l>
					<l n="52" num="9.4">Le saule, arbre des tristes vœux,</l>
					<l n="53" num="9.5">Pleure, et baigne ses longs cheveux</l>
					<l n="54" num="9.6"><space quantity="8" unit="char"></space>Parmi les ondes.</l>
				</lg>
				<lg n="10">
					<l n="55" num="10.1">Tout est calme et silencieux.</l>
					<l n="56" num="10.2">Étoiles que la terre aux cieux</l>
					<l n="57" num="10.3"><space quantity="8" unit="char"></space>A dérobées,</l>
					<l n="58" num="10.4">On voit briller d’un éclat pur</l>
					<l n="59" num="10.5">Les corsages d’or et d’azur</l>
					<l n="60" num="10.6"><space quantity="12" unit="char"></space>Des scarabées.</l>
				</lg>
				<lg n="11">
					<l n="61" num="11.1">Nos yeux s’enivrent, assouplis,</l>
					<l n="62" num="11.2">A voir l’eau dérouler les plis</l>
					<l n="63" num="11.3"><space quantity="8" unit="char"></space>De sa ceinture.</l>
					<l n="64" num="11.4">Je baise en pleurant tes genoux,</l>
					<l n="65" num="11.5">Et nous sommes seuls, rien que nous</l>
					<l n="66" num="11.6"><space quantity="8" unit="char"></space>Et la nature !</l>
				</lg>
				<lg n="12">
					<l n="67" num="12.1">Tout alors, les flots enchanteurs,</l>
					<l n="68" num="12.2">L’arbre ému, les oiseaux chanteurs</l>
					<l n="69" num="12.3"><space quantity="8" unit="char"></space>Et les feuillées,</l>
					<l n="70" num="12.4">Et les voix aux accords touchants</l>
					<l n="71" num="12.5">Que le silence dans les champs</l>
					<l n="72" num="12.6"><space quantity="8" unit="char"></space>Tient éveillées,</l>
				</lg>
				<lg n="13">
					<l n="73" num="13.1">La brise aux parfums caressants,</l>
					<l n="74" num="13.2">Les horizons éblouissants</l>
					<l n="75" num="13.3"><space quantity="12" unit="char"></space>De fantaisie,</l>
					<l n="76" num="13.4">Les serments dans nos cœurs écrits,</l>
					<l n="77" num="13.5">Tout en nous demande à grands cris</l>
					<l n="78" num="13.6"><space quantity="8" unit="char"></space>La Poésie.</l>
				</lg>
				<lg n="14">
					<l n="79" num="14.1">Nous sommes heureux sans froideur.</l>
					<l n="80" num="14.2">Plus de bouderie ou d’humeur</l>
					<l n="81" num="14.3"><space quantity="8" unit="char"></space>Triste ou chagrine ;</l>
					<l n="82" num="14.4">Tu poses d’un air triomphant</l>
					<l n="83" num="14.5">Ta petite tête d’enfant</l>
					<l n="84" num="14.6"><space quantity="8" unit="char"></space>Sur ma poitrine ;</l>
				</lg>
				<lg n="15">
					<l n="85" num="15.1">Tu m’écoutes, et je te lis,</l>
					<l n="86" num="15.2">Quoique ta bouche aux coins pâlis</l>
					<l n="87" num="15.3"><space quantity="8" unit="char"></space>S’ouvre et soupire,</l>
					<l n="88" num="15.4">Quelques stances d’Alighieri,</l>
					<l n="89" num="15.5">Ronsard, le poëte chéri,</l>
					<l n="90" num="15.6"><space quantity="12" unit="char"></space>Ou bien Shakspere.</l>
				</lg>
				<lg n="16">
					<l n="91" num="16.1">Mais je jette le livre ouvert,</l>
					<l n="92" num="16.2">Tandis que ton regard se perd</l>
					<l n="93" num="16.3"><space quantity="8" unit="char"></space>Parmi les mousses,</l>
					<l n="94" num="16.4">Et je préfère, en vrai jaloux,</l>
					<l n="95" num="16.5">A nos poëtes les plus doux</l>
					<l n="96" num="16.6"><space quantity="8" unit="char"></space>Tes lèvres douces !</l>
				</lg>
				<lg n="17">
					<l n="97" num="17.1">Tiens, voici qu’un couple charmant,</l>
					<l n="98" num="17.2">Comme nous jeune et bien aimant,</l>
					<l n="99" num="17.3"><space quantity="8" unit="char"></space>Vient et regarde.</l>
					<l n="100" num="17.4">Que de bonheur rien qu’à leurs pas !</l>
					<l n="101" num="17.5">Ils passent et ne nous voient pas :</l>
					<l n="102" num="17.6"><space quantity="8" unit="char"></space>Que Dieu les garde !</l>
				</lg>
				<lg n="18">
					<l n="103" num="18.1">Ce sont des frères, mon cher cœur,</l>
					<l n="104" num="18.2">Que, comme nous, l’amour vainqueur</l>
					<l n="105" num="18.3"><space quantity="10" unit="char"></space>Fit l’un pour l’autre.</l>
					<l n="106" num="18.4">Ah ! qu’ils soient heureux à leur tour !</l>
					<l n="107" num="18.5">Embrassons-nous pour leur amour</l>
					<l n="108" num="18.6"><space quantity="8" unit="char"></space>Et pour le nôtre !</l>
				</lg>
				<lg n="19">
					<l n="109" num="19.1">Chère, quel ineffable émoi,</l>
					<l n="110" num="19.2">Sur ce rivage où près de moi</l>
					<l n="111" num="19.3"><space quantity="8" unit="char"></space>Tu te recueilles,</l>
					<l n="112" num="19.4">De mêler d’amoureux sanglots</l>
					<l n="113" num="19.5">Aux douces plaintes que les flots</l>
					<l n="114" num="19.6"><space quantity="8" unit="char"></space>Disent aux feuilles !</l>
				</lg>
				<lg n="20">
					<l n="115" num="20.1">Dis, quel bonheur d’être enlacés</l>
					<l n="116" num="20.2">Par des bras forts, jamais lassés !</l>
					<l n="117" num="20.3"><space quantity="8" unit="char"></space>Avec quels charmes,</l>
					<l n="118" num="20.4">Après tous nos mortels exils,</l>
					<l n="119" num="20.5">Je savoure au bout de tes cils</l>
					<l n="120" num="20.6"><space quantity="10" unit="char"></space>De fraîches larmes !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1844">Avril 1844.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>