<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES STALACTITES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1359 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_9</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LES STALACTITES</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/banvillelesstalactictes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
				<monogr>
					<title>Œuvres de Théodore de Banville. Les stalactites, Odelettes, Améthystes, Le Forgeron.</title>
					<author>Théodore de Banville</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Alphonse Lemerre, éditeur</publisher>
						<date when="1889">1889</date>
					</imprint>
				</monogr>
				<note>Édition de référence pour les corrections métriques</note>
			</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1846">1843-1846</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN393">
				<head type="main">Pour mademoiselle ***</head>
				<opener>
					<epigraph>
						<cit>
							<quote>
								22. Car la fille d’Hérodiade y étant entrée <lb></lb>
								et ayant dansé devant le roi, elle lui plut <lb></lb>
								tellement, et à ceux qui étaient à table avec <lb></lb>
								lui, qu’il lui dit : Demandez-moi ce que vous <lb></lb>
								voudrez, et je vous le donnerai. <lb></lb>
								23. Et il ajouta avec serment : Oui, je vous <lb></lb>
								donnerai tout ce que vous me demanderez, <lb></lb>
								quand ce serait la moitié de mon royaume. <lb></lb>
								24. Elle, étant sortie, dit à sa mère : Que <lb></lb>
								demanderai-je ? Sa mère lui répondit : La tête <lb></lb>
								de Jean-Baptiste.
							</quote>
							<bibl>
								<hi rend="ital">Évangile selon saint Marc.</hi>
							</bibl>
						</cit>
					</epigraph>
				</opener>
				<lg n="1">
					<l n="1" num="1.1">Amours des bas-reliefs, ô Nymphes et Bacchantes,</l>
					<l n="2" num="1.2">Qui, sur l’Ida nocturne, au bruit d’un tambourin,</l>
					<l n="3" num="1.3">Les fronts échevelés en tresses provocantes,</l>
					<l n="4" num="1.4">Dansiez en agitant vos crotales d’airain !</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Vous, plus belles déjà que ces filles du Pinde,</l>
					<l n="6" num="2.2">Bayadères d’ébène aux bras purs et nerveux,</l>
					<l n="7" num="2.3">Qui bondissez sans bruit sur les tapis de l’Inde !</l>
					<l n="8" num="2.4">Avec des sequins d’or passés dans vos cheveux !</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Elssler ! Taglioni ! Carlotta ! sœurs divines</l>
					<l n="10" num="3.2">Aux corselets de guêpe, aux regards de houri,</l>
					<l n="11" num="3.3">Qui fouliez, en quittant le gazon des collines,</l>
					<l n="12" num="3.4">Le splendide outremer des ciels de Cicéri !</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">O reines du ballet, toutes les trois si belles !</l>
					<l n="14" num="4.2">Qu’un Homère ébloui fera nymphes un jour,</l>
					<l n="15" num="4.3">Ce n’est plus vous la Danse, allons, coupez vos ailes !</l>
					<l n="16" num="4.4">Éteignez vos regards, ce n’est plus vous l’Amour !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1845">Février 1845.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>