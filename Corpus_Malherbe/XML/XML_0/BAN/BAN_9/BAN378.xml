<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES STALACTITES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1359 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_9</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LES STALACTITES</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/banvillelesstalactictes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
				<monogr>
					<title>Œuvres de Théodore de Banville. Les stalactites, Odelettes, Améthystes, Le Forgeron.</title>
					<author>Théodore de Banville</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Alphonse Lemerre, éditeur</publisher>
						<date when="1889">1889</date>
					</imprint>
				</monogr>
				<note>Édition de référence pour les corrections métriques</note>
			</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1846">1843-1846</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN378">
				<head type="main">Chanson à boire</head>
				<opener>
					<epigraph>
						<cit>
							<quote>Allons en vendanges, <lb></lb>Les raisins sont bons !</quote>
							<bibl><hi rend="ital">Chanson</hi></bibl>
						</cit>
					</epigraph>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><space quantity="4" unit="char"></space>De ce vieux vin que je révère</l>
					<l n="2" num="1.2"><space quantity="4" unit="char"></space>Cherchez un flacon dans ce coin.</l>
					<l n="3" num="1.3"><space quantity="4" unit="char"></space>Çà, qu’on le débouche avec soin,</l>
					<l n="4" num="1.4"><space quantity="4" unit="char"></space>Et qu’on emplisse mon grand verre.</l>
				</lg>
				<lg n="2">
					<l rhyme="none" n="5" num="2.1"><space quantity="16" unit="char"></space>Chantons Io Pæan !</l>
				</lg>
				<lg n="3">
					<l n="6" num="3.1"><space quantity="4" unit="char"></space>Le Léthé des soucis moroses</l>
					<l n="7" num="3.2"><space quantity="4" unit="char"></space>Sous son beau cristal est enclos,</l>
					<l n="8" num="3.3"><space quantity="4" unit="char"></space>Et dans son cœur je veux à flots</l>
					<l n="9" num="3.4"><space quantity="4" unit="char"></space>Boire du soleil et des roses.</l>
				</lg>
				<lg n="4">
					<l n="10" num="4.1">La treille a ployé tout le long des murs,</l>
					<l n="11" num="4.2">Allez, vendangeurs, les raisins sont mûrs !</l>
				</lg>
				<lg n="5">
					<l n="12" num="5.1"><space quantity="4" unit="char"></space>Jusqu’en la moindre gouttelette,</l>
					<l n="13" num="5.2"><space quantity="4" unit="char"></space>La fraîche haleine de ce vin</l>
					<l n="14" num="5.3"><space quantity="4" unit="char"></space>Exhale un parfum plus divin</l>
					<l n="15" num="5.4"><space quantity="4" unit="char"></space>Qu’une touffe de violette,</l>
				</lg>
				<lg n="6">
					<l rhyme="none" n="16" num="6.1"><space quantity="16" unit="char"></space>Chantons Io Pæan !</l>
				</lg>
				<lg n="7">
					<l n="17" num="7.1"><space quantity="4" unit="char"></space>Et, dessus la lèvre endormie</l>
					<l n="18" num="7.2"><space quantity="4" unit="char"></space>Des pâles et tristes songeurs,</l>
					<l n="19" num="7.3"><space quantity="4" unit="char"></space>Met de plus ardentes rougeurs</l>
					<l n="20" num="7.4"><space quantity="4" unit="char"></space>Que n’en a le sein de ma mie.</l>
				</lg>
				<lg n="8">
					<l n="21" num="8.1">La treille a ployé tout le long des murs,</l>
					<l n="22" num="8.2">Allez, vendangeurs, les raisins sont mûrs !</l>
				</lg>
				<lg n="9">
					<l n="23" num="9.1"><space quantity="4" unit="char"></space>A mes yeux, en nappes fleuries</l>
					<l n="24" num="9.2"><space quantity="4" unit="char"></space>Dansantes sous le ciel en feu,</l>
					<l n="25" num="9.3"><space quantity="4" unit="char"></space>L’air se teint de rose et de bleu</l>
					<l n="26" num="9.4"><space quantity="4" unit="char"></space>Comme au théâtre des féeries ;</l>
				</lg>
				<lg n="10">
					<l rhyme="none" n="27" num="10.1"><space quantity="16" unit="char"></space>Chantons Io Pæan !</l>
				</lg>
				<lg n="11">
					<l n="28" num="11.1"><space quantity="4" unit="char"></space>Je vois un cortège fantasque,</l>
					<l n="29" num="11.2"><space quantity="4" unit="char"></space>Suivi de cors et de hautbois,</l>
					<l n="30" num="11.3"><space quantity="4" unit="char"></space>Tourbillonner, et joindre aux voix</l>
					<l n="31" num="11.4"><space quantity="4" unit="char"></space>La flûte et les tambours de basque !</l>
				</lg>
				<lg n="12">
					<l n="32" num="12.1">La treille a ployé tout le long des murs,</l>
					<l n="33" num="12.2">Allez, vendangeurs, les raisins sont mûrs !</l>
				</lg>
				<lg n="13">
					<l n="34" num="13.1"><space quantity="4" unit="char"></space>C’est Galatée ou Vénus même</l>
					<l n="35" num="13.2"><space quantity="4" unit="char"></space>Qui, dans l’éclat du flot profond,</l>
					<l n="36" num="13.3"><space quantity="4" unit="char"></space>Se joue et me sourit au fond</l>
					<l n="37" num="13.4"><space quantity="4" unit="char"></space>De mon grand verre de Bohême.</l>
				</lg>
				<lg n="14">
					<l rhyme="none" n="38" num="14.1"><space quantity="16" unit="char"></space>Chantons Io Pæan !</l>
				</lg>
				<lg n="15">
					<l n="39" num="15.1"><space quantity="4" unit="char"></space>Cette autre Cypris, plus galante,</l>
					<l n="40" num="15.2"><space quantity="4" unit="char"></space>Naît du nectar si bien chanté,</l>
					<l n="41" num="15.3"><space quantity="4" unit="char"></space>Et laisse voir sa nudité</l>
					<l n="42" num="15.4"><space quantity="4" unit="char"></space>Sous une pourpre étincelante.</l>
				</lg>
				<lg n="16">
					<l n="43" num="16.1">La treille a ployé tout le long des murs,</l>
					<l n="44" num="16.2">Allez, vendangeurs, les raisins sont mûrs !</l>
				</lg>
				<lg n="17">
					<l n="45" num="17.1"><space quantity="4" unit="char"></space>Plus d’amante froide ou traîtresse,</l>
					<l n="46" num="17.2"><space quantity="4" unit="char"></space>Plus de poëtes envieux !</l>
					<l n="47" num="17.3"><space quantity="4" unit="char"></space>Dans ce grand verre de vin vieux</l>
					<l n="48" num="17.4"><space quantity="4" unit="char"></space>Pleure une immortelle maîtresse,</l>
				</lg>
				<lg n="18">
					<l rhyme="none" n="49" num="18.1"><space quantity="16" unit="char"></space>Chantons Io Pæan !</l>
				</lg>
				<lg n="19">
					<l n="50" num="19.1"><space quantity="4" unit="char"></space>Et, comme un ballet magnifique,</l>
					<l n="51" num="19.2"><space quantity="4" unit="char"></space>Je vois, dans le flacon vermeil,</l>
					<l n="52" num="19.3"><space quantity="4" unit="char"></space>Couleur de lune et de soleil,</l>
					<l n="53" num="19.4"><space quantity="4" unit="char"></space>Des rhythmes danser en musique !</l>
				</lg>
				<lg n="20">
					<l n="54" num="20.1">La treille a ployé tout le long des murs,</l>
					<l n="55" num="20.2">Allez, vendangeurs, les raisins sont mûrs !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1844">Septembre 1844.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>