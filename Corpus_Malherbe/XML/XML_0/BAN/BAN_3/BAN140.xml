<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Exilés</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4189 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">BAN_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>

			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Exilés</title>
						<author>Théodore de BANVILLE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L923</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
									<title>Les Exilés</title>
									<author>Théodore de BANVILLE</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Fasquelle</publisher>
									<date when="1899">1899</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>

		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1867">1867</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface n’a pas été encodée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN140">
				<head type="main">NÉMÉE</head>
				<lg n="1">
					<l n="1" num="1.1">Dans la vallée où passe une haleine embaumée,</l>
					<l n="2" num="1.2">Hercule combattait le lion de Némée.</l>
					<l n="3" num="1.3">Rampant, agile et nu, parmi les gazons ras,</l>
					<l n="4" num="1.4">Parfois il étreignait le monstre dans ses bras,</l>
					<l n="5" num="1.5">Puis le fuyait ; et, plein de fureur et de joie,</l>
					<l n="6" num="1.6">Par un bond effrayant revenait sur sa proie.</l>
					<l n="7" num="1.7"><space quantity="2" unit="char"></space>Au loin sur les coteaux et dans les bois dormants</l>
					<l n="8" num="1.8">On entendit leurs cris et leurs rugissements ;</l>
					<l n="9" num="1.9">Ils étaient à la fois deux héros et deux bêtes</l>
					<l n="10" num="1.10">Mêlant leurs durs cheveux, entre-choquant leurs têtes,</l>
					<l n="11" num="1.11">Hurlant vers la clarté des cieux qui nous sont chers,</l>
					<l n="12" num="1.12">Avec la griffe et l’ongle ensanglantant leurs chairs ;</l>
					<l n="13" num="1.13">Haletants, ils ouvraient leurs deux bouches pensives,</l>
					<l n="14" num="1.14">Montrant dans la clarté leurs dents et leurs gencives ;</l>
					<l n="15" num="1.15">Puis, vautrés l’un sur l’autre, ils tombaient en roulant</l>
					<l n="16" num="1.16">Sur les pentes en fleur, dans le sable sanglant.</l>
					<l n="17" num="1.17"><space quantity="2" unit="char"></space>Enfin, d’un cri sauvage effrayant les ravines,</l>
					<l n="18" num="1.18">Hercule prit le monstre entre ses mains divines ;</l>
					<l n="19" num="1.19">Alors il lui serra si durement le cou,</l>
					<l n="20" num="1.20">Que le lion sentit la mort dans son œil fou</l>
					<l n="21" num="1.21">Et vit passer sur lui le flot noir de l’Averne.</l>
					<l n="22" num="1.22">Le héros le traîna jusque dans sa caverne ;</l>
					<l n="23" num="1.23">Sombre et morne, elle avait une entrée au levant,</l>
					<l n="24" num="1.24">Et l’autre au couchant sombre, où s’engouffrait le vent.</l>
					<l n="25" num="1.25"><space quantity="2" unit="char"></space>Hercule, contenant d’une main rude et forte</l>
					<l n="26" num="1.26">Le lion qui voulait bondir vers cette porte,</l>
					<l n="27" num="1.27">Prit un quartier de roche avec son autre main,</l>
					<l n="28" num="1.28">Et la boucha ; puis, d’un long effort surhumain,</l>
					<l n="29" num="1.29">Qui fit craquer les os de l’horrible mâchoire</l>
					<l n="30" num="1.30">Et jaillir un sang rouge entre ses dents d’ivoire,</l>
					<l n="31" num="1.31">Il étouffa le monstre, et, penché vers les cieux,</l>
					<l n="32" num="1.32">Il écouta monter dans l’air silencieux</l>
					<l n="33" num="1.33">Son long râle et sa plainte amère aux vents jetée,</l>
					<l n="34" num="1.34">Si triste que la terre en fut épouvantée.</l>
					<l n="35" num="1.35">Puis le héros ouvrit ses bras ; poussant un cri</l>
					<l n="36" num="1.36">Suprême, le lion mourant tomba meurtri,</l>
					<l n="37" num="1.37">Et, se heurtant au mur de la caverne close,</l>
					<l n="38" num="1.38">Il expira, laissant traîner sa langue rose.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1874">lundi 6 juillet 1874</date>.
					</dateline>
				</closer>
			</div></body></text></TEI>