<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Exilés</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4189 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">BAN_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>

			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Exilés</title>
						<author>Théodore de BANVILLE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L923</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
									<title>Les Exilés</title>
									<author>Théodore de BANVILLE</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Fasquelle</publisher>
									<date when="1899">1899</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>

		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1867">1867</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface n’a pas été encodée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN167">
				<head type="main">L’ATTRAIT DU GOUFFRE</head>
				<lg n="1">
					<l n="1" num="1.1">Oh ! Que me voulez-vous, lueurs vertigineuses ?</l>
					<l n="2" num="1.2">Divin silence, attrait du néant, laisse-moi !</l>
					<l n="3" num="1.3">Ainsi la mer, songeant par les nuits lumineuses,</l>
					<l n="4" num="1.4">Me faisait tressaillir de tendresse et d’effroi.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Ces yeux où les chansons des sirènes soupirent,</l>
					<l n="6" num="2.2">Océans éperdus, gouffres inapaisés,</l>
					<l n="7" num="2.3">Bleus firmaments où rien ne doit vivre, m’inspirent</l>
					<l n="8" num="2.4">La haine de la joie et l’oubli des baisers.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Les yeux pensifs, les yeux de cette charmeresse</l>
					<l n="10" num="3.2">Sont faits d’un pur aimant dont le pouvoir fatal</l>
					<l n="11" num="3.3">Communique une chaste et merveilleuse ivresse</l>
					<l n="12" num="3.4">Et ce mal effréné, la soif de l’idéal.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">Ils ne s’abritent pas, solitudes sans voiles,</l>
					<l n="14" num="4.2">Sous des cils baignés d’or et sous de fiers sourcils ;</l>
					<l n="15" num="4.3">Ondes où vont mourir les flèches des étoiles,</l>
					<l n="16" num="4.4">Rien ne cache au regard leur mirage indécis.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1">Ce sont les lacs sans borne où s’égare mon âme ;</l>
					<l n="18" num="5.2">Leur azur éthéré, vaste et silencieux,</l>
					<l n="19" num="5.3">Saphir terrible et doux, sans lumière et sans flamme,</l>
					<l n="20" num="5.4">Vole sa transparence à d’ineffables cieux.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1">Je sais que ce désert plein de mélancolie</l>
					<l n="22" num="6.2">Engloutit mon courage en vain ressuscité,</l>
					<l n="23" num="6.3">Et que je ne peux pas, sans trouver la folie,</l>
					<l n="24" num="6.4">Chercher ta perle, amour ! Dans cette immensité.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1">L’éblouissement clair de ces froides prunelles</l>
					<l n="26" num="7.2">Où le féroce ennui voudrait à son loisir</l>
					<l n="27" num="7.3">Savourer le poison des langueurs éternelles</l>
					<l n="28" num="7.4">M’enchante et me ravit dans un vague désir.</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1">Il n’est plus temps de fuir, laisse toute espérance !</l>
					<l n="30" num="8.2">Ils m’ont appris, ces flots aux cruelles pâleurs,</l>
					<l n="31" num="8.3">Les voluptés du calme et de l’indifférence,</l>
					<l n="32" num="8.4">Et l’extase a tari la source de mes pleurs.</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1">L’abîme où, sans retour, mon rêve s’embarrasse,</l>
					<l n="34" num="9.2">Semble immobile ; mais je le sens tournoyer.</l>
					<l n="35" num="9.3">Comme une lèvre humide, il m’attire et m’embrasse,</l>
					<l n="36" num="9.4">Et ma lâche raison frémit de s’y noyer.</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1">Eh bien, je poursuivrai mon destin misérable :</l>
					<l n="38" num="10.2">Par-delà le fini, par-delà le réel,</l>
					<l n="39" num="10.3">Je veux boire à longs traits cette angoisse adorable</l>
					<l n="40" num="10.4">Et souffrir les ennuis de ce bonheur mortel.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1858">Bellevue, avril 1858</date>.
					</dateline>
				</closer>
			</div></body></text></TEI>