<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Exilés</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4189 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">BAN_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>

			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Exilés</title>
						<author>Théodore de BANVILLE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L923</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
									<title>Les Exilés</title>
									<author>Théodore de BANVILLE</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Fasquelle</publisher>
									<date when="1899">1899</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>

		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1867">1867</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface n’a pas été encodée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN142">
				<head type="main">LA MORT DE L’AMOUR</head>
				<lg n="1">
					<l n="1" num="1.1">Une nuit, j’ai rêvé que l’amour était mort.</l>
					<l n="2" num="1.2"><space quantity="2" unit="char"></space>Au penchant de l’Œta, que l’âpre bise mord,</l>
					<l n="3" num="1.3">Les vierges dont le vent meurtrit de ses caresses</l>
					<l n="4" num="1.4">Les seins nus et les pieds de lys, les chasseresses</l>
					<l n="5" num="1.5">Que la lune voit fuir dans l’antre souterrain,</l>
					<l n="6" num="1.6">L’avaient toutes percé de leurs flèches d’airain.</l>
					<l n="7" num="1.7"><space quantity="2" unit="char"></space>Le jeune dieu tomba, meurtri de cent blessures,</l>
					<l n="8" num="1.8">Et le sang jaillissait sur ses belles chaussures.</l>
					<l n="9" num="1.9">Il expira. Parmi les bois qu’ils parcouraient</l>
					<l n="10" num="1.10">Les loups criaient de peur. Les grands lions pleuraient.</l>
					<l n="11" num="1.11">La terre frissonnait et se sentait perdue.</l>
					<l n="12" num="1.12">Folle, expirante aussi, la nature éperdue</l>
					<l n="13" num="1.13">De voir le divin sang couler en flot vermeil,</l>
					<l n="14" num="1.14">Enveloppa de nuit et d’ombre le soleil,</l>
					<l n="15" num="1.15">Comme pour étouffer sous l’horreur de ces voiles</l>
					<l n="16" num="1.16">L’épouvantable cri qui tombait des étoiles.</l>
					<l n="17" num="1.17"><space quantity="2" unit="char"></space>Laissant pendre sa main qui dompte le vautour,</l>
					<l n="18" num="1.18">Il gisait, l’adorable archer, l’enfant amour,</l>
					<l n="19" num="1.19">Comme un pin abattu vivant par la cognée.</l>
					<l n="20" num="1.20">Alors Psyché vint, blanche et de ses pleurs baignée :</l>
					<l n="21" num="1.21">Elle s’agenouilla près du bel enfant-dieu,</l>
					<l n="22" num="1.22">Et sans repos baisa ses blessures en feu,</l>
					<l n="23" num="1.23">Béantes, comme elle eût baisé de belles bouches,</l>
					<l n="24" num="1.24">Puis se roula dans l’herbe, et dit : « Ô dieux farouches !</l>
					<l n="25" num="1.25">C’est votre œuvre, de vous je n’attendais pas moins.</l>
					<l n="26" num="1.26">Je connais là vos coups. Mais vous êtes témoins,</l>
					<l n="27" num="1.27">Tous, que je donne ici mon souffle à ce cadavre,</l>
					<l n="28" num="1.28">Pour qu’Éros, délivré de la mort qui le navre,</l>
					<l n="29" num="1.29">Renaisse, et dans le vol des astres, d’un pied sûr</l>
					<l n="30" num="1.30">Remonte en bondissant les escaliers d’azur ! »</l>
					<l n="31" num="1.31"><space quantity="2" unit="char"></space>Puis, comprimant son cœur que brûlaient mille fièvres</l>
					<l n="32" num="1.32">Dans un baiser immense elle colla ses lèvres</l>
					<l n="33" num="1.33">Sur la lèvre glacée, hélas ! De son époux,</l>
					<l n="34" num="1.34">Et, tandis que la voix gémissante des loups</l>
					<l n="35" num="1.35">Montait vers le ciel noir sans lumière et sans flamme,</l>
					<l n="36" num="1.36">Elle baisa le mort, et lui souffla son âme.</l>
					<l n="37" num="1.37">Tout à coup le soleil reparut, et le dieu</l>
					<l n="38" num="1.38">Se releva, charmé, vivant, riant. L’air bleu</l>
					<l n="39" num="1.39">Baisait ses cheveux d’or, d’où le zéphyr emporte</l>
					<l n="40" num="1.40">L’extase des parfums, et Psyché tomba morte.</l>
					<l n="41" num="1.41"><space quantity="2" unit="char"></space>Éros emplit le bois de chansons, fier, divin,</l>
					<l n="42" num="1.42">Superbe, et d’une haleine aspirant, comme un vin</l>
					<l n="43" num="1.43">Doux et délicieux, la vie universelle,</l>
					<l n="44" num="1.44">Mais sans s’inquiéter un seul moment de celle</l>
					<l n="45" num="1.45">Qui gisait à ses pieds sur le coteau penchant,</l>
					<l n="46" num="1.46">Et dont le front traînait dans la fange. Et, touchant</l>
					<l n="47" num="1.47">Les flèches dont Zeus même adore la brûlure,</l>
					<l n="48" num="1.48">Il marchait dans son sang et dans sa chevelure.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1862">décembre 1862</date>.
					</dateline>
				</closer>
			</div></body></text></TEI>