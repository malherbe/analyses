<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Exilés</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4189 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">BAN_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>

			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Exilés</title>
						<author>Théodore de BANVILLE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L923</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
									<title>Les Exilés</title>
									<author>Théodore de BANVILLE</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Fasquelle</publisher>
									<date when="1899">1899</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>

		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1867">1867</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface n’a pas été encodée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN177">
				<head type="main">LA CHIMÈRE</head>
				<lg n="1">
					<l n="1" num="1.1">Monstre inspiration, dédaigneuse chimère,</l>
					<l n="2" num="1.2">Je te tiens ! Folle ! En vain, tordant ta lèvre amère,</l>
					<l n="3" num="1.3">Et demi-souriante et pleine de courroux,</l>
					<l n="4" num="1.4">Tu déchires ma main dans tes beaux cheveux roux.</l>
					<l n="5" num="1.5">Non, tu ne fuiras pas. Tu peux battre des ailes ;</l>
					<l n="6" num="1.6">Tout ivre que je suis du feu de tes prunelles</l>
					<l n="7" num="1.7">Et du rose divin de ta chair, je te tiens,</l>
					<l n="8" num="1.8">Et mes yeux de faucon sont cloués sur les tiens !</l>
					<l n="9" num="1.9">C’est l’or de mes sourcils que leur azur reflète.</l>
					<l n="10" num="1.10">Lionne, je te dompte avec un bras d’athlète ;</l>
					<l n="11" num="1.11">Oiseau, je t’ai surpris dans ton vol effaré,</l>
					<l n="12" num="1.12">Je t’arrache à l’éther ! Femme, je te dirai</l>
					<l n="13" num="1.13">Des mots voluptueux et sonores, et même,</l>
					<l n="14" num="1.14">Sans plus m’inquiéter du seul ange qui m’aime,</l>
					<l n="15" num="1.15">Je saurai, pour ravir avec de longs effrois</l>
					<l n="16" num="1.16">Tes limpides regards céruléens, plus froids</l>
					<l n="17" num="1.17">Que le fer de la dague et de la pertuisane,</l>
					<l n="18" num="1.18">Te mordre en te baisant, comme une courtisane.</l>
					<l n="19" num="1.19">Que pleures-tu ? Le ciel immense, ton pays ?</l>
					<l n="20" num="1.20">Tes étoiles ? Mais non, je t’adore, obéis.</l>
					<l n="21" num="1.21">Vite, allons, couche-toi, sauvage, plus de guerres.</l>
					<l n="22" num="1.22">Reste là ! Tu vois bien que je ne tremble guères</l>
					<l n="23" num="1.23">De laisser ma raison dans le réseau vermeil</l>
					<l n="24" num="1.24">De tes tresses en feu de flamme et de soleil,</l>
					<l n="25" num="1.25">Et que ma fière main sur ta croupe se plante,</l>
					<l n="26" num="1.26">Et que je n’ai pas peur de ta griffe sanglante !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1857">Bellevue, 19 décembre 1857</date>.
					</dateline>
				</closer>
			</div></body></text></TEI>