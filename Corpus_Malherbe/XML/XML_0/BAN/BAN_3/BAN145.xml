<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Exilés</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4189 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">BAN_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>

			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Exilés</title>
						<author>Théodore de BANVILLE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L923</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
									<title>Les Exilés</title>
									<author>Théodore de BANVILLE</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Fasquelle</publisher>
									<date when="1899">1899</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>

		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1867">1867</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface n’a pas été encodée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN145">
				<head type="main">LA REINE OMPHALE</head>
				<lg n="1">
					<l n="1" num="1.1">La reine Omphale était assise, comme un dieu,</l>
					<l n="2" num="1.2">Sur un trône ; ses lourds cheveux d’or et de feu</l>
					<l n="3" num="1.3">Étincelaient ; Hermès, pareil au crépuscule,</l>
					<l n="4" num="1.4">Posant sa forte main sur l’épaule d’Hercule,</l>
					<l n="5" num="1.5">Se tourna vers la reine avec un air subtil,</l>
					<l n="6" num="1.6">Et lui dit : « le marché des dieux te convient-il ?</l>
					<l n="7" num="1.7">—Messager, répondit alors d’une voix grave</l>
					<l n="8" num="1.8">La lydienne, pars, laisse-moi pour esclave</l>
					<l n="9" num="1.9">Ce tueur de lions, de sa forêt venu,</l>
					<l n="10" num="1.10">Et je l’achèterai pour le prix convenu. »</l>
					<l n="11" num="1.11">Hermès, gardant toujours sa pose triomphale,</l>
					<l n="12" num="1.12">Reçut les trois talents que lui donnait Omphale,</l>
					<l n="13" num="1.13">Et, montrant le héros aux muscles de titan,</l>
					<l n="14" num="1.14">« Cet homme, lui dit-il, t’appartient pour un an. »</l>
					<l n="15" num="1.15">Parlant ainsi, le dieu souriant de Cyllène,</l>
					<l n="16" num="1.16">Comme un aigle qui va partir, prit son haleine</l>
					<l n="17" num="1.17">Et bondit ; il vola de son pied diligent</l>
					<l n="18" num="1.18">Plus haut que l’éther vaste et les astres d’argent ;</l>
					<l n="19" num="1.19">Puis au ciel, qu’une pourpre éblouissante arrose,</l>
					<l n="20" num="1.20">S’enfuit dans la vapeur en feu du couchant rose.</l>
					<l n="21" num="1.21">La lydienne au front orné de cheveux roux</l>
					<l n="22" num="1.22">Abaissa sur Hercule un œil plein de courroux,</l>
					<l n="23" num="1.23">Et lui cria, superbe et de rage enflammée,</l>
					<l n="24" num="1.24">En touchant la dépouille auguste de Némée :</l>
					<l n="25" num="1.25">« Esclave, donne-moi cette peau de lion. »</l>
					<l n="26" num="1.26">Hercule, sans colère et sans rébellion,</l>
					<l n="27" num="1.27">Obéit. La princesse arrangea comme un casque,</l>
					<l n="28" num="1.28">Sur sa tête aux cheveux brillants, l’horrible masque</l>
					<l n="29" num="1.29">Du lion, puis mêla, plus irritée encor,</l>
					<l n="30" num="1.30">La crinière farouche avec ses cheveux d’or,</l>
					<l n="31" num="1.31">Et, levant par orgueil sa tête étincelante,</l>
					<l n="32" num="1.32">Se fit de la dépouille une robe sanglante.</l>
					<l n="33" num="1.33">« Esclave, que le sort a courbé sous ma loi,</l>
					<l n="34" num="1.34">Reprit-elle en mordant sa lèvre, donne-moi</l>
					<l n="35" num="1.35">Tes flèches, ton épée et ton arc, et déchire</l>
					<l n="36" num="1.36">Ce carquois. » le héros obéit. Un sourire</l>
					<l n="37" num="1.37">Ineffable éclairait, comme un rayon vermeil,</l>
					<l n="38" num="1.38">Son front pensif, hâlé par le fauve soleil.</l>
					<l n="39" num="1.39">Pourquoi vas-tu, couvert de meurtres et de crimes,</l>
					<l n="40" num="1.40">Par les chemins, sous l’œil jaloux des dieux sublimes ?</l>
					<l n="41" num="1.41">Dit Omphale. Tu fuis dans l’univers sacré,</l>
					<l n="42" num="1.42">Toujours ivre de sang et de sang altéré ;</l>
					<l n="43" num="1.43">Tu fais des orphelins désolés et des veuves</l>
					<l n="44" num="1.44">Dont le sanglot amer se mêle au bruit des fleuves ;</l>
					<l n="45" num="1.45">Ton pied impétueux ne marche qu’en heurtant</l>
					<l n="46" num="1.46">Des cadavres ; l’horreur te cherche, et l’on entend</l>
					<l n="47" num="1.47">Crier derrière toi les bouches des blessures.</l>
					<l n="48" num="1.48">Comme un chien dont les dents sont rouges de morsures,</l>
					<l n="49" num="1.49">Et qui, repu déjà, pour se désaltérer</l>
					<l n="50" num="1.50">Cherche encore un lambeau de chair à déchirer,</l>
					<l n="51" num="1.51">Tu peuples d’ossements la terre et les rivages,</l>
					<l n="52" num="1.52">Et tu n’épargnes même, en tes meurtres sauvages,</l>
					<l n="53" num="1.53">Ni les rois au front ceint de laurier, ni les dieux ;</l>
					<l n="54" num="1.54">Mais s’ils ont fui devant ce carnage odieux,</l>
					<l n="55" num="1.55">Comme rougir la terre est ton unique joie,</l>
					<l n="56" num="1.56">Tu cherches les serpents et les bêtes de proie.</l>
					<l n="57" num="1.57">C’est par de tels exploits que tu te signalas ;</l>
					<l n="58" num="1.58">Mais la terre en est lasse et le ciel en est las ;</l>
					<l n="59" num="1.59">Les fleuves rugissants, dans leurs grottes profondes,</l>
					<l n="60" num="1.60">Ne veulent plus rouler du sang avec leurs ondes ;</l>
					<l n="61" num="1.61">Tes pas lourds font horreur aux grands bois chevelus,</l>
					<l n="62" num="1.62">Et, lasse de te voir, la terre ne veut plus</l>
					<l n="63" num="1.63">Cacher au fond du lac pâle ou de la caverne</l>
					<l n="64" num="1.64">Ta moisson de corps morts promis au sombre Averne.</l>
					<l n="65" num="1.65">Et c’est pourquoi les dieux, qui seront tes bourreaux,</l>
					<l n="66" num="1.66">M’ont fait des bras d’athlète et le cœur d’un héros</l>
					<l n="67" num="1.67">Pour vaincre l’oiseleur affreux du lac Stymphale,</l>
					<l n="68" num="1.68">Car ils réserveront à la gloire d’Omphale</l>
					<l n="69" num="1.69">De dompter un brigand, pourvoyeur des tombeaux</l>
					<l n="70" num="1.70">Ouverts, dût-elle avoir comme toi des lambeaux</l>
					<l n="71" num="1.71">De chair après ses dents et du sang à la bouche,</l>
					<l n="72" num="1.72">Et déchirer le cœur d’un assassin farouche. »</l>
					<l n="73" num="1.73">« —Ô reine, répondit Hercule doucement,</l>
					<l n="74" num="1.74">Amazone invincible au cœur de diamant !</l>
					<l n="75" num="1.75">Quand tu parais, on croit voir, à ta noble taille,</l>
					<l n="76" num="1.76">Un jeune dieu cruel armé pour la bataille.</l>
					<l n="77" num="1.77">Ton regard, que la Grèce a tant de fois vanté,</l>
					<l n="78" num="1.78">S’embrase comme un astre au ciel épouvanté,</l>
					<l n="79" num="1.79">Et sur ton sein aigu, que la blancheur décore,</l>
					<l n="80" num="1.80">Tes cheveux rougissants ont des éclats d’aurore.</l>
					<l n="81" num="1.81">Encor tout jeune enfant par le jour ébloui,</l>
					<l n="82" num="1.82">J’eus pour maître Eumolpos, et je puis, comme lui,</l>
					<l n="83" num="1.83">Célébrer la fierté charmante et le sourire</l>
					<l n="84" num="1.84">D’une déesse blonde, ayant tenu la lyre.</l>
					<l n="85" num="1.85">Mais lorsque je parus sous le regard serein</l>
					<l n="86" num="1.86">Des cieux, portant cet arc et ce glaive d’airain,</l>
					<l n="87" num="1.87">La terre gémissait, nourrice des colosses,</l>
					<l n="88" num="1.88">Sous la dent des brigands et des bêtes féroces.</l>
					<l n="89" num="1.89">Des bandits, embusqués près de chaque buisson,</l>
					<l n="90" num="1.90">Arrêtaient le passant pour en tirer rançon ;</l>
					<l n="91" num="1.91">Dans leur démence avide, ils bravaient les tonnerres</l>
					<l n="92" num="1.92">De Zeus ; tout leur cédait, et les plus sanguinaires,</l>
					<l n="93" num="1.93">Ayant jeté l’effroi dans les murs belliqueux</l>
					<l n="94" num="1.94">Des villes, emmenaient les vierges avec eux.</l>
					<l n="95" num="1.95">Les dieux même oubliaient la justice. La peste</l>
					<l n="96" num="1.96">Soufflait sinistrement son haleine funeste</l>
					<l n="97" num="1.97">Dans les marais par l’eau dormante empoisonnés ;</l>
					<l n="98" num="1.98">Mordant les arbres noirs déjà déracinés,</l>
					<l n="99" num="1.99">Des monstres surgissaient, hideux, couverts d’écailles</l>
					<l n="100" num="1.100">Renaissant du sang vil versé dans leurs batailles.</l>
					<l n="101" num="1.101">De lourds dragons ailés se traînaient sur les eaux</l>
					<l n="102" num="1.102">Dans leur bave, jetant le feu par leurs naseaux,</l>
					<l n="103" num="1.103">Et flétrissaient les fleurs de leurs souffles infâmes.</l>
					<l n="104" num="1.104">Ô guerrière fidèle, est-ce toi qui me blâmes ?</l>
					<l n="105" num="1.105">Quand j’avais nettoyé les sourds marais dormants</l>
					<l n="106" num="1.106">En détournant le cours d’un fleuve aux diamants</l>
					<l n="107" num="1.107">Glacés ; quand les dragons, le long des feuilles sèches,</l>
					<l n="108" num="1.108">Se traînaient sur le sol, déchirés par mes flèches,</l>
					<l n="109" num="1.109">J’allais porter secours à des vierges, tes sœurs ;</l>
					<l n="110" num="1.110">Je tuais les brigands furtifs, les ravisseurs,</l>
					<l n="111" num="1.111">Et, près des lacs noyés dans les vapeurs confuses,</l>
					<l n="112" num="1.112">J’écrasais de mes mains les artisans de ruses,</l>
					<l n="113" num="1.113">Afin de ne plus voir leurs vols insidieux,</l>
					<l n="114" num="1.114">Et sans m’inquiéter s’ils étaient rois ni dieux !</l>
					<l n="115" num="1.115">Reine, tu te trompais, tout ce qui souffre m’aime.</l>
					<l n="116" num="1.116">Ah ! Si j’ai quelquefois combattu pour moi-même</l>
					<l n="117" num="1.117">Et pour sacrifier à mon orgueil, du moins</l>
					<l n="118" num="1.118">Ce fut contre les dieux indolents, qui, témoins</l>
					<l n="119" num="1.119">De mes travaux, craignaient la terre rajeunie,</l>
					<l n="120" num="1.120">Et mettaient pour une heure obstacle à mon génie.</l>
					<l n="121" num="1.121">Oui, parfois, las d’errer seul dans leurs durs exils,</l>
					<l n="122" num="1.122">Je les ai défiés ; mais comment pouvaient-ils,</l>
					<l n="123" num="1.123">Sans craindre avec raison que tout s’anéantisse,</l>
					<l n="124" num="1.124">Entraver le héros qui s’appelle Justice ?</l>
					<l n="125" num="1.125">Et ne savaient-ils pas que, sur cet astre noir,</l>
					<l n="126" num="1.126">Si tout les nomme loi, je me nomme devoir ?</l>
					<l n="127" num="1.127">Quand, cherchant, pour ma tâche incessamment subie,</l>
					<l n="128" num="1.128">Les bœufs de Géryon, j’entrai dans la Libye,</l>
					<l n="129" num="1.129">Le dieu Soleil lança sur moi ses traits de feu,</l>
					<l n="130" num="1.130">Et moi, de même aussi, je lançai sur le dieu</l>
					<l n="131" num="1.131">Mes flèches, et je vis vaciller à la voûte</l>
					<l n="132" num="1.132">Céleste sa lumière, et je repris ma route</l>
					<l n="133" num="1.133">Sur l’orageuse mer, dans une barque d’or.</l>
					<l n="134" num="1.134">Quand donc ai-je offensé la vertu, mon trésor !</l>
					<l n="135" num="1.135">J’ai combattu la mort qui voulait prendre Alceste ;</l>
					<l n="136" num="1.136">J’ai violé la nuit de l’Hadès, où l’inceste</l>
					<l n="137" num="1.137">Gémit, et j’ai marché dans le nid du vautour,</l>
					<l n="138" num="1.138">Mais pour rendre Thésée à la clarté du jour !</l>
					<l n="139" num="1.139">La femme, dont le front abrite un saint mystère,</l>
					<l n="140" num="1.140">Est la divinité visible de la terre.</l>
					<l n="141" num="1.141">Elle est comme un parfum dans de riches coffrets ;</l>
					<l n="142" num="1.142">Ses cheveux embaumés ressemblent aux forêts ;</l>
					<l n="143" num="1.143">Son corps harmonieux a la blancheur insigne</l>
					<l n="144" num="1.144">De la neige des monts et de l’aile du cygne ;</l>
					<l n="145" num="1.145">Habile comme nous à dompter les chevaux,</l>
					<l n="146" num="1.146">Elle affronte la guerre auguste, les travaux</l>
					<l n="147" num="1.147">Du glaive, et comme nous, depuis qu’elle respire,</l>
					<l n="148" num="1.148">Sait éveiller les chants qui dorment dans la lyre.</l>
					<l n="149" num="1.149">C’est pour elle, qui prend notre âme sur le seuil</l>
					<l n="150" num="1.150">De la vie, et pour voir ses yeux briller d’orgueil,</l>
					<l n="151" num="1.151">Que j’allais écrasant les hydres dans la plaine,</l>
					<l n="152" num="1.152">Sachant, esprit mêlé d’azur, qu’elle est sa haine</l>
					<l n="153" num="1.153">Contre l’impureté des animaux rampants.</l>
					<l n="154" num="1.154">Partout, guidant ses pas sur le front des serpents,</l>
					<l n="155" num="1.155">Et cherchant sans repos la clarté poursuivie,</l>
					<l n="156" num="1.156">J’ai détesté le meurtre et protégé la vie ;</l>
					<l n="157" num="1.157">Et, calme, usant mes mains à déchirer des fers,</l>
					<l n="158" num="1.158">Quand je ne trouvais plus, entrant dans les déserts</l>
					<l n="159" num="1.159">Les bandits à détruire et leurs embûches viles,</l>
					<l n="160" num="1.160">J’y tuais des lions et j’y laissais des villes !</l>
					<l n="161" num="1.161">Et si, toujours le bras armé, toujours vainqueur,</l>
					<l n="162" num="1.162">J’ai répandu le sang humain, c’est que mon cœur</l>
					<l n="163" num="1.163">Est rempli de courroux contre les impostures,</l>
					<l n="164" num="1.164">Et que je ne puis voir souffrir les créatures. »</l>
					<l n="165" num="1.165">La grande Omphale avait les yeux baignés de pleurs.</l>
					<l n="166" num="1.166">Palpitante, le front tout blêmi des pâleurs</l>
					<l n="167" num="1.167">De l’amour, comme un ciel balayé par l’orage</l>
					<l n="168" num="1.168">S’éclaire, elle sentait les dédains et la rage</l>
					<l n="169" num="1.169">Loin de son cœur blessé déjà prendre leur vol</l>
					<l n="170" num="1.170">Vers le mystérieux enfer, et sur le sol</l>
					<l n="171" num="1.171">Tout brûlé des ardeurs de l’âpre canicule,</l>
					<l n="172" num="1.172">Elle s’agenouilla, baisant les pieds d’Hercule.</l>
					<l n="173" num="1.173">Elle courbait son front orgueilleux et vaincu,</l>
					<l n="174" num="1.174">Et ses lourds cheveux roux couvraient son sein aigu.</l>
					<l n="175" num="1.175">« Digne race des dieux ! Vengeur, ô fils d’Alcmène,</l>
					<l n="176" num="1.176">Dit-elle, j’ai rêvé. Qui donc parlait de haine ?</l>
					<l n="177" num="1.177">Je t’ai volé cet arc pris sur le Pélion,</l>
					<l n="178" num="1.178">Tes flèches, cette peau sanglante de lion,</l>
					<l n="179" num="1.179">Et ce glaive toujours fumant, tes nobles armes.</l>
					<l n="180" num="1.180">Vois, je lave à présent tes pieds avec mes larmes.</l>
					<l n="181" num="1.181">Ces joyaux, dont les feux embrasent mes habits,</l>
					<l n="182" num="1.182">Cette ceinture d’or brillant, où les rubis</l>
					<l n="183" num="1.183">Se heurtent quand je marche avec un bruit sonore,</l>
					<l n="184" num="1.184">Sont mes armes aussi, que l’univers adore</l>
					<l n="185" num="1.185">Et qu’a su conquérir la valeur de mon bras ;</l>
					<l n="186" num="1.186">Tu peux me les ôter, ami, quand tu voudras.</l>
					<l n="187" num="1.187">Mais, afin que je sois à jamais célébrée</l>
					<l n="188" num="1.188">Par les chanteurs épars sous la voûte azurée,</l>
					<l n="189" num="1.189">Et que cette quenouille, où seule j’ai filé</l>
					<l n="190" num="1.190">La blanche laine en mon asile inviolé,</l>
					<l n="191" num="1.191">À jamais parmi les mortels surpasse en gloire</l>
					<l n="192" num="1.192">Le foudre ailé du roi Zeus et la lance noire</l>
					<l n="193" num="1.193">D’Athènè, qui frémit sur son bras inhumain,</l>
					<l n="194" num="1.194">Daigne, oh ! Daigne toucher avec ta noble main</l>
					<l n="195" num="1.195">Cette quenouille, chaude encor de mon haleine,</l>
					<l n="196" num="1.196">Où je filais d’un doigt pensif la blanche laine,</l>
					<l n="197" num="1.197">Et songe que ma mère a tenu ce morceau</l>
					<l n="198" num="1.198">D’ivoire, en m’endormant dans mon petit berceau ! »</l>
					<l n="199" num="1.199">Hercule souriait, penché ; la chevelure</l>
					<l n="200" num="1.200">D’Omphale frissonnait près de sa gorge pure.</l>
					<l n="201" num="1.201">La lydienne, avec la douceur des bourreaux,</l>
					<l n="202" num="1.202">Languissante, et levant vers les yeux du héros</l>
					<l n="203" num="1.203">Ses yeux de violette où flotte une ombre noire,</l>
					<l n="204" num="1.204">Lui posa dans les mains sa quenouille d’ivoire.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1861">juin 1861</date>.
					</dateline>
				</closer>
			</div></body></text></TEI>