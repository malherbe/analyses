<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Exilés</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4189 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">BAN_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>

			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Exilés</title>
						<author>Théodore de BANVILLE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L923</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
									<title>Les Exilés</title>
									<author>Théodore de BANVILLE</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Fasquelle</publisher>
									<date when="1899">1899</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>

		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1867">1867</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface n’a pas été encodée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN150">
				<head type="main">L’ÉDUCATION DE L’AMOUR</head>
				<lg n="1">
					<l n="1" num="1.1">Quand le premier des dieux, amour, pendant mille ans</l>
					<l n="2" num="1.2">Eut tenu sous son joug les cieux étincelants,</l>
					<l n="3" num="1.3">La terre immense et tous les êtres qui respirent,</l>
					<l n="4" num="1.4">Las de souffrir par lui, les immortels se dirent :</l>
					<l n="5" num="1.5">« Ah ! Qu’un autre vainqueur, formidable et serein,</l>
					<l n="6" num="1.6">Paraisse, armé de l’arc et des flèches d’airain ;</l>
					<l n="7" num="1.7">Qu’il porte dans un flot de flamme et de fumée</l>
					<l n="8" num="1.8">Sa torche au Phlégéthon furieux allumée ;</l>
					<l n="9" num="1.9">Qu’il étende sur tous l’inflexible niveau,</l>
					<l n="10" num="1.10">Et nous respirerons sous ce maître nouveau.</l>
					<l n="11" num="1.11">Car comment sa colère, où grondera l’orage,</l>
					<l n="12" num="1.12">Pourrait-elle égaler jamais l’aveugle rage</l>
					<l n="13" num="1.13">Du dieu titan, du roi funeste qui n’eut pas</l>
					<l n="14" num="1.14">De mère, et qui sema la terreur sur ses pas</l>
					<l n="15" num="1.15">Quand frémissaient encor du mot qui les sépare</l>
					<l n="16" num="1.16">Le noir chaos, la terre énorme et le tartare ! »</l>
					<l n="17" num="1.17"><space quantity="2" unit="char"></space>Tels les Olympiens se plaignaient dans l’éther.</l>
					<l n="18" num="1.18">Bientôt d’une déesse à l’œil limpide et fier</l>
					<l n="19" num="1.19">Un autre Éros naquit, charmant, sa lèvre pure</l>
					<l n="20" num="1.20">Tout en fleur, agitant de l’or pour chevelure</l>
					<l n="21" num="1.21">Et portant haut son front de neige, où resplendit</l>
					<l n="22" num="1.22">L’éclat sacré du jour. Mais quand Zeus entendit</l>
					<l n="23" num="1.23">Ses premiers bégaiements, plus doux qu’un chant de lyre,</l>
					<l n="24" num="1.24">Quand il vit ses regards de femme et son sourire</l>
					<l n="25" num="1.25">Où la caresse, les aveux, les doux refus</l>
					<l n="26" num="1.26">Erraient, il devina dans l’avenir confus</l>
					<l n="27" num="1.27">Tant de colère, tant de larmes, tant de crimes</l>
					<l n="28" num="1.28">Hâtant leurs pieds sanglants sur le bord des abîmes,</l>
					<l n="29" num="1.29">Tant de douleurs penchant le front, tant de remords</l>
					<l n="30" num="1.30">Hurlant de longs sanglots à l’oreille des morts ;</l>
					<l n="31" num="1.31">Il vit si clairement la trahison vivante,</l>
					<l n="32" num="1.32">Qu’il sentit dans son cœur s’amasser l’épouvante,</l>
					<l n="33" num="1.33">Et fronça par trois fois son sourcil triomphant.</l>
					<l n="34" num="1.34">Alors il ordonna que le petit enfant,</l>
					<l n="35" num="1.35">Nu, froid, maudit, victime au noir Hadès offerte,</l>
					<l n="36" num="1.36">Fût porté dans le fond d’une forêt déserte</l>
					<l n="37" num="1.37">De l’Inde, dans un lieu du jour même exécré,</l>
					<l n="38" num="1.38">Où jamais l’homme ni les dieux n’ont pénétré,</l>
					<l n="39" num="1.39">Et dont les sourds abris et les rochers colosses</l>
					<l n="40" num="1.40">N’ont pour hôtes vivants que des bêtes féroces.</l>
					<l n="41" num="1.41"><space quantity="2" unit="char"></space>C’était un bois funèbre et pourtant merveilleux ;</l>
					<l n="42" num="1.42">Splendide et noir, baignant ses pieds dans les flots bleus</l>
					<l n="43" num="1.43">D’un golfe de saphir. Debout près de cette onde,</l>
					<l n="44" num="1.44">Il la voyait depuis les premiers jours du monde</l>
					<l n="45" num="1.45">Réfléchir son front noir. Tel son abri géant</l>
					<l n="46" num="1.46">Était sorti de l’ombre et du chaos béant,</l>
					<l n="47" num="1.47">Tel il avait grandi, sans que nulle aventure</l>
					<l n="48" num="1.48">Entamât une fois sa frondaison obscure,</l>
					<l n="49" num="1.49">Et sans que la bataille humaine aux durs éclairs</l>
					<l n="50" num="1.50">Tourmentât follement ses lacs profonds et clairs.</l>
					<l n="51" num="1.51"><space quantity="2" unit="char"></space>Les aloès, les grands tulipiers aux fleurs jaunes</l>
					<l n="52" num="1.52">Vivaient sans avoir vu les nymphes et les faunes</l>
					<l n="53" num="1.53">Qui brisent des rameaux pour en orner leur front.</l>
					<l n="54" num="1.54">Les énormes jasmins fleurissaient sans affront ;</l>
					<l n="55" num="1.55">D’autres arbres mêlaient, comme un riche cortège,</l>
					<l n="56" num="1.56">Des corolles de sang à des feuilles de neige.</l>
					<l n="57" num="1.57">Au fond d’un antre noir d’érables entouré,</l>
					<l n="58" num="1.58">Tout à coup surgissait un fleuve enamouré,</l>
					<l n="59" num="1.59">Mystérieux, baisant ses rives délicates</l>
					<l n="60" num="1.60">Et, par endroits, bordé de lotus écarlates.</l>
					<l n="61" num="1.61">Puis des rocs ; puis des monts neigeux, où les torrents</l>
					<l n="62" num="1.62">Charriaient des rubis ; dans les lointains mourants,</l>
					<l n="63" num="1.63">On ne sait quel flot bleu passe, et traverse encore</l>
					<l n="64" num="1.64">L’insondable océan de verdure sonore.</l>
					<l n="65" num="1.65"><space quantity="2" unit="char"></space>Là, la création gigantesque apparaît</l>
					<l n="66" num="1.66">Toute nue. Un figuier plus grand qu’une forêt</l>
					<l n="67" num="1.67">Enfonce avec fierté, grand aïeul solitaire,</l>
					<l n="68" num="1.68">Trois cents troncs effrayants dans le cœur de la terre</l>
					<l n="69" num="1.69">Pour y prendre le suc de ses fruits au doux miel,</l>
					<l n="70" num="1.70">Et par mille rameaux boit la clarté du ciel.</l>
					<l n="71" num="1.71">Puis une fleur qui, même auprès du figuier, semble</l>
					<l n="72" num="1.72">Prodigieuse, au fond d’un calice qui tremble</l>
					<l n="73" num="1.73">Garde assez d’eau de pluie, alors que la forêt</l>
					<l n="74" num="1.74">Brûle, pour faire boire un titan qui viendrait.</l>
					<l n="75" num="1.75">Ses boutons, sur lesquels un épervier se pose,</l>
					<l n="76" num="1.76">Qui paraissent des blocs polis de marbre rose,</l>
					<l n="77" num="1.77">Et que ne peut ouvrir le soleil étouffant,</l>
					<l n="78" num="1.78">Ont déjà la grosseur d’une tête d’enfant.</l>
					<l n="79" num="1.79"><space quantity="2" unit="char"></space>La vigne monstrueuse étreint les arbres comme</l>
					<l n="80" num="1.80">Un lutteur, puis en troncs pareils à des corps d’homme</l>
					<l n="81" num="1.81">Retombe, puis remonte et va bondir plus loin.</l>
					<l n="82" num="1.82">La végétation en démence n’a soin</l>
					<l n="83" num="1.83">Que de cacher le ciel avec ses créatures.</l>
					<l n="84" num="1.84">Le feuillage se dresse en mille architectures,</l>
					<l n="85" num="1.85">Forme une colonnade aux corridors profonds,</l>
					<l n="86" num="1.86">Sur les pics effarés pose de noirs plafonds,</l>
					<l n="87" num="1.87">Tapisse l’antre, grimpe aux montagnes, s’élance</l>
					<l n="88" num="1.88">Dans l’air bleu, tout à coup éclate en fers de lance,</l>
					<l n="89" num="1.89">Puis, noire frondaison que l’œil en vain poursuit,</l>
					<l n="90" num="1.90">Devient un néant fait de verdure et de nuit,</l>
					<l n="91" num="1.91">Là ruisselle de pourpre et d’argent, partout maître</l>
					<l n="92" num="1.92">Du sol, dans la liane en courant s’enchevêtre ;</l>
					<l n="93" num="1.93">Et des gémissements, des hurlements, des cris</l>
					<l n="94" num="1.94">Retentissent. Au bas des lourds buissons fleuris,</l>
					<l n="95" num="1.95">Des prunelles de flamme, ainsi que des phalènes,</l>
					<l n="96" num="1.96">S’allument, et l’on sent se croiser des haleines.</l>
					<l n="97" num="1.97">Aux racines traînant leurs cheveux, sont mêlés</l>
					<l n="98" num="1.98">Des reptiles ; dans les rameaux échevelés</l>
					<l n="99" num="1.99">Volent de grands oiseaux peints d’azur et de soufre ;</l>
					<l n="100" num="1.100">Des yeux rouges parmi l’obscurité du gouffre</l>
					<l n="101" num="1.101">Luisent, et les petits des louves dans leurs jeux</l>
					<l n="102" num="1.102">Se détachent tout noirs sur un plateau neigeux</l>
					<l n="103" num="1.103">Où brillent sur le blanc tapis jonché de branches</l>
					<l n="104" num="1.104">Des flaques de sang rose et des carcasses blanches.</l>
					<l n="105" num="1.105"><space quantity="2" unit="char"></space>Donc le petit enfant Éros fut apporté</l>
					<l n="106" num="1.106">Dans cette forêt, où, de spectres escorté,</l>
					<l n="107" num="1.107">Le meurtre au front joyeux par les espaces vides</l>
					<l n="108" num="1.108">Court, teignant dans le sang mille gueules avides,</l>
					<l n="109" num="1.109">Où la nature vierge, ivre de son pouvoir,</l>
					<l n="110" num="1.110">Sachant bien que les dieux ne peuvent pas la voir,</l>
					<l n="111" num="1.111">Heurte ses ouragans, ses ondes, ses tonnerres,</l>
					<l n="112" num="1.112">Brise les rocs, meurtrit les arbres centenaires,</l>
					<l n="113" num="1.113">Déchaîne, groupe fou vers le mal entraîné,</l>
					<l n="114" num="1.114">Ses forces qu’elle emporte en un vol effréné</l>
					<l n="115" num="1.115">Et que jamais les lois célestes ne modèrent.</l>
					<l n="116" num="1.116">Quand il fut là, les grands lions le regardèrent.</l>
					<l n="117" num="1.117"><space quantity="2" unit="char"></space>Puis vinrent les bœufs blancs bossus, les loups aux dents</l>
					<l n="118" num="1.118">D’ivoire, le chacal, le tigre aux yeux ardents,</l>
					<l n="119" num="1.119">Les léopards, les lynx, les onces, les panthères,</l>
					<l n="120" num="1.120">Les sangliers, les doux éléphants solitaires,</l>
					<l n="121" num="1.121">L’hyène ; puis, sortis des arbres à leur tour,</l>
					<l n="122" num="1.122">Les oiseaux, l’aigle altier, le milan, le vautour</l>
					<l n="123" num="1.123">Cachant dans un lambeau souillé son bec infâme,</l>
					<l n="124" num="1.124">Les condors dont le vol est comme un jet de flamme,</l>
					<l n="125" num="1.125">Les rapides faucons, l’épervier qui sait voir</l>
					<l n="126" num="1.126">L’infini, le corbeau capuchonné de noir</l>
					<l n="127" num="1.127">Dont l’aile suit d’en haut les guerres infertiles,</l>
					<l n="128" num="1.128">Et les paons somptueux qui mangent des reptiles ;</l>
					<l n="129" num="1.129">Puis les serpents aux plis hideux ; et tous, formant</l>
					<l n="130" num="1.130">Un cercle, regardaient le pauvre être charmant</l>
					<l n="131" num="1.131">Sans défense, et déjà savouraient avec joie</l>
					<l n="132" num="1.132">La douceur de meurtrir cette facile proie.</l>
					<l n="133" num="1.133"><space quantity="2" unit="char"></space>Mais tout à coup, lancé d’en haut par l’arc vermeil</l>
					<l n="134" num="1.134">D’Apollon, un trait d’or, un rayon de soleil</l>
					<l n="135" num="1.135">Enflamma les cheveux d’Éros, sa lèvre rose,</l>
					<l n="136" num="1.136">Son front pur, sa narine où le désir repose,</l>
					<l n="137" num="1.137">Et, miracle ! Sur son doux visage, le dieu,</l>
					<l n="138" num="1.138">Le meurtrier parut, et, sur sa bouche au feu</l>
					<l n="139" num="1.139">Céleste et dans ses yeux brûlants qui nous attirent,</l>
					<l n="140" num="1.140">Ce que Zeus avait vu, ces animaux le virent.</l>
					<l n="141" num="1.141">Ils se dirent alors dans leur langage obscur :</l>
					<l n="142" num="1.142"><space quantity="2" unit="char"></space>« Pourquoi tuer ce prince, échappé de l’azur ?</l>
					<l n="143" num="1.143">Regardez sa prunelle aventureuse, où nage</l>
					<l n="144" num="1.144">Dans la poussière d’or l’appétit du carnage,</l>
					<l n="145" num="1.145">Et ce sourire fait de miel et de poison,</l>
					<l n="146" num="1.146">Où déjà les baisers menteurs, la trahison,</l>
					<l n="147" num="1.147">Le meurtre, le courroux, les embûches, la ruse</l>
					<l n="148" num="1.148">Naissent, et cet attrait de l’enfance confuse</l>
					<l n="149" num="1.149">Dont sa mère a paré l’éternel ennemi !</l>
					<l n="150" num="1.150">Qui mieux que cet enfant né dans les cieux, parmi</l>
					<l n="151" num="1.151">Les éblouissements formidables des astres,</l>
					<l n="152" num="1.152">Sèmera sur ses pas la haine et les désastres,</l>
					<l n="153" num="1.153">Accablera de maux sans fin l’homme odieux</l>
					<l n="154" num="1.154">Et saura nous venger de la race des dieux ?</l>
					<l n="155" num="1.155"><space quantity="2" unit="char"></space>Puisqu’il doit, ce fléau de la faiblesse humaine,</l>
					<l n="156" num="1.156">Prospérer pour le crime et grandir pour la haine,</l>
					<l n="157" num="1.157">Ne le déchirons pas ! Qu’il vive parmi nous</l>
					<l n="158" num="1.158">Dans la grande forêt des vautours et des loups,</l>
					<l n="159" num="1.159">Où nul abri ne peut servir au daim timide,</l>
					<l n="160" num="1.160">Où, sous le verdoyant gazon toujours humide,</l>
					<l n="161" num="1.161">La terre boit toujours du sang frais, où la mort,</l>
					<l n="162" num="1.162">Toujours prête et jamais lassée, égorge et mord</l>
					<l n="163" num="1.163">Et dévore la vie, et comme elle fourmille.</l>
					<l n="164" num="1.164">Élevons-le plutôt ; nous serons sa famille. »</l>
					<l n="165" num="1.165"><space quantity="2" unit="char"></space>Sous l’ombrage, écartant les rameaux querelleurs,</l>
					<l n="166" num="1.166">Ils lui firent un lit de feuilles et de fleurs,</l>
					<l n="167" num="1.167">Et sous ses boucles d’or, doucement protégées,</l>
					<l n="168" num="1.168">Ils mirent des toisons de bêtes égorgées.</l>
					<l n="169" num="1.169">Les louves, s’avançant vers lui d’un pas hautain</l>
					<l n="170" num="1.170">Léchaient pour le polir son visage enfantin ;</l>
					<l n="171" num="1.171">Les lionnes voyant qu’il était fier comme elles,</l>
					<l n="172" num="1.172">Sur sa bouche de rose abaissaient leurs mamelles ;</l>
					<l n="173" num="1.173">Les gueules aux crocs blancs, ces fournaises de feu,</l>
					<l n="174" num="1.174">Baisaient le petit roi frissonnant du ciel bleu.</l>
					<l n="175" num="1.175">Des serpents, s’enroulant sur sa gorge ivoirine,</l>
					<l n="176" num="1.176">S’étalaient en colliers vermeils sur sa poitrine ;</l>
					<l n="177" num="1.177">D’autres, tordant leurs nœuds en soyeux annelets,</l>
					<l n="178" num="1.178">À ses jolis bras nus faisaient des bracelets,</l>
					<l n="179" num="1.179">Et, comme un pharaon d’Égypte, en son repaire</l>
					<l n="180" num="1.180">Il avait pour bandeau royal une vipère.</l>
					<l n="181" num="1.181"><space quantity="2" unit="char"></space>Tout ce qui sait combattre et détruire et briser</l>
					<l n="182" num="1.182">L’enveloppait ainsi d’un immense baiser.</l>
					<l n="183" num="1.183">Le dieu, passant de l’une à l’autre en ses caprices,</l>
					<l n="184" num="1.184">Buvait avidement le lait de ses nourrices,</l>
					<l n="185" num="1.185">Tout joyeux d’assouvir ses rudes appétits</l>
					<l n="186" num="1.186">De héros, ne laissait plus rien pour leurs petits,</l>
					<l n="187" num="1.187">Et, chaque soir, gorgé de vie et de caresses,</l>
					<l n="188" num="1.188">Il s’endormait repu sur le flanc des tigresses.</l>
					<l n="189" num="1.189"><space quantity="2" unit="char"></space>Au réveil, tous ces durs artisans de trépas</l>
					<l n="190" num="1.190">Étayaient de leurs corps puissants les premiers pas</l>
					<l n="191" num="1.191">De l’exilé divin, né pour la grande lutte,</l>
					<l n="192" num="1.192">L’aidant, le consolant d’une légère chute,</l>
					<l n="193" num="1.193">Et lui donnant aussi pour supporter le mal</l>
					<l n="194" num="1.194">La résignation morne de l’animal.</l>
					<l n="195" num="1.195">Il grandit, il devint fauve comme ses hôtes,</l>
					<l n="196" num="1.196">Marchant, courant déjà parmi les herbes hautes,</l>
					<l n="197" num="1.197">Nu, superbe, et portant, sauvage enfantelet,</l>
					<l n="198" num="1.198">Sur son épaule en fleur, que le soleil hâlait</l>
					<l n="199" num="1.199">Et dévorait jusqu’à l’heure du crépuscule,</l>
					<l n="200" num="1.200">La peau d’un lionceau, comme un petit hercule.</l>
					<l n="201" num="1.201">Lui-même, de sa main mignonne, avait cueilli</l>
					<l n="202" num="1.202">La massue ; alors ceux qui l’avaient recueilli</l>
					<l n="203" num="1.203">Connurent qu’ils pouvaient, sans tarder davantage,</l>
					<l n="204" num="1.204">Donner au jeune roi des leçons de carnage.</l>
					<l n="205" num="1.205"><space quantity="2" unit="char"></space>Son heure était venue, et, déjà belliqueux,</l>
					<l n="206" num="1.206">Il s’en alla dès lors à la chasse avec eux.</l>
					<l n="207" num="1.207">Comme Ariane dans Naxos, l’île enchantée,</l>
					<l n="208" num="1.208">Étendu sur un tigre à la peau tachetée,</l>
					<l n="209" num="1.209">Il les suivait, mêlant sa voix aux hurlements ;</l>
					<l n="210" num="1.210">Joyeux, montrant devant les torrents écumants</l>
					<l n="211" num="1.211">L’impassibilité magnifique des bêtes,</l>
					<l n="212" num="1.212">Il s’en allait pensif en guerre, en chasse, aux fêtes,</l>
					<l n="213" num="1.213">Au meurtre, et quand passaient, avec des bonds soudains,</l>
					<l n="214" num="1.214">La gazelle aux yeux bleus, l’antilope, les daims,</l>
					<l n="215" num="1.215">Les chèvres, les troupeaux de cerfs, les bœufs difformes,</l>
					<l n="216" num="1.216">Son tigre le posait sous les feuilles énormes,</l>
					<l n="217" num="1.217">Dans une solitude où rien ne le gardait,</l>
					<l n="218" num="1.218">Et là, les yeux tout grands ouverts, il regardait.</l>
					<l n="219" num="1.219"><space quantity="2" unit="char"></space>Il voyait le combat sinistre, la vaillance,</l>
					<l n="220" num="1.220">La victoire, comment le fier lion s’élance</l>
					<l n="221" num="1.221">Sur sa victime avec de grands bonds souverains,</l>
					<l n="222" num="1.222">La terrasse d’un coup de griffe sur les reins,</l>
					<l n="223" num="1.223">Puis la déchire ; et quand ce beau guerrier qui tue</l>
					<l n="224" num="1.224">Marchait, crinière au vent, sur sa proie abattue,</l>
					<l n="225" num="1.225">Quand le cerf éventré sur la terre appelait</l>
					<l n="226" num="1.226">Sa compagne en versant des larmes, et râlait,</l>
					<l n="227" num="1.227">Quand tout n’était que deuil, massacres, funérailles,</l>
					<l n="228" num="1.228">Quand le sol tout humide était jonché d’entrailles,</l>
					<l n="229" num="1.229">Quand tout autour du bois l’épouvante criait,</l>
					<l n="230" num="1.230">Le petit Éros blond et charmant souriait.</l>
					<l n="231" num="1.231"><space quantity="2" unit="char"></space>Plus tard même il entra nu parmi ces mêlées.</l>
					<l n="232" num="1.232">Ses tresses d’or au vent orageux déroulées,</l>
					<l n="233" num="1.233">Et sur les monts toujours le premier aux assauts</l>
					<l n="234" num="1.234">Il aidait à leurs jeux les petits lionceaux,</l>
					<l n="235" num="1.235">Se jetant sur sa proie, étouffant dans ses courses</l>
					<l n="236" num="1.236">D’humbles victimes ; puis se lavant dans les sources,</l>
					<l n="237" num="1.237">Et n’ayant rien qui hors le combat lui fût cher ;</l>
					<l n="238" num="1.238">Dépeçant, enfonçant ses ongles dans la chair,</l>
					<l n="239" num="1.239">Dans les cris des mourants cherchant des harmonies</l>
					<l n="240" num="1.240">Et tout le long du jour enivré d’agonies,</l>
					<l n="241" num="1.241">De râles, de sanglots et de cris triomphants,</l>
					<l n="242" num="1.242">Excitant les lions contre les éléphants,</l>
					<l n="243" num="1.243">Tuant et se gorgeant de meurtre avec délices,</l>
					<l n="244" num="1.244">Poussant d’un pied haineux la panthère et les lices,</l>
					<l n="245" num="1.245">Donnant la chasse même aux monstres inconnus,</l>
					<l n="246" num="1.246">Pour les atteindre mieux montant des chevaux nus,</l>
					<l n="247" num="1.247">Orgueilleux de pouvoir, en ses fières allures,</l>
					<l n="248" num="1.248">Mordre, briser des dents, tordre des chevelures,</l>
					<l n="249" num="1.249">Et s’éveillant aussi quand le tigre avait faim.</l>
					<l n="250" num="1.250">C’est ainsi que l’enfant jouait, et lorsqu’enfin</l>
					<l n="251" num="1.251">Las de voir sur les monts tout souillés de sa gloire</l>
					<l n="252" num="1.252">De larges ruisseaux noirs baigner ses pieds d’ivoire,</l>
					<l n="253" num="1.253">Il posait sa massue inerte sur son flanc,</l>
					<l n="254" num="1.254">Ses mains et ses bras nus étaient rouges de sang.</l>
					<l n="255" num="1.255"><space quantity="2" unit="char"></space>Pour rendre devant lui toute feinte inutile,</l>
					<l n="256" num="1.256">Il pouvait au besoin ramper comme un reptile ;</l>
					<l n="257" num="1.257">Il savait, se voilant d’un sourire amical,</l>
					<l n="258" num="1.258">Des cruautés de loup, des ruses de chacal,</l>
					<l n="259" num="1.259">Attendait l’ennemi dans l’ombre, et, taciturne,</l>
					<l n="260" num="1.260">Avait des yeux de feu comme un hibou nocturne.</l>
					<l n="261" num="1.261">Comme le bouc lascif il grimpait sur les rocs,</l>
					<l n="262" num="1.262">Et, sans être effrayé de leurs terribles chocs,</l>
					<l n="263" num="1.263">En poussant dans le flot sonore un bloc de marbre</l>
					<l n="264" num="1.264">S’élançait, comme un singe, aux minces branches d’arbre.</l>
					<l n="265" num="1.265"><space quantity="2" unit="char"></space>Puis, trouvant qu’il était le plus doux des fardeaux,</l>
					<l n="266" num="1.266">Les aigles, les condors l’emportaient sur leur dos,</l>
					<l n="267" num="1.267">Et, calme, il traversait l’éther comme une plume.</l>
					<l n="268" num="1.268">Souvent une cascade affreuse au front d’écume</l>
					<l n="269" num="1.269">Sans arrêter leur vol tombait sur leur chemin.</l>
					<l n="270" num="1.270">Le dieu, pâle et riant, essuyait de sa main</l>
					<l n="271" num="1.271">Le vaste flot poudreux qui lui fouettait la face</l>
					<l n="272" num="1.272">Et dans l’air ébloui continuait sa chasse,</l>
					<l n="273" num="1.273">Fondant comme un milan sur quelque oiseau ravi,</l>
					<l n="274" num="1.274">Et tout aise et criant quand l’aigle inassouvi,</l>
					<l n="275" num="1.275">Ayant vu sur la terre une proie assez belle,</l>
					<l n="276" num="1.276">Descendait de l’azur et s’élançait sur elle,</l>
					<l n="277" num="1.277">Et, pour mieux divertir l’enfant malicieux,</l>
					<l n="278" num="1.278">L’emportait pantelante au plus profond des cieux.</l>
					<l n="279" num="1.279"><space quantity="2" unit="char"></space>Souvent encor, parmi les riants groupes d’îles</l>
					<l n="280" num="1.280">Éros voguait, porté par de bruns crocodiles,</l>
					<l n="281" num="1.281">Apprenant d’eux comment dans les ruisseaux taris,</l>
					<l n="282" num="1.282">Cachés par les joncs verts, ils imitent les cris</l>
					<l n="283" num="1.283">D’un nouveau-né qui pleure ; il suivait les batailles</l>
					<l n="284" num="1.284">Des poissons monstrueux aux luisantes écailles ;</l>
					<l n="285" num="1.285">Hôte guerrier du fleuve, il nageait sur ses bords</l>
					<l n="286" num="1.286">Près des chevaux marins et des alligators,</l>
					<l n="287" num="1.287">Ou parfois, se cachant dans une île écartée,</l>
					<l n="288" num="1.288">Penchait ses yeux ravis sur l’onde ensanglantée.</l>
					<l n="289" num="1.289"><space quantity="2" unit="char"></space>Enfin il se lassa de ces monstres soumis.</l>
					<l n="290" num="1.290">Ayant pensé qu’ailleurs de puissants ennemis</l>
					<l n="291" num="1.291">Pourraient occuper mieux sa bravoure et ses charmes,</l>
					<l n="292" num="1.292">Il voulut se munir de véritables armes</l>
					<l n="293" num="1.293">Pour secouer l’ennui d’un repos importun,</l>
					<l n="294" num="1.294">Et, quoiqu’il n’eût jamais vu d’arc, il en fit un.</l>
					<l n="295" num="1.295">Il cueillit une branche avec soin, lisse, droite,</l>
					<l n="296" num="1.296">Plus dure que l’airain, et de sa main adroite</l>
					<l n="297" num="1.297">La courba ; puis tressa des fibres, dont il fit</l>
					<l n="298" num="1.298">Une corde, et, mettant le désert à profit,</l>
					<l n="299" num="1.299">Sans souci de meurtrir la dépouille superbe</l>
					<l n="300" num="1.300">De ses compagnons morts, pour avoir une gerbe</l>
					<l n="301" num="1.301">De traits, il ajusta sur des bouts de roseau</l>
					<l n="302" num="1.302">Une griffe de tigre et des plumes d’oiseau.</l>
					<l n="303" num="1.303">Alors, sans un adieu jeté vers les clairières,</l>
					<l n="304" num="1.304">Fier d’avoir assorti ces flèches meurtrières,</l>
					<l n="305" num="1.305">Il prit sa course à l’heure où le ciel se dorait,</l>
					<l n="306" num="1.306">Et, le cœur tout joyeux, sortit de la forêt.</l>
					<l n="307" num="1.307"><space quantity="2" unit="char"></space>Il arriva d’abord près d’un lac dont l’eau pure</l>
					<l n="308" num="1.308">Réfléchissait le ciel dans la haute verdure,</l>
					<l n="309" num="1.309">Et dont le flot qu’un souffle émeut, rideau changeant,</l>
					<l n="310" num="1.310">S’effaçait à demi sous les lotus d’argent,</l>
					<l n="311" num="1.311">Ces lys chastes, ces lys faits en forme de rose !</l>
					<l n="312" num="1.312">Là, mêlant leurs beaux corps polis que l’onde arrose,</l>
					<l n="313" num="1.313">Des nymphes s’y baignaient, fuyant l’âpre chaleur,</l>
					<l n="314" num="1.314">Couronnant leurs cheveux de la divine fleur,</l>
					<l n="315" num="1.315">Rieuses, folâtrant, voguant sur les eaux calmes,</l>
					<l n="316" num="1.316">Et parfois sur leurs fronts cueillant de vertes palmes</l>
					<l n="317" num="1.317">Pour leurs jeux, ou tressant des colliers odorants,</l>
					<l n="318" num="1.318">Ou, parmi la fraîcheur des doux flots murmurants,</l>
					<l n="319" num="1.319">Sœurs dociles, fendant l’écume en longues lignes,</l>
					<l n="320" num="1.320">Si belles qu’on eût dit une troupe de cygnes</l>
					<l n="321" num="1.321">Dans l’azur ! Mais voici que le cruel amour,</l>
					<l n="322" num="1.322">Ayant tendu son arc les frappa tour à tour</l>
					<l n="323" num="1.323">De ses flèches de feu. Les nymphes éperdues,</l>
					<l n="324" num="1.324">Quittant le lac, au loin sur les roches ardues</l>
					<l n="325" num="1.325">Couraient, folles, sentant brûler leurs seins meurtris,</l>
					<l n="326" num="1.326">Arrachant leurs cheveux touffus, poussant des cris,</l>
					<l n="327" num="1.327">Ne sachant plus où fuir l’épouvantable outrage,</l>
					<l n="328" num="1.328">Et se roulaient dans l’herbe avec des pleurs de rage.</l>
					<l n="329" num="1.329">L’enfant Éros, content de ce premier exploit,</l>
					<l n="330" num="1.330">Regarda les grands cieux qu’il menaça du doigt,</l>
					<l n="331" num="1.331">Et, sans vouloir entendre une plainte importune,</l>
					<l n="332" num="1.332">Entra dans l’univers pour y chercher fortune.</l>
					<l n="333" num="1.333"><space quantity="2" unit="char"></space>Ô muse, c’est ainsi que le dessein prudent</l>
					<l n="334" num="1.334">Du roi Zeus fut trompé ; c’est ainsi que, pendant</l>
					<l n="335" num="1.335">Son enfance, l’amour apprit des tigres même</l>
					<l n="336" num="1.336">La cruauté, la ruse et la fureur suprême,</l>
					<l n="337" num="1.337">S’endormit près des grands lions dans les bois sourds,</l>
					<l n="338" num="1.338">Et fut le compagnon de guerre des vautours.</l>
					<l n="339" num="1.339">C’est ainsi que ce fils éclatant d’une mère</l>
					<l n="340" num="1.340">Adorable épuisa la jouissance amère</l>
					<l n="341" num="1.341">De voir pleurer, de voir souffrir, de voir mourir</l>
					<l n="342" num="1.342">Et de causer des maux que rien ne peut guérir.</l>
					<l n="343" num="1.343"><space quantity="2" unit="char"></space>Et c’est pourquoi tu fais notre dure misère,</l>
					<l n="344" num="1.344">C’est pourquoi tu meurtris nos âmes dans ta serre,</l>
					<l n="345" num="1.345">Amour des sens, ô jeune Éros, toi que le roi</l>
					<l n="346" num="1.346">Amour, le grand Titan, regarde avec effroi,</l>
					<l n="347" num="1.347">Et qui suças la haine impie et ses délices</l>
					<l n="348" num="1.348">Avec le lait cruel de tes noires nourrices !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1864">novembre 1864</date>.
					</dateline>
				</closer>
			</div></body></text></TEI>