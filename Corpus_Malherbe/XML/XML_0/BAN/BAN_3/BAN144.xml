<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Exilés</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4189 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">BAN_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>

			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Exilés</title>
						<author>Théodore de BANVILLE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L923</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
									<title>Les Exilés</title>
									<author>Théodore de BANVILLE</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Fasquelle</publisher>
									<date when="1899">1899</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>

		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1867">1867</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface n’a pas été encodée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN144">
				<head type="main">PENTHÉSILÉE</head>
				<lg n="1">
					<l n="1" num="1.1">Quand son âme se fut tristement exhalée</l>
					<l n="2" num="1.2">Par la blessure ouverte, et quand Penthésilée,</l>
					<l n="3" num="1.3">Une dernière fois se tournant vers les cieux,</l>
					<l n="4" num="1.4">Eut fermé pour jamais ses yeux audacieux,</l>
					<l n="5" num="1.5">Des guerriers, soutenant son front pâle et tranquille,</l>
					<l n="6" num="1.6">L’apportèrent alors sous les tentes d’Achille.</l>
					<l n="7" num="1.7">On détacha son casque au panache mouvant</l>
					<l n="8" num="1.8">Qui tout à l’heure encor frissonnait sous le vent,</l>
					<l n="9" num="1.9">Et puis on dénoua la cuirasse et l’armure ;</l>
					<l n="10" num="1.10">Et, comme on voit le cœur d’une grenade mûre,</l>
					<l n="11" num="1.11">La blessure apparut, dans la blanche pâleur</l>
					<l n="12" num="1.12">De son sein délicat et fier comme une fleur.</l>
					<l n="13" num="1.13">La haine et la fureur crispaient encor sa bouche,</l>
					<l n="14" num="1.14">Et sur ses bras hardis, comme un fleuve farouche</l>
					<l n="15" num="1.15">Se précipite avec d’indomptables élans,</l>
					<l n="16" num="1.16">Tombaient ses noirs cheveux, hérissés et sanglants.</l>
					<l n="17" num="1.17">Le divin meurtrier regarda sa victime.</l>
					<l n="18" num="1.18">Et, tout à coup sentant dans son cœur magnanime</l>
					<l n="19" num="1.19">Une douleur amère, il admira longtemps</l>
					<l n="20" num="1.20">Cette guerrière morte aux beaux cheveux flottants</l>
					<l n="21" num="1.21">Dont nul époux n’avait mérité les caresses,</l>
					<l n="22" num="1.22">Et sa beauté pareille à celle des déesses.</l>
					<l n="23" num="1.23">Puis il pleura. Longtemps, au bruit de ses sanglots,</l>
					<l n="24" num="1.24">Ses larmes de ses yeux brûlants en larges flots</l>
					<l n="25" num="1.25">Ruisselèrent, et, comme un lys pur qui frissonne,</l>
					<l n="26" num="1.26">Il baignait de ses pleurs le front de l’amazone.</l>
					<l n="27" num="1.27">Tous ceux qui sur leurs nefs, jeunes et pleins de jours,</l>
					<l n="28" num="1.28">Pour abattre Ilios environné de tours</l>
					<l n="29" num="1.29">L’avaient accompagné, fendant la mer stérile,</l>
					<l n="30" num="1.30">Frémissaient dans leurs cœurs, à voir pleurer Achille.</l>
					<l n="31" num="1.31">Mais seul Thersite, louche boiteux et tortu</l>
					<l n="32" num="1.32">Et chauve, et n’ayant plus sur son crâne pointu</l>
					<l n="33" num="1.33">Que des cheveux épars comme des herbes folles,</l>
					<l n="34" num="1.34">Outragea le héros par ces dures paroles :</l>
					<l n="35" num="1.35">« Cette femme a tué les meilleurs de nos chefs,</l>
					<l n="36" num="1.36">Dit-il, puis les ayant chassés jusqu’à leurs nefs,</l>
					<l n="37" num="1.37">Envoya chez Aidès, les perçant de ses flèches,</l>
					<l n="38" num="1.38">Des achéens nombreux comme des feuilles sèches</l>
					<l n="39" num="1.39">Que le vent enveloppe en son tourbillon fou ;</l>
					<l n="40" num="1.40">Toi cependant, chacun le voit, cœur lâche et mou,</l>
					<l n="41" num="1.41">Qui te plains et gémis comme le cerf qui brame,</l>
					<l n="42" num="1.42">Tu pleures cette femme avec des pleurs de femme ! »</l>
					<l n="43" num="1.43">À ces mots, regardant le railleur insensé,</l>
					<l n="44" num="1.44">Achille s’éveilla, comme un lion blessé</l>
					<l n="45" num="1.45">Sur le sable sanglant qu’un vent brûlant balaie,</l>
					<l n="46" num="1.46">Dont un insecte affreux vient tourmenter la plaie,</l>
					<l n="47" num="1.47">Et, voyant près de lui ce bouffon sans vertu,</l>
					<l n="48" num="1.48">Il le frappa du poing sur son crâne pointu.</l>
					<l n="49" num="1.49">Tersite expira. Car le poing fermé d’Achille</l>
					<l n="50" num="1.50">Avait fait cent morceaux de son crâne débile,</l>
					<l n="51" num="1.51">De même que l’argile informe cuite au four</l>
					<l n="52" num="1.52">Est fracassée avec un grand bruit à l’entour,</l>
					<l n="53" num="1.53">Alors que le potier, justement pris de rage</l>
					<l n="54" num="1.54">Et fâché d’avoir mal réussi son ouvrage,</l>
					<l n="55" num="1.55">En se ruant dessus brise un vase tout neuf.</l>
					<l n="56" num="1.56">Il tomba lourdement, assommé comme un bœuf,</l>
					<l n="57" num="1.57">Et, regardant encor la guerrière sans armes,</l>
					<l n="58" num="1.58">Achille aux pieds légers versait toujours des larmes.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1872">12 octobre 1872</date>.
					</dateline>
				</closer>
			</div></body></text></TEI>