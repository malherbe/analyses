<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Exilés</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4189 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">BAN_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>

			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Exilés</title>
						<author>Théodore de BANVILLE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L923</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
									<title>Les Exilés</title>
									<author>Théodore de BANVILLE</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Fasquelle</publisher>
									<date when="1899">1899</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>

		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1867">1867</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface n’a pas été encodée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN151">
				<head type="main">ÉRINNA</head>
				<head type="sub_1">À mon cher Philoxène Boyer</head>
				<head type="sub_2"><hi rend="ital">qui a ressuscité la grande figure <lb></lb>de Sappho dans un poème impérissable</hi></head>
				<lg n="1">
					<l n="1" num="1.1">Près du flot glorieux qui baise Mitylène,</l>
					<l n="2" num="1.2">Marchent, vierges en fleur, de jeunes poétesses</l>
					<l n="3" num="1.3">Qui du soir azuré boivent la fraîche haleine</l>
					<l n="4" num="1.4">Et passent dans la nuit comme un vol de déesses.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Elles vont, emportant la brise dans leurs voiles,</l>
					<l n="6" num="2.2">Vers le parfum sauvage et les profonds murmures.</l>
					<l n="7" num="2.3">Les lumières d’argent qui tombent des étoiles</l>
					<l n="8" num="2.4">Sur leurs dos gracieux mordent leurs chevelures.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Celle qui les conduit vers la plage marine,</l>
					<l n="10" num="3.2">C’est Érinna, l’orgueil des roses éphémères,</l>
					<l n="11" num="3.3">L’amante en qui revit dans sa blanche poitrine</l>
					<l n="12" num="3.4">Le grand cœur de Sappho, pâture des chimères.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">Elle leur parle ainsi, grave, tenant la lyre,</l>
					<l n="14" num="4.2">Le regard ébloui de clartés radieuses,</l>
					<l n="15" num="4.3">Et mêlant tendrement la voix de son délire</l>
					<l n="16" num="4.4">Aux plaintes sans repos des eaux mélodieuses :</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1">« Vierges, dit-elle, enfants baignés de tresses blondes,</l>
					<l n="18" num="5.2">Vous dont la lèvre encor n’est pas désaltérée,</l>
					<l n="19" num="5.3">Le rhythme est tout ; c’est lui qui soulève les mondes</l>
					<l n="20" num="5.4">Et les porte en chantant dans la plaine éthérée.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1">Poétesses, qu’il soit pour vous comme l’écorce</l>
					<l n="22" num="6.2">Étroitement unie au tronc même de l’arbre,</l>
					<l n="23" num="6.3">Ou comme la ceinture éprise de sa force</l>
					<l n="24" num="6.4">Qui dans son mince anneau tient notre flanc de marbre !</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1">Qu’il soit aussi pour vous la coupe souveraine</l>
					<l n="26" num="7.2">Où, pour garder l’esprit vivant de l’ancien rite,</l>
					<l n="27" num="7.3">Le vin, libre pourtant, prend la forme sereine</l>
					<l n="28" num="7.4">Moulée aux siècles d’or sur le sein d’Aphrodite !</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1">Le cercle où, par les lois saintes de la musique,</l>
					<l n="30" num="8.2">Les constellations demeurent suspendues,</l>
					<l n="31" num="8.3">N’affaiblit pas l’essor de leur vol magnifique</l>
					<l n="32" num="8.4">Et dans l’immensité les caresse éperdues.</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1">Tel est le rhythme. Enfants, suivez son culte aride.</l>
					<l n="34" num="9.2">Livrez-lui le génie en esclaves fidèles,</l>
					<l n="35" num="9.3">Car il n’offense pas l’auguste Piéride,</l>
					<l n="36" num="9.4">En entravant ses pieds il l’enveloppe d’ailes !</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1">Mais surtout, mais surtout que vos âmes soient blanches</l>
					<l n="38" num="10.2">Comme la neige où rien d’humain n’a mis sa trace !</l>
					<l n="39" num="10.3">Blanches comme l’horreur pâle des avalanches</l>
					<l n="40" num="10.4">Qui roule au flanc des monts irrités de la Thrace !</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1">Ah ! S’il est vrai qu’il faut à la fureur lyrique</l>
					<l n="42" num="11.2">Des victimes dont l’âpre amour ait fait sa proie</l>
					<l n="43" num="11.3">Et que l’ardente soif d’un bonheur tyrannique</l>
					<l n="44" num="11.4">Torture encor par la douleur et par la joie,</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1">Ah ! Du moins, jeunes sœurs, que la pensée altière</l>
					<l n="46" num="12.2">Affranchisse vos sens de toutes les souillures !</l>
					<l n="47" num="12.3">Ivres de volupté pourtant, que la matière</l>
					<l n="48" num="12.4">Ne vous offense pas de ses laideurs impures !</l>
				</lg>
				<lg n="13">
					<l n="49" num="13.1">Car celle qui, pour fuir le fardeau de la vie,</l>
					<l n="50" num="13.2">Impose à son extase une forme sensible,</l>
					<l n="51" num="13.3">Et veut boire, au festin où son dieu la convie,</l>
					<l n="52" num="13.4">Le vin matériel dans la coupe visible,</l>
				</lg>
				<lg n="14">
					<l n="53" num="14.1">Ne connaîtra jamais l’implacable démence</l>
					<l n="54" num="14.2">Qui met dans nos regards la clarté des aurores</l>
					<l n="55" num="14.3">Et qui fait résonner comme un sanglot immense</l>
					<l n="56" num="14.4">L’hymne de nos douleurs sur des cordes sonores !</l>
				</lg>
				<lg n="15">
					<l n="57" num="15.1">Celle qui n’ose pas mépriser la nature</l>
					<l n="58" num="15.2">Et qui, par les désirs terrestres endormie</l>
					<l n="59" num="15.3">Dans l’engourdissement où vit la créature,</l>
					<l n="60" num="15.4">Ne sait pas, en tenant la main de son amie,</l>
				</lg>
				<lg n="16">
					<l n="61" num="16.1">Chaste et vierge, oublier les liens qui l’étreignent,</l>
					<l n="62" num="16.2">Et sentir qu’à ses pieds se déchire un abîme</l>
					<l n="63" num="16.3">Et que son pouls s’arrête et que ses yeux s’éteignent</l>
					<l n="64" num="16.4">Et que la mort tressaille en son cœur magnanime ;</l>
				</lg>
				<lg n="17">
					<l n="65" num="17.1">Si, meurtrie et glacée, au monde évanouie,</l>
					<l n="66" num="17.2">Le sein brûlé des feux de ses pleurs solitaires,</l>
					<l n="67" num="17.3">Elle n’adore pas la douleur inouïe</l>
					<l n="68" num="17.4">Dont les ravissements courent dans ses artères,</l>
				</lg>
				<lg n="18">
					<l n="69" num="18.1">Eh bien, que celle-là, promise à l’hyménée,</l>
					<l n="70" num="18.2">Reste dans la maison où son devoir l’attache,</l>
					<l n="71" num="18.3">Et, souriante, près d’un jeune époux menée,</l>
					<l n="72" num="18.4">File pensivement une laine sans tache !</l>
				</lg>
				<lg n="19">
					<l n="73" num="19.1">Elle n’entendra pas les plaintes de la lyre,</l>
					<l n="74" num="19.2">Et son pied, plus vermeil que la rose naissante,</l>
					<l n="75" num="19.3">N’abordera jamais sur un léger navire</l>
					<l n="76" num="19.4">La Cythère adorable et toujours gémissante</l>
				</lg>
				<lg n="20">
					<l n="77" num="20.1">Mais vous, de vos grands cœurs, du vol de vos pensées,</l>
					<l n="78" num="20.2">Vous dont les doigts charmants ne filent pas de laine,</l>
					<l n="79" num="20.3">Suivez jusqu’à l’éther les ailes élancées,</l>
					<l n="80" num="20.4">Ô vierges sans souillure, orgueil de Mitylène !</l>
				</lg>
				<lg n="21">
					<l n="81" num="21.1">Et dites au ruisseau dont la voix se lamente</l>
					<l n="82" num="21.2">Que rien n’est plus martyre après la poésie,</l>
					<l n="83" num="21.3">Et qu’il n’est pas de flot pour rafraîchir l’amante</l>
					<l n="84" num="21.4">Dont la bouche brûlante a goûté l’ambroisie ! »</l>
				</lg>
				<lg n="22">
					<l n="85" num="22.1">Telle Érinna, livrée à ses mâles tristesses,</l>
					<l n="86" num="22.2">Sur le rivage ému que le laurier décore</l>
					<l n="87" num="22.3">Enseignait le troupeau rêveur des poétesses,</l>
					<l n="88" num="22.4">Et l’écho de son cri jaloux me trouble encore !</l>
				</lg>
				<lg n="23">
					<l n="89" num="23.1">Et j’ai rimé cette ode en rimes féminines</l>
					<l n="90" num="23.2">Pour que l’impression en restât plus poignante,</l>
					<l n="91" num="23.3">Et, par le souvenir des chastes héroïnes,</l>
					<l n="92" num="23.4">Laissât dans plus d’un cœur sa blessure saignante.</l>
				</lg>
				<lg n="24">
					<l n="93" num="24.1">Ô rhythme, tu sais tout ! Sur tes ailes de neige</l>
					<l n="94" num="24.2">Sans cesse nous allons vers des routes nouvelles,</l>
					<l n="95" num="24.3">Et, quel que soit le doute affreux qui nous assiège,</l>
					<l n="96" num="24.4">Il n’est pas de secret que tu ne nous révèles !</l>
				</lg>
				<lg n="25">
					<l n="97" num="25.1">Tu heurtes les soleils comme un oiseau farouche.</l>
					<l n="98" num="25.2">Ce n’est pour toi qu’un jeu d’escalader les cimes,</l>
					<l n="99" num="25.3">Et, lorsqu’un temps railleur n’a plus rien qui te touche,</l>
					<l n="100" num="25.4">Tu rêves dans la nuit, penché sur les abîmes !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1861">Septembre 1861</date>.
					</dateline>
				</closer>
			</div></body></text></TEI>