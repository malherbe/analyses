<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Exilés</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4189 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">BAN_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>

			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Exilés</title>
						<author>Théodore de BANVILLE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L923</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
									<title>Les Exilés</title>
									<author>Théodore de BANVILLE</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Fasquelle</publisher>
									<date when="1899">1899</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>

		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1867">1867</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface n’a pas été encodée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN147">
				<head type="main">DIONÉ</head>
				<lg n="1">
					<l n="1" num="1.1">Abattu par la roche énorme que sans aide,</l>
					<l n="2" num="1.2">Seul, avait soulevée en ses mains Diomède,</l>
					<l n="3" num="1.3">Énée était tombé sous le char de l’ardent</l>
					<l n="4" num="1.4">Fils de Tydée, ainsi qu’un chêne, et cependant</l>
					<l n="5" num="1.5">Que sa mère Aphrodite, au vent échevelée,</l>
					<l n="6" num="1.6">L’emportait mourant loin de la noire mêlée,</l>
					<l n="7" num="1.7">Diomède, sachant qu’elle est faible, et non pas</l>
					<l n="8" num="1.8">Intrépide à guider les hommes sur ses pas</l>
					<l n="9" num="1.9">Vers le carnage, comme Ényo destructrice</l>
					<l n="10" num="1.10">Des citadelles, dont la mort suit le caprice,</l>
					<l n="11" num="1.11">Poursuivit Aphrodite en son hardi chemin ;</l>
					<l n="12" num="1.12">Et de sa lance aiguë il lui perça la main,</l>
					<l n="13" num="1.13">D’où le sang précieux jaillit fluide et rose,</l>
					<l n="14" num="1.14">Délicieux à voir comme une fleur éclose,</l>
					<l n="15" num="1.15">Riant comme la pourpre en son éclat vermeil,</l>
					<l n="16" num="1.16">Et tout éblouissant des perles du soleil.</l>
					<l n="17" num="1.17">Car, pareils dans leur gloire à la blancheur du cygne,</l>
					<l n="18" num="1.18">Les dieux ne boivent pas le vin noir de la vigne.</l>
					<l n="19" num="1.19">Ces rois, pétris d’azur, ne mangent pas de blé,</l>
					<l n="20" num="1.20">Et c’est pourquoi leur sang, qui n’est jamais troublé,</l>
					<l n="21" num="1.21">Court dans leurs veines, beau de sa splendeur première,</l>
					<l n="22" num="1.22">Comme un flot ruisselant d’éther et de lumière.</l>
					<l n="23" num="1.23">Aphrodite poussait des cris, comme un aiglon</l>
					<l n="24" num="1.24">Furieux, cependant que Phœbos-Apollon</l>
					<l n="25" num="1.25">Cachait Énée au sein d’un nuage de flamme,</l>
					<l n="26" num="1.26">De peur qu’un Danaen ne lui vînt ravir l’âme</l>
					<l n="27" num="1.27">En frappant de l’airain ce faiseur de travaux.</l>
					<l n="28" num="1.28">Mais dans le char brillant d’Arès, dont les chevaux</l>
					<l n="29" num="1.29">S’envolèrent au gré de sa fureur amère,</l>
					<l n="30" num="1.30">Aphrodite s’enfuit vers Dioné, sa mère ;</l>
					<l n="31" num="1.31">Iris menait le char rapide, et secouait</l>
					<l n="32" num="1.32">Les rênes, et tantôt frappait à coups de fouet</l>
					<l n="33" num="1.33">Les deux chevaux, tantôt pour presser leur allure</l>
					<l n="34" num="1.34">Leur parlait, caressant leur douce chevelure,</l>
					<l n="35" num="1.35">Employant tour à tour la colère et les jeux.</l>
					<l n="36" num="1.36">Ils arrivent enfin à l’Olympe neigeux,</l>
					<l n="37" num="1.37">Et dans le palais d’ombre où sur son trône songe</l>
					<l n="38" num="1.38">Dioné, dans la nue où sa tête se plonge.</l>
					<l n="39" num="1.39">Or, lorsque sans pâlir de l’amère douleur,</l>
					<l n="40" num="1.40">Calme, et comme une rose ouvrant sa bouche en fleur,</l>
					<l n="41" num="1.41">Aphrodite eut montré sa blanche main d’ivoire</l>
					<l n="42" num="1.42">Déchirée et meurtrie et qui devenait noire,</l>
					<l n="43" num="1.43">La Titane au grand cœur si souvent ulcéré,</l>
					<l n="44" num="1.44">Planant sinistrement d’un front démesuré</l>
					<l n="45" num="1.45">Sur les cieux dont au loin la profondeur s’azure,</l>
					<l n="46" num="1.46">Tressaillit dans ses flancs et lava la blessure.</l>
					<l n="47" num="1.47">Et, rappelant ainsi des crimes odieux,</l>
					<l n="48" num="1.48">Elle nommait tout bas les meurtriers des dieux :</l>
					<l n="49" num="1.49">Hercule, nourrisson de la guerre et, comme elle,</l>
					<l n="50" num="1.50">Ivre d’horreur, blessant Hèra sous la mamelle ;</l>
					<l n="51" num="1.51">Éphialte, en dépit du destin souverain,</l>
					<l n="52" num="1.52">Mettant Arès lié dans un cachot d’airain,</l>
					<l n="53" num="1.53">Et l’emprisonnant, seul avec la nuit maudite.</l>
					<l n="54" num="1.54">Puis, prenant en ses bras la céleste Aphrodite,</l>
					<l n="55" num="1.55">Sans peine elle étendit ses membres assoupis</l>
					<l n="56" num="1.56">Sur des toisons sans tache et de moelleux tapis,</l>
					<l n="57" num="1.57">Car déjà le sommeil, né de l’ombre éternelle,</l>
					<l n="58" num="1.58">Roulait un sable fin dans sa noire prunelle ;</l>
					<l n="59" num="1.59">Et comme Dioné, redoutable aux méchants,</l>
					<l n="60" num="1.60">Se souvenait encor des invincibles chants</l>
					<l n="61" num="1.61">Avec lesquels, avant de subir leurs désastres,</l>
					<l n="62" num="1.62">Les titans conduisaient le blanc troupeau des astres</l>
					<l n="63" num="1.63">Soucieuse de voir la déesse frémir,</l>
					<l n="64" num="1.64">Elle disait ces chants sacrés pour l’endormir,</l>
					<l n="65" num="1.65">Douce et baissant la voix bien plus qu’à l’ordinaire,</l>
					<l n="66" num="1.66">Et les mortels croyaient que c’était le tonnerre.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1874">jeudi 20 août 1874</date>.
					</dateline>
				</closer>
			</div></body></text></TEI>