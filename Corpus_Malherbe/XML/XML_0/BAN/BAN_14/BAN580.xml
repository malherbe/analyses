<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">RIMES DORÉES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1492 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_14</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">RIMES DORÉES</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="1998">1998</date>
								</imprint>
								<biblScope unit="tome">V</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre, éditeur</publisher>
							<date when="1891">1889-1891</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1875">1875</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN580">
				<head type="main">Une Fête chez Gautier</head>
				<div type="section" n="1">
					<head type="number">I</head>
					<lg n="1">
						<l n="1" num="1.1">Hier, — doux remède à nos maux ! —</l>
						<l n="2" num="1.2">Thalie, ivre et fuyant la prose,</l>
						<l n="3" num="1.3">Chez le poëte des Émaux</l>
						<l n="4" num="1.4">Avait planté sa tente rose.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">Le Caprice, qu’il a chanté,</l>
						<l n="6" num="2.2">Riait, sylphe au léger costume,</l>
						<l n="7" num="2.3">Coiffé du tricorne enchanté,</l>
						<l n="8" num="2.4">Et caressait Pierrot posthume.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">Rayée en façon de satin,</l>
						<l n="10" num="3.2">Une salle en toile, folâtre</l>
						<l n="11" num="3.3">Comme un habit de Mezzetin,</l>
						<l n="12" num="3.4">Enfermait le petit théâtre.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">D’ailleurs, un luxe oriental,</l>
						<l n="14" num="4.2">Pour la Muse qu’on divinise,</l>
						<l n="15" num="4.3">Mirait un lustre de cristal</l>
						<l n="16" num="4.4">Dans un beau miroir de Venise.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">S’il faut vous dire quels témoins</l>
						<l n="18" num="5.2">Encombraient ce frêle édifice,</l>
						<l n="19" num="5.3">L’assemblée était certes moins</l>
						<l n="20" num="5.4">Nombreuse qu’au feu d’artifice.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1">Élégante comme il convient</l>
						<l n="22" num="6.2">Pour écouter la Poésie</l>
						<l n="23" num="6.3">Quand ce bel Ange nous revient,</l>
						<l n="24" num="6.4">Elle était illustre et choisie.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1">Tant de beaux yeux, couleur des soirs</l>
						<l n="26" num="7.2">Ou de l’or pur ou des pervenches,</l>
						<l n="27" num="7.3">Faisaient passer les habits noirs</l>
						<l n="28" num="7.4">Masqués par des épaules blanches.</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1">La littérature y comptait,</l>
						<l n="30" num="8.2">L’ancienne aussi bien que la neuve,</l>
						<l n="31" num="8.3">Si bien que Dumas fils était</l>
						<l n="32" num="8.4">Assis auprès de Sainte-Beuve.</l>
					</lg>
				</div>
				<div type="section" n="2">
					<head type="number">II</head>
					<lg n="1">
						<l n="33" num="1.1">En dépit d’un siècle traînard,</l>
						<l n="34" num="1.2">On avait omis la Musique,</l>
						<l n="35" num="1.3">Par la raison que c’est un art</l>
						<l n="36" num="1.4">Trop matériel et physique.</l>
					</lg>
					<lg n="2">
						<l n="37" num="2.1">Devant l’or sacré d’Apollon</l>
						<l n="38" num="2.2">Que devient cette pâle étoile ?</l>
						<l n="39" num="2.3">Donc ce fut sans nul violon</l>
						<l n="40" num="2.4">Que l’on vit se lever la toile.</l>
					</lg>
					<lg n="3">
						<l n="41" num="3.1">Les décors malins et vermeils</l>
						<l n="42" num="3.2">Étaient de Puvis de Chavannes :</l>
						<l n="43" num="3.3">Pour en rencontrer de pareils</l>
						<l n="44" num="3.4">On irait bien plus loin que Vannes !</l>
					</lg>
					<lg n="4">
						<l n="45" num="4.1">La Fantaisie et la Raison</l>
						<l n="46" num="4.2">S’y battaient de façon hautaine,</l>
						<l n="47" num="4.3">Et j’admirai que la maison</l>
						<l n="48" num="4.4">Fût moins grande que la fontaine.</l>
					</lg>
					<lg n="5">
						<l n="49" num="5.1">J’aime ce mur d’un si haut goût</l>
						<l n="50" num="5.2">Où ce grand pot de fleurs flamboie !</l>
						<l n="51" num="5.3">Mais ce que je préfère à tout</l>
						<l n="52" num="5.4">Et ce qui m’a comblé de joie,</l>
					</lg>
					<lg n="6">
						<l n="53" num="6.1">C’est l’enseigne du rôtisseur,</l>
						<l n="54" num="6.2">Qui ne mérite aucun reproche :</l>
						<l n="55" num="6.3">Un saint Laurent plein de douceur</l>
						<l n="56" num="6.4">Achevant de cuire à la broche.</l>
					</lg>
					<lg n="7">
						<l n="57" num="7.1">Pour les pièces, on les connaît :</l>
						<l n="58" num="7.2">C’est la Muse parant la Farce</l>
						<l n="59" num="7.3">De cent perles où le jour naît,</l>
						<l n="60" num="7.4">Couronne sur sa tête éparse ;</l>
					</lg>
					<lg n="8">
						<l n="61" num="8.1">C’est la débauche du Rimeur,</l>
						<l n="62" num="8.2">Qui, le front caressé d’un lierre,</l>
						<l n="63" num="8.3">Avec la Nymphe en belle humeur</l>
						<l n="64" num="8.4">S’enivre du vin de Molière.</l>
					</lg>
					<lg n="9">
						<l n="65" num="9.1">Jamais chasseur en ses liens</l>
						<l n="66" num="9.2">N’a mieux pris la rime galante !</l>
						<l n="67" num="9.3">Mais parlons des comédiens :</l>
						<l n="68" num="9.4">Ma foi la troupe est excellente.</l>
					</lg>
				</div>
				<div type="section" n="3">
					<head type="number">III</head>
					<lg n="1">
						<l n="69" num="1.1">Malgré le Chacun son métier,</l>
						<l n="70" num="1.2">La critique ici ne peut mordre,</l>
						<l n="71" num="1.3">Puisque Théophile Gautier</l>
						<l n="72" num="1.4">Est un acteur de premier ordre.</l>
					</lg>
					<lg n="2">
						<l n="73" num="2.1">Quoi ! direz-vous. — Oui, c’est ainsi.</l>
						<l n="74" num="2.2">On a beau porter une lyre,</l>
						<l n="75" num="2.3">Il paraît que l’on peut aussi,</l>
						<l n="76" num="2.4">Faisant des vers, savoir les dire.</l>
					</lg>
					<lg n="3">
						<l n="77" num="3.1">Comme il a bien peur des filous !</l>
						<l n="78" num="3.2">Oh ! la réplique alerte et vive !</l>
						<l n="79" num="3.3">Les bons airs de tuteur jaloux !</l>
						<l n="80" num="3.4">La bonne bêtise naïve !</l>
					</lg>
					<lg n="4">
						<l n="81" num="4.1">Les directeurs, — allez-y voir ! —</l>
						<l n="82" num="4.2">N’ont rien qui vaille, dans leurs bouges,</l>
						<l n="83" num="4.3">Ce fier Géronte en pourpoint noir,</l>
						<l n="84" num="4.4">En bonnet rouge, en manches rouges.</l>
					</lg>
					<lg n="5">
						<l n="85" num="5.1">Quand à Pierrot, blanc comme un lys</l>
						<l n="86" num="5.2">Et sérieux comme un augure,</l>
						<l n="87" num="5.3">Il empruntait de Gautier fils</l>
						<l n="88" num="5.4">Une très aimable figure.</l>
					</lg>
					<lg n="6">
						<l n="89" num="6.1">Mais vous, Colombine, Arlequin,</l>
						<l n="90" num="6.2">Inez, Marinette, Valère,</l>
						<l n="91" num="6.3">Taille fine, frais casaquin,</l>
						<l n="92" num="6.4">Amour, esprit, gaieté, colère,</l>
					</lg>
					<lg n="7">
						<l n="93" num="7.1">Que dire de vos yeux mutins,</l>
						<l n="94" num="7.2">De la fleur sur vos fronts éclose,</l>
						<l n="95" num="7.3">De vos petits pieds enfantins,</l>
						<l n="96" num="7.4">De vos chastes lèvres de rose ?</l>
					</lg>
					<lg n="8">
						<l n="97" num="8.1">O jeunesse ! ô pourpre de sang !</l>
						<l n="98" num="8.2">Jamais ni Béjart ni de Brie</l>
						<l n="99" num="8.3">Avec un front suave et blanc</l>
						<l n="100" num="8.4">N’eurent la bouche plus fleurie.</l>
					</lg>
					<lg n="9">
						<l n="101" num="9.1">Pour finir, louer Rodolfo</l>
						<l n="102" num="9.2">N’est pas une chose commode,</l>
						<l n="103" num="9.3">Et j’aurais besoin que Sappho</l>
						<l n="104" num="9.4">Me prêtât son grand rythme d’ode.</l>
					</lg>
					<lg n="10">
						<l n="105" num="10.1">Il est flûté comme un hautbois,</l>
						<l n="106" num="10.2">Brillant comme une faux dans l’herbe,</l>
						<l n="107" num="10.3">Et son geste a l’air d’être en bois :</l>
						<l n="108" num="10.4">Il est terrible, il est superbe.</l>
					</lg>
					<lg n="11">
						<l n="109" num="11.1">Je le vois, hélas ! j’aurais dû,</l>
						<l n="110" num="11.2">Moi qui veux la blancheur aux merles,</l>
						<l n="111" num="11.3">A travers ce compte rendu</l>
						<l n="112" num="11.4">Semer les rubis et les perles.</l>
					</lg>
					<lg n="12">
						<l n="113" num="12.1">Qu’il est pâle, mon feuilleton</l>
						<l n="114" num="12.2">Pour cette fête sans seconde ! —</l>
						<l n="115" num="12.3">Mais je suis comme fut, dit-on,</l>
						<l n="116" num="12.4">La plus belle fille du monde.</l>
					</lg>
				</div>
				<closer>
					<dateline>
						<date when="1863">1er septembre 1863.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>