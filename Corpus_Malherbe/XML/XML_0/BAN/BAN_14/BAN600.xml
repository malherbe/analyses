<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">RIMES DORÉES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1492 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_14</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">RIMES DORÉES</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="1998">1998</date>
								</imprint>
								<biblScope unit="tome">V</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre, éditeur</publisher>
							<date when="1891">1889-1891</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1875">1875</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN600">
				<head type="main">Le Théâtre</head>
				<opener>
					<salute>A Jules Bonnassies</salute>
				</opener>
				<lg n="1">
					<l n="1" num="1.1">Lorsque j’entends ces mots magiques : Le Théâtre,</l>
					<l n="2" num="1.2">Un univers diffus, charmant, plus varié</l>
					<l n="3" num="1.3">Que la vie, effrayant, gracieux et folâtre,</l>
					<l n="4" num="1.4">M’apparaît, aux splendeurs des rayons marié.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Ce sont les vendangeurs de la joyeuse Attique,</l>
					<l n="6" num="2.2">Couronnés de feuillage, ivres des plus doux vins,</l>
					<l n="7" num="2.3">Aux quatre vents du ciel jetant l’ode emphatique ;</l>
					<l n="8" num="2.4">C’est Eschyle au front nu, menant les chœurs divins ;</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Ce sont les demi-dieux, les chanteurs, les génies</l>
					<l n="10" num="3.2">Livrant au destin sombre, avec leur plaie au flanc,</l>
					<l n="11" num="3.3">Les Orestes plaintifs et les Iphigénies,</l>
					<l n="12" num="3.4">Et les Œdipes fous aveuglés par le sang ;</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">C’est cet archer vainqueur de la foule profane,</l>
					<l n="14" num="4.2">Sachant faire obéir la flûte de roseau</l>
					<l n="15" num="4.3">Et la lyre, les vers du sage Aristophane,</l>
					<l n="16" num="4.4">Célébrant la fierté superbe de l’Oiseau.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1">C’est le grand créateur mystérieux, Shakspere</l>
					<l n="18" num="5.2">S’élançant comme un Dieu par son hardi chemin,</l>
					<l n="19" num="5.3">Animant la forêt qui parle et qui respire,</l>
					<l n="20" num="5.4">Et de ses doigts rêveurs pétrissant l’être humain ;</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1">C’est le Crime, l’Erreur, la Fureur, la Folie ;</l>
					<l n="22" num="6.2">C’est Lear, dont l’ouragan fait voler le manteau,</l>
					<l n="23" num="6.3">C’est Hamlet se roulant sous les pieds d’Ophélie ;</l>
					<l n="24" num="6.4">Ce sont les Rois jaloux aiguisant leur couteau ;</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1">C’est, doux cygne éploré, la pâle Desdémone,</l>
					<l n="26" num="7.2">C’est Imogène errant sous les chênes profonds,</l>
					<l n="27" num="7.3">Et c’est Titania, pareille à l’anémone,</l>
					<l n="28" num="7.4">Baisant le front de l’âne avec des cris bouffons ;</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1">C’est Orlando semant les diamants de l’Inde</l>
					<l n="30" num="8.2">Et les perles d’Ophir en sa folle chanson,</l>
					<l n="31" num="8.3">Et tressant des sonnets fleuris pour Rosalinde,</l>
					<l n="32" num="8.4">Cette capricieuse, habillée en garçon.</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1">C’est tout le peuple étrange, à son rêve docile</l>
					<l n="34" num="9.2">Et brillant des rubis célestes du matin,</l>
					<l n="35" num="9.3">Que Molière amena de la verte Sicile,</l>
					<l n="36" num="9.4">Et que sa fantaisie a vêtu de satin !</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1">Étalant son manteau comme les paons leurs queues,</l>
					<l n="38" num="10.2">Et versant la folie en sa coupe où je bois,</l>
					<l n="39" num="10.3">C’est Scapin, blanc de neige, orné de quilles bleues,</l>
					<l n="40" num="10.4">Avec sa barbe folle et son poignard de bois ;</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1">Isabelles, Agnès, ce sont les jeunes filles</l>
					<l n="42" num="11.2">Dont Valère chérit les fronts délicieux ;</l>
					<l n="43" num="11.3">C’est Zerbinette ; c’est le roi des Mascarilles</l>
					<l n="44" num="11.4">Faisant tourbillonner sa pourpre vers les cieux ;</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1">Ce sont les Ægipans, les Nymphes, les Déesses,</l>
					<l n="46" num="12.2">Les Turcs, les Espagnols, les Poitevins dansants</l>
					<l n="47" num="12.3">Que le Songeur, suivi d’ombres enchanteresses,</l>
					<l n="48" num="12.4">Évoque aux pieds du roi Louis, ivre d’encens ;</l>
				</lg>
				<lg n="13">
					<l n="49" num="13.1">C’est Tartuffe, essayant les poisons qu’il mélange ;</l>
					<l n="50" num="13.2">C’est don Juan que meurtrit le Désir, ce vautour,</l>
					<l n="51" num="13.3">Et qui sur sa paupière et sur son front d’archange</l>
					<l n="52" num="13.4">Laisse voir la brûlure affreuse de l’amour.</l>
				</lg>
				<lg n="14">
					<l n="53" num="14.1">C’est Regnard, plein d’ivresse, avec son Légataire,</l>
					<l n="54" num="14.2">Et Lisette et Crispin, vêtu du noir manteau ;</l>
					<l n="55" num="14.3">C’est Marivaux pensif, embarquant pour Cythère</l>
					<l n="56" num="14.4">Dorante et Sylvia, costumés par Wateau ;</l>
				</lg>
				<lg n="15">
					<l n="57" num="15.1">C’est Talma, dans Néron, gardant sa noble pose,</l>
					<l n="58" num="15.2">Laissant rugir sa mère et, calme sous l’affront,</l>
					<l n="59" num="15.3">Jouant avec un bout de son écharpe rose ;</l>
					<l n="60" num="15.4">C’est Mars au beau sourire, avec sa rose au front ;</l>
				</lg>
				<lg n="16">
					<l n="61" num="16.1">Puis c’est le Drame, avec son extase féerique,</l>
					<l n="62" num="16.2">Ressuscité, rayant les cieux de son grand vol</l>
					<l n="63" num="16.3">Et planant à la voix du Poëte lyrique ;</l>
					<l n="64" num="16.4">C’est Marion de Lorme, et Blanche et doña Sol ;</l>
				</lg>
				<lg n="17">
					<l n="65" num="17.1">C’est le vieux Job chargé d’attentats et de gloire ;</l>
					<l n="66" num="17.2">C’est Tisbe menaçant par la voix de Dorval ;</l>
					<l n="67" num="17.3">C’est Ruy Blas déchirant sa pourpre dérisoire,</l>
					<l n="68" num="17.4">Et le vieux Frédérick, demeuré sans rival.</l>
				</lg>
				<lg n="18">
					<l n="69" num="18.1">Puis Esther murmurant ses plaintes sous le cèdre,</l>
					<l n="70" num="18.2">Jeanne d’Arc inspirée invoquant saint Michel,</l>
					<l n="71" num="18.3">Pauline s’élançant vers Dieu, Camille, Phèdre,</l>
					<l n="72" num="18.4">C’est l’éblouissement tragique, c’est Rachel !</l>
				</lg>
				<lg n="19">
					<l n="73" num="19.1">Elle est, courant, la haine au front, sur le rivage,</l>
					<l n="74" num="19.2">Hermione, mêlant sa plainte au flot moqueur ;</l>
					<l n="75" num="19.3">Elle est Chimène, ayant en sa fierté sauvage</l>
					<l n="76" num="19.4">Une goutte de sang de taureau dans le cœur.</l>
				</lg>
				<lg n="20">
					<l n="77" num="20.1">C’est Musset, toujours beau de sa douleur insigne,</l>
					<l n="78" num="20.2">Brodant de perles d’or quelque vieux fabliau,</l>
					<l n="79" num="20.3">Par la voix des acteurs disant un chant de cygne,</l>
					<l n="80" num="20.4">Et versant sur nos mains les pleurs de Célio ; —</l>
				</lg>
				<lg n="21">
					<l n="81" num="21.1">C’est le sombre Antony poignardant son Adèle ;</l>
					<l n="82" num="21.2">C’est toi qui meurs si jeune et qui t’humilias,</l>
					<l n="83" num="21.3">Amante, courtisane au front chaste et fidèle,</l>
					<l n="84" num="21.4">Marguerite, portant les blancs camellias !</l>
				</lg>
				<lg n="22">
					<l n="85" num="22.1">C’est Jocrisse, ingénu comme une fille, et rouge</l>
					<l n="86" num="22.2">Comme un coquelicot dans les blés de Cérès,</l>
					<l n="87" num="22.3">Et que, pour nous ravir, tant notre horizon bouge,</l>
					<l n="88" num="22.4">Font si spirituel Arnal et Gil Pérès ;</l>
				</lg>
				<lg n="23">
					<l n="89" num="23.1">C’est le grand Bilboquet dans son carrick noisette,</l>
					<l n="90" num="23.2">Ou montrant le pourpoint du farouche Espagnol,</l>
					<l n="91" num="23.3">Et jouant de son nez comme d’une musette ;</l>
					<l n="92" num="23.4">C’est Prudhomme, rayant l’azur avec son col ;</l>
				</lg>
				<lg n="24">
					<l n="93" num="24.1">Enfin c’est, tout souillé par les fanges nocturnes</l>
					<l n="94" num="24.2">Et tournant dans ses doigts son lorgnon radieux,</l>
					<l n="95" num="24.3">Robert Macaire avec ses souliers à cothurnes</l>
					<l n="96" num="24.4">Et son pantalon fait de la pourpre des Dieux !</l>
				</lg>
				<lg n="25">
					<l n="97" num="25.1">Et sur cette mêlée étrange et surhumaine,</l>
					<l n="98" num="25.2">Près des astres d’argent montrant leurs pieds nacrés,</l>
					<l n="99" num="25.3">Les sœurs aux belles voix, Thalie et Melpomène,</l>
					<l n="100" num="25.4">Planent dans la splendeur des vastes cieux sacrés,</l>
				</lg>
				<lg n="26">
					<l n="101" num="26.1">Celle-ci, furieuse et montant la Chimère,</l>
					<l n="102" num="26.2">Et celle-là, Pégase au regard meurtrier ;</l>
					<l n="103" num="26.3">L’une jetant des fleurs sur les pieds nus d’Homère</l>
					<l n="104" num="26.4">Et l’autre couronnant Rabelais du laurier !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1874">Décembre 1874.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>