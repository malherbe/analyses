<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Cariatides</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>5667 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">BAN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>

			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Cariatides</title>
						<author>Théodore de BANVILLE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L899</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Cariatides</title>
								<author>Théodore de BANVILLE</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Charpentier</publisher>
									<date when="1891">1891</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>

		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1842">1842</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>L’avant-propos n’est pas repris.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-23" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LIVRE DEUXIÈME</head><div type="poem" key="BAN41">
					<head type="main">LOYS</head>
					<opener>
						<epigraph>
							<cit>
								<quote>Elle cueille des marguerites <lb></lb>et les effeuille pour s’assurer de <lb></lb>l’amour de Loys</quote>
								<bibl>
									<name>THÉOPHILE GAUTIER</name>, <lb></lb><hi rend="ital">Giselle</hi>, acte I, scène IV.
								</bibl>
							</cit>
						</epigraph>
					</opener>
					<lg n="1">
						<l n="1" num="1.1">Mon Loys, j’ai sous vos prunelles,</l>
						<l n="2" num="1.2">Oublié, dans mon cœur troublé,</l>
						<l n="3" num="1.3">Mon époux qui s’en est allé</l>
						<l n="4" num="1.4">Pour combattre les infidèles.</l>
						<l n="5" num="1.5">Quand nous le croirons loin encor,</l>
						<l n="6" num="1.6">Il sera là, Dieu nous pardonne !</l>
						<l n="7" num="1.7">Mon beau page, quel bruit résonne ?</l>
						<l n="8" num="1.8">Est-ce lui qui sonne du cor ?</l>
					</lg>
					<lg n="2">
						<l n="9" num="2.1">J’ai lu dans un ancien poëme</l>
						<l n="10" num="2.2">Qu’une autre Yolande autrefois</l>
						<l n="11" num="2.3">Près de son page Hector De Foix</l>
						<l n="12" num="2.4">Oublia son époux de même.</l>
						<l n="13" num="2.5">Elle gardait comme un trésor</l>
						<l n="14" num="2.6">Ces extases que l’amour donne. —</l>
						<l n="15" num="2.7">Mon beau page, quel bruit résonne ?</l>
						<l n="16" num="2.8">Est-ce lui qui sonne du cor ?</l>
					</lg>
					<lg n="3">
						<l n="17" num="3.1">Cette Yolande était duchesse,</l>
						<l n="18" num="3.2">Mille vassaux étaient son bien,</l>
						<l n="19" num="3.3">Et son bel ami n’avait rien</l>
						<l n="20" num="3.4">Que ses cheveux blonds pour richesse.</l>
						<l n="21" num="3.5">Pour cet enfant aux cheveux d’or</l>
						<l n="22" num="3.6">La dame eût vendu sa couronne. —</l>
						<l n="23" num="3.7">Mon beau page, quel bruit résonne ?</l>
						<l n="24" num="3.8">Est-ce lui qui sonne du cor ?</l>
					</lg>
					<lg n="4">
						<l n="25" num="4.1">Ces amants qu’un doux rêve assemble,</l>
						<l n="26" num="4.2">Ont souvent passé plus d’un jour</l>
						<l n="27" num="4.3">À se dire des chants d’amour,</l>
						<l n="28" num="4.4">Ou bien à regarder ensemble</l>
						<l n="29" num="4.5">Les oiseaux prendre leur essor</l>
						<l n="30" num="4.6">Vers l’azur qui tremble et frissonne. —</l>
						<l n="31" num="4.7">Mon beau page, quel bruit résonne ?</l>
						<l n="32" num="4.8">Est-ce lui qui sonne du cor ?</l>
					</lg>
					<lg n="5">
						<l n="33" num="5.1">Ou bien ils passaient leurs journées</l>
						<l n="34" num="5.2">À revoir d’auréoles ceints</l>
						<l n="35" num="5.3">Les bonnes vierges et les saints</l>
						<l n="36" num="5.4">Dans les bibles enluminées.</l>
						<l n="37" num="5.5">L’amour dit son confiteor</l>
						<l n="38" num="5.6">Sans écouter l’heure qui sonne. —</l>
						<l n="39" num="5.7">Mon beau page, quel bruit résonne ?</l>
						<l n="40" num="5.8">Est-ce lui qui sonne du cor ?</l>
					</lg>
					<lg n="6">
						<l n="41" num="6.1">Comme leurs lèvres en délire</l>
						<l n="42" num="6.2">Un soir longuement s’assemblaient,</l>
						<l n="43" num="6.3">En des baisers qui ressemblaient</l>
						<l n="44" num="6.4">Aux frémissements d’une lyre,</l>
						<l n="45" num="6.5">On entendit au corridor</l>
						<l n="46" num="6.6">Les pas de l’époux en personne. —</l>
						<l n="47" num="6.7">Mon beau page, quel bruit résonne ?</l>
						<l n="48" num="6.8">Est-ce lui qui sonne du cor ?</l>
					</lg>
					<lg n="7">
						<l n="49" num="7.1">Sais-tu quel sort on nous destine ?</l>
						<l n="50" num="7.2">Le malheureux page exilé,</l>
						<l n="51" num="7.3">Plein d’un regret inconsolé,</l>
						<l n="52" num="7.4">Alla mourir en Palestine.</l>
						<l n="53" num="7.5">Toujours pleurant son cher Hector,</l>
						<l n="54" num="7.6">La dame au couvent mourut nonne. —</l>
						<l n="55" num="7.7">Mon beau page, quel bruit résonne ?</l>
						<l n="56" num="7.8">Est-ce lui qui sonne du cor ?</l>
					</lg>
					<closer>
						<dateline>
							<date when="1841">février 1841</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>