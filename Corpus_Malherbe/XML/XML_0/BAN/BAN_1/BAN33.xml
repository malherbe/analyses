<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Cariatides</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>5667 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">BAN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>

			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Cariatides</title>
						<author>Théodore de BANVILLE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L899</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Cariatides</title>
								<author>Théodore de BANVILLE</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Charpentier</publisher>
									<date when="1891">1891</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>

		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1842">1842</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>L’avant-propos n’est pas repris.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-23" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LIVRE DEUXIÈME</head><head type="main_subpart">CEUX QUI MEURENT ET CEUX QUI COMBATTENT</head><div type="poem" key="BAN33">
						<head type="number">VI</head>
						<head type="main">NOSTALGIE</head>
						<lg n="1">
							<l n="1" num="1.1">Oh ! Lorsque incessamment tant de caprices noirs</l>
							<l n="2" num="1.2"><space quantity="12" unit="char"></space>S’impriment à la rame,</l>
							<l n="3" num="1.3">Et que notre Thalie accouche tous les soirs</l>
							<l n="4" num="1.4"><space quantity="12" unit="char"></space>D’un nouveau mélodrame ;</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1">Que les analyseurs sur leurs gros feuilletons</l>
							<l n="6" num="2.2"><space quantity="12" unit="char"></space>Jettent leur sel attique,</l>
							<l n="7" num="2.3">Et, tout en disséquant, chantent sur tous les tons</l>
							<l n="8" num="2.4"><space quantity="12" unit="char"></space>Les devoirs du critique ;</l>
						</lg>
						<lg n="3">
							<l n="9" num="3.1">Que dans un bouge affreux des orateurs blafards</l>
							<l n="10" num="3.2"><space quantity="12" unit="char"></space>Dissertent sur les nègres,</l>
							<l n="11" num="3.3">Que l’actrice en haillons étale tous ses fards</l>
							<l n="12" num="3.4"><space quantity="12" unit="char"></space>Sur ses ossements maigres ;</l>
						</lg>
						<lg n="4">
							<l n="13" num="4.1">Qu’au bout d’un pont très lourd trois cents provinciaux</l>
							<l n="14" num="4.2"><space quantity="12" unit="char"></space>Tout altérés de lucre,</l>
							<l n="15" num="4.3">Discutent gravement en des termes si hauts</l>
							<l n="16" num="4.4"><space quantity="12" unit="char"></space>Sur l’avenir du sucre ;</l>
						</lg>
						<lg n="5">
							<l n="17" num="5.1">Que de piètres Phœbus au regard indigo</l>
							<l n="18" num="5.2"><space quantity="12" unit="char"></space>Flattent leur muse vile,</l>
							<l n="19" num="5.3">Encensent D’Ennery, jugent Victor Hugo,</l>
							<l n="20" num="5.4"><space quantity="12" unit="char"></space>Et font du vaudeville ;</l>
						</lg>
						<lg n="6">
							<l n="21" num="6.1">Lorsque de vieux rimeurs fatiguent l’aquilon</l>
							<l n="22" num="6.2"><space quantity="12" unit="char"></space>De strophes chevillées,</l>
							<l n="23" num="6.3">Que sans nulle vergogne on expose au salon</l>
							<l n="24" num="6.4"><space quantity="12" unit="char"></space>Des femmes habillées ;</l>
						</lg>
						<lg n="7">
							<l n="25" num="7.1">Que chez nos miss Lilas, entre deux verres d’eau,</l>
							<l n="26" num="7.2"><space quantity="12" unit="char"></space>Un grand renom se forge,</l>
							<l n="27" num="7.3">Que nos beautés du jour, reines par Cupido,</l>
							<l n="28" num="7.4"><space quantity="12" unit="char"></space>N’ont pas même de gorge ;</l>
						</lg>
						<lg n="8">
							<l n="29" num="8.1">Qu’entre des arbres peints, à ce vieil opéra</l>
							<l n="30" num="8.2"><space quantity="12" unit="char"></space>Dont on dit tant de choses,</l>
							<l n="31" num="8.3">Les fruits du cotonnier qu’un lord anglais paiera</l>
							<l n="32" num="8.4"><space quantity="12" unit="char"></space>Dansent en maillots roses ;</l>
						</lg>
						<lg n="9">
							<l n="33" num="9.1">Que ne puis-je, ô Paris, vieille ville aux abois,</l>
							<l n="34" num="9.2"><space quantity="12" unit="char"></space>Te fuir d’un pas agile,</l>
							<l n="35" num="9.3">Et me mêler là-bas, sous l’ombrage des bois,</l>
							<l n="36" num="9.4"><space quantity="12" unit="char"></space>Aux bergers de Virgile !</l>
						</lg>
						<lg n="10">
							<l n="37" num="10.1">Voir les chevreaux lascifs errer près d’un ravin</l>
							<l n="38" num="10.2"><space quantity="12" unit="char"></space>Ou parcourir la plaine,</l>
							<l n="39" num="10.3">Et, comme Mnasylus, rencontrer, pris de vin,</l>
							<l n="40" num="10.4"><space quantity="12" unit="char"></space>Le bon homme Silène ;</l>
						</lg>
						<lg n="11">
							<l n="41" num="11.1">Près des saules courbés poursuivre Amaryllis</l>
							<l n="42" num="11.2"><space quantity="12" unit="char"></space>Au jeune sein d’albâtre,</l>
							<l n="43" num="11.3">Voir les nymphes emplir leurs corbeilles de lys</l>
							<l n="44" num="11.4"><space quantity="12" unit="char"></space>Pour Alexis le pâtre ;</l>
						</lg>
						<lg n="12">
							<l n="45" num="12.1">Dans les gazons fleuris, au murmure de l’eau,</l>
							<l n="46" num="12.2"><space quantity="12" unit="char"></space>Dépenser mes journées</l>
							<l n="47" num="12.3">À dire quelques chants aux filles d’Apollo</l>
							<l n="48" num="12.4"><space quantity="12" unit="char"></space>En strophes alternées ;</l>
						</lg>
						<lg n="13">
							<l n="49" num="13.1">Pleurer Daphnis ravi par un cruel destin,</l>
							<l n="50" num="13.2"><space quantity="12" unit="char"></space>Et, fuyant nos martyres,</l>
							<l n="51" num="13.3">Mieux qu’Alphesibœus en dansant au festin</l>
							<l n="52" num="13.4"><space quantity="12" unit="char"></space>Imiter les satyres !</l>
						</lg>
					</div></body></text></TEI>