<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Cariatides</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>5667 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">BAN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>

			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Cariatides</title>
						<author>Théodore de BANVILLE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L899</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Cariatides</title>
								<author>Théodore de BANVILLE</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Charpentier</publisher>
									<date when="1891">1891</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>

		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1842">1842</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>L’avant-propos n’est pas repris.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-23" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LIVRE TROISIÈME</head><div type="poem" key="BAN58">
					<head type="main">À MADAME CAROLINE ANGEBERT</head>
					<lg n="1">
						<l n="1" num="1.1">Chanter, mais dans le soir sonore</l>
						<l n="2" num="1.2">Et pour ses amis seulement,</l>
						<l n="3" num="1.3">Fuir le bruit qui nous déshonore</l>
						<l n="4" num="1.4">Et le vil applaudissement ;</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">Brûler, mais conserver sa flamme</l>
						<l n="6" num="2.2">Pour le seul but essentiel,</l>
						<l n="7" num="2.3">Être cette espérance, une âme</l>
						<l n="8" num="2.4">Qui chaque jour s’emplit de ciel ;</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">Avec une pensée insigne</l>
						<l n="10" num="3.2">Qui vous berce dans ses éclairs,</l>
						<l n="11" num="3.3">Vivre, blanche comme le cygne</l>
						<l n="12" num="3.4">Parmi les flots dorés et clairs ;</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">Ne rien chercher que la lumière,</l>
						<l n="14" num="4.2">S’envoler toujours loin du mal</l>
						<l n="15" num="4.3">Sur les ailes de la prière,</l>
						<l n="16" num="4.4">Jusqu’au glorieux idéal ;</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">Sentir l’ode au grand vol qui passe</l>
						<l n="18" num="5.2">En ouvrant ses ailes sans bruit,</l>
						<l n="19" num="5.3">Mais ne lui parler qu’à voix basse</l>
						<l n="20" num="5.4">Dans le silence et dans la nuit ;</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1">Rappeler sa pensée errante</l>
						<l n="22" num="6.2">Dans les pourpres de l’horizon ;</l>
						<l n="23" num="6.3">Être cette fleur odorante</l>
						<l n="24" num="6.4">Qui se cache dans le gazon ;</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1">Telle est votre gloire secrète,</l>
						<l n="26" num="7.2">Esprit de flammes étoilé,</l>
						<l n="27" num="7.3">Dont l’inspiration discrète</l>
						<l n="28" num="7.4">Fait tressaillir un luth voilé !</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1">Ah ! Que la grande poëtesse,</l>
						<l n="30" num="8.2">Devant les vastes flots déserts</l>
						<l n="31" num="8.3">Maudissant la bonne déesse,</l>
						<l n="32" num="8.4">Jette sa plainte dans les airs !</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1">Que la douloureuse Valmore,</l>
						<l n="34" num="9.2">En arrachant l’herbe et les fleurs,</l>
						<l n="35" num="9.3">Montre à l’insoucieuse aurore</l>
						<l n="36" num="9.4">Ses beaux yeux brûlés par les pleurs !</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1">Mais celle qui pourrait comme elles</l>
						<l n="38" num="10.2">Suivre le grand aigle irrité,</l>
						<l n="39" num="10.3">Et qui domptant ses maux rebelles</l>
						<l n="40" num="10.4">Se résigne à l’obscurité,</l>
					</lg>
					<lg n="11">
						<l n="41" num="11.1">Celle-là, guérie en ses veines,</l>
						<l n="42" num="11.2">Sent le calme victorieux</l>
						<l n="43" num="11.3">Triompher des angoisses vaines ;</l>
						<l n="44" num="11.4">Et ces êtres mystérieux</l>
					</lg>
					<lg n="12">
						<l n="45" num="12.1">Dont l’invincible souffle enchante</l>
						<l n="46" num="12.2">Ce qui vit et ce qui fleurit,</l>
						<l n="47" num="12.3">Disent entre eux lorsqu’elle chante :</l>
						<l n="48" num="12.4">Écoutons-la, c’est un esprit.</l>
					</lg>
					<closer>
						<dateline>
							<date when="1842">avril 1842</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>