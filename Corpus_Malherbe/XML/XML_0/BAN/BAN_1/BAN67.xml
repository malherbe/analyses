<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Cariatides</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>5667 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">BAN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>

			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Cariatides</title>
						<author>Théodore de BANVILLE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L899</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Cariatides</title>
								<author>Théodore de BANVILLE</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Charpentier</publisher>
									<date when="1891">1891</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>

		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1842">1842</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>L’avant-propos n’est pas repris.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-23" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LIVRE TROISIÈME</head><head type="main_subpart">EN HABIT ZINZOLIN</head><div type="poem" key="BAN67">
						<head type="number">V</head>
						<head type="main">RONDEAU REDOUBLÉ, À SYLVIE</head>
						<lg n="1">
							<l n="1" num="1.1">Je veux vous peindre, ô belle enchanteresse,</l>
							<l n="2" num="1.2">Dans un fauteuil ouvrant ses bras dorés,</l>
							<l n="3" num="1.3">Comme Diane, en jeune chasseresse,</l>
							<l n="4" num="1.4">L’arc à la main et les cheveux poudrés.</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1">Sur les rougeurs d’un ciel aux feux pourprés</l>
							<l n="6" num="2.2">Quelquefois passe un voile de tristesse,</l>
							<l n="7" num="2.3">Voilà pourquoi, lorsque vous sourirez,</l>
							<l n="8" num="2.4">Je veux vous peindre, ô belle enchanteresse !</l>
						</lg>
						<lg n="3">
							<l n="9" num="3.1">Vous serez là, frivole et charmeresse,</l>
							<l n="10" num="3.2">Parmi les fleurs des jardins adorés</l>
							<l n="11" num="3.3">Où doucement le zéphyr vous caresse</l>
							<l n="12" num="3.4">Dans un fauteuil ouvrant ses bras dorés.</l>
						</lg>
						<lg n="4">
							<l n="13" num="4.1">Auprès de vous, madame, vous aurez</l>
							<l n="14" num="4.2">Le lévrier qui folâtre et se dresse,</l>
							<l n="15" num="4.3">Et le carquois plein de traits désœuvrés,</l>
							<l n="16" num="4.4">Comme Diane en jeune chasseresse.</l>
						</lg>
						<lg n="5">
							<l n="17" num="5.1">Mais n’allez pas, fugitive déesse,</l>
							<l n="18" num="5.2">Chercher, pieds nus, par les bois et les prés</l>
							<l n="19" num="5.3">Un berger grec, et pâlir de tendresse,</l>
							<l n="20" num="5.4">L’arc à la main et les cheveux poudrés.</l>
						</lg>
						<lg n="6">
							<l n="21" num="6.1">Heureusement le cadre d’or qui blesse</l>
							<l n="22" num="6.2">Vous retiendra dans ses bâtons carrés,</l>
							<l n="23" num="6.3">Et sauvera votre antique noblesse</l>
							<l n="24" num="6.4">D’enlèvements trop inconsidérés.</l>
							<l n="25" num="6.5"><space quantity="12" unit="char"></space>Je veux vous peindre.</l>
						</lg>
					</div></body></text></TEI>