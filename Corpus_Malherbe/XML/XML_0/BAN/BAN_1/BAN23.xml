<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Cariatides</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>5667 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">BAN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>

			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Cariatides</title>
						<author>Théodore de BANVILLE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L899</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Cariatides</title>
								<author>Théodore de BANVILLE</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Charpentier</publisher>
									<date when="1891">1891</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>

		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1842">1842</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>L’avant-propos n’est pas repris.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-23" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LIVRE DEUXIÈME</head><head type="main_subpart">SONGE D’HIVER</head><div type="poem" key="BAN23">
						<head type="number">X</head>
						<lg n="1">
							<l n="1" num="1.1"><space quantity="10" unit="char"></space>Je bois à toi, jeune reine !</l>
							<l n="2" num="1.2"><space quantity="10" unit="char"></space>Endormeuse souveraine,</l>
							<l n="3" num="1.3"><space quantity="10" unit="char"></space>Oublieuse des soucis !</l>
							<l n="4" num="1.4"><space quantity="10" unit="char"></space>Car c’est pour bercer ma joie</l>
							<l n="5" num="1.5"><space quantity="10" unit="char"></space>Que ton caprice déploie</l>
							<l n="6" num="1.6"><space quantity="10" unit="char"></space>Les lits de pourpre et de soie,</l>
							<l n="7" num="1.7"><space quantity="10" unit="char"></space>Charmeresse aux noirs sourcils !</l>
						</lg>
						<lg n="2">
							<l n="8" num="2.1"><space quantity="10" unit="char"></space>Ta folle toison hardie</l>
							<l n="9" num="2.2"><space quantity="10" unit="char"></space>Brille comme l’incendie</l>
							<l n="10" num="2.3"><space quantity="10" unit="char"></space>Hôtesse du flot amer,</l>
							<l n="11" num="2.4"><space quantity="10" unit="char"></space>Ta gorge aiguë étincelle</l>
							<l n="12" num="2.5"><space quantity="10" unit="char"></space>Dans un rayon qui ruisselle ;</l>
							<l n="13" num="2.6"><space quantity="10" unit="char"></space>Tu gardes sous ton aisselle</l>
							<l n="14" num="2.7"><space quantity="10" unit="char"></space>Tous les parfums de la mer.</l>
						</lg>
						<lg n="3">
							<l n="15" num="3.1"><space quantity="10" unit="char"></space>Ta chevelure est vivante.</l>
							<l n="16" num="3.2"><space quantity="10" unit="char"></space>Elle frappe d’épouvante</l>
							<l n="17" num="3.3"><space quantity="10" unit="char"></space>Le lion et le vautour :</l>
							<l n="18" num="3.4"><space quantity="10" unit="char"></space>Sur ton beau ventre d’ivoire</l>
							<l n="19" num="3.5"><space quantity="10" unit="char"></space>S’éparpille une ombre noire,</l>
							<l n="20" num="3.6"><space quantity="10" unit="char"></space>Et tu marches dans ta gloire,</l>
							<l n="21" num="3.7"><space quantity="10" unit="char"></space>Superbe comme une tour.</l>
						</lg>
						<lg n="4">
							<l n="22" num="4.1"><space quantity="10" unit="char"></space>Ô déesse protectrice !</l>
							<l n="23" num="4.2"><space quantity="10" unit="char"></space>Heureux, ô sage nourrice,</l>
							<l n="24" num="4.3"><space quantity="10" unit="char"></space>L’athlète aux muscles ardents</l>
							<l n="25" num="4.4"><space quantity="10" unit="char"></space>Qui tout couvert de blessures,</l>
							<l n="26" num="4.5"><space quantity="10" unit="char"></space>D’écume et de meurtrissures,</l>
							<l n="27" num="4.6"><space quantity="10" unit="char"></space>Appelle encor les morsures</l>
							<l n="28" num="4.7"><space quantity="10" unit="char"></space>De ta lèvre et de tes dents !</l>
						</lg>
						<lg n="5">
							<l n="29" num="5.1"><space quantity="10" unit="char"></space>Toi seule, ô bonne déesse,</l>
							<l n="30" num="5.2"><space quantity="10" unit="char"></space>As l’incurable tristesse</l>
							<l n="31" num="5.3"><space quantity="10" unit="char"></space>De l’étoile et de la fleur</l>
							<l n="32" num="5.4"><space quantity="10" unit="char"></space>Sous l’or touffu qui te baigne ;</l>
							<l n="33" num="5.5"><space quantity="10" unit="char"></space>Et ton désespoir m’enseigne</l>
							<l n="34" num="5.6"><space quantity="10" unit="char"></space>Sur ton flanc glacé qui saigne</l>
							<l n="35" num="5.7"><space quantity="10" unit="char"></space>L’extase de la douleur.</l>
						</lg>
						<lg n="6">
							<l n="36" num="6.1"><space quantity="10" unit="char"></space>Honte au cœur timide ! Il trouve</l>
							<l n="37" num="6.2"><space quantity="10" unit="char"></space>Sous ta figure, la louve</l>
							<l n="38" num="6.3"><space quantity="10" unit="char"></space>Qu’il nomme réalité.</l>
							<l n="39" num="6.4"><space quantity="10" unit="char"></space>Mais à celui qui t’adore</l>
							<l n="40" num="6.5"><space quantity="10" unit="char"></space>Ta main, où tout flot se dore,</l>
							<l n="41" num="6.6"><space quantity="10" unit="char"></space>Verse, ô fille de Pandore,</l>
							<l n="42" num="6.7"><space quantity="10" unit="char"></space>Un vin d’immortalité !</l>
						</lg>
					</div></body></text></TEI>