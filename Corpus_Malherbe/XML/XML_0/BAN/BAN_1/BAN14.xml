<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Cariatides</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>5667 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">BAN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>

			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Cariatides</title>
						<author>Théodore de BANVILLE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L899</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Cariatides</title>
								<author>Théodore de BANVILLE</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Charpentier</publisher>
									<date when="1891">1891</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>

		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1842">1842</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>L’avant-propos n’est pas repris.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-23" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LIVRE DEUXIÈME</head><head type="main_subpart">SONGE D’HIVER</head><div type="poem" key="BAN14">
						<head type="number">I</head>
						<lg n="1">
							<l n="1" num="1.1">Dans nos longs soirs d’hiver, où, chez le bon Armand,</l>
							<l n="2" num="1.2">Dans notre far-niente adorable et charmant</l>
							<l n="3" num="1.3"><space quantity="8" unit="char"></space>On oubliait le monde aride,</l>
							<l n="4" num="1.4">Vous demandiez pourquoi sur mon front fatigué,</l>
							<l n="5" num="1.5">Au milieu des éclats du rire le plus gai</l>
							<l n="6" num="1.6"><space quantity="8" unit="char"></space>Grimaçait toujours une ride.</l>
						</lg>
						<lg n="2">
							<l n="7" num="2.1"><space quantity="8" unit="char"></space>Et moi, j’étais plus triste encor</l>
							<l n="8" num="2.2"><space quantity="8" unit="char"></space>Lorsque, comme en un fleuve d’or,</l>
							<l n="9" num="2.3"><space quantity="8" unit="char"></space>Je remontais dans ma mémoire,</l>
							<l n="10" num="2.4"><space quantity="8" unit="char"></space>Et que d’un regard triomphant</l>
							<l n="11" num="2.5"><space quantity="8" unit="char"></space>Je revoyais mes jours d’enfant</l>
							<l n="12" num="2.6"><space quantity="8" unit="char"></space>Couler d’émeraude et de moire,</l>
							<l n="13" num="2.7"><space quantity="8" unit="char"></space>Puis engouffrer leurs tristes flots</l>
							<l n="14" num="2.8"><space quantity="8" unit="char"></space>Au fond d’une mer sombre et noire</l>
							<l n="15" num="2.9"><space quantity="8" unit="char"></space>Avec des bruits et des sanglots.</l>
						</lg>
						<lg n="3">
							<l n="16" num="3.1">Et je me rappelais cette époque oubliée</l>
							<l n="17" num="3.2">Où l’âme d’une femme, à mon âme liée,</l>
							<l n="18" num="3.3"><space quantity="8" unit="char"></space>L’avait brisée avec si peu,</l>
							<l n="19" num="3.4">Et cette nuit d’angoisse, effarée et vivante,</l>
							<l n="20" num="3.5">Où sur ma couche, avec des sanglots d’épouvante,</l>
							<l n="21" num="3.6"><space quantity="8" unit="char"></space>Je pleurais en suppliant Dieu !</l>
						</lg>
						<lg n="4">
							<l n="22" num="4.1"><space quantity="8" unit="char"></space>Oh ! Disais-je alors, quoi ! La bouche</l>
							<l n="23" num="4.2"><space quantity="8" unit="char"></space>Qui vous caresse et qui vous touche</l>
							<l n="24" num="4.3"><space quantity="8" unit="char"></space>Avec un délire inouï,</l>
							<l n="25" num="4.4"><space quantity="8" unit="char"></space>La main frémissante qui presse</l>
							<l n="26" num="4.5"><space quantity="8" unit="char"></space>Les vôtres, les soupirs, l’ivresse,</l>
							<l n="27" num="4.6"><space quantity="8" unit="char"></space>Les yeux éteints qui disent oui,</l>
							<l n="28" num="4.7"><space quantity="8" unit="char"></space>Tout cela, ce n’est qu’un mensonge,</l>
							<l n="29" num="4.8"><space quantity="8" unit="char"></space>Ce n’est qu’un songe évanoui</l>
							<l n="30" num="4.9"><space quantity="8" unit="char"></space>Qui passe comme un autre songe !</l>
						</lg>
						<lg n="5">
							<l n="31" num="5.1">Quoi ! Lorsque je mourrai dans un délire fou,</l>
							<l n="32" num="5.2">Peut-être qu’un autre homme embrassera son cou</l>
							<l n="33" num="5.3"><space quantity="8" unit="char"></space>Malgré ses refus hypocrites,</l>
							<l n="34" num="5.4">Et quand, se souvenant, mon âme gémira,</l>
							<l n="35" num="5.5">Dans un spasme semblable elle lui redira</l>
							<l n="36" num="5.6"><space quantity="8" unit="char"></space>Les choses qu’elle m’avait dites ! »</l>
						</lg>
						<lg n="6">
							<l n="37" num="6.1"><space quantity="8" unit="char"></space>Et sous cet ardent souvenir</l>
							<l n="38" num="6.2"><space quantity="8" unit="char"></space>Du temps qui ne peut revenir</l>
							<l n="39" num="6.3"><space quantity="8" unit="char"></space>Et dont un seul instant vous sèvre,</l>
							<l n="40" num="6.4"><space quantity="8" unit="char"></space>Je me débattais dans la nuit</l>
							<l n="41" num="6.5"><space quantity="8" unit="char"></space>Comme sous un spectre qu’on fuit</l>
							<l n="42" num="6.6"><space quantity="8" unit="char"></space>Dans les visions de la fièvre ;</l>
							<l n="43" num="6.7"><space quantity="8" unit="char"></space>Puis je m’endormis, terrassé,</l>
							<l n="44" num="6.8"><space quantity="8" unit="char"></space>Le sein nu, l’écume à la lèvre,</l>
							<l n="45" num="6.9"><space quantity="8" unit="char"></space>Les yeux brûlants, le front glacé.</l>
						</lg>
						<lg n="7">
							<l n="46" num="7.1">Quand je rouvris les yeux, ô visions étranges !</l>
							<l n="47" num="7.2">Je vis auprès de moi deux femmes ou deux anges</l>
							<l n="48" num="7.3"><space quantity="8" unit="char"></space>Avec de splendides habits,</l>
							<l n="49" num="7.4">Toutes les deux montrant des beautés plus qu’humaines</l>
							<l n="50" num="7.5">Et laissant ondoyer leurs tuniques romaines</l>
							<l n="51" num="7.6"><space quantity="8" unit="char"></space>Sur des cothurnes de rubis.</l>
						</lg>
						<lg n="8">
							<l n="52" num="8.1"><space quantity="8" unit="char"></space>L’une, aux cheveux roulés en onde,</l>
							<l n="53" num="8.2"><space quantity="8" unit="char"></space>Étalait haut sa tête blonde</l>
							<l n="54" num="8.3"><space quantity="8" unit="char"></space>Sur les lignes d’un cou nerveux ;</l>
							<l n="55" num="8.4"><space quantity="8" unit="char"></space>Ardente comme un vent d’orage,</l>
							<l n="56" num="8.5"><space quantity="8" unit="char"></space>Quand son front commandait l’hommage,</l>
							<l n="57" num="8.6"><space quantity="8" unit="char"></space>Sa lèvre commandait les vœux ;</l>
							<l n="58" num="8.7"><space quantity="8" unit="char"></space>L’autre, plus blanche que l’opale,</l>
							<l n="59" num="8.8"><space quantity="8" unit="char"></space>Sous le manteau de ses cheveux</l>
							<l n="60" num="8.9"><space quantity="8" unit="char"></space>Voilait une beauté fatale.</l>
						</lg>
						<lg n="9">
							<l n="61" num="9.1">Et comme j’admirais en moi ces traits si beaux,</l>
							<l n="62" num="9.2">Comme dans leurs linceuls les marbres des tombeaux</l>
							<l n="63" num="9.3"><space quantity="8" unit="char"></space>Qu’on aime et devant qui l’on tremble,</l>
							<l n="64" num="9.4">Toutes deux, entr’ouvrant leurs lèvres à la fois,</l>
							<l n="65" num="9.5">Déployèrent dans l’ombre une splendide voix</l>
							<l n="66" num="9.6"><space quantity="8" unit="char"></space>Et tout bas me dirent ensemble :</l>
						</lg>
						<lg n="10">
							<l n="67" num="10.1"><space quantity="8" unit="char"></space>Quoi ! Parce qu’à ton premier jour</l>
							<l n="68" num="10.2"><space quantity="8" unit="char"></space>Un désenchantement d’amour</l>
							<l n="69" num="10.3"><space quantity="8" unit="char"></space>A secoué sur toi son ombre,</l>
							<l n="70" num="10.4"><space quantity="8" unit="char"></space>Tu te laisses ensevelir</l>
							<l n="71" num="10.5"><space quantity="8" unit="char"></space>Dans cet ennui qui fait pâlir</l>
							<l n="72" num="10.6"><space quantity="8" unit="char"></space>Ton front sous une douleur sombre !</l>
							<l n="73" num="10.7"><space quantity="8" unit="char"></space>Viens avec moi, viens avec nous !</l>
							<l n="74" num="10.8"><space quantity="8" unit="char"></space>Nous avons des plaisirs sans nombre</l>
							<l n="75" num="10.9"><space quantity="8" unit="char"></space>Que nous mettrons à tes genoux !</l>
						</lg>
						<lg n="11">
							<l n="76" num="11.1">—Oh ! S’il en est ainsi, si vous m’aimez, leur dis-je,</l>
							<l n="77" num="11.2">Si vous pouvez encor pour moi faire un prodige,</l>
							<l n="78" num="11.3"><space quantity="8" unit="char"></space>Rappelez l’amour oublieux !</l>
							<l n="79" num="11.4">Mais voici que la femme à blonde chevelure</l>
							<l n="80" num="11.5">M’entoura de ses bras, et, belle de luxure,</l>
							<l n="81" num="11.6"><space quantity="8" unit="char"></space>Mit ses yeux brûlants dans mes yeux.</l>
						</lg>
					</div></body></text></TEI>