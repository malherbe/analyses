<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Cariatides</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>5667 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">BAN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>

			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Cariatides</title>
						<author>Théodore de BANVILLE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L899</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Cariatides</title>
								<author>Théodore de BANVILLE</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Charpentier</publisher>
									<date when="1891">1891</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>

		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1842">1842</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>L’avant-propos n’est pas repris.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-23" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LIVRE DEUXIÈME</head><head type="main_subpart">CEUX QUI MEURENT ET CEUX QUI COMBATTENT</head><div type="poem" key="BAN30">
						<head type="number">III</head>
						<head type="main">LES DEUX FRÈRES</head>
						<lg n="1">
							<l n="1" num="1.1">Patientez encor pour une autre folie.</l>
							<l n="2" num="1.2">Les temps sont si mauvais, que pour son pauvre amant</l>
							<l n="3" num="1.3">La muse n’a gardé que sa mélancolie.</l>
							<l n="4" num="1.4">Donc naguères vivaient, sous l’azur d’Italie,</l>
							<l n="5" num="1.5">Deux frères de Toscane au langage charmant,</l>
							<l n="6" num="1.6">Qui n’avaient qu’eux au monde et s’aimaient saintement.</l>
						</lg>
						<lg n="2">
							<l n="7" num="2.1">Deux lutteurs aguerris, formidables athlètes</l>
							<l n="8" num="2.2">Jetés dans le champ clos de la société,</l>
							<l n="9" num="2.3">Deux nobles parias, en un mot deux poëtes,</l>
							<l n="10" num="2.4">Fouillant dans la nature avec avidité.</l>
							<l n="11" num="2.5">Mêlant tout, leurs douleurs stériles et leurs fêtes,</l>
							<l n="12" num="2.6">Ils se cachaient ainsi, l’un sous l’autre abrité.</l>
						</lg>
						<lg n="3">
							<l n="13" num="3.1">Oui, frères en effet ! J’ai dit qu’ils étaient frères :</l>
							<l n="14" num="3.2">Je ne sais s’ils avaient sucé le même lait</l>
							<l n="15" num="3.3">Ou s’ils s’étaient pendus aux gorges de deux mères,</l>
							<l n="16" num="3.4">Mais ils craignaient de même et la honte et le laid.</l>
							<l n="17" num="3.5">Tous deux comme un bonheur s’étaient pris au collet,</l>
							<l n="18" num="3.6">Pour s’être rencontrés le soir aux réverbères.</l>
						</lg>
						<lg n="4">
							<l n="19" num="4.1">Ils s’appelaient César et Sténio. Ce point</l>
							<l n="20" num="4.2">Éclairci, leurs passés faut-il que je les dise ?</l>
							<l n="21" num="4.3">Le plus âgé des deux c’était César. La bise</l>
							<l n="22" num="4.4">Avait connu longtemps les trous de son pourpoint,</l>
							<l n="23" num="4.5">Comme la pauvreté son lit. De Cidalise,</l>
							<l n="24" num="4.6">Ayant aimé trop tôt, je pense, il n’en eut point.</l>
						</lg>
						<lg n="5">
							<l n="25" num="5.1">Au fait, son existence avait été bizarre,</l>
							<l n="26" num="5.2">Car il était né bon dans un siècle de fer.</l>
							<l n="27" num="5.3">Rêveur dépaysé dont la folle guitare</l>
							<l n="28" num="5.4">Câlinait le passant pour lui dire un vieil air,</l>
							<l n="29" num="5.5">Le monde l’accabla de sa rigueur avare,</l>
							<l n="30" num="5.6">Et le fit, de son ciel, rouler dans un enfer.</l>
						</lg>
						<lg n="6">
							<l n="31" num="6.1">Tout enfant, il aima sa mère, une danseuse</l>
							<l n="32" num="6.2">De Parme, qui louait à tout prix son coton.</l>
							<l n="33" num="6.3">Or, un jour, au sortir d’une nuit amoureuse</l>
							<l n="34" num="6.4">Avec un nelleri, seigneur d’assez haut ton,</l>
							<l n="35" num="6.5">Comme il trouvait l’enfant d’une mine joyeuse,</l>
							<l n="36" num="6.6">Elle le lui vendit pour cent ducats, dit-on.</l>
						</lg>
						<lg n="7">
							<l n="37" num="7.1">Ce seigneur l’aima fort trois jours. Mais sa maîtresse,</l>
							<l n="38" num="7.2">Femme blonde aux yeux noirs, qui le tenait en laisse,</l>
							<l n="39" num="7.3">Choya de préférence un horrible épagneul.</l>
							<l n="40" num="7.4">Si bien qu’en un collège hostile à sa paresse,</l>
							<l n="41" num="7.5">Par un beau soir d’été, César se trouva seul</l>
							<l n="42" num="7.6">Comme un chevalier mort dans son rude linceul.</l>
						</lg>
						<lg n="8">
							<l n="43" num="8.1">Dans ces groupes d’enfants, compagnons de servage,</l>
							<l n="44" num="8.2">Qui l’entouraient, cherchant son âme dans ses yeux,</l>
							<l n="45" num="8.3">César ne se dit rien, sinon que sous les cieux</l>
							<l n="46" num="8.4">Rien ne vaudrait pour lui sa liberté sauvage,</l>
							<l n="47" num="8.5">Sa course vagabonde aux sables du rivage</l>
							<l n="48" num="8.6">Et les enivrements de son cœur soucieux.</l>
						</lg>
						<lg n="9">
							<l n="49" num="9.1">Quoiqu’il fût ennemi de toute amitié fausse,</l>
							<l n="50" num="9.2">Un d’entre eux, fin matois qu’on nommait Annibal,</l>
							<l n="51" num="9.3">Par instants lui fit croire à ces rêves qu’exauce</l>
							<l n="52" num="9.4">L’être à qui le soleil fait un manteau royal.</l>
							<l n="53" num="9.5">Donc, voilà son ami qui le baisse et le hausse</l>
							<l n="54" num="9.6">Comme un polichinelle au bout d’un fil d’archal.</l>
						</lg>
						<lg n="10">
							<l n="55" num="10.1">Plus tard il pend sa vie aux lèvres d’une femme</l>
							<l n="56" num="10.2">Vénitienne, horrible et charmant amalgame</l>
							<l n="57" num="10.3">De feux voluptueux dans un cœur endormi ;</l>
							<l n="58" num="10.4">Et lorsque enfin Thisbé l’appelait : son Pyrame,</l>
							<l n="59" num="10.5">Il trouve un soir la belle ivre, et nue à demi,</l>
							<l n="60" num="10.6">Qui rêve son remords aux bras de son ami.</l>
						</lg>
						<lg n="11">
							<l n="61" num="11.1">C’est ainsi qu’il était, malheureux et tranquille,</l>
							<l n="62" num="11.2">Songeant aux vrais plaisirs si rares et si courts,</l>
							<l n="63" num="11.3">Le front pâli déjà par la débauche vile,</l>
							<l n="64" num="11.4">Et le cœur encor plein de ses jeunes amours,</l>
							<l n="65" num="11.5">Quand, près de la taverne où s’écoulaient ses jours,</l>
							<l n="66" num="11.6">Il vint à rencontrer Sténio par la ville.</l>
						</lg>
						<lg n="12">
							<l n="67" num="12.1">Papillon de la rose et frère de l’oiseau,</l>
							<l n="68" num="12.2">C’était un doux jeune homme enivré d’ambroisie,</l>
							<l n="69" num="12.3">Amoureux du repos et de la fantaisie,</l>
							<l n="70" num="12.4">Laissant courir sa barque aux effluves de l’eau,</l>
							<l n="71" num="12.5">Et dans les bras nerveux de sa muse choisie</l>
							<l n="72" num="12.6">Couché nonchalamment, comme dans un berceau.</l>
						</lg>
						<lg n="13">
							<l n="73" num="13.1">La vaste poésie est faite avec deux choses :</l>
							<l n="74" num="13.2">Une âme, champ brûlé que fécondent les pleurs,</l>
							<l n="75" num="13.3">Puis une lyre d’or, écho de ces douleurs,</l>
							<l n="76" num="13.4">Dont la corde se plie à ses métamorphoses,</l>
							<l n="77" num="13.5">Et vibre sous la peine et sous les amours roses,</l>
							<l n="78" num="13.6">Comme sous le baiser du vent un arbre en fleurs.</l>
						</lg>
						<lg n="14">
							<l n="79" num="14.1">Oh ! Lorsqu’on prend un livre et que l’on daigne lire</l>
							<l n="80" num="14.2">Une riche pensée écrite en nobles vers,</l>
							<l n="81" num="14.3">On ne sait pas combien la page et le revers</l>
							<l n="82" num="14.4">Ont pu coûter souvent de farouche délire</l>
							<l n="83" num="14.5">Et combien le gazon a de gouffres ouverts !</l>
							<l n="84" num="14.6">C’est César qui fut l’âme, et Sténio la lyre.</l>
						</lg>
						<lg n="15">
							<l n="85" num="15.1">C’était un assemblage étrange, et que je veux</l>
							<l n="86" num="15.2">Vous peindre : l’un riant d’un sourire nerveux</l>
							<l n="87" num="15.3">Et sentant chaque jour le désespoir avide</l>
							<l n="88" num="15.4">Graver sur son front large une nouvelle ride,</l>
							<l n="89" num="15.5">Et l’autre, frais et rose avec de blonds cheveux,</l>
							<l n="90" num="15.6">Et foudroyant le mal de son doute candide,</l>
						</lg>
						<lg n="16">
							<l n="91" num="16.1">Pareilles à deux fleurs au parfum pénétrant,</l>
							<l n="92" num="16.2">Ils avaient confondu leurs deux âmes jumelles,</l>
							<l n="93" num="16.3">Si bien que la souffrance avec de sombres ailes</l>
							<l n="94" num="16.4">Emportait le bonheur pour le faire plus grand,</l>
							<l n="95" num="16.5">Noyant sa douce voix dans les plaintes mortelles,</l>
							<l n="96" num="16.6">« Comme un flot de cristal dans un sombre torrent. »</l>
						</lg>
						<lg n="17">
							<l n="97" num="17.1">C’est ainsi que César dans ses longues veillées</l>
							<l n="98" num="17.2">Disait à Sténio ses désillusions,</l>
							<l n="99" num="17.3">Ses premiers jours de foi, diaprés de rayons,</l>
							<l n="100" num="17.4">Ses espoirs, et comment sans relâche éveillées,</l>
							<l n="101" num="17.5">Des haines, par la nuit et l’enfer conseillées,</l>
							<l n="102" num="17.6">Souillent de leur venin tout ce que nous croyons.</l>
						</lg>
						<lg n="18">
							<l n="103" num="18.1">Encore extasié de sa jeunesse franche,</l>
							<l n="104" num="18.2">Pleine d’enthousiasme et de rêves touchants,</l>
							<l n="105" num="18.3">Amoureuse des bois, de la nuit et des champs,</l>
							<l n="106" num="18.4">Et de l’oiseau craintif qui chante sur la branche,</l>
							<l n="107" num="18.5">Il lui parlait de l’homme, et disait ce qui tranche</l>
							<l n="108" num="18.6">Les fils de soie et d’or de l’amour et des chants.</l>
						</lg>
						<lg n="19">
							<l n="109" num="19.1">Il lui disait comment, après des nuits de joie</l>
							<l n="110" num="19.2">Où l’amour étoilé semble un firmament bleu,</l>
							<l n="111" num="19.3">On s’éloigne à pas lents de la couche de soie,</l>
							<l n="112" num="19.4">Emportant dans son cœur la jalousie en feu,</l>
							<l n="113" num="19.5">Et comment à genoux, quand ce spectre flamboie,</l>
							<l n="114" num="19.6">On frappe sa poitrine, en criant : ô mon dieu !</l>
						</lg>
						<lg n="20">
							<l n="115" num="20.1">Mais Sténio, pressant son âme parfumée</l>
							<l n="116" num="20.2">Et blanche jusqu’au fond comme une jeune fleur,</l>
							<l n="117" num="20.3">Enveloppait César de la foi de son cœur.</l>
							<l n="118" num="20.4">Il disait, entouré d’une blanche fumée,</l>
							<l n="119" num="20.5">Et caressant toujours sa cigarette aimée :</l>
							<l n="120" num="20.6">Si c’est un rêve, ami, je veux rêver bonheur.</l>
						</lg>
						<lg n="21">
							<l n="121" num="21.1">Je veux croire à l’amour, à la nature, à l’ange,</l>
							<l n="122" num="21.2">Au doux baiser fidèle, au serrement de main,</l>
							<l n="123" num="21.3">Au rhythme harmonieux, au nectar sans mélange,</l>
							<l n="124" num="21.4">Aux amantes qui font la moitié du chemin,</l>
							<l n="125" num="21.5">Et penser jusqu’au bout que leur blonde phalange,</l>
							<l n="126" num="21.6">En nous quittant le soir, espère un lendemain.</l>
						</lg>
						<lg n="22">
							<l n="127" num="22.1">Je croirai que le monde est une grande auberge</l>
							<l n="128" num="22.2">Où l’hospitalité sans défiance héberge</l>
							<l n="129" num="22.3">Comme le grand seigneur, le passant hasardeux,</l>
							<l n="130" num="22.4">Et leur prête son lit sans se soucier d’eux.</l>
							<l n="131" num="22.5">César, calme et pensif, répondait : ô cœur vierge !</l>
							<l n="132" num="22.6">Et, la main dans la main, ils souriaient tous deux.</l>
						</lg>
						<lg n="23">
							<l n="133" num="23.1">Mais lorsqu’ils se quittaient, c’était comme une trêve</l>
							<l n="134" num="23.2">Où chacun dans son cœur changeant de souvenir,</l>
							<l n="135" num="23.3">Y sentait circuler une nouvelle sève</l>
							<l n="136" num="23.4">Et comme un feu divin la force revenir.</l>
							<l n="137" num="23.5">Car ils rêvaient tous deux, sans s’avouer leur rêve,</l>
							<l n="138" num="23.6">Sténio de douleur, et César d’avenir !</l>
						</lg>
						<lg n="24">
							<l n="139" num="24.1">Et quand César voulait attendre sur sa route</l>
							<l n="140" num="24.2">Le coursier de Lénore et le saisir aux crins,</l>
							<l n="141" num="24.3">Il se disait en lui, comme l’homme qui doute :</l>
							<l n="142" num="24.4">Qui soustraira mon frère aux dangers que j’ai craints ?</l>
							<l n="143" num="24.5">Je lui dois ma douleur, et je la lui dois toute,</l>
							<l n="144" num="24.6">Et j’en garde pour lui les splendides écrins.</l>
						</lg>
						<lg n="25">
							<l n="145" num="25.1">Mais lorsque Sténio fut complet, que la gloire</l>
							<l n="146" num="25.2">L’eut porté rayonnant à son temple d’ivoire,</l>
							<l n="147" num="25.3">César pensa tout bas : ô mort que je rêvais !</l>
							<l n="148" num="25.4">Puisque j’ai pour toujours assuré sa mémoire</l>
							<l n="149" num="25.5">Et qu’il sait à présent tout ce que je savais,</l>
							<l n="150" num="25.6">Je n’ai plus rien à dire au monde et je m’en vais !</l>
						</lg>
						<lg n="26">
							<l n="151" num="26.1">J’étais le piédestal de sa blanche statue :</l>
							<l n="152" num="26.2">Les peuples aujourd’hui la lèvent de leurs fronts.</l>
							<l n="153" num="26.3">Puisque la seule foi que ma pensée ait eue</l>
							<l n="154" num="26.4">Marche dans son triomphe, à l’abri des affronts,</l>
							<l n="155" num="26.5">Je serai tombé seul sous le coup qui me tue,</l>
							<l n="156" num="26.6">Et le repos m’attend dans la tombe : mourons !</l>
						</lg>
						<lg n="27">
							<l n="157" num="27.1">Oui, mourons aujourd’hui. Car si ma douleur cesse,</l>
							<l n="158" num="27.2">Je laisse l’agonie à celle que j’aimais.</l>
							<l n="159" num="27.3">Au milieu des plaisirs, du bruit, de la paresse,</l>
							<l n="160" num="27.4">Des chants dont la splendeur ne s’éteindra jamais</l>
							<l n="161" num="27.5">Avec tes pleurs divins lui rediront sans cesse :</l>
							<l n="162" num="27.6">Regarde, ô lâche cœur, la tombe où tu le mets !</l>
						</lg>
						<lg n="28">
							<l n="163" num="28.1">Par malheur, Sténio ne savait pas maudire.</l>
							<l n="164" num="28.2">Il perdit, le poëte à la coupe de miel !</l>
							<l n="165" num="28.3">Ces vers mélodieux pleins de rage et de fiel.</l>
							<l n="166" num="28.4">Je cherche en vain, dit-il, mon superbe délire,</l>
							<l n="167" num="28.5">Car moi, je n’étais rien que la voix d’une lyre,</l>
							<l n="168" num="28.6">Et mon âme vivante est remontée au ciel !</l>
						</lg>
					</div></body></text></TEI>