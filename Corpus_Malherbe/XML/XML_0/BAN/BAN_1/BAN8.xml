<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Cariatides</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>5667 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">BAN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>

			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Cariatides</title>
						<author>Théodore de BANVILLE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L899</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Cariatides</title>
								<author>Théodore de BANVILLE</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Charpentier</publisher>
									<date when="1891">1891</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>

		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1842">1842</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>L’avant-propos n’est pas repris.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-23" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LIVRE DEUXIÈME</head><head type="main_subpart">AMOURS D’ÉLISE</head><div type="poem" key="BAN8">
					<head type="number">III</head>
					<lg n="1">
						<l n="1" num="1.1">Oui, mon cœur et ma vie !</l>
						<l n="2" num="1.2"><space quantity="4" unit="char"></space>Et je sais bien,</l>
						<l n="3" num="1.3">Ô chère inassouvie,</l>
						<l n="4" num="1.4"><space quantity="4" unit="char"></space>Que ce n’est rien !</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">Ah ! Si j’étais la rose</l>
						<l n="6" num="2.2"><space quantity="4" unit="char"></space>Que le soir brun</l>
						<l n="7" num="2.3">En souriant arrose</l>
						<l n="8" num="2.4"><space quantity="4" unit="char"></space>D’un doux parfum ;</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">Si j’étais le bois sombre</l>
						<l n="10" num="3.2"><space quantity="4" unit="char"></space>Qui sur les champs</l>
						<l n="11" num="3.3">Jette au loin sa grande ombre</l>
						<l n="12" num="3.4"><space quantity="4" unit="char"></space>Et ses doux chants,</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">Ou l’onde triomphale</l>
						<l n="14" num="4.2"><space quantity="4" unit="char"></space>D’où le soleil</l>
						<l n="15" num="4.3">Sur son beau char d’opale</l>
						<l n="16" num="4.4"><space quantity="4" unit="char"></space>S’enfuit vermeil ;</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">Si j’étais la pervenche</l>
						<l n="18" num="5.2"><space quantity="4" unit="char"></space>Ou les roseaux,</l>
						<l n="19" num="5.3">Ou le lac, ou la branche</l>
						<l n="20" num="5.4"><space quantity="4" unit="char"></space>Pleine d’oiseaux,</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1">Ou l’étoile qui marche</l>
						<l n="22" num="6.2"><space quantity="4" unit="char"></space>Dans un ciel pur,</l>
						<l n="23" num="6.3">Ou le vieux pont d’une arche</l>
						<l n="24" num="6.4"><space quantity="4" unit="char"></space>Au profil dur ;</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1">Si j’étais la voix pleine,</l>
						<l n="26" num="7.2"><space quantity="4" unit="char"></space>La voix des cors,</l>
						<l n="27" num="7.3">Qui fait bondir la plaine</l>
						<l n="28" num="7.4"><space quantity="4" unit="char"></space>À ses accords,</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1">Ou la nymphe du saule</l>
						<l n="30" num="8.2"><space quantity="4" unit="char"></space>Au sein nerveux</l>
						<l n="31" num="8.3">Qui met sur son épaule</l>
						<l n="32" num="8.4"><space quantity="4" unit="char"></space>Ses longs cheveux ;</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1">À vous, ô charmeresse</l>
						<l n="34" num="9.2"><space quantity="4" unit="char"></space>Pleine d’attraits,</l>
						<l n="35" num="9.3">Élise, à vous, sans cesse</l>
						<l n="36" num="9.4"><space quantity="4" unit="char"></space>Je donnerais</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1">Ma voix, ma fleur, mon ombre</l>
						<l n="38" num="10.2"><space quantity="4" unit="char"></space>Douce à chacun,</l>
						<l n="39" num="10.3">Mes chants, mes bruits sans nombre</l>
						<l n="40" num="10.4"><space quantity="4" unit="char"></space>Et mon parfum,</l>
					</lg>
					<lg n="11">
						<l n="41" num="11.1">Et tout ce qui vous fête</l>
						<l n="42" num="11.2"><space quantity="4" unit="char"></space>Comme une sœur.</l>
						<l n="43" num="11.3">Mais je suis un poëte</l>
						<l n="44" num="11.4"><space quantity="4" unit="char"></space>Plein de douceur,</l>
					</lg>
					<lg n="12">
						<l n="45" num="12.1">Qui ne sait que bruire</l>
						<l n="46" num="12.2"><space quantity="4" unit="char"></space>À tous les bruits,</l>
						<l n="47" num="12.3">Faire vibrer sa lyre</l>
						<l n="48" num="12.4"><space quantity="4" unit="char"></space>Au vent des nuits,</l>
					</lg>
					<lg n="13">
						<l n="49" num="13.1">Ou, quand le jour se lève</l>
						<l n="50" num="13.2"><space quantity="4" unit="char"></space>Tout azuré,</l>
						<l n="51" num="13.3">S’envoler dans un rêve</l>
						<l n="52" num="13.4"><space quantity="4" unit="char"></space>Démesuré.</l>
					</lg>
					<lg n="14">
						<l n="53" num="14.1">Donc, je vous ai servie,</l>
						<l n="54" num="14.2"><space quantity="4" unit="char"></space>Heureux encor</l>
						<l n="55" num="14.3">De vous donner ma vie,</l>
						<l n="56" num="14.4"><space quantity="4" unit="char"></space>Cette fleur d’or</l>
					</lg>
					<lg n="15">
						<l n="57" num="15.1">Que tourmente et caresse</l>
						<l n="58" num="15.2"><space quantity="4" unit="char"></space>Dans un rayon</l>
						<l n="59" num="15.3">La frivole déesse</l>
						<l n="60" num="15.4"><space quantity="4" unit="char"></space>Illusion ;</l>
					</lg>
					<lg n="16">
						<l n="61" num="16.1">Mon esprit, qui s’enivre</l>
						<l n="62" num="16.2"><space quantity="4" unit="char"></space>De vos clartés,</l>
						<l n="63" num="16.3">Et qui ne veut plus vivre</l>
						<l n="64" num="16.4"><space quantity="4" unit="char"></space>Quand vous partez ;</l>
					</lg>
					<lg n="17">
						<l n="65" num="17.1">Et tout ce que je souffre</l>
						<l n="66" num="17.2"><space quantity="4" unit="char"></space>Si loin du jour,</l>
						<l n="67" num="17.3">Et mon âme, ce gouffre</l>
						<l n="68" num="17.4"><space quantity="4" unit="char"></space>Empli d’amour !</l>
					</lg>
				</div></body></text></TEI>