<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Cariatides</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>5667 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">BAN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>

			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Cariatides</title>
						<author>Théodore de BANVILLE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L899</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Cariatides</title>
								<author>Théodore de BANVILLE</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Charpentier</publisher>
									<date when="1891">1891</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>

		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1842">1842</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>L’avant-propos n’est pas repris.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-23" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LIVRE DEUXIÈME</head><head type="main_subpart">CEUX QUI MEURENT ET CEUX QUI COMBATTENT</head><div type="poem" key="BAN28">
						<head type="number">I</head>
						<head type="main">LA LYRE MORTE</head>
						<lg n="1">
							<l n="1" num="1.1">Ce que je veux rimer, c’est un conte en sixains.</l>
							<l n="2" num="1.2">Surtout n’y cherchez pas la trace d’une intrigue.</l>
							<l n="3" num="1.3">L’air est sans fioriture et le fond sans dessins.</l>
							<l n="4" num="1.4">D’abord j’ai de tout temps exécré la fatigue,</l>
							<l n="5" num="1.5">Puis je n’ai jamais eu que des goûts forts succincts</l>
							<l n="6" num="1.6">Pour l’intérêt nerveux que le vulgaire brigue.</l>
						</lg>
						<lg n="2">
							<l n="7" num="2.1">La Chimère est debout : marche, Bellérophon !</l>
							<l n="8" num="2.2">Quel est donc mon sujet ? Je l’avais dans la tête.</l>
							<l n="9" num="2.3">Ah ! Voici. Le héros, madame, est un poëte,</l>
							<l n="10" num="2.4">C’est-à-dire ce monstre oublié par Buffon</l>
							<l n="11" num="2.5">Dans la liste des ours, dont on fait un bouffon</l>
							<l n="12" num="2.6">Pour égayer son hôte à la fin d’une fête.</l>
						</lg>
						<lg n="3">
							<l n="13" num="3.1">C’était un pauvre hère. Il s’appelait Henri.</l>
							<l n="14" num="3.2">Il n’était pas marquis, ni gendarme, ni comte.</l>
							<l n="15" num="3.3">C’était un de ces nains au regard aguerri</l>
							<l n="16" num="3.4">Dont l’orgueil est coulé dans un moule de fonte,</l>
							<l n="17" num="3.5">Gueux de peu de valeur qui rimaillent sans honte,</l>
							<l n="18" num="3.6">Et que vous laissez là pour le chat favori.</l>
						</lg>
						<lg n="4">
							<l n="19" num="4.1">Et vous faites fort bien. Mais nous, c’est autre chose :</l>
							<l n="20" num="4.2">Une larme du cœur est pour nous un trésor.</l>
							<l n="21" num="4.3">Notre âme en pleurs s’éveille au parfum d’une rose</l>
							<l n="22" num="4.4">Et tressaille au zéphyr où passe un chant de cor,</l>
							<l n="23" num="4.5">Sur l’oreiller de pierre où notre front se pose.</l>
							<l n="24" num="4.6">Tout ce que nous touchons a des paillettes d’or.</l>
						</lg>
						<lg n="5">
							<l n="25" num="5.1">Excusez donc, par grâce, une douce manie.</l>
							<l n="26" num="5.2">Je reprends mon langage. Au fait, il m’en coûtait.</l>
							<l n="27" num="5.3">L’huissier a bien le droit d’écrire son protêt</l>
							<l n="28" num="5.4">Dans un hideux patois que l’univers renie :</l>
							<l n="29" num="5.5">Je puis jeter le masque, et mon héros était</l>
							<l n="30" num="5.6">Ce que nous appelons un homme de génie.</l>
						</lg>
						<lg n="6">
							<l n="31" num="6.1">Il vivait seul chez lui comme un vieux hobereau,</l>
							<l n="32" num="6.2">N’ayant jamais voulu de femme pour maîtresse.</l>
							<l n="33" num="6.3">Mais il avait sa muse et la folle paresse,</l>
							<l n="34" num="6.4">Et près de sa fenêtre un bouquet de sureau :</l>
							<l n="35" num="6.5">Pour employer son temps, il mettait son ivresse</l>
							<l n="36" num="6.6">À noircir du papier devant un vieux bureau.</l>
						</lg>
						<lg n="7">
							<l n="37" num="7.1">Une telle existence est pour tous un mystère</l>
							<l n="38" num="7.2">Que je veux expliquer, et que je devrais taire.</l>
							<l n="39" num="7.3">Quand on est ainsi fait, on vit bien autrement</l>
							<l n="40" num="7.4">Que ne vit le prochain sur cette pauvre terre :</l>
							<l n="41" num="7.5">La douleur est pour l’âme un fécond aliment,</l>
							<l n="42" num="7.6">Et l’âme est un foyer qui s’endort rarement.</l>
						</lg>
						<lg n="8">
							<l n="43" num="8.1">Le poëte est tordu comme était la sybille.</l>
							<l n="44" num="8.2">Quand un livre sincère est jusqu’à moitié fait,</l>
							<l n="45" num="8.3">On sent qu’on a besoin d’air et qu’on étouffait.</l>
							<l n="46" num="8.4">On va se promener en courant par la ville,</l>
							<l n="47" num="8.5">Car l’inspiration, brisant le front débile,</l>
							<l n="48" num="8.6">Pour celui qui la porte a le poids d’un forfait.</l>
						</lg>
						<lg n="9">
							<l n="49" num="9.1">On sent que comme l’aigle on domine la foule,</l>
							<l n="50" num="9.2">Qu’on est le vrai lien de la terre et du ciel,</l>
							<l n="51" num="9.3">Qu’on retient seul du doigt la croyance qui croule</l>
							<l n="52" num="9.4">Et qu’on mourra pourtant comme les deux Abel,</l>
							<l n="53" num="9.5">Car on a comme eux deux un sang divin qui coule</l>
							<l n="54" num="9.6">Pour teindre le gibet et pour laver l’autel.</l>
						</lg>
						<lg n="10">
							<l n="55" num="10.1">Puis, on ne comprend pas qu’une hymne aussi parfaite</l>
							<l n="56" num="10.2">Ait mûri jusqu’au bout dans ce cadavre humain.</l>
							<l n="57" num="10.3">On se demande alors qui vous a fait prophète</l>
							<l n="58" num="10.4">Et qui vous conduisait dans cet ardent chemin,</l>
							<l n="59" num="10.5">Vous, travailleur obscur, à qui les grands, du faîte,</l>
							<l n="60" num="10.6">Jetteraient une obole, en passant, dans la main !</l>
						</lg>
						<lg n="11">
							<l n="61" num="11.1">Henri s’entortillait dans cette étrange trame,</l>
							<l n="62" num="11.2">Sur le bitume gris, près du diorama,</l>
							<l n="63" num="11.3">Lorsque vint à passer, dans sa gloire, une femme</l>
							<l n="64" num="11.4">Dont l’attrait merveilleux le prit et le charma,</l>
							<l n="65" num="11.5">Comme s’il eût pu voir Hélène De Pergame.</l>
							<l n="66" num="11.6">Il regarda longtemps cette femme, et l’aima.</l>
						</lg>
						<lg n="12">
							<l n="67" num="12.1">Elle avait, cher lecteur, une fort belle gorge,</l>
							<l n="68" num="12.2">Un cachemire noir souple comme un collier,</l>
							<l n="69" num="12.3">Brodé d’argent et d’or dans un goût singulier,</l>
							<l n="70" num="12.4">Des doigts fins et longs, tels que l’amour grec en forge,</l>
							<l n="71" num="12.5">Et de plus le profil superbe et régulier</l>
							<l n="72" num="12.6">Comme l’avait jadis Mademoiselle George,</l>
						</lg>
						<lg n="13">
							<l n="73" num="13.1">Son front païen eût mis Corinthe en désarroi ;</l>
							<l n="74" num="13.2">Ses cheveux étaient longs « comme un manteau de roi »,</l>
							<l n="75" num="13.3">Son nez beaucoup plus pur qu’on ne se l’imagine ;</l>
							<l n="76" num="13.4">Ses pieds savaient conter toute son origine,</l>
							<l n="77" num="13.5">Enfin, cette autre Isis des bas-reliefs d’Égine</l>
							<l n="78" num="13.6">Avait la lèvre rouge à donner de l’effroi.</l>
						</lg>
						<lg n="14">
							<l n="79" num="14.1">Je ne veux pas conter une bonne fortune.</l>
							<l n="80" num="14.2">Ces histoires d’amour font un énorme bruit ;</l>
							<l n="81" num="14.3">En somme cependant, quand on en connaît une,</l>
							<l n="82" num="14.4">On peut savoir à quoi le reste se réduit.</l>
							<l n="83" num="14.5">Je ne dirai donc pas comment la belle brune</l>
							<l n="84" num="14.6">Prit Henri pour amant un jour, non, une nuit.</l>
						</lg>
						<lg n="15">
							<l n="85" num="15.1">Henri vers le bonheur s’avança les mains pleines,</l>
							<l n="86" num="15.2">Il courut à l’amour comme au cirque un martyr.</l>
							<l n="87" num="15.3">Venant comme quelqu’un qui ne doit pas partir,</l>
							<l n="88" num="15.4">Il y jeta d’un coup ses bonheurs et ses haines,</l>
							<l n="89" num="15.5">Comme aux marbres du bain les bacchantes romaines</l>
							<l n="90" num="15.6">Leurs essences d’Émèse et leurs parfums de Tyr.</l>
						</lg>
						<lg n="16">
							<l n="91" num="16.1">Dans la vénus de chair qu’il avait asservie</l>
							<l n="92" num="16.2">Il trouva sa parure et son rhythme et sa vie,</l>
							<l n="93" num="16.3">Et s’en enveloppa comme d’un vêtement.</l>
							<l n="94" num="16.4">Toute félicité nous est trop tôt ravie !</l>
							<l n="95" num="16.5">Il s’aperçut un soir, oh rien ! Tout bonnement</l>
							<l n="96" num="16.6">Que son rhythme et sa vie avait un autre amant.</l>
						</lg>
						<lg n="17">
							<l n="97" num="17.1">Comme il ne singeait pas l’Othello de banlieue,</l>
							<l n="98" num="17.2">Il ne tua personne. Hélas ! à pas comptés</l>
							<l n="99" num="17.3">Il sortit sans courroux, fit une bonne lieue,</l>
							<l n="100" num="17.4">Rentra, puis, allumant sa cigarette bleue,</l>
							<l n="101" num="17.5">La maîtresse qu’on a sans infidélités,</l>
							<l n="102" num="17.6">Se dit, je sais encor ce qu’il dit : écoutez !</l>
						</lg>
						<lg n="18">
							<l n="103" num="18.1">Puisque la seule enfant qui pouvait sur la terre</l>
							<l n="104" num="18.2">Étreindre ma pensée et toutes ses splendeurs</l>
							<l n="105" num="18.3">A refusé sa lèvre au fruit qui désaltère</l>
							<l n="106" num="18.4">Et comme un vieux haillon rejeté mes grandeurs,</l>
							<l n="107" num="18.5">J’achèverai tout seul ma course solitaire,</l>
							<l n="108" num="18.6">Et nul ne connaîtra mes sourdes profondeurs.</l>
						</lg>
						<lg n="19">
							<l n="109" num="19.1">Passez autour de moi, femmes riches et belles !</l>
							<l n="110" num="19.2">Je pourrais d’un seul mot conserver ces appas</l>
							<l n="111" num="19.3">Qui jauniront demain sous vos blanches dentelles ;</l>
							<l n="112" num="19.4">Mais ce mot infini qui vous rend immortelles</l>
							<l n="113" num="19.5">Est mon secret à moi que je ne dirai pas,</l>
							<l n="114" num="19.6">Et la droite du temps effacera vos pas !</l>
						</lg>
						<lg n="20">
							<l n="115" num="20.1">Ô lutteurs gangrenés ! Mourantes populaces !</l>
							<l n="116" num="20.2">Je sais sous quel fardeau se courbent vos audaces,</l>
							<l n="117" num="20.3">Et ma parole d’or allégerait vos pas.</l>
							<l n="118" num="20.4">Je pourrais ramener le bonheur sur vos places</l>
							<l n="119" num="20.5">Et sécher la sueur qui mouille vos repas ;</l>
							<l n="120" num="20.6">Mais ce mot qui guérit, je ne le dirai pas !</l>
						</lg>
						<lg n="21">
							<l n="121" num="21.1">Je veux voir le vieux monde élaborer le crime</l>
							<l n="122" num="21.2">Sous le marteau pesant de la fatalité,</l>
							<l n="123" num="21.3">Seul, muet, dédaigneux de l’éternelle cime,</l>
							<l n="124" num="21.4">Avare de ma force et de ma liberté,</l>
							<l n="125" num="21.5">Ne me souciant plus que le vol de la rime</l>
							<l n="126" num="21.6">Emporte mes héros dans l’immortalité !</l>
						</lg>
						<lg n="22">
							<l n="127" num="22.1">Mais comment achever le tableau que j’ébauche,</l>
							<l n="128" num="22.2">Et que se passa-t-il entre sa muse et lui ?</l>
							<l n="129" num="22.3">C’est de la nuit profonde, où nul rayon n’a lui.</l>
							<l n="130" num="22.4">Un serpent le rongeait sous la mamelle gauche.</l>
							<l n="131" num="22.5">Ont-ils fait de l’amour ou bien de la débauche ?</l>
							<l n="132" num="22.6">Je ne le savais pas, je le sais aujourd’hui.</l>
						</lg>
						<lg n="23">
							<l n="133" num="23.1">Un jour la pâle mort vint frapper à sa porte ;</l>
							<l n="134" num="23.2">Il la fit rafraîchir, rajusta son bonnet,</l>
							<l n="135" num="23.3">Et la complimenta si bien, qu’il fit en sorte,</l>
							<l n="136" num="23.4">Avec son agrément, de finir un sonnet.</l>
							<l n="137" num="23.5">Puis il offrit sa main pour lui servir d’escorte ;</l>
							<l n="138" num="23.6">Ce fut au mieux. Voilà tout ce qu’on en connaît.</l>
						</lg>
						<lg n="24">
							<l n="139" num="24.1">Or, ce pauvre Henri, dont la mémoire est vide,</l>
							<l n="140" num="24.2">Fut le dernier chanteur à qui l’Aganippide</l>
							<l n="141" num="24.3">Montrait sa chair de neige et sa fauve toison,</l>
							<l n="142" num="24.4">Et nous sommes restés pour fermer la maison.</l>
							<l n="143" num="24.5">Aussi, quand vous raillez notre horde stupide,</l>
							<l n="144" num="24.6">Vous autres gens d’esprit, vous avez bien raison !</l>
						</lg>
					</div></body></text></TEI>