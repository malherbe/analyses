<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Cariatides</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>5667 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">BAN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>

			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Cariatides</title>
						<author>Théodore de BANVILLE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L899</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Cariatides</title>
								<author>Théodore de BANVILLE</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Charpentier</publisher>
									<date when="1891">1891</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>

		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1842">1842</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>L’avant-propos n’est pas repris.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-23" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LIVRE DEUXIÈME</head><div type="poem" key="BAN27">
					<head type="main">LA NUIT DE PRINTEMPS</head>
					<opener>
						<epigraph>
							<cit>
								<quote>If we shadows have offended, <lb></lb>
									Think but this, (and all is mended) <lb></lb>
									That you have but slumber’d here, <lb></lb>
									While these visions did appear; <lb></lb>
									And this weak and idle theme, <lb></lb>
									No more yiedling but a dream, <lb></lb>
									Gentles, do not reprehend; <lb></lb>
									If you pardon, we will mend.</quote>
								<bibl>
									<name>SHAKSPERE</name> <hi rend="ital">Midsummer-night’s dream</hi>, <lb></lb>acte V, scène II.
								</bibl>
							</cit>
						</epigraph>
					</opener>
					<lg n="1">
						<l n="1" num="1.1">C’était la veille de mai,</l>
						<l n="2" num="1.2">Un soir souriant de fête,</l>
						<l n="3" num="1.3">Et tout semblait embaumé</l>
						<l n="4" num="1.4">D’une tendresse parfaite.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">De son lit à baldaquin,</l>
						<l n="6" num="2.2">Le soleil sur son beau globe</l>
						<l n="7" num="2.3">Avait l’air d’un arlequin</l>
						<l n="8" num="2.4">Étalant sa garde-robe,</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">Et sa sœur au front changeant,</l>
						<l n="10" num="3.2">Mademoiselle la lune</l>
						<l n="11" num="3.3">Avec ses grands yeux d’argent</l>
						<l n="12" num="3.4">Regardait la terre brune,</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">Et du ciel, où, comme un roi,</l>
						<l n="14" num="4.2">Chaque astre vit de ses rentes,</l>
						<l n="15" num="4.3">Contemplait avec effroi</l>
						<l n="16" num="4.4">Le lac aux eaux transparentes ;</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">Comme, avec son air trompeur,</l>
						<l n="18" num="5.2">Colombine, qu’on attrape,</l>
						<l n="19" num="5.3">À la fin du drame a peur</l>
						<l n="20" num="5.4">De tomber dans une trappe.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1">Tous les jeunes séraphins,</l>
						<l n="22" num="6.2">À cheval sur mille nues,</l>
						<l n="23" num="6.3">Agaçaient de regards fins</l>
						<l n="24" num="6.4">Leurs comètes toutes nues.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1">Sur son trône, le bon Dieu,</l>
						<l n="26" num="7.2">Devant qui le lys foisonne,</l>
						<l n="27" num="7.3">Comme un seigneur de haut lieu</l>
						<l n="28" num="7.4">Que sa grandeur emprisonne,</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1">À ces intrigues d’enfants</l>
						<l n="30" num="8.2">N’ayant pas daigné descendre,</l>
						<l n="31" num="8.3">Les laissait, tout triomphants,</l>
						<l n="32" num="8.4">Le tromper comme un Cassandre.</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1">Or, en même temps qu’aux cieux,</l>
						<l n="34" num="9.2">C’était comme un grand remue</l>
						<l n="35" num="9.3">Ménage délicieux</l>
						<l n="36" num="9.4">Sur la pauvre terre émue.</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1">Des sylphes, des chérubins,</l>
						<l n="38" num="10.2">S’occupaient de mille choses,</l>
						<l n="39" num="10.3">Et sous leurs fronts de bambins</l>
						<l n="40" num="10.4">Roulaient de gros yeux moroses.</l>
					</lg>
					<lg n="11">
						<l n="41" num="11.1">Quel embarras, disaient-ils</l>
						<l n="42" num="11.2">Dans leurs langages superbes ;</l>
						<l n="43" num="11.3">À ces fleurs pas de pistils,</l>
						<l n="44" num="11.4">Pas de bleuets dans ces herbes !</l>
					</lg>
					<lg n="12">
						<l n="45" num="12.1">Dans ce ciel pas de saphirs,</l>
						<l n="46" num="12.2">Pas de feuilles à ces arbres !</l>
						<l n="47" num="12.3">Où sont nos frères zéphyrs</l>
						<l n="48" num="12.4">Pour embaumer l’eau des marbres ?</l>
					</lg>
					<lg n="13">
						<l n="49" num="13.1">Hélas ! Comment ferons-nous ?</l>
						<l n="50" num="13.2">Nous méritons qu’on nous tance ;</l>
						<l n="51" num="13.3">Le bon Dieu sur nos genoux</l>
						<l n="52" num="13.4">Va nous mettre en pénitence !</l>
					</lg>
					<lg n="14">
						<l n="53" num="14.1">Car hier au bal dansant,</l>
						<l n="54" num="14.2">Où, sorti pour ses affaires,</l>
						<l n="55" num="14.3">Il mariait en passant</l>
						<l n="56" num="14.4">Deux soleils avec leurs sphères,</l>
					</lg>
					<lg n="15">
						<l n="57" num="15.1">Nous avons de notre main</l>
						<l n="58" num="15.2">Promis sur le divin cierge</l>
						<l n="59" num="15.3">Son mois de mai pour demain</l>
						<l n="60" num="15.4">À notre dame la vierge !</l>
					</lg>
					<lg n="16">
						<l n="61" num="16.1">Hélas ! Jamais tout n’ira</l>
						<l n="62" num="16.2">Comme à la saison dernière,</l>
						<l n="63" num="16.3">Bien sûr on nous punira</l>
						<l n="64" num="16.4">De l’école buissonnière.</l>
					</lg>
					<lg n="17">
						<l n="65" num="17.1">Pour ce mai qu’on nous promet</l>
						<l n="66" num="17.2">Ils versent des pleurs de rage,</l>
						<l n="67" num="17.3">Et vite chacun se met</l>
						<l n="68" num="17.4">À commencer son ouvrage.</l>
					</lg>
					<lg n="18">
						<l n="69" num="18.1">Penchés sur les arbrisseaux,</l>
						<l n="70" num="18.2">Les uns au milieu des prées,</l>
						<l n="71" num="18.3">Avec de petits pinceaux</l>
						<l n="72" num="18.4">Peignent les fleurs diaprées,</l>
					</lg>
					<lg n="19">
						<l n="73" num="19.1">Et, de face ou de profil,</l>
						<l n="74" num="19.2">Après les branches ouvertes</l>
						<l n="75" num="19.3">Attachent avec un fil</l>
						<l n="76" num="19.4">De petites feuilles vertes.</l>
					</lg>
					<lg n="20">
						<l n="77" num="20.1">Les autres au papillon</l>
						<l n="78" num="20.2">Mettent l’azur de ses ailes,</l>
						<l n="79" num="20.3">Qu’ils prennent sur un rayon</l>
						<l n="80" num="20.4">Peint des couleurs les plus belles.</l>
					</lg>
					<lg n="21">
						<l n="81" num="21.1">Des ariels dans les cieux,</l>
						<l n="82" num="21.2">Assis près de leurs amantes,</l>
						<l n="83" num="21.3">Agitent des miroirs bleus</l>
						<l n="84" num="21.4">Au-dessus des eaux dormantes.</l>
					</lg>
					<lg n="22">
						<l n="85" num="22.1">Sur la vague aux cheveux verts</l>
						<l n="86" num="22.2">Les ondins peignent la moire,</l>
						<l n="87" num="22.3">Et lui serinent des vers</l>
						<l n="88" num="22.4">Trouvés dans un vieux grimoire.</l>
					</lg>
					<lg n="23">
						<l n="89" num="23.1">Les sylphes blonds dans son vol</l>
						<l n="90" num="23.2">Arrêtent l’oiseau qui chante,</l>
						<l n="91" num="23.3">Et lui disent : rossignol,</l>
						<l n="92" num="23.4">Apprends ta chanson touchante ;</l>
					</lg>
					<lg n="24">
						<l n="93" num="24.1">Car il faut que pour demain</l>
						<l n="94" num="24.2">On ait la chanson nouvelle.</l>
						<l n="95" num="24.3">Puis le cahier d’une main,</l>
						<l n="96" num="24.4">De l’autre ils lui tiennent l’aile.</l>
					</lg>
					<lg n="25">
						<l n="97" num="25.1">Et ceux-là, portant des fleurs</l>
						<l n="98" num="25.2">Et de jolis flacons d’ambre,</l>
						<l n="99" num="25.3">S’en vont, doux ensorceleurs,</l>
						<l n="100" num="25.4">Voir mainte petite chambre,</l>
					</lg>
					<lg n="26">
						<l n="101" num="26.1">Où mainte enfant, lys pâli,</l>
						<l n="102" num="26.2">Écoute, endormie et nue,</l>
						<l n="103" num="26.3">Fredonner un bengali</l>
						<l n="104" num="26.4">Dans son âme d’ingénue.</l>
					</lg>
					<lg n="27">
						<l n="105" num="27.1">Ils étendent en essaim</l>
						<l n="106" num="27.2">Mille roses sur sa lèvre,</l>
						<l n="107" num="27.3">Un peu de neige à son sein,</l>
						<l n="108" num="27.4">Dans son cœur un peu de fièvre.</l>
					</lg>
					<lg n="28">
						<l n="109" num="28.1">Aucun ne sera puni,</l>
						<l n="110" num="28.2">La vierge sera contente ;</l>
						<l n="111" num="28.3">Car nous avons tout fourni,</l>
						<l n="112" num="28.4">Ce qui charme et ce qui tente !</l>
					</lg>
					<lg n="29">
						<l n="113" num="29.1">Et sylphes, et chérubins,</l>
						<l n="114" num="29.2">Ce joli torrent sans digue,</l>
						<l n="115" num="29.3">Vont se délasser aux bains</l>
						<l n="116" num="29.4">Du bruit et de la fatigue.</l>
					</lg>
					<lg n="30">
						<l n="117" num="30.1">Dieu soit béni, disent-ils,</l>
						<l n="118" num="30.2">Nous avons fini la chose !</l>
						<l n="119" num="30.3">Aux fleurs voici les pistils,</l>
						<l n="120" num="30.4">Des parfums, du satin rose ;</l>
					</lg>
					<lg n="31">
						<l n="121" num="31.1">Au papillon bleu son vol,</l>
						<l n="122" num="31.2">Aux bois rajeunis leur ombre,</l>
						<l n="123" num="31.3">Son doux chant au rossignol</l>
						<l n="124" num="31.4">Caché dans la forêt sombre !</l>
					</lg>
					<lg n="32">
						<l n="125" num="32.1">Voici leur saphir aux cieux</l>
						<l n="126" num="32.2">Dans la lumière fleurie,</l>
						<l n="127" num="32.3">À l’herbe ses bleuets bleus,</l>
						<l n="128" num="32.4">Pour que la vierge sourie !</l>
					</lg>
					<lg n="33">
						<l n="129" num="33.1">Mais ce n’est pas tout encor,</l>
						<l n="130" num="33.2">Car ils me disent : poëte !</l>
						<l n="131" num="33.3">Voilà mille rimes d’or,</l>
						<l n="132" num="33.4">Pour que tu sois de la fête.</l>
					</lg>
					<lg n="34">
						<l n="133" num="34.1">Prends-les, tu feras des chants</l>
						<l n="134" num="34.2">Que nous apprendrons aux roses,</l>
						<l n="135" num="34.3">Pour les dire lorsque aux champs</l>
						<l n="136" num="34.4">Elles s’éveillent mi-closes.</l>
					</lg>
					<lg n="35">
						<l n="137" num="35.1">Et certes mon rêve ailé</l>
						<l n="138" num="35.2">Eût fait une hymne bien belle</l>
						<l n="139" num="35.3">Si ce qu’ils m’ont révélé</l>
						<l n="140" num="35.4">Fût resté dans ma cervelle.</l>
					</lg>
					<lg n="36">
						<l n="141" num="36.1">Ils murmuraient, Dieu le sait,</l>
						<l n="142" num="36.2">Des rimes si bien éprises.</l>
						<l n="143" num="36.3">Mais le zéphyr qui passait</l>
						<l n="144" num="36.4">En passant me les a prises !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1842">avril 1842</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>