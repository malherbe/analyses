<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Cariatides</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>5667 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">BAN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>

			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Cariatides</title>
						<author>Théodore de BANVILLE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L899</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Cariatides</title>
								<author>Théodore de BANVILLE</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Charpentier</publisher>
									<date when="1891">1891</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>

		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1842">1842</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>L’avant-propos n’est pas repris.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-23" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LIVRE DEUXIÈME</head><div type="poem" key="BAN26">
					<head type="main">CLYMÈNE</head>
					<lg n="1">
						<l n="1" num="1.1">L’aurore enveloppait dans une clarté rose</l>
						<l n="2" num="1.2">Le vallon gracieux que le Pénée arrose,</l>
						<l n="3" num="1.3">Et les arbres touffus, et la brise et les flots</l>
						<l n="4" num="1.4">Se redisaient au loin d’harmonieux sanglots.</l>
						<l n="5" num="1.5">Près du fleuve pleurait, parmi les hautes herbes,</l>
						<l n="6" num="1.6">Une nymphe étendue. À ses regards superbes,</l>
						<l n="7" num="1.7">À ses bras vigoureux et vers le ciel ouverts,</l>
						<l n="8" num="1.8">À ses grands cheveux blonds marbrés de reflets verts,</l>
						<l n="9" num="1.9">On eût pu reconnaître une fille honorée</l>
						<l n="10" num="1.10">De Doris aux beaux yeux et du sage Nérée.</l>
						<l n="11" num="1.11">Ses cheveux dénoués se déroulaient épars,</l>
						<l n="12" num="1.12">Et ses pleurs sur son corps tombaient de toutes parts.</l>
						<l n="13" num="1.13"><space quantity="2" unit="char"></space>Ô trop bel Iolas ! Insensé, disait-elle,</l>
						<l n="14" num="1.14">Pourquoi dédaignes-tu l’amour d’une immortelle ?</l>
						<l n="15" num="1.15">Guidé par ta fureur, sans écouter ma voix,</l>
						<l n="16" num="1.16">Tu vas, chasseur cruel, ensanglanter les bois.</l>
						<l n="17" num="1.17">Enfant ! Je ne suis pas une blonde sirène</l>
						<l n="18" num="1.18">Dont les chants radieux pendant la nuit sereine</l>
						<l n="19" num="1.19">Égarent le pilote au milieu des roseaux.</l>
						<l n="20" num="1.20">Hélas ! J’ai bien souvent, sur l’azur de ces eaux,</l>
						<l n="21" num="1.21">Avec mes jeunes sœurs, nymphes aux belles joues,</l>
						<l n="22" num="1.22">Folâtré près de toi dans l’onde où tu te joues,</l>
						<l n="23" num="1.23">Et pour ton fleuve bleu quitté nos océans !</l>
						<l n="24" num="1.24">Bien souvent, pour te voir, j’ai sur les monts géants</l>
						<l n="25" num="1.25">Porté le long carquois des jeunes chasseresses,</l>
						<l n="26" num="1.26">Et, livrant aux zéphyrs tous mes cheveux en tresses,</l>
						<l n="27" num="1.27">Comme font les enfants de l’antique Ilion,</l>
						<l n="28" num="1.28">Jeté sur mon épaule une peau de lion.</l>
						<l n="29" num="1.29"><space quantity="2" unit="char"></space>Bien souvent, nue, en chœur j’ai conduit sous ces arbres</l>
						<l n="30" num="1.30">Les nymphes du vallon aux poitrines de marbres ;</l>
						<l n="31" num="1.31">Mais sous les flots d’azur, aux grands bois, dans les champs,</l>
						<l n="32" num="1.32">Jamais tu n’es venu pour écouter mes chants.</l>
						<l n="33" num="1.33">Et cependant, ainsi que les nymphes des plaines,</l>
						<l n="34" num="1.34">J’avais pour toi des lys dans mes corbeilles pleines ;</l>
						<l n="35" num="1.35">Mais tu les refusais, et la seule Phyllis</l>
						<l n="36" num="1.36">Peut jeter devant toi ses chansons et ses lys.</l>
						<l n="37" num="1.37">Quand je t’ai tout offert, tu gardais tout pour elle.</l>
						<l n="38" num="1.38">Et pourtant de nous deux quelle était la plus belle ?</l>
						<l n="39" num="1.39">Souvent dans nos palais j’ai vu le flot, moins prompt,</l>
						<l n="40" num="1.40">Frémir joyeusement de réfléchir mon front ;</l>
						<l n="41" num="1.41">Sur un sein éclatant mon cou veiné s’incline,</l>
						<l n="42" num="1.42">Un sang pur a pourpré ma lèvre coralline,</l>
						<l n="43" num="1.43">Le ciel rit dans mes yeux, et les divins amants</l>
						<l n="44" num="1.44">Autrefois m’appelaient Clymène aux pieds charmants.</l>
						<l n="45" num="1.45">Ami ! Viens avec moi. Nos sœurs les néréides</l>
						<l n="46" num="1.46">T’ouvriront sur mes pas leurs demeures splendides,</l>
						<l n="47" num="1.47">Et, près des cygnes purs, dans leurs ébats joyeux,</l>
						<l n="48" num="1.48">Nageront, se plaisant à réjouir tes yeux.</l>
						<l n="49" num="1.49"><space quantity="2" unit="char"></space>Là, comme les grands dieux, dans nos chastes délires</l>
						<l n="50" num="1.50">Nous savons marier nos voix aux voix des lyres,</l>
						<l n="51" num="1.51">Ou verser le nectar dans les vases dorés ;</l>
						<l n="52" num="1.52">Et l’onde, en se jouant près de nos bras nacrés,</l>
						<l n="53" num="1.53">Songe encore aux blancheurs de l’anadyomène.</l>
						<l n="54" num="1.54">Oh ! Désarme pour moi ta froideur inhumaine ;</l>
						<l n="55" num="1.55">Viens ! Si tu ne veux pas que sous ces arbrisseaux</l>
						<l n="56" num="1.56">Mes yeux voilés de pleurs se changent en ruisseaux</l>
						<l n="57" num="1.57">Ou que tordant mes bras divins, comme Aréthuse,</l>
						<l n="58" num="1.58">Je meure, en exhalant une plainte confuse.</l>
						<l n="59" num="1.59">Mais, hélas ! L’écho seul répond à mes accords ;</l>
						<l n="60" num="1.60">Le soleil rougissant a desséché mon corps</l>
						<l n="61" num="1.61">Depuis que je t’attends de tes lointaines courses,</l>
						<l n="62" num="1.62">Et mes yeux étoilés pleurent comme deux sources.</l>
						<l n="63" num="1.63"><space quantity="2" unit="char"></space>Ainsi Clymène, offerte au courroux de Vénus,</l>
						<l n="64" num="1.64">Disait sa plainte amère ; et les sœurs de Cycnus</l>
						<l n="65" num="1.65">Pleuraient des larmes d’ambre, et les gouffres du fleuve</l>
						<l n="66" num="1.66">Pleuraient, et la fleur vierge, et la colombe veuve,</l>
						<l n="67" num="1.67">Et la jeune Dryade en tordant ses rameaux,</l>
						<l n="68" num="1.68">Pleuraient et gémissaient avec d’étranges mots.</l>
						<l n="69" num="1.69">Et lorsque vint la nuit ramener sa grande ombre,</l>
						<l n="70" num="1.70">Où scintille Phœbé, sœur des astres sans nombre,</l>
						<l n="71" num="1.71">Au sein des flots troublés et grossis de ses pleurs,</l>
						<l n="72" num="1.72">Triste, elle disparut en arrachant des fleurs.</l>
					</lg>
					<closer>
						<dateline>
							<date when="1842">juillet 1842</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>