<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">OCCIDENTALES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2572 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_13</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">OCCIDENTALES</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="1998">1998</date>
								</imprint>
								<biblScope unit="tome">V</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre, éditeur</publisher>
							<date when="1891">1889-1891</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1875">1875</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-03-13" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN555">
				<head type="main">Leroy s’amuse</head>
				<lg n="1">
					<l n="1" num="1.1">Le soleil continue à tout chauffer à blanc.</l>
					<l n="2" num="1.2"><space quantity="8" unit="char"></space> Du fond de sa rouge fournaise</l>
					<l n="3" num="1.3">Il nous vise, et chacun de nous emporte au flanc</l>
					<l n="4" num="1.4"><space quantity="8" unit="char"></space> Une de ses flèches de braise.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Plus cruel que Néron et que Domitien,</l>
					<l n="6" num="2.2"><space quantity="8" unit="char"></space> Pour griller ce que nous aimâmes,</l>
					<l n="7" num="2.3">Ce bourreau sur son front d’académicien</l>
					<l n="8" num="2.4"><space quantity="8" unit="char"></space> Met une perruque de flammes !</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Ah ! pour le supporter, ce dur soleil roussi,</l>
					<l n="10" num="3.2"><space quantity="8" unit="char"></space> Qui, desséchant les jouvencelles,</l>
					<l n="11" num="3.3">Nous met sa torche aux yeux, et qui nous fait aussi</l>
					<l n="12" num="3.4"><space quantity="8" unit="char"></space> Manger des gerbes d’étincelles,</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">Il faudrait être enfin plus doux que Babylas</l>
					<l n="14" num="4.2"><space quantity="8" unit="char"></space> Et plus patient qu’Athanase,</l>
					<l n="15" num="4.3">Car il nous a, pendant ces jours derniers, hélas !</l>
					<l n="16" num="4.4"><space quantity="8" unit="char"></space> Dévoré même le Gymnase !</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1">On y meurt tout de bon : la feuille de vigne y</l>
					<l n="18" num="5.2"><space quantity="8" unit="char"></space> Semblerait trop chaude, ô mon Ode !</l>
					<l n="19" num="5.3">Et tous les spectateurs de monsieur Montigny</l>
					<l n="20" num="5.4"><space quantity="8" unit="char"></space> Sont changés en bœuf à la mode.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1">Voyant cela, l’auteur de Chemin retrouvé,</l>
					<l n="22" num="6.2"><space quantity="8" unit="char"></space> Pâle et debout contre un pilastre</l>
					<l n="23" num="6.3">De ce théâtre si rudement éprouvé,</l>
					<l n="24" num="6.4"><space quantity="8" unit="char"></space> Fit ce petit discours à l’Astre :</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1">O Phébus-Apollon ! photographe changeant</l>
					<l n="26" num="7.2"><space quantity="8" unit="char"></space> Qui vient laper l’eau dans les auges</l>
					<l n="27" num="7.3">Et qui nous romps le crâne avec ton arc d’argent,</l>
					<l n="28" num="7.4"><space quantity="8" unit="char"></space> Tu n’es qu’un franc-tireur des Vosges !</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1">Ah ! montreur de seins nus qui fais le Richelieu !</l>
					<l n="30" num="8.2"><space quantity="8" unit="char"></space> Coiffeur qui poudres cette ville !</l>
					<l n="31" num="8.3">Joueur de violon et de lyre ! vieux dieu</l>
					<l n="32" num="8.4"><space quantity="8" unit="char"></space> Bon pour Ménard et pour Banville !</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1">Comment ! Régnier et moi, nous donnons, vieil archer,</l>
					<l n="34" num="9.2"><space quantity="8" unit="char"></space> — Tranchons le mot, — un pur chef-d’œuvre ;</l>
					<l n="35" num="9.3">Et toi, rose et brûlant, tu viens nous le lécher</l>
					<l n="36" num="9.4"><space quantity="8" unit="char"></space> Avec tes langues de couleuvre !</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1">Pour notre bonbonnière abandonnant les cieux,</l>
					<l n="38" num="10.2"><space quantity="8" unit="char"></space> Parmi nos loges tu t’installes,</l>
					<l n="39" num="10.3">Et tu viens cuire à point les crânes des messieurs</l>
					<l n="40" num="10.4"><space quantity="8" unit="char"></space> Qui se sont assis dans les stalles !</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1">Même jeu sur la scène. On voit que les pompiers,</l>
					<l n="42" num="11.2"><space quantity="8" unit="char"></space> Incendiés par tes extases,</l>
					<l n="43" num="11.3">Entrent en fusion et coulent à nos pieds :</l>
					<l n="44" num="11.4"><space quantity="8" unit="char"></space> On pourrait en faire des vases !</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1">Tu changes en charbons le riche lampas qu’a</l>
					<l n="46" num="12.2"><space quantity="8" unit="char"></space> Drapé mon directeur artiste,</l>
					<l n="47" num="12.3">Et, grâce à toi, le front de madame Pasca</l>
					<l n="48" num="12.4"><space quantity="8" unit="char"></space> S’enflamme comme une améthyste !</l>
				</lg>
				<lg n="13">
					<l n="49" num="13.1">Tu grilles sans pitié Massin, dont la chanson</l>
					<l n="50" num="13.2"><space quantity="8" unit="char"></space> Vaut bien mieux que celle d’un merle,</l>
					<l n="51" num="13.3">Et tu fonds lâchement Béatrice Pierson,</l>
					<l n="52" num="13.4"><space quantity="8" unit="char"></space> Comme Cléopâtre sa perle !</l>
				</lg>
				<lg n="14">
					<l n="53" num="14.1">La pauvre Mélanie a des feux sur ses doigts :</l>
					<l n="54" num="14.2"><space quantity="8" unit="char"></space> Berton s’efface dans la brume,</l>
					<l n="55" num="14.3">Villeray s’amincit comme un fil, et je vois</l>
					<l n="56" num="14.4"><space quantity="8" unit="char"></space> A l’horizon Landrol qui fume !</l>
				</lg>
				<lg n="15">
					<l n="57" num="15.1">Soleil, moi, vieux lion blanchi sous le harnois,</l>
					<l n="58" num="15.2"><space quantity="8" unit="char"></space> Crois-tu vraiment que je m’amuse</l>
					<l n="59" num="15.3">De te voir envoyer du monde à Cressonnois ?</l>
					<l n="60" num="15.4"><space quantity="8" unit="char"></space> Va-t’en ! laisse en repos ma Muse,</l>
				</lg>
				<lg n="16">
					<l n="61" num="16.1">Ou, s’il fallait encor que ton bras assénât</l>
					<l n="62" num="16.2"><space quantity="8" unit="char"></space> Des coups sur cette fiancée,</l>
					<l n="63" num="16.3">Tremble, je te ferai flétrir en plein Sénat,</l>
					<l n="64" num="16.4"><space quantity="8" unit="char"></space> Comme on a fait pour monsieur Sée !</l>
				</lg>
				<lg n="17">
					<l n="65" num="17.1">C’est ainsi que Leroy, farouche, et par instants</l>
					<l n="66" num="17.2"><space quantity="8" unit="char"></space> De son pied tourmentant la plinthe</l>
					<l n="67" num="17.3">Du corridor, parlait au soleil du printemps</l>
					<l n="68" num="17.4"><space quantity="8" unit="char"></space> Et l’assourdissait de sa plainte.</l>
				</lg>
				<lg n="18">
					<l n="69" num="18.1">Pourtant des spectateurs fort nombreux se montraient</l>
					<l n="70" num="18.2"><space quantity="8" unit="char"></space> Au contrôle, — tous grillés comme</l>
					<l n="71" num="18.3">Des biftecks. Ils entraient brûlés, mais ils entraient.</l>
					<l n="72" num="18.4"><space quantity="8" unit="char"></space> Ils versaient une forte somme ;</l>
				</lg>
				<lg n="19">
					<l n="73" num="19.1">Et notre auteur, avec des sourires charmants,</l>
					<l n="74" num="19.2"><space quantity="8" unit="char"></space> Regardait parmi l’incendie</l>
					<l n="75" num="19.3">Ces tisons à demi consumés, et fumants,</l>
					<l n="76" num="19.4"><space quantity="8" unit="char"></space> Qui venaient voir la comédie !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1868">Juin 1868.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>