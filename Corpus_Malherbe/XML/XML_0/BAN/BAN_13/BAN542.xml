<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">OCCIDENTALES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2572 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_13</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">OCCIDENTALES</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="1998">1998</date>
								</imprint>
								<biblScope unit="tome">V</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre, éditeur</publisher>
							<date when="1891">1889-1891</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1875">1875</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-03-13" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN542">
				<head type="main">Pièces féeries</head>
				<lg n="1">
					<l n="1" num="1.1">Molière, j’ai voulu savoir ce que devient</l>
					<l n="2" num="1.2"><space quantity="8" unit="char"></space>Ton beau rire folâtre,</l>
					<l n="3" num="1.3">Et, pour avoir raison du doute qui me tient,</l>
					<l n="4" num="1.4"><space quantity="8" unit="char"></space>J’entre dans un théâtre.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Un aquarium. Bon. Je vois les dos connus</l>
					<l n="6" num="2.2"><space quantity="8" unit="char"></space>De cinquante ingénues.</l>
					<l n="7" num="2.3">Que de bras nus ! que de seins nus ! que de cous nus !</l>
					<l n="8" num="2.4"><space quantity="8" unit="char"></space>Oh ! que de choses nues !</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Sur quels objets hideux, maigres, flasques et lourds,</l>
					<l n="10" num="3.2"><space quantity="8" unit="char"></space>Lumière, tu te joues !</l>
					<l n="11" num="3.3">Que de croupes, offrant aux regards des contours</l>
					<l n="12" num="3.4"><space quantity="8" unit="char"></space>Horribles ! que de joues !</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">Wateau, qu’en dites-vous ? Qu’en dites-vous, Boucher ?</l>
					<l n="14" num="4.2"><space quantity="8" unit="char"></space>Bien que leur bouche rie,</l>
					<l n="15" num="4.3">On pense voir ces chairs mortes que le boucher</l>
					<l n="16" num="4.4"><space quantity="8" unit="char"></space>Vend à la boucherie.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1">Spectacles écœurants ! Tristes panoramas !</l>
					<l n="18" num="5.2"><space quantity="8" unit="char"></space>Vous fuyez, Muses blanches,</l>
					<l n="19" num="5.3">Vers l’invincible azur, en voyant ces amas</l>
					<l n="20" num="5.4"><space quantity="8" unit="char"></space>De poitrines, d’éclanches,</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1">Et ces ventres hideux, ballonnés par les ans,</l>
					<l n="22" num="6.2"><space quantity="8" unit="char"></space>Qu’on a, masse vermeille,</l>
					<l n="23" num="6.3">Ficelés avec soin dans des maillots luisants,</l>
					<l n="24" num="6.4"><space quantity="8" unit="char"></space>Teints en couleur groseille.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1">Une Javotte, nue et longue comme un ver,</l>
					<l n="26" num="7.2"><space quantity="8" unit="char"></space>Traîne, être chimérique,</l>
					<l n="27" num="7.3">Un vieux manteau de cour, baigné par un éclair</l>
					<l n="28" num="7.4"><space quantity="8" unit="char"></space>De lumière électrique,</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1">Et glapit. Oui, ce tas de cuisses, de chignons,</l>
					<l n="30" num="8.2"><space quantity="8" unit="char"></space>Si bien fait pour se taire,</l>
					<l n="31" num="8.3">Hurle, miaule et roucoule avec des airs mignons,</l>
					<l n="32" num="8.4"><space quantity="8" unit="char"></space>Et chansonne… Voltaire !</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1">O cotonnier ! pour qui rugirent en effet</l>
					<l n="34" num="9.2"><space quantity="8" unit="char"></space>Tant de combats épiques,</l>
					<l n="35" num="9.3">Arbuste précieux, toi que le soleil fait</l>
					<l n="36" num="9.4"><space quantity="8" unit="char"></space>Grandir sous les tropiques ;</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1">Et vous, Hostein ! et vous, Marc Fournier, qui du doigt</l>
					<l n="38" num="10.2"><space quantity="8" unit="char"></space>Chassez les belles proses !</l>
					<l n="39" num="10.3">Régnez, soyez heureux, c’est à vous que l’on doit</l>
					<l n="40" num="10.4"><space quantity="8" unit="char"></space>Ces grosses dames roses !</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1">Naguère on avait dit aux marchands de succès :</l>
					<l n="42" num="11.2"><space quantity="8" unit="char"></space> Pour nous ôter La Biche,</l>
					<l n="43" num="11.3">Dites, que voulez-vous, ô directeurs français ?</l>
					<l n="44" num="11.4"><space quantity="8" unit="char"></space>La gaieté de Labiche ?</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1">La voici. Voulez-vous, pour vous réfugier</l>
					<l n="46" num="12.2"><space quantity="8" unit="char"></space>Dans la pensée altière,</l>
					<l n="47" num="12.3">La verve de Sardou, l’esprit vivant d’Augier,</l>
					<l n="48" num="12.4"><space quantity="8" unit="char"></space>La fureur de Barrière,</l>
				</lg>
				<lg n="13">
					<l n="49" num="13.1">Ou ces drames poignants dans lesquels Dumas fils,</l>
					<l n="50" num="13.2"><space quantity="8" unit="char"></space>De sa main ferme et sûre,</l>
					<l n="51" num="13.3">Montre, ouverte et saignant sous une chair de lys,</l>
					<l n="52" num="13.4"><space quantity="8" unit="char"></space>Quelque affreuse blessure ?</l>
				</lg>
				<lg n="14">
					<l n="53" num="14.1">Mais nos bons directeurs, vieux troupeau coutumier</l>
					<l n="54" num="14.2"><space quantity="8" unit="char"></space>De cette réprimande,</l>
					<l n="55" num="14.3">Ont répondu, pareils à l’enfant de Daumier :</l>
					<l n="56" num="14.4"><space quantity="8" unit="char"></space> J’aime mieux de la viande !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1868">Janvier 1868.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>