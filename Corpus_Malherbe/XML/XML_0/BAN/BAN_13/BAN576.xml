<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">OCCIDENTALES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2572 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_13</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">OCCIDENTALES</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="1998">1998</date>
								</imprint>
								<biblScope unit="tome">V</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre, éditeur</publisher>
							<date when="1891">1889-1891</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1875">1875</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-03-13" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN576">
				<head type="main">Ancien Pierrot</head>
				<lg n="1">
					<l n="1" num="1.1"><space quantity="2" unit="char"></space>Hommes hideux, et vous dont Amour fait sa gloire,</l>
					<l n="2" num="1.2">Femmes ! je vous dirai ma déplorable histoire.</l>
					<l n="3" num="1.3">J’étais Pierrot. — Comment ! Pierrot ? — Mais oui, Pierrot.</l>
					<l n="4" num="1.4">J’étais Pierrot. Voler au rôtisseur son rôt,</l>
					<l n="5" num="1.5">Dérober des poissons aux dames de la Halle</l>
					<l n="6" num="1.6">Tout en les fascinant d’un œil tragique et pâle,</l>
					<l n="7" num="1.7">Boire, manger, dormir, tels étaient mes destins,</l>
					<l n="8" num="1.8">Et je goûtais l’ivresse énorme des festins !</l>
					<l n="9" num="1.9"><space quantity="2" unit="char"></space>Plus blanc que l’avalanche et que l’aile des cygnes,</l>
					<l n="10" num="1.10">J’étais spirituel et je parlais par signes.</l>
					<l n="11" num="1.11">Avec mon maître, vieux et sinistre coquin,</l>
					<l n="12" num="1.12">Nous poursuivions dans les campagnes Arlequin</l>
					<l n="13" num="1.13">Et sa délicieuse amante Colombine.</l>
					<l n="14" num="1.14">Mais dès que je levais contre eux ma carabine,</l>
					<l n="15" num="1.15">Sur un fleuve brillant comme le diamant</l>
					<l n="16" num="1.16">Ils s’enfuyaient dans des nefs d’or. C’était charmant.</l>
					<l n="17" num="1.17"><space quantity="2" unit="char"></space>Nous nous rencontrions parfois. Moins doux qu’Arbate,</l>
					<l n="18" num="1.18">J’assommais Arlequin avec sa propre batte.</l>
					<l n="19" num="1.19">Colombine, fuyant la cage et le réseau,</l>
					<l n="20" num="1.20">M’effleurait, en son vol tremblant, comme un oiseau ;</l>
					<l n="21" num="1.21">Je prodiguais, parmi les cris et les tumultes,</l>
					<l n="22" num="1.22">A Cassandre ébloui, des coups de pied occultes ;</l>
					<l n="23" num="1.23">Je riais, et la fée Azurine parfois,</l>
					<l n="24" num="1.24">A l’heure où le soleil teint de pourpre les bois,</l>
					<l n="25" num="1.25">Faisait jaillir pour moi, parmi les fleurs écloses,</l>
					<l n="26" num="1.26">Des pâtés de lapin dans les buissons de roses !</l>
					<l n="27" num="1.27"><space quantity="2" unit="char"></space>Oh ! la fée Azurine ! Un jour, — ô mon pinceau,</l>
					<l n="28" num="1.28">Reste chaste ! — sur l’herbe, auprès d’un clair ruisseau,</l>
					<l n="29" num="1.29">Je la surpris dormant, sa poitrine de neige</l>
					<l n="30" num="1.30">A découvert. J’étais Pierrot. Que vous dirai-je ?</l>
					<l n="31" num="1.31">Sur ces lys, — un malheur est si vite arrivé !</l>
					<l n="32" num="1.32">Je mis ma lèvre, hélas ! Puis je récidivai,</l>
					<l n="33" num="1.33">Trois fois. J’étais Pierrot. Mais la fée adorable</l>
					<l n="34" num="1.34">S’éveilla toute rouge, et me dit : Misérable,</l>
					<l n="35" num="1.35">Deviens homme ! Aussitôt, — prodige horrible à voir !</l>
					<l n="36" num="1.36">Je sentis sur mon dos pousser un habit noir.</l>
					<l n="37" num="1.37">Comme si j’eusse été Français, Tartare ou Kurde,</l>
					<l n="38" num="1.38">Il me vint des cheveux, cette parure absurde ;</l>
					<l n="39" num="1.39">Sur mon front je sentis passer le badigeon</l>
					<l n="40" num="1.40">Qui rougit l’écrevisse, et comme le pigeon</l>
					<l n="41" num="1.41">Qui chante lorsqu’il frit dans une casserole,</l>
					<l n="42" num="1.42">J’eus cette infirmité stupide, la parole.</l>
					<l n="43" num="1.43"><space quantity="2" unit="char"></space>Oui, je parle à présent. Je fume des londrès.</l>
					<l n="44" num="1.44">Tout comme Bossuet et comme Gil Pérès,</l>
					<l n="45" num="1.45">J’ai des transitions plus grosses que des câbles,</l>
					<l n="46" num="1.46">Et je dis ma pensée au moyen des vocables.</l>
					<l n="47" num="1.47">Tels s’enfuirent ma joie et mon bonheur perdu.</l>
					<l n="48" num="1.48"><space quantity="2" unit="char"></space>Mais, dis-je à la cruelle Azurine, éperdu,</l>
					<l n="49" num="1.49">Souffrirai-je longtemps cette angoisse mortelle ?</l>
					<l n="50" num="1.50">Redeviendrai-je pas Pierrot ? — Si, me dit-elle.</l>
					<l n="51" num="1.51">Je ne veux pas la mort du pécheur. Quand les vers</l>
					<l n="52" num="1.52">Se vendront ; quand, disant : Les raisins sont trop verts !</l>
					<l n="53" num="1.53">Le baron de Rothschild, abandonnant le mythe</l>
					<l n="54" num="1.54">De l’or, embrassera la carrière d’ermite ;</l>
					<l n="55" num="1.55">Lorsque les fabuleux académiciens</l>
					<l n="56" num="1.56">Ne mettront plus d’abat-jour verts ; quand les anciens</l>
					<l n="57" num="1.57">Romantiques, trouvant Hernani par trop raide,</l>
					<l n="58" num="1.58">Pâmeront de bonheur sur les vers de Tancrède ;</l>
					<l n="59" num="1.59">Quand on ne verra plus, chez les Turcs, le vizir</l>
					<l n="60" num="1.60">Étrangler des sultans ; quand, suivant sans plaisir</l>
					<l n="61" num="1.61">Les nymphes aux cheveux maïs, faisant fi d’elles,</l>
					<l n="62" num="1.62">Tous les maris seront à leurs femmes fidèles ;</l>
					<l n="63" num="1.63">Quand la flûte prendra la place des tambours ;</l>
					<l n="64" num="1.64">Lorsque enfin les bourgeois, ces habitants des bourgs</l>
					<l n="65" num="1.65">Qui, dans l’Espagne en feu comme dans le Hanovre,</l>
					<l n="66" num="1.66">Furent extasiés par Le Convoi du Pauvre,</l>
					<l n="67" num="1.67">Aimeront Delacroix et les ciels de Corot,</l>
					<l n="68" num="1.68">Toi, tu redeviendras Pierrot. — Grands dieux ! Pierrot !</l>
					<l n="69" num="1.69">Je serai de nouveau Pierrot, fée Azurine !</l>
					<l n="70" num="1.70">Criai-je ; et cette fois, au lieu de sa poitrine</l>
					<l n="71" num="1.71">Je baisai sa chaussure, et mis ma lèvre sur</l>
					<l n="72" num="1.72">Le pan resplendissant de sa robe d’azur !</l>
					<l n="73" num="1.73"><space quantity="2" unit="char"></space>A présent, me voilà rassuré. Plus de chutes.</l>
					<l n="74" num="1.74">Les soldats voudront bien marcher au son des flûtes.</l>
					<l n="75" num="1.75">Pourquoi pas ? Tout va bien. Je sens pâlir ma chair.</l>
					<l n="76" num="1.76">Les vers, à ce qu’on dit, vont se vendre très cher</l>
					<l n="77" num="1.77">Dans trois jours. Le baron de Rothschild, je l’accorde,</l>
					<l n="78" num="1.78">N’a pas encore pris la bure et ceint la corde ;</l>
					<l n="79" num="1.79">Mais nous avons tous nos projets. Il a les siens.</l>
					<l n="80" num="1.80">Nos seigneurs, messieurs les académiciens,</l>
					<l n="81" num="1.81">Pareils à de vieux Dieux dans leur caverne noire,</l>
					<l n="82" num="1.82">Ornent encor d’abat-jour verts leurs fronts d’ivoire ;</l>
					<l n="83" num="1.83">Mais on doit en nommer de jeunes ce mois-ci.</l>
					<l n="84" num="1.84">Les romantiques, peuple en sa faute endurci,</l>
					<l n="85" num="1.85">Jusqu’ici ne sont pas accourus à notre aide ;</l>
					<l n="86" num="1.86">Mais ils diront bientôt : La flamme est dans Tancrède,</l>
					<l n="87" num="1.87">Et quant à Hernani, ce n’est qu’un feu grégeois.</l>
					<l n="88" num="1.88">Delacroix et Corot prennent chez les bourgeois,</l>
					<l n="89" num="1.89">Positivement. L’art dans leurs locaux motive</l>
					<l n="90" num="1.90">Les éclairs du Progrès, cette locomotive.</l>
					<l n="91" num="1.91">Les cocottes, Souris, Chiffonnette et Laïs,</l>
					<l n="92" num="1.92">Renoncent aux cheveux beurre frais et maïs ;</l>
					<l n="93" num="1.93">Depuis lors, moins friands de leurs épithalames,</l>
					<l n="94" num="1.94">Beaucoup de maris sont fidèles à leurs femmes.</l>
					<l n="95" num="1.95">Donc, en dépit du mal que m’a fait l’archerot</l>
					<l n="96" num="1.96">Amour, je vais bientôt redevenir Pierrot !</l>
					<l n="97" num="1.97"><space quantity="2" unit="char"></space>O mes aïeux ! ce noir habit va disparaître</l>
					<l n="98" num="1.98">De mon dos frémissant ; de nouveau je vais être</l>
					<l n="99" num="1.99">Muet comme une carpe, et je ferai des sauts</l>
					<l n="100" num="1.100">De carpe également, pour étonner les sots.</l>
					<l n="101" num="1.101">Oui, ta prédiction s’accomplit, Azurine !</l>
					<l n="102" num="1.102">Mon teint moins agité prend des tons de farine ;</l>
					<l n="103" num="1.103">Je suis comme tous les ténors, je perds ma voix ;</l>
					<l n="104" num="1.104">Et je ris déjà comme un bossu, quand je vois</l>
					<l n="105" num="1.105">Pâlir mon nez, pareil à celui de la lune.</l>
					<l n="106" num="1.106">Les femmes accourront. — Qu’il est beau ! dira l’une,</l>
					<l n="107" num="1.107">Et j’aurai des effets de neige sur mon front.</l>
					<l n="108" num="1.108">Et lorsque les petits enfants apercevront</l>
					<l n="109" num="1.109">Mon visage embelli d’une blancheur suprême,</l>
					<l n="110" num="1.110">Ils diront : J’en veux. C’est de la tarte à la crème !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1857">Janvier 1857.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>