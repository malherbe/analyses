<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">OCCIDENTALES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2572 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_13</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">OCCIDENTALES</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="1998">1998</date>
								</imprint>
								<biblScope unit="tome">V</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre, éditeur</publisher>
							<date when="1891">1889-1891</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1875">1875</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-03-13" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN556">
				<head type="main">Et Tartuffe ?</head>
				<lg n="1">
					<l n="1" num="1.1">Adam vante et chérit son paradis natal</l>
					<l n="2" num="1.2"><space quantity="8" unit="char"></space> Où, joyeuse et libératrice,</l>
					<l n="3" num="1.3">Dans les Édens baignés par des flots de cristal</l>
					<l n="4" num="1.4"><space quantity="8" unit="char"></space> La vigne est sa mâle nourrice.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Et Tartuffe ? — Il nous dit, entre deux oremus,</l>
					<l n="6" num="2.2">Que pour tout bon Français la patrie est à Rome,</l>
					<l n="7" num="2.3">Et qu’ayant pour aïeux Romulus et Rémus,</l>
					<l n="8" num="2.4">Nous téterons la louve à jamais. — Le pauvre homme !</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Adam, qui veut chasser de son riant jardin</l>
					<l n="10" num="3.2"><space quantity="8" unit="char"></space> La Haine impure, ce reptile,</l>
					<l n="11" num="3.3">Aime un langage clair, et garde son dédain</l>
					<l n="12" num="3.4"><space quantity="8" unit="char"></space> Pour la polémique inutile.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">Et Tartuffe ? — Il écrit des pamphlets, des amas</l>
					<l n="14" num="4.2">De brochures, des tas de discours. Il consomme</l>
					<l n="15" num="4.3">Deux fois plus de papier qu’Alexandre Dumas</l>
					<l n="16" num="4.4">Et même que Ponson du Terrail. — Le pauvre homme !</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1">Adam, toujours épris de l’antique Beauté,</l>
					<l n="18" num="5.2"><space quantity="8" unit="char"></space> Pour se guérir de tant d’épreuves</l>
					<l n="19" num="5.3">Demande, haletant, la force et la santé</l>
					<l n="20" num="5.4"><space quantity="8" unit="char"></space> Au flot mystérieux des fleuves.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1">Et Tartuffe ? — Jamais il n’a que des refus</l>
					<l n="22" num="6.2">Pour la pauvre naïade. Il craint l’eau froide, comme</l>
					<l n="23" num="6.3">Le bienheureux saint Labre, et ses cheveux touffus</l>
					<l n="24" num="6.4">Sont vierges des baisers du peigne. — Le pauvre homme !</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1">Adam veut que sa fille au front pur, son trésor,</l>
					<l n="26" num="7.2"><space quantity="8" unit="char"></space> Sous le noir sanglot des huées</l>
					<l n="27" num="7.3">Ne porte pas la pourpre et les étoffes d’or,</l>
					<l n="28" num="7.4"><space quantity="8" unit="char"></space> Ces haillons des prostituées.</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1">Et Tartuffe ? — Blessé par des yeux vert-de-mer,</l>
					<l n="30" num="8.2">Avec une Ève en fleur il mordille la pomme,</l>
					<l n="31" num="8.3">Et, tout en répétant : Craignez le fruit amer,</l>
					<l n="32" num="8.4">Il vous le croque avec délices. — Le pauvre homme !</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1">Adam, pour mettre un coq à la place d’un lys,</l>
					<l n="34" num="9.2"><space quantity="8" unit="char"></space> Ne veut plus imiter Xaintrailles ;</l>
					<l n="35" num="9.3">Il appelle à grands cris le jour où tous ses fils</l>
					<l n="36" num="9.4"><space quantity="8" unit="char"></space> Ne seront plus chair à mitrailles.</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1">Et Tartuffe ? — Il prétend qu’on acquitte l’impôt</l>
					<l n="38" num="10.2">Du sang. Et si quelqu’un dit : Tue ! il crie : Assomme !</l>
					<l n="39" num="10.3">Ses prédilections sont pour saint Chassepot,</l>
					<l n="40" num="10.4">Pour saint Bonnin et pour saint Dreyse. — Le pauvre homme !</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1">Adam, victorieux du passé triste et vain,</l>
					<l n="42" num="11.2"><space quantity="8" unit="char"></space> Regarde sans terreur les voiles</l>
					<l n="43" num="11.3">De l’insondable azur, où le berger divin</l>
					<l n="44" num="11.4"><space quantity="8" unit="char"></space> Mène ses grands troupeaux d’étoiles.</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1">Et Tartuffe ? — Il nous dit : Les astres, les soleils,</l>
					<l n="46" num="12.2">Les comètes, cela regarde l’astronome.</l>
					<l n="47" num="12.3">Moi, ce que j’aperçois au fond des cieux vermeils,</l>
					<l n="48" num="12.4">C’est un vengeur, un dieu féroce. — Le pauvre homme !</l>
				</lg>
				<lg n="13">
					<l n="49" num="13.1">Raison ! divinité sereine, qu’à genoux</l>
					<l n="50" num="13.2"><space quantity="8" unit="char"></space> Diderot proclama naguère,</l>
					<l n="51" num="13.3">Parle ! protège-nous ! entends-nous ! sauve-nous !</l>
					<l n="52" num="13.4"><space quantity="8" unit="char"></space> Détruis la Bêtise et la Guerre !</l>
				</lg>
				<lg n="14">
					<l n="53" num="14.1">Sauve Marco, la stryge aux yeux froids et hautains ;</l>
					<l n="54" num="14.2">Sauve Shahabaam, sauve monsieur Prudhomme ;</l>
					<l n="55" num="14.3">Sauve les idiots, sauve les philistins</l>
					<l n="56" num="14.4">Et les envieux, — et Tartuffe, le pauvre homme !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1868">Juin 1868.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>