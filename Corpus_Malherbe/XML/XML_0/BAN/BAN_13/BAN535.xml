<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">OCCIDENTALES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2572 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_13</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">OCCIDENTALES</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="1998">1998</date>
								</imprint>
								<biblScope unit="tome">V</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre, éditeur</publisher>
							<date when="1891">1889-1891</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1875">1875</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-03-13" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN535">
				<head type="main">Courbet, seconde manière</head>
				<lg n="1">
					<l n="1" num="1.1">Réalisme, oripeau démodé, vieille enseigne,</l>
					<l n="2" num="1.2">Tu n’as plus ce héros qui te rafistolait.</l>
					<l n="3" num="1.3">Il faut te dire adieu, quoique mon cœur en saigne :</l>
					<l n="4" num="1.4">Courbet ne tire plus de coups de pistolet.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Il est sage à présent : c’en est fait des caprices</l>
					<l n="6" num="2.2">Étranges et bouffons que ce réaliste eut.</l>
					<l n="7" num="2.3">Succès ! il était temps enfin que tu le prisses,</l>
					<l n="8" num="2.4">Et je vois devant lui se dresser l’Institut.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">C’en est fait des lutteurs dont la chair était bleue,</l>
					<l n="10" num="3.2">Des nez extravagants, des yeux à demi ronds !</l>
					<l n="11" num="3.3">Courbet transfiguré ne coupe plus la queue</l>
					<l n="12" num="3.4">De ses chiens. Il n’est plus qu’admirable. Admirons.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">Ses tableaux, attaqués avec un zèle habile,</l>
					<l n="14" num="4.2">Qu’on ne voyait jadis que dans Ornans, ornant</l>
					<l n="15" num="4.3">Les salons bourgeois, ont enfin usé la bile</l>
					<l n="16" num="4.4">Des vingt critiques d’art, qui vont le flagornant !</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1">Au temple de la Gloire il vient, un dieu le porte.</l>
					<l n="18" num="5.2">Gautier devant ses pas s’incline, et Pelloquet</l>
					<l n="19" num="5.3">Rayonnant et pensif lui dit : Voici la porte !</l>
					<l n="20" num="5.4">Et Saint-Victor s’apprête à tourner le loquet.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1">C’est justice, et Courbet s’en va dans la verdure,</l>
					<l n="22" num="6.2">Ivre de l’air salubre et du chant des bouvreuils.</l>
					<l n="23" num="6.3">Il a violemment épousé la Nature</l>
					<l n="24" num="6.4">Au fond d’un bois, dans la remise des chevreuils.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1">Printemps luxurieux dont Avril fait la couche,</l>
					<l n="26" num="7.2">O printemps verdoyant, c’est toi qui les ombras,</l>
					<l n="27" num="7.3">Les rochers où dormait cette Reine farouche :</l>
					<l n="28" num="7.4">Courbet sans dire un mot l’empoigna dans ses bras.</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1">C’est en vain qu’éveillée en sursaut, cette Nymphe</l>
					<l n="30" num="8.2">Cacha de ses deux mains son corps puissant et doux</l>
					<l n="31" num="8.3">Où le sang est bien plus abondant que la lymphe,</l>
					<l n="32" num="8.4">Et lui cria : Monsieur, pour qui me prenez-vous ?</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1">Car le maître d’Ornans l’emporta dans son aire,</l>
					<l n="34" num="9.2">Et, fougueux, lui ferma la bouche ardente avec</l>
					<l n="35" num="9.3">Un baiser appuyé comme un coup de tonnerre,</l>
					<l n="36" num="9.4">En lui disant tout bas : Va te plaindre à l’art grec !</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1">Voilà comment les gens qui ne sont pas timides</l>
					<l n="38" num="10.2">Savent mener à bien leurs affaires de cœur.</l>
					<l n="39" num="10.3">Or, la Nymphe, en rouvrant ses yeux d’amour humides,</l>
					<l n="40" num="10.4">Dit au paysagiste heureux : O mon vainqueur !</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1">O mon roi ! tu m’as fait une cour un peu vive,</l>
					<l n="42" num="11.2">Mais j’aime la franchise, et je ne t’en veux plus !</l>
					<l n="43" num="11.3">Prends mes ruisseaux dormants sous la grotte pensive,</l>
					<l n="44" num="11.4">Prends tout ! prends mes rochers et mes bois chevelus !</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1">C’est ainsi que le maître a fait ce paysage</l>
					<l n="46" num="12.2">Où, sous la frondaison murmurante des bois</l>
					<l n="47" num="12.3">Dont la masse frémit dans l’air comme un visage,</l>
					<l n="48" num="12.4">Frissonne ce ruisseau, si vivant que j’y bois !</l>
				</lg>
				<lg n="13">
					<l n="49" num="13.1">Et puisque sa peinture est vraiment si bien mise</l>
					<l n="50" num="13.2">Dans ce chef-d’œuvre clair, ouvré comme un bijou,</l>
					<l n="51" num="13.3">Ma foi ! pardonnons-lui sa femme sans chemise,</l>
					<l n="52" num="13.4">Dont les cheveux sont faits de copeaux d’acajou !</l>
				</lg>
				<lg n="14">
					<l n="53" num="14.1">Car ce puissant génie ailé qui se déploie</l>
					<l n="54" num="14.2">En liberté, parfois a ses licences, mais</l>
					<l n="55" num="14.3">Se trompe encore avec une robuste joie,</l>
					<l n="56" num="14.4">Et ceux qui ne font rien ne se trompent jamais !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1866">Mai 1866.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>