<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">OCCIDENTALES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2572 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_13</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">OCCIDENTALES</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="1998">1998</date>
								</imprint>
								<biblScope unit="tome">V</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre, éditeur</publisher>
							<date when="1891">1889-1891</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1875">1875</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-03-13" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN568">
				<head type="main">La Mitrailleuse</head>
				<div type="section" n="1">
					<head type="number">I</head>
					<lg n="1">
						<l n="1" num="1.1">La Mitrailleuse, un nom charmant ! J’y veux songer.</l>
						<l n="2" num="1.2"><space quantity="8" unit="char"></space>Elle est d’une bonne syntaxe ;</l>
						<l n="3" num="1.3">J’aime sa tabatière et son affût léger,</l>
						<l n="4" num="1.4"><space quantity="8" unit="char"></space>Ses canons tournant sur un axe,</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">Jolis petits canons, étroitement unis,</l>
						<l n="6" num="2.2"><space quantity="8" unit="char"></space>Sa batterie en féronnière</l>
						<l n="7" num="2.3">Et son récipient à cartouches, munis</l>
						<l n="8" num="2.4"><space quantity="8" unit="char"></space>Chacun d’un couvercle à charnière !</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">La chose est dans sa boîte, et, pour charmer nos yeux,</l>
						<l n="10" num="3.2"><space quantity="8" unit="char"></space>Se manœuvre, (on me le révèle,)</l>
						<l n="11" num="3.3">O Barbarie, ainsi que ton orgue joyeux,</l>
						<l n="12" num="3.4"><space quantity="8" unit="char"></space>En tournant une manivelle ;</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">Grâce à quoi dragons verts, cuirassiers, fusiliers,</l>
						<l n="14" num="4.2"><space quantity="8" unit="char"></space>Déchus de leur beauté physique,</l>
						<l n="15" num="4.3">Tous, par douzaines, par centaines, par milliers</l>
						<l n="16" num="4.4"><space quantity="8" unit="char"></space>Seront foudroyés en musique.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">Un enfant y suffit ; alors, dans un éclair,</l>
						<l n="18" num="5.2"><space quantity="8" unit="char"></space>Notre chair sous le plomb féroce</l>
						<l n="19" num="5.3">Volera par lambeaux ensanglantés, sur l’air</l>
						<l n="20" num="5.4"><space quantity="8" unit="char"></space>Allez-vous-en, gens de la noce !</l>
					</lg>
				</div>
				<div type="section" n="2">
					<head type="number">II</head>
					<lg n="1">
						<l n="21" num="1.1">O mères ! qui, riant au baiser de vos fils,</l>
						<l n="22" num="1.2"><space quantity="8" unit="char"></space>Oubliez l’amère souffrance</l>
						<l n="23" num="1.3">Et portez suspendus à votre sein de lys</l>
						<l n="24" num="1.4"><space quantity="8" unit="char"></space>Ces beaux enfants, fleurs de la France ;</l>
					</lg>
					<lg n="2">
						<l n="25" num="2.1">Ne vous obstinez pas, ô mères que le jour</l>
						<l n="26" num="2.2"><space quantity="8" unit="char"></space>Baigne de sa clarté subtile,</l>
						<l n="27" num="2.3">A les nourrir ainsi du lait de votre amour ;</l>
						<l n="28" num="2.4"><space quantity="8" unit="char"></space>Cessez une lutte inutile.</l>
					</lg>
					<lg n="3">
						<l n="29" num="3.1">Tandis que votre lait abreuve un seul enfant,</l>
						<l n="30" num="3.2"><space quantity="8" unit="char"></space>La Mitrailleuse, mousquetade</l>
						<l n="31" num="3.3">Énorme, a vite mis un millier triomphant</l>
						<l n="32" num="3.4"><space quantity="8" unit="char"></space>D’hommes faits — en capilotade.</l>
					</lg>
					<lg n="4">
						<l n="33" num="4.1">Vous ne résistez pas à la comparaison !</l>
						<l n="34" num="4.2"><space quantity="8" unit="char"></space>Couseuses, rien ne peut absoudre</l>
						<l n="35" num="4.3">Le fil d’or de nos jours ; vous n’aurez pas raison</l>
						<l n="36" num="4.4"><space quantity="8" unit="char"></space>De cette machine à découdre !</l>
					</lg>
					<lg n="5">
						<l n="37" num="5.1">Le fossoyeur n’a plus à creuser de tombeaux.</l>
						<l n="38" num="5.2"><space quantity="8" unit="char"></space>Les oiseaux noirs pendent en grappe</l>
						<l n="39" num="5.3">Sur nous ; voici venir la fête des corbeaux :</l>
						<l n="40" num="5.4"><space quantity="8" unit="char"></space>C’est pour eux que l’on met la nappe !</l>
					</lg>
				</div>
				<div type="section" n="3">
					<head type="number">III</head>
					<lg n="1">
						<l n="41" num="1.1">Car, ô Progrès, génie auguste et factieux !</l>
						<l n="42" num="1.2"><space quantity="8" unit="char"></space>Songeur qui, déployant tes ailes,</l>
						<l n="43" num="1.3">Sous les noirs Océans et dans l’horreur des Cieux</l>
						<l n="44" num="1.4"><space quantity="8" unit="char"></space>Vas chercher des routes nouvelles !</l>
					</lg>
					<lg n="2">
						<l n="45" num="2.1">Un ménechme hideux, ton singe et ton bouffon,</l>
						<l n="46" num="2.2"><space quantity="8" unit="char"></space>Contemplant ton œuvre hardie,</l>
						<l n="47" num="2.3">Pour réjouir la Nuit et pour charmer Typhon</l>
						<l n="48" num="2.4"><space quantity="8" unit="char"></space>En fait l’ignoble parodie ;</l>
					</lg>
					<lg n="3">
						<l n="49" num="3.1">Et quand, victorieux des vieux spectres rampants,</l>
						<l n="50" num="3.2"><space quantity="8" unit="char"></space>Recréant la beauté première,</l>
						<l n="51" num="3.3">Démon de la science et du jour, tu répands</l>
						<l n="52" num="3.4"><space quantity="8" unit="char"></space>La poésie et la lumière ;</l>
					</lg>
					<lg n="4">
						<l n="53" num="4.1">Quand tu pétris, cyclope, avec ton dur marteau,</l>
						<l n="54" num="4.2"><space quantity="8" unit="char"></space>La machine, — bête de somme</l>
						<l n="55" num="4.3">Qui traîne en se jouant le char et le bateau,</l>
						<l n="56" num="4.4"><space quantity="8" unit="char"></space>Détruit l’espace, affranchit l’homme,</l>
					</lg>
					<lg n="5">
						<l n="57" num="5.1">La Machine, qui va pour nous recommencer</l>
						<l n="58" num="5.2"><space quantity="8" unit="char"></space>Les Titans aux labeurs superbes,</l>
						<l n="59" num="5.3">Qui sait creuser le noir sillon, ensemencer,</l>
						<l n="60" num="5.4"><space quantity="8" unit="char"></space>Faucher le blé, lier les gerbes ;</l>
					</lg>
					<lg n="6">
						<l n="61" num="6.1">Alors le faux Progrès, ton singe, acclimaté</l>
						<l n="62" num="6.2"><space quantity="8" unit="char"></space>Dans les batailles volcaniques,</l>
						<l n="63" num="6.3">Pour nous hacher menu comme chair à pâté</l>
						<l n="64" num="6.4"><space quantity="8" unit="char"></space>Forge des bourreaux mécaniques !</l>
					</lg>
				</div>
				<closer>
					<dateline>
						<date when="1868">Septembre 1868.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>