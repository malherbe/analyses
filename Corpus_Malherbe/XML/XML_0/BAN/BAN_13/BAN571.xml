<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">OCCIDENTALES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2572 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_13</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">OCCIDENTALES</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="1998">1998</date>
								</imprint>
								<biblScope unit="tome">V</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre, éditeur</publisher>
							<date when="1891">1889-1891</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1875">1875</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-03-13" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="drama" key="BAN571">
				<head type="main">Chez Guignol</head>
				<div type="body">
					<castList>
						<head>Personnages</head>
							<castItem>
								<role>Polichinelle</role>
							</castItem>
							<castItem>
								<role>Le Commissaire</role>
							</castItem>
							<castItem>
								<role>Le Chat, personnage muet</role>
							</castItem>
					</castList>
					<div type="scene" n="1">
						<sp n="1">
							<speaker>Polichinelle.</speaker>
							<l n="1">Près de la Seine ou près du Tibre</l>
							<l n="2">Tous les esclavages sont laids !</l>
							<l n="3">Cher Commissaire, suis-je libre ?</l>
							<l part="I" n="4">Réponds-moi franchement. </l>
						</sp>
						<sp n="2">
							<speaker>Le Commissaire.</speaker>
							<l part="F" n="4">Tu l’es.</l>
						</sp>
						<sp n="3">
							<speaker>Polichinelle.</speaker>
							<l n="5">Plus d’abus ! Je dois les proscrire.</l>
							<l n="6">Pour éclairer quelque jour nos</l>
							<l n="7">Chers concitoyens, puis-je écrire</l>
							<l n="8">Ce que je veux dans les journaux ?</l>
						</sp>
						<sp n="4">
							<speaker>Le Commissaire.</speaker>
							<l n="9">Oui, tu le peux, — c’est ton affaire,</l>
							<l n="10">A Paris comme à Montbrison,</l>
							<l n="11">En risquant seulement de faire</l>
							<l n="12">Sept ou huit mille ans de prison.</l>
						</sp>
						<sp n="5">
							<speaker>Le Commissaire.</speaker>
							<l n="13">Fort bien. — Mais, de l’Art idolâtre,</l>
							<l n="14">Puis-je, à cette heure où je déchois,</l>
							<l n="15">Représenter sur mon théâtre</l>
							<l n="16">Les anciens drames de mon choix ?</l>
						</sp>
						<sp n="6">
							<speaker>Le Commissaire.</speaker>
							<l n="17">Tu le peux, et que cette fête</l>
							<l n="18">Enchante le ciel indigo.</l>
							<l n="19">(Pourvu que le nom du poëte</l>
							<l n="20">Ne se termine pas en go.)</l>
						</sp>
						<sp n="7">
							<speaker>Le Commissaire.</speaker>
							<l n="21">Pour leur confier, joie ou larmes,</l>
							<l n="22">Tout ce qu’en moi le ciel a mis,</l>
							<l n="23">Puis-je, en l’absence des gendarmes,</l>
							<l n="24">Me réunir à mes amis ?</l>
						</sp>
						<sp n="8">
							<speaker>Le Commissaire.</speaker>
							<l n="25">Oui. — Mais comme, ici-bas, l’on n’aime,</l>
							<l n="26">En ce lieu de perdition,</l>
							<l n="27">Aucun autre ami que soi-même,</l>
							<l n="28">C’est à cette condition</l>
						</sp>
						<sp n="9">
							<speaker></speaker>
							<l n="29">Qu’imitant Vénus dans sa conque,</l>
							<l n="30">Aux champs, à l’ombre d’un tilleul,</l>
							<l n="31">Ou dans une chambre quelconque</l>
							<l n="32">Tu te réuniras — tout seul !</l>
						</sp>
						<sp n="10">
							<speaker>Le Commissaire.</speaker>
							<l n="33">Bon. — Puis-je, lorsque tu me livres</l>
							<l n="34">Cet avenir doux et pompeux,</l>
							<l n="35">Avoir, pour colporter mes livres,</l>
							<l part="I" n="36">Ton estampille ? </l>
						</sp>
						<sp n="11">
							<speaker>Le Commissaire.</speaker>
							<l part="F" n="36">Tu le peux.</l>
							<l n="37">Colporte-les jusqu’aux murs d’Arles !</l>
							<l n="38">Et colporte-les encore à</l>
							<l n="39">Rome, pourvu que tu n’y parles</l>
							<l n="40">Que de Nichette et de Cora !</l>
						</sp>
						<sp n="12">
							<speaker>Le Commissaire.</speaker>
							<l n="41">A l’Oisiveté, qui diffère,</l>
							<l n="42">Apportant un remède sain,</l>
							<l n="43">Mon héritier peut-il se faire</l>
							<l n="44">Agriculteur ou médecin ?</l>
						</sp>
						<sp n="13">
							<speaker>Le Commissaire.</speaker>
							<l n="45">Il le peut. Je détruis, j’efface</l>
							<l n="46">Tout ce qui jadis le bridait,</l>
							<l n="47">Mais à condition qu’il fasse</l>
							<l n="48">L’exercice, — comme Bridet !</l>
						</sp>
						<sp n="14">
							<speaker>Le Commissaire.</speaker>
							<l n="49">Puis-je, allant faire une visite</l>
							<l n="50">A mon jeune ami Briollet,</l>
							<l n="51">Quand l’ouragan fait qu’on hésite,</l>
							<l n="52">Y courir en cabriolet ?</l>
						</sp>
						<sp n="15">
							<speaker>Le Commissaire.</speaker>
							<l n="53">Oui, — pourvu que dans les citernes</l>
							<l n="54">Ton cabriolet n’aille pas,</l>
							<l n="55">S’il est nuit, mirer des — lanternes !</l>
						</sp>
						<sp n="16">
							<speaker>Le Commissaire.</speaker>
							<l n="56">Il suffit. Libre de mes pas,</l>
							<l n="57">Je puis être loyal et brave.</l>
							<l n="58">J’ai craint qu’on ne m’en empêchât,</l>
							<l n="59">Mais point ! Si quelqu’un est esclave,</l>
							<l part="I" n="60">Ce n’est pas moi. </l>
						</sp>
						<sp n="17">
							<speaker>Le Commissaire.</speaker>
							<l part="F" n="60">Non, c’est le chat.</l>
						</sp>
					</div>
				</div>
				<div type="back">
					<closer>
						<dateline>
							<date when="1868">Septembre 1868.</date>
						</dateline>
					</closer>
				</div>
			</div></body></text></TEI>