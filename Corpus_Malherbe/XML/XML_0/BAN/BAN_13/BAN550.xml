<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">OCCIDENTALES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2572 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_13</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">OCCIDENTALES</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="1998">1998</date>
								</imprint>
								<biblScope unit="tome">V</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre, éditeur</publisher>
							<date when="1891">1889-1891</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1875">1875</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-03-13" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN550">
				<head type="main">Masques et Dominos</head>
				<lg n="1">
					<l n="1" num="1.1">Ohé ! voici les masques !</l>
					<l n="2" num="1.2">Fiévreux, coiffés de casques,</l>
					<l n="3" num="1.3">Costumés en titis,</l>
					<l n="4" num="1.4"><space quantity="4" unit="char"></space>En ouistitis,</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Sans mesure et sans règles,</l>
					<l n="6" num="2.2">Ils poussent des cris d’aigles,</l>
					<l n="7" num="2.3">De chenapans, de paons</l>
					<l n="8" num="2.4"><space quantity="4" unit="char"></space>Et d’aegipans !</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Le Délire s’exalte</l>
					<l n="10" num="3.2">Et, le long de l’asphalte,</l>
					<l n="11" num="3.3">Fait ondoyer ces chars</l>
					<l n="12" num="3.4"><space quantity="4" unit="char"></space>De balochards !</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">Hurlez dans les ténèbres !</l>
					<l n="14" num="4.2">Mais, ô têtes célèbres,</l>
					<l n="15" num="4.3">Est-ce vous que je vois ?</l>
					<l n="16" num="4.4"><space quantity="4" unit="char"></space>J’entends des voix</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1">Qui me sont familières !</l>
					<l n="18" num="5.2">Ours blancs sans muselières,</l>
					<l n="19" num="5.3">Chicards, turcs, albanais,</l>
					<l n="20" num="5.4"><space quantity="4" unit="char"></space>Je vous connais !</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1">Car cette fois, sans lustre,</l>
					<l n="22" num="6.2">Tout le Paris illustre</l>
					<l n="23" num="6.3">A pied comme à cheval</l>
					<l n="24" num="6.4"><space quantity="4" unit="char"></space>Fait carnaval !</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1">Voici la Femme à barbe</l>
					<l n="26" num="7.2">Qui but de la rhubarbe ;</l>
					<l n="27" num="7.3">Et c’est d’où vint sa peur</l>
					<l n="28" num="7.4"><space quantity="4" unit="char"></space>Près du sapeur.</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1">Sous tes regards, Europe,</l>
					<l n="30" num="8.2">La Sappho de la chope,</l>
					<l n="31" num="8.3">Œil triste et front pâli,</l>
					<l n="32" num="8.4"><space quantity="4" unit="char"></space>Sort de l’oubli</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1">Et reprend sa marotte.</l>
					<l n="34" num="9.2">(On sait quelle carotte</l>
					<l n="35" num="9.3">Cette Ange de l’aplomb</l>
					<l n="36" num="9.4"><space quantity="4" unit="char"></space>Eut dans le plomb !)</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1">Voici l’Homme au trombone !</l>
					<l n="38" num="10.2">S’il a près de la bonne</l>
					<l n="39" num="10.3">Cet air aguerri, c’est</l>
					<l n="40" num="10.4"><space quantity="4" unit="char"></space>Qu’il guérissait ;</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1">Car, pour rendre aux gens chauves</l>
					<l n="42" num="11.2">Des cheveux noirs ou fauves,</l>
					<l n="43" num="11.3">Ce zouave Jacob</l>
					<l n="44" num="11.4"><space quantity="4" unit="char"></space>Vaut monsieur Lob !</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1">Voici le ferme athlète</l>
					<l n="46" num="12.2">Qu’une lionne allaite</l>
					<l n="47" num="12.3">Et qui cache son nez</l>
					<l n="48" num="12.4"><space quantity="4" unit="char"></space>Aux gens bien nés ;</l>
				</lg>
				<lg n="13">
					<l n="49" num="13.1">Certes il est bel homme ;</l>
					<l n="50" num="13.2">Pourtant Gavroche nomme</l>
					<l n="51" num="13.3">Ce fier lutteur masqué :</l>
					<l n="52" num="13.4"><space quantity="4" unit="char"></space>Communiqué !</l>
				</lg>
				<lg n="14">
					<l n="53" num="14.1">Ah ! te voilà, mon brave !</l>
					<l n="54" num="14.2">Qu’il est triste, le grave</l>
					<l n="55" num="14.3">Constitutionnel,</l>
					<l n="56" num="14.4"><space quantity="4" unit="char"></space>Et solennel !</l>
				</lg>
				<lg n="15">
					<l n="57" num="15.1">Ombre de Boniface,</l>
					<l n="58" num="15.2">Quoi que ta bonne y fasse,</l>
					<l n="59" num="15.3">Il s’en va, Limayrac !</l>
					<l n="60" num="15.4"><space quantity="4" unit="char"></space>Dieux ! que son frac</l>
				</lg>
				<lg n="16">
					<l n="61" num="16.1">Est orné ! Que de plaques !</l>
					<l n="62" num="16.2">Il en a de valaques !</l>
					<l n="63" num="16.3">Sur son cœur et son flanc</l>
					<l n="64" num="16.4"><space quantity="4" unit="char"></space>Que de fer-blanc !</l>
				</lg>
				<lg n="17">
					<l n="65" num="17.1">Voici, dans sa culotte,</l>
					<l n="66" num="17.2">Qui colle, et pourtant flotte,</l>
					<l n="67" num="17.3">L’orateur contenu,</l>
					<l n="68" num="17.4"><space quantity="4" unit="char"></space>Qui va, front nu.</l>
				</lg>
				<lg n="18">
					<l n="69" num="18.1">Pallas, tenant sa lance,</l>
					<l n="70" num="18.2">Lui dit : Ton beau silence</l>
					<l n="71" num="18.3">N’a jamais tari, mon</l>
					<l n="72" num="18.4"><space quantity="4" unit="char"></space>Cher Darimon !</l>
				</lg>
				<lg n="19">
					<l n="73" num="19.1">Près de Camors, qui montre</l>
					<l n="74" num="19.2">Son âme de rencontre,</l>
					<l n="75" num="19.3">Madame de Chalis</l>
					<l n="76" num="19.4"><space quantity="4" unit="char"></space>Montre ses lys ;</l>
				</lg>
				<lg n="20">
					<l n="77" num="20.1">Et même, en cette foule,</l>
					<l n="78" num="20.2">Qui va comme une houle,</l>
					<l n="79" num="20.3">Joyeux, je contemplai</l>
					<l n="80" num="20.4"><space quantity="4" unit="char"></space>Monsieur Leplay,</l>
				</lg>
				<lg n="21">
					<l n="81" num="21.1">Qu’on a pu voir, en somme,</l>
					<l n="82" num="21.2">Réclamant les sous, comme</l>
					<l n="83" num="21.3">Naguère Paul Niquet,</l>
					<l n="84" num="21.4"><space quantity="4" unit="char"></space>Au tourniquet !</l>
				</lg>
				<lg n="22">
					<l n="85" num="22.1">Voici Veuillot. Il livre</l>
					<l n="86" num="22.2">Sa bataille. Il s’enivre</l>
					<l n="87" num="22.3">Des odeurs de Paris.</l>
					<l n="88" num="22.4"><space quantity="4" unit="char"></space>Que de paris</l>
				</lg>
				<lg n="23">
					<l n="89" num="23.1">Pour savoir si Domange</l>
					<l n="90" num="23.2">Est celui qu’il nomme : Ange !</l>
					<l n="91" num="23.3">Ou s’il veut le tricher</l>
					<l n="92" num="23.4"><space quantity="4" unit="char"></space>Avec Richer !</l>
				</lg>
				<lg n="24">
					<l n="93" num="24.1">Je vois, suivant sa piste,</l>
					<l n="94" num="24.2">Un bon feuilletonniste</l>
					<l n="95" num="24.3">Qui le lundi venait :</l>
					<l n="96" num="24.4"><space quantity="4" unit="char"></space>Monsieur Venet !</l>
				</lg>
				<lg n="25">
					<l n="97" num="25.1">Il est dur, mais bien jeune !</l>
					<l n="98" num="25.2">C’est d’Augier qu’il déjeune,</l>
					<l n="99" num="25.3">Et ce dragon dînait</l>
					<l n="100" num="25.4"><space quantity="4" unit="char"></space>De Gondinet !</l>
				</lg>
				<lg n="26">
					<l n="101" num="26.1">Puis voici les cocottes</l>
					<l n="102" num="26.2">Faisant coller leurs cottes</l>
					<l n="103" num="26.3">De satin — sur des monts</l>
					<l n="104" num="26.4"><space quantity="4" unit="char"></space>Chers aux démons !</l>
				</lg>
				<lg n="27">
					<l n="105" num="27.1">Oh ! la charmante pose !</l>
					<l n="106" num="27.2">La chevelure rose</l>
					<l n="107" num="27.3">Vraiment sied encore à</l>
					<l n="108" num="27.4"><space quantity="4" unit="char"></space>Cette Cora ;</l>
				</lg>
				<lg n="28">
					<l n="109" num="28.1">Fille-de-l’Air, qui lève</l>
					<l n="110" num="28.2">Sa jambe, comme un glaive</l>
					<l n="111" num="28.3">Brillant, nous montre son</l>
					<l n="112" num="28.4"><space quantity="4" unit="char"></space>Blanc caleçon ;</l>
				</lg>
				<lg n="29">
					<l n="113" num="29.1">Sans sourciller, pour elles</l>
					<l n="114" num="29.2">L’Amour coupe ses ailes</l>
					<l n="115" num="29.3">Et dit : Je me plais où</l>
					<l n="116" num="29.4"><space quantity="4" unit="char"></space>Je vois Zouzou !</l>
				</lg>
				<lg n="30">
					<l n="117" num="30.1">Voici… Mais, ô ma lyre,</l>
					<l n="118" num="30.2">On ne peut pas tout dire.</l>
					<l n="119" num="30.3">J’en passe et des meilleurs ;</l>
					<l n="120" num="30.4"><space quantity="4" unit="char"></space>C’est comme ailleurs !</l>
				</lg>
				<lg n="31">
					<l n="121" num="31.1">O boulevards fantasques !</l>
					<l n="122" num="31.2">Près de nous, que de masques,</l>
					<l n="123" num="31.3">Tartuffes et Scapins</l>
					<l n="124" num="31.4"><space quantity="4" unit="char"></space>Et galopins,</l>
				</lg>
				<lg n="32">
					<l n="125" num="32.1">Et marchandes de pommes</l>
					<l n="126" num="32.2">Et Pierrots ! mais des hommes</l>
					<l n="127" num="32.3">Parmi tous ces Gil Blas ?</l>
					<l n="128" num="32.4"><space quantity="4" unit="char"></space>Cherchez, hélas !</l>
				</lg>
				<lg n="33">
					<l n="129" num="33.1">Car il en est encore</l>
					<l n="130" num="33.2">Que tourmente et dévore</l>
					<l n="131" num="33.3">L’amour de ta clarté,</l>
					<l n="132" num="33.4"><space quantity="4" unit="char"></space>O vérité ;</l>
				</lg>
				<lg n="34">
					<l n="133" num="34.1">Seulement je suppose</l>
					<l n="134" num="34.2">Qu’ils ont la bouche close.</l>
					<l n="135" num="34.3">Ils n’en pensent pas moins ;</l>
					<l n="136" num="34.4"><space quantity="4" unit="char"></space>Mais ces témoins,</l>
				</lg>
				<lg n="35">
					<l n="137" num="35.1">Pour qui l’éclat sans feinte</l>
					<l n="138" num="35.2">De ta nudité sainte</l>
					<l n="139" num="35.3">Aurait seul des appas,</l>
					<l n="140" num="35.4"><space quantity="4" unit="char"></space>Ne veulent pas,</l>
				</lg>
				<lg n="36">
					<l n="141" num="36.1">Contre tous les usages,</l>
					<l n="142" num="36.2">Parler à des visages</l>
					<l n="143" num="36.3">Ambigus, terminés</l>
					<l n="144" num="36.4"><space quantity="4" unit="char"></space>Par des faux nez !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1868">Février 1868.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>