<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ODELETTES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1529 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_6</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">ODELETTES</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/banvilleodelettes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title> Poésies complètes. Les Éxilés. Odelettes, Améthystes, Rimes dorées, Rondels, les Princesses, Trente-six ballades joyeuses.</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Bibliothèque Charpentier, Eugène Fasquelle, Éditeur</publisher>
							<date when="1878">1878</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1856">1856</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN292">
				<head type="main">A Charles Asselineau</head>
				<lg n="1">
					<l n="1" num="1.1">Vainement tu lui fais affront,</l>
					<l n="2" num="1.2"><space quantity="4" unit="char"></space>Votre brouille m’amuse,</l>
					<l n="3" num="1.3">Car je reconnais sur ton front</l>
					<l n="4" num="1.4"><space quantity="4" unit="char"></space>Le baiser de la Muse.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Tout est fini, si tu le veux ;</l>
					<l n="6" num="2.2"><space quantity="4" unit="char"></space>Mais que le vent les bouge,</l>
					<l n="7" num="2.3">Vite on le voit sous tes cheveux,</l>
					<l n="8" num="2.4"><space quantity="4" unit="char"></space>La place est encor rouge.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Tu fuis le bois des lauriers verts</l>
					<l n="10" num="3.2"><space quantity="4" unit="char"></space>Et la troupe des cygnes,</l>
					<l n="11" num="3.3">Et, pour mieux laisser l’art des vers</l>
					<l n="12" num="3.4"><space quantity="4" unit="char"></space>A des chanteurs plus dignes,</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">Tu ne t’égares plus jamais</l>
					<l n="14" num="4.2"><space quantity="4" unit="char"></space>Sous la lune blafarde.</l>
					<l n="15" num="4.3">La modestie est bonne, mais</l>
					<l n="16" num="4.4"><space quantity="4" unit="char"></space>Cette fois prends-y garde !</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1">Par ces scrupules obligeants,</l>
					<l n="18" num="5.2"><space quantity="4" unit="char"></space>Trop souvent on condamne</l>
					<l n="19" num="5.3">La fée amoureuse à des gens</l>
					<l n="20" num="5.4"><space quantity="4" unit="char"></space>Coiffés de têtes d’âne.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1">Firdusi ne vit plus à Thus !</l>
					<l n="22" num="6.2"><space quantity="4" unit="char"></space>Toutes les nuits un ange</l>
					<l n="23" num="6.3">Vient baiser les fleurs de lotus</l>
					<l n="24" num="6.4"><space quantity="4" unit="char"></space>Aux bords sacrés du Gange ;</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1">L’hyacinthe frissonne encor</l>
					<l n="26" num="7.2"><space quantity="4" unit="char"></space>Dans les clairières lisses ;</l>
					<l n="27" num="7.3">Toujours, faisant du soleil d’or</l>
					<l n="28" num="7.4"><space quantity="4" unit="char"></space>Les plus chères délices,</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1">La rose à sa douce senteur</l>
					<l n="30" num="8.2"><space quantity="4" unit="char"></space>Enivre Polymnie,</l>
					<l n="31" num="8.3">Mais je connais plus d’un auteur</l>
					<l n="32" num="8.4"><space quantity="4" unit="char"></space>Qui n’a pas de génie !</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1">Viens ! ne laisse pas galamment</l>
					<l n="34" num="9.2"><space quantity="4" unit="char"></space>Notre gentille escrime</l>
					<l n="35" num="9.3">Aux sots, privés également</l>
					<l n="36" num="9.4"><space quantity="4" unit="char"></space>De raison et de rime.</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1">Au moins, reprends notre lien</l>
					<l n="38" num="10.2"><space quantity="4" unit="char"></space>Pour une année entière !</l>
					<l n="39" num="10.3">Et d’ailleurs, ami, tu peux bien</l>
					<l n="40" num="10.4"><space quantity="4" unit="char"></space>Chez le vieux Furetière</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1">Errer comme en un Sahara ;</l>
					<l n="42" num="11.2"><space quantity="4" unit="char"></space>Acheter et revendre</l>
					<l n="43" num="11.3">Des bouquins ; Érato saura</l>
					<l n="44" num="11.4"><space quantity="4" unit="char"></space>Toujours où te reprendre !</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1">Au mois où s’ouvrent les boutons,</l>
					<l n="46" num="12.2"><space quantity="4" unit="char"></space>Tous ceux qui l’ont aimée</l>
					<l n="47" num="12.3">Reviennent comme des moutons</l>
					<l n="48" num="12.4"><space quantity="4" unit="char"></space>Sur sa trace charmée.</l>
				</lg>
				<lg n="13">
					<l n="49" num="13.1">Or, justement, pris à l’attrait</l>
					<l n="50" num="13.2"><space quantity="4" unit="char"></space>De mes rimes prolixes,</l>
					<l n="51" num="13.3">J’entends errer dans la forêt</l>
					<l n="52" num="13.4"><space quantity="4" unit="char"></space>Les elfes et les nixes ;</l>
				</lg>
				<lg n="14">
					<l n="53" num="14.1">Et, dans le parc où nous songeons,</l>
					<l n="54" num="14.2"><space quantity="4" unit="char"></space>La sève, dont la force</l>
					<l n="55" num="14.3">Croît, gonfle déjà les bourgeons</l>
					<l n="56" num="14.4"><space quantity="4" unit="char"></space>Prêts à rompre l’écorce.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1855">Mai 1855.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>