<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ODELETTES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1529 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_6</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">ODELETTES</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/banvilleodelettes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title> Poésies complètes. Les Éxilés. Odelettes, Améthystes, Rimes dorées, Rondels, les Princesses, Trente-six ballades joyeuses.</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Bibliothèque Charpentier, Eugène Fasquelle, Éditeur</publisher>
							<date when="1878">1878</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1856">1856</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN314">
				<head type="main">A Alfred Dehodencq</head>
				<lg n="1">
					<l n="1" num="1.1">Tenir la lumière asservie</l>
					<l n="2" num="1.2">Lorsqu’elle voudrait s’envoler,</l>
					<l n="3" num="1.3"><space quantity="12" unit="char"></space>Et voler</l>
					<l n="4" num="1.4">A Dieu le secret de la vie ;</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Pour les mélanger sur des toiles</l>
					<l n="6" num="2.2">Dérober même aux cieux vengeurs</l>
					<l n="7" num="2.3"><space quantity="12" unit="char"></space>Leurs rougeurs</l>
					<l n="8" num="2.4">Et le blanc frisson des étoiles ;</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Comme on cueille une fleur éclose,</l>
					<l n="10" num="3.2">Ravir à l’Orient en feu</l>
					<l n="11" num="3.3"><space quantity="12" unit="char"></space>Son air bleu</l>
					<l n="12" num="3.4">Et son ciel flamboyant et rose ;</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">Pétrir de belles créatures,</l>
					<l n="14" num="4.2">Et sur d’éblouissants amas</l>
					<l n="15" num="4.3"><space quantity="12" unit="char"></space>De damas</l>
					<l n="16" num="4.4">Éparpiller des chevelures ;</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1">Inonder de sang le Calvaire</l>
					<l n="18" num="5.2">Ou jeter un éclat divin</l>
					<l n="19" num="5.3"><space quantity="12" unit="char"></space>Sur le vin</l>
					<l n="20" num="5.4">Qu’un buveur a mis dans son verre ;</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1">Se réjouir des pierreries,</l>
					<l n="22" num="6.2">Et jeter le baiser vermeil</l>
					<l n="23" num="6.3"><space quantity="12" unit="char"></space>Du soleil</l>
					<l n="24" num="6.4">Jusque sur les rouges tueries ;</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1">Créer des êtres, et leur dire :</l>
					<l n="26" num="7.2">Misérables, c’est votre tour !</l>
					<l n="27" num="7.3"><space quantity="12" unit="char"></space>Que l’Amour</l>
					<l n="28" num="7.4">De sa folle main vous déchire ;</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1">Enfin pour ce monde risible</l>
					<l n="30" num="8.2">Forçant la couleur à chanter,</l>
					<l n="31" num="8.3"><space quantity="12" unit="char"></space>L’enchanter</l>
					<l n="32" num="8.4">Par une musique visible,</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1">Voilà vraiment ce que vous faites,</l>
					<l n="34" num="9.2">Peintres ! qui pour nous préparez</l>
					<l n="35" num="9.3"><space quantity="12" unit="char"></space>Et parez</l>
					<l n="36" num="9.4">Sans repos d’éternelles fêtes !</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1">Ouvriers, inventeurs, génies !</l>
					<l n="38" num="10.2">Par un miracle surhumain,</l>
					<l n="39" num="10.3"><space quantity="12" unit="char"></space>Votre main</l>
					<l n="40" num="10.4">Réalise ces harmonies</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1">Où la couleur qui se déploie</l>
					<l n="42" num="11.2">En accords de la nuit vainqueurs,</l>
					<l n="43" num="11.3"><space quantity="12" unit="char"></space>Dans nos cœurs</l>
					<l n="44" num="11.4">Fait jaillir des sources de joie.</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1">Et nos fronts sont baignés d’aurore.</l>
					<l n="46" num="12.2">Mais vous, par un retour fatal,</l>
					<l n="47" num="12.3"><space quantity="12" unit="char"></space>L’Idéal</l>
					<l n="48" num="12.4">Vous martyrise et vous dévore.</l>
				</lg>
				<lg n="13">
					<l n="49" num="13.1">Et vos enchantements sublimes,</l>
					<l n="50" num="13.2">Vous les payez de votre chair ;</l>
					<l n="51" num="13.3"><space quantity="12" unit="char"></space>Il est cher,</l>
					<l n="52" num="13.4">Le feu qu’on vole sur les cimes !</l>
				</lg>
				<lg n="14">
					<l n="53" num="14.1">Si tu montas avec délice</l>
					<l n="54" num="14.2">L’escalier bleu des paradis</l>
					<l n="55" num="14.3"><space quantity="12" unit="char"></space>Interdits,</l>
					<l n="56" num="14.4">Un inexprimable supplice</l>
				</lg>
				<lg n="15">
					<l n="57" num="15.1">Te punit, ô rêveur étrange</l>
					<l n="58" num="15.2">Qui sus donner l’illusion</l>
					<l n="59" num="15.3"><space quantity="12" unit="char"></space>Du rayon</l>
					<l n="60" num="15.4">De lumière où s’envole un Ange ;</l>
				</lg>
				<lg n="16">
					<l n="61" num="16.1">Et lorsque tout le ciel flamboie</l>
					<l n="62" num="16.2">Dans ta prunelle ivre d’amour,</l>
					<l n="63" num="16.3"><space quantity="12" unit="char"></space>Un vautour</l>
					<l n="64" num="16.4">Vient manger ton cœur et ton foie.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1872">24 novembre 1872.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>