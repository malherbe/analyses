<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ODELETTES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1529 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_6</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">ODELETTES</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/banvilleodelettes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title> Poésies complètes. Les Éxilés. Odelettes, Améthystes, Rimes dorées, Rondels, les Princesses, Trente-six ballades joyeuses.</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Bibliothèque Charpentier, Eugène Fasquelle, Éditeur</publisher>
							<date when="1878">1878</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1856">1856</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN298">
				<head type="main">A Méry</head>
				<lg n="1">
					<l n="1" num="1.1">Plus vite que les autans,</l>
					<l n="2" num="1.2">Saqui, l’immortelle, au temps</l>
					<l n="3" num="1.3">De sa royauté naissante,</l>
					<l n="4" num="1.4">Tourbillonnait d’un pied sûr,</l>
					<l n="5" num="1.5">A mille pieds en l’air, sur</l>
					<l n="6" num="1.6">Une corde frémissante.</l>
				</lg>
				<lg n="2">
					<l n="7" num="2.1">Et l’on craignait que d’un bond</l>
					<l n="8" num="2.2">Parfois son vol vagabond</l>
					<l n="9" num="2.3">Décrochât, par aventure,</l>
					<l n="10" num="2.4">Parmi les cieux étoilés,</l>
					<l n="11" num="2.5">Les astres échevelés</l>
					<l n="12" num="2.6">Fouettés par sa chevelure.</l>
				</lg>
				<lg n="3">
					<l n="13" num="3.1">En haut vers elle parfois,</l>
					<l n="14" num="3.2">Comme de tremblantes voix,</l>
					<l n="15" num="3.3">Montaient les cris de la foule</l>
					<l n="16" num="3.4">Qu’elle voyait du ciel clair</l>
					<l n="17" num="3.5">Confuse comme une mer</l>
					<l n="18" num="3.6">Où passe l’ardente houle.</l>
				</lg>
				<lg n="4">
					<l n="19" num="4.1">Et, soit qu’en faisant un pas</l>
					<l n="20" num="4.2">Elle regardât en bas</l>
					<l n="21" num="4.3">Ou vers les célestes cimes,</l>
					<l n="22" num="4.4">Aux cieux que cherchait son vol,</l>
					<l n="23" num="4.5">Comme à ses pieds sur le sol,</l>
					<l n="24" num="4.6">Elle voyait deux abîmes.</l>
				</lg>
				<lg n="5">
					<l n="25" num="5.1">Dans les nuages vermeils,</l>
					<l n="26" num="5.2">Au beau milieu des soleils</l>
					<l n="27" num="5.3">Qu’elle touchait de la tête</l>
					<l n="28" num="5.4">Et parmi l’éther bravé,</l>
					<l n="29" num="5.5">Elle songeait au pavé.</l>
					<l n="30" num="5.6">Tel est le sort du poète.</l>
				</lg>
				<lg n="6">
					<l n="31" num="6.1">Il trône dans la vapeur.</l>
					<l n="32" num="6.2">Beau métier, s’il n’avait peur</l>
					<l n="33" num="6.3">De tomber sur quelque dalle</l>
					<l n="34" num="6.4">Parmi les badauds sereins,</l>
					<l n="35" num="6.5">Et de s’y casser les reins</l>
					<l n="36" num="6.6">Comme le fils de Dédale.</l>
				</lg>
				<lg n="7">
					<l n="37" num="7.1">Dans l’azur aérien</l>
					<l n="38" num="7.2">Qui le sollicite, ou bien</l>
					<l n="39" num="7.3">Sur la terre nue et froide</l>
					<l n="40" num="7.4">Qu’il aperçoit par lambeau,</l>
					<l n="41" num="7.5">Il voit partout son tombeau</l>
					<l n="42" num="7.6">Du haut de la corde roide,</l>
				</lg>
				<lg n="8">
					<l n="43" num="8.1">Et, sylphe au ventre changeant</l>
					<l n="44" num="8.2">Couvert d’écailles d’argent,</l>
					<l n="45" num="8.3">Il se penche vers la place</l>
					<l n="46" num="8.4">Du haut des cieux irisés,</l>
					<l n="47" num="8.5">Pour envoyer des baisers</l>
					<l n="48" num="8.6">A la vile populace.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1855">Mai 1855.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>