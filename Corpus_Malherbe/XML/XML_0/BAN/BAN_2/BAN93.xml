<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Odes funambulesques</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3888 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">BAN_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>

			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Odes funambulesques</title>
						<author>Théodore de BANVILLE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L922</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Odes funambulesques</title>
								<author>Théodore de BANVILLE</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>M. Levy</publisher>
									<date when="1859">1859</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>

			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres de Théodore de Banville : Odes funambulesques</title>
						<author>Théodore de BANVILLE</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre</publisher>
							<date when="1892">1892</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1857">1857</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-23" who="RR">Révision de l’entête pour validation</change>
			<change when="2016-03-14" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">OCCIDENTALES</head><div type="poem" key="BAN93">
					<head type="main">OCCIDENTALE NEUVIÈME</head>
					<head type="sub">REPRISE DE <hi rend="ital">LA DAME</hi></head>
					<lg n="1">
						<l n="1" num="1.1">Mourir de la poitrine</l>
						<l n="2" num="1.2">Quand j’ai ces bras de lys,</l>
						<l n="3" num="1.3">La lèvre purpurine,</l>
						<l n="4" num="1.4">Les cheveux de maïs</l>
						<l n="5" num="1.5">Et cette gorge rose,</l>
						<l n="6" num="1.6">Ah ! La vilaine chose !</l>
						<l n="7" num="1.7">Quel poëte morose</l>
						<l n="8" num="1.8">Est donc ce Dumas fils !</l>
					</lg>
					<lg n="2">
						<l n="9" num="2.1">Je fuis, pauvre colombe,</l>
						<l n="10" num="2.2">Le zéphyr accablant,</l>
						<l n="11" num="2.3">Je m’incline et je tombe</l>
						<l n="12" num="2.4">Comme un roseau tremblant,</l>
						<l n="13" num="2.5">Car, j’en ai fait le pacte,</l>
						<l n="14" num="2.6">Il faut qu’en femme exacte,</l>
						<l n="15" num="2.7">Au bout du cinquième acte</l>
						<l n="16" num="2.8">J’expire en peignoir blanc !</l>
					</lg>
					<lg n="3">
						<l n="17" num="3.1">Pourtant, j’aime une vie</l>
						<l n="18" num="3.2">Qu’un immortel trésor</l>
						<l n="19" num="3.3">Poétise, ravie,</l>
						<l n="20" num="3.4">Dans un si beau décor ;</l>
						<l n="21" num="3.5">J’aime pour mes extases</l>
						<l n="22" num="3.6">Les feux des chrysoprases,</l>
						<l n="23" num="3.7">Les rubis, les topazes,</l>
						<l n="24" num="3.8">Les tas d’argent et d’or !</l>
					</lg>
					<lg n="4">
						<l n="25" num="4.1">Paris est une ville</l>
						<l n="26" num="4.2">Où mille voyageurs</l>
						<l n="27" num="4.3">Cherchent au vaudeville</l>
						<l n="28" num="4.4">De pudiques rougeurs,</l>
						<l n="29" num="4.5">Où toute jeune fille</l>
						<l n="30" num="4.6">Aux façons de torpille</l>
						<l n="31" num="4.7">Peut avoir ce qui brille</l>
						<l n="32" num="4.8">Aux vitres des changeurs !</l>
					</lg>
					<lg n="5">
						<l n="33" num="5.1">J’aime cette lumière</l>
						<l n="34" num="5.2">Qui, des lustres fleuris,</l>
						<l n="35" num="5.3">Tombe aux soirs de première</l>
						<l n="36" num="5.4">Sur ma poudre de riz,</l>
						<l n="37" num="5.5">Quand, aux loges de face,</l>
						<l n="38" num="5.6">Ma petite grimace,</l>
						<l n="39" num="5.7">Malgré leur pose, efface</l>
						<l n="40" num="5.8">Cerisette et souris.</l>
					</lg>
					<lg n="6">
						<l n="41" num="6.1">J’aime qu’en ma fournaise</l>
						<l n="42" num="6.2">Un lingot fonde entier,</l>
						<l n="43" num="6.3">Et que, pour me rendre aise,</l>
						<l n="44" num="6.4">Avec un luxe altier</l>
						<l n="45" num="6.5">Qui ne soit pas un mythe,</l>
						<l n="46" num="6.6">Franchissant la limite,</l>
						<l n="47" num="6.7">Plus d’un caissier imite</l>
						<l n="48" num="6.8">Grellet et Carpentier !</l>
					</lg>
					<lg n="7">
						<l n="49" num="7.1">J’aime que le vieux comte</l>
						<l n="50" num="7.2">Soit réduit aux abois</l>
						<l n="51" num="7.3">En refaisant le compte</l>
						<l n="52" num="7.4">Des perles que je bois,</l>
						<l n="53" num="7.5">Enfin, cela m’allèche</l>
						<l n="54" num="7.6">De sentir ma calèche</l>
						<l n="55" num="7.7">Voler comme une flèche</l>
						<l n="56" num="7.8">Par les détours du bois !</l>
					</lg>
					<lg n="8">
						<l n="57" num="8.1">J’aime que l’on me bouge</l>
						<l n="58" num="8.2">Un grand miroir princier,</l>
						<l n="59" num="8.3">Pour me poser ce rouge</l>
						<l n="60" num="8.4">Qui plaît à mon boursier,</l>
						<l n="61" num="8.5">Tandis que ma compagne,</l>
						<l n="62" num="8.6">Brune fille d’Espagne,</l>
						<l n="63" num="8.7">Sur l’orgue m’accompagne</l>
						<l n="64" num="8.8">Des chansons de Darcier !</l>
					</lg>
					<lg n="9">
						<l n="65" num="9.1">Mais surtout, quand, dès l’aube,</l>
						<l n="66" num="9.2">S’éloigne mon sous-chef</l>
						<l n="67" num="9.3">Natif d’Arcis sur Aube,</l>
						<l n="68" num="9.4">Renvoyé d’un ton bref,</l>
						<l n="69" num="9.5">Dans ma main conquérante</l>
						<l n="70" num="9.6">J’aime à tenir quarante</l>
						<l n="71" num="9.7">Nouveaux coupons de rente,</l>
						<l n="72" num="9.8">Et du papier Joseph !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1857">janvier 1857</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>