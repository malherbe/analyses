<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Odes funambulesques</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3888 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">BAN_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>

			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Odes funambulesques</title>
						<author>Théodore de BANVILLE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L922</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Odes funambulesques</title>
								<author>Théodore de BANVILLE</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>M. Levy</publisher>
									<date when="1859">1859</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>

			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres de Théodore de Banville : Odes funambulesques</title>
						<author>Théodore de BANVILLE</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre</publisher>
							<date when="1892">1892</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1857">1857</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-23" who="RR">Révision de l’entête pour validation</change>
			<change when="2016-03-14" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">OCCIDENTALES</head><div type="poem" key="BAN87">
					<head type="main">OCCIDENTALE TROISIÈME</head>
					<head type="sub">V… LE BAIGNEUR</head>
					<lg n="1">
						<l n="1" num="1.1"><subst reason="analysis" type="completion" hand="RR"><del>V...</del><add rend="hidden">éron</add></subst> tout plein d’insolence</l>
						<l n="2" num="1.2"><space quantity="6" unit="char"></space>Se balance</l>
						<l n="3" num="1.3">Aussi ventru qu’un tonneau,</l>
						<l n="4" num="1.4">Au-dessus d’un bain de siège,</l>
						<l n="5" num="1.5"><space quantity="6" unit="char"></space>Ô barège,</l>
						<l n="6" num="1.6">Plein jusqu’au bord de ton eau !</l>
					</lg>
					<lg n="2">
						<l n="7" num="2.1">Et le flot, comme une nonne</l>
						<l n="8" num="2.2"><space quantity="6" unit="char"></space>Qu’on chiffonne,</l>
						<l n="9" num="2.3">Sous le profil reflété</l>
						<l n="10" num="2.4">De ce sultan ridicule</l>
						<l n="11" num="2.5"><space quantity="6" unit="char"></space>Se recule,</l>
						<l n="12" num="2.6">Se recule épouvanté</l>
					</lg>
					<lg n="3">
						<l n="13" num="3.1">Chaque fois que la courroie,</l>
						<l n="14" num="3.2"><space quantity="6" unit="char"></space>Qui se ploie,</l>
						<l n="15" num="3.3">Passe à fleur d’eau dans son vol,</l>
						<l n="16" num="3.4">On voit de l’eau qui s’agite</l>
						<l n="17" num="3.5"><space quantity="6" unit="char"></space>Sortir vite</l>
						<l n="18" num="3.6">Son pied bot et son faux col.</l>
					</lg>
					<lg n="4">
						<l n="19" num="4.1">Reste ici caché, demeure !</l>
						<l n="20" num="4.2"><space quantity="6" unit="char"></space>Dans une heure,</l>
						<l n="21" num="4.3">Ô spectacle saugrenu !</l>
						<l n="22" num="4.4">Comme actéon le profane</l>
						<l n="23" num="4.5"><space quantity="6" unit="char"></space>Vit diane,</l>
						<l n="24" num="4.6">Tu verras <subst reason="analysis" type="completion" hand="RR"><del>V...</del><add rend="hidden">éron</add></subst> tout nu !</l>
					</lg>
					<lg n="5">
						<l n="25" num="5.1">On voit tout ce que calfate</l>
						<l n="26" num="5.2"><space quantity="6" unit="char"></space>La cravate,</l>
						<l n="27" num="5.3">Et son regard libertin</l>
						<l n="28" num="5.4">Appelle comme remède</l>
						<l n="29" num="5.5"><space quantity="6" unit="char"></space>À son aide</l>
						<l n="30" num="5.6">Héloïse Florentin !</l>
					</lg>
					<lg n="6">
						<l n="31" num="6.1">Mais voyez le sybarite !</l>
						<l n="32" num="6.2"><space quantity="6" unit="char"></space>Il hésite</l>
						<l n="33" num="6.3">À finir ses doux ébats ;</l>
						<l n="34" num="6.4">Toujours <subst reason="analysis" type="completion" hand="RR"><del>V...</del><add rend="hidden">éron</add></subst> se balance</l>
						<l n="35" num="6.5"><space quantity="6" unit="char"></space>En silence,</l>
						<l n="36" num="6.6">Et va murmurant tout bas :</l>
					</lg>
					<lg n="7">
						<l n="37" num="7.1">« Ah ! Si j’étais en décembre</l>
						<l n="38" num="7.2"><space quantity="6" unit="char"></space>À la chambre,</l>
						<l n="39" num="7.3">J’étonnerais l’univers,</l>
						<l n="40" num="7.4">Et je pourrais de mon ombre</l>
						<l n="41" num="7.5"><space quantity="6" unit="char"></space>Faire nombre</l>
						<l n="42" num="7.6">À côté de Monsieur Thiers !</l>
					</lg>
					<lg n="8">
						<l n="43" num="8.1">« J’obtiendrais une recette</l>
						<l n="44" num="8.2"><space quantity="6" unit="char"></space>Grassouillette</l>
						<l n="45" num="8.3">Pour avoir bien ânonné,</l>
						<l n="46" num="8.4">Et la sinécure molle,</l>
						<l n="47" num="8.5"><space quantity="6" unit="char"></space>Qui console</l>
						<l n="48" num="8.6">Des rigueurs de l’abonné !</l>
					</lg>
					<lg n="9">
						<l n="49" num="9.1">« Je pourrais sur mon pupitre</l>
						<l n="50" num="9.2"><space quantity="6" unit="char"></space>Faire, en pitre,</l>
						<l n="51" num="9.3">Le bruit traditionnel,</l>
						<l n="52" num="9.4">Et, commençant une autre ère,</l>
						<l n="53" num="9.5"><space quantity="6" unit="char"></space>Ne plus traire</l>
						<l n="54" num="9.6"><hi rend="ital">le constitutionnel ! »</hi></l>
					</lg>
					<lg n="10">
						<l n="55" num="10.1">Ainsi se parle en monarque</l>
						<l n="56" num="10.2"><space quantity="6" unit="char"></space>Et s’embarque</l>
						<l n="57" num="10.3">Dans un rêve délirant,</l>
						<l n="58" num="10.4">Cet ancien apothicaire,</l>
						<l n="59" num="10.5"><space quantity="6" unit="char"></space>Qui sut faire</l>
						<l n="60" num="10.6">Éclore <hi rend="ital">le juif errant ! </hi></l>
					</lg>
					<lg n="11">
						<l n="61" num="11.1">Et cependant des coulisses</l>
						<l n="62" num="11.2"><space quantity="6" unit="char"></space>Ses complices</l>
						<l n="63" num="11.3">Vont tous prenant le chemin.</l>
						<l n="64" num="11.4">Voici leur troupe frivole</l>
						<l n="65" num="11.5"><space quantity="6" unit="char"></space>Qui s’envole,</l>
						<l n="66" num="11.6">Cigare aux dents, stick en main !</l>
					</lg>
					<lg n="12">
						<l n="67" num="12.1">En passant chacun s’étonne</l>
						<l n="68" num="12.2"><space quantity="6" unit="char"></space>Et chantonne,</l>
						<l n="69" num="12.3">Et lui dit sur l’air du <hi rend="ital">tra : </hi></l>
						<l n="70" num="12.4">« Oh ! La vilaine chenille</l>
						<l n="71" num="12.5"><space quantity="6" unit="char"></space>Qui s’habille</l>
						<l n="72" num="12.6">Si tard un soir d’opéra ! »</l>
					</lg>
					<closer>
						<dateline>
							<date when="1846">avril 1846</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>