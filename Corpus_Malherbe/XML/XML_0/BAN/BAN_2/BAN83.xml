<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Odes funambulesques</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3888 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">BAN_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>

			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Odes funambulesques</title>
						<author>Théodore de BANVILLE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L922</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Odes funambulesques</title>
								<author>Théodore de BANVILLE</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>M. Levy</publisher>
									<date when="1859">1859</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>

			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres de Théodore de Banville : Odes funambulesques</title>
						<author>Théodore de BANVILLE</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre</publisher>
							<date when="1892">1892</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1857">1857</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-23" who="RR">Révision de l’entête pour validation</change>
			<change when="2016-03-14" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ÉVOHÉ</head><head type="sub_part">NÉMÉSIS INTÉRIMAIRE</head><div type="poem" key="BAN83">
					<head type="form">SATIRE SIXIÈME</head>
					<head type="main">UNE VIEILLE LUNE</head>
					<sp n="1">
						<speaker>Moi.</speaker>
						<lg n="1">
							<l n="1" num="1.1">Chère infidèle ! Eh bien, qu’êtes-vous devenue ?</l>
							<l n="2" num="1.2">Depuis quinze grands jours vous n’êtes pas venue !</l>
							<l n="3" num="1.3">Chaque nuit, à l’abri du rideau de satin</l>
							<l n="4" num="1.4">Ma bougie en pleurant brûle jusqu’au matin ;</l>
							<l n="5" num="1.5">Je m’endors sans tenir votre main adorée,</l>
							<l n="6" num="1.6">Et lorsque vient l’aurore en voiture dorée,</l>
							<l n="7" num="1.7">Je cherche vainement dans les plis des coussins</l>
							<l n="8" num="1.8">Les deux nids parfumés où s’endorment vos seins,</l>
							<l n="9" num="1.9">Comme de doux oiseaux sur le marbre des tombes.</l>
							<l n="10" num="1.10">Qu’en faisiez-vous là-bas de ces blanches colombes !</l>
						<l part="I" n="11" num="1.11">Et tu ne m’aimes plus. </l>
						</lg>
					</sp>
					<sp n="2">
						<speaker>Évohé.</speaker>
						<lg n="1">

						<l part="F" n="11">Je vous aime toujours.</l>
						</lg>
					</sp>
					<sp n="3">
						<speaker>Moi.</speaker>
						<lg n="1">
							<l n="12" num="1.1"><choice hand="RR" reason="analysis" type="missing"><sic> </sic><corr source="edition_1892">Que faisais-tu, rivale en fleur des Pompadours ?</corr></choice></l>
							<l n="13" num="1.2">Un corset un peu juste, une étroite chaussure</l>
							<l n="14" num="1.3">Ont-ils égratigné d’une rose blessure</l>
							<l n="15" num="1.4">Tes beaux pieds ou ton corps, ces parterres de lys ?</l>
							<l n="16" num="1.5">Un drap trop dur, froissé par tes ongles polis,</l>
							<l n="17" num="1.6">A-t-il enfin meurtri, dans ses neiges tramées,</l>
							<l n="18" num="1.7">Ces bijoux rougissants, pareils à des camées ?</l>
							<l n="19" num="1.8">As-tu brisé ta lyre en chantant <hi rend="ital">Kradoudja ? </hi></l>
							<l n="20" num="1.9">Ou bien, dans ces doux vers que l’on aimait déjà,</l>
							<l n="21" num="1.10">Ta soubrette Vénus a-t-elle d’aventure</l>
							<l n="22" num="1.11">En te frisant le soir, plié ta chevelure ?</l>
							<l n="23" num="1.12">As-tu perdu ta voix et ton gazouillement ?</l>
						</lg>
					</sp>
					<sp n="4">
						<speaker>Évohé.</speaker>
						<lg n="1">
							<l n="24" num="1.1">Je suis harmonieuse et belle, ô mon amant !</l>
							<l n="25" num="1.2">Le drap tissu de neige et la chaussure noire</l>
							<l n="26" num="1.3">N’a pas mordu mes pieds ni mes ongles d’ivoire ;</l>
							<l n="27" num="1.4">Ma soubrette Cypris, qui m’aime quand je veux,</l>
							<l n="28" num="1.5">N’a pas coupé nos vers pour plier mes cheveux ;</l>
							<l n="29" num="1.6">On admire toujours les cent perles féeriques</l>
							<l n="30" num="1.7">Et les purs diamants de mes écrins lyriques :</l>
							<l n="31" num="1.8">Les cupidons ailés me servent d’échansons,</l>
							<l n="32" num="1.9">Et ma lyre d’argent est pleine de chansons.</l>
						</lg>
					</sp>
					<sp n="5">
						<speaker>Moi.</speaker>
						<lg n="1">
							<l n="33" num="1.1">Pourquoi donc as-tu fui la guerre, toi si brave !</l>
							<l n="34" num="1.2">On reprend <hi rend="ital">Abufar </hi>et <hi rend="ital">Lucrèce, </hi>on te brave !</l>
							<l n="35" num="1.3">Pends-toi, grillon ! <hi rend="ital">Lucrèce, </hi>enfin deux <hi rend="ital">Abufar !</hi></l>
							<l n="36" num="1.4">Et ce bache espagnol ivre de nénufar,</l>
							<l n="37" num="1.5">Damon, ce grand auteur dont la muse civile</l>
							<l n="38" num="1.6">Enchanta si longtemps et Lecourt et Clairville,</l>
							<l n="39" num="1.7">Est photographié pour ses talents divers.</l>
							<l n="40" num="1.8">Le Tarn au loin gémit et demande tes vers.</l>
						</lg>
					</sp>
					<sp n="6">
						<speaker>Évohé.</speaker>
						<lg n="1">
							<l n="41" num="1.1">N’as-tu donc point appris la fameuse nouvelle</l>
							<l n="42" num="1.2">Que l’aveugle déesse, en enflant sa grande aile,</l>
							<l n="43" num="1.3">Emporte aux quatre coins de l’univers connu ?</l>
						</lg>
					</sp>
					<sp n="7">
						<speaker>Moi.</speaker>
						<lg n="1">

						<l part="I" n="44" num="1.1">Non. </l>
						</lg>
					</sp>
					<sp n="8">
						<speaker>Évohé.</speaker>
						<lg n="1">

						<l part="F" n="44">Tremblez, terre et cieux ! Le maître est revenu.</l>
							<l n="45" num="1.1">Némésis-Astronome assemble ses vieux braves,</l>
							<l n="46" num="1.2">Barberousse s’abat au milieu des burgraves,</l>
							<l n="47" num="1.3">Barthélemy rayonne, allumant son fanal,</l>
							<l n="48" num="1.4">Cloué, dernier pamphlet, à son dernier journal !</l>
							<l n="49" num="1.5">Sa muse a, réveillant la satire latine,</l>
							<l n="50" num="1.6">Comme un titan vaincu foudroyé Lamartine ;</l>
							<l n="51" num="1.7">Pareille aux grands parleurs d’Homère et de Hugo,</l>
							<l n="52" num="1.8">Des rocs du feuilleton, la dure virago</l>
							<l n="53" num="1.9">Sur ce cygne plus doux que les cygnes d’Athènes</l>
							<l n="54" num="1.10">Fait couler à grand bruit ces paroles hautaines :</l>
							<l n="55" num="1.11">« Rimeur, que viens-tu faire au milieu du forum ?</l>
							<l n="56" num="1.12">Cet acte audacieux blesse le décorum.</l>
							<l n="57" num="1.13">Reste avec tes pareils ! Les gens de ta séquelle</l>
							<l n="58" num="1.14">Ne sont bons qu’à rimer une ode, telle quelle !</l>
							<l n="59" num="1.15">Tu chantes l’avenir ! Le présent est meilleur.</l>
							<l n="60" num="1.16">Ce qui te convenait, ô divin rimailleur,</l>
							<l n="61" num="1.17">C’était, ambitieux du laurier de Pindare,</l>
							<l n="62" num="1.18">D’aller au mont Horeb pincer de la guitare</l>
							<l n="63" num="1.19">Pour ton roi légitime, ou plutôt d’arranger</l>
							<l n="64" num="1.20">Des vers de confiseur au <hi rend="ital">fidèle-berger. </hi></l>
							<l n="65" num="1.21">Mais ta loi sociale est une rocambole,</l>
							<l n="66" num="1.22">Et Fourier n’est qu’un âne à côté de Chambolle.</l>
							<l n="67" num="1.23">Tombe ! Et le front meurtri par mon divin talon,</l>
							<l n="68" num="1.24">Souviens-toi désormais d’admirer Odilon. »</l>
							<l n="69" num="1.25">Ainsi par ses gros vers, Némésis-Astronome,</l>
							<l n="70" num="1.26">Du poëte sacré, déjà plus grand qu’un homme,</l>
							<l n="71" num="1.27">A brisé fièrement les efforts superflus.</l>
						</lg>
					</sp>
					<sp n="9">
						<speaker>Moi.</speaker>
						<lg n="1">

						<l part="I" n="72" num="1.1">Tiens ! Je n’en savais rien. </l>
						</lg>
					</sp>
					<sp n="10">
						<speaker>Évohé.</speaker>
						<lg n="1">

						<l part="F" n="72">Lamartine non plus.</l>
							<l n="73" num="1.1">Bois, ô mon jeune amant ! Les larmes que je pleure,</l>
							<l n="74" num="1.2">Si Némésis renaît, il faut donc que je meure ?</l>
						</lg>
					</sp>
					<sp n="11">
						<speaker>Moi.</speaker>
						<lg n="1">
							<l n="75" num="1.1">Ta lèvre a le parfum du rosier d’Orient</l>
							<l n="76" num="1.2">Où l’aurore a caché ses perles en riant ;</l>
							<l n="77" num="1.3">Cette bouche folâtre est pleine de féeries,</l>
							<l n="78" num="1.4">Et, comme un voyageur dans des plaines fleuries,</l>
							<l n="79" num="1.5">Mon cœur s’est égaré parmi ses purs contours.</l>
						</lg>
					</sp>
					<sp n="12">
						<speaker>Évohé.</speaker>
						<lg n="1">
							<l n="80" num="1.1">Si je chantais encor, m’aimeriez-vous toujours ?</l>
						</lg>
					</sp>
					<sp n="13">
						<speaker>Moi.</speaker>
						<lg n="1">
							<l n="81" num="1.1">Eh ! Que nous fait à nous Némésis-Astronome ?</l>
							<l n="82" num="1.2">Nous, et Barthélemy que le siècle renomme,</l>
							<l n="83" num="1.3">Nous avons deux tréteaux dressés sous le ciel bleu,</l>
							<l n="84" num="1.4">Deux magasins d’esprit : le sien ressemble à feu</l>
							<l n="85" num="1.5">Le théâtre-français ; une loque de toile</l>
							<l n="86" num="1.6">Y représente Rome ou bien l’arc-de-l’étoile,</l>
							<l n="87" num="1.7">Au choix. Sur le devant, de lourds alexandrins,</l>
							<l n="88" num="1.8">Portant tout le harnois classique sur les reins,</l>
							<l n="89" num="1.9">Casaques abricot, casques de tragédie,</l>
							<l n="90" num="1.10">Déclament, et s’en vont quand on les congédie :</l>
							<l n="91" num="1.11">Ce genre sérieux n’a pas un grand succès ;</l>
							<l n="92" num="1.12">On y bâille parfois, mais c’est l’esprit français ;</l>
							<l n="93" num="1.13">Cela craque partout, mais c’est la bonne école,</l>
							<l n="94" num="1.14">Et cela tient toujours avec un peu de colle.</l>
							<l n="95" num="1.15">Si quelque spectateur pourtant semble fâché,</l>
							<l n="96" num="1.16">On lui répond : Voltaire ! Et le mot est lâché.</l>
							<l n="97" num="1.17">Mais nous, nous travaillons pour un public folâtre.</l>
							<l n="98" num="1.18">En haillons ! En plein vent ! Nous sommes le théâtre</l>
							<l n="99" num="1.19">À quatre sous, un bouge. Aux regards des titis</l>
							<l n="100" num="1.20">Nous offrons éléphants, diables et ouistitis :</l>
							<l n="101" num="1.21">Dans notre drame bleu, la svelte colombine</l>
							<l n="102" num="1.22">A cent mille oripeaux pour cacher sa débine.</l>
							<l n="103" num="1.23">Ses paillettes d’argent et son vieux casaquin</l>
							<l n="104" num="1.24">Éblouissent encor ce filou d’arlequin ;</l>
							<l n="105" num="1.25">On y mord, et parfois la gorge peu sévère</l>
							<l n="106" num="1.26">Sort de la robe, et luit sous les colliers de verre.</l>
							<l n="107" num="1.27">Pour moi, sur ce théâtre où le bon goût n’est pas,</l>
							<l n="108" num="1.28">Paillasse enfariné, je m’escrime à grands pas ;</l>
							<l n="109" num="1.29">Et quand le vieux Cassandre y passe à l’étourdie,</l>
							<l n="110" num="1.30">Au lieu de feindre un peu, comme la tragédie,</l>
							<l n="111" num="1.31">De percer d’un poignard ce farouche barbon,</l>
							<l n="112" num="1.32">Je lui donne des coups de trique, pour de bon !</l>
							<l n="113" num="1.33">Sur cette heureuse scène, on voit le saut de carpe</l>
							<l n="114" num="1.34">Après le saut de sourd ; et Rose, sans écharpe,</l>
							<l n="115" num="1.35">S’y montre à ce public trois fois intelligent,</l>
							<l n="116" num="1.36">Faisant la crapaudine au fond d’un plat d’argent.</l>
							<l n="117" num="1.37">La fée azur, tenant le diable par les cornes,</l>
							<l n="118" num="1.38">Y court dans son char d’or attelé de licornes ;</l>
							<l n="119" num="1.39">L’ange y dévore en scène un cervelas ; des feux</l>
							<l n="120" num="1.40">De Bengale, des feux charmants, roses et bleus,</l>
							<l n="121" num="1.41">Embrasent de rayons cette aimable folie,</l>
							<l n="122" num="1.42">Et l’on y voit passer Rosalinde et Célie !</l>
						</lg>
					</sp>
					<sp n="14">
						<speaker>Évohé.</speaker>
						<lg n="1">
							<l n="123" num="1.1">Eh bien ! Donc, à vos rangs, guignols et bilboquets !</l>
							<l n="124" num="1.2">Ouvrons la grande porte ! Allumons les quinquets !</l>
							<l n="125" num="1.3">Mets ton collier de strass, reine de Trébizonde !</l>
							<l n="126" num="1.4">Entrez, entrez, messieurs ! Entrez ! Suivez le monde !</l>
							<l n="127" num="1.5">Hurrah, la grosse caisse, en avant ! Patapoum !</l>
							<l n="128" num="1.6">Zizi, boumboum ! Zizi, boumboum ! Zizi, boumboum !</l>
							<l n="129" num="1.7">Venez voir <hi rend="ital">colombine et le génie, ou l’hydre</hi></l>
							<l n="130" num="1.8"><hi rend="ital">en mal d’enfant ! </hi>orgeat, de la bière, du cidre !</l>
							</lg>
					</sp>
					<closer>
						<dateline>
							<date when="1846">janvier 1846</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>