<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Odes funambulesques</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3888 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">BAN_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>

			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Odes funambulesques</title>
						<author>Théodore de BANVILLE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L922</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Odes funambulesques</title>
								<author>Théodore de BANVILLE</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>M. Levy</publisher>
									<date when="1859">1859</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>

			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres de Théodore de Banville : Odes funambulesques</title>
						<author>Théodore de BANVILLE</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre</publisher>
							<date when="1892">1892</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1857">1857</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-23" who="RR">Révision de l’entête pour validation</change>
			<change when="2016-03-14" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN128">
				<head type="main">MÉDITATION <lb></lb>POÉTIQUE LITTÉRAIRE</head>
				<lg n="1">
					<l n="1" num="1.1">On écrivait naguère, en ces temps romantiques</l>
					<l n="2" num="1.2">Où les chants de Ducis étaient des émétiques,</l>
					<l n="3" num="1.3">Où, sans pourpoint cinabre, on se voyait banni ;</l>
					<l n="4" num="1.4">Où prudhomme, ventru comme une calebasse,</l>
					<l n="5" num="1.5">Était jeté vivant dans une contre-basse</l>
					<l n="6" num="1.6">Pour avoir contesté les vers de <hi rend="ital">Hernani. </hi></l>
				</lg>
				<lg n="2">
					<l n="7" num="2.1">On écrivait, tandis que maintenant on gèle.</l>
					<l n="8" num="2.2">Où sont les <hi rend="ital">Antony, </hi>les <hi rend="ital">Ruy-Blas, </hi>les <hi rend="ital">Angèle, </hi></l>
					<l n="9" num="2.3"><space quantity="12" unit="char"></space>Et ces jours, morts hélas !</l>
					<l n="10" num="2.4">Où Frédérick, faisant revivre Aristophane,</l>
					<l n="11" num="2.5">Sous le mépris des sots et la robe d’un âne</l>
					<l n="12" num="2.6"><space quantity="12" unit="char"></space>Cachait Tragaldabas !</l>
				</lg>
				<lg n="3">
					<l n="13" num="3.1">On écrivait, au sein de l’antique Bohème</l>
					<l n="14" num="3.2">Où le chat de Mimi brillait sur le poëme,</l>
					<l n="15" num="3.3">Où Schaunard éperdu, dédaignant tout poncif,</l>
					<l n="16" num="3.4">Si quelqu’un devant lui vantait sa pipe blonde,</l>
					<l n="17" num="3.5">Lui répondait : « j’en ai pour aller dans le monde</l>
					<l n="18" num="3.6">Une plus belle encore, » et devenait pensif.</l>
				</lg>
				<lg n="4">
					<l n="19" num="4.1">Aujourd’hui Weill possède un bouchon de carafe,</l>
					<l n="20" num="4.2">Arsène a des maisons, Nadar est photographe,</l>
					<l n="21" num="4.3"><space quantity="12" unit="char"></space>Véron maître-saigneur,</l>
					<l n="22" num="4.4">Fournier construit des bricks de papier, et les mâte,</l>
					<l n="23" num="4.5">Henri La Madelène a fait du carton-pâte :</l>
					<l n="24" num="4.6"><space quantity="12" unit="char"></space>Lequel vaut mieux, seigneur ?</l>
				</lg>
				<closer>
					<dateline>
						<date when="1856">décembre 1856</date>.
					</dateline>
				</closer>
			</div></body></text></TEI>