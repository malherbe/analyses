<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Odes funambulesques</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3888 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">BAN_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>

			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Odes funambulesques</title>
						<author>Théodore de BANVILLE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L922</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Odes funambulesques</title>
								<author>Théodore de BANVILLE</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>M. Levy</publisher>
									<date when="1859">1859</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>

			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres de Théodore de Banville : Odes funambulesques</title>
						<author>Théodore de BANVILLE</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre</publisher>
							<date when="1892">1892</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1857">1857</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-23" who="RR">Révision de l’entête pour validation</change>
			<change when="2016-03-14" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ÉVOHÉ</head><head type="sub_part">NÉMÉSIS INTÉRIMAIRE</head><div type="poem" key="BAN82">
					<head type="form">SATIRE CINQUIÈME</head>
					<head type="main">L’AMOUR À PARIS</head>
					<lg n="1">
						<l n="1" num="1.1"><space quantity="2" unit="char"></space>Fille du grand Daumier ou du sublime Cham,</l>
						<l n="2" num="1.2">Toi qui portes du reps et du madapolam,</l>
						<l n="3" num="1.3">Ô muse de Paris ! Toi par qui l’on admire</l>
						<l n="4" num="1.4">Les peignoirs érudits qui naissent chez Palmyre,</l>
						<l n="5" num="1.5">Toi pour qui notre siècle inventa les <hi rend="ital">corsets </hi></l>
						<l n="6" num="1.6"><hi rend="ital">à la minute, </hi>amour du puff et du succès !</l>
						<l n="7" num="1.7">Toi qui chez la comtesse et chez la chambrière</l>
						<l n="8" num="1.8">Colportes Marivaux retouché par Barrière,</l>
						<l n="9" num="1.9">Précieuse Évohé ! Chante, après Gavarni,</l>
						<l n="10" num="1.10">L’amour et la constance en brodequin verni.</l>
						<l n="11" num="1.11"><space quantity="2" unit="char"></space>Dans ces pays lointains situés à dix lieues,</l>
						<l n="12" num="1.12">Où l’Oise dans la Seine épanche ses eaux bleues,</l>
						<l n="13" num="1.13">Parmi ces saharas récemment découverts,</l>
						<l n="14" num="1.14">Quand l’indigène ému voit passer dans nos vers</l>
						<l n="15" num="1.15">Ces mots déjà caducs : <hi rend="ital">rat, grisette </hi>ou <hi rend="ital">lorette,</hi></l>
						<l n="16" num="1.16">Il se sent vivre, un charme impérieux l’arrête,</l>
						<l n="17" num="1.17">Et, l’œil dans le ciel bleu, ce naturel naïf</l>
						<l n="18" num="1.18">Évacue un sonnet imité de Baïf.</l>
						<l n="19" num="1.19">Il voit dans le verger qu’il eut en patrimoine</l>
						<l n="20" num="1.20">Tourbillonner en chœur les cauchemars d’Antoine ;</l>
						<l n="21" num="1.21">Le voilà frémissant et rouge comme un coq ;</l>
						<l n="22" num="1.22">Il rêve, il doute, il songe, et tout son Paul de Kock</l>
						<l n="23" num="1.23">Lui revient en mémoire, et, pendant trois semaines,</l>
						<l n="24" num="1.24">Fait partir à ses yeux des chandelles romaines</l>
						<l n="25" num="1.25">Et dans son cœur troublé met tout en désarroi,</l>
						<l n="26" num="1.26">Comme un feu d’artifice à la fête du roi.</l>
						<l n="27" num="1.27"><space quantity="2" unit="char"></space>La grisette ! Il revoit la petite fenêtre.</l>
						<l n="28" num="1.28">Les rayons souriants du jour qui vient de naître,</l>
						<l n="29" num="1.29">À leur premier réveil, comme un cadre enchanteur,</l>
						<l n="30" num="1.30">Dorent les liserons et les pois de senteur.</l>
						<l n="31" num="1.31">Une tête charmante, un ange, une vignette</l>
						<l n="32" num="1.32">De ce gai reposoir agace la lorgnette.</l>
						<l n="33" num="1.33">En voyant de la rue un rire triomphant</l>
						<l n="34" num="1.34">Ouvrir des dents de perle, on dirait qu’un enfant</l>
						<l n="35" num="1.35">Ou quelque sylphe, épris de leurs touffes écloses,</l>
						<l n="36" num="1.36">A fait choir, en jouant, du lait parmi les roses.</l>
						<l n="37" num="1.37"><space quantity="2" unit="char"></space>Elle va se lacer en chantant sa chanson,</l>
						<l n="38" num="1.38"><hi rend="ital">Lisette </hi>ou <hi rend="ital">l’andalouse </hi>ou bien <hi rend="ital">mimi pinson, </hi></l>
						<l n="39" num="1.39">Puis tendre son bas blanc sur sa jambe plus blanche ;</l>
						<l n="40" num="1.40">Les plis du frais jupon vont embrasser sa hanche</l>
						<l n="41" num="1.41">Et cacher cent trésors, et du cachot de grès</l>
						<l n="42" num="1.42">La naïade aux yeux bleus glissera sans regrets</l>
						<l n="43" num="1.43">Sur sa folle poitrine et sur son col, que baigne</l>
						<l n="44" num="1.44">Un doux or délivré des morsures du peigne.</l>
						<l n="45" num="1.45">Ce poëme fini, dans un grossier réseau</l>
						<l n="46" num="1.46">Elle va becqueter son déjeuner d’oiseau,</l>
						<l n="47" num="1.47">Puis, son ouvrage en main, sur sa chaise de paille,</l>
						<l n="48" num="1.48">La folle va laisser, tandis qu’elle travaille,</l>
						<l n="49" num="1.49">L’aiguille aux dents d’acier mordre ses petits doigts ;</l>
						<l n="50" num="1.50">Et, comme un frais méandre égaré dans les bois,</l>
						<l n="51" num="1.51">Elle entrelacera, modeste poésie,</l>
						<l n="52" num="1.52">Les fleurs de son caprice et de sa fantaisie.</l>
						<l n="53" num="1.53"><space quantity="2" unit="char"></space>C’est ce que l’on appelle une brodeuse. Hélas !</l>
						<l n="54" num="1.54">Depuis qu’en retournant le sept de cœur ou l’as</l>
						<l n="55" num="1.55">Dans un estaminet, le premier journaliste</l>
						<l n="56" num="1.56">Contre les murs du beau dressa cette baliste,</l>
						<l n="57" num="1.57">Combien ces frais croquis, plus faux que des jetons,</l>
						<l n="58" num="1.58">Ont fait dans notre ciel errer de Phaétons !</l>
						<l n="59" num="1.59">La grisette, doux rêve ! Elle avait ses apôtres,</l>
						<l n="60" num="1.60">Balzac et Gavarni mentaient comme les autres ;</l>
						<l n="61" num="1.61">Mais un jour, Roqueplan, s’étant mis à l’affût,</l>
						<l n="62" num="1.62">Fit un mot de génie, et la <hi rend="ital">lorette </hi>fut !</l>
						<l n="63" num="1.63"><space quantity="2" unit="char"></space>Hurrah ! Les Aglaé ! Les Ida, les charmantes,</l>
						<l n="64" num="1.64">En avant ! Le champagne a baptisé les mantes !</l>
						<l n="65" num="1.65">Déchirons nos gants blancs au seuil de l’opéra !</l>
						<l n="66" num="1.66">Après, la maison-d’or ! Corinne chantera,</l>
						<l n="67" num="1.67">Et puis, nous ferons tous, comme c’est nécessaire,</l>
						<l n="68" num="1.68">Des mots qui paraîtront demain dans <hi rend="ital">le corsaire ! </hi></l>
						<l n="69" num="1.69">Des mots tout neufs, si bien arrachés au trépas</l>
						<l n="70" num="1.70">Qu’ils se rendent parfois, mais qu’ils ne meurent pas !</l>
						<l n="71" num="1.71">Écoutez Célina, reine de la folie,</l>
						<l n="72" num="1.72">Qui chante : <hi rend="ital">un général de l’armée d’Italie ! </hi></l>
						<l n="73" num="1.73">Ah ! Bravo ! C’est épique, on ne peut le nier.</l>
						<l n="74" num="1.74">Quel aplomb ! Je l’avais entendu l’an dernier.</l>
						<l n="75" num="1.75">Vive Aspasie ! Athène existe au sein des gaules !</l>
						<l n="76" num="1.76">Ah ! Nous avons vraiment les femmes les plus drôles</l>
						<l n="77" num="1.77">De Paris ! Périclès vit chez nous en exil,</l>
						<l n="78" num="1.78">Et nous nous amusons beaucoup. Quelle heure est-il ?</l>
						<l n="79" num="1.79"><space quantity="2" unit="char"></space>Évohé ! Toi qui sais le fond de ces arcanes,</l>
						<l n="80" num="1.80">Depuis la maison-d’or jusqu’au bureau des cannes,</l>
						<l n="81" num="1.81">Toi qui portas naguère avec assez d’ardeur</l>
						<l n="82" num="1.82">Le claque enrubanné du fameux débardeur,</l>
						<l n="83" num="1.83">Apparais ! Montre-nous, ô femme sibylline,</l>
						<l n="84" num="1.84">La pâle vérité nue et sans crinoline,</l>
						<l n="85" num="1.85">Et convaincs une fois les faiseurs de journaux</l>
						<l n="86" num="1.86">De complicité vile avec les Oudinots.</l>
						<l n="87" num="1.87"><space quantity="2" unit="char"></space>Descends jusques au fond de ces hontes immenses</l>
						<l n="88" num="1.88">Qui sont le paradis des auteurs de romances,</l>
						<l n="89" num="1.89">Dis-nous tous les détours de ces gouffres amers,</l>
						<l n="90" num="1.90">Et si la perle en feu rayonne au fond des mers,</l>
						<l n="91" num="1.91">Et quels monstres, avec leurs cent gueules ouvertes,</l>
						<l n="92" num="1.92">Attendent le nageur tombé dans les eaux vertes.</l>
						<l n="93" num="1.93">Mène-nous par la main au fond de ces tombeaux !</l>
						<l n="94" num="1.94">Montre ces jeunes corps si pâles et si beaux</l>
						<l n="95" num="1.95">D’où la beauté s’enfuit sans y laisser de trace !</l>
						<l n="96" num="1.96">Fais-nous voir la misère et l’impudeur sans grâce !</l>
						<l n="97" num="1.97">Parcours, en exhalant tes regrets superflus,</l>
						<l n="98" num="1.98">Ces beaux temples de l’âme où le dieu ne vit plus,</l>
						<l n="99" num="1.99">Sans craindre d’y salir ta cheville nacrée.</l>
						<l n="100" num="1.100">Tu peux entrer partout, car la muse est sacrée.</l>
						<l n="101" num="1.101"><space quantity="2" unit="char"></space>Mais du moins, Évohé, si la jeune Laïs,</l>
						<l n="102" num="1.102">Avec ses cheveux d’or, blonds comme le maïs,</l>
						<l n="103" num="1.103">N’enchaîne déjà plus son amant Diogène ;</l>
						<l n="104" num="1.104">Dans ces murs, d’où s’enfuit l’esprit avec la gêne,</l>
						<l n="105" num="1.105">Si leur Alcibiade et leur sage Phryné</l>
						<l n="106" num="1.106">Abandonnent déjà ce siècle nouveau-né,</l>
						<l n="107" num="1.107">Si dans notre Paris leur Athène est bien morte,</l>
						<l n="108" num="1.108">Dans les salons dorés où se tient à la porte</l>
						<l n="109" num="1.109">La noble courtoisie, il est plus d’un grand nom</l>
						<l n="110" num="1.110">Qui dérobe la grâce et l’esprit de Ninon.</l>
						<l n="111" num="1.111">Là, l’amour est un art comme la poésie :</l>
						<l n="112" num="1.112">Le caprice aux yeux verts, la rose fantaisie</l>
						<l n="113" num="1.113">Poussent la blanche nef que guident sur son lac</l>
						<l n="114" num="1.114">Anacréon, Ovide et le divin Balzac,</l>
						<l n="115" num="1.115">Et mènent sur ces flots, célébrés par Horace,</l>
						<l n="116" num="1.116">La volupté plus belle encore que la grâce !</l>
						<l n="117" num="1.117"><space quantity="2" unit="char"></space>Ô doux mensonge ! Avec tes ongles déjà longs,</l>
						<l n="118" num="1.118">Tâche d’égratigner la porte des salons,</l>
						<l n="119" num="1.119">Et peins-nous, s’il se peut, en paroles courtoises,</l>
						<l n="120" num="1.120">Les amours de duchesse et les amours bourgeoises !</l>
						<l n="121" num="1.121">Dis l’enfant chérubin tenant sur ses genoux</l>
						<l n="122" num="1.122">Sa marraine aujourd’hui moins sévère ; dis-nous</l>
						<l n="123" num="1.123">La nouvelle Phryné, lascive et dédaigneuse,</l>
						<l n="124" num="1.124">Instruisant les d’Espard après les Maufrigneuse ;</l>
						<l n="125" num="1.125">Dis-nous les nobles seins que froissent les talons</l>
						<l n="126" num="1.126">Des superbes chasseurs choisis pour étalons ;</l>
						<l n="127" num="1.127">Et comment <subst reason="analysis" type="completion" hand="RR"><del>Mess.....</del><add rend="hidden">aline</add></subst> encore extasiée,</l>
						<l n="128" num="1.128">Au matin rentre lasse et non rassasiée,</l>
						<l n="129" num="1.129">Pâle, essoufflée, en eau, suivant l’ombre du mur,</l>
						<l n="130" num="1.130">Tandis que son époux, orateur déjà mûr,</l>
						<l n="131" num="1.131">Dans son boudoir de pair désinfecté par l’ambre,</l>
						<l n="132" num="1.132">Interpelle un miroir en attendant la chambre !</l>
						<l n="133" num="1.133"><space quantity="2" unit="char"></space>Ah ! Posons nos deux mains sur notre cœur sanglant !</l>
						<l n="134" num="1.134">Ce n’est pas sans gémir qu’on cherche, en se troublant,</l>
						<l n="135" num="1.135">Quelle plaie ouvre encor, dans l’éternelle Troie</l>
						<l n="136" num="1.136">L’implacable Vénus attachée à sa proie !</l>
						<l n="137" num="1.137">Quand il parle d’amour sans pleurer et crier,</l>
						<l n="138" num="1.138">Le plus heureux de nous, quel que soit le laurier</l>
						<l n="139" num="1.139">Ou le myrte charmant dont sa tête se ceigne,</l>
						<l n="140" num="1.140">Sent grincer à son flanc la blessure qui saigne,</l>
						<l n="141" num="1.141">Et se plaindre et frémir avec un ris moqueur,</l>
						<l n="142" num="1.142">L’ouragan du passé dans les flots de son cœur !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1846">février 1846</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>