<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Odes funambulesques</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3888 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">BAN_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>

			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Odes funambulesques</title>
						<author>Théodore de BANVILLE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L922</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Odes funambulesques</title>
								<author>Théodore de BANVILLE</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>M. Levy</publisher>
									<date when="1859">1859</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>

			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres de Théodore de Banville : Odes funambulesques</title>
						<author>Théodore de BANVILLE</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre</publisher>
							<date when="1892">1892</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1857">1857</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-23" who="RR">Révision de l’entête pour validation</change>
			<change when="2016-03-14" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN121">
				<head type="main">CHANSON SUR L’AIR DES LANDRIRY</head>
				<lg n="1">
					<l n="1" num="1.1">Voici l’automne revenu.</l>
					<l n="2" num="1.2">Nos anges, sur un air connu,</l>
					<l rhyme="none" n="3" num="1.3"><space quantity="8" unit="char"></space>Landrirette,</l>
					<l n="4" num="1.4">Arrivent toutes à Paris,</l>
					<l n="5" num="1.5"><space quantity="8" unit="char"></space>Landriry.</l>
				</lg>
				<lg n="2">
					<l n="6" num="2.1">Ces dames, au retour des champs,</l>
					<l n="7" num="2.2">Auront les yeux clairs et méchants</l>
					<l rhyme="none" n="8" num="2.3"><space quantity="8" unit="char"></space>Landrirette,</l>
					<l n="9" num="2.4">Le sein rose et le teint fleuri,</l>
					<l n="10" num="2.5"><space quantity="8" unit="char"></space>Landriry.</l>
				</lg>
				<lg n="3">
					<l n="11" num="3.1">Mais celles qui n’ont pas quitté</l>
					<l n="12" num="3.2">La capitale pour l’été,</l>
					<l rhyme="none" n="13" num="3.3"><space quantity="8" unit="char"></space>Landrirette,</l>
					<l n="14" num="3.4">Ont l’air bien triste et bien marri,</l>
					<l n="15" num="3.5"><space quantity="8" unit="char"></space>Landriry.</l>
				</lg>
				<lg n="4">
					<l n="16" num="4.1">Nos aspasie et nos sontag</l>
					<l n="17" num="4.2">Se promènent au ranelagh</l>
					<l rhyme="none" n="18" num="4.3"><space quantity="8" unit="char"></space>Landrirette,</l>
					<l n="19" num="4.4">Tristes comme un bonnet de nuit,</l>
					<l n="20" num="4.5"><space quantity="8" unit="char"></space>Landriry.</l>
				</lg>
				<lg n="5">
					<l n="21" num="5.1">Elles ont vu fort tristement</l>
					<l n="22" num="5.2">La clôture du parlement,</l>
					<l rhyme="none" n="23" num="5.3"><space quantity="8" unit="char"></space>Landrirette,</l>
					<l n="24" num="5.4">Leurs roses tournent en soucis,</l>
					<l n="25" num="5.5"><space quantity="8" unit="char"></space>Landriry.</l>
				</lg>
				<lg n="6">
					<l n="26" num="6.1">Il est temps que plus d’un banquier</l>
					<l n="27" num="6.2">Quitte Le Havre ou Villequier,</l>
					<l rhyme="none" n="28" num="6.3"><space quantity="8" unit="char"></space>Landrirette,</l>
					<l n="29" num="6.4">Car notre pactole est tari,</l>
					<l n="30" num="6.5"><space quantity="8" unit="char"></space>Landriry.</l>
				</lg>
				<lg n="7">
					<l n="31" num="7.1">Frison, naïs et brancador</l>
					<l n="32" num="7.2">Ont engagé leurs colliers d’or,</l>
					<l rhyme="none" n="33" num="7.3"><space quantity="8" unit="char"></space>Landrirette,</l>
					<l n="34" num="7.4">Et souris n’a plus de mari,</l>
					<l n="35" num="7.5"><space quantity="8" unit="char"></space>Landriry.</l>
				</lg>
				<lg n="8">
					<l n="36" num="8.1">Mais voici le temps des moineaux ;</l>
					<l n="37" num="8.2">Les vacances des tribunaux</l>
					<l rhyme="none" n="38" num="8.3"><space quantity="8" unit="char"></space>Landrirette,</l>
					<l n="39" num="8.4">Vont ramener l’argent ici,</l>
					<l n="40" num="8.5"><space quantity="8" unit="char"></space>Landriry.</l>
				</lg>
				<lg n="9">
					<l n="41" num="9.1">Car déjà, sur le boulevard,</l>
					<l n="42" num="9.2">On voit des habits de Stuttgard</l>
					<l rhyme="none" n="43" num="9.3"><space quantity="8" unit="char"></space>Landrirette,</l>
					<l n="44" num="9.4">Et des vestes de Clamecy,</l>
					<l n="45" num="9.5"><space quantity="8" unit="char"></space>Landriry.</l>
				</lg>
				<lg n="10">
					<l n="46" num="10.1">Tout cela vient avec l’espoir</l>
					<l n="47" num="10.2">D’aller à Mabille, et de voir</l>
					<l rhyme="none" n="48" num="10.3"><space quantity="8" unit="char"></space>Landrirette,</l>
					<l n="49" num="10.4">Page et Mademoiselle Ozy,</l>
					<l n="50" num="10.5"><space quantity="8" unit="char"></space>Landriry.</l>
				</lg>
				<lg n="11">
					<l n="51" num="11.1">Le matin, avec bonne foi,</l>
					<l n="52" num="11.2">Ils tombent au café de Foy,</l>
					<l rhyme="none" n="53" num="11.3"><space quantity="8" unit="char"></space>Landrirette,</l>
					<l n="54" num="11.4">Pour lire <hi rend="ital">le charivari, </hi></l>
					<l n="55" num="11.5"><space quantity="8" unit="char"></space>Landriry.</l>
				</lg>
				<lg n="12">
					<l n="56" num="12.1">Puis ils s’en vont, à leur grand dam,</l>
					<l n="57" num="12.2">Acquérir sur la foi de Cham,</l>
					<l rhyme="none" n="58" num="12.3"><space quantity="8" unit="char"></space>Landrirette,</l>
					<l n="59" num="12.4">Des jaquettes gris de souris,</l>
					<l n="60" num="12.5"><space quantity="8" unit="char"></space>Landriry.</l>
				</lg>
				<lg n="13">
					<l n="61" num="13.1">Un Moulinois de mes cousins</l>
					<l n="62" num="13.2">Contemple tous les magasins,</l>
					<l rhyme="none" n="63" num="13.3"><space quantity="8" unit="char"></space>Landrirette,</l>
					<l n="64" num="13.4">Avec un sourire ébahi,</l>
					<l n="65" num="13.5"><space quantity="8" unit="char"></space>Landriry.</l>
				</lg>
				<lg n="14">
					<l n="66" num="14.1">Et déjà, ce nouvel Hassan</l>
					<l n="67" num="14.2">Guigne un cachemire <hi rend="ital">au persan, </hi></l>
					<l rhyme="none" n="68" num="14.3"><space quantity="8" unit="char"></space>Landrirette,</l>
					<l n="69" num="14.4">C’est pour charmer quelque péri,</l>
					<l n="70" num="14.5"><space quantity="8" unit="char"></space>Landriry.</l>
				</lg>
				<lg n="15">
					<l n="71" num="15.1">Il ira ce soir à Feydeau.</l>
					<l n="72" num="15.2">Avant le lever du rideau,</l>
					<l rhyme="none" n="73" num="15.3"><space quantity="8" unit="char"></space>Landrirette,</l>
					<l n="74" num="15.4">Il s’écriera : « c’est du Grétry,</l>
					<l n="75" num="15.5"><space quantity="8" unit="char"></space>Landriry ! »</l>
				</lg>
				<lg n="16">
					<l n="76" num="16.1">Courage amours, souvent frôlés !</l>
					<l n="77" num="16.2">Demain les bijoux contrôlés</l>
					<l rhyme="none" n="78" num="16.3"><space quantity="8" unit="char"></space>Landrirette,</l>
					<l n="79" num="16.4">Se placeront à juste prix,</l>
					<l n="80" num="16.5"><space quantity="8" unit="char"></space>Landriry.</l>
				</lg>
				<lg n="17">
					<l n="81" num="17.1">Bon appétit, jeunes beautés,</l>
					<l n="82" num="17.2">Qu’adorent les prêtres bottés</l>
					<l rhyme="none" n="83" num="17.3"><space quantity="8" unit="char"></space>Landrirette,</l>
					<l n="84" num="17.4">De Cypris et de Brididi,</l>
					<l n="85" num="17.5"><space quantity="8" unit="char"></space>Landriry.</l>
				</lg>
				<lg n="18">
					<l n="86" num="18.1">Vous allez guérir derechef</l>
					<l n="87" num="18.2">Par l’or et le papier joseph,</l>
					<l rhyme="none" n="88" num="18.3"><space quantity="8" unit="char"></space>Landrirette,</l>
					<l n="89" num="18.4">Vos roses et vos lys flétris,</l>
					<l n="90" num="18.5"><space quantity="8" unit="char"></space>Landriry.</l>
				</lg>
				<lg n="19">
					<l n="91" num="19.1">Si vous savez d’un air vainqueur</l>
					<l n="92" num="19.2">Mettre sur votre bouche en cœur</l>
					<l rhyme="none" n="93" num="19.3"><space quantity="8" unit="char"></space>Landrirette,</l>
					<l n="94" num="19.4">Les jeux, les ris et les souris,</l>
					<l n="95" num="19.5"><space quantity="8" unit="char"></space>Landriry.</l>
				</lg>
				<lg n="20">
					<l n="96" num="20.1">Si vous savez, à chaque pas,</l>
					<l n="97" num="20.2">Murmurer : « je ne polke pas,</l>
					<l rhyme="none" n="98" num="20.3"><space quantity="8" unit="char"></space>Landrirette, »</l>
					<l n="99" num="20.4">Vous allez gagner vos paris,</l>
					<l n="100" num="20.5"><space quantity="8" unit="char"></space>Landriry.</l>
				</lg>
				<lg n="21">
					<l n="101" num="21.1">Vous allez avoir des pompons,</l>
					<l n="102" num="21.2">Des fleurettes et des jupons</l>
					<l rhyme="none" n="103" num="21.3"><space quantity="8" unit="char"></space>Landrirette,</l>
					<l n="104" num="21.4">Comme en portait la Dubarry,</l>
					<l n="105" num="21.5"><space quantity="8" unit="char"></space>Landriry.</l>
				</lg>
				<lg n="22">
					<l n="106" num="22.1">Vous aurez, comme en un sérail,</l>
					<l n="107" num="22.2">Plus de perles et de corail,</l>
					<l rhyme="none" n="108" num="22.3"><space quantity="8" unit="char"></space>Landrirette,</l>
					<l n="109" num="22.4">Qu’un marchand de Pondichéry,</l>
					<l n="110" num="22.5"><space quantity="8" unit="char"></space>Landriry.</l>
				</lg>
				<lg n="23">
					<l n="111" num="23.1">Plus d’étoiles en diamant</l>
					<l n="112" num="23.2">Qu’il ne s’en trouve au firmament</l>
					<l rhyme="none" n="113" num="23.3"><space quantity="8" unit="char"></space>Landrirette,</l>
					<l n="114" num="23.4">Ou dans un roman de Méry,</l>
					<l n="115" num="23.5"><space quantity="8" unit="char"></space>Landriry.</l>
				</lg>
				<lg n="24">
					<l n="116" num="24.1">Et cet hiver à l’opéra,</l>
					<l n="117" num="24.2">Où quelque Amadis vous paiera,</l>
					<l rhyme="none" n="118" num="24.3"><space quantity="8" unit="char"></space>Landrirette,</l>
					<l n="119" num="24.4">Vous poserez pour Gavarni,</l>
					<l n="120" num="24.5"><space quantity="8" unit="char"></space>Landriry.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1846">septembre 1846</date>.
					</dateline>
				</closer>
			</div></body></text></TEI>