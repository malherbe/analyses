<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Odes funambulesques</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3888 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">BAN_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>

			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Odes funambulesques</title>
						<author>Théodore de BANVILLE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L922</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Odes funambulesques</title>
								<author>Théodore de BANVILLE</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>M. Levy</publisher>
									<date when="1859">1859</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>

			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres de Théodore de Banville : Odes funambulesques</title>
						<author>Théodore de BANVILLE</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre</publisher>
							<date when="1892">1892</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1857">1857</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-23" who="RR">Révision de l’entête pour validation</change>
			<change when="2016-03-14" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN73">
				<head type="main">LA VILLE ENCHANTÉE</head>
				<lg n="1">
					<l n="1" num="1.1">Il est de par le monde une cité bizarre,</l>
					<l n="2" num="1.2">Où Plutus en gants blancs, drapé dans son manteau,</l>
					<l n="3" num="1.3">Offre une cigarette à son ami Lazare,</l>
					<l n="4" num="1.4">Et l’emmène souper dans un parc de Wateau.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Les centaures fougueux y portent des badines ;</l>
					<l n="6" num="2.2">Et les dragons, au lieu de garder leur trésor,</l>
					<l n="7" num="2.3">S’en vont sur le minuit, avec des baladines,</l>
					<l n="8" num="2.4">Faire un maigre dîner dans une maison d’or.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">C’est là que parle et chante avec des voix si douces,</l>
					<l n="10" num="3.2">Un essaim de beautés plus nombreuses cent fois,</l>
					<l n="11" num="3.3">En habit de satin, brunes, blondes et rousses,</l>
					<l n="12" num="3.4">Que le nombre infini des feuilles dans les bois !</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">Ô pourpres et blancheurs ! Neiges et rosiers ! L’une</l>
					<l n="14" num="4.2">En découvrant son sein plus blanc que la Jung-Frau,</l>
					<l n="15" num="4.3">Cause avec Cyrano, qui revient de la lune,</l>
					<l n="16" num="4.4">L’autre prend une glace avec Cagliostro.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1">C’est le pays de fange et de nacre de perle ;</l>
					<l n="18" num="5.2">Un tréteau sur les fûts du cabaret prochain,</l>
					<l n="19" num="5.3">Spectacle où les décors sont peints par Diéterle,</l>
					<l n="20" num="5.4">Cambon, Thierry, Séchan, Philastre et Despléchin ;</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1">Un théâtre en plein vent, où, le long de la rue,</l>
					<l n="22" num="6.2">Passe, tantôt de face et tantôt de profil,</l>
					<l n="23" num="6.3">Un mimodrame avec des changements à vue,</l>
					<l n="24" num="6.4">Comme ceux de Gringoire et du céleste Will.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1">Là, depuis Idalie, où Cypris court sur l’onde</l>
					<l n="26" num="7.2">Dans un brougham de nacre attelé d’un dauphin,</l>
					<l n="27" num="7.3">Vous voyez défiler tous les pays du monde</l>
					<l n="28" num="7.4">Avec un air connu, comme chez Séraphin.</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1">La belle au bois dormant, sur la moire fleurie</l>
					<l n="30" num="8.2">De la molle ottomane où rêve le chat Murr,</l>
					<l n="31" num="8.3">Parmi l’air rose et bleu des feux de la féerie</l>
					<l n="32" num="8.4">S’éveille après cent ans sous un baiser d’amour.</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1">La chinoise rêveuse assise dans sa jonque,</l>
					<l n="34" num="9.2">Les yeux peints, et les bras ceints de perles d’Ophir,</l>
					<l n="35" num="9.3">D’un ongle de rubis rose comme une conque</l>
					<l n="36" num="9.4">Agace sur son front un oiseau de saphir.</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1">Sous le ciel étoilé, trempant leurs pieds dans l’onde</l>
					<l n="38" num="10.2">Que parfument la brise et le gazon fleuri,</l>
					<l n="39" num="10.3">Et d’un bois de senteur couvrant leur gorge blonde,</l>
					<l n="40" num="10.4">Dansent à s’enivrer les bibiaderi.</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1">Là, belles des blancheurs de la pâle chlorose,</l>
					<l n="42" num="11.2">Et confiant au soir les rougeurs des aveux,</l>
					<l n="43" num="11.3">Les vierges de Lesbos vont sous le laurier-rose</l>
					<l n="44" num="11.4">S’accroupir dans le sable et causer deux à deux.</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1">La reine Cléopâtre, en sa peine secrète,</l>
					<l n="46" num="12.2">Fière de la morsure attachée à son flanc,</l>
					<l n="47" num="12.3">Laisse tomber sa perle au fond du vin de Crète,</l>
					<l n="48" num="12.4">Et sa pourpre et sa lèvre ont des lueurs de sang.</l>
				</lg>
				<lg n="13">
					<l n="49" num="13.1">Voici les beaux palais où sont les hétaïres,</l>
					<l n="50" num="13.2">Sveltes lys de Corinthe et roses de Milet,</l>
					<l n="51" num="13.3">Qui, dans des bains de marbre, au chant divin des lyres,</l>
					<l n="52" num="13.4">Lavent leurs corps sans tache avec un flot de lait.</l>
				</lg>
				<lg n="14">
					<l n="53" num="14.1">Au fond de ces séjours à pompe triomphale,</l>
					<l n="54" num="14.2">Où l’or met des rayons dans les yeux éblouis,</l>
					<l n="55" num="14.3">Hercule enrubanné file aux genoux d’Omphale.</l>
					<l n="56" num="14.4">Et Diogène dort sur le sein de Laïs.</l>
				</lg>
				<lg n="15">
					<l n="57" num="15.1">Salut, jardin antique, ô Tempé familière</l>
					<l n="58" num="15.2">Où le grand Arouet a chanté Pompadour,</l>
					<l n="59" num="15.3">Où passaient avant eux Louis et La Vallière,</l>
					<l n="60" num="15.4">La lèvre humide encor de cent baisers d’amour !</l>
				</lg>
				<lg n="16">
					<l n="61" num="16.1">C’est là que soupiraient aux pieds de la dryade,</l>
					<l n="62" num="16.2">Dans la nuit bleue, à l’heure où sonne l’angelus,</l>
					<l n="63" num="16.3">Et le jeune Lauzun, fier comme Alcibiade,</l>
					<l n="64" num="16.4">Et le vieux Richelieu, beau comme Antinoüs.</l>
				</lg>
				<lg n="17">
					<l n="65" num="17.1">Mais, ce qui me séduit, et ce qui me ramène</l>
					<l n="66" num="17.2">Dans la verdure, où j’aime à soupirer le soir,</l>
					<l n="67" num="17.3">Ce n’est pas seulement Phyllis et Dorimène,</l>
					<l n="68" num="17.4">Avec sa robe d’or que porte un page noir.</l>
				</lg>
				<lg n="18">
					<l n="69" num="18.1">C’est là que vit encor le peuple des statues</l>
					<l n="70" num="18.2">Sous ses palais taillés dans les mélèzes verts,</l>
					<l n="71" num="18.3">Et que le chœur charmant des nymphes demi-nues</l>
					<l n="72" num="18.4">Pleure et gémit avec la brise des hivers.</l>
				</lg>
				<lg n="19">
					<l n="73" num="19.1">Les naïades sans yeux regardent les grands arbres</l>
					<l n="74" num="19.2">Pousser de longs rameaux qui blessent leurs beaux seins,</l>
					<l n="75" num="19.3">Et, sur ces seins meurtris croisant leurs bras de marbres,</l>
					<l n="76" num="19.4">Augmentent d’un ruisseau les larmes des bassins.</l>
				</lg>
				<lg n="20">
					<l n="77" num="20.1">Aujourd’hui les wagons, dans ces steppes fleuries</l>
					<l n="78" num="20.2">Devancent l’hirondelle en prenant leur essor,</l>
					<l n="79" num="20.3">Et coupent dans leur vol ces suaves prairies,</l>
					<l n="80" num="20.4">Sur un ruban de fer qui borde un chemin d’or.</l>
				</lg>
				<lg n="21">
					<l n="81" num="21.1">Ailleurs, c’est le palais d’Italie et de Grèce</l>
					<l n="82" num="21.2">Où règnent des bergers et des dieux demi-nus,</l>
					<l n="83" num="21.3">Pour lequel Titien a donné sa maîtresse,</l>
					<l n="84" num="21.4">Où Phidias a mis les siennes, ses Vénus !</l>
				</lg>
				<lg n="22">
					<l n="85" num="22.1">Et maintenant, voici la coupole féerique</l>
					<l n="86" num="22.2">Où, près des flots d’argent, sous les lauriers en fleurs,</l>
					<l n="87" num="22.3">Le grand Orphée apporte à la Grèce lyrique</l>
					<l n="88" num="22.4">La lyre que Sappho baignera dans les pleurs.</l>
				</lg>
				<lg n="23">
					<l n="89" num="23.1">Ô ville où le flambeau de l’univers s’allume !</l>
					<l n="90" num="23.2">Aurore dont l’œil bleu, rempli d’illusions,</l>
					<l n="91" num="23.3">Tourné vers l’orient, voit passer dans sa brume</l>
					<l n="92" num="23.4">Des foyers de splendeur étoilés de rayons !</l>
				</lg>
				<lg n="24">
					<l n="93" num="24.1">Ce théâtre en plein vent bâti dans les étoiles,</l>
					<l n="94" num="24.2">Où passent à la fois Cléopâtre et Lola,</l>
					<l n="95" num="24.3">Où défile en dansant, devant les mêmes toiles,</l>
					<l n="96" num="24.4">Un peuple chimérique en habit de gala ;</l>
				</lg>
				<lg n="25">
					<l n="97" num="25.1">Ce pays de soleil, d’or et de terre glaise,</l>
					<l n="98" num="25.2">Cette étrange cité, c’est Athène ou Paris,</l>
					<l n="99" num="25.3">Eldorado du monde, où la fashion anglaise</l>
					<l n="100" num="25.4">Importe deux fois l’an ses tweeds et ses paris.</l>
				</lg>
				<lg n="26">
					<l n="101" num="26.1">Pour moi, c’est dans un coin du salon d’Aspasie,</l>
					<l n="102" num="26.2">Sur l’album électrique où, parmi nos refrains,</l>
					<l n="103" num="26.3">Phidias et Diaz ont mis leur fantaisie,</l>
					<l n="104" num="26.4">Que je rime cette ode en vers alexandrins.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1845">septembre 1845</date>.
					</dateline>
				</closer>
			</div></body></text></TEI>