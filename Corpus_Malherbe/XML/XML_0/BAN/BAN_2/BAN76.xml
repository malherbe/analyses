<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Odes funambulesques</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3888 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">BAN_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>

			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Odes funambulesques</title>
						<author>Théodore de BANVILLE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L922</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Odes funambulesques</title>
								<author>Théodore de BANVILLE</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>M. Levy</publisher>
									<date when="1859">1859</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>

			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres de Théodore de Banville : Odes funambulesques</title>
						<author>Théodore de BANVILLE</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre</publisher>
							<date when="1892">1892</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1857">1857</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-23" who="RR">Révision de l’entête pour validation</change>
			<change when="2016-03-14" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN76">
				<head type="main">PREMIER SOLEIL</head>
				<lg n="1">
					<l n="1" num="1.1">Italie, Italie, ô terre où toutes choses</l>
					<l n="2" num="1.2">Frissonnent de soleil, hormis tes méchants vins !</l>
					<l n="3" num="1.3">Paradis où l’on trouve avec les lauriers-roses</l>
					<l n="4" num="1.4">Des sorbets à la neige et des ballets divins !</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Terre où le doux langage est rempli de diphthongues !</l>
					<l n="6" num="2.2">Voici qu’on pense à toi, car voici venir mai,</l>
					<l n="7" num="2.3">Et nous ne verrons plus les redingotes longues</l>
					<l n="8" num="2.4">Où tout parfait dandy se tenait enfermé.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Sourire du printemps, je t’offre en holocauste</l>
					<l n="10" num="3.2">Les manchons, les albums et le pesant castor.</l>
					<l n="11" num="3.3">Hurrah ! Gais postillons, que les chaises de poste</l>
					<l n="12" num="3.4">Volent, en agitant une poussière d’or !</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">Les lilas vont fleurir, et Ninon me querelle,</l>
					<l n="14" num="4.2">Et ce matin j’ai vu Mademoiselle Ozy</l>
					<l n="15" num="4.3">Près des panoramas déployer son ombrelle :</l>
					<l n="16" num="4.4">C’est que le triste hiver est bien mort, songez-y !</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1">Voici dans le gazon les corolles ouvertes,</l>
					<l n="18" num="5.2">Le parfum de la sève embaumera les soirs,</l>
					<l n="19" num="5.3">Et devant les cafés, des rangs de tables vertes</l>
					<l n="20" num="5.4">Ont par enchantement poussé sur les trottoirs.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1">Adieu donc, nuits en flamme où le bal s’extasie !</l>
					<l n="22" num="6.2">Adieu concerts, scotishs, glaces à l’ananas,</l>
					<l n="23" num="6.3">Fleurissez maintenant, fleurs de la fantaisie,</l>
					<l n="24" num="6.4">Sur la toile imprimée et sur le jaconas !</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1">Et vous, pour qui naîtra la saison des pervenches,</l>
					<l n="26" num="7.2">Rendez à ces zéphyrs que voilà revenus,</l>
					<l n="27" num="7.3">Les légers mantelets avec les robes blanches,</l>
					<l n="28" num="7.4">Et dans un mois d’ici vous sortirez bras nus !</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1">Bientôt, sous les forêts qu’argentera la lune,</l>
					<l n="30" num="8.2">S’envolera gaîment la nouvelle chanson ;</l>
					<l n="31" num="8.3">Nous y verrons courir la rousse avec la brune,</l>
					<l n="32" num="8.4">Et Musette et Nichette avec Mimi Pinson !</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1">Bientôt tu t’enfuiras, ange mélancolie,</l>
					<l n="34" num="9.2">Et dans le bas-meudon les bosquets seront verts.</l>
					<l n="35" num="9.3">Débouchez de ce vin que j’aime à la folie,</l>
					<l n="36" num="9.4">Et donnez-moi Ronsard, je veux lire des vers.</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1">Par ces premiers beaux jours la campagne est en fête</l>
					<l n="38" num="10.2">Ainsi qu’une épousée, et Paris est charmant.</l>
					<l n="39" num="10.3">Chantez, petits oiseaux du ciel, et toi, poëte,</l>
					<l n="40" num="10.4">Parle ! Nous t’écoutons avec ravissement.</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1">C’est le temps où l’on mène une jeune maîtresse</l>
					<l n="42" num="11.2">Cueillir la violette avec ses petits doigts,</l>
					<l n="43" num="11.3">Et toute créature a le cœur plein d’ivresse</l>
					<l n="44" num="11.4">Excepté les pervers et les marchands de bois !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1854">avril 1854</date>.
					</dateline>
				</closer>
			</div></body></text></TEI>