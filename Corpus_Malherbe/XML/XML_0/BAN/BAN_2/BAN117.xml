<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Odes funambulesques</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3888 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">BAN_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>

			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Odes funambulesques</title>
						<author>Théodore de BANVILLE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L922</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Odes funambulesques</title>
								<author>Théodore de BANVILLE</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>M. Levy</publisher>
									<date when="1859">1859</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>

			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres de Théodore de Banville : Odes funambulesques</title>
						<author>Théodore de BANVILLE</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre</publisher>
							<date when="1892">1892</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1857">1857</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-23" who="RR">Révision de l’entête pour validation</change>
			<change when="2016-03-14" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN117">
					<head type="main">À UN AMI POUR LUI RÉCLAMER <lb></lb>LE PRIX D’UN TRAVAIL LITTÉRAIRE</head>
					<lg n="1">
						<l n="1" num="1.1">Mon ami, n’allez pas surtout vous soucier</l>
						<l n="2" num="1.2"><space quantity="12" unit="char"></space>De la lettre qu’on vous apporte ;</l>
						<l n="3" num="1.3">Ce n’est qu’une facture, et c’est un créancier</l>
						<l n="4" num="1.4"><space quantity="12" unit="char"></space>Qui vient de sonner à la porte.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">Parcourant sans repos, dernier des voyageurs,</l>
						<l n="6" num="2.2"><space quantity="12" unit="char"></space>Les hélicons et les permesses,</l>
						<l n="7" num="2.3">Pour payer mes wagons, j’ai dû chez les changeurs</l>
						<l n="8" num="2.4"><space quantity="12" unit="char"></space>Escompter l’or de vos promesses.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">Vérité sans envers, que l’on nierait en vain,</l>
						<l n="10" num="3.2"><space quantity="12" unit="char"></space>Car elle est des plus apparentes,</l>
						<l n="11" num="3.3">L’artiste ne peut guère, avec son luth divin,</l>
						<l n="12" num="3.4"><space quantity="12" unit="char"></space>Réaliser assez de rente.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">Ainsi que la marmotte, il se sent mal au doigt</l>
						<l n="14" num="4.2"><space quantity="12" unit="char"></space>À force de porter sa chaîne :</l>
						<l n="15" num="4.3">Toujours il a mangé le matin ce qu’il doit</l>
						<l n="16" num="4.4"><space quantity="12" unit="char"></space>Toucher la semaine prochaine.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">À moins qu’il soit chasseur de dots, et fait au tour,</l>
						<l n="18" num="5.2"><space quantity="12" unit="char"></space>Dieu sait quelle intrigue il étale</l>
						<l n="19" num="5.3">Pour ne pas déjeuner, plus souvent qu’à son tour,</l>
						<l n="20" num="5.4"><space quantity="12" unit="char"></space>Au restaurant de feu Tantale !</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1">Moi qui n’ai pas les traits de Bacchus, je ne puis</l>
						<l n="22" num="6.2"><space quantity="12" unit="char"></space>Compter sur ma beauté physique.</l>
						<l n="23" num="6.3">Je suis comme la nymphe auguste dans son puits :</l>
						<l n="24" num="6.4"><space quantity="12" unit="char"></space>Je n’ai que ma boîte à musique !</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1">Ainsi, j’ai beau nommer l’amour <hi rend="ital">« my dear child, »</hi></l>
						<l n="26" num="7.2"><space quantity="12" unit="char"></space>Être un saint-George à nos escrimes,</l>
						<l n="27" num="7.3">Et faire encor pâlir le luxe de Rothschild</l>
						<l n="28" num="7.4"><space quantity="12" unit="char"></space>Par la richesse de mes rimes,</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1">Je ne saurais avec tous ces vers, que paiera</l>
						<l n="30" num="8.2"><space quantity="12" unit="char"></space>Buloz, s’il survit aux bagarres,</l>
						<l n="31" num="8.3">D’avance entretenir des filles d’opéra,</l>
						<l n="32" num="8.4"><space quantity="12" unit="char"></space>Ni même acheter des cigares.</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1">Oui, moi que l’univers prendrait pour un richard,</l>
						<l n="34" num="9.2"><space quantity="12" unit="char"></space>Tant je prodigue les tons roses,</l>
						<l n="35" num="9.3">Je suis, pour parler net, semblable à cabochard :</l>
						<l n="36" num="9.4"><space quantity="12" unit="char"></space>Je manque de diverses choses.</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1">Le cabaret prétend que crédit est noyé,</l>
						<l n="38" num="10.2"><space quantity="12" unit="char"></space>Et, si ce n’est chez les Osages,</l>
						<l n="39" num="10.3">Je m’aperçois enfin que l’argent monnoyé</l>
						<l n="40" num="10.4"><space quantity="12" unit="char"></space>S’applique à différents usages.</l>
					</lg>
					<lg n="11">
						<l n="41" num="11.1">Je sais bien que toujours les cygnes aux doux chants,</l>
						<l n="42" num="11.2"><space quantity="12" unit="char"></space>Près des lédas archiduchesses,</l>
						<l n="43" num="11.3">Ont fait de jolis mots sur les filles des champs</l>
						<l n="44" num="11.4"><space quantity="12" unit="char"></space>Et sur le mépris des richesses ;</l>
					</lg>
					<lg n="12">
						<l n="45" num="12.1">Monsieur Scribe lui-même enseigne qu’un trésor</l>
						<l n="46" num="12.2"><space quantity="12" unit="char"></space>Cause mille angoisses amères ;</l>
						<l n="47" num="12.3">Mais je suis intrépide : envoyez-moi de l’or,</l>
						<l n="48" num="12.4"><space quantity="12" unit="char"></space>Je n’ai souci que des chimères !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1856">mars 1856</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>