<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Odes funambulesques</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3888 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">BAN_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>

			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Odes funambulesques</title>
						<author>Théodore de BANVILLE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L922</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Odes funambulesques</title>
								<author>Théodore de BANVILLE</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>M. Levy</publisher>
									<date when="1859">1859</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>

			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres de Théodore de Banville : Odes funambulesques</title>
						<author>Théodore de BANVILLE</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre</publisher>
							<date when="1892">1892</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1857">1857</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-23" who="RR">Révision de l’entête pour validation</change>
			<change when="2016-03-14" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN124">
				<head type="main">BALLADE DES TRAVERS DE CE TEMPS</head>
				<lg n="1">
					<l n="1" num="1.1">Prudhomme, fier de montrer son bon goût,</l>
					<l n="2" num="1.2">Quand il écrit des lettres, les cachète</l>
					<l n="3" num="1.3">D’un casque d’or ou flotte un marabout ;</l>
					<l n="4" num="1.4">Camellia prend des airs de Nichette,</l>
					<l n="5" num="1.5">Et le docteur arbore une brochette.</l>
					<l n="6" num="1.6">Dès l’an passé, Montjoye eut ce travers</l>
					<l n="7" num="1.7">D’aller au bal en bottes à revers ;</l>
					<l n="8" num="1.8">Sur votre front Courbet met des verrues,</l>
					<l n="9" num="1.9">Nymphe aux yeux d’or, sirène aux cheveux verts ;</l>
					<l n="10" num="1.10">Voici le temps pour les coquecigrues.</l>
				</lg>
				<lg n="2">
					<l n="11" num="2.1">Anges bouffis et vermeils, que partout</l>
					<l n="12" num="2.2">L’humble passant peut appeler : « bichette, »</l>
					<l n="13" num="2.3">Dès que Plutus dresse quelque ragoût,</l>
					<l n="14" num="2.4">Cent Dalilas apportent leur fourchette.</l>
					<l n="15" num="2.5">Amour les guide au bruit de sa pochette.</l>
					<l n="16" num="2.6">Par le marteau forgé tout de travers,</l>
					<l n="17" num="2.7">C’est un jupon d’acier qui sert d’envers</l>
					<l n="18" num="2.8">Aux fiers appas de ces femmes ventrues,</l>
					<l n="19" num="2.9">Et ce rempart terrasse les pervers :</l>
					<l n="20" num="2.10">Voici le temps pour les coquecigrues.</l>
				</lg>
				<lg n="3">
					<l n="21" num="3.1">On n’a plus d’or que pour Edmond About</l>
					<l n="22" num="3.2">Au <hi rend="ital">moniteur </hi>ainsi que chez Hachette ;</l>
					<l n="23" num="3.3">C’est pour lui seul que la marmite bout</l>
					<l n="24" num="3.4">Chez Désiré comme au café vachette ;</l>
					<l n="25" num="3.5">C’est lui qu’on prise et c’est lui qu’on achète.</l>
					<l n="26" num="3.6">Pourtant Venet écrit à <hi rend="ital">l’univers ; </hi></l>
					<l n="27" num="3.7">Machin (du Tarn) dans des recueils divers</l>
					<l n="28" num="3.8">Offre au public des lignes incongrues,</l>
					<l n="29" num="3.9">Et Champfleury veut supprimer les vers :</l>
					<l n="30" num="3.10">Voici le temps pour les coquecigrues.</l>
				</lg>
				<lg n="4">
					<head type="form">ENVOI</head>
					<l n="31" num="4.1">Mon cher François, vers la Touraine et vers</l>
					<l n="32" num="4.2">Vos lys, mes chants volent aux bosquets verts.</l>
					<l n="33" num="4.3">Je sais qu’ils ont des rimes un peu crues :</l>
					<l n="34" num="4.4">C’est que depuis ces dix ou douze hivers,</l>
					<l n="35" num="4.5">Voici le temps pour les coquecigrues.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1856">juillet 1856</date>.
					</dateline>
				</closer>
			</div></body></text></TEI>