<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Odes funambulesques</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3888 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">BAN_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>

			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Odes funambulesques</title>
						<author>Théodore de BANVILLE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L922</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Odes funambulesques</title>
								<author>Théodore de BANVILLE</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>M. Levy</publisher>
									<date when="1859">1859</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>

			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres de Théodore de Banville : Odes funambulesques</title>
						<author>Théodore de BANVILLE</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre</publisher>
							<date when="1892">1892</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1857">1857</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-23" who="RR">Révision de l’entête pour validation</change>
			<change when="2016-03-14" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN133">
				<head type="main">LE SAUT DU TREMPLIN</head>
				<lg n="1">
					<l n="1" num="1.1">Clown admirable, en vérité !</l>
					<l n="2" num="1.2">Je crois que la postérité,</l>
					<l n="3" num="1.3">Dont sans cesse l’horizon bouge,</l>
					<l n="4" num="1.4">Le reverra, sa plaie au flanc.</l>
					<l n="5" num="1.5">Il était barbouillé de blanc,</l>
					<l n="6" num="1.6">De jaune, de vert et de rouge.</l>
				</lg>
				<lg n="2">
					<l n="7" num="2.1">Même jusqu’à Madagascar</l>
					<l n="8" num="2.2">Son nom était parvenu, car</l>
					<l n="9" num="2.3">C’était selon tous les principes</l>
					<l n="10" num="2.4">Qu’après les cercles de papier,</l>
					<l n="11" num="2.5">Sans jamais les estropier</l>
					<l n="12" num="2.6">Il traversait le rond des pipes.</l>
				</lg>
				<lg n="3">
					<l n="13" num="3.1">Il s’élevait à des hauteurs</l>
					<l n="14" num="3.2">Telles, que les autres sauteurs</l>
					<l n="15" num="3.3">Se consumaient en luttes vaines.</l>
					<l n="16" num="3.4">Ils le trouvaient décourageant,</l>
					<l n="17" num="3.5">Et murmuraient : « quel vif-argent</l>
					<l n="18" num="3.6">Ce démon a-t-il dans les veines ? »</l>
				</lg>
				<lg n="4">
					<l n="19" num="4.1">Tout le peuple criait : « bravo ! »</l>
					<l n="20" num="4.2">Mais lui, par un effort nouveau,</l>
					<l n="21" num="4.3">Semblait roidir sa jambe nue,</l>
					<l n="22" num="4.4">Et, sans que l’on sût avec qui,</l>
					<l n="23" num="4.5">Cet émule de la saqui</l>
					<l n="24" num="4.6">Parlait bas en langue inconnue.</l>
				</lg>
				<lg n="5">
					<l n="25" num="5.1">C’était avec son cher tremplin.</l>
					<l n="26" num="5.2">Il lui disait : « théâtre, plein</l>
					<l n="27" num="5.3">D’inspiration fantastique,</l>
					<l n="28" num="5.4">Tremplin qui tressailles d’émoi</l>
					<l n="29" num="5.5">Quand je prends un élan, fais-moi</l>
					<l n="30" num="5.6">Bondir plus haut, planche élastique !</l>
				</lg>
				<lg n="6">
					<l n="31" num="6.1">« Frêle machine aux reins puissants,</l>
					<l n="32" num="6.2">Fais-moi bondir, moi qui me sens</l>
					<l n="33" num="6.3">Plus agile que les panthères,</l>
					<l n="34" num="6.4">Si haut que je ne puisse voir</l>
					<l n="35" num="6.5">Avec leur cruel habit noir</l>
					<l n="36" num="6.6">Ces épiciers et ces notaires !</l>
				</lg>
				<lg n="7">
					<l n="37" num="7.1">« Par quelque prodige pompeux,</l>
					<l n="38" num="7.2">Fais-moi monter, si tu le peux,</l>
					<l n="39" num="7.3">Jusqu’à ces sommets où, sans règles,</l>
					<l n="40" num="7.4">Embrouillant les cheveux vermeils</l>
					<l n="41" num="7.5">Des planètes et des soleils,</l>
					<l n="42" num="7.6">Se croisent la foudre et les aigles.</l>
				</lg>
				<lg n="8">
					<l n="43" num="8.1">« Plus haut encor, jusqu’au ciel pur !</l>
					<l n="44" num="8.2">Jusqu’à ce lapis dont l’azur</l>
					<l n="45" num="8.3">Couvre notre prison mouvante !</l>
					<l n="46" num="8.4">Jusqu’à ces rouges orients</l>
					<l n="47" num="8.5">Où marchent des dieux flamboyants,</l>
					<l n="48" num="8.6">Fous de colère et d’épouvante.</l>
				</lg>
				<lg n="9">
					<l n="49" num="9.1">« Plus loin ! Plus haut ! Je vois encor</l>
					<l n="50" num="9.2">Des boursiers à lunettes d’or,</l>
					<l n="51" num="9.3">Des critiques, des demoiselles</l>
					<l n="52" num="9.4">Et des réalistes en feu.</l>
					<l n="53" num="9.5">Plus haut ! Plus loin ! De l’air ! Du bleu !</l>
					<l n="54" num="9.6">Des ailes ! Des ailes ! Des ailes ! »</l>
				</lg>
				<lg n="10">
					<l n="55" num="10.1">Enfin, de son vil échafaud,</l>
					<l n="56" num="10.2">Le clown sauta si haut, si haut,</l>
					<l n="57" num="10.3">Qu’il creva le plafond de toiles</l>
					<l n="58" num="10.4">Au son du cor et du tambour,</l>
					<l n="59" num="10.5">Et, le cœur dévoré d’amour,</l>
					<l n="60" num="10.6">Alla rouler dans les étoiles.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1857">février 1857</date>.
					</dateline>
				</closer>
			</div></body></text></TEI>