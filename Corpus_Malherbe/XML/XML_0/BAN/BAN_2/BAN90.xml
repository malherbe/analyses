<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Odes funambulesques</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3888 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">BAN_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>

			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Odes funambulesques</title>
						<author>Théodore de BANVILLE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L922</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Odes funambulesques</title>
								<author>Théodore de BANVILLE</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>M. Levy</publisher>
									<date when="1859">1859</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>

			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres de Théodore de Banville : Odes funambulesques</title>
						<author>Théodore de BANVILLE</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre</publisher>
							<date when="1892">1892</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1857">1857</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-23" who="RR">Révision de l’entête pour validation</change>
			<change when="2016-03-14" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">OCCIDENTALES</head><div type="poem" key="BAN90">
					<head type="main">OCCIDENTALE SIXIÈME</head>
					<head type="sub">L’ODÉON</head>
					<lg n="1">
						<l n="1" num="1.1">Le mur lui-même semble enrhumé du cerveau.</l>
						<l n="2" num="1.2">Bocage a passé là. L’odéon, noir caveau,</l>
						<l n="3" num="1.3"><space quantity="6" unit="char"></space>Dans ses vastes dodécaèdres</l>
						<l n="4" num="1.4">Voit verdoyer la mousse. Aux fentes des pignons</l>
						<l n="5" num="1.5">Pourrissent les lichens et les grands champignons</l>
						<l n="6" num="1.6"><space quantity="6" unit="char"></space>Bien plus robustes que des cèdres.</l>
					</lg>
					<lg n="2">
						<l n="7" num="2.1">Tout est désert. Mais non, suspendu, sans clocher,</l>
						<l n="8" num="2.2">Le grand nez de Lucas fend l’air comme un clocher.</l>
						<l n="9" num="2.3"><space quantity="6" unit="char"></space>Trop passionné pour Racine,</l>
						<l n="10" num="2.4">Un pompier, dont le dos servait de point d’appui</l>
						<l n="11" num="2.5">À ce nez immoral, sans doute comme lui</l>
						<l n="12" num="2.6"><space quantity="6" unit="char"></space>Dans le sol avait pris racine.</l>
					</lg>
					<lg n="3">
						<l n="13" num="3.1">Ah ! Dit Mauzin touché de pareilles vertus,</l>
						<l n="14" num="3.2">Poëte, pour calmer ces affreux hiatus</l>
						<l n="15" num="3.3"><space quantity="6" unit="char"></space>Dont eût rougi même un cipaye,</l>
						<l n="16" num="3.4">Et pour te voir tordu par ce rire usité</l>
						<l n="17" num="3.5">Chez les hommes qu’afflige une gibbosité,</l>
						<l n="18" num="3.6"><space quantity="6" unit="char"></space>Dis, que veux-tu que je te paye ?</l>
					</lg>
					<lg n="4">
						<l n="19" num="4.1">Que faut-il pour te voir plus gai que Limayrac ?</l>
						<l n="20" num="4.2">Veux-tu que je t’apporte une cruche de rack ?</l>
						<l n="21" num="4.3"><space quantity="6" unit="char"></space>Dis, que te faut-il pour que rie</l>
						<l n="22" num="4.4">Ta prunelle d’azur, pareille à des saphirs,</l>
						<l n="23" num="4.5">Et pour voir tes cheveux s’envoler aux zéphyrs</l>
						<l n="24" num="4.6"><space quantity="6" unit="char"></space>Comme les crins de Vacquerie !</l>
					</lg>
					<lg n="5">
						<l n="25" num="5.1">Qui pourrait dissiper ton noir abattement ?</l>
						<l n="26" num="5.2">Te faut-il les gants bleus de monsieur nettement,</l>
						<l n="27" num="5.3"><space quantity="6" unit="char"></space>Ou ce chapeau qui vient de Tarbe,</l>
						<l n="28" num="5.4">Le chapeau d’Almanzor, cet homme si barbu,</l>
						<l n="29" num="5.5">Qu’un barbier peut à peine, à moins d’avoir trop bu,</l>
						<l n="30" num="5.6"><space quantity="6" unit="char"></space>En quatre ans lui faire la barbe !</l>
					</lg>
					<lg n="6">
						<l n="31" num="6.1">Pour sourire veux-tu le casque du pompier,</l>
						<l n="32" num="6.2">Plus brillant qu’un bonbon plié dans son papier</l>
						<l n="33" num="6.3"><space quantity="6" unit="char"></space>Ou que l’argent d’une timbale ?</l>
						<l n="34" num="6.4">Que veux-tu, rack, gants, feutre ou casque fait au tour ?</l>
						<l n="35" num="6.5">— hélas ! Vieux, dit Lucas, dit l’homme au nez d’autour,</l>
						<l n="36" num="6.6"><space quantity="6" unit="char"></space>Il me faudrait une autre <hi rend="ital">balle ! </hi></l>
					</lg>
					<closer>
						<dateline>
							<date when="1848">juin 1848</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>