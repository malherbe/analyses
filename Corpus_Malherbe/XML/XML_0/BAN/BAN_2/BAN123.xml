<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Odes funambulesques</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3888 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">BAN_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>

			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Odes funambulesques</title>
						<author>Théodore de BANVILLE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L922</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Odes funambulesques</title>
								<author>Théodore de BANVILLE</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>M. Levy</publisher>
									<date when="1859">1859</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>

			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres de Théodore de Banville : Odes funambulesques</title>
						<author>Théodore de BANVILLE</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre</publisher>
							<date when="1892">1892</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1857">1857</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-23" who="RR">Révision de l’entête pour validation</change>
			<change when="2016-03-14" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN123">
				<head type="main">VIRELAI <lb></lb>À MES ÉDITEURS</head>
				<lg n="1">
					<l n="1" num="1.1">Barbanchu nargue la rime !</l>
					<l n="2" num="1.2">Je défends que l’on m’imprime !</l>
				</lg>
				<lg n="2">
					<l n="3" num="2.1">La gloire n’était que frime ;</l>
					<l n="4" num="2.2">Vainement pour elle on trime,</l>
					<l n="5" num="2.3">Car ce point est résolu.</l>
					<l n="6" num="2.4">Il faut bien qu’on nous supprime :</l>
					<l n="7" num="2.5">Barbanchu nargue la rime !</l>
				</lg>
				<lg n="3">
					<l n="8" num="3.1">Le cas enfin s’envenime.</l>
					<l n="9" num="3.2">Le prosateur chevelu</l>
					<l n="10" num="3.3">Trop longtemps fut magnanime.</l>
					<l n="11" num="3.4">Contre la lyre il s’anime,</l>
					<l n="12" num="3.5">Et traite d’hurluberlu</l>
					<l n="13" num="3.6">Ou d’un terme synonyme</l>
					<l n="14" num="3.7">Quiconque ne l’a pas lu.</l>
					<l n="15" num="3.8">Je défends que l’on m’imprime.</l>
				</lg>
				<lg n="4">
					<l n="16" num="4.1">Fou, tremble qu’on ne t’abîme !</l>
					<l n="17" num="4.2">Rimer, ce temps révolu,</l>
					<l n="18" num="4.3">C’est courir vers un abîme,</l>
					<l n="19" num="4.4">Barbanchu nargue la rime !</l>
				</lg>
				<lg n="5">
					<l n="20" num="5.1">Tu ne vaux plus un décime !</l>
					<l n="21" num="5.2">Car l’ennemi nous décime,</l>
					<l n="22" num="5.3">Sur nous pose un doigt velu,</l>
					<l n="23" num="5.4">Et, dans son chenil intime,</l>
					<l n="24" num="5.5">Rit en vrai patte-pelu</l>
					<l n="25" num="5.6">De nous voir pris à sa glu.</l>
					<l n="26" num="5.7">Malgré le monde unanime,</l>
					<l n="27" num="5.8">Tout prodige est superflu.</l>
					<l n="28" num="5.9">Le vulgaire dissolu</l>
					<l n="29" num="5.10">Tient les mètres en estime :</l>
					<l n="30" num="5.11">Il y mord en vrai goulu !</l>
					<l n="31" num="5.12">Bah ! Pour mériter la prime,</l>
					<l n="32" num="5.13">Tu lui diras : lanturlu !</l>
					<l n="33" num="5.14">Je défends que l’on m’imprime.</l>
				</lg>
				<lg n="6">
					<l n="34" num="6.1">Molière au hasard s’escrime,</l>
					<l n="35" num="6.2">C’est un bouffon qui se grime ;</l>
					<l n="36" num="6.3">Dante vieilli se périme,</l>
					<l n="37" num="6.4">Et Shakspere nous opprime !</l>
					<l n="38" num="6.5">Que leur art jadis ait plu,</l>
					<l n="39" num="6.6">Sur la récolte il a plu,</l>
					<l n="40" num="6.7">Et la foudre pour victime</l>
					<l n="41" num="6.8">Choisit leur toit vermoulu.</l>
					<l n="42" num="6.9">C’était un régal minime</l>
					<l n="43" num="6.10">Que Juliette ou Monime !</l>
					<l n="44" num="6.11">Descends de ta double cime,</l>
					<l n="45" num="6.12">Et, sous quelque pseudonyme,</l>
					<l n="46" num="6.13">Fabrique une pantomime ;</l>
					<l n="47" num="6.14">Il le faut, il l’a fallu.</l>
					<l n="48" num="6.15">Mais plus de retour sublime</l>
					<l n="49" num="6.16">Vers Corinthe ou vers Solyme !</l>
					<l n="50" num="6.17">Ciseleur, brise ta lime,</l>
					<l n="51" num="6.18">Barbanchu nargue la rime !</l>
					<l n="52" num="6.19">Seul un réaliste exprime</l>
					<l n="53" num="6.20">Le beau rêche et mamelu :</l>
					<l n="54" num="6.21">En douter serait un crime.</l>
					<l n="55" num="6.22">Barbanchu nargue la rime !</l>
					<l n="56" num="6.23">Je défends que l’on m’imprime.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1856">novembre 1856</date>.
					</dateline>
				</closer>
			</div></body></text></TEI>