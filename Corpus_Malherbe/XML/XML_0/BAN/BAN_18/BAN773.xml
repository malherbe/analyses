<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">PETIT TRAITÉ DE POÉSIE FRANÇAISE</title>
				<title type="medium">Une édition électronique</title>
				<title type="part">Extrait (deux poèmes)</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>60 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BAN_18</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">PETIT TRAITÉ DE POÉSIE FRANÇAISE</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">https://fr.wikisource.org/wiki/Livre:Banville_-_Petit_Trait%C3%A9_de_po%C3%A9sie_fran%C3%A7aise,_1881.djvu</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>PETIT TRAITÉ DE POÉSIE FRANÇAISE</title>
								<author>Théodore de Banville</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER, ÉDITEUR</publisher>
									<date when="1881">1881</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1881">1881</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Extraits : poèmes de l’auteur.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">X. De quelques Curiosités poétiques</head><div type="poem" key="BAN773">
					<head type="main">LA MONTAGNE</head>
					<head type="form">Pantoum</head>
					<lg n="1">
						<l n="1" num="1.1">Sur les bords de ce flot céleste</l>
						<l n="2" num="1.2">Mille oiseaux chantent, querelleurs.</l>
						<l n="3" num="1.3">Mon enfant, seul bien qui me reste,</l>
						<l n="4" num="1.4">Dors sous ces branches d’arbre en fleurs.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">Mille oiseaux chantent, querelleurs.</l>
						<l n="6" num="2.2">Sur la rivière un cygne glisse.</l>
						<l n="7" num="2.3">Dors sous ces branches d’arbre en fleurs,</l>
						<l n="8" num="2.4">Ô toi ma joie et mon délice !</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">Sur la rivière un cygne glisse</l>
						<l n="10" num="3.2">Dans les feux du soleil couchant.</l>
						<l n="11" num="3.3">Ô toi ma joie et mon délice,</l>
						<l n="12" num="3.4">Endors-toi, bercé par mon chant !</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">Dans les feux du soleil couchant</l>
						<l n="14" num="4.2">Le vieux mont est brillant de neige.</l>
						<l n="15" num="4.3">Endors-toi bercé par mon chant,</l>
						<l n="16" num="4.4">Qu’un dieu bienveillant te protège !</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">Le vieux mont est brillant de neige,</l>
						<l n="18" num="5.2">À ses pieds l’ébénier fleurit.</l>
						<l n="19" num="5.3">Qu’un dieu bienveillant te protège !</l>
						<l n="20" num="5.4">Ta petite bouche sourit.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1">À ses pieds l’ébénier fleurit.</l>
						<l n="22" num="6.2">De brillants métaux le recouvrent.</l>
						<l n="23" num="6.3">Ta petite bouche sourit.</l>
						<l n="24" num="6.4">Pareille aux corolles qui s’ouvrent.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1">De brillants métaux le recouvrent,</l>
						<l n="26" num="7.2">Je vois luire des diamants.</l>
						<l n="27" num="7.3">Pareille aux corolles qui s’ouvrent,</l>
						<l n="28" num="7.4">Ta lèvre a des rayons charmants.</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1">Je vois luire des diamants</l>
						<l n="30" num="8.2">Sur la montagne enchanteresse.</l>
						<l n="31" num="8.3">Ta lèvre a des rayons charmants,</l>
						<l n="32" num="8.4">Dors, qu’un rêve heureux te caresse!</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1">Sur la montagne enchanteresse</l>
						<l n="34" num="9.2">Je vois des topazes de feu.</l>
						<l n="35" num="9.3">Dors, qu’un songe heureux te caresse.</l>
						<l n="36" num="9.4">Ferme tes yeux de lotus bleu !</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1">Je vois des topazes de feu</l>
						<l n="38" num="10.2">Qui chassent tout songe funeste.</l>
						<l n="39" num="10.3">Ferme tes yeux de lotus bleu</l>
						<l n="40" num="10.4">Sur les bords de ce flot céleste !</l>
					</lg>
				</div></body></text></TEI>