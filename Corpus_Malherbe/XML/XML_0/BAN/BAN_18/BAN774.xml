<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">PETIT TRAITÉ DE POÉSIE FRANÇAISE</title>
				<title type="medium">Une édition électronique</title>
				<title type="part">Extrait (deux poèmes)</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>60 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BAN_18</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">PETIT TRAITÉ DE POÉSIE FRANÇAISE</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">https://fr.wikisource.org/wiki/Livre:Banville_-_Petit_Trait%C3%A9_de_po%C3%A9sie_fran%C3%A7aise,_1881.djvu</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>PETIT TRAITÉ DE POÉSIE FRANÇAISE</title>
								<author>Théodore de Banville</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER, ÉDITEUR</publisher>
									<date when="1881">1881</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1881">1881</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Extraits : poèmes de l’auteur.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">XI. Conclusion</head><div type="poem" key="BAN774">
					<head type="main">VERS DE NEUF SYLLABES, <lb></lb>AVEC UNE SEULE CÉSURE PLACÉE <lb></lb>APRÈS LA CINQUIÈME SYLLABE</head>
					<lg n="1">
						<head type="main">Le Poète.</head>
						<l n="1" num="1.1">En proie à l’enfer — plein de fureur,</l>
						<l n="2" num="1.2">Avant qu’à jamais — il resplendisse,</l>
						<l n="3" num="1.3">Le poëte voit — avec horreur</l>
						<l n="4" num="1.4">S’enfuir vers la nuit — son Eurydice.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">Il vit exilé — sous l’œil des cieux.</l>
						<l n="6" num="2.2">Les fauves lions — avec délire</l>
						<l n="7" num="2.3">Écoutent son chant — délicieux,</l>
						<l n="8" num="2.4">Captifs qu’a vaincus — la grande Lyre.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">Le tigre féroce — avait pleuré,</l>
						<l n="10" num="3.2">Mais c’était en vain, — il faut que l’Hèbre</l>
						<l n="11" num="3.3">Porte dans ses flots — mort, déchiré,</l>
						<l n="12" num="3.4">Celui dont le nom — vivra célèbre.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">Puis divinisé — par la douleur,</l>
						<l n="14" num="4.2">À présent parmi — les Dieux sans voiles,</l>
						<l n="15" num="4.3">Ce charmeur des bois, — cet oiseleur</l>
						<l n="16" num="4.4">Pose ses pieds blancs — sur les étoiles.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">Mais l’ombre toujours — entend frémir</l>
						<l n="18" num="5.2">Ta plainte qui meurt — comme étouffée,</l>
						<l n="19" num="5.3">Et tes verts roseaux — tout bas gémir,</l>
						<l n="20" num="5.4">Fleuve qu’a rougi — le sang d’Orphée !</l>
					</lg>
				</div></body></text></TEI>