<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">DANS LA FOURNAISE</title>
				<title type="sub_2">Dernières Poésies</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4181 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_17</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">DANS LA FOURNAISE</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="2001">2001</date>
								</imprint>
								<biblScope unit="tome">VIII</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Dans la fournaise</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>G. Charpentier et E. Fasquelle, Éditeurs</publisher>
							<date when="1892">1892</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1892">1892</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN741">
				<head type="main">A Victor Hugo</head>
				<head type="sub_1">Strophes récitées par C. Coquelin <lb></lb>A la matinée du Trocadéro <lb></lb>Le 26 février 1881.</head>
				<lg n="1">
					<l n="1" num="1.1">Père, doux au malheur, au deuil, à la souffrance !</l>
					<l n="2" num="1.2">A l’ombre du laurier dans la lutte conquis,</l>
					<l n="3" num="1.3">Viens sentir sur tes mains le baiser de la France,</l>
					<l n="4" num="1.4">Heureuse de fêter le jour où tu naquis !</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Victor Hugo ! la voix de la Lyre étouffée,</l>
					<l n="6" num="2.2">Se réveilla par toi, plaignant les maux soufferts,</l>
					<l n="7" num="2.3">Et tu connus, ainsi que ton aïeul Orphée,</l>
					<l n="8" num="2.4">L’âpre exil, et ton chant ravit les noirs enfers.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Mais tu vis à présent dans la sereine gloire,</l>
					<l n="10" num="3.2">Calme, heureux, contemplé par le ciel souriant,</l>
					<l n="11" num="3.3">Ainsi qu’Homère assis sur un trône d’ivoire,</l>
					<l n="12" num="3.4">Rayonnant et les yeux tournés vers l’Orient.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">Et tu vois à tes pieds la fille de Pindare,</l>
					<l n="14" num="4.2">L’Ode qui vole et plane au fond des firmaments,</l>
					<l n="15" num="4.3">L’Épopée et l’éclair de son glaive barbare,</l>
					<l n="16" num="4.4">Et la Satire, aux yeux pleins de fiers châtiments ;</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1">Et le Drame, charmeur de la foule pensive,</l>
					<l n="18" num="5.2">Qui du peuple agitant et contenant les flots,</l>
					<l n="19" num="5.3">Sur tous les parias répand, comme une eau vive,</l>
					<l n="20" num="5.4">Sa plainte gémissante et ses amers sanglots.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1">Mais, ô consolateur de tous les misérables !</l>
					<l n="22" num="6.2">Tu détournes les yeux du crime châtié,</l>
					<l n="23" num="6.3">Pour ne plus voir que l’Ange aux larmes adorables</l>
					<l n="24" num="6.4">Qu’au ciel et sur la terre on nomme : la Pitié !</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1">O Père ! s’envolant sur le divin Pégase</l>
					<l n="26" num="7.2">A travers l’infini sublime et radieux,</l>
					<l n="27" num="7.3">Ce génie effrayant, ta Pensée en extase</l>
					<l n="28" num="7.4">A tout vu, le passé, les mystères, les Dieux.</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1">Elle a vu le charnier funèbre de l’Histoire,</l>
					<l n="30" num="8.2">Les sages poursuivant le but essentiel,</l>
					<l n="31" num="8.3">Et les démons forgeant dans leur caverne noire,</l>
					<l n="32" num="8.4">Les brasiers de l’aurore et les saphirs du ciel ;</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1">Elle a vu les combats, les horreurs, les désastres,</l>
					<l n="34" num="9.2">Les exilés pleurant les paradis perdus,</l>
					<l n="35" num="9.3">Et les fouets acharnés sur le troupeau des astres ;</l>
					<l n="36" num="9.4">Et, lorsqu’elle revient des gouffres éperdus,</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1">Lorsque nous lui disons : Parle. Que faut-il faire ?</l>
					<l n="38" num="10.2">Enseigne-nous le vrai chemin. D’où vient le jour ?</l>
					<l n="39" num="10.3">Pour nous sauver, faut-il qu’on lutte ou qu’on diffère ?</l>
					<l n="40" num="10.4">Elle répond : Le mot du problème est Amour !</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1">Aimez-vous ! Ces deux mots qui changèrent le monde</l>
					<l n="42" num="11.2">Et vainquirent le Mal et ses rébellions,</l>
					<l n="43" num="11.3">Comme autrefois, redits avec ta voix profonde,</l>
					<l n="44" num="11.4">Émeuvent les rochers et domptent les lions.</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1">Oh ! parle ! Que ton chant merveilleux retentisse !</l>
					<l n="46" num="12.2">Dis-nous en tes récits, pleins de charmants effrois,</l>
					<l n="47" num="12.3">Comment quelque Roland armé pour la justice,</l>
					<l n="48" num="12.4">Pour sauver un enfant égorge un tas de rois !</l>
				</lg>
				<lg n="13">
					<l n="49" num="13.1">O maître bien-aimé, qui sans cesse t’élèves,</l>
					<l n="50" num="13.2">La France acclame en toi le plus grand de ses fils ;</l>
					<l n="51" num="13.3">Elle bénit ton front plein d’espoir et de rêves,</l>
					<l n="52" num="13.4">Et tes cheveux pareils à la neige des lys !</l>
				</lg>
				<lg n="14">
					<l n="53" num="14.1">Ton œuvre, dont le Temps a soulevé les voiles,</l>
					<l n="54" num="14.2">S’est déroulée ainsi que de riches colliers,</l>
					<l n="55" num="14.3">Comme après des milliers et des milliers d’étoiles,</l>
					<l n="56" num="14.4">Des étoiles au ciel s’allument par milliers.</l>
				</lg>
				<lg n="15">
					<l n="57" num="15.1">Oh ! parle ! ravis-nous, poëte ! chante encore,</l>
					<l n="58" num="15.2">Effaçant nos malheurs, nos deuils, l’antique affront ;</l>
					<l n="59" num="15.3">Et donne-nous l’immense orgueil de voir éclore</l>
					<l n="60" num="15.4">Les chefs-d’œuvre futurs qui germent sous ton front !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1881"> 26 février 1881.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>