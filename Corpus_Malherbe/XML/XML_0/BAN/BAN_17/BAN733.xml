<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">DANS LA FOURNAISE</title>
				<title type="sub_2">Dernières Poésies</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4181 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_17</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">DANS LA FOURNAISE</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="2001">2001</date>
								</imprint>
								<biblScope unit="tome">VIII</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Dans la fournaise</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>G. Charpentier et E. Fasquelle, Éditeurs</publisher>
							<date when="1892">1892</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1892">1892</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">Chansons sur des airs connus</head><div type="poem" key="BAN733">
					<head type="main">La Lune</head>
					<head type="sub_2">Air : Au clair de la lune</head>
					<lg n="1">
						<l n="1" num="1.1">Au clair de la lune</l>
						<l n="2" num="1.2">Brillent follement</l>
						<l n="3" num="1.3">Ta prunelle brune</l>
						<l n="4" num="1.4">Et ton sein charmant.</l>
						<l n="5" num="1.5">Pâle Cidalise,</l>
						<l n="6" num="1.6">Ton front sans rival</l>
						<l n="7" num="1.7">Éclaire Venise</l>
						<l n="8" num="1.8">Et son carnaval.</l>
					</lg>
					<lg n="2">
						<l n="9" num="2.1">Cachant sous ton masque</l>
						<l n="10" num="2.2">Un sourire amer,</l>
						<l n="11" num="2.3">Tu t’en vas, fantasque,</l>
						<l n="12" num="2.4">Sur la vaste mer.</l>
						<l n="13" num="2.5">Et frottant son aile</l>
						<l n="14" num="2.6">A ton casaquin,</l>
						<l n="15" num="2.7">Voilà Pulcinelle</l>
						<l n="16" num="2.8">Avec Arlequin !</l>
					</lg>
					<lg n="3">
						<l n="17" num="3.1">Voilà Scaramouche</l>
						<l n="18" num="3.2">Et don Spavento,</l>
						<l n="19" num="3.3">Et Scapin farouche</l>
						<l n="20" num="3.4">Dans son vert manteau ;</l>
						<l n="21" num="3.5">Et, comme Tityre</l>
						<l n="22" num="3.6">Près d’Amaryllis,</l>
						<l n="23" num="3.7">Pierrot qui s’étire,</l>
						<l n="24" num="3.8">Mince comme un lys.</l>
					</lg>
					<lg n="4">
						<l n="25" num="4.1">Zerbin, dans sa fièvre,</l>
						<l n="26" num="4.2">Après Mezzetin,</l>
						<l n="27" num="4.3">Baise à pleine lèvre</l>
						<l n="28" num="4.4">Tes bras de satin.</l>
						<l n="29" num="4.5">Verse-leur l’ivresse !</l>
						<l n="30" num="4.6">O toi qui me plus,</l>
						<l n="31" num="4.7">Folle charmeresse,</l>
						<l n="32" num="4.8">Je ne t’aime plus.</l>
					</lg>
					<lg n="5">
						<l n="33" num="5.1">Je ris, ma guitare</l>
						<l n="34" num="5.2">Chante un air moqueur ;</l>
						<l n="35" num="5.3">Pourtant c’est bizarre,</l>
						<l n="36" num="5.4">J’ai froid dans le cœur.</l>
						<l n="37" num="5.5">Et je vois la lune,</l>
						<l n="38" num="5.6">Dans l’ardente nuit,</l>
						<l n="39" num="5.7">Frissonner, comme une</l>
						<l n="40" num="5.8">Clarté qui s’enfuit.</l>
					</lg>
					<lg n="6">
						<l n="41" num="6.1">Phœbé, la perverse,</l>
						<l n="42" num="6.2">Peut-être à son tour</l>
						<l n="43" num="6.3">S’alanguit et verse</l>
						<l n="44" num="6.4">Des larmes d’amour.</l>
						<l n="45" num="6.5">Et son char l’emporte,</l>
						<l n="46" num="6.6">Dans la nuit en feu,</l>
						<l n="47" num="6.7">Désolée et morte</l>
						<l n="48" num="6.8">Au fond du ciel bleu.</l>
					</lg>
					<closer>
						<dateline>
							<date when="1876">Octobre 1876.</date>
						</dateline>
					</closer>
				</div></body></text></TEI>