<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">DANS LA FOURNAISE</title>
				<title type="sub_2">Dernières Poésies</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4181 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_17</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">DANS LA FOURNAISE</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="2001">2001</date>
								</imprint>
								<biblScope unit="tome">VIII</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Dans la fournaise</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>G. Charpentier et E. Fasquelle, Éditeurs</publisher>
							<date when="1892">1892</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1892">1892</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN732">
				<head type="main">L’Année cruelle</head>
			<opener>
				<salute>A Émile Bergerat</salute>
			</opener>
				<lg n="1">
					<l n="1" num="1.1">Oui, j’aimerais mieux être, ô mon cher Bergerat,</l>
					<l n="2" num="1.2">Chien dans la rue, ou bien dans une auberge rat,</l>
					<l n="3" num="1.3">Ou mesurer du drap d’Elbeuf par centimètres,</l>
					<l n="4" num="1.4">Que de faire ce dur métier d’homme de lettres !</l>
					<l n="5" num="1.5">Eh ! quoi, toujours pâlir ainsi que Deburau,</l>
					<l n="6" num="1.6">Et, les yeux sur le cuir violet d’un bureau,</l>
					<l n="7" num="1.7">Sans avoir su quel crime ici-bas l’on expie,</l>
					<l n="8" num="1.8">Entasser en monceau des feuillets de copie !</l>
					<l n="9" num="1.9">Ah ! je n’étais pas né pour ce fatal destin !</l>
					<l n="10" num="1.10">Au lieu de respirer au bois l’odeur du thym,</l>
					<l n="11" num="1.11">Comme un noyé blême à qui nul ne tend la perche,</l>
					<l n="12" num="1.12">Enfoncé dans sa nuit, l’homme de lettres cherche</l>
					<l n="13" num="1.13">Les traits spirituels et la transition,</l>
					<l n="14" num="1.14">Et ne peut même aller voir l’Exposition.</l>
					<l n="15" num="1.15">Car je n’irai pas ! Corps en proie à la névrose,</l>
					<l n="16" num="1.16">Pâture du journal, vil forçat de la prose,</l>
					<l n="17" num="1.17">Je dois, par ce beau temps, me barricader au</l>
					<l n="18" num="1.18">Logis, au lieu d’aller voir le Trocadéro !</l>
					<l n="19" num="1.19">Ah ! j’ai rêvé souvent en ce siècle fantoche</l>
					<l n="20" num="1.20">De me trouver un jour libre, ayant dans ma poche</l>
					<l n="21" num="1.21">De l’argent pour pouvoir engager des paris,</l>
					<l n="22" num="1.22">O poëte, et de faire… un voyage à Paris !</l>
					<l n="23" num="1.23">Semblant venir de loin par un vain simulacre,</l>
					<l n="24" num="1.24">Je monterais avec des colis dans un fiacre,</l>
					<l n="25" num="1.25">Et de mes Dieux jaloux abandonnant l’autel,</l>
					<l n="26" num="1.26">Je me ferais alors conduire au Grand-Hôtel.</l>
					<l n="27" num="1.27">J’ai fait ce rêve. Ainsi qu’un Triton dans ma conque,</l>
					<l n="28" num="1.28">Je feignais d’arriver d’une gare quelconque,</l>
					<l n="29" num="1.29">Je fumais un londrès, j’avais l’air d’être Anglais,</l>
					<l n="30" num="1.30">Serré dans un faux-col de marbre où j’étranglais,</l>
					<l n="31" num="1.31">Et comme on voit le chêne environné de lierre,</l>
					<l n="32" num="1.32">J’avais sur la poitrine un sac en bandoulière !</l>
					<l n="33" num="1.33">Oui, dans ce songe heureux, mon esprit se complaît.</l>
					<l n="34" num="1.34">Coiffé d’une casquette et vêtu d’un complet,</l>
					<l n="35" num="1.35">Je souris, je m’assieds dans la chambre où l’on dîne</l>
					<l n="36" num="1.36">A côté d’une miss blanche comme l’Ondine,</l>
					<l n="37" num="1.37">Et je cause à voix haute avec des Islandais</l>
					<l n="38" num="1.38">Consultant, pour parler, Napoléon Landais.</l>
					<l n="39" num="1.39">Avec ces étrangers que leur panache appelle</l>
					<l n="40" num="1.40">Je visite le Louvre et la Sainte-Chapelle,</l>
					<l n="41" num="1.41">Puis le Bois et son lac, où vient le nénuphar.</l>
					<l n="42" num="1.42">Je vois tout. Je me fais montrer Zulma Bouffar.</l>
					<l n="43" num="1.43">Pareil au mont chenu que la tempête assiège,</l>
					<l n="44" num="1.44">Un vieillard passe, ayant sur sa barbe de neige</l>
					<l n="45" num="1.45">L’âpre sérénité des glaciers blancs et clairs</l>
					<l n="46" num="1.46">Que vient traverser l’or fulgurant des éclairs ;</l>
					<l n="47" num="1.47">Sa tempe, mille fois par la douleur broyée,</l>
					<l n="48" num="1.48">Semble une roche dans l’ouragan foudroyée ;</l>
					<l n="49" num="1.49">Sa lèvre a la beauté sereine du Devoir ;</l>
					<l n="50" num="1.50">Auprès de lui, dans l’ombre épaissie, on croit voir</l>
					<l n="51" num="1.51">Un lion familier que sa lèvre gourmande ;</l>
					<l n="52" num="1.52">Il nous frôle en rêvant, et comme je demande :</l>
					<l n="53" num="1.53">Quel est donc ce passant ? Vient-il de Chicago ?</l>
					<l n="54" num="1.54">On me répond : Non, c’est monsieur Victor Hugo !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1878">1878.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>