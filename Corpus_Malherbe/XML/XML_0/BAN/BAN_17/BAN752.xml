<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">DANS LA FOURNAISE</title>
				<title type="sub_2">Dernières Poésies</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4181 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_17</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">DANS LA FOURNAISE</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="2001">2001</date>
								</imprint>
								<biblScope unit="tome">VIII</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Dans la fournaise</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>G. Charpentier et E. Fasquelle, Éditeurs</publisher>
							<date when="1892">1892</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1892">1892</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN752">
				<head type="main">Déja vus</head>
				<lg n="1">
					<l n="1" num="1.1">Céline, avec ses cheveux roux</l>
					<l n="2" num="1.2">Dont la fauve splendeur nous flatte,</l>
					<l n="3" num="1.3">Darde ses yeux pleins de courroux,</l>
					<l n="4" num="1.4">Pareille à la bête écarlate.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">Magnifique dans le printemps</l>
					<l n="6" num="2.2">Comme une grande fleur qui bouge,</l>
					<l n="7" num="2.3">Elle charme les airs flottants,</l>
					<l n="8" num="2.4">En portant son ombrelle rouge.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Albert, l’enragé promeneur,</l>
					<l n="10" num="3.2">Qui rappelle, en chantant sa gamme,</l>
					<l n="11" num="3.3">Le prince Hamlet, dans Elseneur,</l>
					<l n="12" num="3.4">La rencontre et lui dit : Madame,</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">Il faut employer les moments</l>
					<l n="14" num="4.2">Sans penser aux futurs désastres.</l>
					<l n="15" num="4.3">Voulez-vous de clairs diamants</l>
					<l n="16" num="4.4">Pareils à des cassures d’astres ?</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1">Entrons là, chez le joaillier ;</l>
					<l n="18" num="5.2">Je veux être certain qu’on m’aime.</l>
					<l n="19" num="5.3">Acceptez un riche collier.</l>
					<l n="20" num="5.4">Céline répond : Tout de même.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1">Oui, dit Albert, nous penserons</l>
					<l n="22" num="6.2">A des rivières sans pareilles</l>
					<l n="23" num="6.3">Et, pendant que nous y serons,</l>
					<l n="24" num="6.4">Nous prendrons des pendants d’oreilles.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1">Mais on va parfois à Choisy !</l>
					<l n="26" num="7.2">Êtes-vous de celles qu’allèche</l>
					<l n="27" num="7.3">Un équipage bien choisi ?</l>
					<l n="28" num="7.4">Bon. Je vous offre une calèche.</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1">Je prétends vous la décocher,</l>
					<l n="30" num="8.2">Svelte et volant comme la foudre,</l>
					<l n="31" num="8.3">Avec chevaux, groom et cocher</l>
					<l n="32" num="8.4">Obèse, rouge sous la poudre.</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1">Voulez-vous, madame, un hôtel</l>
					<l n="34" num="9.2">Tout en briques, dans l’avenue</l>
					<l n="35" num="9.3">De Villiers ? Ce sera l’autel</l>
					<l n="36" num="9.4">Où rira Vénus toute nue.</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1">Et ce n’est pas tout, les poneys !</l>
					<l n="38" num="10.2">Il faut que le soleil arrose</l>
					<l n="39" num="10.3">Chez vous, des tableaux japonais</l>
					<l n="40" num="10.4">Où flambe le ciel rouge et rose.</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1">Céline, qu’afflige une toux</l>
					<l n="42" num="11.2">Sèche, répond : C’est une affaire.</l>
					<l n="43" num="11.3">Cher monsieur, j’accepterai tous</l>
					<l n="44" num="11.4">Les dons que vous voulez me faire.</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1">Et vous ne perdrez pas au troc !</l>
					<l n="46" num="12.2">Jeune homme, pâle comme Oreste,</l>
					<l n="47" num="12.3">C’est bien. Je prendrai tout en bloc,</l>
					<l n="48" num="12.4">Chevaux, diamants et le reste.</l>
				</lg>
				<lg n="13">
					<l n="49" num="13.1">Mais, avec les riches appas</l>
					<l n="50" num="13.2">Qui sont mon armure de guerre,</l>
					<l n="51" num="13.3">Vous ne me reconnaissez pas ?</l>
					<l n="52" num="13.4">Vous m’avez vue enfant naguère.</l>
				</lg>
				<lg n="14">
					<l n="53" num="14.1">Vous me courtisiez déjà, car</l>
					<l n="54" num="14.2">Jamais vous ne vous en privâtes,</l>
					<l n="55" num="14.3">Quand mes pieds nus s’évadaient, par</l>
					<l n="56" num="14.4">Les trous béants de mes savates.</l>
				</lg>
				<lg n="15">
					<l n="57" num="15.1">J’avais l’air d’un jeune filou ;</l>
					<l n="58" num="15.2">Ma peau brune vous semblait douce.</l>
					<l n="59" num="15.3">Je peignais avec un vieux clou</l>
					<l n="60" num="15.4">Ma folle chevelure rousse.</l>
				</lg>
				<lg n="16">
					<l n="61" num="16.1">Et vous, faisant tous les métiers</l>
					<l n="62" num="16.2">Pour un gain souvent illusoire,</l>
					<l n="63" num="16.3">Couchant sous les ponts, vous étiez</l>
					<l n="64" num="16.4">Un petit voyou dérisoire.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1890"> 10 juin 1890.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>