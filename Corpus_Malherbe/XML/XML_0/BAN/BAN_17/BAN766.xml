<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">DANS LA FOURNAISE</title>
				<title type="sub_2">Dernières Poésies</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4181 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_17</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">DANS LA FOURNAISE</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="2001">2001</date>
								</imprint>
								<biblScope unit="tome">VIII</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Dans la fournaise</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>G. Charpentier et E. Fasquelle, Éditeurs</publisher>
							<date when="1892">1892</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1892">1892</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN766">
				<head type="main">Fleur</head>
				<lg n="1">
					<l n="1" num="1.1">Triste comme le prince Hamlet,</l>
					<l n="2" num="1.2">Guy cria d’une façon nette :</l>
					<l n="3" num="1.3">Je vois notre avenir en laid.</l>
					<l n="4" num="1.4">Qu’elle est vieille, notre planète !</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">On y cherchera vainement</l>
					<l n="6" num="2.2">Dans peu de temps la bête fauve,</l>
					<l n="7" num="2.3">Et ce fatal événement</l>
					<l n="8" num="2.4">Se produit : elle devient chauve.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">Pour plaire à nos petits-neveux,</l>
					<l n="10" num="3.2">Étant sans feuillage et sans marbres,</l>
					<l n="11" num="3.3">Comme on se met de faux cheveux</l>
					<l n="12" num="3.4">Elle se mettra de faux arbres.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">Depuis le roi du ciel, Indra,</l>
					<l n="14" num="4.2">Tous les volcans, souffrant d’un asthme,</l>
					<l n="15" num="4.3">Toussent leurs poumons ; il faudra</l>
					<l n="16" num="4.4">Qu’on leur mette un grand cataplasme.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1">Se glaçant, par un triste jeu,</l>
					<l n="18" num="5.2">Des extrémités jusqu’au centre,</l>
					<l n="19" num="5.3">La pauvre Terre, au lieu de feu</l>
					<l n="20" num="5.4">A de la neige dans le ventre.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1">Et c’est là son moindre défaut.</l>
					<l n="22" num="6.2">Depuis que le pic la farfouille</l>
					<l n="23" num="6.3">Elle est vidée, ou peu s’en faut,</l>
					<l n="24" num="6.4">On n’aura bientôt plus de houille.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1">Quant à l’homme, drôle de corps !</l>
					<l n="26" num="7.2">Jusqu’à ce que la mort s’en suive</l>
					<l n="27" num="7.3">Il doit écouter les accords</l>
					<l n="28" num="7.4">Des Huguenots et de la Juive.</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1">Et tant de malheureux ont faim !</l>
					<l n="30" num="8.2">Le ciel est froid, la neige est dure,</l>
					<l n="31" num="8.3">Par l’hiver qui n’a pas de fin.</l>
					<l n="32" num="8.4">Oh ! la bise dans la froidure !</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1">Engin cruel, affreux joyau</l>
					<l n="34" num="9.2">Que la Démence voit en songe,</l>
					<l n="35" num="9.3">En abominable tuyau</l>
					<l n="36" num="9.4">Le sombre acier de Krupp s’allonge.</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1">Et les belles Illusions,</l>
					<l n="38" num="10.2">Engouffrant leurs comiques robes</l>
					<l n="39" num="10.3">Dans le ciel plein de visions,</l>
					<l n="40" num="10.4">Laissent l’homme en proie aux microbes.</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1">On va sans espoir et sans but</l>
					<l n="42" num="11.2">Dans cette ombre mal habitée.</l>
					<l n="43" num="11.3">Il est temps qu’on mette au rebut</l>
					<l n="44" num="11.4">La planète désorbitée.</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1">Tel Guy, sans pitié, ni merci,</l>
					<l n="46" num="12.2">Injuriait l’astre morose.</l>
					<l n="47" num="12.3">Mais comme il s’écriait ainsi,</l>
					<l n="48" num="12.4">Vint à passer la jeune Rose.</l>
				</lg>
				<lg n="13">
					<l n="49" num="13.1">Douce, autour d’elle ruisselait</l>
					<l n="50" num="13.2">Comme une lumière inconnue.</l>
					<l n="51" num="13.3">Elle a seize ans tout juste, elle est</l>
					<l n="52" num="13.4">Folâtre, naïve, ingénue.</l>
				</lg>
				<lg n="14">
					<l n="53" num="14.1">Pétrie avec un peu d’azur</l>
					<l n="54" num="14.2">Ainsi qu’un Ange, elle est de celles</l>
					<l n="55" num="14.3">Dont on admire le front pur.</l>
					<l n="56" num="14.4">Ses yeux d’or sont pleins d’étincelles.</l>
				</lg>
				<lg n="15">
					<l n="57" num="15.1">Pareille au gai matin vermeil,</l>
					<l n="58" num="15.2">Elle est enfantine et superbe</l>
					<l n="59" num="15.3">Et, sous un rayon de soleil,</l>
					<l n="60" num="15.4">Semble un grand lys, fleuri dans l’herbe.</l>
				</lg>
				<lg n="16">
					<l n="61" num="16.1">Regardant cette floraison,</l>
					<l n="62" num="16.2">Je dis à Guy, l’âme ravie ;</l>
					<l n="63" num="16.3">Mon ami, vous avez raison,</l>
					<l n="64" num="16.4">Elle est monotone, la vie.</l>
				</lg>
				<lg n="17">
					<l n="65" num="17.1">Paris, que le songe berçait,</l>
					<l n="66" num="17.2">Comme Ecbatane et comme Tarse,</l>
					<l n="67" num="17.3">Rentre au néant tragique, et c’est</l>
					<l n="68" num="17.4">Toujours la même vieille farce.</l>
				</lg>
				<lg n="18">
					<l n="69" num="18.1">Partout c’est — on n’en sort jamais</l>
					<l n="70" num="18.2">L’orgie écœurante ou le jeûne,</l>
					<l n="71" num="18.3">Et la planète est vieille, mais</l>
					<l n="72" num="18.4">Comme la jeune fille est jeune !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1890"> 23 décembre 1890.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>