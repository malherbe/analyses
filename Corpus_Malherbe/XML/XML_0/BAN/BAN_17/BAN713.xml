<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">DANS LA FOURNAISE</title>
				<title type="sub_2">Dernières Poésies</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4181 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_17</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">DANS LA FOURNAISE</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="2001">2001</date>
								</imprint>
								<biblScope unit="tome">VIII</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Dans la fournaise</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>G. Charpentier et E. Fasquelle, Éditeurs</publisher>
							<date when="1892">1892</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1892">1892</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN713">
				<head type="main">Turbulent</head>
				<opener>
					<salute>A José-Maria de Heredia</salute>
				</opener>
				<lg n="1">
					<l n="1" num="1.1">O vous pour qui toujours le ciel s’irradia,</l>
					<l n="2" num="1.2">Véronèse des mots flambants, Heredia,</l>
					<l n="3" num="1.3">Vous que la Muse fête et suit d’un œil affable,</l>
					<l n="4" num="1.4">Je veux exprès pour vous inventer une fable.</l>
					<l n="5" num="1.5">Jadis au temps du Roi-Soleil, quand Sévigné</l>
					<l n="6" num="1.6">Usurpait tout l’encens de l’Olympe indigné,</l>
					<l n="7" num="1.7">Le dieu qui réveilla les plus secrètes fibres,</l>
					<l n="8" num="1.8">La Fontaine eût conté cette histoire en vers libres,</l>
					<l n="9" num="1.9">En ces vers dont le pas comme un oiseau marchait.</l>
					<l n="10" num="1.10">Mais nous avons perdu son rhythme et son archet,</l>
					<l n="11" num="1.11">Et nous ferions, je pense, une triste figure</l>
					<l n="12" num="1.12">En voulant de son vol imiter l’envergure.</l>
					<l n="13" num="1.13">Donc, pour conter l’exploit d’un jeune malandrin,</l>
					<l n="14" num="1.14">Je me contenterai du vers alexandrin.</l>
					<l n="15" num="1.15">Mais avec sa grandeur, sa flamme et son délire,</l>
					<l n="16" num="1.16">Il suffit à la joie immense de la Lyre,</l>
					<l n="17" num="1.17">Et mon maître, le roi du faste oriental,</l>
					<l n="18" num="1.18">Qui dans l’ardent brasier forgea son dur métal,</l>
					<l n="19" num="1.19">A prouvé qu’il sait être, avec son fier mélange,</l>
					<l n="20" num="1.20">Bon pour Benvenuto comme pour Michel-Ange.</l>
					<l n="21" num="1.21">On en fait, si l’on veut, le glaive aux durs éclairs,</l>
					<l n="22" num="1.22">Ou le joyau qui rit à l’azur des yeux clairs,</l>
					<l n="23" num="1.23">Ou le paillon furtif du svelte funambule.</l>
					<l n="24" num="1.24">Mais, poëte, j’arrête ici mon préambule.</l>
					<l n="25" num="1.25">Voici le fait. Ce dieu, souvent digne du fouet,</l>
					<l n="26" num="1.26">L’enfant Éros avait disloqué son jouet</l>
					<l n="27" num="1.27">Et son pantin funèbre et morne rendait l’âme.</l>
					<l n="28" num="1.28">Ces deux êtres formaient un assemblage infâme,</l>
					<l n="29" num="1.29">L’un pantelant, brisé, tordu, le corps en deux,</l>
					<l n="30" num="1.30">Et l’autre s’acharnant sur des débris hideux.</l>
					<l n="31" num="1.31">Oh ! le pantin ! Ses bras éperdus et fantasques</l>
					<l n="32" num="1.32">Se balançaient, épars, comme des choses flasques,</l>
					<l n="33" num="1.33">Et la langue bleuie était horrible à voir.</l>
					<l n="34" num="1.34">L’enfant tirait toujours le nez tragique et noir</l>
					<l n="35" num="1.35">Du misérable, et fou comme un loup dans son antre,</l>
					<l n="36" num="1.36">Lui fourrait jusqu’au fond ses ongles dans le ventre.</l>
					<l n="37" num="1.37">Sur le beau pavé d’or, à présent méprisés,</l>
					<l n="38" num="1.38">Gisaient partout des traits cassés, des arcs brisés</l>
					<l n="39" num="1.39">Et la chambre de jaspe avait l’air d’un vrai bouge.</l>
					<l n="40" num="1.40">Mais Aphrodite vint et se fâcha tout rouge.</l>
					<l n="41" num="1.41">Oh ! le vrai brise-fer et l’indocile enfant !</l>
					<l n="42" num="1.42">Dit-elle. Donc tu fais tout ce qu’on te défend.</l>
					<l n="43" num="1.43">C’est Massacre et Fureur que le grand ciel te nomme.</l>
					<l n="44" num="1.44">A quoi sert-il d’avoir une mère économe ?</l>
					<l n="45" num="1.45">Va, tes caprices, plus cruels que les autans,</l>
					<l n="46" num="1.46">Nous auront ruinés avant qu’il soit longtemps</l>
					<l n="47" num="1.47">Et nous mourrons de faim dans nos terres en friche.</l>
					<l n="48" num="1.48">Pour le moment, il est certain que je suis riche.</l>
					<l n="49" num="1.49">Mes domaines, trésors toujours inépuisés,</l>
					<l n="50" num="1.50">Sont tous ceux où frémit le doux vol des baisers.</l>
					<l n="51" num="1.51">J’ai Naples, dont jamais le golfe n’est morose,</l>
					<l n="52" num="1.52">Et j’ai Paris et j’ai Venise toute rose.</l>
					<l n="53" num="1.53">Mais au train dont tu vas pour me désespérer,</l>
					<l n="54" num="1.54">Je n’aurai bientôt plus que les yeux pour pleurer.</l>
					<l n="55" num="1.55">Par suite des excès farouches où tu tombes,</l>
					<l n="56" num="1.56">Je n’aurai plus de quoi nourrir mes deux colombes.</l>
					<l n="57" num="1.57">Dans ce pays qu’au ciel bleu nous assimilions,</l>
					<l n="58" num="1.58">Que me restera-t-il ? De vagues millions.</l>
					<l n="59" num="1.59">Et réduite, pleurant mon antique richesse,</l>
					<l n="60" num="1.60">A marcher sur la pourpre ainsi qu’une duchesse,</l>
					<l n="61" num="1.61">On me verra bientôt parer mes bras charmants</l>
					<l n="62" num="1.62">Avec ces cailloux vils qu’on nomme diamants.</l>
					<l n="63" num="1.63">C’est ainsi qu’Aphrodite, en sa douleur amère,</l>
					<l n="64" num="1.64">Se plaignait. Mais Éros lui dit : Ma douce mère,</l>
					<l n="65" num="1.65">Ne gronde pas. Fluide et plus subtile encor,</l>
					<l n="66" num="1.66">La flamme du soleil rit dans tes cheveux d’or.</l>
					<l n="67" num="1.67">A l’avenir, je veux être sage comme une</l>
					<l n="68" num="1.68">Image. On trouvera ma bonté peu commune.</l>
					<l n="69" num="1.69">Jamais plus je n’aurai de cruel appétit</l>
					<l n="70" num="1.70">Et tu voudras encore embrasser ton petit.</l>
					<l n="71" num="1.71">O Mère, il est bien vrai que d’une façon nette</l>
					<l n="72" num="1.72">J’ai démantibulé notre marionnette,</l>
					<l n="73" num="1.73">Mais parfois, ce n’est pas ma faute, mon sang bout.</l>
					<l n="74" num="1.74">J’en ai fait un débris, des loques, rien du tout,</l>
					<l n="75" num="1.75">Un haillon ridicule et triste. Mais, en somme,</l>
					<l n="76" num="1.76">Ce pantin ne valait pas cher. Ce n’est que l’Homme.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1887">Novembre 1887.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>