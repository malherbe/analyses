<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">DANS LA FOURNAISE</title>
				<title type="sub_2">Dernières Poésies</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4181 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_17</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">DANS LA FOURNAISE</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="2001">2001</date>
								</imprint>
								<biblScope unit="tome">VIII</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Dans la fournaise</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>G. Charpentier et E. Fasquelle, Éditeurs</publisher>
							<date when="1892">1892</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1892">1892</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN711">
				<head type="main">Semper adora</head>
				<lg n="1">
					<l n="1" num="1.1">O Maître de la Lyre, aïeul, race d’Homère !</l>
					<l n="2" num="1.2">Hugo, quand tu vivais cette vie éphémère,</l>
					<l n="3" num="1.3">Devant le vaste flot que seul tu remuais,</l>
					<l n="4" num="1.4">Tes envieux restaient stupéfaits et muets.</l>
					<l n="5" num="1.5">Ils ne moissonnaient pas leur haine déjà mûre,</l>
					<l n="6" num="1.6">Et pâles, dans leurs seins, étouffaient leur murmure.</l>
					<l n="7" num="1.7">Maître, quand près de toi, dans un repas divin,</l>
					<l n="8" num="1.8">Nous te parlions, mangeant ton pain, buvant ton vin,</l>
					<l n="9" num="1.9">Quand nous goûtions, hélas ! vieille troupe écolière,</l>
					<l n="10" num="1.10">Tes entretiens charmants de bonté familière,</l>
					<l n="11" num="1.11">Dans notre souvenir devenus solennels ;</l>
					<l n="12" num="1.12">Quand tu nous regardais de tes yeux éternels,</l>
					<l n="13" num="1.13">Te garder, c’est le rêve enivrant que nous fîmes !</l>
					<l n="14" num="1.14">Eux pourtant, devant toi vaincus, domptés, infimes,</l>
					<l n="15" num="1.15">Pleins d’une rage sourde et remâchant leur fiel,</l>
					<l n="16" num="1.16">Petits, ils t’admiraient comme un archer du ciel</l>
					<l n="17" num="1.17">Lançant tes flèches d’or sur les marais immondes,</l>
					<l n="18" num="1.18">Ou portant dans ta main, comme Dieu fait des mondes,</l>
					<l n="19" num="1.19">L’idéal grandiose et la réalité,</l>
					<l n="20" num="1.20">Génie entré vivant dans l’immortalité.</l>
					<l n="21" num="1.21">Mage qui dans les cieux mystérieux sus lire,</l>
					<l n="22" num="1.22">Faisant parler, chanter, frémir toute la Lyre,</l>
					<l n="23" num="1.23">Évoquant dans ta voix les crimes, les bourreaux,</l>
					<l n="24" num="1.24">Les baisers, tout un peuple effrayant de héros,</l>
					<l n="25" num="1.25">Tu nous rendais, parmi nos pleurs et nos désastres,</l>
					<l n="26" num="1.26">En un tas d’odes, plus nombreuses que les astres,</l>
					<l n="27" num="1.27">Les Pindares et les Eschyles disparus,</l>
					<l n="28" num="1.28">Et ne pouvant plus rien ici-bas, tu mourus.</l>
					<l n="29" num="1.29">Les Zoïles bouffons, dont le front vil rougeoie,</l>
					<l n="30" num="1.30">En hurlèrent alors de colère et de joie ;</l>
					<l n="31" num="1.31">Ils crièrent, montrant leurs appétits flagrants :</l>
					<l n="32" num="1.32">A présent qu’il n’est plus, nous pouvons être grands.</l>
					<l n="33" num="1.33">Puisqu’il prenait nos parts d’orgueil et de lumière,</l>
					<l n="34" num="1.34">Brillons ! notre place est à présent la première.</l>
					<l n="35" num="1.35">Nous serions comme lui bientôt, si nous voulions.</l>
					<l n="36" num="1.36">Frères, être un berger d’aigles et de lions,</l>
					<l n="37" num="1.37">Un Hugo, ce n’est pas du tout la mer à boire :</l>
					<l n="38" num="1.38">C’est un peu de génie avec un peu de gloire,</l>
					<l n="39" num="1.39">Et le vent de l’exil parmi des cheveux blancs.</l>
					<l n="40" num="1.40">C’est ainsi que ces nains heureux, jadis tremblants,</l>
					<l n="41" num="1.41">Exultaient. Ils disaient : Tout doit finir, en somme.</l>
					<l n="42" num="1.42">Voici longtemps déjà qu’on admire cet homme.</l>
					<l n="43" num="1.43">Assez. Ne suivez plus la trace de ses pas.</l>
					<l n="44" num="1.44">Allons ailleurs. — Pardon, messieurs, je n’en suis pas.</l>
					<l n="45" num="1.45">Maître, je suis un flot parmi les flots sans nombre ;</l>
					<l n="46" num="1.46">Mais, depuis le matin, j’ai marché dans ton ombre.</l>
					<l n="47" num="1.47">J’ai parfois réfléchi ta lumière, et si peu</l>
					<l n="48" num="1.48">Que je sois, j’ai pu voir en toi l’infini bleu.</l>
					<l n="49" num="1.49">Tant que je vivrai sous les grands cieux qui se dorent,</l>
					<l n="50" num="1.50">O Père, je serai parmi ceux qui t’adorent,</l>
					<l n="51" num="1.51">Fidèles, et s’il n’en reste qu’un, je serai</l>
					<l n="52" num="1.52">Celui-là, plein d’amour et le cœur ulcéré !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1887">Mardi, 26 juillet 1887.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>