<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">DANS LA FOURNAISE</title>
				<title type="sub_2">Dernières Poésies</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4181 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_17</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">DANS LA FOURNAISE</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="2001">2001</date>
								</imprint>
								<biblScope unit="tome">VIII</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Dans la fournaise</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>G. Charpentier et E. Fasquelle, Éditeurs</publisher>
							<date when="1892">1892</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1892">1892</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN710">
				<head type="main">Sous bois</head>
				<lg n="1">
					<l n="1" num="1.1">Un malheureux, il est vrai, bachelier ès lettres,</l>
					<l n="2" num="1.2">Mais fort triste, nourri par les seuls hexamètres,</l>
					<l n="3" num="1.3">Et dans le bois riant, au milieu des ronds d’ifs,</l>
					<l n="4" num="1.4">Hanté par les supins et par les gérondifs,</l>
					<l n="5" num="1.5">Un loqueteux, marqué d’avance pour la tombe,</l>
					<l n="6" num="1.6">Ayant son habit noir plus blanc qu’une colombe,</l>
					<l n="7" num="1.7">Et tordu comme un cep de vigne, un avorton</l>
					<l n="8" num="1.8">Mal venu, tourmentait du bout de son bâton</l>
					<l n="9" num="1.9">Dans l’herbe drue et dans les fleurs, une charogne.</l>
					<l n="10" num="1.10">Ce lettré, mangé par la gale et par la rogne,</l>
					<l n="11" num="1.11">Disait — vain discours, moins murmuré que rêvé :</l>
					<l n="12" num="1.12">Que diable peut-on faire avec un chien crevé ?</l>
					<l n="13" num="1.13">Et songeait combien peu, dans cette pourriture,</l>
					<l n="14" num="1.14">Sourit le bifteck, cher à la littérature.</l>
					<l n="15" num="1.15">Et le Gueux, dont la peste aurait fait son époux,</l>
					<l n="16" num="1.16">Avec son autre main, libre, grattait ses poux.</l>
					<l n="17" num="1.17">A ce moment, parut la belle Cyprienne,</l>
					<l n="18" num="1.18">Glorieuse, avec sa démarche aérienne,</l>
					<l n="19" num="1.19">Qui, voyant le maudit, fit un geste d’horreur.</l>
					<l n="20" num="1.20">Mais il dit : En effet, madame, ce doreur,</l>
					<l n="21" num="1.21">Le matin rougissant, vous baise et vous caresse ;</l>
					<l n="22" num="1.22">Vous êtes Joie, Orgueil, Beauté, Grâce, Paresse ;</l>
					<l n="23" num="1.23">Vos regards fulgurants, pareils à deux brasiers,</l>
					<l n="24" num="1.24">Font palpiter d’amour les cœurs extasiés,</l>
					<l n="25" num="1.25">Et quand on voit les fleurs de vos lèvres éclore,</l>
					<l n="26" num="1.26">On croit facilement que vous êtes l’Aurore.</l>
					<l n="27" num="1.27">Votre chair est pareille à des fleurs de lotus.</l>
					<l n="28" num="1.28">Cléopâtre, sous la figure de Vénus,</l>
					<l n="29" num="1.29">C’est vous-même. C’est vous Hélène, aux jours de Troie.</l>
					<l n="30" num="1.30">Bienheureux le vainqueur dont vous êtes la proie !</l>
					<l n="31" num="1.31">Sur votre sein charmant vous avez plus de lys</l>
					<l n="32" num="1.32">Que n’en ont eus Phryné, Cléopâtre et Laïs ;</l>
					<l n="33" num="1.33">C’est pour vous que Louis, Roi-Soleil, eût pris Dôle,</l>
					<l n="34" num="1.34">Et vous auriez été la femme de Candaule.</l>
					<l n="35" num="1.35">Vivre est délicieux, mais vous voir est plus doux.</l>
					<l n="36" num="1.36">Pourtant, rayon, clarté, perle, souvenez-vous</l>
					<l n="37" num="1.37">Que la rose est mortelle, et que tout se termine</l>
					<l n="38" num="1.38">Par de la pourriture et par de la vermine.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1887">Vendredi, 15 juillet 1887.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>