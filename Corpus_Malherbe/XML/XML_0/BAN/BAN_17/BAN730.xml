<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">DANS LA FOURNAISE</title>
				<title type="sub_2">Dernières Poésies</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4181 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_17</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">DANS LA FOURNAISE</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="2001">2001</date>
								</imprint>
								<biblScope unit="tome">VIII</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Dans la fournaise</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>G. Charpentier et E. Fasquelle, Éditeurs</publisher>
							<date when="1892">1892</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1892">1892</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN730">
				<head type="main">Ballade <lb></lb>Pour Mademoiselle Edmée Daudet</head>
				<lg n="1">
					<l n="1" num="1.1">Dans vos yeux, sur la vie amère</l>
					<l n="2" num="1.2">Brilleront les clairs diamants</l>
					<l n="3" num="1.3">Qu’on voit dans ceux de votre mère.</l>
					<l n="4" num="1.4">Entre mille éblouissements,</l>
					<l n="5" num="1.5">Au milieu des rêves charmants</l>
					<l n="6" num="1.6">Dont se pare la Renommée,</l>
					<l n="7" num="1.7">Vous naissez parmi les romans.</l>
					<l n="8" num="1.8">Bonjour, mademoiselle Edmée.</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1">Bien mieux que la rose éphémère,</l>
					<l n="10" num="2.2">Vos lèvres, ces enchantements,</l>
					<l n="11" num="2.3">Riront à la belle chimère.</l>
					<l n="12" num="2.4">Vos prunelles aux feux dormants</l>
					<l n="13" num="2.5">Ont de vagues rayonnements,</l>
					<l n="14" num="2.6">Comme une lueur allumée</l>
					<l n="15" num="2.7">Aux mystérieux firmaments.</l>
					<l n="16" num="2.8">Bonjour, mademoiselle Edmée.</l>
				</lg>
				<lg n="3">
					<l n="17" num="3.1">Comme, avec un dédain sommaire,</l>
					<l n="18" num="3.2">Le poëte, en ces doux moments,</l>
					<l n="19" num="3.3">Quittant la Muse et sa grammaire,</l>
					<l n="20" num="3.4">A vite oublié les tourments,</l>
					<l n="21" num="3.5">L’orgueil, les applaudissements</l>
					<l n="22" num="3.6">Et la gloire, cette fumée,</l>
					<l n="23" num="3.7">Avec de longs ravissements !</l>
					<l n="24" num="3.8">Bonjour, Mademoiselle Edmée.</l>
				</lg>
				<lg n="4">
					<head type="form">Envoi</head>
					<l n="25" num="4.1">Princesse, des regards aimants</l>
					<l n="26" num="4.2">Fêtent votre chair parfumée</l>
					<l n="27" num="4.3">Et vos tendres vagissements.</l>
					<l n="28" num="4.4">Bonjour, mademoiselle Edmée.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1886">Villa de Banville, mercredi 21 juillet 1886.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>