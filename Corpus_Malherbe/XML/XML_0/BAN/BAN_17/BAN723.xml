<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">DANS LA FOURNAISE</title>
				<title type="sub_2">Dernières Poésies</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4181 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_17</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">DANS LA FOURNAISE</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="2001">2001</date>
								</imprint>
								<biblScope unit="tome">VIII</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Dans la fournaise</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>G. Charpentier et E. Fasquelle, Éditeurs</publisher>
							<date when="1892">1892</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1892">1892</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN723">
				<head type="main">Triomphe</head>
			<opener>
				<salute>A Georges Rochegrosse</salute>
			</opener>
				<lg n="1">
					<l n="1" num="1.1">Cher Georges, vois, je tente un effort hasardeux,</l>
					<l n="2" num="1.2">Et j’ai voulu tâcher de fixer pour nous deux,</l>
					<l n="3" num="1.3">En des vers où frémit la Rime épouvantée,</l>
					<l n="4" num="1.4">L’étrange vision que tu m’as racontée.</l>
					<l n="5" num="1.5">C’est la Débauche. C’est la grande Impure. Elle a</l>
					<l n="6" num="1.6">Des épaules de neige, et cette Dalila</l>
					<l n="7" num="1.7">Avec ses durs ciseaux tranche des chevelures.</l>
					<l n="8" num="1.8">Sa bouche est une braise et fait d’âcres brûlures ;</l>
					<l n="9" num="1.9">Et vieillards, beaux enfants au sourire ingénu</l>
					<l n="10" num="1.10">Et jeunes hommes, tous adorent son sein nu.</l>
					<l n="11" num="1.11">Tous chantent son orgueil et célèbrent sa gloire.</l>
					<l n="12" num="1.12">Ils disent : Si ta coupe est la mort, j’y veux boire.</l>
					<l n="13" num="1.13">Je veux manger ta chair, je veux mordre tes lys !</l>
					<l n="14" num="1.14">Cléopâtre, Astarté, Phryné, Sémiramis,</l>
					<l n="15" num="1.15">Je t’adore ! Je veux dans ta glauque prunelle</l>
					<l n="16" num="1.16">Puiser incessamment la Démence éternelle.</l>
					<l n="17" num="1.17">Et la Dominatrice étonne les cieux clairs</l>
					<l n="18" num="1.18">De ses yeux glorieux, pleins d’astres et d’éclairs ;</l>
					<l n="19" num="1.19">Sa toison folle est comme un boisseau d’or qu’on pèse ;</l>
					<l n="20" num="1.20">Le Désir la caresse et le Regard la baise.</l>
					<l n="21" num="1.21">Les hommes sur ses pas, ainsi qu’un vil troupeau,</l>
					<l n="22" num="1.22">Se pressent, alléchés par l’odeur de sa peau ;</l>
					<l n="23" num="1.23">Elle a, pour triompher dans les apothéoses,</l>
					<l n="24" num="1.24">Sur son front la tiare et sur son flanc des roses.</l>
					<l n="25" num="1.25">Elle marche, riante, au bord des claires eaux ;</l>
					<l n="26" num="1.26">A l’entour de son front voltigent des oiseaux ;</l>
					<l n="27" num="1.27">Des chats voluptueux, des belettes lascives</l>
					<l n="28" num="1.28">La suivent, lentement, en montrant leurs gencives.</l>
					<l n="29" num="1.29">Sur son corsage aux fiers contours, les diamants</l>
					<l n="30" num="1.30">Fleurissent éblouis, en lys blancs et charmants ;</l>
					<l n="31" num="1.31">Derrière elle, avec un murmure qui la flatte,</l>
					<l n="32" num="1.32">Courent les flots pompeux de sa jupe écarlate,</l>
					<l n="33" num="1.33">Et tout en elle est joie, enchantement, parfum.</l>
					<l n="34" num="1.34">Mais tout à coup le vent affolé soulève un</l>
					<l n="35" num="1.35">Coin de sa robe, et sur sa jambe noble et pure</l>
					<l n="36" num="1.36">On peut voir une plaie affreuse qui suppure,</l>
					<l n="37" num="1.37">Toujours humide, avec ses bords jaunes et verts</l>
					<l n="38" num="1.38">Et son écorchement pâle où grouillent des vers.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1886">Lundi, 26 avril 1886.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>